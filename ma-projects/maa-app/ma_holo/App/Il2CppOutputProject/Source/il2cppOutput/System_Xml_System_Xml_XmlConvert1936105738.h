﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1936105738  : public Il2CppObject
{
public:

public:
};

struct XmlConvert_t1936105738_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.XmlConvert::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_0;
	// System.Char[] System.Xml.XmlConvert::crt
	CharU5BU5D_t1328083999* ___crt_1;
	// System.Int32 System.Xml.XmlConvert::c_EncodedCharLength
	int32_t ___c_EncodedCharLength_2;
	// System.Char[] System.Xml.XmlConvert::WhitespaceChars
	CharU5BU5D_t1328083999* ___WhitespaceChars_3;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t1050521405  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t1050521405  value)
	{
		___xmlCharType_0 = value;
	}

	inline static int32_t get_offset_of_crt_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___crt_1)); }
	inline CharU5BU5D_t1328083999* get_crt_1() const { return ___crt_1; }
	inline CharU5BU5D_t1328083999** get_address_of_crt_1() { return &___crt_1; }
	inline void set_crt_1(CharU5BU5D_t1328083999* value)
	{
		___crt_1 = value;
		Il2CppCodeGenWriteBarrier(&___crt_1, value);
	}

	inline static int32_t get_offset_of_c_EncodedCharLength_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___c_EncodedCharLength_2)); }
	inline int32_t get_c_EncodedCharLength_2() const { return ___c_EncodedCharLength_2; }
	inline int32_t* get_address_of_c_EncodedCharLength_2() { return &___c_EncodedCharLength_2; }
	inline void set_c_EncodedCharLength_2(int32_t value)
	{
		___c_EncodedCharLength_2 = value;
	}

	inline static int32_t get_offset_of_WhitespaceChars_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___WhitespaceChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhitespaceChars_3() const { return ___WhitespaceChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhitespaceChars_3() { return &___WhitespaceChars_3; }
	inline void set_WhitespaceChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhitespaceChars_3 = value;
		Il2CppCodeGenWriteBarrier(&___WhitespaceChars_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
