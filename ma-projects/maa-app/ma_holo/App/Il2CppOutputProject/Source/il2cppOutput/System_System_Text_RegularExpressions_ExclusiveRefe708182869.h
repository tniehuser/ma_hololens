﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.RegularExpressions.RegexRunner
struct RegexRunner_t3983612747;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.ExclusiveReference
struct  ExclusiveReference_t708182869  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.ExclusiveReference::_ref
	RegexRunner_t3983612747 * ____ref_0;
	// System.Object System.Text.RegularExpressions.ExclusiveReference::_obj
	Il2CppObject * ____obj_1;
	// System.Int32 System.Text.RegularExpressions.ExclusiveReference::_locked
	int32_t ____locked_2;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____ref_0)); }
	inline RegexRunner_t3983612747 * get__ref_0() const { return ____ref_0; }
	inline RegexRunner_t3983612747 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(RegexRunner_t3983612747 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier(&____ref_0, value);
	}

	inline static int32_t get_offset_of__obj_1() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____obj_1)); }
	inline Il2CppObject * get__obj_1() const { return ____obj_1; }
	inline Il2CppObject ** get_address_of__obj_1() { return &____obj_1; }
	inline void set__obj_1(Il2CppObject * value)
	{
		____obj_1 = value;
		Il2CppCodeGenWriteBarrier(&____obj_1, value);
	}

	inline static int32_t get_offset_of__locked_2() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____locked_2)); }
	inline int32_t get__locked_2() const { return ____locked_2; }
	inline int32_t* get_address_of__locked_2() { return &____locked_2; }
	inline void set__locked_2(int32_t value)
	{
		____locked_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
