﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo2135528789.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate
struct  HoldCompletedEventDelegate_t3501137495  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
