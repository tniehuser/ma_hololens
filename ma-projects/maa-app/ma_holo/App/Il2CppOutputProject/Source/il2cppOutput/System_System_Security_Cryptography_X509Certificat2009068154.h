﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Security_Cryptography_X509Certificat2703153821.h"

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct X509ExtensionCollection_t650873211;
// System.String
struct String_t;
// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t870392;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct X500DistinguishedName_t452415348;
// System.Security.Cryptography.Oid
struct Oid_t3221867120;
// System.Security.Cryptography.X509Certificates.X509CertificateImplCollection
struct X509CertificateImplCollection_t255811311;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t324051958;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono
struct  X509Certificate2ImplMono_t2009068154  : public X509Certificate2Impl_t2703153821
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_archived
	bool ____archived_1;
	// System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_extensions
	X509ExtensionCollection_t650873211 * ____extensions_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_serial
	String_t* ____serial_3;
	// System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_publicKey
	PublicKey_t870392 * ____publicKey_4;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::issuer_name
	X500DistinguishedName_t452415348 * ___issuer_name_5;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::subject_name
	X500DistinguishedName_t452415348 * ___subject_name_6;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::signature_algorithm
	Oid_t3221867120 * ___signature_algorithm_7;
	// System.Security.Cryptography.X509Certificates.X509CertificateImplCollection System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::intermediateCerts
	X509CertificateImplCollection_t255811311 * ___intermediateCerts_8;
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_cert
	X509Certificate_t324051958 * ____cert_9;

public:
	inline static int32_t get_offset_of__archived_1() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____archived_1)); }
	inline bool get__archived_1() const { return ____archived_1; }
	inline bool* get_address_of__archived_1() { return &____archived_1; }
	inline void set__archived_1(bool value)
	{
		____archived_1 = value;
	}

	inline static int32_t get_offset_of__extensions_2() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____extensions_2)); }
	inline X509ExtensionCollection_t650873211 * get__extensions_2() const { return ____extensions_2; }
	inline X509ExtensionCollection_t650873211 ** get_address_of__extensions_2() { return &____extensions_2; }
	inline void set__extensions_2(X509ExtensionCollection_t650873211 * value)
	{
		____extensions_2 = value;
		Il2CppCodeGenWriteBarrier(&____extensions_2, value);
	}

	inline static int32_t get_offset_of__serial_3() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____serial_3)); }
	inline String_t* get__serial_3() const { return ____serial_3; }
	inline String_t** get_address_of__serial_3() { return &____serial_3; }
	inline void set__serial_3(String_t* value)
	{
		____serial_3 = value;
		Il2CppCodeGenWriteBarrier(&____serial_3, value);
	}

	inline static int32_t get_offset_of__publicKey_4() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____publicKey_4)); }
	inline PublicKey_t870392 * get__publicKey_4() const { return ____publicKey_4; }
	inline PublicKey_t870392 ** get_address_of__publicKey_4() { return &____publicKey_4; }
	inline void set__publicKey_4(PublicKey_t870392 * value)
	{
		____publicKey_4 = value;
		Il2CppCodeGenWriteBarrier(&____publicKey_4, value);
	}

	inline static int32_t get_offset_of_issuer_name_5() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___issuer_name_5)); }
	inline X500DistinguishedName_t452415348 * get_issuer_name_5() const { return ___issuer_name_5; }
	inline X500DistinguishedName_t452415348 ** get_address_of_issuer_name_5() { return &___issuer_name_5; }
	inline void set_issuer_name_5(X500DistinguishedName_t452415348 * value)
	{
		___issuer_name_5 = value;
		Il2CppCodeGenWriteBarrier(&___issuer_name_5, value);
	}

	inline static int32_t get_offset_of_subject_name_6() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___subject_name_6)); }
	inline X500DistinguishedName_t452415348 * get_subject_name_6() const { return ___subject_name_6; }
	inline X500DistinguishedName_t452415348 ** get_address_of_subject_name_6() { return &___subject_name_6; }
	inline void set_subject_name_6(X500DistinguishedName_t452415348 * value)
	{
		___subject_name_6 = value;
		Il2CppCodeGenWriteBarrier(&___subject_name_6, value);
	}

	inline static int32_t get_offset_of_signature_algorithm_7() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___signature_algorithm_7)); }
	inline Oid_t3221867120 * get_signature_algorithm_7() const { return ___signature_algorithm_7; }
	inline Oid_t3221867120 ** get_address_of_signature_algorithm_7() { return &___signature_algorithm_7; }
	inline void set_signature_algorithm_7(Oid_t3221867120 * value)
	{
		___signature_algorithm_7 = value;
		Il2CppCodeGenWriteBarrier(&___signature_algorithm_7, value);
	}

	inline static int32_t get_offset_of_intermediateCerts_8() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___intermediateCerts_8)); }
	inline X509CertificateImplCollection_t255811311 * get_intermediateCerts_8() const { return ___intermediateCerts_8; }
	inline X509CertificateImplCollection_t255811311 ** get_address_of_intermediateCerts_8() { return &___intermediateCerts_8; }
	inline void set_intermediateCerts_8(X509CertificateImplCollection_t255811311 * value)
	{
		___intermediateCerts_8 = value;
		Il2CppCodeGenWriteBarrier(&___intermediateCerts_8, value);
	}

	inline static int32_t get_offset_of__cert_9() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____cert_9)); }
	inline X509Certificate_t324051958 * get__cert_9() const { return ____cert_9; }
	inline X509Certificate_t324051958 ** get_address_of__cert_9() { return &____cert_9; }
	inline void set__cert_9(X509Certificate_t324051958 * value)
	{
		____cert_9 = value;
		Il2CppCodeGenWriteBarrier(&____cert_9, value);
	}
};

struct X509Certificate2ImplMono_t2009068154_StaticFields
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::empty_error
	String_t* ___empty_error_10;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::commonName
	ByteU5BU5D_t3397334013* ___commonName_11;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::email
	ByteU5BU5D_t3397334013* ___email_12;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::signedData
	ByteU5BU5D_t3397334013* ___signedData_13;

public:
	inline static int32_t get_offset_of_empty_error_10() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___empty_error_10)); }
	inline String_t* get_empty_error_10() const { return ___empty_error_10; }
	inline String_t** get_address_of_empty_error_10() { return &___empty_error_10; }
	inline void set_empty_error_10(String_t* value)
	{
		___empty_error_10 = value;
		Il2CppCodeGenWriteBarrier(&___empty_error_10, value);
	}

	inline static int32_t get_offset_of_commonName_11() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___commonName_11)); }
	inline ByteU5BU5D_t3397334013* get_commonName_11() const { return ___commonName_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_commonName_11() { return &___commonName_11; }
	inline void set_commonName_11(ByteU5BU5D_t3397334013* value)
	{
		___commonName_11 = value;
		Il2CppCodeGenWriteBarrier(&___commonName_11, value);
	}

	inline static int32_t get_offset_of_email_12() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___email_12)); }
	inline ByteU5BU5D_t3397334013* get_email_12() const { return ___email_12; }
	inline ByteU5BU5D_t3397334013** get_address_of_email_12() { return &___email_12; }
	inline void set_email_12(ByteU5BU5D_t3397334013* value)
	{
		___email_12 = value;
		Il2CppCodeGenWriteBarrier(&___email_12, value);
	}

	inline static int32_t get_offset_of_signedData_13() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___signedData_13)); }
	inline ByteU5BU5D_t3397334013* get_signedData_13() const { return ___signedData_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_signedData_13() { return &___signedData_13; }
	inline void set_signedData_13(ByteU5BU5D_t3397334013* value)
	{
		___signedData_13 = value;
		Il2CppCodeGenWriteBarrier(&___signedData_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
