﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_TerrainData1351141029.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard601950206.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe4226409784.h"
#include "mscorlib_System_Attribute542643598.h"
#include "UnityEngine_UnityEngine_Time31991979.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer10059812.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_EventHan2685920451.h"
#include "mscorlib_System_Int64909078037.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_FrameRea2353988013.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_ErrorEve3983973519.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3303813975.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogni343438253.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo2135528789.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1855824201.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3501137495.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3415544547.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2437856093.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2553296249.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2759974023.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2487153075.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2394416487.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1045620518.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1982950364.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3123342528.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3960847450.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogni831649826.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3116179703.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2343902086.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1972476489.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1509183288.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSou396045678.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSou830383220.h"
#include "UnityEngine_UnityEngine_VR_WSA_Persistence_WorldAn1514582394.h"
#include "UnityEngine_UnityEngine_VR_WSA_Persistence_WorldAn1561547849.h"
#include "UnityEngine_UnityEngine_VR_WSA_PositionalLocatorSt3556528907.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_Serializati3597317192.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor4032480564.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor2381700809.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchorT389490593.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor2482917876.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceChange769713231.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceData3147297845.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceId4207907960.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldAnchor100028935.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver3799331897.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver_Sur3444411290.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver_Sur1375437319.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_CapturePixel3370027235.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1414746886.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture3813459671.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture3943606183.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture2698966894.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture_522077320.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture2742193416.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1784202510.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1623734756.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCaptureF928993319.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3470796049.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3170939459.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture2660512180.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3210647042.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1811850718.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1862037614.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture2612745932.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1976525720.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldAnchor_OnTracki417897799.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldManager2948982693.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldManager_OnPosi3864636813.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_Windows_Speech_ConfidenceL3540086540.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationCom808236964.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRec894834159.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe1495849926.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe1941514337.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe3326551541.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe3730830311.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog1642125421.h"
#include "UnityEngine_UnityEngine_Windows_Speech_SpeechError3947426626.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog3690655641.h"
#include "UnityEngine_UnityEngine_Windows_Speech_SpeechSyste1827393363.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog2739717665.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog3185826360.h"
#include "UnityEngine_UnityEngine_Windows_Speech_SemanticMea2306793223.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog1627945097.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecogn438723648.h"
#include "mscorlib_System_Char3454481338.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420.h"

// UnityEngine.TerrainData
struct TerrainData_t1351141029;
// UnityEngine.TextEditor
struct TextEditor_t3975561390;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.GUIContent
struct GUIContent_t4210063000;
// System.Object
struct Il2CppObject;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t4226409784;
// System.Attribute
struct Attribute_t542643598;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// UnityEngine.TrackedReference
struct TrackedReference_t1045890189;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform/Enumerator
struct Enumerator_t1251553160;
// UnityEngine.UnityException
struct UnityException_t2687879050;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t2685920451;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t2353988013;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t3983973519;
// System.Delegate
struct Delegate_t3022476291;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.VR.WSA.Input.GestureRecognizer
struct GestureRecognizer_t3303813975;
// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate
struct HoldCanceledEventDelegate_t1855824201;
// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate
struct HoldCompletedEventDelegate_t3501137495;
// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate
struct HoldStartedEventDelegate_t3415544547;
// System.ArgumentException
struct ArgumentException_t3259014390;
// UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate
struct TappedEventDelegate_t2437856093;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate
struct ManipulationCanceledEventDelegate_t2553296249;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate
struct ManipulationCompletedEventDelegate_t2759974023;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate
struct ManipulationStartedEventDelegate_t2487153075;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate
struct ManipulationUpdatedEventDelegate_t2394416487;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate
struct NavigationCanceledEventDelegate_t1045620518;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate
struct NavigationCompletedEventDelegate_t1982950364;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate
struct NavigationStartedEventDelegate_t3123342528;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate
struct NavigationUpdatedEventDelegate_t3960847450;
// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate
struct RecognitionEndedEventDelegate_t831649826;
// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate
struct RecognitionStartedEventDelegate_t3116179703;
// UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate
struct GestureErrorDelegate_t2343902086;
// UnityEngine.VR.WSA.Persistence.WorldAnchorStore
struct WorldAnchorStore_t1514582394;
// UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate
struct GetAsyncDelegate_t1561547849;
// UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch
struct WorldAnchorTransferBatch_t4032480564;
// UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate
struct SerializationDataAvailableDelegate_t2381700809;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate
struct SerializationCompleteDelegate_t389490593;
// UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate
struct DeserializationCompleteDelegate_t2482917876;
// UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate
struct SurfaceChangedDelegate_t3444411290;
// UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate
struct SurfaceDataReadyDelegate_t1375437319;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.VR.WSA.WorldAnchor
struct WorldAnchor_t100028935;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.VR.WSA.SurfaceObserver
struct SurfaceObserver_t3799331897;
// UnityEngine.VR.WSA.WebCam.PhotoCapture
struct PhotoCapture_t1414746886;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback
struct OnCaptureResourceCreatedCallback_t2698966894;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback
struct OnPhotoModeStartedCallback_t522077320;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback
struct OnPhotoModeStoppedCallback_t2742193416;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback
struct OnCapturedToDiskCallback_t1784202510;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback
struct OnCapturedToMemoryCallback_t1623734756;
// UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame
struct PhotoCaptureFrame_t928993319;
// UnityEngine.VR.WSA.WebCam.VideoCapture
struct VideoCapture_t3470796049;
// UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback
struct OnVideoCaptureResourceCreatedCallback_t3210647042;
// UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback
struct OnVideoModeStartedCallback_t1811850718;
// UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback
struct OnVideoModeStoppedCallback_t1862037614;
// UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback
struct OnStartedRecordingVideoCallback_t2612745932;
// UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback
struct OnStoppedRecordingVideoCallback_t1976525720;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate
struct OnTrackingChangedDelegate_t417897799;
// UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate
struct OnPositionalLocatorStateChangedDelegate_t3864636813;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t3968615785;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// UnityEngine.Windows.Speech.DictationRecognizer
struct DictationRecognizer_t894834159;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate
struct DictationHypothesisDelegate_t1495849926;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate
struct DictationResultDelegate_t1941514337;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate
struct DictationCompletedDelegate_t3326551541;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler
struct DictationErrorHandler_t3730830311;
// UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate
struct ErrorDelegate_t3690655641;
// UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate
struct StatusDelegate_t2739717665;
// UnityEngine.Windows.Speech.SemanticMeaning
struct SemanticMeaning_t2306793223;
// UnityEngine.Windows.Speech.SemanticMeaning[]
struct SemanticMeaningU5BU5D_t2038511870;
// UnityEngine.Windows.Speech.PhraseRecognizer
struct PhraseRecognizer_t1627945097;
// UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate
struct PhraseRecognizedDelegate_t438723648;
// UnityEngine.WritableAttribute
struct WritableAttribute_t3715198420;
extern Il2CppClass* TerrainData_t1351141029_il2cpp_TypeInfo_var;
extern const uint32_t TerrainData__cctor_m1858770469_MetadataUsageId;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor__ctor_m1990252461_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_op_Equality_m3491334086_MetadataUsageId;
extern Il2CppClass* TrackedReference_t1045890189_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m3153703389_MetadataUsageId;
extern Il2CppClass* Enumerator_t1251553160_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m3479720613_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386007604;
extern const uint32_t UnityException__ctor_m3650417185_MetadataUsageId;
extern const uint32_t UnityException__ctor_m1554762831_MetadataUsageId;
extern const uint32_t UnityException__ctor_m2835958127_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2736546956;
extern const uint32_t Vector2_ToString_m775491729_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Equals_m1405920279_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Equals_m2692262876_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2889564913;
extern const uint32_t Vector3_ToString_m3857189970_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Equals_m3783731577_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Vector4_ToString_m2340321043_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t FrameReadyEventHandler_BeginInvoke_m3022200868_MetadataUsageId;
extern Il2CppClass* GC_t2902933594_il2cpp_TypeInfo_var;
extern const uint32_t GestureRecognizer_Finalize_m2935797859_MetadataUsageId;
extern const uint32_t GestureRecognizer_Dispose_m1417326810_MetadataUsageId;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4065364047;
extern const uint32_t GestureRecognizer_InvokeHoldEvent_m3380740601_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3874384063;
extern const uint32_t GestureRecognizer_InvokeManipulationEvent_m2299514574_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2481806678;
extern const uint32_t GestureRecognizer_InvokeNavigationEvent_m3680868741_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1467117991;
extern const uint32_t GestureRecognizer_InvokeRecognitionEvent_m1997165549_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t GestureErrorDelegate_BeginInvoke_m1907077102_MetadataUsageId;
extern Il2CppClass* InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var;
extern Il2CppClass* Ray_t2469606224_il2cpp_TypeInfo_var;
extern const uint32_t HoldCanceledEventDelegate_BeginInvoke_m2553842636_MetadataUsageId;
extern const uint32_t HoldCompletedEventDelegate_BeginInvoke_m3382701298_MetadataUsageId;
extern const uint32_t HoldStartedEventDelegate_BeginInvoke_m3730235758_MetadataUsageId;
extern const uint32_t ManipulationCanceledEventDelegate_BeginInvoke_m1007525451_MetadataUsageId;
extern const uint32_t ManipulationCompletedEventDelegate_BeginInvoke_m3070802273_MetadataUsageId;
extern const uint32_t ManipulationStartedEventDelegate_BeginInvoke_m682094053_MetadataUsageId;
extern const uint32_t ManipulationUpdatedEventDelegate_BeginInvoke_m1137995473_MetadataUsageId;
extern const uint32_t NavigationCanceledEventDelegate_BeginInvoke_m2652326424_MetadataUsageId;
extern const uint32_t NavigationCompletedEventDelegate_BeginInvoke_m1962981452_MetadataUsageId;
extern const uint32_t NavigationStartedEventDelegate_BeginInvoke_m891507608_MetadataUsageId;
extern const uint32_t NavigationUpdatedEventDelegate_BeginInvoke_m4025316318_MetadataUsageId;
extern const uint32_t RecognitionEndedEventDelegate_BeginInvoke_m789652035_MetadataUsageId;
extern const uint32_t RecognitionStartedEventDelegate_BeginInvoke_m1618128194_MetadataUsageId;
extern const uint32_t TappedEventDelegate_BeginInvoke_m3560818467_MetadataUsageId;
extern const uint32_t WorldAnchorStore__ctor_m388893287_MetadataUsageId;
extern Il2CppClass* WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var;
extern const uint32_t WorldAnchorStore_InvokeGetAsyncDelegate_m1814413383_MetadataUsageId;
extern const uint32_t WorldAnchorStore__cctor_m4175401318_MetadataUsageId;
extern const uint32_t WorldAnchorTransferBatch__ctor_m2842525734_MetadataUsageId;
extern const uint32_t WorldAnchorTransferBatch_Finalize_m3625681664_MetadataUsageId;
extern const uint32_t WorldAnchorTransferBatch_Dispose_m1432692473_MetadataUsageId;
extern Il2CppClass* WorldAnchorTransferBatch_t4032480564_il2cpp_TypeInfo_var;
extern const uint32_t WorldAnchorTransferBatch_InvokeWorldAnchorDeserializationCompleteDelegate_m4057073830_MetadataUsageId;
extern Il2CppClass* SerializationCompletionReason_t3597317192_il2cpp_TypeInfo_var;
extern const uint32_t DeserializationCompleteDelegate_BeginInvoke_m4030138881_MetadataUsageId;
extern const uint32_t SerializationCompleteDelegate_BeginInvoke_m2069317740_MetadataUsageId;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SurfaceObserver_InvokeSurfaceChangedEvent_m2394764740_MetadataUsageId;
extern const uint32_t SurfaceObserver_Finalize_m1712600395_MetadataUsageId;
extern const uint32_t SurfaceObserver_Dispose_m2279662588_MetadataUsageId;
extern Il2CppClass* SurfaceId_t4207907960_il2cpp_TypeInfo_var;
extern Il2CppClass* SurfaceChange_t769713231_il2cpp_TypeInfo_var;
extern Il2CppClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern const uint32_t SurfaceChangedDelegate_BeginInvoke_m2555310712_MetadataUsageId;
extern Il2CppClass* SurfaceData_t3147297845_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t SurfaceDataReadyDelegate_BeginInvoke_m1819939565_MetadataUsageId;
extern Il2CppClass* PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var;
extern Il2CppClass* PhotoCapture_t1414746886_il2cpp_TypeInfo_var;
extern const uint32_t PhotoCapture_MakeCaptureResult_m4180913700_MetadataUsageId;
extern const uint32_t PhotoCapture_InvokeOnCreatedResourceDelegate_m2701547345_MetadataUsageId;
extern const uint32_t PhotoCapture_InvokeOnPhotoModeStartedDelegate_m367056465_MetadataUsageId;
extern const uint32_t PhotoCapture_InvokeOnPhotoModeStoppedDelegate_m2878721745_MetadataUsageId;
extern const uint32_t PhotoCapture_InvokeOnCapturedPhotoToDiskDelegate_m1345926005_MetadataUsageId;
extern Il2CppClass* PhotoCaptureFrame_t928993319_il2cpp_TypeInfo_var;
extern const uint32_t PhotoCapture_InvokeOnCapturedPhotoToMemoryDelegate_m1206852871_MetadataUsageId;
extern const uint32_t PhotoCapture_Dispose_m1060921546_MetadataUsageId;
extern const uint32_t PhotoCapture_Finalize_m1498549843_MetadataUsageId;
extern const uint32_t PhotoCapture__cctor_m3299331514_MetadataUsageId;
extern const uint32_t OnCapturedToDiskCallback_BeginInvoke_m1055237728_MetadataUsageId;
extern const uint32_t OnCapturedToMemoryCallback_BeginInvoke_m1549072172_MetadataUsageId;
extern const uint32_t OnPhotoModeStartedCallback_BeginInvoke_m285549642_MetadataUsageId;
extern const uint32_t OnPhotoModeStoppedCallback_BeginInvoke_m4070686730_MetadataUsageId;
extern const uint32_t PhotoCaptureFrame__ctor_m326376562_MetadataUsageId;
extern const uint32_t PhotoCaptureFrame_Cleanup_m1070424360_MetadataUsageId;
extern const uint32_t PhotoCaptureFrame_Dispose_m3758964267_MetadataUsageId;
extern Il2CppClass* VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var;
extern Il2CppClass* VideoCapture_t3470796049_il2cpp_TypeInfo_var;
extern const uint32_t VideoCapture_MakeCaptureResult_m3906400075_MetadataUsageId;
extern const uint32_t VideoCapture_InvokeOnCreatedVideoCaptureResourceDelegate_m2707758557_MetadataUsageId;
extern const uint32_t VideoCapture_InvokeOnVideoModeStartedDelegate_m1129777589_MetadataUsageId;
extern const uint32_t VideoCapture_InvokeOnVideoModeStoppedDelegate_m4012563765_MetadataUsageId;
extern const uint32_t VideoCapture_InvokeOnStartedRecordingVideoToDiskDelegate_m2101797547_MetadataUsageId;
extern const uint32_t VideoCapture_InvokeOnStoppedRecordingVideoToDiskDelegate_m2832855883_MetadataUsageId;
extern const uint32_t VideoCapture_Dispose_m3064824343_MetadataUsageId;
extern const uint32_t VideoCapture_Finalize_m577118944_MetadataUsageId;
extern const uint32_t VideoCapture__cctor_m2923959343_MetadataUsageId;
extern const uint32_t OnStartedRecordingVideoCallback_BeginInvoke_m3360981378_MetadataUsageId;
extern const uint32_t OnStoppedRecordingVideoCallback_BeginInvoke_m3232744182_MetadataUsageId;
extern const uint32_t OnVideoModeStartedCallback_BeginInvoke_m598044810_MetadataUsageId;
extern const uint32_t OnVideoModeStoppedCallback_BeginInvoke_m1376282954_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t WorldAnchor_Internal_TriggerEventOnTrackingLost_m1446545095_MetadataUsageId;
extern const uint32_t OnTrackingChangedDelegate_BeginInvoke_m4035619098_MetadataUsageId;
extern Il2CppClass* WorldManager_t2948982693_il2cpp_TypeInfo_var;
extern const uint32_t WorldManager_Internal_TriggerPositionalLocatorStateChanged_m3687861698_MetadataUsageId;
extern Il2CppClass* PositionalLocatorState_t3556528907_il2cpp_TypeInfo_var;
extern const uint32_t OnPositionalLocatorStateChangedDelegate_BeginInvoke_m3423894028_MetadataUsageId;
extern const uint32_t DictationRecognizer_Finalize_m2612255109_MetadataUsageId;
extern const uint32_t DictationRecognizer_Dispose_m3128061898_MetadataUsageId;
extern Il2CppClass* DictationCompletionCause_t808236964_il2cpp_TypeInfo_var;
extern const uint32_t DictationCompletedDelegate_BeginInvoke_m1045226018_MetadataUsageId;
extern const uint32_t DictationErrorHandler_BeginInvoke_m3004529127_MetadataUsageId;
extern Il2CppClass* ConfidenceLevel_t3540086540_il2cpp_TypeInfo_var;
extern const uint32_t DictationResultDelegate_BeginInvoke_m2097544744_MetadataUsageId;
extern Il2CppClass* PhraseRecognitionSystem_t1642125421_il2cpp_TypeInfo_var;
extern const uint32_t PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeErrorEvent_m2371098489_MetadataUsageId;
extern const uint32_t PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeStatusChangedEvent_m2976065820_MetadataUsageId;
extern Il2CppClass* SpeechError_t3947426626_il2cpp_TypeInfo_var;
extern const uint32_t ErrorDelegate_BeginInvoke_m1046632338_MetadataUsageId;
extern Il2CppClass* SpeechSystemStatus_t1827393363_il2cpp_TypeInfo_var;
extern const uint32_t StatusDelegate_BeginInvoke_m3757077471_MetadataUsageId;
struct SemanticMeaning_t2306793223_marshaled_pinvoke;
struct SemanticMeaning_t2306793223;;
struct SemanticMeaning_t2306793223_marshaled_pinvoke;;
struct SemanticMeaning_t2306793223_marshaled_com;
struct SemanticMeaning_t2306793223_marshaled_com;;
extern const uint32_t PhraseRecognizer_Finalize_m3514624903_MetadataUsageId;
extern const uint32_t PhraseRecognizer_Dispose_m40972164_MetadataUsageId;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t PhraseRecognizer_InvokePhraseRecognizedEvent_m4062135779_MetadataUsageId;
extern Il2CppClass* SemanticMeaningU5BU5D_t2038511870_il2cpp_TypeInfo_var;
extern Il2CppClass* SemanticMeaning_t2306793223_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU2A_t2911836366_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t PhraseRecognizer_MarshalSemanticMeaning_m621334147_MetadataUsageId;
extern Il2CppClass* PhraseRecognizedEventArgs_t3185826360_il2cpp_TypeInfo_var;
extern const uint32_t PhraseRecognizedDelegate_BeginInvoke_m1044057473_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1606206610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3022476291 * m_Items[1];

public:
	inline Delegate_t3022476291 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t3022476291 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Windows.Speech.SemanticMeaning[]
struct SemanticMeaningU5BU5D_t2038511870  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SemanticMeaning_t2306793223  m_Items[1];

public:
	inline SemanticMeaning_t2306793223  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SemanticMeaning_t2306793223 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SemanticMeaning_t2306793223  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline SemanticMeaning_t2306793223  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SemanticMeaning_t2306793223 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SemanticMeaning_t2306793223  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke(const SemanticMeaning_t2306793223& unmarshaled, SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled);
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_back(const SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled, SemanticMeaning_t2306793223& unmarshaled);
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_cleanup(SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled);
extern "C" void SemanticMeaning_t2306793223_marshal_com(const SemanticMeaning_t2306793223& unmarshaled, SemanticMeaning_t2306793223_marshaled_com& marshaled);
extern "C" void SemanticMeaning_t2306793223_marshal_com_back(const SemanticMeaning_t2306793223_marshaled_com& marshaled, SemanticMeaning_t2306793223& unmarshaled);
extern "C" void SemanticMeaning_t2306793223_marshal_com_cleanup(SemanticMeaning_t2306793223_marshaled_com& marshaled);


// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumResolution_m1726089468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumDetailResolutionPerPatch()
extern "C"  int32_t TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailResolutionPerPatch()
extern "C"  int32_t TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailPatchCount()
extern "C"  int32_t TerrainData_Internal_GetMaximumDetailPatchCount_m611112374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumAlphamapResolution()
extern "C"  int32_t TerrainData_Internal_GetMinimumAlphamapResolution_m412927128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumAlphamapResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumBaseMapResolution()
extern "C"  int32_t TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumBaseMapResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern "C"  GUIStyle_t1799908754 * GUIStyle_get_none_m4224270950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor()
extern "C"  void GUIContent__ctor_m3889310883 (GUIContent_t4210063000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1730479323 (Attribute_t542643598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m1110429671 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m4087144328 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1573482188 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m3491334086 (Il2CppObject * __this /* static, unused */, TrackedReference_t1045890189 * ___x0, TrackedReference_t1045890189 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  int32_t IntPtr_op_Explicit_m1458664696 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m147705785 (Enumerator_t1251553160 * __this, Transform_t3275118058 * ___outer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HResult(System.Int32)
extern "C"  void Exception_set_HResult_m2376998645 (Exception_t1927440687 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String,System.Exception)
extern "C"  void Exception__ctor_m2453009240 (Exception_t1927440687 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1263743648 (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m2949645127 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m775491729 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3102305584 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2353429373 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m3359827399 (float* __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m1405920279 (Vector2_t2243707579 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m1226294581 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1984215297 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m1754570744 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m2692262876 (Vector3_t2243707580 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m3857189970 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1222289168 (Vector4_t2243707581 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1576457715 (Vector4_t2243707581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m3783731577 (Vector4_t2243707581 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2340321043 (Vector4_t2243707581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Video.VideoPlayer/EventHandler::Invoke(UnityEngine.Video.VideoPlayer)
extern "C"  void EventHandler_Invoke_m2873584157 (EventHandler_t2685920451 * __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Int64)
extern "C"  void FrameReadyEventHandler_Invoke_m2483052669 (FrameReadyEventHandler_t2353988013 * __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.String)
extern "C"  void ErrorEventHandler_Invoke_m1545469159 (ErrorEventHandler_t3983973519 * __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3044532593 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void GestureRecognizer_DestroyThreaded_m2059341382 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m953228702 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::Destroy(System.IntPtr)
extern "C"  void GestureRecognizer_Destroy_m735490521 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldCanceledEventDelegate_Invoke_m2676160853 (HoldCanceledEventDelegate_t1855824201 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldCompletedEventDelegate_Invoke_m374647459 (HoldCompletedEventDelegate_t3501137495 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldStartedEventDelegate_Invoke_m969835999 (HoldStartedEventDelegate_t3415544547 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,System.Int32,UnityEngine.Ray)
extern "C"  void TappedEventDelegate_Invoke_m2930607236 (TappedEventDelegate_t2437856093 * __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationCanceledEventDelegate_Invoke_m2601086364 (ManipulationCanceledEventDelegate_t2553296249 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationCompletedEventDelegate_Invoke_m3941201946 (ManipulationCompletedEventDelegate_t2759974023 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationStartedEventDelegate_Invoke_m903963214 (ManipulationStartedEventDelegate_t2487153075 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationUpdatedEventDelegate_Invoke_m3124433824 (ManipulationUpdatedEventDelegate_t2394416487 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationCanceledEventDelegate_Invoke_m2246703729 (NavigationCanceledEventDelegate_t1045620518 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationCompletedEventDelegate_Invoke_m2138941927 (NavigationCompletedEventDelegate_t1982950364 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationStartedEventDelegate_Invoke_m601783779 (NavigationStartedEventDelegate_t3123342528 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationUpdatedEventDelegate_Invoke_m3550528331 (NavigationUpdatedEventDelegate_t3960847450 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void RecognitionEndedEventDelegate_Invoke_m3665837954 (RecognitionEndedEventDelegate_t831649826 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void RecognitionStartedEventDelegate_Invoke_m3422656115 (RecognitionStartedEventDelegate_t3116179703 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate::Invoke(System.String,System.Int32)
extern "C"  void GestureErrorDelegate_Invoke_m639061785 (GestureErrorDelegate_t2343902086 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.VR.WSA.Input.InteractionSource::get_id()
extern "C"  uint32_t InteractionSource_get_id_m208794706 (InteractionSource_t1972476489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.Input.InteractionSourceKind UnityEngine.VR.WSA.Input.InteractionSource::get_kind()
extern "C"  int32_t InteractionSource_get_kind_m590471592 (InteractionSource_t1972476489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceLocation::TryGetPosition(UnityEngine.Vector3&)
extern "C"  bool InteractionSourceLocation_TryGetPosition_m1370729813 (InteractionSourceLocation_t1509183288 * __this, Vector3_t2243707580 * ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceLocation::TryGetVelocity(UnityEngine.Vector3&)
extern "C"  bool InteractionSourceLocation_TryGetVelocity_m989728567 (InteractionSourceLocation_t1509183288 * __this, Vector3_t2243707580 * ___velocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_sourceLossRisk()
extern "C"  double InteractionSourceProperties_get_sourceLossRisk_m3412787295 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_sourceLossMitigationDirection()
extern "C"  Vector3_t2243707580  InteractionSourceProperties_get_sourceLossMitigationDirection_m3544872833 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.Input.InteractionSourceLocation UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_location()
extern "C"  InteractionSourceLocation_t1509183288  InteractionSourceProperties_get_location_m1069792109 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceState::get_pressed()
extern "C"  bool InteractionSourceState_get_pressed_m310769651 (InteractionSourceState_t830383220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.Input.InteractionSourceProperties UnityEngine.VR.WSA.Input.InteractionSourceState::get_properties()
extern "C"  InteractionSourceProperties_t396045678  InteractionSourceState_get_properties_m3535152895 (InteractionSourceState_t830383220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.Input.InteractionSource UnityEngine.VR.WSA.Input.InteractionSourceState::get_source()
extern "C"  InteractionSource_t1972476489  InteractionSourceState_get_source_m3299051932 (InteractionSourceState_t830383220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.VR.WSA.Input.InteractionSourceState::get_headRay()
extern "C"  Ray_t2469606224  InteractionSourceState_get_headRay_m802244013 (InteractionSourceState_t830383220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore::.ctor(System.IntPtr)
extern "C"  void WorldAnchorStore__ctor_m388893287 (WorldAnchorStore_t1514582394 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate::Invoke(UnityEngine.VR.WSA.Persistence.WorldAnchorStore)
extern "C"  void GetAsyncDelegate_Invoke_m3132607376 (GetAsyncDelegate_t1561547849 * __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::Dispose_Internal(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch_Dispose_Internal_m3965827083 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate::Invoke(System.Byte[])
extern "C"  void SerializationDataAvailableDelegate_Invoke_m3283329308 (SerializationDataAvailableDelegate_t2381700809 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate::Invoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason)
extern "C"  void SerializationCompleteDelegate_Invoke_m4015421603 (SerializationCompleteDelegate_t389490593 * __this, int32_t ___completionReason0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::.ctor(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch__ctor_m2842525734 (WorldAnchorTransferBatch_t4032480564 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate::Invoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason,UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch)
extern "C"  void DeserializationCompleteDelegate_Invoke_m4282608372 (DeserializationCompleteDelegate_t2482917876 * __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::FromFileTime(System.Int64)
extern "C"  DateTime_t693205669  DateTime_FromFileTime_m725937452 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate::Invoke(UnityEngine.VR.WSA.SurfaceId,UnityEngine.VR.WSA.SurfaceChange,UnityEngine.Bounds,System.DateTime)
extern "C"  void SurfaceChangedDelegate_Invoke_m1835305981 (SurfaceChangedDelegate_t3444411290 * __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate::Invoke(UnityEngine.VR.WSA.SurfaceData,System.Boolean,System.Single)
extern "C"  void SurfaceDataReadyDelegate_Invoke_m1482963986 (SurfaceDataReadyDelegate_t1375437319 * __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.SurfaceObserver::DestroyThreaded(System.IntPtr)
extern "C"  void SurfaceObserver_DestroyThreaded_m424102712 (Il2CppObject * __this /* static, unused */, IntPtr_t ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.SurfaceObserver::Destroy(System.IntPtr)
extern "C"  void SurfaceObserver_Destroy_m2667935701 (Il2CppObject * __this /* static, unused */, IntPtr_t ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture)
extern "C"  void OnCaptureResourceCreatedCallback_Invoke_m3100503923 (OnCaptureResourceCreatedCallback_t2698966894 * __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::.ctor(System.IntPtr)
extern "C"  void PhotoCapture__ctor_m845114063 (PhotoCapture_t1414746886 * __this, IntPtr_t ___nativeCaptureObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult UnityEngine.VR.WSA.WebCam.PhotoCapture::MakeCaptureResult(System.Int64)
extern "C"  PhotoCaptureResult_t3813459671  PhotoCapture_MakeCaptureResult_m4180913700 (Il2CppObject * __this /* static, unused */, int64_t ___hResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnPhotoModeStartedCallback_Invoke_m2244798845 (OnPhotoModeStartedCallback_t522077320 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnPhotoModeStoppedCallback_Invoke_m2835537 (OnPhotoModeStoppedCallback_t2742193416 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnCapturedToDiskCallback_Invoke_m2804421377 (OnCapturedToDiskCallback_t1784202510 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::.ctor(System.IntPtr)
extern "C"  void PhotoCaptureFrame__ctor_m326376562 (PhotoCaptureFrame_t928993319 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame)
extern "C"  void OnCapturedToMemoryCallback_Invoke_m1564803831 (OnCapturedToMemoryCallback_t1623734756 * __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::Dispose_Internal(System.IntPtr)
extern "C"  void PhotoCapture_Dispose_Internal_m374635362 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void PhotoCapture_DisposeThreaded_Internal_m3747816943 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetDataLength(System.IntPtr)
extern "C"  int32_t PhotoCaptureFrame_GetDataLength_m1647924026 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_dataLength(System.Int32)
extern "C"  void PhotoCaptureFrame_set_dataLength_m297082938 (PhotoCaptureFrame_t928993319 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetHasLocationData(System.IntPtr)
extern "C"  bool PhotoCaptureFrame_GetHasLocationData_m1030592023 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_hasLocationData(System.Boolean)
extern "C"  void PhotoCaptureFrame_set_hasLocationData_m822935925 (PhotoCaptureFrame_t928993319 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.WebCam.CapturePixelFormat UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetCapturePixelFormat(System.IntPtr)
extern "C"  int32_t PhotoCaptureFrame_GetCapturePixelFormat_m549361216 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_pixelFormat(UnityEngine.VR.WSA.WebCam.CapturePixelFormat)
extern "C"  void PhotoCaptureFrame_set_pixelFormat_m1931590910 (PhotoCaptureFrame_t928993319 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::get_dataLength()
extern "C"  int32_t PhotoCaptureFrame_get_dataLength_m393412179 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::AddMemoryPressure(System.Int64)
extern "C"  void GC_AddMemoryPressure_m4007918840 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::RemoveMemoryPressure(System.Int64)
extern "C"  void GC_RemoveMemoryPressure_m3910440451 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Dispose_Internal(System.IntPtr)
extern "C"  void PhotoCaptureFrame_Dispose_Internal_m3212242885 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Cleanup()
extern "C"  void PhotoCaptureFrame_Cleanup_m1070424360 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture)
extern "C"  void OnVideoCaptureResourceCreatedCallback_Invoke_m742532456 (OnVideoCaptureResourceCreatedCallback_t3210647042 * __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::.ctor(System.IntPtr)
extern "C"  void VideoCapture__ctor_m2979903562 (VideoCapture_t3470796049 * __this, IntPtr_t ___nativeCaptureObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult UnityEngine.VR.WSA.WebCam.VideoCapture::MakeCaptureResult(System.Int64)
extern "C"  VideoCaptureResult_t3170939459  VideoCapture_MakeCaptureResult_m3906400075 (Il2CppObject * __this /* static, unused */, int64_t ___hResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnVideoModeStartedCallback_Invoke_m1419327261 (OnVideoModeStartedCallback_t1811850718 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnVideoModeStoppedCallback_Invoke_m2621135601 (OnVideoModeStoppedCallback_t1862037614 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnStartedRecordingVideoCallback_Invoke_m3078230371 (OnStartedRecordingVideoCallback_t2612745932 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnStoppedRecordingVideoCallback_Invoke_m421367107 (OnStoppedRecordingVideoCallback_t1976525720 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::Dispose_Internal(System.IntPtr)
extern "C"  void VideoCapture_Dispose_Internal_m369542261 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoCaptureObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void VideoCapture_DisposeThreaded_Internal_m3056527932 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoCaptureObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate::Invoke(UnityEngine.VR.WSA.WorldAnchor,System.Boolean)
extern "C"  void OnTrackingChangedDelegate_Invoke_m1976992465 (OnTrackingChangedDelegate_t417897799 * __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate::Invoke(UnityEngine.VR.WSA.PositionalLocatorState,UnityEngine.VR.WSA.PositionalLocatorState)
extern "C"  void OnPositionalLocatorStateChangedDelegate_Invoke_m3729624897 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m2014522928 (YieldInstruction_t3462875981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void DictationRecognizer_DestroyThreaded_m4202694898 (Il2CppObject * __this /* static, unused */, IntPtr_t ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::Destroy(System.IntPtr)
extern "C"  void DictationRecognizer_Destroy_m2946358739 (Il2CppObject * __this /* static, unused */, IntPtr_t ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate::Invoke(System.String)
extern "C"  void DictationHypothesisDelegate_Invoke_m254632730 (DictationHypothesisDelegate_t1495849926 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate::Invoke(System.String,UnityEngine.Windows.Speech.ConfidenceLevel)
extern "C"  void DictationResultDelegate_Invoke_m415359993 (DictationResultDelegate_t1941514337 * __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate::Invoke(UnityEngine.Windows.Speech.DictationCompletionCause)
extern "C"  void DictationCompletedDelegate_Invoke_m1200484595 (DictationCompletedDelegate_t3326551541 * __this, int32_t ___cause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler::Invoke(System.String,System.Int32)
extern "C"  void DictationErrorHandler_Invoke_m665668646 (DictationErrorHandler_t3730830311 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate::Invoke(UnityEngine.Windows.Speech.SpeechError)
extern "C"  void ErrorDelegate_Invoke_m370336905 (ErrorDelegate_t3690655641 * __this, int32_t ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate::Invoke(UnityEngine.Windows.Speech.SpeechSystemStatus)
extern "C"  void StatusDelegate_Invoke_m4078514336 (StatusDelegate_t2739717665 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognizedEventArgs::.ctor(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,UnityEngine.Windows.Speech.SemanticMeaning[],System.DateTime,System.TimeSpan)
extern "C"  void PhraseRecognizedEventArgs__ctor_m1949420707 (PhraseRecognizedEventArgs_t3185826360 * __this, String_t* ___text0, int32_t ___confidence1, SemanticMeaningU5BU5D_t2038511870* ___semanticMeanings2, DateTime_t693205669  ___phraseStartTime3, TimeSpan_t3430258949  ___phraseDuration4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void PhraseRecognizer_DestroyThreaded_m1399474592 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::Destroy(System.IntPtr)
extern "C"  void PhraseRecognizer_Destroy_m3239968945 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.TimeSpan::FromTicks(System.Int64)
extern "C"  TimeSpan_t3430258949  TimeSpan_FromTicks_m827965597 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate::Invoke(UnityEngine.Windows.Speech.PhraseRecognizedEventArgs)
extern "C"  void PhraseRecognizedDelegate_Invoke_m2632448538 (PhraseRecognizedDelegate_t438723648 * __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m1073656736 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::CreateString(System.Char*)
extern "C"  String_t* String_CreateString_m4236499327 (String_t* __this, Il2CppChar* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumResolution_m1726089468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMaximumResolution_m1726089468_ftn) ();
	static TerrainData_Internal_GetMaximumResolution_m1726089468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMaximumResolution_m1726089468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMaximumResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumDetailResolutionPerPatch()
extern "C"  int32_t TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712_ftn) ();
	static TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMinimumDetailResolutionPerPatch()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailResolutionPerPatch()
extern "C"  int32_t TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530_ftn) ();
	static TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMaximumDetailResolutionPerPatch()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumDetailPatchCount()
extern "C"  int32_t TerrainData_Internal_GetMaximumDetailPatchCount_m611112374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMaximumDetailPatchCount_m611112374_ftn) ();
	static TerrainData_Internal_GetMaximumDetailPatchCount_m611112374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMaximumDetailPatchCount_m611112374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMaximumDetailPatchCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumAlphamapResolution()
extern "C"  int32_t TerrainData_Internal_GetMinimumAlphamapResolution_m412927128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMinimumAlphamapResolution_m412927128_ftn) ();
	static TerrainData_Internal_GetMinimumAlphamapResolution_m412927128_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMinimumAlphamapResolution_m412927128_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMinimumAlphamapResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumAlphamapResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202_ftn) ();
	static TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMaximumAlphamapResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMinimumBaseMapResolution()
extern "C"  int32_t TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607_ftn) ();
	static TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMinimumBaseMapResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.TerrainData::Internal_GetMaximumBaseMapResolution()
extern "C"  int32_t TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397_ftn) ();
	static TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::Internal_GetMaximumBaseMapResolution()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.TerrainData::GetAlphamapResolutionInternal()
extern "C"  float TerrainData_GetAlphamapResolutionInternal_m2290069727 (TerrainData_t1351141029 * __this, const MethodInfo* method)
{
	typedef float (*TerrainData_GetAlphamapResolutionInternal_m2290069727_ftn) (TerrainData_t1351141029 *);
	static TerrainData_GetAlphamapResolutionInternal_m2290069727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TerrainData_GetAlphamapResolutionInternal_m2290069727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TerrainData::GetAlphamapResolutionInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TerrainData::.cctor()
extern "C"  void TerrainData__cctor_m1858770469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TerrainData__cctor_m1858770469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = TerrainData_Internal_GetMaximumResolution_m1726089468(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMaximumResolution_2(L_0);
		int32_t L_1 = TerrainData_Internal_GetMinimumDetailResolutionPerPatch_m4256328712(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMinimumDetailResolutionPerPatch_3(L_1);
		int32_t L_2 = TerrainData_Internal_GetMaximumDetailResolutionPerPatch_m2220059530(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMaximumDetailResolutionPerPatch_4(L_2);
		int32_t L_3 = TerrainData_Internal_GetMaximumDetailPatchCount_m611112374(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMaximumDetailPatchCount_5(L_3);
		int32_t L_4 = TerrainData_Internal_GetMinimumAlphamapResolution_m412927128(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMinimumAlphamapResolution_6(L_4);
		int32_t L_5 = TerrainData_Internal_GetMaximumAlphamapResolution_m2965232202(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMaximumAlphamapResolution_7(L_5);
		int32_t L_6 = TerrainData_Internal_GetMinimumBaseMapResolution_m3854864607(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMinimumBaseMapResolution_8(L_6);
		int32_t L_7 = TerrainData_Internal_GetMaximumBaseMapResolution_m2656985397(NULL /*static, unused*/, /*hidden argument*/NULL);
		((TerrainData_t1351141029_StaticFields*)TerrainData_t1351141029_il2cpp_TypeInfo_var->static_fields)->set_kMaximumBaseMapResolution_9(L_7);
		return;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern "C"  void TextEditor__ctor_m1990252461 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m1990252461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_keyboardOnScreen_0((TouchScreenKeyboard_t601950206 *)NULL);
		__this->set_controlID_1(0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_0 = GUIStyle_get_none_m4224270950(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_style_2(L_0);
		__this->set_multiline_3((bool)0);
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_isPasswordField_5((bool)0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_6(L_1);
		GUIContent_t4210063000 * L_2 = (GUIContent_t4210063000 *)il2cpp_codegen_object_new(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3889310883(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_7(L_2);
		__this->set_m_CursorIndex_8(0);
		__this->set_m_SelectIndex_9(0);
		__this->set_m_RevealCursor_10((bool)0);
		__this->set_m_MouseDragSelectsWholeWords_11((bool)0);
		__this->set_m_DblClickInitPos_12(0);
		__this->set_m_DblClickSnap_13(0);
		__this->set_m_bJustSelected_14((bool)0);
		__this->set_m_iAltCursorPos_15((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m3736564847 (ThreadAndSerializationSafeAttribute_t4226409784 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m357614587 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m357614587_ftn) ();
	static Time_get_realtimeSinceStartup_m357614587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m357614587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m1110429671 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m1110429671_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_Destroy_m1110429671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m1110429671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m2608266435 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m1110429671(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_pinvoke(const TrackedReference_t1045890189& unmarshaled, TrackedReference_t1045890189_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_back(const TrackedReference_t1045890189_marshaled_pinvoke& marshaled, TrackedReference_t1045890189& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_cleanup(TrackedReference_t1045890189_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_com(const TrackedReference_t1045890189& unmarshaled, TrackedReference_t1045890189_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t1045890189_marshal_com_back(const TrackedReference_t1045890189_marshaled_com& marshaled, TrackedReference_t1045890189& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_com_cleanup(TrackedReference_t1045890189_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m3491334086 (Il2CppObject * __this /* static, unused */, TrackedReference_t1045890189 * ___x0, TrackedReference_t1045890189 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m3491334086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	{
		TrackedReference_t1045890189 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t1045890189 * L_1 = ___y1;
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0067;
	}

IL_0018:
	{
		Il2CppObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		TrackedReference_t1045890189 * L_5 = ___x0;
		NullCheck(L_5);
		IntPtr_t L_6 = L_5->get_m_Ptr_0();
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_8 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_0067;
	}

IL_0034:
	{
		Il2CppObject * L_9 = V_0;
		if (L_9)
		{
			goto IL_0050;
		}
	}
	{
		TrackedReference_t1045890189 * L_10 = ___y1;
		NullCheck(L_10);
		IntPtr_t L_11 = L_10->get_m_Ptr_0();
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_13 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		goto IL_0067;
	}

IL_0050:
	{
		TrackedReference_t1045890189 * L_14 = ___x0;
		NullCheck(L_14);
		IntPtr_t L_15 = L_14->get_m_Ptr_0();
		TrackedReference_t1045890189 * L_16 = ___y1;
		NullCheck(L_16);
		IntPtr_t L_17 = L_16->get_m_Ptr_0();
		bool L_18 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		goto IL_0067;
	}

IL_0067:
	{
		bool L_19 = V_2;
		return L_19;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern "C"  bool TrackedReference_Equals_m3153703389 (TrackedReference_t1045890189 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m3153703389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m3491334086(NULL /*static, unused*/, ((TrackedReference_t1045890189 *)IsInstClass(L_0, TrackedReference_t1045890189_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m811248179 (TrackedReference_t1045890189 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m1458664696(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m881385315_ftn) (Transform_t3275118058 *);
	static Transform_get_childCount_m881385315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m881385315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m3479720613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Enumerator_t1251553160 * L_0 = (Enumerator_t1251553160 *)il2cpp_codegen_object_new(Enumerator_t1251553160_il2cpp_TypeInfo_var);
		Enumerator__ctor_m147705785(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Transform_GetChild_m3838588184_ftn) (Transform_t3275118058 *, int32_t);
	static Transform_GetChild_m3838588184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m3838588184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m147705785 (Enumerator_t1251553160 * __this, Transform_t3275118058 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2520481711 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3980662062 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Transform_t3275118058 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m881385315(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		goto IL_0027;
	}

IL_0027:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m950879083 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern "C"  void UnityException__ctor_m3650417185 (UnityException_t2687879050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m3650417185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(__this, _stringLiteral386007604, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m1554762831 (UnityException_t2687879050 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m1554762831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C"  void UnityException__ctor_m2835958127 (UnityException_t2687879050 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m2835958127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m2453009240(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m2949645127 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t3614634134* L_1 = ___args1;
		String_t* L_2 = String_Format_m1263743648(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m3067419446_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	Vector2__ctor_m3067419446(_thisAdjusted, ___x0, ___y1, method);
}
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m775491729 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m775491729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral2736546956, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Vector2_ToString_m775491729_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_ToString_m775491729(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2353429373 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_002c;
	}

IL_002c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t Vector2_GetHashCode_m2353429373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_GetHashCode_m2353429373(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m1405920279 (Vector2_t2243707579 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m1405920279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector2_t2243707579_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_004c;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_1, Vector2_t2243707579_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004c;
	}

IL_004c:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Vector2_Equals_m1405920279_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_Equals_m1405920279(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m1226294581 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0022;
	}

IL_0022:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m1226294581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m1226294581(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1984215297 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t2243707579  L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m4168854394 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2243707579  L_0 = ___lhs0;
		Vector2_t2243707579  L_1 = ___rhs1;
		Vector2_t2243707579  L_2 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_sqrMagnitude_m1226294581((&V_0), /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0016;
	}

IL_0016:
	{
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m2638739322_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3__ctor_m2638739322(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m1754570744 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
		goto IL_0040;
	}

IL_0040:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
extern "C"  int32_t Vector3_GetHashCode_m1754570744_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_GetHashCode_m1754570744(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m2692262876 (Vector3_t2243707580 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m2692262876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector3_t2243707580_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0063;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_1, Vector3_t2243707580_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005c;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_005d;
	}

IL_005c:
	{
		G_B6_0 = 0;
	}

IL_005d:
	{
		V_0 = (bool)G_B6_0;
		goto IL_0063;
	}

IL_0063:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
extern "C"  bool Vector3_Equals_m2692262876_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_Equals_m2692262876(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001b;
	}

IL_001b:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m3857189970 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m3857189970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral2889564913, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Vector3_ToString_m3857189970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_ToString_m3857189970(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1222289168 (Vector4_t2243707581 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m1222289168_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	Vector4__ctor_m1222289168(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1576457715 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Vector4_GetHashCode_m1576457715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_GetHashCode_m1576457715(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m3783731577 (Vector4_t2243707581 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m3783731577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector4_t2243707581_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_1, Vector4_t2243707581_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Vector4_Equals_m3783731577_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_Equals_m3783731577(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2340321043 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m2340321043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Vector4_ToString_m2340321043_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_ToString_m2340321043(_thisAdjusted, method);
}
// System.Void UnityEngine.Video.VideoPlayer::InvokePrepareCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern "C"  void VideoPlayer_InvokePrepareCompletedCallback_Internal_m2209413459 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		EventHandler_t2685920451 * L_1 = L_0->get_prepareCompleted_2();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		EventHandler_t2685920451 * L_3 = L_2->get_prepareCompleted_2();
		VideoPlayer_t10059812 * L_4 = ___source0;
		NullCheck(L_3);
		EventHandler_Invoke_m2873584157(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeFrameReadyCallback_Internal(UnityEngine.Video.VideoPlayer,System.Int64)
extern "C"  void VideoPlayer_InvokeFrameReadyCallback_Internal_m3788999635 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		FrameReadyEventHandler_t2353988013 * L_1 = L_0->get_frameReady_8();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		FrameReadyEventHandler_t2353988013 * L_3 = L_2->get_frameReady_8();
		VideoPlayer_t10059812 * L_4 = ___source0;
		int64_t L_5 = ___frameIdx1;
		NullCheck(L_3);
		FrameReadyEventHandler_Invoke_m2483052669(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeLoopPointReachedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern "C"  void VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1495616359 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		EventHandler_t2685920451 * L_1 = L_0->get_loopPointReached_3();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		EventHandler_t2685920451 * L_3 = L_2->get_loopPointReached_3();
		VideoPlayer_t10059812 * L_4 = ___source0;
		NullCheck(L_3);
		EventHandler_Invoke_m2873584157(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeStartedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern "C"  void VideoPlayer_InvokeStartedCallback_Internal_m303286616 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		EventHandler_t2685920451 * L_1 = L_0->get_started_4();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		EventHandler_t2685920451 * L_3 = L_2->get_started_4();
		VideoPlayer_t10059812 * L_4 = ___source0;
		NullCheck(L_3);
		EventHandler_Invoke_m2873584157(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeFrameDroppedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern "C"  void VideoPlayer_InvokeFrameDroppedCallback_Internal_m4234239774 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		EventHandler_t2685920451 * L_1 = L_0->get_frameDropped_5();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		EventHandler_t2685920451 * L_3 = L_2->get_frameDropped_5();
		VideoPlayer_t10059812 * L_4 = ___source0;
		NullCheck(L_3);
		EventHandler_Invoke_m2873584157(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeErrorReceivedCallback_Internal(UnityEngine.Video.VideoPlayer,System.String)
extern "C"  void VideoPlayer_InvokeErrorReceivedCallback_Internal_m4209059756 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, String_t* ___errorStr1, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		ErrorEventHandler_t3983973519 * L_1 = L_0->get_errorReceived_6();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		ErrorEventHandler_t3983973519 * L_3 = L_2->get_errorReceived_6();
		VideoPlayer_t10059812 * L_4 = ___source0;
		String_t* L_5 = ___errorStr1;
		NullCheck(L_3);
		ErrorEventHandler_Invoke_m1545469159(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer::InvokeSeekCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern "C"  void VideoPlayer_InvokeSeekCompletedCallback_Internal_m2526020444 (Il2CppObject * __this /* static, unused */, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	{
		VideoPlayer_t10059812 * L_0 = ___source0;
		NullCheck(L_0);
		EventHandler_t2685920451 * L_1 = L_0->get_seekCompleted_7();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		VideoPlayer_t10059812 * L_2 = ___source0;
		NullCheck(L_2);
		EventHandler_t2685920451 * L_3 = L_2->get_seekCompleted_7();
		VideoPlayer_t10059812 * L_4 = ___source0;
		NullCheck(L_3);
		EventHandler_Invoke_m2873584157(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorEventHandler__ctor_m2587793811 (ErrorEventHandler_t3983973519 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.String)
extern "C"  void ErrorEventHandler_Invoke_m1545469159 (ErrorEventHandler_t3983973519 * __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___message1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___message1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, String_t* ___message1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___source0, ___message1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, String_t* ___message1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___source0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Video.VideoPlayer/ErrorEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorEventHandler_BeginInvoke_m1584132788 (ErrorEventHandler_t3983973519 * __this, VideoPlayer_t10059812 * ___source0, String_t* ___message1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___source0;
	__d_args[1] = ___message1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorEventHandler_EndInvoke_m3824161421 (ErrorEventHandler_t3983973519 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Video.VideoPlayer/EventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void EventHandler__ctor_m2074097083 (EventHandler_t2685920451 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Video.VideoPlayer/EventHandler::Invoke(UnityEngine.Video.VideoPlayer)
extern "C"  void EventHandler_Invoke_m2873584157 (EventHandler_t2685920451 * __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___source0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Video.VideoPlayer/EventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EventHandler_BeginInvoke_m1209029734 (EventHandler_t2685920451 * __this, VideoPlayer_t10059812 * ___source0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___source0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Video.VideoPlayer/EventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void EventHandler_EndInvoke_m3709665097 (EventHandler_t2685920451 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void FrameReadyEventHandler__ctor_m1740305025 (FrameReadyEventHandler_t2353988013 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Int64)
extern "C"  void FrameReadyEventHandler_Invoke_m2483052669 (FrameReadyEventHandler_t2353988013 * __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___frameIdx1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___frameIdx1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int64_t ___frameIdx1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___source0, ___frameIdx1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___frameIdx1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___frameIdx1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int64_t ___frameIdx1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___source0, ___frameIdx1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Int64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FrameReadyEventHandler_BeginInvoke_m3022200868 (FrameReadyEventHandler_t2353988013 * __this, VideoPlayer_t10059812 * ___source0, int64_t ___frameIdx1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FrameReadyEventHandler_BeginInvoke_m3022200868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___source0;
	__d_args[1] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___frameIdx1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void FrameReadyEventHandler_EndInvoke_m759749803 (FrameReadyEventHandler_t2353988013 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::Finalize()
extern "C"  void GestureRecognizer_Finalize_m2935797859 (GestureRecognizer_t3303813975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_Finalize_m2935797859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Recognizer_15();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0034;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_Recognizer_15();
			GestureRecognizer_DestroyThreaded_m2059341382(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_Recognizer_15(L_4);
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::Dispose()
extern "C"  void GestureRecognizer_Dispose_m1417326810 (GestureRecognizer_t3303813975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_Dispose_m1417326810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_Recognizer_15();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_Recognizer_15();
		GestureRecognizer_Destroy_m735490521(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Recognizer_15(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeHoldEvent(UnityEngine.VR.WSA.Input.GestureRecognizer/GestureEventType,UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void GestureRecognizer_InvokeHoldEvent_m3380740601 (GestureRecognizer_t3303813975 * __this, int32_t ___eventType0, int32_t ___source1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_InvokeHoldEvent_m3380740601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HoldCanceledEventDelegate_t1855824201 * V_0 = NULL;
	HoldCompletedEventDelegate_t3501137495 * V_1 = NULL;
	HoldStartedEventDelegate_t3415544547 * V_2 = NULL;
	{
		int32_t L_0 = ___eventType0;
		switch (((int32_t)((int32_t)L_0-(int32_t)1)))
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0056;
			}
		}
	}
	{
		goto IL_0074;
	}

IL_001a:
	{
		HoldCanceledEventDelegate_t1855824201 * L_1 = __this->get_HoldCanceledEvent_0();
		V_0 = L_1;
		HoldCanceledEventDelegate_t1855824201 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		HoldCanceledEventDelegate_t1855824201 * L_3 = V_0;
		int32_t L_4 = ___source1;
		Ray_t2469606224  L_5 = ___headRay2;
		NullCheck(L_3);
		HoldCanceledEventDelegate_Invoke_m2676160853(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0032:
	{
		goto IL_007f;
	}

IL_0038:
	{
		HoldCompletedEventDelegate_t3501137495 * L_6 = __this->get_HoldCompletedEvent_1();
		V_1 = L_6;
		HoldCompletedEventDelegate_t3501137495 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		HoldCompletedEventDelegate_t3501137495 * L_8 = V_1;
		int32_t L_9 = ___source1;
		Ray_t2469606224  L_10 = ___headRay2;
		NullCheck(L_8);
		HoldCompletedEventDelegate_Invoke_m374647459(L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		goto IL_007f;
	}

IL_0056:
	{
		HoldStartedEventDelegate_t3415544547 * L_11 = __this->get_HoldStartedEvent_2();
		V_2 = L_11;
		HoldStartedEventDelegate_t3415544547 * L_12 = V_2;
		if (!L_12)
		{
			goto IL_006e;
		}
	}
	{
		HoldStartedEventDelegate_t3415544547 * L_13 = V_2;
		int32_t L_14 = ___source1;
		Ray_t2469606224  L_15 = ___headRay2;
		NullCheck(L_13);
		HoldStartedEventDelegate_Invoke_m969835999(L_13, L_14, L_15, /*hidden argument*/NULL);
	}

IL_006e:
	{
		goto IL_007f;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_16 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_16, _stringLiteral4065364047, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeTapEvent(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.Int32)
extern "C"  void GestureRecognizer_InvokeTapEvent_m3523911585 (GestureRecognizer_t3303813975 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, int32_t ___tapCount2, const MethodInfo* method)
{
	TappedEventDelegate_t2437856093 * V_0 = NULL;
	{
		TappedEventDelegate_t2437856093 * L_0 = __this->get_TappedEvent_3();
		V_0 = L_0;
		TappedEventDelegate_t2437856093 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		TappedEventDelegate_t2437856093 * L_2 = V_0;
		int32_t L_3 = ___source0;
		int32_t L_4 = ___tapCount2;
		Ray_t2469606224  L_5 = ___headRay1;
		NullCheck(L_2);
		TappedEventDelegate_Invoke_m2930607236(L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeManipulationEvent(UnityEngine.VR.WSA.Input.GestureRecognizer/GestureEventType,UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void GestureRecognizer_InvokeManipulationEvent_m2299514574 (GestureRecognizer_t3303813975 * __this, int32_t ___eventType0, int32_t ___source1, Vector3_t2243707580  ___position2, Ray_t2469606224  ___headRay3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_InvokeManipulationEvent_m2299514574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ManipulationCanceledEventDelegate_t2553296249 * V_0 = NULL;
	ManipulationCompletedEventDelegate_t2759974023 * V_1 = NULL;
	ManipulationStartedEventDelegate_t2487153075 * V_2 = NULL;
	ManipulationUpdatedEventDelegate_t2394416487 * V_3 = NULL;
	{
		int32_t L_0 = ___eventType0;
		switch (((int32_t)((int32_t)L_0-(int32_t)5)))
		{
			case 0:
			{
				goto IL_001e;
			}
			case 1:
			{
				goto IL_003e;
			}
			case 2:
			{
				goto IL_005e;
			}
			case 3:
			{
				goto IL_007e;
			}
		}
	}
	{
		goto IL_009e;
	}

IL_001e:
	{
		ManipulationCanceledEventDelegate_t2553296249 * L_1 = __this->get_ManipulationCanceledEvent_4();
		V_0 = L_1;
		ManipulationCanceledEventDelegate_t2553296249 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		ManipulationCanceledEventDelegate_t2553296249 * L_3 = V_0;
		int32_t L_4 = ___source1;
		Vector3_t2243707580  L_5 = ___position2;
		Ray_t2469606224  L_6 = ___headRay3;
		NullCheck(L_3);
		ManipulationCanceledEventDelegate_Invoke_m2601086364(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		goto IL_00a9;
	}

IL_003e:
	{
		ManipulationCompletedEventDelegate_t2759974023 * L_7 = __this->get_ManipulationCompletedEvent_5();
		V_1 = L_7;
		ManipulationCompletedEventDelegate_t2759974023 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0058;
		}
	}
	{
		ManipulationCompletedEventDelegate_t2759974023 * L_9 = V_1;
		int32_t L_10 = ___source1;
		Vector3_t2243707580  L_11 = ___position2;
		Ray_t2469606224  L_12 = ___headRay3;
		NullCheck(L_9);
		ManipulationCompletedEventDelegate_Invoke_m3941201946(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0058:
	{
		goto IL_00a9;
	}

IL_005e:
	{
		ManipulationStartedEventDelegate_t2487153075 * L_13 = __this->get_ManipulationStartedEvent_6();
		V_2 = L_13;
		ManipulationStartedEventDelegate_t2487153075 * L_14 = V_2;
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		ManipulationStartedEventDelegate_t2487153075 * L_15 = V_2;
		int32_t L_16 = ___source1;
		Vector3_t2243707580  L_17 = ___position2;
		Ray_t2469606224  L_18 = ___headRay3;
		NullCheck(L_15);
		ManipulationStartedEventDelegate_Invoke_m903963214(L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0078:
	{
		goto IL_00a9;
	}

IL_007e:
	{
		ManipulationUpdatedEventDelegate_t2394416487 * L_19 = __this->get_ManipulationUpdatedEvent_7();
		V_3 = L_19;
		ManipulationUpdatedEventDelegate_t2394416487 * L_20 = V_3;
		if (!L_20)
		{
			goto IL_0098;
		}
	}
	{
		ManipulationUpdatedEventDelegate_t2394416487 * L_21 = V_3;
		int32_t L_22 = ___source1;
		Vector3_t2243707580  L_23 = ___position2;
		Ray_t2469606224  L_24 = ___headRay3;
		NullCheck(L_21);
		ManipulationUpdatedEventDelegate_Invoke_m3124433824(L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
	}

IL_0098:
	{
		goto IL_00a9;
	}

IL_009e:
	{
		ArgumentException_t3259014390 * L_25 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_25, _stringLiteral3874384063, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeNavigationEvent(UnityEngine.VR.WSA.Input.GestureRecognizer/GestureEventType,UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void GestureRecognizer_InvokeNavigationEvent_m3680868741 (GestureRecognizer_t3303813975 * __this, int32_t ___eventType0, int32_t ___source1, Vector3_t2243707580  ___relativePosition2, Ray_t2469606224  ___headRay3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_InvokeNavigationEvent_m3680868741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NavigationCanceledEventDelegate_t1045620518 * V_0 = NULL;
	NavigationCompletedEventDelegate_t1982950364 * V_1 = NULL;
	NavigationStartedEventDelegate_t3123342528 * V_2 = NULL;
	NavigationUpdatedEventDelegate_t3960847450 * V_3 = NULL;
	{
		int32_t L_0 = ___eventType0;
		switch (((int32_t)((int32_t)L_0-(int32_t)((int32_t)9))))
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_005f;
			}
			case 3:
			{
				goto IL_007f;
			}
		}
	}
	{
		goto IL_009f;
	}

IL_001f:
	{
		NavigationCanceledEventDelegate_t1045620518 * L_1 = __this->get_NavigationCanceledEvent_8();
		V_0 = L_1;
		NavigationCanceledEventDelegate_t1045620518 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		NavigationCanceledEventDelegate_t1045620518 * L_3 = V_0;
		int32_t L_4 = ___source1;
		Vector3_t2243707580  L_5 = ___relativePosition2;
		Ray_t2469606224  L_6 = ___headRay3;
		NullCheck(L_3);
		NavigationCanceledEventDelegate_Invoke_m2246703729(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		goto IL_00aa;
	}

IL_003f:
	{
		NavigationCompletedEventDelegate_t1982950364 * L_7 = __this->get_NavigationCompletedEvent_9();
		V_1 = L_7;
		NavigationCompletedEventDelegate_t1982950364 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		NavigationCompletedEventDelegate_t1982950364 * L_9 = V_1;
		int32_t L_10 = ___source1;
		Vector3_t2243707580  L_11 = ___relativePosition2;
		Ray_t2469606224  L_12 = ___headRay3;
		NullCheck(L_9);
		NavigationCompletedEventDelegate_Invoke_m2138941927(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0059:
	{
		goto IL_00aa;
	}

IL_005f:
	{
		NavigationStartedEventDelegate_t3123342528 * L_13 = __this->get_NavigationStartedEvent_10();
		V_2 = L_13;
		NavigationStartedEventDelegate_t3123342528 * L_14 = V_2;
		if (!L_14)
		{
			goto IL_0079;
		}
	}
	{
		NavigationStartedEventDelegate_t3123342528 * L_15 = V_2;
		int32_t L_16 = ___source1;
		Vector3_t2243707580  L_17 = ___relativePosition2;
		Ray_t2469606224  L_18 = ___headRay3;
		NullCheck(L_15);
		NavigationStartedEventDelegate_Invoke_m601783779(L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0079:
	{
		goto IL_00aa;
	}

IL_007f:
	{
		NavigationUpdatedEventDelegate_t3960847450 * L_19 = __this->get_NavigationUpdatedEvent_11();
		V_3 = L_19;
		NavigationUpdatedEventDelegate_t3960847450 * L_20 = V_3;
		if (!L_20)
		{
			goto IL_0099;
		}
	}
	{
		NavigationUpdatedEventDelegate_t3960847450 * L_21 = V_3;
		int32_t L_22 = ___source1;
		Vector3_t2243707580  L_23 = ___relativePosition2;
		Ray_t2469606224  L_24 = ___headRay3;
		NullCheck(L_21);
		NavigationUpdatedEventDelegate_Invoke_m3550528331(L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
	}

IL_0099:
	{
		goto IL_00aa;
	}

IL_009f:
	{
		ArgumentException_t3259014390 * L_25 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_25, _stringLiteral2481806678, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeRecognitionEvent(UnityEngine.VR.WSA.Input.GestureRecognizer/GestureEventType,UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void GestureRecognizer_InvokeRecognitionEvent_m1997165549 (GestureRecognizer_t3303813975 * __this, int32_t ___eventType0, int32_t ___source1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureRecognizer_InvokeRecognitionEvent_m1997165549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecognitionEndedEventDelegate_t831649826 * V_0 = NULL;
	RecognitionStartedEventDelegate_t3116179703 * V_1 = NULL;
	{
		int32_t L_0 = ___eventType0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)14))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___eventType0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)13))))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0052;
	}

IL_0016:
	{
		RecognitionEndedEventDelegate_t831649826 * L_2 = __this->get_RecognitionEndedEvent_12();
		V_0 = L_2;
		RecognitionEndedEventDelegate_t831649826 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		RecognitionEndedEventDelegate_t831649826 * L_4 = V_0;
		int32_t L_5 = ___source1;
		Ray_t2469606224  L_6 = ___headRay2;
		NullCheck(L_4);
		RecognitionEndedEventDelegate_Invoke_m3665837954(L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		goto IL_005d;
	}

IL_0034:
	{
		RecognitionStartedEventDelegate_t3116179703 * L_7 = __this->get_RecognitionStartedEvent_13();
		V_1 = L_7;
		RecognitionStartedEventDelegate_t3116179703 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		RecognitionStartedEventDelegate_t3116179703 * L_9 = V_1;
		int32_t L_10 = ___source1;
		Ray_t2469606224  L_11 = ___headRay2;
		NullCheck(L_9);
		RecognitionStartedEventDelegate_Invoke_m3422656115(L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_004c:
	{
		goto IL_005d;
	}

IL_0052:
	{
		ArgumentException_t3259014390 * L_12 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_12, _stringLiteral1467117991, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005d:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::InvokeErrorEvent(System.String,System.Int32)
extern "C"  void GestureRecognizer_InvokeErrorEvent_m2507623944 (GestureRecognizer_t3303813975 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	GestureErrorDelegate_t2343902086 * V_0 = NULL;
	{
		GestureErrorDelegate_t2343902086 * L_0 = __this->get_GestureErrorEvent_14();
		V_0 = L_0;
		GestureErrorDelegate_t2343902086 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		GestureErrorDelegate_t2343902086 * L_2 = V_0;
		String_t* L_3 = ___error0;
		int32_t L_4 = ___hresult1;
		NullCheck(L_2);
		GestureErrorDelegate_Invoke_m639061785(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::Destroy(System.IntPtr)
extern "C"  void GestureRecognizer_Destroy_m735490521 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method)
{
	typedef void (*GestureRecognizer_Destroy_m735490521_ftn) (IntPtr_t);
	static GestureRecognizer_Destroy_m735490521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GestureRecognizer_Destroy_m735490521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.Input.GestureRecognizer::Destroy(System.IntPtr)");
	_il2cpp_icall_func(___recognizer0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void GestureRecognizer_DestroyThreaded_m2059341382 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method)
{
	typedef void (*GestureRecognizer_DestroyThreaded_m2059341382_ftn) (IntPtr_t);
	static GestureRecognizer_DestroyThreaded_m2059341382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GestureRecognizer_DestroyThreaded_m2059341382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.Input.GestureRecognizer::DestroyThreaded(System.IntPtr)");
	_il2cpp_icall_func(___recognizer0);
}
extern "C"  void DelegatePInvokeWrapper_GestureErrorDelegate_t2343902086 (GestureErrorDelegate_t2343902086 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled, ___hresult1);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GestureErrorDelegate__ctor_m2428331680 (GestureErrorDelegate_t2343902086 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate::Invoke(System.String,System.Int32)
extern "C"  void GestureErrorDelegate_Invoke_m639061785 (GestureErrorDelegate_t2343902086 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate::BeginInvoke(System.String,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GestureErrorDelegate_BeginInvoke_m1907077102 (GestureErrorDelegate_t2343902086 * __this, String_t* ___error0, int32_t ___hresult1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GestureErrorDelegate_BeginInvoke_m1907077102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___error0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___hresult1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void GestureErrorDelegate_EndInvoke_m945044694 (GestureErrorDelegate_t2343902086 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_HoldCanceledEventDelegate_t1855824201 (HoldCanceledEventDelegate_t1855824201 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___headRay1);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void HoldCanceledEventDelegate__ctor_m1518078557 (HoldCanceledEventDelegate_t1855824201 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldCanceledEventDelegate_Invoke_m2676160853 (HoldCanceledEventDelegate_t1855824201 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HoldCanceledEventDelegate_BeginInvoke_m2553842636 (HoldCanceledEventDelegate_t1855824201 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HoldCanceledEventDelegate_BeginInvoke_m2553842636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void HoldCanceledEventDelegate_EndInvoke_m1978338831 (HoldCanceledEventDelegate_t1855824201 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_HoldCompletedEventDelegate_t3501137495 (HoldCompletedEventDelegate_t3501137495 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___headRay1);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void HoldCompletedEventDelegate__ctor_m2929741419 (HoldCompletedEventDelegate_t3501137495 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldCompletedEventDelegate_Invoke_m374647459 (HoldCompletedEventDelegate_t3501137495 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HoldCompletedEventDelegate_BeginInvoke_m3382701298 (HoldCompletedEventDelegate_t3501137495 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HoldCompletedEventDelegate_BeginInvoke_m3382701298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void HoldCompletedEventDelegate_EndInvoke_m2068695397 (HoldCompletedEventDelegate_t3501137495 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_HoldStartedEventDelegate_t3415544547 (HoldStartedEventDelegate_t3415544547 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___headRay1);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void HoldStartedEventDelegate__ctor_m1081727879 (HoldStartedEventDelegate_t3415544547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void HoldStartedEventDelegate_Invoke_m969835999 (HoldStartedEventDelegate_t3415544547 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HoldStartedEventDelegate_BeginInvoke_m3730235758 (HoldStartedEventDelegate_t3415544547 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HoldStartedEventDelegate_BeginInvoke_m3730235758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void HoldStartedEventDelegate_EndInvoke_m1237530313 (HoldStartedEventDelegate_t3415544547 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ManipulationCanceledEventDelegate_t2553296249 (ManipulationCanceledEventDelegate_t2553296249 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___cumulativeDelta1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ManipulationCanceledEventDelegate__ctor_m1026233357 (ManipulationCanceledEventDelegate_t2553296249 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationCanceledEventDelegate_Invoke_m2601086364 (ManipulationCanceledEventDelegate_t2553296249 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ManipulationCanceledEventDelegate_BeginInvoke_m1007525451 (ManipulationCanceledEventDelegate_t2553296249 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManipulationCanceledEventDelegate_BeginInvoke_m1007525451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___cumulativeDelta1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ManipulationCanceledEventDelegate_EndInvoke_m1443363199 (ManipulationCanceledEventDelegate_t2553296249 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ManipulationCompletedEventDelegate_t2759974023 (ManipulationCompletedEventDelegate_t2759974023 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___cumulativeDelta1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ManipulationCompletedEventDelegate__ctor_m3034722747 (ManipulationCompletedEventDelegate_t2759974023 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationCompletedEventDelegate_Invoke_m3941201946 (ManipulationCompletedEventDelegate_t2759974023 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ManipulationCompletedEventDelegate_BeginInvoke_m3070802273 (ManipulationCompletedEventDelegate_t2759974023 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManipulationCompletedEventDelegate_BeginInvoke_m3070802273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___cumulativeDelta1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ManipulationCompletedEventDelegate_EndInvoke_m3071382325 (ManipulationCompletedEventDelegate_t2759974023 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ManipulationStartedEventDelegate_t2487153075 (ManipulationStartedEventDelegate_t2487153075 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___cumulativeDelta1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ManipulationStartedEventDelegate__ctor_m1720894263 (ManipulationStartedEventDelegate_t2487153075 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationStartedEventDelegate_Invoke_m903963214 (ManipulationStartedEventDelegate_t2487153075 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ManipulationStartedEventDelegate_BeginInvoke_m682094053 (ManipulationStartedEventDelegate_t2487153075 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManipulationStartedEventDelegate_BeginInvoke_m682094053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___cumulativeDelta1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ManipulationStartedEventDelegate_EndInvoke_m1059813369 (ManipulationStartedEventDelegate_t2487153075 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ManipulationUpdatedEventDelegate_t2394416487 (ManipulationUpdatedEventDelegate_t2394416487 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___cumulativeDelta1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ManipulationUpdatedEventDelegate__ctor_m1547632399 (ManipulationUpdatedEventDelegate_t2394416487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void ManipulationUpdatedEventDelegate_Invoke_m3124433824 (ManipulationUpdatedEventDelegate_t2394416487 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___cumulativeDelta1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ManipulationUpdatedEventDelegate_BeginInvoke_m1137995473 (ManipulationUpdatedEventDelegate_t2394416487 * __this, int32_t ___source0, Vector3_t2243707580  ___cumulativeDelta1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManipulationUpdatedEventDelegate_BeginInvoke_m1137995473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___cumulativeDelta1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ManipulationUpdatedEventDelegate_EndInvoke_m1418595205 (ManipulationUpdatedEventDelegate_t2394416487 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_NavigationCanceledEventDelegate_t1045620518 (NavigationCanceledEventDelegate_t1045620518 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___normalizedOffset1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void NavigationCanceledEventDelegate__ctor_m2346535194 (NavigationCanceledEventDelegate_t1045620518 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationCanceledEventDelegate_Invoke_m2246703729 (NavigationCanceledEventDelegate_t1045620518 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NavigationCanceledEventDelegate_BeginInvoke_m2652326424 (NavigationCanceledEventDelegate_t1045620518 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavigationCanceledEventDelegate_BeginInvoke_m2652326424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___normalizedOffset1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void NavigationCanceledEventDelegate_EndInvoke_m3825903956 (NavigationCanceledEventDelegate_t1045620518 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_NavigationCompletedEventDelegate_t1982950364 (NavigationCompletedEventDelegate_t1982950364 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___normalizedOffset1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void NavigationCompletedEventDelegate__ctor_m669833806 (NavigationCompletedEventDelegate_t1982950364 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationCompletedEventDelegate_Invoke_m2138941927 (NavigationCompletedEventDelegate_t1982950364 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NavigationCompletedEventDelegate_BeginInvoke_m1962981452 (NavigationCompletedEventDelegate_t1982950364 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavigationCompletedEventDelegate_BeginInvoke_m1962981452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___normalizedOffset1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void NavigationCompletedEventDelegate_EndInvoke_m666349120 (NavigationCompletedEventDelegate_t1982950364 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_NavigationStartedEventDelegate_t3123342528 (NavigationStartedEventDelegate_t3123342528 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___normalizedOffset1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void NavigationStartedEventDelegate__ctor_m2979897538 (NavigationStartedEventDelegate_t3123342528 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationStartedEventDelegate_Invoke_m601783779 (NavigationStartedEventDelegate_t3123342528 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NavigationStartedEventDelegate_BeginInvoke_m891507608 (NavigationStartedEventDelegate_t3123342528 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavigationStartedEventDelegate_BeginInvoke_m891507608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___normalizedOffset1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void NavigationStartedEventDelegate_EndInvoke_m1524997068 (NavigationStartedEventDelegate_t3123342528 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_NavigationUpdatedEventDelegate_t3960847450 (NavigationUpdatedEventDelegate_t3960847450 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Vector3_t2243707580 , Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___normalizedOffset1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void NavigationUpdatedEventDelegate__ctor_m2061757380 (NavigationUpdatedEventDelegate_t3960847450 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray)
extern "C"  void NavigationUpdatedEventDelegate_Invoke_m3550528331 (NavigationUpdatedEventDelegate_t3960847450 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___normalizedOffset1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Vector3,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NavigationUpdatedEventDelegate_BeginInvoke_m4025316318 (NavigationUpdatedEventDelegate_t3960847450 * __this, int32_t ___source0, Vector3_t2243707580  ___normalizedOffset1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavigationUpdatedEventDelegate_BeginInvoke_m4025316318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___normalizedOffset1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void NavigationUpdatedEventDelegate_EndInvoke_m662585202 (NavigationUpdatedEventDelegate_t3960847450 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_RecognitionEndedEventDelegate_t831649826 (RecognitionEndedEventDelegate_t831649826 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___headRay1);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecognitionEndedEventDelegate__ctor_m2302119162 (RecognitionEndedEventDelegate_t831649826 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void RecognitionEndedEventDelegate_Invoke_m3665837954 (RecognitionEndedEventDelegate_t831649826 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecognitionEndedEventDelegate_BeginInvoke_m789652035 (RecognitionEndedEventDelegate_t831649826 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RecognitionEndedEventDelegate_BeginInvoke_m789652035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecognitionEndedEventDelegate_EndInvoke_m2755240920 (RecognitionEndedEventDelegate_t831649826 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_RecognitionStartedEventDelegate_t3116179703 (RecognitionStartedEventDelegate_t3116179703 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___headRay1);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecognitionStartedEventDelegate__ctor_m2346976043 (RecognitionStartedEventDelegate_t3116179703 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray)
extern "C"  void RecognitionStartedEventDelegate_Invoke_m3422656115 (RecognitionStartedEventDelegate_t3116179703 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, Ray_t2469606224  ___headRay1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___headRay1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecognitionStartedEventDelegate_BeginInvoke_m1618128194 (RecognitionStartedEventDelegate_t3116179703 * __this, int32_t ___source0, Ray_t2469606224  ___headRay1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RecognitionStartedEventDelegate_BeginInvoke_m1618128194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecognitionStartedEventDelegate_EndInvoke_m924804325 (RecognitionStartedEventDelegate_t3116179703 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_TappedEventDelegate_t2437856093 (TappedEventDelegate_t2437856093 * __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, int32_t, Ray_t2469606224 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___source0, ___tapCount1, ___headRay2);

}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void TappedEventDelegate__ctor_m1195445713 (TappedEventDelegate_t2437856093 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate::Invoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,System.Int32,UnityEngine.Ray)
extern "C"  void TappedEventDelegate_Invoke_m2930607236 (TappedEventDelegate_t2437856093 * __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___source0, ___tapCount1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___source0, ___tapCount1, ___headRay2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___source0, ___tapCount1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___source0, ___tapCount1, ___headRay2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate::BeginInvoke(UnityEngine.VR.WSA.Input.InteractionSourceKind,System.Int32,UnityEngine.Ray,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TappedEventDelegate_BeginInvoke_m3560818467 (TappedEventDelegate_t2437856093 * __this, int32_t ___source0, int32_t ___tapCount1, Ray_t2469606224  ___headRay2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TappedEventDelegate_BeginInvoke_m3560818467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(InteractionSourceKind_t2135528789_il2cpp_TypeInfo_var, &___source0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___tapCount1);
	__d_args[2] = Box(Ray_t2469606224_il2cpp_TypeInfo_var, &___headRay2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void TappedEventDelegate_EndInvoke_m1557694011 (TappedEventDelegate_t2437856093 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.UInt32 UnityEngine.VR.WSA.Input.InteractionSource::get_id()
extern "C"  uint32_t InteractionSource_get_id_m208794706 (InteractionSource_t1972476489 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		uint32_t L_0 = __this->get_m_id_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		uint32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  uint32_t InteractionSource_get_id_m208794706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSource_t1972476489 * _thisAdjusted = reinterpret_cast<InteractionSource_t1972476489 *>(__this + 1);
	return InteractionSource_get_id_m208794706(_thisAdjusted, method);
}
// UnityEngine.VR.WSA.Input.InteractionSourceKind UnityEngine.VR.WSA.Input.InteractionSource::get_kind()
extern "C"  int32_t InteractionSource_get_kind_m590471592 (InteractionSource_t1972476489 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_kind_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t InteractionSource_get_kind_m590471592_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSource_t1972476489 * _thisAdjusted = reinterpret_cast<InteractionSource_t1972476489 *>(__this + 1);
	return InteractionSource_get_kind_m590471592(_thisAdjusted, method);
}
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceLocation::TryGetPosition(UnityEngine.Vector3&)
extern "C"  bool InteractionSourceLocation_TryGetPosition_m1370729813 (InteractionSourceLocation_t1509183288 * __this, Vector3_t2243707580 * ___position0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580 * L_0 = ___position0;
		Vector3_t2243707580  L_1 = __this->get_m_position_1();
		(*(Vector3_t2243707580 *)L_0) = L_1;
		uint8_t L_2 = __this->get_m_hasPosition_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001f:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool InteractionSourceLocation_TryGetPosition_m1370729813_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580 * ___position0, const MethodInfo* method)
{
	InteractionSourceLocation_t1509183288 * _thisAdjusted = reinterpret_cast<InteractionSourceLocation_t1509183288 *>(__this + 1);
	return InteractionSourceLocation_TryGetPosition_m1370729813(_thisAdjusted, ___position0, method);
}
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceLocation::TryGetVelocity(UnityEngine.Vector3&)
extern "C"  bool InteractionSourceLocation_TryGetVelocity_m989728567 (InteractionSourceLocation_t1509183288 * __this, Vector3_t2243707580 * ___velocity0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580 * L_0 = ___velocity0;
		Vector3_t2243707580  L_1 = __this->get_m_velocity_3();
		(*(Vector3_t2243707580 *)L_0) = L_1;
		uint8_t L_2 = __this->get_m_hasVelocity_2();
		V_0 = (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001f:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool InteractionSourceLocation_TryGetVelocity_m989728567_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580 * ___velocity0, const MethodInfo* method)
{
	InteractionSourceLocation_t1509183288 * _thisAdjusted = reinterpret_cast<InteractionSourceLocation_t1509183288 *>(__this + 1);
	return InteractionSourceLocation_TryGetVelocity_m989728567(_thisAdjusted, ___velocity0, method);
}
// System.Double UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_sourceLossRisk()
extern "C"  double InteractionSourceProperties_get_sourceLossRisk_m3412787295 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		double L_0 = __this->get_m_sourceLossRisk_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		double L_1 = V_0;
		return L_1;
	}
}
extern "C"  double InteractionSourceProperties_get_sourceLossRisk_m3412787295_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceProperties_t396045678 * _thisAdjusted = reinterpret_cast<InteractionSourceProperties_t396045678 *>(__this + 1);
	return InteractionSourceProperties_get_sourceLossRisk_m3412787295(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_sourceLossMitigationDirection()
extern "C"  Vector3_t2243707580  InteractionSourceProperties_get_sourceLossMitigationDirection_m3544872833 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_sourceLossMitigationDirection_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  InteractionSourceProperties_get_sourceLossMitigationDirection_m3544872833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceProperties_t396045678 * _thisAdjusted = reinterpret_cast<InteractionSourceProperties_t396045678 *>(__this + 1);
	return InteractionSourceProperties_get_sourceLossMitigationDirection_m3544872833(_thisAdjusted, method);
}
// UnityEngine.VR.WSA.Input.InteractionSourceLocation UnityEngine.VR.WSA.Input.InteractionSourceProperties::get_location()
extern "C"  InteractionSourceLocation_t1509183288  InteractionSourceProperties_get_location_m1069792109 (InteractionSourceProperties_t396045678 * __this, const MethodInfo* method)
{
	InteractionSourceLocation_t1509183288  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		InteractionSourceLocation_t1509183288  L_0 = __this->get_m_location_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		InteractionSourceLocation_t1509183288  L_1 = V_0;
		return L_1;
	}
}
extern "C"  InteractionSourceLocation_t1509183288  InteractionSourceProperties_get_location_m1069792109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceProperties_t396045678 * _thisAdjusted = reinterpret_cast<InteractionSourceProperties_t396045678 *>(__this + 1);
	return InteractionSourceProperties_get_location_m1069792109(_thisAdjusted, method);
}
// System.Boolean UnityEngine.VR.WSA.Input.InteractionSourceState::get_pressed()
extern "C"  bool InteractionSourceState_get_pressed_m310769651 (InteractionSourceState_t830383220 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		uint8_t L_0 = __this->get_m_pressed_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool InteractionSourceState_get_pressed_m310769651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceState_t830383220 * _thisAdjusted = reinterpret_cast<InteractionSourceState_t830383220 *>(__this + 1);
	return InteractionSourceState_get_pressed_m310769651(_thisAdjusted, method);
}
// UnityEngine.VR.WSA.Input.InteractionSourceProperties UnityEngine.VR.WSA.Input.InteractionSourceState::get_properties()
extern "C"  InteractionSourceProperties_t396045678  InteractionSourceState_get_properties_m3535152895 (InteractionSourceState_t830383220 * __this, const MethodInfo* method)
{
	InteractionSourceProperties_t396045678  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		InteractionSourceProperties_t396045678  L_0 = __this->get_m_properties_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		InteractionSourceProperties_t396045678  L_1 = V_0;
		return L_1;
	}
}
extern "C"  InteractionSourceProperties_t396045678  InteractionSourceState_get_properties_m3535152895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceState_t830383220 * _thisAdjusted = reinterpret_cast<InteractionSourceState_t830383220 *>(__this + 1);
	return InteractionSourceState_get_properties_m3535152895(_thisAdjusted, method);
}
// UnityEngine.VR.WSA.Input.InteractionSource UnityEngine.VR.WSA.Input.InteractionSourceState::get_source()
extern "C"  InteractionSource_t1972476489  InteractionSourceState_get_source_m3299051932 (InteractionSourceState_t830383220 * __this, const MethodInfo* method)
{
	InteractionSource_t1972476489  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		InteractionSource_t1972476489  L_0 = __this->get_m_source_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		InteractionSource_t1972476489  L_1 = V_0;
		return L_1;
	}
}
extern "C"  InteractionSource_t1972476489  InteractionSourceState_get_source_m3299051932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceState_t830383220 * _thisAdjusted = reinterpret_cast<InteractionSourceState_t830383220 *>(__this + 1);
	return InteractionSourceState_get_source_m3299051932(_thisAdjusted, method);
}
// UnityEngine.Ray UnityEngine.VR.WSA.Input.InteractionSourceState::get_headRay()
extern "C"  Ray_t2469606224  InteractionSourceState_get_headRay_m802244013 (InteractionSourceState_t830383220 * __this, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Ray_t2469606224  L_0 = __this->get_m_headRay_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Ray_t2469606224  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Ray_t2469606224  InteractionSourceState_get_headRay_m802244013_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InteractionSourceState_t830383220 * _thisAdjusted = reinterpret_cast<InteractionSourceState_t830383220 *>(__this + 1);
	return InteractionSourceState_get_headRay_m802244013(_thisAdjusted, method);
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore::.ctor(System.IntPtr)
extern "C"  void WorldAnchorStore__ctor_m388893287 (WorldAnchorStore_t1514582394 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorStore__ctor_m388893287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___nativePtr0;
		__this->set_m_NativePtr_1(L_1);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore::InvokeGetAsyncDelegate(UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate,System.IntPtr)
extern "C"  void WorldAnchorStore_InvokeGetAsyncDelegate_m1814413383 (Il2CppObject * __this /* static, unused */, GetAsyncDelegate_t1561547849 * ___handler0, IntPtr_t ___nativePtr1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorStore_InvokeGetAsyncDelegate_m1814413383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___nativePtr1;
		WorldAnchorStore_t1514582394 * L_1 = (WorldAnchorStore_t1514582394 *)il2cpp_codegen_object_new(WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var);
		WorldAnchorStore__ctor_m388893287(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var);
		((WorldAnchorStore_t1514582394_StaticFields*)WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var->static_fields)->set_s_Instance_0(L_1);
		GetAsyncDelegate_t1561547849 * L_2 = ___handler0;
		WorldAnchorStore_t1514582394 * L_3 = ((WorldAnchorStore_t1514582394_StaticFields*)WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_0();
		NullCheck(L_2);
		GetAsyncDelegate_Invoke_m3132607376(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore::.cctor()
extern "C"  void WorldAnchorStore__cctor_m4175401318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorStore__cctor_m4175401318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((WorldAnchorStore_t1514582394_StaticFields*)WorldAnchorStore_t1514582394_il2cpp_TypeInfo_var->static_fields)->set_s_Instance_0((WorldAnchorStore_t1514582394 *)NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAsyncDelegate__ctor_m1670183969 (GetAsyncDelegate_t1561547849 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate::Invoke(UnityEngine.VR.WSA.Persistence.WorldAnchorStore)
extern "C"  void GetAsyncDelegate_Invoke_m3132607376 (GetAsyncDelegate_t1561547849 * __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___store0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___store0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___store0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___store0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, WorldAnchorStore_t1514582394 * ___store0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___store0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___store0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate::BeginInvoke(UnityEngine.VR.WSA.Persistence.WorldAnchorStore,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAsyncDelegate_BeginInvoke_m49230981 (GetAsyncDelegate_t1561547849 * __this, WorldAnchorStore_t1514582394 * ___store0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___store0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.Persistence.WorldAnchorStore/GetAsyncDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void GetAsyncDelegate_EndInvoke_m330427183 (GetAsyncDelegate_t1561547849 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::.ctor(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch__ctor_m2842525734 (WorldAnchorTransferBatch_t4032480564 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorTransferBatch__ctor_m2842525734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___nativePtr0;
		__this->set_m_NativePtr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::Finalize()
extern "C"  void WorldAnchorTransferBatch_Finalize_m3625681664 (WorldAnchorTransferBatch_t4032480564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorTransferBatch_Finalize_m3625681664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_NativePtr_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_002e;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_NativePtr_0();
			WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_NativePtr_0(L_4);
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3A, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003a:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::Dispose()
extern "C"  void WorldAnchorTransferBatch_Dispose_m1432692473 (WorldAnchorTransferBatch_t4032480564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorTransferBatch_Dispose_m1432692473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativePtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_NativePtr_0();
		WorldAnchorTransferBatch_Dispose_Internal_m3965827083(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_0(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::InvokeWorldAnchorSerializationDataAvailableDelegate(UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate,System.Byte[])
extern "C"  void WorldAnchorTransferBatch_InvokeWorldAnchorSerializationDataAvailableDelegate_m1928933793 (Il2CppObject * __this /* static, unused */, SerializationDataAvailableDelegate_t2381700809 * ___onSerializationDataAvailable0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method)
{
	{
		SerializationDataAvailableDelegate_t2381700809 * L_0 = ___onSerializationDataAvailable0;
		ByteU5BU5D_t3397334013* L_1 = ___data1;
		NullCheck(L_0);
		SerializationDataAvailableDelegate_Invoke_m3283329308(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::InvokeWorldAnchorSerializationCompleteDelegate(UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate,UnityEngine.VR.WSA.Sharing.SerializationCompletionReason)
extern "C"  void WorldAnchorTransferBatch_InvokeWorldAnchorSerializationCompleteDelegate_m737862886 (Il2CppObject * __this /* static, unused */, SerializationCompleteDelegate_t389490593 * ___onSerializationComplete0, int32_t ___completionReason1, const MethodInfo* method)
{
	{
		SerializationCompleteDelegate_t389490593 * L_0 = ___onSerializationComplete0;
		int32_t L_1 = ___completionReason1;
		NullCheck(L_0);
		SerializationCompleteDelegate_Invoke_m4015421603(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::InvokeWorldAnchorDeserializationCompleteDelegate(UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate,UnityEngine.VR.WSA.Sharing.SerializationCompletionReason,System.IntPtr)
extern "C"  void WorldAnchorTransferBatch_InvokeWorldAnchorDeserializationCompleteDelegate_m4057073830 (Il2CppObject * __this /* static, unused */, DeserializationCompleteDelegate_t2482917876 * ___onDeserializationComplete0, int32_t ___completionReason1, IntPtr_t ___nativePtr2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchorTransferBatch_InvokeWorldAnchorDeserializationCompleteDelegate_m4057073830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WorldAnchorTransferBatch_t4032480564 * V_0 = NULL;
	{
		IntPtr_t L_0 = ___nativePtr2;
		WorldAnchorTransferBatch_t4032480564 * L_1 = (WorldAnchorTransferBatch_t4032480564 *)il2cpp_codegen_object_new(WorldAnchorTransferBatch_t4032480564_il2cpp_TypeInfo_var);
		WorldAnchorTransferBatch__ctor_m2842525734(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DeserializationCompleteDelegate_t2482917876 * L_2 = ___onDeserializationComplete0;
		int32_t L_3 = ___completionReason1;
		WorldAnchorTransferBatch_t4032480564 * L_4 = V_0;
		NullCheck(L_2);
		DeserializationCompleteDelegate_Invoke_m4282608372(L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::Dispose_Internal(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch_Dispose_Internal_m3965827083 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method)
{
	typedef void (*WorldAnchorTransferBatch_Dispose_Internal_m3965827083_ftn) (IntPtr_t);
	static WorldAnchorTransferBatch_Dispose_Internal_m3965827083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WorldAnchorTransferBatch_Dispose_Internal_m3965827083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::Dispose_Internal(System.IntPtr)");
	_il2cpp_icall_func(___context0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method)
{
	typedef void (*WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492_ftn) (IntPtr_t);
	static WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WorldAnchorTransferBatch_DisposeThreaded_Internal_m3875352492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::DisposeThreaded_Internal(System.IntPtr)");
	_il2cpp_icall_func(___context0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DeserializationCompleteDelegate__ctor_m4209756382 (DeserializationCompleteDelegate_t2482917876 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate::Invoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason,UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch)
extern "C"  void DeserializationCompleteDelegate_Invoke_m4282608372 (DeserializationCompleteDelegate_t2482917876 * __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___completionReason0, ___deserializedTransferBatch1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___completionReason0, ___deserializedTransferBatch1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___completionReason0, ___deserializedTransferBatch1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___completionReason0, ___deserializedTransferBatch1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate::BeginInvoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason,UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DeserializationCompleteDelegate_BeginInvoke_m4030138881 (DeserializationCompleteDelegate_t2482917876 * __this, int32_t ___completionReason0, WorldAnchorTransferBatch_t4032480564 * ___deserializedTransferBatch1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeserializationCompleteDelegate_BeginInvoke_m4030138881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SerializationCompletionReason_t3597317192_il2cpp_TypeInfo_var, &___completionReason0);
	__d_args[1] = ___deserializedTransferBatch1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/DeserializationCompleteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DeserializationCompleteDelegate_EndInvoke_m2243171768 (DeserializationCompleteDelegate_t2482917876 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_SerializationCompleteDelegate_t389490593 (SerializationCompleteDelegate_t389490593 * __this, int32_t ___completionReason0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___completionReason0);

}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SerializationCompleteDelegate__ctor_m1714777825 (SerializationCompleteDelegate_t389490593 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate::Invoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason)
extern "C"  void SerializationCompleteDelegate_Invoke_m4015421603 (SerializationCompleteDelegate_t389490593 * __this, int32_t ___completionReason0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___completionReason0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___completionReason0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___completionReason0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___completionReason0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___completionReason0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___completionReason0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___completionReason0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___completionReason0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate::BeginInvoke(UnityEngine.VR.WSA.Sharing.SerializationCompletionReason,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SerializationCompleteDelegate_BeginInvoke_m2069317740 (SerializationCompleteDelegate_t389490593 * __this, int32_t ___completionReason0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationCompleteDelegate_BeginInvoke_m2069317740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SerializationCompletionReason_t3597317192_il2cpp_TypeInfo_var, &___completionReason0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SerializationCompleteDelegate_EndInvoke_m3213254571 (SerializationCompleteDelegate_t389490593 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_SerializationDataAvailableDelegate_t2381700809 (SerializationDataAvailableDelegate_t2381700809 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(uint8_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___data0' to native representation
	uint8_t* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<uint8_t*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SerializationDataAvailableDelegate__ctor_m272877505 (SerializationDataAvailableDelegate_t2381700809 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate::Invoke(System.Byte[])
extern "C"  void SerializationDataAvailableDelegate_Invoke_m3283329308 (SerializationDataAvailableDelegate_t2381700809 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___data0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___data0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___data0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SerializationDataAvailableDelegate_BeginInvoke_m1722749893 (SerializationDataAvailableDelegate_t2381700809 * __this, ByteU5BU5D_t3397334013* ___data0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SerializationDataAvailableDelegate_EndInvoke_m3443478479 (SerializationDataAvailableDelegate_t2381700809 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.VR.WSA.SurfaceData
extern "C" void SurfaceData_t3147297845_marshal_pinvoke(const SurfaceData_t3147297845& unmarshaled, SurfaceData_t3147297845_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___outputMesh_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'outputMesh' of type 'SurfaceData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___outputMesh_1Exception);
}
extern "C" void SurfaceData_t3147297845_marshal_pinvoke_back(const SurfaceData_t3147297845_marshaled_pinvoke& marshaled, SurfaceData_t3147297845& unmarshaled)
{
	Il2CppCodeGenException* ___outputMesh_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'outputMesh' of type 'SurfaceData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___outputMesh_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.VR.WSA.SurfaceData
extern "C" void SurfaceData_t3147297845_marshal_pinvoke_cleanup(SurfaceData_t3147297845_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.VR.WSA.SurfaceData
extern "C" void SurfaceData_t3147297845_marshal_com(const SurfaceData_t3147297845& unmarshaled, SurfaceData_t3147297845_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___outputMesh_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'outputMesh' of type 'SurfaceData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___outputMesh_1Exception);
}
extern "C" void SurfaceData_t3147297845_marshal_com_back(const SurfaceData_t3147297845_marshaled_com& marshaled, SurfaceData_t3147297845& unmarshaled)
{
	Il2CppCodeGenException* ___outputMesh_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'outputMesh' of type 'SurfaceData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___outputMesh_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.VR.WSA.SurfaceData
extern "C" void SurfaceData_t3147297845_marshal_com_cleanup(SurfaceData_t3147297845_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::InvokeSurfaceChangedEvent(UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate,System.Int32,UnityEngine.VR.WSA.SurfaceChange,UnityEngine.Bounds,System.Int64)
extern "C"  void SurfaceObserver_InvokeSurfaceChangedEvent_m2394764740 (Il2CppObject * __this /* static, unused */, SurfaceChangedDelegate_t3444411290 * ___onSurfaceChanged0, int32_t ___surfaceId1, int32_t ___changeType2, Bounds_t3033363703  ___bounds3, int64_t ___updateTime4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceObserver_InvokeSurfaceChangedEvent_m2394764740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SurfaceId_t4207907960  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SurfaceChangedDelegate_t3444411290 * L_0 = ___onSurfaceChanged0;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = ___surfaceId1;
		(&V_0)->set_handle_0(L_1);
		SurfaceChangedDelegate_t3444411290 * L_2 = ___onSurfaceChanged0;
		SurfaceId_t4207907960  L_3 = V_0;
		int32_t L_4 = ___changeType2;
		Bounds_t3033363703  L_5 = ___bounds3;
		int64_t L_6 = ___updateTime4;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_7 = DateTime_FromFileTime_m725937452(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		SurfaceChangedDelegate_Invoke_m1835305981(L_2, L_3, L_4, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::InvokeSurfaceDataReadyEvent(UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate,System.Int32,UnityEngine.MeshFilter,UnityEngine.VR.WSA.WorldAnchor,UnityEngine.MeshCollider,System.Single,System.Boolean,System.Boolean,System.Single)
extern "C"  void SurfaceObserver_InvokeSurfaceDataReadyEvent_m3096418088 (Il2CppObject * __this /* static, unused */, SurfaceDataReadyDelegate_t1375437319 * ___onDataReady0, int32_t ___surfaceId1, MeshFilter_t3026937449 * ___outputMesh2, WorldAnchor_t100028935 * ___outputAnchor3, MeshCollider_t2718867283 * ___outputCollider4, float ___trisPerCubicMeter5, bool ___bakeCollider6, bool ___outputWritten7, float ___elapsedBakeTimeSeconds8, const MethodInfo* method)
{
	SurfaceData_t3147297845  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SurfaceDataReadyDelegate_t1375437319 * L_0 = ___onDataReady0;
		if (!L_0)
		{
			goto IL_004c;
		}
	}
	{
		SurfaceId_t4207907960 * L_1 = (&V_0)->get_address_of_id_0();
		int32_t L_2 = ___surfaceId1;
		L_1->set_handle_0(L_2);
		MeshFilter_t3026937449 * L_3 = ___outputMesh2;
		(&V_0)->set_outputMesh_1(L_3);
		WorldAnchor_t100028935 * L_4 = ___outputAnchor3;
		(&V_0)->set_outputAnchor_2(L_4);
		MeshCollider_t2718867283 * L_5 = ___outputCollider4;
		(&V_0)->set_outputCollider_3(L_5);
		float L_6 = ___trisPerCubicMeter5;
		(&V_0)->set_trianglesPerCubicMeter_4(L_6);
		bool L_7 = ___bakeCollider6;
		(&V_0)->set_bakeCollider_5(L_7);
		SurfaceDataReadyDelegate_t1375437319 * L_8 = ___onDataReady0;
		SurfaceData_t3147297845  L_9 = V_0;
		bool L_10 = ___outputWritten7;
		float L_11 = ___elapsedBakeTimeSeconds8;
		NullCheck(L_8);
		SurfaceDataReadyDelegate_Invoke_m1482963986(L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::Finalize()
extern "C"  void SurfaceObserver_Finalize_m1712600395 (SurfaceObserver_t3799331897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceObserver_Finalize_m1712600395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Observer_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0034;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_Observer_0();
			SurfaceObserver_DestroyThreaded_m424102712(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_Observer_0(L_4);
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::Dispose()
extern "C"  void SurfaceObserver_Dispose_m2279662588 (SurfaceObserver_t3799331897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceObserver_Dispose_m2279662588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_Observer_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_Observer_0();
		SurfaceObserver_Destroy_m2667935701(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Observer_0(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::Destroy(System.IntPtr)
extern "C"  void SurfaceObserver_Destroy_m2667935701 (Il2CppObject * __this /* static, unused */, IntPtr_t ___observer0, const MethodInfo* method)
{
	typedef void (*SurfaceObserver_Destroy_m2667935701_ftn) (IntPtr_t);
	static SurfaceObserver_Destroy_m2667935701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SurfaceObserver_Destroy_m2667935701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.SurfaceObserver::Destroy(System.IntPtr)");
	_il2cpp_icall_func(___observer0);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver::DestroyThreaded(System.IntPtr)
extern "C"  void SurfaceObserver_DestroyThreaded_m424102712 (Il2CppObject * __this /* static, unused */, IntPtr_t ___observer0, const MethodInfo* method)
{
	typedef void (*SurfaceObserver_DestroyThreaded_m424102712_ftn) (IntPtr_t);
	static SurfaceObserver_DestroyThreaded_m424102712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SurfaceObserver_DestroyThreaded_m424102712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.SurfaceObserver::DestroyThreaded(System.IntPtr)");
	_il2cpp_icall_func(___observer0);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SurfaceChangedDelegate__ctor_m2166145028 (SurfaceChangedDelegate_t3444411290 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate::Invoke(UnityEngine.VR.WSA.SurfaceId,UnityEngine.VR.WSA.SurfaceChange,UnityEngine.Bounds,System.DateTime)
extern "C"  void SurfaceChangedDelegate_Invoke_m1835305981 (SurfaceChangedDelegate_t3444411290 * __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___surfaceId0, ___changeType1, ___bounds2, ___updateTime3,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___surfaceId0, ___changeType1, ___bounds2, ___updateTime3,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___surfaceId0, ___changeType1, ___bounds2, ___updateTime3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___surfaceId0, ___changeType1, ___bounds2, ___updateTime3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate::BeginInvoke(UnityEngine.VR.WSA.SurfaceId,UnityEngine.VR.WSA.SurfaceChange,UnityEngine.Bounds,System.DateTime,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SurfaceChangedDelegate_BeginInvoke_m2555310712 (SurfaceChangedDelegate_t3444411290 * __this, SurfaceId_t4207907960  ___surfaceId0, int32_t ___changeType1, Bounds_t3033363703  ___bounds2, DateTime_t693205669  ___updateTime3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceChangedDelegate_BeginInvoke_m2555310712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(SurfaceId_t4207907960_il2cpp_TypeInfo_var, &___surfaceId0);
	__d_args[1] = Box(SurfaceChange_t769713231_il2cpp_TypeInfo_var, &___changeType1);
	__d_args[2] = Box(Bounds_t3033363703_il2cpp_TypeInfo_var, &___bounds2);
	__d_args[3] = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &___updateTime3);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SurfaceChangedDelegate_EndInvoke_m2151865314 (SurfaceChangedDelegate_t3444411290 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SurfaceDataReadyDelegate__ctor_m3682796387 (SurfaceDataReadyDelegate_t1375437319 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate::Invoke(UnityEngine.VR.WSA.SurfaceData,System.Boolean,System.Single)
extern "C"  void SurfaceDataReadyDelegate_Invoke_m1482963986 (SurfaceDataReadyDelegate_t1375437319 * __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___bakedData0, ___outputWritten1, ___elapsedBakeTimeSeconds2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___bakedData0, ___outputWritten1, ___elapsedBakeTimeSeconds2,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___bakedData0, ___outputWritten1, ___elapsedBakeTimeSeconds2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___bakedData0, ___outputWritten1, ___elapsedBakeTimeSeconds2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate::BeginInvoke(UnityEngine.VR.WSA.SurfaceData,System.Boolean,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SurfaceDataReadyDelegate_BeginInvoke_m1819939565 (SurfaceDataReadyDelegate_t1375437319 * __this, SurfaceData_t3147297845  ___bakedData0, bool ___outputWritten1, float ___elapsedBakeTimeSeconds2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceDataReadyDelegate_BeginInvoke_m1819939565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(SurfaceData_t3147297845_il2cpp_TypeInfo_var, &___bakedData0);
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___outputWritten1);
	__d_args[2] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___elapsedBakeTimeSeconds2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.VR.WSA.SurfaceObserver/SurfaceDataReadyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SurfaceDataReadyDelegate_EndInvoke_m1213124261 (SurfaceDataReadyDelegate_t1375437319 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::.ctor(System.IntPtr)
extern "C"  void PhotoCapture__ctor_m845114063 (PhotoCapture_t1414746886 * __this, IntPtr_t ___nativeCaptureObject0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeCaptureObject0;
		__this->set_m_NativePtr_1(L_0);
		return;
	}
}
// UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult UnityEngine.VR.WSA.WebCam.PhotoCapture::MakeCaptureResult(System.Int64)
extern "C"  PhotoCaptureResult_t3813459671  PhotoCapture_MakeCaptureResult_m4180913700 (Il2CppObject * __this /* static, unused */, int64_t ___hResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_MakeCaptureResult_m4180913700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PhotoCaptureResult_t3813459671  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	PhotoCaptureResult_t3813459671  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Initobj (PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = ___hResult0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		int64_t L_1 = ((PhotoCapture_t1414746886_StaticFields*)PhotoCapture_t1414746886_il2cpp_TypeInfo_var->static_fields)->get_HR_SUCCESS_0();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_1))))
		{
			goto IL_001d;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_001d:
	{
		V_1 = 1;
	}

IL_0021:
	{
		int32_t L_2 = V_1;
		(&V_0)->set_resultType_0(L_2);
		int64_t L_3 = ___hResult0;
		(&V_0)->set_hResult_1(L_3);
		PhotoCaptureResult_t3813459671  L_4 = V_0;
		V_2 = L_4;
		goto IL_0038;
	}

IL_0038:
	{
		PhotoCaptureResult_t3813459671  L_5 = V_2;
		return L_5;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::InvokeOnCreatedResourceDelegate(UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback,System.IntPtr)
extern "C"  void PhotoCapture_InvokeOnCreatedResourceDelegate_m2701547345 (Il2CppObject * __this /* static, unused */, OnCaptureResourceCreatedCallback_t2698966894 * ___callback0, IntPtr_t ___nativePtr1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_InvokeOnCreatedResourceDelegate_m2701547345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___nativePtr1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		OnCaptureResourceCreatedCallback_t2698966894 * L_3 = ___callback0;
		NullCheck(L_3);
		OnCaptureResourceCreatedCallback_Invoke_m3100503923(L_3, (PhotoCapture_t1414746886 *)NULL, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_001f:
	{
		OnCaptureResourceCreatedCallback_t2698966894 * L_4 = ___callback0;
		IntPtr_t L_5 = ___nativePtr1;
		PhotoCapture_t1414746886 * L_6 = (PhotoCapture_t1414746886 *)il2cpp_codegen_object_new(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCapture__ctor_m845114063(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		OnCaptureResourceCreatedCallback_Invoke_m3100503923(L_4, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::InvokeOnPhotoModeStartedDelegate(UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback,System.Int64)
extern "C"  void PhotoCapture_InvokeOnPhotoModeStartedDelegate_m367056465 (Il2CppObject * __this /* static, unused */, OnPhotoModeStartedCallback_t522077320 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_InvokeOnPhotoModeStartedDelegate_m367056465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnPhotoModeStartedCallback_t522077320 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCaptureResult_t3813459671  L_2 = PhotoCapture_MakeCaptureResult_m4180913700(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnPhotoModeStartedCallback_Invoke_m2244798845(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::InvokeOnPhotoModeStoppedDelegate(UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback,System.Int64)
extern "C"  void PhotoCapture_InvokeOnPhotoModeStoppedDelegate_m2878721745 (Il2CppObject * __this /* static, unused */, OnPhotoModeStoppedCallback_t2742193416 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_InvokeOnPhotoModeStoppedDelegate_m2878721745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnPhotoModeStoppedCallback_t2742193416 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCaptureResult_t3813459671  L_2 = PhotoCapture_MakeCaptureResult_m4180913700(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnPhotoModeStoppedCallback_Invoke_m2835537(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::InvokeOnCapturedPhotoToDiskDelegate(UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback,System.Int64)
extern "C"  void PhotoCapture_InvokeOnCapturedPhotoToDiskDelegate_m1345926005 (Il2CppObject * __this /* static, unused */, OnCapturedToDiskCallback_t1784202510 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_InvokeOnCapturedPhotoToDiskDelegate_m1345926005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnCapturedToDiskCallback_t1784202510 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCaptureResult_t3813459671  L_2 = PhotoCapture_MakeCaptureResult_m4180913700(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnCapturedToDiskCallback_Invoke_m2804421377(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::InvokeOnCapturedPhotoToMemoryDelegate(UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback,System.Int64,System.IntPtr)
extern "C"  void PhotoCapture_InvokeOnCapturedPhotoToMemoryDelegate_m1206852871 (Il2CppObject * __this /* static, unused */, OnCapturedToMemoryCallback_t1623734756 * ___callback0, int64_t ___hResult1, IntPtr_t ___photoCaptureFramePtr2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_InvokeOnCapturedPhotoToMemoryDelegate_m1206852871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PhotoCaptureFrame_t928993319 * V_0 = NULL;
	{
		V_0 = (PhotoCaptureFrame_t928993319 *)NULL;
		IntPtr_t L_0 = ___photoCaptureFramePtr2;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3 = ___photoCaptureFramePtr2;
		PhotoCaptureFrame_t928993319 * L_4 = (PhotoCaptureFrame_t928993319 *)il2cpp_codegen_object_new(PhotoCaptureFrame_t928993319_il2cpp_TypeInfo_var);
		PhotoCaptureFrame__ctor_m326376562(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_001c:
	{
		OnCapturedToMemoryCallback_t1623734756 * L_5 = ___callback0;
		int64_t L_6 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCaptureResult_t3813459671  L_7 = PhotoCapture_MakeCaptureResult_m4180913700(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		PhotoCaptureFrame_t928993319 * L_8 = V_0;
		NullCheck(L_5);
		OnCapturedToMemoryCallback_Invoke_m1564803831(L_5, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::Dispose()
extern "C"  void PhotoCapture_Dispose_m1060921546 (PhotoCapture_t1414746886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_Dispose_m1060921546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativePtr_1();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_NativePtr_1();
		IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
		PhotoCapture_Dispose_Internal_m374635362(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_1(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::Finalize()
extern "C"  void PhotoCapture_Finalize_m1498549843 (PhotoCapture_t1414746886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture_Finalize_m1498549843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_NativePtr_1();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_002e;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_NativePtr_1();
			IL2CPP_RUNTIME_CLASS_INIT(PhotoCapture_t1414746886_il2cpp_TypeInfo_var);
			PhotoCapture_DisposeThreaded_Internal_m3747816943(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_NativePtr_1(L_4);
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3A, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003a:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::Dispose_Internal(System.IntPtr)
extern "C"  void PhotoCapture_Dispose_Internal_m374635362 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureObj0, const MethodInfo* method)
{
	typedef void (*PhotoCapture_Dispose_Internal_m374635362_ftn) (IntPtr_t);
	static PhotoCapture_Dispose_Internal_m374635362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCapture_Dispose_Internal_m374635362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCapture::Dispose_Internal(System.IntPtr)");
	_il2cpp_icall_func(___photoCaptureObj0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void PhotoCapture_DisposeThreaded_Internal_m3747816943 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureObj0, const MethodInfo* method)
{
	typedef void (*PhotoCapture_DisposeThreaded_Internal_m3747816943_ftn) (IntPtr_t);
	static PhotoCapture_DisposeThreaded_Internal_m3747816943_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCapture_DisposeThreaded_Internal_m3747816943_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCapture::DisposeThreaded_Internal(System.IntPtr)");
	_il2cpp_icall_func(___photoCaptureObj0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture::.cctor()
extern "C"  void PhotoCapture__cctor_m3299331514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCapture__cctor_m3299331514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PhotoCapture_t1414746886_StaticFields*)PhotoCapture_t1414746886_il2cpp_TypeInfo_var->static_fields)->set_HR_SUCCESS_0((((int64_t)((int64_t)0))));
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_OnCapturedToDiskCallback_t1784202510 (OnCapturedToDiskCallback_t1784202510 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(PhotoCaptureResult_t3813459671 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCapturedToDiskCallback__ctor_m4050088316 (OnCapturedToDiskCallback_t1784202510 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnCapturedToDiskCallback_Invoke_m2804421377 (OnCapturedToDiskCallback_t1784202510 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCapturedToDiskCallback_BeginInvoke_m1055237728 (OnCapturedToDiskCallback_t1784202510 * __this, PhotoCaptureResult_t3813459671  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnCapturedToDiskCallback_BeginInvoke_m1055237728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnCapturedToDiskCallback_EndInvoke_m736427534 (OnCapturedToDiskCallback_t1784202510 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCapturedToMemoryCallback__ctor_m3129599262 (OnCapturedToMemoryCallback_t1623734756 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame)
extern "C"  void OnCapturedToMemoryCallback_Invoke_m1564803831 (OnCapturedToMemoryCallback_t1623734756 * __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0, ___photoCaptureFrame1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0, ___photoCaptureFrame1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0, ___photoCaptureFrame1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0, ___photoCaptureFrame1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCapturedToMemoryCallback_BeginInvoke_m1549072172 (OnCapturedToMemoryCallback_t1623734756 * __this, PhotoCaptureResult_t3813459671  ___result0, PhotoCaptureFrame_t928993319 * ___photoCaptureFrame1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnCapturedToMemoryCallback_BeginInvoke_m1549072172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var, &___result0);
	__d_args[1] = ___photoCaptureFrame1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnCapturedToMemoryCallback_EndInvoke_m2067935000 (OnCapturedToMemoryCallback_t1623734756 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCaptureResourceCreatedCallback__ctor_m3504338716 (OnCaptureResourceCreatedCallback_t2698966894 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture)
extern "C"  void OnCaptureResourceCreatedCallback_Invoke_m3100503923 (OnCaptureResourceCreatedCallback_t2698966894 * __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, PhotoCapture_t1414746886 * ___captureObject0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.PhotoCapture,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCaptureResourceCreatedCallback_BeginInvoke_m3755782204 (OnCaptureResourceCreatedCallback_t2698966894 * __this, PhotoCapture_t1414746886 * ___captureObject0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___captureObject0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCaptureResourceCreatedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnCaptureResourceCreatedCallback_EndInvoke_m2343171838 (OnCaptureResourceCreatedCallback_t2698966894 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnPhotoModeStartedCallback_t522077320 (OnPhotoModeStartedCallback_t522077320 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(PhotoCaptureResult_t3813459671 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPhotoModeStartedCallback__ctor_m1045404962 (OnPhotoModeStartedCallback_t522077320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnPhotoModeStartedCallback_Invoke_m2244798845 (OnPhotoModeStartedCallback_t522077320 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPhotoModeStartedCallback_BeginInvoke_m285549642 (OnPhotoModeStartedCallback_t522077320 * __this, PhotoCaptureResult_t3813459671  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPhotoModeStartedCallback_BeginInvoke_m285549642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPhotoModeStartedCallback_EndInvoke_m3373477828 (OnPhotoModeStartedCallback_t522077320 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnPhotoModeStoppedCallback_t2742193416 (OnPhotoModeStoppedCallback_t2742193416 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(PhotoCaptureResult_t3813459671 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPhotoModeStoppedCallback__ctor_m252680630 (OnPhotoModeStoppedCallback_t2742193416 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback::Invoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult)
extern "C"  void OnPhotoModeStoppedCallback_Invoke_m2835537 (OnPhotoModeStoppedCallback_t2742193416 * __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, PhotoCaptureResult_t3813459671  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.PhotoCapture/PhotoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPhotoModeStoppedCallback_BeginInvoke_m4070686730 (OnPhotoModeStoppedCallback_t2742193416 * __this, PhotoCaptureResult_t3813459671  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPhotoModeStoppedCallback_BeginInvoke_m4070686730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhotoCaptureResult_t3813459671_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPhotoModeStoppedCallback_EndInvoke_m3652094132 (OnPhotoModeStoppedCallback_t2742193416 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::.ctor(System.IntPtr)
extern "C"  void PhotoCaptureFrame__ctor_m326376562 (PhotoCaptureFrame_t928993319 * __this, IntPtr_t ___nativePtr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCaptureFrame__ctor_m326376562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativePtr0;
		__this->set_m_NativePtr_0(L_0);
		IntPtr_t L_1 = ___nativePtr0;
		int32_t L_2 = PhotoCaptureFrame_GetDataLength_m1647924026(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		PhotoCaptureFrame_set_dataLength_m297082938(__this, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3 = ___nativePtr0;
		bool L_4 = PhotoCaptureFrame_GetHasLocationData_m1030592023(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		PhotoCaptureFrame_set_hasLocationData_m822935925(__this, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5 = ___nativePtr0;
		int32_t L_6 = PhotoCaptureFrame_GetCapturePixelFormat_m549361216(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		PhotoCaptureFrame_set_pixelFormat_m1931590910(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = PhotoCaptureFrame_get_dataLength_m393412179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_AddMemoryPressure_m4007918840(NULL /*static, unused*/, (((int64_t)((int64_t)L_7))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::get_dataLength()
extern "C"  int32_t PhotoCaptureFrame_get_dataLength_m393412179 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CdataLengthU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_dataLength(System.Int32)
extern "C"  void PhotoCaptureFrame_set_dataLength_m297082938 (PhotoCaptureFrame_t928993319 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CdataLengthU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_hasLocationData(System.Boolean)
extern "C"  void PhotoCaptureFrame_set_hasLocationData_m822935925 (PhotoCaptureFrame_t928993319 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ChasLocationDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::set_pixelFormat(UnityEngine.VR.WSA.WebCam.CapturePixelFormat)
extern "C"  void PhotoCaptureFrame_set_pixelFormat_m1931590910 (PhotoCaptureFrame_t928993319 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CpixelFormatU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Cleanup()
extern "C"  void PhotoCaptureFrame_Cleanup_m1070424360 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCaptureFrame_Cleanup_m1070424360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativePtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_3 = PhotoCaptureFrame_get_dataLength_m393412179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_RemoveMemoryPressure_m3910440451(NULL /*static, unused*/, (((int64_t)((int64_t)L_3))), /*hidden argument*/NULL);
		IntPtr_t L_4 = __this->get_m_NativePtr_0();
		PhotoCaptureFrame_Dispose_Internal_m3212242885(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_0(L_5);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Dispose()
extern "C"  void PhotoCaptureFrame_Dispose_m3758964267 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotoCaptureFrame_Dispose_m3758964267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotoCaptureFrame_Cleanup_m1070424360(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Finalize()
extern "C"  void PhotoCaptureFrame_Finalize_m4284305224 (PhotoCaptureFrame_t928993319 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		PhotoCaptureFrame_Cleanup_m1070424360(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// UnityEngine.VR.WSA.WebCam.CapturePixelFormat UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetCapturePixelFormat(System.IntPtr)
extern "C"  int32_t PhotoCaptureFrame_GetCapturePixelFormat_m549361216 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method)
{
	typedef int32_t (*PhotoCaptureFrame_GetCapturePixelFormat_m549361216_ftn) (IntPtr_t);
	static PhotoCaptureFrame_GetCapturePixelFormat_m549361216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCaptureFrame_GetCapturePixelFormat_m549361216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetCapturePixelFormat(System.IntPtr)");
	return _il2cpp_icall_func(___photoCaptureFrame0);
}
// System.Boolean UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetHasLocationData(System.IntPtr)
extern "C"  bool PhotoCaptureFrame_GetHasLocationData_m1030592023 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method)
{
	typedef bool (*PhotoCaptureFrame_GetHasLocationData_m1030592023_ftn) (IntPtr_t);
	static PhotoCaptureFrame_GetHasLocationData_m1030592023_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCaptureFrame_GetHasLocationData_m1030592023_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetHasLocationData(System.IntPtr)");
	return _il2cpp_icall_func(___photoCaptureFrame0);
}
// System.Int32 UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetDataLength(System.IntPtr)
extern "C"  int32_t PhotoCaptureFrame_GetDataLength_m1647924026 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method)
{
	typedef int32_t (*PhotoCaptureFrame_GetDataLength_m1647924026_ftn) (IntPtr_t);
	static PhotoCaptureFrame_GetDataLength_m1647924026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCaptureFrame_GetDataLength_m1647924026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::GetDataLength(System.IntPtr)");
	return _il2cpp_icall_func(___photoCaptureFrame0);
}
// System.Void UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Dispose_Internal(System.IntPtr)
extern "C"  void PhotoCaptureFrame_Dispose_Internal_m3212242885 (Il2CppObject * __this /* static, unused */, IntPtr_t ___photoCaptureFrame0, const MethodInfo* method)
{
	typedef void (*PhotoCaptureFrame_Dispose_Internal_m3212242885_ftn) (IntPtr_t);
	static PhotoCaptureFrame_Dispose_Internal_m3212242885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhotoCaptureFrame_Dispose_Internal_m3212242885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.PhotoCaptureFrame::Dispose_Internal(System.IntPtr)");
	_il2cpp_icall_func(___photoCaptureFrame0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::.ctor(System.IntPtr)
extern "C"  void VideoCapture__ctor_m2979903562 (VideoCapture_t3470796049 * __this, IntPtr_t ___nativeCaptureObject0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeCaptureObject0;
		__this->set_m_NativePtr_1(L_0);
		return;
	}
}
// UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult UnityEngine.VR.WSA.WebCam.VideoCapture::MakeCaptureResult(System.Int64)
extern "C"  VideoCaptureResult_t3170939459  VideoCapture_MakeCaptureResult_m3906400075 (Il2CppObject * __this /* static, unused */, int64_t ___hResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_MakeCaptureResult_m3906400075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VideoCaptureResult_t3170939459  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	VideoCaptureResult_t3170939459  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Initobj (VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = ___hResult0;
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		int64_t L_1 = ((VideoCapture_t3470796049_StaticFields*)VideoCapture_t3470796049_il2cpp_TypeInfo_var->static_fields)->get_HR_SUCCESS_0();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_1))))
		{
			goto IL_001d;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_001d:
	{
		V_1 = 1;
	}

IL_0021:
	{
		int32_t L_2 = V_1;
		(&V_0)->set_resultType_0(L_2);
		int64_t L_3 = ___hResult0;
		(&V_0)->set_hResult_1(L_3);
		VideoCaptureResult_t3170939459  L_4 = V_0;
		V_2 = L_4;
		goto IL_0038;
	}

IL_0038:
	{
		VideoCaptureResult_t3170939459  L_5 = V_2;
		return L_5;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::InvokeOnCreatedVideoCaptureResourceDelegate(UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback,System.IntPtr)
extern "C"  void VideoCapture_InvokeOnCreatedVideoCaptureResourceDelegate_m2707758557 (Il2CppObject * __this /* static, unused */, OnVideoCaptureResourceCreatedCallback_t3210647042 * ___callback0, IntPtr_t ___nativePtr1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_InvokeOnCreatedVideoCaptureResourceDelegate_m2707758557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___nativePtr1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		OnVideoCaptureResourceCreatedCallback_t3210647042 * L_3 = ___callback0;
		NullCheck(L_3);
		OnVideoCaptureResourceCreatedCallback_Invoke_m742532456(L_3, (VideoCapture_t3470796049 *)NULL, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_001f:
	{
		OnVideoCaptureResourceCreatedCallback_t3210647042 * L_4 = ___callback0;
		IntPtr_t L_5 = ___nativePtr1;
		VideoCapture_t3470796049 * L_6 = (VideoCapture_t3470796049 *)il2cpp_codegen_object_new(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCapture__ctor_m2979903562(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		OnVideoCaptureResourceCreatedCallback_Invoke_m742532456(L_4, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::InvokeOnVideoModeStartedDelegate(UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback,System.Int64)
extern "C"  void VideoCapture_InvokeOnVideoModeStartedDelegate_m1129777589 (Il2CppObject * __this /* static, unused */, OnVideoModeStartedCallback_t1811850718 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_InvokeOnVideoModeStartedDelegate_m1129777589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnVideoModeStartedCallback_t1811850718 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCaptureResult_t3170939459  L_2 = VideoCapture_MakeCaptureResult_m3906400075(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnVideoModeStartedCallback_Invoke_m1419327261(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::InvokeOnVideoModeStoppedDelegate(UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback,System.Int64)
extern "C"  void VideoCapture_InvokeOnVideoModeStoppedDelegate_m4012563765 (Il2CppObject * __this /* static, unused */, OnVideoModeStoppedCallback_t1862037614 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_InvokeOnVideoModeStoppedDelegate_m4012563765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnVideoModeStoppedCallback_t1862037614 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCaptureResult_t3170939459  L_2 = VideoCapture_MakeCaptureResult_m3906400075(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnVideoModeStoppedCallback_Invoke_m2621135601(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::InvokeOnStartedRecordingVideoToDiskDelegate(UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback,System.Int64)
extern "C"  void VideoCapture_InvokeOnStartedRecordingVideoToDiskDelegate_m2101797547 (Il2CppObject * __this /* static, unused */, OnStartedRecordingVideoCallback_t2612745932 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_InvokeOnStartedRecordingVideoToDiskDelegate_m2101797547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnStartedRecordingVideoCallback_t2612745932 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCaptureResult_t3170939459  L_2 = VideoCapture_MakeCaptureResult_m3906400075(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnStartedRecordingVideoCallback_Invoke_m3078230371(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::InvokeOnStoppedRecordingVideoToDiskDelegate(UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback,System.Int64)
extern "C"  void VideoCapture_InvokeOnStoppedRecordingVideoToDiskDelegate_m2832855883 (Il2CppObject * __this /* static, unused */, OnStoppedRecordingVideoCallback_t1976525720 * ___callback0, int64_t ___hResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_InvokeOnStoppedRecordingVideoToDiskDelegate_m2832855883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnStoppedRecordingVideoCallback_t1976525720 * L_0 = ___callback0;
		int64_t L_1 = ___hResult1;
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCaptureResult_t3170939459  L_2 = VideoCapture_MakeCaptureResult_m3906400075(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		OnStoppedRecordingVideoCallback_Invoke_m421367107(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::Dispose()
extern "C"  void VideoCapture_Dispose_m3064824343 (VideoCapture_t3470796049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_Dispose_m3064824343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativePtr_1();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_NativePtr_1();
		IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
		VideoCapture_Dispose_Internal_m369542261(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_NativePtr_1(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::Finalize()
extern "C"  void VideoCapture_Finalize_m577118944 (VideoCapture_t3470796049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture_Finalize_m577118944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_NativePtr_1();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_002e;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_NativePtr_1();
			IL2CPP_RUNTIME_CLASS_INIT(VideoCapture_t3470796049_il2cpp_TypeInfo_var);
			VideoCapture_DisposeThreaded_Internal_m3056527932(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_NativePtr_1(L_4);
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3A, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003a:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::Dispose_Internal(System.IntPtr)
extern "C"  void VideoCapture_Dispose_Internal_m369542261 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoCaptureObj0, const MethodInfo* method)
{
	typedef void (*VideoCapture_Dispose_Internal_m369542261_ftn) (IntPtr_t);
	static VideoCapture_Dispose_Internal_m369542261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (VideoCapture_Dispose_Internal_m369542261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.VideoCapture::Dispose_Internal(System.IntPtr)");
	_il2cpp_icall_func(___videoCaptureObj0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::DisposeThreaded_Internal(System.IntPtr)
extern "C"  void VideoCapture_DisposeThreaded_Internal_m3056527932 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoCaptureObj0, const MethodInfo* method)
{
	typedef void (*VideoCapture_DisposeThreaded_Internal_m3056527932_ftn) (IntPtr_t);
	static VideoCapture_DisposeThreaded_Internal_m3056527932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (VideoCapture_DisposeThreaded_Internal_m3056527932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.VR.WSA.WebCam.VideoCapture::DisposeThreaded_Internal(System.IntPtr)");
	_il2cpp_icall_func(___videoCaptureObj0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture::.cctor()
extern "C"  void VideoCapture__cctor_m2923959343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapture__cctor_m2923959343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VideoCapture_t3470796049_StaticFields*)VideoCapture_t3470796049_il2cpp_TypeInfo_var->static_fields)->set_HR_SUCCESS_0((((int64_t)((int64_t)0))));
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_OnStartedRecordingVideoCallback_t2612745932 (OnStartedRecordingVideoCallback_t2612745932 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(VideoCaptureResult_t3170939459 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnStartedRecordingVideoCallback__ctor_m3370445248 (OnStartedRecordingVideoCallback_t2612745932 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnStartedRecordingVideoCallback_Invoke_m3078230371 (OnStartedRecordingVideoCallback_t2612745932 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnStartedRecordingVideoCallback_BeginInvoke_m3360981378 (OnStartedRecordingVideoCallback_t2612745932 * __this, VideoCaptureResult_t3170939459  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnStartedRecordingVideoCallback_BeginInvoke_m3360981378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnStartedRecordingVideoCallback_EndInvoke_m2190174290 (OnStartedRecordingVideoCallback_t2612745932 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnStoppedRecordingVideoCallback_t1976525720 (OnStoppedRecordingVideoCallback_t1976525720 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(VideoCaptureResult_t3170939459 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnStoppedRecordingVideoCallback__ctor_m4251414672 (OnStoppedRecordingVideoCallback_t1976525720 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnStoppedRecordingVideoCallback_Invoke_m421367107 (OnStoppedRecordingVideoCallback_t1976525720 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnStoppedRecordingVideoCallback_BeginInvoke_m3232744182 (OnStoppedRecordingVideoCallback_t1976525720 * __this, VideoCaptureResult_t3170939459  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnStoppedRecordingVideoCallback_BeginInvoke_m3232744182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnStoppedRecordingVideoCallback_EndInvoke_m2526284270 (OnStoppedRecordingVideoCallback_t1976525720 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVideoCaptureResourceCreatedCallback__ctor_m533083098 (OnVideoCaptureResourceCreatedCallback_t3210647042 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture)
extern "C"  void OnVideoCaptureResourceCreatedCallback_Invoke_m742532456 (OnVideoCaptureResourceCreatedCallback_t3210647042 * __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___captureObject0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, VideoCapture_t3470796049 * ___captureObject0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___captureObject0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.VideoCapture,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVideoCaptureResourceCreatedCallback_BeginInvoke_m1622189105 (OnVideoCaptureResourceCreatedCallback_t3210647042 * __this, VideoCapture_t3470796049 * ___captureObject0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___captureObject0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoCaptureResourceCreatedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnVideoCaptureResourceCreatedCallback_EndInvoke_m2959759928 (OnVideoCaptureResourceCreatedCallback_t3210647042 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnVideoModeStartedCallback_t1811850718 (OnVideoModeStartedCallback_t1811850718 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(VideoCaptureResult_t3170939459 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVideoModeStartedCallback__ctor_m2101836460 (OnVideoModeStartedCallback_t1811850718 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnVideoModeStartedCallback_Invoke_m1419327261 (OnVideoModeStartedCallback_t1811850718 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVideoModeStartedCallback_BeginInvoke_m598044810 (OnVideoModeStartedCallback_t1811850718 * __this, VideoCaptureResult_t3170939459  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnVideoModeStartedCallback_BeginInvoke_m598044810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnVideoModeStartedCallback_EndInvoke_m1039292382 (OnVideoModeStartedCallback_t1811850718 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnVideoModeStoppedCallback_t1862037614 (OnVideoModeStoppedCallback_t1862037614 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(VideoCaptureResult_t3170939459 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___result0);

}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVideoModeStoppedCallback__ctor_m17486384 (OnVideoModeStoppedCallback_t1862037614 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback::Invoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult)
extern "C"  void OnVideoModeStoppedCallback_Invoke_m2621135601 (OnVideoModeStoppedCallback_t1862037614 * __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___result0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, VideoCaptureResult_t3170939459  ___result0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback::BeginInvoke(UnityEngine.VR.WSA.WebCam.VideoCapture/VideoCaptureResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVideoModeStoppedCallback_BeginInvoke_m1376282954 (OnVideoModeStoppedCallback_t1862037614 * __this, VideoCaptureResult_t3170939459  ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnVideoModeStoppedCallback_BeginInvoke_m1376282954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VideoCaptureResult_t3170939459_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnVideoModeStoppedCallback_EndInvoke_m2527219166 (OnVideoModeStoppedCallback_t1862037614 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WorldAnchor::Internal_TriggerEventOnTrackingLost(UnityEngine.VR.WSA.WorldAnchor,System.Boolean)
extern "C"  void WorldAnchor_Internal_TriggerEventOnTrackingLost_m1446545095 (Il2CppObject * __this /* static, unused */, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldAnchor_Internal_TriggerEventOnTrackingLost_m1446545095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WorldAnchor_t100028935 * L_0 = ___self0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		WorldAnchor_t100028935 * L_2 = ___self0;
		NullCheck(L_2);
		OnTrackingChangedDelegate_t417897799 * L_3 = L_2->get_OnTrackingChanged_2();
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		WorldAnchor_t100028935 * L_4 = ___self0;
		NullCheck(L_4);
		OnTrackingChangedDelegate_t417897799 * L_5 = L_4->get_OnTrackingChanged_2();
		WorldAnchor_t100028935 * L_6 = ___self0;
		bool L_7 = ___located1;
		NullCheck(L_5);
		OnTrackingChangedDelegate_Invoke_m1976992465(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTrackingChangedDelegate__ctor_m1211037979 (OnTrackingChangedDelegate_t417897799 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate::Invoke(UnityEngine.VR.WSA.WorldAnchor,System.Boolean)
extern "C"  void OnTrackingChangedDelegate_Invoke_m1976992465 (OnTrackingChangedDelegate_t417897799 * __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___self0, ___located1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___self0, ___located1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, bool ___located1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___self0, ___located1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___self0, ___located1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, WorldAnchor_t100028935 * ___self0, bool ___located1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___self0, ___located1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, bool ___located1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___self0, ___located1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate::BeginInvoke(UnityEngine.VR.WSA.WorldAnchor,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTrackingChangedDelegate_BeginInvoke_m4035619098 (OnTrackingChangedDelegate_t417897799 * __this, WorldAnchor_t100028935 * ___self0, bool ___located1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnTrackingChangedDelegate_BeginInvoke_m4035619098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___self0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___located1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnTrackingChangedDelegate_EndInvoke_m1185612877 (OnTrackingChangedDelegate_t417897799 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.VR.WSA.WorldManager::Internal_TriggerPositionalLocatorStateChanged(UnityEngine.VR.WSA.PositionalLocatorState,UnityEngine.VR.WSA.PositionalLocatorState)
extern "C"  void WorldManager_Internal_TriggerPositionalLocatorStateChanged_m3687861698 (Il2CppObject * __this /* static, unused */, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldManager_Internal_TriggerPositionalLocatorStateChanged_m3687861698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnPositionalLocatorStateChangedDelegate_t3864636813 * L_0 = ((WorldManager_t2948982693_StaticFields*)WorldManager_t2948982693_il2cpp_TypeInfo_var->static_fields)->get_OnPositionalLocatorStateChanged_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		OnPositionalLocatorStateChangedDelegate_t3864636813 * L_1 = ((WorldManager_t2948982693_StaticFields*)WorldManager_t2948982693_il2cpp_TypeInfo_var->static_fields)->get_OnPositionalLocatorStateChanged_0();
		int32_t L_2 = ___oldState0;
		int32_t L_3 = ___newState1;
		NullCheck(L_1);
		OnPositionalLocatorStateChangedDelegate_Invoke_m3729624897(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_OnPositionalLocatorStateChangedDelegate_t3864636813 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___oldState0, ___newState1);

}
// System.Void UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPositionalLocatorStateChangedDelegate__ctor_m3769223057 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate::Invoke(UnityEngine.VR.WSA.PositionalLocatorState,UnityEngine.VR.WSA.PositionalLocatorState)
extern "C"  void OnPositionalLocatorStateChangedDelegate_Invoke_m3729624897 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___oldState0, ___newState1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___oldState0, ___newState1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___oldState0, ___newState1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___oldState0, int32_t ___newState1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___oldState0, ___newState1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate::BeginInvoke(UnityEngine.VR.WSA.PositionalLocatorState,UnityEngine.VR.WSA.PositionalLocatorState,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPositionalLocatorStateChangedDelegate_BeginInvoke_m3423894028 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, int32_t ___oldState0, int32_t ___newState1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPositionalLocatorStateChangedDelegate_BeginInvoke_m3423894028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PositionalLocatorState_t3556528907_il2cpp_TypeInfo_var, &___oldState0);
	__d_args[1] = Box(PositionalLocatorState_t3556528907_il2cpp_TypeInfo_var, &___newState1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnPositionalLocatorStateChangedDelegate_EndInvoke_m3755248415 (OnPositionalLocatorStateChangedDelegate_t3864636813 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m3062480170 (WaitForEndOfFrame_t1785723201 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m3781413380 (WaitForFixedUpdate_t3968615785 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke(const WaitForSeconds_t3839502067& unmarshaled, WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_back(const WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled, WaitForSeconds_t3839502067& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_cleanup(WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_com(const WaitForSeconds_t3839502067& unmarshaled, WaitForSeconds_t3839502067_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t3839502067_marshal_com_back(const WaitForSeconds_t3839502067_marshaled_com& marshaled, WaitForSeconds_t3839502067& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_com_cleanup(WaitForSeconds_t3839502067_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float ___seconds0, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::Destroy(System.IntPtr)
extern "C"  void DictationRecognizer_Destroy_m2946358739 (Il2CppObject * __this /* static, unused */, IntPtr_t ___self0, const MethodInfo* method)
{
	typedef void (*DictationRecognizer_Destroy_m2946358739_ftn) (IntPtr_t);
	static DictationRecognizer_Destroy_m2946358739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DictationRecognizer_Destroy_m2946358739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Windows.Speech.DictationRecognizer::Destroy(System.IntPtr)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void DictationRecognizer_DestroyThreaded_m4202694898 (Il2CppObject * __this /* static, unused */, IntPtr_t ___self0, const MethodInfo* method)
{
	typedef void (*DictationRecognizer_DestroyThreaded_m4202694898_ftn) (IntPtr_t);
	static DictationRecognizer_DestroyThreaded_m4202694898_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DictationRecognizer_DestroyThreaded_m4202694898_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Windows.Speech.DictationRecognizer::DestroyThreaded(System.IntPtr)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::Finalize()
extern "C"  void DictationRecognizer_Finalize_m2612255109 (DictationRecognizer_t894834159 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictationRecognizer_Finalize_m2612255109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Recognizer_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0034;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_Recognizer_0();
			DictationRecognizer_DestroyThreaded_m4202694898(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_Recognizer_0(L_4);
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::Dispose()
extern "C"  void DictationRecognizer_Dispose_m3128061898 (DictationRecognizer_t894834159 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictationRecognizer_Dispose_m3128061898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_Recognizer_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_Recognizer_0();
		DictationRecognizer_Destroy_m2946358739(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Recognizer_0(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeHypothesisGeneratedEvent(System.String)
extern "C"  void DictationRecognizer_DictationRecognizer_InvokeHypothesisGeneratedEvent_m3853140412 (DictationRecognizer_t894834159 * __this, String_t* ___keyword0, const MethodInfo* method)
{
	DictationHypothesisDelegate_t1495849926 * V_0 = NULL;
	{
		DictationHypothesisDelegate_t1495849926 * L_0 = __this->get_DictationHypothesis_1();
		V_0 = L_0;
		DictationHypothesisDelegate_t1495849926 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		DictationHypothesisDelegate_t1495849926 * L_2 = V_0;
		String_t* L_3 = ___keyword0;
		NullCheck(L_2);
		DictationHypothesisDelegate_Invoke_m254632730(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeResultGeneratedEvent(System.String,UnityEngine.Windows.Speech.ConfidenceLevel)
extern "C"  void DictationRecognizer_DictationRecognizer_InvokeResultGeneratedEvent_m4212436139 (DictationRecognizer_t894834159 * __this, String_t* ___keyword0, int32_t ___minimumConfidence1, const MethodInfo* method)
{
	DictationResultDelegate_t1941514337 * V_0 = NULL;
	{
		DictationResultDelegate_t1941514337 * L_0 = __this->get_DictationResult_2();
		V_0 = L_0;
		DictationResultDelegate_t1941514337 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		DictationResultDelegate_t1941514337 * L_2 = V_0;
		String_t* L_3 = ___keyword0;
		int32_t L_4 = ___minimumConfidence1;
		NullCheck(L_2);
		DictationResultDelegate_Invoke_m415359993(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeCompletedEvent(UnityEngine.Windows.Speech.DictationCompletionCause)
extern "C"  void DictationRecognizer_DictationRecognizer_InvokeCompletedEvent_m587188734 (DictationRecognizer_t894834159 * __this, int32_t ___cause0, const MethodInfo* method)
{
	DictationCompletedDelegate_t3326551541 * V_0 = NULL;
	{
		DictationCompletedDelegate_t3326551541 * L_0 = __this->get_DictationComplete_3();
		V_0 = L_0;
		DictationCompletedDelegate_t3326551541 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		DictationCompletedDelegate_t3326551541 * L_2 = V_0;
		int32_t L_3 = ___cause0;
		NullCheck(L_2);
		DictationCompletedDelegate_Invoke_m1200484595(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeErrorEvent(System.String,System.Int32)
extern "C"  void DictationRecognizer_DictationRecognizer_InvokeErrorEvent_m1347773128 (DictationRecognizer_t894834159 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	DictationErrorHandler_t3730830311 * V_0 = NULL;
	{
		DictationErrorHandler_t3730830311 * L_0 = __this->get_DictationError_4();
		V_0 = L_0;
		DictationErrorHandler_t3730830311 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		DictationErrorHandler_t3730830311 * L_2 = V_0;
		String_t* L_3 = ___error0;
		int32_t L_4 = ___hresult1;
		NullCheck(L_2);
		DictationErrorHandler_Invoke_m665668646(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DictationCompletedDelegate_t3326551541 (DictationCompletedDelegate_t3326551541 * __this, int32_t ___cause0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___cause0);

}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DictationCompletedDelegate__ctor_m4154266941 (DictationCompletedDelegate_t3326551541 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate::Invoke(UnityEngine.Windows.Speech.DictationCompletionCause)
extern "C"  void DictationCompletedDelegate_Invoke_m1200484595 (DictationCompletedDelegate_t3326551541 * __this, int32_t ___cause0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___cause0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___cause0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___cause0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___cause0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___cause0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cause0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___cause0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cause0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate::BeginInvoke(UnityEngine.Windows.Speech.DictationCompletionCause,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DictationCompletedDelegate_BeginInvoke_m1045226018 (DictationCompletedDelegate_t3326551541 * __this, int32_t ___cause0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictationCompletedDelegate_BeginInvoke_m1045226018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DictationCompletionCause_t808236964_il2cpp_TypeInfo_var, &___cause0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DictationCompletedDelegate_EndInvoke_m3750415895 (DictationCompletedDelegate_t3326551541 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_DictationErrorHandler_t3730830311 (DictationErrorHandler_t3730830311 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled, ___hresult1);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DictationErrorHandler__ctor_m1283703231 (DictationErrorHandler_t3730830311 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler::Invoke(System.String,System.Int32)
extern "C"  void DictationErrorHandler_Invoke_m665668646 (DictationErrorHandler_t3730830311 * __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___hresult1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___error0, ___hresult1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___hresult1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___error0, ___hresult1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler::BeginInvoke(System.String,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DictationErrorHandler_BeginInvoke_m3004529127 (DictationErrorHandler_t3730830311 * __this, String_t* ___error0, int32_t ___hresult1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictationErrorHandler_BeginInvoke_m3004529127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___error0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___hresult1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DictationErrorHandler_EndInvoke_m146761245 (DictationErrorHandler_t3730830311 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_DictationHypothesisDelegate_t1495849926 (DictationHypothesisDelegate_t1495849926 * __this, String_t* ___text0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___text0' to native representation
	char* ____text0_marshaled = NULL;
	____text0_marshaled = il2cpp_codegen_marshal_string(___text0);

	// Native function invocation
	il2cppPInvokeFunc(____text0_marshaled);

	// Marshaling cleanup of parameter '___text0' native representation
	il2cpp_codegen_marshal_free(____text0_marshaled);
	____text0_marshaled = NULL;

}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DictationHypothesisDelegate__ctor_m114531386 (DictationHypothesisDelegate_t1495849926 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate::Invoke(System.String)
extern "C"  void DictationHypothesisDelegate_Invoke_m254632730 (DictationHypothesisDelegate_t1495849926 * __this, String_t* ___text0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___text0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___text0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, String_t* ___text0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___text0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___text0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___text0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___text0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, String_t* ___text0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___text0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___text0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DictationHypothesisDelegate_BeginInvoke_m840961127 (DictationHypothesisDelegate_t1495849926 * __this, String_t* ___text0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___text0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DictationHypothesisDelegate_EndInvoke_m67509276 (DictationHypothesisDelegate_t1495849926 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_DictationResultDelegate_t1941514337 (DictationResultDelegate_t1941514337 * __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___text0' to native representation
	char* ____text0_marshaled = NULL;
	____text0_marshaled = il2cpp_codegen_marshal_string(___text0);

	// Native function invocation
	il2cppPInvokeFunc(____text0_marshaled, ___confidence1);

	// Marshaling cleanup of parameter '___text0' native representation
	il2cpp_codegen_marshal_free(____text0_marshaled);
	____text0_marshaled = NULL;

}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DictationResultDelegate__ctor_m3584513173 (DictationResultDelegate_t1941514337 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate::Invoke(System.String,UnityEngine.Windows.Speech.ConfidenceLevel)
extern "C"  void DictationResultDelegate_Invoke_m415359993 (DictationResultDelegate_t1941514337 * __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___text0, ___confidence1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___text0, ___confidence1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___confidence1, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___text0, ___confidence1,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___text0, ___confidence1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, String_t* ___text0, int32_t ___confidence1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___text0, ___confidence1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___confidence1, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___text0, ___confidence1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate::BeginInvoke(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DictationResultDelegate_BeginInvoke_m2097544744 (DictationResultDelegate_t1941514337 * __this, String_t* ___text0, int32_t ___confidence1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictationResultDelegate_BeginInvoke_m2097544744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___text0;
	__d_args[1] = Box(ConfidenceLevel_t3540086540_il2cpp_TypeInfo_var, &___confidence1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DictationResultDelegate_EndInvoke_m998944031 (DictationResultDelegate_t1941514337 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem::PhraseRecognitionSystem_InvokeErrorEvent(UnityEngine.Windows.Speech.SpeechError)
extern "C"  void PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeErrorEvent_m2371098489 (Il2CppObject * __this /* static, unused */, int32_t ___errorCode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeErrorEvent_m2371098489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorDelegate_t3690655641 * V_0 = NULL;
	{
		ErrorDelegate_t3690655641 * L_0 = ((PhraseRecognitionSystem_t1642125421_StaticFields*)PhraseRecognitionSystem_t1642125421_il2cpp_TypeInfo_var->static_fields)->get_OnError_0();
		V_0 = L_0;
		ErrorDelegate_t3690655641 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		ErrorDelegate_t3690655641 * L_2 = V_0;
		int32_t L_3 = ___errorCode0;
		NullCheck(L_2);
		ErrorDelegate_Invoke_m370336905(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem::PhraseRecognitionSystem_InvokeStatusChangedEvent(UnityEngine.Windows.Speech.SpeechSystemStatus)
extern "C"  void PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeStatusChangedEvent_m2976065820 (Il2CppObject * __this /* static, unused */, int32_t ___status0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeStatusChangedEvent_m2976065820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StatusDelegate_t2739717665 * V_0 = NULL;
	{
		StatusDelegate_t2739717665 * L_0 = ((PhraseRecognitionSystem_t1642125421_StaticFields*)PhraseRecognitionSystem_t1642125421_il2cpp_TypeInfo_var->static_fields)->get_OnStatusChanged_1();
		V_0 = L_0;
		StatusDelegate_t2739717665 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		StatusDelegate_t2739717665 * L_2 = V_0;
		int32_t L_3 = ___status0;
		NullCheck(L_2);
		StatusDelegate_Invoke_m4078514336(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_ErrorDelegate_t3690655641 (ErrorDelegate_t3690655641 * __this, int32_t ___errorCode0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___errorCode0);

}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorDelegate__ctor_m3890014189 (ErrorDelegate_t3690655641 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate::Invoke(UnityEngine.Windows.Speech.SpeechError)
extern "C"  void ErrorDelegate_Invoke_m370336905 (ErrorDelegate_t3690655641 * __this, int32_t ___errorCode0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___errorCode0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___errorCode0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___errorCode0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___errorCode0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___errorCode0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___errorCode0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___errorCode0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___errorCode0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate::BeginInvoke(UnityEngine.Windows.Speech.SpeechError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorDelegate_BeginInvoke_m1046632338 (ErrorDelegate_t3690655641 * __this, int32_t ___errorCode0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorDelegate_BeginInvoke_m1046632338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SpeechError_t3947426626_il2cpp_TypeInfo_var, &___errorCode0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorDelegate_EndInvoke_m1800410711 (ErrorDelegate_t3690655641 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_StatusDelegate_t2739717665 (StatusDelegate_t2739717665 * __this, int32_t ___status0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___status0);

}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void StatusDelegate__ctor_m2548030881 (StatusDelegate_t2739717665 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate::Invoke(UnityEngine.Windows.Speech.SpeechSystemStatus)
extern "C"  void StatusDelegate_Invoke_m4078514336 (StatusDelegate_t2739717665 * __this, int32_t ___status0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___status0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___status0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___status0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___status0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___status0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___status0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___status0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___status0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate::BeginInvoke(UnityEngine.Windows.Speech.SpeechSystemStatus,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StatusDelegate_BeginInvoke_m3757077471 (StatusDelegate_t2739717665 * __this, int32_t ___status0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StatusDelegate_BeginInvoke_m3757077471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SpeechSystemStatus_t1827393363_il2cpp_TypeInfo_var, &___status0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void StatusDelegate_EndInvoke_m777792603 (StatusDelegate_t2739717665 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}


// Conversion methods for marshalling of: UnityEngine.Windows.Speech.PhraseRecognizedEventArgs
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke(const PhraseRecognizedEventArgs_t3185826360& unmarshaled, PhraseRecognizedEventArgs_t3185826360_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___semanticMeanings_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'semanticMeanings' of type 'PhraseRecognizedEventArgs'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___semanticMeanings_1Exception);
}
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_back(const PhraseRecognizedEventArgs_t3185826360_marshaled_pinvoke& marshaled, PhraseRecognizedEventArgs_t3185826360& unmarshaled)
{
	Il2CppCodeGenException* ___semanticMeanings_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'semanticMeanings' of type 'PhraseRecognizedEventArgs'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___semanticMeanings_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Windows.Speech.PhraseRecognizedEventArgs
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_cleanup(PhraseRecognizedEventArgs_t3185826360_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.Windows.Speech.PhraseRecognizedEventArgs
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_com(const PhraseRecognizedEventArgs_t3185826360& unmarshaled, PhraseRecognizedEventArgs_t3185826360_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___semanticMeanings_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'semanticMeanings' of type 'PhraseRecognizedEventArgs'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___semanticMeanings_1Exception);
}
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_com_back(const PhraseRecognizedEventArgs_t3185826360_marshaled_com& marshaled, PhraseRecognizedEventArgs_t3185826360& unmarshaled)
{
	Il2CppCodeGenException* ___semanticMeanings_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'semanticMeanings' of type 'PhraseRecognizedEventArgs'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___semanticMeanings_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Windows.Speech.PhraseRecognizedEventArgs
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_com_cleanup(PhraseRecognizedEventArgs_t3185826360_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizedEventArgs::.ctor(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,UnityEngine.Windows.Speech.SemanticMeaning[],System.DateTime,System.TimeSpan)
extern "C"  void PhraseRecognizedEventArgs__ctor_m1949420707 (PhraseRecognizedEventArgs_t3185826360 * __this, String_t* ___text0, int32_t ___confidence1, SemanticMeaningU5BU5D_t2038511870* ___semanticMeanings2, DateTime_t693205669  ___phraseStartTime3, TimeSpan_t3430258949  ___phraseDuration4, const MethodInfo* method)
{
	{
		String_t* L_0 = ___text0;
		__this->set_text_2(L_0);
		int32_t L_1 = ___confidence1;
		__this->set_confidence_0(L_1);
		SemanticMeaningU5BU5D_t2038511870* L_2 = ___semanticMeanings2;
		__this->set_semanticMeanings_1(L_2);
		DateTime_t693205669  L_3 = ___phraseStartTime3;
		__this->set_phraseStartTime_3(L_3);
		TimeSpan_t3430258949  L_4 = ___phraseDuration4;
		__this->set_phraseDuration_4(L_4);
		return;
	}
}
extern "C"  void PhraseRecognizedEventArgs__ctor_m1949420707_AdjustorThunk (Il2CppObject * __this, String_t* ___text0, int32_t ___confidence1, SemanticMeaningU5BU5D_t2038511870* ___semanticMeanings2, DateTime_t693205669  ___phraseStartTime3, TimeSpan_t3430258949  ___phraseDuration4, const MethodInfo* method)
{
	PhraseRecognizedEventArgs_t3185826360 * _thisAdjusted = reinterpret_cast<PhraseRecognizedEventArgs_t3185826360 *>(__this + 1);
	PhraseRecognizedEventArgs__ctor_m1949420707(_thisAdjusted, ___text0, ___confidence1, ___semanticMeanings2, ___phraseStartTime3, ___phraseDuration4, method);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::Destroy(System.IntPtr)
extern "C"  void PhraseRecognizer_Destroy_m3239968945 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method)
{
	typedef void (*PhraseRecognizer_Destroy_m3239968945_ftn) (IntPtr_t);
	static PhraseRecognizer_Destroy_m3239968945_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhraseRecognizer_Destroy_m3239968945_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Windows.Speech.PhraseRecognizer::Destroy(System.IntPtr)");
	_il2cpp_icall_func(___recognizer0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::DestroyThreaded(System.IntPtr)
extern "C"  void PhraseRecognizer_DestroyThreaded_m1399474592 (Il2CppObject * __this /* static, unused */, IntPtr_t ___recognizer0, const MethodInfo* method)
{
	typedef void (*PhraseRecognizer_DestroyThreaded_m1399474592_ftn) (IntPtr_t);
	static PhraseRecognizer_DestroyThreaded_m1399474592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhraseRecognizer_DestroyThreaded_m1399474592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Windows.Speech.PhraseRecognizer::DestroyThreaded(System.IntPtr)");
	_il2cpp_icall_func(___recognizer0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::Finalize()
extern "C"  void PhraseRecognizer_Finalize_m3514624903 (PhraseRecognizer_t1627945097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognizer_Finalize_m3514624903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Recognizer_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0034;
			}
		}

IL_0016:
		{
			IntPtr_t L_3 = __this->get_m_Recognizer_0();
			PhraseRecognizer_DestroyThreaded_m1399474592(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_m_Recognizer_0(L_4);
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::Dispose()
extern "C"  void PhraseRecognizer_Dispose_m40972164 (PhraseRecognizer_t1627945097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognizer_Dispose_m40972164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_Recognizer_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		IntPtr_t L_3 = __this->get_m_Recognizer_0();
		PhraseRecognizer_Destroy_m3239968945(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Recognizer_0(L_4);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer::InvokePhraseRecognizedEvent(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,UnityEngine.Windows.Speech.SemanticMeaning[],System.Int64,System.Int64)
extern "C"  void PhraseRecognizer_InvokePhraseRecognizedEvent_m4062135779 (PhraseRecognizer_t1627945097 * __this, String_t* ___text0, int32_t ___confidence1, SemanticMeaningU5BU5D_t2038511870* ___semanticMeanings2, int64_t ___phraseStartFileTime3, int64_t ___phraseDurationTicks4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognizer_InvokePhraseRecognizedEvent_m4062135779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PhraseRecognizedDelegate_t438723648 * V_0 = NULL;
	{
		PhraseRecognizedDelegate_t438723648 * L_0 = __this->get_OnPhraseRecognized_1();
		V_0 = L_0;
		PhraseRecognizedDelegate_t438723648 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		PhraseRecognizedDelegate_t438723648 * L_2 = V_0;
		String_t* L_3 = ___text0;
		int32_t L_4 = ___confidence1;
		SemanticMeaningU5BU5D_t2038511870* L_5 = ___semanticMeanings2;
		int64_t L_6 = ___phraseStartFileTime3;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_7 = DateTime_FromFileTime_m725937452(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int64_t L_8 = ___phraseDurationTicks4;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_9 = TimeSpan_FromTicks_m827965597(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		PhraseRecognizedEventArgs_t3185826360  L_10;
		memset(&L_10, 0, sizeof(L_10));
		PhraseRecognizedEventArgs__ctor_m1949420707(&L_10, L_3, L_4, L_5, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		PhraseRecognizedDelegate_Invoke_m2632448538(L_2, L_10, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// UnityEngine.Windows.Speech.SemanticMeaning[] UnityEngine.Windows.Speech.PhraseRecognizer::MarshalSemanticMeaning(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern "C"  SemanticMeaningU5BU5D_t2038511870* PhraseRecognizer_MarshalSemanticMeaning_m621334147 (Il2CppObject * __this /* static, unused */, IntPtr_t ___keys0, IntPtr_t ___values1, IntPtr_t ___valueSizes2, int32_t ___valueCount3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognizer_MarshalSemanticMeaning_m621334147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SemanticMeaningU5BU5D_t2038511870* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	SemanticMeaning_t2306793223  V_4;
	memset(&V_4, 0, sizeof(V_4));
	SemanticMeaning_t2306793223  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	SemanticMeaningU5BU5D_t2038511870* V_7 = NULL;
	{
		int32_t L_0 = ___valueCount3;
		V_0 = ((SemanticMeaningU5BU5D_t2038511870*)SZArrayNew(SemanticMeaningU5BU5D_t2038511870_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = 0;
		V_2 = 0;
		goto IL_00a8;
	}

IL_0011:
	{
		IntPtr_t L_1 = ___valueSizes2;
		void* L_2 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_2;
		V_3 = (*((uint32_t*)((void*)((intptr_t)L_2+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_3))*(int32_t)4))))));
		Initobj (SemanticMeaning_t2306793223_il2cpp_TypeInfo_var, (&V_5));
		IntPtr_t L_4 = ___keys0;
		void* L_5 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		uint32_t L_7 = il2cpp_codegen_sizeof(CharU2A_t2911836366_il2cpp_TypeInfo_var);
		String_t* L_8 = String_CreateString_m4236499327(NULL, (Il2CppChar*)(Il2CppChar*)(*((intptr_t*)((void*)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7)))))), /*hidden argument*/NULL);
		(&V_5)->set_key_0(L_8);
		uint32_t L_9 = V_3;
		(&V_5)->set_values_1(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_9)))));
		SemanticMeaning_t2306793223  L_10 = V_5;
		V_4 = L_10;
		V_6 = 0;
		goto IL_0087;
	}

IL_005e:
	{
		StringU5BU5D_t1642385972* L_11 = (&V_4)->get_values_1();
		int32_t L_12 = V_6;
		IntPtr_t L_13 = ___values1;
		void* L_14 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		int32_t L_16 = V_6;
		uint32_t L_17 = il2cpp_codegen_sizeof(CharU2A_t2911836366_il2cpp_TypeInfo_var);
		String_t* L_18 = String_CreateString_m4236499327(NULL, (Il2CppChar*)(Il2CppChar*)(*((intptr_t*)((void*)((intptr_t)L_14+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)((int32_t)((int32_t)L_15+(int32_t)L_16))))*(int32_t)L_17)))))), /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_18);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (String_t*)L_18);
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_20 = V_6;
		uint32_t L_21 = V_3;
		if ((((int64_t)(((int64_t)((int64_t)L_20)))) < ((int64_t)(((int64_t)((uint64_t)L_21))))))
		{
			goto IL_005e;
		}
	}
	{
		SemanticMeaningU5BU5D_t2038511870* L_22 = V_0;
		int32_t L_23 = V_2;
		NullCheck(L_22);
		SemanticMeaning_t2306793223  L_24 = V_4;
		(*(SemanticMeaning_t2306793223 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
		int32_t L_25 = V_1;
		uint32_t L_26 = V_3;
		V_1 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00a8:
	{
		int32_t L_28 = V_2;
		int32_t L_29 = ___valueCount3;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0011;
		}
	}
	{
		SemanticMeaningU5BU5D_t2038511870* L_30 = V_0;
		V_7 = L_30;
		goto IL_00b7;
	}

IL_00b7:
	{
		SemanticMeaningU5BU5D_t2038511870* L_31 = V_7;
		return L_31;
	}
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void PhraseRecognizedDelegate__ctor_m789028824 (PhraseRecognizedDelegate_t438723648 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate::Invoke(UnityEngine.Windows.Speech.PhraseRecognizedEventArgs)
extern "C"  void PhraseRecognizedDelegate_Invoke_m2632448538 (PhraseRecognizedDelegate_t438723648 * __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___args0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___args0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, PhraseRecognizedEventArgs_t3185826360  ___args0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate::BeginInvoke(UnityEngine.Windows.Speech.PhraseRecognizedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PhraseRecognizedDelegate_BeginInvoke_m1044057473 (PhraseRecognizedDelegate_t438723648 * __this, PhraseRecognizedEventArgs_t3185826360  ___args0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhraseRecognizedDelegate_BeginInvoke_m1044057473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PhraseRecognizedEventArgs_t3185826360_il2cpp_TypeInfo_var, &___args0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Windows.Speech.PhraseRecognizer/PhraseRecognizedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void PhraseRecognizedDelegate_EndInvoke_m4241877806 (PhraseRecognizedDelegate_t438723648 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.Windows.Speech.SemanticMeaning
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke(const SemanticMeaning_t2306793223& unmarshaled, SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___values_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'values' of type 'SemanticMeaning'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___values_1Exception);
}
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_back(const SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled, SemanticMeaning_t2306793223& unmarshaled)
{
	Il2CppCodeGenException* ___values_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'values' of type 'SemanticMeaning'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___values_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Windows.Speech.SemanticMeaning
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_cleanup(SemanticMeaning_t2306793223_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Windows.Speech.SemanticMeaning
extern "C" void SemanticMeaning_t2306793223_marshal_com(const SemanticMeaning_t2306793223& unmarshaled, SemanticMeaning_t2306793223_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___values_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'values' of type 'SemanticMeaning'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___values_1Exception);
}
extern "C" void SemanticMeaning_t2306793223_marshal_com_back(const SemanticMeaning_t2306793223_marshaled_com& marshaled, SemanticMeaning_t2306793223& unmarshaled)
{
	Il2CppCodeGenException* ___values_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'values' of type 'SemanticMeaning'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___values_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Windows.Speech.SemanticMeaning
extern "C" void SemanticMeaning_t2306793223_marshal_com_cleanup(SemanticMeaning_t2306793223_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m761932763 (WritableAttribute_t3715198420 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
