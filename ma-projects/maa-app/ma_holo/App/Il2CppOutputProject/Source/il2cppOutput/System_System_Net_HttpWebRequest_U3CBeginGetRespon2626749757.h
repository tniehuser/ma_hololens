﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.WebAsyncResult
struct WebAsyncResult_t905414499;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest/<BeginGetResponse>c__AnonStorey0
struct  U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757  : public Il2CppObject
{
public:
	// System.Net.WebAsyncResult System.Net.HttpWebRequest/<BeginGetResponse>c__AnonStorey0::aread
	WebAsyncResult_t905414499 * ___aread_0;
	// System.Net.HttpWebRequest System.Net.HttpWebRequest/<BeginGetResponse>c__AnonStorey0::$this
	HttpWebRequest_t1951404513 * ___U24this_1;

public:
	inline static int32_t get_offset_of_aread_0() { return static_cast<int32_t>(offsetof(U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757, ___aread_0)); }
	inline WebAsyncResult_t905414499 * get_aread_0() const { return ___aread_0; }
	inline WebAsyncResult_t905414499 ** get_address_of_aread_0() { return &___aread_0; }
	inline void set_aread_0(WebAsyncResult_t905414499 * value)
	{
		___aread_0 = value;
		Il2CppCodeGenWriteBarrier(&___aread_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757, ___U24this_1)); }
	inline HttpWebRequest_t1951404513 * get_U24this_1() const { return ___U24this_1; }
	inline HttpWebRequest_t1951404513 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HttpWebRequest_t1951404513 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
