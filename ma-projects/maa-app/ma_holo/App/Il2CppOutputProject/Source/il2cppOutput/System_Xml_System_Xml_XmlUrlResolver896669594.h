﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlResolver2024571559.h"

// System.Object
struct Il2CppObject;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t896669594  : public XmlResolver_t2024571559
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::_credentials
	Il2CppObject * ____credentials_1;
	// System.Net.IWebProxy System.Xml.XmlUrlResolver::_proxy
	Il2CppObject * ____proxy_2;
	// System.Net.Cache.RequestCachePolicy System.Xml.XmlUrlResolver::_cachePolicy
	RequestCachePolicy_t2663429579 * ____cachePolicy_3;

public:
	inline static int32_t get_offset_of__credentials_1() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____credentials_1)); }
	inline Il2CppObject * get__credentials_1() const { return ____credentials_1; }
	inline Il2CppObject ** get_address_of__credentials_1() { return &____credentials_1; }
	inline void set__credentials_1(Il2CppObject * value)
	{
		____credentials_1 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_1, value);
	}

	inline static int32_t get_offset_of__proxy_2() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____proxy_2)); }
	inline Il2CppObject * get__proxy_2() const { return ____proxy_2; }
	inline Il2CppObject ** get_address_of__proxy_2() { return &____proxy_2; }
	inline void set__proxy_2(Il2CppObject * value)
	{
		____proxy_2 = value;
		Il2CppCodeGenWriteBarrier(&____proxy_2, value);
	}

	inline static int32_t get_offset_of__cachePolicy_3() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____cachePolicy_3)); }
	inline RequestCachePolicy_t2663429579 * get__cachePolicy_3() const { return ____cachePolicy_3; }
	inline RequestCachePolicy_t2663429579 ** get_address_of__cachePolicy_3() { return &____cachePolicy_3; }
	inline void set__cachePolicy_3(RequestCachePolicy_t2663429579 * value)
	{
		____cachePolicy_3 = value;
		Il2CppCodeGenWriteBarrier(&____cachePolicy_3, value);
	}
};

struct XmlUrlResolver_t896669594_StaticFields
{
public:
	// System.Object System.Xml.XmlUrlResolver::s_DownloadManager
	Il2CppObject * ___s_DownloadManager_0;

public:
	inline static int32_t get_offset_of_s_DownloadManager_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594_StaticFields, ___s_DownloadManager_0)); }
	inline Il2CppObject * get_s_DownloadManager_0() const { return ___s_DownloadManager_0; }
	inline Il2CppObject ** get_address_of_s_DownloadManager_0() { return &___s_DownloadManager_0; }
	inline void set_s_DownloadManager_0(Il2CppObject * value)
	{
		___s_DownloadManager_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_DownloadManager_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
