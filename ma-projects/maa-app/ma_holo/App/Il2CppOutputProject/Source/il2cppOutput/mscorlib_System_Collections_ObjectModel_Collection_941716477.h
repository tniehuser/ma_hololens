﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IList`1<System.Net.IPAddress>
struct IList_1_t1940912324;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<System.Net.IPAddress>
struct  Collection_1_t941716477  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	Il2CppObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	Il2CppObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t941716477, ___items_0)); }
	inline Il2CppObject* get_items_0() const { return ___items_0; }
	inline Il2CppObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(Il2CppObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier(&___items_0, value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t941716477, ____syncRoot_1)); }
	inline Il2CppObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline Il2CppObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(Il2CppObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
