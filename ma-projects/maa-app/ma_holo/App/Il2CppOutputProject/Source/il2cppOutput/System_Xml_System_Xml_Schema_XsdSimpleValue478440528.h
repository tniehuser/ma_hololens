﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdSimpleValue
struct  XsdSimpleValue_t478440528  : public Il2CppObject
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XsdSimpleValue::xmlType
	XmlSchemaSimpleType_t248156492 * ___xmlType_0;
	// System.Object System.Xml.Schema.XsdSimpleValue::typedValue
	Il2CppObject * ___typedValue_1;

public:
	inline static int32_t get_offset_of_xmlType_0() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t478440528, ___xmlType_0)); }
	inline XmlSchemaSimpleType_t248156492 * get_xmlType_0() const { return ___xmlType_0; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_xmlType_0() { return &___xmlType_0; }
	inline void set_xmlType_0(XmlSchemaSimpleType_t248156492 * value)
	{
		___xmlType_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlType_0, value);
	}

	inline static int32_t get_offset_of_typedValue_1() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t478440528, ___typedValue_1)); }
	inline Il2CppObject * get_typedValue_1() const { return ___typedValue_1; }
	inline Il2CppObject ** get_address_of_typedValue_1() { return &___typedValue_1; }
	inline void set_typedValue_1(Il2CppObject * value)
	{
		___typedValue_1 = value;
		Il2CppCodeGenWriteBarrier(&___typedValue_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
