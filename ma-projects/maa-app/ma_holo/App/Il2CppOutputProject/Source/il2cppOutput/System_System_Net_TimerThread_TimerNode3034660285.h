﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_TimerThread_Timer2936528677.h"
#include "System_System_Net_TimerThread_TimerNode_TimerState3530038064.h"

// System.Net.TimerThread/Callback
struct Callback_t697818895;
// System.Object
struct Il2CppObject;
// System.Net.TimerThread/TimerNode
struct TimerNode_t3034660285;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/TimerNode
struct  TimerNode_t3034660285  : public Timer_t2936528677
{
public:
	// System.Net.TimerThread/TimerNode/TimerState System.Net.TimerThread/TimerNode::m_TimerState
	int32_t ___m_TimerState_2;
	// System.Net.TimerThread/Callback System.Net.TimerThread/TimerNode::m_Callback
	Callback_t697818895 * ___m_Callback_3;
	// System.Object System.Net.TimerThread/TimerNode::m_Context
	Il2CppObject * ___m_Context_4;
	// System.Object System.Net.TimerThread/TimerNode::m_QueueLock
	Il2CppObject * ___m_QueueLock_5;
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::next
	TimerNode_t3034660285 * ___next_6;
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::prev
	TimerNode_t3034660285 * ___prev_7;

public:
	inline static int32_t get_offset_of_m_TimerState_2() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___m_TimerState_2)); }
	inline int32_t get_m_TimerState_2() const { return ___m_TimerState_2; }
	inline int32_t* get_address_of_m_TimerState_2() { return &___m_TimerState_2; }
	inline void set_m_TimerState_2(int32_t value)
	{
		___m_TimerState_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___m_Callback_3)); }
	inline Callback_t697818895 * get_m_Callback_3() const { return ___m_Callback_3; }
	inline Callback_t697818895 ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(Callback_t697818895 * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Callback_3, value);
	}

	inline static int32_t get_offset_of_m_Context_4() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___m_Context_4)); }
	inline Il2CppObject * get_m_Context_4() const { return ___m_Context_4; }
	inline Il2CppObject ** get_address_of_m_Context_4() { return &___m_Context_4; }
	inline void set_m_Context_4(Il2CppObject * value)
	{
		___m_Context_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Context_4, value);
	}

	inline static int32_t get_offset_of_m_QueueLock_5() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___m_QueueLock_5)); }
	inline Il2CppObject * get_m_QueueLock_5() const { return ___m_QueueLock_5; }
	inline Il2CppObject ** get_address_of_m_QueueLock_5() { return &___m_QueueLock_5; }
	inline void set_m_QueueLock_5(Il2CppObject * value)
	{
		___m_QueueLock_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_QueueLock_5, value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___next_6)); }
	inline TimerNode_t3034660285 * get_next_6() const { return ___next_6; }
	inline TimerNode_t3034660285 ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(TimerNode_t3034660285 * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier(&___next_6, value);
	}

	inline static int32_t get_offset_of_prev_7() { return static_cast<int32_t>(offsetof(TimerNode_t3034660285, ___prev_7)); }
	inline TimerNode_t3034660285 * get_prev_7() const { return ___prev_7; }
	inline TimerNode_t3034660285 ** get_address_of_prev_7() { return &___prev_7; }
	inline void set_prev_7(TimerNode_t3034660285 * value)
	{
		___prev_7 = value;
		Il2CppCodeGenWriteBarrier(&___prev_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
