﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "mscorlib_System_Guid2533601593.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReflectTypeDescriptionProvider
struct  ReflectTypeDescriptionProvider_t1753777466  : public TypeDescriptionProvider_t2438624375
{
public:
	// System.Collections.Hashtable System.ComponentModel.ReflectTypeDescriptionProvider::_typeData
	Hashtable_t909839986 * ____typeData_2;

public:
	inline static int32_t get_offset_of__typeData_2() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466, ____typeData_2)); }
	inline Hashtable_t909839986 * get__typeData_2() const { return ____typeData_2; }
	inline Hashtable_t909839986 ** get_address_of__typeData_2() { return &____typeData_2; }
	inline void set__typeData_2(Hashtable_t909839986 * value)
	{
		____typeData_2 = value;
		Il2CppCodeGenWriteBarrier(&____typeData_2, value);
	}
};

struct ReflectTypeDescriptionProvider_t1753777466_StaticFields
{
public:
	// System.Type[] System.ComponentModel.ReflectTypeDescriptionProvider::_typeConstructor
	TypeU5BU5D_t1664964607* ____typeConstructor_3;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicTypeConverters
	Hashtable_t909839986 * ____intrinsicTypeConverters_4;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicReferenceKey
	Il2CppObject * ____intrinsicReferenceKey_5;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_intrinsicNullableKey
	Il2CppObject * ____intrinsicNullableKey_6;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_dictionaryKey
	Il2CppObject * ____dictionaryKey_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.ReflectTypeDescriptionProvider::_attributeCache
	Hashtable_t909839986 * ____attributeCache_8;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderProviderKey
	Guid_t  ____extenderProviderKey_9;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderPropertiesKey
	Guid_t  ____extenderPropertiesKey_10;
	// System.Guid System.ComponentModel.ReflectTypeDescriptionProvider::_extenderProviderPropertiesKey
	Guid_t  ____extenderProviderPropertiesKey_11;
	// System.Type[] System.ComponentModel.ReflectTypeDescriptionProvider::_skipInterfaceAttributeList
	TypeU5BU5D_t1664964607* ____skipInterfaceAttributeList_12;
	// System.Object System.ComponentModel.ReflectTypeDescriptionProvider::_internalSyncObject
	Il2CppObject * ____internalSyncObject_13;

public:
	inline static int32_t get_offset_of__typeConstructor_3() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____typeConstructor_3)); }
	inline TypeU5BU5D_t1664964607* get__typeConstructor_3() const { return ____typeConstructor_3; }
	inline TypeU5BU5D_t1664964607** get_address_of__typeConstructor_3() { return &____typeConstructor_3; }
	inline void set__typeConstructor_3(TypeU5BU5D_t1664964607* value)
	{
		____typeConstructor_3 = value;
		Il2CppCodeGenWriteBarrier(&____typeConstructor_3, value);
	}

	inline static int32_t get_offset_of__intrinsicTypeConverters_4() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____intrinsicTypeConverters_4)); }
	inline Hashtable_t909839986 * get__intrinsicTypeConverters_4() const { return ____intrinsicTypeConverters_4; }
	inline Hashtable_t909839986 ** get_address_of__intrinsicTypeConverters_4() { return &____intrinsicTypeConverters_4; }
	inline void set__intrinsicTypeConverters_4(Hashtable_t909839986 * value)
	{
		____intrinsicTypeConverters_4 = value;
		Il2CppCodeGenWriteBarrier(&____intrinsicTypeConverters_4, value);
	}

	inline static int32_t get_offset_of__intrinsicReferenceKey_5() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____intrinsicReferenceKey_5)); }
	inline Il2CppObject * get__intrinsicReferenceKey_5() const { return ____intrinsicReferenceKey_5; }
	inline Il2CppObject ** get_address_of__intrinsicReferenceKey_5() { return &____intrinsicReferenceKey_5; }
	inline void set__intrinsicReferenceKey_5(Il2CppObject * value)
	{
		____intrinsicReferenceKey_5 = value;
		Il2CppCodeGenWriteBarrier(&____intrinsicReferenceKey_5, value);
	}

	inline static int32_t get_offset_of__intrinsicNullableKey_6() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____intrinsicNullableKey_6)); }
	inline Il2CppObject * get__intrinsicNullableKey_6() const { return ____intrinsicNullableKey_6; }
	inline Il2CppObject ** get_address_of__intrinsicNullableKey_6() { return &____intrinsicNullableKey_6; }
	inline void set__intrinsicNullableKey_6(Il2CppObject * value)
	{
		____intrinsicNullableKey_6 = value;
		Il2CppCodeGenWriteBarrier(&____intrinsicNullableKey_6, value);
	}

	inline static int32_t get_offset_of__dictionaryKey_7() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____dictionaryKey_7)); }
	inline Il2CppObject * get__dictionaryKey_7() const { return ____dictionaryKey_7; }
	inline Il2CppObject ** get_address_of__dictionaryKey_7() { return &____dictionaryKey_7; }
	inline void set__dictionaryKey_7(Il2CppObject * value)
	{
		____dictionaryKey_7 = value;
		Il2CppCodeGenWriteBarrier(&____dictionaryKey_7, value);
	}

	inline static int32_t get_offset_of__attributeCache_8() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____attributeCache_8)); }
	inline Hashtable_t909839986 * get__attributeCache_8() const { return ____attributeCache_8; }
	inline Hashtable_t909839986 ** get_address_of__attributeCache_8() { return &____attributeCache_8; }
	inline void set__attributeCache_8(Hashtable_t909839986 * value)
	{
		____attributeCache_8 = value;
		Il2CppCodeGenWriteBarrier(&____attributeCache_8, value);
	}

	inline static int32_t get_offset_of__extenderProviderKey_9() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____extenderProviderKey_9)); }
	inline Guid_t  get__extenderProviderKey_9() const { return ____extenderProviderKey_9; }
	inline Guid_t * get_address_of__extenderProviderKey_9() { return &____extenderProviderKey_9; }
	inline void set__extenderProviderKey_9(Guid_t  value)
	{
		____extenderProviderKey_9 = value;
	}

	inline static int32_t get_offset_of__extenderPropertiesKey_10() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____extenderPropertiesKey_10)); }
	inline Guid_t  get__extenderPropertiesKey_10() const { return ____extenderPropertiesKey_10; }
	inline Guid_t * get_address_of__extenderPropertiesKey_10() { return &____extenderPropertiesKey_10; }
	inline void set__extenderPropertiesKey_10(Guid_t  value)
	{
		____extenderPropertiesKey_10 = value;
	}

	inline static int32_t get_offset_of__extenderProviderPropertiesKey_11() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____extenderProviderPropertiesKey_11)); }
	inline Guid_t  get__extenderProviderPropertiesKey_11() const { return ____extenderProviderPropertiesKey_11; }
	inline Guid_t * get_address_of__extenderProviderPropertiesKey_11() { return &____extenderProviderPropertiesKey_11; }
	inline void set__extenderProviderPropertiesKey_11(Guid_t  value)
	{
		____extenderProviderPropertiesKey_11 = value;
	}

	inline static int32_t get_offset_of__skipInterfaceAttributeList_12() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____skipInterfaceAttributeList_12)); }
	inline TypeU5BU5D_t1664964607* get__skipInterfaceAttributeList_12() const { return ____skipInterfaceAttributeList_12; }
	inline TypeU5BU5D_t1664964607** get_address_of__skipInterfaceAttributeList_12() { return &____skipInterfaceAttributeList_12; }
	inline void set__skipInterfaceAttributeList_12(TypeU5BU5D_t1664964607* value)
	{
		____skipInterfaceAttributeList_12 = value;
		Il2CppCodeGenWriteBarrier(&____skipInterfaceAttributeList_12, value);
	}

	inline static int32_t get_offset_of__internalSyncObject_13() { return static_cast<int32_t>(offsetof(ReflectTypeDescriptionProvider_t1753777466_StaticFields, ____internalSyncObject_13)); }
	inline Il2CppObject * get__internalSyncObject_13() const { return ____internalSyncObject_13; }
	inline Il2CppObject ** get_address_of__internalSyncObject_13() { return &____internalSyncObject_13; }
	inline void set__internalSyncObject_13(Il2CppObject * value)
	{
		____internalSyncObject_13 = value;
		Il2CppCodeGenWriteBarrier(&____internalSyncObject_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
