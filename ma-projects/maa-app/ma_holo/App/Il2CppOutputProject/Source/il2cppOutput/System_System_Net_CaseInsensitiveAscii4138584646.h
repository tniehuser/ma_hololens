﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.CaseInsensitiveAscii
struct CaseInsensitiveAscii_t4138584646;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CaseInsensitiveAscii
struct  CaseInsensitiveAscii_t4138584646  : public Il2CppObject
{
public:

public:
};

struct CaseInsensitiveAscii_t4138584646_StaticFields
{
public:
	// System.Net.CaseInsensitiveAscii System.Net.CaseInsensitiveAscii::StaticInstance
	CaseInsensitiveAscii_t4138584646 * ___StaticInstance_0;
	// System.Byte[] System.Net.CaseInsensitiveAscii::AsciiToLower
	ByteU5BU5D_t3397334013* ___AsciiToLower_1;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_t4138584646_StaticFields, ___StaticInstance_0)); }
	inline CaseInsensitiveAscii_t4138584646 * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline CaseInsensitiveAscii_t4138584646 ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(CaseInsensitiveAscii_t4138584646 * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___StaticInstance_0, value);
	}

	inline static int32_t get_offset_of_AsciiToLower_1() { return static_cast<int32_t>(offsetof(CaseInsensitiveAscii_t4138584646_StaticFields, ___AsciiToLower_1)); }
	inline ByteU5BU5D_t3397334013* get_AsciiToLower_1() const { return ___AsciiToLower_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_AsciiToLower_1() { return &___AsciiToLower_1; }
	inline void set_AsciiToLower_1(ByteU5BU5D_t3397334013* value)
	{
		___AsciiToLower_1 = value;
		Il2CppCodeGenWriteBarrier(&___AsciiToLower_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
