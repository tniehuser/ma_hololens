﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[]
struct VirtualAttributeU5BU5D_t586434538;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderNavigator
struct  XmlNodeReaderNavigator_t4234558191  : public Il2CppObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::curNode
	XmlNode_t616554813 * ___curNode_0;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::elemNode
	XmlNode_t616554813 * ___elemNode_1;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::logNode
	XmlNode_t616554813 * ___logNode_2;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::attrIndex
	int32_t ___attrIndex_3;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::logAttrIndex
	int32_t ___logAttrIndex_4;
	// System.Xml.XmlNameTable System.Xml.XmlNodeReaderNavigator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_5;
	// System.Xml.XmlDocument System.Xml.XmlNodeReaderNavigator::doc
	XmlDocument_t3649534162 * ___doc_6;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nAttrInd
	int32_t ___nAttrInd_7;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nDeclarationAttrCount
	int32_t ___nDeclarationAttrCount_8;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nDocTypeAttrCount
	int32_t ___nDocTypeAttrCount_9;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nLogLevel
	int32_t ___nLogLevel_10;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nLogAttrInd
	int32_t ___nLogAttrInd_11;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bLogOnAttrVal
	bool ___bLogOnAttrVal_12;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bCreatedOnAttribute
	bool ___bCreatedOnAttribute_13;
	// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[] System.Xml.XmlNodeReaderNavigator::decNodeAttributes
	VirtualAttributeU5BU5D_t586434538* ___decNodeAttributes_14;
	// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[] System.Xml.XmlNodeReaderNavigator::docTypeNodeAttributes
	VirtualAttributeU5BU5D_t586434538* ___docTypeNodeAttributes_15;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bOnAttrVal
	bool ___bOnAttrVal_16;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___curNode_0)); }
	inline XmlNode_t616554813 * get_curNode_0() const { return ___curNode_0; }
	inline XmlNode_t616554813 ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(XmlNode_t616554813 * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___curNode_0, value);
	}

	inline static int32_t get_offset_of_elemNode_1() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___elemNode_1)); }
	inline XmlNode_t616554813 * get_elemNode_1() const { return ___elemNode_1; }
	inline XmlNode_t616554813 ** get_address_of_elemNode_1() { return &___elemNode_1; }
	inline void set_elemNode_1(XmlNode_t616554813 * value)
	{
		___elemNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___elemNode_1, value);
	}

	inline static int32_t get_offset_of_logNode_2() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___logNode_2)); }
	inline XmlNode_t616554813 * get_logNode_2() const { return ___logNode_2; }
	inline XmlNode_t616554813 ** get_address_of_logNode_2() { return &___logNode_2; }
	inline void set_logNode_2(XmlNode_t616554813 * value)
	{
		___logNode_2 = value;
		Il2CppCodeGenWriteBarrier(&___logNode_2, value);
	}

	inline static int32_t get_offset_of_attrIndex_3() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___attrIndex_3)); }
	inline int32_t get_attrIndex_3() const { return ___attrIndex_3; }
	inline int32_t* get_address_of_attrIndex_3() { return &___attrIndex_3; }
	inline void set_attrIndex_3(int32_t value)
	{
		___attrIndex_3 = value;
	}

	inline static int32_t get_offset_of_logAttrIndex_4() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___logAttrIndex_4)); }
	inline int32_t get_logAttrIndex_4() const { return ___logAttrIndex_4; }
	inline int32_t* get_address_of_logAttrIndex_4() { return &___logAttrIndex_4; }
	inline void set_logAttrIndex_4(int32_t value)
	{
		___logAttrIndex_4 = value;
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nameTable_5)); }
	inline XmlNameTable_t1345805268 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t1345805268 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_5, value);
	}

	inline static int32_t get_offset_of_doc_6() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___doc_6)); }
	inline XmlDocument_t3649534162 * get_doc_6() const { return ___doc_6; }
	inline XmlDocument_t3649534162 ** get_address_of_doc_6() { return &___doc_6; }
	inline void set_doc_6(XmlDocument_t3649534162 * value)
	{
		___doc_6 = value;
		Il2CppCodeGenWriteBarrier(&___doc_6, value);
	}

	inline static int32_t get_offset_of_nAttrInd_7() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nAttrInd_7)); }
	inline int32_t get_nAttrInd_7() const { return ___nAttrInd_7; }
	inline int32_t* get_address_of_nAttrInd_7() { return &___nAttrInd_7; }
	inline void set_nAttrInd_7(int32_t value)
	{
		___nAttrInd_7 = value;
	}

	inline static int32_t get_offset_of_nDeclarationAttrCount_8() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nDeclarationAttrCount_8)); }
	inline int32_t get_nDeclarationAttrCount_8() const { return ___nDeclarationAttrCount_8; }
	inline int32_t* get_address_of_nDeclarationAttrCount_8() { return &___nDeclarationAttrCount_8; }
	inline void set_nDeclarationAttrCount_8(int32_t value)
	{
		___nDeclarationAttrCount_8 = value;
	}

	inline static int32_t get_offset_of_nDocTypeAttrCount_9() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nDocTypeAttrCount_9)); }
	inline int32_t get_nDocTypeAttrCount_9() const { return ___nDocTypeAttrCount_9; }
	inline int32_t* get_address_of_nDocTypeAttrCount_9() { return &___nDocTypeAttrCount_9; }
	inline void set_nDocTypeAttrCount_9(int32_t value)
	{
		___nDocTypeAttrCount_9 = value;
	}

	inline static int32_t get_offset_of_nLogLevel_10() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nLogLevel_10)); }
	inline int32_t get_nLogLevel_10() const { return ___nLogLevel_10; }
	inline int32_t* get_address_of_nLogLevel_10() { return &___nLogLevel_10; }
	inline void set_nLogLevel_10(int32_t value)
	{
		___nLogLevel_10 = value;
	}

	inline static int32_t get_offset_of_nLogAttrInd_11() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___nLogAttrInd_11)); }
	inline int32_t get_nLogAttrInd_11() const { return ___nLogAttrInd_11; }
	inline int32_t* get_address_of_nLogAttrInd_11() { return &___nLogAttrInd_11; }
	inline void set_nLogAttrInd_11(int32_t value)
	{
		___nLogAttrInd_11 = value;
	}

	inline static int32_t get_offset_of_bLogOnAttrVal_12() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___bLogOnAttrVal_12)); }
	inline bool get_bLogOnAttrVal_12() const { return ___bLogOnAttrVal_12; }
	inline bool* get_address_of_bLogOnAttrVal_12() { return &___bLogOnAttrVal_12; }
	inline void set_bLogOnAttrVal_12(bool value)
	{
		___bLogOnAttrVal_12 = value;
	}

	inline static int32_t get_offset_of_bCreatedOnAttribute_13() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___bCreatedOnAttribute_13)); }
	inline bool get_bCreatedOnAttribute_13() const { return ___bCreatedOnAttribute_13; }
	inline bool* get_address_of_bCreatedOnAttribute_13() { return &___bCreatedOnAttribute_13; }
	inline void set_bCreatedOnAttribute_13(bool value)
	{
		___bCreatedOnAttribute_13 = value;
	}

	inline static int32_t get_offset_of_decNodeAttributes_14() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___decNodeAttributes_14)); }
	inline VirtualAttributeU5BU5D_t586434538* get_decNodeAttributes_14() const { return ___decNodeAttributes_14; }
	inline VirtualAttributeU5BU5D_t586434538** get_address_of_decNodeAttributes_14() { return &___decNodeAttributes_14; }
	inline void set_decNodeAttributes_14(VirtualAttributeU5BU5D_t586434538* value)
	{
		___decNodeAttributes_14 = value;
		Il2CppCodeGenWriteBarrier(&___decNodeAttributes_14, value);
	}

	inline static int32_t get_offset_of_docTypeNodeAttributes_15() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___docTypeNodeAttributes_15)); }
	inline VirtualAttributeU5BU5D_t586434538* get_docTypeNodeAttributes_15() const { return ___docTypeNodeAttributes_15; }
	inline VirtualAttributeU5BU5D_t586434538** get_address_of_docTypeNodeAttributes_15() { return &___docTypeNodeAttributes_15; }
	inline void set_docTypeNodeAttributes_15(VirtualAttributeU5BU5D_t586434538* value)
	{
		___docTypeNodeAttributes_15 = value;
		Il2CppCodeGenWriteBarrier(&___docTypeNodeAttributes_15, value);
	}

	inline static int32_t get_offset_of_bOnAttrVal_16() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t4234558191, ___bOnAttrVal_16)); }
	inline bool get_bOnAttrVal_16() const { return ___bOnAttrVal_16; }
	inline bool* get_address_of_bOnAttrVal_16() { return &___bOnAttrVal_16; }
	inline void set_bOnAttrVal_16(bool value)
	{
		___bOnAttrVal_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
