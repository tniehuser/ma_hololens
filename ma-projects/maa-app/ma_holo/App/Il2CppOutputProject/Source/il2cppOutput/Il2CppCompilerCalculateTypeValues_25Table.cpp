﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_ActiveAxis439376929.h"
#include "System_Xml_System_Xml_Schema_DoubleLinkAxis3164907012.h"
#include "System_Xml_System_Xml_Schema_ForwardAxis3876904870.h"
#include "System_Xml_System_Xml_Schema_Asttree3451058494.h"
#include "System_Xml_System_Xml_Schema_AutoValidator190363951.h"
#include "System_Xml_System_Xml_Schema_BaseProcessor2373158431.h"
#include "System_Xml_System_Xml_Schema_BaseValidator3557140249.h"
#include "System_Xml_System_Xml_Schema_BitSet1062448123.h"
#include "System_Xml_System_Xml_Schema_CompiledIdentityConstr964629540.h"
#include "System_Xml_System_Xml_Schema_CompiledIdentityConst3811561833.h"
#include "System_Xml_System_Xml_Schema_ConstraintStruct2462842120.h"
#include "System_Xml_System_Xml_Schema_LocatedActiveAxis90453917.h"
#include "System_Xml_System_Xml_Schema_SelectorActiveAxis789423304.h"
#include "System_Xml_System_Xml_Schema_KSStruct704598349.h"
#include "System_Xml_System_Xml_Schema_TypedObject1797374135.h"
#include "System_Xml_System_Xml_Schema_TypedObject_DecimalStr715828147.h"
#include "System_Xml_System_Xml_Schema_KeySequence746093258.h"
#include "System_Xml_System_Xml_Schema_UpaException656169215.h"
#include "System_Xml_System_Xml_Schema_SymbolsDictionary1753655453.h"
#include "System_Xml_System_Xml_Schema_Position1796812729.h"
#include "System_Xml_System_Xml_Schema_Positions3593914952.h"
#include "System_Xml_System_Xml_Schema_SyntaxTreeNode2397191729.h"
#include "System_Xml_System_Xml_Schema_LeafNode3748718316.h"
#include "System_Xml_System_Xml_Schema_NamespaceListNode2509262495.h"
#include "System_Xml_System_Xml_Schema_InteriorNode2716368958.h"
#include "System_Xml_System_Xml_Schema_SequenceNode4039907291.h"
#include "System_Xml_System_Xml_Schema_SequenceNode_Sequence3853454650.h"
#include "System_Xml_System_Xml_Schema_ChoiceNode3123692209.h"
#include "System_Xml_System_Xml_Schema_PlusNode3494526928.h"
#include "System_Xml_System_Xml_Schema_QmarkNode902109702.h"
#include "System_Xml_System_Xml_Schema_StarNode2416964522.h"
#include "System_Xml_System_Xml_Schema_LeafRangeNode2572019409.h"
#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"
#include "System_Xml_System_Xml_Schema_ParticleContentValida1341047977.h"
#include "System_Xml_System_Xml_Schema_DfaContentValidator429624170.h"
#include "System_Xml_System_Xml_Schema_NfaContentValidator1842871216.h"
#include "System_Xml_System_Xml_Schema_RangePositionInfo2780802922.h"
#include "System_Xml_System_Xml_Schema_RangeContentValidator857885766.h"
#include "System_Xml_System_Xml_Schema_AllElementsContentVal2095068475.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatypeVari2237606318.h"
#include "System_Xml_System_Xml_Schema_XsdSimpleValue478440528.h"
#include "System_Xml_System_Xml_Schema_RestrictionFlags2588355947.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpace3746245107.h"
#include "System_Xml_System_Xml_Schema_RestrictionFacets4012658256.h"
#include "System_Xml_System_Xml_Schema_DatatypeImplementatio1152094268.h"
#include "System_Xml_System_Xml_Schema_DatatypeImplementatio2661667341.h"
#include "System_Xml_System_Xml_Schema_Datatype_List1892289229.h"
#include "System_Xml_System_Xml_Schema_Datatype_union2515141346.h"
#include "System_Xml_System_Xml_Schema_Datatype_anySimpleTyp4012795865.h"
#include "System_Xml_System_Xml_Schema_Datatype_anyAtomicTyp4212901794.h"
#include "System_Xml_System_Xml_Schema_Datatype_untypedAtomi3332440493.h"
#include "System_Xml_System_Xml_Schema_Datatype_string995037180.h"
#include "System_Xml_System_Xml_Schema_Datatype_boolean293982753.h"
#include "System_Xml_System_Xml_Schema_Datatype_float3149441939.h"
#include "System_Xml_System_Xml_Schema_Datatype_double1050796240.h"
#include "System_Xml_System_Xml_Schema_Datatype_decimal2973594954.h"
#include "System_Xml_System_Xml_Schema_Datatype_duration1871787273.h"
#include "System_Xml_System_Xml_Schema_Datatype_yearMonthDur1235863080.h"
#include "System_Xml_System_Xml_Schema_Datatype_dayTimeDurat2638197894.h"
#include "System_Xml_System_Xml_Schema_Datatype_dateTimeBase2449194189.h"
#include "System_Xml_System_Xml_Schema_Datatype_dateTimeNoTi3887107098.h"
#include "System_Xml_System_Xml_Schema_Datatype_dateTimeTime3703324323.h"
#include "System_Xml_System_Xml_Schema_Datatype_dateTime1101103220.h"
#include "System_Xml_System_Xml_Schema_Datatype_timeNoTimeZo1747796622.h"
#include "System_Xml_System_Xml_Schema_Datatype_timeTimeZone3702809551.h"
#include "System_Xml_System_Xml_Schema_Datatype_time305227304.h"
#include "System_Xml_System_Xml_Schema_Datatype_date903634781.h"
#include "System_Xml_System_Xml_Schema_Datatype_yearMonth1775492394.h"
#include "System_Xml_System_Xml_Schema_Datatype_year527201590.h"
#include "System_Xml_System_Xml_Schema_Datatype_monthDay1515801313.h"
#include "System_Xml_System_Xml_Schema_Datatype_day3625353511.h"
#include "System_Xml_System_Xml_Schema_Datatype_month2424718095.h"
#include "System_Xml_System_Xml_Schema_Datatype_hexBinary2599154205.h"
#include "System_Xml_System_Xml_Schema_Datatype_base64Binary3902009307.h"
#include "System_Xml_System_Xml_Schema_Datatype_anyURI2417434093.h"
#include "System_Xml_System_Xml_Schema_Datatype_QName2180543649.h"
#include "System_Xml_System_Xml_Schema_Datatype_normalizedSt2314411649.h"
#include "System_Xml_System_Xml_Schema_Datatype_normalizedSt2394903178.h"
#include "System_Xml_System_Xml_Schema_Datatype_token464702686.h"
#include "System_Xml_System_Xml_Schema_Datatype_tokenV1Compat64797735.h"
#include "System_Xml_System_Xml_Schema_Datatype_language2173537947.h"
#include "System_Xml_System_Xml_Schema_Datatype_NMTOKEN2677332311.h"
#include "System_Xml_System_Xml_Schema_Datatype_Name2060302642.h"
#include "System_Xml_System_Xml_Schema_Datatype_NCName1627675837.h"
#include "System_Xml_System_Xml_Schema_Datatype_ID3661482256.h"
#include "System_Xml_System_Xml_Schema_Datatype_IDREF3125569863.h"
#include "System_Xml_System_Xml_Schema_Datatype_ENTITY863271950.h"
#include "System_Xml_System_Xml_Schema_Datatype_NOTATION3412197483.h"
#include "System_Xml_System_Xml_Schema_Datatype_integer404053727.h"
#include "System_Xml_System_Xml_Schema_Datatype_nonPositiveIn863439515.h"
#include "System_Xml_System_Xml_Schema_Datatype_negativeInte1070964020.h"
#include "System_Xml_System_Xml_Schema_Datatype_long513905339.h"
#include "System_Xml_System_Xml_Schema_Datatype_int2281243990.h"
#include "System_Xml_System_Xml_Schema_Datatype_short3426858539.h"
#include "System_Xml_System_Xml_Schema_Datatype_byte702122475.h"
#include "System_Xml_System_Xml_Schema_Datatype_nonNegativeI1851861419.h"
#include "System_Xml_System_Xml_Schema_Datatype_unsignedLong3462046402.h"
#include "System_Xml_System_Xml_Schema_Datatype_unsignedInt4210266973.h"
#include "System_Xml_System_Xml_Schema_Datatype_unsignedShor2975932210.h"
#include "System_Xml_System_Xml_Schema_Datatype_unsignedByte322972434.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (ActiveAxis_t439376929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	ActiveAxis_t439376929::get_offset_of_currentDepth_0(),
	ActiveAxis_t439376929::get_offset_of_isActive_1(),
	ActiveAxis_t439376929::get_offset_of_axisTree_2(),
	ActiveAxis_t439376929::get_offset_of_axisStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (DoubleLinkAxis_t3164907012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	DoubleLinkAxis_t3164907012::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (ForwardAxis_t3876904870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[5] = 
{
	ForwardAxis_t3876904870::get_offset_of_topNode_0(),
	ForwardAxis_t3876904870::get_offset_of_rootNode_1(),
	ForwardAxis_t3876904870::get_offset_of_isAttribute_2(),
	ForwardAxis_t3876904870::get_offset_of_isDss_3(),
	ForwardAxis_t3876904870::get_offset_of_isSelfAxis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Asttree_t3451058494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	Asttree_t3451058494::get_offset_of_fAxisArray_0(),
	Asttree_t3451058494::get_offset_of_xpathexpr_1(),
	Asttree_t3451058494::get_offset_of_isField_2(),
	Asttree_t3451058494::get_offset_of_nsmgr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (AutoValidator_t190363951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (BaseProcessor_t2373158431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	BaseProcessor_t2373158431::get_offset_of_nameTable_0(),
	BaseProcessor_t2373158431::get_offset_of_schemaNames_1(),
	BaseProcessor_t2373158431::get_offset_of_eventHandler_2(),
	BaseProcessor_t2373158431::get_offset_of_compilationSettings_3(),
	BaseProcessor_t2373158431::get_offset_of_errorCount_4(),
	BaseProcessor_t2373158431::get_offset_of_NsXml_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (BaseValidator_t3557140249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[15] = 
{
	BaseValidator_t3557140249::get_offset_of_schemaCollection_0(),
	BaseValidator_t3557140249::get_offset_of_eventHandling_1(),
	BaseValidator_t3557140249::get_offset_of_nameTable_2(),
	BaseValidator_t3557140249::get_offset_of_schemaNames_3(),
	BaseValidator_t3557140249::get_offset_of_positionInfo_4(),
	BaseValidator_t3557140249::get_offset_of_xmlResolver_5(),
	BaseValidator_t3557140249::get_offset_of_baseUri_6(),
	BaseValidator_t3557140249::get_offset_of_schemaInfo_7(),
	BaseValidator_t3557140249::get_offset_of_reader_8(),
	BaseValidator_t3557140249::get_offset_of_elementName_9(),
	BaseValidator_t3557140249::get_offset_of_context_10(),
	BaseValidator_t3557140249::get_offset_of_textValue_11(),
	BaseValidator_t3557140249::get_offset_of_textString_12(),
	BaseValidator_t3557140249::get_offset_of_hasSibling_13(),
	BaseValidator_t3557140249::get_offset_of_checkDatatype_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (BitSet_t1062448123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[2] = 
{
	BitSet_t1062448123::get_offset_of_count_0(),
	BitSet_t1062448123::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (CompiledIdentityConstraint_t964629540), -1, sizeof(CompiledIdentityConstraint_t964629540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2508[6] = 
{
	CompiledIdentityConstraint_t964629540::get_offset_of_name_0(),
	CompiledIdentityConstraint_t964629540::get_offset_of_role_1(),
	CompiledIdentityConstraint_t964629540::get_offset_of_selector_2(),
	CompiledIdentityConstraint_t964629540::get_offset_of_fields_3(),
	CompiledIdentityConstraint_t964629540::get_offset_of_refer_4(),
	CompiledIdentityConstraint_t964629540_StaticFields::get_offset_of_Empty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (ConstraintRole_t3811561833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[4] = 
{
	ConstraintRole_t3811561833::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ConstraintStruct_t2462842120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[6] = 
{
	ConstraintStruct_t2462842120::get_offset_of_constraint_0(),
	ConstraintStruct_t2462842120::get_offset_of_axisSelector_1(),
	ConstraintStruct_t2462842120::get_offset_of_axisFields_2(),
	ConstraintStruct_t2462842120::get_offset_of_qualifiedTable_3(),
	ConstraintStruct_t2462842120::get_offset_of_keyrefTable_4(),
	ConstraintStruct_t2462842120::get_offset_of_tableDim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (LocatedActiveAxis_t90453917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[3] = 
{
	LocatedActiveAxis_t90453917::get_offset_of_column_4(),
	LocatedActiveAxis_t90453917::get_offset_of_isMatched_5(),
	LocatedActiveAxis_t90453917::get_offset_of_Ks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (SelectorActiveAxis_t789423304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[3] = 
{
	SelectorActiveAxis_t789423304::get_offset_of_cs_4(),
	SelectorActiveAxis_t789423304::get_offset_of_KSs_5(),
	SelectorActiveAxis_t789423304::get_offset_of_KSpointer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (KSStruct_t704598349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[3] = 
{
	KSStruct_t704598349::get_offset_of_depth_0(),
	KSStruct_t704598349::get_offset_of_ks_1(),
	KSStruct_t704598349::get_offset_of_fields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (TypedObject_t1797374135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[6] = 
{
	TypedObject_t1797374135::get_offset_of_dstruct_0(),
	TypedObject_t1797374135::get_offset_of_ovalue_1(),
	TypedObject_t1797374135::get_offset_of_svalue_2(),
	TypedObject_t1797374135::get_offset_of_xsdtype_3(),
	TypedObject_t1797374135::get_offset_of_dim_4(),
	TypedObject_t1797374135::get_offset_of_isList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (DecimalStruct_t715828147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[2] = 
{
	DecimalStruct_t715828147::get_offset_of_isDecimal_0(),
	DecimalStruct_t715828147::get_offset_of_dvalue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (KeySequence_t746093258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[5] = 
{
	KeySequence_t746093258::get_offset_of_ks_0(),
	KeySequence_t746093258::get_offset_of_dim_1(),
	KeySequence_t746093258::get_offset_of_hashcode_2(),
	KeySequence_t746093258::get_offset_of_posline_3(),
	KeySequence_t746093258::get_offset_of_poscol_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (UpaException_t656169215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	UpaException_t656169215::get_offset_of_particle1_16(),
	UpaException_t656169215::get_offset_of_particle2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (SymbolsDictionary_t1753655453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[6] = 
{
	SymbolsDictionary_t1753655453::get_offset_of_last_0(),
	SymbolsDictionary_t1753655453::get_offset_of_names_1(),
	SymbolsDictionary_t1753655453::get_offset_of_wildcards_2(),
	SymbolsDictionary_t1753655453::get_offset_of_particles_3(),
	SymbolsDictionary_t1753655453::get_offset_of_particleLast_4(),
	SymbolsDictionary_t1753655453::get_offset_of_isUpaEnforced_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Position_t1796812729)+ sizeof (Il2CppObject), sizeof(Position_t1796812729_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	Position_t1796812729::get_offset_of_symbol_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Position_t1796812729::get_offset_of_particle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (Positions_t3593914952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[1] = 
{
	Positions_t3593914952::get_offset_of_positions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (SyntaxTreeNode_t2397191729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (LeafNode_t3748718316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	LeafNode_t3748718316::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (NamespaceListNode_t2509262495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[2] = 
{
	NamespaceListNode_t2509262495::get_offset_of_namespaceList_0(),
	NamespaceListNode_t2509262495::get_offset_of_particle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (InteriorNode_t2716368958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	InteriorNode_t2716368958::get_offset_of_leftChild_0(),
	InteriorNode_t2716368958::get_offset_of_rightChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (SequenceNode_t4039907291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (SequenceConstructPosContext_t3853454650)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[5] = 
{
	SequenceConstructPosContext_t3853454650::get_offset_of_this__0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_firstpos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_lastpos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_lastposLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_firstposRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (ChoiceNode_t3123692209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (PlusNode_t3494526928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (QmarkNode_t902109702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (StarNode_t2416964522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (LeafRangeNode_t2572019409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	LeafRangeNode_t2572019409::get_offset_of_min_1(),
	LeafRangeNode_t2572019409::get_offset_of_max_2(),
	LeafRangeNode_t2572019409::get_offset_of_nextIteration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (ContentValidator_t2510151843), -1, sizeof(ContentValidator_t2510151843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[7] = 
{
	ContentValidator_t2510151843::get_offset_of_contentType_0(),
	ContentValidator_t2510151843::get_offset_of_isOpen_1(),
	ContentValidator_t2510151843::get_offset_of_isEmptiable_2(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Empty_3(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_TextOnly_4(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Mixed_5(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Any_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (ParticleContentValidator_t1341047977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[7] = 
{
	ParticleContentValidator_t1341047977::get_offset_of_symbols_7(),
	ParticleContentValidator_t1341047977::get_offset_of_positions_8(),
	ParticleContentValidator_t1341047977::get_offset_of_stack_9(),
	ParticleContentValidator_t1341047977::get_offset_of_contentNode_10(),
	ParticleContentValidator_t1341047977::get_offset_of_isPartial_11(),
	ParticleContentValidator_t1341047977::get_offset_of_minMaxNodesCount_12(),
	ParticleContentValidator_t1341047977::get_offset_of_enableUpaCheck_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (DfaContentValidator_t429624170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[2] = 
{
	DfaContentValidator_t429624170::get_offset_of_transitionTable_7(),
	DfaContentValidator_t429624170::get_offset_of_symbols_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (NfaContentValidator_t1842871216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[5] = 
{
	NfaContentValidator_t1842871216::get_offset_of_firstpos_7(),
	NfaContentValidator_t1842871216::get_offset_of_followpos_8(),
	NfaContentValidator_t1842871216::get_offset_of_symbols_9(),
	NfaContentValidator_t1842871216::get_offset_of_positions_10(),
	NfaContentValidator_t1842871216::get_offset_of_endMarkerPos_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (RangePositionInfo_t2780802922)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	RangePositionInfo_t2780802922::get_offset_of_curpos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RangePositionInfo_t2780802922::get_offset_of_rangeCounters_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (RangeContentValidator_t857885766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[7] = 
{
	RangeContentValidator_t857885766::get_offset_of_firstpos_7(),
	RangeContentValidator_t857885766::get_offset_of_followpos_8(),
	RangeContentValidator_t857885766::get_offset_of_positionsWithRangeTerminals_9(),
	RangeContentValidator_t857885766::get_offset_of_symbols_10(),
	RangeContentValidator_t857885766::get_offset_of_positions_11(),
	RangeContentValidator_t857885766::get_offset_of_minMaxNodesCount_12(),
	RangeContentValidator_t857885766::get_offset_of_endMarkerPos_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (AllElementsContentValidator_t2095068475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[4] = 
{
	AllElementsContentValidator_t2095068475::get_offset_of_elements_7(),
	AllElementsContentValidator_t2095068475::get_offset_of_particles_8(),
	AllElementsContentValidator_t2095068475::get_offset_of_isRequired_9(),
	AllElementsContentValidator_t2095068475::get_offset_of_countRequired_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (XmlSchemaDatatypeVariety_t2237606318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	XmlSchemaDatatypeVariety_t2237606318::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (XsdSimpleValue_t478440528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	XsdSimpleValue_t478440528::get_offset_of_xmlType_0(),
	XsdSimpleValue_t478440528::get_offset_of_typedValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (RestrictionFlags_t2588355947)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2541[13] = 
{
	RestrictionFlags_t2588355947::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (XmlSchemaWhiteSpace_t3746245107)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2542[4] = 
{
	XmlSchemaWhiteSpace_t3746245107::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (RestrictionFacets_t4012658256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[14] = 
{
	RestrictionFacets_t4012658256::get_offset_of_Length_0(),
	RestrictionFacets_t4012658256::get_offset_of_MinLength_1(),
	RestrictionFacets_t4012658256::get_offset_of_MaxLength_2(),
	RestrictionFacets_t4012658256::get_offset_of_Patterns_3(),
	RestrictionFacets_t4012658256::get_offset_of_Enumeration_4(),
	RestrictionFacets_t4012658256::get_offset_of_WhiteSpace_5(),
	RestrictionFacets_t4012658256::get_offset_of_MaxInclusive_6(),
	RestrictionFacets_t4012658256::get_offset_of_MaxExclusive_7(),
	RestrictionFacets_t4012658256::get_offset_of_MinInclusive_8(),
	RestrictionFacets_t4012658256::get_offset_of_MinExclusive_9(),
	RestrictionFacets_t4012658256::get_offset_of_TotalDigits_10(),
	RestrictionFacets_t4012658256::get_offset_of_FractionDigits_11(),
	RestrictionFacets_t4012658256::get_offset_of_Flags_12(),
	RestrictionFacets_t4012658256::get_offset_of_FixedFlags_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (DatatypeImplementation_t1152094268), -1, sizeof(DatatypeImplementation_t1152094268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[91] = 
{
	DatatypeImplementation_t1152094268::get_offset_of_variety_0(),
	DatatypeImplementation_t1152094268::get_offset_of_restriction_1(),
	DatatypeImplementation_t1152094268::get_offset_of_baseType_2(),
	DatatypeImplementation_t1152094268::get_offset_of_valueConverter_3(),
	DatatypeImplementation_t1152094268::get_offset_of_parentSchemaType_4(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_builtinTypes_5(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_enumToTypeCode_6(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_anySimpleType_7(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_anyAtomicType_8(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_untypedAtomicType_9(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_yearMonthDurationType_10(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_dayTimeDurationType_11(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_normalizedStringTypeV1Compat_12(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_tokenTypeV1Compat_13(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_QnAnySimpleType_14(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_QnAnyType_15(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_stringFacetsChecker_16(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_miscFacetsChecker_17(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_numeric2FacetsChecker_18(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_binaryFacetsChecker_19(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_dateTimeFacetsChecker_20(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_durationFacetsChecker_21(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_listFacetsChecker_22(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_qnameFacetsChecker_23(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_unionFacetsChecker_24(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anySimpleType_25(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anyURI_26(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_base64Binary_27(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_boolean_28(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_byte_29(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_char_30(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_date_31(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTime_32(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTimeNoTz_33(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTimeTz_34(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_day_35(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_decimal_36(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_double_37(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_doubleXdr_38(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_duration_39(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENTITY_40(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENTITIES_41(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENUMERATION_42(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_fixed_43(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_float_44(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_floatXdr_45(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_hexBinary_46(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ID_47(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_IDREF_48(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_IDREFS_49(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_int_50(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_integer_51(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_language_52(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_long_53(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_month_54(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_monthDay_55(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_Name_56(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NCName_57(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_negativeInteger_58(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NMTOKEN_59(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NMTOKENS_60(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_nonNegativeInteger_61(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_nonPositiveInteger_62(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_normalizedString_63(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NOTATION_64(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_positiveInteger_65(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_QName_66(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_QNameXdr_67(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_short_68(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_string_69(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_time_70(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_timeNoTz_71(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_timeTz_72(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_token_73(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedByte_74(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedInt_75(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedLong_76(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedShort_77(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_uuid_78(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_year_79(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_yearMonth_80(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_normalizedStringV1Compat_81(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenV1Compat_82(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anyAtomicType_83(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dayTimeDuration_84(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_untypedAtomicType_85(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_yearMonthDuration_86(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenizedTypes_87(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenizedTypesXsd_88(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_XdrTypes_89(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_XsdTypes_90(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (SchemaDatatypeMap_t2661667341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[3] = 
{
	SchemaDatatypeMap_t2661667341::get_offset_of_name_0(),
	SchemaDatatypeMap_t2661667341::get_offset_of_type_1(),
	SchemaDatatypeMap_t2661667341::get_offset_of_parentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (Datatype_List_t1892289229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[2] = 
{
	Datatype_List_t1892289229::get_offset_of_itemType_93(),
	Datatype_List_t1892289229::get_offset_of_minListSize_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (Datatype_union_t2515141346), -1, sizeof(Datatype_union_t2515141346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2547[3] = 
{
	Datatype_union_t2515141346_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_union_t2515141346_StaticFields::get_offset_of_listValueType_94(),
	Datatype_union_t2515141346::get_offset_of_types_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (Datatype_anySimpleType_t4012795865), -1, sizeof(Datatype_anySimpleType_t4012795865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2548[2] = 
{
	Datatype_anySimpleType_t4012795865_StaticFields::get_offset_of_atomicValueType_91(),
	Datatype_anySimpleType_t4012795865_StaticFields::get_offset_of_listValueType_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (Datatype_anyAtomicType_t4212901794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (Datatype_untypedAtomicType_t3332440493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (Datatype_string_t995037180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (Datatype_boolean_t293982753), -1, sizeof(Datatype_boolean_t293982753_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	Datatype_boolean_t293982753_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_boolean_t293982753_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Datatype_float_t3149441939), -1, sizeof(Datatype_float_t3149441939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2553[2] = 
{
	Datatype_float_t3149441939_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_float_t3149441939_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (Datatype_double_t1050796240), -1, sizeof(Datatype_double_t1050796240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2554[2] = 
{
	Datatype_double_t1050796240_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_double_t1050796240_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (Datatype_decimal_t2973594954), -1, sizeof(Datatype_decimal_t2973594954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[3] = 
{
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_listValueType_94(),
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_numeric10FacetsChecker_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (Datatype_duration_t1871787273), -1, sizeof(Datatype_duration_t1871787273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[2] = 
{
	Datatype_duration_t1871787273_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_duration_t1871787273_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (Datatype_yearMonthDuration_t1235863080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (Datatype_dayTimeDuration_t2638197894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (Datatype_dateTimeBase_t2449194189), -1, sizeof(Datatype_dateTimeBase_t2449194189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[3] = 
{
	Datatype_dateTimeBase_t2449194189_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_dateTimeBase_t2449194189_StaticFields::get_offset_of_listValueType_94(),
	Datatype_dateTimeBase_t2449194189::get_offset_of_dateTimeFlags_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (Datatype_dateTimeNoTimeZone_t3887107098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (Datatype_dateTimeTimeZone_t3703324323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (Datatype_dateTime_t1101103220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (Datatype_timeNoTimeZone_t1747796622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Datatype_timeTimeZone_t3702809551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (Datatype_time_t305227304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Datatype_date_t903634781), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (Datatype_yearMonth_t1775492394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (Datatype_year_t527201590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Datatype_monthDay_t1515801313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (Datatype_day_t3625353511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Datatype_month_t2424718095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (Datatype_hexBinary_t2599154205), -1, sizeof(Datatype_hexBinary_t2599154205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[2] = 
{
	Datatype_hexBinary_t2599154205_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_hexBinary_t2599154205_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (Datatype_base64Binary_t3902009307), -1, sizeof(Datatype_base64Binary_t3902009307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2573[2] = 
{
	Datatype_base64Binary_t3902009307_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_base64Binary_t3902009307_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (Datatype_anyURI_t2417434093), -1, sizeof(Datatype_anyURI_t2417434093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2574[2] = 
{
	Datatype_anyURI_t2417434093_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_anyURI_t2417434093_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (Datatype_QName_t2180543649), -1, sizeof(Datatype_QName_t2180543649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2575[2] = 
{
	Datatype_QName_t2180543649_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QName_t2180543649_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Datatype_normalizedString_t2314411649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Datatype_normalizedStringV1Compat_t2394903178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (Datatype_token_t464702686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (Datatype_tokenV1Compat_t64797735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (Datatype_language_t2173537947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (Datatype_NMTOKEN_t2677332311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Datatype_Name_t2060302642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Datatype_NCName_t1627675837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (Datatype_ID_t3661482256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (Datatype_IDREF_t3125569863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (Datatype_ENTITY_t863271950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Datatype_NOTATION_t3412197483), -1, sizeof(Datatype_NOTATION_t3412197483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2587[2] = 
{
	Datatype_NOTATION_t3412197483_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_NOTATION_t3412197483_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (Datatype_integer_t404053727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Datatype_nonPositiveInteger_t863439515), -1, sizeof(Datatype_nonPositiveInteger_t863439515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[1] = 
{
	Datatype_nonPositiveInteger_t863439515_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (Datatype_negativeInteger_t1070964020), -1, sizeof(Datatype_negativeInteger_t1070964020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	Datatype_negativeInteger_t1070964020_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Datatype_long_t513905339), -1, sizeof(Datatype_long_t513905339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	Datatype_long_t513905339_StaticFields::get_offset_of_atomicValueType_96(),
	Datatype_long_t513905339_StaticFields::get_offset_of_listValueType_97(),
	Datatype_long_t513905339_StaticFields::get_offset_of_numeric10FacetsChecker_98(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (Datatype_int_t2281243990), -1, sizeof(Datatype_int_t2281243990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	Datatype_int_t2281243990_StaticFields::get_offset_of_atomicValueType_99(),
	Datatype_int_t2281243990_StaticFields::get_offset_of_listValueType_100(),
	Datatype_int_t2281243990_StaticFields::get_offset_of_numeric10FacetsChecker_101(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (Datatype_short_t3426858539), -1, sizeof(Datatype_short_t3426858539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2593[3] = 
{
	Datatype_short_t3426858539_StaticFields::get_offset_of_atomicValueType_102(),
	Datatype_short_t3426858539_StaticFields::get_offset_of_listValueType_103(),
	Datatype_short_t3426858539_StaticFields::get_offset_of_numeric10FacetsChecker_104(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (Datatype_byte_t702122475), -1, sizeof(Datatype_byte_t702122475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[3] = 
{
	Datatype_byte_t702122475_StaticFields::get_offset_of_atomicValueType_105(),
	Datatype_byte_t702122475_StaticFields::get_offset_of_listValueType_106(),
	Datatype_byte_t702122475_StaticFields::get_offset_of_numeric10FacetsChecker_107(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (Datatype_nonNegativeInteger_t1851861419), -1, sizeof(Datatype_nonNegativeInteger_t1851861419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	Datatype_nonNegativeInteger_t1851861419_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (Datatype_unsignedLong_t3462046402), -1, sizeof(Datatype_unsignedLong_t3462046402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[3] = 
{
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_atomicValueType_97(),
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_listValueType_98(),
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_numeric10FacetsChecker_99(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (Datatype_unsignedInt_t4210266973), -1, sizeof(Datatype_unsignedInt_t4210266973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_atomicValueType_100(),
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_listValueType_101(),
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_numeric10FacetsChecker_102(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (Datatype_unsignedShort_t2975932210), -1, sizeof(Datatype_unsignedShort_t2975932210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[3] = 
{
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_atomicValueType_103(),
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_listValueType_104(),
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_numeric10FacetsChecker_105(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Datatype_unsignedByte_t322972434), -1, sizeof(Datatype_unsignedByte_t322972434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_atomicValueType_106(),
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_listValueType_107(),
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_numeric10FacetsChecker_108(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
