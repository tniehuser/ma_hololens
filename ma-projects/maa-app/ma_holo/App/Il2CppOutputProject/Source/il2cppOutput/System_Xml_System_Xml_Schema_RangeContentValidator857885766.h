﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"

// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_t2256991674;
// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_t1753655453;
// System.Xml.Schema.Positions
struct Positions_t3593914952;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangeContentValidator
struct  RangeContentValidator_t857885766  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::firstpos
	BitSet_t1062448123 * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.RangeContentValidator::followpos
	BitSetU5BU5D_t2256991674* ___followpos_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::positionsWithRangeTerminals
	BitSet_t1062448123 * ___positionsWithRangeTerminals_9;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.RangeContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_10;
	// System.Xml.Schema.Positions System.Xml.Schema.RangeContentValidator::positions
	Positions_t3593914952 * ___positions_11;
	// System.Int32 System.Xml.Schema.RangeContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Int32 System.Xml.Schema.RangeContentValidator::endMarkerPos
	int32_t ___endMarkerPos_13;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___firstpos_7)); }
	inline BitSet_t1062448123 * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t1062448123 * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier(&___firstpos_7, value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___followpos_8)); }
	inline BitSetU5BU5D_t2256991674* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_t2256991674** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_t2256991674* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier(&___followpos_8, value);
	}

	inline static int32_t get_offset_of_positionsWithRangeTerminals_9() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___positionsWithRangeTerminals_9)); }
	inline BitSet_t1062448123 * get_positionsWithRangeTerminals_9() const { return ___positionsWithRangeTerminals_9; }
	inline BitSet_t1062448123 ** get_address_of_positionsWithRangeTerminals_9() { return &___positionsWithRangeTerminals_9; }
	inline void set_positionsWithRangeTerminals_9(BitSet_t1062448123 * value)
	{
		___positionsWithRangeTerminals_9 = value;
		Il2CppCodeGenWriteBarrier(&___positionsWithRangeTerminals_9, value);
	}

	inline static int32_t get_offset_of_symbols_10() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___symbols_10)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_10() const { return ___symbols_10; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_10() { return &___symbols_10; }
	inline void set_symbols_10(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_10 = value;
		Il2CppCodeGenWriteBarrier(&___symbols_10, value);
	}

	inline static int32_t get_offset_of_positions_11() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___positions_11)); }
	inline Positions_t3593914952 * get_positions_11() const { return ___positions_11; }
	inline Positions_t3593914952 ** get_address_of_positions_11() { return &___positions_11; }
	inline void set_positions_11(Positions_t3593914952 * value)
	{
		___positions_11 = value;
		Il2CppCodeGenWriteBarrier(&___positions_11, value);
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_endMarkerPos_13() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___endMarkerPos_13)); }
	inline int32_t get_endMarkerPos_13() const { return ___endMarkerPos_13; }
	inline int32_t* get_address_of_endMarkerPos_13() { return &___endMarkerPos_13; }
	inline void set_endMarkerPos_13(int32_t value)
	{
		___endMarkerPos_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
