﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_IOException2458421087.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileNotFoundException
struct  FileNotFoundException_t4200667904  : public IOException_t2458421087
{
public:
	// System.String System.IO.FileNotFoundException::_fileName
	String_t* ____fileName_17;
	// System.String System.IO.FileNotFoundException::_fusionLog
	String_t* ____fusionLog_18;

public:
	inline static int32_t get_offset_of__fileName_17() { return static_cast<int32_t>(offsetof(FileNotFoundException_t4200667904, ____fileName_17)); }
	inline String_t* get__fileName_17() const { return ____fileName_17; }
	inline String_t** get_address_of__fileName_17() { return &____fileName_17; }
	inline void set__fileName_17(String_t* value)
	{
		____fileName_17 = value;
		Il2CppCodeGenWriteBarrier(&____fileName_17, value);
	}

	inline static int32_t get_offset_of__fusionLog_18() { return static_cast<int32_t>(offsetof(FileNotFoundException_t4200667904, ____fusionLog_18)); }
	inline String_t* get__fusionLog_18() const { return ____fusionLog_18; }
	inline String_t** get_address_of__fusionLog_18() { return &____fusionLog_18; }
	inline void set__fusionLog_18(String_t* value)
	{
		____fusionLog_18 = value;
		Il2CppCodeGenWriteBarrier(&____fusionLog_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
