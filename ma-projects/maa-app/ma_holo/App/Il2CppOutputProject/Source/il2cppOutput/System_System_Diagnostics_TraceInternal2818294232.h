﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"

// System.String
struct String_t;
// System.Diagnostics.TraceListenerCollection
struct TraceListenerCollection_t2289511703;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceInternal
struct  TraceInternal_t2818294232  : public Il2CppObject
{
public:

public:
};

struct TraceInternal_t2818294232_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::appName
	String_t* ___appName_0;
	// System.Diagnostics.TraceListenerCollection modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::listeners
	TraceListenerCollection_t2289511703 * ___listeners_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::autoFlush
	bool ___autoFlush_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::useGlobalLock
	bool ___useGlobalLock_3;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::indentSize
	int32_t ___indentSize_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::settingsInitialized
	bool ___settingsInitialized_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.TraceInternal::defaultInitialized
	bool ___defaultInitialized_7;
	// System.Object System.Diagnostics.TraceInternal::critSec
	Il2CppObject * ___critSec_8;

public:
	inline static int32_t get_offset_of_appName_0() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___appName_0)); }
	inline String_t* get_appName_0() const { return ___appName_0; }
	inline String_t** get_address_of_appName_0() { return &___appName_0; }
	inline void set_appName_0(String_t* value)
	{
		___appName_0 = value;
		Il2CppCodeGenWriteBarrier(&___appName_0, value);
	}

	inline static int32_t get_offset_of_listeners_1() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___listeners_1)); }
	inline TraceListenerCollection_t2289511703 * get_listeners_1() const { return ___listeners_1; }
	inline TraceListenerCollection_t2289511703 ** get_address_of_listeners_1() { return &___listeners_1; }
	inline void set_listeners_1(TraceListenerCollection_t2289511703 * value)
	{
		___listeners_1 = value;
		Il2CppCodeGenWriteBarrier(&___listeners_1, value);
	}

	inline static int32_t get_offset_of_autoFlush_2() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___autoFlush_2)); }
	inline bool get_autoFlush_2() const { return ___autoFlush_2; }
	inline bool* get_address_of_autoFlush_2() { return &___autoFlush_2; }
	inline void set_autoFlush_2(bool value)
	{
		___autoFlush_2 = value;
	}

	inline static int32_t get_offset_of_useGlobalLock_3() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___useGlobalLock_3)); }
	inline bool get_useGlobalLock_3() const { return ___useGlobalLock_3; }
	inline bool* get_address_of_useGlobalLock_3() { return &___useGlobalLock_3; }
	inline void set_useGlobalLock_3(bool value)
	{
		___useGlobalLock_3 = value;
	}

	inline static int32_t get_offset_of_indentSize_5() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___indentSize_5)); }
	inline int32_t get_indentSize_5() const { return ___indentSize_5; }
	inline int32_t* get_address_of_indentSize_5() { return &___indentSize_5; }
	inline void set_indentSize_5(int32_t value)
	{
		___indentSize_5 = value;
	}

	inline static int32_t get_offset_of_settingsInitialized_6() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___settingsInitialized_6)); }
	inline bool get_settingsInitialized_6() const { return ___settingsInitialized_6; }
	inline bool* get_address_of_settingsInitialized_6() { return &___settingsInitialized_6; }
	inline void set_settingsInitialized_6(bool value)
	{
		___settingsInitialized_6 = value;
	}

	inline static int32_t get_offset_of_defaultInitialized_7() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___defaultInitialized_7)); }
	inline bool get_defaultInitialized_7() const { return ___defaultInitialized_7; }
	inline bool* get_address_of_defaultInitialized_7() { return &___defaultInitialized_7; }
	inline void set_defaultInitialized_7(bool value)
	{
		___defaultInitialized_7 = value;
	}

	inline static int32_t get_offset_of_critSec_8() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_StaticFields, ___critSec_8)); }
	inline Il2CppObject * get_critSec_8() const { return ___critSec_8; }
	inline Il2CppObject ** get_address_of_critSec_8() { return &___critSec_8; }
	inline void set_critSec_8(Il2CppObject * value)
	{
		___critSec_8 = value;
		Il2CppCodeGenWriteBarrier(&___critSec_8, value);
	}
};

struct TraceInternal_t2818294232_ThreadStaticFields
{
public:
	// System.Int32 System.Diagnostics.TraceInternal::indentLevel
	int32_t ___indentLevel_4;

public:
	inline static int32_t get_offset_of_indentLevel_4() { return static_cast<int32_t>(offsetof(TraceInternal_t2818294232_ThreadStaticFields, ___indentLevel_4)); }
	inline int32_t get_indentLevel_4() const { return ___indentLevel_4; }
	inline int32_t* get_address_of_indentLevel_4() { return &___indentLevel_4; }
	inline void set_indentLevel_4(int32_t value)
	{
		___indentLevel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
