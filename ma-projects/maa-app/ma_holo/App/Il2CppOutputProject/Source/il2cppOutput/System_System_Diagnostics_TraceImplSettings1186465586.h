﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Diagnostics.TraceListenerCollection
struct TraceListenerCollection_t2289511703;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceImplSettings
struct  TraceImplSettings_t1186465586  : public Il2CppObject
{
public:
	// System.Boolean System.Diagnostics.TraceImplSettings::AutoFlush
	bool ___AutoFlush_0;
	// System.Int32 System.Diagnostics.TraceImplSettings::IndentSize
	int32_t ___IndentSize_1;
	// System.Diagnostics.TraceListenerCollection System.Diagnostics.TraceImplSettings::Listeners
	TraceListenerCollection_t2289511703 * ___Listeners_2;

public:
	inline static int32_t get_offset_of_AutoFlush_0() { return static_cast<int32_t>(offsetof(TraceImplSettings_t1186465586, ___AutoFlush_0)); }
	inline bool get_AutoFlush_0() const { return ___AutoFlush_0; }
	inline bool* get_address_of_AutoFlush_0() { return &___AutoFlush_0; }
	inline void set_AutoFlush_0(bool value)
	{
		___AutoFlush_0 = value;
	}

	inline static int32_t get_offset_of_IndentSize_1() { return static_cast<int32_t>(offsetof(TraceImplSettings_t1186465586, ___IndentSize_1)); }
	inline int32_t get_IndentSize_1() const { return ___IndentSize_1; }
	inline int32_t* get_address_of_IndentSize_1() { return &___IndentSize_1; }
	inline void set_IndentSize_1(int32_t value)
	{
		___IndentSize_1 = value;
	}

	inline static int32_t get_offset_of_Listeners_2() { return static_cast<int32_t>(offsetof(TraceImplSettings_t1186465586, ___Listeners_2)); }
	inline TraceListenerCollection_t2289511703 * get_Listeners_2() const { return ___Listeners_2; }
	inline TraceListenerCollection_t2289511703 ** get_address_of_Listeners_2() { return &___Listeners_2; }
	inline void set_Listeners_2(TraceListenerCollection_t2289511703 * value)
	{
		___Listeners_2 = value;
		Il2CppCodeGenWriteBarrier(&___Listeners_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
