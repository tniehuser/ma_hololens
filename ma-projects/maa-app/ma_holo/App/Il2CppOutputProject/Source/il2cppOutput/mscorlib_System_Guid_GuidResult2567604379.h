﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Guid_GuidParseThrowStyle3041578670.h"
#include "mscorlib_System_Guid_ParseFailureKind3905197472.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1927440687;
struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid/GuidResult
struct  GuidResult_t2567604379 
{
public:
	// System.Guid System.Guid/GuidResult::parsedGuid
	Guid_t  ___parsedGuid_0;
	// System.Guid/GuidParseThrowStyle System.Guid/GuidResult::throwStyle
	int32_t ___throwStyle_1;
	// System.Guid/ParseFailureKind System.Guid/GuidResult::m_failure
	int32_t ___m_failure_2;
	// System.String System.Guid/GuidResult::m_failureMessageID
	String_t* ___m_failureMessageID_3;
	// System.Object System.Guid/GuidResult::m_failureMessageFormatArgument
	Il2CppObject * ___m_failureMessageFormatArgument_4;
	// System.String System.Guid/GuidResult::m_failureArgumentName
	String_t* ___m_failureArgumentName_5;
	// System.Exception System.Guid/GuidResult::m_innerException
	Exception_t1927440687 * ___m_innerException_6;

public:
	inline static int32_t get_offset_of_parsedGuid_0() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___parsedGuid_0)); }
	inline Guid_t  get_parsedGuid_0() const { return ___parsedGuid_0; }
	inline Guid_t * get_address_of_parsedGuid_0() { return &___parsedGuid_0; }
	inline void set_parsedGuid_0(Guid_t  value)
	{
		___parsedGuid_0 = value;
	}

	inline static int32_t get_offset_of_throwStyle_1() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___throwStyle_1)); }
	inline int32_t get_throwStyle_1() const { return ___throwStyle_1; }
	inline int32_t* get_address_of_throwStyle_1() { return &___throwStyle_1; }
	inline void set_throwStyle_1(int32_t value)
	{
		___throwStyle_1 = value;
	}

	inline static int32_t get_offset_of_m_failure_2() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___m_failure_2)); }
	inline int32_t get_m_failure_2() const { return ___m_failure_2; }
	inline int32_t* get_address_of_m_failure_2() { return &___m_failure_2; }
	inline void set_m_failure_2(int32_t value)
	{
		___m_failure_2 = value;
	}

	inline static int32_t get_offset_of_m_failureMessageID_3() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___m_failureMessageID_3)); }
	inline String_t* get_m_failureMessageID_3() const { return ___m_failureMessageID_3; }
	inline String_t** get_address_of_m_failureMessageID_3() { return &___m_failureMessageID_3; }
	inline void set_m_failureMessageID_3(String_t* value)
	{
		___m_failureMessageID_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureMessageID_3, value);
	}

	inline static int32_t get_offset_of_m_failureMessageFormatArgument_4() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___m_failureMessageFormatArgument_4)); }
	inline Il2CppObject * get_m_failureMessageFormatArgument_4() const { return ___m_failureMessageFormatArgument_4; }
	inline Il2CppObject ** get_address_of_m_failureMessageFormatArgument_4() { return &___m_failureMessageFormatArgument_4; }
	inline void set_m_failureMessageFormatArgument_4(Il2CppObject * value)
	{
		___m_failureMessageFormatArgument_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureMessageFormatArgument_4, value);
	}

	inline static int32_t get_offset_of_m_failureArgumentName_5() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___m_failureArgumentName_5)); }
	inline String_t* get_m_failureArgumentName_5() const { return ___m_failureArgumentName_5; }
	inline String_t** get_address_of_m_failureArgumentName_5() { return &___m_failureArgumentName_5; }
	inline void set_m_failureArgumentName_5(String_t* value)
	{
		___m_failureArgumentName_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureArgumentName_5, value);
	}

	inline static int32_t get_offset_of_m_innerException_6() { return static_cast<int32_t>(offsetof(GuidResult_t2567604379, ___m_innerException_6)); }
	inline Exception_t1927440687 * get_m_innerException_6() const { return ___m_innerException_6; }
	inline Exception_t1927440687 ** get_address_of_m_innerException_6() { return &___m_innerException_6; }
	inline void set_m_innerException_6(Exception_t1927440687 * value)
	{
		___m_innerException_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_innerException_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Guid/GuidResult
struct GuidResult_t2567604379_marshaled_pinvoke
{
	Guid_t  ___parsedGuid_0;
	int32_t ___throwStyle_1;
	int32_t ___m_failure_2;
	char* ___m_failureMessageID_3;
	Il2CppIUnknown* ___m_failureMessageFormatArgument_4;
	char* ___m_failureArgumentName_5;
	Exception_t1927440687_marshaled_pinvoke* ___m_innerException_6;
};
// Native definition for COM marshalling of System.Guid/GuidResult
struct GuidResult_t2567604379_marshaled_com
{
	Guid_t  ___parsedGuid_0;
	int32_t ___throwStyle_1;
	int32_t ___m_failure_2;
	Il2CppChar* ___m_failureMessageID_3;
	Il2CppIUnknown* ___m_failureMessageFormatArgument_4;
	Il2CppChar* ___m_failureArgumentName_5;
	Exception_t1927440687_marshaled_com* ___m_innerException_6;
};
