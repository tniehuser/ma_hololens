﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.BinaryReader
struct BinaryReader_t2491843768;
// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceLocator>
struct Dictionary_2_t4071170146;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.RuntimeType[]
struct RuntimeTypeU5BU5D_t2680569683;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t1866979105;
// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t822875729;
// System.Int32
struct Int32_t2071877448;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceReader
struct  ResourceReader_t2463923611  : public Il2CppObject
{
public:
	// System.IO.BinaryReader System.Resources.ResourceReader::_store
	BinaryReader_t2491843768 * ____store_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceLocator> System.Resources.ResourceReader::_resCache
	Dictionary_2_t4071170146 * ____resCache_1;
	// System.Int64 System.Resources.ResourceReader::_nameSectionOffset
	int64_t ____nameSectionOffset_2;
	// System.Int64 System.Resources.ResourceReader::_dataSectionOffset
	int64_t ____dataSectionOffset_3;
	// System.Int32[] System.Resources.ResourceReader::_nameHashes
	Int32U5BU5D_t3030399641* ____nameHashes_4;
	// System.Int32* System.Resources.ResourceReader::_nameHashesPtr
	int32_t* ____nameHashesPtr_5;
	// System.Int32[] System.Resources.ResourceReader::_namePositions
	Int32U5BU5D_t3030399641* ____namePositions_6;
	// System.Int32* System.Resources.ResourceReader::_namePositionsPtr
	int32_t* ____namePositionsPtr_7;
	// System.RuntimeType[] System.Resources.ResourceReader::_typeTable
	RuntimeTypeU5BU5D_t2680569683* ____typeTable_8;
	// System.Int32[] System.Resources.ResourceReader::_typeNamePositions
	Int32U5BU5D_t3030399641* ____typeNamePositions_9;
	// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter System.Resources.ResourceReader::_objFormatter
	BinaryFormatter_t1866979105 * ____objFormatter_10;
	// System.Int32 System.Resources.ResourceReader::_numResources
	int32_t ____numResources_11;
	// System.IO.UnmanagedMemoryStream System.Resources.ResourceReader::_ums
	UnmanagedMemoryStream_t822875729 * ____ums_12;
	// System.Int32 System.Resources.ResourceReader::_version
	int32_t ____version_13;

public:
	inline static int32_t get_offset_of__store_0() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____store_0)); }
	inline BinaryReader_t2491843768 * get__store_0() const { return ____store_0; }
	inline BinaryReader_t2491843768 ** get_address_of__store_0() { return &____store_0; }
	inline void set__store_0(BinaryReader_t2491843768 * value)
	{
		____store_0 = value;
		Il2CppCodeGenWriteBarrier(&____store_0, value);
	}

	inline static int32_t get_offset_of__resCache_1() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____resCache_1)); }
	inline Dictionary_2_t4071170146 * get__resCache_1() const { return ____resCache_1; }
	inline Dictionary_2_t4071170146 ** get_address_of__resCache_1() { return &____resCache_1; }
	inline void set__resCache_1(Dictionary_2_t4071170146 * value)
	{
		____resCache_1 = value;
		Il2CppCodeGenWriteBarrier(&____resCache_1, value);
	}

	inline static int32_t get_offset_of__nameSectionOffset_2() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____nameSectionOffset_2)); }
	inline int64_t get__nameSectionOffset_2() const { return ____nameSectionOffset_2; }
	inline int64_t* get_address_of__nameSectionOffset_2() { return &____nameSectionOffset_2; }
	inline void set__nameSectionOffset_2(int64_t value)
	{
		____nameSectionOffset_2 = value;
	}

	inline static int32_t get_offset_of__dataSectionOffset_3() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____dataSectionOffset_3)); }
	inline int64_t get__dataSectionOffset_3() const { return ____dataSectionOffset_3; }
	inline int64_t* get_address_of__dataSectionOffset_3() { return &____dataSectionOffset_3; }
	inline void set__dataSectionOffset_3(int64_t value)
	{
		____dataSectionOffset_3 = value;
	}

	inline static int32_t get_offset_of__nameHashes_4() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____nameHashes_4)); }
	inline Int32U5BU5D_t3030399641* get__nameHashes_4() const { return ____nameHashes_4; }
	inline Int32U5BU5D_t3030399641** get_address_of__nameHashes_4() { return &____nameHashes_4; }
	inline void set__nameHashes_4(Int32U5BU5D_t3030399641* value)
	{
		____nameHashes_4 = value;
		Il2CppCodeGenWriteBarrier(&____nameHashes_4, value);
	}

	inline static int32_t get_offset_of__nameHashesPtr_5() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____nameHashesPtr_5)); }
	inline int32_t* get__nameHashesPtr_5() const { return ____nameHashesPtr_5; }
	inline int32_t** get_address_of__nameHashesPtr_5() { return &____nameHashesPtr_5; }
	inline void set__nameHashesPtr_5(int32_t* value)
	{
		____nameHashesPtr_5 = value;
	}

	inline static int32_t get_offset_of__namePositions_6() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____namePositions_6)); }
	inline Int32U5BU5D_t3030399641* get__namePositions_6() const { return ____namePositions_6; }
	inline Int32U5BU5D_t3030399641** get_address_of__namePositions_6() { return &____namePositions_6; }
	inline void set__namePositions_6(Int32U5BU5D_t3030399641* value)
	{
		____namePositions_6 = value;
		Il2CppCodeGenWriteBarrier(&____namePositions_6, value);
	}

	inline static int32_t get_offset_of__namePositionsPtr_7() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____namePositionsPtr_7)); }
	inline int32_t* get__namePositionsPtr_7() const { return ____namePositionsPtr_7; }
	inline int32_t** get_address_of__namePositionsPtr_7() { return &____namePositionsPtr_7; }
	inline void set__namePositionsPtr_7(int32_t* value)
	{
		____namePositionsPtr_7 = value;
	}

	inline static int32_t get_offset_of__typeTable_8() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____typeTable_8)); }
	inline RuntimeTypeU5BU5D_t2680569683* get__typeTable_8() const { return ____typeTable_8; }
	inline RuntimeTypeU5BU5D_t2680569683** get_address_of__typeTable_8() { return &____typeTable_8; }
	inline void set__typeTable_8(RuntimeTypeU5BU5D_t2680569683* value)
	{
		____typeTable_8 = value;
		Il2CppCodeGenWriteBarrier(&____typeTable_8, value);
	}

	inline static int32_t get_offset_of__typeNamePositions_9() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____typeNamePositions_9)); }
	inline Int32U5BU5D_t3030399641* get__typeNamePositions_9() const { return ____typeNamePositions_9; }
	inline Int32U5BU5D_t3030399641** get_address_of__typeNamePositions_9() { return &____typeNamePositions_9; }
	inline void set__typeNamePositions_9(Int32U5BU5D_t3030399641* value)
	{
		____typeNamePositions_9 = value;
		Il2CppCodeGenWriteBarrier(&____typeNamePositions_9, value);
	}

	inline static int32_t get_offset_of__objFormatter_10() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____objFormatter_10)); }
	inline BinaryFormatter_t1866979105 * get__objFormatter_10() const { return ____objFormatter_10; }
	inline BinaryFormatter_t1866979105 ** get_address_of__objFormatter_10() { return &____objFormatter_10; }
	inline void set__objFormatter_10(BinaryFormatter_t1866979105 * value)
	{
		____objFormatter_10 = value;
		Il2CppCodeGenWriteBarrier(&____objFormatter_10, value);
	}

	inline static int32_t get_offset_of__numResources_11() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____numResources_11)); }
	inline int32_t get__numResources_11() const { return ____numResources_11; }
	inline int32_t* get_address_of__numResources_11() { return &____numResources_11; }
	inline void set__numResources_11(int32_t value)
	{
		____numResources_11 = value;
	}

	inline static int32_t get_offset_of__ums_12() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____ums_12)); }
	inline UnmanagedMemoryStream_t822875729 * get__ums_12() const { return ____ums_12; }
	inline UnmanagedMemoryStream_t822875729 ** get_address_of__ums_12() { return &____ums_12; }
	inline void set__ums_12(UnmanagedMemoryStream_t822875729 * value)
	{
		____ums_12 = value;
		Il2CppCodeGenWriteBarrier(&____ums_12, value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(ResourceReader_t2463923611, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
