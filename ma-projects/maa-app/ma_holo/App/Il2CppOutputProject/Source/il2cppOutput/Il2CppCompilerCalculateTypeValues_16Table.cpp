﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3880736488.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3955735183.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Subject604050261.h"
#include "Mono_Security_Mono_Security_Cryptography_HMAC2707728663.h"
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA13340472487.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLeve1706602846.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescr844791462.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert3405955216.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlg4212518094.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuit491456551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui2431504453.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui3273693255.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon3002158488.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec2694504884.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3468069089.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3595945587.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTyp859870085.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context4285182719.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAl954949548.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1820731088.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor1654661965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClie3823629320.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3166895267.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro1946181211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProt173216930.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig1282301050.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC3722381418.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2290372928.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityPr155967584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3823737132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat989458295.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3318447433.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3721235490.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe1663566523.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3918817353.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClientSt62894417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3602869449.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1404755603.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh3044322977.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslServerS330206493.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamB934199321.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1610391122.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherS396038680.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2311449551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcepti583514812.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerS403340211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream4089752859.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3938752374.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2540099417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2537917473.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4150496570.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3939745042.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2939633944.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3808761250.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_905088469.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2187269356.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1869592958.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1289300668.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_530021076.h"
#include "Mono_Security_Mono_Xml_MiniParser185565106.h"
#include "Mono_Security_Mono_Xml_MiniParser_HandlerAdapter3228504856.h"
#include "Mono_Security_Mono_Xml_MiniParser_AttrListImpl383345538.h"
#include "Mono_Security_Mono_Xml_MiniParser_XMLError1680455686.h"
#include "Mono_Security_Mono_Xml_SecurityParser30730985.h"
#include "Mono_Security_Mono_Security_Interface_ValidationRe2452252148.h"
#include "Mono_Security_Mono_Security_Interface_CertificateV1569212914.h"
#include "Mono_Security_Mono_Security_Interface_CipherSuiteC4229451518.h"
#include "Mono_Security_Mono_Security_Interface_MonoSslPolicy621534536.h"
#include "Mono_Security_Mono_Security_Interface_MonoRemoteCe1929047274.h"
#include "Mono_Security_Mono_Security_Interface_MonoLocalCer4080334132.h"
#include "Mono_Security_Mono_Security_Interface_MonoTlsProvid823784021.h"
#include "Mono_Security_Mono_Security_Interface_MonoTlsSettin302829305.h"
#include "Mono_Security_Mono_Security_Interface_TlsProtocols1951446164.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2882533465.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2731437132.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1568637719.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2375206766.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2762068660.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3894236545.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1459944470.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1568637717.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2306864645.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_SR2523137203.h"
#include "System_System_Configuration_ConfigurationException3814184945.h"
#include "System_System_Configuration_ConfigurationSettings1600776263.h"
#include "System_System_Configuration_DefaultConfig320482295.h"
#include "System_System_Diagnostics_DefaultTraceListener1568159610.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (NetscapeCertTypeExtension_t3880736488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	NetscapeCertTypeExtension_t3880736488::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (CertTypes_t3955735183)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1601[8] = 
{
	CertTypes_t3955735183::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (SubjectAltNameExtension_t604050261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[1] = 
{
	SubjectAltNameExtension_t604050261::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (HMAC_t2707728663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[4] = 
{
	HMAC_t2707728663::get_offset_of_hash_5(),
	HMAC_t2707728663::get_offset_of_hashing_6(),
	HMAC_t2707728663::get_offset_of_innerPad_7(),
	HMAC_t2707728663::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (MD5SHA1_t3340472487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[3] = 
{
	MD5SHA1_t3340472487::get_offset_of_md5_4(),
	MD5SHA1_t3340472487::get_offset_of_sha_5(),
	MD5SHA1_t3340472487::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (AlertLevel_t1706602846)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1605[3] = 
{
	AlertLevel_t1706602846::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (AlertDescription_t844791462)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1606[25] = 
{
	AlertDescription_t844791462::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Alert_t3405955216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[2] = 
{
	Alert_t3405955216::get_offset_of_level_0(),
	Alert_t3405955216::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (CipherAlgorithmType_t4212518094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[8] = 
{
	CipherAlgorithmType_t4212518094::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (CipherSuite_t491456551), -1, sizeof(CipherSuite_t491456551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1609[21] = 
{
	CipherSuite_t491456551_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t491456551::get_offset_of_code_1(),
	CipherSuite_t491456551::get_offset_of_name_2(),
	CipherSuite_t491456551::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t491456551::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t491456551::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t491456551::get_offset_of_isExportable_6(),
	CipherSuite_t491456551::get_offset_of_cipherMode_7(),
	CipherSuite_t491456551::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t491456551::get_offset_of_keyBlockSize_9(),
	CipherSuite_t491456551::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t491456551::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t491456551::get_offset_of_ivSize_12(),
	CipherSuite_t491456551::get_offset_of_blockSize_13(),
	CipherSuite_t491456551::get_offset_of_context_14(),
	CipherSuite_t491456551::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t491456551::get_offset_of_encryptionCipher_16(),
	CipherSuite_t491456551::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t491456551::get_offset_of_decryptionCipher_18(),
	CipherSuite_t491456551::get_offset_of_clientHMAC_19(),
	CipherSuite_t491456551::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (CipherSuiteCollection_t2431504453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[1] = 
{
	CipherSuiteCollection_t2431504453::get_offset_of_protocol_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (CipherSuiteFactory_t3273693255), -1, sizeof(CipherSuiteFactory_t3273693255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1611[1] = 
{
	CipherSuiteFactory_t3273693255_StaticFields::get_offset_of_spm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (ClientContext_t3002158488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[2] = 
{
	ClientContext_t3002158488::get_offset_of_sslStream_31(),
	ClientContext_t3002158488::get_offset_of_clientHelloProtocol_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (ClientRecordProtocol_t2694504884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (ClientSessionInfo_t3468069089), -1, sizeof(ClientSessionInfo_t3468069089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1614[6] = 
{
	ClientSessionInfo_t3468069089_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t3468069089::get_offset_of_disposed_1(),
	ClientSessionInfo_t3468069089::get_offset_of_validuntil_2(),
	ClientSessionInfo_t3468069089::get_offset_of_host_3(),
	ClientSessionInfo_t3468069089::get_offset_of_sid_4(),
	ClientSessionInfo_t3468069089::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (ClientSessionCache_t3595945587), -1, sizeof(ClientSessionCache_t3595945587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[2] = 
{
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (ContentType_t859870085)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1616[5] = 
{
	ContentType_t859870085::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Context_t4285182719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[31] = 
{
	Context_t4285182719::get_offset_of_securityProtocol_0(),
	Context_t4285182719::get_offset_of_sessionId_1(),
	Context_t4285182719::get_offset_of_compressionMethod_2(),
	Context_t4285182719::get_offset_of_serverSettings_3(),
	Context_t4285182719::get_offset_of_clientSettings_4(),
	Context_t4285182719::get_offset_of_current_5(),
	Context_t4285182719::get_offset_of_negotiating_6(),
	Context_t4285182719::get_offset_of_read_7(),
	Context_t4285182719::get_offset_of_write_8(),
	Context_t4285182719::get_offset_of_supportedCiphers_9(),
	Context_t4285182719::get_offset_of_lastHandshakeMsg_10(),
	Context_t4285182719::get_offset_of_handshakeState_11(),
	Context_t4285182719::get_offset_of_abbreviatedHandshake_12(),
	Context_t4285182719::get_offset_of_receivedConnectionEnd_13(),
	Context_t4285182719::get_offset_of_sentConnectionEnd_14(),
	Context_t4285182719::get_offset_of_protocolNegotiated_15(),
	Context_t4285182719::get_offset_of_writeSequenceNumber_16(),
	Context_t4285182719::get_offset_of_readSequenceNumber_17(),
	Context_t4285182719::get_offset_of_clientRandom_18(),
	Context_t4285182719::get_offset_of_serverRandom_19(),
	Context_t4285182719::get_offset_of_randomCS_20(),
	Context_t4285182719::get_offset_of_randomSC_21(),
	Context_t4285182719::get_offset_of_masterSecret_22(),
	Context_t4285182719::get_offset_of_clientWriteKey_23(),
	Context_t4285182719::get_offset_of_serverWriteKey_24(),
	Context_t4285182719::get_offset_of_clientWriteIV_25(),
	Context_t4285182719::get_offset_of_serverWriteIV_26(),
	Context_t4285182719::get_offset_of_handshakeMessages_27(),
	Context_t4285182719::get_offset_of_random_28(),
	Context_t4285182719::get_offset_of_recordProtocol_29(),
	Context_t4285182719::get_offset_of_U3CChangeCipherSpecDoneU3Ek__BackingField_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (ExchangeAlgorithmType_t954949548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[6] = 
{
	ExchangeAlgorithmType_t954949548::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (HandshakeState_t1820731088)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1619[4] = 
{
	HandshakeState_t1820731088::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (HashAlgorithmType_t1654661965)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[4] = 
{
	HashAlgorithmType_t1654661965::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (HttpsClientStream_t3823629320), -1, sizeof(HttpsClientStream_t3823629320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1621[4] = 
{
	HttpsClientStream_t3823629320::get_offset_of__request_26(),
	HttpsClientStream_t3823629320::get_offset_of__status_27(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_28(),
	HttpsClientStream_t3823629320_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (RecordProtocol_t3166895267), -1, sizeof(RecordProtocol_t3166895267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[3] = 
{
	RecordProtocol_t3166895267_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3166895267::get_offset_of_innerStream_1(),
	RecordProtocol_t3166895267::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (ReceiveRecordAsyncResult_t1946181211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1623[9] = 
{
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t1946181211::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (SendRecordAsyncResult_t173216930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[7] = 
{
	SendRecordAsyncResult_t173216930::get_offset_of_locker_0(),
	SendRecordAsyncResult_t173216930::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t173216930::get_offset_of__userState_2(),
	SendRecordAsyncResult_t173216930::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t173216930::get_offset_of_handle_4(),
	SendRecordAsyncResult_t173216930::get_offset_of__message_5(),
	SendRecordAsyncResult_t173216930::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (RSASslSignatureFormatter_t1282301050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1625[2] = 
{
	RSASslSignatureFormatter_t1282301050::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1282301050::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (SecurityCompressionType_t3722381418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1626[3] = 
{
	SecurityCompressionType_t3722381418::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (SecurityParameters_t2290372928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[3] = 
{
	SecurityParameters_t2290372928::get_offset_of_cipher_0(),
	SecurityParameters_t2290372928::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2290372928::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (SecurityProtocolType_t155967584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1628[7] = 
{
	SecurityProtocolType_t155967584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (ServerContext_t3823737132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[2] = 
{
	ServerContext_t3823737132::get_offset_of_request_client_certificate_31(),
	ServerContext_t3823737132::get_offset_of_clientCertificateRequired_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (SslClientStream_t3918817353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[4] = 
{
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation_22(),
	SslClientStream_t3918817353::get_offset_of_ClientCertSelection_23(),
	SslClientStream_t3918817353::get_offset_of_PrivateKeySelection_24(),
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation2_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (NegotiateState_t62894417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1635[8] = 
{
	NegotiateState_t62894417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (NegotiateAsyncResult_t3602869449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[7] = 
{
	NegotiateAsyncResult_t3602869449::get_offset_of_locker_0(),
	NegotiateAsyncResult_t3602869449::get_offset_of__userCallback_1(),
	NegotiateAsyncResult_t3602869449::get_offset_of__userState_2(),
	NegotiateAsyncResult_t3602869449::get_offset_of__asyncException_3(),
	NegotiateAsyncResult_t3602869449::get_offset_of_handle_4(),
	NegotiateAsyncResult_t3602869449::get_offset_of__state_5(),
	NegotiateAsyncResult_t3602869449::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (SslCipherSuite_t1404755603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[3] = 
{
	SslCipherSuite_t1404755603::get_offset_of_pad1_21(),
	SslCipherSuite_t1404755603::get_offset_of_pad2_22(),
	SslCipherSuite_t1404755603::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (SslHandshakeHash_t3044322977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[8] = 
{
	SslHandshakeHash_t3044322977::get_offset_of_md5_4(),
	SslHandshakeHash_t3044322977::get_offset_of_sha_5(),
	SslHandshakeHash_t3044322977::get_offset_of_hashing_6(),
	SslHandshakeHash_t3044322977::get_offset_of_secret_7(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (SslServerStream_t330206493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[3] = 
{
	SslServerStream_t330206493::get_offset_of_ClientCertValidation_22(),
	SslServerStream_t330206493::get_offset_of_PrivateKeySelection_23(),
	SslServerStream_t330206493::get_offset_of_ClientCertValidation2_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (SslStreamBase_t934199321), -1, sizeof(SslStreamBase_t934199321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1640[14] = 
{
	SslStreamBase_t934199321_StaticFields::get_offset_of_record_processing_8(),
	SslStreamBase_t934199321::get_offset_of_innerStream_9(),
	SslStreamBase_t934199321::get_offset_of_inputBuffer_10(),
	SslStreamBase_t934199321::get_offset_of_context_11(),
	SslStreamBase_t934199321::get_offset_of_protocol_12(),
	SslStreamBase_t934199321::get_offset_of_ownsStream_13(),
	SslStreamBase_t934199321::get_offset_of_disposed_14(),
	SslStreamBase_t934199321::get_offset_of_checkCertRevocationStatus_15(),
	SslStreamBase_t934199321::get_offset_of_negotiate_16(),
	SslStreamBase_t934199321::get_offset_of_read_17(),
	SslStreamBase_t934199321::get_offset_of_write_18(),
	SslStreamBase_t934199321::get_offset_of_negotiationComplete_19(),
	SslStreamBase_t934199321::get_offset_of_recbuf_20(),
	SslStreamBase_t934199321::get_offset_of_recordStream_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (InternalAsyncResult_t1610391122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[12] = 
{
	InternalAsyncResult_t1610391122::get_offset_of_locker_0(),
	InternalAsyncResult_t1610391122::get_offset_of__userCallback_1(),
	InternalAsyncResult_t1610391122::get_offset_of__userState_2(),
	InternalAsyncResult_t1610391122::get_offset_of__asyncException_3(),
	InternalAsyncResult_t1610391122::get_offset_of_handle_4(),
	InternalAsyncResult_t1610391122::get_offset_of_completed_5(),
	InternalAsyncResult_t1610391122::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t1610391122::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t1610391122::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t1610391122::get_offset_of__buffer_9(),
	InternalAsyncResult_t1610391122::get_offset_of__offset_10(),
	InternalAsyncResult_t1610391122::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (TlsCipherSuite_t396038680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[2] = 
{
	TlsCipherSuite_t396038680::get_offset_of_header_21(),
	TlsCipherSuite_t396038680::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (TlsClientSettings_t2311449551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[4] = 
{
	TlsClientSettings_t2311449551::get_offset_of_targetHost_0(),
	TlsClientSettings_t2311449551::get_offset_of_certificates_1(),
	TlsClientSettings_t2311449551::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2311449551::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (TlsException_t583514812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[1] = 
{
	TlsException_t583514812::get_offset_of_alert_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (TlsServerSettings_t403340211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[7] = 
{
	TlsServerSettings_t403340211::get_offset_of_certificates_0(),
	TlsServerSettings_t403340211::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t403340211::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t403340211::get_offset_of_distinguisedNames_3(),
	TlsServerSettings_t403340211::get_offset_of_serverKeyExchange_4(),
	TlsServerSettings_t403340211::get_offset_of_certificateRequest_5(),
	TlsServerSettings_t403340211::get_offset_of_certificateTypes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (TlsStream_t4089752859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[4] = 
{
	TlsStream_t4089752859::get_offset_of_canRead_8(),
	TlsStream_t4089752859::get_offset_of_canWrite_9(),
	TlsStream_t4089752859::get_offset_of_buffer_10(),
	TlsStream_t4089752859::get_offset_of_temp_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (ClientCertificateType_t4001384466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1647[6] = 
{
	ClientCertificateType_t4001384466::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (HandshakeMessage_t3938752374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[4] = 
{
	HandshakeMessage_t3938752374::get_offset_of_context_12(),
	HandshakeMessage_t3938752374::get_offset_of_handshakeType_13(),
	HandshakeMessage_t3938752374::get_offset_of_contentType_14(),
	HandshakeMessage_t3938752374::get_offset_of_cache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (HandshakeType_t2540099417)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1649[12] = 
{
	HandshakeType_t2540099417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (TlsClientCertificate_t2537917473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[2] = 
{
	TlsClientCertificate_t2537917473::get_offset_of_clientCertSelected_16(),
	TlsClientCertificate_t2537917473::get_offset_of_clientCert_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (TlsClientCertificateVerify_t4150496570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (TlsClientFinished_t3939745042), -1, sizeof(TlsClientFinished_t3939745042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1652[1] = 
{
	TlsClientFinished_t3939745042_StaticFields::get_offset_of_Ssl3Marker_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (TlsClientHello_t2939633944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[1] = 
{
	TlsClientHello_t2939633944::get_offset_of_random_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (TlsClientKeyExchange_t3808761250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (TlsServerCertificate_t905088469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[1] = 
{
	TlsServerCertificate_t905088469::get_offset_of_certificates_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (TlsServerCertificateRequest_t2187269356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[2] = 
{
	TlsServerCertificateRequest_t2187269356::get_offset_of_certificateTypes_16(),
	TlsServerCertificateRequest_t2187269356::get_offset_of_distinguisedNames_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (TlsServerFinished_t1869592958), -1, sizeof(TlsServerFinished_t1869592958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	TlsServerFinished_t1869592958_StaticFields::get_offset_of_Ssl3Marker_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (TlsServerHello_t1289300668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[4] = 
{
	TlsServerHello_t1289300668::get_offset_of_compressionMethod_16(),
	TlsServerHello_t1289300668::get_offset_of_random_17(),
	TlsServerHello_t1289300668::get_offset_of_sessionId_18(),
	TlsServerHello_t1289300668::get_offset_of_cipherSuite_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (TlsServerHelloDone_t530021076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (MiniParser_t185565106), -1, sizeof(MiniParser_t185565106_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[7] = 
{
	MiniParser_t185565106_StaticFields::get_offset_of_INPUT_RANGE_0(),
	MiniParser_t185565106_StaticFields::get_offset_of_tbl_1(),
	MiniParser_t185565106_StaticFields::get_offset_of_errors_2(),
	MiniParser_t185565106::get_offset_of_line_3(),
	MiniParser_t185565106::get_offset_of_col_4(),
	MiniParser_t185565106::get_offset_of_twoCharBuff_5(),
	MiniParser_t185565106::get_offset_of_splitCData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (HandlerAdapter_t3228504856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (AttrListImpl_t383345538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[2] = 
{
	AttrListImpl_t383345538::get_offset_of_names_0(),
	AttrListImpl_t383345538::get_offset_of_values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (XMLError_t1680455686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[3] = 
{
	XMLError_t1680455686::get_offset_of_descr_16(),
	XMLError_t1680455686::get_offset_of_line_17(),
	XMLError_t1680455686::get_offset_of_column_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (SecurityParser_t30730986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[5] = 
{
	SecurityParser_t30730986::get_offset_of_root_7(),
	SecurityParser_t30730986::get_offset_of_xmldoc_8(),
	SecurityParser_t30730986::get_offset_of_pos_9(),
	SecurityParser_t30730986::get_offset_of_current_10(),
	SecurityParser_t30730986::get_offset_of_stack_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (ValidationResult_t2452252148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	ValidationResult_t2452252148::get_offset_of_trusted_0(),
	ValidationResult_t2452252148::get_offset_of_user_denied_1(),
	ValidationResult_t2452252148::get_offset_of_error_code_2(),
	ValidationResult_t2452252148::get_offset_of_policy_errors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (CertificateValidationHelper_t1569212914), -1, sizeof(CertificateValidationHelper_t1569212914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1672[2] = 
{
	CertificateValidationHelper_t1569212914_StaticFields::get_offset_of_noX509Chain_0(),
	CertificateValidationHelper_t1569212914_StaticFields::get_offset_of_supportsTrustAnchors_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (CipherSuiteCode_t4229451518)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1673[267] = 
{
	CipherSuiteCode_t4229451518::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (MonoSslPolicyErrors_t621534536)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1675[5] = 
{
	MonoSslPolicyErrors_t621534536::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (MonoRemoteCertificateValidationCallback_t1929047274), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (MonoLocalCertificateSelectionCallback_t4080334132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (MonoTlsProvider_t823784021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (MonoTlsSettings_t302829305), -1, sizeof(MonoTlsSettings_t302829305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1679[14] = 
{
	MonoTlsSettings_t302829305::get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(),
	MonoTlsSettings_t302829305::get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(),
	MonoTlsSettings_t302829305::get_offset_of_U3CTrustAnchorsU3Ek__BackingField_2(),
	MonoTlsSettings_t302829305::get_offset_of_U3CUserSettingsU3Ek__BackingField_3(),
	MonoTlsSettings_t302829305::get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_4(),
	MonoTlsSettings_t302829305::get_offset_of_U3CEnabledCiphersU3Ek__BackingField_5(),
	MonoTlsSettings_t302829305::get_offset_of_cloned_6(),
	MonoTlsSettings_t302829305::get_offset_of_checkCertName_7(),
	MonoTlsSettings_t302829305::get_offset_of_checkCertRevocationStatus_8(),
	MonoTlsSettings_t302829305::get_offset_of_useServicePointManagerCallback_9(),
	MonoTlsSettings_t302829305::get_offset_of_skipSystemValidators_10(),
	MonoTlsSettings_t302829305::get_offset_of_callbackNeedsChain_11(),
	MonoTlsSettings_t302829305::get_offset_of_certificateValidator_12(),
	MonoTlsSettings_t302829305_StaticFields::get_offset_of_defaultSettings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (TlsProtocols_t1951446164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1680[13] = 
{
	TlsProtocols_t1951446164::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305138), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1681[27] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_0(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DBE1BDEC0AA74B4DCB079943E70528096CCA985F8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DD28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_2(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_3(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DE75835D001C843F156FBA01B001DFE1B8029AC17_4(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D320B018758ECE3752FFEDBAEB1A6DB67C80B9359_5(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DCF0B42666EF5E37EDEA0AB8E173E42C196D03814_6(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_7(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DDF2F233DCDBB3568E0A055EB44EF8664BDAC9B0A_8(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DDDC2ED880C539C7F33DCD753EFC6935C3B89FBE3_9(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DC52D8C8A03F8882B47882CF64D7CEB5B16ABCB6F_10(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D479E6F4B309C1271A72CE31C4B974CBD1E46BF65_11(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D7C2A9CB288780591A4DD199ED7FDF1C472107480_12(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D37B9BCADB3EB6095AB8C927DAE9A0E9A441DAF18_13(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D1A5C53512C2C03B33C159F7812B37583471A8D03_14(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D44AF584FD19CC6DF2385F293ADA6E0019E7BE183_15(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DCC15284E6A49EF7DB9D2B39A26B096384F569476_16(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DA721C62955A330286E96B479DB7541A41C4983D7_17(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DE8AA9C2F35723FDE82408FCF60820DFBB7EDCBE7_18(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D23A65DEDCF5EE72DA424C5523A346EA9D72E35AC_19(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D5D033C1A00E415600D96B834A19DE753D0F86B39_20(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D1708D3021987F34D391566AA230CDFE0413CED85_21(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D3443C5AD82F84896DC3D0BE60E26BB64179F0E3C_22(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D3B357C7DF6D4E1233422B95EE0BA35C67A4AF17B_23(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_24(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_25(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24fieldU2DD693078666967609CB4A2B840921F9E052031847_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (U24ArrayTypeU3D3132_t2882533466)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D3132_t2882533466 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (U24ArrayTypeU3D20_t2731437133)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437133 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (U24ArrayTypeU3D32_t1568637720)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637720 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (U24ArrayTypeU3D48_t2375206767)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206767 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (U24ArrayTypeU3D64_t762068661)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t762068661 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U24ArrayTypeU3D16_t3894236546)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236546 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (U24ArrayTypeU3D4_t1459944471)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4_t1459944471 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (U24ArrayTypeU3D524_t2306864645)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D524_t2306864645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (SR_t2523137203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (ConfigurationException_t3814184945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[2] = 
{
	ConfigurationException_t3814184945::get_offset_of_filename_16(),
	ConfigurationException_t3814184945::get_offset_of_line_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (ConfigurationSettings_t1600776263), -1, sizeof(ConfigurationSettings_t1600776263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1694[2] = 
{
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (DefaultConfig_t320482295), -1, sizeof(DefaultConfig_t320482295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1695[1] = 
{
	DefaultConfig_t320482295_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (DefaultTraceListener_t1568159610), -1, sizeof(DefaultTraceListener_t1568159610_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_OnWin32_9(),
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_MonoTracePrefix_10(),
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_MonoTraceFile_11(),
	DefaultTraceListener_t1568159610::get_offset_of_logFileName_12(),
	DefaultTraceListener_t1568159610::get_offset_of_assertUiEnabled_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
