﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaType3522160305.h"

// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl>
struct Dictionary_2_t2930592312;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity>
struct Dictionary_2_t1970389535;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1445386684;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef>
struct Dictionary_2_t2500647674;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.SchemaNotation>
struct Dictionary_2_t3998263357;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaInfo
struct  SchemaInfo_t87206461  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::elementDecls
	Dictionary_2_t2930592312 * ___elementDecls_0;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::undeclaredElementDecls
	Dictionary_2_t2930592312 * ___undeclaredElementDecls_1;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity> System.Xml.Schema.SchemaInfo::generalEntities
	Dictionary_2_t1970389535 * ___generalEntities_2;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity> System.Xml.Schema.SchemaInfo::parameterEntities
	Dictionary_2_t1970389535 * ___parameterEntities_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaInfo::docTypeName
	XmlQualifiedName_t1944712516 * ___docTypeName_4;
	// System.String System.Xml.Schema.SchemaInfo::internalDtdSubset
	String_t* ___internalDtdSubset_5;
	// System.Boolean System.Xml.Schema.SchemaInfo::hasNonCDataAttributes
	bool ___hasNonCDataAttributes_6;
	// System.Boolean System.Xml.Schema.SchemaInfo::hasDefaultAttributes
	bool ___hasDefaultAttributes_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Xml.Schema.SchemaInfo::targetNamespaces
	Dictionary_2_t1445386684 * ___targetNamespaces_8;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef> System.Xml.Schema.SchemaInfo::attributeDecls
	Dictionary_2_t2500647674 * ___attributeDecls_9;
	// System.Int32 System.Xml.Schema.SchemaInfo::errorCount
	int32_t ___errorCount_10;
	// System.Xml.Schema.SchemaType System.Xml.Schema.SchemaInfo::schemaType
	int32_t ___schemaType_11;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::elementDeclsByType
	Dictionary_2_t2930592312 * ___elementDeclsByType_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.SchemaNotation> System.Xml.Schema.SchemaInfo::notations
	Dictionary_2_t3998263357 * ___notations_13;

public:
	inline static int32_t get_offset_of_elementDecls_0() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___elementDecls_0)); }
	inline Dictionary_2_t2930592312 * get_elementDecls_0() const { return ___elementDecls_0; }
	inline Dictionary_2_t2930592312 ** get_address_of_elementDecls_0() { return &___elementDecls_0; }
	inline void set_elementDecls_0(Dictionary_2_t2930592312 * value)
	{
		___elementDecls_0 = value;
		Il2CppCodeGenWriteBarrier(&___elementDecls_0, value);
	}

	inline static int32_t get_offset_of_undeclaredElementDecls_1() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___undeclaredElementDecls_1)); }
	inline Dictionary_2_t2930592312 * get_undeclaredElementDecls_1() const { return ___undeclaredElementDecls_1; }
	inline Dictionary_2_t2930592312 ** get_address_of_undeclaredElementDecls_1() { return &___undeclaredElementDecls_1; }
	inline void set_undeclaredElementDecls_1(Dictionary_2_t2930592312 * value)
	{
		___undeclaredElementDecls_1 = value;
		Il2CppCodeGenWriteBarrier(&___undeclaredElementDecls_1, value);
	}

	inline static int32_t get_offset_of_generalEntities_2() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___generalEntities_2)); }
	inline Dictionary_2_t1970389535 * get_generalEntities_2() const { return ___generalEntities_2; }
	inline Dictionary_2_t1970389535 ** get_address_of_generalEntities_2() { return &___generalEntities_2; }
	inline void set_generalEntities_2(Dictionary_2_t1970389535 * value)
	{
		___generalEntities_2 = value;
		Il2CppCodeGenWriteBarrier(&___generalEntities_2, value);
	}

	inline static int32_t get_offset_of_parameterEntities_3() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___parameterEntities_3)); }
	inline Dictionary_2_t1970389535 * get_parameterEntities_3() const { return ___parameterEntities_3; }
	inline Dictionary_2_t1970389535 ** get_address_of_parameterEntities_3() { return &___parameterEntities_3; }
	inline void set_parameterEntities_3(Dictionary_2_t1970389535 * value)
	{
		___parameterEntities_3 = value;
		Il2CppCodeGenWriteBarrier(&___parameterEntities_3, value);
	}

	inline static int32_t get_offset_of_docTypeName_4() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___docTypeName_4)); }
	inline XmlQualifiedName_t1944712516 * get_docTypeName_4() const { return ___docTypeName_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_docTypeName_4() { return &___docTypeName_4; }
	inline void set_docTypeName_4(XmlQualifiedName_t1944712516 * value)
	{
		___docTypeName_4 = value;
		Il2CppCodeGenWriteBarrier(&___docTypeName_4, value);
	}

	inline static int32_t get_offset_of_internalDtdSubset_5() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___internalDtdSubset_5)); }
	inline String_t* get_internalDtdSubset_5() const { return ___internalDtdSubset_5; }
	inline String_t** get_address_of_internalDtdSubset_5() { return &___internalDtdSubset_5; }
	inline void set_internalDtdSubset_5(String_t* value)
	{
		___internalDtdSubset_5 = value;
		Il2CppCodeGenWriteBarrier(&___internalDtdSubset_5, value);
	}

	inline static int32_t get_offset_of_hasNonCDataAttributes_6() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___hasNonCDataAttributes_6)); }
	inline bool get_hasNonCDataAttributes_6() const { return ___hasNonCDataAttributes_6; }
	inline bool* get_address_of_hasNonCDataAttributes_6() { return &___hasNonCDataAttributes_6; }
	inline void set_hasNonCDataAttributes_6(bool value)
	{
		___hasNonCDataAttributes_6 = value;
	}

	inline static int32_t get_offset_of_hasDefaultAttributes_7() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___hasDefaultAttributes_7)); }
	inline bool get_hasDefaultAttributes_7() const { return ___hasDefaultAttributes_7; }
	inline bool* get_address_of_hasDefaultAttributes_7() { return &___hasDefaultAttributes_7; }
	inline void set_hasDefaultAttributes_7(bool value)
	{
		___hasDefaultAttributes_7 = value;
	}

	inline static int32_t get_offset_of_targetNamespaces_8() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___targetNamespaces_8)); }
	inline Dictionary_2_t1445386684 * get_targetNamespaces_8() const { return ___targetNamespaces_8; }
	inline Dictionary_2_t1445386684 ** get_address_of_targetNamespaces_8() { return &___targetNamespaces_8; }
	inline void set_targetNamespaces_8(Dictionary_2_t1445386684 * value)
	{
		___targetNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier(&___targetNamespaces_8, value);
	}

	inline static int32_t get_offset_of_attributeDecls_9() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___attributeDecls_9)); }
	inline Dictionary_2_t2500647674 * get_attributeDecls_9() const { return ___attributeDecls_9; }
	inline Dictionary_2_t2500647674 ** get_address_of_attributeDecls_9() { return &___attributeDecls_9; }
	inline void set_attributeDecls_9(Dictionary_2_t2500647674 * value)
	{
		___attributeDecls_9 = value;
		Il2CppCodeGenWriteBarrier(&___attributeDecls_9, value);
	}

	inline static int32_t get_offset_of_errorCount_10() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___errorCount_10)); }
	inline int32_t get_errorCount_10() const { return ___errorCount_10; }
	inline int32_t* get_address_of_errorCount_10() { return &___errorCount_10; }
	inline void set_errorCount_10(int32_t value)
	{
		___errorCount_10 = value;
	}

	inline static int32_t get_offset_of_schemaType_11() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___schemaType_11)); }
	inline int32_t get_schemaType_11() const { return ___schemaType_11; }
	inline int32_t* get_address_of_schemaType_11() { return &___schemaType_11; }
	inline void set_schemaType_11(int32_t value)
	{
		___schemaType_11 = value;
	}

	inline static int32_t get_offset_of_elementDeclsByType_12() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___elementDeclsByType_12)); }
	inline Dictionary_2_t2930592312 * get_elementDeclsByType_12() const { return ___elementDeclsByType_12; }
	inline Dictionary_2_t2930592312 ** get_address_of_elementDeclsByType_12() { return &___elementDeclsByType_12; }
	inline void set_elementDeclsByType_12(Dictionary_2_t2930592312 * value)
	{
		___elementDeclsByType_12 = value;
		Il2CppCodeGenWriteBarrier(&___elementDeclsByType_12, value);
	}

	inline static int32_t get_offset_of_notations_13() { return static_cast<int32_t>(offsetof(SchemaInfo_t87206461, ___notations_13)); }
	inline Dictionary_2_t3998263357 * get_notations_13() const { return ___notations_13; }
	inline Dictionary_2_t3998263357 ** get_address_of_notations_13() { return &___notations_13; }
	inline void set_notations_13(Dictionary_2_t3998263357 * value)
	{
		___notations_13 = value;
		Il2CppCodeGenWriteBarrier(&___notations_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
