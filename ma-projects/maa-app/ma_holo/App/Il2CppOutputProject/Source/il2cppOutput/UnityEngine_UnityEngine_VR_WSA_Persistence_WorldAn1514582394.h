﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.VR.WSA.Persistence.WorldAnchorStore
struct WorldAnchorStore_t1514582394;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Persistence.WorldAnchorStore
struct  WorldAnchorStore_t1514582394  : public Il2CppObject
{
public:
	// System.IntPtr UnityEngine.VR.WSA.Persistence.WorldAnchorStore::m_NativePtr
	IntPtr_t ___m_NativePtr_1;

public:
	inline static int32_t get_offset_of_m_NativePtr_1() { return static_cast<int32_t>(offsetof(WorldAnchorStore_t1514582394, ___m_NativePtr_1)); }
	inline IntPtr_t get_m_NativePtr_1() const { return ___m_NativePtr_1; }
	inline IntPtr_t* get_address_of_m_NativePtr_1() { return &___m_NativePtr_1; }
	inline void set_m_NativePtr_1(IntPtr_t value)
	{
		___m_NativePtr_1 = value;
	}
};

struct WorldAnchorStore_t1514582394_StaticFields
{
public:
	// UnityEngine.VR.WSA.Persistence.WorldAnchorStore UnityEngine.VR.WSA.Persistence.WorldAnchorStore::s_Instance
	WorldAnchorStore_t1514582394 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(WorldAnchorStore_t1514582394_StaticFields, ___s_Instance_0)); }
	inline WorldAnchorStore_t1514582394 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline WorldAnchorStore_t1514582394 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(WorldAnchorStore_t1514582394 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
