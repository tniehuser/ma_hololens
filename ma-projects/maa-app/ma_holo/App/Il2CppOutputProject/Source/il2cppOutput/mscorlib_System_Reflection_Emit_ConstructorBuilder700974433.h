﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "mscorlib_System_RuntimeMethodHandle894824333.h"
#include "mscorlib_System_Reflection_MethodAttributes790385034.h"
#include "mscorlib_System_Reflection_MethodImplAttributes1541361196.h"
#include "mscorlib_System_Reflection_CallingConventions1097349142.h"

// System.Reflection.Emit.ILGenerator
struct ILGenerator_t99948092;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t2122994367;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2318378278;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t3643576428;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ConstructorBuilder
struct  ConstructorBuilder_t700974433  : public ConstructorInfo_t2851816542
{
public:
	// System.RuntimeMethodHandle System.Reflection.Emit.ConstructorBuilder::mhandle
	RuntimeMethodHandle_t894824333  ___mhandle_2;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.ConstructorBuilder::ilgen
	ILGenerator_t99948092 * ___ilgen_3;
	// System.Type[] System.Reflection.Emit.ConstructorBuilder::parameters
	TypeU5BU5D_t1664964607* ___parameters_4;
	// System.Reflection.MethodAttributes System.Reflection.Emit.ConstructorBuilder::attrs
	int32_t ___attrs_5;
	// System.Reflection.MethodImplAttributes System.Reflection.Emit.ConstructorBuilder::iattrs
	int32_t ___iattrs_6;
	// System.Int32 System.Reflection.Emit.ConstructorBuilder::table_idx
	int32_t ___table_idx_7;
	// System.Reflection.CallingConventions System.Reflection.Emit.ConstructorBuilder::call_conv
	int32_t ___call_conv_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ConstructorBuilder::type
	TypeBuilder_t3308873219 * ___type_9;
	// System.Reflection.Emit.ParameterBuilder[] System.Reflection.Emit.ConstructorBuilder::pinfo
	ParameterBuilderU5BU5D_t2122994367* ___pinfo_10;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.ConstructorBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_11;
	// System.Boolean System.Reflection.Emit.ConstructorBuilder::init_locals
	bool ___init_locals_12;
	// System.Type[][] System.Reflection.Emit.ConstructorBuilder::paramModReq
	TypeU5BU5DU5BU5D_t2318378278* ___paramModReq_13;
	// System.Type[][] System.Reflection.Emit.ConstructorBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t2318378278* ___paramModOpt_14;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.ConstructorBuilder::permissions
	RefEmitPermissionSetU5BU5D_t3643576428* ___permissions_15;

public:
	inline static int32_t get_offset_of_mhandle_2() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___mhandle_2)); }
	inline RuntimeMethodHandle_t894824333  get_mhandle_2() const { return ___mhandle_2; }
	inline RuntimeMethodHandle_t894824333 * get_address_of_mhandle_2() { return &___mhandle_2; }
	inline void set_mhandle_2(RuntimeMethodHandle_t894824333  value)
	{
		___mhandle_2 = value;
	}

	inline static int32_t get_offset_of_ilgen_3() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___ilgen_3)); }
	inline ILGenerator_t99948092 * get_ilgen_3() const { return ___ilgen_3; }
	inline ILGenerator_t99948092 ** get_address_of_ilgen_3() { return &___ilgen_3; }
	inline void set_ilgen_3(ILGenerator_t99948092 * value)
	{
		___ilgen_3 = value;
		Il2CppCodeGenWriteBarrier(&___ilgen_3, value);
	}

	inline static int32_t get_offset_of_parameters_4() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___parameters_4)); }
	inline TypeU5BU5D_t1664964607* get_parameters_4() const { return ___parameters_4; }
	inline TypeU5BU5D_t1664964607** get_address_of_parameters_4() { return &___parameters_4; }
	inline void set_parameters_4(TypeU5BU5D_t1664964607* value)
	{
		___parameters_4 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_4, value);
	}

	inline static int32_t get_offset_of_attrs_5() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___attrs_5)); }
	inline int32_t get_attrs_5() const { return ___attrs_5; }
	inline int32_t* get_address_of_attrs_5() { return &___attrs_5; }
	inline void set_attrs_5(int32_t value)
	{
		___attrs_5 = value;
	}

	inline static int32_t get_offset_of_iattrs_6() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___iattrs_6)); }
	inline int32_t get_iattrs_6() const { return ___iattrs_6; }
	inline int32_t* get_address_of_iattrs_6() { return &___iattrs_6; }
	inline void set_iattrs_6(int32_t value)
	{
		___iattrs_6 = value;
	}

	inline static int32_t get_offset_of_table_idx_7() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___table_idx_7)); }
	inline int32_t get_table_idx_7() const { return ___table_idx_7; }
	inline int32_t* get_address_of_table_idx_7() { return &___table_idx_7; }
	inline void set_table_idx_7(int32_t value)
	{
		___table_idx_7 = value;
	}

	inline static int32_t get_offset_of_call_conv_8() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___call_conv_8)); }
	inline int32_t get_call_conv_8() const { return ___call_conv_8; }
	inline int32_t* get_address_of_call_conv_8() { return &___call_conv_8; }
	inline void set_call_conv_8(int32_t value)
	{
		___call_conv_8 = value;
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___type_9)); }
	inline TypeBuilder_t3308873219 * get_type_9() const { return ___type_9; }
	inline TypeBuilder_t3308873219 ** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(TypeBuilder_t3308873219 * value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier(&___type_9, value);
	}

	inline static int32_t get_offset_of_pinfo_10() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___pinfo_10)); }
	inline ParameterBuilderU5BU5D_t2122994367* get_pinfo_10() const { return ___pinfo_10; }
	inline ParameterBuilderU5BU5D_t2122994367** get_address_of_pinfo_10() { return &___pinfo_10; }
	inline void set_pinfo_10(ParameterBuilderU5BU5D_t2122994367* value)
	{
		___pinfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___pinfo_10, value);
	}

	inline static int32_t get_offset_of_cattrs_11() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___cattrs_11)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_11() const { return ___cattrs_11; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_11() { return &___cattrs_11; }
	inline void set_cattrs_11(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_11 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_11, value);
	}

	inline static int32_t get_offset_of_init_locals_12() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___init_locals_12)); }
	inline bool get_init_locals_12() const { return ___init_locals_12; }
	inline bool* get_address_of_init_locals_12() { return &___init_locals_12; }
	inline void set_init_locals_12(bool value)
	{
		___init_locals_12 = value;
	}

	inline static int32_t get_offset_of_paramModReq_13() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___paramModReq_13)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModReq_13() const { return ___paramModReq_13; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModReq_13() { return &___paramModReq_13; }
	inline void set_paramModReq_13(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModReq_13 = value;
		Il2CppCodeGenWriteBarrier(&___paramModReq_13, value);
	}

	inline static int32_t get_offset_of_paramModOpt_14() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___paramModOpt_14)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModOpt_14() const { return ___paramModOpt_14; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModOpt_14() { return &___paramModOpt_14; }
	inline void set_paramModOpt_14(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModOpt_14 = value;
		Il2CppCodeGenWriteBarrier(&___paramModOpt_14, value);
	}

	inline static int32_t get_offset_of_permissions_15() { return static_cast<int32_t>(offsetof(ConstructorBuilder_t700974433, ___permissions_15)); }
	inline RefEmitPermissionSetU5BU5D_t3643576428* get_permissions_15() const { return ___permissions_15; }
	inline RefEmitPermissionSetU5BU5D_t3643576428** get_address_of_permissions_15() { return &___permissions_15; }
	inline void set_permissions_15(RefEmitPermissionSetU5BU5D_t3643576428* value)
	{
		___permissions_15 = value;
		Il2CppCodeGenWriteBarrier(&___permissions_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
