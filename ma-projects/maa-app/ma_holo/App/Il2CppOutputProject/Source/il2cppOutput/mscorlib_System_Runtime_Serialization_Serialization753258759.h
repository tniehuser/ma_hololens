﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationException
struct  SerializationException_t753258759  : public SystemException_t3877406272
{
public:

public:
};

struct SerializationException_t753258759_StaticFields
{
public:
	// System.String System.Runtime.Serialization.SerializationException::_nullMessage
	String_t* ____nullMessage_16;

public:
	inline static int32_t get_offset_of__nullMessage_16() { return static_cast<int32_t>(offsetof(SerializationException_t753258759_StaticFields, ____nullMessage_16)); }
	inline String_t* get__nullMessage_16() const { return ____nullMessage_16; }
	inline String_t** get_address_of__nullMessage_16() { return &____nullMessage_16; }
	inline void set__nullMessage_16(String_t* value)
	{
		____nullMessage_16 = value;
		Il2CppCodeGenWriteBarrier(&____nullMessage_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
