﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Threading.Tasks.StackGuard
struct StackGuard_t1348285595;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t4228908881;
// System.Object
struct Il2CppObject;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_t851061742;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t606988207;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_t2561380889;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_t286206222;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Threading.TimerCallback
struct TimerCallback_t1684927372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task
struct  Task_t1843236107  : public Il2CppObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	Il2CppObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	Il2CppObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t3932792796 * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t1843236107 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	Il2CppObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t606988207 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_action_5)); }
	inline Il2CppObject * get_m_action_5() const { return ___m_action_5; }
	inline Il2CppObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(Il2CppObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_action_5, value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_stateObject_6)); }
	inline Il2CppObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline Il2CppObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(Il2CppObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_stateObject_6, value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_taskScheduler_7)); }
	inline TaskScheduler_t3932792796 * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t3932792796 ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t3932792796 * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_taskScheduler_7, value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_parent_8)); }
	inline Task_t1843236107 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t1843236107 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t1843236107 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_parent_8, value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_continuationObject_10)); }
	inline Il2CppObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline Il2CppObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(Il2CppObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_continuationObject_10, value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t1843236107, ___m_contingentProperties_15)); }
	inline ContingentProperties_t606988207 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t606988207 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t606988207 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_contingentProperties_15, value);
	}
};

struct Task_t1843236107_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t4228908881 * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	Il2CppObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_t851061742 * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	Il2CppObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_t2491248677 * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_t2561380889 * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t1843236107 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_t286206222 * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t2287130692 * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t1132419410 * ___s_IsTaskContinuationNullPredicate_21;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::<>f__am$cache0
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache0_22;
	// System.Threading.TimerCallback System.Threading.Tasks.Task::<>f__am$cache1
	TimerCallback_t1684927372 * ___U3CU3Ef__amU24cache1_23;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t4228908881 * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t4228908881 ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t4228908881 * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_factory_3, value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline Il2CppObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline Il2CppObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(Il2CppObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier(&___s_taskCompletionSentinel_11, value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_t851061742 * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_t851061742 ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_t851061742 * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier(&___s_currentActiveTasks_13, value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_activeTasksLock_14)); }
	inline Il2CppObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline Il2CppObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(Il2CppObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier(&___s_activeTasksLock_14, value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_t2491248677 * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_t2491248677 ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_t2491248677 * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier(&___s_taskCancelCallback_16, value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_t2561380889 * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_t2561380889 ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_t2561380889 * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier(&___s_createContingentProperties_17, value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_completedTask_18)); }
	inline Task_t1843236107 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t1843236107 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t1843236107 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier(&___s_completedTask_18, value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_t286206222 * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_t286206222 ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_t286206222 * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier(&___s_IsExceptionObservedByParentPredicate_19, value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t2287130692 * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t2287130692 ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t2287130692 * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier(&___s_ecCallback_20, value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t1132419410 * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t1132419410 ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t1132419410 * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier(&___s_IsTaskContinuationNullPredicate_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_23() { return static_cast<int32_t>(offsetof(Task_t1843236107_StaticFields, ___U3CU3Ef__amU24cache1_23)); }
	inline TimerCallback_t1684927372 * get_U3CU3Ef__amU24cache1_23() const { return ___U3CU3Ef__amU24cache1_23; }
	inline TimerCallback_t1684927372 ** get_address_of_U3CU3Ef__amU24cache1_23() { return &___U3CU3Ef__amU24cache1_23; }
	inline void set_U3CU3Ef__amU24cache1_23(TimerCallback_t1684927372 * value)
	{
		___U3CU3Ef__amU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_23, value);
	}
};

struct Task_t1843236107_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t1843236107 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t1348285595 * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t1843236107_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t1843236107 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t1843236107 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t1843236107 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_currentTask_0, value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t1843236107_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t1348285595 * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t1348285595 ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t1348285595 * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier(&___t_stackGuard_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
