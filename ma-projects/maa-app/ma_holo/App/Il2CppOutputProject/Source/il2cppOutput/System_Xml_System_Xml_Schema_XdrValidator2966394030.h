﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseValidator3557140249.h"

// System.Xml.HWStack
struct HWStack_t738999989;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;
// System.Xml.Schema.Parser
struct Parser_t1940171737;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrValidator
struct  XdrValidator_t2966394030  : public BaseValidator_t3557140249
{
public:
	// System.Xml.HWStack System.Xml.Schema.XdrValidator::validationStack
	HWStack_t738999989 * ___validationStack_15;
	// System.Collections.Hashtable System.Xml.Schema.XdrValidator::attPresence
	Hashtable_t909839986 * ___attPresence_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrValidator::name
	XmlQualifiedName_t1944712516 * ___name_17;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XdrValidator::nsManager
	XmlNamespaceManager_t486731501 * ___nsManager_18;
	// System.Boolean System.Xml.Schema.XdrValidator::isProcessContents
	bool ___isProcessContents_19;
	// System.Collections.Hashtable System.Xml.Schema.XdrValidator::IDs
	Hashtable_t909839986 * ___IDs_20;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XdrValidator::idRefListHead
	IdRefNode_t224554150 * ___idRefListHead_21;
	// System.Xml.Schema.Parser System.Xml.Schema.XdrValidator::inlineSchemaParser
	Parser_t1940171737 * ___inlineSchemaParser_22;

public:
	inline static int32_t get_offset_of_validationStack_15() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___validationStack_15)); }
	inline HWStack_t738999989 * get_validationStack_15() const { return ___validationStack_15; }
	inline HWStack_t738999989 ** get_address_of_validationStack_15() { return &___validationStack_15; }
	inline void set_validationStack_15(HWStack_t738999989 * value)
	{
		___validationStack_15 = value;
		Il2CppCodeGenWriteBarrier(&___validationStack_15, value);
	}

	inline static int32_t get_offset_of_attPresence_16() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___attPresence_16)); }
	inline Hashtable_t909839986 * get_attPresence_16() const { return ___attPresence_16; }
	inline Hashtable_t909839986 ** get_address_of_attPresence_16() { return &___attPresence_16; }
	inline void set_attPresence_16(Hashtable_t909839986 * value)
	{
		___attPresence_16 = value;
		Il2CppCodeGenWriteBarrier(&___attPresence_16, value);
	}

	inline static int32_t get_offset_of_name_17() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___name_17)); }
	inline XmlQualifiedName_t1944712516 * get_name_17() const { return ___name_17; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_17() { return &___name_17; }
	inline void set_name_17(XmlQualifiedName_t1944712516 * value)
	{
		___name_17 = value;
		Il2CppCodeGenWriteBarrier(&___name_17, value);
	}

	inline static int32_t get_offset_of_nsManager_18() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___nsManager_18)); }
	inline XmlNamespaceManager_t486731501 * get_nsManager_18() const { return ___nsManager_18; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsManager_18() { return &___nsManager_18; }
	inline void set_nsManager_18(XmlNamespaceManager_t486731501 * value)
	{
		___nsManager_18 = value;
		Il2CppCodeGenWriteBarrier(&___nsManager_18, value);
	}

	inline static int32_t get_offset_of_isProcessContents_19() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___isProcessContents_19)); }
	inline bool get_isProcessContents_19() const { return ___isProcessContents_19; }
	inline bool* get_address_of_isProcessContents_19() { return &___isProcessContents_19; }
	inline void set_isProcessContents_19(bool value)
	{
		___isProcessContents_19 = value;
	}

	inline static int32_t get_offset_of_IDs_20() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___IDs_20)); }
	inline Hashtable_t909839986 * get_IDs_20() const { return ___IDs_20; }
	inline Hashtable_t909839986 ** get_address_of_IDs_20() { return &___IDs_20; }
	inline void set_IDs_20(Hashtable_t909839986 * value)
	{
		___IDs_20 = value;
		Il2CppCodeGenWriteBarrier(&___IDs_20, value);
	}

	inline static int32_t get_offset_of_idRefListHead_21() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___idRefListHead_21)); }
	inline IdRefNode_t224554150 * get_idRefListHead_21() const { return ___idRefListHead_21; }
	inline IdRefNode_t224554150 ** get_address_of_idRefListHead_21() { return &___idRefListHead_21; }
	inline void set_idRefListHead_21(IdRefNode_t224554150 * value)
	{
		___idRefListHead_21 = value;
		Il2CppCodeGenWriteBarrier(&___idRefListHead_21, value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XdrValidator_t2966394030, ___inlineSchemaParser_22)); }
	inline Parser_t1940171737 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_t1940171737 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_t1940171737 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier(&___inlineSchemaParser_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
