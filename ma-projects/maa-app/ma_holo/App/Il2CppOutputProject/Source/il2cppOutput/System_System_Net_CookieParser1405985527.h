﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.CookieTokenizer
struct CookieTokenizer_t3643404755;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieParser
struct  CookieParser_t1405985527  : public Il2CppObject
{
public:
	// System.Net.CookieTokenizer System.Net.CookieParser::m_tokenizer
	CookieTokenizer_t3643404755 * ___m_tokenizer_0;

public:
	inline static int32_t get_offset_of_m_tokenizer_0() { return static_cast<int32_t>(offsetof(CookieParser_t1405985527, ___m_tokenizer_0)); }
	inline CookieTokenizer_t3643404755 * get_m_tokenizer_0() const { return ___m_tokenizer_0; }
	inline CookieTokenizer_t3643404755 ** get_address_of_m_tokenizer_0() { return &___m_tokenizer_0; }
	inline void set_m_tokenizer_0(CookieTokenizer_t3643404755 * value)
	{
		___m_tokenizer_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tokenizer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
