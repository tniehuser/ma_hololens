﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.EventHandlerList/ListEntry
struct ListEntry_t385037026;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3022476291;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventHandlerList/ListEntry
struct  ListEntry_t385037026  : public Il2CppObject
{
public:
	// System.ComponentModel.EventHandlerList/ListEntry System.ComponentModel.EventHandlerList/ListEntry::next
	ListEntry_t385037026 * ___next_0;
	// System.Object System.ComponentModel.EventHandlerList/ListEntry::key
	Il2CppObject * ___key_1;
	// System.Delegate System.ComponentModel.EventHandlerList/ListEntry::handler
	Delegate_t3022476291 * ___handler_2;

public:
	inline static int32_t get_offset_of_next_0() { return static_cast<int32_t>(offsetof(ListEntry_t385037026, ___next_0)); }
	inline ListEntry_t385037026 * get_next_0() const { return ___next_0; }
	inline ListEntry_t385037026 ** get_address_of_next_0() { return &___next_0; }
	inline void set_next_0(ListEntry_t385037026 * value)
	{
		___next_0 = value;
		Il2CppCodeGenWriteBarrier(&___next_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(ListEntry_t385037026, ___key_1)); }
	inline Il2CppObject * get_key_1() const { return ___key_1; }
	inline Il2CppObject ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(Il2CppObject * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(ListEntry_t385037026, ___handler_2)); }
	inline Delegate_t3022476291 * get_handler_2() const { return ___handler_2; }
	inline Delegate_t3022476291 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Delegate_t3022476291 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier(&___handler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
