﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_Component2826673791.h"

// System.String
struct String_t;
// System.Diagnostics.EventLogImpl
struct EventLogImpl_t3456077238;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.EventLog
struct  EventLog_t681067562  : public Component_t2826673791
{
public:
	// System.String System.Diagnostics.EventLog::source
	String_t* ___source_4;
	// System.Boolean System.Diagnostics.EventLog::doRaiseEvents
	bool ___doRaiseEvents_5;
	// System.Diagnostics.EventLogImpl System.Diagnostics.EventLog::Impl
	EventLogImpl_t3456077238 * ___Impl_6;

public:
	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(EventLog_t681067562, ___source_4)); }
	inline String_t* get_source_4() const { return ___source_4; }
	inline String_t** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(String_t* value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier(&___source_4, value);
	}

	inline static int32_t get_offset_of_doRaiseEvents_5() { return static_cast<int32_t>(offsetof(EventLog_t681067562, ___doRaiseEvents_5)); }
	inline bool get_doRaiseEvents_5() const { return ___doRaiseEvents_5; }
	inline bool* get_address_of_doRaiseEvents_5() { return &___doRaiseEvents_5; }
	inline void set_doRaiseEvents_5(bool value)
	{
		___doRaiseEvents_5 = value;
	}

	inline static int32_t get_offset_of_Impl_6() { return static_cast<int32_t>(offsetof(EventLog_t681067562, ___Impl_6)); }
	inline EventLogImpl_t3456077238 * get_Impl_6() const { return ___Impl_6; }
	inline EventLogImpl_t3456077238 ** get_address_of_Impl_6() { return &___Impl_6; }
	inline void set_Impl_6(EventLogImpl_t3456077238 * value)
	{
		___Impl_6 = value;
		Il2CppCodeGenWriteBarrier(&___Impl_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
