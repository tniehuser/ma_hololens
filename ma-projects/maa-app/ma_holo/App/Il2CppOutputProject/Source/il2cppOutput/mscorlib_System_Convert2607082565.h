﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.RuntimeType[]
struct RuntimeTypeU5BU5D_t2680569683;
// System.RuntimeType
struct RuntimeType_t2836228502;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Convert
struct  Convert_t2607082565  : public Il2CppObject
{
public:

public:
};

struct Convert_t2607082565_StaticFields
{
public:
	// System.RuntimeType[] System.Convert::ConvertTypes
	RuntimeTypeU5BU5D_t2680569683* ___ConvertTypes_0;
	// System.RuntimeType System.Convert::EnumType
	RuntimeType_t2836228502 * ___EnumType_1;
	// System.Char[] System.Convert::base64Table
	CharU5BU5D_t1328083999* ___base64Table_2;
	// System.Object System.Convert::DBNull
	Il2CppObject * ___DBNull_3;

public:
	inline static int32_t get_offset_of_ConvertTypes_0() { return static_cast<int32_t>(offsetof(Convert_t2607082565_StaticFields, ___ConvertTypes_0)); }
	inline RuntimeTypeU5BU5D_t2680569683* get_ConvertTypes_0() const { return ___ConvertTypes_0; }
	inline RuntimeTypeU5BU5D_t2680569683** get_address_of_ConvertTypes_0() { return &___ConvertTypes_0; }
	inline void set_ConvertTypes_0(RuntimeTypeU5BU5D_t2680569683* value)
	{
		___ConvertTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___ConvertTypes_0, value);
	}

	inline static int32_t get_offset_of_EnumType_1() { return static_cast<int32_t>(offsetof(Convert_t2607082565_StaticFields, ___EnumType_1)); }
	inline RuntimeType_t2836228502 * get_EnumType_1() const { return ___EnumType_1; }
	inline RuntimeType_t2836228502 ** get_address_of_EnumType_1() { return &___EnumType_1; }
	inline void set_EnumType_1(RuntimeType_t2836228502 * value)
	{
		___EnumType_1 = value;
		Il2CppCodeGenWriteBarrier(&___EnumType_1, value);
	}

	inline static int32_t get_offset_of_base64Table_2() { return static_cast<int32_t>(offsetof(Convert_t2607082565_StaticFields, ___base64Table_2)); }
	inline CharU5BU5D_t1328083999* get_base64Table_2() const { return ___base64Table_2; }
	inline CharU5BU5D_t1328083999** get_address_of_base64Table_2() { return &___base64Table_2; }
	inline void set_base64Table_2(CharU5BU5D_t1328083999* value)
	{
		___base64Table_2 = value;
		Il2CppCodeGenWriteBarrier(&___base64Table_2, value);
	}

	inline static int32_t get_offset_of_DBNull_3() { return static_cast<int32_t>(offsetof(Convert_t2607082565_StaticFields, ___DBNull_3)); }
	inline Il2CppObject * get_DBNull_3() const { return ___DBNull_3; }
	inline Il2CppObject ** get_address_of_DBNull_3() { return &___DBNull_3; }
	inline void set_DBNull_3(Il2CppObject * value)
	{
		___DBNull_3 = value;
		Il2CppCodeGenWriteBarrier(&___DBNull_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
