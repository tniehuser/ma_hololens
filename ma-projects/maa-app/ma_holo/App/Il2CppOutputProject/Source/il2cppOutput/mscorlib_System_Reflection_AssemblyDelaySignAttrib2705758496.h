﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyDelaySignAttribute
struct  AssemblyDelaySignAttribute_t2705758496  : public Attribute_t542643598
{
public:
	// System.Boolean System.Reflection.AssemblyDelaySignAttribute::m_delaySign
	bool ___m_delaySign_0;

public:
	inline static int32_t get_offset_of_m_delaySign_0() { return static_cast<int32_t>(offsetof(AssemblyDelaySignAttribute_t2705758496, ___m_delaySign_0)); }
	inline bool get_m_delaySign_0() const { return ___m_delaySign_0; }
	inline bool* get_address_of_m_delaySign_0() { return &___m_delaySign_0; }
	inline void set_m_delaySign_0(bool value)
	{
		___m_delaySign_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
