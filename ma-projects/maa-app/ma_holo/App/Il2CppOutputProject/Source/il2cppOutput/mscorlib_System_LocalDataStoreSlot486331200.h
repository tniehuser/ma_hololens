﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1152954092;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalDataStoreSlot
struct  LocalDataStoreSlot_t486331200  : public Il2CppObject
{
public:
	// System.LocalDataStoreMgr System.LocalDataStoreSlot::m_mgr
	LocalDataStoreMgr_t1152954092 * ___m_mgr_0;
	// System.Int32 System.LocalDataStoreSlot::m_slot
	int32_t ___m_slot_1;
	// System.Int64 System.LocalDataStoreSlot::m_cookie
	int64_t ___m_cookie_2;

public:
	inline static int32_t get_offset_of_m_mgr_0() { return static_cast<int32_t>(offsetof(LocalDataStoreSlot_t486331200, ___m_mgr_0)); }
	inline LocalDataStoreMgr_t1152954092 * get_m_mgr_0() const { return ___m_mgr_0; }
	inline LocalDataStoreMgr_t1152954092 ** get_address_of_m_mgr_0() { return &___m_mgr_0; }
	inline void set_m_mgr_0(LocalDataStoreMgr_t1152954092 * value)
	{
		___m_mgr_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_mgr_0, value);
	}

	inline static int32_t get_offset_of_m_slot_1() { return static_cast<int32_t>(offsetof(LocalDataStoreSlot_t486331200, ___m_slot_1)); }
	inline int32_t get_m_slot_1() const { return ___m_slot_1; }
	inline int32_t* get_address_of_m_slot_1() { return &___m_slot_1; }
	inline void set_m_slot_1(int32_t value)
	{
		___m_slot_1 = value;
	}

	inline static int32_t get_offset_of_m_cookie_2() { return static_cast<int32_t>(offsetof(LocalDataStoreSlot_t486331200, ___m_cookie_2)); }
	inline int64_t get_m_cookie_2() const { return ___m_cookie_2; }
	inline int64_t* get_address_of_m_cookie_2() { return &___m_cookie_2; }
	inline void set_m_cookie_2(int64_t value)
	{
		___m_cookie_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
