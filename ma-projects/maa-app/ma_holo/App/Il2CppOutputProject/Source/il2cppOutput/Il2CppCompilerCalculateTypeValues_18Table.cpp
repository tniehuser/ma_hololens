﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_sockaddr_in62899168071.h"
#include "System_System_Net_NetworkInformation_in6_addr4035827331.h"
#include "System_System_Net_NetworkInformation_sockaddr_ll1681025498.h"
#include "System_System_Net_NetworkInformation_LinuxArpHardw4257672401.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_i937751619.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_3329842375.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_2792443317.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_1014645363.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_s834146887.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_3955242742.h"
#include "System_System_Net_NetworkInformation_MacOsArpHardwa650784048.h"
#include "System_System_Net_NetworkInformation_SystemNetwork1719461838.h"
#include "System_System_Net_NetworkInformation_NetworkInterf2383622021.h"
#include "System_System_Net_NetworkInformation_NetworkInterf2424465329.h"
#include "System_System_Net_NetworkInformation_NetworkInterfa491877318.h"
#include "System_System_Net_NetworkInformation_NetworkInterfa950011009.h"
#include "System_System_Net_NetworkInformation_NetworkInterfa393399220.h"
#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1454185290.h"
#include "System_System_Net_NetworkInformation_Win32NetworkIn482839970.h"
#include "System_System_Net_NetworkInformation_Win32_FIXED_I1371335919.h"
#include "System_System_Net_NetworkInformation_AlignmentUnion707470070.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAPT680756680.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADDR2646152127.h"
#include "System_System_Net_NetworkInformation_Win32LengthFl1910118479.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP2512883708.h"
#include "System_System_Net_NetworkInformation_Win32_SOCKADD1606057231.h"
#include "System_System_Net_NetworkInformation_Win32_SOCKET_1093973396.h"
#include "System_System_Net_Security_AuthenticatedStream1183414097.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Net_Security_LocalCertSelectionCallb2279727292.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Net_ServicePoint2765344313.h"
#include "System_System_Net_ServicePointManager745663000.h"
#include "System_System_Net_ServicePointManager_SPKey1552752485.h"
#include "System_System_Net_SimpleAsyncCallback3151114241.h"
#include "System_System_Net_SimpleAsyncResult2937691397.h"
#include "System_System_Net_SimpleAsyncResult_U3CSimpleAsync2576756614.h"
#include "System_System_Net_SimpleAsyncResult_U3CRunWithLockU660223254.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_IOControlCode1541062490.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"
#include "System_System_Net_Sockets_SafeSocketHandle1628787412.h"
#include "System_System_Net_Sockets_SelectMode3413969319.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_Socket_WSABUF2199312694.h"
#include "System_System_Net_Sockets_Socket_U3CBeginMConnectU3586789679.h"
#include "System_System_Net_Sockets_Socket_U3CBeginSendCallba652620410.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs2815111766.h"
#include "System_System_Net_Sockets_SocketAsyncResult3950677696.h"
#include "System_System_Net_Sockets_SocketAsyncResult_U3CCom3411761453.h"
#include "System_System_Net_Sockets_SocketError307542793.h"
#include "System_System_Net_Sockets_SocketException1618573604.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"
#include "System_System_Net_Sockets_SocketOperation3807127992.h"
#include "System_System_Net_Sockets_SocketOptionLevel1505247880.h"
#include "System_System_Net_Sockets_SocketOptionName1089121285.h"
#include "System_System_Net_Sockets_SocketShutdown3247039417.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_WebAsyncResult905414499.h"
#include "System_System_Net_ReadState657568301.h"
#include "System_System_Net_WebConnection324679648.h"
#include "System_System_Net_WebConnection_NtlmAuthState1285319885.h"
#include "System_System_Net_WebConnection_AbortHelper2895113041.h"
#include "System_System_Net_WebConnectionData3550260432.h"
#include "System_System_Net_WebConnectionGroup3242458773.h"
#include "System_System_Net_WebConnectionGroup_ConnectionSta2608615043.h"
#include "System_System_Net_WebConnectionStream1922483508.h"
#include "System_System_Net_WebConnectionStream_U3CSetHeader3819940672.h"
#include "System_System_Net_WebConnectionStream_U3CSetHeader1091057317.h"
#include "System_System_Net_WebConnectionStream_U3CWriteReque510420496.h"
#include "System_System_Security_Authentication_SslProtocols894678499.h"
#include "System_System_Security_Cryptography_AsnDecodeStatu1962003286.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "System_System_Security_Cryptography_X509Certificat2370524385.h"
#include "System_System_Security_Cryptography_X509Certificate384932784.h"
#include "System_System_Security_Cryptography_X509Certificat1335602280.h"
#include "System_System_Security_Cryptography_X509Certificates_P870392.h"
#include "System_System_Security_Cryptography_X509Certificat1570828128.h"
#include "System_System_Security_Cryptography_X509Certificat2183514610.h"
#include "System_System_Security_Cryptography_X509Certificate452415348.h"
#include "System_System_Security_Cryptography_X509Certificat2005802885.h"
#include "System_System_Security_Cryptography_X509Certificat1562873317.h"
#include "System_System_Security_Cryptography_X509Certificat1108969367.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "System_System_Security_Cryptography_X509Certificat2356134957.h"
#include "System_System_Security_Cryptography_X509Certificat2703153821.h"
#include "System_System_Security_Cryptography_X509Certificat2009068154.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "System_System_Security_Cryptography_X509Certificat1208230922.h"
#include "System_System_Security_Cryptography_X509Certificate255811311.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Security_Cryptography_X509Certificat2081831987.h"
#include "System_System_Security_Cryptography_X509Certificate528874471.h"
#include "System_System_Security_Cryptography_X509Certificat3304975821.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (sockaddr_in6_t2899168071)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t2899168071_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	sockaddr_in6_t2899168071::get_offset_of_sin6_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_flowinfo_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_scope_id_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (in6_addr_t4035827331)+ sizeof (Il2CppObject), sizeof(in6_addr_t4035827331_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	in6_addr_t4035827331::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (sockaddr_ll_t1681025498)+ sizeof (Il2CppObject), sizeof(sockaddr_ll_t1681025498_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1802[7] = 
{
	sockaddr_ll_t1681025498::get_offset_of_sll_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_protocol_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_ifindex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_hatype_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_pkttype_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_halen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_addr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (LinuxArpHardware_t4257672401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[18] = 
{
	LinuxArpHardware_t4257672401::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ifaddrs_t937751619)+ sizeof (Il2CppObject), sizeof(ifaddrs_t937751619_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[7] = 
{
	ifaddrs_t937751619::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_dstaddr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (sockaddr_t3329842375)+ sizeof (Il2CppObject), sizeof(sockaddr_t3329842375 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	sockaddr_t3329842375::get_offset_of_sa_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_t3329842375::get_offset_of_sa_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (sockaddr_in_t2792443317)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t2792443317 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[4] = 
{
	sockaddr_in_t2792443317::get_offset_of_sin_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (in6_addr_t1014645363)+ sizeof (Il2CppObject), sizeof(in6_addr_t1014645363_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	in6_addr_t1014645363::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (sockaddr_in6_t834146887)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t834146887_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[6] = 
{
	sockaddr_in6_t834146887::get_offset_of_sin6_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_flowinfo_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_addr_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_scope_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (sockaddr_dl_t3955242742)+ sizeof (Il2CppObject), sizeof(sockaddr_dl_t3955242742_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[8] = 
{
	sockaddr_dl_t3955242742::get_offset_of_sdl_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_index_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_type_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_nlen_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_alen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_slen_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_data_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (MacOsArpHardware_t650784048)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[7] = 
{
	MacOsArpHardware_t650784048::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (SystemNetworkInterface_t1719461838), -1, sizeof(SystemNetworkInterface_t1719461838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	SystemNetworkInterface_t1719461838_StaticFields::get_offset_of_nif_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (NetworkInterfaceFactory_t2383622021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (UnixNetworkInterfaceAPI_t2424465329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (MacOsNetworkInterfaceAPI_t491877318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (LinuxNetworkInterfaceAPI_t950011009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Win32NetworkInterfaceAPI_t393399220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (UnixNetworkInterface_t1000704527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	UnixNetworkInterface_t1000704527::get_offset_of_ipproperties_0(),
	UnixNetworkInterface_t1000704527::get_offset_of_name_1(),
	UnixNetworkInterface_t1000704527::get_offset_of_addresses_2(),
	UnixNetworkInterface_t1000704527::get_offset_of_macAddress_3(),
	UnixNetworkInterface_t1000704527::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (LinuxNetworkInterface_t3864470295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_path_5(),
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_operstate_path_6(),
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_flags_path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (MacOsNetworkInterface_t1454185290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	MacOsNetworkInterface_t1454185290::get_offset_of__ifa_flags_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (Win32NetworkInterface2_t482839970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[5] = 
{
	Win32NetworkInterface2_t482839970::get_offset_of_addr_0(),
	Win32NetworkInterface2_t482839970::get_offset_of_mib4_1(),
	Win32NetworkInterface2_t482839970::get_offset_of_mib6_2(),
	Win32NetworkInterface2_t482839970::get_offset_of_ip4stats_3(),
	Win32NetworkInterface2_t482839970::get_offset_of_ip_if_props_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (Win32_FIXED_INFO_t1371335919), sizeof(Win32_FIXED_INFO_t1371335919_marshaled_pinvoke), sizeof(Win32_FIXED_INFO_t1371335919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1821[10] = 
{
	Win32_FIXED_INFO_t1371335919_StaticFields::get_offset_of_fixed_info_0(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_HostName_1(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_DomainName_2(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_CurrentDnsServer_3(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_DnsServerList_4(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_NodeType_5(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_ScopeId_6(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_EnableRouting_7(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_EnableProxy_8(),
	Win32_FIXED_INFO_t1371335919::get_offset_of_EnableDns_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (AlignmentUnion_t707470070)+ sizeof (Il2CppObject), sizeof(AlignmentUnion_t707470070 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[3] = 
{
	AlignmentUnion_t707470070::get_offset_of_Alignment_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t707470070::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t707470070::get_offset_of_IfIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (Win32_IP_ADAPTER_ADDRESSES_t680756680), sizeof(Win32_IP_ADAPTER_ADDRESSES_t680756680_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[18] = 
{
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Alignment_0(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Next_1(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_AdapterName_2(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstUnicastAddress_3(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstAnycastAddress_4(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstMulticastAddress_5(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstDnsServerAddress_6(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_DnsSuffix_7(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Description_8(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FriendlyName_9(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_PhysicalAddress_10(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_PhysicalAddressLength_11(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Flags_12(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Mtu_13(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_IfType_14(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_OperStatus_15(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Ipv6IfIndex_16(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_ZoneIndices_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Win32_MIB_IFROW_t4215928996)+ sizeof (Il2CppObject), sizeof(Win32_MIB_IFROW_t4215928996_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[24] = 
{
	Win32_MIB_IFROW_t4215928996::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Mtu_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Speed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_PhysAddrLen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_PhysAddr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_AdminStatus_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OperStatus_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_LastChange_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InOctets_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InUcastPkts_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InNUcastPkts_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InDiscards_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InErrors_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InUnknownProtos_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutOctets_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutUcastPkts_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutNUcastPkts_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutDiscards_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutErrors_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutQLen_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_DescrLen_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Descr_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Win32_IP_ADDR_STRING_t2646152127)+ sizeof (Il2CppObject), sizeof(Win32_IP_ADDR_STRING_t2646152127_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	Win32_IP_ADDR_STRING_t2646152127::get_offset_of_Next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_IP_ADDR_STRING_t2646152127::get_offset_of_IpAddress_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_IP_ADDR_STRING_t2646152127::get_offset_of_IpMask_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_IP_ADDR_STRING_t2646152127::get_offset_of_Context_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Win32LengthFlagsUnion_t1910118479)+ sizeof (Il2CppObject), sizeof(Win32LengthFlagsUnion_t1910118479 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[2] = 
{
	Win32LengthFlagsUnion_t1910118479::get_offset_of_Length_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32LengthFlagsUnion_t1910118479::get_offset_of_Flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (Win32_IP_ADAPTER_DNS_SERVER_ADDRESS_t2512883708)+ sizeof (Il2CppObject), sizeof(Win32_IP_ADAPTER_DNS_SERVER_ADDRESS_t2512883708 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[3] = 
{
	Win32_IP_ADAPTER_DNS_SERVER_ADDRESS_t2512883708::get_offset_of_LengthFlags_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_IP_ADAPTER_DNS_SERVER_ADDRESS_t2512883708::get_offset_of_Next_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_IP_ADAPTER_DNS_SERVER_ADDRESS_t2512883708::get_offset_of_Address_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (Win32_SOCKADDR_t1606057231)+ sizeof (Il2CppObject), sizeof(Win32_SOCKADDR_t1606057231_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	Win32_SOCKADDR_t1606057231::get_offset_of_AddressFamily_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_SOCKADDR_t1606057231::get_offset_of_AddressData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Win32_SOCKET_ADDRESS_t1093973396)+ sizeof (Il2CppObject), sizeof(Win32_SOCKET_ADDRESS_t1093973396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[2] = 
{
	Win32_SOCKET_ADDRESS_t1093973396::get_offset_of_Sockaddr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_SOCKET_ADDRESS_t1093973396::get_offset_of_SockaddrLength_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (AuthenticatedStream_t1183414097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[2] = 
{
	AuthenticatedStream_t1183414097::get_offset_of__InnerStream_8(),
	AuthenticatedStream_t1183414097::get_offset_of__LeaveStreamOpen_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (AuthenticationLevel_t2424130044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1831[4] = 
{
	AuthenticationLevel_t2424130044::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (LocalCertSelectionCallback_t2279727292), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (SslPolicyErrors_t1928581431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1834[5] = 
{
	SslPolicyErrors_t1928581431::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (ServicePoint_t2765344313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[21] = 
{
	ServicePoint_t2765344313::get_offset_of_uri_0(),
	ServicePoint_t2765344313::get_offset_of_connectionLimit_1(),
	ServicePoint_t2765344313::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2765344313::get_offset_of_currentConnections_3(),
	ServicePoint_t2765344313::get_offset_of_idleSince_4(),
	ServicePoint_t2765344313::get_offset_of_lastDnsResolve_5(),
	ServicePoint_t2765344313::get_offset_of_protocolVersion_6(),
	ServicePoint_t2765344313::get_offset_of_host_7(),
	ServicePoint_t2765344313::get_offset_of_usesProxy_8(),
	ServicePoint_t2765344313::get_offset_of_groups_9(),
	ServicePoint_t2765344313::get_offset_of_sendContinue_10(),
	ServicePoint_t2765344313::get_offset_of_useConnect_11(),
	ServicePoint_t2765344313::get_offset_of_hostE_12(),
	ServicePoint_t2765344313::get_offset_of_useNagle_13(),
	ServicePoint_t2765344313::get_offset_of_endPointCallback_14(),
	ServicePoint_t2765344313::get_offset_of_tcp_keepalive_15(),
	ServicePoint_t2765344313::get_offset_of_tcp_keepalive_time_16(),
	ServicePoint_t2765344313::get_offset_of_tcp_keepalive_interval_17(),
	ServicePoint_t2765344313::get_offset_of_idleTimer_18(),
	ServicePoint_t2765344313::get_offset_of_m_ServerCertificateOrBytes_19(),
	ServicePoint_t2765344313::get_offset_of_m_ClientCertificateOrBytes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (ServicePointManager_t745663000), -1, sizeof(ServicePointManager_t745663000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[15] = 
{
	ServicePointManager_t745663000_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_dnsRefreshTimeout_5(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__checkCRL_6(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__securityProtocol_7(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_expectContinue_8(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_useNagle_9(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_server_cert_cb_10(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_tcp_keepalive_11(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_tcp_keepalive_time_12(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_tcp_keepalive_interval_13(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_manager_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (SPKey_t1552752485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[3] = 
{
	SPKey_t1552752485::get_offset_of_uri_0(),
	SPKey_t1552752485::get_offset_of_proxy_1(),
	SPKey_t1552752485::get_offset_of_use_connect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (SimpleAsyncCallback_t3151114241), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (SimpleAsyncResult_t2937691397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[9] = 
{
	SimpleAsyncResult_t2937691397::get_offset_of_handle_0(),
	SimpleAsyncResult_t2937691397::get_offset_of_synch_1(),
	SimpleAsyncResult_t2937691397::get_offset_of_isCompleted_2(),
	SimpleAsyncResult_t2937691397::get_offset_of_cb_3(),
	SimpleAsyncResult_t2937691397::get_offset_of_state_4(),
	SimpleAsyncResult_t2937691397::get_offset_of_callbackDone_5(),
	SimpleAsyncResult_t2937691397::get_offset_of_exc_6(),
	SimpleAsyncResult_t2937691397::get_offset_of_locker_7(),
	SimpleAsyncResult_t2937691397::get_offset_of_user_read_synch_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614::get_offset_of_cb_0(),
	U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U3CRunWithLockU3Ec__AnonStorey1_t660223254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[3] = 
{
	U3CRunWithLockU3Ec__AnonStorey1_t660223254::get_offset_of_func_0(),
	U3CRunWithLockU3Ec__AnonStorey1_t660223254::get_offset_of_locker_1(),
	U3CRunWithLockU3Ec__AnonStorey1_t660223254::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (IOControlCode_t1541062490)+ sizeof (Il2CppObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1843[35] = 
{
	IOControlCode_t1541062490::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_lingerTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (ProtocolType_t2178963134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1846[26] = 
{
	ProtocolType_t2178963134::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (SafeSocketHandle_t1628787412), sizeof(void*), sizeof(SafeSocketHandle_t1628787412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1847[4] = 
{
	SafeSocketHandle_t1628787412::get_offset_of_blocking_threads_4(),
	SafeSocketHandle_t1628787412::get_offset_of_threads_stacktraces_5(),
	SafeSocketHandle_t1628787412::get_offset_of_in_cleanup_6(),
	SafeSocketHandle_t1628787412_StaticFields::get_offset_of_THROW_ON_ABORT_RETRIES_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (SelectMode_t3413969319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[4] = 
{
	SelectMode_t3413969319::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (Socket_t3821512045), -1, sizeof(Socket_t3821512045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[33] = 
{
	Socket_t3821512045_StaticFields::get_offset_of_ipv4_supported_0(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv6_supported_1(),
	Socket_t3821512045::get_offset_of_is_closed_2(),
	Socket_t3821512045::get_offset_of_is_listening_3(),
	Socket_t3821512045::get_offset_of_linger_timeout_4(),
	Socket_t3821512045::get_offset_of_address_family_5(),
	Socket_t3821512045::get_offset_of_socket_type_6(),
	Socket_t3821512045::get_offset_of_protocol_type_7(),
	Socket_t3821512045::get_offset_of_safe_handle_8(),
	Socket_t3821512045::get_offset_of_seed_endpoint_9(),
	Socket_t3821512045::get_offset_of_readQ_10(),
	Socket_t3821512045::get_offset_of_writeQ_11(),
	Socket_t3821512045::get_offset_of_is_blocking_12(),
	Socket_t3821512045::get_offset_of_is_bound_13(),
	Socket_t3821512045::get_offset_of_is_connected_14(),
	Socket_t3821512045::get_offset_of_is_disposed_15(),
	Socket_t3821512045::get_offset_of_connect_in_progress_16(),
	Socket_t3821512045_StaticFields::get_offset_of_AcceptAsyncCallback_17(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginAcceptCallback_18(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginAcceptReceiveCallback_19(),
	Socket_t3821512045_StaticFields::get_offset_of_ConnectAsyncCallback_20(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginConnectCallback_21(),
	Socket_t3821512045_StaticFields::get_offset_of_DisconnectAsyncCallback_22(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginDisconnectCallback_23(),
	Socket_t3821512045_StaticFields::get_offset_of_ReceiveAsyncCallback_24(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginReceiveCallback_25(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginReceiveGenericCallback_26(),
	Socket_t3821512045_StaticFields::get_offset_of_ReceiveFromAsyncCallback_27(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginReceiveFromCallback_28(),
	Socket_t3821512045_StaticFields::get_offset_of_SendAsyncCallback_29(),
	Socket_t3821512045_StaticFields::get_offset_of_BeginSendGenericCallback_30(),
	Socket_t3821512045_StaticFields::get_offset_of_SendToAsyncCallback_31(),
	Socket_t3821512045_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (WSABUF_t2199312694)+ sizeof (Il2CppObject), sizeof(WSABUF_t2199312694 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	WSABUF_t2199312694::get_offset_of_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WSABUF_t2199312694::get_offset_of_buf_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (U3CBeginMConnectU3Ec__AnonStorey0_t586789679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	U3CBeginMConnectU3Ec__AnonStorey0_t586789679::get_offset_of_callback_0(),
	U3CBeginMConnectU3Ec__AnonStorey0_t586789679::get_offset_of_ares_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (U3CBeginSendCallbackU3Ec__AnonStorey1_t652620410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	U3CBeginSendCallbackU3Ec__AnonStorey1_t652620410::get_offset_of_sent_so_far_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (SocketAsyncEventArgs_t2815111766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[8] = 
{
	SocketAsyncEventArgs_t2815111766::get_offset_of_disposed_1(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_in_progress_2(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_remote_ep_3(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_current_socket_4(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_U3CAcceptSocketU3Ek__BackingField_5(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_U3CBytesTransferredU3Ek__BackingField_6(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_U3CSocketErrorU3Ek__BackingField_7(),
	SocketAsyncEventArgs_t2815111766::get_offset_of_Completed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (SocketAsyncResult_t3950677696), -1, sizeof(SocketAsyncResult_t3950677696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[19] = 
{
	SocketAsyncResult_t3950677696::get_offset_of_socket_5(),
	SocketAsyncResult_t3950677696::get_offset_of_operation_6(),
	SocketAsyncResult_t3950677696::get_offset_of_DelayedException_7(),
	SocketAsyncResult_t3950677696::get_offset_of_EndPoint_8(),
	SocketAsyncResult_t3950677696::get_offset_of_Buffer_9(),
	SocketAsyncResult_t3950677696::get_offset_of_Offset_10(),
	SocketAsyncResult_t3950677696::get_offset_of_Size_11(),
	SocketAsyncResult_t3950677696::get_offset_of_SockFlags_12(),
	SocketAsyncResult_t3950677696::get_offset_of_AcceptSocket_13(),
	SocketAsyncResult_t3950677696::get_offset_of_Addresses_14(),
	SocketAsyncResult_t3950677696::get_offset_of_Port_15(),
	SocketAsyncResult_t3950677696::get_offset_of_Buffers_16(),
	SocketAsyncResult_t3950677696::get_offset_of_ReuseSocket_17(),
	SocketAsyncResult_t3950677696::get_offset_of_CurrentAddress_18(),
	SocketAsyncResult_t3950677696::get_offset_of_AcceptedSocket_19(),
	SocketAsyncResult_t3950677696::get_offset_of_Total_20(),
	SocketAsyncResult_t3950677696::get_offset_of_error_21(),
	SocketAsyncResult_t3950677696::get_offset_of_EndCalled_22(),
	SocketAsyncResult_t3950677696_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (U3CCompleteU3Ec__AnonStorey0_t3411761453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[2] = 
{
	U3CCompleteU3Ec__AnonStorey0_t3411761453::get_offset_of_callback_0(),
	U3CCompleteU3Ec__AnonStorey0_t3411761453::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (SocketError_t307542793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[48] = 
{
	SocketError_t307542793::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (SocketException_t1618573604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[1] = 
{
	SocketException_t1618573604::get_offset_of_m_EndPoint_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (SocketFlags_t2353657790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[11] = 
{
	SocketFlags_t2353657790::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (SocketOperation_t3807127992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[13] = 
{
	SocketOperation_t3807127992::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (SocketOptionLevel_t1505247880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1860[6] = 
{
	SocketOptionLevel_t1505247880::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (SocketOptionName_t1089121285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[47] = 
{
	SocketOptionName_t1089121285::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (SocketShutdown_t3247039417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[4] = 
{
	SocketShutdown_t3247039417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (SocketType_t1143498533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1863[7] = 
{
	SocketType_t1143498533::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (WebAsyncResult_t905414499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[10] = 
{
	WebAsyncResult_t905414499::get_offset_of_nbytes_9(),
	WebAsyncResult_t905414499::get_offset_of_innerAsyncResult_10(),
	WebAsyncResult_t905414499::get_offset_of_response_11(),
	WebAsyncResult_t905414499::get_offset_of_writeStream_12(),
	WebAsyncResult_t905414499::get_offset_of_buffer_13(),
	WebAsyncResult_t905414499::get_offset_of_offset_14(),
	WebAsyncResult_t905414499::get_offset_of_size_15(),
	WebAsyncResult_t905414499::get_offset_of_EndCalled_16(),
	WebAsyncResult_t905414499::get_offset_of_AsyncWriteAll_17(),
	WebAsyncResult_t905414499::get_offset_of_AsyncObject_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (ReadState_t657568301)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[6] = 
{
	ReadState_t657568301::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (WebConnection_t324679648), -1, sizeof(WebConnection_t324679648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[27] = 
{
	WebConnection_t324679648::get_offset_of_sPoint_0(),
	WebConnection_t324679648::get_offset_of_nstream_1(),
	WebConnection_t324679648::get_offset_of_socket_2(),
	WebConnection_t324679648::get_offset_of_socketLock_3(),
	WebConnection_t324679648::get_offset_of_state_4(),
	WebConnection_t324679648::get_offset_of_status_5(),
	WebConnection_t324679648::get_offset_of_initConn_6(),
	WebConnection_t324679648::get_offset_of_keepAlive_7(),
	WebConnection_t324679648::get_offset_of_buffer_8(),
	WebConnection_t324679648_StaticFields::get_offset_of_readDoneDelegate_9(),
	WebConnection_t324679648::get_offset_of_abortHandler_10(),
	WebConnection_t324679648::get_offset_of_abortHelper_11(),
	WebConnection_t324679648::get_offset_of_Data_12(),
	WebConnection_t324679648::get_offset_of_chunkedRead_13(),
	WebConnection_t324679648::get_offset_of_chunkStream_14(),
	WebConnection_t324679648::get_offset_of_queue_15(),
	WebConnection_t324679648::get_offset_of_reused_16(),
	WebConnection_t324679648::get_offset_of_position_17(),
	WebConnection_t324679648::get_offset_of_priority_request_18(),
	WebConnection_t324679648::get_offset_of_ntlm_credentials_19(),
	WebConnection_t324679648::get_offset_of_ntlm_authenticated_20(),
	WebConnection_t324679648::get_offset_of_unsafe_sharing_21(),
	WebConnection_t324679648::get_offset_of_connect_ntlm_auth_state_22(),
	WebConnection_t324679648::get_offset_of_connect_request_23(),
	WebConnection_t324679648::get_offset_of_connect_exception_24(),
	WebConnection_t324679648_StaticFields::get_offset_of_classLock_25(),
	WebConnection_t324679648::get_offset_of_tlsStream_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (NtlmAuthState_t1285319885)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[4] = 
{
	NtlmAuthState_t1285319885::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (AbortHelper_t2895113041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[1] = 
{
	AbortHelper_t2895113041::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (WebConnectionData_t3550260432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[9] = 
{
	WebConnectionData_t3550260432::get_offset_of__request_0(),
	WebConnectionData_t3550260432::get_offset_of_StatusCode_1(),
	WebConnectionData_t3550260432::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3550260432::get_offset_of_Headers_3(),
	WebConnectionData_t3550260432::get_offset_of_Version_4(),
	WebConnectionData_t3550260432::get_offset_of_ProxyVersion_5(),
	WebConnectionData_t3550260432::get_offset_of_stream_6(),
	WebConnectionData_t3550260432::get_offset_of_Challenge_7(),
	WebConnectionData_t3550260432::get_offset_of__readState_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (WebConnectionGroup_t3242458773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[6] = 
{
	WebConnectionGroup_t3242458773::get_offset_of_sPoint_0(),
	WebConnectionGroup_t3242458773::get_offset_of_name_1(),
	WebConnectionGroup_t3242458773::get_offset_of_connections_2(),
	WebConnectionGroup_t3242458773::get_offset_of_queue_3(),
	WebConnectionGroup_t3242458773::get_offset_of_closing_4(),
	WebConnectionGroup_t3242458773::get_offset_of_ConnectionClosed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (ConnectionState_t2608615043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	ConnectionState_t2608615043::get_offset_of_U3CConnectionU3Ek__BackingField_0(),
	ConnectionState_t2608615043::get_offset_of_U3CGroupU3Ek__BackingField_1(),
	ConnectionState_t2608615043::get_offset_of_busy_2(),
	ConnectionState_t2608615043::get_offset_of_idleSince_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (WebConnectionStream_t1922483508), -1, sizeof(WebConnectionStream_t1922483508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1872[31] = 
{
	WebConnectionStream_t1922483508_StaticFields::get_offset_of_crlf_8(),
	WebConnectionStream_t1922483508::get_offset_of_isRead_9(),
	WebConnectionStream_t1922483508::get_offset_of_cnc_10(),
	WebConnectionStream_t1922483508::get_offset_of_request_11(),
	WebConnectionStream_t1922483508::get_offset_of_readBuffer_12(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferOffset_13(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferSize_14(),
	WebConnectionStream_t1922483508::get_offset_of_stream_length_15(),
	WebConnectionStream_t1922483508::get_offset_of_contentLength_16(),
	WebConnectionStream_t1922483508::get_offset_of_totalRead_17(),
	WebConnectionStream_t1922483508::get_offset_of_totalWritten_18(),
	WebConnectionStream_t1922483508::get_offset_of_nextReadCalled_19(),
	WebConnectionStream_t1922483508::get_offset_of_pendingReads_20(),
	WebConnectionStream_t1922483508::get_offset_of_pendingWrites_21(),
	WebConnectionStream_t1922483508::get_offset_of_pending_22(),
	WebConnectionStream_t1922483508::get_offset_of_allowBuffering_23(),
	WebConnectionStream_t1922483508::get_offset_of_sendChunked_24(),
	WebConnectionStream_t1922483508::get_offset_of_writeBuffer_25(),
	WebConnectionStream_t1922483508::get_offset_of_requestWritten_26(),
	WebConnectionStream_t1922483508::get_offset_of_headers_27(),
	WebConnectionStream_t1922483508::get_offset_of_disposed_28(),
	WebConnectionStream_t1922483508::get_offset_of_headersSent_29(),
	WebConnectionStream_t1922483508::get_offset_of_locker_30(),
	WebConnectionStream_t1922483508::get_offset_of_initRead_31(),
	WebConnectionStream_t1922483508::get_offset_of_read_eof_32(),
	WebConnectionStream_t1922483508::get_offset_of_complete_request_written_33(),
	WebConnectionStream_t1922483508::get_offset_of_read_timeout_34(),
	WebConnectionStream_t1922483508::get_offset_of_write_timeout_35(),
	WebConnectionStream_t1922483508::get_offset_of_cb_wrapper_36(),
	WebConnectionStream_t1922483508::get_offset_of_IgnoreIOErrors_37(),
	WebConnectionStream_t1922483508::get_offset_of_U3CGetResponseOnCloseU3Ek__BackingField_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U3CSetHeadersAsyncU3Ec__AnonStorey0_t3819940672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[2] = 
{
	U3CSetHeadersAsyncU3Ec__AnonStorey0_t3819940672::get_offset_of_setInternalLength_0(),
	U3CSetHeadersAsyncU3Ec__AnonStorey0_t3819940672::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[2] = 
{
	U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317::get_offset_of_result_0(),
	U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[4] = 
{
	U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496::get_offset_of_result_0(),
	U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496::get_offset_of_length_1(),
	U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496::get_offset_of_bytes_2(),
	U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (SslProtocols_t894678499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[8] = 
{
	SslProtocols_t894678499::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (AsnDecodeStatus_t1962003286)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1877[7] = 
{
	AsnDecodeStatus_t1962003286::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (AsnEncodedData_t463456204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[2] = 
{
	AsnEncodedData_t463456204::get_offset_of__oid_0(),
	AsnEncodedData_t463456204::get_offset_of__raw_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (OpenFlags_t2370524385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1879[6] = 
{
	OpenFlags_t2370524385::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (OSX509Certificates_t384932784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (SecTrustResult_t1335602280)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1881[9] = 
{
	SecTrustResult_t1335602280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (PublicKey_t870392), -1, sizeof(PublicKey_t870392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1882[5] = 
{
	PublicKey_t870392::get_offset_of__key_0(),
	PublicKey_t870392::get_offset_of__keyValue_1(),
	PublicKey_t870392::get_offset_of__params_2(),
	PublicKey_t870392::get_offset_of__oid_3(),
	PublicKey_t870392_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (StoreLocation_t1570828128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	StoreLocation_t1570828128::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (StoreName_t2183514610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[9] = 
{
	StoreName_t2183514610::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (X500DistinguishedName_t452415348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[2] = 
{
	X500DistinguishedName_t452415348::get_offset_of_name_2(),
	X500DistinguishedName_t452415348::get_offset_of_canonEncoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (X500DistinguishedNameFlags_t2005802885)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[11] = 
{
	X500DistinguishedNameFlags_t2005802885::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (X509BasicConstraintsExtension_t1562873317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1562873317::get_offset_of__certificateAuthority_5(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__hasPathLengthConstraint_6(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__pathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (X509Certificate2Collection_t1108969367), -1, sizeof(X509Certificate2Collection_t1108969367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	X509Certificate2Collection_t1108969367_StaticFields::get_offset_of_newline_split_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (X509Certificate2_t4056456767), -1, sizeof(X509Certificate2_t4056456767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	X509Certificate2_t4056456767::get_offset_of_friendlyName_4(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_signedData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (X509Certificate2Enumerator_t2356134957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	X509Certificate2Enumerator_t2356134957::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (X509Certificate2Impl_t2703153821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (X509Certificate2ImplMono_t2009068154), -1, sizeof(X509Certificate2ImplMono_t2009068154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1892[13] = 
{
	X509Certificate2ImplMono_t2009068154::get_offset_of__archived_1(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__extensions_2(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__serial_3(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__publicKey_4(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_issuer_name_5(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_subject_name_6(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_signature_algorithm_7(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_intermediateCerts_8(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__cert_9(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_empty_error_10(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_commonName_11(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_email_12(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_signedData_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (X509CertificateCollection_t1197680765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (X509CertificateEnumerator_t1208230922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	X509CertificateEnumerator_t1208230922::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (X509CertificateImplCollection_t255811311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[1] = 
{
	X509CertificateImplCollection_t255811311::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (X509Chain_t777637347), -1, sizeof(X509Chain_t777637347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[2] = 
{
	X509Chain_t777637347::get_offset_of_impl_0(),
	X509Chain_t777637347_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (X509ChainElementCollection_t2081831987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	X509ChainElementCollection_t2081831987::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (X509ChainElement_t528874471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	X509ChainElement_t528874471::get_offset_of_certificate_0(),
	X509ChainElement_t528874471::get_offset_of_status_1(),
	X509ChainElement_t528874471::get_offset_of_info_2(),
	X509ChainElement_t528874471::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (X509ChainElementEnumerator_t3304975821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	X509ChainElementEnumerator_t3304975821::get_offset_of_enumerator_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
