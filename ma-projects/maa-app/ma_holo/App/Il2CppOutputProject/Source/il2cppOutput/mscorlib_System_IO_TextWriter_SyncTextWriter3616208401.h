﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter4027217640.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter/SyncTextWriter
struct  SyncTextWriter_t3616208401  : public TextWriter_t4027217640
{
public:
	// System.IO.TextWriter System.IO.TextWriter/SyncTextWriter::_out
	TextWriter_t4027217640 * ____out_11;

public:
	inline static int32_t get_offset_of__out_11() { return static_cast<int32_t>(offsetof(SyncTextWriter_t3616208401, ____out_11)); }
	inline TextWriter_t4027217640 * get__out_11() const { return ____out_11; }
	inline TextWriter_t4027217640 ** get_address_of__out_11() { return &____out_11; }
	inline void set__out_11(TextWriter_t4027217640 * value)
	{
		____out_11 = value;
		Il2CppCodeGenWriteBarrier(&____out_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
