﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Mono_Net_Dns_DnsType1822475631.h"
#include "System_Mono_Net_Dns_DnsClass3168085651.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecord
struct  DnsResourceRecord_t2943454412  : public Il2CppObject
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecord::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsType Mono.Net.Dns.DnsResourceRecord::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsClass Mono.Net.Dns.DnsResourceRecord::klass
	uint16_t ___klass_2;
	// System.Int32 Mono.Net.Dns.DnsResourceRecord::ttl
	int32_t ___ttl_3;
	// System.UInt16 Mono.Net.Dns.DnsResourceRecord::rdlength
	uint16_t ___rdlength_4;
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsResourceRecord::m_rdata
	ArraySegment_1_t2594217482  ___m_rdata_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_klass_2() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___klass_2)); }
	inline uint16_t get_klass_2() const { return ___klass_2; }
	inline uint16_t* get_address_of_klass_2() { return &___klass_2; }
	inline void set_klass_2(uint16_t value)
	{
		___klass_2 = value;
	}

	inline static int32_t get_offset_of_ttl_3() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___ttl_3)); }
	inline int32_t get_ttl_3() const { return ___ttl_3; }
	inline int32_t* get_address_of_ttl_3() { return &___ttl_3; }
	inline void set_ttl_3(int32_t value)
	{
		___ttl_3 = value;
	}

	inline static int32_t get_offset_of_rdlength_4() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___rdlength_4)); }
	inline uint16_t get_rdlength_4() const { return ___rdlength_4; }
	inline uint16_t* get_address_of_rdlength_4() { return &___rdlength_4; }
	inline void set_rdlength_4(uint16_t value)
	{
		___rdlength_4 = value;
	}

	inline static int32_t get_offset_of_m_rdata_5() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___m_rdata_5)); }
	inline ArraySegment_1_t2594217482  get_m_rdata_5() const { return ___m_rdata_5; }
	inline ArraySegment_1_t2594217482 * get_address_of_m_rdata_5() { return &___m_rdata_5; }
	inline void set_m_rdata_5(ArraySegment_1_t2594217482  value)
	{
		___m_rdata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
