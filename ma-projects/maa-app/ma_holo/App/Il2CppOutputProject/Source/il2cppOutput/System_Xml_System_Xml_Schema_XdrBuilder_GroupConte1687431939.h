﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/GroupContent
struct  GroupContent_t1687431939  : public Il2CppObject
{
public:
	// System.UInt32 System.Xml.Schema.XdrBuilder/GroupContent::_MinVal
	uint32_t ____MinVal_0;
	// System.UInt32 System.Xml.Schema.XdrBuilder/GroupContent::_MaxVal
	uint32_t ____MaxVal_1;
	// System.Boolean System.Xml.Schema.XdrBuilder/GroupContent::_HasMaxAttr
	bool ____HasMaxAttr_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/GroupContent::_HasMinAttr
	bool ____HasMinAttr_3;
	// System.Int32 System.Xml.Schema.XdrBuilder/GroupContent::_Order
	int32_t ____Order_4;

public:
	inline static int32_t get_offset_of__MinVal_0() { return static_cast<int32_t>(offsetof(GroupContent_t1687431939, ____MinVal_0)); }
	inline uint32_t get__MinVal_0() const { return ____MinVal_0; }
	inline uint32_t* get_address_of__MinVal_0() { return &____MinVal_0; }
	inline void set__MinVal_0(uint32_t value)
	{
		____MinVal_0 = value;
	}

	inline static int32_t get_offset_of__MaxVal_1() { return static_cast<int32_t>(offsetof(GroupContent_t1687431939, ____MaxVal_1)); }
	inline uint32_t get__MaxVal_1() const { return ____MaxVal_1; }
	inline uint32_t* get_address_of__MaxVal_1() { return &____MaxVal_1; }
	inline void set__MaxVal_1(uint32_t value)
	{
		____MaxVal_1 = value;
	}

	inline static int32_t get_offset_of__HasMaxAttr_2() { return static_cast<int32_t>(offsetof(GroupContent_t1687431939, ____HasMaxAttr_2)); }
	inline bool get__HasMaxAttr_2() const { return ____HasMaxAttr_2; }
	inline bool* get_address_of__HasMaxAttr_2() { return &____HasMaxAttr_2; }
	inline void set__HasMaxAttr_2(bool value)
	{
		____HasMaxAttr_2 = value;
	}

	inline static int32_t get_offset_of__HasMinAttr_3() { return static_cast<int32_t>(offsetof(GroupContent_t1687431939, ____HasMinAttr_3)); }
	inline bool get__HasMinAttr_3() const { return ____HasMinAttr_3; }
	inline bool* get_address_of__HasMinAttr_3() { return &____HasMinAttr_3; }
	inline void set__HasMinAttr_3(bool value)
	{
		____HasMinAttr_3 = value;
	}

	inline static int32_t get_offset_of__Order_4() { return static_cast<int32_t>(offsetof(GroupContent_t1687431939, ____Order_4)); }
	inline int32_t get__Order_4() const { return ____Order_4; }
	inline int32_t* get_address_of__Order_4() { return &____Order_4; }
	inline void set__Order_4(int32_t value)
	{
		____Order_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
