﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`2<System.Object,System.Object>
struct  Tuple_2_t1036200771  : public Il2CppObject
{
public:
	// T1 System.Tuple`2::m_Item1
	Il2CppObject * ___m_Item1_0;
	// T2 System.Tuple`2::m_Item2
	Il2CppObject * ___m_Item2_1;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t1036200771, ___m_Item1_0)); }
	inline Il2CppObject * get_m_Item1_0() const { return ___m_Item1_0; }
	inline Il2CppObject ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(Il2CppObject * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t1036200771, ___m_Item2_1)); }
	inline Il2CppObject * get_m_Item2_1() const { return ___m_Item2_1; }
	inline Il2CppObject ** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(Il2CppObject * value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
