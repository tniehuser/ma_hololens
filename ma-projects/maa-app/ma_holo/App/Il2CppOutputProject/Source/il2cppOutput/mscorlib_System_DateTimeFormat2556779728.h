﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeFormat
struct  DateTimeFormat_t2556779728  : public Il2CppObject
{
public:

public:
};

struct DateTimeFormat_t2556779728_StaticFields
{
public:
	// System.TimeSpan System.DateTimeFormat::NullOffset
	TimeSpan_t3430258949  ___NullOffset_0;
	// System.Char[] System.DateTimeFormat::allStandardFormats
	CharU5BU5D_t1328083999* ___allStandardFormats_1;
	// System.String[] System.DateTimeFormat::fixedNumberFormats
	StringU5BU5D_t1642385972* ___fixedNumberFormats_2;

public:
	inline static int32_t get_offset_of_NullOffset_0() { return static_cast<int32_t>(offsetof(DateTimeFormat_t2556779728_StaticFields, ___NullOffset_0)); }
	inline TimeSpan_t3430258949  get_NullOffset_0() const { return ___NullOffset_0; }
	inline TimeSpan_t3430258949 * get_address_of_NullOffset_0() { return &___NullOffset_0; }
	inline void set_NullOffset_0(TimeSpan_t3430258949  value)
	{
		___NullOffset_0 = value;
	}

	inline static int32_t get_offset_of_allStandardFormats_1() { return static_cast<int32_t>(offsetof(DateTimeFormat_t2556779728_StaticFields, ___allStandardFormats_1)); }
	inline CharU5BU5D_t1328083999* get_allStandardFormats_1() const { return ___allStandardFormats_1; }
	inline CharU5BU5D_t1328083999** get_address_of_allStandardFormats_1() { return &___allStandardFormats_1; }
	inline void set_allStandardFormats_1(CharU5BU5D_t1328083999* value)
	{
		___allStandardFormats_1 = value;
		Il2CppCodeGenWriteBarrier(&___allStandardFormats_1, value);
	}

	inline static int32_t get_offset_of_fixedNumberFormats_2() { return static_cast<int32_t>(offsetof(DateTimeFormat_t2556779728_StaticFields, ___fixedNumberFormats_2)); }
	inline StringU5BU5D_t1642385972* get_fixedNumberFormats_2() const { return ___fixedNumberFormats_2; }
	inline StringU5BU5D_t1642385972** get_address_of_fixedNumberFormats_2() { return &___fixedNumberFormats_2; }
	inline void set_fixedNumberFormats_2(StringU5BU5D_t1642385972* value)
	{
		___fixedNumberFormats_2 = value;
		Il2CppCodeGenWriteBarrier(&___fixedNumberFormats_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
