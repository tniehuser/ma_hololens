﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_TaskContinuation1422769290.h"

// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Action
struct Action_t3226471752;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;
// System.Threading.WaitCallback
struct WaitCallback_t2798937288;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.AwaitTaskContinuation
struct  AwaitTaskContinuation_t2160930432  : public TaskContinuation_t1422769290
{
public:
	// System.Threading.ExecutionContext System.Threading.Tasks.AwaitTaskContinuation::m_capturedContext
	ExecutionContext_t1392266323 * ___m_capturedContext_0;
	// System.Action System.Threading.Tasks.AwaitTaskContinuation::m_action
	Action_t3226471752 * ___m_action_1;

public:
	inline static int32_t get_offset_of_m_capturedContext_0() { return static_cast<int32_t>(offsetof(AwaitTaskContinuation_t2160930432, ___m_capturedContext_0)); }
	inline ExecutionContext_t1392266323 * get_m_capturedContext_0() const { return ___m_capturedContext_0; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_capturedContext_0() { return &___m_capturedContext_0; }
	inline void set_m_capturedContext_0(ExecutionContext_t1392266323 * value)
	{
		___m_capturedContext_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_capturedContext_0, value);
	}

	inline static int32_t get_offset_of_m_action_1() { return static_cast<int32_t>(offsetof(AwaitTaskContinuation_t2160930432, ___m_action_1)); }
	inline Action_t3226471752 * get_m_action_1() const { return ___m_action_1; }
	inline Action_t3226471752 ** get_address_of_m_action_1() { return &___m_action_1; }
	inline void set_m_action_1(Action_t3226471752 * value)
	{
		___m_action_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_action_1, value);
	}
};

struct AwaitTaskContinuation_t2160930432_StaticFields
{
public:
	// System.Threading.ContextCallback System.Threading.Tasks.AwaitTaskContinuation::s_invokeActionCallback
	ContextCallback_t2287130692 * ___s_invokeActionCallback_2;
	// System.Threading.ContextCallback System.Threading.Tasks.AwaitTaskContinuation::<>f__mg$cache0
	ContextCallback_t2287130692 * ___U3CU3Ef__mgU24cache0_3;
	// System.Threading.WaitCallback System.Threading.Tasks.AwaitTaskContinuation::<>f__am$cache0
	WaitCallback_t2798937288 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_s_invokeActionCallback_2() { return static_cast<int32_t>(offsetof(AwaitTaskContinuation_t2160930432_StaticFields, ___s_invokeActionCallback_2)); }
	inline ContextCallback_t2287130692 * get_s_invokeActionCallback_2() const { return ___s_invokeActionCallback_2; }
	inline ContextCallback_t2287130692 ** get_address_of_s_invokeActionCallback_2() { return &___s_invokeActionCallback_2; }
	inline void set_s_invokeActionCallback_2(ContextCallback_t2287130692 * value)
	{
		___s_invokeActionCallback_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_invokeActionCallback_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(AwaitTaskContinuation_t2160930432_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ContextCallback_t2287130692 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ContextCallback_t2287130692 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ContextCallback_t2287130692 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(AwaitTaskContinuation_t2160930432_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline WaitCallback_t2798937288 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline WaitCallback_t2798937288 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(WaitCallback_t2798937288 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
