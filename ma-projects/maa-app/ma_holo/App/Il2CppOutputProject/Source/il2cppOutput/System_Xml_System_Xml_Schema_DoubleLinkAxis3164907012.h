﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_Axis1660922251.h"

// MS.Internal.Xml.XPath.Axis
struct Axis_t1660922251;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DoubleLinkAxis
struct  DoubleLinkAxis_t3164907012  : public Axis_t1660922251
{
public:
	// MS.Internal.Xml.XPath.Axis System.Xml.Schema.DoubleLinkAxis::next
	Axis_t1660922251 * ___next_7;

public:
	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(DoubleLinkAxis_t3164907012, ___next_7)); }
	inline Axis_t1660922251 * get_next_7() const { return ___next_7; }
	inline Axis_t1660922251 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(Axis_t1660922251 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier(&___next_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
