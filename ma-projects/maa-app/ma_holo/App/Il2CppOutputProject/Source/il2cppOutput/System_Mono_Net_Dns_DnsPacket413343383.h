﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Net.Dns.DnsHeader
struct DnsHeader_t2481892084;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsPacket
struct  DnsPacket_t413343383  : public Il2CppObject
{
public:
	// System.Byte[] Mono.Net.Dns.DnsPacket::packet
	ByteU5BU5D_t3397334013* ___packet_0;
	// System.Int32 Mono.Net.Dns.DnsPacket::position
	int32_t ___position_1;
	// Mono.Net.Dns.DnsHeader Mono.Net.Dns.DnsPacket::header
	DnsHeader_t2481892084 * ___header_2;

public:
	inline static int32_t get_offset_of_packet_0() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___packet_0)); }
	inline ByteU5BU5D_t3397334013* get_packet_0() const { return ___packet_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_packet_0() { return &___packet_0; }
	inline void set_packet_0(ByteU5BU5D_t3397334013* value)
	{
		___packet_0 = value;
		Il2CppCodeGenWriteBarrier(&___packet_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_header_2() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___header_2)); }
	inline DnsHeader_t2481892084 * get_header_2() const { return ___header_2; }
	inline DnsHeader_t2481892084 ** get_address_of_header_2() { return &___header_2; }
	inline void set_header_2(DnsHeader_t2481892084 * value)
	{
		___header_2 = value;
		Il2CppCodeGenWriteBarrier(&___header_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
