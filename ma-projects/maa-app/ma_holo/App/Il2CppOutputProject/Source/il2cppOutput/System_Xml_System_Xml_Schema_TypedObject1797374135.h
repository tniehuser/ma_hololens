﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.TypedObject/DecimalStruct
struct DecimalStruct_t715828147;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject
struct  TypedObject_t1797374135  : public Il2CppObject
{
public:
	// System.Xml.Schema.TypedObject/DecimalStruct System.Xml.Schema.TypedObject::dstruct
	DecimalStruct_t715828147 * ___dstruct_0;
	// System.Object System.Xml.Schema.TypedObject::ovalue
	Il2CppObject * ___ovalue_1;
	// System.String System.Xml.Schema.TypedObject::svalue
	String_t* ___svalue_2;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.TypedObject::xsdtype
	XmlSchemaDatatype_t1195946242 * ___xsdtype_3;
	// System.Int32 System.Xml.Schema.TypedObject::dim
	int32_t ___dim_4;
	// System.Boolean System.Xml.Schema.TypedObject::isList
	bool ___isList_5;

public:
	inline static int32_t get_offset_of_dstruct_0() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___dstruct_0)); }
	inline DecimalStruct_t715828147 * get_dstruct_0() const { return ___dstruct_0; }
	inline DecimalStruct_t715828147 ** get_address_of_dstruct_0() { return &___dstruct_0; }
	inline void set_dstruct_0(DecimalStruct_t715828147 * value)
	{
		___dstruct_0 = value;
		Il2CppCodeGenWriteBarrier(&___dstruct_0, value);
	}

	inline static int32_t get_offset_of_ovalue_1() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___ovalue_1)); }
	inline Il2CppObject * get_ovalue_1() const { return ___ovalue_1; }
	inline Il2CppObject ** get_address_of_ovalue_1() { return &___ovalue_1; }
	inline void set_ovalue_1(Il2CppObject * value)
	{
		___ovalue_1 = value;
		Il2CppCodeGenWriteBarrier(&___ovalue_1, value);
	}

	inline static int32_t get_offset_of_svalue_2() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___svalue_2)); }
	inline String_t* get_svalue_2() const { return ___svalue_2; }
	inline String_t** get_address_of_svalue_2() { return &___svalue_2; }
	inline void set_svalue_2(String_t* value)
	{
		___svalue_2 = value;
		Il2CppCodeGenWriteBarrier(&___svalue_2, value);
	}

	inline static int32_t get_offset_of_xsdtype_3() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___xsdtype_3)); }
	inline XmlSchemaDatatype_t1195946242 * get_xsdtype_3() const { return ___xsdtype_3; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_xsdtype_3() { return &___xsdtype_3; }
	inline void set_xsdtype_3(XmlSchemaDatatype_t1195946242 * value)
	{
		___xsdtype_3 = value;
		Il2CppCodeGenWriteBarrier(&___xsdtype_3, value);
	}

	inline static int32_t get_offset_of_dim_4() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___dim_4)); }
	inline int32_t get_dim_4() const { return ___dim_4; }
	inline int32_t* get_address_of_dim_4() { return &___dim_4; }
	inline void set_dim_4(int32_t value)
	{
		___dim_4 = value;
	}

	inline static int32_t get_offset_of_isList_5() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___isList_5)); }
	inline bool get_isList_5() const { return ___isList_5; }
	inline bool* get_address_of_isList_5() { return &___isList_5; }
	inline void set_isList_5(bool value)
	{
		___isList_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
