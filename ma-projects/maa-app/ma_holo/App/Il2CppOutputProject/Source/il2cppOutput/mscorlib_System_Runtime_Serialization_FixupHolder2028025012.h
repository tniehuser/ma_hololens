﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FixupHolder
struct  FixupHolder_t2028025012  : public Il2CppObject
{
public:
	// System.Int64 System.Runtime.Serialization.FixupHolder::m_id
	int64_t ___m_id_0;
	// System.Object System.Runtime.Serialization.FixupHolder::m_fixupInfo
	Il2CppObject * ___m_fixupInfo_1;
	// System.Int32 System.Runtime.Serialization.FixupHolder::m_fixupType
	int32_t ___m_fixupType_2;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(FixupHolder_t2028025012, ___m_id_0)); }
	inline int64_t get_m_id_0() const { return ___m_id_0; }
	inline int64_t* get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(int64_t value)
	{
		___m_id_0 = value;
	}

	inline static int32_t get_offset_of_m_fixupInfo_1() { return static_cast<int32_t>(offsetof(FixupHolder_t2028025012, ___m_fixupInfo_1)); }
	inline Il2CppObject * get_m_fixupInfo_1() const { return ___m_fixupInfo_1; }
	inline Il2CppObject ** get_address_of_m_fixupInfo_1() { return &___m_fixupInfo_1; }
	inline void set_m_fixupInfo_1(Il2CppObject * value)
	{
		___m_fixupInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_fixupInfo_1, value);
	}

	inline static int32_t get_offset_of_m_fixupType_2() { return static_cast<int32_t>(offsetof(FixupHolder_t2028025012, ___m_fixupType_2)); }
	inline int32_t get_m_fixupType_2() const { return ___m_fixupType_2; }
	inline int32_t* get_address_of_m_fixupType_2() { return &___m_fixupType_2; }
	inline void set_m_fixupType_2(int32_t value)
	{
		___m_fixupType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
