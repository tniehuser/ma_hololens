﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParamsArray
struct  ParamsArray_t2726825425 
{
public:
	// System.Object System.ParamsArray::arg0
	Il2CppObject * ___arg0_3;
	// System.Object System.ParamsArray::arg1
	Il2CppObject * ___arg1_4;
	// System.Object System.ParamsArray::arg2
	Il2CppObject * ___arg2_5;
	// System.Object[] System.ParamsArray::args
	ObjectU5BU5D_t3614634134* ___args_6;

public:
	inline static int32_t get_offset_of_arg0_3() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425, ___arg0_3)); }
	inline Il2CppObject * get_arg0_3() const { return ___arg0_3; }
	inline Il2CppObject ** get_address_of_arg0_3() { return &___arg0_3; }
	inline void set_arg0_3(Il2CppObject * value)
	{
		___arg0_3 = value;
		Il2CppCodeGenWriteBarrier(&___arg0_3, value);
	}

	inline static int32_t get_offset_of_arg1_4() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425, ___arg1_4)); }
	inline Il2CppObject * get_arg1_4() const { return ___arg1_4; }
	inline Il2CppObject ** get_address_of_arg1_4() { return &___arg1_4; }
	inline void set_arg1_4(Il2CppObject * value)
	{
		___arg1_4 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_4, value);
	}

	inline static int32_t get_offset_of_arg2_5() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425, ___arg2_5)); }
	inline Il2CppObject * get_arg2_5() const { return ___arg2_5; }
	inline Il2CppObject ** get_address_of_arg2_5() { return &___arg2_5; }
	inline void set_arg2_5(Il2CppObject * value)
	{
		___arg2_5 = value;
		Il2CppCodeGenWriteBarrier(&___arg2_5, value);
	}

	inline static int32_t get_offset_of_args_6() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425, ___args_6)); }
	inline ObjectU5BU5D_t3614634134* get_args_6() const { return ___args_6; }
	inline ObjectU5BU5D_t3614634134** get_address_of_args_6() { return &___args_6; }
	inline void set_args_6(ObjectU5BU5D_t3614634134* value)
	{
		___args_6 = value;
		Il2CppCodeGenWriteBarrier(&___args_6, value);
	}
};

struct ParamsArray_t2726825425_StaticFields
{
public:
	// System.Object[] System.ParamsArray::oneArgArray
	ObjectU5BU5D_t3614634134* ___oneArgArray_0;
	// System.Object[] System.ParamsArray::twoArgArray
	ObjectU5BU5D_t3614634134* ___twoArgArray_1;
	// System.Object[] System.ParamsArray::threeArgArray
	ObjectU5BU5D_t3614634134* ___threeArgArray_2;

public:
	inline static int32_t get_offset_of_oneArgArray_0() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425_StaticFields, ___oneArgArray_0)); }
	inline ObjectU5BU5D_t3614634134* get_oneArgArray_0() const { return ___oneArgArray_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_oneArgArray_0() { return &___oneArgArray_0; }
	inline void set_oneArgArray_0(ObjectU5BU5D_t3614634134* value)
	{
		___oneArgArray_0 = value;
		Il2CppCodeGenWriteBarrier(&___oneArgArray_0, value);
	}

	inline static int32_t get_offset_of_twoArgArray_1() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425_StaticFields, ___twoArgArray_1)); }
	inline ObjectU5BU5D_t3614634134* get_twoArgArray_1() const { return ___twoArgArray_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_twoArgArray_1() { return &___twoArgArray_1; }
	inline void set_twoArgArray_1(ObjectU5BU5D_t3614634134* value)
	{
		___twoArgArray_1 = value;
		Il2CppCodeGenWriteBarrier(&___twoArgArray_1, value);
	}

	inline static int32_t get_offset_of_threeArgArray_2() { return static_cast<int32_t>(offsetof(ParamsArray_t2726825425_StaticFields, ___threeArgArray_2)); }
	inline ObjectU5BU5D_t3614634134* get_threeArgArray_2() const { return ___threeArgArray_2; }
	inline ObjectU5BU5D_t3614634134** get_address_of_threeArgArray_2() { return &___threeArgArray_2; }
	inline void set_threeArgArray_2(ObjectU5BU5D_t3614634134* value)
	{
		___threeArgArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___threeArgArray_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ParamsArray
struct ParamsArray_t2726825425_marshaled_pinvoke
{
	Il2CppIUnknown* ___arg0_3;
	Il2CppIUnknown* ___arg1_4;
	Il2CppIUnknown* ___arg2_5;
	ObjectU5BU5D_t3614634134* ___args_6;
};
// Native definition for COM marshalling of System.ParamsArray
struct ParamsArray_t2726825425_marshaled_com
{
	Il2CppIUnknown* ___arg0_3;
	Il2CppIUnknown* ___arg1_4;
	Il2CppIUnknown* ___arg2_5;
	ObjectU5BU5D_t3614634134* ___args_6;
};
