﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Position
struct  Position_t1796812729 
{
public:
	// System.Int32 System.Xml.Schema.Position::symbol
	int32_t ___symbol_0;
	// System.Object System.Xml.Schema.Position::particle
	Il2CppObject * ___particle_1;

public:
	inline static int32_t get_offset_of_symbol_0() { return static_cast<int32_t>(offsetof(Position_t1796812729, ___symbol_0)); }
	inline int32_t get_symbol_0() const { return ___symbol_0; }
	inline int32_t* get_address_of_symbol_0() { return &___symbol_0; }
	inline void set_symbol_0(int32_t value)
	{
		___symbol_0 = value;
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(Position_t1796812729, ___particle_1)); }
	inline Il2CppObject * get_particle_1() const { return ___particle_1; }
	inline Il2CppObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(Il2CppObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier(&___particle_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.Position
struct Position_t1796812729_marshaled_pinvoke
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
// Native definition for COM marshalling of System.Xml.Schema.Position
struct Position_t1796812729_marshaled_com
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
