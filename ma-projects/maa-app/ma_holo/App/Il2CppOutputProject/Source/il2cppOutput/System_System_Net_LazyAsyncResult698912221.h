﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.LazyAsyncResult/ThreadContext
struct ThreadContext_t1626776243;
// System.Object
struct Il2CppObject;
// System.AsyncCallback
struct AsyncCallback_t163412349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult
struct  LazyAsyncResult_t698912221  : public Il2CppObject
{
public:
	// System.Object System.Net.LazyAsyncResult::m_AsyncObject
	Il2CppObject * ___m_AsyncObject_1;
	// System.Object System.Net.LazyAsyncResult::m_AsyncState
	Il2CppObject * ___m_AsyncState_2;
	// System.AsyncCallback System.Net.LazyAsyncResult::m_AsyncCallback
	AsyncCallback_t163412349 * ___m_AsyncCallback_3;
	// System.Object System.Net.LazyAsyncResult::m_Result
	Il2CppObject * ___m_Result_4;
	// System.Int32 System.Net.LazyAsyncResult::m_IntCompleted
	int32_t ___m_IntCompleted_5;
	// System.Boolean System.Net.LazyAsyncResult::m_UserEvent
	bool ___m_UserEvent_6;
	// System.Object System.Net.LazyAsyncResult::m_Event
	Il2CppObject * ___m_Event_7;

public:
	inline static int32_t get_offset_of_m_AsyncObject_1() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_AsyncObject_1)); }
	inline Il2CppObject * get_m_AsyncObject_1() const { return ___m_AsyncObject_1; }
	inline Il2CppObject ** get_address_of_m_AsyncObject_1() { return &___m_AsyncObject_1; }
	inline void set_m_AsyncObject_1(Il2CppObject * value)
	{
		___m_AsyncObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncObject_1, value);
	}

	inline static int32_t get_offset_of_m_AsyncState_2() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_AsyncState_2)); }
	inline Il2CppObject * get_m_AsyncState_2() const { return ___m_AsyncState_2; }
	inline Il2CppObject ** get_address_of_m_AsyncState_2() { return &___m_AsyncState_2; }
	inline void set_m_AsyncState_2(Il2CppObject * value)
	{
		___m_AsyncState_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncState_2, value);
	}

	inline static int32_t get_offset_of_m_AsyncCallback_3() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_AsyncCallback_3)); }
	inline AsyncCallback_t163412349 * get_m_AsyncCallback_3() const { return ___m_AsyncCallback_3; }
	inline AsyncCallback_t163412349 ** get_address_of_m_AsyncCallback_3() { return &___m_AsyncCallback_3; }
	inline void set_m_AsyncCallback_3(AsyncCallback_t163412349 * value)
	{
		___m_AsyncCallback_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncCallback_3, value);
	}

	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_Result_4)); }
	inline Il2CppObject * get_m_Result_4() const { return ___m_Result_4; }
	inline Il2CppObject ** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(Il2CppObject * value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Result_4, value);
	}

	inline static int32_t get_offset_of_m_IntCompleted_5() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_IntCompleted_5)); }
	inline int32_t get_m_IntCompleted_5() const { return ___m_IntCompleted_5; }
	inline int32_t* get_address_of_m_IntCompleted_5() { return &___m_IntCompleted_5; }
	inline void set_m_IntCompleted_5(int32_t value)
	{
		___m_IntCompleted_5 = value;
	}

	inline static int32_t get_offset_of_m_UserEvent_6() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_UserEvent_6)); }
	inline bool get_m_UserEvent_6() const { return ___m_UserEvent_6; }
	inline bool* get_address_of_m_UserEvent_6() { return &___m_UserEvent_6; }
	inline void set_m_UserEvent_6(bool value)
	{
		___m_UserEvent_6 = value;
	}

	inline static int32_t get_offset_of_m_Event_7() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221, ___m_Event_7)); }
	inline Il2CppObject * get_m_Event_7() const { return ___m_Event_7; }
	inline Il2CppObject ** get_address_of_m_Event_7() { return &___m_Event_7; }
	inline void set_m_Event_7(Il2CppObject * value)
	{
		___m_Event_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Event_7, value);
	}
};

struct LazyAsyncResult_t698912221_ThreadStaticFields
{
public:
	// System.Net.LazyAsyncResult/ThreadContext System.Net.LazyAsyncResult::t_ThreadContext
	ThreadContext_t1626776243 * ___t_ThreadContext_0;

public:
	inline static int32_t get_offset_of_t_ThreadContext_0() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t698912221_ThreadStaticFields, ___t_ThreadContext_0)); }
	inline ThreadContext_t1626776243 * get_t_ThreadContext_0() const { return ___t_ThreadContext_0; }
	inline ThreadContext_t1626776243 ** get_address_of_t_ThreadContext_0() { return &___t_ThreadContext_0; }
	inline void set_t_ThreadContext_0(ThreadContext_t1626776243 * value)
	{
		___t_ThreadContext_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_ThreadContext_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
