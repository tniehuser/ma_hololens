﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.Design.ITypeDescriptorFilterService
struct ITypeDescriptorFilterService_t3228556567;
// System.Collections.ICollection
struct ICollection_t91669223;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor/FilterCacheItem
struct  FilterCacheItem_t42357068  : public Il2CppObject
{
public:
	// System.ComponentModel.Design.ITypeDescriptorFilterService System.ComponentModel.TypeDescriptor/FilterCacheItem::_filterService
	Il2CppObject * ____filterService_0;
	// System.Collections.ICollection System.ComponentModel.TypeDescriptor/FilterCacheItem::FilteredMembers
	Il2CppObject * ___FilteredMembers_1;

public:
	inline static int32_t get_offset_of__filterService_0() { return static_cast<int32_t>(offsetof(FilterCacheItem_t42357068, ____filterService_0)); }
	inline Il2CppObject * get__filterService_0() const { return ____filterService_0; }
	inline Il2CppObject ** get_address_of__filterService_0() { return &____filterService_0; }
	inline void set__filterService_0(Il2CppObject * value)
	{
		____filterService_0 = value;
		Il2CppCodeGenWriteBarrier(&____filterService_0, value);
	}

	inline static int32_t get_offset_of_FilteredMembers_1() { return static_cast<int32_t>(offsetof(FilterCacheItem_t42357068, ___FilteredMembers_1)); }
	inline Il2CppObject * get_FilteredMembers_1() const { return ___FilteredMembers_1; }
	inline Il2CppObject ** get_address_of_FilteredMembers_1() { return &___FilteredMembers_1; }
	inline void set_FilteredMembers_1(Il2CppObject * value)
	{
		___FilteredMembers_1 = value;
		Il2CppCodeGenWriteBarrier(&___FilteredMembers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
