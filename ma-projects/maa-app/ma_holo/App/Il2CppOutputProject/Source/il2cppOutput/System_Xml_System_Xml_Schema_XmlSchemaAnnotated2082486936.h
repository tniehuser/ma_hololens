﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t2400301303;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t287209776;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t2082486936  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_6;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_7;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::moreAttributes
	XmlAttributeU5BU5D_t287209776* ___moreAttributes_8;

public:
	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___id_6)); }
	inline String_t* get_id_6() const { return ___id_6; }
	inline String_t** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(String_t* value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier(&___id_6, value);
	}

	inline static int32_t get_offset_of_annotation_7() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___annotation_7)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_7() const { return ___annotation_7; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_7() { return &___annotation_7; }
	inline void set_annotation_7(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_7 = value;
		Il2CppCodeGenWriteBarrier(&___annotation_7, value);
	}

	inline static int32_t get_offset_of_moreAttributes_8() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___moreAttributes_8)); }
	inline XmlAttributeU5BU5D_t287209776* get_moreAttributes_8() const { return ___moreAttributes_8; }
	inline XmlAttributeU5BU5D_t287209776** get_address_of_moreAttributes_8() { return &___moreAttributes_8; }
	inline void set_moreAttributes_8(XmlAttributeU5BU5D_t287209776* value)
	{
		___moreAttributes_8 = value;
		Il2CppCodeGenWriteBarrier(&___moreAttributes_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
