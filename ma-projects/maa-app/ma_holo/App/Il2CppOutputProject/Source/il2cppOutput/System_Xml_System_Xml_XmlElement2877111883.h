﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"

// System.Xml.XmlName
struct XmlName_t3016058992;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t3359885287;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t2877111883  : public XmlLinkedNode_t1287616130
{
public:
	// System.Xml.XmlName System.Xml.XmlElement::name
	XmlName_t3016058992 * ___name_2;
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t3359885287 * ___attributes_3;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastChild
	XmlLinkedNode_t1287616130 * ___lastChild_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___name_2)); }
	inline XmlName_t3016058992 * get_name_2() const { return ___name_2; }
	inline XmlName_t3016058992 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XmlName_t3016058992 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___attributes_3)); }
	inline XmlAttributeCollection_t3359885287 * get_attributes_3() const { return ___attributes_3; }
	inline XmlAttributeCollection_t3359885287 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlAttributeCollection_t3359885287 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_3, value);
	}

	inline static int32_t get_offset_of_lastChild_4() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___lastChild_4)); }
	inline XmlLinkedNode_t1287616130 * get_lastChild_4() const { return ___lastChild_4; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastChild_4() { return &___lastChild_4; }
	inline void set_lastChild_4(XmlLinkedNode_t1287616130 * value)
	{
		___lastChild_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastChild_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
