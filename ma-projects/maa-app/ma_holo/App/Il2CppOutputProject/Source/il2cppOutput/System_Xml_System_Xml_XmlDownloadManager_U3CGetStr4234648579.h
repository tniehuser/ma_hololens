﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Uri
struct Uri_t19570940;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager/<GetStreamAsync>c__AnonStorey1
struct  U3CGetStreamAsyncU3Ec__AnonStorey1_t4234648579  : public Il2CppObject
{
public:
	// System.Uri System.Xml.XmlDownloadManager/<GetStreamAsync>c__AnonStorey1::uri
	Uri_t19570940 * ___uri_0;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CGetStreamAsyncU3Ec__AnonStorey1_t4234648579, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier(&___uri_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
