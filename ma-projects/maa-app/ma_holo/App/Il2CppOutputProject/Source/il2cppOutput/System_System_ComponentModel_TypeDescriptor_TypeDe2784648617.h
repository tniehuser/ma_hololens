﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"

// System.ComponentModel.TypeDescriptor/TypeDescriptionNode
struct TypeDescriptionNode_t2784648617;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t2438624375;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor/TypeDescriptionNode
struct  TypeDescriptionNode_t2784648617  : public TypeDescriptionProvider_t2438624375
{
public:
	// System.ComponentModel.TypeDescriptor/TypeDescriptionNode System.ComponentModel.TypeDescriptor/TypeDescriptionNode::Next
	TypeDescriptionNode_t2784648617 * ___Next_2;
	// System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptor/TypeDescriptionNode::Provider
	TypeDescriptionProvider_t2438624375 * ___Provider_3;

public:
	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(TypeDescriptionNode_t2784648617, ___Next_2)); }
	inline TypeDescriptionNode_t2784648617 * get_Next_2() const { return ___Next_2; }
	inline TypeDescriptionNode_t2784648617 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(TypeDescriptionNode_t2784648617 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier(&___Next_2, value);
	}

	inline static int32_t get_offset_of_Provider_3() { return static_cast<int32_t>(offsetof(TypeDescriptionNode_t2784648617, ___Provider_3)); }
	inline TypeDescriptionProvider_t2438624375 * get_Provider_3() const { return ___Provider_3; }
	inline TypeDescriptionProvider_t2438624375 ** get_address_of_Provider_3() { return &___Provider_3; }
	inline void set_Provider_3(TypeDescriptionProvider_t2438624375 * value)
	{
		___Provider_3 = value;
		Il2CppCodeGenWriteBarrier(&___Provider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
