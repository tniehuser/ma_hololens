﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HWStack
struct  HWStack_t738999989  : public Il2CppObject
{
public:
	// System.Object[] System.Xml.HWStack::stack
	ObjectU5BU5D_t3614634134* ___stack_0;
	// System.Int32 System.Xml.HWStack::growthRate
	int32_t ___growthRate_1;
	// System.Int32 System.Xml.HWStack::used
	int32_t ___used_2;
	// System.Int32 System.Xml.HWStack::size
	int32_t ___size_3;
	// System.Int32 System.Xml.HWStack::limit
	int32_t ___limit_4;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(HWStack_t738999989, ___stack_0)); }
	inline ObjectU5BU5D_t3614634134* get_stack_0() const { return ___stack_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ObjectU5BU5D_t3614634134* value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier(&___stack_0, value);
	}

	inline static int32_t get_offset_of_growthRate_1() { return static_cast<int32_t>(offsetof(HWStack_t738999989, ___growthRate_1)); }
	inline int32_t get_growthRate_1() const { return ___growthRate_1; }
	inline int32_t* get_address_of_growthRate_1() { return &___growthRate_1; }
	inline void set_growthRate_1(int32_t value)
	{
		___growthRate_1 = value;
	}

	inline static int32_t get_offset_of_used_2() { return static_cast<int32_t>(offsetof(HWStack_t738999989, ___used_2)); }
	inline int32_t get_used_2() const { return ___used_2; }
	inline int32_t* get_address_of_used_2() { return &___used_2; }
	inline void set_used_2(int32_t value)
	{
		___used_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(HWStack_t738999989, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_limit_4() { return static_cast<int32_t>(offsetof(HWStack_t738999989, ___limit_4)); }
	inline int32_t get_limit_4() const { return ___limit_4; }
	inline int32_t* get_address_of_limit_4() { return &___limit_4; }
	inline void set_limit_4(int32_t value)
	{
		___limit_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
