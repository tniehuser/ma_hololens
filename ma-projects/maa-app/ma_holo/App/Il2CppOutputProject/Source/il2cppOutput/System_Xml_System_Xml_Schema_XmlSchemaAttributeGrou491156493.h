﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaAttributeGroup
struct XmlSchemaAttributeGroup_t491156493;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroup
struct  XmlSchemaAttributeGroup_t491156493  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaAttributeGroup::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAttributeGroup::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_10;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroup::qname
	XmlQualifiedName_t1944712516 * ___qname_12;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XmlSchemaAttributeGroup::redefined
	XmlSchemaAttributeGroup_t491156493 * ___redefined_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaAttributeGroup::attributeUses
	XmlSchemaObjectTable_t3364835593 * ___attributeUses_14;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::attributeWildcard
	XmlSchemaAnyAttribute_t530453212 * ___attributeWildcard_15;
	// System.Int32 System.Xml.Schema.XmlSchemaAttributeGroup::selfReferenceCount
	int32_t ___selfReferenceCount_16;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___attributes_10)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_10() const { return ___attributes_10; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_10, value);
	}

	inline static int32_t get_offset_of_anyAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___anyAttribute_11)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_11() const { return ___anyAttribute_11; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_11() { return &___anyAttribute_11; }
	inline void set_anyAttribute_11(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_11 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_11, value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___qname_12)); }
	inline XmlQualifiedName_t1944712516 * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_t1944712516 * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier(&___qname_12, value);
	}

	inline static int32_t get_offset_of_redefined_13() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___redefined_13)); }
	inline XmlSchemaAttributeGroup_t491156493 * get_redefined_13() const { return ___redefined_13; }
	inline XmlSchemaAttributeGroup_t491156493 ** get_address_of_redefined_13() { return &___redefined_13; }
	inline void set_redefined_13(XmlSchemaAttributeGroup_t491156493 * value)
	{
		___redefined_13 = value;
		Il2CppCodeGenWriteBarrier(&___redefined_13, value);
	}

	inline static int32_t get_offset_of_attributeUses_14() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___attributeUses_14)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeUses_14() const { return ___attributeUses_14; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeUses_14() { return &___attributeUses_14; }
	inline void set_attributeUses_14(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeUses_14 = value;
		Il2CppCodeGenWriteBarrier(&___attributeUses_14, value);
	}

	inline static int32_t get_offset_of_attributeWildcard_15() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___attributeWildcard_15)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_attributeWildcard_15() const { return ___attributeWildcard_15; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_attributeWildcard_15() { return &___attributeWildcard_15; }
	inline void set_attributeWildcard_15(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___attributeWildcard_15 = value;
		Il2CppCodeGenWriteBarrier(&___attributeWildcard_15, value);
	}

	inline static int32_t get_offset_of_selfReferenceCount_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___selfReferenceCount_16)); }
	inline int32_t get_selfReferenceCount_16() const { return ___selfReferenceCount_16; }
	inline int32_t* get_address_of_selfReferenceCount_16() { return &___selfReferenceCount_16; }
	inline void set_selfReferenceCount_16(int32_t value)
	{
		___selfReferenceCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
