﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_Binder3404612058.h"

// System.Predicate`1<System.Type>
struct Predicate_1_t4041740637;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DefaultBinder
struct  DefaultBinder_t424142959  : public Binder_t3404612058
{
public:

public:
};

struct DefaultBinder_t424142959_StaticFields
{
public:
	// System.Predicate`1<System.Type> System.DefaultBinder::<>f__am$cache0
	Predicate_1_t4041740637 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(DefaultBinder_t424142959_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Predicate_1_t4041740637 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Predicate_1_t4041740637 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Predicate_1_t4041740637 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
