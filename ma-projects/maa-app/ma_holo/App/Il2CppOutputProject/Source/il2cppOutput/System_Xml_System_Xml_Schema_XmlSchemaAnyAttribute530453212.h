﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"

// System.String
struct String_t;
// System.Xml.Schema.NamespaceList
struct NamespaceList_t848177191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnyAttribute
struct  XmlSchemaAnyAttribute_t530453212  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnyAttribute::ns
	String_t* ___ns_9;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAnyAttribute::processContents
	int32_t ___processContents_10;
	// System.Xml.Schema.NamespaceList System.Xml.Schema.XmlSchemaAnyAttribute::namespaceList
	NamespaceList_t848177191 * ___namespaceList_11;

public:
	inline static int32_t get_offset_of_ns_9() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___ns_9)); }
	inline String_t* get_ns_9() const { return ___ns_9; }
	inline String_t** get_address_of_ns_9() { return &___ns_9; }
	inline void set_ns_9(String_t* value)
	{
		___ns_9 = value;
		Il2CppCodeGenWriteBarrier(&___ns_9, value);
	}

	inline static int32_t get_offset_of_processContents_10() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___processContents_10)); }
	inline int32_t get_processContents_10() const { return ___processContents_10; }
	inline int32_t* get_address_of_processContents_10() { return &___processContents_10; }
	inline void set_processContents_10(int32_t value)
	{
		___processContents_10 = value;
	}

	inline static int32_t get_offset_of_namespaceList_11() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___namespaceList_11)); }
	inline NamespaceList_t848177191 * get_namespaceList_11() const { return ___namespaceList_11; }
	inline NamespaceList_t848177191 ** get_address_of_namespaceList_11() { return &___namespaceList_11; }
	inline void set_namespaceList_11(NamespaceList_t848177191 * value)
	{
		___namespaceList_11 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceList_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
