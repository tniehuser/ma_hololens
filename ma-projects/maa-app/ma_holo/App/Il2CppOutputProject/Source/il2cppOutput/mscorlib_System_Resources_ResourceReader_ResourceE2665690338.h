﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Resources.ResourceReader
struct ResourceReader_t2463923611;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceReader/ResourceEnumerator
struct  ResourceEnumerator_t2665690338  : public Il2CppObject
{
public:
	// System.Resources.ResourceReader System.Resources.ResourceReader/ResourceEnumerator::_reader
	ResourceReader_t2463923611 * ____reader_0;
	// System.Boolean System.Resources.ResourceReader/ResourceEnumerator::_currentIsValid
	bool ____currentIsValid_1;
	// System.Int32 System.Resources.ResourceReader/ResourceEnumerator::_currentName
	int32_t ____currentName_2;
	// System.Int32 System.Resources.ResourceReader/ResourceEnumerator::_dataPosition
	int32_t ____dataPosition_3;

public:
	inline static int32_t get_offset_of__reader_0() { return static_cast<int32_t>(offsetof(ResourceEnumerator_t2665690338, ____reader_0)); }
	inline ResourceReader_t2463923611 * get__reader_0() const { return ____reader_0; }
	inline ResourceReader_t2463923611 ** get_address_of__reader_0() { return &____reader_0; }
	inline void set__reader_0(ResourceReader_t2463923611 * value)
	{
		____reader_0 = value;
		Il2CppCodeGenWriteBarrier(&____reader_0, value);
	}

	inline static int32_t get_offset_of__currentIsValid_1() { return static_cast<int32_t>(offsetof(ResourceEnumerator_t2665690338, ____currentIsValid_1)); }
	inline bool get__currentIsValid_1() const { return ____currentIsValid_1; }
	inline bool* get_address_of__currentIsValid_1() { return &____currentIsValid_1; }
	inline void set__currentIsValid_1(bool value)
	{
		____currentIsValid_1 = value;
	}

	inline static int32_t get_offset_of__currentName_2() { return static_cast<int32_t>(offsetof(ResourceEnumerator_t2665690338, ____currentName_2)); }
	inline int32_t get__currentName_2() const { return ____currentName_2; }
	inline int32_t* get_address_of__currentName_2() { return &____currentName_2; }
	inline void set__currentName_2(int32_t value)
	{
		____currentName_2 = value;
	}

	inline static int32_t get_offset_of__dataPosition_3() { return static_cast<int32_t>(offsetof(ResourceEnumerator_t2665690338, ____dataPosition_3)); }
	inline int32_t get__dataPosition_3() const { return ____dataPosition_3; }
	inline int32_t* get_address_of__dataPosition_3() { return &____dataPosition_3; }
	inline void set__dataPosition_3(int32_t value)
	{
		____dataPosition_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
