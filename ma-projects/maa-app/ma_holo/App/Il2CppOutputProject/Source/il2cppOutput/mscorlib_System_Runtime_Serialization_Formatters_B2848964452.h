﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2877339122.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4279057407.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi414850623.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryArray
struct  BinaryArray_t2848964452  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::rank
	int32_t ___rank_1;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryArray::lengthA
	Int32U5BU5D_t3030399641* ___lengthA_2;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryArray::lowerBoundA
	Int32U5BU5D_t3030399641* ___lowerBoundA_3;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryTypeEnum
	int32_t ___binaryTypeEnum_4;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryArray::typeInformation
	Il2CppObject * ___typeInformation_5;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::assemId
	int32_t ___assemId_6;
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_7;
	// System.Runtime.Serialization.Formatters.Binary.BinaryArrayTypeEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryArrayTypeEnum
	int32_t ___binaryArrayTypeEnum_8;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_rank_1() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___rank_1)); }
	inline int32_t get_rank_1() const { return ___rank_1; }
	inline int32_t* get_address_of_rank_1() { return &___rank_1; }
	inline void set_rank_1(int32_t value)
	{
		___rank_1 = value;
	}

	inline static int32_t get_offset_of_lengthA_2() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___lengthA_2)); }
	inline Int32U5BU5D_t3030399641* get_lengthA_2() const { return ___lengthA_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_lengthA_2() { return &___lengthA_2; }
	inline void set_lengthA_2(Int32U5BU5D_t3030399641* value)
	{
		___lengthA_2 = value;
		Il2CppCodeGenWriteBarrier(&___lengthA_2, value);
	}

	inline static int32_t get_offset_of_lowerBoundA_3() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___lowerBoundA_3)); }
	inline Int32U5BU5D_t3030399641* get_lowerBoundA_3() const { return ___lowerBoundA_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_lowerBoundA_3() { return &___lowerBoundA_3; }
	inline void set_lowerBoundA_3(Int32U5BU5D_t3030399641* value)
	{
		___lowerBoundA_3 = value;
		Il2CppCodeGenWriteBarrier(&___lowerBoundA_3, value);
	}

	inline static int32_t get_offset_of_binaryTypeEnum_4() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___binaryTypeEnum_4)); }
	inline int32_t get_binaryTypeEnum_4() const { return ___binaryTypeEnum_4; }
	inline int32_t* get_address_of_binaryTypeEnum_4() { return &___binaryTypeEnum_4; }
	inline void set_binaryTypeEnum_4(int32_t value)
	{
		___binaryTypeEnum_4 = value;
	}

	inline static int32_t get_offset_of_typeInformation_5() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___typeInformation_5)); }
	inline Il2CppObject * get_typeInformation_5() const { return ___typeInformation_5; }
	inline Il2CppObject ** get_address_of_typeInformation_5() { return &___typeInformation_5; }
	inline void set_typeInformation_5(Il2CppObject * value)
	{
		___typeInformation_5 = value;
		Il2CppCodeGenWriteBarrier(&___typeInformation_5, value);
	}

	inline static int32_t get_offset_of_assemId_6() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___assemId_6)); }
	inline int32_t get_assemId_6() const { return ___assemId_6; }
	inline int32_t* get_address_of_assemId_6() { return &___assemId_6; }
	inline void set_assemId_6(int32_t value)
	{
		___assemId_6 = value;
	}

	inline static int32_t get_offset_of_binaryHeaderEnum_7() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___binaryHeaderEnum_7)); }
	inline int32_t get_binaryHeaderEnum_7() const { return ___binaryHeaderEnum_7; }
	inline int32_t* get_address_of_binaryHeaderEnum_7() { return &___binaryHeaderEnum_7; }
	inline void set_binaryHeaderEnum_7(int32_t value)
	{
		___binaryHeaderEnum_7 = value;
	}

	inline static int32_t get_offset_of_binaryArrayTypeEnum_8() { return static_cast<int32_t>(offsetof(BinaryArray_t2848964452, ___binaryArrayTypeEnum_8)); }
	inline int32_t get_binaryArrayTypeEnum_8() const { return ___binaryArrayTypeEnum_8; }
	inline int32_t* get_address_of_binaryArrayTypeEnum_8() { return &___binaryArrayTypeEnum_8; }
	inline void set_binaryArrayTypeEnum_8(int32_t value)
	{
		___binaryArrayTypeEnum_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
