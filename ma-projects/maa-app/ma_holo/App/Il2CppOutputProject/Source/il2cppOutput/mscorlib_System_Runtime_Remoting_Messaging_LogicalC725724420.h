﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type
struct Type_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2648008188;
// System.Runtime.Remoting.Messaging.CallContextSecurityData
struct CallContextSecurityData_t4091024823;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.LogicalCallContext
struct  LogicalCallContext_t725724420  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.LogicalCallContext::m_Datastore
	Hashtable_t909839986 * ___m_Datastore_1;
	// System.Runtime.Remoting.Messaging.CallContextRemotingData System.Runtime.Remoting.Messaging.LogicalCallContext::m_RemotingData
	CallContextRemotingData_t2648008188 * ___m_RemotingData_2;
	// System.Runtime.Remoting.Messaging.CallContextSecurityData System.Runtime.Remoting.Messaging.LogicalCallContext::m_SecurityData
	CallContextSecurityData_t4091024823 * ___m_SecurityData_3;
	// System.Object System.Runtime.Remoting.Messaging.LogicalCallContext::m_HostContext
	Il2CppObject * ___m_HostContext_4;
	// System.Boolean System.Runtime.Remoting.Messaging.LogicalCallContext::m_IsCorrelationMgr
	bool ___m_IsCorrelationMgr_5;

public:
	inline static int32_t get_offset_of_m_Datastore_1() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420, ___m_Datastore_1)); }
	inline Hashtable_t909839986 * get_m_Datastore_1() const { return ___m_Datastore_1; }
	inline Hashtable_t909839986 ** get_address_of_m_Datastore_1() { return &___m_Datastore_1; }
	inline void set_m_Datastore_1(Hashtable_t909839986 * value)
	{
		___m_Datastore_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Datastore_1, value);
	}

	inline static int32_t get_offset_of_m_RemotingData_2() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420, ___m_RemotingData_2)); }
	inline CallContextRemotingData_t2648008188 * get_m_RemotingData_2() const { return ___m_RemotingData_2; }
	inline CallContextRemotingData_t2648008188 ** get_address_of_m_RemotingData_2() { return &___m_RemotingData_2; }
	inline void set_m_RemotingData_2(CallContextRemotingData_t2648008188 * value)
	{
		___m_RemotingData_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_RemotingData_2, value);
	}

	inline static int32_t get_offset_of_m_SecurityData_3() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420, ___m_SecurityData_3)); }
	inline CallContextSecurityData_t4091024823 * get_m_SecurityData_3() const { return ___m_SecurityData_3; }
	inline CallContextSecurityData_t4091024823 ** get_address_of_m_SecurityData_3() { return &___m_SecurityData_3; }
	inline void set_m_SecurityData_3(CallContextSecurityData_t4091024823 * value)
	{
		___m_SecurityData_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_SecurityData_3, value);
	}

	inline static int32_t get_offset_of_m_HostContext_4() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420, ___m_HostContext_4)); }
	inline Il2CppObject * get_m_HostContext_4() const { return ___m_HostContext_4; }
	inline Il2CppObject ** get_address_of_m_HostContext_4() { return &___m_HostContext_4; }
	inline void set_m_HostContext_4(Il2CppObject * value)
	{
		___m_HostContext_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_HostContext_4, value);
	}

	inline static int32_t get_offset_of_m_IsCorrelationMgr_5() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420, ___m_IsCorrelationMgr_5)); }
	inline bool get_m_IsCorrelationMgr_5() const { return ___m_IsCorrelationMgr_5; }
	inline bool* get_address_of_m_IsCorrelationMgr_5() { return &___m_IsCorrelationMgr_5; }
	inline void set_m_IsCorrelationMgr_5(bool value)
	{
		___m_IsCorrelationMgr_5 = value;
	}
};

struct LogicalCallContext_t725724420_StaticFields
{
public:
	// System.Type System.Runtime.Remoting.Messaging.LogicalCallContext::s_callContextType
	Type_t * ___s_callContextType_0;

public:
	inline static int32_t get_offset_of_s_callContextType_0() { return static_cast<int32_t>(offsetof(LogicalCallContext_t725724420_StaticFields, ___s_callContextType_0)); }
	inline Type_t * get_s_callContextType_0() const { return ___s_callContextType_0; }
	inline Type_t ** get_address_of_s_callContextType_0() { return &___s_callContextType_0; }
	inline void set_s_callContextType_0(Type_t * value)
	{
		___s_callContextType_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_callContextType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
