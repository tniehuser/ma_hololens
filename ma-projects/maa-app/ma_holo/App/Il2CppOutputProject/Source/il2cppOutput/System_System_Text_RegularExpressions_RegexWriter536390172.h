﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexWriter
struct  RegexWriter_t536390172  : public Il2CppObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_intStack
	Int32U5BU5D_t3030399641* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_depth
	int32_t ____depth_1;
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_emitted
	Int32U5BU5D_t3030399641* ____emitted_2;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_curpos
	int32_t ____curpos_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Text.RegularExpressions.RegexWriter::_stringhash
	Dictionary_2_t3986656710 * ____stringhash_4;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexWriter::_stringtable
	List_1_t1398341365 * ____stringtable_5;
	// System.Boolean System.Text.RegularExpressions.RegexWriter::_counting
	bool ____counting_6;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_count
	int32_t ____count_7;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_trackcount
	int32_t ____trackcount_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexWriter::_caps
	Hashtable_t909839986 * ____caps_9;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____intStack_0)); }
	inline Int32U5BU5D_t3030399641* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t3030399641* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier(&____intStack_0, value);
	}

	inline static int32_t get_offset_of__depth_1() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____depth_1)); }
	inline int32_t get__depth_1() const { return ____depth_1; }
	inline int32_t* get_address_of__depth_1() { return &____depth_1; }
	inline void set__depth_1(int32_t value)
	{
		____depth_1 = value;
	}

	inline static int32_t get_offset_of__emitted_2() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____emitted_2)); }
	inline Int32U5BU5D_t3030399641* get__emitted_2() const { return ____emitted_2; }
	inline Int32U5BU5D_t3030399641** get_address_of__emitted_2() { return &____emitted_2; }
	inline void set__emitted_2(Int32U5BU5D_t3030399641* value)
	{
		____emitted_2 = value;
		Il2CppCodeGenWriteBarrier(&____emitted_2, value);
	}

	inline static int32_t get_offset_of__curpos_3() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____curpos_3)); }
	inline int32_t get__curpos_3() const { return ____curpos_3; }
	inline int32_t* get_address_of__curpos_3() { return &____curpos_3; }
	inline void set__curpos_3(int32_t value)
	{
		____curpos_3 = value;
	}

	inline static int32_t get_offset_of__stringhash_4() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____stringhash_4)); }
	inline Dictionary_2_t3986656710 * get__stringhash_4() const { return ____stringhash_4; }
	inline Dictionary_2_t3986656710 ** get_address_of__stringhash_4() { return &____stringhash_4; }
	inline void set__stringhash_4(Dictionary_2_t3986656710 * value)
	{
		____stringhash_4 = value;
		Il2CppCodeGenWriteBarrier(&____stringhash_4, value);
	}

	inline static int32_t get_offset_of__stringtable_5() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____stringtable_5)); }
	inline List_1_t1398341365 * get__stringtable_5() const { return ____stringtable_5; }
	inline List_1_t1398341365 ** get_address_of__stringtable_5() { return &____stringtable_5; }
	inline void set__stringtable_5(List_1_t1398341365 * value)
	{
		____stringtable_5 = value;
		Il2CppCodeGenWriteBarrier(&____stringtable_5, value);
	}

	inline static int32_t get_offset_of__counting_6() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____counting_6)); }
	inline bool get__counting_6() const { return ____counting_6; }
	inline bool* get_address_of__counting_6() { return &____counting_6; }
	inline void set__counting_6(bool value)
	{
		____counting_6 = value;
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__trackcount_8() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____trackcount_8)); }
	inline int32_t get__trackcount_8() const { return ____trackcount_8; }
	inline int32_t* get_address_of__trackcount_8() { return &____trackcount_8; }
	inline void set__trackcount_8(int32_t value)
	{
		____trackcount_8 = value;
	}

	inline static int32_t get_offset_of__caps_9() { return static_cast<int32_t>(offsetof(RegexWriter_t536390172, ____caps_9)); }
	inline Hashtable_t909839986 * get__caps_9() const { return ____caps_9; }
	inline Hashtable_t909839986 ** get_address_of__caps_9() { return &____caps_9; }
	inline void set__caps_9(Hashtable_t909839986 * value)
	{
		____caps_9 = value;
		Il2CppCodeGenWriteBarrier(&____caps_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
