﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_MatchEnumera3726517973.h"
#include "System_System_Text_RegularExpressions_RegexMatchTim197735700.h"
#include "System_System_Text_RegularExpressions_RegexNode2469392321.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "System_System_Text_RegularExpressions_RegexParser5944516.h"
#include "System_System_Text_RegularExpressions_RegexRunner3983612747.h"
#include "System_System_Text_RegularExpressions_RegexRunnerF3902733837.h"
#include "System_System_Text_RegularExpressions_RegexTree3175204897.h"
#include "System_System_Text_RegularExpressions_RegexWriter536390172.h"
#include "System_System_Text_RegularExpressions_CompiledRegex592995170.h"
#include "System_System_Text_RegularExpressions_NoParamDeleg2890182105.h"
#include "System_System_Text_RegularExpressions_FindFirstCha4203859986.h"
#include "System_System_Text_RegularExpressions_CompiledRege3379954222.h"
#include "System_System_ThrowHelper3666395978.h"
#include "System_System_ExceptionArgument2966871834.h"
#include "System_System_ExceptionResource2812258639.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictionar292699876.h"
#include "System_System_Collections_Specialized_ListDictiona2980500098.h"
#include "System_System_Collections_Specialized_ListDictionary79932263.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo4229094479.h"
#include "System_System_Collections_Specialized_NameObjectCo1857758119.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_CompatibleCom452154975.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_OrderedDiction14805809.h"
#include "System_System_Collections_Specialized_OrderedDicti3156431966.h"
#include "System_System_Collections_Specialized_OrderedDicti3490502636.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArraySubsetEnumerator764103185.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_AttributeCollection_At168441916.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"
#include "System_System_ComponentModel_BooleanConverter284715810.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_ByteConverter1265255600.h"
#include "System_System_ComponentModel_CharConverter437233350.h"
#include "System_System_ComponentModel_CollectionConverter2459375096.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_CultureInfoConverter2239982248.h"
#include "System_System_ComponentModel_CultureInfoConverter_1185978443.h"
#include "System_System_ComponentModel_CultureInfoConverter_Cu26447631.h"
#include "System_System_ComponentModel_CustomTypeDescriptor1720788626.h"
#include "System_System_ComponentModel_DateTimeConverter2436647419.h"
#include "System_System_ComponentModel_DateTimeOffsetConvert2176982818.h"
#include "System_System_ComponentModel_DecimalConverter1618403211.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DelegatingTypeDescrip1537564179.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672.h"
#include "System_System_ComponentModel_Design_Serialization_2188593799.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_Serialization_1162957127.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_DoubleConverter864652623.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EnumConverter2538808523.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"
#include "System_System_ComponentModel_EventDescriptorCollec3053042509.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_EventHandlerList_ListE385037026.h"
#include "System_System_ComponentModel_ExtenderProvidedPrope3223729015.h"
#include "System_System_ComponentModel_GuidConverter1547586607.h"
#include "System_System_ComponentModel_InstallerTypeAttribut2978264484.h"
#include "System_System_ComponentModel_Int16Converter903627590.h"
#include "System_System_ComponentModel_Int32Converter957938388.h"
#include "System_System_ComponentModel_Int64Converter3186343659.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (MatchEnumerator_t3726517973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[4] = 
{
	MatchEnumerator_t3726517973::get_offset_of__matchcoll_0(),
	MatchEnumerator_t3726517973::get_offset_of__match_1(),
	MatchEnumerator_t3726517973::get_offset_of__curindex_2(),
	MatchEnumerator_t3726517973::get_offset_of__done_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (RegexMatchTimeoutException_t197735700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[3] = 
{
	RegexMatchTimeoutException_t197735700::get_offset_of_regexInput_16(),
	RegexMatchTimeoutException_t197735700::get_offset_of_regexPattern_17(),
	RegexMatchTimeoutException_t197735700::get_offset_of_matchTimeout_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (RegexNode_t2469392321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[8] = 
{
	RegexNode_t2469392321::get_offset_of__type_0(),
	RegexNode_t2469392321::get_offset_of__children_1(),
	RegexNode_t2469392321::get_offset_of__str_2(),
	RegexNode_t2469392321::get_offset_of__ch_3(),
	RegexNode_t2469392321::get_offset_of__m_4(),
	RegexNode_t2469392321::get_offset_of__n_5(),
	RegexNode_t2469392321::get_offset_of__options_6(),
	RegexNode_t2469392321::get_offset_of__next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (RegexOptions_t2418259727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[11] = 
{
	RegexOptions_t2418259727::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (RegexParser_t5944516), -1, sizeof(RegexParser_t5944516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[20] = 
{
	RegexParser_t5944516::get_offset_of__stack_0(),
	RegexParser_t5944516::get_offset_of__group_1(),
	RegexParser_t5944516::get_offset_of__alternation_2(),
	RegexParser_t5944516::get_offset_of__concatenation_3(),
	RegexParser_t5944516::get_offset_of__unit_4(),
	RegexParser_t5944516::get_offset_of__pattern_5(),
	RegexParser_t5944516::get_offset_of__currentPos_6(),
	RegexParser_t5944516::get_offset_of__culture_7(),
	RegexParser_t5944516::get_offset_of__autocap_8(),
	RegexParser_t5944516::get_offset_of__capcount_9(),
	RegexParser_t5944516::get_offset_of__captop_10(),
	RegexParser_t5944516::get_offset_of__capsize_11(),
	RegexParser_t5944516::get_offset_of__caps_12(),
	RegexParser_t5944516::get_offset_of__capnames_13(),
	RegexParser_t5944516::get_offset_of__capnumlist_14(),
	RegexParser_t5944516::get_offset_of__capnamelist_15(),
	RegexParser_t5944516::get_offset_of__options_16(),
	RegexParser_t5944516::get_offset_of__optionsStack_17(),
	RegexParser_t5944516::get_offset_of__ignoreNextParen_18(),
	RegexParser_t5944516_StaticFields::get_offset_of__category_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (RegexRunner_t3983612747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[18] = 
{
	RegexRunner_t3983612747::get_offset_of_runtextbeg_0(),
	RegexRunner_t3983612747::get_offset_of_runtextend_1(),
	RegexRunner_t3983612747::get_offset_of_runtextstart_2(),
	RegexRunner_t3983612747::get_offset_of_runtext_3(),
	RegexRunner_t3983612747::get_offset_of_runtextpos_4(),
	RegexRunner_t3983612747::get_offset_of_runtrack_5(),
	RegexRunner_t3983612747::get_offset_of_runtrackpos_6(),
	RegexRunner_t3983612747::get_offset_of_runstack_7(),
	RegexRunner_t3983612747::get_offset_of_runstackpos_8(),
	RegexRunner_t3983612747::get_offset_of_runcrawl_9(),
	RegexRunner_t3983612747::get_offset_of_runcrawlpos_10(),
	RegexRunner_t3983612747::get_offset_of_runtrackcount_11(),
	RegexRunner_t3983612747::get_offset_of_runmatch_12(),
	RegexRunner_t3983612747::get_offset_of_runregex_13(),
	RegexRunner_t3983612747::get_offset_of_timeout_14(),
	RegexRunner_t3983612747::get_offset_of_ignoreTimeout_15(),
	RegexRunner_t3983612747::get_offset_of_timeoutOccursAt_16(),
	RegexRunner_t3983612747::get_offset_of_timeoutChecksToSkip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (RegexRunnerFactory_t3902733837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (RegexTree_t3175204897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[7] = 
{
	RegexTree_t3175204897::get_offset_of__root_0(),
	RegexTree_t3175204897::get_offset_of__caps_1(),
	RegexTree_t3175204897::get_offset_of__capnumlist_2(),
	RegexTree_t3175204897::get_offset_of__capnames_3(),
	RegexTree_t3175204897::get_offset_of__capslist_4(),
	RegexTree_t3175204897::get_offset_of__options_5(),
	RegexTree_t3175204897::get_offset_of__captop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (RegexWriter_t536390172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[10] = 
{
	RegexWriter_t536390172::get_offset_of__intStack_0(),
	RegexWriter_t536390172::get_offset_of__depth_1(),
	RegexWriter_t536390172::get_offset_of__emitted_2(),
	RegexWriter_t536390172::get_offset_of__curpos_3(),
	RegexWriter_t536390172::get_offset_of__stringhash_4(),
	RegexWriter_t536390172::get_offset_of__stringtable_5(),
	RegexWriter_t536390172::get_offset_of__counting_6(),
	RegexWriter_t536390172::get_offset_of__count_7(),
	RegexWriter_t536390172::get_offset_of__trackcount_8(),
	RegexWriter_t536390172::get_offset_of__caps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (CompiledRegexRunner_t592995170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	CompiledRegexRunner_t592995170::get_offset_of_goMethod_18(),
	CompiledRegexRunner_t592995170::get_offset_of_findFirstCharMethod_19(),
	CompiledRegexRunner_t592995170::get_offset_of_initTrackCountMethod_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (NoParamDelegate_t2890182105), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (FindFirstCharDelegate_t4203859986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (CompiledRegexRunnerFactory_t3379954222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[3] = 
{
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_goMethod_0(),
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_findFirstCharMethod_1(),
	CompiledRegexRunnerFactory_t3379954222::get_offset_of_initTrackCountMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (ThrowHelper_t3666395979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (ExceptionArgument_t2966871835)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2024[18] = 
{
	ExceptionArgument_t2966871835::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (ExceptionResource_t2812258640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[27] = 
{
	ExceptionResource_t2812258640::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[3] = 
{
	HybridDictionary_t290043810::get_offset_of_list_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[5] = 
{
	ListDictionary_t3458713452::get_offset_of_head_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_count_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
	ListDictionary_t3458713452::get_offset_of__syncRoot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (NodeEnumerator_t292699876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[4] = 
{
	NodeEnumerator_t292699876::get_offset_of_list_0(),
	NodeEnumerator_t292699876::get_offset_of_current_1(),
	NodeEnumerator_t292699876::get_offset_of_version_2(),
	NodeEnumerator_t292699876::get_offset_of_start_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (NodeKeyValueCollection_t2980500098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	NodeKeyValueCollection_t2980500098::get_offset_of_list_0(),
	NodeKeyValueCollection_t2980500098::get_offset_of_isKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (NodeKeyValueEnumerator_t79932263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[5] = 
{
	NodeKeyValueEnumerator_t79932263::get_offset_of_list_0(),
	NodeKeyValueEnumerator_t79932263::get_offset_of_current_1(),
	NodeKeyValueEnumerator_t79932263::get_offset_of_version_2(),
	NodeKeyValueEnumerator_t79932263::get_offset_of_isKeys_3(),
	NodeKeyValueEnumerator_t79932263::get_offset_of_start_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (NameObjectCollectionBase_t2034248631), -1, sizeof(NameObjectCollectionBase_t2034248631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2033[11] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of__readOnly_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of__entriesArray_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of__keyComparer_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of__entriesTable_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of__nullKeyEntry_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of__keys_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of__serializationInfo_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of__version_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of__syncRoot_8(),
	NameObjectCollectionBase_t2034248631_StaticFields::get_offset_of_defaultComparer_9(),
	NameObjectCollectionBase_t2034248631_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (NameObjectEntry_t4229094479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[2] = 
{
	NameObjectEntry_t4229094479::get_offset_of_Key_0(),
	NameObjectEntry_t4229094479::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (NameObjectKeysEnumerator_t1857758119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[3] = 
{
	NameObjectKeysEnumerator_t1857758119::get_offset_of__pos_0(),
	NameObjectKeysEnumerator_t1857758119::get_offset_of__coll_1(),
	NameObjectKeysEnumerator_t1857758119::get_offset_of__version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[1] = 
{
	KeysCollection_t633582367::get_offset_of__coll_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (CompatibleComparer_t452154975), -1, sizeof(CompatibleComparer_t452154975_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2037[4] = 
{
	CompatibleComparer_t452154975::get_offset_of__comparer_0(),
	CompatibleComparer_t452154975_StaticFields::get_offset_of_defaultComparer_1(),
	CompatibleComparer_t452154975::get_offset_of__hcp_2(),
	CompatibleComparer_t452154975_StaticFields::get_offset_of_defaultHashProvider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[2] = 
{
	NameValueCollection_t3047564564::get_offset_of__all_11(),
	NameValueCollection_t3047564564::get_offset_of__allKeys_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (OrderedDictionary_t14805809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[7] = 
{
	OrderedDictionary_t14805809::get_offset_of__objectsArray_0(),
	OrderedDictionary_t14805809::get_offset_of__objectsTable_1(),
	OrderedDictionary_t14805809::get_offset_of__initialCapacity_2(),
	OrderedDictionary_t14805809::get_offset_of__comparer_3(),
	OrderedDictionary_t14805809::get_offset_of__readOnly_4(),
	OrderedDictionary_t14805809::get_offset_of__syncRoot_5(),
	OrderedDictionary_t14805809::get_offset_of__siInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (OrderedDictionaryEnumerator_t3156431966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[2] = 
{
	OrderedDictionaryEnumerator_t3156431966::get_offset_of__objectReturnType_0(),
	OrderedDictionaryEnumerator_t3156431966::get_offset_of_arrayEnumerator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (OrderedDictionaryKeyValueCollection_t3490502636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[2] = 
{
	OrderedDictionaryKeyValueCollection_t3490502636::get_offset_of__objects_0(),
	OrderedDictionaryKeyValueCollection_t3490502636::get_offset_of_isKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (ArraySubsetEnumerator_t764103185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	ArraySubsetEnumerator_t764103185::get_offset_of_array_0(),
	ArraySubsetEnumerator_t764103185::get_offset_of_total_1(),
	ArraySubsetEnumerator_t764103185::get_offset_of_current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2046[6] = 
{
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of__defaultAttributes_1(),
	AttributeCollection_t1925812292::get_offset_of__attributes_2(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_internalSyncObject_3(),
	AttributeCollection_t1925812292::get_offset_of__foundAttributeTypes_4(),
	AttributeCollection_t1925812292::get_offset_of__index_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (AttributeEntry_t168441916)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[2] = 
{
	AttributeEntry_t168441916::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AttributeEntry_t168441916::get_offset_of_index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (BooleanConverter_t284715810), -1, sizeof(BooleanConverter_t284715810_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	BooleanConverter_t284715810_StaticFields::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (BrowsableAttribute_t2487167291), -1, sizeof(BrowsableAttribute_t2487167291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Yes_0(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_No_1(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Default_2(),
	BrowsableAttribute_t2487167291::get_offset_of_browsable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ByteConverter_t1265255600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (CharConverter_t437233350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (CollectionConverter_t2459375096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Component_t2826673791), -1, sizeof(Component_t2826673791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2054[3] = 
{
	Component_t2826673791_StaticFields::get_offset_of_EventDisposed_1(),
	Component_t2826673791::get_offset_of_site_2(),
	Component_t2826673791::get_offset_of_events_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (ComponentCollection_t737017907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (CultureInfoConverter_t2239982248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	CultureInfoConverter_t2239982248::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (CultureComparer_t1185978443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	CultureComparer_t1185978443::get_offset_of_converter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (CultureInfoMapper_t26447631), -1, sizeof(CultureInfoMapper_t26447631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2059[1] = 
{
	CultureInfoMapper_t26447631_StaticFields::get_offset_of_cultureInfoNameMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (CustomTypeDescriptor_t1720788626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[1] = 
{
	CustomTypeDescriptor_t1720788626::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (DateTimeConverter_t2436647419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (DateTimeOffsetConverter_t2176982818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (DecimalConverter_t1618403211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (DefaultEventAttribute_t1079704873), -1, sizeof(DefaultEventAttribute_t1079704873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[2] = 
{
	DefaultEventAttribute_t1079704873::get_offset_of_name_0(),
	DefaultEventAttribute_t1079704873_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (DelegatingTypeDescriptionProvider_t1537564179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[1] = 
{
	DelegatingTypeDescriptionProvider_t1537564179::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (DescriptionAttribute_t3207779672), -1, sizeof(DescriptionAttribute_t3207779672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2067[2] = 
{
	DescriptionAttribute_t3207779672_StaticFields::get_offset_of_Default_0(),
	DescriptionAttribute_t3207779672::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (DesignerSerializerAttribute_t2188593799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[3] = 
{
	DesignerSerializerAttribute_t2188593799::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2188593799::get_offset_of_serializerBaseTypeName_1(),
	DesignerSerializerAttribute_t2188593799::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (RootDesignerSerializerAttribute_t1162957127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[4] = 
{
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_reloadable_0(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_serializerTypeName_1(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_serializerBaseTypeName_2(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_typeId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[3] = 
{
	DesignerAttribute_t2778719479::get_offset_of_designerTypeName_0(),
	DesignerAttribute_t2778719479::get_offset_of_designerBaseTypeName_1(),
	DesignerAttribute_t2778719479::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (DesignerCategoryAttribute_t1270090451), -1, sizeof(DesignerCategoryAttribute_t1270090451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2077[6] = 
{
	DesignerCategoryAttribute_t1270090451::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1270090451::get_offset_of_typeId_1(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Component_2(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Default_3(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Form_4(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Generic_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (DesignerSerializationVisibility_t3751360903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2078[4] = 
{
	DesignerSerializationVisibility_t3751360903::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (DesignerSerializationVisibilityAttribute_t2980019899), -1, sizeof(DesignerSerializationVisibilityAttribute_t2980019899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2079[5] = 
{
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Content_0(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Hidden_1(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Visible_2(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Default_3(),
	DesignerSerializationVisibilityAttribute_t2980019899::get_offset_of_visibility_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (DoubleConverter_t864652623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_browsableState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (EnumConverter_t2538808523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[2] = 
{
	EnumConverter_t2538808523::get_offset_of_values_4(),
	EnumConverter_t2538808523::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (EventDescriptor_t962731901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (EventDescriptorCollection_t3053042509), -1, sizeof(EventDescriptorCollection_t3053042509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[8] = 
{
	EventDescriptorCollection_t3053042509::get_offset_of_events_0(),
	EventDescriptorCollection_t3053042509::get_offset_of_namedSort_1(),
	EventDescriptorCollection_t3053042509::get_offset_of_comparer_2(),
	EventDescriptorCollection_t3053042509::get_offset_of_eventsOwned_3(),
	EventDescriptorCollection_t3053042509::get_offset_of_needSort_4(),
	EventDescriptorCollection_t3053042509::get_offset_of_eventCount_5(),
	EventDescriptorCollection_t3053042509::get_offset_of_readOnly_6(),
	EventDescriptorCollection_t3053042509_StaticFields::get_offset_of_Empty_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (EventHandlerList_t1298116880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[2] = 
{
	EventHandlerList_t1298116880::get_offset_of_head_0(),
	EventHandlerList_t1298116880::get_offset_of_parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (ListEntry_t385037026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[3] = 
{
	ListEntry_t385037026::get_offset_of_next_0(),
	ListEntry_t385037026::get_offset_of_key_1(),
	ListEntry_t385037026::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (ExtenderProvidedPropertyAttribute_t3223729015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[3] = 
{
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_extenderProperty_0(),
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_provider_1(),
	ExtenderProvidedPropertyAttribute_t3223729015::get_offset_of_receiverType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (GuidConverter_t1547586607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (InstallerTypeAttribute_t2978264484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[1] = 
{
	InstallerTypeAttribute_t2978264484::get_offset_of__typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (Int16Converter_t903627590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Int32Converter_t957938388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Int64Converter_t3186343659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
