﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t3950677696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncResult/<Complete>c__AnonStorey0
struct  U3CCompleteU3Ec__AnonStorey0_t3411761453  : public Il2CppObject
{
public:
	// System.AsyncCallback System.Net.Sockets.SocketAsyncResult/<Complete>c__AnonStorey0::callback
	AsyncCallback_t163412349 * ___callback_0;
	// System.Net.Sockets.SocketAsyncResult System.Net.Sockets.SocketAsyncResult/<Complete>c__AnonStorey0::$this
	SocketAsyncResult_t3950677696 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CCompleteU3Ec__AnonStorey0_t3411761453, ___callback_0)); }
	inline AsyncCallback_t163412349 * get_callback_0() const { return ___callback_0; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AsyncCallback_t163412349 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCompleteU3Ec__AnonStorey0_t3411761453, ___U24this_1)); }
	inline SocketAsyncResult_t3950677696 * get_U24this_1() const { return ___U24this_1; }
	inline SocketAsyncResult_t3950677696 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SocketAsyncResult_t3950677696 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
