﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData>
struct Queue_1_t1206146221;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.Threading.ThreadStart
struct ThreadStart_t3437517264;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork
struct  CFNetwork_t2805084167  : public Il2CppObject
{
public:

public:
};

struct CFNetwork_t2805084167_StaticFields
{
public:
	// System.Object Mono.Net.CFNetwork::lock_obj
	Il2CppObject * ___lock_obj_0;
	// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData> Mono.Net.CFNetwork::get_proxy_queue
	Queue_1_t1206146221 * ___get_proxy_queue_1;
	// System.Threading.AutoResetEvent Mono.Net.CFNetwork::proxy_event
	AutoResetEvent_t15112628 * ___proxy_event_2;
	// System.Threading.ThreadStart Mono.Net.CFNetwork::<>f__mg$cache0
	ThreadStart_t3437517264 * ___U3CU3Ef__mgU24cache0_3;

public:
	inline static int32_t get_offset_of_lock_obj_0() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___lock_obj_0)); }
	inline Il2CppObject * get_lock_obj_0() const { return ___lock_obj_0; }
	inline Il2CppObject ** get_address_of_lock_obj_0() { return &___lock_obj_0; }
	inline void set_lock_obj_0(Il2CppObject * value)
	{
		___lock_obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___lock_obj_0, value);
	}

	inline static int32_t get_offset_of_get_proxy_queue_1() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___get_proxy_queue_1)); }
	inline Queue_1_t1206146221 * get_get_proxy_queue_1() const { return ___get_proxy_queue_1; }
	inline Queue_1_t1206146221 ** get_address_of_get_proxy_queue_1() { return &___get_proxy_queue_1; }
	inline void set_get_proxy_queue_1(Queue_1_t1206146221 * value)
	{
		___get_proxy_queue_1 = value;
		Il2CppCodeGenWriteBarrier(&___get_proxy_queue_1, value);
	}

	inline static int32_t get_offset_of_proxy_event_2() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___proxy_event_2)); }
	inline AutoResetEvent_t15112628 * get_proxy_event_2() const { return ___proxy_event_2; }
	inline AutoResetEvent_t15112628 ** get_address_of_proxy_event_2() { return &___proxy_event_2; }
	inline void set_proxy_event_2(AutoResetEvent_t15112628 * value)
	{
		___proxy_event_2 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_event_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ThreadStart_t3437517264 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ThreadStart_t3437517264 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ThreadStart_t3437517264 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
