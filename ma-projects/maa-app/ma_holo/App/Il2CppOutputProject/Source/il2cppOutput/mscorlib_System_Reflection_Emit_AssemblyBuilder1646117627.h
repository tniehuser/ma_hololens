﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "mscorlib_System_UIntPtr1549717846.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds4139237606.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds3142660980.h"
#include "mscorlib_System_Reflection_ImageFileMachine128670190.h"
#include "mscorlib_System_Reflection_Emit_NativeResourceType548430477.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t3642333382;
// System.String
struct String_t;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t3865978872;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Reflection.Module[]
struct ModuleU5BU5D_t3593287923;
// System.Reflection.Emit.MonoWin32Resource[]
struct MonoWin32ResourceU5BU5D_t3763542575;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t3643576428;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Type
struct Type_t;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Resources.Win32VersionResource
struct Win32VersionResource_t548350325;
// Mono.Security.StrongName
struct StrongName_t117835354;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t4156028127;
struct MonoResource_t3127387157_marshaled_pinvoke;
struct MonoWin32Resource_t2467306218_marshaled_pinvoke;
struct RefEmitPermissionSet_t2708608433_marshaled_pinvoke;
struct ModuleBuilder_t4156028127_marshaled_pinvoke;
struct MonoResource_t3127387157_marshaled_com;
struct MonoWin32Resource_t2467306218_marshaled_com;
struct RefEmitPermissionSet_t2708608433_marshaled_com;
struct ModuleBuilder_t4156028127_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.AssemblyBuilder
struct  AssemblyBuilder_t1646117627  : public Assembly_t4268412390
{
public:
	// System.UIntPtr System.Reflection.Emit.AssemblyBuilder::dynamic_assembly
	UIntPtr_t  ___dynamic_assembly_10;
	// System.Reflection.MethodInfo System.Reflection.Emit.AssemblyBuilder::entry_point
	MethodInfo_t * ___entry_point_11;
	// System.Reflection.Emit.ModuleBuilder[] System.Reflection.Emit.AssemblyBuilder::modules
	ModuleBuilderU5BU5D_t3642333382* ___modules_12;
	// System.String System.Reflection.Emit.AssemblyBuilder::name
	String_t* ___name_13;
	// System.String System.Reflection.Emit.AssemblyBuilder::dir
	String_t* ___dir_14;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.AssemblyBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_15;
	// System.Reflection.Emit.MonoResource[] System.Reflection.Emit.AssemblyBuilder::resources
	MonoResourceU5BU5D_t3865978872* ___resources_16;
	// System.Byte[] System.Reflection.Emit.AssemblyBuilder::public_key
	ByteU5BU5D_t3397334013* ___public_key_17;
	// System.String System.Reflection.Emit.AssemblyBuilder::version
	String_t* ___version_18;
	// System.String System.Reflection.Emit.AssemblyBuilder::culture
	String_t* ___culture_19;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::algid
	uint32_t ___algid_20;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::flags
	uint32_t ___flags_21;
	// System.Reflection.Emit.PEFileKinds System.Reflection.Emit.AssemblyBuilder::pekind
	int32_t ___pekind_22;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::delay_sign
	bool ___delay_sign_23;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::access
	uint32_t ___access_24;
	// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::loaded_modules
	ModuleU5BU5D_t3593287923* ___loaded_modules_25;
	// System.Reflection.Emit.MonoWin32Resource[] System.Reflection.Emit.AssemblyBuilder::win32_resources
	MonoWin32ResourceU5BU5D_t3763542575* ___win32_resources_26;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_minimum
	RefEmitPermissionSetU5BU5D_t3643576428* ___permissions_minimum_27;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_optional
	RefEmitPermissionSetU5BU5D_t3643576428* ___permissions_optional_28;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_refused
	RefEmitPermissionSetU5BU5D_t3643576428* ___permissions_refused_29;
	// System.Reflection.PortableExecutableKinds System.Reflection.Emit.AssemblyBuilder::peKind
	int32_t ___peKind_30;
	// System.Reflection.ImageFileMachine System.Reflection.Emit.AssemblyBuilder::machine
	int32_t ___machine_31;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::corlib_internal
	bool ___corlib_internal_32;
	// System.Type[] System.Reflection.Emit.AssemblyBuilder::type_forwarders
	TypeU5BU5D_t1664964607* ___type_forwarders_33;
	// System.Byte[] System.Reflection.Emit.AssemblyBuilder::pktoken
	ByteU5BU5D_t3397334013* ___pktoken_34;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_object_type
	Type_t * ___corlib_object_type_35;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_value_type
	Type_t * ___corlib_value_type_36;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_enum_type
	Type_t * ___corlib_enum_type_37;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_void_type
	Type_t * ___corlib_void_type_38;
	// System.Collections.ArrayList System.Reflection.Emit.AssemblyBuilder::resource_writers
	ArrayList_t4252133567 * ___resource_writers_39;
	// System.Resources.Win32VersionResource System.Reflection.Emit.AssemblyBuilder::version_res
	Win32VersionResource_t548350325 * ___version_res_40;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::created
	bool ___created_41;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::is_module_only
	bool ___is_module_only_42;
	// Mono.Security.StrongName System.Reflection.Emit.AssemblyBuilder::sn
	StrongName_t117835354 * ___sn_43;
	// System.Reflection.Emit.NativeResourceType System.Reflection.Emit.AssemblyBuilder::native_resource
	int32_t ___native_resource_44;
	// System.String System.Reflection.Emit.AssemblyBuilder::versioninfo_culture
	String_t* ___versioninfo_culture_45;
	// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::manifest_module
	ModuleBuilder_t4156028127 * ___manifest_module_46;

public:
	inline static int32_t get_offset_of_dynamic_assembly_10() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___dynamic_assembly_10)); }
	inline UIntPtr_t  get_dynamic_assembly_10() const { return ___dynamic_assembly_10; }
	inline UIntPtr_t * get_address_of_dynamic_assembly_10() { return &___dynamic_assembly_10; }
	inline void set_dynamic_assembly_10(UIntPtr_t  value)
	{
		___dynamic_assembly_10 = value;
	}

	inline static int32_t get_offset_of_entry_point_11() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___entry_point_11)); }
	inline MethodInfo_t * get_entry_point_11() const { return ___entry_point_11; }
	inline MethodInfo_t ** get_address_of_entry_point_11() { return &___entry_point_11; }
	inline void set_entry_point_11(MethodInfo_t * value)
	{
		___entry_point_11 = value;
		Il2CppCodeGenWriteBarrier(&___entry_point_11, value);
	}

	inline static int32_t get_offset_of_modules_12() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___modules_12)); }
	inline ModuleBuilderU5BU5D_t3642333382* get_modules_12() const { return ___modules_12; }
	inline ModuleBuilderU5BU5D_t3642333382** get_address_of_modules_12() { return &___modules_12; }
	inline void set_modules_12(ModuleBuilderU5BU5D_t3642333382* value)
	{
		___modules_12 = value;
		Il2CppCodeGenWriteBarrier(&___modules_12, value);
	}

	inline static int32_t get_offset_of_name_13() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___name_13)); }
	inline String_t* get_name_13() const { return ___name_13; }
	inline String_t** get_address_of_name_13() { return &___name_13; }
	inline void set_name_13(String_t* value)
	{
		___name_13 = value;
		Il2CppCodeGenWriteBarrier(&___name_13, value);
	}

	inline static int32_t get_offset_of_dir_14() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___dir_14)); }
	inline String_t* get_dir_14() const { return ___dir_14; }
	inline String_t** get_address_of_dir_14() { return &___dir_14; }
	inline void set_dir_14(String_t* value)
	{
		___dir_14 = value;
		Il2CppCodeGenWriteBarrier(&___dir_14, value);
	}

	inline static int32_t get_offset_of_cattrs_15() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___cattrs_15)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_15() const { return ___cattrs_15; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_15() { return &___cattrs_15; }
	inline void set_cattrs_15(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_15 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_15, value);
	}

	inline static int32_t get_offset_of_resources_16() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___resources_16)); }
	inline MonoResourceU5BU5D_t3865978872* get_resources_16() const { return ___resources_16; }
	inline MonoResourceU5BU5D_t3865978872** get_address_of_resources_16() { return &___resources_16; }
	inline void set_resources_16(MonoResourceU5BU5D_t3865978872* value)
	{
		___resources_16 = value;
		Il2CppCodeGenWriteBarrier(&___resources_16, value);
	}

	inline static int32_t get_offset_of_public_key_17() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___public_key_17)); }
	inline ByteU5BU5D_t3397334013* get_public_key_17() const { return ___public_key_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_public_key_17() { return &___public_key_17; }
	inline void set_public_key_17(ByteU5BU5D_t3397334013* value)
	{
		___public_key_17 = value;
		Il2CppCodeGenWriteBarrier(&___public_key_17, value);
	}

	inline static int32_t get_offset_of_version_18() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___version_18)); }
	inline String_t* get_version_18() const { return ___version_18; }
	inline String_t** get_address_of_version_18() { return &___version_18; }
	inline void set_version_18(String_t* value)
	{
		___version_18 = value;
		Il2CppCodeGenWriteBarrier(&___version_18, value);
	}

	inline static int32_t get_offset_of_culture_19() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___culture_19)); }
	inline String_t* get_culture_19() const { return ___culture_19; }
	inline String_t** get_address_of_culture_19() { return &___culture_19; }
	inline void set_culture_19(String_t* value)
	{
		___culture_19 = value;
		Il2CppCodeGenWriteBarrier(&___culture_19, value);
	}

	inline static int32_t get_offset_of_algid_20() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___algid_20)); }
	inline uint32_t get_algid_20() const { return ___algid_20; }
	inline uint32_t* get_address_of_algid_20() { return &___algid_20; }
	inline void set_algid_20(uint32_t value)
	{
		___algid_20 = value;
	}

	inline static int32_t get_offset_of_flags_21() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___flags_21)); }
	inline uint32_t get_flags_21() const { return ___flags_21; }
	inline uint32_t* get_address_of_flags_21() { return &___flags_21; }
	inline void set_flags_21(uint32_t value)
	{
		___flags_21 = value;
	}

	inline static int32_t get_offset_of_pekind_22() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___pekind_22)); }
	inline int32_t get_pekind_22() const { return ___pekind_22; }
	inline int32_t* get_address_of_pekind_22() { return &___pekind_22; }
	inline void set_pekind_22(int32_t value)
	{
		___pekind_22 = value;
	}

	inline static int32_t get_offset_of_delay_sign_23() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___delay_sign_23)); }
	inline bool get_delay_sign_23() const { return ___delay_sign_23; }
	inline bool* get_address_of_delay_sign_23() { return &___delay_sign_23; }
	inline void set_delay_sign_23(bool value)
	{
		___delay_sign_23 = value;
	}

	inline static int32_t get_offset_of_access_24() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___access_24)); }
	inline uint32_t get_access_24() const { return ___access_24; }
	inline uint32_t* get_address_of_access_24() { return &___access_24; }
	inline void set_access_24(uint32_t value)
	{
		___access_24 = value;
	}

	inline static int32_t get_offset_of_loaded_modules_25() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___loaded_modules_25)); }
	inline ModuleU5BU5D_t3593287923* get_loaded_modules_25() const { return ___loaded_modules_25; }
	inline ModuleU5BU5D_t3593287923** get_address_of_loaded_modules_25() { return &___loaded_modules_25; }
	inline void set_loaded_modules_25(ModuleU5BU5D_t3593287923* value)
	{
		___loaded_modules_25 = value;
		Il2CppCodeGenWriteBarrier(&___loaded_modules_25, value);
	}

	inline static int32_t get_offset_of_win32_resources_26() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___win32_resources_26)); }
	inline MonoWin32ResourceU5BU5D_t3763542575* get_win32_resources_26() const { return ___win32_resources_26; }
	inline MonoWin32ResourceU5BU5D_t3763542575** get_address_of_win32_resources_26() { return &___win32_resources_26; }
	inline void set_win32_resources_26(MonoWin32ResourceU5BU5D_t3763542575* value)
	{
		___win32_resources_26 = value;
		Il2CppCodeGenWriteBarrier(&___win32_resources_26, value);
	}

	inline static int32_t get_offset_of_permissions_minimum_27() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___permissions_minimum_27)); }
	inline RefEmitPermissionSetU5BU5D_t3643576428* get_permissions_minimum_27() const { return ___permissions_minimum_27; }
	inline RefEmitPermissionSetU5BU5D_t3643576428** get_address_of_permissions_minimum_27() { return &___permissions_minimum_27; }
	inline void set_permissions_minimum_27(RefEmitPermissionSetU5BU5D_t3643576428* value)
	{
		___permissions_minimum_27 = value;
		Il2CppCodeGenWriteBarrier(&___permissions_minimum_27, value);
	}

	inline static int32_t get_offset_of_permissions_optional_28() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___permissions_optional_28)); }
	inline RefEmitPermissionSetU5BU5D_t3643576428* get_permissions_optional_28() const { return ___permissions_optional_28; }
	inline RefEmitPermissionSetU5BU5D_t3643576428** get_address_of_permissions_optional_28() { return &___permissions_optional_28; }
	inline void set_permissions_optional_28(RefEmitPermissionSetU5BU5D_t3643576428* value)
	{
		___permissions_optional_28 = value;
		Il2CppCodeGenWriteBarrier(&___permissions_optional_28, value);
	}

	inline static int32_t get_offset_of_permissions_refused_29() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___permissions_refused_29)); }
	inline RefEmitPermissionSetU5BU5D_t3643576428* get_permissions_refused_29() const { return ___permissions_refused_29; }
	inline RefEmitPermissionSetU5BU5D_t3643576428** get_address_of_permissions_refused_29() { return &___permissions_refused_29; }
	inline void set_permissions_refused_29(RefEmitPermissionSetU5BU5D_t3643576428* value)
	{
		___permissions_refused_29 = value;
		Il2CppCodeGenWriteBarrier(&___permissions_refused_29, value);
	}

	inline static int32_t get_offset_of_peKind_30() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___peKind_30)); }
	inline int32_t get_peKind_30() const { return ___peKind_30; }
	inline int32_t* get_address_of_peKind_30() { return &___peKind_30; }
	inline void set_peKind_30(int32_t value)
	{
		___peKind_30 = value;
	}

	inline static int32_t get_offset_of_machine_31() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___machine_31)); }
	inline int32_t get_machine_31() const { return ___machine_31; }
	inline int32_t* get_address_of_machine_31() { return &___machine_31; }
	inline void set_machine_31(int32_t value)
	{
		___machine_31 = value;
	}

	inline static int32_t get_offset_of_corlib_internal_32() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___corlib_internal_32)); }
	inline bool get_corlib_internal_32() const { return ___corlib_internal_32; }
	inline bool* get_address_of_corlib_internal_32() { return &___corlib_internal_32; }
	inline void set_corlib_internal_32(bool value)
	{
		___corlib_internal_32 = value;
	}

	inline static int32_t get_offset_of_type_forwarders_33() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___type_forwarders_33)); }
	inline TypeU5BU5D_t1664964607* get_type_forwarders_33() const { return ___type_forwarders_33; }
	inline TypeU5BU5D_t1664964607** get_address_of_type_forwarders_33() { return &___type_forwarders_33; }
	inline void set_type_forwarders_33(TypeU5BU5D_t1664964607* value)
	{
		___type_forwarders_33 = value;
		Il2CppCodeGenWriteBarrier(&___type_forwarders_33, value);
	}

	inline static int32_t get_offset_of_pktoken_34() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___pktoken_34)); }
	inline ByteU5BU5D_t3397334013* get_pktoken_34() const { return ___pktoken_34; }
	inline ByteU5BU5D_t3397334013** get_address_of_pktoken_34() { return &___pktoken_34; }
	inline void set_pktoken_34(ByteU5BU5D_t3397334013* value)
	{
		___pktoken_34 = value;
		Il2CppCodeGenWriteBarrier(&___pktoken_34, value);
	}

	inline static int32_t get_offset_of_corlib_object_type_35() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___corlib_object_type_35)); }
	inline Type_t * get_corlib_object_type_35() const { return ___corlib_object_type_35; }
	inline Type_t ** get_address_of_corlib_object_type_35() { return &___corlib_object_type_35; }
	inline void set_corlib_object_type_35(Type_t * value)
	{
		___corlib_object_type_35 = value;
		Il2CppCodeGenWriteBarrier(&___corlib_object_type_35, value);
	}

	inline static int32_t get_offset_of_corlib_value_type_36() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___corlib_value_type_36)); }
	inline Type_t * get_corlib_value_type_36() const { return ___corlib_value_type_36; }
	inline Type_t ** get_address_of_corlib_value_type_36() { return &___corlib_value_type_36; }
	inline void set_corlib_value_type_36(Type_t * value)
	{
		___corlib_value_type_36 = value;
		Il2CppCodeGenWriteBarrier(&___corlib_value_type_36, value);
	}

	inline static int32_t get_offset_of_corlib_enum_type_37() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___corlib_enum_type_37)); }
	inline Type_t * get_corlib_enum_type_37() const { return ___corlib_enum_type_37; }
	inline Type_t ** get_address_of_corlib_enum_type_37() { return &___corlib_enum_type_37; }
	inline void set_corlib_enum_type_37(Type_t * value)
	{
		___corlib_enum_type_37 = value;
		Il2CppCodeGenWriteBarrier(&___corlib_enum_type_37, value);
	}

	inline static int32_t get_offset_of_corlib_void_type_38() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___corlib_void_type_38)); }
	inline Type_t * get_corlib_void_type_38() const { return ___corlib_void_type_38; }
	inline Type_t ** get_address_of_corlib_void_type_38() { return &___corlib_void_type_38; }
	inline void set_corlib_void_type_38(Type_t * value)
	{
		___corlib_void_type_38 = value;
		Il2CppCodeGenWriteBarrier(&___corlib_void_type_38, value);
	}

	inline static int32_t get_offset_of_resource_writers_39() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___resource_writers_39)); }
	inline ArrayList_t4252133567 * get_resource_writers_39() const { return ___resource_writers_39; }
	inline ArrayList_t4252133567 ** get_address_of_resource_writers_39() { return &___resource_writers_39; }
	inline void set_resource_writers_39(ArrayList_t4252133567 * value)
	{
		___resource_writers_39 = value;
		Il2CppCodeGenWriteBarrier(&___resource_writers_39, value);
	}

	inline static int32_t get_offset_of_version_res_40() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___version_res_40)); }
	inline Win32VersionResource_t548350325 * get_version_res_40() const { return ___version_res_40; }
	inline Win32VersionResource_t548350325 ** get_address_of_version_res_40() { return &___version_res_40; }
	inline void set_version_res_40(Win32VersionResource_t548350325 * value)
	{
		___version_res_40 = value;
		Il2CppCodeGenWriteBarrier(&___version_res_40, value);
	}

	inline static int32_t get_offset_of_created_41() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___created_41)); }
	inline bool get_created_41() const { return ___created_41; }
	inline bool* get_address_of_created_41() { return &___created_41; }
	inline void set_created_41(bool value)
	{
		___created_41 = value;
	}

	inline static int32_t get_offset_of_is_module_only_42() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___is_module_only_42)); }
	inline bool get_is_module_only_42() const { return ___is_module_only_42; }
	inline bool* get_address_of_is_module_only_42() { return &___is_module_only_42; }
	inline void set_is_module_only_42(bool value)
	{
		___is_module_only_42 = value;
	}

	inline static int32_t get_offset_of_sn_43() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___sn_43)); }
	inline StrongName_t117835354 * get_sn_43() const { return ___sn_43; }
	inline StrongName_t117835354 ** get_address_of_sn_43() { return &___sn_43; }
	inline void set_sn_43(StrongName_t117835354 * value)
	{
		___sn_43 = value;
		Il2CppCodeGenWriteBarrier(&___sn_43, value);
	}

	inline static int32_t get_offset_of_native_resource_44() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___native_resource_44)); }
	inline int32_t get_native_resource_44() const { return ___native_resource_44; }
	inline int32_t* get_address_of_native_resource_44() { return &___native_resource_44; }
	inline void set_native_resource_44(int32_t value)
	{
		___native_resource_44 = value;
	}

	inline static int32_t get_offset_of_versioninfo_culture_45() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___versioninfo_culture_45)); }
	inline String_t* get_versioninfo_culture_45() const { return ___versioninfo_culture_45; }
	inline String_t** get_address_of_versioninfo_culture_45() { return &___versioninfo_culture_45; }
	inline void set_versioninfo_culture_45(String_t* value)
	{
		___versioninfo_culture_45 = value;
		Il2CppCodeGenWriteBarrier(&___versioninfo_culture_45, value);
	}

	inline static int32_t get_offset_of_manifest_module_46() { return static_cast<int32_t>(offsetof(AssemblyBuilder_t1646117627, ___manifest_module_46)); }
	inline ModuleBuilder_t4156028127 * get_manifest_module_46() const { return ___manifest_module_46; }
	inline ModuleBuilder_t4156028127 ** get_address_of_manifest_module_46() { return &___manifest_module_46; }
	inline void set_manifest_module_46(ModuleBuilder_t4156028127 * value)
	{
		___manifest_module_46 = value;
		Il2CppCodeGenWriteBarrier(&___manifest_module_46, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1646117627_marshaled_pinvoke : public Assembly_t4268412390_marshaled_pinvoke
{
	uintptr_t ___dynamic_assembly_10;
	MethodInfo_t * ___entry_point_11;
	ModuleBuilderU5BU5D_t3642333382* ___modules_12;
	char* ___name_13;
	char* ___dir_14;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_15;
	MonoResource_t3127387157_marshaled_pinvoke* ___resources_16;
	uint8_t* ___public_key_17;
	char* ___version_18;
	char* ___culture_19;
	uint32_t ___algid_20;
	uint32_t ___flags_21;
	int32_t ___pekind_22;
	int32_t ___delay_sign_23;
	uint32_t ___access_24;
	ModuleU5BU5D_t3593287923* ___loaded_modules_25;
	MonoWin32Resource_t2467306218_marshaled_pinvoke* ___win32_resources_26;
	RefEmitPermissionSet_t2708608433_marshaled_pinvoke* ___permissions_minimum_27;
	RefEmitPermissionSet_t2708608433_marshaled_pinvoke* ___permissions_optional_28;
	RefEmitPermissionSet_t2708608433_marshaled_pinvoke* ___permissions_refused_29;
	int32_t ___peKind_30;
	int32_t ___machine_31;
	int32_t ___corlib_internal_32;
	TypeU5BU5D_t1664964607* ___type_forwarders_33;
	uint8_t* ___pktoken_34;
	Type_t * ___corlib_object_type_35;
	Type_t * ___corlib_value_type_36;
	Type_t * ___corlib_enum_type_37;
	Type_t * ___corlib_void_type_38;
	ArrayList_t4252133567 * ___resource_writers_39;
	Win32VersionResource_t548350325 * ___version_res_40;
	int32_t ___created_41;
	int32_t ___is_module_only_42;
	StrongName_t117835354 * ___sn_43;
	int32_t ___native_resource_44;
	char* ___versioninfo_culture_45;
	ModuleBuilder_t4156028127_marshaled_pinvoke* ___manifest_module_46;
};
// Native definition for COM marshalling of System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1646117627_marshaled_com : public Assembly_t4268412390_marshaled_com
{
	uintptr_t ___dynamic_assembly_10;
	MethodInfo_t * ___entry_point_11;
	ModuleBuilderU5BU5D_t3642333382* ___modules_12;
	Il2CppChar* ___name_13;
	Il2CppChar* ___dir_14;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_15;
	MonoResource_t3127387157_marshaled_com* ___resources_16;
	uint8_t* ___public_key_17;
	Il2CppChar* ___version_18;
	Il2CppChar* ___culture_19;
	uint32_t ___algid_20;
	uint32_t ___flags_21;
	int32_t ___pekind_22;
	int32_t ___delay_sign_23;
	uint32_t ___access_24;
	ModuleU5BU5D_t3593287923* ___loaded_modules_25;
	MonoWin32Resource_t2467306218_marshaled_com* ___win32_resources_26;
	RefEmitPermissionSet_t2708608433_marshaled_com* ___permissions_minimum_27;
	RefEmitPermissionSet_t2708608433_marshaled_com* ___permissions_optional_28;
	RefEmitPermissionSet_t2708608433_marshaled_com* ___permissions_refused_29;
	int32_t ___peKind_30;
	int32_t ___machine_31;
	int32_t ___corlib_internal_32;
	TypeU5BU5D_t1664964607* ___type_forwarders_33;
	uint8_t* ___pktoken_34;
	Type_t * ___corlib_object_type_35;
	Type_t * ___corlib_value_type_36;
	Type_t * ___corlib_enum_type_37;
	Type_t * ___corlib_void_type_38;
	ArrayList_t4252133567 * ___resource_writers_39;
	Win32VersionResource_t548350325 * ___version_res_40;
	int32_t ___created_41;
	int32_t ___is_module_only_42;
	StrongName_t117835354 * ___sn_43;
	int32_t ___native_resource_44;
	Il2CppChar* ___versioninfo_culture_45;
	ModuleBuilder_t4156028127_marshaled_com* ___manifest_module_46;
};
