﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNameTable1345805268.h"

// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t180042139;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t594386929  : public XmlNameTable_t1345805268
{
public:
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::entries
	EntryU5BU5D_t180042139* ___entries_0;
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_1;
	// System.Int32 System.Xml.NameTable::mask
	int32_t ___mask_2;
	// System.Int32 System.Xml.NameTable::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_3;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___entries_0)); }
	inline EntryU5BU5D_t180042139* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t180042139** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t180042139* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_hashCodeRandomizer_3() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___hashCodeRandomizer_3)); }
	inline int32_t get_hashCodeRandomizer_3() const { return ___hashCodeRandomizer_3; }
	inline int32_t* get_address_of_hashCodeRandomizer_3() { return &___hashCodeRandomizer_3; }
	inline void set_hashCodeRandomizer_3(int32_t value)
	{
		___hashCodeRandomizer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
