﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlAttribute175731005.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUnspecifiedAttribute
struct  XmlUnspecifiedAttribute_t3928824946  : public XmlAttribute_t175731005
{
public:
	// System.Boolean System.Xml.XmlUnspecifiedAttribute::fSpecified
	bool ___fSpecified_3;

public:
	inline static int32_t get_offset_of_fSpecified_3() { return static_cast<int32_t>(offsetof(XmlUnspecifiedAttribute_t3928824946, ___fSpecified_3)); }
	inline bool get_fSpecified_3() const { return ___fSpecified_3; }
	inline bool* get_address_of_fSpecified_3() { return &___fSpecified_3; }
	inline void set_fSpecified_3(bool value)
	{
		___fSpecified_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
