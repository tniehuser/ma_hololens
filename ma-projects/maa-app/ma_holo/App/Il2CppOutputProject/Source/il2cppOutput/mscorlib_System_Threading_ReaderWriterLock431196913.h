﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Runtime_ConstrainedExecution_Criti1920899984.h"

// System.Threading.LockQueue
struct LockQueue_t2670078596;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ReaderWriterLock
struct  ReaderWriterLock_t431196913  : public CriticalFinalizerObject_t1920899984
{
public:
	// System.Int32 System.Threading.ReaderWriterLock::seq_num
	int32_t ___seq_num_0;
	// System.Int32 System.Threading.ReaderWriterLock::state
	int32_t ___state_1;
	// System.Int32 System.Threading.ReaderWriterLock::readers
	int32_t ___readers_2;
	// System.Int32 System.Threading.ReaderWriterLock::writer_lock_owner
	int32_t ___writer_lock_owner_3;
	// System.Threading.LockQueue System.Threading.ReaderWriterLock::writer_queue
	LockQueue_t2670078596 * ___writer_queue_4;

public:
	inline static int32_t get_offset_of_seq_num_0() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t431196913, ___seq_num_0)); }
	inline int32_t get_seq_num_0() const { return ___seq_num_0; }
	inline int32_t* get_address_of_seq_num_0() { return &___seq_num_0; }
	inline void set_seq_num_0(int32_t value)
	{
		___seq_num_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t431196913, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t431196913, ___readers_2)); }
	inline int32_t get_readers_2() const { return ___readers_2; }
	inline int32_t* get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(int32_t value)
	{
		___readers_2 = value;
	}

	inline static int32_t get_offset_of_writer_lock_owner_3() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t431196913, ___writer_lock_owner_3)); }
	inline int32_t get_writer_lock_owner_3() const { return ___writer_lock_owner_3; }
	inline int32_t* get_address_of_writer_lock_owner_3() { return &___writer_lock_owner_3; }
	inline void set_writer_lock_owner_3(int32_t value)
	{
		___writer_lock_owner_3 = value;
	}

	inline static int32_t get_offset_of_writer_queue_4() { return static_cast<int32_t>(offsetof(ReaderWriterLock_t431196913, ___writer_queue_4)); }
	inline LockQueue_t2670078596 * get_writer_queue_4() const { return ___writer_queue_4; }
	inline LockQueue_t2670078596 ** get_address_of_writer_queue_4() { return &___writer_queue_4; }
	inline void set_writer_queue_4(LockQueue_t2670078596 * value)
	{
		___writer_queue_4 = value;
		Il2CppCodeGenWriteBarrier(&___writer_queue_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
