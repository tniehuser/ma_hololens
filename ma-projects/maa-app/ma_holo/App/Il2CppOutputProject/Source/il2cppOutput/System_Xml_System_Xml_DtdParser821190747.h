﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"
#include "System_Xml_System_Xml_DtdParser_ScanningFunction2556434333.h"
#include "System_Xml_System_Xml_LineInfo1429635508.h"

// System.Xml.IDtdParserAdapter
struct IDtdParserAdapter_t2279635749;
// System.Xml.IDtdParserAdapterWithValidation
struct IDtdParserAdapterWithValidation_t636382188;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation>
struct Dictionary_2_t3981174159;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser
struct  DtdParser_t821190747  : public Il2CppObject
{
public:
	// System.Xml.IDtdParserAdapter System.Xml.DtdParser::readerAdapter
	Il2CppObject * ___readerAdapter_0;
	// System.Xml.IDtdParserAdapterWithValidation System.Xml.DtdParser::readerAdapterWithValidation
	Il2CppObject * ___readerAdapterWithValidation_1;
	// System.Xml.XmlNameTable System.Xml.DtdParser::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaInfo System.Xml.DtdParser::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_3;
	// System.Xml.XmlCharType System.Xml.DtdParser::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_4;
	// System.String System.Xml.DtdParser::systemId
	String_t* ___systemId_5;
	// System.String System.Xml.DtdParser::publicId
	String_t* ___publicId_6;
	// System.Boolean System.Xml.DtdParser::normalize
	bool ___normalize_7;
	// System.Boolean System.Xml.DtdParser::validate
	bool ___validate_8;
	// System.Boolean System.Xml.DtdParser::supportNamespaces
	bool ___supportNamespaces_9;
	// System.Boolean System.Xml.DtdParser::v1Compat
	bool ___v1Compat_10;
	// System.Char[] System.Xml.DtdParser::chars
	CharU5BU5D_t1328083999* ___chars_11;
	// System.Int32 System.Xml.DtdParser::charsUsed
	int32_t ___charsUsed_12;
	// System.Int32 System.Xml.DtdParser::curPos
	int32_t ___curPos_13;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::scanningFunction
	int32_t ___scanningFunction_14;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::nextScaningFunction
	int32_t ___nextScaningFunction_15;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::savedScanningFunction
	int32_t ___savedScanningFunction_16;
	// System.Boolean System.Xml.DtdParser::whitespaceSeen
	bool ___whitespaceSeen_17;
	// System.Int32 System.Xml.DtdParser::tokenStartPos
	int32_t ___tokenStartPos_18;
	// System.Int32 System.Xml.DtdParser::colonPos
	int32_t ___colonPos_19;
	// System.Text.StringBuilder System.Xml.DtdParser::internalSubsetValueSb
	StringBuilder_t1221177846 * ___internalSubsetValueSb_20;
	// System.Int32 System.Xml.DtdParser::externalEntitiesDepth
	int32_t ___externalEntitiesDepth_21;
	// System.Int32 System.Xml.DtdParser::currentEntityId
	int32_t ___currentEntityId_22;
	// System.Boolean System.Xml.DtdParser::freeFloatingDtd
	bool ___freeFloatingDtd_23;
	// System.Boolean System.Xml.DtdParser::hasFreeFloatingInternalSubset
	bool ___hasFreeFloatingInternalSubset_24;
	// System.Text.StringBuilder System.Xml.DtdParser::stringBuilder
	StringBuilder_t1221177846 * ___stringBuilder_25;
	// System.Int32 System.Xml.DtdParser::condSectionDepth
	int32_t ___condSectionDepth_26;
	// System.Xml.LineInfo System.Xml.DtdParser::literalLineInfo
	LineInfo_t1429635508  ___literalLineInfo_27;
	// System.Char System.Xml.DtdParser::literalQuoteChar
	Il2CppChar ___literalQuoteChar_28;
	// System.String System.Xml.DtdParser::documentBaseUri
	String_t* ___documentBaseUri_29;
	// System.String System.Xml.DtdParser::externalDtdBaseUri
	String_t* ___externalDtdBaseUri_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation> System.Xml.DtdParser::undeclaredNotations
	Dictionary_2_t3981174159 * ___undeclaredNotations_31;
	// System.Int32[] System.Xml.DtdParser::condSectionEntityIds
	Int32U5BU5D_t3030399641* ___condSectionEntityIds_32;

public:
	inline static int32_t get_offset_of_readerAdapter_0() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___readerAdapter_0)); }
	inline Il2CppObject * get_readerAdapter_0() const { return ___readerAdapter_0; }
	inline Il2CppObject ** get_address_of_readerAdapter_0() { return &___readerAdapter_0; }
	inline void set_readerAdapter_0(Il2CppObject * value)
	{
		___readerAdapter_0 = value;
		Il2CppCodeGenWriteBarrier(&___readerAdapter_0, value);
	}

	inline static int32_t get_offset_of_readerAdapterWithValidation_1() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___readerAdapterWithValidation_1)); }
	inline Il2CppObject * get_readerAdapterWithValidation_1() const { return ___readerAdapterWithValidation_1; }
	inline Il2CppObject ** get_address_of_readerAdapterWithValidation_1() { return &___readerAdapterWithValidation_1; }
	inline void set_readerAdapterWithValidation_1(Il2CppObject * value)
	{
		___readerAdapterWithValidation_1 = value;
		Il2CppCodeGenWriteBarrier(&___readerAdapterWithValidation_1, value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_2, value);
	}

	inline static int32_t get_offset_of_schemaInfo_3() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___schemaInfo_3)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_3() const { return ___schemaInfo_3; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_3() { return &___schemaInfo_3; }
	inline void set_schemaInfo_3(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___schemaInfo_3, value);
	}

	inline static int32_t get_offset_of_xmlCharType_4() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___xmlCharType_4)); }
	inline XmlCharType_t1050521405  get_xmlCharType_4() const { return ___xmlCharType_4; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_4() { return &___xmlCharType_4; }
	inline void set_xmlCharType_4(XmlCharType_t1050521405  value)
	{
		___xmlCharType_4 = value;
	}

	inline static int32_t get_offset_of_systemId_5() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___systemId_5)); }
	inline String_t* get_systemId_5() const { return ___systemId_5; }
	inline String_t** get_address_of_systemId_5() { return &___systemId_5; }
	inline void set_systemId_5(String_t* value)
	{
		___systemId_5 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_5, value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_6, value);
	}

	inline static int32_t get_offset_of_normalize_7() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___normalize_7)); }
	inline bool get_normalize_7() const { return ___normalize_7; }
	inline bool* get_address_of_normalize_7() { return &___normalize_7; }
	inline void set_normalize_7(bool value)
	{
		___normalize_7 = value;
	}

	inline static int32_t get_offset_of_validate_8() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___validate_8)); }
	inline bool get_validate_8() const { return ___validate_8; }
	inline bool* get_address_of_validate_8() { return &___validate_8; }
	inline void set_validate_8(bool value)
	{
		___validate_8 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_9() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___supportNamespaces_9)); }
	inline bool get_supportNamespaces_9() const { return ___supportNamespaces_9; }
	inline bool* get_address_of_supportNamespaces_9() { return &___supportNamespaces_9; }
	inline void set_supportNamespaces_9(bool value)
	{
		___supportNamespaces_9 = value;
	}

	inline static int32_t get_offset_of_v1Compat_10() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___v1Compat_10)); }
	inline bool get_v1Compat_10() const { return ___v1Compat_10; }
	inline bool* get_address_of_v1Compat_10() { return &___v1Compat_10; }
	inline void set_v1Compat_10(bool value)
	{
		___v1Compat_10 = value;
	}

	inline static int32_t get_offset_of_chars_11() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___chars_11)); }
	inline CharU5BU5D_t1328083999* get_chars_11() const { return ___chars_11; }
	inline CharU5BU5D_t1328083999** get_address_of_chars_11() { return &___chars_11; }
	inline void set_chars_11(CharU5BU5D_t1328083999* value)
	{
		___chars_11 = value;
		Il2CppCodeGenWriteBarrier(&___chars_11, value);
	}

	inline static int32_t get_offset_of_charsUsed_12() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___charsUsed_12)); }
	inline int32_t get_charsUsed_12() const { return ___charsUsed_12; }
	inline int32_t* get_address_of_charsUsed_12() { return &___charsUsed_12; }
	inline void set_charsUsed_12(int32_t value)
	{
		___charsUsed_12 = value;
	}

	inline static int32_t get_offset_of_curPos_13() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___curPos_13)); }
	inline int32_t get_curPos_13() const { return ___curPos_13; }
	inline int32_t* get_address_of_curPos_13() { return &___curPos_13; }
	inline void set_curPos_13(int32_t value)
	{
		___curPos_13 = value;
	}

	inline static int32_t get_offset_of_scanningFunction_14() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___scanningFunction_14)); }
	inline int32_t get_scanningFunction_14() const { return ___scanningFunction_14; }
	inline int32_t* get_address_of_scanningFunction_14() { return &___scanningFunction_14; }
	inline void set_scanningFunction_14(int32_t value)
	{
		___scanningFunction_14 = value;
	}

	inline static int32_t get_offset_of_nextScaningFunction_15() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___nextScaningFunction_15)); }
	inline int32_t get_nextScaningFunction_15() const { return ___nextScaningFunction_15; }
	inline int32_t* get_address_of_nextScaningFunction_15() { return &___nextScaningFunction_15; }
	inline void set_nextScaningFunction_15(int32_t value)
	{
		___nextScaningFunction_15 = value;
	}

	inline static int32_t get_offset_of_savedScanningFunction_16() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___savedScanningFunction_16)); }
	inline int32_t get_savedScanningFunction_16() const { return ___savedScanningFunction_16; }
	inline int32_t* get_address_of_savedScanningFunction_16() { return &___savedScanningFunction_16; }
	inline void set_savedScanningFunction_16(int32_t value)
	{
		___savedScanningFunction_16 = value;
	}

	inline static int32_t get_offset_of_whitespaceSeen_17() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___whitespaceSeen_17)); }
	inline bool get_whitespaceSeen_17() const { return ___whitespaceSeen_17; }
	inline bool* get_address_of_whitespaceSeen_17() { return &___whitespaceSeen_17; }
	inline void set_whitespaceSeen_17(bool value)
	{
		___whitespaceSeen_17 = value;
	}

	inline static int32_t get_offset_of_tokenStartPos_18() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___tokenStartPos_18)); }
	inline int32_t get_tokenStartPos_18() const { return ___tokenStartPos_18; }
	inline int32_t* get_address_of_tokenStartPos_18() { return &___tokenStartPos_18; }
	inline void set_tokenStartPos_18(int32_t value)
	{
		___tokenStartPos_18 = value;
	}

	inline static int32_t get_offset_of_colonPos_19() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___colonPos_19)); }
	inline int32_t get_colonPos_19() const { return ___colonPos_19; }
	inline int32_t* get_address_of_colonPos_19() { return &___colonPos_19; }
	inline void set_colonPos_19(int32_t value)
	{
		___colonPos_19 = value;
	}

	inline static int32_t get_offset_of_internalSubsetValueSb_20() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___internalSubsetValueSb_20)); }
	inline StringBuilder_t1221177846 * get_internalSubsetValueSb_20() const { return ___internalSubsetValueSb_20; }
	inline StringBuilder_t1221177846 ** get_address_of_internalSubsetValueSb_20() { return &___internalSubsetValueSb_20; }
	inline void set_internalSubsetValueSb_20(StringBuilder_t1221177846 * value)
	{
		___internalSubsetValueSb_20 = value;
		Il2CppCodeGenWriteBarrier(&___internalSubsetValueSb_20, value);
	}

	inline static int32_t get_offset_of_externalEntitiesDepth_21() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___externalEntitiesDepth_21)); }
	inline int32_t get_externalEntitiesDepth_21() const { return ___externalEntitiesDepth_21; }
	inline int32_t* get_address_of_externalEntitiesDepth_21() { return &___externalEntitiesDepth_21; }
	inline void set_externalEntitiesDepth_21(int32_t value)
	{
		___externalEntitiesDepth_21 = value;
	}

	inline static int32_t get_offset_of_currentEntityId_22() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___currentEntityId_22)); }
	inline int32_t get_currentEntityId_22() const { return ___currentEntityId_22; }
	inline int32_t* get_address_of_currentEntityId_22() { return &___currentEntityId_22; }
	inline void set_currentEntityId_22(int32_t value)
	{
		___currentEntityId_22 = value;
	}

	inline static int32_t get_offset_of_freeFloatingDtd_23() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___freeFloatingDtd_23)); }
	inline bool get_freeFloatingDtd_23() const { return ___freeFloatingDtd_23; }
	inline bool* get_address_of_freeFloatingDtd_23() { return &___freeFloatingDtd_23; }
	inline void set_freeFloatingDtd_23(bool value)
	{
		___freeFloatingDtd_23 = value;
	}

	inline static int32_t get_offset_of_hasFreeFloatingInternalSubset_24() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___hasFreeFloatingInternalSubset_24)); }
	inline bool get_hasFreeFloatingInternalSubset_24() const { return ___hasFreeFloatingInternalSubset_24; }
	inline bool* get_address_of_hasFreeFloatingInternalSubset_24() { return &___hasFreeFloatingInternalSubset_24; }
	inline void set_hasFreeFloatingInternalSubset_24(bool value)
	{
		___hasFreeFloatingInternalSubset_24 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_25() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___stringBuilder_25)); }
	inline StringBuilder_t1221177846 * get_stringBuilder_25() const { return ___stringBuilder_25; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuilder_25() { return &___stringBuilder_25; }
	inline void set_stringBuilder_25(StringBuilder_t1221177846 * value)
	{
		___stringBuilder_25 = value;
		Il2CppCodeGenWriteBarrier(&___stringBuilder_25, value);
	}

	inline static int32_t get_offset_of_condSectionDepth_26() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___condSectionDepth_26)); }
	inline int32_t get_condSectionDepth_26() const { return ___condSectionDepth_26; }
	inline int32_t* get_address_of_condSectionDepth_26() { return &___condSectionDepth_26; }
	inline void set_condSectionDepth_26(int32_t value)
	{
		___condSectionDepth_26 = value;
	}

	inline static int32_t get_offset_of_literalLineInfo_27() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___literalLineInfo_27)); }
	inline LineInfo_t1429635508  get_literalLineInfo_27() const { return ___literalLineInfo_27; }
	inline LineInfo_t1429635508 * get_address_of_literalLineInfo_27() { return &___literalLineInfo_27; }
	inline void set_literalLineInfo_27(LineInfo_t1429635508  value)
	{
		___literalLineInfo_27 = value;
	}

	inline static int32_t get_offset_of_literalQuoteChar_28() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___literalQuoteChar_28)); }
	inline Il2CppChar get_literalQuoteChar_28() const { return ___literalQuoteChar_28; }
	inline Il2CppChar* get_address_of_literalQuoteChar_28() { return &___literalQuoteChar_28; }
	inline void set_literalQuoteChar_28(Il2CppChar value)
	{
		___literalQuoteChar_28 = value;
	}

	inline static int32_t get_offset_of_documentBaseUri_29() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___documentBaseUri_29)); }
	inline String_t* get_documentBaseUri_29() const { return ___documentBaseUri_29; }
	inline String_t** get_address_of_documentBaseUri_29() { return &___documentBaseUri_29; }
	inline void set_documentBaseUri_29(String_t* value)
	{
		___documentBaseUri_29 = value;
		Il2CppCodeGenWriteBarrier(&___documentBaseUri_29, value);
	}

	inline static int32_t get_offset_of_externalDtdBaseUri_30() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___externalDtdBaseUri_30)); }
	inline String_t* get_externalDtdBaseUri_30() const { return ___externalDtdBaseUri_30; }
	inline String_t** get_address_of_externalDtdBaseUri_30() { return &___externalDtdBaseUri_30; }
	inline void set_externalDtdBaseUri_30(String_t* value)
	{
		___externalDtdBaseUri_30 = value;
		Il2CppCodeGenWriteBarrier(&___externalDtdBaseUri_30, value);
	}

	inline static int32_t get_offset_of_undeclaredNotations_31() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___undeclaredNotations_31)); }
	inline Dictionary_2_t3981174159 * get_undeclaredNotations_31() const { return ___undeclaredNotations_31; }
	inline Dictionary_2_t3981174159 ** get_address_of_undeclaredNotations_31() { return &___undeclaredNotations_31; }
	inline void set_undeclaredNotations_31(Dictionary_2_t3981174159 * value)
	{
		___undeclaredNotations_31 = value;
		Il2CppCodeGenWriteBarrier(&___undeclaredNotations_31, value);
	}

	inline static int32_t get_offset_of_condSectionEntityIds_32() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___condSectionEntityIds_32)); }
	inline Int32U5BU5D_t3030399641* get_condSectionEntityIds_32() const { return ___condSectionEntityIds_32; }
	inline Int32U5BU5D_t3030399641** get_address_of_condSectionEntityIds_32() { return &___condSectionEntityIds_32; }
	inline void set_condSectionEntityIds_32(Int32U5BU5D_t3030399641* value)
	{
		___condSectionEntityIds_32 = value;
		Il2CppCodeGenWriteBarrier(&___condSectionEntityIds_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
