﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t663144255;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_t2728039553  : public Il2CppObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::_nt
	XmlNameTable_t1345805268 * ____nt_0;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::_nsMgr
	XmlNamespaceManager_t486731501 * ____nsMgr_1;
	// System.String System.Xml.XmlParserContext::_docTypeName
	String_t* ____docTypeName_2;
	// System.String System.Xml.XmlParserContext::_pubId
	String_t* ____pubId_3;
	// System.String System.Xml.XmlParserContext::_sysId
	String_t* ____sysId_4;
	// System.String System.Xml.XmlParserContext::_internalSubset
	String_t* ____internalSubset_5;
	// System.String System.Xml.XmlParserContext::_xmlLang
	String_t* ____xmlLang_6;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::_xmlSpace
	int32_t ____xmlSpace_7;
	// System.String System.Xml.XmlParserContext::_baseURI
	String_t* ____baseURI_8;
	// System.Text.Encoding System.Xml.XmlParserContext::_encoding
	Encoding_t663144255 * ____encoding_9;

public:
	inline static int32_t get_offset_of__nt_0() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____nt_0)); }
	inline XmlNameTable_t1345805268 * get__nt_0() const { return ____nt_0; }
	inline XmlNameTable_t1345805268 ** get_address_of__nt_0() { return &____nt_0; }
	inline void set__nt_0(XmlNameTable_t1345805268 * value)
	{
		____nt_0 = value;
		Il2CppCodeGenWriteBarrier(&____nt_0, value);
	}

	inline static int32_t get_offset_of__nsMgr_1() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____nsMgr_1)); }
	inline XmlNamespaceManager_t486731501 * get__nsMgr_1() const { return ____nsMgr_1; }
	inline XmlNamespaceManager_t486731501 ** get_address_of__nsMgr_1() { return &____nsMgr_1; }
	inline void set__nsMgr_1(XmlNamespaceManager_t486731501 * value)
	{
		____nsMgr_1 = value;
		Il2CppCodeGenWriteBarrier(&____nsMgr_1, value);
	}

	inline static int32_t get_offset_of__docTypeName_2() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____docTypeName_2)); }
	inline String_t* get__docTypeName_2() const { return ____docTypeName_2; }
	inline String_t** get_address_of__docTypeName_2() { return &____docTypeName_2; }
	inline void set__docTypeName_2(String_t* value)
	{
		____docTypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&____docTypeName_2, value);
	}

	inline static int32_t get_offset_of__pubId_3() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____pubId_3)); }
	inline String_t* get__pubId_3() const { return ____pubId_3; }
	inline String_t** get_address_of__pubId_3() { return &____pubId_3; }
	inline void set__pubId_3(String_t* value)
	{
		____pubId_3 = value;
		Il2CppCodeGenWriteBarrier(&____pubId_3, value);
	}

	inline static int32_t get_offset_of__sysId_4() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____sysId_4)); }
	inline String_t* get__sysId_4() const { return ____sysId_4; }
	inline String_t** get_address_of__sysId_4() { return &____sysId_4; }
	inline void set__sysId_4(String_t* value)
	{
		____sysId_4 = value;
		Il2CppCodeGenWriteBarrier(&____sysId_4, value);
	}

	inline static int32_t get_offset_of__internalSubset_5() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____internalSubset_5)); }
	inline String_t* get__internalSubset_5() const { return ____internalSubset_5; }
	inline String_t** get_address_of__internalSubset_5() { return &____internalSubset_5; }
	inline void set__internalSubset_5(String_t* value)
	{
		____internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier(&____internalSubset_5, value);
	}

	inline static int32_t get_offset_of__xmlLang_6() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____xmlLang_6)); }
	inline String_t* get__xmlLang_6() const { return ____xmlLang_6; }
	inline String_t** get_address_of__xmlLang_6() { return &____xmlLang_6; }
	inline void set__xmlLang_6(String_t* value)
	{
		____xmlLang_6 = value;
		Il2CppCodeGenWriteBarrier(&____xmlLang_6, value);
	}

	inline static int32_t get_offset_of__xmlSpace_7() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____xmlSpace_7)); }
	inline int32_t get__xmlSpace_7() const { return ____xmlSpace_7; }
	inline int32_t* get_address_of__xmlSpace_7() { return &____xmlSpace_7; }
	inline void set__xmlSpace_7(int32_t value)
	{
		____xmlSpace_7 = value;
	}

	inline static int32_t get_offset_of__baseURI_8() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____baseURI_8)); }
	inline String_t* get__baseURI_8() const { return ____baseURI_8; }
	inline String_t** get_address_of__baseURI_8() { return &____baseURI_8; }
	inline void set__baseURI_8(String_t* value)
	{
		____baseURI_8 = value;
		Il2CppCodeGenWriteBarrier(&____baseURI_8, value);
	}

	inline static int32_t get_offset_of__encoding_9() { return static_cast<int32_t>(offsetof(XmlParserContext_t2728039553, ____encoding_9)); }
	inline Encoding_t663144255 * get__encoding_9() const { return ____encoding_9; }
	inline Encoding_t663144255 ** get_address_of__encoding_9() { return &____encoding_9; }
	inline void set__encoding_9(Encoding_t663144255 * value)
	{
		____encoding_9 = value;
		Il2CppCodeGenWriteBarrier(&____encoding_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
