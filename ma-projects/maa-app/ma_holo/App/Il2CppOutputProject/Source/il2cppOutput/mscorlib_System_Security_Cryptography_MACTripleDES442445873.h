﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_KeyedHashAlg1374150027.h"

// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;
// System.Security.Cryptography.CryptoStream
struct CryptoStream_t3531341937;
// System.Security.Cryptography.TailStream
struct TailStream_t1978912806;
// System.Security.Cryptography.TripleDES
struct TripleDES_t243950698;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MACTripleDES
struct  MACTripleDES_t442445873  : public KeyedHashAlgorithm_t1374150027
{
public:
	// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.MACTripleDES::m_encryptor
	Il2CppObject * ___m_encryptor_5;
	// System.Security.Cryptography.CryptoStream System.Security.Cryptography.MACTripleDES::_cs
	CryptoStream_t3531341937 * ____cs_6;
	// System.Security.Cryptography.TailStream System.Security.Cryptography.MACTripleDES::_ts
	TailStream_t1978912806 * ____ts_7;
	// System.Int32 System.Security.Cryptography.MACTripleDES::m_bytesPerBlock
	int32_t ___m_bytesPerBlock_8;
	// System.Security.Cryptography.TripleDES System.Security.Cryptography.MACTripleDES::des
	TripleDES_t243950698 * ___des_9;

public:
	inline static int32_t get_offset_of_m_encryptor_5() { return static_cast<int32_t>(offsetof(MACTripleDES_t442445873, ___m_encryptor_5)); }
	inline Il2CppObject * get_m_encryptor_5() const { return ___m_encryptor_5; }
	inline Il2CppObject ** get_address_of_m_encryptor_5() { return &___m_encryptor_5; }
	inline void set_m_encryptor_5(Il2CppObject * value)
	{
		___m_encryptor_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_encryptor_5, value);
	}

	inline static int32_t get_offset_of__cs_6() { return static_cast<int32_t>(offsetof(MACTripleDES_t442445873, ____cs_6)); }
	inline CryptoStream_t3531341937 * get__cs_6() const { return ____cs_6; }
	inline CryptoStream_t3531341937 ** get_address_of__cs_6() { return &____cs_6; }
	inline void set__cs_6(CryptoStream_t3531341937 * value)
	{
		____cs_6 = value;
		Il2CppCodeGenWriteBarrier(&____cs_6, value);
	}

	inline static int32_t get_offset_of__ts_7() { return static_cast<int32_t>(offsetof(MACTripleDES_t442445873, ____ts_7)); }
	inline TailStream_t1978912806 * get__ts_7() const { return ____ts_7; }
	inline TailStream_t1978912806 ** get_address_of__ts_7() { return &____ts_7; }
	inline void set__ts_7(TailStream_t1978912806 * value)
	{
		____ts_7 = value;
		Il2CppCodeGenWriteBarrier(&____ts_7, value);
	}

	inline static int32_t get_offset_of_m_bytesPerBlock_8() { return static_cast<int32_t>(offsetof(MACTripleDES_t442445873, ___m_bytesPerBlock_8)); }
	inline int32_t get_m_bytesPerBlock_8() const { return ___m_bytesPerBlock_8; }
	inline int32_t* get_address_of_m_bytesPerBlock_8() { return &___m_bytesPerBlock_8; }
	inline void set_m_bytesPerBlock_8(int32_t value)
	{
		___m_bytesPerBlock_8 = value;
	}

	inline static int32_t get_offset_of_des_9() { return static_cast<int32_t>(offsetof(MACTripleDES_t442445873, ___des_9)); }
	inline TripleDES_t243950698 * get_des_9() const { return ___des_9; }
	inline TripleDES_t243950698 ** get_address_of_des_9() { return &___des_9; }
	inline void set_des_9(TripleDES_t243950698 * value)
	{
		___des_9 = value;
		Il2CppCodeGenWriteBarrier(&___des_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
