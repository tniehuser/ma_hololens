﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3688495056.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1381019216.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3036622417.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anima641234490.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4062767676.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anima859920217.h"
#include "UnityEngine_UnityEngine_Motion2415020824.h"
#include "UnityEngine_UnityEngine_TerrainData1351141029.h"
#include "UnityEngine_UnityEngine_Terrain59182933.h"
#include "UnityEngine_UnityEngine_FontStyle2764949590.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal1272078033.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_GUI4082743951.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type4024155706.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup3975363388.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup755788567.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3828586629.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility996096873.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCac3120781045.h"
#include "UnityEngine_UnityEngine_GUISettings622856320.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat3594822336.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_ImagePosition3491916276.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute863467180.h"
#include "UnityEngine_UnityEngine_ExitGUIException1618397098.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671.h"
#include "UnityEngine_UnityEngine_ScrollViewState3820542997.h"
#include "UnityEngine_UnityEngine_SliderState1595681032.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments2834709342.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec1327795077.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils4100941042.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178.h"
#include "UnityEngine_UnityEngine_Analytics_CustomEventData1269126727.h"
#include "UnityEngine_UnityEngine_Analytics_AnalyticsResult3037633135.h"
#include "UnityEngine_UnityEngine_Analytics_Analytics2007048212.h"
#include "UnityEngine_UnityEngine_Analytics_UnityAnalyticsHa3238795095.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3303813975.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1855824201.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3501137495.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3415544547.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2437856093.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2553296249.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2759974023.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2487153075.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2394416487.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1045620518.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn1982950364.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3123342528.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3960847450.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogni831649826.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn3116179703.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogn2343902086.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_GestureRecogni343438253.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceChange769713231.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceId4207907960.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceData3147297845.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver3799331897.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver_Sur3444411290.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceObserver_Sur1375437319.h"
#include "UnityEngine_UnityEngine_VR_WSA_Persistence_WorldAn1514582394.h"
#include "UnityEngine_UnityEngine_VR_WSA_Persistence_WorldAn1561547849.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_Serializati3597317192.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor4032480564.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor2381700809.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchorT389490593.h"
#include "UnityEngine_UnityEngine_VR_WSA_Sharing_WorldAnchor2482917876.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (SharedBetweenAnimatorsAttribute_t1565472209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (StateMachineBehaviour_t2151245329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (AnimationEventSource_t3560017945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3002[4] = 
{
	AnimationEventSource_t3560017945::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (AnimationEvent_t2428323300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[11] = 
{
	AnimationEvent_t2428323300::get_offset_of_m_Time_0(),
	AnimationEvent_t2428323300::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t2428323300::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t2428323300::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t2428323300::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t2428323300::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t2428323300::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t2428323300::get_offset_of_m_Source_7(),
	AnimationEvent_t2428323300::get_offset_of_m_StateSender_8(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (AnimationClip_t3510324950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (AnimationState_t1303741697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (AnimatorControllerParameterType_t3688495056)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3006[5] = 
{
	AnimatorControllerParameterType_t3688495056::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (AnimatorClipInfo_t3905751349)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3905751349 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	AnimatorClipInfo_t3905751349::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3905751349::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (AnimatorStateInfo_t2577870592)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t2577870592 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3008[9] = 
{
	AnimatorStateInfo_t2577870592::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (AnimatorTransitionInfo_t2410896200)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3009[6] = 
{
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (Animator_t69676727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (AnimatorControllerParameter_t1381019216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[5] = 
{
	AnimatorControllerParameter_t1381019216::get_offset_of_m_Name_0(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_Type_1(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultFloat_2(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultInt_3(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultBool_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (SkeletonBone_t345082847)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t345082847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3012[5] = 
{
	SkeletonBone_t345082847::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (HumanLimit_t250797648)+ sizeof (Il2CppObject), sizeof(HumanLimit_t250797648 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3013[5] = 
{
	HumanLimit_t250797648::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (HumanBone_t1529896151)+ sizeof (Il2CppObject), sizeof(HumanBone_t1529896151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3014[3] = 
{
	HumanBone_t1529896151::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (RuntimeAnimatorController_t670468573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (AnimatorControllerPlayable_t4078305555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (AnimationMixerPlayable_t3036622417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (AnimationLayerMixerPlayable_t641234490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (AnimationClipPlayable_t4062767676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (AnimationPlayable_t1693994278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (AnimationOffsetPlayable_t859920217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (Motion_t2415020824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (TerrainData_t1351141029), -1, sizeof(TerrainData_t1351141029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3023[8] = 
{
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumResolution_2(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumDetailResolutionPerPatch_3(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumDetailResolutionPerPatch_4(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumDetailPatchCount_5(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumAlphamapResolution_6(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumAlphamapResolution_7(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumBaseMapResolution_8(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumBaseMapResolution_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (Terrain_t59182933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (FontStyle_t2764949590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3025[5] = 
{
	FontStyle_t2764949590::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (TextAnchor_t112990806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3026[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3027[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (Canvas_t209405766), -1, sizeof(Canvas_t209405766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3029[1] = 
{
	Canvas_t209405766_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (WillRenderCanvases_t3522132132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (Event_t3028476042), sizeof(Event_t3028476042_marshaled_pinvoke), sizeof(Event_t3028476042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3031[3] = 
{
	Event_t3028476042::get_offset_of_m_Ptr_0(),
	Event_t3028476042_StaticFields::get_offset_of_s_Current_1(),
	Event_t3028476042_StaticFields::get_offset_of_s_MasterEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (EventType_t3919834026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3032[33] = 
{
	EventType_t3919834026::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (EventModifiers_t2690251474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3033[9] = 
{
	EventModifiers_t2690251474::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (GUI_t4082743951), -1, sizeof(GUI_t4082743951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3034[12] = 
{
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t4082743951_StaticFields::get_offset_of_s_HotTextField_1(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BoxHash_2(),
	GUI_t4082743951_StaticFields::get_offset_of_s_RepeatButtonHash_3(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ToggleHash_4(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ButtonGridHash_5(),
	GUI_t4082743951_StaticFields::get_offset_of_s_SliderHash_6(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BeginGroupHash_7(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollviewHash_8(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_9(),
	GUI_t4082743951_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollViewStates_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (WindowFunction_t3486805455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (GUIContent_t4210063000), -1, sizeof(GUIContent_t4210063000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3036[7] = 
{
	GUIContent_t4210063000::get_offset_of_m_Text_0(),
	GUIContent_t4210063000::get_offset_of_m_Image_1(),
	GUIContent_t4210063000::get_offset_of_m_Tooltip_2(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t4210063000_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (GUILayout_t2579273657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (GUILayoutOption_t4183744904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[2] = 
{
	GUILayoutOption_t4183744904::get_offset_of_type_0(),
	GUILayoutOption_t4183744904::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (Type_t4024155706)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3039[15] = 
{
	Type_t4024155706::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (GUILayoutGroup_t3975363388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[17] = 
{
	GUILayoutGroup_t3975363388::get_offset_of_entries_10(),
	GUILayoutGroup_t3975363388::get_offset_of_isVertical_11(),
	GUILayoutGroup_t3975363388::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t3975363388::get_offset_of_spacing_13(),
	GUILayoutGroup_t3975363388::get_offset_of_sameSize_14(),
	GUILayoutGroup_t3975363388::get_offset_of_isWindow_15(),
	GUILayoutGroup_t3975363388::get_offset_of_windowID_16(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (GUIScrollGroup_t755788567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[12] = 
{
	GUIScrollGroup_t755788567::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t755788567::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t755788567::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t755788567::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t755788567::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t755788567::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t755788567::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t755788567::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t755788567::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t755788567::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (GUILayoutEntry_t3828586629), -1, sizeof(GUILayoutEntry_t3828586629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3042[10] = 
{
	GUILayoutEntry_t3828586629::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3828586629::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3828586629::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3828586629::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3828586629::get_offset_of_rect_4(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3828586629::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (GUILayoutUtility_t996096873), -1, sizeof(GUILayoutUtility_t996096873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3043[5] = 
{
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (LayoutCache_t3120781045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[3] = 
{
	LayoutCache_t3120781045::get_offset_of_topLevel_0(),
	LayoutCache_t3120781045::get_offset_of_layoutGroups_1(),
	LayoutCache_t3120781045::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (GUISettings_t622856320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[5] = 
{
	GUISettings_t622856320::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t622856320::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t622856320::get_offset_of_m_CursorColor_2(),
	GUISettings_t622856320::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t622856320::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (GUISkin_t1436893342), -1, sizeof(GUISkin_t1436893342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3046[27] = 
{
	GUISkin_t1436893342::get_offset_of_m_Font_2(),
	GUISkin_t1436893342::get_offset_of_m_box_3(),
	GUISkin_t1436893342::get_offset_of_m_button_4(),
	GUISkin_t1436893342::get_offset_of_m_toggle_5(),
	GUISkin_t1436893342::get_offset_of_m_label_6(),
	GUISkin_t1436893342::get_offset_of_m_textField_7(),
	GUISkin_t1436893342::get_offset_of_m_textArea_8(),
	GUISkin_t1436893342::get_offset_of_m_window_9(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1436893342::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1436893342::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1436893342::get_offset_of_m_ScrollView_22(),
	GUISkin_t1436893342::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1436893342::get_offset_of_m_Settings_24(),
	GUISkin_t1436893342_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1436893342::get_offset_of_m_Styles_26(),
	GUISkin_t1436893342_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1436893342_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (SkinChangedDelegate_t3594822336), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (GUIStyleState_t3801000545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[3] = 
{
	GUIStyleState_t3801000545::get_offset_of_m_Ptr_0(),
	GUIStyleState_t3801000545::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t3801000545::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (ImagePosition_t3491916276)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3049[5] = 
{
	ImagePosition_t3491916276::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (GUIStyle_t1799908754), -1, sizeof(GUIStyle_t1799908754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3050[16] = 
{
	GUIStyle_t1799908754::get_offset_of_m_Ptr_0(),
	GUIStyle_t1799908754::get_offset_of_m_Normal_1(),
	GUIStyle_t1799908754::get_offset_of_m_Hover_2(),
	GUIStyle_t1799908754::get_offset_of_m_Active_3(),
	GUIStyle_t1799908754::get_offset_of_m_Focused_4(),
	GUIStyle_t1799908754::get_offset_of_m_OnNormal_5(),
	GUIStyle_t1799908754::get_offset_of_m_OnHover_6(),
	GUIStyle_t1799908754::get_offset_of_m_OnActive_7(),
	GUIStyle_t1799908754::get_offset_of_m_OnFocused_8(),
	GUIStyle_t1799908754::get_offset_of_m_Border_9(),
	GUIStyle_t1799908754::get_offset_of_m_Padding_10(),
	GUIStyle_t1799908754::get_offset_of_m_Margin_11(),
	GUIStyle_t1799908754::get_offset_of_m_Overflow_12(),
	GUIStyle_t1799908754::get_offset_of_m_FontInternal_13(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (TextClipping_t2573530411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3051[3] = 
{
	TextClipping_t2573530411::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (GUITargetAttribute_t863467180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (ExitGUIException_t1618397098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (GUIUtility_t3275770671), -1, sizeof(GUIUtility_t3275770671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3054[4] = 
{
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (ScrollViewState_t3820542997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (SliderState_t1595681032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	SliderState_t1595681032::get_offset_of_dragStartPos_0(),
	SliderState_t1595681032::get_offset_of_dragStartValue_1(),
	SliderState_t1595681032::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (TextEditor_t3975561390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[16] = 
{
	TextEditor_t3975561390::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t3975561390::get_offset_of_controlID_1(),
	TextEditor_t3975561390::get_offset_of_style_2(),
	TextEditor_t3975561390::get_offset_of_multiline_3(),
	TextEditor_t3975561390::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t3975561390::get_offset_of_isPasswordField_5(),
	TextEditor_t3975561390::get_offset_of_scrollOffset_6(),
	TextEditor_t3975561390::get_offset_of_m_Content_7(),
	TextEditor_t3975561390::get_offset_of_m_CursorIndex_8(),
	TextEditor_t3975561390::get_offset_of_m_SelectIndex_9(),
	TextEditor_t3975561390::get_offset_of_m_RevealCursor_10(),
	TextEditor_t3975561390::get_offset_of_m_MouseDragSelectsWholeWords_11(),
	TextEditor_t3975561390::get_offset_of_m_DblClickInitPos_12(),
	TextEditor_t3975561390::get_offset_of_m_DblClickSnap_13(),
	TextEditor_t3975561390::get_offset_of_m_bJustSelected_14(),
	TextEditor_t3975561390::get_offset_of_m_iAltCursorPos_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (DblClickSnapping_t1119726228)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[3] = 
{
	DblClickSnapping_t1119726228::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (Internal_DrawArguments_t2834709342)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t2834709342 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3059[6] = 
{
	Internal_DrawArguments_t2834709342::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (Internal_DrawWithTextSelectionArguments_t1327795077)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t1327795077 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3060[11] = 
{
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (WebRequestUtils_t4100941042), -1, sizeof(WebRequestUtils_t4100941042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3061[1] = 
{
	WebRequestUtils_t4100941042_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (DownloadHandler_t1216180266), sizeof(DownloadHandler_t1216180266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3062[1] = 
{
	DownloadHandler_t1216180266::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (DownloadHandlerAudioClip_t1520641178), sizeof(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (CustomEventData_t1269126727), sizeof(CustomEventData_t1269126727_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3064[1] = 
{
	CustomEventData_t1269126727::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (AnalyticsResult_t3037633135)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3065[9] = 
{
	AnalyticsResult_t3037633135::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (Analytics_t2007048212), -1, sizeof(Analytics_t2007048212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3066[1] = 
{
	Analytics_t2007048212_StaticFields::get_offset_of_s_UnityAnalyticsHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (UnityAnalyticsHandler_t3238795095), sizeof(UnityAnalyticsHandler_t3238795095_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3067[1] = 
{
	UnityAnalyticsHandler_t3238795095::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (RemoteSettings_t392466225), -1, sizeof(RemoteSettings_t392466225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3068[1] = 
{
	RemoteSettings_t392466225_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (UpdatedEventHandler_t3033456180), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (GestureRecognizer_t3303813975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[16] = 
{
	GestureRecognizer_t3303813975::get_offset_of_HoldCanceledEvent_0(),
	GestureRecognizer_t3303813975::get_offset_of_HoldCompletedEvent_1(),
	GestureRecognizer_t3303813975::get_offset_of_HoldStartedEvent_2(),
	GestureRecognizer_t3303813975::get_offset_of_TappedEvent_3(),
	GestureRecognizer_t3303813975::get_offset_of_ManipulationCanceledEvent_4(),
	GestureRecognizer_t3303813975::get_offset_of_ManipulationCompletedEvent_5(),
	GestureRecognizer_t3303813975::get_offset_of_ManipulationStartedEvent_6(),
	GestureRecognizer_t3303813975::get_offset_of_ManipulationUpdatedEvent_7(),
	GestureRecognizer_t3303813975::get_offset_of_NavigationCanceledEvent_8(),
	GestureRecognizer_t3303813975::get_offset_of_NavigationCompletedEvent_9(),
	GestureRecognizer_t3303813975::get_offset_of_NavigationStartedEvent_10(),
	GestureRecognizer_t3303813975::get_offset_of_NavigationUpdatedEvent_11(),
	GestureRecognizer_t3303813975::get_offset_of_RecognitionEndedEvent_12(),
	GestureRecognizer_t3303813975::get_offset_of_RecognitionStartedEvent_13(),
	GestureRecognizer_t3303813975::get_offset_of_GestureErrorEvent_14(),
	GestureRecognizer_t3303813975::get_offset_of_m_Recognizer_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (HoldCanceledEventDelegate_t1855824201), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (HoldCompletedEventDelegate_t3501137495), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (HoldStartedEventDelegate_t3415544547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (TappedEventDelegate_t2437856093), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ManipulationCanceledEventDelegate_t2553296249), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (ManipulationCompletedEventDelegate_t2759974023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (ManipulationStartedEventDelegate_t2487153075), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (ManipulationUpdatedEventDelegate_t2394416487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (NavigationCanceledEventDelegate_t1045620518), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (NavigationCompletedEventDelegate_t1982950364), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (NavigationStartedEventDelegate_t3123342528), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (NavigationUpdatedEventDelegate_t3960847450), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (RecognitionEndedEventDelegate_t831649826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (RecognitionStartedEventDelegate_t3116179703), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (GestureErrorDelegate_t2343902086), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (GestureEventType_t343438253)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[16] = 
{
	GestureEventType_t343438253::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (SurfaceChange_t769713231)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	SurfaceChange_t769713231::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (SurfaceId_t4207907960)+ sizeof (Il2CppObject), sizeof(SurfaceId_t4207907960 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3088[1] = 
{
	SurfaceId_t4207907960::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (SurfaceData_t3147297845)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[6] = 
{
	SurfaceData_t3147297845::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t3147297845::get_offset_of_outputMesh_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t3147297845::get_offset_of_outputAnchor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t3147297845::get_offset_of_outputCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t3147297845::get_offset_of_trianglesPerCubicMeter_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t3147297845::get_offset_of_bakeCollider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (SurfaceObserver_t3799331897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[1] = 
{
	SurfaceObserver_t3799331897::get_offset_of_m_Observer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (SurfaceChangedDelegate_t3444411290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (SurfaceDataReadyDelegate_t1375437319), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (WorldAnchorStore_t1514582394), -1, sizeof(WorldAnchorStore_t1514582394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3093[2] = 
{
	WorldAnchorStore_t1514582394_StaticFields::get_offset_of_s_Instance_0(),
	WorldAnchorStore_t1514582394::get_offset_of_m_NativePtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (GetAsyncDelegate_t1561547849), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (SerializationCompletionReason_t3597317192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3095[5] = 
{
	SerializationCompletionReason_t3597317192::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (WorldAnchorTransferBatch_t4032480564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[1] = 
{
	WorldAnchorTransferBatch_t4032480564::get_offset_of_m_NativePtr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (SerializationDataAvailableDelegate_t2381700809), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (SerializationCompleteDelegate_t389490593), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (DeserializationCompleteDelegate_t2482917876), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
