﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CAPI
struct  CAPI_t3094615495  : public Il2CppObject
{
public:

public:
};

struct CAPI_t3094615495_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.CAPI::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.CAPI::<>f__switch$map5
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map5_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_0() { return static_cast<int32_t>(offsetof(CAPI_t3094615495_StaticFields, ___U3CU3Ef__switchU24map4_0)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_0() const { return ___U3CU3Ef__switchU24map4_0; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_0() { return &___U3CU3Ef__switchU24map4_0; }
	inline void set_U3CU3Ef__switchU24map4_0(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_1() { return static_cast<int32_t>(offsetof(CAPI_t3094615495_StaticFields, ___U3CU3Ef__switchU24map5_1)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map5_1() const { return ___U3CU3Ef__switchU24map5_1; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map5_1() { return &___U3CU3Ef__switchU24map5_1; }
	inline void set_U3CU3Ef__switchU24map5_1(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map5_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
