﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Positions
struct  Positions_t3593914952  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Positions::positions
	ArrayList_t4252133567 * ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(Positions_t3593914952, ___positions_0)); }
	inline ArrayList_t4252133567 * get_positions_0() const { return ___positions_0; }
	inline ArrayList_t4252133567 ** get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(ArrayList_t4252133567 * value)
	{
		___positions_0 = value;
		Il2CppCodeGenWriteBarrier(&___positions_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
