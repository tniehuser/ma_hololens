﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Util.TokenizerShortBlock
struct TokenizerShortBlock_t1058612006;
// System.Security.Util.TokenizerStringBlock
struct TokenizerStringBlock_t1488754793;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.TokenizerStream
struct  TokenizerStream_t3223310371  : public Il2CppObject
{
public:
	// System.Int32 System.Security.Util.TokenizerStream::m_countTokens
	int32_t ___m_countTokens_0;
	// System.Security.Util.TokenizerShortBlock System.Security.Util.TokenizerStream::m_headTokens
	TokenizerShortBlock_t1058612006 * ___m_headTokens_1;
	// System.Security.Util.TokenizerShortBlock System.Security.Util.TokenizerStream::m_lastTokens
	TokenizerShortBlock_t1058612006 * ___m_lastTokens_2;
	// System.Security.Util.TokenizerShortBlock System.Security.Util.TokenizerStream::m_currentTokens
	TokenizerShortBlock_t1058612006 * ___m_currentTokens_3;
	// System.Int32 System.Security.Util.TokenizerStream::m_indexTokens
	int32_t ___m_indexTokens_4;
	// System.Security.Util.TokenizerStringBlock System.Security.Util.TokenizerStream::m_headStrings
	TokenizerStringBlock_t1488754793 * ___m_headStrings_5;
	// System.Security.Util.TokenizerStringBlock System.Security.Util.TokenizerStream::m_currentStrings
	TokenizerStringBlock_t1488754793 * ___m_currentStrings_6;
	// System.Int32 System.Security.Util.TokenizerStream::m_indexStrings
	int32_t ___m_indexStrings_7;

public:
	inline static int32_t get_offset_of_m_countTokens_0() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_countTokens_0)); }
	inline int32_t get_m_countTokens_0() const { return ___m_countTokens_0; }
	inline int32_t* get_address_of_m_countTokens_0() { return &___m_countTokens_0; }
	inline void set_m_countTokens_0(int32_t value)
	{
		___m_countTokens_0 = value;
	}

	inline static int32_t get_offset_of_m_headTokens_1() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_headTokens_1)); }
	inline TokenizerShortBlock_t1058612006 * get_m_headTokens_1() const { return ___m_headTokens_1; }
	inline TokenizerShortBlock_t1058612006 ** get_address_of_m_headTokens_1() { return &___m_headTokens_1; }
	inline void set_m_headTokens_1(TokenizerShortBlock_t1058612006 * value)
	{
		___m_headTokens_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_headTokens_1, value);
	}

	inline static int32_t get_offset_of_m_lastTokens_2() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_lastTokens_2)); }
	inline TokenizerShortBlock_t1058612006 * get_m_lastTokens_2() const { return ___m_lastTokens_2; }
	inline TokenizerShortBlock_t1058612006 ** get_address_of_m_lastTokens_2() { return &___m_lastTokens_2; }
	inline void set_m_lastTokens_2(TokenizerShortBlock_t1058612006 * value)
	{
		___m_lastTokens_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_lastTokens_2, value);
	}

	inline static int32_t get_offset_of_m_currentTokens_3() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_currentTokens_3)); }
	inline TokenizerShortBlock_t1058612006 * get_m_currentTokens_3() const { return ___m_currentTokens_3; }
	inline TokenizerShortBlock_t1058612006 ** get_address_of_m_currentTokens_3() { return &___m_currentTokens_3; }
	inline void set_m_currentTokens_3(TokenizerShortBlock_t1058612006 * value)
	{
		___m_currentTokens_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_currentTokens_3, value);
	}

	inline static int32_t get_offset_of_m_indexTokens_4() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_indexTokens_4)); }
	inline int32_t get_m_indexTokens_4() const { return ___m_indexTokens_4; }
	inline int32_t* get_address_of_m_indexTokens_4() { return &___m_indexTokens_4; }
	inline void set_m_indexTokens_4(int32_t value)
	{
		___m_indexTokens_4 = value;
	}

	inline static int32_t get_offset_of_m_headStrings_5() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_headStrings_5)); }
	inline TokenizerStringBlock_t1488754793 * get_m_headStrings_5() const { return ___m_headStrings_5; }
	inline TokenizerStringBlock_t1488754793 ** get_address_of_m_headStrings_5() { return &___m_headStrings_5; }
	inline void set_m_headStrings_5(TokenizerStringBlock_t1488754793 * value)
	{
		___m_headStrings_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_headStrings_5, value);
	}

	inline static int32_t get_offset_of_m_currentStrings_6() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_currentStrings_6)); }
	inline TokenizerStringBlock_t1488754793 * get_m_currentStrings_6() const { return ___m_currentStrings_6; }
	inline TokenizerStringBlock_t1488754793 ** get_address_of_m_currentStrings_6() { return &___m_currentStrings_6; }
	inline void set_m_currentStrings_6(TokenizerStringBlock_t1488754793 * value)
	{
		___m_currentStrings_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_currentStrings_6, value);
	}

	inline static int32_t get_offset_of_m_indexStrings_7() { return static_cast<int32_t>(offsetof(TokenizerStream_t3223310371, ___m_indexStrings_7)); }
	inline int32_t get_m_indexStrings_7() const { return ___m_indexStrings_7; }
	inline int32_t* get_address_of_m_indexStrings_7() { return &___m_indexStrings_7; }
	inline void set_m_indexStrings_7(int32_t value)
	{
		___m_indexStrings_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
