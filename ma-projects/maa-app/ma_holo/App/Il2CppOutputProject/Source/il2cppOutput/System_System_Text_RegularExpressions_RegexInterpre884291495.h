﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_RegexRunner3983612747.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t2469392150;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t2204811018;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexInterpreter
struct  RegexInterpreter_t884291495  : public RegexRunner_t3983612747
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runoperator
	int32_t ___runoperator_18;
	// System.Int32[] System.Text.RegularExpressions.RegexInterpreter::runcodes
	Int32U5BU5D_t3030399641* ___runcodes_19;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runcodepos
	int32_t ___runcodepos_20;
	// System.String[] System.Text.RegularExpressions.RegexInterpreter::runstrings
	StringU5BU5D_t1642385972* ___runstrings_21;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexInterpreter::runcode
	RegexCode_t2469392150 * ___runcode_22;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexInterpreter::runfcPrefix
	RegexPrefix_t1013837165 * ___runfcPrefix_23;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexInterpreter::runbmPrefix
	RegexBoyerMoore_t2204811018 * ___runbmPrefix_24;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runanchors
	int32_t ___runanchors_25;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runrtl
	bool ___runrtl_26;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runci
	bool ___runci_27;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexInterpreter::runculture
	CultureInfo_t3500843524 * ___runculture_28;

public:
	inline static int32_t get_offset_of_runoperator_18() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runoperator_18)); }
	inline int32_t get_runoperator_18() const { return ___runoperator_18; }
	inline int32_t* get_address_of_runoperator_18() { return &___runoperator_18; }
	inline void set_runoperator_18(int32_t value)
	{
		___runoperator_18 = value;
	}

	inline static int32_t get_offset_of_runcodes_19() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcodes_19)); }
	inline Int32U5BU5D_t3030399641* get_runcodes_19() const { return ___runcodes_19; }
	inline Int32U5BU5D_t3030399641** get_address_of_runcodes_19() { return &___runcodes_19; }
	inline void set_runcodes_19(Int32U5BU5D_t3030399641* value)
	{
		___runcodes_19 = value;
		Il2CppCodeGenWriteBarrier(&___runcodes_19, value);
	}

	inline static int32_t get_offset_of_runcodepos_20() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcodepos_20)); }
	inline int32_t get_runcodepos_20() const { return ___runcodepos_20; }
	inline int32_t* get_address_of_runcodepos_20() { return &___runcodepos_20; }
	inline void set_runcodepos_20(int32_t value)
	{
		___runcodepos_20 = value;
	}

	inline static int32_t get_offset_of_runstrings_21() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runstrings_21)); }
	inline StringU5BU5D_t1642385972* get_runstrings_21() const { return ___runstrings_21; }
	inline StringU5BU5D_t1642385972** get_address_of_runstrings_21() { return &___runstrings_21; }
	inline void set_runstrings_21(StringU5BU5D_t1642385972* value)
	{
		___runstrings_21 = value;
		Il2CppCodeGenWriteBarrier(&___runstrings_21, value);
	}

	inline static int32_t get_offset_of_runcode_22() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runcode_22)); }
	inline RegexCode_t2469392150 * get_runcode_22() const { return ___runcode_22; }
	inline RegexCode_t2469392150 ** get_address_of_runcode_22() { return &___runcode_22; }
	inline void set_runcode_22(RegexCode_t2469392150 * value)
	{
		___runcode_22 = value;
		Il2CppCodeGenWriteBarrier(&___runcode_22, value);
	}

	inline static int32_t get_offset_of_runfcPrefix_23() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runfcPrefix_23)); }
	inline RegexPrefix_t1013837165 * get_runfcPrefix_23() const { return ___runfcPrefix_23; }
	inline RegexPrefix_t1013837165 ** get_address_of_runfcPrefix_23() { return &___runfcPrefix_23; }
	inline void set_runfcPrefix_23(RegexPrefix_t1013837165 * value)
	{
		___runfcPrefix_23 = value;
		Il2CppCodeGenWriteBarrier(&___runfcPrefix_23, value);
	}

	inline static int32_t get_offset_of_runbmPrefix_24() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runbmPrefix_24)); }
	inline RegexBoyerMoore_t2204811018 * get_runbmPrefix_24() const { return ___runbmPrefix_24; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of_runbmPrefix_24() { return &___runbmPrefix_24; }
	inline void set_runbmPrefix_24(RegexBoyerMoore_t2204811018 * value)
	{
		___runbmPrefix_24 = value;
		Il2CppCodeGenWriteBarrier(&___runbmPrefix_24, value);
	}

	inline static int32_t get_offset_of_runanchors_25() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runanchors_25)); }
	inline int32_t get_runanchors_25() const { return ___runanchors_25; }
	inline int32_t* get_address_of_runanchors_25() { return &___runanchors_25; }
	inline void set_runanchors_25(int32_t value)
	{
		___runanchors_25 = value;
	}

	inline static int32_t get_offset_of_runrtl_26() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runrtl_26)); }
	inline bool get_runrtl_26() const { return ___runrtl_26; }
	inline bool* get_address_of_runrtl_26() { return &___runrtl_26; }
	inline void set_runrtl_26(bool value)
	{
		___runrtl_26 = value;
	}

	inline static int32_t get_offset_of_runci_27() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runci_27)); }
	inline bool get_runci_27() const { return ___runci_27; }
	inline bool* get_address_of_runci_27() { return &___runci_27; }
	inline void set_runci_27(bool value)
	{
		___runci_27 = value;
	}

	inline static int32_t get_offset_of_runculture_28() { return static_cast<int32_t>(offsetof(RegexInterpreter_t884291495, ___runculture_28)); }
	inline CultureInfo_t3500843524 * get_runculture_28() const { return ___runculture_28; }
	inline CultureInfo_t3500843524 ** get_address_of_runculture_28() { return &___runculture_28; }
	inline void set_runculture_28(CultureInfo_t3500843524 * value)
	{
		___runculture_28 = value;
		Il2CppCodeGenWriteBarrier(&___runculture_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
