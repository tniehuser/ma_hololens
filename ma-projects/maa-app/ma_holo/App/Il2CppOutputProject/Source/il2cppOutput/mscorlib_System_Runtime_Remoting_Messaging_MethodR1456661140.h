﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Reflection.MethodBase
struct MethodBase_t904190842;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1927440687;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Runtime.Remoting.Messaging.ArgInfo
struct ArgInfo_t688271106;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t645865707;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.MethodResponse
struct  MethodResponse_t1456661140  : public Il2CppObject
{
public:
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_methodName
	String_t* ____methodName_0;
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_uri
	String_t* ____uri_1;
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_typeName
	String_t* ____typeName_2;
	// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodResponse::_methodBase
	MethodBase_t904190842 * ____methodBase_3;
	// System.Object System.Runtime.Remoting.Messaging.MethodResponse::_returnValue
	Il2CppObject * ____returnValue_4;
	// System.Exception System.Runtime.Remoting.Messaging.MethodResponse::_exception
	Exception_t1927440687 * ____exception_5;
	// System.Type[] System.Runtime.Remoting.Messaging.MethodResponse::_methodSignature
	TypeU5BU5D_t1664964607* ____methodSignature_6;
	// System.Runtime.Remoting.Messaging.ArgInfo System.Runtime.Remoting.Messaging.MethodResponse::_inArgInfo
	ArgInfo_t688271106 * ____inArgInfo_7;
	// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::_args
	ObjectU5BU5D_t3614634134* ____args_8;
	// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::_outArgs
	ObjectU5BU5D_t3614634134* ____outArgs_9;
	// System.Runtime.Remoting.Messaging.IMethodCallMessage System.Runtime.Remoting.Messaging.MethodResponse::_callMsg
	Il2CppObject * ____callMsg_10;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodResponse::_callContext
	LogicalCallContext_t725724420 * ____callContext_11;
	// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodResponse::ExternalProperties
	Il2CppObject * ___ExternalProperties_12;
	// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodResponse::InternalProperties
	Il2CppObject * ___InternalProperties_13;

public:
	inline static int32_t get_offset_of__methodName_0() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____methodName_0)); }
	inline String_t* get__methodName_0() const { return ____methodName_0; }
	inline String_t** get_address_of__methodName_0() { return &____methodName_0; }
	inline void set__methodName_0(String_t* value)
	{
		____methodName_0 = value;
		Il2CppCodeGenWriteBarrier(&____methodName_0, value);
	}

	inline static int32_t get_offset_of__uri_1() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____uri_1)); }
	inline String_t* get__uri_1() const { return ____uri_1; }
	inline String_t** get_address_of__uri_1() { return &____uri_1; }
	inline void set__uri_1(String_t* value)
	{
		____uri_1 = value;
		Il2CppCodeGenWriteBarrier(&____uri_1, value);
	}

	inline static int32_t get_offset_of__typeName_2() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____typeName_2)); }
	inline String_t* get__typeName_2() const { return ____typeName_2; }
	inline String_t** get_address_of__typeName_2() { return &____typeName_2; }
	inline void set__typeName_2(String_t* value)
	{
		____typeName_2 = value;
		Il2CppCodeGenWriteBarrier(&____typeName_2, value);
	}

	inline static int32_t get_offset_of__methodBase_3() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____methodBase_3)); }
	inline MethodBase_t904190842 * get__methodBase_3() const { return ____methodBase_3; }
	inline MethodBase_t904190842 ** get_address_of__methodBase_3() { return &____methodBase_3; }
	inline void set__methodBase_3(MethodBase_t904190842 * value)
	{
		____methodBase_3 = value;
		Il2CppCodeGenWriteBarrier(&____methodBase_3, value);
	}

	inline static int32_t get_offset_of__returnValue_4() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____returnValue_4)); }
	inline Il2CppObject * get__returnValue_4() const { return ____returnValue_4; }
	inline Il2CppObject ** get_address_of__returnValue_4() { return &____returnValue_4; }
	inline void set__returnValue_4(Il2CppObject * value)
	{
		____returnValue_4 = value;
		Il2CppCodeGenWriteBarrier(&____returnValue_4, value);
	}

	inline static int32_t get_offset_of__exception_5() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____exception_5)); }
	inline Exception_t1927440687 * get__exception_5() const { return ____exception_5; }
	inline Exception_t1927440687 ** get_address_of__exception_5() { return &____exception_5; }
	inline void set__exception_5(Exception_t1927440687 * value)
	{
		____exception_5 = value;
		Il2CppCodeGenWriteBarrier(&____exception_5, value);
	}

	inline static int32_t get_offset_of__methodSignature_6() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____methodSignature_6)); }
	inline TypeU5BU5D_t1664964607* get__methodSignature_6() const { return ____methodSignature_6; }
	inline TypeU5BU5D_t1664964607** get_address_of__methodSignature_6() { return &____methodSignature_6; }
	inline void set__methodSignature_6(TypeU5BU5D_t1664964607* value)
	{
		____methodSignature_6 = value;
		Il2CppCodeGenWriteBarrier(&____methodSignature_6, value);
	}

	inline static int32_t get_offset_of__inArgInfo_7() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____inArgInfo_7)); }
	inline ArgInfo_t688271106 * get__inArgInfo_7() const { return ____inArgInfo_7; }
	inline ArgInfo_t688271106 ** get_address_of__inArgInfo_7() { return &____inArgInfo_7; }
	inline void set__inArgInfo_7(ArgInfo_t688271106 * value)
	{
		____inArgInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&____inArgInfo_7, value);
	}

	inline static int32_t get_offset_of__args_8() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____args_8)); }
	inline ObjectU5BU5D_t3614634134* get__args_8() const { return ____args_8; }
	inline ObjectU5BU5D_t3614634134** get_address_of__args_8() { return &____args_8; }
	inline void set__args_8(ObjectU5BU5D_t3614634134* value)
	{
		____args_8 = value;
		Il2CppCodeGenWriteBarrier(&____args_8, value);
	}

	inline static int32_t get_offset_of__outArgs_9() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____outArgs_9)); }
	inline ObjectU5BU5D_t3614634134* get__outArgs_9() const { return ____outArgs_9; }
	inline ObjectU5BU5D_t3614634134** get_address_of__outArgs_9() { return &____outArgs_9; }
	inline void set__outArgs_9(ObjectU5BU5D_t3614634134* value)
	{
		____outArgs_9 = value;
		Il2CppCodeGenWriteBarrier(&____outArgs_9, value);
	}

	inline static int32_t get_offset_of__callMsg_10() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____callMsg_10)); }
	inline Il2CppObject * get__callMsg_10() const { return ____callMsg_10; }
	inline Il2CppObject ** get_address_of__callMsg_10() { return &____callMsg_10; }
	inline void set__callMsg_10(Il2CppObject * value)
	{
		____callMsg_10 = value;
		Il2CppCodeGenWriteBarrier(&____callMsg_10, value);
	}

	inline static int32_t get_offset_of__callContext_11() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ____callContext_11)); }
	inline LogicalCallContext_t725724420 * get__callContext_11() const { return ____callContext_11; }
	inline LogicalCallContext_t725724420 ** get_address_of__callContext_11() { return &____callContext_11; }
	inline void set__callContext_11(LogicalCallContext_t725724420 * value)
	{
		____callContext_11 = value;
		Il2CppCodeGenWriteBarrier(&____callContext_11, value);
	}

	inline static int32_t get_offset_of_ExternalProperties_12() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ___ExternalProperties_12)); }
	inline Il2CppObject * get_ExternalProperties_12() const { return ___ExternalProperties_12; }
	inline Il2CppObject ** get_address_of_ExternalProperties_12() { return &___ExternalProperties_12; }
	inline void set_ExternalProperties_12(Il2CppObject * value)
	{
		___ExternalProperties_12 = value;
		Il2CppCodeGenWriteBarrier(&___ExternalProperties_12, value);
	}

	inline static int32_t get_offset_of_InternalProperties_13() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140, ___InternalProperties_13)); }
	inline Il2CppObject * get_InternalProperties_13() const { return ___InternalProperties_13; }
	inline Il2CppObject ** get_address_of_InternalProperties_13() { return &___InternalProperties_13; }
	inline void set_InternalProperties_13(Il2CppObject * value)
	{
		___InternalProperties_13 = value;
		Il2CppCodeGenWriteBarrier(&___InternalProperties_13, value);
	}
};

struct MethodResponse_t1456661140_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Remoting.Messaging.MethodResponse::<>f__switch$mapE
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapE_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_14() { return static_cast<int32_t>(offsetof(MethodResponse_t1456661140_StaticFields, ___U3CU3Ef__switchU24mapE_14)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapE_14() const { return ___U3CU3Ef__switchU24mapE_14; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapE_14() { return &___U3CU3Ef__switchU24mapE_14; }
	inline void set_U3CU3Ef__switchU24mapE_14(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
