﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t2945603725;
// System.Threading.Tasks.Task`1<System.Int32>[]
struct Task_1U5BU5D_t2481017646;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskCache
struct  AsyncTaskCache_t3842594813  : public Il2CppObject
{
public:

public:
};

struct AsyncTaskCache_t3842594813_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Boolean> System.Runtime.CompilerServices.AsyncTaskCache::TrueTask
	Task_1_t2945603725 * ___TrueTask_0;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Runtime.CompilerServices.AsyncTaskCache::FalseTask
	Task_1_t2945603725 * ___FalseTask_1;
	// System.Threading.Tasks.Task`1<System.Int32>[] System.Runtime.CompilerServices.AsyncTaskCache::Int32Tasks
	Task_1U5BU5D_t2481017646* ___Int32Tasks_2;

public:
	inline static int32_t get_offset_of_TrueTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskCache_t3842594813_StaticFields, ___TrueTask_0)); }
	inline Task_1_t2945603725 * get_TrueTask_0() const { return ___TrueTask_0; }
	inline Task_1_t2945603725 ** get_address_of_TrueTask_0() { return &___TrueTask_0; }
	inline void set_TrueTask_0(Task_1_t2945603725 * value)
	{
		___TrueTask_0 = value;
		Il2CppCodeGenWriteBarrier(&___TrueTask_0, value);
	}

	inline static int32_t get_offset_of_FalseTask_1() { return static_cast<int32_t>(offsetof(AsyncTaskCache_t3842594813_StaticFields, ___FalseTask_1)); }
	inline Task_1_t2945603725 * get_FalseTask_1() const { return ___FalseTask_1; }
	inline Task_1_t2945603725 ** get_address_of_FalseTask_1() { return &___FalseTask_1; }
	inline void set_FalseTask_1(Task_1_t2945603725 * value)
	{
		___FalseTask_1 = value;
		Il2CppCodeGenWriteBarrier(&___FalseTask_1, value);
	}

	inline static int32_t get_offset_of_Int32Tasks_2() { return static_cast<int32_t>(offsetof(AsyncTaskCache_t3842594813_StaticFields, ___Int32Tasks_2)); }
	inline Task_1U5BU5D_t2481017646* get_Int32Tasks_2() const { return ___Int32Tasks_2; }
	inline Task_1U5BU5D_t2481017646** get_address_of_Int32Tasks_2() { return &___Int32Tasks_2; }
	inline void set_Int32Tasks_2(Task_1U5BU5D_t2481017646* value)
	{
		___Int32Tasks_2 = value;
		Il2CppCodeGenWriteBarrier(&___Int32Tasks_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
