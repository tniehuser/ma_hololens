﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_RegexCompile1714699756.h"

// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexLWCGCompiler
struct  RegexLWCGCompiler_t1552814047  : public RegexCompiler_t1714699756
{
public:

public:
};

struct RegexLWCGCompiler_t1552814047_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexLWCGCompiler::_regexCount
	int32_t ____regexCount_56;
	// System.Type[] System.Text.RegularExpressions.RegexLWCGCompiler::_paramTypes
	TypeU5BU5D_t1664964607* ____paramTypes_57;

public:
	inline static int32_t get_offset_of__regexCount_56() { return static_cast<int32_t>(offsetof(RegexLWCGCompiler_t1552814047_StaticFields, ____regexCount_56)); }
	inline int32_t get__regexCount_56() const { return ____regexCount_56; }
	inline int32_t* get_address_of__regexCount_56() { return &____regexCount_56; }
	inline void set__regexCount_56(int32_t value)
	{
		____regexCount_56 = value;
	}

	inline static int32_t get_offset_of__paramTypes_57() { return static_cast<int32_t>(offsetof(RegexLWCGCompiler_t1552814047_StaticFields, ____paramTypes_57)); }
	inline TypeU5BU5D_t1664964607* get__paramTypes_57() const { return ____paramTypes_57; }
	inline TypeU5BU5D_t1664964607** get_address_of__paramTypes_57() { return &____paramTypes_57; }
	inline void set__paramTypes_57(TypeU5BU5D_t1664964607* value)
	{
		____paramTypes_57 = value;
		Il2CppCodeGenWriteBarrier(&____paramTypes_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
