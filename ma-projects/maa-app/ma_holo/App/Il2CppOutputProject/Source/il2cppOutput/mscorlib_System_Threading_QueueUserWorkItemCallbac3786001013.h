﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.WaitCallback
struct WaitCallback_t2798937288;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Object
struct Il2CppObject;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.QueueUserWorkItemCallback
struct  QueueUserWorkItemCallback_t3786001013  : public Il2CppObject
{
public:
	// System.Threading.WaitCallback System.Threading.QueueUserWorkItemCallback::callback
	WaitCallback_t2798937288 * ___callback_0;
	// System.Threading.ExecutionContext System.Threading.QueueUserWorkItemCallback::context
	ExecutionContext_t1392266323 * ___context_1;
	// System.Object System.Threading.QueueUserWorkItemCallback::state
	Il2CppObject * ___state_2;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(QueueUserWorkItemCallback_t3786001013, ___callback_0)); }
	inline WaitCallback_t2798937288 * get_callback_0() const { return ___callback_0; }
	inline WaitCallback_t2798937288 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(WaitCallback_t2798937288 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(QueueUserWorkItemCallback_t3786001013, ___context_1)); }
	inline ExecutionContext_t1392266323 * get_context_1() const { return ___context_1; }
	inline ExecutionContext_t1392266323 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ExecutionContext_t1392266323 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(QueueUserWorkItemCallback_t3786001013, ___state_2)); }
	inline Il2CppObject * get_state_2() const { return ___state_2; }
	inline Il2CppObject ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(Il2CppObject * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier(&___state_2, value);
	}
};

struct QueueUserWorkItemCallback_t3786001013_StaticFields
{
public:
	// System.Threading.ContextCallback System.Threading.QueueUserWorkItemCallback::ccb
	ContextCallback_t2287130692 * ___ccb_3;

public:
	inline static int32_t get_offset_of_ccb_3() { return static_cast<int32_t>(offsetof(QueueUserWorkItemCallback_t3786001013_StaticFields, ___ccb_3)); }
	inline ContextCallback_t2287130692 * get_ccb_3() const { return ___ccb_3; }
	inline ContextCallback_t2287130692 ** get_address_of_ccb_3() { return &___ccb_3; }
	inline void set_ccb_3(ContextCallback_t2287130692 * value)
	{
		___ccb_3 = value;
		Il2CppCodeGenWriteBarrier(&___ccb_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
