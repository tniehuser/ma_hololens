﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseValidator3557140249.h"

// System.Xml.Schema.DtdValidator/NamespaceManager
struct NamespaceManager_t1407344;
// System.Xml.HWStack
struct HWStack_t738999989;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DtdValidator
struct  DtdValidator_t1639720164  : public BaseValidator_t3557140249
{
public:
	// System.Xml.HWStack System.Xml.Schema.DtdValidator::validationStack
	HWStack_t738999989 * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::attPresence
	Hashtable_t909839986 * ___attPresence_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DtdValidator::name
	XmlQualifiedName_t1944712516 * ___name_18;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::IDs
	Hashtable_t909839986 * ___IDs_19;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.DtdValidator::idRefListHead
	IdRefNode_t224554150 * ___idRefListHead_20;
	// System.Boolean System.Xml.Schema.DtdValidator::processIdentityConstraints
	bool ___processIdentityConstraints_21;

public:
	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___validationStack_16)); }
	inline HWStack_t738999989 * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t738999989 ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t738999989 * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier(&___validationStack_16, value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___attPresence_17)); }
	inline Hashtable_t909839986 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t909839986 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t909839986 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier(&___attPresence_17, value);
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___name_18)); }
	inline XmlQualifiedName_t1944712516 * get_name_18() const { return ___name_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(XmlQualifiedName_t1944712516 * value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier(&___name_18, value);
	}

	inline static int32_t get_offset_of_IDs_19() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___IDs_19)); }
	inline Hashtable_t909839986 * get_IDs_19() const { return ___IDs_19; }
	inline Hashtable_t909839986 ** get_address_of_IDs_19() { return &___IDs_19; }
	inline void set_IDs_19(Hashtable_t909839986 * value)
	{
		___IDs_19 = value;
		Il2CppCodeGenWriteBarrier(&___IDs_19, value);
	}

	inline static int32_t get_offset_of_idRefListHead_20() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___idRefListHead_20)); }
	inline IdRefNode_t224554150 * get_idRefListHead_20() const { return ___idRefListHead_20; }
	inline IdRefNode_t224554150 ** get_address_of_idRefListHead_20() { return &___idRefListHead_20; }
	inline void set_idRefListHead_20(IdRefNode_t224554150 * value)
	{
		___idRefListHead_20 = value;
		Il2CppCodeGenWriteBarrier(&___idRefListHead_20, value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_21() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___processIdentityConstraints_21)); }
	inline bool get_processIdentityConstraints_21() const { return ___processIdentityConstraints_21; }
	inline bool* get_address_of_processIdentityConstraints_21() { return &___processIdentityConstraints_21; }
	inline void set_processIdentityConstraints_21(bool value)
	{
		___processIdentityConstraints_21 = value;
	}
};

struct DtdValidator_t1639720164_StaticFields
{
public:
	// System.Xml.Schema.DtdValidator/NamespaceManager System.Xml.Schema.DtdValidator::namespaceManager
	NamespaceManager_t1407344 * ___namespaceManager_15;

public:
	inline static int32_t get_offset_of_namespaceManager_15() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164_StaticFields, ___namespaceManager_15)); }
	inline NamespaceManager_t1407344 * get_namespaceManager_15() const { return ___namespaceManager_15; }
	inline NamespaceManager_t1407344 ** get_address_of_namespaceManager_15() { return &___namespaceManager_15; }
	inline void set_namespaceManager_15(NamespaceManager_t1407344 * value)
	{
		___namespaceManager_15 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceManager_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
