﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayFragment_1_t2654867320;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SparselyPopulatedArray`1<System.Threading.CancellationCallbackInfo>
struct  SparselyPopulatedArray_1_t4170979568  : public Il2CppObject
{
public:
	// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArray`1::m_head
	SparselyPopulatedArrayFragment_1_t2654867320 * ___m_head_0;
	// System.Threading.SparselyPopulatedArrayFragment`1<T> modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SparselyPopulatedArray`1::m_tail
	SparselyPopulatedArrayFragment_1_t2654867320 * ___m_tail_1;

public:
	inline static int32_t get_offset_of_m_head_0() { return static_cast<int32_t>(offsetof(SparselyPopulatedArray_1_t4170979568, ___m_head_0)); }
	inline SparselyPopulatedArrayFragment_1_t2654867320 * get_m_head_0() const { return ___m_head_0; }
	inline SparselyPopulatedArrayFragment_1_t2654867320 ** get_address_of_m_head_0() { return &___m_head_0; }
	inline void set_m_head_0(SparselyPopulatedArrayFragment_1_t2654867320 * value)
	{
		___m_head_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_head_0, value);
	}

	inline static int32_t get_offset_of_m_tail_1() { return static_cast<int32_t>(offsetof(SparselyPopulatedArray_1_t4170979568, ___m_tail_1)); }
	inline SparselyPopulatedArrayFragment_1_t2654867320 * get_m_tail_1() const { return ___m_tail_1; }
	inline SparselyPopulatedArrayFragment_1_t2654867320 ** get_address_of_m_tail_1() { return &___m_tail_1; }
	inline void set_m_tail_1(SparselyPopulatedArrayFragment_1_t2654867320 * value)
	{
		___m_tail_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_tail_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
