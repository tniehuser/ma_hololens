﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"

// System.String
struct String_t;
// System.ComponentModel.DesignerCategoryAttribute
struct DesignerCategoryAttribute_t1270090451;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignerCategoryAttribute
struct  DesignerCategoryAttribute_t1270090451  : public Attribute_t542643598
{
public:
	// System.String System.ComponentModel.DesignerCategoryAttribute::category
	String_t* ___category_0;
	// System.String System.ComponentModel.DesignerCategoryAttribute::typeId
	String_t* ___typeId_1;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451, ___category_0)); }
	inline String_t* get_category_0() const { return ___category_0; }
	inline String_t** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(String_t* value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier(&___category_0, value);
	}

	inline static int32_t get_offset_of_typeId_1() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451, ___typeId_1)); }
	inline String_t* get_typeId_1() const { return ___typeId_1; }
	inline String_t** get_address_of_typeId_1() { return &___typeId_1; }
	inline void set_typeId_1(String_t* value)
	{
		___typeId_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeId_1, value);
	}
};

struct DesignerCategoryAttribute_t1270090451_StaticFields
{
public:
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Component
	DesignerCategoryAttribute_t1270090451 * ___Component_2;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Default
	DesignerCategoryAttribute_t1270090451 * ___Default_3;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Form
	DesignerCategoryAttribute_t1270090451 * ___Form_4;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Generic
	DesignerCategoryAttribute_t1270090451 * ___Generic_5;

public:
	inline static int32_t get_offset_of_Component_2() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451_StaticFields, ___Component_2)); }
	inline DesignerCategoryAttribute_t1270090451 * get_Component_2() const { return ___Component_2; }
	inline DesignerCategoryAttribute_t1270090451 ** get_address_of_Component_2() { return &___Component_2; }
	inline void set_Component_2(DesignerCategoryAttribute_t1270090451 * value)
	{
		___Component_2 = value;
		Il2CppCodeGenWriteBarrier(&___Component_2, value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451_StaticFields, ___Default_3)); }
	inline DesignerCategoryAttribute_t1270090451 * get_Default_3() const { return ___Default_3; }
	inline DesignerCategoryAttribute_t1270090451 ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(DesignerCategoryAttribute_t1270090451 * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier(&___Default_3, value);
	}

	inline static int32_t get_offset_of_Form_4() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451_StaticFields, ___Form_4)); }
	inline DesignerCategoryAttribute_t1270090451 * get_Form_4() const { return ___Form_4; }
	inline DesignerCategoryAttribute_t1270090451 ** get_address_of_Form_4() { return &___Form_4; }
	inline void set_Form_4(DesignerCategoryAttribute_t1270090451 * value)
	{
		___Form_4 = value;
		Il2CppCodeGenWriteBarrier(&___Form_4, value);
	}

	inline static int32_t get_offset_of_Generic_5() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_t1270090451_StaticFields, ___Generic_5)); }
	inline DesignerCategoryAttribute_t1270090451 * get_Generic_5() const { return ___Generic_5; }
	inline DesignerCategoryAttribute_t1270090451 ** get_address_of_Generic_5() { return &___Generic_5; }
	inline void set_Generic_5(DesignerCategoryAttribute_t1270090451 * value)
	{
		___Generic_5 = value;
		Il2CppCodeGenWriteBarrier(&___Generic_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
