﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.WeakReference>
struct List_1_t446526699;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Switch
struct  Switch_t2611057356  : public Il2CppObject
{
public:
	// System.String System.Diagnostics.Switch::description
	String_t* ___description_0;
	// System.String System.Diagnostics.Switch::displayName
	String_t* ___displayName_1;
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.Diagnostics.Switch::switchValueString
	String_t* ___switchValueString_2;
	// System.String System.Diagnostics.Switch::defaultValue
	String_t* ___defaultValue_3;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(Switch_t2611057356, ___description_0)); }
	inline String_t* get_description_0() const { return ___description_0; }
	inline String_t** get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(String_t* value)
	{
		___description_0 = value;
		Il2CppCodeGenWriteBarrier(&___description_0, value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(Switch_t2611057356, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier(&___displayName_1, value);
	}

	inline static int32_t get_offset_of_switchValueString_2() { return static_cast<int32_t>(offsetof(Switch_t2611057356, ___switchValueString_2)); }
	inline String_t* get_switchValueString_2() const { return ___switchValueString_2; }
	inline String_t** get_address_of_switchValueString_2() { return &___switchValueString_2; }
	inline void set_switchValueString_2(String_t* value)
	{
		___switchValueString_2 = value;
		Il2CppCodeGenWriteBarrier(&___switchValueString_2, value);
	}

	inline static int32_t get_offset_of_defaultValue_3() { return static_cast<int32_t>(offsetof(Switch_t2611057356, ___defaultValue_3)); }
	inline String_t* get_defaultValue_3() const { return ___defaultValue_3; }
	inline String_t** get_address_of_defaultValue_3() { return &___defaultValue_3; }
	inline void set_defaultValue_3(String_t* value)
	{
		___defaultValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultValue_3, value);
	}
};

struct Switch_t2611057356_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.WeakReference> System.Diagnostics.Switch::switches
	List_1_t446526699 * ___switches_4;
	// System.Int32 System.Diagnostics.Switch::s_LastCollectionCount
	int32_t ___s_LastCollectionCount_5;

public:
	inline static int32_t get_offset_of_switches_4() { return static_cast<int32_t>(offsetof(Switch_t2611057356_StaticFields, ___switches_4)); }
	inline List_1_t446526699 * get_switches_4() const { return ___switches_4; }
	inline List_1_t446526699 ** get_address_of_switches_4() { return &___switches_4; }
	inline void set_switches_4(List_1_t446526699 * value)
	{
		___switches_4 = value;
		Il2CppCodeGenWriteBarrier(&___switches_4, value);
	}

	inline static int32_t get_offset_of_s_LastCollectionCount_5() { return static_cast<int32_t>(offsetof(Switch_t2611057356_StaticFields, ___s_LastCollectionCount_5)); }
	inline int32_t get_s_LastCollectionCount_5() const { return ___s_LastCollectionCount_5; }
	inline int32_t* get_address_of_s_LastCollectionCount_5() { return &___s_LastCollectionCount_5; }
	inline void set_s_LastCollectionCount_5(int32_t value)
	{
		___s_LastCollectionCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
