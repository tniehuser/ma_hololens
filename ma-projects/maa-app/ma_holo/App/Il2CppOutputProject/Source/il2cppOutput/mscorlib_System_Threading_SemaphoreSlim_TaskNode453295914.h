﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task_1_gen2945603725.h"

// System.Threading.SemaphoreSlim/TaskNode
struct TaskNode_t453295914;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SemaphoreSlim/TaskNode
struct  TaskNode_t453295914  : public Task_1_t2945603725
{
public:
	// System.Threading.SemaphoreSlim/TaskNode System.Threading.SemaphoreSlim/TaskNode::Prev
	TaskNode_t453295914 * ___Prev_27;
	// System.Threading.SemaphoreSlim/TaskNode System.Threading.SemaphoreSlim/TaskNode::Next
	TaskNode_t453295914 * ___Next_28;

public:
	inline static int32_t get_offset_of_Prev_27() { return static_cast<int32_t>(offsetof(TaskNode_t453295914, ___Prev_27)); }
	inline TaskNode_t453295914 * get_Prev_27() const { return ___Prev_27; }
	inline TaskNode_t453295914 ** get_address_of_Prev_27() { return &___Prev_27; }
	inline void set_Prev_27(TaskNode_t453295914 * value)
	{
		___Prev_27 = value;
		Il2CppCodeGenWriteBarrier(&___Prev_27, value);
	}

	inline static int32_t get_offset_of_Next_28() { return static_cast<int32_t>(offsetof(TaskNode_t453295914, ___Next_28)); }
	inline TaskNode_t453295914 * get_Next_28() const { return ___Next_28; }
	inline TaskNode_t453295914 ** get_address_of_Next_28() { return &___Next_28; }
	inline void set_Next_28(TaskNode_t453295914 * value)
	{
		___Next_28 = value;
		Il2CppCodeGenWriteBarrier(&___Next_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
