﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"

// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNotation
struct  XmlSchemaNotation_t346281646  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaNotation::name
	String_t* ___name_9;
	// System.String System.Xml.Schema.XmlSchemaNotation::publicId
	String_t* ___publicId_10;
	// System.String System.Xml.Schema.XmlSchemaNotation::systemId
	String_t* ___systemId_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaNotation::qname
	XmlQualifiedName_t1944712516 * ___qname_12;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_publicId_10() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___publicId_10)); }
	inline String_t* get_publicId_10() const { return ___publicId_10; }
	inline String_t** get_address_of_publicId_10() { return &___publicId_10; }
	inline void set_publicId_10(String_t* value)
	{
		___publicId_10 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_10, value);
	}

	inline static int32_t get_offset_of_systemId_11() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___systemId_11)); }
	inline String_t* get_systemId_11() const { return ___systemId_11; }
	inline String_t** get_address_of_systemId_11() { return &___systemId_11; }
	inline void set_systemId_11(String_t* value)
	{
		___systemId_11 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_11, value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___qname_12)); }
	inline XmlQualifiedName_t1944712516 * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_t1944712516 * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier(&___qname_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
