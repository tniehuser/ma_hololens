﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Decoder3792697818.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder
struct  Ucs4Decoder_t2645160027  : public Decoder_t3792697818
{
public:
	// System.Byte[] System.Xml.Ucs4Decoder::lastBytes
	ByteU5BU5D_t3397334013* ___lastBytes_2;
	// System.Int32 System.Xml.Ucs4Decoder::lastBytesCount
	int32_t ___lastBytesCount_3;

public:
	inline static int32_t get_offset_of_lastBytes_2() { return static_cast<int32_t>(offsetof(Ucs4Decoder_t2645160027, ___lastBytes_2)); }
	inline ByteU5BU5D_t3397334013* get_lastBytes_2() const { return ___lastBytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_lastBytes_2() { return &___lastBytes_2; }
	inline void set_lastBytes_2(ByteU5BU5D_t3397334013* value)
	{
		___lastBytes_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastBytes_2, value);
	}

	inline static int32_t get_offset_of_lastBytesCount_3() { return static_cast<int32_t>(offsetof(Ucs4Decoder_t2645160027, ___lastBytesCount_3)); }
	inline int32_t get_lastBytesCount_3() const { return ___lastBytesCount_3; }
	inline int32_t* get_address_of_lastBytesCount_3() { return &___lastBytesCount_3; }
	inline void set_lastBytesCount_3(int32_t value)
	{
		___lastBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
