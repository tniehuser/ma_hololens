﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t339325318;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct Dictionary_2_t3726397491;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathDocument
struct  XPathDocument_t1328191420  : public Il2CppObject
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] System.Xml.XPath.XPathDocument::pageXmlNmsp
	XPathNodeU5BU5D_t339325318* ___pageXmlNmsp_0;
	// System.Int32 System.Xml.XPath.XPathDocument::idxXmlNmsp
	int32_t ___idxXmlNmsp_1;
	// System.Xml.XmlNameTable System.Xml.XPath.XPathDocument::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef> System.Xml.XPath.XPathDocument::mapNmsp
	Dictionary_2_t3726397491 * ___mapNmsp_3;

public:
	inline static int32_t get_offset_of_pageXmlNmsp_0() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___pageXmlNmsp_0)); }
	inline XPathNodeU5BU5D_t339325318* get_pageXmlNmsp_0() const { return ___pageXmlNmsp_0; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageXmlNmsp_0() { return &___pageXmlNmsp_0; }
	inline void set_pageXmlNmsp_0(XPathNodeU5BU5D_t339325318* value)
	{
		___pageXmlNmsp_0 = value;
		Il2CppCodeGenWriteBarrier(&___pageXmlNmsp_0, value);
	}

	inline static int32_t get_offset_of_idxXmlNmsp_1() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___idxXmlNmsp_1)); }
	inline int32_t get_idxXmlNmsp_1() const { return ___idxXmlNmsp_1; }
	inline int32_t* get_address_of_idxXmlNmsp_1() { return &___idxXmlNmsp_1; }
	inline void set_idxXmlNmsp_1(int32_t value)
	{
		___idxXmlNmsp_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_2, value);
	}

	inline static int32_t get_offset_of_mapNmsp_3() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___mapNmsp_3)); }
	inline Dictionary_2_t3726397491 * get_mapNmsp_3() const { return ___mapNmsp_3; }
	inline Dictionary_2_t3726397491 ** get_address_of_mapNmsp_3() { return &___mapNmsp_3; }
	inline void set_mapNmsp_3(Dictionary_2_t3726397491 * value)
	{
		___mapNmsp_3 = value;
		Il2CppCodeGenWriteBarrier(&___mapNmsp_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
