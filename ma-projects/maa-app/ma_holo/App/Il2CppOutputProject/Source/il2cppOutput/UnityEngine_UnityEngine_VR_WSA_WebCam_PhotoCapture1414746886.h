﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.WebCam.PhotoCapture
struct  PhotoCapture_t1414746886  : public Il2CppObject
{
public:
	// System.IntPtr UnityEngine.VR.WSA.WebCam.PhotoCapture::m_NativePtr
	IntPtr_t ___m_NativePtr_1;

public:
	inline static int32_t get_offset_of_m_NativePtr_1() { return static_cast<int32_t>(offsetof(PhotoCapture_t1414746886, ___m_NativePtr_1)); }
	inline IntPtr_t get_m_NativePtr_1() const { return ___m_NativePtr_1; }
	inline IntPtr_t* get_address_of_m_NativePtr_1() { return &___m_NativePtr_1; }
	inline void set_m_NativePtr_1(IntPtr_t value)
	{
		___m_NativePtr_1 = value;
	}
};

struct PhotoCapture_t1414746886_StaticFields
{
public:
	// System.Int64 UnityEngine.VR.WSA.WebCam.PhotoCapture::HR_SUCCESS
	int64_t ___HR_SUCCESS_0;

public:
	inline static int32_t get_offset_of_HR_SUCCESS_0() { return static_cast<int32_t>(offsetof(PhotoCapture_t1414746886_StaticFields, ___HR_SUCCESS_0)); }
	inline int64_t get_HR_SUCCESS_0() const { return ___HR_SUCCESS_0; }
	inline int64_t* get_address_of_HR_SUCCESS_0() { return &___HR_SUCCESS_0; }
	inline void set_HR_SUCCESS_0(int64_t value)
	{
		___HR_SUCCESS_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
