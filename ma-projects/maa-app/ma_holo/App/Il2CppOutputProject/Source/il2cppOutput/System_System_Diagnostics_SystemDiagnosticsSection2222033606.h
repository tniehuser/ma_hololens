﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur2600766927.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.SystemDiagnosticsSection
struct  SystemDiagnosticsSection_t2222033606  : public ConfigurationSection_t2600766927
{
public:

public:
};

struct SystemDiagnosticsSection_t2222033606_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.SystemDiagnosticsSection::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_19;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propAssert
	ConfigurationProperty_t2048066811 * ____propAssert_20;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propPerfCounters
	ConfigurationProperty_t2048066811 * ____propPerfCounters_21;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propSources
	ConfigurationProperty_t2048066811 * ____propSources_22;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propSharedListeners
	ConfigurationProperty_t2048066811 * ____propSharedListeners_23;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propSwitches
	ConfigurationProperty_t2048066811 * ____propSwitches_24;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SystemDiagnosticsSection::_propTrace
	ConfigurationProperty_t2048066811 * ____propTrace_25;

public:
	inline static int32_t get_offset_of__properties_19() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____properties_19)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_19() const { return ____properties_19; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_19() { return &____properties_19; }
	inline void set__properties_19(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_19 = value;
		Il2CppCodeGenWriteBarrier(&____properties_19, value);
	}

	inline static int32_t get_offset_of__propAssert_20() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propAssert_20)); }
	inline ConfigurationProperty_t2048066811 * get__propAssert_20() const { return ____propAssert_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propAssert_20() { return &____propAssert_20; }
	inline void set__propAssert_20(ConfigurationProperty_t2048066811 * value)
	{
		____propAssert_20 = value;
		Il2CppCodeGenWriteBarrier(&____propAssert_20, value);
	}

	inline static int32_t get_offset_of__propPerfCounters_21() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propPerfCounters_21)); }
	inline ConfigurationProperty_t2048066811 * get__propPerfCounters_21() const { return ____propPerfCounters_21; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propPerfCounters_21() { return &____propPerfCounters_21; }
	inline void set__propPerfCounters_21(ConfigurationProperty_t2048066811 * value)
	{
		____propPerfCounters_21 = value;
		Il2CppCodeGenWriteBarrier(&____propPerfCounters_21, value);
	}

	inline static int32_t get_offset_of__propSources_22() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propSources_22)); }
	inline ConfigurationProperty_t2048066811 * get__propSources_22() const { return ____propSources_22; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSources_22() { return &____propSources_22; }
	inline void set__propSources_22(ConfigurationProperty_t2048066811 * value)
	{
		____propSources_22 = value;
		Il2CppCodeGenWriteBarrier(&____propSources_22, value);
	}

	inline static int32_t get_offset_of__propSharedListeners_23() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propSharedListeners_23)); }
	inline ConfigurationProperty_t2048066811 * get__propSharedListeners_23() const { return ____propSharedListeners_23; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSharedListeners_23() { return &____propSharedListeners_23; }
	inline void set__propSharedListeners_23(ConfigurationProperty_t2048066811 * value)
	{
		____propSharedListeners_23 = value;
		Il2CppCodeGenWriteBarrier(&____propSharedListeners_23, value);
	}

	inline static int32_t get_offset_of__propSwitches_24() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propSwitches_24)); }
	inline ConfigurationProperty_t2048066811 * get__propSwitches_24() const { return ____propSwitches_24; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSwitches_24() { return &____propSwitches_24; }
	inline void set__propSwitches_24(ConfigurationProperty_t2048066811 * value)
	{
		____propSwitches_24 = value;
		Il2CppCodeGenWriteBarrier(&____propSwitches_24, value);
	}

	inline static int32_t get_offset_of__propTrace_25() { return static_cast<int32_t>(offsetof(SystemDiagnosticsSection_t2222033606_StaticFields, ____propTrace_25)); }
	inline ConfigurationProperty_t2048066811 * get__propTrace_25() const { return ____propTrace_25; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propTrace_25() { return &____propTrace_25; }
	inline void set__propTrace_25(ConfigurationProperty_t2048066811 * value)
	{
		____propTrace_25 = value;
		Il2CppCodeGenWriteBarrier(&____propTrace_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
