﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_ReadContentAsBinaryHelper_St4184127726.h"

// System.Xml.XmlReader
struct XmlReader_t3675626668;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadContentAsBinaryHelper
struct  ReadContentAsBinaryHelper_t1064946902  : public Il2CppObject
{
public:
	// System.Xml.XmlReader System.Xml.ReadContentAsBinaryHelper::reader
	XmlReader_t3675626668 * ___reader_0;
	// System.Xml.ReadContentAsBinaryHelper/State System.Xml.ReadContentAsBinaryHelper::state
	int32_t ___state_1;
	// System.Int32 System.Xml.ReadContentAsBinaryHelper::valueOffset
	int32_t ___valueOffset_2;
	// System.Boolean System.Xml.ReadContentAsBinaryHelper::isEnd
	bool ___isEnd_3;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t1064946902, ___reader_0)); }
	inline XmlReader_t3675626668 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_t3675626668 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_t3675626668 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t1064946902, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_valueOffset_2() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t1064946902, ___valueOffset_2)); }
	inline int32_t get_valueOffset_2() const { return ___valueOffset_2; }
	inline int32_t* get_address_of_valueOffset_2() { return &___valueOffset_2; }
	inline void set_valueOffset_2(int32_t value)
	{
		___valueOffset_2 = value;
	}

	inline static int32_t get_offset_of_isEnd_3() { return static_cast<int32_t>(offsetof(ReadContentAsBinaryHelper_t1064946902, ___isEnd_3)); }
	inline bool get_isEnd_3() const { return ___isEnd_3; }
	inline bool* get_address_of_isEnd_3() { return &___isEnd_3; }
	inline void set_isEnd_3(bool value)
	{
		___isEnd_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
