﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly
struct  BinaryCrossAppDomainAssembly_t3309678508  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly::assemId
	int32_t ___assemId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly::assemblyIndex
	int32_t ___assemblyIndex_1;

public:
	inline static int32_t get_offset_of_assemId_0() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainAssembly_t3309678508, ___assemId_0)); }
	inline int32_t get_assemId_0() const { return ___assemId_0; }
	inline int32_t* get_address_of_assemId_0() { return &___assemId_0; }
	inline void set_assemId_0(int32_t value)
	{
		___assemId_0 = value;
	}

	inline static int32_t get_offset_of_assemblyIndex_1() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainAssembly_t3309678508, ___assemblyIndex_1)); }
	inline int32_t get_assemblyIndex_1() const { return ___assemblyIndex_1; }
	inline int32_t* get_address_of_assemblyIndex_1() { return &___assemblyIndex_1; }
	inline void set_assemblyIndex_1(int32_t value)
	{
		___assemblyIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
