﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array/QSortStack
struct  QSortStack_t3667817390 
{
public:
	// System.Int32 System.Array/QSortStack::high
	int32_t ___high_0;
	// System.Int32 System.Array/QSortStack::low
	int32_t ___low_1;

public:
	inline static int32_t get_offset_of_high_0() { return static_cast<int32_t>(offsetof(QSortStack_t3667817390, ___high_0)); }
	inline int32_t get_high_0() const { return ___high_0; }
	inline int32_t* get_address_of_high_0() { return &___high_0; }
	inline void set_high_0(int32_t value)
	{
		___high_0 = value;
	}

	inline static int32_t get_offset_of_low_1() { return static_cast<int32_t>(offsetof(QSortStack_t3667817390, ___low_1)); }
	inline int32_t get_low_1() const { return ___low_1; }
	inline int32_t* get_address_of_low_1() { return &___low_1; }
	inline void set_low_1(int32_t value)
	{
		___low_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
