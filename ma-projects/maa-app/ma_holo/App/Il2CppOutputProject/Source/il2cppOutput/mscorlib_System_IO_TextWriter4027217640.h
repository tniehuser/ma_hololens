﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IFormatProvider
struct IFormatProvider_t2849799027;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t4027217640  : public MarshalByRefObject_t1285298191
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t1328083999* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	Il2CppObject * ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640, ___CoreNewLine_9)); }
	inline CharU5BU5D_t1328083999* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t1328083999** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t1328083999* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier(&___CoreNewLine_9, value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640, ___InternalFormatProvider_10)); }
	inline Il2CppObject * get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline Il2CppObject ** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(Il2CppObject * value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier(&___InternalFormatProvider_10, value);
	}
};

struct TextWriter_t4027217640_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4027217640 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_t2491248677 * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_t2491248677 * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_t2491248677 * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_t2491248677 * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_t2491248677 * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_t2491248677 * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_t2491248677 * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ___Null_1)); }
	inline TextWriter_t4027217640 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t4027217640 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t4027217640 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier(&___Null_1, value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_t2491248677 * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_t2491248677 ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_t2491248677 * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier(&____WriteCharDelegate_2, value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_t2491248677 * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_t2491248677 ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_t2491248677 * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier(&____WriteStringDelegate_3, value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_t2491248677 * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_t2491248677 ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_t2491248677 * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier(&____WriteCharArrayRangeDelegate_4, value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_t2491248677 * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_t2491248677 ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_t2491248677 * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier(&____WriteLineCharDelegate_5, value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_t2491248677 * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_t2491248677 ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_t2491248677 * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier(&____WriteLineStringDelegate_6, value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_t2491248677 * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_t2491248677 ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_t2491248677 * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier(&____WriteLineCharArrayRangeDelegate_7, value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_t2491248677 * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_t2491248677 ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_t2491248677 * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier(&____FlushDelegate_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
