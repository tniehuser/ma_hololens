﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Exception1927440687.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t2687879050  : public Exception_t1927440687
{
public:
	// System.String UnityEngine.UnityException::unityStackTrace
	String_t* ___unityStackTrace_17;

public:
	inline static int32_t get_offset_of_unityStackTrace_17() { return static_cast<int32_t>(offsetof(UnityException_t2687879050, ___unityStackTrace_17)); }
	inline String_t* get_unityStackTrace_17() const { return ___unityStackTrace_17; }
	inline String_t** get_address_of_unityStackTrace_17() { return &___unityStackTrace_17; }
	inline void set_unityStackTrace_17(String_t* value)
	{
		___unityStackTrace_17 = value;
		Il2CppCodeGenWriteBarrier(&___unityStackTrace_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
