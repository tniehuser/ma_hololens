﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_t838303055  : public Il2CppObject
{
public:
	// System.Int32 System.Net.SocketAddress::m_Size
	int32_t ___m_Size_0;
	// System.Byte[] System.Net.SocketAddress::m_Buffer
	ByteU5BU5D_t3397334013* ___m_Buffer_1;
	// System.Boolean System.Net.SocketAddress::m_changed
	bool ___m_changed_2;
	// System.Int32 System.Net.SocketAddress::m_hash
	int32_t ___m_hash_3;

public:
	inline static int32_t get_offset_of_m_Size_0() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_Size_0)); }
	inline int32_t get_m_Size_0() const { return ___m_Size_0; }
	inline int32_t* get_address_of_m_Size_0() { return &___m_Size_0; }
	inline void set_m_Size_0(int32_t value)
	{
		___m_Size_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_Buffer_1)); }
	inline ByteU5BU5D_t3397334013* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_t3397334013* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Buffer_1, value);
	}

	inline static int32_t get_offset_of_m_changed_2() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_changed_2)); }
	inline bool get_m_changed_2() const { return ___m_changed_2; }
	inline bool* get_address_of_m_changed_2() { return &___m_changed_2; }
	inline void set_m_changed_2(bool value)
	{
		___m_changed_2 = value;
	}

	inline static int32_t get_offset_of_m_hash_3() { return static_cast<int32_t>(offsetof(SocketAddress_t838303055, ___m_hash_3)); }
	inline int32_t get_m_hash_3() const { return ___m_hash_3; }
	inline int32_t* get_address_of_m_hash_3() { return &___m_hash_3; }
	inline void set_m_hash_3(int32_t value)
	{
		___m_hash_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
