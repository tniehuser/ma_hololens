﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList/ReadOnlyArrayList
struct  ReadOnlyArrayList_t1065935995  : public ArrayList_t4252133567
{
public:
	// System.Collections.ArrayList System.Collections.ArrayList/ReadOnlyArrayList::_list
	ArrayList_t4252133567 * ____list_5;

public:
	inline static int32_t get_offset_of__list_5() { return static_cast<int32_t>(offsetof(ReadOnlyArrayList_t1065935995, ____list_5)); }
	inline ArrayList_t4252133567 * get__list_5() const { return ____list_5; }
	inline ArrayList_t4252133567 ** get_address_of__list_5() { return &____list_5; }
	inline void set__list_5(ArrayList_t4252133567 * value)
	{
		____list_5 = value;
		Il2CppCodeGenWriteBarrier(&____list_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
