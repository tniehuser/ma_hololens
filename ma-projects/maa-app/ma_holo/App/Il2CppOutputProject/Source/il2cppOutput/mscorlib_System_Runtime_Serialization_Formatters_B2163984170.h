﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4279057407.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap
struct  BinaryObjectWithMap_t2163984170  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::objectId
	int32_t ___objectId_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::name
	String_t* ___name_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::numMembers
	int32_t ___numMembers_3;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::memberNames
	StringU5BU5D_t1642385972* ___memberNames_4;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::assemId
	int32_t ___assemId_5;

public:
	inline static int32_t get_offset_of_binaryHeaderEnum_0() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___binaryHeaderEnum_0)); }
	inline int32_t get_binaryHeaderEnum_0() const { return ___binaryHeaderEnum_0; }
	inline int32_t* get_address_of_binaryHeaderEnum_0() { return &___binaryHeaderEnum_0; }
	inline void set_binaryHeaderEnum_0(int32_t value)
	{
		___binaryHeaderEnum_0 = value;
	}

	inline static int32_t get_offset_of_objectId_1() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___objectId_1)); }
	inline int32_t get_objectId_1() const { return ___objectId_1; }
	inline int32_t* get_address_of_objectId_1() { return &___objectId_1; }
	inline void set_objectId_1(int32_t value)
	{
		___objectId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_numMembers_3() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___numMembers_3)); }
	inline int32_t get_numMembers_3() const { return ___numMembers_3; }
	inline int32_t* get_address_of_numMembers_3() { return &___numMembers_3; }
	inline void set_numMembers_3(int32_t value)
	{
		___numMembers_3 = value;
	}

	inline static int32_t get_offset_of_memberNames_4() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___memberNames_4)); }
	inline StringU5BU5D_t1642385972* get_memberNames_4() const { return ___memberNames_4; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_4() { return &___memberNames_4; }
	inline void set_memberNames_4(StringU5BU5D_t1642385972* value)
	{
		___memberNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_4, value);
	}

	inline static int32_t get_offset_of_assemId_5() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t2163984170, ___assemId_5)); }
	inline int32_t get_assemId_5() const { return ___assemId_5; }
	inline int32_t* get_address_of_assemId_5() { return &___assemId_5; }
	inline void set_assemId_5(int32_t value)
	{
		___assemId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
