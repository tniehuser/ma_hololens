﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_NetworkCredential1714133953.h"

// System.Net.SystemNetworkCredential
struct SystemNetworkCredential_t677084018;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SystemNetworkCredential
struct  SystemNetworkCredential_t677084018  : public NetworkCredential_t1714133953
{
public:

public:
};

struct SystemNetworkCredential_t677084018_StaticFields
{
public:
	// System.Net.SystemNetworkCredential System.Net.SystemNetworkCredential::defaultCredential
	SystemNetworkCredential_t677084018 * ___defaultCredential_3;

public:
	inline static int32_t get_offset_of_defaultCredential_3() { return static_cast<int32_t>(offsetof(SystemNetworkCredential_t677084018_StaticFields, ___defaultCredential_3)); }
	inline SystemNetworkCredential_t677084018 * get_defaultCredential_3() const { return ___defaultCredential_3; }
	inline SystemNetworkCredential_t677084018 ** get_address_of_defaultCredential_3() { return &___defaultCredential_3; }
	inline void set_defaultCredential_3(SystemNetworkCredential_t677084018 * value)
	{
		___defaultCredential_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCredential_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
