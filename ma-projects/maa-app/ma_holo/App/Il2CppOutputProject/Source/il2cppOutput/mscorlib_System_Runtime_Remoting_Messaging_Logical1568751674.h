﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.LogicalCallContext/Reader
struct  Reader_t1568751674 
{
public:
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.LogicalCallContext/Reader::m_ctx
	LogicalCallContext_t725724420 * ___m_ctx_0;

public:
	inline static int32_t get_offset_of_m_ctx_0() { return static_cast<int32_t>(offsetof(Reader_t1568751674, ___m_ctx_0)); }
	inline LogicalCallContext_t725724420 * get_m_ctx_0() const { return ___m_ctx_0; }
	inline LogicalCallContext_t725724420 ** get_address_of_m_ctx_0() { return &___m_ctx_0; }
	inline void set_m_ctx_0(LogicalCallContext_t725724420 * value)
	{
		___m_ctx_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_ctx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Remoting.Messaging.LogicalCallContext/Reader
struct Reader_t1568751674_marshaled_pinvoke
{
	LogicalCallContext_t725724420 * ___m_ctx_0;
};
// Native definition for COM marshalling of System.Runtime.Remoting.Messaging.LogicalCallContext/Reader
struct Reader_t1568751674_marshaled_com
{
	LogicalCallContext_t725724420 * ___m_ctx_0;
};
