﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.SimpleAsyncResult
struct SimpleAsyncResult_t2937691397;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.WebConnectionStream
struct WebConnectionStream_t1922483508;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream/<WriteRequestAsync>c__AnonStorey2
struct  U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496  : public Il2CppObject
{
public:
	// System.Net.SimpleAsyncResult System.Net.WebConnectionStream/<WriteRequestAsync>c__AnonStorey2::result
	SimpleAsyncResult_t2937691397 * ___result_0;
	// System.Int32 System.Net.WebConnectionStream/<WriteRequestAsync>c__AnonStorey2::length
	int32_t ___length_1;
	// System.Byte[] System.Net.WebConnectionStream/<WriteRequestAsync>c__AnonStorey2::bytes
	ByteU5BU5D_t3397334013* ___bytes_2;
	// System.Net.WebConnectionStream System.Net.WebConnectionStream/<WriteRequestAsync>c__AnonStorey2::$this
	WebConnectionStream_t1922483508 * ___U24this_3;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496, ___result_0)); }
	inline SimpleAsyncResult_t2937691397 * get_result_0() const { return ___result_0; }
	inline SimpleAsyncResult_t2937691397 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(SimpleAsyncResult_t2937691397 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496, ___bytes_2)); }
	inline ByteU5BU5D_t3397334013* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_t3397334013* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CWriteRequestAsyncU3Ec__AnonStorey2_t510420496, ___U24this_3)); }
	inline WebConnectionStream_t1922483508 * get_U24this_3() const { return ___U24this_3; }
	inline WebConnectionStream_t1922483508 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebConnectionStream_t1922483508 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
