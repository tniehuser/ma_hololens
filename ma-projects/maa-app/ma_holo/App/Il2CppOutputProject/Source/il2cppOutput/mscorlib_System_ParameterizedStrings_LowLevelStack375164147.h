﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ParameterizedStrings/FormatParam[]
struct FormatParamU5BU5D_t628988914;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParameterizedStrings/LowLevelStack
struct  LowLevelStack_t375164147  : public Il2CppObject
{
public:
	// System.ParameterizedStrings/FormatParam[] System.ParameterizedStrings/LowLevelStack::_arr
	FormatParamU5BU5D_t628988914* ____arr_0;
	// System.Int32 System.ParameterizedStrings/LowLevelStack::_count
	int32_t ____count_1;

public:
	inline static int32_t get_offset_of__arr_0() { return static_cast<int32_t>(offsetof(LowLevelStack_t375164147, ____arr_0)); }
	inline FormatParamU5BU5D_t628988914* get__arr_0() const { return ____arr_0; }
	inline FormatParamU5BU5D_t628988914** get_address_of__arr_0() { return &____arr_0; }
	inline void set__arr_0(FormatParamU5BU5D_t628988914* value)
	{
		____arr_0 = value;
		Il2CppCodeGenWriteBarrier(&____arr_0, value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(LowLevelStack_t375164147, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
