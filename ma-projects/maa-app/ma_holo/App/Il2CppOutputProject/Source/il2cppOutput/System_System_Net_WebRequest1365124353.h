﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat2477301187.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;
// System.Net.TimerThread/Queue
struct Queue_t1332364135;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t2110185277;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_t114276176;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t3086960480;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t1365124353  : public MarshalByRefObject_t1285298191
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_4;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_5;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t2663429579 * ___m_CachePolicy_6;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t2110185277 * ___m_CacheProtocol_7;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_t114276176 * ___m_CacheBinding_8;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_4() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_AuthenticationLevel_4)); }
	inline int32_t get_m_AuthenticationLevel_4() const { return ___m_AuthenticationLevel_4; }
	inline int32_t* get_address_of_m_AuthenticationLevel_4() { return &___m_AuthenticationLevel_4; }
	inline void set_m_AuthenticationLevel_4(int32_t value)
	{
		___m_AuthenticationLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_ImpersonationLevel_5)); }
	inline int32_t get_m_ImpersonationLevel_5() const { return ___m_ImpersonationLevel_5; }
	inline int32_t* get_address_of_m_ImpersonationLevel_5() { return &___m_ImpersonationLevel_5; }
	inline void set_m_ImpersonationLevel_5(int32_t value)
	{
		___m_ImpersonationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_6() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CachePolicy_6)); }
	inline RequestCachePolicy_t2663429579 * get_m_CachePolicy_6() const { return ___m_CachePolicy_6; }
	inline RequestCachePolicy_t2663429579 ** get_address_of_m_CachePolicy_6() { return &___m_CachePolicy_6; }
	inline void set_m_CachePolicy_6(RequestCachePolicy_t2663429579 * value)
	{
		___m_CachePolicy_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_CachePolicy_6, value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_7() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CacheProtocol_7)); }
	inline RequestCacheProtocol_t2110185277 * get_m_CacheProtocol_7() const { return ___m_CacheProtocol_7; }
	inline RequestCacheProtocol_t2110185277 ** get_address_of_m_CacheProtocol_7() { return &___m_CacheProtocol_7; }
	inline void set_m_CacheProtocol_7(RequestCacheProtocol_t2110185277 * value)
	{
		___m_CacheProtocol_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CacheProtocol_7, value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_8() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353, ___m_CacheBinding_8)); }
	inline RequestCacheBinding_t114276176 * get_m_CacheBinding_8() const { return ___m_CacheBinding_8; }
	inline RequestCacheBinding_t114276176 ** get_address_of_m_CacheBinding_8() { return &___m_CacheBinding_8; }
	inline void set_m_CacheBinding_8(RequestCacheBinding_t114276176 * value)
	{
		___m_CacheBinding_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_CacheBinding_8, value);
	}
};

struct WebRequest_t1365124353_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4252133567 * ___s_PrefixList_1;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	Il2CppObject * ___s_InternalSyncObject_2;
	// System.Net.TimerThread/Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_t1332364135 * ___s_DefaultTimerQueue_3;
	// System.Net.WebRequest/DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t3086960480 * ___webRequestCreate_9;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	Il2CppObject * ___s_DefaultWebProxy_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_11;

public:
	inline static int32_t get_offset_of_s_PrefixList_1() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_PrefixList_1)); }
	inline ArrayList_t4252133567 * get_s_PrefixList_1() const { return ___s_PrefixList_1; }
	inline ArrayList_t4252133567 ** get_address_of_s_PrefixList_1() { return &___s_PrefixList_1; }
	inline void set_s_PrefixList_1(ArrayList_t4252133567 * value)
	{
		___s_PrefixList_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_PrefixList_1, value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_2() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_InternalSyncObject_2)); }
	inline Il2CppObject * get_s_InternalSyncObject_2() const { return ___s_InternalSyncObject_2; }
	inline Il2CppObject ** get_address_of_s_InternalSyncObject_2() { return &___s_InternalSyncObject_2; }
	inline void set_s_InternalSyncObject_2(Il2CppObject * value)
	{
		___s_InternalSyncObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_InternalSyncObject_2, value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_3() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultTimerQueue_3)); }
	inline Queue_t1332364135 * get_s_DefaultTimerQueue_3() const { return ___s_DefaultTimerQueue_3; }
	inline Queue_t1332364135 ** get_address_of_s_DefaultTimerQueue_3() { return &___s_DefaultTimerQueue_3; }
	inline void set_s_DefaultTimerQueue_3(Queue_t1332364135 * value)
	{
		___s_DefaultTimerQueue_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_DefaultTimerQueue_3, value);
	}

	inline static int32_t get_offset_of_webRequestCreate_9() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___webRequestCreate_9)); }
	inline DesignerWebRequestCreate_t3086960480 * get_webRequestCreate_9() const { return ___webRequestCreate_9; }
	inline DesignerWebRequestCreate_t3086960480 ** get_address_of_webRequestCreate_9() { return &___webRequestCreate_9; }
	inline void set_webRequestCreate_9(DesignerWebRequestCreate_t3086960480 * value)
	{
		___webRequestCreate_9 = value;
		Il2CppCodeGenWriteBarrier(&___webRequestCreate_9, value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_10() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultWebProxy_10)); }
	inline Il2CppObject * get_s_DefaultWebProxy_10() const { return ___s_DefaultWebProxy_10; }
	inline Il2CppObject ** get_address_of_s_DefaultWebProxy_10() { return &___s_DefaultWebProxy_10; }
	inline void set_s_DefaultWebProxy_10(Il2CppObject * value)
	{
		___s_DefaultWebProxy_10 = value;
		Il2CppCodeGenWriteBarrier(&___s_DefaultWebProxy_10, value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_11() { return static_cast<int32_t>(offsetof(WebRequest_t1365124353_StaticFields, ___s_DefaultWebProxyInitialized_11)); }
	inline bool get_s_DefaultWebProxyInitialized_11() const { return ___s_DefaultWebProxyInitialized_11; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_11() { return &___s_DefaultWebProxyInitialized_11; }
	inline void set_s_DefaultWebProxyInitialized_11(bool value)
	{
		___s_DefaultWebProxyInitialized_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
