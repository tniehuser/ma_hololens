﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ValueTypeFixupInfo
struct  ValueTypeFixupInfo_t1676793155  : public Il2CppObject
{
public:
	// System.Int64 System.Runtime.Serialization.ValueTypeFixupInfo::m_containerID
	int64_t ___m_containerID_0;
	// System.Reflection.FieldInfo System.Runtime.Serialization.ValueTypeFixupInfo::m_parentField
	FieldInfo_t * ___m_parentField_1;
	// System.Int32[] System.Runtime.Serialization.ValueTypeFixupInfo::m_parentIndex
	Int32U5BU5D_t3030399641* ___m_parentIndex_2;

public:
	inline static int32_t get_offset_of_m_containerID_0() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1676793155, ___m_containerID_0)); }
	inline int64_t get_m_containerID_0() const { return ___m_containerID_0; }
	inline int64_t* get_address_of_m_containerID_0() { return &___m_containerID_0; }
	inline void set_m_containerID_0(int64_t value)
	{
		___m_containerID_0 = value;
	}

	inline static int32_t get_offset_of_m_parentField_1() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1676793155, ___m_parentField_1)); }
	inline FieldInfo_t * get_m_parentField_1() const { return ___m_parentField_1; }
	inline FieldInfo_t ** get_address_of_m_parentField_1() { return &___m_parentField_1; }
	inline void set_m_parentField_1(FieldInfo_t * value)
	{
		___m_parentField_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_parentField_1, value);
	}

	inline static int32_t get_offset_of_m_parentIndex_2() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1676793155, ___m_parentIndex_2)); }
	inline Int32U5BU5D_t3030399641* get_m_parentIndex_2() const { return ___m_parentIndex_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_parentIndex_2() { return &___m_parentIndex_2; }
	inline void set_m_parentIndex_2(Int32U5BU5D_t3030399641* value)
	{
		___m_parentIndex_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_parentIndex_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
