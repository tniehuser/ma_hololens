﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Uri
struct Uri_t19570940;
// System.Version
struct Version_t1755874712;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.WebConnectionGroup>
struct Dictionary_2_t862270739;
// System.Object
struct Il2CppObject;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t635820671;
// System.Threading.Timer
struct Timer_t791717973;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_t2765344313  : public Il2CppObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_t19570940 * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t693205669  ___idleSince_4;
	// System.DateTime System.Net.ServicePoint::lastDnsResolve
	DateTime_t693205669  ___lastDnsResolve_5;
	// System.Version System.Net.ServicePoint::protocolVersion
	Version_t1755874712 * ___protocolVersion_6;
	// System.Net.IPHostEntry System.Net.ServicePoint::host
	IPHostEntry_t994738509 * ___host_7;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.WebConnectionGroup> System.Net.ServicePoint::groups
	Dictionary_2_t862270739 * ___groups_9;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_10;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_11;
	// System.Object System.Net.ServicePoint::hostE
	Il2CppObject * ___hostE_12;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_13;
	// System.Net.BindIPEndPoint System.Net.ServicePoint::endPointCallback
	BindIPEndPoint_t635820671 * ___endPointCallback_14;
	// System.Boolean System.Net.ServicePoint::tcp_keepalive
	bool ___tcp_keepalive_15;
	// System.Int32 System.Net.ServicePoint::tcp_keepalive_time
	int32_t ___tcp_keepalive_time_16;
	// System.Int32 System.Net.ServicePoint::tcp_keepalive_interval
	int32_t ___tcp_keepalive_interval_17;
	// System.Threading.Timer System.Net.ServicePoint::idleTimer
	Timer_t791717973 * ___idleTimer_18;
	// System.Object System.Net.ServicePoint::m_ServerCertificateOrBytes
	Il2CppObject * ___m_ServerCertificateOrBytes_19;
	// System.Object System.Net.ServicePoint::m_ClientCertificateOrBytes
	Il2CppObject * ___m_ClientCertificateOrBytes_20;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier(&___uri_0, value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___idleSince_4)); }
	inline DateTime_t693205669  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t693205669 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t693205669  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_lastDnsResolve_5() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___lastDnsResolve_5)); }
	inline DateTime_t693205669  get_lastDnsResolve_5() const { return ___lastDnsResolve_5; }
	inline DateTime_t693205669 * get_address_of_lastDnsResolve_5() { return &___lastDnsResolve_5; }
	inline void set_lastDnsResolve_5(DateTime_t693205669  value)
	{
		___lastDnsResolve_5 = value;
	}

	inline static int32_t get_offset_of_protocolVersion_6() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___protocolVersion_6)); }
	inline Version_t1755874712 * get_protocolVersion_6() const { return ___protocolVersion_6; }
	inline Version_t1755874712 ** get_address_of_protocolVersion_6() { return &___protocolVersion_6; }
	inline void set_protocolVersion_6(Version_t1755874712 * value)
	{
		___protocolVersion_6 = value;
		Il2CppCodeGenWriteBarrier(&___protocolVersion_6, value);
	}

	inline static int32_t get_offset_of_host_7() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___host_7)); }
	inline IPHostEntry_t994738509 * get_host_7() const { return ___host_7; }
	inline IPHostEntry_t994738509 ** get_address_of_host_7() { return &___host_7; }
	inline void set_host_7(IPHostEntry_t994738509 * value)
	{
		___host_7 = value;
		Il2CppCodeGenWriteBarrier(&___host_7, value);
	}

	inline static int32_t get_offset_of_usesProxy_8() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___usesProxy_8)); }
	inline bool get_usesProxy_8() const { return ___usesProxy_8; }
	inline bool* get_address_of_usesProxy_8() { return &___usesProxy_8; }
	inline void set_usesProxy_8(bool value)
	{
		___usesProxy_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___groups_9)); }
	inline Dictionary_2_t862270739 * get_groups_9() const { return ___groups_9; }
	inline Dictionary_2_t862270739 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(Dictionary_2_t862270739 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier(&___groups_9, value);
	}

	inline static int32_t get_offset_of_sendContinue_10() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___sendContinue_10)); }
	inline bool get_sendContinue_10() const { return ___sendContinue_10; }
	inline bool* get_address_of_sendContinue_10() { return &___sendContinue_10; }
	inline void set_sendContinue_10(bool value)
	{
		___sendContinue_10 = value;
	}

	inline static int32_t get_offset_of_useConnect_11() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___useConnect_11)); }
	inline bool get_useConnect_11() const { return ___useConnect_11; }
	inline bool* get_address_of_useConnect_11() { return &___useConnect_11; }
	inline void set_useConnect_11(bool value)
	{
		___useConnect_11 = value;
	}

	inline static int32_t get_offset_of_hostE_12() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___hostE_12)); }
	inline Il2CppObject * get_hostE_12() const { return ___hostE_12; }
	inline Il2CppObject ** get_address_of_hostE_12() { return &___hostE_12; }
	inline void set_hostE_12(Il2CppObject * value)
	{
		___hostE_12 = value;
		Il2CppCodeGenWriteBarrier(&___hostE_12, value);
	}

	inline static int32_t get_offset_of_useNagle_13() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___useNagle_13)); }
	inline bool get_useNagle_13() const { return ___useNagle_13; }
	inline bool* get_address_of_useNagle_13() { return &___useNagle_13; }
	inline void set_useNagle_13(bool value)
	{
		___useNagle_13 = value;
	}

	inline static int32_t get_offset_of_endPointCallback_14() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___endPointCallback_14)); }
	inline BindIPEndPoint_t635820671 * get_endPointCallback_14() const { return ___endPointCallback_14; }
	inline BindIPEndPoint_t635820671 ** get_address_of_endPointCallback_14() { return &___endPointCallback_14; }
	inline void set_endPointCallback_14(BindIPEndPoint_t635820671 * value)
	{
		___endPointCallback_14 = value;
		Il2CppCodeGenWriteBarrier(&___endPointCallback_14, value);
	}

	inline static int32_t get_offset_of_tcp_keepalive_15() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___tcp_keepalive_15)); }
	inline bool get_tcp_keepalive_15() const { return ___tcp_keepalive_15; }
	inline bool* get_address_of_tcp_keepalive_15() { return &___tcp_keepalive_15; }
	inline void set_tcp_keepalive_15(bool value)
	{
		___tcp_keepalive_15 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_time_16() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___tcp_keepalive_time_16)); }
	inline int32_t get_tcp_keepalive_time_16() const { return ___tcp_keepalive_time_16; }
	inline int32_t* get_address_of_tcp_keepalive_time_16() { return &___tcp_keepalive_time_16; }
	inline void set_tcp_keepalive_time_16(int32_t value)
	{
		___tcp_keepalive_time_16 = value;
	}

	inline static int32_t get_offset_of_tcp_keepalive_interval_17() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___tcp_keepalive_interval_17)); }
	inline int32_t get_tcp_keepalive_interval_17() const { return ___tcp_keepalive_interval_17; }
	inline int32_t* get_address_of_tcp_keepalive_interval_17() { return &___tcp_keepalive_interval_17; }
	inline void set_tcp_keepalive_interval_17(int32_t value)
	{
		___tcp_keepalive_interval_17 = value;
	}

	inline static int32_t get_offset_of_idleTimer_18() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___idleTimer_18)); }
	inline Timer_t791717973 * get_idleTimer_18() const { return ___idleTimer_18; }
	inline Timer_t791717973 ** get_address_of_idleTimer_18() { return &___idleTimer_18; }
	inline void set_idleTimer_18(Timer_t791717973 * value)
	{
		___idleTimer_18 = value;
		Il2CppCodeGenWriteBarrier(&___idleTimer_18, value);
	}

	inline static int32_t get_offset_of_m_ServerCertificateOrBytes_19() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___m_ServerCertificateOrBytes_19)); }
	inline Il2CppObject * get_m_ServerCertificateOrBytes_19() const { return ___m_ServerCertificateOrBytes_19; }
	inline Il2CppObject ** get_address_of_m_ServerCertificateOrBytes_19() { return &___m_ServerCertificateOrBytes_19; }
	inline void set_m_ServerCertificateOrBytes_19(Il2CppObject * value)
	{
		___m_ServerCertificateOrBytes_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_ServerCertificateOrBytes_19, value);
	}

	inline static int32_t get_offset_of_m_ClientCertificateOrBytes_20() { return static_cast<int32_t>(offsetof(ServicePoint_t2765344313, ___m_ClientCertificateOrBytes_20)); }
	inline Il2CppObject * get_m_ClientCertificateOrBytes_20() const { return ___m_ClientCertificateOrBytes_20; }
	inline Il2CppObject ** get_address_of_m_ClientCertificateOrBytes_20() { return &___m_ClientCertificateOrBytes_20; }
	inline void set_m_ClientCertificateOrBytes_20(Il2CppObject * value)
	{
		___m_ClientCertificateOrBytes_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_ClientCertificateOrBytes_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
