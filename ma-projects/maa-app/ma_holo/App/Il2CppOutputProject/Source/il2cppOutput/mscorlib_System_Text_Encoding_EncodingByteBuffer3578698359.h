﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.Text.EncoderNLS
struct EncoderNLS_t1944053629;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t3883615514;
// System.Byte
struct Byte_t3683104436;
// System.Char
struct Char_t3454481338;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding/EncodingByteBuffer
struct  EncodingByteBuffer_t3578698359  : public Il2CppObject
{
public:
	// System.Byte* System.Text.Encoding/EncodingByteBuffer::bytes
	uint8_t* ___bytes_0;
	// System.Byte* System.Text.Encoding/EncodingByteBuffer::byteStart
	uint8_t* ___byteStart_1;
	// System.Byte* System.Text.Encoding/EncodingByteBuffer::byteEnd
	uint8_t* ___byteEnd_2;
	// System.Char* System.Text.Encoding/EncodingByteBuffer::chars
	Il2CppChar* ___chars_3;
	// System.Char* System.Text.Encoding/EncodingByteBuffer::charStart
	Il2CppChar* ___charStart_4;
	// System.Char* System.Text.Encoding/EncodingByteBuffer::charEnd
	Il2CppChar* ___charEnd_5;
	// System.Int32 System.Text.Encoding/EncodingByteBuffer::byteCountResult
	int32_t ___byteCountResult_6;
	// System.Text.Encoding System.Text.Encoding/EncodingByteBuffer::enc
	Encoding_t663144255 * ___enc_7;
	// System.Text.EncoderNLS System.Text.Encoding/EncodingByteBuffer::encoder
	EncoderNLS_t1944053629 * ___encoder_8;
	// System.Text.EncoderFallbackBuffer System.Text.Encoding/EncodingByteBuffer::fallbackBuffer
	EncoderFallbackBuffer_t3883615514 * ___fallbackBuffer_9;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___bytes_0)); }
	inline uint8_t* get_bytes_0() const { return ___bytes_0; }
	inline uint8_t** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(uint8_t* value)
	{
		___bytes_0 = value;
	}

	inline static int32_t get_offset_of_byteStart_1() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___byteStart_1)); }
	inline uint8_t* get_byteStart_1() const { return ___byteStart_1; }
	inline uint8_t** get_address_of_byteStart_1() { return &___byteStart_1; }
	inline void set_byteStart_1(uint8_t* value)
	{
		___byteStart_1 = value;
	}

	inline static int32_t get_offset_of_byteEnd_2() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___byteEnd_2)); }
	inline uint8_t* get_byteEnd_2() const { return ___byteEnd_2; }
	inline uint8_t** get_address_of_byteEnd_2() { return &___byteEnd_2; }
	inline void set_byteEnd_2(uint8_t* value)
	{
		___byteEnd_2 = value;
	}

	inline static int32_t get_offset_of_chars_3() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___chars_3)); }
	inline Il2CppChar* get_chars_3() const { return ___chars_3; }
	inline Il2CppChar** get_address_of_chars_3() { return &___chars_3; }
	inline void set_chars_3(Il2CppChar* value)
	{
		___chars_3 = value;
	}

	inline static int32_t get_offset_of_charStart_4() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___charStart_4)); }
	inline Il2CppChar* get_charStart_4() const { return ___charStart_4; }
	inline Il2CppChar** get_address_of_charStart_4() { return &___charStart_4; }
	inline void set_charStart_4(Il2CppChar* value)
	{
		___charStart_4 = value;
	}

	inline static int32_t get_offset_of_charEnd_5() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___charEnd_5)); }
	inline Il2CppChar* get_charEnd_5() const { return ___charEnd_5; }
	inline Il2CppChar** get_address_of_charEnd_5() { return &___charEnd_5; }
	inline void set_charEnd_5(Il2CppChar* value)
	{
		___charEnd_5 = value;
	}

	inline static int32_t get_offset_of_byteCountResult_6() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___byteCountResult_6)); }
	inline int32_t get_byteCountResult_6() const { return ___byteCountResult_6; }
	inline int32_t* get_address_of_byteCountResult_6() { return &___byteCountResult_6; }
	inline void set_byteCountResult_6(int32_t value)
	{
		___byteCountResult_6 = value;
	}

	inline static int32_t get_offset_of_enc_7() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___enc_7)); }
	inline Encoding_t663144255 * get_enc_7() const { return ___enc_7; }
	inline Encoding_t663144255 ** get_address_of_enc_7() { return &___enc_7; }
	inline void set_enc_7(Encoding_t663144255 * value)
	{
		___enc_7 = value;
		Il2CppCodeGenWriteBarrier(&___enc_7, value);
	}

	inline static int32_t get_offset_of_encoder_8() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___encoder_8)); }
	inline EncoderNLS_t1944053629 * get_encoder_8() const { return ___encoder_8; }
	inline EncoderNLS_t1944053629 ** get_address_of_encoder_8() { return &___encoder_8; }
	inline void set_encoder_8(EncoderNLS_t1944053629 * value)
	{
		___encoder_8 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_8, value);
	}

	inline static int32_t get_offset_of_fallbackBuffer_9() { return static_cast<int32_t>(offsetof(EncodingByteBuffer_t3578698359, ___fallbackBuffer_9)); }
	inline EncoderFallbackBuffer_t3883615514 * get_fallbackBuffer_9() const { return ___fallbackBuffer_9; }
	inline EncoderFallbackBuffer_t3883615514 ** get_address_of_fallbackBuffer_9() { return &___fallbackBuffer_9; }
	inline void set_fallbackBuffer_9(EncoderFallbackBuffer_t3883615514 * value)
	{
		___fallbackBuffer_9 = value;
		Il2CppCodeGenWriteBarrier(&___fallbackBuffer_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
