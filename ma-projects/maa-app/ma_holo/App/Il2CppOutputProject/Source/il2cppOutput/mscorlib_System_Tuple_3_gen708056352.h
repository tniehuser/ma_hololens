﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Threading.Tasks.TaskContinuation
struct TaskContinuation_t1422769290;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`3<System.Threading.Tasks.Task,System.Threading.Tasks.Task,System.Threading.Tasks.TaskContinuation>
struct  Tuple_3_t708056352  : public Il2CppObject
{
public:
	// T1 System.Tuple`3::m_Item1
	Task_t1843236107 * ___m_Item1_0;
	// T2 System.Tuple`3::m_Item2
	Task_t1843236107 * ___m_Item2_1;
	// T3 System.Tuple`3::m_Item3
	TaskContinuation_t1422769290 * ___m_Item3_2;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_3_t708056352, ___m_Item1_0)); }
	inline Task_t1843236107 * get_m_Item1_0() const { return ___m_Item1_0; }
	inline Task_t1843236107 ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(Task_t1843236107 * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_3_t708056352, ___m_Item2_1)); }
	inline Task_t1843236107 * get_m_Item2_1() const { return ___m_Item2_1; }
	inline Task_t1843236107 ** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(Task_t1843236107 * value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}

	inline static int32_t get_offset_of_m_Item3_2() { return static_cast<int32_t>(offsetof(Tuple_3_t708056352, ___m_Item3_2)); }
	inline TaskContinuation_t1422769290 * get_m_Item3_2() const { return ___m_Item3_2; }
	inline TaskContinuation_t1422769290 ** get_address_of_m_Item3_2() { return &___m_Item3_2; }
	inline void set_m_Item3_2(TaskContinuation_t1422769290 * value)
	{
		___m_Item3_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item3_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
