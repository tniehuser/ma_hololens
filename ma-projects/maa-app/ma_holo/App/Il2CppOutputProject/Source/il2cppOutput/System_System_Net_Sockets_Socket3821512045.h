﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"

// System.Net.Sockets.SafeSocketHandle
struct SafeSocketHandle_t1628787412;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.IOSelectorJob>>
struct Queue_1_t40964461;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.IOAsyncCallback
struct IOAsyncCallback_t2427139621;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket
struct  Socket_t3821512045  : public Il2CppObject
{
public:
	// System.Boolean System.Net.Sockets.Socket::is_closed
	bool ___is_closed_2;
	// System.Boolean System.Net.Sockets.Socket::is_listening
	bool ___is_listening_3;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_4;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::address_family
	int32_t ___address_family_5;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socket_type
	int32_t ___socket_type_6;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocol_type
	int32_t ___protocol_type_7;
	// System.Net.Sockets.SafeSocketHandle System.Net.Sockets.Socket::safe_handle
	SafeSocketHandle_t1628787412 * ___safe_handle_8;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_t4156119363 * ___seed_endpoint_9;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.IOSelectorJob>> System.Net.Sockets.Socket::readQ
	Queue_1_t40964461 * ___readQ_10;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.IOSelectorJob>> System.Net.Sockets.Socket::writeQ
	Queue_1_t40964461 * ___writeQ_11;
	// System.Boolean System.Net.Sockets.Socket::is_blocking
	bool ___is_blocking_12;
	// System.Boolean System.Net.Sockets.Socket::is_bound
	bool ___is_bound_13;
	// System.Boolean System.Net.Sockets.Socket::is_connected
	bool ___is_connected_14;
	// System.Boolean System.Net.Sockets.Socket::is_disposed
	bool ___is_disposed_15;
	// System.Boolean System.Net.Sockets.Socket::connect_in_progress
	bool ___connect_in_progress_16;

public:
	inline static int32_t get_offset_of_is_closed_2() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_closed_2)); }
	inline bool get_is_closed_2() const { return ___is_closed_2; }
	inline bool* get_address_of_is_closed_2() { return &___is_closed_2; }
	inline void set_is_closed_2(bool value)
	{
		___is_closed_2 = value;
	}

	inline static int32_t get_offset_of_is_listening_3() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_listening_3)); }
	inline bool get_is_listening_3() const { return ___is_listening_3; }
	inline bool* get_address_of_is_listening_3() { return &___is_listening_3; }
	inline void set_is_listening_3(bool value)
	{
		___is_listening_3 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_4() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___linger_timeout_4)); }
	inline int32_t get_linger_timeout_4() const { return ___linger_timeout_4; }
	inline int32_t* get_address_of_linger_timeout_4() { return &___linger_timeout_4; }
	inline void set_linger_timeout_4(int32_t value)
	{
		___linger_timeout_4 = value;
	}

	inline static int32_t get_offset_of_address_family_5() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___address_family_5)); }
	inline int32_t get_address_family_5() const { return ___address_family_5; }
	inline int32_t* get_address_of_address_family_5() { return &___address_family_5; }
	inline void set_address_family_5(int32_t value)
	{
		___address_family_5 = value;
	}

	inline static int32_t get_offset_of_socket_type_6() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___socket_type_6)); }
	inline int32_t get_socket_type_6() const { return ___socket_type_6; }
	inline int32_t* get_address_of_socket_type_6() { return &___socket_type_6; }
	inline void set_socket_type_6(int32_t value)
	{
		___socket_type_6 = value;
	}

	inline static int32_t get_offset_of_protocol_type_7() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___protocol_type_7)); }
	inline int32_t get_protocol_type_7() const { return ___protocol_type_7; }
	inline int32_t* get_address_of_protocol_type_7() { return &___protocol_type_7; }
	inline void set_protocol_type_7(int32_t value)
	{
		___protocol_type_7 = value;
	}

	inline static int32_t get_offset_of_safe_handle_8() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___safe_handle_8)); }
	inline SafeSocketHandle_t1628787412 * get_safe_handle_8() const { return ___safe_handle_8; }
	inline SafeSocketHandle_t1628787412 ** get_address_of_safe_handle_8() { return &___safe_handle_8; }
	inline void set_safe_handle_8(SafeSocketHandle_t1628787412 * value)
	{
		___safe_handle_8 = value;
		Il2CppCodeGenWriteBarrier(&___safe_handle_8, value);
	}

	inline static int32_t get_offset_of_seed_endpoint_9() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___seed_endpoint_9)); }
	inline EndPoint_t4156119363 * get_seed_endpoint_9() const { return ___seed_endpoint_9; }
	inline EndPoint_t4156119363 ** get_address_of_seed_endpoint_9() { return &___seed_endpoint_9; }
	inline void set_seed_endpoint_9(EndPoint_t4156119363 * value)
	{
		___seed_endpoint_9 = value;
		Il2CppCodeGenWriteBarrier(&___seed_endpoint_9, value);
	}

	inline static int32_t get_offset_of_readQ_10() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___readQ_10)); }
	inline Queue_1_t40964461 * get_readQ_10() const { return ___readQ_10; }
	inline Queue_1_t40964461 ** get_address_of_readQ_10() { return &___readQ_10; }
	inline void set_readQ_10(Queue_1_t40964461 * value)
	{
		___readQ_10 = value;
		Il2CppCodeGenWriteBarrier(&___readQ_10, value);
	}

	inline static int32_t get_offset_of_writeQ_11() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___writeQ_11)); }
	inline Queue_1_t40964461 * get_writeQ_11() const { return ___writeQ_11; }
	inline Queue_1_t40964461 ** get_address_of_writeQ_11() { return &___writeQ_11; }
	inline void set_writeQ_11(Queue_1_t40964461 * value)
	{
		___writeQ_11 = value;
		Il2CppCodeGenWriteBarrier(&___writeQ_11, value);
	}

	inline static int32_t get_offset_of_is_blocking_12() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_blocking_12)); }
	inline bool get_is_blocking_12() const { return ___is_blocking_12; }
	inline bool* get_address_of_is_blocking_12() { return &___is_blocking_12; }
	inline void set_is_blocking_12(bool value)
	{
		___is_blocking_12 = value;
	}

	inline static int32_t get_offset_of_is_bound_13() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_bound_13)); }
	inline bool get_is_bound_13() const { return ___is_bound_13; }
	inline bool* get_address_of_is_bound_13() { return &___is_bound_13; }
	inline void set_is_bound_13(bool value)
	{
		___is_bound_13 = value;
	}

	inline static int32_t get_offset_of_is_connected_14() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_connected_14)); }
	inline bool get_is_connected_14() const { return ___is_connected_14; }
	inline bool* get_address_of_is_connected_14() { return &___is_connected_14; }
	inline void set_is_connected_14(bool value)
	{
		___is_connected_14 = value;
	}

	inline static int32_t get_offset_of_is_disposed_15() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___is_disposed_15)); }
	inline bool get_is_disposed_15() const { return ___is_disposed_15; }
	inline bool* get_address_of_is_disposed_15() { return &___is_disposed_15; }
	inline void set_is_disposed_15(bool value)
	{
		___is_disposed_15 = value;
	}

	inline static int32_t get_offset_of_connect_in_progress_16() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___connect_in_progress_16)); }
	inline bool get_connect_in_progress_16() const { return ___connect_in_progress_16; }
	inline bool* get_address_of_connect_in_progress_16() { return &___connect_in_progress_16; }
	inline void set_connect_in_progress_16(bool value)
	{
		___connect_in_progress_16 = value;
	}
};

struct Socket_t3821512045_StaticFields
{
public:
	// System.Int32 System.Net.Sockets.Socket::ipv4_supported
	int32_t ___ipv4_supported_0;
	// System.Int32 System.Net.Sockets.Socket::ipv6_supported
	int32_t ___ipv6_supported_1;
	// System.AsyncCallback System.Net.Sockets.Socket::AcceptAsyncCallback
	AsyncCallback_t163412349 * ___AcceptAsyncCallback_17;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptCallback
	IOAsyncCallback_t2427139621 * ___BeginAcceptCallback_18;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptReceiveCallback
	IOAsyncCallback_t2427139621 * ___BeginAcceptReceiveCallback_19;
	// System.AsyncCallback System.Net.Sockets.Socket::ConnectAsyncCallback
	AsyncCallback_t163412349 * ___ConnectAsyncCallback_20;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginConnectCallback
	IOAsyncCallback_t2427139621 * ___BeginConnectCallback_21;
	// System.AsyncCallback System.Net.Sockets.Socket::DisconnectAsyncCallback
	AsyncCallback_t163412349 * ___DisconnectAsyncCallback_22;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginDisconnectCallback
	IOAsyncCallback_t2427139621 * ___BeginDisconnectCallback_23;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveAsyncCallback
	AsyncCallback_t163412349 * ___ReceiveAsyncCallback_24;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveCallback
	IOAsyncCallback_t2427139621 * ___BeginReceiveCallback_25;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveGenericCallback
	IOAsyncCallback_t2427139621 * ___BeginReceiveGenericCallback_26;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveFromAsyncCallback
	AsyncCallback_t163412349 * ___ReceiveFromAsyncCallback_27;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveFromCallback
	IOAsyncCallback_t2427139621 * ___BeginReceiveFromCallback_28;
	// System.AsyncCallback System.Net.Sockets.Socket::SendAsyncCallback
	AsyncCallback_t163412349 * ___SendAsyncCallback_29;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginSendGenericCallback
	IOAsyncCallback_t2427139621 * ___BeginSendGenericCallback_30;
	// System.AsyncCallback System.Net.Sockets.Socket::SendToAsyncCallback
	AsyncCallback_t163412349 * ___SendToAsyncCallback_31;
	// System.IOAsyncCallback System.Net.Sockets.Socket::<>f__am$cache1
	IOAsyncCallback_t2427139621 * ___U3CU3Ef__amU24cache1_32;

public:
	inline static int32_t get_offset_of_ipv4_supported_0() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ipv4_supported_0)); }
	inline int32_t get_ipv4_supported_0() const { return ___ipv4_supported_0; }
	inline int32_t* get_address_of_ipv4_supported_0() { return &___ipv4_supported_0; }
	inline void set_ipv4_supported_0(int32_t value)
	{
		___ipv4_supported_0 = value;
	}

	inline static int32_t get_offset_of_ipv6_supported_1() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ipv6_supported_1)); }
	inline int32_t get_ipv6_supported_1() const { return ___ipv6_supported_1; }
	inline int32_t* get_address_of_ipv6_supported_1() { return &___ipv6_supported_1; }
	inline void set_ipv6_supported_1(int32_t value)
	{
		___ipv6_supported_1 = value;
	}

	inline static int32_t get_offset_of_AcceptAsyncCallback_17() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___AcceptAsyncCallback_17)); }
	inline AsyncCallback_t163412349 * get_AcceptAsyncCallback_17() const { return ___AcceptAsyncCallback_17; }
	inline AsyncCallback_t163412349 ** get_address_of_AcceptAsyncCallback_17() { return &___AcceptAsyncCallback_17; }
	inline void set_AcceptAsyncCallback_17(AsyncCallback_t163412349 * value)
	{
		___AcceptAsyncCallback_17 = value;
		Il2CppCodeGenWriteBarrier(&___AcceptAsyncCallback_17, value);
	}

	inline static int32_t get_offset_of_BeginAcceptCallback_18() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginAcceptCallback_18)); }
	inline IOAsyncCallback_t2427139621 * get_BeginAcceptCallback_18() const { return ___BeginAcceptCallback_18; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginAcceptCallback_18() { return &___BeginAcceptCallback_18; }
	inline void set_BeginAcceptCallback_18(IOAsyncCallback_t2427139621 * value)
	{
		___BeginAcceptCallback_18 = value;
		Il2CppCodeGenWriteBarrier(&___BeginAcceptCallback_18, value);
	}

	inline static int32_t get_offset_of_BeginAcceptReceiveCallback_19() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginAcceptReceiveCallback_19)); }
	inline IOAsyncCallback_t2427139621 * get_BeginAcceptReceiveCallback_19() const { return ___BeginAcceptReceiveCallback_19; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginAcceptReceiveCallback_19() { return &___BeginAcceptReceiveCallback_19; }
	inline void set_BeginAcceptReceiveCallback_19(IOAsyncCallback_t2427139621 * value)
	{
		___BeginAcceptReceiveCallback_19 = value;
		Il2CppCodeGenWriteBarrier(&___BeginAcceptReceiveCallback_19, value);
	}

	inline static int32_t get_offset_of_ConnectAsyncCallback_20() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ConnectAsyncCallback_20)); }
	inline AsyncCallback_t163412349 * get_ConnectAsyncCallback_20() const { return ___ConnectAsyncCallback_20; }
	inline AsyncCallback_t163412349 ** get_address_of_ConnectAsyncCallback_20() { return &___ConnectAsyncCallback_20; }
	inline void set_ConnectAsyncCallback_20(AsyncCallback_t163412349 * value)
	{
		___ConnectAsyncCallback_20 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectAsyncCallback_20, value);
	}

	inline static int32_t get_offset_of_BeginConnectCallback_21() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginConnectCallback_21)); }
	inline IOAsyncCallback_t2427139621 * get_BeginConnectCallback_21() const { return ___BeginConnectCallback_21; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginConnectCallback_21() { return &___BeginConnectCallback_21; }
	inline void set_BeginConnectCallback_21(IOAsyncCallback_t2427139621 * value)
	{
		___BeginConnectCallback_21 = value;
		Il2CppCodeGenWriteBarrier(&___BeginConnectCallback_21, value);
	}

	inline static int32_t get_offset_of_DisconnectAsyncCallback_22() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___DisconnectAsyncCallback_22)); }
	inline AsyncCallback_t163412349 * get_DisconnectAsyncCallback_22() const { return ___DisconnectAsyncCallback_22; }
	inline AsyncCallback_t163412349 ** get_address_of_DisconnectAsyncCallback_22() { return &___DisconnectAsyncCallback_22; }
	inline void set_DisconnectAsyncCallback_22(AsyncCallback_t163412349 * value)
	{
		___DisconnectAsyncCallback_22 = value;
		Il2CppCodeGenWriteBarrier(&___DisconnectAsyncCallback_22, value);
	}

	inline static int32_t get_offset_of_BeginDisconnectCallback_23() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginDisconnectCallback_23)); }
	inline IOAsyncCallback_t2427139621 * get_BeginDisconnectCallback_23() const { return ___BeginDisconnectCallback_23; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginDisconnectCallback_23() { return &___BeginDisconnectCallback_23; }
	inline void set_BeginDisconnectCallback_23(IOAsyncCallback_t2427139621 * value)
	{
		___BeginDisconnectCallback_23 = value;
		Il2CppCodeGenWriteBarrier(&___BeginDisconnectCallback_23, value);
	}

	inline static int32_t get_offset_of_ReceiveAsyncCallback_24() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ReceiveAsyncCallback_24)); }
	inline AsyncCallback_t163412349 * get_ReceiveAsyncCallback_24() const { return ___ReceiveAsyncCallback_24; }
	inline AsyncCallback_t163412349 ** get_address_of_ReceiveAsyncCallback_24() { return &___ReceiveAsyncCallback_24; }
	inline void set_ReceiveAsyncCallback_24(AsyncCallback_t163412349 * value)
	{
		___ReceiveAsyncCallback_24 = value;
		Il2CppCodeGenWriteBarrier(&___ReceiveAsyncCallback_24, value);
	}

	inline static int32_t get_offset_of_BeginReceiveCallback_25() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginReceiveCallback_25)); }
	inline IOAsyncCallback_t2427139621 * get_BeginReceiveCallback_25() const { return ___BeginReceiveCallback_25; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginReceiveCallback_25() { return &___BeginReceiveCallback_25; }
	inline void set_BeginReceiveCallback_25(IOAsyncCallback_t2427139621 * value)
	{
		___BeginReceiveCallback_25 = value;
		Il2CppCodeGenWriteBarrier(&___BeginReceiveCallback_25, value);
	}

	inline static int32_t get_offset_of_BeginReceiveGenericCallback_26() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginReceiveGenericCallback_26)); }
	inline IOAsyncCallback_t2427139621 * get_BeginReceiveGenericCallback_26() const { return ___BeginReceiveGenericCallback_26; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginReceiveGenericCallback_26() { return &___BeginReceiveGenericCallback_26; }
	inline void set_BeginReceiveGenericCallback_26(IOAsyncCallback_t2427139621 * value)
	{
		___BeginReceiveGenericCallback_26 = value;
		Il2CppCodeGenWriteBarrier(&___BeginReceiveGenericCallback_26, value);
	}

	inline static int32_t get_offset_of_ReceiveFromAsyncCallback_27() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ReceiveFromAsyncCallback_27)); }
	inline AsyncCallback_t163412349 * get_ReceiveFromAsyncCallback_27() const { return ___ReceiveFromAsyncCallback_27; }
	inline AsyncCallback_t163412349 ** get_address_of_ReceiveFromAsyncCallback_27() { return &___ReceiveFromAsyncCallback_27; }
	inline void set_ReceiveFromAsyncCallback_27(AsyncCallback_t163412349 * value)
	{
		___ReceiveFromAsyncCallback_27 = value;
		Il2CppCodeGenWriteBarrier(&___ReceiveFromAsyncCallback_27, value);
	}

	inline static int32_t get_offset_of_BeginReceiveFromCallback_28() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginReceiveFromCallback_28)); }
	inline IOAsyncCallback_t2427139621 * get_BeginReceiveFromCallback_28() const { return ___BeginReceiveFromCallback_28; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginReceiveFromCallback_28() { return &___BeginReceiveFromCallback_28; }
	inline void set_BeginReceiveFromCallback_28(IOAsyncCallback_t2427139621 * value)
	{
		___BeginReceiveFromCallback_28 = value;
		Il2CppCodeGenWriteBarrier(&___BeginReceiveFromCallback_28, value);
	}

	inline static int32_t get_offset_of_SendAsyncCallback_29() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___SendAsyncCallback_29)); }
	inline AsyncCallback_t163412349 * get_SendAsyncCallback_29() const { return ___SendAsyncCallback_29; }
	inline AsyncCallback_t163412349 ** get_address_of_SendAsyncCallback_29() { return &___SendAsyncCallback_29; }
	inline void set_SendAsyncCallback_29(AsyncCallback_t163412349 * value)
	{
		___SendAsyncCallback_29 = value;
		Il2CppCodeGenWriteBarrier(&___SendAsyncCallback_29, value);
	}

	inline static int32_t get_offset_of_BeginSendGenericCallback_30() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___BeginSendGenericCallback_30)); }
	inline IOAsyncCallback_t2427139621 * get_BeginSendGenericCallback_30() const { return ___BeginSendGenericCallback_30; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_BeginSendGenericCallback_30() { return &___BeginSendGenericCallback_30; }
	inline void set_BeginSendGenericCallback_30(IOAsyncCallback_t2427139621 * value)
	{
		___BeginSendGenericCallback_30 = value;
		Il2CppCodeGenWriteBarrier(&___BeginSendGenericCallback_30, value);
	}

	inline static int32_t get_offset_of_SendToAsyncCallback_31() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___SendToAsyncCallback_31)); }
	inline AsyncCallback_t163412349 * get_SendToAsyncCallback_31() const { return ___SendToAsyncCallback_31; }
	inline AsyncCallback_t163412349 ** get_address_of_SendToAsyncCallback_31() { return &___SendToAsyncCallback_31; }
	inline void set_SendToAsyncCallback_31(AsyncCallback_t163412349 * value)
	{
		___SendToAsyncCallback_31 = value;
		Il2CppCodeGenWriteBarrier(&___SendToAsyncCallback_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_32() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___U3CU3Ef__amU24cache1_32)); }
	inline IOAsyncCallback_t2427139621 * get_U3CU3Ef__amU24cache1_32() const { return ___U3CU3Ef__amU24cache1_32; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_U3CU3Ef__amU24cache1_32() { return &___U3CU3Ef__amU24cache1_32; }
	inline void set_U3CU3Ef__amU24cache1_32(IOAsyncCallback_t2427139621 * value)
	{
		___U3CU3Ef__amU24cache1_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
