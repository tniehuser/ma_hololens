﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaExternal3943748629.h"

// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaRedefine
struct  XmlSchemaRedefine_t3478619248  : public XmlSchemaExternal_t3943748629
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaRedefine::items
	XmlSchemaObjectCollection_t395083109 * ___items_12;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::attributeGroups
	XmlSchemaObjectTable_t3364835593 * ___attributeGroups_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::types
	XmlSchemaObjectTable_t3364835593 * ___types_14;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::groups
	XmlSchemaObjectTable_t3364835593 * ___groups_15;

public:
	inline static int32_t get_offset_of_items_12() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___items_12)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_12() const { return ___items_12; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_12() { return &___items_12; }
	inline void set_items_12(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_12 = value;
		Il2CppCodeGenWriteBarrier(&___items_12, value);
	}

	inline static int32_t get_offset_of_attributeGroups_13() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___attributeGroups_13)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeGroups_13() const { return ___attributeGroups_13; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeGroups_13() { return &___attributeGroups_13; }
	inline void set_attributeGroups_13(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeGroups_13 = value;
		Il2CppCodeGenWriteBarrier(&___attributeGroups_13, value);
	}

	inline static int32_t get_offset_of_types_14() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___types_14)); }
	inline XmlSchemaObjectTable_t3364835593 * get_types_14() const { return ___types_14; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_types_14() { return &___types_14; }
	inline void set_types_14(XmlSchemaObjectTable_t3364835593 * value)
	{
		___types_14 = value;
		Il2CppCodeGenWriteBarrier(&___types_14, value);
	}

	inline static int32_t get_offset_of_groups_15() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___groups_15)); }
	inline XmlSchemaObjectTable_t3364835593 * get_groups_15() const { return ___groups_15; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_groups_15() { return &___groups_15; }
	inline void set_groups_15(XmlSchemaObjectTable_t3364835593 * value)
	{
		___groups_15 = value;
		Il2CppCodeGenWriteBarrier(&___groups_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
