﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureNotFoundException
struct  CultureNotFoundException_t1206712152  : public ArgumentException_t3259014390
{
public:
	// System.String System.Globalization.CultureNotFoundException::m_invalidCultureName
	String_t* ___m_invalidCultureName_17;
	// System.Nullable`1<System.Int32> System.Globalization.CultureNotFoundException::m_invalidCultureId
	Nullable_1_t334943763  ___m_invalidCultureId_18;

public:
	inline static int32_t get_offset_of_m_invalidCultureName_17() { return static_cast<int32_t>(offsetof(CultureNotFoundException_t1206712152, ___m_invalidCultureName_17)); }
	inline String_t* get_m_invalidCultureName_17() const { return ___m_invalidCultureName_17; }
	inline String_t** get_address_of_m_invalidCultureName_17() { return &___m_invalidCultureName_17; }
	inline void set_m_invalidCultureName_17(String_t* value)
	{
		___m_invalidCultureName_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_invalidCultureName_17, value);
	}

	inline static int32_t get_offset_of_m_invalidCultureId_18() { return static_cast<int32_t>(offsetof(CultureNotFoundException_t1206712152, ___m_invalidCultureId_18)); }
	inline Nullable_1_t334943763  get_m_invalidCultureId_18() const { return ___m_invalidCultureId_18; }
	inline Nullable_1_t334943763 * get_address_of_m_invalidCultureId_18() { return &___m_invalidCultureId_18; }
	inline void set_m_invalidCultureId_18(Nullable_1_t334943763  value)
	{
		___m_invalidCultureId_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
