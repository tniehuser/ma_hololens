﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_Aes2354947465.h"

// System.Security.Cryptography.RijndaelManaged
struct RijndaelManaged_t1034060848;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesManaged
struct  AesManaged_t3721278648  : public Aes_t2354947465
{
public:
	// System.Security.Cryptography.RijndaelManaged System.Security.Cryptography.AesManaged::m_rijndael
	RijndaelManaged_t1034060848 * ___m_rijndael_11;

public:
	inline static int32_t get_offset_of_m_rijndael_11() { return static_cast<int32_t>(offsetof(AesManaged_t3721278648, ___m_rijndael_11)); }
	inline RijndaelManaged_t1034060848 * get_m_rijndael_11() const { return ___m_rijndael_11; }
	inline RijndaelManaged_t1034060848 ** get_address_of_m_rijndael_11() { return &___m_rijndael_11; }
	inline void set_m_rijndael_11(RijndaelManaged_t1034060848 * value)
	{
		___m_rijndael_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_rijndael_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
