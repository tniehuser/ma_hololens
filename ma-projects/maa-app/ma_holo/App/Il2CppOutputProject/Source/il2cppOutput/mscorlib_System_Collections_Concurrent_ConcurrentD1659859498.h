﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentDictionary`2/DictionaryEnumerator<System.Object,System.Object>
struct  DictionaryEnumerator_t1659859498  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Concurrent.ConcurrentDictionary`2/DictionaryEnumerator::m_enumerator
	Il2CppObject* ___m_enumerator_0;

public:
	inline static int32_t get_offset_of_m_enumerator_0() { return static_cast<int32_t>(offsetof(DictionaryEnumerator_t1659859498, ___m_enumerator_0)); }
	inline Il2CppObject* get_m_enumerator_0() const { return ___m_enumerator_0; }
	inline Il2CppObject** get_address_of_m_enumerator_0() { return &___m_enumerator_0; }
	inline void set_m_enumerator_0(Il2CppObject* value)
	{
		___m_enumerator_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_enumerator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
