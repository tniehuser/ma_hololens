﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAParameters
struct  RSAParameters_t1462703416 
{
public:
	// System.Byte[] System.Security.Cryptography.RSAParameters::Exponent
	ByteU5BU5D_t3397334013* ___Exponent_0;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Modulus
	ByteU5BU5D_t3397334013* ___Modulus_1;
	// System.Byte[] System.Security.Cryptography.RSAParameters::P
	ByteU5BU5D_t3397334013* ___P_2;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Q
	ByteU5BU5D_t3397334013* ___Q_3;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DP
	ByteU5BU5D_t3397334013* ___DP_4;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DQ
	ByteU5BU5D_t3397334013* ___DQ_5;
	// System.Byte[] System.Security.Cryptography.RSAParameters::InverseQ
	ByteU5BU5D_t3397334013* ___InverseQ_6;
	// System.Byte[] System.Security.Cryptography.RSAParameters::D
	ByteU5BU5D_t3397334013* ___D_7;

public:
	inline static int32_t get_offset_of_Exponent_0() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Exponent_0)); }
	inline ByteU5BU5D_t3397334013* get_Exponent_0() const { return ___Exponent_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Exponent_0() { return &___Exponent_0; }
	inline void set_Exponent_0(ByteU5BU5D_t3397334013* value)
	{
		___Exponent_0 = value;
		Il2CppCodeGenWriteBarrier(&___Exponent_0, value);
	}

	inline static int32_t get_offset_of_Modulus_1() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Modulus_1)); }
	inline ByteU5BU5D_t3397334013* get_Modulus_1() const { return ___Modulus_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_Modulus_1() { return &___Modulus_1; }
	inline void set_Modulus_1(ByteU5BU5D_t3397334013* value)
	{
		___Modulus_1 = value;
		Il2CppCodeGenWriteBarrier(&___Modulus_1, value);
	}

	inline static int32_t get_offset_of_P_2() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___P_2)); }
	inline ByteU5BU5D_t3397334013* get_P_2() const { return ___P_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_P_2() { return &___P_2; }
	inline void set_P_2(ByteU5BU5D_t3397334013* value)
	{
		___P_2 = value;
		Il2CppCodeGenWriteBarrier(&___P_2, value);
	}

	inline static int32_t get_offset_of_Q_3() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Q_3)); }
	inline ByteU5BU5D_t3397334013* get_Q_3() const { return ___Q_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_Q_3() { return &___Q_3; }
	inline void set_Q_3(ByteU5BU5D_t3397334013* value)
	{
		___Q_3 = value;
		Il2CppCodeGenWriteBarrier(&___Q_3, value);
	}

	inline static int32_t get_offset_of_DP_4() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DP_4)); }
	inline ByteU5BU5D_t3397334013* get_DP_4() const { return ___DP_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_DP_4() { return &___DP_4; }
	inline void set_DP_4(ByteU5BU5D_t3397334013* value)
	{
		___DP_4 = value;
		Il2CppCodeGenWriteBarrier(&___DP_4, value);
	}

	inline static int32_t get_offset_of_DQ_5() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DQ_5)); }
	inline ByteU5BU5D_t3397334013* get_DQ_5() const { return ___DQ_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_DQ_5() { return &___DQ_5; }
	inline void set_DQ_5(ByteU5BU5D_t3397334013* value)
	{
		___DQ_5 = value;
		Il2CppCodeGenWriteBarrier(&___DQ_5, value);
	}

	inline static int32_t get_offset_of_InverseQ_6() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___InverseQ_6)); }
	inline ByteU5BU5D_t3397334013* get_InverseQ_6() const { return ___InverseQ_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_InverseQ_6() { return &___InverseQ_6; }
	inline void set_InverseQ_6(ByteU5BU5D_t3397334013* value)
	{
		___InverseQ_6 = value;
		Il2CppCodeGenWriteBarrier(&___InverseQ_6, value);
	}

	inline static int32_t get_offset_of_D_7() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___D_7)); }
	inline ByteU5BU5D_t3397334013* get_D_7() const { return ___D_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_D_7() { return &___D_7; }
	inline void set_D_7(ByteU5BU5D_t3397334013* value)
	{
		___D_7 = value;
		Il2CppCodeGenWriteBarrier(&___D_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_pinvoke
{
	uint8_t* ___Exponent_0;
	uint8_t* ___Modulus_1;
	uint8_t* ___P_2;
	uint8_t* ___Q_3;
	uint8_t* ___DP_4;
	uint8_t* ___DQ_5;
	uint8_t* ___InverseQ_6;
	uint8_t* ___D_7;
};
// Native definition for COM marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_com
{
	uint8_t* ___Exponent_0;
	uint8_t* ___Modulus_1;
	uint8_t* ___P_2;
	uint8_t* ___Q_3;
	uint8_t* ___DP_4;
	uint8_t* ___DQ_5;
	uint8_t* ___InverseQ_6;
	uint8_t* ___D_7;
};
