﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMA1590223312.h"
#include "mscorlib_Interop_mincore_SYSTEMTIME2580015906.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMAT757325618.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMAT748200035.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION
struct  TIME_DYNAMIC_ZONE_INFORMATION_t2883369749 
{
public:
	// System.Int32 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::Bias
	int32_t ___Bias_0;
	// Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION/<StandardName>__FixedBuffer0 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::StandardName
	U3CStandardNameU3E__FixedBuffer0_t1590223312  ___StandardName_1;
	// Interop/mincore/SYSTEMTIME Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::StandardDate
	SYSTEMTIME_t2580015906  ___StandardDate_2;
	// System.Int32 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::StandardBias
	int32_t ___StandardBias_3;
	// Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION/<DaylightName>__FixedBuffer1 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::DaylightName
	U3CDaylightNameU3E__FixedBuffer1_t757325618  ___DaylightName_4;
	// Interop/mincore/SYSTEMTIME Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::DaylightDate
	SYSTEMTIME_t2580015906  ___DaylightDate_5;
	// System.Int32 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::DaylightBias
	int32_t ___DaylightBias_6;
	// Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION/<TimeZoneKeyName>__FixedBuffer2 Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::TimeZoneKeyName
	U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035  ___TimeZoneKeyName_7;
	// System.Byte Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION::DynamicDaylightTimeDisabled
	uint8_t ___DynamicDaylightTimeDisabled_8;

public:
	inline static int32_t get_offset_of_Bias_0() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___Bias_0)); }
	inline int32_t get_Bias_0() const { return ___Bias_0; }
	inline int32_t* get_address_of_Bias_0() { return &___Bias_0; }
	inline void set_Bias_0(int32_t value)
	{
		___Bias_0 = value;
	}

	inline static int32_t get_offset_of_StandardName_1() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___StandardName_1)); }
	inline U3CStandardNameU3E__FixedBuffer0_t1590223312  get_StandardName_1() const { return ___StandardName_1; }
	inline U3CStandardNameU3E__FixedBuffer0_t1590223312 * get_address_of_StandardName_1() { return &___StandardName_1; }
	inline void set_StandardName_1(U3CStandardNameU3E__FixedBuffer0_t1590223312  value)
	{
		___StandardName_1 = value;
	}

	inline static int32_t get_offset_of_StandardDate_2() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___StandardDate_2)); }
	inline SYSTEMTIME_t2580015906  get_StandardDate_2() const { return ___StandardDate_2; }
	inline SYSTEMTIME_t2580015906 * get_address_of_StandardDate_2() { return &___StandardDate_2; }
	inline void set_StandardDate_2(SYSTEMTIME_t2580015906  value)
	{
		___StandardDate_2 = value;
	}

	inline static int32_t get_offset_of_StandardBias_3() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___StandardBias_3)); }
	inline int32_t get_StandardBias_3() const { return ___StandardBias_3; }
	inline int32_t* get_address_of_StandardBias_3() { return &___StandardBias_3; }
	inline void set_StandardBias_3(int32_t value)
	{
		___StandardBias_3 = value;
	}

	inline static int32_t get_offset_of_DaylightName_4() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___DaylightName_4)); }
	inline U3CDaylightNameU3E__FixedBuffer1_t757325618  get_DaylightName_4() const { return ___DaylightName_4; }
	inline U3CDaylightNameU3E__FixedBuffer1_t757325618 * get_address_of_DaylightName_4() { return &___DaylightName_4; }
	inline void set_DaylightName_4(U3CDaylightNameU3E__FixedBuffer1_t757325618  value)
	{
		___DaylightName_4 = value;
	}

	inline static int32_t get_offset_of_DaylightDate_5() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___DaylightDate_5)); }
	inline SYSTEMTIME_t2580015906  get_DaylightDate_5() const { return ___DaylightDate_5; }
	inline SYSTEMTIME_t2580015906 * get_address_of_DaylightDate_5() { return &___DaylightDate_5; }
	inline void set_DaylightDate_5(SYSTEMTIME_t2580015906  value)
	{
		___DaylightDate_5 = value;
	}

	inline static int32_t get_offset_of_DaylightBias_6() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___DaylightBias_6)); }
	inline int32_t get_DaylightBias_6() const { return ___DaylightBias_6; }
	inline int32_t* get_address_of_DaylightBias_6() { return &___DaylightBias_6; }
	inline void set_DaylightBias_6(int32_t value)
	{
		___DaylightBias_6 = value;
	}

	inline static int32_t get_offset_of_TimeZoneKeyName_7() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___TimeZoneKeyName_7)); }
	inline U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035  get_TimeZoneKeyName_7() const { return ___TimeZoneKeyName_7; }
	inline U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035 * get_address_of_TimeZoneKeyName_7() { return &___TimeZoneKeyName_7; }
	inline void set_TimeZoneKeyName_7(U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035  value)
	{
		___TimeZoneKeyName_7 = value;
	}

	inline static int32_t get_offset_of_DynamicDaylightTimeDisabled_8() { return static_cast<int32_t>(offsetof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749, ___DynamicDaylightTimeDisabled_8)); }
	inline uint8_t get_DynamicDaylightTimeDisabled_8() const { return ___DynamicDaylightTimeDisabled_8; }
	inline uint8_t* get_address_of_DynamicDaylightTimeDisabled_8() { return &___DynamicDaylightTimeDisabled_8; }
	inline void set_DynamicDaylightTimeDisabled_8(uint8_t value)
	{
		___DynamicDaylightTimeDisabled_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
