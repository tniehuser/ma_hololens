﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MonoGenericClass976877918.h"
#include "mscorlib_System_Reflection_MonoGenericMethod1068099169.h"
#include "mscorlib_System_Reflection_MonoGenericCMethod2923423538.h"
#include "mscorlib_System_Reflection_MonoMethodInfo3646562144.h"
#include "mscorlib_System_Reflection_RuntimeMethodInfo3887273209.h"
#include "mscorlib_System_Reflection_MonoMethod116053496.h"
#include "mscorlib_System_Reflection_RuntimeConstructorInfo311714390.h"
#include "mscorlib_System_Reflection_MonoCMethod611352247.h"
#include "mscorlib_System_Reflection_RuntimeModule2087152846.h"
#include "mscorlib_System_Reflection_MonoModule1684978549.h"
#include "mscorlib_System_Reflection_RuntimeParameterInfo477802511.h"
#include "mscorlib_System_Reflection_MonoParameterInfo2391409224.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo486106184.h"
#include "mscorlib_System_Reflection_PInfo957350482.h"
#include "mscorlib_System_Reflection_RuntimePropertyInfo3497259377.h"
#include "mscorlib_System_Reflection_MonoProperty2242413552.h"
#include "mscorlib_System_Reflection_MonoProperty_GetterAdap1423755509.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds3142660980.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_ReflectionTypeLoadExcep4074666396.h"
#include "mscorlib_System_Reflection_StrongNameKeyPair4090869089.h"
#include "mscorlib_System_Resources_Win32ResourceType130810739.h"
#include "mscorlib_System_Resources_NameOrId2460371499.h"
#include "mscorlib_System_Resources_Win32Resource2185668907.h"
#include "mscorlib_System_Resources_Win32VersionResource548350325.h"
#include "mscorlib_System_Runtime_CompilerServices_Ephemeron1875076633.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArguments640021366.h"
#include "mscorlib_System_Runtime_InteropServices_WindowsRunt251036611.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1970708122.h"
#include "mscorlib_System_Runtime_InteropServices_IErrorInfo3966376335.h"
#include "mscorlib_System_Runtime_InteropServices_ManagedErro914761495.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal785896760.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalAsA2900773360.h"
#include "mscorlib_System_Runtime_InteropServices_SafeBuffer699764933.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Activa1532663650.h"
#include "mscorlib_System_Runtime_Remoting_Activation_AppDoma834876328.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Constr2284932402.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Contex1784331636.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteA213750447.h"
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAtt1544437301.h"
#include "mscorlib_System_Runtime_Remoting_ChannelInfo709892715.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelS2007814595.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppD816071813.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp2471623380.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp2368859578.h"
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProv2645445792.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Context502196753.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicP2282532998.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicP1839195831.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextC3978189709.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAt197102333.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCon2302426108.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCont754146990.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (MonoGenericClass_t976877918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1100[6] = 
{
	MonoGenericClass_t976877918::get_offset_of_generic_type_8(),
	MonoGenericClass_t976877918::get_offset_of_type_arguments_9(),
	MonoGenericClass_t976877918::get_offset_of_initialized_10(),
	MonoGenericClass_t976877918::get_offset_of_fields_11(),
	MonoGenericClass_t976877918::get_offset_of_ctors_12(),
	MonoGenericClass_t976877918::get_offset_of_methods_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (MonoGenericMethod_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (MonoGenericCMethod_t2923423538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (MonoMethodInfo_t3646562144)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1103[5] = 
{
	MonoMethodInfo_t3646562144::get_offset_of_parent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoMethodInfo_t3646562144::get_offset_of_ret_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoMethodInfo_t3646562144::get_offset_of_attrs_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoMethodInfo_t3646562144::get_offset_of_iattrs_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoMethodInfo_t3646562144::get_offset_of_callconv_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (RuntimeMethodInfo_t3887273209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (MonoMethod_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1105[3] = 
{
	MonoMethod_t::get_offset_of_mhandle_0(),
	MonoMethod_t::get_offset_of_name_1(),
	MonoMethod_t::get_offset_of_reftype_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (RuntimeConstructorInfo_t311714390), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (MonoCMethod_t611352247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1107[3] = 
{
	MonoCMethod_t611352247::get_offset_of_mhandle_2(),
	MonoCMethod_t611352247::get_offset_of_name_3(),
	MonoCMethod_t611352247::get_offset_of_reftype_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (RuntimeModule_t2087152846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (MonoModule_t1684978549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (RuntimeParameterInfo_t477802511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (MonoParameterInfo_t2391409224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (MonoPropertyInfo_t486106184)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1112[6] = 
{
	MonoPropertyInfo_t486106184::get_offset_of_parent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoPropertyInfo_t486106184::get_offset_of_declaring_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoPropertyInfo_t486106184::get_offset_of_name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoPropertyInfo_t486106184::get_offset_of_get_method_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoPropertyInfo_t486106184::get_offset_of_set_method_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoPropertyInfo_t486106184::get_offset_of_attrs_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (PInfo_t957350482)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1113[7] = 
{
	PInfo_t957350482::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (RuntimePropertyInfo_t3497259377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (MonoProperty_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1115[5] = 
{
	MonoProperty_t::get_offset_of_klass_0(),
	MonoProperty_t::get_offset_of_prop_1(),
	MonoProperty_t::get_offset_of_info_2(),
	MonoProperty_t::get_offset_of_cached_3(),
	MonoProperty_t::get_offset_of_cached_getter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (GetterAdapter_t1423755509), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (ParameterInfo_t2249040075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[7] = 
{
	ParameterInfo_t2249040075::get_offset_of_ClassImpl_0(),
	ParameterInfo_t2249040075::get_offset_of_DefaultValueImpl_1(),
	ParameterInfo_t2249040075::get_offset_of_MemberImpl_2(),
	ParameterInfo_t2249040075::get_offset_of_NameImpl_3(),
	ParameterInfo_t2249040075::get_offset_of_PositionImpl_4(),
	ParameterInfo_t2249040075::get_offset_of_AttrsImpl_5(),
	ParameterInfo_t2249040075::get_offset_of_marshalAs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (PortableExecutableKinds_t3142660980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1120[7] = 
{
	PortableExecutableKinds_t3142660980::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (PropertyInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (ReflectionTypeLoadException_t4074666396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1122[2] = 
{
	ReflectionTypeLoadException_t4074666396::get_offset_of_loaderExceptions_16(),
	ReflectionTypeLoadException_t4074666396::get_offset_of_types_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (StrongNameKeyPair_t4090869089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[5] = 
{
	StrongNameKeyPair_t4090869089::get_offset_of__publicKey_0(),
	StrongNameKeyPair_t4090869089::get_offset_of__keyPairContainer_1(),
	StrongNameKeyPair_t4090869089::get_offset_of__keyPairExported_2(),
	StrongNameKeyPair_t4090869089::get_offset_of__keyPairArray_3(),
	StrongNameKeyPair_t4090869089::get_offset_of__rsa_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (Win32ResourceType_t130810739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1124[21] = 
{
	Win32ResourceType_t130810739::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (NameOrId_t2460371499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[2] = 
{
	NameOrId_t2460371499::get_offset_of_name_0(),
	NameOrId_t2460371499::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (Win32Resource_t2185668907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1126[2] = 
{
	Win32Resource_t2185668907::get_offset_of_type_0(),
	Win32Resource_t2185668907::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (Win32VersionResource_t548350325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (Ephemeron_t1875076633)+ sizeof (Il2CppObject), sizeof(Ephemeron_t1875076633_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1128[2] = 
{
	Ephemeron_t1875076633::get_offset_of_key_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ephemeron_t1875076633::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1129[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (RuntimeHelpers_t266230107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (ActivationArguments_t640021366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (UnsafeNativeMethods_t251036611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (GCHandle_t3409268066)+ sizeof (Il2CppObject), sizeof(GCHandle_t3409268066 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1133[1] = 
{
	GCHandle_t3409268066::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (GCHandleType_t1970708122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1134[5] = 
{
	GCHandleType_t1970708122::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { 0, sizeof(IErrorInfo_t3966376335*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (ManagedErrorInfo_t914761495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1136[1] = 
{
	ManagedErrorInfo_t914761495::get_offset_of_m_Exception_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (Marshal_t785896760), -1, sizeof(Marshal_t785896760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1137[4] = 
{
	Marshal_t785896760_StaticFields::get_offset_of_SystemMaxDBCSCharSize_0(),
	Marshal_t785896760_StaticFields::get_offset_of_SystemDefaultCharSize_1(),
	Marshal_t785896760_StaticFields::get_offset_of_SetErrorInfoNotAvailable_2(),
	Marshal_t785896760_StaticFields::get_offset_of_GetErrorInfoNotAvailable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (MarshalAsAttribute_t2900773360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1138[10] = 
{
	MarshalAsAttribute_t2900773360::get_offset_of_MarshalCookie_0(),
	MarshalAsAttribute_t2900773360::get_offset_of_MarshalType_1(),
	MarshalAsAttribute_t2900773360::get_offset_of_MarshalTypeRef_2(),
	MarshalAsAttribute_t2900773360::get_offset_of_SafeArrayUserDefinedSubType_3(),
	MarshalAsAttribute_t2900773360::get_offset_of_utype_4(),
	MarshalAsAttribute_t2900773360::get_offset_of_ArraySubType_5(),
	MarshalAsAttribute_t2900773360::get_offset_of_SafeArraySubType_6(),
	MarshalAsAttribute_t2900773360::get_offset_of_SizeConst_7(),
	MarshalAsAttribute_t2900773360::get_offset_of_IidParameterIndex_8(),
	MarshalAsAttribute_t2900773360::get_offset_of_SizeParamIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (SafeBuffer_t699764933), sizeof(void*), 0, 0 };
extern const int32_t g_FieldOffsetTable1139[1] = 
{
	SafeBuffer_t699764933::get_offset_of_inited_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (ActivationServices_t1532663650), -1, sizeof(ActivationServices_t1532663650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1168[1] = 
{
	ActivationServices_t1532663650_StaticFields::get_offset_of__constructionActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (AppDomainLevelActivator_t834876328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1169[2] = 
{
	AppDomainLevelActivator_t834876328::get_offset_of__activationUrl_0(),
	AppDomainLevelActivator_t834876328::get_offset_of__next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (ConstructionLevelActivator_t2284932402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (ContextLevelActivator_t1784331636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[1] = 
{
	ContextLevelActivator_t1784331636::get_offset_of_m_NextActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (RemoteActivator_t213750447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (UrlAttribute_t1544437301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1175[1] = 
{
	UrlAttribute_t1544437301::get_offset_of_url_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (ChannelInfo_t709892715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1176[1] = 
{
	ChannelInfo_t709892715::get_offset_of_channelData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (ChannelServices_t2007814595), -1, sizeof(ChannelServices_t2007814595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1177[5] = 
{
	ChannelServices_t2007814595_StaticFields::get_offset_of_registeredChannels_0(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_delayedClientChannels_1(),
	ChannelServices_t2007814595_StaticFields::get_offset_of__crossContextSink_2(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_CrossContextUrl_3(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_oldStartModeTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (CrossAppDomainData_t816071813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1178[3] = 
{
	CrossAppDomainData_t816071813::get_offset_of__ContextID_0(),
	CrossAppDomainData_t816071813::get_offset_of__DomainID_1(),
	CrossAppDomainData_t816071813::get_offset_of__processGuid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (CrossAppDomainChannel_t2471623380), -1, sizeof(CrossAppDomainChannel_t2471623380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1179[1] = 
{
	CrossAppDomainChannel_t2471623380_StaticFields::get_offset_of_s_lock_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (CrossAppDomainSink_t2368859578), -1, sizeof(CrossAppDomainSink_t2368859578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1180[3] = 
{
	CrossAppDomainSink_t2368859578_StaticFields::get_offset_of_s_sinks_0(),
	CrossAppDomainSink_t2368859578_StaticFields::get_offset_of_processMessageMethod_1(),
	CrossAppDomainSink_t2368859578::get_offset_of__domainID_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (SinkProviderData_t2645445792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1188[3] = 
{
	SinkProviderData_t2645445792::get_offset_of_sinkName_0(),
	SinkProviderData_t2645445792::get_offset_of_children_1(),
	SinkProviderData_t2645445792::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (Context_t502196753), -1, sizeof(Context_t502196753_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1189[15] = 
{
	Context_t502196753::get_offset_of_domain_id_0(),
	Context_t502196753::get_offset_of_context_id_1(),
	Context_t502196753::get_offset_of_static_data_2(),
	Context_t502196753::get_offset_of_data_3(),
	Context_t502196753_StaticFields::get_offset_of_local_slots_4(),
	Context_t502196753_StaticFields::get_offset_of_default_server_context_sink_5(),
	Context_t502196753::get_offset_of_server_context_sink_chain_6(),
	Context_t502196753::get_offset_of_client_context_sink_chain_7(),
	Context_t502196753::get_offset_of_context_properties_8(),
	Context_t502196753_StaticFields::get_offset_of_global_count_9(),
	Context_t502196753::get_offset_of__localDataStore_10(),
	Context_t502196753_StaticFields::get_offset_of__localDataStoreMgr_11(),
	Context_t502196753_StaticFields::get_offset_of_global_dynamic_properties_12(),
	Context_t502196753::get_offset_of_context_dynamic_properties_13(),
	Context_t502196753::get_offset_of_callback_object_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (DynamicPropertyCollection_t2282532998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[1] = 
{
	DynamicPropertyCollection_t2282532998::get_offset_of__properties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (DynamicPropertyReg_t1839195831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1191[2] = 
{
	DynamicPropertyReg_t1839195831::get_offset_of_Property_0(),
	DynamicPropertyReg_t1839195831::get_offset_of_Sink_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (ContextCallbackObject_t3978189709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (ContextAttribute_t197102333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1193[1] = 
{
	ContextAttribute_t197102333::get_offset_of_AttributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (CrossContextChannel_t2302426108), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (CrossContextDelegate_t754146990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
