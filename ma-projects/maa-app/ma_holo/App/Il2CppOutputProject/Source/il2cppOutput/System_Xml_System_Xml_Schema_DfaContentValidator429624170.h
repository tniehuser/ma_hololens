﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_t1753655453;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DfaContentValidator
struct  DfaContentValidator_t429624170  : public ContentValidator_t2510151843
{
public:
	// System.Int32[][] System.Xml.Schema.DfaContentValidator::transitionTable
	Int32U5BU5DU5BU5D_t3750818532* ___transitionTable_7;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.DfaContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_8;

public:
	inline static int32_t get_offset_of_transitionTable_7() { return static_cast<int32_t>(offsetof(DfaContentValidator_t429624170, ___transitionTable_7)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get_transitionTable_7() const { return ___transitionTable_7; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of_transitionTable_7() { return &___transitionTable_7; }
	inline void set_transitionTable_7(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		___transitionTable_7 = value;
		Il2CppCodeGenWriteBarrier(&___transitionTable_7, value);
	}

	inline static int32_t get_offset_of_symbols_8() { return static_cast<int32_t>(offsetof(DfaContentValidator_t429624170, ___symbols_8)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_8() const { return ___symbols_8; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_8() { return &___symbols_8; }
	inline void set_symbols_8(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_8 = value;
		Il2CppCodeGenWriteBarrier(&___symbols_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
