﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.AggregateException[]
struct AggregateExceptionU5BU5D_t713784977;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.AggregateException>
struct  List_1_t4084901404  : public Il2CppObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AggregateExceptionU5BU5D_t713784977* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	Il2CppObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4084901404, ____items_1)); }
	inline AggregateExceptionU5BU5D_t713784977* get__items_1() const { return ____items_1; }
	inline AggregateExceptionU5BU5D_t713784977** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AggregateExceptionU5BU5D_t713784977* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier(&____items_1, value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4084901404, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4084901404, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4084901404, ____syncRoot_4)); }
	inline Il2CppObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline Il2CppObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(Il2CppObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_4, value);
	}
};

struct List_1_t4084901404_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AggregateExceptionU5BU5D_t713784977* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4084901404_StaticFields, ____emptyArray_5)); }
	inline AggregateExceptionU5BU5D_t713784977* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AggregateExceptionU5BU5D_t713784977** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AggregateExceptionU5BU5D_t713784977* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier(&____emptyArray_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
