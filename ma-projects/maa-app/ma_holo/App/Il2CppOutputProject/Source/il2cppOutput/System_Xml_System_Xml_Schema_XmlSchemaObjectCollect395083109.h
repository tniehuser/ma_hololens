﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_CollectionBase1101587467.h"

// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectCollection
struct  XmlSchemaObjectCollection_t395083109  : public CollectionBase_t1101587467
{
public:
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectCollection::parent
	XmlSchemaObject_t2050913741 * ___parent_1;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectCollection_t395083109, ___parent_1)); }
	inline XmlSchemaObject_t2050913741 * get_parent_1() const { return ___parent_1; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlSchemaObject_t2050913741 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
