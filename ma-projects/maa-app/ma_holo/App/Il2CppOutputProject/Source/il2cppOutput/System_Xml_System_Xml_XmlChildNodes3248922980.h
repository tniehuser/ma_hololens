﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildNodes
struct  XmlChildNodes_t3248922980  : public XmlNodeList_t497326455
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildNodes::container
	XmlNode_t616554813 * ___container_0;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildNodes_t3248922980, ___container_0)); }
	inline XmlNode_t616554813 * get_container_0() const { return ___container_0; }
	inline XmlNode_t616554813 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t616554813 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier(&___container_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
