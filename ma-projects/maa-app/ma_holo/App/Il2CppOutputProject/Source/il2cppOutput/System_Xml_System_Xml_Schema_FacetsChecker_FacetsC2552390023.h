﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct  Map_t2552390023 
{
public:
	// System.Char System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::match
	Il2CppChar ___match_0;
	// System.String System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::replacement
	String_t* ___replacement_1;

public:
	inline static int32_t get_offset_of_match_0() { return static_cast<int32_t>(offsetof(Map_t2552390023, ___match_0)); }
	inline Il2CppChar get_match_0() const { return ___match_0; }
	inline Il2CppChar* get_address_of_match_0() { return &___match_0; }
	inline void set_match_0(Il2CppChar value)
	{
		___match_0 = value;
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(Map_t2552390023, ___replacement_1)); }
	inline String_t* get_replacement_1() const { return ___replacement_1; }
	inline String_t** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(String_t* value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier(&___replacement_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t2552390023_marshaled_pinvoke
{
	uint8_t ___match_0;
	char* ___replacement_1;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t2552390023_marshaled_com
{
	uint8_t ___match_0;
	Il2CppChar* ___replacement_1;
};
