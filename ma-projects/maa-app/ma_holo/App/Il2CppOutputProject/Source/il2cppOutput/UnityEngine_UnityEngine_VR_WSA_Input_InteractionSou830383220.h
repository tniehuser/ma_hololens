﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSou396045678.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1972476489.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.InteractionSourceState
struct  InteractionSourceState_t830383220 
{
public:
	// System.Byte UnityEngine.VR.WSA.Input.InteractionSourceState::m_pressed
	uint8_t ___m_pressed_0;
	// UnityEngine.VR.WSA.Input.InteractionSourceProperties UnityEngine.VR.WSA.Input.InteractionSourceState::m_properties
	InteractionSourceProperties_t396045678  ___m_properties_1;
	// UnityEngine.VR.WSA.Input.InteractionSource UnityEngine.VR.WSA.Input.InteractionSourceState::m_source
	InteractionSource_t1972476489  ___m_source_2;
	// UnityEngine.Ray UnityEngine.VR.WSA.Input.InteractionSourceState::m_headRay
	Ray_t2469606224  ___m_headRay_3;

public:
	inline static int32_t get_offset_of_m_pressed_0() { return static_cast<int32_t>(offsetof(InteractionSourceState_t830383220, ___m_pressed_0)); }
	inline uint8_t get_m_pressed_0() const { return ___m_pressed_0; }
	inline uint8_t* get_address_of_m_pressed_0() { return &___m_pressed_0; }
	inline void set_m_pressed_0(uint8_t value)
	{
		___m_pressed_0 = value;
	}

	inline static int32_t get_offset_of_m_properties_1() { return static_cast<int32_t>(offsetof(InteractionSourceState_t830383220, ___m_properties_1)); }
	inline InteractionSourceProperties_t396045678  get_m_properties_1() const { return ___m_properties_1; }
	inline InteractionSourceProperties_t396045678 * get_address_of_m_properties_1() { return &___m_properties_1; }
	inline void set_m_properties_1(InteractionSourceProperties_t396045678  value)
	{
		___m_properties_1 = value;
	}

	inline static int32_t get_offset_of_m_source_2() { return static_cast<int32_t>(offsetof(InteractionSourceState_t830383220, ___m_source_2)); }
	inline InteractionSource_t1972476489  get_m_source_2() const { return ___m_source_2; }
	inline InteractionSource_t1972476489 * get_address_of_m_source_2() { return &___m_source_2; }
	inline void set_m_source_2(InteractionSource_t1972476489  value)
	{
		___m_source_2 = value;
	}

	inline static int32_t get_offset_of_m_headRay_3() { return static_cast<int32_t>(offsetof(InteractionSourceState_t830383220, ___m_headRay_3)); }
	inline Ray_t2469606224  get_m_headRay_3() const { return ___m_headRay_3; }
	inline Ray_t2469606224 * get_address_of_m_headRay_3() { return &___m_headRay_3; }
	inline void set_m_headRay_3(Ray_t2469606224  value)
	{
		___m_headRay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
