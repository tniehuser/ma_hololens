﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceFilter
struct  TraceFilter_t87508953  : public Il2CppObject
{
public:
	// System.String System.Diagnostics.TraceFilter::initializeData
	String_t* ___initializeData_0;

public:
	inline static int32_t get_offset_of_initializeData_0() { return static_cast<int32_t>(offsetof(TraceFilter_t87508953, ___initializeData_0)); }
	inline String_t* get_initializeData_0() const { return ___initializeData_0; }
	inline String_t** get_address_of_initializeData_0() { return &___initializeData_0; }
	inline void set_initializeData_0(String_t* value)
	{
		___initializeData_0 = value;
		Il2CppCodeGenWriteBarrier(&___initializeData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
