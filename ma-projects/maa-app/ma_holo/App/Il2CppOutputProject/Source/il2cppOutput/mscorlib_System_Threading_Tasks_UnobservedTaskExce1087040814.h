﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"

// System.AggregateException
struct AggregateException_t420812976;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.UnobservedTaskExceptionEventArgs
struct  UnobservedTaskExceptionEventArgs_t1087040814  : public EventArgs_t3289624707
{
public:
	// System.AggregateException System.Threading.Tasks.UnobservedTaskExceptionEventArgs::m_exception
	AggregateException_t420812976 * ___m_exception_1;
	// System.Boolean System.Threading.Tasks.UnobservedTaskExceptionEventArgs::m_observed
	bool ___m_observed_2;

public:
	inline static int32_t get_offset_of_m_exception_1() { return static_cast<int32_t>(offsetof(UnobservedTaskExceptionEventArgs_t1087040814, ___m_exception_1)); }
	inline AggregateException_t420812976 * get_m_exception_1() const { return ___m_exception_1; }
	inline AggregateException_t420812976 ** get_address_of_m_exception_1() { return &___m_exception_1; }
	inline void set_m_exception_1(AggregateException_t420812976 * value)
	{
		___m_exception_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_exception_1, value);
	}

	inline static int32_t get_offset_of_m_observed_2() { return static_cast<int32_t>(offsetof(UnobservedTaskExceptionEventArgs_t1087040814, ___m_observed_2)); }
	inline bool get_m_observed_2() const { return ___m_observed_2; }
	inline bool* get_address_of_m_observed_2() { return &___m_observed_2; }
	inline void set_m_observed_2(bool value)
	{
		___m_observed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
