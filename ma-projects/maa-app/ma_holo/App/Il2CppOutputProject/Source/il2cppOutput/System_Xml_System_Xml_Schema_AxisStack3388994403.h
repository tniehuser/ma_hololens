﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.Schema.ForwardAxis
struct ForwardAxis_t3876904870;
// System.Xml.Schema.ActiveAxis
struct ActiveAxis_t439376929;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisStack
struct  AxisStack_t3388994403  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.AxisStack::stack
	ArrayList_t4252133567 * ___stack_0;
	// System.Xml.Schema.ForwardAxis System.Xml.Schema.AxisStack::subtree
	ForwardAxis_t3876904870 * ___subtree_1;
	// System.Xml.Schema.ActiveAxis System.Xml.Schema.AxisStack::parent
	ActiveAxis_t439376929 * ___parent_2;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___stack_0)); }
	inline ArrayList_t4252133567 * get_stack_0() const { return ___stack_0; }
	inline ArrayList_t4252133567 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ArrayList_t4252133567 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier(&___stack_0, value);
	}

	inline static int32_t get_offset_of_subtree_1() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___subtree_1)); }
	inline ForwardAxis_t3876904870 * get_subtree_1() const { return ___subtree_1; }
	inline ForwardAxis_t3876904870 ** get_address_of_subtree_1() { return &___subtree_1; }
	inline void set_subtree_1(ForwardAxis_t3876904870 * value)
	{
		___subtree_1 = value;
		Il2CppCodeGenWriteBarrier(&___subtree_1, value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___parent_2)); }
	inline ActiveAxis_t439376929 * get_parent_2() const { return ___parent_2; }
	inline ActiveAxis_t439376929 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ActiveAxis_t439376929 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
