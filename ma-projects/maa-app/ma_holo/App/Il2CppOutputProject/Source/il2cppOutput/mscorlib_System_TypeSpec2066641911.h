﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.TypeIdentifier
struct TypeIdentifier_t2572806063;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.TypeIdentifier>
struct List_1_t1941927195;
// System.Collections.Generic.List`1<System.TypeSpec>
struct List_1_t1435763043;
// System.Collections.Generic.List`1<System.ModifierSpec>
struct List_1_t4235013868;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeSpec
struct  TypeSpec_t2066641911  : public Il2CppObject
{
public:
	// System.TypeIdentifier System.TypeSpec::name
	Il2CppObject * ___name_0;
	// System.String System.TypeSpec::assembly_name
	String_t* ___assembly_name_1;
	// System.Collections.Generic.List`1<System.TypeIdentifier> System.TypeSpec::nested
	List_1_t1941927195 * ___nested_2;
	// System.Collections.Generic.List`1<System.TypeSpec> System.TypeSpec::generic_params
	List_1_t1435763043 * ___generic_params_3;
	// System.Collections.Generic.List`1<System.ModifierSpec> System.TypeSpec::modifier_spec
	List_1_t4235013868 * ___modifier_spec_4;
	// System.Boolean System.TypeSpec::is_byref
	bool ___is_byref_5;
	// System.String System.TypeSpec::display_fullname
	String_t* ___display_fullname_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___name_0)); }
	inline Il2CppObject * get_name_0() const { return ___name_0; }
	inline Il2CppObject ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(Il2CppObject * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_assembly_name_1() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___assembly_name_1)); }
	inline String_t* get_assembly_name_1() const { return ___assembly_name_1; }
	inline String_t** get_address_of_assembly_name_1() { return &___assembly_name_1; }
	inline void set_assembly_name_1(String_t* value)
	{
		___assembly_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___assembly_name_1, value);
	}

	inline static int32_t get_offset_of_nested_2() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___nested_2)); }
	inline List_1_t1941927195 * get_nested_2() const { return ___nested_2; }
	inline List_1_t1941927195 ** get_address_of_nested_2() { return &___nested_2; }
	inline void set_nested_2(List_1_t1941927195 * value)
	{
		___nested_2 = value;
		Il2CppCodeGenWriteBarrier(&___nested_2, value);
	}

	inline static int32_t get_offset_of_generic_params_3() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___generic_params_3)); }
	inline List_1_t1435763043 * get_generic_params_3() const { return ___generic_params_3; }
	inline List_1_t1435763043 ** get_address_of_generic_params_3() { return &___generic_params_3; }
	inline void set_generic_params_3(List_1_t1435763043 * value)
	{
		___generic_params_3 = value;
		Il2CppCodeGenWriteBarrier(&___generic_params_3, value);
	}

	inline static int32_t get_offset_of_modifier_spec_4() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___modifier_spec_4)); }
	inline List_1_t4235013868 * get_modifier_spec_4() const { return ___modifier_spec_4; }
	inline List_1_t4235013868 ** get_address_of_modifier_spec_4() { return &___modifier_spec_4; }
	inline void set_modifier_spec_4(List_1_t4235013868 * value)
	{
		___modifier_spec_4 = value;
		Il2CppCodeGenWriteBarrier(&___modifier_spec_4, value);
	}

	inline static int32_t get_offset_of_is_byref_5() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___is_byref_5)); }
	inline bool get_is_byref_5() const { return ___is_byref_5; }
	inline bool* get_address_of_is_byref_5() { return &___is_byref_5; }
	inline void set_is_byref_5(bool value)
	{
		___is_byref_5 = value;
	}

	inline static int32_t get_offset_of_display_fullname_6() { return static_cast<int32_t>(offsetof(TypeSpec_t2066641911, ___display_fullname_6)); }
	inline String_t* get_display_fullname_6() const { return ___display_fullname_6; }
	inline String_t** get_address_of_display_fullname_6() { return &___display_fullname_6; }
	inline void set_display_fullname_6(String_t* value)
	{
		___display_fullname_6 = value;
		Il2CppCodeGenWriteBarrier(&___display_fullname_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
