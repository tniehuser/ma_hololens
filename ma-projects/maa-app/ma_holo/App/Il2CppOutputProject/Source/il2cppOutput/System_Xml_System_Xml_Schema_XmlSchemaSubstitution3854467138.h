﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSubstitutionGroup
struct  XmlSchemaSubstitutionGroup_t3854467138  : public XmlSchemaObject_t2050913741
{
public:
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaSubstitutionGroup::membersList
	ArrayList_t4252133567 * ___membersList_6;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSubstitutionGroup::examplar
	XmlQualifiedName_t1944712516 * ___examplar_7;

public:
	inline static int32_t get_offset_of_membersList_6() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroup_t3854467138, ___membersList_6)); }
	inline ArrayList_t4252133567 * get_membersList_6() const { return ___membersList_6; }
	inline ArrayList_t4252133567 ** get_address_of_membersList_6() { return &___membersList_6; }
	inline void set_membersList_6(ArrayList_t4252133567 * value)
	{
		___membersList_6 = value;
		Il2CppCodeGenWriteBarrier(&___membersList_6, value);
	}

	inline static int32_t get_offset_of_examplar_7() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroup_t3854467138, ___examplar_7)); }
	inline XmlQualifiedName_t1944712516 * get_examplar_7() const { return ___examplar_7; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_examplar_7() { return &___examplar_7; }
	inline void set_examplar_7(XmlQualifiedName_t1944712516 * value)
	{
		___examplar_7 = value;
		Il2CppCodeGenWriteBarrier(&___examplar_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
