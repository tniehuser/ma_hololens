﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_t1152094268;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap
struct  SchemaDatatypeMap_t2661667341  : public Il2CppObject
{
public:
	// System.String System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::name
	String_t* ___name_0;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::type
	DatatypeImplementation_t1152094268 * ___type_1;
	// System.Int32 System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::parentIndex
	int32_t ___parentIndex_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___type_1)); }
	inline DatatypeImplementation_t1152094268 * get_type_1() const { return ___type_1; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(DatatypeImplementation_t1152094268 * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_parentIndex_2() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___parentIndex_2)); }
	inline int32_t get_parentIndex_2() const { return ___parentIndex_2; }
	inline int32_t* get_address_of_parentIndex_2() { return &___parentIndex_2; }
	inline void set_parentIndex_2(int32_t value)
	{
		___parentIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
