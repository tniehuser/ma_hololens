﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operand
struct  Operand_t3474020745  : public AstNode_t2002670936
{
public:
	// System.Xml.XPath.XPathResultType MS.Internal.Xml.XPath.Operand::type
	int32_t ___type_0;
	// System.Object MS.Internal.Xml.XPath.Operand::val
	Il2CppObject * ___val_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Operand_t3474020745, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(Operand_t3474020745, ___val_1)); }
	inline Il2CppObject * get_val_1() const { return ___val_1; }
	inline Il2CppObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(Il2CppObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier(&___val_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
