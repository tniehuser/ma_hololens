﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Security.Util.TokenizerStringBlock
struct TokenizerStringBlock_t1488754793;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.TokenizerStringBlock
struct  TokenizerStringBlock_t1488754793  : public Il2CppObject
{
public:
	// System.String[] System.Security.Util.TokenizerStringBlock::m_block
	StringU5BU5D_t1642385972* ___m_block_0;
	// System.Security.Util.TokenizerStringBlock System.Security.Util.TokenizerStringBlock::m_next
	TokenizerStringBlock_t1488754793 * ___m_next_1;

public:
	inline static int32_t get_offset_of_m_block_0() { return static_cast<int32_t>(offsetof(TokenizerStringBlock_t1488754793, ___m_block_0)); }
	inline StringU5BU5D_t1642385972* get_m_block_0() const { return ___m_block_0; }
	inline StringU5BU5D_t1642385972** get_address_of_m_block_0() { return &___m_block_0; }
	inline void set_m_block_0(StringU5BU5D_t1642385972* value)
	{
		___m_block_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_block_0, value);
	}

	inline static int32_t get_offset_of_m_next_1() { return static_cast<int32_t>(offsetof(TokenizerStringBlock_t1488754793, ___m_next_1)); }
	inline TokenizerStringBlock_t1488754793 * get_m_next_1() const { return ___m_next_1; }
	inline TokenizerStringBlock_t1488754793 ** get_address_of_m_next_1() { return &___m_next_1; }
	inline void set_m_next_1(TokenizerStringBlock_t1488754793 * value)
	{
		___m_next_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_next_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
