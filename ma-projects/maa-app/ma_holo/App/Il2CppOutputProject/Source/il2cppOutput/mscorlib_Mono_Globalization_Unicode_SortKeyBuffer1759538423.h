﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_CompareOptions2829943955.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.SortKeyBuffer
struct  SortKeyBuffer_t1759538423  : public Il2CppObject
{
public:
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l1b
	ByteU5BU5D_t3397334013* ___l1b_0;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l2b
	ByteU5BU5D_t3397334013* ___l2b_1;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l3b
	ByteU5BU5D_t3397334013* ___l3b_2;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l4sb
	ByteU5BU5D_t3397334013* ___l4sb_3;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l4tb
	ByteU5BU5D_t3397334013* ___l4tb_4;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l4kb
	ByteU5BU5D_t3397334013* ___l4kb_5;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l4wb
	ByteU5BU5D_t3397334013* ___l4wb_6;
	// System.Byte[] Mono.Globalization.Unicode.SortKeyBuffer::l5b
	ByteU5BU5D_t3397334013* ___l5b_7;
	// System.String Mono.Globalization.Unicode.SortKeyBuffer::source
	String_t* ___source_8;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l1
	int32_t ___l1_9;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l2
	int32_t ___l2_10;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l3
	int32_t ___l3_11;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l4s
	int32_t ___l4s_12;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l4t
	int32_t ___l4t_13;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l4k
	int32_t ___l4k_14;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l4w
	int32_t ___l4w_15;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::l5
	int32_t ___l5_16;
	// System.Int32 Mono.Globalization.Unicode.SortKeyBuffer::lcid
	int32_t ___lcid_17;
	// System.Globalization.CompareOptions Mono.Globalization.Unicode.SortKeyBuffer::options
	int32_t ___options_18;
	// System.Boolean Mono.Globalization.Unicode.SortKeyBuffer::processLevel2
	bool ___processLevel2_19;
	// System.Boolean Mono.Globalization.Unicode.SortKeyBuffer::frenchSort
	bool ___frenchSort_20;
	// System.Boolean Mono.Globalization.Unicode.SortKeyBuffer::frenchSorted
	bool ___frenchSorted_21;

public:
	inline static int32_t get_offset_of_l1b_0() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l1b_0)); }
	inline ByteU5BU5D_t3397334013* get_l1b_0() const { return ___l1b_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_l1b_0() { return &___l1b_0; }
	inline void set_l1b_0(ByteU5BU5D_t3397334013* value)
	{
		___l1b_0 = value;
		Il2CppCodeGenWriteBarrier(&___l1b_0, value);
	}

	inline static int32_t get_offset_of_l2b_1() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l2b_1)); }
	inline ByteU5BU5D_t3397334013* get_l2b_1() const { return ___l2b_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_l2b_1() { return &___l2b_1; }
	inline void set_l2b_1(ByteU5BU5D_t3397334013* value)
	{
		___l2b_1 = value;
		Il2CppCodeGenWriteBarrier(&___l2b_1, value);
	}

	inline static int32_t get_offset_of_l3b_2() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l3b_2)); }
	inline ByteU5BU5D_t3397334013* get_l3b_2() const { return ___l3b_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_l3b_2() { return &___l3b_2; }
	inline void set_l3b_2(ByteU5BU5D_t3397334013* value)
	{
		___l3b_2 = value;
		Il2CppCodeGenWriteBarrier(&___l3b_2, value);
	}

	inline static int32_t get_offset_of_l4sb_3() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4sb_3)); }
	inline ByteU5BU5D_t3397334013* get_l4sb_3() const { return ___l4sb_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_l4sb_3() { return &___l4sb_3; }
	inline void set_l4sb_3(ByteU5BU5D_t3397334013* value)
	{
		___l4sb_3 = value;
		Il2CppCodeGenWriteBarrier(&___l4sb_3, value);
	}

	inline static int32_t get_offset_of_l4tb_4() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4tb_4)); }
	inline ByteU5BU5D_t3397334013* get_l4tb_4() const { return ___l4tb_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_l4tb_4() { return &___l4tb_4; }
	inline void set_l4tb_4(ByteU5BU5D_t3397334013* value)
	{
		___l4tb_4 = value;
		Il2CppCodeGenWriteBarrier(&___l4tb_4, value);
	}

	inline static int32_t get_offset_of_l4kb_5() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4kb_5)); }
	inline ByteU5BU5D_t3397334013* get_l4kb_5() const { return ___l4kb_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_l4kb_5() { return &___l4kb_5; }
	inline void set_l4kb_5(ByteU5BU5D_t3397334013* value)
	{
		___l4kb_5 = value;
		Il2CppCodeGenWriteBarrier(&___l4kb_5, value);
	}

	inline static int32_t get_offset_of_l4wb_6() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4wb_6)); }
	inline ByteU5BU5D_t3397334013* get_l4wb_6() const { return ___l4wb_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_l4wb_6() { return &___l4wb_6; }
	inline void set_l4wb_6(ByteU5BU5D_t3397334013* value)
	{
		___l4wb_6 = value;
		Il2CppCodeGenWriteBarrier(&___l4wb_6, value);
	}

	inline static int32_t get_offset_of_l5b_7() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l5b_7)); }
	inline ByteU5BU5D_t3397334013* get_l5b_7() const { return ___l5b_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_l5b_7() { return &___l5b_7; }
	inline void set_l5b_7(ByteU5BU5D_t3397334013* value)
	{
		___l5b_7 = value;
		Il2CppCodeGenWriteBarrier(&___l5b_7, value);
	}

	inline static int32_t get_offset_of_source_8() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___source_8)); }
	inline String_t* get_source_8() const { return ___source_8; }
	inline String_t** get_address_of_source_8() { return &___source_8; }
	inline void set_source_8(String_t* value)
	{
		___source_8 = value;
		Il2CppCodeGenWriteBarrier(&___source_8, value);
	}

	inline static int32_t get_offset_of_l1_9() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l1_9)); }
	inline int32_t get_l1_9() const { return ___l1_9; }
	inline int32_t* get_address_of_l1_9() { return &___l1_9; }
	inline void set_l1_9(int32_t value)
	{
		___l1_9 = value;
	}

	inline static int32_t get_offset_of_l2_10() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l2_10)); }
	inline int32_t get_l2_10() const { return ___l2_10; }
	inline int32_t* get_address_of_l2_10() { return &___l2_10; }
	inline void set_l2_10(int32_t value)
	{
		___l2_10 = value;
	}

	inline static int32_t get_offset_of_l3_11() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l3_11)); }
	inline int32_t get_l3_11() const { return ___l3_11; }
	inline int32_t* get_address_of_l3_11() { return &___l3_11; }
	inline void set_l3_11(int32_t value)
	{
		___l3_11 = value;
	}

	inline static int32_t get_offset_of_l4s_12() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4s_12)); }
	inline int32_t get_l4s_12() const { return ___l4s_12; }
	inline int32_t* get_address_of_l4s_12() { return &___l4s_12; }
	inline void set_l4s_12(int32_t value)
	{
		___l4s_12 = value;
	}

	inline static int32_t get_offset_of_l4t_13() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4t_13)); }
	inline int32_t get_l4t_13() const { return ___l4t_13; }
	inline int32_t* get_address_of_l4t_13() { return &___l4t_13; }
	inline void set_l4t_13(int32_t value)
	{
		___l4t_13 = value;
	}

	inline static int32_t get_offset_of_l4k_14() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4k_14)); }
	inline int32_t get_l4k_14() const { return ___l4k_14; }
	inline int32_t* get_address_of_l4k_14() { return &___l4k_14; }
	inline void set_l4k_14(int32_t value)
	{
		___l4k_14 = value;
	}

	inline static int32_t get_offset_of_l4w_15() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l4w_15)); }
	inline int32_t get_l4w_15() const { return ___l4w_15; }
	inline int32_t* get_address_of_l4w_15() { return &___l4w_15; }
	inline void set_l4w_15(int32_t value)
	{
		___l4w_15 = value;
	}

	inline static int32_t get_offset_of_l5_16() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___l5_16)); }
	inline int32_t get_l5_16() const { return ___l5_16; }
	inline int32_t* get_address_of_l5_16() { return &___l5_16; }
	inline void set_l5_16(int32_t value)
	{
		___l5_16 = value;
	}

	inline static int32_t get_offset_of_lcid_17() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___lcid_17)); }
	inline int32_t get_lcid_17() const { return ___lcid_17; }
	inline int32_t* get_address_of_lcid_17() { return &___lcid_17; }
	inline void set_lcid_17(int32_t value)
	{
		___lcid_17 = value;
	}

	inline static int32_t get_offset_of_options_18() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___options_18)); }
	inline int32_t get_options_18() const { return ___options_18; }
	inline int32_t* get_address_of_options_18() { return &___options_18; }
	inline void set_options_18(int32_t value)
	{
		___options_18 = value;
	}

	inline static int32_t get_offset_of_processLevel2_19() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___processLevel2_19)); }
	inline bool get_processLevel2_19() const { return ___processLevel2_19; }
	inline bool* get_address_of_processLevel2_19() { return &___processLevel2_19; }
	inline void set_processLevel2_19(bool value)
	{
		___processLevel2_19 = value;
	}

	inline static int32_t get_offset_of_frenchSort_20() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___frenchSort_20)); }
	inline bool get_frenchSort_20() const { return ___frenchSort_20; }
	inline bool* get_address_of_frenchSort_20() { return &___frenchSort_20; }
	inline void set_frenchSort_20(bool value)
	{
		___frenchSort_20 = value;
	}

	inline static int32_t get_offset_of_frenchSorted_21() { return static_cast<int32_t>(offsetof(SortKeyBuffer_t1759538423, ___frenchSorted_21)); }
	inline bool get_frenchSorted_21() const { return ___frenchSorted_21; }
	inline bool* get_address_of_frenchSorted_21() { return &___frenchSorted_21; }
	inline void set_frenchSorted_21(bool value)
	{
		___frenchSorted_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
