﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_long513905339.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_int
struct  Datatype_int_t2281243990  : public Datatype_long_t513905339
{
public:

public:
};

struct Datatype_int_t2281243990_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_int::atomicValueType
	Type_t * ___atomicValueType_99;
	// System.Type System.Xml.Schema.Datatype_int::listValueType
	Type_t * ___listValueType_100;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_int::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_101;

public:
	inline static int32_t get_offset_of_atomicValueType_99() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___atomicValueType_99)); }
	inline Type_t * get_atomicValueType_99() const { return ___atomicValueType_99; }
	inline Type_t ** get_address_of_atomicValueType_99() { return &___atomicValueType_99; }
	inline void set_atomicValueType_99(Type_t * value)
	{
		___atomicValueType_99 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_99, value);
	}

	inline static int32_t get_offset_of_listValueType_100() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___listValueType_100)); }
	inline Type_t * get_listValueType_100() const { return ___listValueType_100; }
	inline Type_t ** get_address_of_listValueType_100() { return &___listValueType_100; }
	inline void set_listValueType_100(Type_t * value)
	{
		___listValueType_100 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_100, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_101() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___numeric10FacetsChecker_101)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_101() const { return ___numeric10FacetsChecker_101; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_101() { return &___numeric10FacetsChecker_101; }
	inline void set_numeric10FacetsChecker_101(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_101 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_101, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
