﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.IO.Compression.DeflateStreamNative
struct DeflateStreamNative_t76712276;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream
struct  DeflateStream_t3198596725  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.IO.Compression.DeflateStream::base_stream
	Stream_t3255436806 * ___base_stream_8;
	// System.IO.Compression.CompressionMode System.IO.Compression.DeflateStream::mode
	int32_t ___mode_9;
	// System.Boolean System.IO.Compression.DeflateStream::leaveOpen
	bool ___leaveOpen_10;
	// System.Boolean System.IO.Compression.DeflateStream::disposed
	bool ___disposed_11;
	// System.IO.Compression.DeflateStreamNative System.IO.Compression.DeflateStream::native
	DeflateStreamNative_t76712276 * ___native_12;

public:
	inline static int32_t get_offset_of_base_stream_8() { return static_cast<int32_t>(offsetof(DeflateStream_t3198596725, ___base_stream_8)); }
	inline Stream_t3255436806 * get_base_stream_8() const { return ___base_stream_8; }
	inline Stream_t3255436806 ** get_address_of_base_stream_8() { return &___base_stream_8; }
	inline void set_base_stream_8(Stream_t3255436806 * value)
	{
		___base_stream_8 = value;
		Il2CppCodeGenWriteBarrier(&___base_stream_8, value);
	}

	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(DeflateStream_t3198596725, ___mode_9)); }
	inline int32_t get_mode_9() const { return ___mode_9; }
	inline int32_t* get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(int32_t value)
	{
		___mode_9 = value;
	}

	inline static int32_t get_offset_of_leaveOpen_10() { return static_cast<int32_t>(offsetof(DeflateStream_t3198596725, ___leaveOpen_10)); }
	inline bool get_leaveOpen_10() const { return ___leaveOpen_10; }
	inline bool* get_address_of_leaveOpen_10() { return &___leaveOpen_10; }
	inline void set_leaveOpen_10(bool value)
	{
		___leaveOpen_10 = value;
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(DeflateStream_t3198596725, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_native_12() { return static_cast<int32_t>(offsetof(DeflateStream_t3198596725, ___native_12)); }
	inline DeflateStreamNative_t76712276 * get_native_12() const { return ___native_12; }
	inline DeflateStreamNative_t76712276 ** get_address_of_native_12() { return &___native_12; }
	inline void set_native_12(DeflateStreamNative_t76712276 * value)
	{
		___native_12 = value;
		Il2CppCodeGenWriteBarrier(&___native_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
