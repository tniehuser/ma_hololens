﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_IncrementalReadDecoder403822042.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexDecoder
struct  BinHexDecoder_t3180637936  : public IncrementalReadDecoder_t403822042
{
public:
	// System.Byte[] System.Xml.BinHexDecoder::buffer
	ByteU5BU5D_t3397334013* ___buffer_0;
	// System.Int32 System.Xml.BinHexDecoder::curIndex
	int32_t ___curIndex_1;
	// System.Int32 System.Xml.BinHexDecoder::endIndex
	int32_t ___endIndex_2;
	// System.Boolean System.Xml.BinHexDecoder::hasHalfByteCached
	bool ___hasHalfByteCached_3;
	// System.Byte System.Xml.BinHexDecoder::cachedHalfByte
	uint8_t ___cachedHalfByte_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BinHexDecoder_t3180637936, ___buffer_0)); }
	inline ByteU5BU5D_t3397334013* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t3397334013* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_0, value);
	}

	inline static int32_t get_offset_of_curIndex_1() { return static_cast<int32_t>(offsetof(BinHexDecoder_t3180637936, ___curIndex_1)); }
	inline int32_t get_curIndex_1() const { return ___curIndex_1; }
	inline int32_t* get_address_of_curIndex_1() { return &___curIndex_1; }
	inline void set_curIndex_1(int32_t value)
	{
		___curIndex_1 = value;
	}

	inline static int32_t get_offset_of_endIndex_2() { return static_cast<int32_t>(offsetof(BinHexDecoder_t3180637936, ___endIndex_2)); }
	inline int32_t get_endIndex_2() const { return ___endIndex_2; }
	inline int32_t* get_address_of_endIndex_2() { return &___endIndex_2; }
	inline void set_endIndex_2(int32_t value)
	{
		___endIndex_2 = value;
	}

	inline static int32_t get_offset_of_hasHalfByteCached_3() { return static_cast<int32_t>(offsetof(BinHexDecoder_t3180637936, ___hasHalfByteCached_3)); }
	inline bool get_hasHalfByteCached_3() const { return ___hasHalfByteCached_3; }
	inline bool* get_address_of_hasHalfByteCached_3() { return &___hasHalfByteCached_3; }
	inline void set_hasHalfByteCached_3(bool value)
	{
		___hasHalfByteCached_3 = value;
	}

	inline static int32_t get_offset_of_cachedHalfByte_4() { return static_cast<int32_t>(offsetof(BinHexDecoder_t3180637936, ___cachedHalfByte_4)); }
	inline uint8_t get_cachedHalfByte_4() const { return ___cachedHalfByte_4; }
	inline uint8_t* get_address_of_cachedHalfByte_4() { return &___cachedHalfByte_4; }
	inline void set_cachedHalfByte_4(uint8_t value)
	{
		___cachedHalfByte_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
