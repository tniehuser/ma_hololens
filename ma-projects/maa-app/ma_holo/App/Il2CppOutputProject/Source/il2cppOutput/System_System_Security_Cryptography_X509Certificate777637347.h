﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Cryptography.X509Certificates.X509ChainImpl
struct X509ChainImpl_t2968295413;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t830390908;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t777637347  : public Il2CppObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainImpl System.Security.Cryptography.X509Certificates.X509Chain::impl
	X509ChainImpl_t2968295413 * ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Chain_t777637347, ___impl_0)); }
	inline X509ChainImpl_t2968295413 * get_impl_0() const { return ___impl_0; }
	inline X509ChainImpl_t2968295413 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509ChainImpl_t2968295413 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier(&___impl_0, value);
	}
};

struct X509Chain_t777637347_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::Empty
	X509ChainStatusU5BU5D_t830390908* ___Empty_1;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(X509Chain_t777637347_StaticFields, ___Empty_1)); }
	inline X509ChainStatusU5BU5D_t830390908* get_Empty_1() const { return ___Empty_1; }
	inline X509ChainStatusU5BU5D_t830390908** get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(X509ChainStatusU5BU5D_t830390908* value)
	{
		___Empty_1 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
