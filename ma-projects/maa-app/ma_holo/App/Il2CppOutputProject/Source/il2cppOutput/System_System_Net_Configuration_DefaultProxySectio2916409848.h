﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur2600766927.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.DefaultProxySection
struct  DefaultProxySection_t2916409848  : public ConfigurationSection_t2600766927
{
public:

public:
};

struct DefaultProxySection_t2916409848_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.DefaultProxySection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_19;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::bypassListProp
	ConfigurationProperty_t2048066811 * ___bypassListProp_20;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::enabledProp
	ConfigurationProperty_t2048066811 * ___enabledProp_21;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::moduleProp
	ConfigurationProperty_t2048066811 * ___moduleProp_22;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::proxyProp
	ConfigurationProperty_t2048066811 * ___proxyProp_23;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::useDefaultCredentialsProp
	ConfigurationProperty_t2048066811 * ___useDefaultCredentialsProp_24;

public:
	inline static int32_t get_offset_of_properties_19() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___properties_19)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_19() const { return ___properties_19; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_19() { return &___properties_19; }
	inline void set_properties_19(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_19 = value;
		Il2CppCodeGenWriteBarrier(&___properties_19, value);
	}

	inline static int32_t get_offset_of_bypassListProp_20() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___bypassListProp_20)); }
	inline ConfigurationProperty_t2048066811 * get_bypassListProp_20() const { return ___bypassListProp_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_bypassListProp_20() { return &___bypassListProp_20; }
	inline void set_bypassListProp_20(ConfigurationProperty_t2048066811 * value)
	{
		___bypassListProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___bypassListProp_20, value);
	}

	inline static int32_t get_offset_of_enabledProp_21() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___enabledProp_21)); }
	inline ConfigurationProperty_t2048066811 * get_enabledProp_21() const { return ___enabledProp_21; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_enabledProp_21() { return &___enabledProp_21; }
	inline void set_enabledProp_21(ConfigurationProperty_t2048066811 * value)
	{
		___enabledProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___enabledProp_21, value);
	}

	inline static int32_t get_offset_of_moduleProp_22() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___moduleProp_22)); }
	inline ConfigurationProperty_t2048066811 * get_moduleProp_22() const { return ___moduleProp_22; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_moduleProp_22() { return &___moduleProp_22; }
	inline void set_moduleProp_22(ConfigurationProperty_t2048066811 * value)
	{
		___moduleProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___moduleProp_22, value);
	}

	inline static int32_t get_offset_of_proxyProp_23() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___proxyProp_23)); }
	inline ConfigurationProperty_t2048066811 * get_proxyProp_23() const { return ___proxyProp_23; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_proxyProp_23() { return &___proxyProp_23; }
	inline void set_proxyProp_23(ConfigurationProperty_t2048066811 * value)
	{
		___proxyProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___proxyProp_23, value);
	}

	inline static int32_t get_offset_of_useDefaultCredentialsProp_24() { return static_cast<int32_t>(offsetof(DefaultProxySection_t2916409848_StaticFields, ___useDefaultCredentialsProp_24)); }
	inline ConfigurationProperty_t2048066811 * get_useDefaultCredentialsProp_24() const { return ___useDefaultCredentialsProp_24; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_useDefaultCredentialsProp_24() { return &___useDefaultCredentialsProp_24; }
	inline void set_useDefaultCredentialsProp_24(ConfigurationProperty_t2048066811 * value)
	{
		___useDefaultCredentialsProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___useDefaultCredentialsProp_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
