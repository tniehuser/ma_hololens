﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t1606103299;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t248156492  : public XmlSchemaType_t1795078578
{
public:
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t1606103299 * ___content_19;

public:
	inline static int32_t get_offset_of_content_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492, ___content_19)); }
	inline XmlSchemaSimpleTypeContent_t1606103299 * get_content_19() const { return ___content_19; }
	inline XmlSchemaSimpleTypeContent_t1606103299 ** get_address_of_content_19() { return &___content_19; }
	inline void set_content_19(XmlSchemaSimpleTypeContent_t1606103299 * value)
	{
		___content_19 = value;
		Il2CppCodeGenWriteBarrier(&___content_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
