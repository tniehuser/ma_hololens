﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextEncoder
struct  XmlTextEncoder_t2806209204  : public Il2CppObject
{
public:
	// System.IO.TextWriter System.Xml.XmlTextEncoder::textWriter
	TextWriter_t4027217640 * ___textWriter_0;
	// System.Boolean System.Xml.XmlTextEncoder::inAttribute
	bool ___inAttribute_1;
	// System.Char System.Xml.XmlTextEncoder::quoteChar
	Il2CppChar ___quoteChar_2;
	// System.Text.StringBuilder System.Xml.XmlTextEncoder::attrValue
	StringBuilder_t1221177846 * ___attrValue_3;
	// System.Boolean System.Xml.XmlTextEncoder::cacheAttrValue
	bool ___cacheAttrValue_4;
	// System.Xml.XmlCharType System.Xml.XmlTextEncoder::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_5;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___textWriter_0)); }
	inline TextWriter_t4027217640 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t4027217640 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t4027217640 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier(&___textWriter_0, value);
	}

	inline static int32_t get_offset_of_inAttribute_1() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___inAttribute_1)); }
	inline bool get_inAttribute_1() const { return ___inAttribute_1; }
	inline bool* get_address_of_inAttribute_1() { return &___inAttribute_1; }
	inline void set_inAttribute_1(bool value)
	{
		___inAttribute_1 = value;
	}

	inline static int32_t get_offset_of_quoteChar_2() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___quoteChar_2)); }
	inline Il2CppChar get_quoteChar_2() const { return ___quoteChar_2; }
	inline Il2CppChar* get_address_of_quoteChar_2() { return &___quoteChar_2; }
	inline void set_quoteChar_2(Il2CppChar value)
	{
		___quoteChar_2 = value;
	}

	inline static int32_t get_offset_of_attrValue_3() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___attrValue_3)); }
	inline StringBuilder_t1221177846 * get_attrValue_3() const { return ___attrValue_3; }
	inline StringBuilder_t1221177846 ** get_address_of_attrValue_3() { return &___attrValue_3; }
	inline void set_attrValue_3(StringBuilder_t1221177846 * value)
	{
		___attrValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___attrValue_3, value);
	}

	inline static int32_t get_offset_of_cacheAttrValue_4() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___cacheAttrValue_4)); }
	inline bool get_cacheAttrValue_4() const { return ___cacheAttrValue_4; }
	inline bool* get_address_of_cacheAttrValue_4() { return &___cacheAttrValue_4; }
	inline void set_cacheAttrValue_4(bool value)
	{
		___cacheAttrValue_4 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t2806209204, ___xmlCharType_5)); }
	inline XmlCharType_t1050521405  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t1050521405  value)
	{
		___xmlCharType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
