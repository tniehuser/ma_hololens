﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_Datatype_positiveInte3806316496.h"
#include "System_Xml_System_Xml_Schema_Datatype_doubleXdr2833069974.h"
#include "System_Xml_System_Xml_Schema_Datatype_floatXdr1510312673.h"
#include "System_Xml_System_Xml_Schema_Datatype_QNameXdr2452527483.h"
#include "System_Xml_System_Xml_Schema_Datatype_ENUMERATION3978335496.h"
#include "System_Xml_System_Xml_Schema_Datatype_char534108237.h"
#include "System_Xml_System_Xml_Schema_Datatype_fixed1126112663.h"
#include "System_Xml_System_Xml_Schema_Datatype_uuid2771989020.h"
#include "System_Xml_System_Xml_DtdParser821190747.h"
#include "System_Xml_System_Xml_DtdParser_Token322298853.h"
#include "System_Xml_System_Xml_DtdParser_ScanningFunction2556434333.h"
#include "System_Xml_System_Xml_DtdParser_LiteralType3592345881.h"
#include "System_Xml_System_Xml_DtdParser_UndeclaredNotation2066394897.h"
#include "System_Xml_System_Xml_DtdParser_ParseElementOnlyCon225387055.h"
#include "System_Xml_System_Xml_Schema_DtdValidator1639720164.h"
#include "System_Xml_System_Xml_Schema_DtdValidator_NamespaceMa1407344.h"
#include "System_Xml_System_Xml_Schema_FacetsChecker1235574227.h"
#include "System_Xml_System_Xml_Schema_FacetsChecker_FacetsC3565032206.h"
#include "System_Xml_System_Xml_Schema_FacetsChecker_FacetsC2552390023.h"
#include "System_Xml_System_Xml_Schema_Numeric10FacetsChecke3431899547.h"
#include "System_Xml_System_Xml_Schema_Numeric2FacetsChecker2224492892.h"
#include "System_Xml_System_Xml_Schema_DurationFacetsChecker4254067383.h"
#include "System_Xml_System_Xml_Schema_DateTimeFacetsChecker2220827556.h"
#include "System_Xml_System_Xml_Schema_StringFacetsChecker2109036348.h"
#include "System_Xml_System_Xml_Schema_QNameFacetsChecker3974186457.h"
#include "System_Xml_System_Xml_Schema_MiscFacetsChecker3607863675.h"
#include "System_Xml_System_Xml_Schema_BinaryFacetsChecker4115715422.h"
#include "System_Xml_System_Xml_Schema_ListFacetsChecker961741019.h"
#include "System_Xml_System_Xml_Schema_UnionFacetsChecker3025714558.h"
#include "System_Xml_System_Xml_Schema_NamespaceList848177191.h"
#include "System_Xml_System_Xml_Schema_NamespaceList_ListTyp2879180877.h"
#include "System_Xml_System_Xml_Schema_NamespaceListV1Compat2783980816.h"
#include "System_Xml_System_Xml_Schema_Parser1940171737.h"
#include "System_Xml_System_Xml_Schema_Compositor2211431003.h"
#include "System_Xml_System_Xml_Schema_Preprocessor4137165425.h"
#include "System_Xml_System_Xml_Schema_SchemaAttDef1510907267.h"
#include "System_Xml_System_Xml_Schema_SchemaAttDef_Reserve2330987269.h"
#include "System_Xml_System_Xml_Schema_SchemaBuilder908297946.h"
#include "System_Xml_System_Xml_Schema_SchemaCollectionCompi1443051254.h"
#include "System_Xml_System_Xml_Schema_SchemaCollectionPrepr2028437652.h"
#include "System_Xml_System_Xml_Schema_SchemaCollectionPrepr3622502717.h"
#include "System_Xml_System_Xml_Schema_SchemaDeclBase797759480.h"
#include "System_Xml_System_Xml_Schema_SchemaDeclBase_Use2612116671.h"
#include "System_Xml_System_Xml_Schema_SchemaElementDecl1940851905.h"
#include "System_Xml_System_Xml_Schema_SchemaEntity980649128.h"
#include "System_Xml_System_Xml_Schema_AttributeMatchState2850601114.h"
#include "System_Xml_System_Xml_Schema_SchemaInfo87206461.h"
#include "System_Xml_System_Xml_Schema_SchemaNames1619962557.h"
#include "System_Xml_System_Xml_Schema_SchemaNames_Token1005517746.h"
#include "System_Xml_System_Xml_Schema_SchemaNamespaceManage3179848951.h"
#include "System_Xml_System_Xml_Schema_SchemaNotation2083484095.h"
#include "System_Xml_System_Xml_Schema_Compiler2139734799.h"
#include "System_Xml_System_Xml_Schema_SchemaType3522160305.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs1577905814.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle1580700381.h"
#include "System_Xml_System_Xml_Schema_StateUnion1440994946.h"
#include "System_Xml_System_Xml_Schema_ValidationState3143048826.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder1520335019.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_DeclBaseIn1711216790.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_GroupConte1687431939.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_ElementCon3043903854.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_AttributeC2961854454.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrBuildFu4291501283.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrInitFun2565742755.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrBeginChi218963458.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrEndChil4290565954.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrAttribu2834798683.h"
#include "System_Xml_System_Xml_Schema_XdrBuilder_XdrEntry2813485863.h"
#include "System_Xml_System_Xml_Schema_XdrValidator2966394030.h"
#include "System_Xml_System_Xml_Schema_XmlAtomicValue752869371.h"
#include "System_Xml_System_Xml_Schema_XmlAtomicValue_Union69719246.h"
#include "System_Xml_System_Xml_Schema_XmlAtomicValue_Namesp3069403619.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAll1805755215.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation2400301303.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnyAttribute530453212.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAny3277730824.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo2033489551.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute4015859774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou491156493.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou825996660.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaChoice654568461.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection3518500204.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollectionNo3122448512.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollectionEn1538181312.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS2971213394.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte2065934415.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConten655218998.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte1722137421.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType4086789226.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContent3733871217.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentModel907989596.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2874429441.h"
#include "System_Xml_System_Xml_Schema_XmlSchema880472818.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDocumentatio3832803992.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement2433337156.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Datatype_positiveInteger_t3806316496), -1, sizeof(Datatype_positiveInteger_t3806316496_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	Datatype_positiveInteger_t3806316496_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (Datatype_doubleXdr_t2833069974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (Datatype_floatXdr_t1510312673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (Datatype_QNameXdr_t2452527483), -1, sizeof(Datatype_QNameXdr_t2452527483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	Datatype_QNameXdr_t2452527483_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QNameXdr_t2452527483_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (Datatype_ENUMERATION_t3978335496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (Datatype_char_t534108237), -1, sizeof(Datatype_char_t534108237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2605[2] = 
{
	Datatype_char_t534108237_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_char_t534108237_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (Datatype_fixed_t1126112663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (Datatype_uuid_t2771989020), -1, sizeof(Datatype_uuid_t2771989020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2607[2] = 
{
	Datatype_uuid_t2771989020_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_uuid_t2771989020_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (DtdParser_t821190747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[33] = 
{
	DtdParser_t821190747::get_offset_of_readerAdapter_0(),
	DtdParser_t821190747::get_offset_of_readerAdapterWithValidation_1(),
	DtdParser_t821190747::get_offset_of_nameTable_2(),
	DtdParser_t821190747::get_offset_of_schemaInfo_3(),
	DtdParser_t821190747::get_offset_of_xmlCharType_4(),
	DtdParser_t821190747::get_offset_of_systemId_5(),
	DtdParser_t821190747::get_offset_of_publicId_6(),
	DtdParser_t821190747::get_offset_of_normalize_7(),
	DtdParser_t821190747::get_offset_of_validate_8(),
	DtdParser_t821190747::get_offset_of_supportNamespaces_9(),
	DtdParser_t821190747::get_offset_of_v1Compat_10(),
	DtdParser_t821190747::get_offset_of_chars_11(),
	DtdParser_t821190747::get_offset_of_charsUsed_12(),
	DtdParser_t821190747::get_offset_of_curPos_13(),
	DtdParser_t821190747::get_offset_of_scanningFunction_14(),
	DtdParser_t821190747::get_offset_of_nextScaningFunction_15(),
	DtdParser_t821190747::get_offset_of_savedScanningFunction_16(),
	DtdParser_t821190747::get_offset_of_whitespaceSeen_17(),
	DtdParser_t821190747::get_offset_of_tokenStartPos_18(),
	DtdParser_t821190747::get_offset_of_colonPos_19(),
	DtdParser_t821190747::get_offset_of_internalSubsetValueSb_20(),
	DtdParser_t821190747::get_offset_of_externalEntitiesDepth_21(),
	DtdParser_t821190747::get_offset_of_currentEntityId_22(),
	DtdParser_t821190747::get_offset_of_freeFloatingDtd_23(),
	DtdParser_t821190747::get_offset_of_hasFreeFloatingInternalSubset_24(),
	DtdParser_t821190747::get_offset_of_stringBuilder_25(),
	DtdParser_t821190747::get_offset_of_condSectionDepth_26(),
	DtdParser_t821190747::get_offset_of_literalLineInfo_27(),
	DtdParser_t821190747::get_offset_of_literalQuoteChar_28(),
	DtdParser_t821190747::get_offset_of_documentBaseUri_29(),
	DtdParser_t821190747::get_offset_of_externalDtdBaseUri_30(),
	DtdParser_t821190747::get_offset_of_undeclaredNotations_31(),
	DtdParser_t821190747::get_offset_of_condSectionEntityIds_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (Token_t322298853)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[49] = 
{
	Token_t322298853::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (ScanningFunction_t2556434333)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[35] = 
{
	ScanningFunction_t2556434333::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (LiteralType_t3592345881)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2611[4] = 
{
	LiteralType_t3592345881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (UndeclaredNotation_t2066394897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[4] = 
{
	UndeclaredNotation_t2066394897::get_offset_of_name_0(),
	UndeclaredNotation_t2066394897::get_offset_of_lineNo_1(),
	UndeclaredNotation_t2066394897::get_offset_of_linePos_2(),
	UndeclaredNotation_t2066394897::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (ParseElementOnlyContent_LocalFrame_t225387055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[2] = 
{
	ParseElementOnlyContent_LocalFrame_t225387055::get_offset_of_startParenEntityId_0(),
	ParseElementOnlyContent_LocalFrame_t225387055::get_offset_of_parsingSchema_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (DtdValidator_t1639720164), -1, sizeof(DtdValidator_t1639720164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[7] = 
{
	DtdValidator_t1639720164_StaticFields::get_offset_of_namespaceManager_15(),
	DtdValidator_t1639720164::get_offset_of_validationStack_16(),
	DtdValidator_t1639720164::get_offset_of_attPresence_17(),
	DtdValidator_t1639720164::get_offset_of_name_18(),
	DtdValidator_t1639720164::get_offset_of_IDs_19(),
	DtdValidator_t1639720164::get_offset_of_idRefListHead_20(),
	DtdValidator_t1639720164::get_offset_of_processIdentityConstraints_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (NamespaceManager_t1407344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (FacetsChecker_t1235574227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (FacetsCompiler_t3565032206)+ sizeof (Il2CppObject), -1, sizeof(FacetsCompiler_t3565032206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2617[12] = 
{
	FacetsCompiler_t3565032206::get_offset_of_datatype_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_derivedRestriction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_baseFlags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_baseFixedFlags_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_validRestrictionFlags_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_nonNegativeInt_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_builtInType_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_builtInEnum_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_firstPattern_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_regStr_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206::get_offset_of_pattern_facet_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FacetsCompiler_t3565032206_StaticFields::get_offset_of_c_map_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (Map_t2552390023)+ sizeof (Il2CppObject), sizeof(Map_t2552390023_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2618[2] = 
{
	Map_t2552390023::get_offset_of_match_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Map_t2552390023::get_offset_of_replacement_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (Numeric10FacetsChecker_t3431899547), -1, sizeof(Numeric10FacetsChecker_t3431899547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[3] = 
{
	Numeric10FacetsChecker_t3431899547_StaticFields::get_offset_of_signs_0(),
	Numeric10FacetsChecker_t3431899547::get_offset_of_maxValue_1(),
	Numeric10FacetsChecker_t3431899547::get_offset_of_minValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (Numeric2FacetsChecker_t2224492892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (DurationFacetsChecker_t4254067383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (DateTimeFacetsChecker_t2220827556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (StringFacetsChecker_t2109036348), -1, sizeof(StringFacetsChecker_t2109036348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2623[1] = 
{
	StringFacetsChecker_t2109036348_StaticFields::get_offset_of_languagePattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (QNameFacetsChecker_t3974186457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (MiscFacetsChecker_t3607863675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (BinaryFacetsChecker_t4115715422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (ListFacetsChecker_t961741019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (UnionFacetsChecker_t3025714558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (NamespaceList_t848177191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[3] = 
{
	NamespaceList_t848177191::get_offset_of_type_0(),
	NamespaceList_t848177191::get_offset_of_set_1(),
	NamespaceList_t848177191::get_offset_of_targetNamespace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (ListType_t2879180877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2631[4] = 
{
	ListType_t2879180877::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (NamespaceListV1Compat_t2783980816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (Parser_t1940171737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[20] = 
{
	Parser_t1940171737::get_offset_of_schemaType_0(),
	Parser_t1940171737::get_offset_of_nameTable_1(),
	Parser_t1940171737::get_offset_of_schemaNames_2(),
	Parser_t1940171737::get_offset_of_eventHandler_3(),
	Parser_t1940171737::get_offset_of_namespaceManager_4(),
	Parser_t1940171737::get_offset_of_reader_5(),
	Parser_t1940171737::get_offset_of_positionInfo_6(),
	Parser_t1940171737::get_offset_of_isProcessNamespaces_7(),
	Parser_t1940171737::get_offset_of_schemaXmlDepth_8(),
	Parser_t1940171737::get_offset_of_markupDepth_9(),
	Parser_t1940171737::get_offset_of_builder_10(),
	Parser_t1940171737::get_offset_of_schema_11(),
	Parser_t1940171737::get_offset_of_xdrSchema_12(),
	Parser_t1940171737::get_offset_of_xmlResolver_13(),
	Parser_t1940171737::get_offset_of_dummyDocument_14(),
	Parser_t1940171737::get_offset_of_processMarkup_15(),
	Parser_t1940171737::get_offset_of_parentNode_16(),
	Parser_t1940171737::get_offset_of_annotationNSManager_17(),
	Parser_t1940171737::get_offset_of_xmlns_18(),
	Parser_t1940171737::get_offset_of_xmlCharType_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (Compositor_t2211431003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2634[5] = 
{
	Compositor_t2211431003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (Preprocessor_t4137165425), -1, sizeof(Preprocessor_t4137165425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2635[1] = 
{
	Preprocessor_t4137165425_StaticFields::get_offset_of_builtInSchemaForXmlNS_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (SchemaAttDef_t1510907267), -1, sizeof(SchemaAttDef_t1510907267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2636[8] = 
{
	SchemaAttDef_t1510907267::get_offset_of_defExpanded_11(),
	SchemaAttDef_t1510907267::get_offset_of_lineNum_12(),
	SchemaAttDef_t1510907267::get_offset_of_linePos_13(),
	SchemaAttDef_t1510907267::get_offset_of_valueLineNum_14(),
	SchemaAttDef_t1510907267::get_offset_of_valueLinePos_15(),
	SchemaAttDef_t1510907267::get_offset_of_reserved_16(),
	SchemaAttDef_t1510907267::get_offset_of_schemaAttribute_17(),
	SchemaAttDef_t1510907267_StaticFields::get_offset_of_Empty_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (Reserve_t2330987269)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2637[4] = 
{
	Reserve_t2330987269::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (SchemaBuilder_t908297946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (SchemaCollectionCompiler_t1443051254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[4] = 
{
	SchemaCollectionCompiler_t1443051254::get_offset_of_compileContentModel_6(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_examplars_7(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_complexTypeStack_8(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_schema_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (SchemaCollectionPreprocessor_t2028437652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[11] = 
{
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_schema_6(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_targetNamespace_7(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_buildinIncluded_8(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_elementFormDefault_9(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_attributeFormDefault_10(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_blockDefault_11(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_finalDefault_12(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_schemaLocations_13(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_referenceNamespaces_14(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_Xmlns_15(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_xmlResolver_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (Compositor_t3622502717)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2641[4] = 
{
	Compositor_t3622502717::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (SchemaDeclBase_t797759480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[11] = 
{
	SchemaDeclBase_t797759480::get_offset_of_name_0(),
	SchemaDeclBase_t797759480::get_offset_of_prefix_1(),
	SchemaDeclBase_t797759480::get_offset_of_isDeclaredInExternal_2(),
	SchemaDeclBase_t797759480::get_offset_of_presence_3(),
	SchemaDeclBase_t797759480::get_offset_of_schemaType_4(),
	SchemaDeclBase_t797759480::get_offset_of_datatype_5(),
	SchemaDeclBase_t797759480::get_offset_of_defaultValueRaw_6(),
	SchemaDeclBase_t797759480::get_offset_of_defaultValueTyped_7(),
	SchemaDeclBase_t797759480::get_offset_of_maxLength_8(),
	SchemaDeclBase_t797759480::get_offset_of_minLength_9(),
	SchemaDeclBase_t797759480::get_offset_of_values_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (Use_t2612116671)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2643[6] = 
{
	Use_t2612116671::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (SchemaElementDecl_t1940851905), -1, sizeof(SchemaElementDecl_t1940851905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2644[15] = 
{
	SchemaElementDecl_t1940851905::get_offset_of_attdefs_11(),
	SchemaElementDecl_t1940851905::get_offset_of_defaultAttdefs_12(),
	SchemaElementDecl_t1940851905::get_offset_of_isIdDeclared_13(),
	SchemaElementDecl_t1940851905::get_offset_of_hasNonCDataAttribute_14(),
	SchemaElementDecl_t1940851905::get_offset_of_isAbstract_15(),
	SchemaElementDecl_t1940851905::get_offset_of_isNillable_16(),
	SchemaElementDecl_t1940851905::get_offset_of_hasRequiredAttribute_17(),
	SchemaElementDecl_t1940851905::get_offset_of_isNotationDeclared_18(),
	SchemaElementDecl_t1940851905::get_offset_of_prohibitedAttributes_19(),
	SchemaElementDecl_t1940851905::get_offset_of_contentValidator_20(),
	SchemaElementDecl_t1940851905::get_offset_of_anyAttribute_21(),
	SchemaElementDecl_t1940851905::get_offset_of_block_22(),
	SchemaElementDecl_t1940851905::get_offset_of_constraints_23(),
	SchemaElementDecl_t1940851905::get_offset_of_schemaElement_24(),
	SchemaElementDecl_t1940851905_StaticFields::get_offset_of_Empty_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (SchemaEntity_t980649128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[13] = 
{
	SchemaEntity_t980649128::get_offset_of_qname_0(),
	SchemaEntity_t980649128::get_offset_of_url_1(),
	SchemaEntity_t980649128::get_offset_of_pubid_2(),
	SchemaEntity_t980649128::get_offset_of_text_3(),
	SchemaEntity_t980649128::get_offset_of_ndata_4(),
	SchemaEntity_t980649128::get_offset_of_lineNumber_5(),
	SchemaEntity_t980649128::get_offset_of_linePosition_6(),
	SchemaEntity_t980649128::get_offset_of_isParameter_7(),
	SchemaEntity_t980649128::get_offset_of_isExternal_8(),
	SchemaEntity_t980649128::get_offset_of_parsingInProgress_9(),
	SchemaEntity_t980649128::get_offset_of_isDeclaredInExternal_10(),
	SchemaEntity_t980649128::get_offset_of_baseURI_11(),
	SchemaEntity_t980649128::get_offset_of_declaredURI_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (AttributeMatchState_t2850601114)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2646[11] = 
{
	AttributeMatchState_t2850601114::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (SchemaInfo_t87206461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[14] = 
{
	SchemaInfo_t87206461::get_offset_of_elementDecls_0(),
	SchemaInfo_t87206461::get_offset_of_undeclaredElementDecls_1(),
	SchemaInfo_t87206461::get_offset_of_generalEntities_2(),
	SchemaInfo_t87206461::get_offset_of_parameterEntities_3(),
	SchemaInfo_t87206461::get_offset_of_docTypeName_4(),
	SchemaInfo_t87206461::get_offset_of_internalDtdSubset_5(),
	SchemaInfo_t87206461::get_offset_of_hasNonCDataAttributes_6(),
	SchemaInfo_t87206461::get_offset_of_hasDefaultAttributes_7(),
	SchemaInfo_t87206461::get_offset_of_targetNamespaces_8(),
	SchemaInfo_t87206461::get_offset_of_attributeDecls_9(),
	SchemaInfo_t87206461::get_offset_of_errorCount_10(),
	SchemaInfo_t87206461::get_offset_of_schemaType_11(),
	SchemaInfo_t87206461::get_offset_of_elementDeclsByType_12(),
	SchemaInfo_t87206461::get_offset_of_notations_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (SchemaNames_t1619962557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[141] = 
{
	SchemaNames_t1619962557::get_offset_of_nameTable_0(),
	SchemaNames_t1619962557::get_offset_of_NsDataType_1(),
	SchemaNames_t1619962557::get_offset_of_NsDataTypeAlias_2(),
	SchemaNames_t1619962557::get_offset_of_NsDataTypeOld_3(),
	SchemaNames_t1619962557::get_offset_of_NsXml_4(),
	SchemaNames_t1619962557::get_offset_of_NsXmlNs_5(),
	SchemaNames_t1619962557::get_offset_of_NsXdr_6(),
	SchemaNames_t1619962557::get_offset_of_NsXdrAlias_7(),
	SchemaNames_t1619962557::get_offset_of_NsXs_8(),
	SchemaNames_t1619962557::get_offset_of_NsXsi_9(),
	SchemaNames_t1619962557::get_offset_of_XsiType_10(),
	SchemaNames_t1619962557::get_offset_of_XsiNil_11(),
	SchemaNames_t1619962557::get_offset_of_XsiSchemaLocation_12(),
	SchemaNames_t1619962557::get_offset_of_XsiNoNamespaceSchemaLocation_13(),
	SchemaNames_t1619962557::get_offset_of_XsdSchema_14(),
	SchemaNames_t1619962557::get_offset_of_XdrSchema_15(),
	SchemaNames_t1619962557::get_offset_of_QnPCData_16(),
	SchemaNames_t1619962557::get_offset_of_QnXml_17(),
	SchemaNames_t1619962557::get_offset_of_QnXmlNs_18(),
	SchemaNames_t1619962557::get_offset_of_QnDtDt_19(),
	SchemaNames_t1619962557::get_offset_of_QnXmlLang_20(),
	SchemaNames_t1619962557::get_offset_of_QnName_21(),
	SchemaNames_t1619962557::get_offset_of_QnType_22(),
	SchemaNames_t1619962557::get_offset_of_QnMaxOccurs_23(),
	SchemaNames_t1619962557::get_offset_of_QnMinOccurs_24(),
	SchemaNames_t1619962557::get_offset_of_QnInfinite_25(),
	SchemaNames_t1619962557::get_offset_of_QnModel_26(),
	SchemaNames_t1619962557::get_offset_of_QnOpen_27(),
	SchemaNames_t1619962557::get_offset_of_QnClosed_28(),
	SchemaNames_t1619962557::get_offset_of_QnContent_29(),
	SchemaNames_t1619962557::get_offset_of_QnMixed_30(),
	SchemaNames_t1619962557::get_offset_of_QnEmpty_31(),
	SchemaNames_t1619962557::get_offset_of_QnEltOnly_32(),
	SchemaNames_t1619962557::get_offset_of_QnTextOnly_33(),
	SchemaNames_t1619962557::get_offset_of_QnOrder_34(),
	SchemaNames_t1619962557::get_offset_of_QnSeq_35(),
	SchemaNames_t1619962557::get_offset_of_QnOne_36(),
	SchemaNames_t1619962557::get_offset_of_QnMany_37(),
	SchemaNames_t1619962557::get_offset_of_QnRequired_38(),
	SchemaNames_t1619962557::get_offset_of_QnYes_39(),
	SchemaNames_t1619962557::get_offset_of_QnNo_40(),
	SchemaNames_t1619962557::get_offset_of_QnString_41(),
	SchemaNames_t1619962557::get_offset_of_QnID_42(),
	SchemaNames_t1619962557::get_offset_of_QnIDRef_43(),
	SchemaNames_t1619962557::get_offset_of_QnIDRefs_44(),
	SchemaNames_t1619962557::get_offset_of_QnEntity_45(),
	SchemaNames_t1619962557::get_offset_of_QnEntities_46(),
	SchemaNames_t1619962557::get_offset_of_QnNmToken_47(),
	SchemaNames_t1619962557::get_offset_of_QnNmTokens_48(),
	SchemaNames_t1619962557::get_offset_of_QnEnumeration_49(),
	SchemaNames_t1619962557::get_offset_of_QnDefault_50(),
	SchemaNames_t1619962557::get_offset_of_QnXdrSchema_51(),
	SchemaNames_t1619962557::get_offset_of_QnXdrElementType_52(),
	SchemaNames_t1619962557::get_offset_of_QnXdrElement_53(),
	SchemaNames_t1619962557::get_offset_of_QnXdrGroup_54(),
	SchemaNames_t1619962557::get_offset_of_QnXdrAttributeType_55(),
	SchemaNames_t1619962557::get_offset_of_QnXdrAttribute_56(),
	SchemaNames_t1619962557::get_offset_of_QnXdrDataType_57(),
	SchemaNames_t1619962557::get_offset_of_QnXdrDescription_58(),
	SchemaNames_t1619962557::get_offset_of_QnXdrExtends_59(),
	SchemaNames_t1619962557::get_offset_of_QnXdrAliasSchema_60(),
	SchemaNames_t1619962557::get_offset_of_QnDtType_61(),
	SchemaNames_t1619962557::get_offset_of_QnDtValues_62(),
	SchemaNames_t1619962557::get_offset_of_QnDtMaxLength_63(),
	SchemaNames_t1619962557::get_offset_of_QnDtMinLength_64(),
	SchemaNames_t1619962557::get_offset_of_QnDtMax_65(),
	SchemaNames_t1619962557::get_offset_of_QnDtMin_66(),
	SchemaNames_t1619962557::get_offset_of_QnDtMinExclusive_67(),
	SchemaNames_t1619962557::get_offset_of_QnDtMaxExclusive_68(),
	SchemaNames_t1619962557::get_offset_of_QnTargetNamespace_69(),
	SchemaNames_t1619962557::get_offset_of_QnVersion_70(),
	SchemaNames_t1619962557::get_offset_of_QnFinalDefault_71(),
	SchemaNames_t1619962557::get_offset_of_QnBlockDefault_72(),
	SchemaNames_t1619962557::get_offset_of_QnFixed_73(),
	SchemaNames_t1619962557::get_offset_of_QnAbstract_74(),
	SchemaNames_t1619962557::get_offset_of_QnBlock_75(),
	SchemaNames_t1619962557::get_offset_of_QnSubstitutionGroup_76(),
	SchemaNames_t1619962557::get_offset_of_QnFinal_77(),
	SchemaNames_t1619962557::get_offset_of_QnNillable_78(),
	SchemaNames_t1619962557::get_offset_of_QnRef_79(),
	SchemaNames_t1619962557::get_offset_of_QnBase_80(),
	SchemaNames_t1619962557::get_offset_of_QnDerivedBy_81(),
	SchemaNames_t1619962557::get_offset_of_QnNamespace_82(),
	SchemaNames_t1619962557::get_offset_of_QnProcessContents_83(),
	SchemaNames_t1619962557::get_offset_of_QnRefer_84(),
	SchemaNames_t1619962557::get_offset_of_QnPublic_85(),
	SchemaNames_t1619962557::get_offset_of_QnSystem_86(),
	SchemaNames_t1619962557::get_offset_of_QnSchemaLocation_87(),
	SchemaNames_t1619962557::get_offset_of_QnValue_88(),
	SchemaNames_t1619962557::get_offset_of_QnUse_89(),
	SchemaNames_t1619962557::get_offset_of_QnForm_90(),
	SchemaNames_t1619962557::get_offset_of_QnElementFormDefault_91(),
	SchemaNames_t1619962557::get_offset_of_QnAttributeFormDefault_92(),
	SchemaNames_t1619962557::get_offset_of_QnItemType_93(),
	SchemaNames_t1619962557::get_offset_of_QnMemberTypes_94(),
	SchemaNames_t1619962557::get_offset_of_QnXPath_95(),
	SchemaNames_t1619962557::get_offset_of_QnXsdSchema_96(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAnnotation_97(),
	SchemaNames_t1619962557::get_offset_of_QnXsdInclude_98(),
	SchemaNames_t1619962557::get_offset_of_QnXsdImport_99(),
	SchemaNames_t1619962557::get_offset_of_QnXsdElement_100(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAttribute_101(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAttributeGroup_102(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAnyAttribute_103(),
	SchemaNames_t1619962557::get_offset_of_QnXsdGroup_104(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAll_105(),
	SchemaNames_t1619962557::get_offset_of_QnXsdChoice_106(),
	SchemaNames_t1619962557::get_offset_of_QnXsdSequence_107(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAny_108(),
	SchemaNames_t1619962557::get_offset_of_QnXsdNotation_109(),
	SchemaNames_t1619962557::get_offset_of_QnXsdSimpleType_110(),
	SchemaNames_t1619962557::get_offset_of_QnXsdComplexType_111(),
	SchemaNames_t1619962557::get_offset_of_QnXsdUnique_112(),
	SchemaNames_t1619962557::get_offset_of_QnXsdKey_113(),
	SchemaNames_t1619962557::get_offset_of_QnXsdKeyRef_114(),
	SchemaNames_t1619962557::get_offset_of_QnXsdSelector_115(),
	SchemaNames_t1619962557::get_offset_of_QnXsdField_116(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMinExclusive_117(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMinInclusive_118(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMaxInclusive_119(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMaxExclusive_120(),
	SchemaNames_t1619962557::get_offset_of_QnXsdTotalDigits_121(),
	SchemaNames_t1619962557::get_offset_of_QnXsdFractionDigits_122(),
	SchemaNames_t1619962557::get_offset_of_QnXsdLength_123(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMinLength_124(),
	SchemaNames_t1619962557::get_offset_of_QnXsdMaxLength_125(),
	SchemaNames_t1619962557::get_offset_of_QnXsdEnumeration_126(),
	SchemaNames_t1619962557::get_offset_of_QnXsdPattern_127(),
	SchemaNames_t1619962557::get_offset_of_QnXsdDocumentation_128(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAppinfo_129(),
	SchemaNames_t1619962557::get_offset_of_QnSource_130(),
	SchemaNames_t1619962557::get_offset_of_QnXsdComplexContent_131(),
	SchemaNames_t1619962557::get_offset_of_QnXsdSimpleContent_132(),
	SchemaNames_t1619962557::get_offset_of_QnXsdRestriction_133(),
	SchemaNames_t1619962557::get_offset_of_QnXsdExtension_134(),
	SchemaNames_t1619962557::get_offset_of_QnXsdUnion_135(),
	SchemaNames_t1619962557::get_offset_of_QnXsdList_136(),
	SchemaNames_t1619962557::get_offset_of_QnXsdWhiteSpace_137(),
	SchemaNames_t1619962557::get_offset_of_QnXsdRedefine_138(),
	SchemaNames_t1619962557::get_offset_of_QnXsdAnyType_139(),
	SchemaNames_t1619962557::get_offset_of_TokenToQName_140(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (Token_t1005517746)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2649[124] = 
{
	Token_t1005517746::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (SchemaNamespaceManager_t3179848951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[1] = 
{
	SchemaNamespaceManager_t3179848951::get_offset_of_node_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (SchemaNotation_t2083484095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[3] = 
{
	SchemaNotation_t2083484095::get_offset_of_name_0(),
	SchemaNotation_t2083484095::get_offset_of_systemLiteral_1(),
	SchemaNotation_t2083484095::get_offset_of_pubid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (Compiler_t2139734799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[13] = 
{
	Compiler_t2139734799::get_offset_of_restrictionErrorMsg_6(),
	Compiler_t2139734799::get_offset_of_attributes_7(),
	Compiler_t2139734799::get_offset_of_attributeGroups_8(),
	Compiler_t2139734799::get_offset_of_elements_9(),
	Compiler_t2139734799::get_offset_of_schemaTypes_10(),
	Compiler_t2139734799::get_offset_of_groups_11(),
	Compiler_t2139734799::get_offset_of_notations_12(),
	Compiler_t2139734799::get_offset_of_examplars_13(),
	Compiler_t2139734799::get_offset_of_identityConstraints_14(),
	Compiler_t2139734799::get_offset_of_complexTypeStack_15(),
	Compiler_t2139734799::get_offset_of_schemasToCompile_16(),
	Compiler_t2139734799::get_offset_of_importedSchemas_17(),
	Compiler_t2139734799::get_offset_of_schemaForSchema_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (SchemaType_t3522160305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2653[5] = 
{
	SchemaType_t3522160305::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (ValidationEventArgs_t1577905814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[2] = 
{
	ValidationEventArgs_t1577905814::get_offset_of_ex_1(),
	ValidationEventArgs_t1577905814::get_offset_of_severity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (ValidationEventHandler_t1580700381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (StateUnion_t1440994946)+ sizeof (Il2CppObject), sizeof(StateUnion_t1440994946 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[4] = 
{
	StateUnion_t1440994946::get_offset_of_State_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StateUnion_t1440994946::get_offset_of_AllElementsRequired_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StateUnion_t1440994946::get_offset_of_CurPosIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StateUnion_t1440994946::get_offset_of_NumberOfRunningPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (ValidationState_t3143048826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[13] = 
{
	ValidationState_t3143048826::get_offset_of_IsNill_0(),
	ValidationState_t3143048826::get_offset_of_NeedValidateChildren_1(),
	ValidationState_t3143048826::get_offset_of_ProcessContents_2(),
	ValidationState_t3143048826::get_offset_of_ElementDecl_3(),
	ValidationState_t3143048826::get_offset_of_LocalName_4(),
	ValidationState_t3143048826::get_offset_of_Namespace_5(),
	ValidationState_t3143048826::get_offset_of_Constr_6(),
	ValidationState_t3143048826::get_offset_of_CurrentState_7(),
	ValidationState_t3143048826::get_offset_of_HasMatched_8(),
	ValidationState_t3143048826::get_offset_of_CurPos_9(),
	ValidationState_t3143048826::get_offset_of_AllElementsSet_10(),
	ValidationState_t3143048826::get_offset_of_RunningPositions_11(),
	ValidationState_t3143048826::get_offset_of_TooComplex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (XdrBuilder_t1520335019), -1, sizeof(XdrBuilder_t1520335019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2658[37] = 
{
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Root_Element_0(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Root_SubElements_1(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_ElementType_SubElements_2(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_AttributeType_SubElements_3(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Group_SubElements_4(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Root_Attributes_5(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_ElementType_Attributes_6(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_AttributeType_Attributes_7(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Element_Attributes_8(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Attribute_Attributes_9(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_Group_Attributes_10(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_ElementDataType_Attributes_11(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_XDR_AttributeDataType_Attributes_12(),
	XdrBuilder_t1520335019_StaticFields::get_offset_of_S_SchemaEntries_13(),
	XdrBuilder_t1520335019::get_offset_of__SchemaInfo_14(),
	XdrBuilder_t1520335019::get_offset_of__TargetNamespace_15(),
	XdrBuilder_t1520335019::get_offset_of__reader_16(),
	XdrBuilder_t1520335019::get_offset_of_positionInfo_17(),
	XdrBuilder_t1520335019::get_offset_of__contentValidator_18(),
	XdrBuilder_t1520335019::get_offset_of__CurState_19(),
	XdrBuilder_t1520335019::get_offset_of__NextState_20(),
	XdrBuilder_t1520335019::get_offset_of__StateHistory_21(),
	XdrBuilder_t1520335019::get_offset_of__GroupStack_22(),
	XdrBuilder_t1520335019::get_offset_of__XdrName_23(),
	XdrBuilder_t1520335019::get_offset_of__XdrPrefix_24(),
	XdrBuilder_t1520335019::get_offset_of__ElementDef_25(),
	XdrBuilder_t1520335019::get_offset_of__GroupDef_26(),
	XdrBuilder_t1520335019::get_offset_of__AttributeDef_27(),
	XdrBuilder_t1520335019::get_offset_of__UndefinedAttributeTypes_28(),
	XdrBuilder_t1520335019::get_offset_of__BaseDecl_29(),
	XdrBuilder_t1520335019::get_offset_of__NameTable_30(),
	XdrBuilder_t1520335019::get_offset_of__SchemaNames_31(),
	XdrBuilder_t1520335019::get_offset_of__CurNsMgr_32(),
	XdrBuilder_t1520335019::get_offset_of__Text_33(),
	XdrBuilder_t1520335019::get_offset_of_validationEventHandler_34(),
	XdrBuilder_t1520335019::get_offset_of__UndeclaredElements_35(),
	XdrBuilder_t1520335019::get_offset_of_xmlResolver_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (DeclBaseInfo_t1711216790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[12] = 
{
	DeclBaseInfo_t1711216790::get_offset_of__Name_0(),
	DeclBaseInfo_t1711216790::get_offset_of__Prefix_1(),
	DeclBaseInfo_t1711216790::get_offset_of__TypeName_2(),
	DeclBaseInfo_t1711216790::get_offset_of__TypePrefix_3(),
	DeclBaseInfo_t1711216790::get_offset_of__Default_4(),
	DeclBaseInfo_t1711216790::get_offset_of__Revises_5(),
	DeclBaseInfo_t1711216790::get_offset_of__MaxOccurs_6(),
	DeclBaseInfo_t1711216790::get_offset_of__MinOccurs_7(),
	DeclBaseInfo_t1711216790::get_offset_of__Checking_8(),
	DeclBaseInfo_t1711216790::get_offset_of__ElementDecl_9(),
	DeclBaseInfo_t1711216790::get_offset_of__Attdef_10(),
	DeclBaseInfo_t1711216790::get_offset_of__Next_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (GroupContent_t1687431939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[5] = 
{
	GroupContent_t1687431939::get_offset_of__MinVal_0(),
	GroupContent_t1687431939::get_offset_of__MaxVal_1(),
	GroupContent_t1687431939::get_offset_of__HasMaxAttr_2(),
	GroupContent_t1687431939::get_offset_of__HasMinAttr_3(),
	GroupContent_t1687431939::get_offset_of__Order_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (ElementContent_t3043903854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[14] = 
{
	ElementContent_t3043903854::get_offset_of__ElementDecl_0(),
	ElementContent_t3043903854::get_offset_of__ContentAttr_1(),
	ElementContent_t3043903854::get_offset_of__OrderAttr_2(),
	ElementContent_t3043903854::get_offset_of__MasterGroupRequired_3(),
	ElementContent_t3043903854::get_offset_of__ExistTerminal_4(),
	ElementContent_t3043903854::get_offset_of__AllowDataType_5(),
	ElementContent_t3043903854::get_offset_of__HasDataType_6(),
	ElementContent_t3043903854::get_offset_of__HasType_7(),
	ElementContent_t3043903854::get_offset_of__EnumerationRequired_8(),
	ElementContent_t3043903854::get_offset_of__MinVal_9(),
	ElementContent_t3043903854::get_offset_of__MaxVal_10(),
	ElementContent_t3043903854::get_offset_of__MaxLength_11(),
	ElementContent_t3043903854::get_offset_of__MinLength_12(),
	ElementContent_t3043903854::get_offset_of__AttDefList_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (AttributeContent_t2961854454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[12] = 
{
	AttributeContent_t2961854454::get_offset_of__AttDef_0(),
	AttributeContent_t2961854454::get_offset_of__Name_1(),
	AttributeContent_t2961854454::get_offset_of__Prefix_2(),
	AttributeContent_t2961854454::get_offset_of__Required_3(),
	AttributeContent_t2961854454::get_offset_of__MinVal_4(),
	AttributeContent_t2961854454::get_offset_of__MaxVal_5(),
	AttributeContent_t2961854454::get_offset_of__MaxLength_6(),
	AttributeContent_t2961854454::get_offset_of__MinLength_7(),
	AttributeContent_t2961854454::get_offset_of__EnumerationRequired_8(),
	AttributeContent_t2961854454::get_offset_of__HasDataType_9(),
	AttributeContent_t2961854454::get_offset_of__Global_10(),
	AttributeContent_t2961854454::get_offset_of__Default_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (XdrBuildFunction_t4291501283), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (XdrInitFunction_t2565742755), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (XdrBeginChildFunction_t218963458), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (XdrEndChildFunction_t4290565954), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (XdrAttributeEntry_t2834798683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[4] = 
{
	XdrAttributeEntry_t2834798683::get_offset_of__Attribute_0(),
	XdrAttributeEntry_t2834798683::get_offset_of__SchemaFlags_1(),
	XdrAttributeEntry_t2834798683::get_offset_of__Datatype_2(),
	XdrAttributeEntry_t2834798683::get_offset_of__BuildFunc_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (XdrEntry_t2813485863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[7] = 
{
	XdrEntry_t2813485863::get_offset_of__Name_0(),
	XdrEntry_t2813485863::get_offset_of__NextStates_1(),
	XdrEntry_t2813485863::get_offset_of__Attributes_2(),
	XdrEntry_t2813485863::get_offset_of__InitFunc_3(),
	XdrEntry_t2813485863::get_offset_of__BeginChildFunc_4(),
	XdrEntry_t2813485863::get_offset_of__EndChildFunc_5(),
	XdrEntry_t2813485863::get_offset_of__AllowText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (XdrValidator_t2966394030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[8] = 
{
	XdrValidator_t2966394030::get_offset_of_validationStack_15(),
	XdrValidator_t2966394030::get_offset_of_attPresence_16(),
	XdrValidator_t2966394030::get_offset_of_name_17(),
	XdrValidator_t2966394030::get_offset_of_nsManager_18(),
	XdrValidator_t2966394030::get_offset_of_isProcessContents_19(),
	XdrValidator_t2966394030::get_offset_of_IDs_20(),
	XdrValidator_t2966394030::get_offset_of_idRefListHead_21(),
	XdrValidator_t2966394030::get_offset_of_inlineSchemaParser_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (XmlAtomicValue_t752869371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[5] = 
{
	XmlAtomicValue_t752869371::get_offset_of_xmlType_0(),
	XmlAtomicValue_t752869371::get_offset_of_objVal_1(),
	XmlAtomicValue_t752869371::get_offset_of_clrType_2(),
	XmlAtomicValue_t752869371::get_offset_of_unionVal_3(),
	XmlAtomicValue_t752869371::get_offset_of_nsPrefix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (Union_t69719246)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[5] = 
{
	Union_t69719246::get_offset_of_boolVal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Union_t69719246::get_offset_of_dblVal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Union_t69719246::get_offset_of_i64Val_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Union_t69719246::get_offset_of_i32Val_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Union_t69719246::get_offset_of_dtVal_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (NamespacePrefixForQName_t3069403619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[2] = 
{
	NamespacePrefixForQName_t3069403619::get_offset_of_prefix_0(),
	NamespacePrefixForQName_t3069403619::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (XmlSchemaAll_t1805755215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[1] = 
{
	XmlSchemaAll_t1805755215::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[3] = 
{
	XmlSchemaAnnotated_t2082486936::get_offset_of_id_6(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_annotation_7(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_moreAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (XmlSchemaAnnotation_t2400301303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[3] = 
{
	XmlSchemaAnnotation_t2400301303::get_offset_of_id_6(),
	XmlSchemaAnnotation_t2400301303::get_offset_of_items_7(),
	XmlSchemaAnnotation_t2400301303::get_offset_of_moreAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (XmlSchemaAnyAttribute_t530453212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[3] = 
{
	XmlSchemaAnyAttribute_t530453212::get_offset_of_ns_9(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_processContents_10(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_namespaceList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (XmlSchemaAny_t3277730824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	XmlSchemaAny_t3277730824::get_offset_of_ns_13(),
	XmlSchemaAny_t3277730824::get_offset_of_processContents_14(),
	XmlSchemaAny_t3277730824::get_offset_of_namespaceList_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (XmlSchemaAppInfo_t2033489551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[2] = 
{
	XmlSchemaAppInfo_t2033489551::get_offset_of_source_6(),
	XmlSchemaAppInfo_t2033489551::get_offset_of_markup_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[11] = 
{
	XmlSchemaAttribute_t4015859774::get_offset_of_defaultValue_9(),
	XmlSchemaAttribute_t4015859774::get_offset_of_fixedValue_10(),
	XmlSchemaAttribute_t4015859774::get_offset_of_name_11(),
	XmlSchemaAttribute_t4015859774::get_offset_of_form_12(),
	XmlSchemaAttribute_t4015859774::get_offset_of_use_13(),
	XmlSchemaAttribute_t4015859774::get_offset_of_refName_14(),
	XmlSchemaAttribute_t4015859774::get_offset_of_typeName_15(),
	XmlSchemaAttribute_t4015859774::get_offset_of_qualifiedName_16(),
	XmlSchemaAttribute_t4015859774::get_offset_of_type_17(),
	XmlSchemaAttribute_t4015859774::get_offset_of_attributeType_18(),
	XmlSchemaAttribute_t4015859774::get_offset_of_attDef_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (XmlSchemaAttributeGroup_t491156493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[8] = 
{
	XmlSchemaAttributeGroup_t491156493::get_offset_of_name_9(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributes_10(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_anyAttribute_11(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_qname_12(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_redefined_13(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributeUses_14(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributeWildcard_15(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_selfReferenceCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (XmlSchemaAttributeGroupRef_t825996660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[1] = 
{
	XmlSchemaAttributeGroupRef_t825996660::get_offset_of_refName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (XmlSchemaChoice_t654568461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	XmlSchemaChoice_t654568461::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (XmlSchemaCollection_t3518500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[8] = 
{
	XmlSchemaCollection_t3518500204::get_offset_of_collection_0(),
	XmlSchemaCollection_t3518500204::get_offset_of_nameTable_1(),
	XmlSchemaCollection_t3518500204::get_offset_of_schemaNames_2(),
	XmlSchemaCollection_t3518500204::get_offset_of_wLock_3(),
	XmlSchemaCollection_t3518500204::get_offset_of_timeout_4(),
	XmlSchemaCollection_t3518500204::get_offset_of_isThreadSafe_5(),
	XmlSchemaCollection_t3518500204::get_offset_of_validationEventHandler_6(),
	XmlSchemaCollection_t3518500204::get_offset_of_xmlResolver_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (XmlSchemaCollectionNode_t3122448512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[3] = 
{
	XmlSchemaCollectionNode_t3122448512::get_offset_of_namespaceUri_0(),
	XmlSchemaCollectionNode_t3122448512::get_offset_of_schemaInfo_1(),
	XmlSchemaCollectionNode_t3122448512::get_offset_of_schema_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (XmlSchemaCollectionEnumerator_t1538181312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[1] = 
{
	XmlSchemaCollectionEnumerator_t1538181312::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (XmlSchemaCompilationSettings_t2971213394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	XmlSchemaCompilationSettings_t2971213394::get_offset_of_enableUpaCheck_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (XmlSchemaComplexContent_t2065934415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	XmlSchemaComplexContent_t2065934415::get_offset_of_content_9(),
	XmlSchemaComplexContent_t2065934415::get_offset_of_isMixed_10(),
	XmlSchemaComplexContent_t2065934415::get_offset_of_hasMixedAttribute_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (XmlSchemaComplexContentExtension_t655218998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[4] = 
{
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_particle_9(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_attributes_10(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_anyAttribute_11(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_baseTypeName_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (XmlSchemaComplexContentRestriction_t1722137421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_particle_9(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_attributes_10(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_anyAttribute_11(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_baseTypeName_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (XmlSchemaComplexType_t4086789226), -1, sizeof(XmlSchemaComplexType_t4086789226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2690[14] = 
{
	XmlSchemaComplexType_t4086789226::get_offset_of_block_19(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentModel_20(),
	XmlSchemaComplexType_t4086789226::get_offset_of_particle_21(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributes_22(),
	XmlSchemaComplexType_t4086789226::get_offset_of_anyAttribute_23(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentTypeParticle_24(),
	XmlSchemaComplexType_t4086789226::get_offset_of_blockResolved_25(),
	XmlSchemaComplexType_t4086789226::get_offset_of_localElements_26(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeUses_27(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeWildcard_28(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_anyTypeLax_29(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_anyTypeSkip_30(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_untypedAnyType_31(),
	XmlSchemaComplexType_t4086789226::get_offset_of_pvFlags_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (XmlSchemaContent_t3733871217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (XmlSchemaContentModel_t907989596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (XmlSchemaContentProcessing_t74226324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[5] = 
{
	XmlSchemaContentProcessing_t74226324::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (XmlSchemaContentType_t2874429441)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[5] = 
{
	XmlSchemaContentType_t2874429441::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (XmlSchema_t880472818), -1, sizeof(XmlSchema_t880472818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2695[26] = 
{
	XmlSchema_t880472818::get_offset_of_attributeFormDefault_6(),
	XmlSchema_t880472818::get_offset_of_elementFormDefault_7(),
	XmlSchema_t880472818::get_offset_of_blockDefault_8(),
	XmlSchema_t880472818::get_offset_of_finalDefault_9(),
	XmlSchema_t880472818::get_offset_of_targetNs_10(),
	XmlSchema_t880472818::get_offset_of_version_11(),
	XmlSchema_t880472818::get_offset_of_includes_12(),
	XmlSchema_t880472818::get_offset_of_items_13(),
	XmlSchema_t880472818::get_offset_of_id_14(),
	XmlSchema_t880472818::get_offset_of_moreAttributes_15(),
	XmlSchema_t880472818::get_offset_of_isCompiled_16(),
	XmlSchema_t880472818::get_offset_of_isCompiledBySet_17(),
	XmlSchema_t880472818::get_offset_of_isPreprocessed_18(),
	XmlSchema_t880472818::get_offset_of_errorCount_19(),
	XmlSchema_t880472818::get_offset_of_attributes_20(),
	XmlSchema_t880472818::get_offset_of_attributeGroups_21(),
	XmlSchema_t880472818::get_offset_of_elements_22(),
	XmlSchema_t880472818::get_offset_of_types_23(),
	XmlSchema_t880472818::get_offset_of_groups_24(),
	XmlSchema_t880472818::get_offset_of_notations_25(),
	XmlSchema_t880472818::get_offset_of_identityConstraints_26(),
	XmlSchema_t880472818_StaticFields::get_offset_of_globalIdCounter_27(),
	XmlSchema_t880472818::get_offset_of_schemaId_28(),
	XmlSchema_t880472818::get_offset_of_baseUri_29(),
	XmlSchema_t880472818::get_offset_of_ids_30(),
	XmlSchema_t880472818::get_offset_of_document_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (XmlSchemaDatatype_t1195946242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2697[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (XmlSchemaDocumentation_t3832803992), -1, sizeof(XmlSchemaDocumentation_t3832803992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2698[4] = 
{
	XmlSchemaDocumentation_t3832803992::get_offset_of_source_6(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_language_7(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_markup_8(),
	XmlSchemaDocumentation_t3832803992_StaticFields::get_offset_of_languageType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[21] = 
{
	XmlSchemaElement_t2433337156::get_offset_of_isAbstract_13(),
	XmlSchemaElement_t2433337156::get_offset_of_hasAbstractAttribute_14(),
	XmlSchemaElement_t2433337156::get_offset_of_isNillable_15(),
	XmlSchemaElement_t2433337156::get_offset_of_hasNillableAttribute_16(),
	XmlSchemaElement_t2433337156::get_offset_of_isLocalTypeDerivationChecked_17(),
	XmlSchemaElement_t2433337156::get_offset_of_block_18(),
	XmlSchemaElement_t2433337156::get_offset_of_final_19(),
	XmlSchemaElement_t2433337156::get_offset_of_form_20(),
	XmlSchemaElement_t2433337156::get_offset_of_defaultValue_21(),
	XmlSchemaElement_t2433337156::get_offset_of_fixedValue_22(),
	XmlSchemaElement_t2433337156::get_offset_of_name_23(),
	XmlSchemaElement_t2433337156::get_offset_of_refName_24(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutionGroup_25(),
	XmlSchemaElement_t2433337156::get_offset_of_typeName_26(),
	XmlSchemaElement_t2433337156::get_offset_of_type_27(),
	XmlSchemaElement_t2433337156::get_offset_of_qualifiedName_28(),
	XmlSchemaElement_t2433337156::get_offset_of_elementType_29(),
	XmlSchemaElement_t2433337156::get_offset_of_blockResolved_30(),
	XmlSchemaElement_t2433337156::get_offset_of_finalResolved_31(),
	XmlSchemaElement_t2433337156::get_offset_of_constraints_32(),
	XmlSchemaElement_t2433337156::get_offset_of_elementDecl_33(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
