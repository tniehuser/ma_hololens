﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidCollection
struct  OidCollection_t3790243618  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::m_list
	ArrayList_t4252133567 * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(OidCollection_t3790243618, ___m_list_0)); }
	inline ArrayList_t4252133567 * get_m_list_0() const { return ___m_list_0; }
	inline ArrayList_t4252133567 ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(ArrayList_t4252133567 * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_list_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
