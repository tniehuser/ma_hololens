﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage
struct  BinaryMethodReturnMessage_t3642132829  : public Il2CppObject
{
public:
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_outargs
	ObjectU5BU5D_t3614634134* ____outargs_0;
	// System.Exception System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_exception
	Exception_t1927440687 * ____exception_1;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_returnValue
	Il2CppObject * ____returnValue_2;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_args
	ObjectU5BU5D_t3614634134* ____args_3;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_logicalCallContext
	LogicalCallContext_t725724420 * ____logicalCallContext_4;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturnMessage::_properties
	ObjectU5BU5D_t3614634134* ____properties_5;

public:
	inline static int32_t get_offset_of__outargs_0() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____outargs_0)); }
	inline ObjectU5BU5D_t3614634134* get__outargs_0() const { return ____outargs_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of__outargs_0() { return &____outargs_0; }
	inline void set__outargs_0(ObjectU5BU5D_t3614634134* value)
	{
		____outargs_0 = value;
		Il2CppCodeGenWriteBarrier(&____outargs_0, value);
	}

	inline static int32_t get_offset_of__exception_1() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____exception_1)); }
	inline Exception_t1927440687 * get__exception_1() const { return ____exception_1; }
	inline Exception_t1927440687 ** get_address_of__exception_1() { return &____exception_1; }
	inline void set__exception_1(Exception_t1927440687 * value)
	{
		____exception_1 = value;
		Il2CppCodeGenWriteBarrier(&____exception_1, value);
	}

	inline static int32_t get_offset_of__returnValue_2() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____returnValue_2)); }
	inline Il2CppObject * get__returnValue_2() const { return ____returnValue_2; }
	inline Il2CppObject ** get_address_of__returnValue_2() { return &____returnValue_2; }
	inline void set__returnValue_2(Il2CppObject * value)
	{
		____returnValue_2 = value;
		Il2CppCodeGenWriteBarrier(&____returnValue_2, value);
	}

	inline static int32_t get_offset_of__args_3() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____args_3)); }
	inline ObjectU5BU5D_t3614634134* get__args_3() const { return ____args_3; }
	inline ObjectU5BU5D_t3614634134** get_address_of__args_3() { return &____args_3; }
	inline void set__args_3(ObjectU5BU5D_t3614634134* value)
	{
		____args_3 = value;
		Il2CppCodeGenWriteBarrier(&____args_3, value);
	}

	inline static int32_t get_offset_of__logicalCallContext_4() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____logicalCallContext_4)); }
	inline LogicalCallContext_t725724420 * get__logicalCallContext_4() const { return ____logicalCallContext_4; }
	inline LogicalCallContext_t725724420 ** get_address_of__logicalCallContext_4() { return &____logicalCallContext_4; }
	inline void set__logicalCallContext_4(LogicalCallContext_t725724420 * value)
	{
		____logicalCallContext_4 = value;
		Il2CppCodeGenWriteBarrier(&____logicalCallContext_4, value);
	}

	inline static int32_t get_offset_of__properties_5() { return static_cast<int32_t>(offsetof(BinaryMethodReturnMessage_t3642132829, ____properties_5)); }
	inline ObjectU5BU5D_t3614634134* get__properties_5() const { return ____properties_5; }
	inline ObjectU5BU5D_t3614634134** get_address_of__properties_5() { return &____properties_5; }
	inline void set__properties_5(ObjectU5BU5D_t3614634134* value)
	{
		____properties_5 = value;
		Il2CppCodeGenWriteBarrier(&____properties_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
