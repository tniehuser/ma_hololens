﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.CredentialCache
struct CredentialCache_t1992799279;
// System.Net.ICredentials[]
struct ICredentialsU5BU5D_t196403748;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialCache/CredentialEnumerator
struct  CredentialEnumerator_t3600195683  : public Il2CppObject
{
public:
	// System.Net.CredentialCache System.Net.CredentialCache/CredentialEnumerator::m_cache
	CredentialCache_t1992799279 * ___m_cache_0;
	// System.Net.ICredentials[] System.Net.CredentialCache/CredentialEnumerator::m_array
	ICredentialsU5BU5D_t196403748* ___m_array_1;
	// System.Int32 System.Net.CredentialCache/CredentialEnumerator::m_index
	int32_t ___m_index_2;
	// System.Int32 System.Net.CredentialCache/CredentialEnumerator::m_version
	int32_t ___m_version_3;

public:
	inline static int32_t get_offset_of_m_cache_0() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_cache_0)); }
	inline CredentialCache_t1992799279 * get_m_cache_0() const { return ___m_cache_0; }
	inline CredentialCache_t1992799279 ** get_address_of_m_cache_0() { return &___m_cache_0; }
	inline void set_m_cache_0(CredentialCache_t1992799279 * value)
	{
		___m_cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_cache_0, value);
	}

	inline static int32_t get_offset_of_m_array_1() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_array_1)); }
	inline ICredentialsU5BU5D_t196403748* get_m_array_1() const { return ___m_array_1; }
	inline ICredentialsU5BU5D_t196403748** get_address_of_m_array_1() { return &___m_array_1; }
	inline void set_m_array_1(ICredentialsU5BU5D_t196403748* value)
	{
		___m_array_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_array_1, value);
	}

	inline static int32_t get_offset_of_m_index_2() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_index_2)); }
	inline int32_t get_m_index_2() const { return ___m_index_2; }
	inline int32_t* get_address_of_m_index_2() { return &___m_index_2; }
	inline void set_m_index_2(int32_t value)
	{
		___m_index_2 = value;
	}

	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(CredentialEnumerator_t3600195683, ___m_version_3)); }
	inline int32_t get_m_version_3() const { return ___m_version_3; }
	inline int32_t* get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(int32_t value)
	{
		___m_version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
