﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.InteractionSourceLocation
struct  InteractionSourceLocation_t1509183288 
{
public:
	// System.Byte UnityEngine.VR.WSA.Input.InteractionSourceLocation::m_hasPosition
	uint8_t ___m_hasPosition_0;
	// UnityEngine.Vector3 UnityEngine.VR.WSA.Input.InteractionSourceLocation::m_position
	Vector3_t2243707580  ___m_position_1;
	// System.Byte UnityEngine.VR.WSA.Input.InteractionSourceLocation::m_hasVelocity
	uint8_t ___m_hasVelocity_2;
	// UnityEngine.Vector3 UnityEngine.VR.WSA.Input.InteractionSourceLocation::m_velocity
	Vector3_t2243707580  ___m_velocity_3;

public:
	inline static int32_t get_offset_of_m_hasPosition_0() { return static_cast<int32_t>(offsetof(InteractionSourceLocation_t1509183288, ___m_hasPosition_0)); }
	inline uint8_t get_m_hasPosition_0() const { return ___m_hasPosition_0; }
	inline uint8_t* get_address_of_m_hasPosition_0() { return &___m_hasPosition_0; }
	inline void set_m_hasPosition_0(uint8_t value)
	{
		___m_hasPosition_0 = value;
	}

	inline static int32_t get_offset_of_m_position_1() { return static_cast<int32_t>(offsetof(InteractionSourceLocation_t1509183288, ___m_position_1)); }
	inline Vector3_t2243707580  get_m_position_1() const { return ___m_position_1; }
	inline Vector3_t2243707580 * get_address_of_m_position_1() { return &___m_position_1; }
	inline void set_m_position_1(Vector3_t2243707580  value)
	{
		___m_position_1 = value;
	}

	inline static int32_t get_offset_of_m_hasVelocity_2() { return static_cast<int32_t>(offsetof(InteractionSourceLocation_t1509183288, ___m_hasVelocity_2)); }
	inline uint8_t get_m_hasVelocity_2() const { return ___m_hasVelocity_2; }
	inline uint8_t* get_address_of_m_hasVelocity_2() { return &___m_hasVelocity_2; }
	inline void set_m_hasVelocity_2(uint8_t value)
	{
		___m_hasVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_velocity_3() { return static_cast<int32_t>(offsetof(InteractionSourceLocation_t1509183288, ___m_velocity_3)); }
	inline Vector3_t2243707580  get_m_velocity_3() const { return ___m_velocity_3; }
	inline Vector3_t2243707580 * get_address_of_m_velocity_3() { return &___m_velocity_3; }
	inline void set_m_velocity_3(Vector3_t2243707580  value)
	{
		___m_velocity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
