﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Collections.SortedList
struct SortedList_t3004938869;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t2971213394;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSet
struct  XmlSchemaSet_t313318308  : public Il2CppObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaSet::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.Collections.SortedList System.Xml.Schema.XmlSchemaSet::schemas
	SortedList_t3004938869 * ___schemas_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::internalEventHandler
	ValidationEventHandler_t1580700381 * ___internalEventHandler_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_3;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::schemaLocations
	Hashtable_t909839986 * ___schemaLocations_4;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::chameleonSchemas
	Hashtable_t909839986 * ___chameleonSchemas_5;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::targetNamespaces
	Hashtable_t909839986 * ___targetNamespaces_6;
	// System.Boolean System.Xml.Schema.XmlSchemaSet::compileAll
	bool ___compileAll_7;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XmlSchemaSet::cachedCompiledInfo
	SchemaInfo_t87206461 * ___cachedCompiledInfo_8;
	// System.Xml.XmlReaderSettings System.Xml.Schema.XmlSchemaSet::readerSettings
	XmlReaderSettings_t1578612233 * ___readerSettings_9;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.XmlSchemaSet::compilationSettings
	XmlSchemaCompilationSettings_t2971213394 * ___compilationSettings_10;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::substitutionGroups
	XmlSchemaObjectTable_t3364835593 * ___substitutionGroups_11;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_0, value);
	}

	inline static int32_t get_offset_of_schemas_1() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___schemas_1)); }
	inline SortedList_t3004938869 * get_schemas_1() const { return ___schemas_1; }
	inline SortedList_t3004938869 ** get_address_of_schemas_1() { return &___schemas_1; }
	inline void set_schemas_1(SortedList_t3004938869 * value)
	{
		___schemas_1 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_1, value);
	}

	inline static int32_t get_offset_of_internalEventHandler_2() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___internalEventHandler_2)); }
	inline ValidationEventHandler_t1580700381 * get_internalEventHandler_2() const { return ___internalEventHandler_2; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_internalEventHandler_2() { return &___internalEventHandler_2; }
	inline void set_internalEventHandler_2(ValidationEventHandler_t1580700381 * value)
	{
		___internalEventHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___internalEventHandler_2, value);
	}

	inline static int32_t get_offset_of_eventHandler_3() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___eventHandler_3)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_3() const { return ___eventHandler_3; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_3() { return &___eventHandler_3; }
	inline void set_eventHandler_3(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandler_3, value);
	}

	inline static int32_t get_offset_of_schemaLocations_4() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___schemaLocations_4)); }
	inline Hashtable_t909839986 * get_schemaLocations_4() const { return ___schemaLocations_4; }
	inline Hashtable_t909839986 ** get_address_of_schemaLocations_4() { return &___schemaLocations_4; }
	inline void set_schemaLocations_4(Hashtable_t909839986 * value)
	{
		___schemaLocations_4 = value;
		Il2CppCodeGenWriteBarrier(&___schemaLocations_4, value);
	}

	inline static int32_t get_offset_of_chameleonSchemas_5() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___chameleonSchemas_5)); }
	inline Hashtable_t909839986 * get_chameleonSchemas_5() const { return ___chameleonSchemas_5; }
	inline Hashtable_t909839986 ** get_address_of_chameleonSchemas_5() { return &___chameleonSchemas_5; }
	inline void set_chameleonSchemas_5(Hashtable_t909839986 * value)
	{
		___chameleonSchemas_5 = value;
		Il2CppCodeGenWriteBarrier(&___chameleonSchemas_5, value);
	}

	inline static int32_t get_offset_of_targetNamespaces_6() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___targetNamespaces_6)); }
	inline Hashtable_t909839986 * get_targetNamespaces_6() const { return ___targetNamespaces_6; }
	inline Hashtable_t909839986 ** get_address_of_targetNamespaces_6() { return &___targetNamespaces_6; }
	inline void set_targetNamespaces_6(Hashtable_t909839986 * value)
	{
		___targetNamespaces_6 = value;
		Il2CppCodeGenWriteBarrier(&___targetNamespaces_6, value);
	}

	inline static int32_t get_offset_of_compileAll_7() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___compileAll_7)); }
	inline bool get_compileAll_7() const { return ___compileAll_7; }
	inline bool* get_address_of_compileAll_7() { return &___compileAll_7; }
	inline void set_compileAll_7(bool value)
	{
		___compileAll_7 = value;
	}

	inline static int32_t get_offset_of_cachedCompiledInfo_8() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___cachedCompiledInfo_8)); }
	inline SchemaInfo_t87206461 * get_cachedCompiledInfo_8() const { return ___cachedCompiledInfo_8; }
	inline SchemaInfo_t87206461 ** get_address_of_cachedCompiledInfo_8() { return &___cachedCompiledInfo_8; }
	inline void set_cachedCompiledInfo_8(SchemaInfo_t87206461 * value)
	{
		___cachedCompiledInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___cachedCompiledInfo_8, value);
	}

	inline static int32_t get_offset_of_readerSettings_9() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___readerSettings_9)); }
	inline XmlReaderSettings_t1578612233 * get_readerSettings_9() const { return ___readerSettings_9; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_readerSettings_9() { return &___readerSettings_9; }
	inline void set_readerSettings_9(XmlReaderSettings_t1578612233 * value)
	{
		___readerSettings_9 = value;
		Il2CppCodeGenWriteBarrier(&___readerSettings_9, value);
	}

	inline static int32_t get_offset_of_compilationSettings_10() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___compilationSettings_10)); }
	inline XmlSchemaCompilationSettings_t2971213394 * get_compilationSettings_10() const { return ___compilationSettings_10; }
	inline XmlSchemaCompilationSettings_t2971213394 ** get_address_of_compilationSettings_10() { return &___compilationSettings_10; }
	inline void set_compilationSettings_10(XmlSchemaCompilationSettings_t2971213394 * value)
	{
		___compilationSettings_10 = value;
		Il2CppCodeGenWriteBarrier(&___compilationSettings_10, value);
	}

	inline static int32_t get_offset_of_substitutionGroups_11() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___substitutionGroups_11)); }
	inline XmlSchemaObjectTable_t3364835593 * get_substitutionGroups_11() const { return ___substitutionGroups_11; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_substitutionGroups_11() { return &___substitutionGroups_11; }
	inline void set_substitutionGroups_11(XmlSchemaObjectTable_t3364835593 * value)
	{
		___substitutionGroups_11 = value;
		Il2CppCodeGenWriteBarrier(&___substitutionGroups_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
