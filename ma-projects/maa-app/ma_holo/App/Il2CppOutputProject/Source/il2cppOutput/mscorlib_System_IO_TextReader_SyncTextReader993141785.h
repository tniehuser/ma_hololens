﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextReader1561828458.h"

// System.IO.TextReader
struct TextReader_t1561828458;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader/SyncTextReader
struct  SyncTextReader_t993141785  : public TextReader_t1561828458
{
public:
	// System.IO.TextReader System.IO.TextReader/SyncTextReader::_in
	TextReader_t1561828458 * ____in_4;

public:
	inline static int32_t get_offset_of__in_4() { return static_cast<int32_t>(offsetof(SyncTextReader_t993141785, ____in_4)); }
	inline TextReader_t1561828458 * get__in_4() const { return ____in_4; }
	inline TextReader_t1561828458 ** get_address_of__in_4() { return &____in_4; }
	inline void set__in_4(TextReader_t1561828458 * value)
	{
		____in_4 = value;
		Il2CppCodeGenWriteBarrier(&____in_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
