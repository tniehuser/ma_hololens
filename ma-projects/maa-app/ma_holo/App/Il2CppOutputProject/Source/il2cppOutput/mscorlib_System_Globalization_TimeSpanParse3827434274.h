﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp2031659367.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TimeSpanParse
struct  TimeSpanParse_t3827434274  : public Il2CppObject
{
public:

public:
};

struct TimeSpanParse_t3827434274_StaticFields
{
public:
	// System.Globalization.TimeSpanParse/TimeSpanToken System.Globalization.TimeSpanParse::zero
	TimeSpanToken_t2031659367  ___zero_0;

public:
	inline static int32_t get_offset_of_zero_0() { return static_cast<int32_t>(offsetof(TimeSpanParse_t3827434274_StaticFields, ___zero_0)); }
	inline TimeSpanToken_t2031659367  get_zero_0() const { return ___zero_0; }
	inline TimeSpanToken_t2031659367 * get_address_of_zero_0() { return &___zero_0; }
	inline void set_zero_0(TimeSpanToken_t2031659367  value)
	{
		___zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
