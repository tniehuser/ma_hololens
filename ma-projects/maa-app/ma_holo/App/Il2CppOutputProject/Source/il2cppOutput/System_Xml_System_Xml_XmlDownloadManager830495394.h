﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager
struct  XmlDownloadManager_t830495394  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlDownloadManager::connections
	Hashtable_t909839986 * ___connections_0;

public:
	inline static int32_t get_offset_of_connections_0() { return static_cast<int32_t>(offsetof(XmlDownloadManager_t830495394, ___connections_0)); }
	inline Hashtable_t909839986 * get_connections_0() const { return ___connections_0; }
	inline Hashtable_t909839986 ** get_address_of_connections_0() { return &___connections_0; }
	inline void set_connections_0(Hashtable_t909839986 * value)
	{
		___connections_0 = value;
		Il2CppCodeGenWriteBarrier(&___connections_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
