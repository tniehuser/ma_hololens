﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_LeafNode3748718316.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Xml.Schema.BitSet
struct BitSet_t1062448123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LeafRangeNode
struct  LeafRangeNode_t2572019409  : public LeafNode_t3748718316
{
public:
	// System.Decimal System.Xml.Schema.LeafRangeNode::min
	Decimal_t724701077  ___min_1;
	// System.Decimal System.Xml.Schema.LeafRangeNode::max
	Decimal_t724701077  ___max_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.LeafRangeNode::nextIteration
	BitSet_t1062448123 * ___nextIteration_3;

public:
	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___min_1)); }
	inline Decimal_t724701077  get_min_1() const { return ___min_1; }
	inline Decimal_t724701077 * get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(Decimal_t724701077  value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___max_2)); }
	inline Decimal_t724701077  get_max_2() const { return ___max_2; }
	inline Decimal_t724701077 * get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(Decimal_t724701077  value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_nextIteration_3() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___nextIteration_3)); }
	inline BitSet_t1062448123 * get_nextIteration_3() const { return ___nextIteration_3; }
	inline BitSet_t1062448123 ** get_address_of_nextIteration_3() { return &___nextIteration_3; }
	inline void set_nextIteration_3(BitSet_t1062448123 * value)
	{
		___nextIteration_3 = value;
		Il2CppCodeGenWriteBarrier(&___nextIteration_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
