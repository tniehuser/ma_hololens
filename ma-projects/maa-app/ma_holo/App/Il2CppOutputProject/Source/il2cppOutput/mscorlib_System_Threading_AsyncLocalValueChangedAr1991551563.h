﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.AsyncLocalValueChangedArgs`1<System.Globalization.CultureInfo>
struct  AsyncLocalValueChangedArgs_1_t1991551563 
{
public:
	union
	{
		struct
		{
			// T System.Threading.AsyncLocalValueChangedArgs`1::<PreviousValue>k__BackingField
			CultureInfo_t3500843524 * ___U3CPreviousValueU3Ek__BackingField_0;
			// T System.Threading.AsyncLocalValueChangedArgs`1::<CurrentValue>k__BackingField
			CultureInfo_t3500843524 * ___U3CCurrentValueU3Ek__BackingField_1;
			// System.Boolean System.Threading.AsyncLocalValueChangedArgs`1::<ThreadContextChanged>k__BackingField
			bool ___U3CThreadContextChangedU3Ek__BackingField_2;
		};
		uint8_t AsyncLocalValueChangedArgs_1_t3798497170__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CPreviousValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncLocalValueChangedArgs_1_t1991551563, ___U3CPreviousValueU3Ek__BackingField_0)); }
	inline CultureInfo_t3500843524 * get_U3CPreviousValueU3Ek__BackingField_0() const { return ___U3CPreviousValueU3Ek__BackingField_0; }
	inline CultureInfo_t3500843524 ** get_address_of_U3CPreviousValueU3Ek__BackingField_0() { return &___U3CPreviousValueU3Ek__BackingField_0; }
	inline void set_U3CPreviousValueU3Ek__BackingField_0(CultureInfo_t3500843524 * value)
	{
		___U3CPreviousValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPreviousValueU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCurrentValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncLocalValueChangedArgs_1_t1991551563, ___U3CCurrentValueU3Ek__BackingField_1)); }
	inline CultureInfo_t3500843524 * get_U3CCurrentValueU3Ek__BackingField_1() const { return ___U3CCurrentValueU3Ek__BackingField_1; }
	inline CultureInfo_t3500843524 ** get_address_of_U3CCurrentValueU3Ek__BackingField_1() { return &___U3CCurrentValueU3Ek__BackingField_1; }
	inline void set_U3CCurrentValueU3Ek__BackingField_1(CultureInfo_t3500843524 * value)
	{
		___U3CCurrentValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentValueU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CThreadContextChangedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncLocalValueChangedArgs_1_t1991551563, ___U3CThreadContextChangedU3Ek__BackingField_2)); }
	inline bool get_U3CThreadContextChangedU3Ek__BackingField_2() const { return ___U3CThreadContextChangedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CThreadContextChangedU3Ek__BackingField_2() { return &___U3CThreadContextChangedU3Ek__BackingField_2; }
	inline void set_U3CThreadContextChangedU3Ek__BackingField_2(bool value)
	{
		___U3CThreadContextChangedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
