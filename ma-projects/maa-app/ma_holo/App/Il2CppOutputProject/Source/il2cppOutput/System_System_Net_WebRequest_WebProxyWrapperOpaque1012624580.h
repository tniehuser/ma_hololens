﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.WebProxy
struct WebProxy_t1169192840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest/WebProxyWrapperOpaque
struct  WebProxyWrapperOpaque_t1012624580  : public Il2CppObject
{
public:
	// System.Net.WebProxy System.Net.WebRequest/WebProxyWrapperOpaque::webProxy
	WebProxy_t1169192840 * ___webProxy_0;

public:
	inline static int32_t get_offset_of_webProxy_0() { return static_cast<int32_t>(offsetof(WebProxyWrapperOpaque_t1012624580, ___webProxy_0)); }
	inline WebProxy_t1169192840 * get_webProxy_0() const { return ___webProxy_0; }
	inline WebProxy_t1169192840 ** get_address_of_webProxy_0() { return &___webProxy_0; }
	inline void set_webProxy_0(WebProxy_t1169192840 * value)
	{
		___webProxy_0 = value;
		Il2CppCodeGenWriteBarrier(&___webProxy_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
