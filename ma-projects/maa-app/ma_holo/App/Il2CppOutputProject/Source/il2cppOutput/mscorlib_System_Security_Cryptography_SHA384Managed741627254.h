﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_SHA384535510267.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt64[]
struct UInt64U5BU5D_t1668688775;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SHA384Managed
struct  SHA384Managed_t741627254  : public SHA384_t535510267
{
public:
	// System.Byte[] System.Security.Cryptography.SHA384Managed::_buffer
	ByteU5BU5D_t3397334013* ____buffer_4;
	// System.UInt64 System.Security.Cryptography.SHA384Managed::_count
	uint64_t ____count_5;
	// System.UInt64[] System.Security.Cryptography.SHA384Managed::_stateSHA384
	UInt64U5BU5D_t1668688775* ____stateSHA384_6;
	// System.UInt64[] System.Security.Cryptography.SHA384Managed::_W
	UInt64U5BU5D_t1668688775* ____W_7;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(SHA384Managed_t741627254, ____buffer_4)); }
	inline ByteU5BU5D_t3397334013* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_t3397334013* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_4, value);
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(SHA384Managed_t741627254, ____count_5)); }
	inline uint64_t get__count_5() const { return ____count_5; }
	inline uint64_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(uint64_t value)
	{
		____count_5 = value;
	}

	inline static int32_t get_offset_of__stateSHA384_6() { return static_cast<int32_t>(offsetof(SHA384Managed_t741627254, ____stateSHA384_6)); }
	inline UInt64U5BU5D_t1668688775* get__stateSHA384_6() const { return ____stateSHA384_6; }
	inline UInt64U5BU5D_t1668688775** get_address_of__stateSHA384_6() { return &____stateSHA384_6; }
	inline void set__stateSHA384_6(UInt64U5BU5D_t1668688775* value)
	{
		____stateSHA384_6 = value;
		Il2CppCodeGenWriteBarrier(&____stateSHA384_6, value);
	}

	inline static int32_t get_offset_of__W_7() { return static_cast<int32_t>(offsetof(SHA384Managed_t741627254, ____W_7)); }
	inline UInt64U5BU5D_t1668688775* get__W_7() const { return ____W_7; }
	inline UInt64U5BU5D_t1668688775** get_address_of__W_7() { return &____W_7; }
	inline void set__W_7(UInt64U5BU5D_t1668688775* value)
	{
		____W_7 = value;
		Il2CppCodeGenWriteBarrier(&____W_7, value);
	}
};

struct SHA384Managed_t741627254_StaticFields
{
public:
	// System.UInt64[] System.Security.Cryptography.SHA384Managed::_K
	UInt64U5BU5D_t1668688775* ____K_8;

public:
	inline static int32_t get_offset_of__K_8() { return static_cast<int32_t>(offsetof(SHA384Managed_t741627254_StaticFields, ____K_8)); }
	inline UInt64U5BU5D_t1668688775* get__K_8() const { return ____K_8; }
	inline UInt64U5BU5D_t1668688775** get_address_of__K_8() { return &____K_8; }
	inline void set__K_8(UInt64U5BU5D_t1668688775* value)
	{
		____K_8 = value;
		Il2CppCodeGenWriteBarrier(&____K_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
