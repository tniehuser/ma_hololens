﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TypedElement
struct  TypedElement_t4034655484  : public ConfigurationElement_t1776195828
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.TypedElement::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_17;
	// System.Object System.Diagnostics.TypedElement::_runtimeObject
	Il2CppObject * ____runtimeObject_18;
	// System.Type System.Diagnostics.TypedElement::_baseType
	Type_t * ____baseType_19;

public:
	inline static int32_t get_offset_of__properties_17() { return static_cast<int32_t>(offsetof(TypedElement_t4034655484, ____properties_17)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_17() const { return ____properties_17; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_17() { return &____properties_17; }
	inline void set__properties_17(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_17 = value;
		Il2CppCodeGenWriteBarrier(&____properties_17, value);
	}

	inline static int32_t get_offset_of__runtimeObject_18() { return static_cast<int32_t>(offsetof(TypedElement_t4034655484, ____runtimeObject_18)); }
	inline Il2CppObject * get__runtimeObject_18() const { return ____runtimeObject_18; }
	inline Il2CppObject ** get_address_of__runtimeObject_18() { return &____runtimeObject_18; }
	inline void set__runtimeObject_18(Il2CppObject * value)
	{
		____runtimeObject_18 = value;
		Il2CppCodeGenWriteBarrier(&____runtimeObject_18, value);
	}

	inline static int32_t get_offset_of__baseType_19() { return static_cast<int32_t>(offsetof(TypedElement_t4034655484, ____baseType_19)); }
	inline Type_t * get__baseType_19() const { return ____baseType_19; }
	inline Type_t ** get_address_of__baseType_19() { return &____baseType_19; }
	inline void set__baseType_19(Type_t * value)
	{
		____baseType_19 = value;
		Il2CppCodeGenWriteBarrier(&____baseType_19, value);
	}
};

struct TypedElement_t4034655484_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Diagnostics.TypedElement::_propTypeName
	ConfigurationProperty_t2048066811 * ____propTypeName_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.TypedElement::_propInitData
	ConfigurationProperty_t2048066811 * ____propInitData_16;

public:
	inline static int32_t get_offset_of__propTypeName_15() { return static_cast<int32_t>(offsetof(TypedElement_t4034655484_StaticFields, ____propTypeName_15)); }
	inline ConfigurationProperty_t2048066811 * get__propTypeName_15() const { return ____propTypeName_15; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propTypeName_15() { return &____propTypeName_15; }
	inline void set__propTypeName_15(ConfigurationProperty_t2048066811 * value)
	{
		____propTypeName_15 = value;
		Il2CppCodeGenWriteBarrier(&____propTypeName_15, value);
	}

	inline static int32_t get_offset_of__propInitData_16() { return static_cast<int32_t>(offsetof(TypedElement_t4034655484_StaticFields, ____propInitData_16)); }
	inline ConfigurationProperty_t2048066811 * get__propInitData_16() const { return ____propInitData_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propInitData_16() { return &____propInitData_16; }
	inline void set__propInitData_16(ConfigurationProperty_t2048066811 * value)
	{
		____propInitData_16 = value;
		Il2CppCodeGenWriteBarrier(&____propInitData_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
