﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MemberReference
struct  MemberReference_t1102219583  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.MemberReference::idRef
	int32_t ___idRef_0;

public:
	inline static int32_t get_offset_of_idRef_0() { return static_cast<int32_t>(offsetof(MemberReference_t1102219583, ___idRef_0)); }
	inline int32_t get_idRef_0() const { return ___idRef_0; }
	inline int32_t* get_address_of_idRef_0() { return &___idRef_0; }
	inline void set_idRef_0(int32_t value)
	{
		___idRef_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
