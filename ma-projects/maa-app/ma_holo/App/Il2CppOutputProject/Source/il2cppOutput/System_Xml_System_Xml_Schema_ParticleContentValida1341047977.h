﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"

// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_t1753655453;
// System.Xml.Schema.Positions
struct Positions_t3593914952;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Xml.Schema.SyntaxTreeNode
struct SyntaxTreeNode_t2397191729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ParticleContentValidator
struct  ParticleContentValidator_t1341047977  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.ParticleContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_7;
	// System.Xml.Schema.Positions System.Xml.Schema.ParticleContentValidator::positions
	Positions_t3593914952 * ___positions_8;
	// System.Collections.Stack System.Xml.Schema.ParticleContentValidator::stack
	Stack_t1043988394 * ___stack_9;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.ParticleContentValidator::contentNode
	SyntaxTreeNode_t2397191729 * ___contentNode_10;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::isPartial
	bool ___isPartial_11;
	// System.Int32 System.Xml.Schema.ParticleContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::enableUpaCheck
	bool ___enableUpaCheck_13;

public:
	inline static int32_t get_offset_of_symbols_7() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___symbols_7)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_7() const { return ___symbols_7; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_7() { return &___symbols_7; }
	inline void set_symbols_7(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_7 = value;
		Il2CppCodeGenWriteBarrier(&___symbols_7, value);
	}

	inline static int32_t get_offset_of_positions_8() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___positions_8)); }
	inline Positions_t3593914952 * get_positions_8() const { return ___positions_8; }
	inline Positions_t3593914952 ** get_address_of_positions_8() { return &___positions_8; }
	inline void set_positions_8(Positions_t3593914952 * value)
	{
		___positions_8 = value;
		Il2CppCodeGenWriteBarrier(&___positions_8, value);
	}

	inline static int32_t get_offset_of_stack_9() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___stack_9)); }
	inline Stack_t1043988394 * get_stack_9() const { return ___stack_9; }
	inline Stack_t1043988394 ** get_address_of_stack_9() { return &___stack_9; }
	inline void set_stack_9(Stack_t1043988394 * value)
	{
		___stack_9 = value;
		Il2CppCodeGenWriteBarrier(&___stack_9, value);
	}

	inline static int32_t get_offset_of_contentNode_10() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___contentNode_10)); }
	inline SyntaxTreeNode_t2397191729 * get_contentNode_10() const { return ___contentNode_10; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_contentNode_10() { return &___contentNode_10; }
	inline void set_contentNode_10(SyntaxTreeNode_t2397191729 * value)
	{
		___contentNode_10 = value;
		Il2CppCodeGenWriteBarrier(&___contentNode_10, value);
	}

	inline static int32_t get_offset_of_isPartial_11() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___isPartial_11)); }
	inline bool get_isPartial_11() const { return ___isPartial_11; }
	inline bool* get_address_of_isPartial_11() { return &___isPartial_11; }
	inline void set_isPartial_11(bool value)
	{
		___isPartial_11 = value;
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_enableUpaCheck_13() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___enableUpaCheck_13)); }
	inline bool get_enableUpaCheck_13() const { return ___enableUpaCheck_13; }
	inline bool* get_address_of_enableUpaCheck_13() { return &___enableUpaCheck_13; }
	inline void set_enableUpaCheck_13(bool value)
	{
		___enableUpaCheck_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
