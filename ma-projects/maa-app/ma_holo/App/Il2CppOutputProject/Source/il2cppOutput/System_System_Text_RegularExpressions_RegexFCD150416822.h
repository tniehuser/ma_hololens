﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Text.RegularExpressions.RegexFC[]
struct RegexFCU5BU5D_t2364340679;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFCD
struct  RegexFCD_t150416822  : public Il2CppObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexFCD::_intStack
	Int32U5BU5D_t3030399641* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_intDepth
	int32_t ____intDepth_1;
	// System.Text.RegularExpressions.RegexFC[] System.Text.RegularExpressions.RegexFCD::_fcStack
	RegexFCU5BU5D_t2364340679* ____fcStack_2;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_fcDepth
	int32_t ____fcDepth_3;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipAllChildren
	bool ____skipAllChildren_4;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipchild
	bool ____skipchild_5;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_failed
	bool ____failed_6;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____intStack_0)); }
	inline Int32U5BU5D_t3030399641* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t3030399641* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier(&____intStack_0, value);
	}

	inline static int32_t get_offset_of__intDepth_1() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____intDepth_1)); }
	inline int32_t get__intDepth_1() const { return ____intDepth_1; }
	inline int32_t* get_address_of__intDepth_1() { return &____intDepth_1; }
	inline void set__intDepth_1(int32_t value)
	{
		____intDepth_1 = value;
	}

	inline static int32_t get_offset_of__fcStack_2() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____fcStack_2)); }
	inline RegexFCU5BU5D_t2364340679* get__fcStack_2() const { return ____fcStack_2; }
	inline RegexFCU5BU5D_t2364340679** get_address_of__fcStack_2() { return &____fcStack_2; }
	inline void set__fcStack_2(RegexFCU5BU5D_t2364340679* value)
	{
		____fcStack_2 = value;
		Il2CppCodeGenWriteBarrier(&____fcStack_2, value);
	}

	inline static int32_t get_offset_of__fcDepth_3() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____fcDepth_3)); }
	inline int32_t get__fcDepth_3() const { return ____fcDepth_3; }
	inline int32_t* get_address_of__fcDepth_3() { return &____fcDepth_3; }
	inline void set__fcDepth_3(int32_t value)
	{
		____fcDepth_3 = value;
	}

	inline static int32_t get_offset_of__skipAllChildren_4() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____skipAllChildren_4)); }
	inline bool get__skipAllChildren_4() const { return ____skipAllChildren_4; }
	inline bool* get_address_of__skipAllChildren_4() { return &____skipAllChildren_4; }
	inline void set__skipAllChildren_4(bool value)
	{
		____skipAllChildren_4 = value;
	}

	inline static int32_t get_offset_of__skipchild_5() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____skipchild_5)); }
	inline bool get__skipchild_5() const { return ____skipchild_5; }
	inline bool* get_address_of__skipchild_5() { return &____skipchild_5; }
	inline void set__skipchild_5(bool value)
	{
		____skipchild_5 = value;
	}

	inline static int32_t get_offset_of__failed_6() { return static_cast<int32_t>(offsetof(RegexFCD_t150416822, ____failed_6)); }
	inline bool get__failed_6() const { return ____failed_6; }
	inline bool* get_address_of__failed_6() { return &____failed_6; }
	inline void set__failed_6(bool value)
	{
		____failed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
