﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_RegexRunner3983612747.h"

// System.Text.RegularExpressions.NoParamDelegate
struct NoParamDelegate_t2890182105;
// System.Text.RegularExpressions.FindFirstCharDelegate
struct FindFirstCharDelegate_t4203859986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CompiledRegexRunner
struct  CompiledRegexRunner_t592995170  : public RegexRunner_t3983612747
{
public:
	// System.Text.RegularExpressions.NoParamDelegate System.Text.RegularExpressions.CompiledRegexRunner::goMethod
	NoParamDelegate_t2890182105 * ___goMethod_18;
	// System.Text.RegularExpressions.FindFirstCharDelegate System.Text.RegularExpressions.CompiledRegexRunner::findFirstCharMethod
	FindFirstCharDelegate_t4203859986 * ___findFirstCharMethod_19;
	// System.Text.RegularExpressions.NoParamDelegate System.Text.RegularExpressions.CompiledRegexRunner::initTrackCountMethod
	NoParamDelegate_t2890182105 * ___initTrackCountMethod_20;

public:
	inline static int32_t get_offset_of_goMethod_18() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___goMethod_18)); }
	inline NoParamDelegate_t2890182105 * get_goMethod_18() const { return ___goMethod_18; }
	inline NoParamDelegate_t2890182105 ** get_address_of_goMethod_18() { return &___goMethod_18; }
	inline void set_goMethod_18(NoParamDelegate_t2890182105 * value)
	{
		___goMethod_18 = value;
		Il2CppCodeGenWriteBarrier(&___goMethod_18, value);
	}

	inline static int32_t get_offset_of_findFirstCharMethod_19() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___findFirstCharMethod_19)); }
	inline FindFirstCharDelegate_t4203859986 * get_findFirstCharMethod_19() const { return ___findFirstCharMethod_19; }
	inline FindFirstCharDelegate_t4203859986 ** get_address_of_findFirstCharMethod_19() { return &___findFirstCharMethod_19; }
	inline void set_findFirstCharMethod_19(FindFirstCharDelegate_t4203859986 * value)
	{
		___findFirstCharMethod_19 = value;
		Il2CppCodeGenWriteBarrier(&___findFirstCharMethod_19, value);
	}

	inline static int32_t get_offset_of_initTrackCountMethod_20() { return static_cast<int32_t>(offsetof(CompiledRegexRunner_t592995170, ___initTrackCountMethod_20)); }
	inline NoParamDelegate_t2890182105 * get_initTrackCountMethod_20() const { return ___initTrackCountMethod_20; }
	inline NoParamDelegate_t2890182105 ** get_address_of_initTrackCountMethod_20() { return &___initTrackCountMethod_20; }
	inline void set_initTrackCountMethod_20(NoParamDelegate_t2890182105 * value)
	{
		___initTrackCountMethod_20 = value;
		Il2CppCodeGenWriteBarrier(&___initTrackCountMethod_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
