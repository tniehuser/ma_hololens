﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Asttree
struct  Asttree_t3451058494  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Asttree::fAxisArray
	ArrayList_t4252133567 * ___fAxisArray_0;
	// System.String System.Xml.Schema.Asttree::xpathexpr
	String_t* ___xpathexpr_1;
	// System.Boolean System.Xml.Schema.Asttree::isField
	bool ___isField_2;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Asttree::nsmgr
	XmlNamespaceManager_t486731501 * ___nsmgr_3;

public:
	inline static int32_t get_offset_of_fAxisArray_0() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___fAxisArray_0)); }
	inline ArrayList_t4252133567 * get_fAxisArray_0() const { return ___fAxisArray_0; }
	inline ArrayList_t4252133567 ** get_address_of_fAxisArray_0() { return &___fAxisArray_0; }
	inline void set_fAxisArray_0(ArrayList_t4252133567 * value)
	{
		___fAxisArray_0 = value;
		Il2CppCodeGenWriteBarrier(&___fAxisArray_0, value);
	}

	inline static int32_t get_offset_of_xpathexpr_1() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___xpathexpr_1)); }
	inline String_t* get_xpathexpr_1() const { return ___xpathexpr_1; }
	inline String_t** get_address_of_xpathexpr_1() { return &___xpathexpr_1; }
	inline void set_xpathexpr_1(String_t* value)
	{
		___xpathexpr_1 = value;
		Il2CppCodeGenWriteBarrier(&___xpathexpr_1, value);
	}

	inline static int32_t get_offset_of_isField_2() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___isField_2)); }
	inline bool get_isField_2() const { return ___isField_2; }
	inline bool* get_address_of_isField_2() { return &___isField_2; }
	inline void set_isField_2(bool value)
	{
		___isField_2 = value;
	}

	inline static int32_t get_offset_of_nsmgr_3() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___nsmgr_3)); }
	inline XmlNamespaceManager_t486731501 * get_nsmgr_3() const { return ___nsmgr_3; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsmgr_3() { return &___nsmgr_3; }
	inline void set_nsmgr_3(XmlNamespaceManager_t486731501 * value)
	{
		___nsmgr_3 = value;
		Il2CppCodeGenWriteBarrier(&___nsmgr_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
