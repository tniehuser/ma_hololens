﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_U3CModuleU3E3783534214.h"
#include "mscorlib_Locale4255929014.h"
#include "mscorlib_System_MonoTODOAttribute3487514019.h"
#include "mscorlib_System_MonoLimitationAttribute17004303.h"
#include "mscorlib_System_Threading_AtomicBoolean379413895.h"
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorB1053438167.h"
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearch463670656.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor1997037801.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest572679901.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTests3283102398.h"
#include "mscorlib_Mono_Math_BigInteger925946152.h"
#include "mscorlib_Mono_Math_BigInteger_Sign874893935.h"
#include "mscorlib_Mono_Math_BigInteger_ModulusRing80355991.h"
#include "mscorlib_Mono_Math_BigInteger_Kernel1353186455.h"
#include "mscorlib_Mono_Security_Authenticode_AuthenticodeBa3368165232.h"
#include "mscorlib_Mono_Security_Authenticode_AuthenticodeDef978432004.h"
#include "mscorlib_Mono_Security_Cryptography_CryptoConvert4146607874.h"
#include "mscorlib_Mono_Security_Cryptography_KeyBuilder3965881084.h"
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersist3637935872.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS13312870480.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS82103016899.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKey92917103.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS8_Encrypte1722354997.h"
#include "mscorlib_Mono_Security_Cryptography_RSAManaged3034748747.h"
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyG108853709.h"
#include "mscorlib_Mono_Security_Cryptography_SymmetricTrans1394030013.h"
#include "mscorlib_Mono_Security_X509_Extensions_BasicConstr3608227951.h"
#include "mscorlib_Mono_Security_X509_SafeBag2166702855.h"
#include "mscorlib_Mono_Security_X509_PKCS121362584794.h"
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytes1740753016.h"
#include "mscorlib_Mono_Security_X509_X501349661534.h"
#include "mscorlib_Mono_Security_X509_X509Certificate324051957.h"
#include "mscorlib_Mono_Security_X509_X509CertificateCollect3592472865.h"
#include "mscorlib_Mono_Security_X509_X509CertificateCollect3487770522.h"
#include "mscorlib_Mono_Security_X509_X509Chain1938971907.h"
#include "mscorlib_Mono_Security_X509_X509ChainStatusFlags2843686920.h"
#include "mscorlib_Mono_Security_X509_X509Extension1439760127.h"
#include "mscorlib_Mono_Security_X509_X509ExtensionCollectio1640144839.h"
#include "mscorlib_Mono_Security_X509_X509Store4028973563.h"
#include "mscorlib_Mono_Security_X509_X509StoreManager1740460066.h"
#include "mscorlib_Mono_Security_X509_X509Stores3001420398.h"
#include "mscorlib_Mono_Security_ASN1924533535.h"
#include "mscorlib_Mono_Security_ASN1Convert3301846396.h"
#include "mscorlib_Mono_Security_BitConverterLE2825370260.h"
#include "mscorlib_Mono_Security_PKCS73223261922.h"
#include "mscorlib_Mono_Security_PKCS7_ContentInfo1443605387.h"
#include "mscorlib_Mono_Security_PKCS7_EncryptedData2656813772.h"
#include "mscorlib_Mono_Security_PKCS7_SignedData1944945924.h"
#include "mscorlib_Mono_Security_PKCS7_SignerInfo1683925522.h"
#include "mscorlib_Mono_Security_StrongName117835354.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Criti1920899984.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi1390725888.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer2101567438.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Relia1625655220.h"
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeFileHandle243342855.h"
#include "mscorlib_Microsoft_Win32_SafeLibraryHandle116810554.h"
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeRegistryH1955425892.h"
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandle481461830.h"
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZer1177681199.h"
#include "mscorlib_Microsoft_Win32_UnsafeNativeMethods1041081549.h"
#include "mscorlib_Microsoft_Win32_Win32Native932910218.h"
#include "mscorlib_Microsoft_Win32_Win32Native_RegistryTimeZ3530070130.h"
#include "mscorlib_System_AggregateException420812976.h"
#include "mscorlib_System___Filters3245412223.h"
#include "mscorlib_System_LocalDataStoreHolder2240136856.h"
#include "mscorlib_System_LocalDataStoreElement3980707294.h"
#include "mscorlib_System_LocalDataStore228818476.h"
#include "mscorlib_System_LocalDataStoreSlot486331200.h"
#include "mscorlib_System_LocalDataStoreMgr1152954092.h"
#include "mscorlib_System_Action3226471752.h"
#include "mscorlib_System_Activator1850728717.h"
#include "mscorlib_System_LoaderOptimization1143026982.h"
#include "mscorlib_System_AppDomainUnloadedException4216041716.h"
#include "mscorlib_System_ApplicationException474868623.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_ArithmeticException3261462543.h"
#include "mscorlib_System_ArrayTypeMismatchException2071164632.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Attribute542643598.h"
#include "mscorlib_System_AttributeTargets1984597432.h"
#include "mscorlib_System_AttributeUsageAttribute1057435127.h"
#include "mscorlib_System_BadImageFormatException4102491222.h"
#include "mscorlib_System_BitConverter3195628829.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Buffer3497320070.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Char3454481338.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize0 = { sizeof (U3CModuleU3E_t3783534214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1 = { sizeof (Locale_t4255929014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2 = { sizeof (MonoTODOAttribute_t3487514019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2[1] = 
{
	MonoTODOAttribute_t3487514019::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3 = { sizeof (MonoLimitationAttribute_t17004303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4 = { sizeof (AtomicBoolean_t379413895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4[1] = 
{
	AtomicBoolean_t379413895::get_offset_of_flag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5 = { sizeof (PrimeGeneratorBase_t1053438167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6 = { sizeof (SequentialSearchPrimeGeneratorBase_t463670656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7 = { sizeof (ConfidenceFactor_t1997037801)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7[7] = 
{
	ConfidenceFactor_t1997037801::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8 = { sizeof (PrimalityTest_t572679901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize9 = { sizeof (PrimalityTests_t3283102398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize10 = { sizeof (BigInteger_t925946152), -1, sizeof(BigInteger_t925946152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable10[4] = 
{
	BigInteger_t925946152::get_offset_of_length_0(),
	BigInteger_t925946152::get_offset_of_data_1(),
	BigInteger_t925946152_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t925946152_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize11 = { sizeof (Sign_t874893935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable11[4] = 
{
	Sign_t874893935::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize12 = { sizeof (ModulusRing_t80355991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable12[2] = 
{
	ModulusRing_t80355991::get_offset_of_mod_0(),
	ModulusRing_t80355991::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize13 = { sizeof (Kernel_t1353186455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize14 = { sizeof (AuthenticodeBase_t3368165232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable14[8] = 
{
	AuthenticodeBase_t3368165232::get_offset_of_fileblock_0(),
	AuthenticodeBase_t3368165232::get_offset_of_fs_1(),
	AuthenticodeBase_t3368165232::get_offset_of_blockNo_2(),
	AuthenticodeBase_t3368165232::get_offset_of_blockLength_3(),
	AuthenticodeBase_t3368165232::get_offset_of_peOffset_4(),
	AuthenticodeBase_t3368165232::get_offset_of_dirSecurityOffset_5(),
	AuthenticodeBase_t3368165232::get_offset_of_dirSecuritySize_6(),
	AuthenticodeBase_t3368165232::get_offset_of_coffSymbolTableOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize15 = { sizeof (AuthenticodeDeformatter_t978432004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable15[12] = 
{
	AuthenticodeDeformatter_t978432004::get_offset_of_filename_8(),
	AuthenticodeDeformatter_t978432004::get_offset_of_hash_9(),
	AuthenticodeDeformatter_t978432004::get_offset_of_coll_10(),
	AuthenticodeDeformatter_t978432004::get_offset_of_signedHash_11(),
	AuthenticodeDeformatter_t978432004::get_offset_of_timestamp_12(),
	AuthenticodeDeformatter_t978432004::get_offset_of_signingCertificate_13(),
	AuthenticodeDeformatter_t978432004::get_offset_of_reason_14(),
	AuthenticodeDeformatter_t978432004::get_offset_of_trustedRoot_15(),
	AuthenticodeDeformatter_t978432004::get_offset_of_trustedTimestampRoot_16(),
	AuthenticodeDeformatter_t978432004::get_offset_of_entry_17(),
	AuthenticodeDeformatter_t978432004::get_offset_of_signerChain_18(),
	AuthenticodeDeformatter_t978432004::get_offset_of_timestampChain_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize16 = { sizeof (CryptoConvert_t4146607874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize17 = { sizeof (KeyBuilder_t3965881084), -1, sizeof(KeyBuilder_t3965881084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable17[1] = 
{
	KeyBuilder_t3965881084_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize18 = { sizeof (KeyPairPersistence_t3637935872), -1, sizeof(KeyPairPersistence_t3637935872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable18[9] = 
{
	KeyPairPersistence_t3637935872_StaticFields::get_offset_of__userPathExists_0(),
	KeyPairPersistence_t3637935872_StaticFields::get_offset_of__userPath_1(),
	KeyPairPersistence_t3637935872_StaticFields::get_offset_of__machinePathExists_2(),
	KeyPairPersistence_t3637935872_StaticFields::get_offset_of__machinePath_3(),
	KeyPairPersistence_t3637935872::get_offset_of__params_4(),
	KeyPairPersistence_t3637935872::get_offset_of__keyvalue_5(),
	KeyPairPersistence_t3637935872::get_offset_of__filename_6(),
	KeyPairPersistence_t3637935872::get_offset_of__container_7(),
	KeyPairPersistence_t3637935872_StaticFields::get_offset_of_lockobj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize19 = { sizeof (PKCS1_t3312870480), -1, sizeof(PKCS1_t3312870480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable19[5] = 
{
	PKCS1_t3312870480_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3312870480_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3312870480_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3312870480_StaticFields::get_offset_of_emptySHA512_3(),
	PKCS1_t3312870480_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize20 = { sizeof (PKCS8_t2103016899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize21 = { sizeof (PrivateKeyInfo_t92917103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable21[4] = 
{
	PrivateKeyInfo_t92917103::get_offset_of__version_0(),
	PrivateKeyInfo_t92917103::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t92917103::get_offset_of__key_2(),
	PrivateKeyInfo_t92917103::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize22 = { sizeof (EncryptedPrivateKeyInfo_t1722354997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable22[4] = 
{
	EncryptedPrivateKeyInfo_t1722354997::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t1722354997::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t1722354997::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t1722354997::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize23 = { sizeof (RSAManaged_t3034748747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable23[13] = 
{
	RSAManaged_t3034748747::get_offset_of_isCRTpossible_2(),
	RSAManaged_t3034748747::get_offset_of_keyBlinding_3(),
	RSAManaged_t3034748747::get_offset_of_keypairGenerated_4(),
	RSAManaged_t3034748747::get_offset_of_m_disposed_5(),
	RSAManaged_t3034748747::get_offset_of_d_6(),
	RSAManaged_t3034748747::get_offset_of_p_7(),
	RSAManaged_t3034748747::get_offset_of_q_8(),
	RSAManaged_t3034748747::get_offset_of_dp_9(),
	RSAManaged_t3034748747::get_offset_of_dq_10(),
	RSAManaged_t3034748747::get_offset_of_qInv_11(),
	RSAManaged_t3034748747::get_offset_of_n_12(),
	RSAManaged_t3034748747::get_offset_of_e_13(),
	RSAManaged_t3034748747::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize24 = { sizeof (KeyGeneratedEventHandler_t108853709), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize25 = { sizeof (SymmetricTransform_t1394030013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable25[12] = 
{
	SymmetricTransform_t1394030013::get_offset_of_algo_0(),
	SymmetricTransform_t1394030013::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030013::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030013::get_offset_of_temp_3(),
	SymmetricTransform_t1394030013::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030013::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030013::get_offset_of_workout_6(),
	SymmetricTransform_t1394030013::get_offset_of_padmode_7(),
	SymmetricTransform_t1394030013::get_offset_of_FeedBackByte_8(),
	SymmetricTransform_t1394030013::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030013::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030013::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize26 = { sizeof (BasicConstraintsExtension_t3608227951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable26[2] = 
{
	BasicConstraintsExtension_t3608227951::get_offset_of_cA_3(),
	BasicConstraintsExtension_t3608227951::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize27 = { sizeof (SafeBag_t2166702855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable27[2] = 
{
	SafeBag_t2166702855::get_offset_of__bagOID_0(),
	SafeBag_t2166702855::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize28 = { sizeof (PKCS12_t1362584794), -1, sizeof(PKCS12_t1362584794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable28[12] = 
{
	PKCS12_t1362584794::get_offset_of__password_0(),
	PKCS12_t1362584794::get_offset_of__keyBags_1(),
	PKCS12_t1362584794::get_offset_of__secretBags_2(),
	PKCS12_t1362584794::get_offset_of__certs_3(),
	PKCS12_t1362584794::get_offset_of__keyBagsChanged_4(),
	PKCS12_t1362584794::get_offset_of__secretBagsChanged_5(),
	PKCS12_t1362584794::get_offset_of__certsChanged_6(),
	PKCS12_t1362584794::get_offset_of__iterations_7(),
	PKCS12_t1362584794::get_offset_of__safeBags_8(),
	PKCS12_t1362584794::get_offset_of__rng_9(),
	PKCS12_t1362584794_StaticFields::get_offset_of_password_max_length_10(),
	PKCS12_t1362584794_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize29 = { sizeof (DeriveBytes_t1740753016), -1, sizeof(DeriveBytes_t1740753016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable29[7] = 
{
	DeriveBytes_t1740753016_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t1740753016_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t1740753016_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t1740753016::get_offset_of__hashName_3(),
	DeriveBytes_t1740753016::get_offset_of__iterations_4(),
	DeriveBytes_t1740753016::get_offset_of__password_5(),
	DeriveBytes_t1740753016::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize30 = { sizeof (X501_t349661534), -1, sizeof(X501_t349661534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable30[15] = 
{
	X501_t349661534_StaticFields::get_offset_of_countryName_0(),
	X501_t349661534_StaticFields::get_offset_of_organizationName_1(),
	X501_t349661534_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_t349661534_StaticFields::get_offset_of_commonName_3(),
	X501_t349661534_StaticFields::get_offset_of_localityName_4(),
	X501_t349661534_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_t349661534_StaticFields::get_offset_of_streetAddress_6(),
	X501_t349661534_StaticFields::get_offset_of_domainComponent_7(),
	X501_t349661534_StaticFields::get_offset_of_userid_8(),
	X501_t349661534_StaticFields::get_offset_of_email_9(),
	X501_t349661534_StaticFields::get_offset_of_dnQualifier_10(),
	X501_t349661534_StaticFields::get_offset_of_title_11(),
	X501_t349661534_StaticFields::get_offset_of_surname_12(),
	X501_t349661534_StaticFields::get_offset_of_givenName_13(),
	X501_t349661534_StaticFields::get_offset_of_initial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize31 = { sizeof (X509Certificate_t324051957), -1, sizeof(X509Certificate_t324051957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable31[24] = 
{
	X509Certificate_t324051957::get_offset_of_decoder_0(),
	X509Certificate_t324051957::get_offset_of_m_encodedcert_1(),
	X509Certificate_t324051957::get_offset_of_m_from_2(),
	X509Certificate_t324051957::get_offset_of_m_until_3(),
	X509Certificate_t324051957::get_offset_of_issuer_4(),
	X509Certificate_t324051957::get_offset_of_m_issuername_5(),
	X509Certificate_t324051957::get_offset_of_m_keyalgo_6(),
	X509Certificate_t324051957::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_t324051957::get_offset_of_subject_8(),
	X509Certificate_t324051957::get_offset_of_m_subject_9(),
	X509Certificate_t324051957::get_offset_of_m_publickey_10(),
	X509Certificate_t324051957::get_offset_of_signature_11(),
	X509Certificate_t324051957::get_offset_of_m_signaturealgo_12(),
	X509Certificate_t324051957::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_t324051957::get_offset_of_certhash_14(),
	X509Certificate_t324051957::get_offset_of__rsa_15(),
	X509Certificate_t324051957::get_offset_of__dsa_16(),
	X509Certificate_t324051957::get_offset_of_version_17(),
	X509Certificate_t324051957::get_offset_of_serialnumber_18(),
	X509Certificate_t324051957::get_offset_of_issuerUniqueID_19(),
	X509Certificate_t324051957::get_offset_of_subjectUniqueID_20(),
	X509Certificate_t324051957::get_offset_of_extensions_21(),
	X509Certificate_t324051957_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_t324051957_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize32 = { sizeof (X509CertificateCollection_t3592472865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize33 = { sizeof (X509CertificateEnumerator_t3487770522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable33[1] = 
{
	X509CertificateEnumerator_t3487770522::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize34 = { sizeof (X509Chain_t1938971907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable34[5] = 
{
	X509Chain_t1938971907::get_offset_of_roots_0(),
	X509Chain_t1938971907::get_offset_of_certs_1(),
	X509Chain_t1938971907::get_offset_of__root_2(),
	X509Chain_t1938971907::get_offset_of__chain_3(),
	X509Chain_t1938971907::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize35 = { sizeof (X509ChainStatusFlags_t2843686921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable35[8] = 
{
	X509ChainStatusFlags_t2843686921::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize36 = { sizeof (X509Extension_t1439760127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable36[3] = 
{
	X509Extension_t1439760127::get_offset_of_extnOid_0(),
	X509Extension_t1439760127::get_offset_of_extnCritical_1(),
	X509Extension_t1439760127::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize37 = { sizeof (X509ExtensionCollection_t1640144839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable37[1] = 
{
	X509ExtensionCollection_t1640144839::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize38 = { sizeof (X509Store_t4028973563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable38[4] = 
{
	X509Store_t4028973563::get_offset_of__storePath_0(),
	X509Store_t4028973563::get_offset_of__certificates_1(),
	X509Store_t4028973563::get_offset_of__crl_2(),
	X509Store_t4028973563::get_offset_of__newFormat_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize39 = { sizeof (X509StoreManager_t1740460066), -1, sizeof(X509StoreManager_t1740460066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable39[4] = 
{
	X509StoreManager_t1740460066_StaticFields::get_offset_of__userPath_0(),
	X509StoreManager_t1740460066_StaticFields::get_offset_of__localMachinePath_1(),
	X509StoreManager_t1740460066_StaticFields::get_offset_of__userStore_2(),
	X509StoreManager_t1740460066_StaticFields::get_offset_of__machineStore_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize40 = { sizeof (X509Stores_t3001420398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable40[3] = 
{
	X509Stores_t3001420398::get_offset_of__storePath_0(),
	X509Stores_t3001420398::get_offset_of__newFormat_1(),
	X509Stores_t3001420398::get_offset_of__trusted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize41 = { sizeof (ASN1_t924533535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable41[3] = 
{
	ASN1_t924533535::get_offset_of_m_nTag_0(),
	ASN1_t924533535::get_offset_of_m_aValue_1(),
	ASN1_t924533535::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize42 = { sizeof (ASN1Convert_t3301846396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize43 = { sizeof (BitConverterLE_t2825370260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize44 = { sizeof (PKCS7_t3223261922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize45 = { sizeof (ContentInfo_t1443605387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable45[2] = 
{
	ContentInfo_t1443605387::get_offset_of_contentType_0(),
	ContentInfo_t1443605387::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize46 = { sizeof (EncryptedData_t2656813772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable46[4] = 
{
	EncryptedData_t2656813772::get_offset_of__version_0(),
	EncryptedData_t2656813772::get_offset_of__content_1(),
	EncryptedData_t2656813772::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2656813772::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize47 = { sizeof (SignedData_t1944945924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable47[7] = 
{
	SignedData_t1944945924::get_offset_of_version_0(),
	SignedData_t1944945924::get_offset_of_hashAlgorithm_1(),
	SignedData_t1944945924::get_offset_of_contentInfo_2(),
	SignedData_t1944945924::get_offset_of_certs_3(),
	SignedData_t1944945924::get_offset_of_crls_4(),
	SignedData_t1944945924::get_offset_of_signerInfo_5(),
	SignedData_t1944945924::get_offset_of_mda_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize48 = { sizeof (SignerInfo_t1683925522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable48[8] = 
{
	SignerInfo_t1683925522::get_offset_of_version_0(),
	SignerInfo_t1683925522::get_offset_of_hashAlgorithm_1(),
	SignerInfo_t1683925522::get_offset_of_authenticatedAttributes_2(),
	SignerInfo_t1683925522::get_offset_of_unauthenticatedAttributes_3(),
	SignerInfo_t1683925522::get_offset_of_signature_4(),
	SignerInfo_t1683925522::get_offset_of_issuer_5(),
	SignerInfo_t1683925522::get_offset_of_serial_6(),
	SignerInfo_t1683925522::get_offset_of_ski_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize49 = { sizeof (StrongName_t117835354), -1, sizeof(StrongName_t117835354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable49[5] = 
{
	StrongName_t117835354::get_offset_of_rsa_0(),
	StrongName_t117835354::get_offset_of_publicKey_1(),
	StrongName_t117835354::get_offset_of_keyToken_2(),
	StrongName_t117835354::get_offset_of_tokenAlgorithm_3(),
	StrongName_t117835354_StaticFields::get_offset_of_lockObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize50 = { sizeof (CriticalFinalizerObject_t1920899984), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize51 = { sizeof (Consistency_t1390725888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable51[5] = 
{
	Consistency_t1390725888::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize52 = { sizeof (Cer_t2101567438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable52[4] = 
{
	Cer_t2101567438::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize53 = { sizeof (ReliabilityContractAttribute_t1625655220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable53[2] = 
{
	ReliabilityContractAttribute_t1625655220::get_offset_of__consistency_0(),
	ReliabilityContractAttribute_t1625655220::get_offset_of__cer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize54 = { sizeof (SafeFileHandle_t243342855), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize55 = { sizeof (SafeLibraryHandle_t116810554), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize56 = { sizeof (SafeRegistryHandle_t1955425892), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize57 = { sizeof (SafeWaitHandle_t481461830), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize58 = { sizeof (SafeHandleZeroOrMinusOneIsInvalid_t1177681199), sizeof(void*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize59 = { sizeof (UnsafeNativeMethods_t1041081549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize60 = { sizeof (Win32Native_t932910218), -1, sizeof(Win32Native_t932910218_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable60[2] = 
{
	Win32Native_t932910218_StaticFields::get_offset_of_INVALID_HANDLE_VALUE_0(),
	Win32Native_t932910218_StaticFields::get_offset_of_ThreadErrorModeMinOsVersion_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize61 = { sizeof (RegistryTimeZoneInformation_t3530070130)+ sizeof (Il2CppObject), sizeof(RegistryTimeZoneInformation_t3530070130 ), 0, 0 };
extern const int32_t g_FieldOffsetTable61[5] = 
{
	RegistryTimeZoneInformation_t3530070130::get_offset_of_Bias_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RegistryTimeZoneInformation_t3530070130::get_offset_of_StandardBias_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RegistryTimeZoneInformation_t3530070130::get_offset_of_DaylightBias_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RegistryTimeZoneInformation_t3530070130::get_offset_of_StandardDate_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RegistryTimeZoneInformation_t3530070130::get_offset_of_DaylightDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize62 = { sizeof (AggregateException_t420812976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable62[1] = 
{
	AggregateException_t420812976::get_offset_of_m_innerExceptions_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize63 = { sizeof (__Filters_t3245412223), -1, sizeof(__Filters_t3245412223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable63[1] = 
{
	__Filters_t3245412223_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize64 = { sizeof (LocalDataStoreHolder_t2240136856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable64[1] = 
{
	LocalDataStoreHolder_t2240136856::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize65 = { sizeof (LocalDataStoreElement_t3980707294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable65[2] = 
{
	LocalDataStoreElement_t3980707294::get_offset_of_m_value_0(),
	LocalDataStoreElement_t3980707294::get_offset_of_m_cookie_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize66 = { sizeof (LocalDataStore_t228818476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable66[2] = 
{
	LocalDataStore_t228818476::get_offset_of_m_DataTable_0(),
	LocalDataStore_t228818476::get_offset_of_m_Manager_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize67 = { sizeof (LocalDataStoreSlot_t486331200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable67[3] = 
{
	LocalDataStoreSlot_t486331200::get_offset_of_m_mgr_0(),
	LocalDataStoreSlot_t486331200::get_offset_of_m_slot_1(),
	LocalDataStoreSlot_t486331200::get_offset_of_m_cookie_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize68 = { sizeof (LocalDataStoreMgr_t1152954092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable68[5] = 
{
	LocalDataStoreMgr_t1152954092::get_offset_of_m_SlotInfoTable_0(),
	LocalDataStoreMgr_t1152954092::get_offset_of_m_FirstAvailableSlot_1(),
	LocalDataStoreMgr_t1152954092::get_offset_of_m_ManagedLocalDataStores_2(),
	LocalDataStoreMgr_t1152954092::get_offset_of_m_KeyToSlotMap_3(),
	LocalDataStoreMgr_t1152954092::get_offset_of_m_CookieGenerator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize69 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize70 = { sizeof (Action_t3226471752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize71 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize72 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize73 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize74 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize75 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize76 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize77 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize78 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize79 = { sizeof (Activator_t1850728717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize80 = { sizeof (LoaderOptimization_t1143026982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable80[7] = 
{
	LoaderOptimization_t1143026982::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize81 = { sizeof (AppDomainUnloadedException_t4216041716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize82 = { sizeof (ApplicationException_t474868623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize83 = { sizeof (ArgumentException_t3259014390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable83[1] = 
{
	ArgumentException_t3259014390::get_offset_of_m_paramName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize84 = { sizeof (ArgumentNullException_t628810857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize85 = { sizeof (ArgumentOutOfRangeException_t279959794), -1, sizeof(ArgumentOutOfRangeException_t279959794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable85[2] = 
{
	ArgumentOutOfRangeException_t279959794_StaticFields::get_offset_of__rangeMessage_17(),
	ArgumentOutOfRangeException_t279959794::get_offset_of_m_actualValue_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize86 = { sizeof (ArithmeticException_t3261462543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize87 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable87[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize88 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable88[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize89 = { sizeof (ArrayTypeMismatchException_t2071164632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize90 = { sizeof (AsyncCallback_t163412349), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize91 = { sizeof (Attribute_t542643598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize92 = { sizeof (AttributeTargets_t1984597432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable92[17] = 
{
	AttributeTargets_t1984597432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize93 = { sizeof (AttributeUsageAttribute_t1057435127), -1, sizeof(AttributeUsageAttribute_t1057435127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable93[4] = 
{
	AttributeUsageAttribute_t1057435127::get_offset_of_m_attributeTarget_0(),
	AttributeUsageAttribute_t1057435127::get_offset_of_m_allowMultiple_1(),
	AttributeUsageAttribute_t1057435127::get_offset_of_m_inherited_2(),
	AttributeUsageAttribute_t1057435127_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize94 = { sizeof (BadImageFormatException_t4102491222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable94[2] = 
{
	BadImageFormatException_t4102491222::get_offset_of__fileName_16(),
	BadImageFormatException_t4102491222::get_offset_of__fusionLog_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize95 = { sizeof (BitConverter_t3195628829), -1, sizeof(BitConverter_t3195628829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable95[1] = 
{
	BitConverter_t3195628829_StaticFields::get_offset_of_IsLittleEndian_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize96 = { sizeof (Boolean_t3825574718)+ sizeof (Il2CppObject), 4, sizeof(Boolean_t3825574718_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable96[7] = 
{
	Boolean_t3825574718::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	Boolean_t3825574718_StaticFields::get_offset_of_TrueString_5(),
	Boolean_t3825574718_StaticFields::get_offset_of_FalseString_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize97 = { sizeof (Buffer_t3497320070), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize98 = { sizeof (Byte_t3683104436)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable98[3] = 
{
	Byte_t3683104436::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize99 = { sizeof (Char_t3454481338)+ sizeof (Il2CppObject), 1, sizeof(Char_t3454481338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable99[9] = 
{
	Char_t3454481338::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	Char_t3454481338_StaticFields::get_offset_of_categoryForLatin1_3(),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
