﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_nonNegativeI1851861419.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedLong
struct  Datatype_unsignedLong_t3462046402  : public Datatype_nonNegativeInteger_t1851861419
{
public:

public:
};

struct Datatype_unsignedLong_t3462046402_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedLong::atomicValueType
	Type_t * ___atomicValueType_97;
	// System.Type System.Xml.Schema.Datatype_unsignedLong::listValueType
	Type_t * ___listValueType_98;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedLong::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_99;

public:
	inline static int32_t get_offset_of_atomicValueType_97() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___atomicValueType_97)); }
	inline Type_t * get_atomicValueType_97() const { return ___atomicValueType_97; }
	inline Type_t ** get_address_of_atomicValueType_97() { return &___atomicValueType_97; }
	inline void set_atomicValueType_97(Type_t * value)
	{
		___atomicValueType_97 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_97, value);
	}

	inline static int32_t get_offset_of_listValueType_98() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___listValueType_98)); }
	inline Type_t * get_listValueType_98() const { return ___listValueType_98; }
	inline Type_t ** get_address_of_listValueType_98() { return &___listValueType_98; }
	inline void set_listValueType_98(Type_t * value)
	{
		___listValueType_98 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_98, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_99() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___numeric10FacetsChecker_99)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_99() const { return ___numeric10FacetsChecker_99; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_99() { return &___numeric10FacetsChecker_99; }
	inline void set_numeric10FacetsChecker_99(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_99 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_99, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
