﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebConnection_NtlmAuthState1285319885.h"

// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.IO.Stream
struct Stream_t3255436806;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Object
struct Il2CppObject;
// System.Net.IWebConnectionState
struct IWebConnectionState_t1335839370;
// System.Threading.WaitCallback
struct WaitCallback_t2798937288;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.EventHandler
struct EventHandler_t277755526;
// System.Net.WebConnection/AbortHelper
struct AbortHelper_t2895113041;
// System.Net.WebConnectionData
struct WebConnectionData_t3550260432;
// System.Net.ChunkStream
struct ChunkStream_t91719323;
// System.Collections.Queue
struct Queue_t1288490777;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Net.NetworkCredential
struct NetworkCredential_t1714133953;
// System.Exception
struct Exception_t1927440687;
// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t1249652258;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection
struct  WebConnection_t324679648  : public Il2CppObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnection::sPoint
	ServicePoint_t2765344313 * ___sPoint_0;
	// System.IO.Stream System.Net.WebConnection::nstream
	Stream_t3255436806 * ___nstream_1;
	// System.Net.Sockets.Socket System.Net.WebConnection::socket
	Socket_t3821512045 * ___socket_2;
	// System.Object System.Net.WebConnection::socketLock
	Il2CppObject * ___socketLock_3;
	// System.Net.IWebConnectionState System.Net.WebConnection::state
	Il2CppObject * ___state_4;
	// System.Net.WebExceptionStatus System.Net.WebConnection::status
	int32_t ___status_5;
	// System.Threading.WaitCallback System.Net.WebConnection::initConn
	WaitCallback_t2798937288 * ___initConn_6;
	// System.Boolean System.Net.WebConnection::keepAlive
	bool ___keepAlive_7;
	// System.Byte[] System.Net.WebConnection::buffer
	ByteU5BU5D_t3397334013* ___buffer_8;
	// System.EventHandler System.Net.WebConnection::abortHandler
	EventHandler_t277755526 * ___abortHandler_10;
	// System.Net.WebConnection/AbortHelper System.Net.WebConnection::abortHelper
	AbortHelper_t2895113041 * ___abortHelper_11;
	// System.Net.WebConnectionData System.Net.WebConnection::Data
	WebConnectionData_t3550260432 * ___Data_12;
	// System.Boolean System.Net.WebConnection::chunkedRead
	bool ___chunkedRead_13;
	// System.Net.ChunkStream System.Net.WebConnection::chunkStream
	ChunkStream_t91719323 * ___chunkStream_14;
	// System.Collections.Queue System.Net.WebConnection::queue
	Queue_t1288490777 * ___queue_15;
	// System.Boolean System.Net.WebConnection::reused
	bool ___reused_16;
	// System.Int32 System.Net.WebConnection::position
	int32_t ___position_17;
	// System.Net.HttpWebRequest System.Net.WebConnection::priority_request
	HttpWebRequest_t1951404513 * ___priority_request_18;
	// System.Net.NetworkCredential System.Net.WebConnection::ntlm_credentials
	NetworkCredential_t1714133953 * ___ntlm_credentials_19;
	// System.Boolean System.Net.WebConnection::ntlm_authenticated
	bool ___ntlm_authenticated_20;
	// System.Boolean System.Net.WebConnection::unsafe_sharing
	bool ___unsafe_sharing_21;
	// System.Net.WebConnection/NtlmAuthState System.Net.WebConnection::connect_ntlm_auth_state
	int32_t ___connect_ntlm_auth_state_22;
	// System.Net.HttpWebRequest System.Net.WebConnection::connect_request
	HttpWebRequest_t1951404513 * ___connect_request_23;
	// System.Exception System.Net.WebConnection::connect_exception
	Exception_t1927440687 * ___connect_exception_24;
	// Mono.Net.Security.MonoTlsStream System.Net.WebConnection::tlsStream
	MonoTlsStream_t1249652258 * ___tlsStream_26;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___sPoint_0)); }
	inline ServicePoint_t2765344313 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t2765344313 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t2765344313 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier(&___sPoint_0, value);
	}

	inline static int32_t get_offset_of_nstream_1() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___nstream_1)); }
	inline Stream_t3255436806 * get_nstream_1() const { return ___nstream_1; }
	inline Stream_t3255436806 ** get_address_of_nstream_1() { return &___nstream_1; }
	inline void set_nstream_1(Stream_t3255436806 * value)
	{
		___nstream_1 = value;
		Il2CppCodeGenWriteBarrier(&___nstream_1, value);
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___socket_2)); }
	inline Socket_t3821512045 * get_socket_2() const { return ___socket_2; }
	inline Socket_t3821512045 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t3821512045 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier(&___socket_2, value);
	}

	inline static int32_t get_offset_of_socketLock_3() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___socketLock_3)); }
	inline Il2CppObject * get_socketLock_3() const { return ___socketLock_3; }
	inline Il2CppObject ** get_address_of_socketLock_3() { return &___socketLock_3; }
	inline void set_socketLock_3(Il2CppObject * value)
	{
		___socketLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___socketLock_3, value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___state_4)); }
	inline Il2CppObject * get_state_4() const { return ___state_4; }
	inline Il2CppObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(Il2CppObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier(&___state_4, value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_initConn_6() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___initConn_6)); }
	inline WaitCallback_t2798937288 * get_initConn_6() const { return ___initConn_6; }
	inline WaitCallback_t2798937288 ** get_address_of_initConn_6() { return &___initConn_6; }
	inline void set_initConn_6(WaitCallback_t2798937288 * value)
	{
		___initConn_6 = value;
		Il2CppCodeGenWriteBarrier(&___initConn_6, value);
	}

	inline static int32_t get_offset_of_keepAlive_7() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___keepAlive_7)); }
	inline bool get_keepAlive_7() const { return ___keepAlive_7; }
	inline bool* get_address_of_keepAlive_7() { return &___keepAlive_7; }
	inline void set_keepAlive_7(bool value)
	{
		___keepAlive_7 = value;
	}

	inline static int32_t get_offset_of_buffer_8() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___buffer_8)); }
	inline ByteU5BU5D_t3397334013* get_buffer_8() const { return ___buffer_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_8() { return &___buffer_8; }
	inline void set_buffer_8(ByteU5BU5D_t3397334013* value)
	{
		___buffer_8 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_8, value);
	}

	inline static int32_t get_offset_of_abortHandler_10() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___abortHandler_10)); }
	inline EventHandler_t277755526 * get_abortHandler_10() const { return ___abortHandler_10; }
	inline EventHandler_t277755526 ** get_address_of_abortHandler_10() { return &___abortHandler_10; }
	inline void set_abortHandler_10(EventHandler_t277755526 * value)
	{
		___abortHandler_10 = value;
		Il2CppCodeGenWriteBarrier(&___abortHandler_10, value);
	}

	inline static int32_t get_offset_of_abortHelper_11() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___abortHelper_11)); }
	inline AbortHelper_t2895113041 * get_abortHelper_11() const { return ___abortHelper_11; }
	inline AbortHelper_t2895113041 ** get_address_of_abortHelper_11() { return &___abortHelper_11; }
	inline void set_abortHelper_11(AbortHelper_t2895113041 * value)
	{
		___abortHelper_11 = value;
		Il2CppCodeGenWriteBarrier(&___abortHelper_11, value);
	}

	inline static int32_t get_offset_of_Data_12() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___Data_12)); }
	inline WebConnectionData_t3550260432 * get_Data_12() const { return ___Data_12; }
	inline WebConnectionData_t3550260432 ** get_address_of_Data_12() { return &___Data_12; }
	inline void set_Data_12(WebConnectionData_t3550260432 * value)
	{
		___Data_12 = value;
		Il2CppCodeGenWriteBarrier(&___Data_12, value);
	}

	inline static int32_t get_offset_of_chunkedRead_13() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___chunkedRead_13)); }
	inline bool get_chunkedRead_13() const { return ___chunkedRead_13; }
	inline bool* get_address_of_chunkedRead_13() { return &___chunkedRead_13; }
	inline void set_chunkedRead_13(bool value)
	{
		___chunkedRead_13 = value;
	}

	inline static int32_t get_offset_of_chunkStream_14() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___chunkStream_14)); }
	inline ChunkStream_t91719323 * get_chunkStream_14() const { return ___chunkStream_14; }
	inline ChunkStream_t91719323 ** get_address_of_chunkStream_14() { return &___chunkStream_14; }
	inline void set_chunkStream_14(ChunkStream_t91719323 * value)
	{
		___chunkStream_14 = value;
		Il2CppCodeGenWriteBarrier(&___chunkStream_14, value);
	}

	inline static int32_t get_offset_of_queue_15() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___queue_15)); }
	inline Queue_t1288490777 * get_queue_15() const { return ___queue_15; }
	inline Queue_t1288490777 ** get_address_of_queue_15() { return &___queue_15; }
	inline void set_queue_15(Queue_t1288490777 * value)
	{
		___queue_15 = value;
		Il2CppCodeGenWriteBarrier(&___queue_15, value);
	}

	inline static int32_t get_offset_of_reused_16() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___reused_16)); }
	inline bool get_reused_16() const { return ___reused_16; }
	inline bool* get_address_of_reused_16() { return &___reused_16; }
	inline void set_reused_16(bool value)
	{
		___reused_16 = value;
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___position_17)); }
	inline int32_t get_position_17() const { return ___position_17; }
	inline int32_t* get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(int32_t value)
	{
		___position_17 = value;
	}

	inline static int32_t get_offset_of_priority_request_18() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___priority_request_18)); }
	inline HttpWebRequest_t1951404513 * get_priority_request_18() const { return ___priority_request_18; }
	inline HttpWebRequest_t1951404513 ** get_address_of_priority_request_18() { return &___priority_request_18; }
	inline void set_priority_request_18(HttpWebRequest_t1951404513 * value)
	{
		___priority_request_18 = value;
		Il2CppCodeGenWriteBarrier(&___priority_request_18, value);
	}

	inline static int32_t get_offset_of_ntlm_credentials_19() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___ntlm_credentials_19)); }
	inline NetworkCredential_t1714133953 * get_ntlm_credentials_19() const { return ___ntlm_credentials_19; }
	inline NetworkCredential_t1714133953 ** get_address_of_ntlm_credentials_19() { return &___ntlm_credentials_19; }
	inline void set_ntlm_credentials_19(NetworkCredential_t1714133953 * value)
	{
		___ntlm_credentials_19 = value;
		Il2CppCodeGenWriteBarrier(&___ntlm_credentials_19, value);
	}

	inline static int32_t get_offset_of_ntlm_authenticated_20() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___ntlm_authenticated_20)); }
	inline bool get_ntlm_authenticated_20() const { return ___ntlm_authenticated_20; }
	inline bool* get_address_of_ntlm_authenticated_20() { return &___ntlm_authenticated_20; }
	inline void set_ntlm_authenticated_20(bool value)
	{
		___ntlm_authenticated_20 = value;
	}

	inline static int32_t get_offset_of_unsafe_sharing_21() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___unsafe_sharing_21)); }
	inline bool get_unsafe_sharing_21() const { return ___unsafe_sharing_21; }
	inline bool* get_address_of_unsafe_sharing_21() { return &___unsafe_sharing_21; }
	inline void set_unsafe_sharing_21(bool value)
	{
		___unsafe_sharing_21 = value;
	}

	inline static int32_t get_offset_of_connect_ntlm_auth_state_22() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___connect_ntlm_auth_state_22)); }
	inline int32_t get_connect_ntlm_auth_state_22() const { return ___connect_ntlm_auth_state_22; }
	inline int32_t* get_address_of_connect_ntlm_auth_state_22() { return &___connect_ntlm_auth_state_22; }
	inline void set_connect_ntlm_auth_state_22(int32_t value)
	{
		___connect_ntlm_auth_state_22 = value;
	}

	inline static int32_t get_offset_of_connect_request_23() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___connect_request_23)); }
	inline HttpWebRequest_t1951404513 * get_connect_request_23() const { return ___connect_request_23; }
	inline HttpWebRequest_t1951404513 ** get_address_of_connect_request_23() { return &___connect_request_23; }
	inline void set_connect_request_23(HttpWebRequest_t1951404513 * value)
	{
		___connect_request_23 = value;
		Il2CppCodeGenWriteBarrier(&___connect_request_23, value);
	}

	inline static int32_t get_offset_of_connect_exception_24() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___connect_exception_24)); }
	inline Exception_t1927440687 * get_connect_exception_24() const { return ___connect_exception_24; }
	inline Exception_t1927440687 ** get_address_of_connect_exception_24() { return &___connect_exception_24; }
	inline void set_connect_exception_24(Exception_t1927440687 * value)
	{
		___connect_exception_24 = value;
		Il2CppCodeGenWriteBarrier(&___connect_exception_24, value);
	}

	inline static int32_t get_offset_of_tlsStream_26() { return static_cast<int32_t>(offsetof(WebConnection_t324679648, ___tlsStream_26)); }
	inline MonoTlsStream_t1249652258 * get_tlsStream_26() const { return ___tlsStream_26; }
	inline MonoTlsStream_t1249652258 ** get_address_of_tlsStream_26() { return &___tlsStream_26; }
	inline void set_tlsStream_26(MonoTlsStream_t1249652258 * value)
	{
		___tlsStream_26 = value;
		Il2CppCodeGenWriteBarrier(&___tlsStream_26, value);
	}
};

struct WebConnection_t324679648_StaticFields
{
public:
	// System.AsyncCallback System.Net.WebConnection::readDoneDelegate
	AsyncCallback_t163412349 * ___readDoneDelegate_9;
	// System.Object System.Net.WebConnection::classLock
	Il2CppObject * ___classLock_25;

public:
	inline static int32_t get_offset_of_readDoneDelegate_9() { return static_cast<int32_t>(offsetof(WebConnection_t324679648_StaticFields, ___readDoneDelegate_9)); }
	inline AsyncCallback_t163412349 * get_readDoneDelegate_9() const { return ___readDoneDelegate_9; }
	inline AsyncCallback_t163412349 ** get_address_of_readDoneDelegate_9() { return &___readDoneDelegate_9; }
	inline void set_readDoneDelegate_9(AsyncCallback_t163412349 * value)
	{
		___readDoneDelegate_9 = value;
		Il2CppCodeGenWriteBarrier(&___readDoneDelegate_9, value);
	}

	inline static int32_t get_offset_of_classLock_25() { return static_cast<int32_t>(offsetof(WebConnection_t324679648_StaticFields, ___classLock_25)); }
	inline Il2CppObject * get_classLock_25() const { return ___classLock_25; }
	inline Il2CppObject ** get_address_of_classLock_25() { return &___classLock_25; }
	inline void set_classLock_25(Il2CppObject * value)
	{
		___classLock_25 = value;
		Il2CppCodeGenWriteBarrier(&___classLock_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
