﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatypeVari2237606318.h"

// System.Xml.Schema.RestrictionFacets
struct RestrictionFacets_t4012658256;
// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_t1152094268;
// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t192177157;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;
// System.Xml.Schema.DatatypeImplementation[]
struct DatatypeImplementationU5BU5D_t3131202389;
// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[]
struct SchemaDatatypeMapU5BU5D_t863562528;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation
struct  DatatypeImplementation_t1152094268  : public XmlSchemaDatatype_t1195946242
{
public:
	// System.Xml.Schema.XmlSchemaDatatypeVariety System.Xml.Schema.DatatypeImplementation::variety
	int32_t ___variety_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.DatatypeImplementation::restriction
	RestrictionFacets_t4012658256 * ___restriction_1;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::baseType
	DatatypeImplementation_t1152094268 * ___baseType_2;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.DatatypeImplementation::valueConverter
	XmlValueConverter_t68179724 * ___valueConverter_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.DatatypeImplementation::parentSchemaType
	XmlSchemaType_t1795078578 * ___parentSchemaType_4;

public:
	inline static int32_t get_offset_of_variety_0() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___variety_0)); }
	inline int32_t get_variety_0() const { return ___variety_0; }
	inline int32_t* get_address_of_variety_0() { return &___variety_0; }
	inline void set_variety_0(int32_t value)
	{
		___variety_0 = value;
	}

	inline static int32_t get_offset_of_restriction_1() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___restriction_1)); }
	inline RestrictionFacets_t4012658256 * get_restriction_1() const { return ___restriction_1; }
	inline RestrictionFacets_t4012658256 ** get_address_of_restriction_1() { return &___restriction_1; }
	inline void set_restriction_1(RestrictionFacets_t4012658256 * value)
	{
		___restriction_1 = value;
		Il2CppCodeGenWriteBarrier(&___restriction_1, value);
	}

	inline static int32_t get_offset_of_baseType_2() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___baseType_2)); }
	inline DatatypeImplementation_t1152094268 * get_baseType_2() const { return ___baseType_2; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_baseType_2() { return &___baseType_2; }
	inline void set_baseType_2(DatatypeImplementation_t1152094268 * value)
	{
		___baseType_2 = value;
		Il2CppCodeGenWriteBarrier(&___baseType_2, value);
	}

	inline static int32_t get_offset_of_valueConverter_3() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___valueConverter_3)); }
	inline XmlValueConverter_t68179724 * get_valueConverter_3() const { return ___valueConverter_3; }
	inline XmlValueConverter_t68179724 ** get_address_of_valueConverter_3() { return &___valueConverter_3; }
	inline void set_valueConverter_3(XmlValueConverter_t68179724 * value)
	{
		___valueConverter_3 = value;
		Il2CppCodeGenWriteBarrier(&___valueConverter_3, value);
	}

	inline static int32_t get_offset_of_parentSchemaType_4() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___parentSchemaType_4)); }
	inline XmlSchemaType_t1795078578 * get_parentSchemaType_4() const { return ___parentSchemaType_4; }
	inline XmlSchemaType_t1795078578 ** get_address_of_parentSchemaType_4() { return &___parentSchemaType_4; }
	inline void set_parentSchemaType_4(XmlSchemaType_t1795078578 * value)
	{
		___parentSchemaType_4 = value;
		Il2CppCodeGenWriteBarrier(&___parentSchemaType_4, value);
	}
};

struct DatatypeImplementation_t1152094268_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Schema.DatatypeImplementation::builtinTypes
	Hashtable_t909839986 * ___builtinTypes_5;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.DatatypeImplementation::enumToTypeCode
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___enumToTypeCode_6;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anySimpleType
	XmlSchemaSimpleType_t248156492 * ___anySimpleType_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anyAtomicType
	XmlSchemaSimpleType_t248156492 * ___anyAtomicType_8;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::untypedAtomicType
	XmlSchemaSimpleType_t248156492 * ___untypedAtomicType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::yearMonthDurationType
	XmlSchemaSimpleType_t248156492 * ___yearMonthDurationType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::dayTimeDurationType
	XmlSchemaSimpleType_t248156492 * ___dayTimeDurationType_11;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::normalizedStringTypeV1Compat
	XmlSchemaSimpleType_t248156492 * ___normalizedStringTypeV1Compat_12;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::tokenTypeV1Compat
	XmlSchemaSimpleType_t248156492 * ___tokenTypeV1Compat_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnySimpleType
	XmlQualifiedName_t1944712516 * ___QnAnySimpleType_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnyType
	XmlQualifiedName_t1944712516 * ___QnAnyType_15;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::stringFacetsChecker
	FacetsChecker_t1235574227 * ___stringFacetsChecker_16;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::miscFacetsChecker
	FacetsChecker_t1235574227 * ___miscFacetsChecker_17;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::numeric2FacetsChecker
	FacetsChecker_t1235574227 * ___numeric2FacetsChecker_18;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::binaryFacetsChecker
	FacetsChecker_t1235574227 * ___binaryFacetsChecker_19;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::dateTimeFacetsChecker
	FacetsChecker_t1235574227 * ___dateTimeFacetsChecker_20;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::durationFacetsChecker
	FacetsChecker_t1235574227 * ___durationFacetsChecker_21;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::listFacetsChecker
	FacetsChecker_t1235574227 * ___listFacetsChecker_22;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::qnameFacetsChecker
	FacetsChecker_t1235574227 * ___qnameFacetsChecker_23;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::unionFacetsChecker
	FacetsChecker_t1235574227 * ___unionFacetsChecker_24;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anySimpleType
	DatatypeImplementation_t1152094268 * ___c_anySimpleType_25;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyURI
	DatatypeImplementation_t1152094268 * ___c_anyURI_26;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_base64Binary
	DatatypeImplementation_t1152094268 * ___c_base64Binary_27;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_boolean
	DatatypeImplementation_t1152094268 * ___c_boolean_28;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_byte
	DatatypeImplementation_t1152094268 * ___c_byte_29;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_char
	DatatypeImplementation_t1152094268 * ___c_char_30;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_date
	DatatypeImplementation_t1152094268 * ___c_date_31;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTime
	DatatypeImplementation_t1152094268 * ___c_dateTime_32;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeNoTz
	DatatypeImplementation_t1152094268 * ___c_dateTimeNoTz_33;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeTz
	DatatypeImplementation_t1152094268 * ___c_dateTimeTz_34;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_day
	DatatypeImplementation_t1152094268 * ___c_day_35;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_decimal
	DatatypeImplementation_t1152094268 * ___c_decimal_36;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_double
	DatatypeImplementation_t1152094268 * ___c_double_37;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_doubleXdr
	DatatypeImplementation_t1152094268 * ___c_doubleXdr_38;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_duration
	DatatypeImplementation_t1152094268 * ___c_duration_39;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITY
	DatatypeImplementation_t1152094268 * ___c_ENTITY_40;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITIES
	DatatypeImplementation_t1152094268 * ___c_ENTITIES_41;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENUMERATION
	DatatypeImplementation_t1152094268 * ___c_ENUMERATION_42;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_fixed
	DatatypeImplementation_t1152094268 * ___c_fixed_43;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_float
	DatatypeImplementation_t1152094268 * ___c_float_44;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_floatXdr
	DatatypeImplementation_t1152094268 * ___c_floatXdr_45;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_hexBinary
	DatatypeImplementation_t1152094268 * ___c_hexBinary_46;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ID
	DatatypeImplementation_t1152094268 * ___c_ID_47;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREF
	DatatypeImplementation_t1152094268 * ___c_IDREF_48;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREFS
	DatatypeImplementation_t1152094268 * ___c_IDREFS_49;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_int
	DatatypeImplementation_t1152094268 * ___c_int_50;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_integer
	DatatypeImplementation_t1152094268 * ___c_integer_51;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_language
	DatatypeImplementation_t1152094268 * ___c_language_52;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_long
	DatatypeImplementation_t1152094268 * ___c_long_53;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_month
	DatatypeImplementation_t1152094268 * ___c_month_54;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_monthDay
	DatatypeImplementation_t1152094268 * ___c_monthDay_55;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_Name
	DatatypeImplementation_t1152094268 * ___c_Name_56;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NCName
	DatatypeImplementation_t1152094268 * ___c_NCName_57;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_negativeInteger
	DatatypeImplementation_t1152094268 * ___c_negativeInteger_58;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKEN
	DatatypeImplementation_t1152094268 * ___c_NMTOKEN_59;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKENS
	DatatypeImplementation_t1152094268 * ___c_NMTOKENS_60;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonNegativeInteger
	DatatypeImplementation_t1152094268 * ___c_nonNegativeInteger_61;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonPositiveInteger
	DatatypeImplementation_t1152094268 * ___c_nonPositiveInteger_62;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedString
	DatatypeImplementation_t1152094268 * ___c_normalizedString_63;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NOTATION
	DatatypeImplementation_t1152094268 * ___c_NOTATION_64;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_positiveInteger
	DatatypeImplementation_t1152094268 * ___c_positiveInteger_65;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QName
	DatatypeImplementation_t1152094268 * ___c_QName_66;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QNameXdr
	DatatypeImplementation_t1152094268 * ___c_QNameXdr_67;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_short
	DatatypeImplementation_t1152094268 * ___c_short_68;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_string
	DatatypeImplementation_t1152094268 * ___c_string_69;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_time
	DatatypeImplementation_t1152094268 * ___c_time_70;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeNoTz
	DatatypeImplementation_t1152094268 * ___c_timeNoTz_71;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeTz
	DatatypeImplementation_t1152094268 * ___c_timeTz_72;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_token
	DatatypeImplementation_t1152094268 * ___c_token_73;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedByte
	DatatypeImplementation_t1152094268 * ___c_unsignedByte_74;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedInt
	DatatypeImplementation_t1152094268 * ___c_unsignedInt_75;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedLong
	DatatypeImplementation_t1152094268 * ___c_unsignedLong_76;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedShort
	DatatypeImplementation_t1152094268 * ___c_unsignedShort_77;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_uuid
	DatatypeImplementation_t1152094268 * ___c_uuid_78;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_year
	DatatypeImplementation_t1152094268 * ___c_year_79;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonth
	DatatypeImplementation_t1152094268 * ___c_yearMonth_80;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedStringV1Compat
	DatatypeImplementation_t1152094268 * ___c_normalizedStringV1Compat_81;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_tokenV1Compat
	DatatypeImplementation_t1152094268 * ___c_tokenV1Compat_82;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyAtomicType
	DatatypeImplementation_t1152094268 * ___c_anyAtomicType_83;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dayTimeDuration
	DatatypeImplementation_t1152094268 * ___c_dayTimeDuration_84;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_untypedAtomicType
	DatatypeImplementation_t1152094268 * ___c_untypedAtomicType_85;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonthDuration
	DatatypeImplementation_t1152094268 * ___c_yearMonthDuration_86;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypes
	DatatypeImplementationU5BU5D_t3131202389* ___c_tokenizedTypes_87;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypesXsd
	DatatypeImplementationU5BU5D_t3131202389* ___c_tokenizedTypesXsd_88;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XdrTypes
	SchemaDatatypeMapU5BU5D_t863562528* ___c_XdrTypes_89;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XsdTypes
	SchemaDatatypeMapU5BU5D_t863562528* ___c_XsdTypes_90;

public:
	inline static int32_t get_offset_of_builtinTypes_5() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___builtinTypes_5)); }
	inline Hashtable_t909839986 * get_builtinTypes_5() const { return ___builtinTypes_5; }
	inline Hashtable_t909839986 ** get_address_of_builtinTypes_5() { return &___builtinTypes_5; }
	inline void set_builtinTypes_5(Hashtable_t909839986 * value)
	{
		___builtinTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___builtinTypes_5, value);
	}

	inline static int32_t get_offset_of_enumToTypeCode_6() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___enumToTypeCode_6)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_enumToTypeCode_6() const { return ___enumToTypeCode_6; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_enumToTypeCode_6() { return &___enumToTypeCode_6; }
	inline void set_enumToTypeCode_6(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___enumToTypeCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___enumToTypeCode_6, value);
	}

	inline static int32_t get_offset_of_anySimpleType_7() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___anySimpleType_7)); }
	inline XmlSchemaSimpleType_t248156492 * get_anySimpleType_7() const { return ___anySimpleType_7; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_anySimpleType_7() { return &___anySimpleType_7; }
	inline void set_anySimpleType_7(XmlSchemaSimpleType_t248156492 * value)
	{
		___anySimpleType_7 = value;
		Il2CppCodeGenWriteBarrier(&___anySimpleType_7, value);
	}

	inline static int32_t get_offset_of_anyAtomicType_8() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___anyAtomicType_8)); }
	inline XmlSchemaSimpleType_t248156492 * get_anyAtomicType_8() const { return ___anyAtomicType_8; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_anyAtomicType_8() { return &___anyAtomicType_8; }
	inline void set_anyAtomicType_8(XmlSchemaSimpleType_t248156492 * value)
	{
		___anyAtomicType_8 = value;
		Il2CppCodeGenWriteBarrier(&___anyAtomicType_8, value);
	}

	inline static int32_t get_offset_of_untypedAtomicType_9() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___untypedAtomicType_9)); }
	inline XmlSchemaSimpleType_t248156492 * get_untypedAtomicType_9() const { return ___untypedAtomicType_9; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_untypedAtomicType_9() { return &___untypedAtomicType_9; }
	inline void set_untypedAtomicType_9(XmlSchemaSimpleType_t248156492 * value)
	{
		___untypedAtomicType_9 = value;
		Il2CppCodeGenWriteBarrier(&___untypedAtomicType_9, value);
	}

	inline static int32_t get_offset_of_yearMonthDurationType_10() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___yearMonthDurationType_10)); }
	inline XmlSchemaSimpleType_t248156492 * get_yearMonthDurationType_10() const { return ___yearMonthDurationType_10; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_yearMonthDurationType_10() { return &___yearMonthDurationType_10; }
	inline void set_yearMonthDurationType_10(XmlSchemaSimpleType_t248156492 * value)
	{
		___yearMonthDurationType_10 = value;
		Il2CppCodeGenWriteBarrier(&___yearMonthDurationType_10, value);
	}

	inline static int32_t get_offset_of_dayTimeDurationType_11() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___dayTimeDurationType_11)); }
	inline XmlSchemaSimpleType_t248156492 * get_dayTimeDurationType_11() const { return ___dayTimeDurationType_11; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_dayTimeDurationType_11() { return &___dayTimeDurationType_11; }
	inline void set_dayTimeDurationType_11(XmlSchemaSimpleType_t248156492 * value)
	{
		___dayTimeDurationType_11 = value;
		Il2CppCodeGenWriteBarrier(&___dayTimeDurationType_11, value);
	}

	inline static int32_t get_offset_of_normalizedStringTypeV1Compat_12() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___normalizedStringTypeV1Compat_12)); }
	inline XmlSchemaSimpleType_t248156492 * get_normalizedStringTypeV1Compat_12() const { return ___normalizedStringTypeV1Compat_12; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_normalizedStringTypeV1Compat_12() { return &___normalizedStringTypeV1Compat_12; }
	inline void set_normalizedStringTypeV1Compat_12(XmlSchemaSimpleType_t248156492 * value)
	{
		___normalizedStringTypeV1Compat_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedStringTypeV1Compat_12, value);
	}

	inline static int32_t get_offset_of_tokenTypeV1Compat_13() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___tokenTypeV1Compat_13)); }
	inline XmlSchemaSimpleType_t248156492 * get_tokenTypeV1Compat_13() const { return ___tokenTypeV1Compat_13; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_tokenTypeV1Compat_13() { return &___tokenTypeV1Compat_13; }
	inline void set_tokenTypeV1Compat_13(XmlSchemaSimpleType_t248156492 * value)
	{
		___tokenTypeV1Compat_13 = value;
		Il2CppCodeGenWriteBarrier(&___tokenTypeV1Compat_13, value);
	}

	inline static int32_t get_offset_of_QnAnySimpleType_14() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___QnAnySimpleType_14)); }
	inline XmlQualifiedName_t1944712516 * get_QnAnySimpleType_14() const { return ___QnAnySimpleType_14; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAnySimpleType_14() { return &___QnAnySimpleType_14; }
	inline void set_QnAnySimpleType_14(XmlQualifiedName_t1944712516 * value)
	{
		___QnAnySimpleType_14 = value;
		Il2CppCodeGenWriteBarrier(&___QnAnySimpleType_14, value);
	}

	inline static int32_t get_offset_of_QnAnyType_15() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___QnAnyType_15)); }
	inline XmlQualifiedName_t1944712516 * get_QnAnyType_15() const { return ___QnAnyType_15; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAnyType_15() { return &___QnAnyType_15; }
	inline void set_QnAnyType_15(XmlQualifiedName_t1944712516 * value)
	{
		___QnAnyType_15 = value;
		Il2CppCodeGenWriteBarrier(&___QnAnyType_15, value);
	}

	inline static int32_t get_offset_of_stringFacetsChecker_16() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___stringFacetsChecker_16)); }
	inline FacetsChecker_t1235574227 * get_stringFacetsChecker_16() const { return ___stringFacetsChecker_16; }
	inline FacetsChecker_t1235574227 ** get_address_of_stringFacetsChecker_16() { return &___stringFacetsChecker_16; }
	inline void set_stringFacetsChecker_16(FacetsChecker_t1235574227 * value)
	{
		___stringFacetsChecker_16 = value;
		Il2CppCodeGenWriteBarrier(&___stringFacetsChecker_16, value);
	}

	inline static int32_t get_offset_of_miscFacetsChecker_17() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___miscFacetsChecker_17)); }
	inline FacetsChecker_t1235574227 * get_miscFacetsChecker_17() const { return ___miscFacetsChecker_17; }
	inline FacetsChecker_t1235574227 ** get_address_of_miscFacetsChecker_17() { return &___miscFacetsChecker_17; }
	inline void set_miscFacetsChecker_17(FacetsChecker_t1235574227 * value)
	{
		___miscFacetsChecker_17 = value;
		Il2CppCodeGenWriteBarrier(&___miscFacetsChecker_17, value);
	}

	inline static int32_t get_offset_of_numeric2FacetsChecker_18() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___numeric2FacetsChecker_18)); }
	inline FacetsChecker_t1235574227 * get_numeric2FacetsChecker_18() const { return ___numeric2FacetsChecker_18; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric2FacetsChecker_18() { return &___numeric2FacetsChecker_18; }
	inline void set_numeric2FacetsChecker_18(FacetsChecker_t1235574227 * value)
	{
		___numeric2FacetsChecker_18 = value;
		Il2CppCodeGenWriteBarrier(&___numeric2FacetsChecker_18, value);
	}

	inline static int32_t get_offset_of_binaryFacetsChecker_19() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___binaryFacetsChecker_19)); }
	inline FacetsChecker_t1235574227 * get_binaryFacetsChecker_19() const { return ___binaryFacetsChecker_19; }
	inline FacetsChecker_t1235574227 ** get_address_of_binaryFacetsChecker_19() { return &___binaryFacetsChecker_19; }
	inline void set_binaryFacetsChecker_19(FacetsChecker_t1235574227 * value)
	{
		___binaryFacetsChecker_19 = value;
		Il2CppCodeGenWriteBarrier(&___binaryFacetsChecker_19, value);
	}

	inline static int32_t get_offset_of_dateTimeFacetsChecker_20() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___dateTimeFacetsChecker_20)); }
	inline FacetsChecker_t1235574227 * get_dateTimeFacetsChecker_20() const { return ___dateTimeFacetsChecker_20; }
	inline FacetsChecker_t1235574227 ** get_address_of_dateTimeFacetsChecker_20() { return &___dateTimeFacetsChecker_20; }
	inline void set_dateTimeFacetsChecker_20(FacetsChecker_t1235574227 * value)
	{
		___dateTimeFacetsChecker_20 = value;
		Il2CppCodeGenWriteBarrier(&___dateTimeFacetsChecker_20, value);
	}

	inline static int32_t get_offset_of_durationFacetsChecker_21() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___durationFacetsChecker_21)); }
	inline FacetsChecker_t1235574227 * get_durationFacetsChecker_21() const { return ___durationFacetsChecker_21; }
	inline FacetsChecker_t1235574227 ** get_address_of_durationFacetsChecker_21() { return &___durationFacetsChecker_21; }
	inline void set_durationFacetsChecker_21(FacetsChecker_t1235574227 * value)
	{
		___durationFacetsChecker_21 = value;
		Il2CppCodeGenWriteBarrier(&___durationFacetsChecker_21, value);
	}

	inline static int32_t get_offset_of_listFacetsChecker_22() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___listFacetsChecker_22)); }
	inline FacetsChecker_t1235574227 * get_listFacetsChecker_22() const { return ___listFacetsChecker_22; }
	inline FacetsChecker_t1235574227 ** get_address_of_listFacetsChecker_22() { return &___listFacetsChecker_22; }
	inline void set_listFacetsChecker_22(FacetsChecker_t1235574227 * value)
	{
		___listFacetsChecker_22 = value;
		Il2CppCodeGenWriteBarrier(&___listFacetsChecker_22, value);
	}

	inline static int32_t get_offset_of_qnameFacetsChecker_23() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___qnameFacetsChecker_23)); }
	inline FacetsChecker_t1235574227 * get_qnameFacetsChecker_23() const { return ___qnameFacetsChecker_23; }
	inline FacetsChecker_t1235574227 ** get_address_of_qnameFacetsChecker_23() { return &___qnameFacetsChecker_23; }
	inline void set_qnameFacetsChecker_23(FacetsChecker_t1235574227 * value)
	{
		___qnameFacetsChecker_23 = value;
		Il2CppCodeGenWriteBarrier(&___qnameFacetsChecker_23, value);
	}

	inline static int32_t get_offset_of_unionFacetsChecker_24() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___unionFacetsChecker_24)); }
	inline FacetsChecker_t1235574227 * get_unionFacetsChecker_24() const { return ___unionFacetsChecker_24; }
	inline FacetsChecker_t1235574227 ** get_address_of_unionFacetsChecker_24() { return &___unionFacetsChecker_24; }
	inline void set_unionFacetsChecker_24(FacetsChecker_t1235574227 * value)
	{
		___unionFacetsChecker_24 = value;
		Il2CppCodeGenWriteBarrier(&___unionFacetsChecker_24, value);
	}

	inline static int32_t get_offset_of_c_anySimpleType_25() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anySimpleType_25)); }
	inline DatatypeImplementation_t1152094268 * get_c_anySimpleType_25() const { return ___c_anySimpleType_25; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anySimpleType_25() { return &___c_anySimpleType_25; }
	inline void set_c_anySimpleType_25(DatatypeImplementation_t1152094268 * value)
	{
		___c_anySimpleType_25 = value;
		Il2CppCodeGenWriteBarrier(&___c_anySimpleType_25, value);
	}

	inline static int32_t get_offset_of_c_anyURI_26() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anyURI_26)); }
	inline DatatypeImplementation_t1152094268 * get_c_anyURI_26() const { return ___c_anyURI_26; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anyURI_26() { return &___c_anyURI_26; }
	inline void set_c_anyURI_26(DatatypeImplementation_t1152094268 * value)
	{
		___c_anyURI_26 = value;
		Il2CppCodeGenWriteBarrier(&___c_anyURI_26, value);
	}

	inline static int32_t get_offset_of_c_base64Binary_27() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_base64Binary_27)); }
	inline DatatypeImplementation_t1152094268 * get_c_base64Binary_27() const { return ___c_base64Binary_27; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_base64Binary_27() { return &___c_base64Binary_27; }
	inline void set_c_base64Binary_27(DatatypeImplementation_t1152094268 * value)
	{
		___c_base64Binary_27 = value;
		Il2CppCodeGenWriteBarrier(&___c_base64Binary_27, value);
	}

	inline static int32_t get_offset_of_c_boolean_28() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_boolean_28)); }
	inline DatatypeImplementation_t1152094268 * get_c_boolean_28() const { return ___c_boolean_28; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_boolean_28() { return &___c_boolean_28; }
	inline void set_c_boolean_28(DatatypeImplementation_t1152094268 * value)
	{
		___c_boolean_28 = value;
		Il2CppCodeGenWriteBarrier(&___c_boolean_28, value);
	}

	inline static int32_t get_offset_of_c_byte_29() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_byte_29)); }
	inline DatatypeImplementation_t1152094268 * get_c_byte_29() const { return ___c_byte_29; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_byte_29() { return &___c_byte_29; }
	inline void set_c_byte_29(DatatypeImplementation_t1152094268 * value)
	{
		___c_byte_29 = value;
		Il2CppCodeGenWriteBarrier(&___c_byte_29, value);
	}

	inline static int32_t get_offset_of_c_char_30() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_char_30)); }
	inline DatatypeImplementation_t1152094268 * get_c_char_30() const { return ___c_char_30; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_char_30() { return &___c_char_30; }
	inline void set_c_char_30(DatatypeImplementation_t1152094268 * value)
	{
		___c_char_30 = value;
		Il2CppCodeGenWriteBarrier(&___c_char_30, value);
	}

	inline static int32_t get_offset_of_c_date_31() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_date_31)); }
	inline DatatypeImplementation_t1152094268 * get_c_date_31() const { return ___c_date_31; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_date_31() { return &___c_date_31; }
	inline void set_c_date_31(DatatypeImplementation_t1152094268 * value)
	{
		___c_date_31 = value;
		Il2CppCodeGenWriteBarrier(&___c_date_31, value);
	}

	inline static int32_t get_offset_of_c_dateTime_32() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTime_32)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTime_32() const { return ___c_dateTime_32; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTime_32() { return &___c_dateTime_32; }
	inline void set_c_dateTime_32(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTime_32 = value;
		Il2CppCodeGenWriteBarrier(&___c_dateTime_32, value);
	}

	inline static int32_t get_offset_of_c_dateTimeNoTz_33() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTimeNoTz_33)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTimeNoTz_33() const { return ___c_dateTimeNoTz_33; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTimeNoTz_33() { return &___c_dateTimeNoTz_33; }
	inline void set_c_dateTimeNoTz_33(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTimeNoTz_33 = value;
		Il2CppCodeGenWriteBarrier(&___c_dateTimeNoTz_33, value);
	}

	inline static int32_t get_offset_of_c_dateTimeTz_34() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTimeTz_34)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTimeTz_34() const { return ___c_dateTimeTz_34; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTimeTz_34() { return &___c_dateTimeTz_34; }
	inline void set_c_dateTimeTz_34(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTimeTz_34 = value;
		Il2CppCodeGenWriteBarrier(&___c_dateTimeTz_34, value);
	}

	inline static int32_t get_offset_of_c_day_35() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_day_35)); }
	inline DatatypeImplementation_t1152094268 * get_c_day_35() const { return ___c_day_35; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_day_35() { return &___c_day_35; }
	inline void set_c_day_35(DatatypeImplementation_t1152094268 * value)
	{
		___c_day_35 = value;
		Il2CppCodeGenWriteBarrier(&___c_day_35, value);
	}

	inline static int32_t get_offset_of_c_decimal_36() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_decimal_36)); }
	inline DatatypeImplementation_t1152094268 * get_c_decimal_36() const { return ___c_decimal_36; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_decimal_36() { return &___c_decimal_36; }
	inline void set_c_decimal_36(DatatypeImplementation_t1152094268 * value)
	{
		___c_decimal_36 = value;
		Il2CppCodeGenWriteBarrier(&___c_decimal_36, value);
	}

	inline static int32_t get_offset_of_c_double_37() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_double_37)); }
	inline DatatypeImplementation_t1152094268 * get_c_double_37() const { return ___c_double_37; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_double_37() { return &___c_double_37; }
	inline void set_c_double_37(DatatypeImplementation_t1152094268 * value)
	{
		___c_double_37 = value;
		Il2CppCodeGenWriteBarrier(&___c_double_37, value);
	}

	inline static int32_t get_offset_of_c_doubleXdr_38() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_doubleXdr_38)); }
	inline DatatypeImplementation_t1152094268 * get_c_doubleXdr_38() const { return ___c_doubleXdr_38; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_doubleXdr_38() { return &___c_doubleXdr_38; }
	inline void set_c_doubleXdr_38(DatatypeImplementation_t1152094268 * value)
	{
		___c_doubleXdr_38 = value;
		Il2CppCodeGenWriteBarrier(&___c_doubleXdr_38, value);
	}

	inline static int32_t get_offset_of_c_duration_39() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_duration_39)); }
	inline DatatypeImplementation_t1152094268 * get_c_duration_39() const { return ___c_duration_39; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_duration_39() { return &___c_duration_39; }
	inline void set_c_duration_39(DatatypeImplementation_t1152094268 * value)
	{
		___c_duration_39 = value;
		Il2CppCodeGenWriteBarrier(&___c_duration_39, value);
	}

	inline static int32_t get_offset_of_c_ENTITY_40() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENTITY_40)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENTITY_40() const { return ___c_ENTITY_40; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENTITY_40() { return &___c_ENTITY_40; }
	inline void set_c_ENTITY_40(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENTITY_40 = value;
		Il2CppCodeGenWriteBarrier(&___c_ENTITY_40, value);
	}

	inline static int32_t get_offset_of_c_ENTITIES_41() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENTITIES_41)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENTITIES_41() const { return ___c_ENTITIES_41; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENTITIES_41() { return &___c_ENTITIES_41; }
	inline void set_c_ENTITIES_41(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENTITIES_41 = value;
		Il2CppCodeGenWriteBarrier(&___c_ENTITIES_41, value);
	}

	inline static int32_t get_offset_of_c_ENUMERATION_42() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENUMERATION_42)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENUMERATION_42() const { return ___c_ENUMERATION_42; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENUMERATION_42() { return &___c_ENUMERATION_42; }
	inline void set_c_ENUMERATION_42(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENUMERATION_42 = value;
		Il2CppCodeGenWriteBarrier(&___c_ENUMERATION_42, value);
	}

	inline static int32_t get_offset_of_c_fixed_43() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_fixed_43)); }
	inline DatatypeImplementation_t1152094268 * get_c_fixed_43() const { return ___c_fixed_43; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_fixed_43() { return &___c_fixed_43; }
	inline void set_c_fixed_43(DatatypeImplementation_t1152094268 * value)
	{
		___c_fixed_43 = value;
		Il2CppCodeGenWriteBarrier(&___c_fixed_43, value);
	}

	inline static int32_t get_offset_of_c_float_44() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_float_44)); }
	inline DatatypeImplementation_t1152094268 * get_c_float_44() const { return ___c_float_44; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_float_44() { return &___c_float_44; }
	inline void set_c_float_44(DatatypeImplementation_t1152094268 * value)
	{
		___c_float_44 = value;
		Il2CppCodeGenWriteBarrier(&___c_float_44, value);
	}

	inline static int32_t get_offset_of_c_floatXdr_45() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_floatXdr_45)); }
	inline DatatypeImplementation_t1152094268 * get_c_floatXdr_45() const { return ___c_floatXdr_45; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_floatXdr_45() { return &___c_floatXdr_45; }
	inline void set_c_floatXdr_45(DatatypeImplementation_t1152094268 * value)
	{
		___c_floatXdr_45 = value;
		Il2CppCodeGenWriteBarrier(&___c_floatXdr_45, value);
	}

	inline static int32_t get_offset_of_c_hexBinary_46() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_hexBinary_46)); }
	inline DatatypeImplementation_t1152094268 * get_c_hexBinary_46() const { return ___c_hexBinary_46; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_hexBinary_46() { return &___c_hexBinary_46; }
	inline void set_c_hexBinary_46(DatatypeImplementation_t1152094268 * value)
	{
		___c_hexBinary_46 = value;
		Il2CppCodeGenWriteBarrier(&___c_hexBinary_46, value);
	}

	inline static int32_t get_offset_of_c_ID_47() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ID_47)); }
	inline DatatypeImplementation_t1152094268 * get_c_ID_47() const { return ___c_ID_47; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ID_47() { return &___c_ID_47; }
	inline void set_c_ID_47(DatatypeImplementation_t1152094268 * value)
	{
		___c_ID_47 = value;
		Il2CppCodeGenWriteBarrier(&___c_ID_47, value);
	}

	inline static int32_t get_offset_of_c_IDREF_48() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_IDREF_48)); }
	inline DatatypeImplementation_t1152094268 * get_c_IDREF_48() const { return ___c_IDREF_48; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_IDREF_48() { return &___c_IDREF_48; }
	inline void set_c_IDREF_48(DatatypeImplementation_t1152094268 * value)
	{
		___c_IDREF_48 = value;
		Il2CppCodeGenWriteBarrier(&___c_IDREF_48, value);
	}

	inline static int32_t get_offset_of_c_IDREFS_49() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_IDREFS_49)); }
	inline DatatypeImplementation_t1152094268 * get_c_IDREFS_49() const { return ___c_IDREFS_49; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_IDREFS_49() { return &___c_IDREFS_49; }
	inline void set_c_IDREFS_49(DatatypeImplementation_t1152094268 * value)
	{
		___c_IDREFS_49 = value;
		Il2CppCodeGenWriteBarrier(&___c_IDREFS_49, value);
	}

	inline static int32_t get_offset_of_c_int_50() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_int_50)); }
	inline DatatypeImplementation_t1152094268 * get_c_int_50() const { return ___c_int_50; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_int_50() { return &___c_int_50; }
	inline void set_c_int_50(DatatypeImplementation_t1152094268 * value)
	{
		___c_int_50 = value;
		Il2CppCodeGenWriteBarrier(&___c_int_50, value);
	}

	inline static int32_t get_offset_of_c_integer_51() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_integer_51)); }
	inline DatatypeImplementation_t1152094268 * get_c_integer_51() const { return ___c_integer_51; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_integer_51() { return &___c_integer_51; }
	inline void set_c_integer_51(DatatypeImplementation_t1152094268 * value)
	{
		___c_integer_51 = value;
		Il2CppCodeGenWriteBarrier(&___c_integer_51, value);
	}

	inline static int32_t get_offset_of_c_language_52() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_language_52)); }
	inline DatatypeImplementation_t1152094268 * get_c_language_52() const { return ___c_language_52; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_language_52() { return &___c_language_52; }
	inline void set_c_language_52(DatatypeImplementation_t1152094268 * value)
	{
		___c_language_52 = value;
		Il2CppCodeGenWriteBarrier(&___c_language_52, value);
	}

	inline static int32_t get_offset_of_c_long_53() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_long_53)); }
	inline DatatypeImplementation_t1152094268 * get_c_long_53() const { return ___c_long_53; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_long_53() { return &___c_long_53; }
	inline void set_c_long_53(DatatypeImplementation_t1152094268 * value)
	{
		___c_long_53 = value;
		Il2CppCodeGenWriteBarrier(&___c_long_53, value);
	}

	inline static int32_t get_offset_of_c_month_54() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_month_54)); }
	inline DatatypeImplementation_t1152094268 * get_c_month_54() const { return ___c_month_54; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_month_54() { return &___c_month_54; }
	inline void set_c_month_54(DatatypeImplementation_t1152094268 * value)
	{
		___c_month_54 = value;
		Il2CppCodeGenWriteBarrier(&___c_month_54, value);
	}

	inline static int32_t get_offset_of_c_monthDay_55() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_monthDay_55)); }
	inline DatatypeImplementation_t1152094268 * get_c_monthDay_55() const { return ___c_monthDay_55; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_monthDay_55() { return &___c_monthDay_55; }
	inline void set_c_monthDay_55(DatatypeImplementation_t1152094268 * value)
	{
		___c_monthDay_55 = value;
		Il2CppCodeGenWriteBarrier(&___c_monthDay_55, value);
	}

	inline static int32_t get_offset_of_c_Name_56() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_Name_56)); }
	inline DatatypeImplementation_t1152094268 * get_c_Name_56() const { return ___c_Name_56; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_Name_56() { return &___c_Name_56; }
	inline void set_c_Name_56(DatatypeImplementation_t1152094268 * value)
	{
		___c_Name_56 = value;
		Il2CppCodeGenWriteBarrier(&___c_Name_56, value);
	}

	inline static int32_t get_offset_of_c_NCName_57() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NCName_57)); }
	inline DatatypeImplementation_t1152094268 * get_c_NCName_57() const { return ___c_NCName_57; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NCName_57() { return &___c_NCName_57; }
	inline void set_c_NCName_57(DatatypeImplementation_t1152094268 * value)
	{
		___c_NCName_57 = value;
		Il2CppCodeGenWriteBarrier(&___c_NCName_57, value);
	}

	inline static int32_t get_offset_of_c_negativeInteger_58() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_negativeInteger_58)); }
	inline DatatypeImplementation_t1152094268 * get_c_negativeInteger_58() const { return ___c_negativeInteger_58; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_negativeInteger_58() { return &___c_negativeInteger_58; }
	inline void set_c_negativeInteger_58(DatatypeImplementation_t1152094268 * value)
	{
		___c_negativeInteger_58 = value;
		Il2CppCodeGenWriteBarrier(&___c_negativeInteger_58, value);
	}

	inline static int32_t get_offset_of_c_NMTOKEN_59() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NMTOKEN_59)); }
	inline DatatypeImplementation_t1152094268 * get_c_NMTOKEN_59() const { return ___c_NMTOKEN_59; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NMTOKEN_59() { return &___c_NMTOKEN_59; }
	inline void set_c_NMTOKEN_59(DatatypeImplementation_t1152094268 * value)
	{
		___c_NMTOKEN_59 = value;
		Il2CppCodeGenWriteBarrier(&___c_NMTOKEN_59, value);
	}

	inline static int32_t get_offset_of_c_NMTOKENS_60() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NMTOKENS_60)); }
	inline DatatypeImplementation_t1152094268 * get_c_NMTOKENS_60() const { return ___c_NMTOKENS_60; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NMTOKENS_60() { return &___c_NMTOKENS_60; }
	inline void set_c_NMTOKENS_60(DatatypeImplementation_t1152094268 * value)
	{
		___c_NMTOKENS_60 = value;
		Il2CppCodeGenWriteBarrier(&___c_NMTOKENS_60, value);
	}

	inline static int32_t get_offset_of_c_nonNegativeInteger_61() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_nonNegativeInteger_61)); }
	inline DatatypeImplementation_t1152094268 * get_c_nonNegativeInteger_61() const { return ___c_nonNegativeInteger_61; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_nonNegativeInteger_61() { return &___c_nonNegativeInteger_61; }
	inline void set_c_nonNegativeInteger_61(DatatypeImplementation_t1152094268 * value)
	{
		___c_nonNegativeInteger_61 = value;
		Il2CppCodeGenWriteBarrier(&___c_nonNegativeInteger_61, value);
	}

	inline static int32_t get_offset_of_c_nonPositiveInteger_62() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_nonPositiveInteger_62)); }
	inline DatatypeImplementation_t1152094268 * get_c_nonPositiveInteger_62() const { return ___c_nonPositiveInteger_62; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_nonPositiveInteger_62() { return &___c_nonPositiveInteger_62; }
	inline void set_c_nonPositiveInteger_62(DatatypeImplementation_t1152094268 * value)
	{
		___c_nonPositiveInteger_62 = value;
		Il2CppCodeGenWriteBarrier(&___c_nonPositiveInteger_62, value);
	}

	inline static int32_t get_offset_of_c_normalizedString_63() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_normalizedString_63)); }
	inline DatatypeImplementation_t1152094268 * get_c_normalizedString_63() const { return ___c_normalizedString_63; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_normalizedString_63() { return &___c_normalizedString_63; }
	inline void set_c_normalizedString_63(DatatypeImplementation_t1152094268 * value)
	{
		___c_normalizedString_63 = value;
		Il2CppCodeGenWriteBarrier(&___c_normalizedString_63, value);
	}

	inline static int32_t get_offset_of_c_NOTATION_64() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NOTATION_64)); }
	inline DatatypeImplementation_t1152094268 * get_c_NOTATION_64() const { return ___c_NOTATION_64; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NOTATION_64() { return &___c_NOTATION_64; }
	inline void set_c_NOTATION_64(DatatypeImplementation_t1152094268 * value)
	{
		___c_NOTATION_64 = value;
		Il2CppCodeGenWriteBarrier(&___c_NOTATION_64, value);
	}

	inline static int32_t get_offset_of_c_positiveInteger_65() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_positiveInteger_65)); }
	inline DatatypeImplementation_t1152094268 * get_c_positiveInteger_65() const { return ___c_positiveInteger_65; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_positiveInteger_65() { return &___c_positiveInteger_65; }
	inline void set_c_positiveInteger_65(DatatypeImplementation_t1152094268 * value)
	{
		___c_positiveInteger_65 = value;
		Il2CppCodeGenWriteBarrier(&___c_positiveInteger_65, value);
	}

	inline static int32_t get_offset_of_c_QName_66() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_QName_66)); }
	inline DatatypeImplementation_t1152094268 * get_c_QName_66() const { return ___c_QName_66; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_QName_66() { return &___c_QName_66; }
	inline void set_c_QName_66(DatatypeImplementation_t1152094268 * value)
	{
		___c_QName_66 = value;
		Il2CppCodeGenWriteBarrier(&___c_QName_66, value);
	}

	inline static int32_t get_offset_of_c_QNameXdr_67() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_QNameXdr_67)); }
	inline DatatypeImplementation_t1152094268 * get_c_QNameXdr_67() const { return ___c_QNameXdr_67; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_QNameXdr_67() { return &___c_QNameXdr_67; }
	inline void set_c_QNameXdr_67(DatatypeImplementation_t1152094268 * value)
	{
		___c_QNameXdr_67 = value;
		Il2CppCodeGenWriteBarrier(&___c_QNameXdr_67, value);
	}

	inline static int32_t get_offset_of_c_short_68() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_short_68)); }
	inline DatatypeImplementation_t1152094268 * get_c_short_68() const { return ___c_short_68; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_short_68() { return &___c_short_68; }
	inline void set_c_short_68(DatatypeImplementation_t1152094268 * value)
	{
		___c_short_68 = value;
		Il2CppCodeGenWriteBarrier(&___c_short_68, value);
	}

	inline static int32_t get_offset_of_c_string_69() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_string_69)); }
	inline DatatypeImplementation_t1152094268 * get_c_string_69() const { return ___c_string_69; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_string_69() { return &___c_string_69; }
	inline void set_c_string_69(DatatypeImplementation_t1152094268 * value)
	{
		___c_string_69 = value;
		Il2CppCodeGenWriteBarrier(&___c_string_69, value);
	}

	inline static int32_t get_offset_of_c_time_70() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_time_70)); }
	inline DatatypeImplementation_t1152094268 * get_c_time_70() const { return ___c_time_70; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_time_70() { return &___c_time_70; }
	inline void set_c_time_70(DatatypeImplementation_t1152094268 * value)
	{
		___c_time_70 = value;
		Il2CppCodeGenWriteBarrier(&___c_time_70, value);
	}

	inline static int32_t get_offset_of_c_timeNoTz_71() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_timeNoTz_71)); }
	inline DatatypeImplementation_t1152094268 * get_c_timeNoTz_71() const { return ___c_timeNoTz_71; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_timeNoTz_71() { return &___c_timeNoTz_71; }
	inline void set_c_timeNoTz_71(DatatypeImplementation_t1152094268 * value)
	{
		___c_timeNoTz_71 = value;
		Il2CppCodeGenWriteBarrier(&___c_timeNoTz_71, value);
	}

	inline static int32_t get_offset_of_c_timeTz_72() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_timeTz_72)); }
	inline DatatypeImplementation_t1152094268 * get_c_timeTz_72() const { return ___c_timeTz_72; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_timeTz_72() { return &___c_timeTz_72; }
	inline void set_c_timeTz_72(DatatypeImplementation_t1152094268 * value)
	{
		___c_timeTz_72 = value;
		Il2CppCodeGenWriteBarrier(&___c_timeTz_72, value);
	}

	inline static int32_t get_offset_of_c_token_73() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_token_73)); }
	inline DatatypeImplementation_t1152094268 * get_c_token_73() const { return ___c_token_73; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_token_73() { return &___c_token_73; }
	inline void set_c_token_73(DatatypeImplementation_t1152094268 * value)
	{
		___c_token_73 = value;
		Il2CppCodeGenWriteBarrier(&___c_token_73, value);
	}

	inline static int32_t get_offset_of_c_unsignedByte_74() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedByte_74)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedByte_74() const { return ___c_unsignedByte_74; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedByte_74() { return &___c_unsignedByte_74; }
	inline void set_c_unsignedByte_74(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedByte_74 = value;
		Il2CppCodeGenWriteBarrier(&___c_unsignedByte_74, value);
	}

	inline static int32_t get_offset_of_c_unsignedInt_75() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedInt_75)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedInt_75() const { return ___c_unsignedInt_75; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedInt_75() { return &___c_unsignedInt_75; }
	inline void set_c_unsignedInt_75(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedInt_75 = value;
		Il2CppCodeGenWriteBarrier(&___c_unsignedInt_75, value);
	}

	inline static int32_t get_offset_of_c_unsignedLong_76() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedLong_76)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedLong_76() const { return ___c_unsignedLong_76; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedLong_76() { return &___c_unsignedLong_76; }
	inline void set_c_unsignedLong_76(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedLong_76 = value;
		Il2CppCodeGenWriteBarrier(&___c_unsignedLong_76, value);
	}

	inline static int32_t get_offset_of_c_unsignedShort_77() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedShort_77)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedShort_77() const { return ___c_unsignedShort_77; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedShort_77() { return &___c_unsignedShort_77; }
	inline void set_c_unsignedShort_77(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedShort_77 = value;
		Il2CppCodeGenWriteBarrier(&___c_unsignedShort_77, value);
	}

	inline static int32_t get_offset_of_c_uuid_78() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_uuid_78)); }
	inline DatatypeImplementation_t1152094268 * get_c_uuid_78() const { return ___c_uuid_78; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_uuid_78() { return &___c_uuid_78; }
	inline void set_c_uuid_78(DatatypeImplementation_t1152094268 * value)
	{
		___c_uuid_78 = value;
		Il2CppCodeGenWriteBarrier(&___c_uuid_78, value);
	}

	inline static int32_t get_offset_of_c_year_79() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_year_79)); }
	inline DatatypeImplementation_t1152094268 * get_c_year_79() const { return ___c_year_79; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_year_79() { return &___c_year_79; }
	inline void set_c_year_79(DatatypeImplementation_t1152094268 * value)
	{
		___c_year_79 = value;
		Il2CppCodeGenWriteBarrier(&___c_year_79, value);
	}

	inline static int32_t get_offset_of_c_yearMonth_80() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_yearMonth_80)); }
	inline DatatypeImplementation_t1152094268 * get_c_yearMonth_80() const { return ___c_yearMonth_80; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_yearMonth_80() { return &___c_yearMonth_80; }
	inline void set_c_yearMonth_80(DatatypeImplementation_t1152094268 * value)
	{
		___c_yearMonth_80 = value;
		Il2CppCodeGenWriteBarrier(&___c_yearMonth_80, value);
	}

	inline static int32_t get_offset_of_c_normalizedStringV1Compat_81() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_normalizedStringV1Compat_81)); }
	inline DatatypeImplementation_t1152094268 * get_c_normalizedStringV1Compat_81() const { return ___c_normalizedStringV1Compat_81; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_normalizedStringV1Compat_81() { return &___c_normalizedStringV1Compat_81; }
	inline void set_c_normalizedStringV1Compat_81(DatatypeImplementation_t1152094268 * value)
	{
		___c_normalizedStringV1Compat_81 = value;
		Il2CppCodeGenWriteBarrier(&___c_normalizedStringV1Compat_81, value);
	}

	inline static int32_t get_offset_of_c_tokenV1Compat_82() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenV1Compat_82)); }
	inline DatatypeImplementation_t1152094268 * get_c_tokenV1Compat_82() const { return ___c_tokenV1Compat_82; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_tokenV1Compat_82() { return &___c_tokenV1Compat_82; }
	inline void set_c_tokenV1Compat_82(DatatypeImplementation_t1152094268 * value)
	{
		___c_tokenV1Compat_82 = value;
		Il2CppCodeGenWriteBarrier(&___c_tokenV1Compat_82, value);
	}

	inline static int32_t get_offset_of_c_anyAtomicType_83() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anyAtomicType_83)); }
	inline DatatypeImplementation_t1152094268 * get_c_anyAtomicType_83() const { return ___c_anyAtomicType_83; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anyAtomicType_83() { return &___c_anyAtomicType_83; }
	inline void set_c_anyAtomicType_83(DatatypeImplementation_t1152094268 * value)
	{
		___c_anyAtomicType_83 = value;
		Il2CppCodeGenWriteBarrier(&___c_anyAtomicType_83, value);
	}

	inline static int32_t get_offset_of_c_dayTimeDuration_84() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dayTimeDuration_84)); }
	inline DatatypeImplementation_t1152094268 * get_c_dayTimeDuration_84() const { return ___c_dayTimeDuration_84; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dayTimeDuration_84() { return &___c_dayTimeDuration_84; }
	inline void set_c_dayTimeDuration_84(DatatypeImplementation_t1152094268 * value)
	{
		___c_dayTimeDuration_84 = value;
		Il2CppCodeGenWriteBarrier(&___c_dayTimeDuration_84, value);
	}

	inline static int32_t get_offset_of_c_untypedAtomicType_85() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_untypedAtomicType_85)); }
	inline DatatypeImplementation_t1152094268 * get_c_untypedAtomicType_85() const { return ___c_untypedAtomicType_85; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_untypedAtomicType_85() { return &___c_untypedAtomicType_85; }
	inline void set_c_untypedAtomicType_85(DatatypeImplementation_t1152094268 * value)
	{
		___c_untypedAtomicType_85 = value;
		Il2CppCodeGenWriteBarrier(&___c_untypedAtomicType_85, value);
	}

	inline static int32_t get_offset_of_c_yearMonthDuration_86() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_yearMonthDuration_86)); }
	inline DatatypeImplementation_t1152094268 * get_c_yearMonthDuration_86() const { return ___c_yearMonthDuration_86; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_yearMonthDuration_86() { return &___c_yearMonthDuration_86; }
	inline void set_c_yearMonthDuration_86(DatatypeImplementation_t1152094268 * value)
	{
		___c_yearMonthDuration_86 = value;
		Il2CppCodeGenWriteBarrier(&___c_yearMonthDuration_86, value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypes_87() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenizedTypes_87)); }
	inline DatatypeImplementationU5BU5D_t3131202389* get_c_tokenizedTypes_87() const { return ___c_tokenizedTypes_87; }
	inline DatatypeImplementationU5BU5D_t3131202389** get_address_of_c_tokenizedTypes_87() { return &___c_tokenizedTypes_87; }
	inline void set_c_tokenizedTypes_87(DatatypeImplementationU5BU5D_t3131202389* value)
	{
		___c_tokenizedTypes_87 = value;
		Il2CppCodeGenWriteBarrier(&___c_tokenizedTypes_87, value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypesXsd_88() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenizedTypesXsd_88)); }
	inline DatatypeImplementationU5BU5D_t3131202389* get_c_tokenizedTypesXsd_88() const { return ___c_tokenizedTypesXsd_88; }
	inline DatatypeImplementationU5BU5D_t3131202389** get_address_of_c_tokenizedTypesXsd_88() { return &___c_tokenizedTypesXsd_88; }
	inline void set_c_tokenizedTypesXsd_88(DatatypeImplementationU5BU5D_t3131202389* value)
	{
		___c_tokenizedTypesXsd_88 = value;
		Il2CppCodeGenWriteBarrier(&___c_tokenizedTypesXsd_88, value);
	}

	inline static int32_t get_offset_of_c_XdrTypes_89() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_XdrTypes_89)); }
	inline SchemaDatatypeMapU5BU5D_t863562528* get_c_XdrTypes_89() const { return ___c_XdrTypes_89; }
	inline SchemaDatatypeMapU5BU5D_t863562528** get_address_of_c_XdrTypes_89() { return &___c_XdrTypes_89; }
	inline void set_c_XdrTypes_89(SchemaDatatypeMapU5BU5D_t863562528* value)
	{
		___c_XdrTypes_89 = value;
		Il2CppCodeGenWriteBarrier(&___c_XdrTypes_89, value);
	}

	inline static int32_t get_offset_of_c_XsdTypes_90() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_XsdTypes_90)); }
	inline SchemaDatatypeMapU5BU5D_t863562528* get_c_XsdTypes_90() const { return ___c_XsdTypes_90; }
	inline SchemaDatatypeMapU5BU5D_t863562528** get_address_of_c_XsdTypes_90() { return &___c_XsdTypes_90; }
	inline void set_c_XsdTypes_90(SchemaDatatypeMapU5BU5D_t863562528* value)
	{
		___c_XsdTypes_90 = value;
		Il2CppCodeGenWriteBarrier(&___c_XsdTypes_90, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
