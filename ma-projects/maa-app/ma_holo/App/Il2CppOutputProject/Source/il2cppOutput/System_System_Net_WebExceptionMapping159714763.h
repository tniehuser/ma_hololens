﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionMapping
struct  WebExceptionMapping_t159714763  : public Il2CppObject
{
public:

public:
};

struct WebExceptionMapping_t159714763_StaticFields
{
public:
	// System.String[] System.Net.WebExceptionMapping::s_Mapping
	StringU5BU5D_t1642385972* ___s_Mapping_0;

public:
	inline static int32_t get_offset_of_s_Mapping_0() { return static_cast<int32_t>(offsetof(WebExceptionMapping_t159714763_StaticFields, ___s_Mapping_0)); }
	inline StringU5BU5D_t1642385972* get_s_Mapping_0() const { return ___s_Mapping_0; }
	inline StringU5BU5D_t1642385972** get_address_of_s_Mapping_0() { return &___s_Mapping_0; }
	inline void set_s_Mapping_0(StringU5BU5D_t1642385972* value)
	{
		___s_Mapping_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Mapping_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
