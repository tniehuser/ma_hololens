﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Encoding663144255.h"

// System.Xml.Ucs4Decoder
struct Ucs4Decoder_t2645160027;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding
struct  Ucs4Encoding_t650080996  : public Encoding_t663144255
{
public:
	// System.Xml.Ucs4Decoder System.Xml.Ucs4Encoding::ucs4Decoder
	Ucs4Decoder_t2645160027 * ___ucs4Decoder_15;

public:
	inline static int32_t get_offset_of_ucs4Decoder_15() { return static_cast<int32_t>(offsetof(Ucs4Encoding_t650080996, ___ucs4Decoder_15)); }
	inline Ucs4Decoder_t2645160027 * get_ucs4Decoder_15() const { return ___ucs4Decoder_15; }
	inline Ucs4Decoder_t2645160027 ** get_address_of_ucs4Decoder_15() { return &___ucs4Decoder_15; }
	inline void set_ucs4Decoder_15(Ucs4Decoder_t2645160027 * value)
	{
		___ucs4Decoder_15 = value;
		Il2CppCodeGenWriteBarrier(&___ucs4Decoder_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
