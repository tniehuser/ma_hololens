﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_TaskContinuation1422769290.h"
#include "mscorlib_System_Threading_Tasks_TaskContinuationOp2826278702.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.StandardTaskContinuation
struct  StandardTaskContinuation_t3524399553  : public TaskContinuation_t1422769290
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.StandardTaskContinuation::m_task
	Task_t1843236107 * ___m_task_0;
	// System.Threading.Tasks.TaskContinuationOptions System.Threading.Tasks.StandardTaskContinuation::m_options
	int32_t ___m_options_1;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.StandardTaskContinuation::m_taskScheduler
	TaskScheduler_t3932792796 * ___m_taskScheduler_2;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(StandardTaskContinuation_t3524399553, ___m_task_0)); }
	inline Task_t1843236107 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1843236107 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1843236107 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_task_0, value);
	}

	inline static int32_t get_offset_of_m_options_1() { return static_cast<int32_t>(offsetof(StandardTaskContinuation_t3524399553, ___m_options_1)); }
	inline int32_t get_m_options_1() const { return ___m_options_1; }
	inline int32_t* get_address_of_m_options_1() { return &___m_options_1; }
	inline void set_m_options_1(int32_t value)
	{
		___m_options_1 = value;
	}

	inline static int32_t get_offset_of_m_taskScheduler_2() { return static_cast<int32_t>(offsetof(StandardTaskContinuation_t3524399553, ___m_taskScheduler_2)); }
	inline TaskScheduler_t3932792796 * get_m_taskScheduler_2() const { return ___m_taskScheduler_2; }
	inline TaskScheduler_t3932792796 ** get_address_of_m_taskScheduler_2() { return &___m_taskScheduler_2; }
	inline void set_m_taskScheduler_2(TaskScheduler_t3932792796 * value)
	{
		___m_taskScheduler_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_taskScheduler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
