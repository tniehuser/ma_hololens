﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Encoding663144255.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.SByte[]
struct SByteU5BU5D_t3472287392;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.UTF7Encoding
struct  UTF7Encoding_t741406939  : public Encoding_t663144255
{
public:
	// System.Byte[] System.Text.UTF7Encoding::base64Bytes
	ByteU5BU5D_t3397334013* ___base64Bytes_15;
	// System.SByte[] System.Text.UTF7Encoding::base64Values
	SByteU5BU5D_t3472287392* ___base64Values_16;
	// System.Boolean[] System.Text.UTF7Encoding::directEncode
	BooleanU5BU5D_t3568034315* ___directEncode_17;
	// System.Boolean System.Text.UTF7Encoding::m_allowOptionals
	bool ___m_allowOptionals_18;

public:
	inline static int32_t get_offset_of_base64Bytes_15() { return static_cast<int32_t>(offsetof(UTF7Encoding_t741406939, ___base64Bytes_15)); }
	inline ByteU5BU5D_t3397334013* get_base64Bytes_15() const { return ___base64Bytes_15; }
	inline ByteU5BU5D_t3397334013** get_address_of_base64Bytes_15() { return &___base64Bytes_15; }
	inline void set_base64Bytes_15(ByteU5BU5D_t3397334013* value)
	{
		___base64Bytes_15 = value;
		Il2CppCodeGenWriteBarrier(&___base64Bytes_15, value);
	}

	inline static int32_t get_offset_of_base64Values_16() { return static_cast<int32_t>(offsetof(UTF7Encoding_t741406939, ___base64Values_16)); }
	inline SByteU5BU5D_t3472287392* get_base64Values_16() const { return ___base64Values_16; }
	inline SByteU5BU5D_t3472287392** get_address_of_base64Values_16() { return &___base64Values_16; }
	inline void set_base64Values_16(SByteU5BU5D_t3472287392* value)
	{
		___base64Values_16 = value;
		Il2CppCodeGenWriteBarrier(&___base64Values_16, value);
	}

	inline static int32_t get_offset_of_directEncode_17() { return static_cast<int32_t>(offsetof(UTF7Encoding_t741406939, ___directEncode_17)); }
	inline BooleanU5BU5D_t3568034315* get_directEncode_17() const { return ___directEncode_17; }
	inline BooleanU5BU5D_t3568034315** get_address_of_directEncode_17() { return &___directEncode_17; }
	inline void set_directEncode_17(BooleanU5BU5D_t3568034315* value)
	{
		___directEncode_17 = value;
		Il2CppCodeGenWriteBarrier(&___directEncode_17, value);
	}

	inline static int32_t get_offset_of_m_allowOptionals_18() { return static_cast<int32_t>(offsetof(UTF7Encoding_t741406939, ___m_allowOptionals_18)); }
	inline bool get_m_allowOptionals_18() const { return ___m_allowOptionals_18; }
	inline bool* get_address_of_m_allowOptionals_18() { return &___m_allowOptionals_18; }
	inline void set_m_allowOptionals_18(bool value)
	{
		___m_allowOptionals_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
