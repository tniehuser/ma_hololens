﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_Int322071877448.h"

// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Threading.ManualResetEventSlim
struct ManualResetEventSlim_t964365518;
// System.Threading.Tasks.TaskExceptionHolder
struct TaskExceptionHolder_t2208677448;
// System.Threading.Tasks.Shared`1<System.Threading.CancellationTokenRegistration>
struct Shared_1_t2858597776;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_t1212357239;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task/ContingentProperties
struct  ContingentProperties_t606988207  : public Il2CppObject
{
public:
	// System.Threading.ExecutionContext System.Threading.Tasks.Task/ContingentProperties::m_capturedContext
	ExecutionContext_t1392266323 * ___m_capturedContext_0;
	// System.Threading.ManualResetEventSlim modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task/ContingentProperties::m_completionEvent
	ManualResetEventSlim_t964365518 * ___m_completionEvent_1;
	// System.Threading.Tasks.TaskExceptionHolder modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task/ContingentProperties::m_exceptionsHolder
	TaskExceptionHolder_t2208677448 * ___m_exceptionsHolder_2;
	// System.Threading.CancellationToken System.Threading.Tasks.Task/ContingentProperties::m_cancellationToken
	CancellationToken_t1851405782  ___m_cancellationToken_3;
	// System.Threading.Tasks.Shared`1<System.Threading.CancellationTokenRegistration> System.Threading.Tasks.Task/ContingentProperties::m_cancellationRegistration
	Shared_1_t2858597776 * ___m_cancellationRegistration_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task/ContingentProperties::m_internalCancellationRequested
	int32_t ___m_internalCancellationRequested_5;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task/ContingentProperties::m_completionCountdown
	int32_t ___m_completionCountdown_6;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task/ContingentProperties::m_exceptionalChildren
	List_1_t1212357239 * ___m_exceptionalChildren_7;

public:
	inline static int32_t get_offset_of_m_capturedContext_0() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_capturedContext_0)); }
	inline ExecutionContext_t1392266323 * get_m_capturedContext_0() const { return ___m_capturedContext_0; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_capturedContext_0() { return &___m_capturedContext_0; }
	inline void set_m_capturedContext_0(ExecutionContext_t1392266323 * value)
	{
		___m_capturedContext_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_capturedContext_0, value);
	}

	inline static int32_t get_offset_of_m_completionEvent_1() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_completionEvent_1)); }
	inline ManualResetEventSlim_t964365518 * get_m_completionEvent_1() const { return ___m_completionEvent_1; }
	inline ManualResetEventSlim_t964365518 ** get_address_of_m_completionEvent_1() { return &___m_completionEvent_1; }
	inline void set_m_completionEvent_1(ManualResetEventSlim_t964365518 * value)
	{
		___m_completionEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_completionEvent_1, value);
	}

	inline static int32_t get_offset_of_m_exceptionsHolder_2() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_exceptionsHolder_2)); }
	inline TaskExceptionHolder_t2208677448 * get_m_exceptionsHolder_2() const { return ___m_exceptionsHolder_2; }
	inline TaskExceptionHolder_t2208677448 ** get_address_of_m_exceptionsHolder_2() { return &___m_exceptionsHolder_2; }
	inline void set_m_exceptionsHolder_2(TaskExceptionHolder_t2208677448 * value)
	{
		___m_exceptionsHolder_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_exceptionsHolder_2, value);
	}

	inline static int32_t get_offset_of_m_cancellationToken_3() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_cancellationToken_3)); }
	inline CancellationToken_t1851405782  get_m_cancellationToken_3() const { return ___m_cancellationToken_3; }
	inline CancellationToken_t1851405782 * get_address_of_m_cancellationToken_3() { return &___m_cancellationToken_3; }
	inline void set_m_cancellationToken_3(CancellationToken_t1851405782  value)
	{
		___m_cancellationToken_3 = value;
	}

	inline static int32_t get_offset_of_m_cancellationRegistration_4() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_cancellationRegistration_4)); }
	inline Shared_1_t2858597776 * get_m_cancellationRegistration_4() const { return ___m_cancellationRegistration_4; }
	inline Shared_1_t2858597776 ** get_address_of_m_cancellationRegistration_4() { return &___m_cancellationRegistration_4; }
	inline void set_m_cancellationRegistration_4(Shared_1_t2858597776 * value)
	{
		___m_cancellationRegistration_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_cancellationRegistration_4, value);
	}

	inline static int32_t get_offset_of_m_internalCancellationRequested_5() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_internalCancellationRequested_5)); }
	inline int32_t get_m_internalCancellationRequested_5() const { return ___m_internalCancellationRequested_5; }
	inline int32_t* get_address_of_m_internalCancellationRequested_5() { return &___m_internalCancellationRequested_5; }
	inline void set_m_internalCancellationRequested_5(int32_t value)
	{
		___m_internalCancellationRequested_5 = value;
	}

	inline static int32_t get_offset_of_m_completionCountdown_6() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_completionCountdown_6)); }
	inline int32_t get_m_completionCountdown_6() const { return ___m_completionCountdown_6; }
	inline int32_t* get_address_of_m_completionCountdown_6() { return &___m_completionCountdown_6; }
	inline void set_m_completionCountdown_6(int32_t value)
	{
		___m_completionCountdown_6 = value;
	}

	inline static int32_t get_offset_of_m_exceptionalChildren_7() { return static_cast<int32_t>(offsetof(ContingentProperties_t606988207, ___m_exceptionalChildren_7)); }
	inline List_1_t1212357239 * get_m_exceptionalChildren_7() const { return ___m_exceptionalChildren_7; }
	inline List_1_t1212357239 ** get_address_of_m_exceptionalChildren_7() { return &___m_exceptionalChildren_7; }
	inline void set_m_exceptionalChildren_7(List_1_t1212357239 * value)
	{
		___m_exceptionalChildren_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_exceptionalChildren_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
