﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_Net_Dns_DnsResourceRecord2943454412.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordPTR
struct  DnsResourceRecordPTR_t1722196618  : public DnsResourceRecord_t2943454412
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordPTR::dname
	String_t* ___dname_6;

public:
	inline static int32_t get_offset_of_dname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordPTR_t1722196618, ___dname_6)); }
	inline String_t* get_dname_6() const { return ___dname_6; }
	inline String_t** get_address_of_dname_6() { return &___dname_6; }
	inline void set_dname_6(String_t* value)
	{
		___dname_6 = value;
		Il2CppCodeGenWriteBarrier(&___dname_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
