﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_DtdParser_Token322298853.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame
struct  ParseElementOnlyContent_LocalFrame_t225387055  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame::startParenEntityId
	int32_t ___startParenEntityId_0;
	// System.Xml.DtdParser/Token System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame::parsingSchema
	int32_t ___parsingSchema_1;

public:
	inline static int32_t get_offset_of_startParenEntityId_0() { return static_cast<int32_t>(offsetof(ParseElementOnlyContent_LocalFrame_t225387055, ___startParenEntityId_0)); }
	inline int32_t get_startParenEntityId_0() const { return ___startParenEntityId_0; }
	inline int32_t* get_address_of_startParenEntityId_0() { return &___startParenEntityId_0; }
	inline void set_startParenEntityId_0(int32_t value)
	{
		___startParenEntityId_0 = value;
	}

	inline static int32_t get_offset_of_parsingSchema_1() { return static_cast<int32_t>(offsetof(ParseElementOnlyContent_LocalFrame_t225387055, ___parsingSchema_1)); }
	inline int32_t get_parsingSchema_1() const { return ___parsingSchema_1; }
	inline int32_t* get_address_of_parsingSchema_1() { return &___parsingSchema_1; }
	inline void set_parsingSchema_1(int32_t value)
	{
		___parsingSchema_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
