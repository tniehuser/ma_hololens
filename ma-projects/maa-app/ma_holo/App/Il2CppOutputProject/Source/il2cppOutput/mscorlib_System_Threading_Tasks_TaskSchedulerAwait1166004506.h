﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_AwaitTaskContinuat2160930432.h"

// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;
// System.Action`1<System.Object>
struct Action_1_t2491248677;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskSchedulerAwaitTaskContinuation
struct  TaskSchedulerAwaitTaskContinuation_t1166004506  : public AwaitTaskContinuation_t2160930432
{
public:
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.TaskSchedulerAwaitTaskContinuation::m_scheduler
	TaskScheduler_t3932792796 * ___m_scheduler_5;

public:
	inline static int32_t get_offset_of_m_scheduler_5() { return static_cast<int32_t>(offsetof(TaskSchedulerAwaitTaskContinuation_t1166004506, ___m_scheduler_5)); }
	inline TaskScheduler_t3932792796 * get_m_scheduler_5() const { return ___m_scheduler_5; }
	inline TaskScheduler_t3932792796 ** get_address_of_m_scheduler_5() { return &___m_scheduler_5; }
	inline void set_m_scheduler_5(TaskScheduler_t3932792796 * value)
	{
		___m_scheduler_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_scheduler_5, value);
	}
};

struct TaskSchedulerAwaitTaskContinuation_t1166004506_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.Tasks.TaskSchedulerAwaitTaskContinuation::<>f__am$cache0
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(TaskSchedulerAwaitTaskContinuation_t1166004506_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
