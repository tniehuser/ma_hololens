﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1912587528;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2645893724;
// System.Runtime.Serialization.Formatters.Binary.InternalFE
struct InternalFE_t3355145566;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t3985864818;
// System.Object
struct Il2CppObject;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2408360458;
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t324204131;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit
struct SerObjectInfoInit_t4094458531;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1473156697;
// System.Runtime.Serialization.Formatters.Binary.SerStack
struct SerStack_t3886188184;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall
struct BinaryMethodCall_t1773568836;
// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn
struct BinaryMethodReturn_t3366135632;
// System.Runtime.Serialization.Formatters.Binary.IntSizedArray
struct IntSizedArray_t2017939501;
// System.Runtime.Serialization.Formatters.Binary.NameCache
struct NameCache_t3722972407;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Func`2<System.Reflection.AssemblyName,System.Reflection.Assembly>
struct Func_2_t2776464558;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectReader
struct  ObjectReader_t1476095226  : public Il2CppObject
{
public:
	// System.IO.Stream System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_stream
	Stream_t3255436806 * ___m_stream_0;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_surrogates
	Il2CppObject * ___m_surrogates_1;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_context
	StreamingContext_t1417235061  ___m_context_2;
	// System.Runtime.Serialization.ObjectManager System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_objectManager
	ObjectManager_t2645893724 * ___m_objectManager_3;
	// System.Runtime.Serialization.Formatters.Binary.InternalFE System.Runtime.Serialization.Formatters.Binary.ObjectReader::formatterEnums
	InternalFE_t3355145566 * ___formatterEnums_4;
	// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_binder
	SerializationBinder_t3985864818 * ___m_binder_5;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ObjectReader::topId
	int64_t ___topId_6;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bSimpleAssembly
	bool ___bSimpleAssembly_7;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::handlerObject
	Il2CppObject * ___handlerObject_8;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_topObject
	Il2CppObject * ___m_topObject_9;
	// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Serialization.Formatters.Binary.ObjectReader::headers
	HeaderU5BU5D_t2408360458* ___headers_10;
	// System.Runtime.Remoting.Messaging.HeaderHandler System.Runtime.Serialization.Formatters.Binary.ObjectReader::handler
	HeaderHandler_t324204131 * ___handler_11;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit System.Runtime.Serialization.Formatters.Binary.ObjectReader::serObjectInfoInit
	SerObjectInfoInit_t4094458531 * ___serObjectInfoInit_12;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.Formatters.Binary.ObjectReader::m_formatterConverter
	Il2CppObject * ___m_formatterConverter_13;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.ObjectReader::stack
	SerStack_t3886188184 * ___stack_14;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.ObjectReader::valueFixupStack
	SerStack_t3886188184 * ___valueFixupStack_15;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ObjectReader::crossAppDomainArray
	ObjectU5BU5D_t3614634134* ___crossAppDomainArray_16;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bFullDeserialization
	bool ___bFullDeserialization_17;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bMethodCall
	bool ___bMethodCall_18;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bMethodReturn
	bool ___bMethodReturn_19;
	// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall System.Runtime.Serialization.Formatters.Binary.ObjectReader::binaryMethodCall
	BinaryMethodCall_t1773568836 * ___binaryMethodCall_20;
	// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn System.Runtime.Serialization.Formatters.Binary.ObjectReader::binaryMethodReturn
	BinaryMethodReturn_t3366135632 * ___binaryMethodReturn_21;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bIsCrossAppDomain
	bool ___bIsCrossAppDomain_22;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::bOldFormatDetected
	bool ___bOldFormatDetected_23;
	// System.Runtime.Serialization.Formatters.Binary.IntSizedArray System.Runtime.Serialization.Formatters.Binary.ObjectReader::valTypeObjectIdTable
	IntSizedArray_t2017939501 * ___valTypeObjectIdTable_24;
	// System.Runtime.Serialization.Formatters.Binary.NameCache System.Runtime.Serialization.Formatters.Binary.ObjectReader::typeCache
	NameCache_t3722972407 * ___typeCache_25;
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectReader::previousAssemblyString
	String_t* ___previousAssemblyString_26;
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectReader::previousName
	String_t* ___previousName_27;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::previousType
	Type_t * ___previousType_28;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_stream_0)); }
	inline Stream_t3255436806 * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_t3255436806 ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_t3255436806 * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_stream_0, value);
	}

	inline static int32_t get_offset_of_m_surrogates_1() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_surrogates_1)); }
	inline Il2CppObject * get_m_surrogates_1() const { return ___m_surrogates_1; }
	inline Il2CppObject ** get_address_of_m_surrogates_1() { return &___m_surrogates_1; }
	inline void set_m_surrogates_1(Il2CppObject * value)
	{
		___m_surrogates_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_surrogates_1, value);
	}

	inline static int32_t get_offset_of_m_context_2() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_context_2)); }
	inline StreamingContext_t1417235061  get_m_context_2() const { return ___m_context_2; }
	inline StreamingContext_t1417235061 * get_address_of_m_context_2() { return &___m_context_2; }
	inline void set_m_context_2(StreamingContext_t1417235061  value)
	{
		___m_context_2 = value;
	}

	inline static int32_t get_offset_of_m_objectManager_3() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_objectManager_3)); }
	inline ObjectManager_t2645893724 * get_m_objectManager_3() const { return ___m_objectManager_3; }
	inline ObjectManager_t2645893724 ** get_address_of_m_objectManager_3() { return &___m_objectManager_3; }
	inline void set_m_objectManager_3(ObjectManager_t2645893724 * value)
	{
		___m_objectManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_objectManager_3, value);
	}

	inline static int32_t get_offset_of_formatterEnums_4() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___formatterEnums_4)); }
	inline InternalFE_t3355145566 * get_formatterEnums_4() const { return ___formatterEnums_4; }
	inline InternalFE_t3355145566 ** get_address_of_formatterEnums_4() { return &___formatterEnums_4; }
	inline void set_formatterEnums_4(InternalFE_t3355145566 * value)
	{
		___formatterEnums_4 = value;
		Il2CppCodeGenWriteBarrier(&___formatterEnums_4, value);
	}

	inline static int32_t get_offset_of_m_binder_5() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_binder_5)); }
	inline SerializationBinder_t3985864818 * get_m_binder_5() const { return ___m_binder_5; }
	inline SerializationBinder_t3985864818 ** get_address_of_m_binder_5() { return &___m_binder_5; }
	inline void set_m_binder_5(SerializationBinder_t3985864818 * value)
	{
		___m_binder_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_binder_5, value);
	}

	inline static int32_t get_offset_of_topId_6() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___topId_6)); }
	inline int64_t get_topId_6() const { return ___topId_6; }
	inline int64_t* get_address_of_topId_6() { return &___topId_6; }
	inline void set_topId_6(int64_t value)
	{
		___topId_6 = value;
	}

	inline static int32_t get_offset_of_bSimpleAssembly_7() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bSimpleAssembly_7)); }
	inline bool get_bSimpleAssembly_7() const { return ___bSimpleAssembly_7; }
	inline bool* get_address_of_bSimpleAssembly_7() { return &___bSimpleAssembly_7; }
	inline void set_bSimpleAssembly_7(bool value)
	{
		___bSimpleAssembly_7 = value;
	}

	inline static int32_t get_offset_of_handlerObject_8() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___handlerObject_8)); }
	inline Il2CppObject * get_handlerObject_8() const { return ___handlerObject_8; }
	inline Il2CppObject ** get_address_of_handlerObject_8() { return &___handlerObject_8; }
	inline void set_handlerObject_8(Il2CppObject * value)
	{
		___handlerObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___handlerObject_8, value);
	}

	inline static int32_t get_offset_of_m_topObject_9() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_topObject_9)); }
	inline Il2CppObject * get_m_topObject_9() const { return ___m_topObject_9; }
	inline Il2CppObject ** get_address_of_m_topObject_9() { return &___m_topObject_9; }
	inline void set_m_topObject_9(Il2CppObject * value)
	{
		___m_topObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_topObject_9, value);
	}

	inline static int32_t get_offset_of_headers_10() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___headers_10)); }
	inline HeaderU5BU5D_t2408360458* get_headers_10() const { return ___headers_10; }
	inline HeaderU5BU5D_t2408360458** get_address_of_headers_10() { return &___headers_10; }
	inline void set_headers_10(HeaderU5BU5D_t2408360458* value)
	{
		___headers_10 = value;
		Il2CppCodeGenWriteBarrier(&___headers_10, value);
	}

	inline static int32_t get_offset_of_handler_11() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___handler_11)); }
	inline HeaderHandler_t324204131 * get_handler_11() const { return ___handler_11; }
	inline HeaderHandler_t324204131 ** get_address_of_handler_11() { return &___handler_11; }
	inline void set_handler_11(HeaderHandler_t324204131 * value)
	{
		___handler_11 = value;
		Il2CppCodeGenWriteBarrier(&___handler_11, value);
	}

	inline static int32_t get_offset_of_serObjectInfoInit_12() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___serObjectInfoInit_12)); }
	inline SerObjectInfoInit_t4094458531 * get_serObjectInfoInit_12() const { return ___serObjectInfoInit_12; }
	inline SerObjectInfoInit_t4094458531 ** get_address_of_serObjectInfoInit_12() { return &___serObjectInfoInit_12; }
	inline void set_serObjectInfoInit_12(SerObjectInfoInit_t4094458531 * value)
	{
		___serObjectInfoInit_12 = value;
		Il2CppCodeGenWriteBarrier(&___serObjectInfoInit_12, value);
	}

	inline static int32_t get_offset_of_m_formatterConverter_13() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___m_formatterConverter_13)); }
	inline Il2CppObject * get_m_formatterConverter_13() const { return ___m_formatterConverter_13; }
	inline Il2CppObject ** get_address_of_m_formatterConverter_13() { return &___m_formatterConverter_13; }
	inline void set_m_formatterConverter_13(Il2CppObject * value)
	{
		___m_formatterConverter_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_formatterConverter_13, value);
	}

	inline static int32_t get_offset_of_stack_14() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___stack_14)); }
	inline SerStack_t3886188184 * get_stack_14() const { return ___stack_14; }
	inline SerStack_t3886188184 ** get_address_of_stack_14() { return &___stack_14; }
	inline void set_stack_14(SerStack_t3886188184 * value)
	{
		___stack_14 = value;
		Il2CppCodeGenWriteBarrier(&___stack_14, value);
	}

	inline static int32_t get_offset_of_valueFixupStack_15() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___valueFixupStack_15)); }
	inline SerStack_t3886188184 * get_valueFixupStack_15() const { return ___valueFixupStack_15; }
	inline SerStack_t3886188184 ** get_address_of_valueFixupStack_15() { return &___valueFixupStack_15; }
	inline void set_valueFixupStack_15(SerStack_t3886188184 * value)
	{
		___valueFixupStack_15 = value;
		Il2CppCodeGenWriteBarrier(&___valueFixupStack_15, value);
	}

	inline static int32_t get_offset_of_crossAppDomainArray_16() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___crossAppDomainArray_16)); }
	inline ObjectU5BU5D_t3614634134* get_crossAppDomainArray_16() const { return ___crossAppDomainArray_16; }
	inline ObjectU5BU5D_t3614634134** get_address_of_crossAppDomainArray_16() { return &___crossAppDomainArray_16; }
	inline void set_crossAppDomainArray_16(ObjectU5BU5D_t3614634134* value)
	{
		___crossAppDomainArray_16 = value;
		Il2CppCodeGenWriteBarrier(&___crossAppDomainArray_16, value);
	}

	inline static int32_t get_offset_of_bFullDeserialization_17() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bFullDeserialization_17)); }
	inline bool get_bFullDeserialization_17() const { return ___bFullDeserialization_17; }
	inline bool* get_address_of_bFullDeserialization_17() { return &___bFullDeserialization_17; }
	inline void set_bFullDeserialization_17(bool value)
	{
		___bFullDeserialization_17 = value;
	}

	inline static int32_t get_offset_of_bMethodCall_18() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bMethodCall_18)); }
	inline bool get_bMethodCall_18() const { return ___bMethodCall_18; }
	inline bool* get_address_of_bMethodCall_18() { return &___bMethodCall_18; }
	inline void set_bMethodCall_18(bool value)
	{
		___bMethodCall_18 = value;
	}

	inline static int32_t get_offset_of_bMethodReturn_19() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bMethodReturn_19)); }
	inline bool get_bMethodReturn_19() const { return ___bMethodReturn_19; }
	inline bool* get_address_of_bMethodReturn_19() { return &___bMethodReturn_19; }
	inline void set_bMethodReturn_19(bool value)
	{
		___bMethodReturn_19 = value;
	}

	inline static int32_t get_offset_of_binaryMethodCall_20() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___binaryMethodCall_20)); }
	inline BinaryMethodCall_t1773568836 * get_binaryMethodCall_20() const { return ___binaryMethodCall_20; }
	inline BinaryMethodCall_t1773568836 ** get_address_of_binaryMethodCall_20() { return &___binaryMethodCall_20; }
	inline void set_binaryMethodCall_20(BinaryMethodCall_t1773568836 * value)
	{
		___binaryMethodCall_20 = value;
		Il2CppCodeGenWriteBarrier(&___binaryMethodCall_20, value);
	}

	inline static int32_t get_offset_of_binaryMethodReturn_21() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___binaryMethodReturn_21)); }
	inline BinaryMethodReturn_t3366135632 * get_binaryMethodReturn_21() const { return ___binaryMethodReturn_21; }
	inline BinaryMethodReturn_t3366135632 ** get_address_of_binaryMethodReturn_21() { return &___binaryMethodReturn_21; }
	inline void set_binaryMethodReturn_21(BinaryMethodReturn_t3366135632 * value)
	{
		___binaryMethodReturn_21 = value;
		Il2CppCodeGenWriteBarrier(&___binaryMethodReturn_21, value);
	}

	inline static int32_t get_offset_of_bIsCrossAppDomain_22() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bIsCrossAppDomain_22)); }
	inline bool get_bIsCrossAppDomain_22() const { return ___bIsCrossAppDomain_22; }
	inline bool* get_address_of_bIsCrossAppDomain_22() { return &___bIsCrossAppDomain_22; }
	inline void set_bIsCrossAppDomain_22(bool value)
	{
		___bIsCrossAppDomain_22 = value;
	}

	inline static int32_t get_offset_of_bOldFormatDetected_23() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___bOldFormatDetected_23)); }
	inline bool get_bOldFormatDetected_23() const { return ___bOldFormatDetected_23; }
	inline bool* get_address_of_bOldFormatDetected_23() { return &___bOldFormatDetected_23; }
	inline void set_bOldFormatDetected_23(bool value)
	{
		___bOldFormatDetected_23 = value;
	}

	inline static int32_t get_offset_of_valTypeObjectIdTable_24() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___valTypeObjectIdTable_24)); }
	inline IntSizedArray_t2017939501 * get_valTypeObjectIdTable_24() const { return ___valTypeObjectIdTable_24; }
	inline IntSizedArray_t2017939501 ** get_address_of_valTypeObjectIdTable_24() { return &___valTypeObjectIdTable_24; }
	inline void set_valTypeObjectIdTable_24(IntSizedArray_t2017939501 * value)
	{
		___valTypeObjectIdTable_24 = value;
		Il2CppCodeGenWriteBarrier(&___valTypeObjectIdTable_24, value);
	}

	inline static int32_t get_offset_of_typeCache_25() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___typeCache_25)); }
	inline NameCache_t3722972407 * get_typeCache_25() const { return ___typeCache_25; }
	inline NameCache_t3722972407 ** get_address_of_typeCache_25() { return &___typeCache_25; }
	inline void set_typeCache_25(NameCache_t3722972407 * value)
	{
		___typeCache_25 = value;
		Il2CppCodeGenWriteBarrier(&___typeCache_25, value);
	}

	inline static int32_t get_offset_of_previousAssemblyString_26() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___previousAssemblyString_26)); }
	inline String_t* get_previousAssemblyString_26() const { return ___previousAssemblyString_26; }
	inline String_t** get_address_of_previousAssemblyString_26() { return &___previousAssemblyString_26; }
	inline void set_previousAssemblyString_26(String_t* value)
	{
		___previousAssemblyString_26 = value;
		Il2CppCodeGenWriteBarrier(&___previousAssemblyString_26, value);
	}

	inline static int32_t get_offset_of_previousName_27() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___previousName_27)); }
	inline String_t* get_previousName_27() const { return ___previousName_27; }
	inline String_t** get_address_of_previousName_27() { return &___previousName_27; }
	inline void set_previousName_27(String_t* value)
	{
		___previousName_27 = value;
		Il2CppCodeGenWriteBarrier(&___previousName_27, value);
	}

	inline static int32_t get_offset_of_previousType_28() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226, ___previousType_28)); }
	inline Type_t * get_previousType_28() const { return ___previousType_28; }
	inline Type_t ** get_address_of_previousType_28() { return &___previousType_28; }
	inline void set_previousType_28(Type_t * value)
	{
		___previousType_28 = value;
		Il2CppCodeGenWriteBarrier(&___previousType_28, value);
	}
};

struct ObjectReader_t1476095226_StaticFields
{
public:
	// System.Func`2<System.Reflection.AssemblyName,System.Reflection.Assembly> System.Runtime.Serialization.Formatters.Binary.ObjectReader::<>f__mg$cache0
	Func_2_t2776464558 * ___U3CU3Ef__mgU24cache0_29;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_29() { return static_cast<int32_t>(offsetof(ObjectReader_t1476095226_StaticFields, ___U3CU3Ef__mgU24cache0_29)); }
	inline Func_2_t2776464558 * get_U3CU3Ef__mgU24cache0_29() const { return ___U3CU3Ef__mgU24cache0_29; }
	inline Func_2_t2776464558 ** get_address_of_U3CU3Ef__mgU24cache0_29() { return &___U3CU3Ef__mgU24cache0_29; }
	inline void set_U3CU3Ef__mgU24cache0_29(Func_2_t2776464558 * value)
	{
		___U3CU3Ef__mgU24cache0_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
