﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.CultureInfoConverter
struct CultureInfoConverter_t2239982248;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CultureInfoConverter/CultureComparer
struct  CultureComparer_t1185978443  : public Il2CppObject
{
public:
	// System.ComponentModel.CultureInfoConverter System.ComponentModel.CultureInfoConverter/CultureComparer::converter
	CultureInfoConverter_t2239982248 * ___converter_0;

public:
	inline static int32_t get_offset_of_converter_0() { return static_cast<int32_t>(offsetof(CultureComparer_t1185978443, ___converter_0)); }
	inline CultureInfoConverter_t2239982248 * get_converter_0() const { return ___converter_0; }
	inline CultureInfoConverter_t2239982248 ** get_address_of_converter_0() { return &___converter_0; }
	inline void set_converter_0(CultureInfoConverter_t2239982248 * value)
	{
		___converter_0 = value;
		Il2CppCodeGenWriteBarrier(&___converter_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
