﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>[]
struct KeyValuePair_2U5BU5D_t3308171066;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct  Queue_1_t708476670  : public Il2CppObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	KeyValuePair_2U5BU5D_t3308171066* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	Il2CppObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____array_0)); }
	inline KeyValuePair_2U5BU5D_t3308171066* get__array_0() const { return ____array_0; }
	inline KeyValuePair_2U5BU5D_t3308171066** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(KeyValuePair_2U5BU5D_t3308171066* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier(&____array_0, value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_t708476670, ____syncRoot_5)); }
	inline Il2CppObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline Il2CppObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(Il2CppObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_5, value);
	}
};

struct Queue_1_t708476670_StaticFields
{
public:
	// T[] System.Collections.Generic.Queue`1::_emptyArray
	KeyValuePair_2U5BU5D_t3308171066* ____emptyArray_6;

public:
	inline static int32_t get_offset_of__emptyArray_6() { return static_cast<int32_t>(offsetof(Queue_1_t708476670_StaticFields, ____emptyArray_6)); }
	inline KeyValuePair_2U5BU5D_t3308171066* get__emptyArray_6() const { return ____emptyArray_6; }
	inline KeyValuePair_2U5BU5D_t3308171066** get_address_of__emptyArray_6() { return &____emptyArray_6; }
	inline void set__emptyArray_6(KeyValuePair_2U5BU5D_t3308171066* value)
	{
		____emptyArray_6 = value;
		Il2CppCodeGenWriteBarrier(&____emptyArray_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
