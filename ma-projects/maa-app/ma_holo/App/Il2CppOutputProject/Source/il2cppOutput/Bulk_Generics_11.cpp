﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_Enumerable_U3CCombinePredi3658276611.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Func_2_gen3961629604.h"
#include "System_Core_System_Linq_Enumerable_Iterator_1_gen3157253607.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "System_Core_System_Linq_Enumerable_WhereArrayItera2998102595.h"
#include "System_Core_System_Linq_Enumerable_WhereEnumerable3208243354.h"
#include "System_Core_System_Linq_Enumerable_WhereListIterat1069043000.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Nullable_1_gen3179568147.h"
#include "Mono_Security_Mono_Security_Interface_MonoSslPolicy621534536.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Nullable_1_gen214512479.h"
#include "Mono_Security_Mono_Security_Interface_TlsProtocols1951446164.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Predicate_1_gen2268544833.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Predicate_1_gen2126074551.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Predicate_1_gen3431143080.h"
#include "mscorlib_System_Predicate_1_gen4100926317.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Predicate_1_gen3462638488.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Predicate_1_gen2520985796.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Predicate_1_gen2484216029.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen3647015448.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen3192354960.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Predicate_1_gen519480047.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Predicate_1_gen861229842.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "mscorlib_System_Predicate_1_gen1873229064.h"
#include "mscorlib_System_Predicate_1_gen3724820022.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Predicate_1_gen592652136.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Predicate_1_gen1352167029.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Predicate_1_gen1223773037.h"
#include "System_Xml_System_Xml_Schema_RangePositionInfo2780802922.h"
#include "mscorlib_System_Predicate_1_gen953556625.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2510586510.h"
#include "mscorlib_System_Predicate_1_gen2348721464.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask2408546353.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncMeth2485284745.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen2945603725.h"
#include "mscorlib_System_Threading_Tasks_CausalityTraceLevel536076440.h"
#include "mscorlib_System_Threading_Tasks_AsyncCausalityStat2986644407.h"
#include "mscorlib_System_Threading_Tasks_Task1843236107.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_OperationCanceledException2897400967.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_UIntPtr1549717846.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen1191906455.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask3842594813.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask1272420930.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen1809478302.h"
#include "mscorlib_System_Runtime_CompilerServices_Condition1530143791.h"
#include "mscorlib_System_Runtime_CompilerServices_Ephemeron1875076633.h"
#include "mscorlib_System_GC2902933594.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Runtime_CompilerServices_Configured446638270.h"
#include "mscorlib_System_Action3226471752.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure2987908296.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure3605480143.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure4241341646.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen2445339805.h"
#include "mscorlib_System_Threading_Tasks_VoidTaskResult3325310798.h"
#include "mscorlib_System_Runtime_CompilerServices_ConfiguredT34161435.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure2575431461.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure3193003308.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure3828864811.h"
#include "mscorlib_System_Runtime_CompilerServices_TaskAwaite899200082.h"
#include "mscorlib_System_Runtime_CompilerServices_TaskAwait3440470108.h"
#include "mscorlib_System_Runtime_CompilerServices_TaskAwait4058041955.h"
#include "mscorlib_System_Runtime_CompilerServices_TaskAwaite398936162.h"
#include "mscorlib_System_RuntimeType_ListBuilder_1_gen3063240520.h"
#include "mscorlib_System_EmptyArray_1_gen3025629597.h"
#include "mscorlib_System_Threading_AsyncLocal_1_gen31129824.h"
#include "mscorlib_System_Action_1_gen981956716.h"
#include "mscorlib_System_Threading_AsyncLocalValueChangedAr1180157334.h"
#include "mscorlib_System_Threading_SparselyPopulatedArray_11092078389.h"
#include "mscorlib_System_Threading_SparselyPopulatedArrayFr3870933437.h"
#include "mscorlib_System_Threading_SparselyPopulatedArrayAdd810080232.h"
#include "mscorlib_System_Threading_Tasks_Shared_1_gen3839187714.h"
#include "mscorlib_System_Threading_Tasks_Shared_1_gen2858597776.h"
#include "mscorlib_System_Threading_CancellationTokenRegistr1708859357.h"
#include "mscorlib_System_Threading_Tasks_SystemThreadingTas1098291612.h"
#include "mscorlib_System_Threading_Tasks_TaskCreationOptions547302442.h"
#include "mscorlib_System_Threading_StackCrawlMark2550138982.h"
#include "mscorlib_System_Threading_Tasks_InternalTaskOption4086034348.h"
#include "mscorlib_System_Threading_Tasks_TaskScheduler3932792796.h"
#include "mscorlib_System_Func_1_gen1485000104.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Threading_Tasks_Task_ContingentProp606988207.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_gen220925529.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen963265114.h"
#include "mscorlib_System_Func_2_gen3758303068.h"
#include "mscorlib_System_Func_2_gen2207932334.h"
#include "mscorlib_System_Func_1_gen4026270130.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_gen2762195555.h"
#include "mscorlib_System_Func_2_gen2004605798.h"
#include "mscorlib_System_Func_2_gen2825504181.h"
#include "mscorlib_System_Func_1_gen348874681.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_gen3379767402.h"
#include "mscorlib_System_Func_2_gen2622177645.h"
#include "mscorlib_System_Func_2_gen3461365684.h"
#include "mscorlib_System_Func_1_gen984736184.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_gen4015628905.h"
#include "mscorlib_System_Func_2_gen3258039148.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CF2092671311.h"
#include "mscorlib_System_Threading_AtomicBoolean379413895.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CFr768948844.h"
#include "mscorlib_System_Func_2_gen3480198991.h"
#include "mscorlib_System_Action_1_gen1801450390.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CFr338974041.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CF3310218870.h"
#include "mscorlib_System_Func_2_gen1726501721.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CFr956545888.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CF3927790717.h"
#include "mscorlib_System_Func_2_gen2344073568.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CF1592407391.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CFr268684924.h"
#include "mscorlib_System_Func_2_gen2979935071.h"
#include "mscorlib_System_Threading_Tasks_TaskContinuationOp2826278702.h"
#include "mscorlib_System_Threading_ThreadAbortException1150575753.h"
#include "mscorlib_System_Threading_Tasks_TaskExceptionHolde2208677448.h"
#include "mscorlib_System_Func_3_gen1604491142.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"

// System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1<System.Object>
struct U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3658276611;
// System.Object
struct Il2CppObject;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t3157253607;
// System.Threading.Thread
struct Thread_t241561612;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.NotImplementedException
struct NotImplementedException_t2785117854;
// System.Linq.Enumerable/WhereArrayIterator`1<System.Object>
struct WhereArrayIterator_1_t2998102595;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct WhereEnumerableIterator_1_t3208243354;
// System.Linq.Enumerable/WhereListIterator`1<System.Object>
struct WhereListIterator_1_t1069043000;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.String
struct String_t;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t2268544833;
// System.Delegate
struct Delegate_t3022476291;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2126074551;
// System.Predicate`1<System.DateTime>
struct Predicate_1_t3431143080;
// System.Predicate`1<System.DateTimeOffset>
struct Predicate_1_t4100926317;
// System.Predicate`1<System.Decimal>
struct Predicate_1_t3462638488;
// System.Predicate`1<System.Double>
struct Predicate_1_t2520985796;
// System.Predicate`1<System.Int16>
struct Predicate_1_t2484216029;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Int64>
struct Predicate_1_t3647015448;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.SByte>
struct Predicate_1_t3192354960;
// System.Predicate`1<System.Single>
struct Predicate_1_t519480047;
// System.Predicate`1<System.Text.RegularExpressions.RegexOptions>
struct Predicate_1_t861229842;
// System.Predicate`1<System.TimeSpan>
struct Predicate_1_t1873229064;
// System.Predicate`1<System.UInt16>
struct Predicate_1_t3724820022;
// System.Predicate`1<System.UInt32>
struct Predicate_1_t592652136;
// System.Predicate`1<System.UInt64>
struct Predicate_1_t1352167029;
// System.Predicate`1<System.Xml.Schema.RangePositionInfo>
struct Predicate_1_t1223773037;
// System.Predicate`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct Predicate_1_t953556625;
// System.Predicate`1<UnityEngine.AnimatorClipInfo>
struct Predicate_1_t2348721464;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t4179406139;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t1095697167;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t3152578875;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t2945603725;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Exception
struct Exception_t1927440687;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.OperationCanceledException
struct OperationCanceledException_t2897400967;
// System.Type
struct Type_t;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t1191906455;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_t1809478302;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>
struct ConditionalWeakTable_2_t1530143791;
// System.Runtime.CompilerServices.Ephemeron[]
struct EphemeronU5BU5D_t3006146916;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.Action
struct Action_t3226471752;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t2445339805;
// System.Array
struct Il2CppArray;
// System.Threading.AsyncLocal`1<System.Object>
struct AsyncLocal_1_t31129824;
// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>
struct Action_1_t981956716;
// System.Threading.IAsyncLocal
struct IAsyncLocal_t3643786784;
// System.Threading.SparselyPopulatedArray`1<System.Object>
struct SparselyPopulatedArray_1_t1092078389;
// System.Threading.SparselyPopulatedArrayFragment`1<System.Object>
struct SparselyPopulatedArrayFragment_1_t3870933437;
// System.Threading.Tasks.Shared`1<System.Object>
struct Shared_1_t3839187714;
// System.Threading.Tasks.Shared`1<System.Threading.CancellationTokenRegistration>
struct Shared_1_t2858597776;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t279959794;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t606988207;
// System.Threading.Tasks.TaskFactory`1<System.Boolean>
struct TaskFactory_1_t220925529;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>
struct Task_1_t963265114;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.Func`1<System.Int32>
struct Func_1_t4026270130;
// System.Threading.Tasks.TaskFactory`1<System.Int32>
struct TaskFactory_1_t2762195555;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Func`1<System.Object>
struct Func_1_t348874681;
// System.Threading.Tasks.TaskFactory`1<System.Object>
struct TaskFactory_1_t3379767402;
// System.Func`2<System.Object,System.Threading.Tasks.VoidTaskResult>
struct Func_2_t3461365684;
// System.Func`1<System.Threading.Tasks.VoidTaskResult>
struct Func_1_t984736184;
// System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>
struct TaskFactory_1_t4015628905;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Boolean>
struct U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311;
// System.Threading.AtomicBoolean
struct AtomicBoolean_t379413895;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Int32>
struct U3CFromAsyncImplU3Ec__AnonStorey1_t338974041;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Object>
struct U3CFromAsyncImplU3Ec__AnonStorey1_t956545888;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Threading.Tasks.VoidTaskResult>
struct U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Boolean>
struct U3CFromAsyncImplU3Ec__AnonStorey2_t768948844;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Int32>
struct U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Object>
struct U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Threading.Tasks.VoidTaskResult>
struct U3CFromAsyncImplU3Ec__AnonStorey2_t268684924;
// System.Func`2<System.IAsyncResult,System.Boolean>
struct Func_2_t3480198991;
// System.Action`1<System.IAsyncResult>
struct Action_1_t1801450390;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Threading.Tasks.TaskExceptionHolder
struct TaskExceptionHolder_t2208677448;
// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t1604491142;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Func`2<System.IAsyncResult,System.Int32>
struct Func_2_t1726501721;
// System.Func`2<System.IAsyncResult,System.Object>
struct Func_2_t2344073568;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_Dispose_m1990198537_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t Iterator_1_System_Collections_IEnumerator_Reset_m1017663060_MetadataUsageId;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t WhereEnumerableIterator_1_Dispose_m2421906470_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t WhereEnumerableIterator_1_MoveNext_m2092954763_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3600185652_MetadataUsageId;
extern Il2CppClass* MonoSslPolicyErrors_t621534536_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1226087740_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3150199456_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m840862642_MetadataUsageId;
extern Il2CppClass* TlsProtocols_t1951446164_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m686690116_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m2516240674_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m1158440123_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1318699267_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m678068069_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m1118025076_MetadataUsageId;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1089953100_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1419821888_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m2201992629_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m2848647165_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m2285560203_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m3860982732_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern const uint32_t Predicate_1_BeginInvoke_m1973834093_MetadataUsageId;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m767759683_MetadataUsageId;
extern const uint32_t Predicate_1_BeginInvoke_m2874596364_MetadataUsageId;
extern Il2CppClass* DateTimeOffset_t1362988906_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3523490745_MetadataUsageId;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3982629388_MetadataUsageId;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2128514704_MetadataUsageId;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1777010513_MetadataUsageId;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2896842008_MetadataUsageId;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3863950084_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3791856215_MetadataUsageId;
extern Il2CppClass* RegexOptions_t2418259727_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2847696724_MetadataUsageId;
extern const uint32_t Predicate_1_BeginInvoke_m2757499760_MetadataUsageId;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m946193182_MetadataUsageId;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m739040720_MetadataUsageId;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3457072769_MetadataUsageId;
extern Il2CppClass* RangePositionInfo_t2780802922_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m930472829_MetadataUsageId;
extern Il2CppClass* XmlSchemaObjectEntry_t2510586510_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1405349914_MetadataUsageId;
extern Il2CppClass* AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2336395304_MetadataUsageId;
extern Il2CppClass* AsyncTaskMethodBuilder_1_t2408546353_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTaskMethodBuilder_1_Create_m3160705057_MetadataUsageId;
extern Il2CppClass* Task_t1843236107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3568671706;
extern const uint32_t AsyncTaskMethodBuilder_1_SetResult_m3861164633_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* OperationCanceledException_t2897400967_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2453856579;
extern const uint32_t AsyncTaskMethodBuilder_1_SetException_m3130084070_MetadataUsageId;
extern const Il2CppType* Boolean_t3825574718_0_0_0_var;
extern const Il2CppType* Int32_t2071877448_0_0_0_var;
extern const Il2CppType* UInt32_t2149682021_0_0_0_var;
extern const Il2CppType* Byte_t3683104436_0_0_0_var;
extern const Il2CppType* SByte_t454417549_0_0_0_var;
extern const Il2CppType* Char_t3454481338_0_0_0_var;
extern const Il2CppType* Decimal_t724701077_0_0_0_var;
extern const Il2CppType* Int64_t909078037_0_0_0_var;
extern const Il2CppType* UInt64_t2909196914_0_0_0_var;
extern const Il2CppType* Int16_t4041245914_0_0_0_var;
extern const Il2CppType* UInt16_t986882611_0_0_0_var;
extern const Il2CppType* IntPtr_t_0_0_0_var;
extern const Il2CppType* UIntPtr_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_MetadataUsageId;
extern const uint32_t AsyncTaskMethodBuilder_1__cctor_m3426799844_MetadataUsageId;
extern Il2CppClass* AsyncTaskMethodBuilder_1_t1272420930_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTaskMethodBuilder_1_Create_m3623012989_MetadataUsageId;
extern const uint32_t AsyncTaskMethodBuilder_1_SetResult_m4050605073_MetadataUsageId;
extern const uint32_t AsyncTaskMethodBuilder_1_SetException_m4054625890_MetadataUsageId;
extern const uint32_t AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_MetadataUsageId;
extern const uint32_t AsyncTaskMethodBuilder_1__cctor_m4171130357_MetadataUsageId;
extern Il2CppClass* EphemeronU5BU5D_t3006146916_il2cpp_TypeInfo_var;
extern Il2CppClass* GC_t2902933594_il2cpp_TypeInfo_var;
extern const uint32_t ConditionalWeakTable_2__ctor_m3945060102_MetadataUsageId;
extern Il2CppClass* HashHelpers_t2236981683_il2cpp_TypeInfo_var;
extern const uint32_t ConditionalWeakTable_2_Rehash_m2504890315_MetadataUsageId;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1609019432;
extern Il2CppCodeGenString* _stringLiteral3021628599;
extern Il2CppCodeGenString* _stringLiteral2956391297;
extern const uint32_t ConditionalWeakTable_2_Add_m1961639487_MetadataUsageId;
extern const uint32_t ConditionalWeakTable_2_Remove_m554617975_MetadataUsageId;
extern const uint32_t ConditionalWeakTable_2_TryGetValue_m2783224372_MetadataUsageId;
extern Il2CppClass* ExecutionContext_t1392266323_il2cpp_TypeInfo_var;
extern const uint32_t AsyncLocal_1_set_Value_m3424254923_MetadataUsageId;
extern const uint32_t AsyncLocal_1_System_Threading_IAsyncLocal_OnValueChanged_m341948749_MetadataUsageId;
extern Il2CppClass* AsyncLocalValueChangedArgs_1_t1180157334_il2cpp_TypeInfo_var;
extern const uint32_t AsyncLocalValueChangedArgs_1__ctor_m1667703908_MetadataUsageId;
extern const uint32_t Task_1__ctor_m314248096_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2571213091_MetadataUsageId;
extern Il2CppClass* CancellationToken_t1851405782_il2cpp_TypeInfo_var;
extern const uint32_t Task_1__ctor_m4222636859_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1487818250_MetadataUsageId;
extern const uint32_t Task_1__ctor_m4001507066_MetadataUsageId;
extern const uint32_t Task_1__ctor_m4131560672_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1588292733;
extern Il2CppCodeGenString* _stringLiteral2943658594;
extern const uint32_t Task_1__ctor_m3631457423_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2769197254_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral878805882;
extern Il2CppCodeGenString* _stringLiteral3323468611;
extern const uint32_t Task_1_StartNew_m2115868853_MetadataUsageId;
extern const uint32_t Task_1_GetResultCore_m410448687_MetadataUsageId;
extern const MethodInfo* Task_1_get_Result_m1316373833_MethodInfo_var;
extern const uint32_t Task_1_U3CTaskWhenAnyCastU3Em__0_m3063195731_MetadataUsageId;
extern const uint32_t Task_1__ctor_m172046308_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1030422093_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2344439049_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2745384578_MetadataUsageId;
extern const uint32_t Task_1__ctor_m298167690_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1777040968_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2408210937_MetadataUsageId;
extern const uint32_t Task_1__ctor_m968257538_MetadataUsageId;
extern const uint32_t Task_1_StartNew_m870754347_MetadataUsageId;
extern const uint32_t Task_1_GetResultCore_m88725665_MetadataUsageId;
extern const uint32_t Task_1_U3CTaskWhenAnyCastU3Em__0_m4259118645_MetadataUsageId;
extern const uint32_t Task_1__ctor_m213817015_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3678853734_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3150838094_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1204176217_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3239156755_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1658954423_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3126613762_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3688860927_MetadataUsageId;
extern const uint32_t Task_1_StartNew_m3808576068_MetadataUsageId;
extern const uint32_t Task_1_GetResultCore_m2608934740_MetadataUsageId;
extern const uint32_t Task_1_U3CTaskWhenAnyCastU3Em__0_m1495298862_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3445550078_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3175258807_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2127647879_MetadataUsageId;
extern const uint32_t Task_1__ctor_m3068577956_MetadataUsageId;
extern const uint32_t Task_1__ctor_m2082199556_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1861180062_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1693928371_MetadataUsageId;
extern const uint32_t Task_1__ctor_m1335966936_MetadataUsageId;
extern const uint32_t Task_1_StartNew_m3556824229_MetadataUsageId;
extern const uint32_t Task_1_GetResultCore_m2672380779_MetadataUsageId;
extern const uint32_t Task_1_U3CTaskWhenAnyCastU3Em__0_m214963539_MetadataUsageId;
extern const uint32_t TaskFactory_1__ctor_m1536387660_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadAbortException_t1150575753_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2407548955_MethodInfo_var;
extern const uint32_t TaskFactory_1_FromAsyncCoreLogic_m3757763575_MetadataUsageId;
extern Il2CppClass* BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var;
extern Il2CppClass* AtomicBoolean_t379413895_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern Il2CppClass* IAsyncResult_t1999651008_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_Invoke_m2184768394_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral784393026;
extern Il2CppCodeGenString* _stringLiteral3599838238;
extern Il2CppCodeGenString* _stringLiteral2115721401;
extern const uint32_t TaskFactory_1_FromAsyncImpl_m1220570521_MetadataUsageId;
extern const uint32_t TaskFactory_1__ctor_m3007216488_MetadataUsageId;
extern const uint32_t TaskFactory_1_FromAsyncCoreLogic_m4081154385_MetadataUsageId;
extern const uint32_t TaskFactory_1_FromAsyncImpl_m2198391319_MetadataUsageId;
extern const uint32_t TaskFactory_1__ctor_m2717008271_MetadataUsageId;
extern const uint32_t TaskFactory_1_FromAsyncCoreLogic_m3841454974_MetadataUsageId;
extern const uint32_t TaskFactory_1_FromAsyncImpl_m4043021740_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1606206610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3022476291 * m_Items[1];

public:
	inline Delegate_t3022476291 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t3022476291 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Threading.Tasks.Task`1<System.Int32>[]
struct Task_1U5BU5D_t2481017646  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Task_1_t1191906455 * m_Items[1];

public:
	inline Task_1_t1191906455 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Task_1_t1191906455 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Task_1_t1191906455 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Task_1_t1191906455 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Task_1_t1191906455 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Task_1_t1191906455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.CompilerServices.Ephemeron[]
struct EphemeronU5BU5D_t3006146916  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Ephemeron_t1875076633  m_Items[1];

public:
	inline Ephemeron_t1875076633  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Ephemeron_t1875076633 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Ephemeron_t1875076633  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Ephemeron_t1875076633  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Ephemeron_t1875076633 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Ephemeron_t1875076633  value)
	{
		m_Items[index] = value;
	}
};


// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3108634708_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::.ctor(T)
extern "C"  void Nullable_1__ctor_m235056776_gshared (Nullable_1_t3179568147 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1085446683_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method);
// T System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3600185652_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2263234329_gshared (Nullable_1_t3179568147 * __this, Nullable_1_t3179568147  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1226087740_gshared (Nullable_1_t3179568147 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2766960772_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method);
// System.String System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3150199456_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<Mono.Security.Interface.TlsProtocols>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1255861601_gshared (Nullable_1_t214512479 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2390520973_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method);
// T System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m840862642_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3528580847_gshared (Nullable_1_t214512479 * __this, Nullable_1_t214512479  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m686690116_gshared (Nullable_1_t214512479 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<Mono.Security.Interface.TlsProtocols>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m133456640_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method);
// System.String System.Nullable`1<Mono.Security.Interface.TlsProtocols>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2516240674_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1785320616_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1428381008_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C"  bool Nullable_1_get_Value_m1158440123_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3532149783_gshared (Nullable_1_t3251239280 * __this, DateTime_t693205669  ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4025045115_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method);
// T System.Nullable`1<System.DateTime>::get_Value()
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m1118025076_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1817623273_gshared (Nullable_1_t3251239280 * __this, Nullable_1_t3251239280  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1089953100_gshared (Nullable_1_t3251239280 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.DateTime>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1419821888_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1825326012_gshared (Nullable_1_t334943763 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1182428964_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Int32>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2201992629_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1118562548_gshared (Nullable_1_t334943763 * __this, Nullable_1_t334943763  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2848647165_gshared (Nullable_1_t334943763 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.Int32>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2285560203_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::get_Task()
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_get_Task_m3423503422_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, const MethodInfo* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::GetTaskForResult(TResult)
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, bool p0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetResult(TResult)
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m3861164633_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, bool ___result0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetException(System.Exception)
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m3130084070_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_get_Task_m2705772806_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, const MethodInfo* method);
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::GetTaskForResult(TResult)
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetResult(TResult)
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m4050605073_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * ___result0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetException(System.Exception)
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m4054625890_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m136027847_gshared (ConfiguredTaskAwaiter_t446638270 * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2127236892_gshared (ConfiguredTaskAwaiter_t446638270 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015_gshared (ConfiguredTaskAwaiter_t446638270 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::GetResult()
extern "C"  bool ConfiguredTaskAwaiter_GetResult_m2361999369_gshared (ConfiguredTaskAwaiter_t446638270 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m3345350369_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m998688528_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::GetResult()
extern "C"  int32_t ConfiguredTaskAwaiter_GetResult_m1667949563_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m2085377116_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m1244481799_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::GetResult()
extern "C"  Il2CppObject * ConfiguredTaskAwaiter_GetResult_m417350004_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m3341903075_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2053182946_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::GetResult()
extern "C"  VoidTaskResult_t3325310798  ConfiguredTaskAwaiter_GetResult_m1629433097_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m1905790602_gshared (ConfiguredTaskAwaitable_1_t34161435 * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t446638270  ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489_gshared (ConfiguredTaskAwaitable_1_t34161435 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2687722690_gshared (ConfiguredTaskAwaitable_1_t2575431461 * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t2987908296  ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283_gshared (ConfiguredTaskAwaitable_1_t2575431461 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m740361621_gshared (ConfiguredTaskAwaitable_1_t3193003308 * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t3605480143  ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240_gshared (ConfiguredTaskAwaitable_1_t3193003308 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2250484098_gshared (ConfiguredTaskAwaitable_1_t3828864811 * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method);
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t4241341646  ConfiguredTaskAwaitable_1_GetAwaiter_m56114985_gshared (ConfiguredTaskAwaitable_1_t3828864811 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m3987458582_gshared (TaskAwaiter_1_t899200082 * __this, Task_1_t2945603725 * ___task0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1467565677_gshared (TaskAwaiter_1_t899200082 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::GetResult()
extern "C"  bool TaskAwaiter_1_GetResult_m2570428347_gshared (TaskAwaiter_1_t899200082 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m3485680026_gshared (TaskAwaiter_1_t3440470108 * __this, Task_1_t1191906455 * ___task0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1344578231_gshared (TaskAwaiter_1_t3440470108 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::GetResult()
extern "C"  int32_t TaskAwaiter_1_GetResult_m2916616501_gshared (TaskAwaiter_1_t3440470108 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m882453359_gshared (TaskAwaiter_1_t4058041955 * __this, Task_1_t1809478302 * ___task0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m3372870064_gshared (TaskAwaiter_1_t4058041955 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::GetResult()
extern "C"  Il2CppObject * TaskAwaiter_1_GetResult_m2924234054_gshared (TaskAwaiter_1_t4058041955 * __this, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m4174446952_gshared (TaskAwaiter_1_t398936162 * __this, Task_1_t2445339805 * ___task0, const MethodInfo* method);
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m522515533_gshared (TaskAwaiter_1_t398936162 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method);
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::GetResult()
extern "C"  VoidTaskResult_t3325310798  TaskAwaiter_1_GetResult_m2990722415_gshared (TaskAwaiter_1_t398936162 * __this, const MethodInfo* method);
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::.ctor(System.Int32)
extern "C"  void ListBuilder_1__ctor_m2403890235_gshared (ListBuilder_1_t3063240520 * __this, int32_t ___capacity0, const MethodInfo* method);
// T System.RuntimeType/ListBuilder`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListBuilder_1_get_Item_m2628732860_gshared (ListBuilder_1_t3063240520 * __this, int32_t ___index0, const MethodInfo* method);
// T[] System.RuntimeType/ListBuilder`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* ListBuilder_1_ToArray_m1877055377_gshared (ListBuilder_1_t3063240520 * __this, const MethodInfo* method);
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::CopyTo(System.Object[],System.Int32)
extern "C"  void ListBuilder_1_CopyTo_m1925739081_gshared (ListBuilder_1_t3063240520 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
// System.Int32 System.RuntimeType/ListBuilder`1<System.Object>::get_Count()
extern "C"  int32_t ListBuilder_1_get_Count_m2360264158_gshared (ListBuilder_1_t3063240520 * __this, const MethodInfo* method);
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::Add(T)
extern "C"  void ListBuilder_1_Add_m54035051_gshared (ListBuilder_1_t3063240520 * __this, Il2CppObject * ___item0, const MethodInfo* method);
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::.ctor(T,T,System.Boolean)
extern "C"  void AsyncLocalValueChangedArgs_1__ctor_m1667703908_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * p0, Il2CppObject * p1, bool p2, const MethodInfo* method);
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_PreviousValue(T)
extern "C"  void AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_CurrentValue(T)
extern "C"  void AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_ThreadContextChanged(System.Boolean)
extern "C"  void AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, bool p0, const MethodInfo* method);
// T System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, const MethodInfo* method);
// System.Void System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::.ctor(System.Threading.SparselyPopulatedArrayFragment`1<T>,System.Int32)
extern "C"  void SparselyPopulatedArrayAddInfo_1__ctor_m1128745264_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, SparselyPopulatedArrayFragment_1_t3870933437 * p0, int32_t p1, const MethodInfo* method);
// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Source()
extern "C"  SparselyPopulatedArrayFragment_1_t3870933437 * SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, const MethodInfo* method);
// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Index()
extern "C"  int32_t SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, const MethodInfo* method);
// TResult System.Threading.Tasks.Task`1<System.Object>::get_Result()
extern "C"  Il2CppObject * Task_1_get_Result_m2758788574_gshared (Task_1_t1809478302 * __this, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * p0, const MethodInfo* method);
// TResult System.Func`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1656275607_gshared (Func_3_t3369346583 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t241561612 * Thread_get_CurrentThread_m3667342817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1995754972 (Thread_t241561612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m808189835 (NotImplementedException_t2785117854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m3108634708(__this, method) ((  Il2CppObject * (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m44995089(__this, method) ((  bool (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::.ctor(T)
#define Nullable_1__ctor_m235056776(__this, ___value0, method) ((  void (*) (Nullable_1_t3179568147 *, int32_t, const MethodInfo*))Nullable_1__ctor_m235056776_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_HasValue()
#define Nullable_1_get_HasValue_m1085446683(__this, method) ((  bool (*) (Nullable_1_t3179568147 *, const MethodInfo*))Nullable_1_get_HasValue_m1085446683_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_Value()
#define Nullable_1_get_Value_m3600185652(__this, method) ((  int32_t (*) (Nullable_1_t3179568147 *, const MethodInfo*))Nullable_1_get_Value_m3600185652_gshared)(__this, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m2263234329(__this, p0, method) ((  bool (*) (Nullable_1_t3179568147 *, Nullable_1_t3179568147 , const MethodInfo*))Nullable_1_Equals_m2263234329_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Object)
#define Nullable_1_Equals_m1226087740(__this, ___other0, method) ((  bool (*) (Nullable_1_t3179568147 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1226087740_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::GetHashCode()
#define Nullable_1_GetHashCode_m2766960772(__this, method) ((  int32_t (*) (Nullable_1_t3179568147 *, const MethodInfo*))Nullable_1_GetHashCode_m2766960772_gshared)(__this, method)
// System.String System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::ToString()
#define Nullable_1_ToString_m3150199456(__this, method) ((  String_t* (*) (Nullable_1_t3179568147 *, const MethodInfo*))Nullable_1_ToString_m3150199456_gshared)(__this, method)
// System.Void System.Nullable`1<Mono.Security.Interface.TlsProtocols>::.ctor(T)
#define Nullable_1__ctor_m1255861601(__this, ___value0, method) ((  void (*) (Nullable_1_t214512479 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1255861601_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_HasValue()
#define Nullable_1_get_HasValue_m2390520973(__this, method) ((  bool (*) (Nullable_1_t214512479 *, const MethodInfo*))Nullable_1_get_HasValue_m2390520973_gshared)(__this, method)
// T System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_Value()
#define Nullable_1_get_Value_m840862642(__this, method) ((  int32_t (*) (Nullable_1_t214512479 *, const MethodInfo*))Nullable_1_get_Value_m840862642_gshared)(__this, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m3528580847(__this, p0, method) ((  bool (*) (Nullable_1_t214512479 *, Nullable_1_t214512479 , const MethodInfo*))Nullable_1_Equals_m3528580847_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Object)
#define Nullable_1_Equals_m686690116(__this, ___other0, method) ((  bool (*) (Nullable_1_t214512479 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m686690116_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Mono.Security.Interface.TlsProtocols>::GetHashCode()
#define Nullable_1_GetHashCode_m133456640(__this, method) ((  int32_t (*) (Nullable_1_t214512479 *, const MethodInfo*))Nullable_1_GetHashCode_m133456640_gshared)(__this, method)
// System.String System.Nullable`1<Mono.Security.Interface.TlsProtocols>::ToString()
#define Nullable_1_ToString_m2516240674(__this, method) ((  String_t* (*) (Nullable_1_t214512479 *, const MethodInfo*))Nullable_1_ToString_m2516240674_gshared)(__this, method)
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
#define Nullable_1__ctor_m1785320616(__this, ___value0, method) ((  void (*) (Nullable_1_t2088641033 *, bool, const MethodInfo*))Nullable_1__ctor_m1785320616_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
#define Nullable_1_get_HasValue_m1428381008(__this, method) ((  bool (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_get_HasValue_m1428381008_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
#define Nullable_1_get_Value_m1158440123(__this, method) ((  bool (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_get_Value_m1158440123_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m2189684888(__this, p0, method) ((  bool (*) (Nullable_1_t2088641033 *, Nullable_1_t2088641033 , const MethodInfo*))Nullable_1_Equals_m2189684888_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
#define Nullable_1_Equals_m1318699267(__this, ___other0, method) ((  bool (*) (Nullable_1_t2088641033 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1318699267_gshared)(__this, ___other0, method)
// System.Boolean System.Boolean::Equals(System.Object)
extern "C"  bool Boolean_Equals_m2118901528 (bool* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1894638460 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
#define Nullable_1_GetHashCode_m1645245653(__this, method) ((  int32_t (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_GetHashCode_m1645245653_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.Boolean>::ToString()
#define Nullable_1_ToString_m678068069(__this, method) ((  String_t* (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_ToString_m678068069_gshared)(__this, method)
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
#define Nullable_1__ctor_m3532149783(__this, ___value0, method) ((  void (*) (Nullable_1_t3251239280 *, DateTime_t693205669 , const MethodInfo*))Nullable_1__ctor_m3532149783_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
#define Nullable_1_get_HasValue_m4025045115(__this, method) ((  bool (*) (Nullable_1_t3251239280 *, const MethodInfo*))Nullable_1_get_HasValue_m4025045115_gshared)(__this, method)
// T System.Nullable`1<System.DateTime>::get_Value()
#define Nullable_1_get_Value_m1118025076(__this, method) ((  DateTime_t693205669  (*) (Nullable_1_t3251239280 *, const MethodInfo*))Nullable_1_get_Value_m1118025076_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m1817623273(__this, p0, method) ((  bool (*) (Nullable_1_t3251239280 *, Nullable_1_t3251239280 , const MethodInfo*))Nullable_1_Equals_m1817623273_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
#define Nullable_1_Equals_m1089953100(__this, ___other0, method) ((  bool (*) (Nullable_1_t3251239280 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1089953100_gshared)(__this, ___other0, method)
// System.Boolean System.DateTime::Equals(System.Object)
extern "C"  bool DateTime_Equals_m2562884703 (DateTime_t693205669 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m974799321 (DateTime_t693205669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
#define Nullable_1_GetHashCode_m3047479588(__this, method) ((  int32_t (*) (Nullable_1_t3251239280 *, const MethodInfo*))Nullable_1_GetHashCode_m3047479588_gshared)(__this, method)
// System.String System.DateTime::ToString()
extern "C"  String_t* DateTime_ToString_m1117481977 (DateTime_t693205669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.DateTime>::ToString()
#define Nullable_1_ToString_m1419821888(__this, method) ((  String_t* (*) (Nullable_1_t3251239280 *, const MethodInfo*))Nullable_1_ToString_m1419821888_gshared)(__this, method)
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
#define Nullable_1__ctor_m1825326012(__this, ___value0, method) ((  void (*) (Nullable_1_t334943763 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1825326012_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
#define Nullable_1_get_HasValue_m1182428964(__this, method) ((  bool (*) (Nullable_1_t334943763 *, const MethodInfo*))Nullable_1_get_HasValue_m1182428964_gshared)(__this, method)
// T System.Nullable`1<System.Int32>::get_Value()
#define Nullable_1_get_Value_m2201992629(__this, method) ((  int32_t (*) (Nullable_1_t334943763 *, const MethodInfo*))Nullable_1_get_Value_m2201992629_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m1118562548(__this, p0, method) ((  bool (*) (Nullable_1_t334943763 *, Nullable_1_t334943763 , const MethodInfo*))Nullable_1_Equals_m1118562548_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
#define Nullable_1_Equals_m2848647165(__this, ___other0, method) ((  bool (*) (Nullable_1_t334943763 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2848647165_gshared)(__this, ___other0, method)
// System.Boolean System.Int32::Equals(System.Object)
extern "C"  bool Int32_Equals_m753832628 (int32_t* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m1381647448 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
#define Nullable_1_GetHashCode_m1859855859(__this, method) ((  int32_t (*) (Nullable_1_t334943763 *, const MethodInfo*))Nullable_1_GetHashCode_m1859855859_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.Int32>::ToString()
#define Nullable_1_ToString_m2285560203(__this, method) ((  String_t* (*) (Nullable_1_t334943763 *, const MethodInfo*))Nullable_1_ToString_m2285560203_gshared)(__this, method)
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
#define Nullable_1__ctor_m796575255(__this, ___value0, method) ((  void (*) (Nullable_1_t1693325264 *, TimeSpan_t3430258949 , const MethodInfo*))Nullable_1__ctor_m796575255_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
#define Nullable_1_get_HasValue_m3663286555(__this, method) ((  bool (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_get_HasValue_m3663286555_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
#define Nullable_1_get_Value_m1743067844(__this, method) ((  TimeSpan_t3430258949  (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_get_Value_m1743067844_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m1889119397(__this, p0, method) ((  bool (*) (Nullable_1_t1693325264 *, Nullable_1_t1693325264 , const MethodInfo*))Nullable_1_Equals_m1889119397_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
#define Nullable_1_Equals_m3860982732(__this, ___other0, method) ((  bool (*) (Nullable_1_t1693325264 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3860982732_gshared)(__this, ___other0, method)
// System.Boolean System.TimeSpan::Equals(System.Object)
extern "C"  bool TimeSpan_Equals_m4102942751 (TimeSpan_t3430258949 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m550404245 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
#define Nullable_1_GetHashCode_m1791015856(__this, method) ((  int32_t (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_GetHashCode_m1791015856_gshared)(__this, method)
// System.String System.TimeSpan::ToString()
extern "C"  String_t* TimeSpan_ToString_m2947282901 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.TimeSpan>::ToString()
#define Nullable_1_ToString_m1238126148(__this, method) ((  String_t* (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_ToString_m1238126148_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.AsyncMethodBuilderCore::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern "C"  void AsyncMethodBuilderCore_SetStateMachine_m1089807594 (AsyncMethodBuilderCore_t2485284745 * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
#define AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434(__this, ___stateMachine0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t2408546353 *, Il2CppObject *, const MethodInfo*))AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434_gshared)(__this, ___stateMachine0, method)
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::get_Task()
#define AsyncTaskMethodBuilder_1_get_Task_m3423503422(__this, method) ((  Task_1_t2945603725 * (*) (AsyncTaskMethodBuilder_1_t2408546353 *, const MethodInfo*))AsyncTaskMethodBuilder_1_get_Task_m3423503422_gshared)(__this, method)
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::GetTaskForResult(TResult)
#define AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084(__this, p0, method) ((  Task_1_t2945603725 * (*) (AsyncTaskMethodBuilder_1_t2408546353 *, bool, const MethodInfo*))AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_gshared)(__this, p0, method)
// System.Boolean System.Threading.Tasks.AsyncCausalityTracer::get_LoggingOn()
extern "C"  bool AsyncCausalityTracer_get_LoggingOn_m929857466 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Tasks.Task::get_Id()
extern "C"  int32_t Task_get_Id_m4106115082 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.AsyncCausalityTracer::TraceOperationCompletion(System.Threading.Tasks.CausalityTraceLevel,System.Int32,System.Threading.Tasks.AsyncCausalityStatus)
extern "C" IL2CPP_NO_INLINE void AsyncCausalityTracer_TraceOperationCompletion_m1161622555 (Il2CppObject * __this /* static, unused */, int32_t ___traceLevel0, int32_t ___taskId1, int32_t ___status2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::RemoveFromActiveTasks(System.Int32)
extern "C"  void Task_RemoveFromActiveTasks_m437324001 (Il2CppObject * __this /* static, unused */, int32_t ___taskId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetResourceString(System.String)
extern "C"  String_t* Environment_GetResourceString_m2533878090 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetResult(TResult)
#define AsyncTaskMethodBuilder_1_SetResult_m3861164633(__this, ___result0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t2408546353 *, bool, const MethodInfo*))AsyncTaskMethodBuilder_1_SetResult_m3861164633_gshared)(__this, ___result0, method)
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CancellationToken System.OperationCanceledException::get_CancellationToken()
extern "C"  CancellationToken_t1851405782  OperationCanceledException_get_CancellationToken_m821612483 (OperationCanceledException_t2897400967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetException(System.Exception)
#define AsyncTaskMethodBuilder_1_SetException_m3130084070(__this, ___exception0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t2408546353 *, Exception_t1927440687 *, const MethodInfo*))AsyncTaskMethodBuilder_1_SetException_m3130084070_gshared)(__this, ___exception0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
extern "C"  bool Type_op_Equality_m3620493675 (Il2CppObject * __this /* static, unused */, Type_t * ___left0, Type_t * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Int32)
extern "C"  void Decimal__ctor_m1010012873 (Decimal_t724701077 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_Equality(System.Decimal,System.Decimal)
extern "C"  bool Decimal_op_Equality_m2278618154 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___d10, Decimal_t724701077  ___d21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1573482188 (Il2CppObject * __this /* static, unused */, IntPtr_t ___value10, IntPtr_t ___value21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UIntPtr::op_Equality(System.UIntPtr,System.UIntPtr)
extern "C"  bool UIntPtr_op_Equality_m1435732519 (Il2CppObject * __this /* static, unused */, UIntPtr_t  ___value10, UIntPtr_t  ___value21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
#define AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762(__this, ___stateMachine0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t1272420930 *, Il2CppObject *, const MethodInfo*))AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762_gshared)(__this, ___stateMachine0, method)
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
#define AsyncTaskMethodBuilder_1_get_Task_m2705772806(__this, method) ((  Task_1_t1809478302 * (*) (AsyncTaskMethodBuilder_1_t1272420930 *, const MethodInfo*))AsyncTaskMethodBuilder_1_get_Task_m2705772806_gshared)(__this, method)
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::GetTaskForResult(TResult)
#define AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425(__this, p0, method) ((  Task_1_t1809478302 * (*) (AsyncTaskMethodBuilder_1_t1272420930 *, Il2CppObject *, const MethodInfo*))AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_gshared)(__this, p0, method)
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetResult(TResult)
#define AsyncTaskMethodBuilder_1_SetResult_m4050605073(__this, ___result0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t1272420930 *, Il2CppObject *, const MethodInfo*))AsyncTaskMethodBuilder_1_SetResult_m4050605073_gshared)(__this, ___result0, method)
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetException(System.Exception)
#define AsyncTaskMethodBuilder_1_SetException_m4054625890(__this, ___exception0, method) ((  void (*) (AsyncTaskMethodBuilder_1_t1272420930 *, Exception_t1927440687 *, const MethodInfo*))AsyncTaskMethodBuilder_1_SetException_m4054625890_gshared)(__this, ___exception0, method)
// System.Void System.GC::register_ephemeron_array(System.Runtime.CompilerServices.Ephemeron[])
extern "C"  void GC_register_ephemeron_array_m3882692219 (Il2CppObject * __this /* static, unused */, EphemeronU5BU5D_t3006146916* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m4087144328 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.HashHelpers::GetPrime(System.Int32)
extern "C"  int32_t HashHelpers_GetPrime_m3996345109 (Il2CppObject * __this /* static, unused */, int32_t ___min0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::GetHashCode(System.Object)
extern "C"  int32_t RuntimeHelpers_GetHashCode_m3703616886 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m2624491786 (ArgumentNullException_t628810857 * __this, String_t* ___paramName0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C"  void Monitor_Enter_m2026616996 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, bool* ___lockTaken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m544251339 (ArgumentException_t3259014390 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaiter__ctor_m136027847(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaiter_t446638270 *, Task_1_t2945603725 *, bool, const MethodInfo*))ConfiguredTaskAwaiter__ctor_m136027847_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Boolean System.Threading.Tasks.Task::get_IsCompleted()
extern "C"  bool Task_get_IsCompleted_m669578966 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::get_IsCompleted()
#define ConfiguredTaskAwaiter_get_IsCompleted_m2127236892(__this, method) ((  bool (*) (ConfiguredTaskAwaiter_t446638270 *, const MethodInfo*))ConfiguredTaskAwaiter_get_IsCompleted_m2127236892_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter::OnCompletedInternal(System.Threading.Tasks.Task,System.Action,System.Boolean,System.Boolean)
extern "C" IL2CPP_NO_INLINE void TaskAwaiter_OnCompletedInternal_m1395860754 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___task0, Action_t3226471752 * ___continuation1, bool ___continueOnCapturedContext2, bool ___flowExecutionContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::UnsafeOnCompleted(System.Action)
#define ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015(__this, ___continuation0, method) ((  void (*) (ConfiguredTaskAwaiter_t446638270 *, Action_t3226471752 *, const MethodInfo*))ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015_gshared)(__this, ___continuation0, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter::ValidateEnd(System.Threading.Tasks.Task)
extern "C"  void TaskAwaiter_ValidateEnd_m4057275037 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::GetResult()
#define ConfiguredTaskAwaiter_GetResult_m2361999369(__this, method) ((  bool (*) (ConfiguredTaskAwaiter_t446638270 *, const MethodInfo*))ConfiguredTaskAwaiter_GetResult_m2361999369_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaiter__ctor_m3345350369(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaiter_t2987908296 *, Task_1_t1191906455 *, bool, const MethodInfo*))ConfiguredTaskAwaiter__ctor_m3345350369_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::get_IsCompleted()
#define ConfiguredTaskAwaiter_get_IsCompleted_m998688528(__this, method) ((  bool (*) (ConfiguredTaskAwaiter_t2987908296 *, const MethodInfo*))ConfiguredTaskAwaiter_get_IsCompleted_m998688528_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::UnsafeOnCompleted(System.Action)
#define ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689(__this, ___continuation0, method) ((  void (*) (ConfiguredTaskAwaiter_t2987908296 *, Action_t3226471752 *, const MethodInfo*))ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::GetResult()
#define ConfiguredTaskAwaiter_GetResult_m1667949563(__this, method) ((  int32_t (*) (ConfiguredTaskAwaiter_t2987908296 *, const MethodInfo*))ConfiguredTaskAwaiter_GetResult_m1667949563_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaiter__ctor_m2085377116(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaiter_t3605480143 *, Task_1_t1809478302 *, bool, const MethodInfo*))ConfiguredTaskAwaiter__ctor_m2085377116_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::get_IsCompleted()
#define ConfiguredTaskAwaiter_get_IsCompleted_m1244481799(__this, method) ((  bool (*) (ConfiguredTaskAwaiter_t3605480143 *, const MethodInfo*))ConfiguredTaskAwaiter_get_IsCompleted_m1244481799_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::UnsafeOnCompleted(System.Action)
#define ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546(__this, ___continuation0, method) ((  void (*) (ConfiguredTaskAwaiter_t3605480143 *, Action_t3226471752 *, const MethodInfo*))ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::GetResult()
#define ConfiguredTaskAwaiter_GetResult_m417350004(__this, method) ((  Il2CppObject * (*) (ConfiguredTaskAwaiter_t3605480143 *, const MethodInfo*))ConfiguredTaskAwaiter_GetResult_m417350004_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaiter__ctor_m3341903075(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaiter_t4241341646 *, Task_1_t2445339805 *, bool, const MethodInfo*))ConfiguredTaskAwaiter__ctor_m3341903075_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::get_IsCompleted()
#define ConfiguredTaskAwaiter_get_IsCompleted_m2053182946(__this, method) ((  bool (*) (ConfiguredTaskAwaiter_t4241341646 *, const MethodInfo*))ConfiguredTaskAwaiter_get_IsCompleted_m2053182946_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
#define ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523(__this, ___continuation0, method) ((  void (*) (ConfiguredTaskAwaiter_t4241341646 *, Action_t3226471752 *, const MethodInfo*))ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::GetResult()
#define ConfiguredTaskAwaiter_GetResult_m1629433097(__this, method) ((  VoidTaskResult_t3325310798  (*) (ConfiguredTaskAwaiter_t4241341646 *, const MethodInfo*))ConfiguredTaskAwaiter_GetResult_m1629433097_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaitable_1__ctor_m1905790602(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaitable_1_t34161435 *, Task_1_t2945603725 *, bool, const MethodInfo*))ConfiguredTaskAwaitable_1__ctor_m1905790602_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::GetAwaiter()
#define ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489(__this, method) ((  ConfiguredTaskAwaiter_t446638270  (*) (ConfiguredTaskAwaitable_1_t34161435 *, const MethodInfo*))ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaitable_1__ctor_m2687722690(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaitable_1_t2575431461 *, Task_1_t1191906455 *, bool, const MethodInfo*))ConfiguredTaskAwaitable_1__ctor_m2687722690_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::GetAwaiter()
#define ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283(__this, method) ((  ConfiguredTaskAwaiter_t2987908296  (*) (ConfiguredTaskAwaitable_1_t2575431461 *, const MethodInfo*))ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaitable_1__ctor_m740361621(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaitable_1_t3193003308 *, Task_1_t1809478302 *, bool, const MethodInfo*))ConfiguredTaskAwaitable_1__ctor_m740361621_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::GetAwaiter()
#define ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240(__this, method) ((  ConfiguredTaskAwaiter_t3605480143  (*) (ConfiguredTaskAwaitable_1_t3193003308 *, const MethodInfo*))ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
#define ConfiguredTaskAwaitable_1__ctor_m2250484098(__this, ___task0, ___continueOnCapturedContext1, method) ((  void (*) (ConfiguredTaskAwaitable_1_t3828864811 *, Task_1_t2445339805 *, bool, const MethodInfo*))ConfiguredTaskAwaitable_1__ctor_m2250484098_gshared)(__this, ___task0, ___continueOnCapturedContext1, method)
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::GetAwaiter()
#define ConfiguredTaskAwaitable_1_GetAwaiter_m56114985(__this, method) ((  ConfiguredTaskAwaiter_t4241341646  (*) (ConfiguredTaskAwaitable_1_t3828864811 *, const MethodInfo*))ConfiguredTaskAwaitable_1_GetAwaiter_m56114985_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>)
#define TaskAwaiter_1__ctor_m3987458582(__this, ___task0, method) ((  void (*) (TaskAwaiter_1_t899200082 *, Task_1_t2945603725 *, const MethodInfo*))TaskAwaiter_1__ctor_m3987458582_gshared)(__this, ___task0, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::UnsafeOnCompleted(System.Action)
#define TaskAwaiter_1_UnsafeOnCompleted_m1467565677(__this, ___continuation0, method) ((  void (*) (TaskAwaiter_1_t899200082 *, Action_t3226471752 *, const MethodInfo*))TaskAwaiter_1_UnsafeOnCompleted_m1467565677_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::GetResult()
#define TaskAwaiter_1_GetResult_m2570428347(__this, method) ((  bool (*) (TaskAwaiter_1_t899200082 *, const MethodInfo*))TaskAwaiter_1_GetResult_m2570428347_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>)
#define TaskAwaiter_1__ctor_m3485680026(__this, ___task0, method) ((  void (*) (TaskAwaiter_1_t3440470108 *, Task_1_t1191906455 *, const MethodInfo*))TaskAwaiter_1__ctor_m3485680026_gshared)(__this, ___task0, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::UnsafeOnCompleted(System.Action)
#define TaskAwaiter_1_UnsafeOnCompleted_m1344578231(__this, ___continuation0, method) ((  void (*) (TaskAwaiter_1_t3440470108 *, Action_t3226471752 *, const MethodInfo*))TaskAwaiter_1_UnsafeOnCompleted_m1344578231_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::GetResult()
#define TaskAwaiter_1_GetResult_m2916616501(__this, method) ((  int32_t (*) (TaskAwaiter_1_t3440470108 *, const MethodInfo*))TaskAwaiter_1_GetResult_m2916616501_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>)
#define TaskAwaiter_1__ctor_m882453359(__this, ___task0, method) ((  void (*) (TaskAwaiter_1_t4058041955 *, Task_1_t1809478302 *, const MethodInfo*))TaskAwaiter_1__ctor_m882453359_gshared)(__this, ___task0, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::UnsafeOnCompleted(System.Action)
#define TaskAwaiter_1_UnsafeOnCompleted_m3372870064(__this, ___continuation0, method) ((  void (*) (TaskAwaiter_1_t4058041955 *, Action_t3226471752 *, const MethodInfo*))TaskAwaiter_1_UnsafeOnCompleted_m3372870064_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::GetResult()
#define TaskAwaiter_1_GetResult_m2924234054(__this, method) ((  Il2CppObject * (*) (TaskAwaiter_1_t4058041955 *, const MethodInfo*))TaskAwaiter_1_GetResult_m2924234054_gshared)(__this, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>)
#define TaskAwaiter_1__ctor_m4174446952(__this, ___task0, method) ((  void (*) (TaskAwaiter_1_t398936162 *, Task_1_t2445339805 *, const MethodInfo*))TaskAwaiter_1__ctor_m4174446952_gshared)(__this, ___task0, method)
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
#define TaskAwaiter_1_UnsafeOnCompleted_m522515533(__this, ___continuation0, method) ((  void (*) (TaskAwaiter_1_t398936162 *, Action_t3226471752 *, const MethodInfo*))TaskAwaiter_1_UnsafeOnCompleted_m522515533_gshared)(__this, ___continuation0, method)
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::GetResult()
#define TaskAwaiter_1_GetResult_m2990722415(__this, method) ((  VoidTaskResult_t3325310798  (*) (TaskAwaiter_1_t398936162 *, const MethodInfo*))TaskAwaiter_1_GetResult_m2990722415_gshared)(__this, method)
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::.ctor(System.Int32)
#define ListBuilder_1__ctor_m2403890235(__this, ___capacity0, method) ((  void (*) (ListBuilder_1_t3063240520 *, int32_t, const MethodInfo*))ListBuilder_1__ctor_m2403890235_gshared)(__this, ___capacity0, method)
// T System.RuntimeType/ListBuilder`1<System.Object>::get_Item(System.Int32)
#define ListBuilder_1_get_Item_m2628732860(__this, ___index0, method) ((  Il2CppObject * (*) (ListBuilder_1_t3063240520 *, int32_t, const MethodInfo*))ListBuilder_1_get_Item_m2628732860_gshared)(__this, ___index0, method)
// T[] System.RuntimeType/ListBuilder`1<System.Object>::ToArray()
#define ListBuilder_1_ToArray_m1877055377(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (ListBuilder_1_t3063240520 *, const MethodInfo*))ListBuilder_1_ToArray_m1877055377_gshared)(__this, method)
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m3808317496 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___sourceArray0, int32_t ___sourceIndex1, Il2CppArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::CopyTo(System.Object[],System.Int32)
#define ListBuilder_1_CopyTo_m1925739081(__this, ___array0, ___index1, method) ((  void (*) (ListBuilder_1_t3063240520 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ListBuilder_1_CopyTo_m1925739081_gshared)(__this, ___array0, ___index1, method)
// System.Int32 System.RuntimeType/ListBuilder`1<System.Object>::get_Count()
#define ListBuilder_1_get_Count_m2360264158(__this, method) ((  int32_t (*) (ListBuilder_1_t3063240520 *, const MethodInfo*))ListBuilder_1_get_Count_m2360264158_gshared)(__this, method)
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::Add(T)
#define ListBuilder_1_Add_m54035051(__this, ___item0, method) ((  void (*) (ListBuilder_1_t3063240520 *, Il2CppObject *, const MethodInfo*))ListBuilder_1_Add_m54035051_gshared)(__this, ___item0, method)
// System.Void System.Threading.ExecutionContext::SetLocalValue(System.Threading.IAsyncLocal,System.Object,System.Boolean)
extern "C"  void ExecutionContext_SetLocalValue_m452313524 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___local0, Il2CppObject * ___newValue1, bool ___needChangeNotifications2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::.ctor(T,T,System.Boolean)
#define AsyncLocalValueChangedArgs_1__ctor_m1667703908(__this, p0, p1, p2, method) ((  void (*) (AsyncLocalValueChangedArgs_1_t1180157334 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))AsyncLocalValueChangedArgs_1__ctor_m1667703908_gshared)(__this, p0, p1, p2, method)
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_PreviousValue(T)
#define AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970(__this, p0, method) ((  void (*) (AsyncLocalValueChangedArgs_1_t1180157334 *, Il2CppObject *, const MethodInfo*))AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970_gshared)(__this, p0, method)
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_CurrentValue(T)
#define AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672(__this, p0, method) ((  void (*) (AsyncLocalValueChangedArgs_1_t1180157334 *, Il2CppObject *, const MethodInfo*))AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672_gshared)(__this, p0, method)
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_ThreadContextChanged(System.Boolean)
#define AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772(__this, p0, method) ((  void (*) (AsyncLocalValueChangedArgs_1_t1180157334 *, bool, const MethodInfo*))AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772_gshared)(__this, p0, method)
// T System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::get_CurrentValue()
#define AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739(__this, method) ((  Il2CppObject * (*) (AsyncLocalValueChangedArgs_1_t1180157334 *, const MethodInfo*))AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739_gshared)(__this, method)
// System.Void System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::.ctor(System.Threading.SparselyPopulatedArrayFragment`1<T>,System.Int32)
#define SparselyPopulatedArrayAddInfo_1__ctor_m1128745264(__this, p0, p1, method) ((  void (*) (SparselyPopulatedArrayAddInfo_1_t810080232 *, SparselyPopulatedArrayFragment_1_t3870933437 *, int32_t, const MethodInfo*))SparselyPopulatedArrayAddInfo_1__ctor_m1128745264_gshared)(__this, p0, p1, method)
// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Source()
#define SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194(__this, method) ((  SparselyPopulatedArrayFragment_1_t3870933437 * (*) (SparselyPopulatedArrayAddInfo_1_t810080232 *, const MethodInfo*))SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194_gshared)(__this, method)
// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Index()
#define SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712(__this, method) ((  int32_t (*) (SparselyPopulatedArrayAddInfo_1_t810080232 *, const MethodInfo*))SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task::.ctor()
extern "C"  void Task__ctor_m3731960308 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::.ctor(System.Object,System.Threading.Tasks.TaskCreationOptions,System.Boolean)
extern "C"  void Task__ctor_m922749962 (Task_t1843236107 * __this, Il2CppObject * ___state0, int32_t ___creationOptions1, bool ___promiseStyle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::.ctor(System.Boolean,System.Threading.Tasks.TaskCreationOptions,System.Threading.CancellationToken)
extern "C"  void Task__ctor_m2563468577 (Task_t1843236107 * __this, bool ___canceled0, int32_t ___creationOptions1, CancellationToken_t1851405782  ___ct2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task System.Threading.Tasks.Task::InternalCurrentIfAttached(System.Threading.Tasks.TaskCreationOptions)
extern "C"  Task_t1843236107 * Task_InternalCurrentIfAttached_m1086187635 (Il2CppObject * __this /* static, unused */, int32_t ___creationOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::PossiblyCaptureContext(System.Threading.StackCrawlMark&)
extern "C"  void Task_PossiblyCaptureContext_m3657345937 (Task_t1843236107 * __this, int32_t* ___stackMark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::.ctor(System.Delegate,System.Object,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task__ctor_m4014766898 (Task_t1843236107 * __this, Delegate_t3022476291 * ___action0, Il2CppObject * ___state1, Task_t1843236107 * ___parent2, CancellationToken_t1851405782  ___cancellationToken3, int32_t ___creationOptions4, int32_t ___internalOptions5, TaskScheduler_t3932792796 * ___scheduler6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m4234257711 (ArgumentOutOfRangeException_t279959794 * __this, String_t* ___paramName0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::ScheduleAndStart(System.Boolean)
extern "C"  void Task_ScheduleAndStart_m2476171743 (Task_t1843236107 * __this, bool ___needsProtection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::AtomicStateUpdate(System.Int32,System.Int32)
extern "C"  bool Task_AtomicStateUpdate_m4068350973 (Task_t1843236107 * __this, int32_t ___newBits0, int32_t ___illegalBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Exchange(System.Int32&,System.Int32)
extern "C"  int32_t Interlocked_Exchange_m4103465028 (Il2CppObject * __this /* static, unused */, int32_t* ___location10, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task/ContingentProperties::SetCompleted()
extern "C"  void ContingentProperties_SetCompleted_m2487364104 (ContingentProperties_t606988207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::FinishStageThree()
extern "C"  void Task_FinishStageThree_m4010028243 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::get_IsWaitNotificationEnabledOrNotRanToCompletion()
extern "C"  bool Task_get_IsWaitNotificationEnabledOrNotRanToCompletion_m2720866512 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::InternalWait(System.Int32,System.Threading.CancellationToken)
extern "C"  bool Task_InternalWait_m945475364 (Task_t1843236107 * __this, int32_t ___millisecondsTimeout0, CancellationToken_t1851405782  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::NotifyDebuggerOfWaitCompletionIfNecessary()
extern "C"  bool Task_NotifyDebuggerOfWaitCompletionIfNecessary_m4037125766 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::get_IsRanToCompletion()
extern "C"  bool Task_get_IsRanToCompletion_m3601607431 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::ThrowIfExceptional(System.Boolean)
extern "C"  void Task_ThrowIfExceptional_m2165180494 (Task_t1843236107 * __this, bool ___includeTaskCanceledExceptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task/ContingentProperties System.Threading.Tasks.Task::EnsureContingentPropertiesInitialized(System.Boolean)
extern "C"  ContingentProperties_t606988207 * Task_EnsureContingentPropertiesInitialized_m3371331877 (Task_t1843236107 * __this, bool ___needsProtection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::AddException(System.Object)
extern "C"  void Task_AddException_m3383991754 (Task_t1843236107 * __this, Il2CppObject * ___exceptionObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::Finish(System.Boolean)
extern "C"  void Task_Finish_m2698939980 (Task_t1843236107 * __this, bool ___bUserDelegateExecuted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::RecordInternalCancellationRequest(System.Threading.CancellationToken,System.Object)
extern "C"  void Task_RecordInternalCancellationRequest_m3918176011 (Task_t1843236107 * __this, CancellationToken_t1851405782  ___tokenToRecord0, Il2CppObject * ___cancellationException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::CancellationCleanupLogic()
extern "C"  void Task_CancellationCleanupLogic_m2712825363 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TResult System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>::get_Result()
#define Task_1_get_Result_m1316373833(__this, method) ((  Task_t1843236107 * (*) (Task_1_t963265114 *, const MethodInfo*))Task_1_get_Result_m2758788574_gshared)(__this, method)
// System.Boolean System.Threading.AtomicBoolean::TryRelaxedSet()
extern "C"  bool AtomicBoolean_TryRelaxedSet_m1519556294 (AtomicBoolean_t379413895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckMultiTaskContinuationOptions(System.Threading.Tasks.TaskContinuationOptions)
extern "C"  void TaskFactory_CheckMultiTaskContinuationOptions_m717392640 (Il2CppObject * __this /* static, unused */, int32_t ___continuationOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckCreationOptions(System.Threading.Tasks.TaskCreationOptions)
extern "C"  void TaskFactory_CheckCreationOptions_m1907973182 (Il2CppObject * __this /* static, unused */, int32_t ___creationOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.IAsyncResult>::Invoke(T)
#define Action_1_Invoke_m2407548955(__this, p0, method) ((  void (*) (Action_1_t1801450390 *, Il2CppObject *, const MethodInfo*))Action_1_Invoke_m1684652980_gshared)(__this, p0, method)
// System.Void System.Threading.Tasks.TaskExceptionHolder::MarkAsHandled(System.Boolean)
extern "C"  void TaskExceptionHolder_MarkAsHandled_m698250467 (TaskExceptionHolder_t2208677448 * __this, bool ___calledFromFinalizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckFromAsyncOptions(System.Threading.Tasks.TaskCreationOptions,System.Boolean)
extern "C"  void TaskFactory_CheckFromAsyncOptions_m238110038 (Il2CppObject * __this /* static, unused */, int32_t ___creationOptions0, bool ___hasBeginMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Delegate::get_Method()
extern "C"  MethodInfo_t * Delegate_get_Method_m2968370506 (Delegate_t3022476291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* ___str00, String_t* ___str11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.AsyncCausalityTracer::TraceOperationCreation(System.Threading.Tasks.CausalityTraceLevel,System.Int32,System.String,System.UInt64)
extern "C" IL2CPP_NO_INLINE void AsyncCausalityTracer_TraceOperationCreation_m3376453187 (Il2CppObject * __this /* static, unused */, int32_t ___traceLevel0, int32_t ___taskId1, String_t* ___operationName2, uint64_t ___relatedContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::AddToActiveTasks(System.Threading.Tasks.Task)
extern "C"  bool Task_AddToActiveTasks_m2680726720 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Versioning.BinaryCompatibility::get_TargetsAtLeast_Desktop_V4_5()
extern "C"  bool BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AtomicBoolean::.ctor()
extern "C"  void AtomicBoolean__ctor_m845874490 (AtomicBoolean_t379413895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AsyncCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCallback__ctor_m3071689932 (AsyncCallback_t163412349 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TResult System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>::Invoke(T1,T2)
#define Func_3_Invoke_m2184768394(__this, p0, p1, method) ((  Il2CppObject * (*) (Func_3_t1604491142 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_3_Invoke_m1656275607_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1<System.Object>::.ctor()
extern "C"  void U3CCombinePredicatesU3Ec__AnonStorey1A_1__ctor_m3424779062_gshared (U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3658276611 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1<System.Object>::<>m__0(TSource)
extern "C"  bool U3CCombinePredicatesU3Ec__AnonStorey1A_1_U3CU3Em__0_m2745270844_gshared (U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3658276611 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Func_2_t3961629604 * L_0 = (Func_2_t3961629604 *)__this->get_predicate1_0();
		Il2CppObject * L_1 = ___x0;
		NullCheck((Func_2_t3961629604 *)L_0);
		bool L_2 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t3961629604 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		Func_2_t3961629604 * L_3 = (Func_2_t3961629604 *)__this->get_predicate2_1();
		Il2CppObject * L_4 = ___x0;
		NullCheck((Func_2_t3961629604 *)L_3);
		bool L_5 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t3961629604 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::.ctor()
extern "C"  void Iterator_1__ctor_m274416062_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Thread_t241561612 * L_0 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Thread_t241561612 *)L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m1995754972((Thread_t241561612 *)L_0, /*hidden argument*/NULL);
		__this->set_threadId_0(L_1);
		return;
	}
}
// TSource System.Linq.Enumerable/Iterator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * Iterator_1_get_Current_m2488383170_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_2();
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose()
extern "C"  void Iterator_1_Dispose_m1990198537_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_Dispose_m1990198537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_current_2(L_0);
		__this->set_state_1((-1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Iterator_1_GetEnumerator_m3758676431_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	Iterator_1_t3157253607 * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_threadId_0();
		Thread_t241561612 * L_1 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Thread_t241561612 *)L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1995754972((Thread_t241561612 *)L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_state_1();
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		__this->set_state_1(1);
		return __this;
	}

IL_0029:
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		Iterator_1_t3157253607 * L_4 = VirtFuncInvoker0< Iterator_1_t3157253607 * >::Invoke(11 /* System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1<System.Object>::Clone() */, (Iterator_1_t3157253607 *)__this);
		V_0 = (Iterator_1_t3157253607 *)L_4;
		Iterator_1_t3157253607 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_state_1(1);
		Iterator_1_t3157253607 * L_6 = V_0;
		return L_6;
	}
}
// System.Object System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Iterator_1_System_Collections_IEnumerator_get_Current_m1152138170_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Iterator_1_System_Collections_IEnumerable_GetEnumerator_m981744771_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Void System.Linq.Enumerable/Iterator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Iterator_1_System_Collections_IEnumerator_Reset_m1017663060_gshared (Iterator_1_t3157253607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Iterator_1_System_Collections_IEnumerator_Reset_m1017663060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
extern "C"  void WhereArrayIterator_1__ctor_m25159286_gshared (WhereArrayIterator_1_t2998102595 * __this, ObjectU5BU5D_t3614634134* ___source0, Func_2_t3961629604 * ___predicate1, const MethodInfo* method)
{
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		((  void (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t3961629604 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::Clone()
extern "C"  Iterator_1_t3157253607 * WhereArrayIterator_1_Clone_m2413235442_gshared (WhereArrayIterator_1_t2998102595 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		WhereArrayIterator_1_t2998102595 * L_2 = (WhereArrayIterator_1_t2998102595 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t2998102595 *, ObjectU5BU5D_t3614634134*, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (ObjectU5BU5D_t3614634134*)L_0, (Func_2_t3961629604 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::MoveNext()
extern "C"  bool WhereArrayIterator_1_MoveNext_m307717878_gshared (WhereArrayIterator_1_t2998102595 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t3157253607 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0064;
		}
	}
	{
		goto IL_004b;
	}

IL_0011:
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_5();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (Il2CppObject *)L_4;
		int32_t L_5 = (int32_t)__this->get_index_5();
		__this->set_index_5(((int32_t)((int32_t)L_5+(int32_t)1)));
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Il2CppObject * L_7 = V_0;
		NullCheck((Func_2_t3961629604 *)L_6);
		bool L_8 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t3961629604 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		Il2CppObject * L_9 = V_0;
		((Iterator_1_t3157253607 *)__this)->set_current_2(L_9);
		return (bool)1;
	}

IL_004b:
	{
		int32_t L_10 = (int32_t)__this->get_index_5();
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)__this->get_source_3();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t3157253607 *)__this);
	}

IL_0064:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject* WhereArrayIterator_1_Where_m1975930481_gshared (WhereArrayIterator_1_t2998102595 * __this, Func_2_t3961629604 * ___predicate0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Func_2_t3961629604 * L_2 = ___predicate0;
		Func_2_t3961629604 * L_3 = ((  Func_2_t3961629604 * (*) (Il2CppObject * /* static, unused */, Func_2_t3961629604 *, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Func_2_t3961629604 *)L_1, (Func_2_t3961629604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		WhereArrayIterator_1_t2998102595 * L_4 = (WhereArrayIterator_1_t2998102595 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t2998102595 *, ObjectU5BU5D_t3614634134*, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_4, (ObjectU5BU5D_t3614634134*)L_0, (Func_2_t3961629604 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  void WhereEnumerableIterator_1__ctor_m2792192076_gshared (WhereEnumerableIterator_1_t3208243354 * __this, Il2CppObject* ___source0, Func_2_t3961629604 * ___predicate1, const MethodInfo* method)
{
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		((  void (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t3961629604 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Clone()
extern "C"  Iterator_1_t3157253607 * WhereEnumerableIterator_1_Clone_m796931259_gshared (WhereEnumerableIterator_1_t3208243354 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		WhereEnumerableIterator_1_t3208243354 * L_2 = (WhereEnumerableIterator_1_t3208243354 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t3208243354 *, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject*)L_0, (Func_2_t3961629604 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Dispose()
extern "C"  void WhereEnumerableIterator_1_Dispose_m2421906470_gshared (WhereEnumerableIterator_1_t3208243354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereEnumerableIterator_1_Dispose_m2421906470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_enumerator_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_enumerator_5();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_enumerator_5((Il2CppObject*)NULL);
		NullCheck((Iterator_1_t3157253607 *)__this);
		((  void (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::MoveNext()
extern "C"  bool WhereEnumerableIterator_1_MoveNext_m2092954763_gshared (WhereEnumerableIterator_1_t3208243354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereEnumerableIterator_1_MoveNext_m2092954763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t3157253607 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0037;
		}
	}
	{
		goto IL_007d;
	}

IL_001a:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_3();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_3);
		__this->set_enumerator_5(L_4);
		((Iterator_1_t3157253607 *)__this)->set_state_1(2);
		goto IL_0037;
	}

IL_0037:
	{
		goto IL_0062;
	}

IL_003c:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_enumerator_5();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_5);
		V_1 = (Il2CppObject *)L_6;
		Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Il2CppObject * L_8 = V_1;
		NullCheck((Func_2_t3961629604 *)L_7);
		bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		Il2CppObject * L_10 = V_1;
		((Iterator_1_t3157253607 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_0062:
	{
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_enumerator_5();
		NullCheck((Il2CppObject *)L_11);
		bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		if (L_12)
		{
			goto IL_003c;
		}
	}
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t3157253607 *)__this);
		goto IL_007d;
	}

IL_007d:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject* WhereEnumerableIterator_1_Where_m2204257518_gshared (WhereEnumerableIterator_1_t3208243354 * __this, Func_2_t3961629604 * ___predicate0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Func_2_t3961629604 * L_2 = ___predicate0;
		Func_2_t3961629604 * L_3 = ((  Func_2_t3961629604 * (*) (Il2CppObject * /* static, unused */, Func_2_t3961629604 *, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Func_2_t3961629604 *)L_1, (Func_2_t3961629604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		WhereEnumerableIterator_1_t3208243354 * L_4 = (WhereEnumerableIterator_1_t3208243354 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t3208243354 *, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_4, (Il2CppObject*)L_0, (Func_2_t3961629604 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Linq.Enumerable/WhereListIterator`1<System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  void WhereListIterator_1__ctor_m2764151393_gshared (WhereListIterator_1_t1069043000 * __this, List_1_t2058570427 * ___source0, Func_2_t3961629604 * ___predicate1, const MethodInfo* method)
{
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		((  void (*) (Iterator_1_t3157253607 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Iterator_1_t3157253607 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		List_1_t2058570427 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t3961629604 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1<System.Object>::Clone()
extern "C"  Iterator_1_t3157253607 * WhereListIterator_1_Clone_m44700347_gshared (WhereListIterator_1_t1069043000 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		WhereListIterator_1_t1069043000 * L_2 = (WhereListIterator_1_t1069043000 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t1069043000 *, List_1_t2058570427 *, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (List_1_t2058570427 *)L_0, (Func_2_t3961629604 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereListIterator`1<System.Object>::MoveNext()
extern "C"  bool WhereListIterator_1_MoveNext_m2627973827_gshared (WhereListIterator_1_t1069043000 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t3157253607 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0037;
		}
	}
	{
		goto IL_007d;
	}

IL_001a:
	{
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)__this->get_source_3();
		NullCheck((List_1_t2058570427 *)L_3);
		Enumerator_t1593300101  L_4 = ((  Enumerator_t1593300101  (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_enumerator_5(L_4);
		((Iterator_1_t3157253607 *)__this)->set_state_1(2);
		goto IL_0037;
	}

IL_0037:
	{
		goto IL_0062;
	}

IL_003c:
	{
		Enumerator_t1593300101 * L_5 = (Enumerator_t1593300101 *)__this->get_address_of_enumerator_5();
		Il2CppObject * L_6 = Enumerator_get_Current_m3108634708((Enumerator_t1593300101 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_1 = (Il2CppObject *)L_6;
		Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Il2CppObject * L_8 = V_1;
		NullCheck((Func_2_t3961629604 *)L_7);
		bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		Il2CppObject * L_10 = V_1;
		((Iterator_1_t3157253607 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_0062:
	{
		Enumerator_t1593300101 * L_11 = (Enumerator_t1593300101 *)__this->get_address_of_enumerator_5();
		bool L_12 = Enumerator_MoveNext_m44995089((Enumerator_t1593300101 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (L_12)
		{
			goto IL_003c;
		}
	}
	{
		NullCheck((Iterator_1_t3157253607 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t3157253607 *)__this);
		goto IL_007d;
	}

IL_007d:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject* WhereListIterator_1_Where_m683512768_gshared (WhereListIterator_1_t1069043000 * __this, Func_2_t3961629604 * ___predicate0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_source_3();
		Func_2_t3961629604 * L_1 = (Func_2_t3961629604 *)__this->get_predicate_4();
		Func_2_t3961629604 * L_2 = ___predicate0;
		Func_2_t3961629604 * L_3 = ((  Func_2_t3961629604 * (*) (Il2CppObject * /* static, unused */, Func_2_t3961629604 *, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Func_2_t3961629604 *)L_1, (Func_2_t3961629604 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		WhereListIterator_1_t1069043000 * L_4 = (WhereListIterator_1_t1069043000 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t1069043000 *, List_1_t2058570427 *, Func_2_t3961629604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_4, (List_1_t2058570427 *)L_0, (Func_2_t3961629604 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}
}
// System.Void System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::.ctor(T)
extern "C"  void Nullable_1__ctor_m235056776_gshared (Nullable_1_t3179568147 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m235056776_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m235056776(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1085446683_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1085446683_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1085446683(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3600185652_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3600185652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m3600185652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m3600185652(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1226087740_gshared (Nullable_1_t3179568147 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1226087740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3179568147 ));
		UnBoxNullable(L_3, MonoSslPolicyErrors_t621534536_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m2263234329((Nullable_1_t3179568147 *)__this, (Nullable_1_t3179568147 )((*(Nullable_1_t3179568147 *)((Nullable_1_t3179568147 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1226087740_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1226087740(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2263234329_gshared (Nullable_1_t3179568147 * __this, Nullable_1_t3179568147  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m2263234329_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3179568147  ___other0, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2263234329(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2766960772_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((Il2CppObject *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2766960772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2766960772(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Mono.Security.Interface.MonoSslPolicyErrors>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3150199456_gshared (Nullable_1_t3179568147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3150199456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((Il2CppObject *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_2);
		return L_3;
	}

IL_001d:
	{
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3150199456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3179568147  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3150199456(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Mono.Security.Interface.TlsProtocols>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1255861601_gshared (Nullable_1_t214512479 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1255861601_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1255861601(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2390520973_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2390520973_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2390520973(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Mono.Security.Interface.TlsProtocols>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m840862642_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m840862642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m840862642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m840862642(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m686690116_gshared (Nullable_1_t214512479 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m686690116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t214512479 ));
		UnBoxNullable(L_3, TlsProtocols_t1951446164_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m3528580847((Nullable_1_t214512479 *)__this, (Nullable_1_t214512479 )((*(Nullable_1_t214512479 *)((Nullable_1_t214512479 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m686690116_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m686690116(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Mono.Security.Interface.TlsProtocols>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3528580847_gshared (Nullable_1_t214512479 * __this, Nullable_1_t214512479  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((Il2CppObject *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3528580847_AdjustorThunk (Il2CppObject * __this, Nullable_1_t214512479  ___other0, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3528580847(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Mono.Security.Interface.TlsProtocols>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m133456640_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((Il2CppObject *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m133456640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m133456640(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Mono.Security.Interface.TlsProtocols>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2516240674_gshared (Nullable_1_t214512479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2516240674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((Il2CppObject *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_2);
		return L_3;
	}

IL_001d:
	{
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2516240674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t214512479  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2516240674(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1785320616_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		bool L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1785320616_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1785320616(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1428381008_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1428381008_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1428381008(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C"  bool Nullable_1_get_Value_m1158440123_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1158440123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_value_0();
		return L_2;
	}
}
extern "C"  bool Nullable_1_get_Value_m1158440123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_Value_m1158440123(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1318699267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2088641033 ));
		UnBoxNullable(L_3, Boolean_t3825574718_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m2189684888((Nullable_1_t2088641033 *)__this, (Nullable_1_t2088641033 )((*(Nullable_1_t2088641033 *)((Nullable_1_t2088641033 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1318699267_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1318699267(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		bool* L_3 = (bool*)(&___other0)->get_address_of_value_0();
		bool L_4 = (bool)__this->get_value_0();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m2118901528((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2189684888_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2189684888(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		int32_t L_2 = Boolean_GetHashCode_m1894638460((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1645245653(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m678068069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		String_t* L_2 = Boolean_ToString_m1253164328((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m678068069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m678068069(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3532149783_gshared (Nullable_1_t3251239280 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTime_t693205669  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3532149783_AdjustorThunk (Il2CppObject * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3532149783(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4025045115_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4025045115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4025045115(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::get_Value()
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m1118025076_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1118025076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTime_t693205669  L_2 = (DateTime_t693205669 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m1118025076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_get_Value_m1118025076(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1089953100_gshared (Nullable_1_t3251239280 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1089953100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3251239280 ));
		UnBoxNullable(L_3, DateTime_t693205669_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m1817623273((Nullable_1_t3251239280 *)__this, (Nullable_1_t3251239280 )((*(Nullable_1_t3251239280 *)((Nullable_1_t3251239280 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1089953100_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1089953100(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1817623273_gshared (Nullable_1_t3251239280 * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTime_t693205669 * L_3 = (DateTime_t693205669 *)(&___other0)->get_address_of_value_0();
		DateTime_t693205669  L_4 = (DateTime_t693205669 )__this->get_value_0();
		DateTime_t693205669  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m2562884703((DateTime_t693205669 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1817623273_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1817623273(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTime_GetHashCode_m974799321((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3047479588(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTime>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1419821888_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1419821888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTime_ToString_m1117481977((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1419821888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1419821888(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1825326012_gshared (Nullable_1_t334943763 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1825326012_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1825326012(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1182428964_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1182428964_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1182428964(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2201992629_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2201992629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m2201992629_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m2201992629(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2848647165_gshared (Nullable_1_t334943763 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m2848647165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t334943763 ));
		UnBoxNullable(L_3, Int32_t2071877448_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m1118562548((Nullable_1_t334943763 *)__this, (Nullable_1_t334943763 )((*(Nullable_1_t334943763 *)((Nullable_1_t334943763 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2848647165_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2848647165(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1118562548_gshared (Nullable_1_t334943763 * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m753832628((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1118562548_AdjustorThunk (Il2CppObject * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1118562548(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int32_GetHashCode_m1381647448((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1859855859(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int32>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2285560203_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2285560203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int32_ToString_m2960866144((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2285560203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2285560203(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m3860982732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, TimeSpan_t3430258949_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3379886338_gshared (Predicate_1_t2268544833 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Boolean>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3492771294_gshared (Predicate_1_t2268544833 * __this, bool ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1973834093_gshared (Predicate_1_t2268544833 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1973834093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m43128384_gshared (Predicate_1_t2268544833 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m461821738_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2886027714_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m767759683_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m767759683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2487624220_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3470414727_gshared (Predicate_1_t3431143080 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.DateTime>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m990427423_gshared (Predicate_1_t3431143080 * __this, DateTime_t693205669  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2874596364_gshared (Predicate_1_t3431143080 * __this, DateTime_t693205669  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2874596364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2956280229_gshared (Predicate_1_t3431143080 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.DateTimeOffset>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4014280626_gshared (Predicate_1_t4100926317 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.DateTimeOffset>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2103982598_gshared (Predicate_1_t4100926317 * __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.DateTimeOffset>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3523490745_gshared (Predicate_1_t4100926317 * __this, DateTimeOffset_t1362988906  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3523490745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTimeOffset_t1362988906_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.DateTimeOffset>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3351206924_gshared (Predicate_1_t4100926317 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Decimal>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m128958593_gshared (Predicate_1_t3462638488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Decimal>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1560621461_gshared (Predicate_1_t3462638488 * __this, Decimal_t724701077  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, Decimal_t724701077  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Decimal>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3982629388_gshared (Predicate_1_t3462638488 * __this, Decimal_t724701077  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3982629388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Decimal>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3331768491_gshared (Predicate_1_t3462638488 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1270135435_gshared (Predicate_1_t2520985796 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Double>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m878796563_gshared (Predicate_1_t2520985796 * __this, double ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, double ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, double ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, double ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, double ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2128514704_gshared (Predicate_1_t2520985796 * __this, double ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2128514704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2464215417_gshared (Predicate_1_t2520985796 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2170364528_gshared (Predicate_1_t2484216029 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int16>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3072524500_gshared (Predicate_1_t2484216029 * __this, int16_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int16_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, int16_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int16_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, int16_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Int16>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1777010513_gshared (Predicate_1_t2484216029 * __this, int16_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1777010513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int16>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2234368510_gshared (Predicate_1_t2484216029 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3174496963_gshared (Predicate_1_t3647015448 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int64>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3434591315_gshared (Predicate_1_t3647015448 * __this, int64_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, int64_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, int64_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2896842008_gshared (Predicate_1_t3647015448 * __this, int64_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2896842008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1296795441_gshared (Predicate_1_t3647015448 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2289454599_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.SByte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1815369027_gshared (Predicate_1_t3192354960 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.SByte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4259007563_gshared (Predicate_1_t3192354960 * __this, int8_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int8_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, int8_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int8_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, int8_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.SByte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3863950084_gshared (Predicate_1_t3192354960 * __this, int8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3863950084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SByte_t454417549_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.SByte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3562306061_gshared (Predicate_1_t3192354960 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3886687440_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Single>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2581621416_gshared (Predicate_1_t519480047 * __this, float ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3791856215_gshared (Predicate_1_t519480047 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3791856215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3081660750_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Text.RegularExpressions.RegexOptions>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3590135001_gshared (Predicate_1_t861229842 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Text.RegularExpressions.RegexOptions>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m155252749_gshared (Predicate_1_t861229842 * __this, int32_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Text.RegularExpressions.RegexOptions>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2847696724_gshared (Predicate_1_t861229842 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2847696724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RegexOptions_t2418259727_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Text.RegularExpressions.RegexOptions>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2126732967_gshared (Predicate_1_t861229842 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.TimeSpan>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3336667639_gshared (Predicate_1_t1873229064 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.TimeSpan>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m104110639_gshared (Predicate_1_t1873229064 * __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.TimeSpan>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2757499760_gshared (Predicate_1_t1873229064 * __this, TimeSpan_t3430258949  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2757499760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TimeSpan_t3430258949_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.TimeSpan>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3211340977_gshared (Predicate_1_t1873229064 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2635057443_gshared (Predicate_1_t3724820022 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt16>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1330548883_gshared (Predicate_1_t3724820022 * __this, uint16_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, uint16_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, uint16_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt16>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m946193182_gshared (Predicate_1_t3724820022 * __this, uint16_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m946193182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m611568309_gshared (Predicate_1_t3724820022 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3732669481_gshared (Predicate_1_t592652136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m539473317_gshared (Predicate_1_t592652136 * __this, uint32_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, uint32_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, uint32_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m739040720_gshared (Predicate_1_t592652136 * __this, uint32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m739040720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3585710307_gshared (Predicate_1_t592652136 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4171522490_gshared (Predicate_1_t1352167029 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt64>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1858076446_gshared (Predicate_1_t1352167029 * __this, uint64_t ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, uint64_t ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, uint64_t ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3457072769_gshared (Predicate_1_t1352167029 * __this, uint64_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3457072769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m58933604_gshared (Predicate_1_t1352167029 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Xml.Schema.RangePositionInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2309810198_gshared (Predicate_1_t1223773037 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Xml.Schema.RangePositionInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m387811538_gshared (Predicate_1_t1223773037 * __this, RangePositionInfo_t2780802922  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RangePositionInfo_t2780802922  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, RangePositionInfo_t2780802922  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RangePositionInfo_t2780802922  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, RangePositionInfo_t2780802922  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Xml.Schema.RangePositionInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m930472829_gshared (Predicate_1_t1223773037 * __this, RangePositionInfo_t2780802922  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m930472829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RangePositionInfo_t2780802922_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Xml.Schema.RangePositionInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3807313552_gshared (Predicate_1_t1223773037 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m467479487_gshared (Predicate_1_t953556625 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1985205951_gshared (Predicate_1_t953556625 * __this, XmlSchemaObjectEntry_t2510586510  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, XmlSchemaObjectEntry_t2510586510  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, XmlSchemaObjectEntry_t2510586510  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, XmlSchemaObjectEntry_t2510586510  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, XmlSchemaObjectEntry_t2510586510  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1405349914_gshared (Predicate_1_t953556625 * __this, XmlSchemaObjectEntry_t2510586510  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1405349914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(XmlSchemaObjectEntry_t2510586510_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1294000769_gshared (Predicate_1_t953556625 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m541404361_gshared (Predicate_1_t2348721464 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m744913181_gshared (Predicate_1_t2348721464 * __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool result = false;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef bool (*FunctionPointerType) (void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef bool (*FunctionPointerType) (void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.AnimatorClipInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2336395304_gshared (Predicate_1_t2348721464 * __this, AnimatorClipInfo_t3905751349  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2336395304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1604508263_gshared (Predicate_1_t2348721464 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m653998582_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	Il2CppObject * result = NULL;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),____this0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),____this0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(____this0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m2080015031_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m977999903_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m1290492285_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m1348877692_gshared (StaticGetter_1_t1095697167 * __this, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	Il2CppObject * result = NULL;
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if ((currentDelegate->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()))) && ___methodIsStatic)
			{
				typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
				result = ((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
		return result;
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
		{
			typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
			return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m2732579814_gshared (StaticGetter_1_t1095697167 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m44757160_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::Create()
extern "C"  AsyncTaskMethodBuilder_1_t2408546353  AsyncTaskMethodBuilder_1_Create_m3160705057_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_Create_m3160705057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AsyncTaskMethodBuilder_1_t2408546353  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (AsyncTaskMethodBuilder_1_t2408546353_il2cpp_TypeInfo_var, (&V_0));
		AsyncTaskMethodBuilder_1_t2408546353  L_0 = V_0;
		return L_0;
	}
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method)
{
	{
		AsyncMethodBuilderCore_t2485284745 * L_0 = (AsyncMethodBuilderCore_t2485284745 *)__this->get_address_of_m_coreState_1();
		Il2CppObject * L_1 = ___stateMachine0;
		AsyncMethodBuilderCore_SetStateMachine_m1089807594((AsyncMethodBuilderCore_t2485284745 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t2408546353 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t2408546353 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetStateMachine_m2826209434(_thisAdjusted, ___stateMachine0, method);
}
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::get_Task()
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_get_Task_m3423503422_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, const MethodInfo* method)
{
	Task_1_t2945603725 * V_0 = NULL;
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_2();
		V_0 = (Task_1_t2945603725 *)L_0;
		Task_1_t2945603725 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Task_1_t2945603725 * L_2 = (Task_1_t2945603725 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Task_1_t2945603725 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Task_1_t2945603725 * L_3 = (Task_1_t2945603725 *)L_2;
		V_0 = (Task_1_t2945603725 *)L_3;
		__this->set_m_task_2(L_3);
	}

IL_001a:
	{
		Task_1_t2945603725 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_get_Task_m3423503422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t2408546353 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t2408546353 *>(__this + 1);
	return AsyncTaskMethodBuilder_1_get_Task_m3423503422(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetResult(TResult)
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m3861164633_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_SetResult_m3861164633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t2945603725 * V_0 = NULL;
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_2();
		V_0 = (Task_1_t2945603725 *)L_0;
		Task_1_t2945603725 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = ___result0;
		Task_1_t2945603725 * L_3 = AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084((AsyncTaskMethodBuilder_1_t2408546353 *)__this, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_m_task_2(L_3);
		goto IL_0067;
	}

IL_001f:
	{
		bool L_4 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Task_1_t2945603725 * L_5 = V_0;
		NullCheck((Task_t1843236107 *)L_5);
		int32_t L_6 = Task_get_Id_m4106115082((Task_t1843236107 *)L_5, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_7 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		Task_1_t2945603725 * L_8 = V_0;
		NullCheck((Task_t1843236107 *)L_8);
		int32_t L_9 = Task_get_Id_m4106115082((Task_t1843236107 *)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_9, /*hidden argument*/NULL);
	}

IL_004b:
	{
		Task_1_t2945603725 * L_10 = V_0;
		bool L_11 = ___result0;
		NullCheck((Task_1_t2945603725 *)L_10);
		bool L_12 = ((  bool (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t2945603725 *)L_10, (bool)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (L_12)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_13 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral3568671706, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_14 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_14, (String_t*)L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0067:
	{
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m3861164633_AdjustorThunk (Il2CppObject * __this, bool ___result0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t2408546353 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t2408546353 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetResult_m3861164633(_thisAdjusted, ___result0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::SetException(System.Exception)
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m3130084070_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_SetException_m3130084070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t2945603725 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	bool V_2 = false;
	bool G_B7_0 = false;
	{
		Exception_t1927440687 * L_0 = ___exception0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral2453856579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Task_1_t2945603725 * L_2 = (Task_1_t2945603725 *)__this->get_m_task_2();
		V_0 = (Task_1_t2945603725 *)L_2;
		Task_1_t2945603725 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Task_1_t2945603725 * L_4 = AsyncTaskMethodBuilder_1_get_Task_m3423503422((AsyncTaskMethodBuilder_1_t2408546353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Task_1_t2945603725 *)L_4;
	}

IL_0025:
	{
		Exception_t1927440687 * L_5 = ___exception0;
		V_1 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)IsInst(L_5, OperationCanceledException_t2897400967_il2cpp_TypeInfo_var));
		OperationCanceledException_t2897400967 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		Task_1_t2945603725 * L_7 = V_0;
		OperationCanceledException_t2897400967 * L_8 = V_1;
		NullCheck((OperationCanceledException_t2897400967 *)L_8);
		CancellationToken_t1851405782  L_9 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_8, /*hidden argument*/NULL);
		OperationCanceledException_t2897400967 * L_10 = V_1;
		NullCheck((Task_1_t2945603725 *)L_7);
		bool L_11 = ((  bool (*) (Task_1_t2945603725 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t2945603725 *)L_7, (CancellationToken_t1851405782 )L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B7_0 = L_11;
		goto IL_004b;
	}

IL_0044:
	{
		Task_1_t2945603725 * L_12 = V_0;
		Exception_t1927440687 * L_13 = ___exception0;
		NullCheck((Task_1_t2945603725 *)L_12);
		bool L_14 = ((  bool (*) (Task_1_t2945603725 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((Task_1_t2945603725 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		G_B7_0 = L_14;
	}

IL_004b:
	{
		V_2 = (bool)G_B7_0;
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_16 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral3568671706, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_17 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_17, (String_t*)L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0062:
	{
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m3130084070_AdjustorThunk (Il2CppObject * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t2408546353 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t2408546353 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetException_m3130084070(_thisAdjusted, ___exception0, method);
}
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::GetTaskForResult(TResult)
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_gshared (AsyncTaskMethodBuilder_1_t2408546353 * __this, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Task_1_t2945603725 * V_2 = NULL;
	int32_t V_3 = 0;
	Task_1_t1191906455 * V_4 = NULL;
	IntPtr_t V_5;
	memset(&V_5, 0, sizeof(V_5));
	UIntPtr_t  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Task_1_t2945603725 * G_B5_0 = NULL;
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Boolean_t3825574718_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		bool L_4 = ___result0;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_5);
		V_1 = (bool)((*(bool*)((bool*)UnBox(L_6, Boolean_t3825574718_il2cpp_TypeInfo_var))));
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t2945603725 * L_8 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_TrueTask_0();
		G_B5_0 = L_8;
		goto IL_0052;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t2945603725 * L_9 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_FalseTask_1();
		G_B5_0 = L_9;
	}

IL_0052:
	{
		V_2 = (Task_1_t2945603725 *)G_B5_0;
		Task_1_t2945603725 * L_10 = V_2;
		Task_1_t2945603725 * L_11 = ((  Task_1_t2945603725 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_11;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		bool L_14 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_12, (Type_t *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00ab;
		}
	}
	{
		bool L_15 = ___result0;
		bool L_16 = L_15;
		Il2CppObject * L_17 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_16);
		V_3 = (int32_t)((*(int32_t*)((int32_t*)UnBox(L_17, Int32_t2071877448_il2cpp_TypeInfo_var))));
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) >= ((int32_t)((int32_t)9))))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_19 = V_3;
		if ((((int32_t)L_19) < ((int32_t)(-1))))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1U5BU5D_t2481017646* L_20 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_Int32Tasks_2();
		int32_t L_21 = V_3;
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)L_21-(int32_t)(-1)));
		Task_1_t1191906455 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_4 = (Task_1_t1191906455 *)L_23;
		Task_1_t1191906455 * L_24 = V_4;
		Task_1_t2945603725 * L_25 = ((  Task_1_t2945603725 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_25;
	}

IL_00a6:
	{
		goto IL_02d9;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt32_t2149682021_0_0_0_var), /*hidden argument*/NULL);
		bool L_28 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_26, (Type_t *)L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00d9;
		}
	}
	{
		bool L_29 = ___result0;
		bool L_30 = L_29;
		Il2CppObject * L_31 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_30);
		if (!((*(uint32_t*)((uint32_t*)UnBox(L_31, UInt32_t2149682021_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_34 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_32, (Type_t *)L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0107;
		}
	}
	{
		bool L_35 = ___result0;
		bool L_36 = L_35;
		Il2CppObject * L_37 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_36);
		if (!((*(uint8_t*)((uint8_t*)UnBox(L_37, Byte_t3683104436_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0107:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByte_t454417549_0_0_0_var), /*hidden argument*/NULL);
		bool L_40 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_38, (Type_t *)L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0136;
		}
	}
	{
		bool L_41 = ___result0;
		bool L_42 = L_41;
		Il2CppObject * L_43 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_42);
		if (!(((int32_t)((int32_t)((*(int8_t*)((int8_t*)UnBox(L_43, SByte_t454417549_il2cpp_TypeInfo_var))))))))
		{
			goto IL_02d3;
		}
	}

IL_0136:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Char_t3454481338_0_0_0_var), /*hidden argument*/NULL);
		bool L_46 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_44, (Type_t *)L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0164;
		}
	}
	{
		bool L_47 = ___result0;
		bool L_48 = L_47;
		Il2CppObject * L_49 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_48);
		if (!((*(Il2CppChar*)((Il2CppChar*)UnBox(L_49, Char_t3454481338_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0164:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_50 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_51 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Decimal_t724701077_0_0_0_var), /*hidden argument*/NULL);
		bool L_52 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_50, (Type_t *)L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_019d;
		}
	}
	{
		Decimal_t724701077  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Decimal__ctor_m1010012873(&L_53, (int32_t)0, /*hidden argument*/NULL);
		bool L_54 = ___result0;
		bool L_55 = L_54;
		Il2CppObject * L_56 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_55);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_57 = Decimal_op_Equality_m2278618154(NULL /*static, unused*/, (Decimal_t724701077 )L_53, (Decimal_t724701077 )((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox(L_56, Decimal_t724701077_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_02d3;
		}
	}

IL_019d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_58 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_59 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		bool L_60 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_58, (Type_t *)L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_01cd;
		}
	}
	{
		bool L_61 = ___result0;
		bool L_62 = L_61;
		Il2CppObject * L_63 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_62);
		if ((((int64_t)((*(int64_t*)((int64_t*)UnBox(L_63, Int64_t909078037_il2cpp_TypeInfo_var))))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02d3;
		}
	}

IL_01cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_65 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt64_t2909196914_0_0_0_var), /*hidden argument*/NULL);
		bool L_66 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_64, (Type_t *)L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01fd;
		}
	}
	{
		bool L_67 = ___result0;
		bool L_68 = L_67;
		Il2CppObject * L_69 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_68);
		if ((((int64_t)((*(uint64_t*)((uint64_t*)UnBox(L_69, UInt64_t2909196914_il2cpp_TypeInfo_var))))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02d3;
		}
	}

IL_01fd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_70 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_71 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int16_t4041245914_0_0_0_var), /*hidden argument*/NULL);
		bool L_72 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_70, (Type_t *)L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_022b;
		}
	}
	{
		bool L_73 = ___result0;
		bool L_74 = L_73;
		Il2CppObject * L_75 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_74);
		if (!((*(int16_t*)((int16_t*)UnBox(L_75, Int16_t4041245914_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_022b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_76 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_77 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt16_t986882611_0_0_0_var), /*hidden argument*/NULL);
		bool L_78 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_76, (Type_t *)L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_0259;
		}
	}
	{
		bool L_79 = ___result0;
		bool L_80 = L_79;
		Il2CppObject * L_81 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_80);
		if (!((*(uint16_t*)((uint16_t*)UnBox(L_81, UInt16_t986882611_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0259:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_82 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_83 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IntPtr_t_0_0_0_var), /*hidden argument*/NULL);
		bool L_84 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_82, (Type_t *)L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0296;
		}
	}
	{
		Initobj (IntPtr_t_il2cpp_TypeInfo_var, (&V_5));
		IntPtr_t L_85 = V_5;
		bool L_86 = ___result0;
		bool L_87 = L_86;
		Il2CppObject * L_88 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_87);
		bool L_89 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, (IntPtr_t)L_85, (IntPtr_t)((*(IntPtr_t*)((IntPtr_t*)UnBox(L_88, IntPtr_t_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (L_89)
		{
			goto IL_02d3;
		}
	}

IL_0296:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_90 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_91 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UIntPtr_t_0_0_0_var), /*hidden argument*/NULL);
		bool L_92 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_90, (Type_t *)L_91, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_02d9;
		}
	}
	{
		Initobj (UIntPtr_t_il2cpp_TypeInfo_var, (&V_6));
		UIntPtr_t  L_93 = V_6;
		bool L_94 = ___result0;
		bool L_95 = L_94;
		Il2CppObject * L_96 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_95);
		IL2CPP_RUNTIME_CLASS_INIT(UIntPtr_t_il2cpp_TypeInfo_var);
		bool L_97 = UIntPtr_op_Equality_m1435732519(NULL /*static, unused*/, (UIntPtr_t )L_93, (UIntPtr_t )((*(UIntPtr_t *)((UIntPtr_t *)UnBox(L_96, UIntPtr_t_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_02d9;
		}
	}

IL_02d3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Task_1_t2945603725 * L_98 = ((AsyncTaskMethodBuilder_1_t2408546353_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->get_s_defaultResultTask_0();
		return L_98;
	}

IL_02d9:
	{
		goto IL_02ef;
	}

IL_02de:
	{
		goto IL_02ef;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Task_1_t2945603725 * L_100 = ((AsyncTaskMethodBuilder_1_t2408546353_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->get_s_defaultResultTask_0();
		return L_100;
	}

IL_02ef:
	{
		bool L_101 = ___result0;
		Task_1_t2945603725 * L_102 = (Task_1_t2945603725 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_102, (bool)L_101, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_102;
	}
}
extern "C"  Task_1_t2945603725 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084_AdjustorThunk (Il2CppObject * __this, bool ___result0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t2408546353 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t2408546353 *>(__this + 1);
	return AsyncTaskMethodBuilder_1_GetTaskForResult_m1470153084(_thisAdjusted, ___result0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean>::.cctor()
extern "C"  void AsyncTaskMethodBuilder_1__cctor_m3426799844_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1__cctor_m3426799844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t2945603725 * L_1 = ((  Task_1_t2945603725 * (*) (Il2CppObject * /* static, unused */, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((AsyncTaskMethodBuilder_1_t2408546353_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->set_s_defaultResultTask_0(L_1);
		return;
	}
}
// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::Create()
extern "C"  AsyncTaskMethodBuilder_1_t1272420930  AsyncTaskMethodBuilder_1_Create_m3623012989_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_Create_m3623012989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AsyncTaskMethodBuilder_1_t1272420930  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (AsyncTaskMethodBuilder_1_t1272420930_il2cpp_TypeInfo_var, (&V_0));
		AsyncTaskMethodBuilder_1_t1272420930  L_0 = V_0;
		return L_0;
	}
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method)
{
	{
		AsyncMethodBuilderCore_t2485284745 * L_0 = (AsyncMethodBuilderCore_t2485284745 *)__this->get_address_of_m_coreState_1();
		Il2CppObject * L_1 = ___stateMachine0;
		AsyncMethodBuilderCore_SetStateMachine_m1089807594((AsyncMethodBuilderCore_t2485284745 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___stateMachine0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t1272420930 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t1272420930 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetStateMachine_m2809413762(_thisAdjusted, ___stateMachine0, method);
}
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::get_Task()
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_get_Task_m2705772806_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, const MethodInfo* method)
{
	Task_1_t1809478302 * V_0 = NULL;
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_2();
		V_0 = (Task_1_t1809478302 *)L_0;
		Task_1_t1809478302 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Task_1_t1809478302 * L_2 = (Task_1_t1809478302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Task_1_t1809478302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Task_1_t1809478302 * L_3 = (Task_1_t1809478302 *)L_2;
		V_0 = (Task_1_t1809478302 *)L_3;
		__this->set_m_task_2(L_3);
	}

IL_001a:
	{
		Task_1_t1809478302 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_get_Task_m2705772806_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t1272420930 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t1272420930 *>(__this + 1);
	return AsyncTaskMethodBuilder_1_get_Task_m2705772806(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetResult(TResult)
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m4050605073_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_SetResult_m4050605073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t1809478302 * V_0 = NULL;
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_2();
		V_0 = (Task_1_t1809478302 *)L_0;
		Task_1_t1809478302 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_2 = ___result0;
		Task_1_t1809478302 * L_3 = AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425((AsyncTaskMethodBuilder_1_t1272420930 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_m_task_2(L_3);
		goto IL_0067;
	}

IL_001f:
	{
		bool L_4 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Task_1_t1809478302 * L_5 = V_0;
		NullCheck((Task_t1843236107 *)L_5);
		int32_t L_6 = Task_get_Id_m4106115082((Task_t1843236107 *)L_5, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_7 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		Task_1_t1809478302 * L_8 = V_0;
		NullCheck((Task_t1843236107 *)L_8);
		int32_t L_9 = Task_get_Id_m4106115082((Task_t1843236107 *)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_9, /*hidden argument*/NULL);
	}

IL_004b:
	{
		Task_1_t1809478302 * L_10 = V_0;
		Il2CppObject * L_11 = ___result0;
		NullCheck((Task_1_t1809478302 *)L_10);
		bool L_12 = ((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t1809478302 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (L_12)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_13 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral3568671706, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_14 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_14, (String_t*)L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0067:
	{
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetResult_m4050605073_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t1272420930 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t1272420930 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetResult_m4050605073(_thisAdjusted, ___result0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::SetException(System.Exception)
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m4054625890_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_SetException_m4054625890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t1809478302 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	bool V_2 = false;
	bool G_B7_0 = false;
	{
		Exception_t1927440687 * L_0 = ___exception0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral2453856579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Task_1_t1809478302 * L_2 = (Task_1_t1809478302 *)__this->get_m_task_2();
		V_0 = (Task_1_t1809478302 *)L_2;
		Task_1_t1809478302 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Task_1_t1809478302 * L_4 = AsyncTaskMethodBuilder_1_get_Task_m2705772806((AsyncTaskMethodBuilder_1_t1272420930 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Task_1_t1809478302 *)L_4;
	}

IL_0025:
	{
		Exception_t1927440687 * L_5 = ___exception0;
		V_1 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)IsInst(L_5, OperationCanceledException_t2897400967_il2cpp_TypeInfo_var));
		OperationCanceledException_t2897400967 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		Task_1_t1809478302 * L_7 = V_0;
		OperationCanceledException_t2897400967 * L_8 = V_1;
		NullCheck((OperationCanceledException_t2897400967 *)L_8);
		CancellationToken_t1851405782  L_9 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_8, /*hidden argument*/NULL);
		OperationCanceledException_t2897400967 * L_10 = V_1;
		NullCheck((Task_1_t1809478302 *)L_7);
		bool L_11 = ((  bool (*) (Task_1_t1809478302 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t1809478302 *)L_7, (CancellationToken_t1851405782 )L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B7_0 = L_11;
		goto IL_004b;
	}

IL_0044:
	{
		Task_1_t1809478302 * L_12 = V_0;
		Exception_t1927440687 * L_13 = ___exception0;
		NullCheck((Task_1_t1809478302 *)L_12);
		bool L_14 = ((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((Task_1_t1809478302 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		G_B7_0 = L_14;
	}

IL_004b:
	{
		V_2 = (bool)G_B7_0;
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_16 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral3568671706, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_17 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_17, (String_t*)L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0062:
	{
		return;
	}
}
extern "C"  void AsyncTaskMethodBuilder_1_SetException_m4054625890_AdjustorThunk (Il2CppObject * __this, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t1272420930 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t1272420930 *>(__this + 1);
	AsyncTaskMethodBuilder_1_SetException_m4054625890(_thisAdjusted, ___exception0, method);
}
// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::GetTaskForResult(TResult)
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_gshared (AsyncTaskMethodBuilder_1_t1272420930 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Task_1_t2945603725 * V_2 = NULL;
	int32_t V_3 = 0;
	Task_1_t1191906455 * V_4 = NULL;
	IntPtr_t V_5;
	memset(&V_5, 0, sizeof(V_5));
	UIntPtr_t  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Task_1_t2945603725 * G_B5_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		if (!L_0)
		{
			goto IL_02de;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Boolean_t3825574718_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		Il2CppObject * L_4 = ___result0;
		V_1 = (bool)((*(bool*)((bool*)UnBox(L_4, Boolean_t3825574718_il2cpp_TypeInfo_var))));
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t2945603725 * L_6 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_TrueTask_0();
		G_B5_0 = L_6;
		goto IL_0052;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t2945603725 * L_7 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_FalseTask_1();
		G_B5_0 = L_7;
	}

IL_0052:
	{
		V_2 = (Task_1_t2945603725 *)G_B5_0;
		Task_1_t2945603725 * L_8 = V_2;
		Task_1_t1809478302 * L_9 = ((  Task_1_t1809478302 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_9;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		bool L_12 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_10, (Type_t *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ab;
		}
	}
	{
		Il2CppObject * L_13 = ___result0;
		V_3 = (int32_t)((*(int32_t*)((int32_t*)UnBox(L_13, Int32_t2071877448_il2cpp_TypeInfo_var))));
		int32_t L_14 = V_3;
		if ((((int32_t)L_14) >= ((int32_t)((int32_t)9))))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_15 = V_3;
		if ((((int32_t)L_15) < ((int32_t)(-1))))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1U5BU5D_t2481017646* L_16 = ((AsyncTaskCache_t3842594813_StaticFields*)AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var->static_fields)->get_Int32Tasks_2();
		int32_t L_17 = V_3;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)((int32_t)L_17-(int32_t)(-1)));
		Task_1_t1191906455 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_4 = (Task_1_t1191906455 *)L_19;
		Task_1_t1191906455 * L_20 = V_4;
		Task_1_t1809478302 * L_21 = ((  Task_1_t1809478302 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_21;
	}

IL_00a6:
	{
		goto IL_02d9;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_23 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt32_t2149682021_0_0_0_var), /*hidden argument*/NULL);
		bool L_24 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_22, (Type_t *)L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d9;
		}
	}
	{
		Il2CppObject * L_25 = ___result0;
		if (!((*(uint32_t*)((uint32_t*)UnBox(L_25, UInt32_t2149682021_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_28 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_26, (Type_t *)L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0107;
		}
	}
	{
		Il2CppObject * L_29 = ___result0;
		if (!((*(uint8_t*)((uint8_t*)UnBox(L_29, Byte_t3683104436_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0107:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_31 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByte_t454417549_0_0_0_var), /*hidden argument*/NULL);
		bool L_32 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_30, (Type_t *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0136;
		}
	}
	{
		Il2CppObject * L_33 = ___result0;
		if (!(((int32_t)((int32_t)((*(int8_t*)((int8_t*)UnBox(L_33, SByte_t454417549_il2cpp_TypeInfo_var))))))))
		{
			goto IL_02d3;
		}
	}

IL_0136:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_35 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Char_t3454481338_0_0_0_var), /*hidden argument*/NULL);
		bool L_36 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_34, (Type_t *)L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0164;
		}
	}
	{
		Il2CppObject * L_37 = ___result0;
		if (!((*(Il2CppChar*)((Il2CppChar*)UnBox(L_37, Char_t3454481338_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0164:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Decimal_t724701077_0_0_0_var), /*hidden argument*/NULL);
		bool L_40 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_38, (Type_t *)L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_019d;
		}
	}
	{
		Decimal_t724701077  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Decimal__ctor_m1010012873(&L_41, (int32_t)0, /*hidden argument*/NULL);
		Il2CppObject * L_42 = ___result0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_43 = Decimal_op_Equality_m2278618154(NULL /*static, unused*/, (Decimal_t724701077 )L_41, (Decimal_t724701077 )((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox(L_42, Decimal_t724701077_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_02d3;
		}
	}

IL_019d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		bool L_46 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_44, (Type_t *)L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01cd;
		}
	}
	{
		Il2CppObject * L_47 = ___result0;
		if ((((int64_t)((*(int64_t*)((int64_t*)UnBox(L_47, Int64_t909078037_il2cpp_TypeInfo_var))))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02d3;
		}
	}

IL_01cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_49 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt64_t2909196914_0_0_0_var), /*hidden argument*/NULL);
		bool L_50 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_48, (Type_t *)L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01fd;
		}
	}
	{
		Il2CppObject * L_51 = ___result0;
		if ((((int64_t)((*(uint64_t*)((uint64_t*)UnBox(L_51, UInt64_t2909196914_il2cpp_TypeInfo_var))))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02d3;
		}
	}

IL_01fd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_52 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_53 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Int16_t4041245914_0_0_0_var), /*hidden argument*/NULL);
		bool L_54 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_52, (Type_t *)L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_022b;
		}
	}
	{
		Il2CppObject * L_55 = ___result0;
		if (!((*(int16_t*)((int16_t*)UnBox(L_55, Int16_t4041245914_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_022b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_56 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_57 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UInt16_t986882611_0_0_0_var), /*hidden argument*/NULL);
		bool L_58 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_56, (Type_t *)L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0259;
		}
	}
	{
		Il2CppObject * L_59 = ___result0;
		if (!((*(uint16_t*)((uint16_t*)UnBox(L_59, UInt16_t986882611_il2cpp_TypeInfo_var)))))
		{
			goto IL_02d3;
		}
	}

IL_0259:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_60 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_61 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IntPtr_t_0_0_0_var), /*hidden argument*/NULL);
		bool L_62 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_60, (Type_t *)L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0296;
		}
	}
	{
		Initobj (IntPtr_t_il2cpp_TypeInfo_var, (&V_5));
		IntPtr_t L_63 = V_5;
		Il2CppObject * L_64 = ___result0;
		bool L_65 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, (IntPtr_t)L_63, (IntPtr_t)((*(IntPtr_t*)((IntPtr_t*)UnBox(L_64, IntPtr_t_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (L_65)
		{
			goto IL_02d3;
		}
	}

IL_0296:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_66 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		Type_t * L_67 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(UIntPtr_t_0_0_0_var), /*hidden argument*/NULL);
		bool L_68 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_66, (Type_t *)L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_02d9;
		}
	}
	{
		Initobj (UIntPtr_t_il2cpp_TypeInfo_var, (&V_6));
		UIntPtr_t  L_69 = V_6;
		Il2CppObject * L_70 = ___result0;
		IL2CPP_RUNTIME_CLASS_INIT(UIntPtr_t_il2cpp_TypeInfo_var);
		bool L_71 = UIntPtr_op_Equality_m1435732519(NULL /*static, unused*/, (UIntPtr_t )L_69, (UIntPtr_t )((*(UIntPtr_t *)((UIntPtr_t *)UnBox(L_70, UIntPtr_t_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_02d9;
		}
	}

IL_02d3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Task_1_t1809478302 * L_72 = ((AsyncTaskMethodBuilder_1_t1272420930_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->get_s_defaultResultTask_0();
		return L_72;
	}

IL_02d9:
	{
		goto IL_02ef;
	}

IL_02de:
	{
		Il2CppObject * L_73 = ___result0;
		if (L_73)
		{
			goto IL_02ef;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Task_1_t1809478302 * L_74 = ((AsyncTaskMethodBuilder_1_t1272420930_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->get_s_defaultResultTask_0();
		return L_74;
	}

IL_02ef:
	{
		Il2CppObject * L_75 = ___result0;
		Task_1_t1809478302 * L_76 = (Task_1_t1809478302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_76, (Il2CppObject *)L_75, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_76;
	}
}
extern "C"  Task_1_t1809478302 * AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	AsyncTaskMethodBuilder_1_t1272420930 * _thisAdjusted = reinterpret_cast<AsyncTaskMethodBuilder_1_t1272420930 *>(__this + 1);
	return AsyncTaskMethodBuilder_1_GetTaskForResult_m1442674425(_thisAdjusted, ___result0, method);
}
// System.Void System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>::.cctor()
extern "C"  void AsyncTaskMethodBuilder_1__cctor_m4171130357_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncTaskMethodBuilder_1__cctor_m4171130357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AsyncTaskCache_t3842594813_il2cpp_TypeInfo_var);
		Task_1_t1809478302 * L_1 = ((  Task_1_t1809478302 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((AsyncTaskMethodBuilder_1_t1272420930_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->static_fields)->set_s_defaultResultTask_0(L_1);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::.ctor()
extern "C"  void ConditionalWeakTable_2__ctor_m3945060102_gshared (ConditionalWeakTable_2_t1530143791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionalWeakTable_2__ctor_m3945060102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_1(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_data_0(((EphemeronU5BU5D_t3006146916*)SZArrayNew(EphemeronU5BU5D_t3006146916_il2cpp_TypeInfo_var, (uint32_t)((int32_t)13))));
		EphemeronU5BU5D_t3006146916* L_1 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_register_ephemeron_array_m3882692219(NULL /*static, unused*/, (EphemeronU5BU5D_t3006146916*)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::Finalize()
extern "C"  void ConditionalWeakTable_2_Finalize_m1923314542_gshared (ConditionalWeakTable_2_t1530143791 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		NullCheck((Il2CppObject *)__this);
		Object_Finalize_m4087144328((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::Rehash()
extern "C"  void ConditionalWeakTable_2_Rehash_m2504890315_gshared (ConditionalWeakTable_2_t1530143791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionalWeakTable_2_Rehash_m2504890315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	EphemeronU5BU5D_t3006146916* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Il2CppObject * V_9 = NULL;
	{
		EphemeronU5BU5D_t3006146916* L_0 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(HashHelpers_t2236981683_il2cpp_TypeInfo_var);
		int32_t L_1 = HashHelpers_GetPrime_m3996345109(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (uint32_t)L_1;
		uint32_t L_2 = V_0;
		V_1 = (EphemeronU5BU5D_t3006146916*)((EphemeronU5BU5D_t3006146916*)SZArrayNew(EphemeronU5BU5D_t3006146916_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_2))));
		EphemeronU5BU5D_t3006146916* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		GC_register_ephemeron_array_m3882692219(NULL /*static, unused*/, (EphemeronU5BU5D_t3006146916*)L_3, /*hidden argument*/NULL);
		__this->set_size_2(0);
		V_2 = (int32_t)0;
		goto IL_00f9;
	}

IL_002e:
	{
		EphemeronU5BU5D_t3006146916* L_4 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Il2CppObject * L_6 = (Il2CppObject *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_key_0();
		V_3 = (Il2CppObject *)L_6;
		EphemeronU5BU5D_t3006146916* L_7 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Il2CppObject * L_9 = (Il2CppObject *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))->get_value_1();
		V_4 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_3;
		if (!L_10)
		{
			goto IL_0064;
		}
	}
	{
		Il2CppObject * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = ((GC_t2902933594_StaticFields*)GC_t2902933594_il2cpp_TypeInfo_var->static_fields)->get_EPHEMERON_TOMBSTONE_0();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_11) == ((Il2CppObject*)(Il2CppObject *)L_12))))
		{
			goto IL_0069;
		}
	}

IL_0064:
	{
		goto IL_00f5;
	}

IL_0069:
	{
		EphemeronU5BU5D_t3006146916* L_13 = V_1;
		NullCheck(L_13);
		V_5 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		V_8 = (int32_t)(-1);
		Il2CppObject * L_14 = V_3;
		int32_t L_15 = RuntimeHelpers_GetHashCode_m3703616886(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_5;
		int32_t L_17 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_16));
		V_7 = (int32_t)L_17;
		V_6 = (int32_t)L_17;
	}

IL_0085:
	{
		EphemeronU5BU5D_t3006146916* L_18 = V_1;
		int32_t L_19 = V_6;
		NullCheck(L_18);
		Il2CppObject * L_20 = (Il2CppObject *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->get_key_0();
		V_9 = (Il2CppObject *)L_20;
		Il2CppObject * L_21 = V_9;
		if (!L_21)
		{
			goto IL_00a7;
		}
	}
	{
		Il2CppObject * L_22 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
		Il2CppObject * L_23 = ((GC_t2902933594_StaticFields*)GC_t2902933594_il2cpp_TypeInfo_var->static_fields)->get_EPHEMERON_TOMBSTONE_0();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_22) == ((Il2CppObject*)(Il2CppObject *)L_23))))
		{
			goto IL_00b0;
		}
	}

IL_00a7:
	{
		int32_t L_24 = V_6;
		V_8 = (int32_t)L_24;
		goto IL_00ca;
	}

IL_00b0:
	{
		int32_t L_25 = V_6;
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_6 = (int32_t)L_26;
		int32_t L_27 = V_5;
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			goto IL_00c1;
		}
	}
	{
		V_6 = (int32_t)0;
	}

IL_00c1:
	{
		int32_t L_28 = V_6;
		int32_t L_29 = V_7;
		if ((!(((uint32_t)L_28) == ((uint32_t)L_29))))
		{
			goto IL_0085;
		}
	}

IL_00ca:
	{
		EphemeronU5BU5D_t3006146916* L_30 = V_1;
		int32_t L_31 = V_8;
		NullCheck(L_30);
		Il2CppObject * L_32 = V_3;
		((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_31)))->set_key_0(L_32);
		EphemeronU5BU5D_t3006146916* L_33 = V_1;
		int32_t L_34 = V_8;
		NullCheck(L_33);
		Il2CppObject * L_35 = V_4;
		((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->set_value_1(L_35);
		int32_t L_36 = (int32_t)__this->get_size_2();
		__this->set_size_2(((int32_t)((int32_t)L_36+(int32_t)1)));
	}

IL_00f5:
	{
		int32_t L_37 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f9:
	{
		int32_t L_38 = V_2;
		EphemeronU5BU5D_t3006146916* L_39 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_002e;
		}
	}
	{
		EphemeronU5BU5D_t3006146916* L_40 = V_1;
		__this->set_data_0(L_40);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ConditionalWeakTable_2_Add_m1961639487_gshared (ConditionalWeakTable_2_t1530143791 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionalWeakTable_2_Add_m1961639487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___key0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))))))
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, (String_t*)_stringLiteral1609019432, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0026:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__lock_1();
		V_0 = (Il2CppObject *)L_2;
		V_1 = (bool)0;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_3 = V_0;
			Monitor_Enter_m2026616996(NULL /*static, unused*/, (Il2CppObject *)L_3, (bool*)(&V_1), /*hidden argument*/NULL);
			int32_t L_4 = (int32_t)__this->get_size_2();
			EphemeronU5BU5D_t3006146916* L_5 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			NullCheck(L_5);
			if ((!(((float)(((float)((float)L_4)))) >= ((float)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))))*(float)(0.7f)))))))
			{
				goto IL_0058;
			}
		}

IL_0052:
		{
			NullCheck((ConditionalWeakTable_2_t1530143791 *)__this);
			((  void (*) (ConditionalWeakTable_2_t1530143791 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ConditionalWeakTable_2_t1530143791 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		}

IL_0058:
		{
			EphemeronU5BU5D_t3006146916* L_6 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			NullCheck(L_6);
			V_2 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))));
			V_5 = (int32_t)(-1);
			Il2CppObject * L_7 = ___key0;
			int32_t L_8 = RuntimeHelpers_GetHashCode_m3703616886(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
			int32_t L_9 = V_2;
			int32_t L_10 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_9));
			V_4 = (int32_t)L_10;
			V_3 = (int32_t)L_10;
		}

IL_007b:
		{
			EphemeronU5BU5D_t3006146916* L_11 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_12 = V_3;
			NullCheck(L_11);
			Il2CppObject * L_13 = (Il2CppObject *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_key_0();
			V_6 = (Il2CppObject *)L_13;
			Il2CppObject * L_14 = V_6;
			if (L_14)
			{
				goto IL_00a5;
			}
		}

IL_0095:
		{
			int32_t L_15 = V_5;
			if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
			{
				goto IL_00a0;
			}
		}

IL_009d:
		{
			int32_t L_16 = V_3;
			V_5 = (int32_t)L_16;
		}

IL_00a0:
		{
			goto IL_00f3;
		}

IL_00a5:
		{
			Il2CppObject * L_17 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			Il2CppObject * L_18 = ((GC_t2902933594_StaticFields*)GC_t2902933594_il2cpp_TypeInfo_var->static_fields)->get_EPHEMERON_TOMBSTONE_0();
			if ((!(((Il2CppObject*)(Il2CppObject *)L_17) == ((Il2CppObject*)(Il2CppObject *)L_18))))
			{
				goto IL_00c1;
			}
		}

IL_00b1:
		{
			int32_t L_19 = V_5;
			if ((!(((uint32_t)L_19) == ((uint32_t)(-1)))))
			{
				goto IL_00c1;
			}
		}

IL_00b9:
		{
			int32_t L_20 = V_3;
			V_5 = (int32_t)L_20;
			goto IL_00de;
		}

IL_00c1:
		{
			Il2CppObject * L_21 = V_6;
			Il2CppObject * L_22 = ___key0;
			if ((!(((Il2CppObject*)(Il2CppObject *)L_21) == ((Il2CppObject*)(Il2CppObject *)L_22))))
			{
				goto IL_00de;
			}
		}

IL_00ce:
		{
			ArgumentException_t3259014390 * L_23 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m544251339(L_23, (String_t*)_stringLiteral2956391297, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
		}

IL_00de:
		{
			int32_t L_24 = V_3;
			int32_t L_25 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
			V_3 = (int32_t)L_25;
			int32_t L_26 = V_2;
			if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
			{
				goto IL_00eb;
			}
		}

IL_00e9:
		{
			V_3 = (int32_t)0;
		}

IL_00eb:
		{
			int32_t L_27 = V_3;
			int32_t L_28 = V_4;
			if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
			{
				goto IL_007b;
			}
		}

IL_00f3:
		{
			EphemeronU5BU5D_t3006146916* L_29 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_30 = V_5;
			NullCheck(L_29);
			Il2CppObject * L_31 = ___key0;
			((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30)))->set_key_0(L_31);
			EphemeronU5BU5D_t3006146916* L_32 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_33 = V_5;
			NullCheck(L_32);
			Il2CppObject * L_34 = ___value1;
			((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33)))->set_value_1(L_34);
			int32_t L_35 = (int32_t)__this->get_size_2();
			__this->set_size_2(((int32_t)((int32_t)L_35+(int32_t)1)));
			IL2CPP_LEAVE(0x140, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		{
			bool L_36 = V_1;
			if (!L_36)
			{
				goto IL_013f;
			}
		}

IL_0139:
		{
			Il2CppObject * L_37 = V_0;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_37, /*hidden argument*/NULL);
		}

IL_013f:
		{
			IL2CPP_END_FINALLY(310)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x140, IL_0140)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0140:
	{
		return;
	}
}
// System.Boolean System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ConditionalWeakTable_2_Remove_m554617975_gshared (ConditionalWeakTable_2_t1530143791 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionalWeakTable_2_Remove_m554617975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___key0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)))))))
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, (String_t*)_stringLiteral1609019432, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0026:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__lock_1();
		V_0 = (Il2CppObject *)L_2;
		V_1 = (bool)0;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_3 = V_0;
			Monitor_Enter_m2026616996(NULL /*static, unused*/, (Il2CppObject *)L_3, (bool*)(&V_1), /*hidden argument*/NULL);
			EphemeronU5BU5D_t3006146916* L_4 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			NullCheck(L_4);
			V_2 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))));
			Il2CppObject * L_5 = ___key0;
			int32_t L_6 = RuntimeHelpers_GetHashCode_m3703616886(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
			int32_t L_7 = V_2;
			int32_t L_8 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_7));
			V_4 = (int32_t)L_8;
			V_3 = (int32_t)L_8;
		}

IL_0057:
		{
			EphemeronU5BU5D_t3006146916* L_9 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_10 = V_3;
			NullCheck(L_9);
			Il2CppObject * L_11 = (Il2CppObject *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_key_0();
			V_5 = (Il2CppObject *)L_11;
			Il2CppObject * L_12 = V_5;
			Il2CppObject * L_13 = ___key0;
			if ((!(((Il2CppObject*)(Il2CppObject *)L_12) == ((Il2CppObject*)(Il2CppObject *)L_13))))
			{
				goto IL_00b5;
			}
		}

IL_0077:
		{
			EphemeronU5BU5D_t3006146916* L_14 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_15 = V_3;
			NullCheck(L_14);
			IL2CPP_RUNTIME_CLASS_INIT(GC_t2902933594_il2cpp_TypeInfo_var);
			Il2CppObject * L_16 = ((GC_t2902933594_StaticFields*)GC_t2902933594_il2cpp_TypeInfo_var->static_fields)->get_EPHEMERON_TOMBSTONE_0();
			((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))->set_key_0(L_16);
			EphemeronU5BU5D_t3006146916* L_17 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_18 = V_3;
			NullCheck(L_17);
			((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))->set_value_1(NULL);
			int32_t L_19 = (int32_t)__this->get_size_2();
			__this->set_size_2(((int32_t)((int32_t)L_19-(int32_t)1)));
			V_6 = (bool)1;
			IL2CPP_LEAVE(0xE7, FINALLY_00db);
		}

IL_00b5:
		{
			Il2CppObject * L_20 = V_5;
			if (L_20)
			{
				goto IL_00c1;
			}
		}

IL_00bc:
		{
			goto IL_00d6;
		}

IL_00c1:
		{
			int32_t L_21 = V_3;
			int32_t L_22 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
			V_3 = (int32_t)L_22;
			int32_t L_23 = V_2;
			if ((!(((uint32_t)L_22) == ((uint32_t)L_23))))
			{
				goto IL_00ce;
			}
		}

IL_00cc:
		{
			V_3 = (int32_t)0;
		}

IL_00ce:
		{
			int32_t L_24 = V_3;
			int32_t L_25 = V_4;
			if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
			{
				goto IL_0057;
			}
		}

IL_00d6:
		{
			IL2CPP_LEAVE(0xE5, FINALLY_00db);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00db;
	}

FINALLY_00db:
	{ // begin finally (depth: 1)
		{
			bool L_26 = V_1;
			if (!L_26)
			{
				goto IL_00e4;
			}
		}

IL_00de:
		{
			Il2CppObject * L_27 = V_0;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
		}

IL_00e4:
		{
			IL2CPP_END_FINALLY(219)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(219)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_JUMP_TBL(0xE5, IL_00e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e5:
	{
		return (bool)0;
	}

IL_00e7:
	{
		bool L_28 = V_6;
		return L_28;
	}
}
// System.Boolean System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ConditionalWeakTable_2_TryGetValue_m2783224372_gshared (ConditionalWeakTable_2_t1530143791 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionalWeakTable_2_TryGetValue_m2783224372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_1, (String_t*)_stringLiteral1609019432, (String_t*)_stringLiteral3021628599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001b:
	{
		Il2CppObject ** L_2 = ___value1;
		(*(Il2CppObject **)L_2) = ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_2, ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_1();
		V_0 = (Il2CppObject *)L_3;
		V_1 = (bool)0;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_4 = V_0;
			Monitor_Enter_m2026616996(NULL /*static, unused*/, (Il2CppObject *)L_4, (bool*)(&V_1), /*hidden argument*/NULL);
			EphemeronU5BU5D_t3006146916* L_5 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			NullCheck(L_5);
			V_2 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))));
			Il2CppObject * L_6 = ___key0;
			int32_t L_7 = RuntimeHelpers_GetHashCode_m3703616886(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
			int32_t L_8 = V_2;
			int32_t L_9 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_8));
			V_4 = (int32_t)L_9;
			V_3 = (int32_t)L_9;
		}

IL_0058:
		{
			EphemeronU5BU5D_t3006146916* L_10 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_11 = V_3;
			NullCheck(L_10);
			Il2CppObject * L_12 = (Il2CppObject *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_key_0();
			V_5 = (Il2CppObject *)L_12;
			Il2CppObject * L_13 = V_5;
			Il2CppObject * L_14 = ___key0;
			if ((!(((Il2CppObject*)(Il2CppObject *)L_13) == ((Il2CppObject*)(Il2CppObject *)L_14))))
			{
				goto IL_009c;
			}
		}

IL_0078:
		{
			Il2CppObject ** L_15 = ___value1;
			EphemeronU5BU5D_t3006146916* L_16 = (EphemeronU5BU5D_t3006146916*)__this->get_data_0();
			int32_t L_17 = V_3;
			NullCheck(L_16);
			Il2CppObject * L_18 = (Il2CppObject *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))->get_value_1();
			(*(Il2CppObject **)L_15) = ((Il2CppObject *)Castclass(L_18, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
			Il2CppCodeGenWriteBarrier((Il2CppObject **)L_15, ((Il2CppObject *)Castclass(L_18, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
			V_6 = (bool)1;
			IL2CPP_LEAVE(0xCE, FINALLY_00c2);
		}

IL_009c:
		{
			Il2CppObject * L_19 = V_5;
			if (L_19)
			{
				goto IL_00a8;
			}
		}

IL_00a3:
		{
			goto IL_00bd;
		}

IL_00a8:
		{
			int32_t L_20 = V_3;
			int32_t L_21 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
			V_3 = (int32_t)L_21;
			int32_t L_22 = V_2;
			if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
			{
				goto IL_00b5;
			}
		}

IL_00b3:
		{
			V_3 = (int32_t)0;
		}

IL_00b5:
		{
			int32_t L_23 = V_3;
			int32_t L_24 = V_4;
			if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
			{
				goto IL_0058;
			}
		}

IL_00bd:
		{
			IL2CPP_LEAVE(0xCC, FINALLY_00c2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c2;
	}

FINALLY_00c2:
	{ // begin finally (depth: 1)
		{
			bool L_25 = V_1;
			if (!L_25)
			{
				goto IL_00cb;
			}
		}

IL_00c5:
		{
			Il2CppObject * L_26 = V_0;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_26, /*hidden argument*/NULL);
		}

IL_00cb:
		{
			IL2CPP_END_FINALLY(194)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(194)
	{
		IL2CPP_JUMP_TBL(0xCE, IL_00ce)
		IL2CPP_JUMP_TBL(0xCC, IL_00cc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00cc:
	{
		return (bool)0;
	}

IL_00ce:
	{
		bool L_27 = V_6;
		return L_27;
	}
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m136027847_gshared (ConfiguredTaskAwaiter_t446638270 * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		bool L_1 = ___continueOnCapturedContext1;
		__this->set_m_continueOnCapturedContext_1(L_1);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter__ctor_m136027847_AdjustorThunk (Il2CppObject * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t446638270 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t446638270 *>(__this + 1);
	ConfiguredTaskAwaiter__ctor_m136027847(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2127236892_gshared (ConfiguredTaskAwaiter_t446638270 * __this, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_0();
		NullCheck((Task_t1843236107 *)L_0);
		bool L_1 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2127236892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t446638270 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t446638270 *>(__this + 1);
	return ConfiguredTaskAwaiter_get_IsCompleted_m2127236892(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015_gshared (ConfiguredTaskAwaiter_t446638270 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		bool L_2 = (bool)__this->get_m_continueOnCapturedContext_1();
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t446638270 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t446638270 *>(__this + 1);
	ConfiguredTaskAwaiter_UnsafeOnCompleted_m1131091015(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean>::GetResult()
extern "C"  bool ConfiguredTaskAwaiter_GetResult_m2361999369_gshared (ConfiguredTaskAwaiter_t446638270 * __this, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t2945603725 * L_1 = (Task_1_t2945603725 *)__this->get_m_task_0();
		NullCheck((Task_1_t2945603725 *)L_1);
		bool L_2 = ((  bool (*) (Task_1_t2945603725 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t2945603725 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  bool ConfiguredTaskAwaiter_GetResult_m2361999369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t446638270 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t446638270 *>(__this + 1);
	return ConfiguredTaskAwaiter_GetResult_m2361999369(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m3345350369_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		bool L_1 = ___continueOnCapturedContext1;
		__this->set_m_continueOnCapturedContext_1(L_1);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter__ctor_m3345350369_AdjustorThunk (Il2CppObject * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t2987908296 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t2987908296 *>(__this + 1);
	ConfiguredTaskAwaiter__ctor_m3345350369(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m998688528_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = (Task_1_t1191906455 *)__this->get_m_task_0();
		NullCheck((Task_t1843236107 *)L_0);
		bool L_1 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m998688528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t2987908296 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t2987908296 *>(__this + 1);
	return ConfiguredTaskAwaiter_get_IsCompleted_m998688528(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = (Task_1_t1191906455 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		bool L_2 = (bool)__this->get_m_continueOnCapturedContext_1();
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t2987908296 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t2987908296 *>(__this + 1);
	ConfiguredTaskAwaiter_UnsafeOnCompleted_m158591689(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>::GetResult()
extern "C"  int32_t ConfiguredTaskAwaiter_GetResult_m1667949563_gshared (ConfiguredTaskAwaiter_t2987908296 * __this, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = (Task_1_t1191906455 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t1191906455 * L_1 = (Task_1_t1191906455 *)__this->get_m_task_0();
		NullCheck((Task_1_t1191906455 *)L_1);
		int32_t L_2 = ((  int32_t (*) (Task_1_t1191906455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t1191906455 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  int32_t ConfiguredTaskAwaiter_GetResult_m1667949563_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t2987908296 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t2987908296 *>(__this + 1);
	return ConfiguredTaskAwaiter_GetResult_m1667949563(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m2085377116_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		bool L_1 = ___continueOnCapturedContext1;
		__this->set_m_continueOnCapturedContext_1(L_1);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter__ctor_m2085377116_AdjustorThunk (Il2CppObject * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t3605480143 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t3605480143 *>(__this + 1);
	ConfiguredTaskAwaiter__ctor_m2085377116(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m1244481799_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_0();
		NullCheck((Task_t1843236107 *)L_0);
		bool L_1 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m1244481799_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t3605480143 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t3605480143 *>(__this + 1);
	return ConfiguredTaskAwaiter_get_IsCompleted_m1244481799(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		bool L_2 = (bool)__this->get_m_continueOnCapturedContext_1();
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t3605480143 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t3605480143 *>(__this + 1);
	ConfiguredTaskAwaiter_UnsafeOnCompleted_m470959546(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Object>::GetResult()
extern "C"  Il2CppObject * ConfiguredTaskAwaiter_GetResult_m417350004_gshared (ConfiguredTaskAwaiter_t3605480143 * __this, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t1809478302 * L_1 = (Task_1_t1809478302 *)__this->get_m_task_0();
		NullCheck((Task_1_t1809478302 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Task_1_t1809478302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t1809478302 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  Il2CppObject * ConfiguredTaskAwaiter_GetResult_m417350004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t3605480143 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t3605480143 *>(__this + 1);
	return ConfiguredTaskAwaiter_GetResult_m417350004(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaiter__ctor_m3341903075_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		bool L_1 = ___continueOnCapturedContext1;
		__this->set_m_continueOnCapturedContext_1(L_1);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter__ctor_m3341903075_AdjustorThunk (Il2CppObject * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t4241341646 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t4241341646 *>(__this + 1);
	ConfiguredTaskAwaiter__ctor_m3341903075(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::get_IsCompleted()
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2053182946_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = (Task_1_t2445339805 *)__this->get_m_task_0();
		NullCheck((Task_t1843236107 *)L_0);
		bool L_1 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  bool ConfiguredTaskAwaiter_get_IsCompleted_m2053182946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t4241341646 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t4241341646 *>(__this + 1);
	return ConfiguredTaskAwaiter_get_IsCompleted_m2053182946(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = (Task_1_t2445339805 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		bool L_2 = (bool)__this->get_m_continueOnCapturedContext_1();
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t4241341646 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t4241341646 *>(__this + 1);
	ConfiguredTaskAwaiter_UnsafeOnCompleted_m1833350523(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.VoidTaskResult>::GetResult()
extern "C"  VoidTaskResult_t3325310798  ConfiguredTaskAwaiter_GetResult_m1629433097_gshared (ConfiguredTaskAwaiter_t4241341646 * __this, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = (Task_1_t2445339805 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t2445339805 * L_1 = (Task_1_t2445339805 *)__this->get_m_task_0();
		NullCheck((Task_1_t2445339805 *)L_1);
		VoidTaskResult_t3325310798  L_2 = ((  VoidTaskResult_t3325310798  (*) (Task_1_t2445339805 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t2445339805 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  VoidTaskResult_t3325310798  ConfiguredTaskAwaiter_GetResult_m1629433097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaiter_t4241341646 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaiter_t4241341646 *>(__this + 1);
	return ConfiguredTaskAwaiter_GetResult_m1629433097(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m1905790602_gshared (ConfiguredTaskAwaitable_1_t34161435 * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = ___task0;
		bool L_1 = ___continueOnCapturedContext1;
		ConfiguredTaskAwaiter_t446638270  L_2;
		memset(&L_2, 0, sizeof(L_2));
		ConfiguredTaskAwaiter__ctor_m136027847(&L_2, (Task_1_t2945603725 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_m_configuredTaskAwaiter_0(L_2);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m1905790602_AdjustorThunk (Il2CppObject * __this, Task_1_t2945603725 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t34161435 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t34161435 *>(__this + 1);
	ConfiguredTaskAwaitable_1__ctor_m1905790602(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Boolean>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t446638270  ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489_gshared (ConfiguredTaskAwaitable_1_t34161435 * __this, const MethodInfo* method)
{
	{
		ConfiguredTaskAwaiter_t446638270  L_0 = (ConfiguredTaskAwaiter_t446638270 )__this->get_m_configuredTaskAwaiter_0();
		return L_0;
	}
}
extern "C"  ConfiguredTaskAwaiter_t446638270  ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t34161435 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t34161435 *>(__this + 1);
	return ConfiguredTaskAwaitable_1_GetAwaiter_m1352754489(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2687722690_gshared (ConfiguredTaskAwaitable_1_t2575431461 * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = ___task0;
		bool L_1 = ___continueOnCapturedContext1;
		ConfiguredTaskAwaiter_t2987908296  L_2;
		memset(&L_2, 0, sizeof(L_2));
		ConfiguredTaskAwaiter__ctor_m3345350369(&L_2, (Task_1_t1191906455 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_m_configuredTaskAwaiter_0(L_2);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2687722690_AdjustorThunk (Il2CppObject * __this, Task_1_t1191906455 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t2575431461 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t2575431461 *>(__this + 1);
	ConfiguredTaskAwaitable_1__ctor_m2687722690(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Int32>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t2987908296  ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283_gshared (ConfiguredTaskAwaitable_1_t2575431461 * __this, const MethodInfo* method)
{
	{
		ConfiguredTaskAwaiter_t2987908296  L_0 = (ConfiguredTaskAwaiter_t2987908296 )__this->get_m_configuredTaskAwaiter_0();
		return L_0;
	}
}
extern "C"  ConfiguredTaskAwaiter_t2987908296  ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t2575431461 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t2575431461 *>(__this + 1);
	return ConfiguredTaskAwaitable_1_GetAwaiter_m2329495283(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m740361621_gshared (ConfiguredTaskAwaitable_1_t3193003308 * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = ___task0;
		bool L_1 = ___continueOnCapturedContext1;
		ConfiguredTaskAwaiter_t3605480143  L_2;
		memset(&L_2, 0, sizeof(L_2));
		ConfiguredTaskAwaiter__ctor_m2085377116(&L_2, (Task_1_t1809478302 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_m_configuredTaskAwaiter_0(L_2);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m740361621_AdjustorThunk (Il2CppObject * __this, Task_1_t1809478302 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t3193003308 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t3193003308 *>(__this + 1);
	ConfiguredTaskAwaitable_1__ctor_m740361621(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Object>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t3605480143  ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240_gshared (ConfiguredTaskAwaitable_1_t3193003308 * __this, const MethodInfo* method)
{
	{
		ConfiguredTaskAwaiter_t3605480143  L_0 = (ConfiguredTaskAwaiter_t3605480143 )__this->get_m_configuredTaskAwaiter_0();
		return L_0;
	}
}
extern "C"  ConfiguredTaskAwaiter_t3605480143  ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t3193003308 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t3193003308 *>(__this + 1);
	return ConfiguredTaskAwaitable_1_GetAwaiter_m1320990240(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2250484098_gshared (ConfiguredTaskAwaitable_1_t3828864811 * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = ___task0;
		bool L_1 = ___continueOnCapturedContext1;
		ConfiguredTaskAwaiter_t4241341646  L_2;
		memset(&L_2, 0, sizeof(L_2));
		ConfiguredTaskAwaiter__ctor_m3341903075(&L_2, (Task_1_t2445339805 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_m_configuredTaskAwaiter_0(L_2);
		return;
	}
}
extern "C"  void ConfiguredTaskAwaitable_1__ctor_m2250484098_AdjustorThunk (Il2CppObject * __this, Task_1_t2445339805 * ___task0, bool ___continueOnCapturedContext1, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t3828864811 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t3828864811 *>(__this + 1);
	ConfiguredTaskAwaitable_1__ctor_m2250484098(_thisAdjusted, ___task0, ___continueOnCapturedContext1, method);
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<System.Threading.Tasks.VoidTaskResult>::GetAwaiter()
extern "C"  ConfiguredTaskAwaiter_t4241341646  ConfiguredTaskAwaitable_1_GetAwaiter_m56114985_gshared (ConfiguredTaskAwaitable_1_t3828864811 * __this, const MethodInfo* method)
{
	{
		ConfiguredTaskAwaiter_t4241341646  L_0 = (ConfiguredTaskAwaiter_t4241341646 )__this->get_m_configuredTaskAwaiter_0();
		return L_0;
	}
}
extern "C"  ConfiguredTaskAwaiter_t4241341646  ConfiguredTaskAwaitable_1_GetAwaiter_m56114985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ConfiguredTaskAwaitable_1_t3828864811 * _thisAdjusted = reinterpret_cast<ConfiguredTaskAwaitable_1_t3828864811 *>(__this + 1);
	return ConfiguredTaskAwaitable_1_GetAwaiter_m56114985(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m3987458582_gshared (TaskAwaiter_1_t899200082 * __this, Task_1_t2945603725 * ___task0, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		return;
	}
}
extern "C"  void TaskAwaiter_1__ctor_m3987458582_AdjustorThunk (Il2CppObject * __this, Task_1_t2945603725 * ___task0, const MethodInfo* method)
{
	TaskAwaiter_1_t899200082 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t899200082 *>(__this + 1);
	TaskAwaiter_1__ctor_m3987458582(_thisAdjusted, ___task0, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1467565677_gshared (TaskAwaiter_1_t899200082 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1467565677_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	TaskAwaiter_1_t899200082 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t899200082 *>(__this + 1);
	TaskAwaiter_1_UnsafeOnCompleted_m1467565677(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>::GetResult()
extern "C"  bool TaskAwaiter_1_GetResult_m2570428347_gshared (TaskAwaiter_1_t899200082 * __this, const MethodInfo* method)
{
	{
		Task_1_t2945603725 * L_0 = (Task_1_t2945603725 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t2945603725 * L_1 = (Task_1_t2945603725 *)__this->get_m_task_0();
		NullCheck((Task_1_t2945603725 *)L_1);
		bool L_2 = ((  bool (*) (Task_1_t2945603725 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t2945603725 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  bool TaskAwaiter_1_GetResult_m2570428347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TaskAwaiter_1_t899200082 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t899200082 *>(__this + 1);
	return TaskAwaiter_1_GetResult_m2570428347(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m3485680026_gshared (TaskAwaiter_1_t3440470108 * __this, Task_1_t1191906455 * ___task0, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		return;
	}
}
extern "C"  void TaskAwaiter_1__ctor_m3485680026_AdjustorThunk (Il2CppObject * __this, Task_1_t1191906455 * ___task0, const MethodInfo* method)
{
	TaskAwaiter_1_t3440470108 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t3440470108 *>(__this + 1);
	TaskAwaiter_1__ctor_m3485680026(_thisAdjusted, ___task0, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1344578231_gshared (TaskAwaiter_1_t3440470108 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = (Task_1_t1191906455 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m1344578231_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	TaskAwaiter_1_t3440470108 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t3440470108 *>(__this + 1);
	TaskAwaiter_1_UnsafeOnCompleted_m1344578231(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32>::GetResult()
extern "C"  int32_t TaskAwaiter_1_GetResult_m2916616501_gshared (TaskAwaiter_1_t3440470108 * __this, const MethodInfo* method)
{
	{
		Task_1_t1191906455 * L_0 = (Task_1_t1191906455 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t1191906455 * L_1 = (Task_1_t1191906455 *)__this->get_m_task_0();
		NullCheck((Task_1_t1191906455 *)L_1);
		int32_t L_2 = ((  int32_t (*) (Task_1_t1191906455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t1191906455 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  int32_t TaskAwaiter_1_GetResult_m2916616501_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TaskAwaiter_1_t3440470108 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t3440470108 *>(__this + 1);
	return TaskAwaiter_1_GetResult_m2916616501(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m882453359_gshared (TaskAwaiter_1_t4058041955 * __this, Task_1_t1809478302 * ___task0, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		return;
	}
}
extern "C"  void TaskAwaiter_1__ctor_m882453359_AdjustorThunk (Il2CppObject * __this, Task_1_t1809478302 * ___task0, const MethodInfo* method)
{
	TaskAwaiter_1_t4058041955 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t4058041955 *>(__this + 1);
	TaskAwaiter_1__ctor_m882453359(_thisAdjusted, ___task0, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m3372870064_gshared (TaskAwaiter_1_t4058041955 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m3372870064_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	TaskAwaiter_1_t4058041955 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t4058041955 *>(__this + 1);
	TaskAwaiter_1_UnsafeOnCompleted_m3372870064(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Object>::GetResult()
extern "C"  Il2CppObject * TaskAwaiter_1_GetResult_m2924234054_gshared (TaskAwaiter_1_t4058041955 * __this, const MethodInfo* method)
{
	{
		Task_1_t1809478302 * L_0 = (Task_1_t1809478302 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t1809478302 * L_1 = (Task_1_t1809478302 *)__this->get_m_task_0();
		NullCheck((Task_1_t1809478302 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Task_1_t1809478302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t1809478302 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  Il2CppObject * TaskAwaiter_1_GetResult_m2924234054_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TaskAwaiter_1_t4058041955 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t4058041955 *>(__this + 1);
	return TaskAwaiter_1_GetResult_m2924234054(_thisAdjusted, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.Tasks.Task`1<TResult>)
extern "C"  void TaskAwaiter_1__ctor_m4174446952_gshared (TaskAwaiter_1_t398936162 * __this, Task_1_t2445339805 * ___task0, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = ___task0;
		__this->set_m_task_0(L_0);
		return;
	}
}
extern "C"  void TaskAwaiter_1__ctor_m4174446952_AdjustorThunk (Il2CppObject * __this, Task_1_t2445339805 * ___task0, const MethodInfo* method)
{
	TaskAwaiter_1_t398936162 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t398936162 *>(__this + 1);
	TaskAwaiter_1__ctor_m4174446952(_thisAdjusted, ___task0, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::UnsafeOnCompleted(System.Action)
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m522515533_gshared (TaskAwaiter_1_t398936162 * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = (Task_1_t2445339805 *)__this->get_m_task_0();
		Action_t3226471752 * L_1 = ___continuation0;
		TaskAwaiter_OnCompletedInternal_m1395860754(NULL /*static, unused*/, (Task_t1843236107 *)L_0, (Action_t3226471752 *)L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void TaskAwaiter_1_UnsafeOnCompleted_m522515533_AdjustorThunk (Il2CppObject * __this, Action_t3226471752 * ___continuation0, const MethodInfo* method)
{
	TaskAwaiter_1_t398936162 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t398936162 *>(__this + 1);
	TaskAwaiter_1_UnsafeOnCompleted_m522515533(_thisAdjusted, ___continuation0, method);
}
// TResult System.Runtime.CompilerServices.TaskAwaiter`1<System.Threading.Tasks.VoidTaskResult>::GetResult()
extern "C"  VoidTaskResult_t3325310798  TaskAwaiter_1_GetResult_m2990722415_gshared (TaskAwaiter_1_t398936162 * __this, const MethodInfo* method)
{
	{
		Task_1_t2445339805 * L_0 = (Task_1_t2445339805 *)__this->get_m_task_0();
		TaskAwaiter_ValidateEnd_m4057275037(NULL /*static, unused*/, (Task_t1843236107 *)L_0, /*hidden argument*/NULL);
		Task_1_t2445339805 * L_1 = (Task_1_t2445339805 *)__this->get_m_task_0();
		NullCheck((Task_1_t2445339805 *)L_1);
		VoidTaskResult_t3325310798  L_2 = ((  VoidTaskResult_t3325310798  (*) (Task_1_t2445339805 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Task_1_t2445339805 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
extern "C"  VoidTaskResult_t3325310798  TaskAwaiter_1_GetResult_m2990722415_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	TaskAwaiter_1_t398936162 * _thisAdjusted = reinterpret_cast<TaskAwaiter_1_t398936162 *>(__this + 1);
	return TaskAwaiter_1_GetResult_m2990722415(_thisAdjusted, method);
}
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::.ctor(System.Int32)
extern "C"  void ListBuilder_1__ctor_m2403890235_gshared (ListBuilder_1_t3063240520 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	{
		__this->set__items_0((ObjectU5BU5D_t3614634134*)NULL);
		__this->set__item_1(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))));
		__this->set__count_2(0);
		int32_t L_0 = ___capacity0;
		__this->set__capacity_3(L_0);
		return;
	}
}
extern "C"  void ListBuilder_1__ctor_m2403890235_AdjustorThunk (Il2CppObject * __this, int32_t ___capacity0, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	ListBuilder_1__ctor_m2403890235(_thisAdjusted, ___capacity0, method);
}
// T System.RuntimeType/ListBuilder`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListBuilder_1_get_Item_m2628732860_gshared (ListBuilder_1_t3063240520 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * G_B3_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		G_B3_0 = L_4;
		goto IL_0022;
	}

IL_001c:
	{
		Il2CppObject * L_5 = (Il2CppObject *)__this->get__item_1();
		G_B3_0 = L_5;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Il2CppObject * ListBuilder_1_get_Item_m2628732860_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	return ListBuilder_1_get_Item_m2628732860(_thisAdjusted, ___index0, method);
}
// T[] System.RuntimeType/ListBuilder`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* ListBuilder_1_ToArray_m1877055377_gshared (ListBuilder_1_t3063240520 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ObjectU5BU5D_t3614634134* L_1 = ((EmptyArray_1_t3025629597_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Value_0();
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)__this->get__count_2();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)1));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__item_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		return L_3;
	}

IL_0031:
	{
		ObjectU5BU5D_t3614634134** L_5 = (ObjectU5BU5D_t3614634134**)__this->get_address_of__items_0();
		int32_t L_6 = (int32_t)__this->get__count_2();
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134**)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_7 = (int32_t)__this->get__count_2();
		__this->set__capacity_3(L_7);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		return L_8;
	}
}
extern "C"  ObjectU5BU5D_t3614634134* ListBuilder_1_ToArray_m1877055377_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	return ListBuilder_1_ToArray_m1877055377(_thisAdjusted, method);
}
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::CopyTo(System.Object[],System.Int32)
extern "C"  void ListBuilder_1_CopyTo_m1925739081_gshared (ListBuilder_1_t3063240520 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = (int32_t)__this->get__count_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = ___array0;
		int32_t L_3 = ___index1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__item_1();
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)L_4);
		return;
	}

IL_0027:
	{
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		ObjectU5BU5D_t3614634134* L_6 = ___array0;
		int32_t L_7 = ___index1;
		int32_t L_8 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)L_7, (int32_t)L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ListBuilder_1_CopyTo_m1925739081_AdjustorThunk (Il2CppObject * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	ListBuilder_1_CopyTo_m1925739081(_thisAdjusted, ___array0, ___index1, method);
}
// System.Int32 System.RuntimeType/ListBuilder`1<System.Object>::get_Count()
extern "C"  int32_t ListBuilder_1_get_Count_m2360264158_gshared (ListBuilder_1_t3063240520 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return L_0;
	}
}
extern "C"  int32_t ListBuilder_1_get_Count_m2360264158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	return ListBuilder_1_get_Count_m2360264158(_thisAdjusted, method);
}
// System.Void System.RuntimeType/ListBuilder`1<System.Object>::Add(T)
extern "C"  void ListBuilder_1_Add_m54035051_gshared (ListBuilder_1_t3063240520 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		__this->set__item_1(L_1);
		goto IL_009d;
	}

IL_0017:
	{
		int32_t L_2 = (int32_t)__this->get__count_2();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get__capacity_3();
		if ((((int32_t)L_3) >= ((int32_t)2)))
		{
			goto IL_0036;
		}
	}
	{
		__this->set__capacity_3(4);
	}

IL_0036:
	{
		int32_t L_4 = (int32_t)__this->get__capacity_3();
		__this->set__items_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_4)));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		Il2CppObject * L_6 = (Il2CppObject *)__this->get__item_1();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		goto IL_008b;
	}

IL_005e:
	{
		int32_t L_7 = (int32_t)__this->get__capacity_3();
		int32_t L_8 = (int32_t)__this->get__count_2();
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get__capacity_3();
		V_0 = (int32_t)((int32_t)((int32_t)2*(int32_t)L_9));
		ObjectU5BU5D_t3614634134** L_10 = (ObjectU5BU5D_t3614634134**)__this->get_address_of__items_0();
		int32_t L_11 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134**)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_12 = V_0;
		__this->set__capacity_3(L_12);
	}

IL_008b:
	{
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)__this->get__items_0();
		int32_t L_14 = (int32_t)__this->get__count_2();
		Il2CppObject * L_15 = ___item0;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
	}

IL_009d:
	{
		int32_t L_16 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_16+(int32_t)1)));
		return;
	}
}
extern "C"  void ListBuilder_1_Add_m54035051_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	ListBuilder_1_t3063240520 * _thisAdjusted = reinterpret_cast<ListBuilder_1_t3063240520 *>(__this + 1);
	ListBuilder_1_Add_m54035051(_thisAdjusted, ___item0, method);
}
// System.Void System.Threading.AsyncLocal`1<System.Object>::.ctor(System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<T>>)
extern "C"  void AsyncLocal_1__ctor_m17043560_gshared (AsyncLocal_1_t31129824 * __this, Action_1_t981956716 * ___valueChangedHandler0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t981956716 * L_0 = ___valueChangedHandler0;
		__this->set_m_valueChangedHandler_0(L_0);
		return;
	}
}
// System.Void System.Threading.AsyncLocal`1<System.Object>::set_Value(T)
extern "C"  void AsyncLocal_1_set_Value_m3424254923_gshared (AsyncLocal_1_t31129824 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncLocal_1_set_Value_m3424254923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		Action_1_t981956716 * L_1 = (Action_1_t981956716 *)__this->get_m_valueChangedHandler_0();
		IL2CPP_RUNTIME_CLASS_INIT(ExecutionContext_t1392266323_il2cpp_TypeInfo_var);
		ExecutionContext_SetLocalValue_m452313524(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_0, (bool)((((int32_t)((((Il2CppObject*)(Action_1_t981956716 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.AsyncLocal`1<System.Object>::System.Threading.IAsyncLocal.OnValueChanged(System.Object,System.Object,System.Boolean)
extern "C"  void AsyncLocal_1_System_Threading_IAsyncLocal_OnValueChanged_m341948749_gshared (AsyncLocal_1_t31129824 * __this, Il2CppObject * ___previousValueObj0, Il2CppObject * ___currentValueObj1, bool ___contextChanged2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncLocal_1_System_Threading_IAsyncLocal_OnValueChanged_m341948749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	{
		Il2CppObject * L_0 = ___previousValueObj0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		G_B3_0 = L_1;
		goto IL_001a;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___previousValueObj0;
		G_B3_0 = ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}

IL_001a:
	{
		V_0 = (Il2CppObject *)G_B3_0;
		Il2CppObject * L_3 = ___currentValueObj1;
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_4 = V_1;
		G_B6_0 = L_4;
		goto IL_0035;
	}

IL_002f:
	{
		Il2CppObject * L_5 = ___currentValueObj1;
		G_B6_0 = ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}

IL_0035:
	{
		V_2 = (Il2CppObject *)G_B6_0;
		Action_1_t981956716 * L_6 = (Action_1_t981956716 *)__this->get_m_valueChangedHandler_0();
		Il2CppObject * L_7 = V_0;
		Il2CppObject * L_8 = V_2;
		bool L_9 = ___contextChanged2;
		AsyncLocalValueChangedArgs_1_t1180157334  L_10;
		memset(&L_10, 0, sizeof(L_10));
		AsyncLocalValueChangedArgs_1__ctor_m1667703908(&L_10, (Il2CppObject *)L_7, (Il2CppObject *)L_8, (bool)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((Action_1_t981956716 *)L_6);
		((  void (*) (Action_1_t981956716 *, AsyncLocalValueChangedArgs_1_t1180157334 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Action_1_t981956716 *)L_6, (AsyncLocalValueChangedArgs_1_t1180157334 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::.ctor(T,T,System.Boolean)
extern "C"  void AsyncLocalValueChangedArgs_1__ctor_m1667703908_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * ___previousValue0, Il2CppObject * ___currentValue1, bool ___contextChanged2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncLocalValueChangedArgs_1__ctor_m1667703908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Initobj (AsyncLocalValueChangedArgs_1_t1180157334_il2cpp_TypeInfo_var, __this);
		Il2CppObject * L_0 = ___previousValue0;
		AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970((AsyncLocalValueChangedArgs_1_t1180157334 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___currentValue1;
		AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672((AsyncLocalValueChangedArgs_1_t1180157334 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_2 = ___contextChanged2;
		AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772((AsyncLocalValueChangedArgs_1_t1180157334 *)__this, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void AsyncLocalValueChangedArgs_1__ctor_m1667703908_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___previousValue0, Il2CppObject * ___currentValue1, bool ___contextChanged2, const MethodInfo* method)
{
	AsyncLocalValueChangedArgs_1_t1180157334 * _thisAdjusted = reinterpret_cast<AsyncLocalValueChangedArgs_1_t1180157334 *>(__this + 1);
	AsyncLocalValueChangedArgs_1__ctor_m1667703908(_thisAdjusted, ___previousValue0, ___currentValue1, ___contextChanged2, method);
}
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_PreviousValue(T)
extern "C"  void AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CPreviousValueU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	AsyncLocalValueChangedArgs_1_t1180157334 * _thisAdjusted = reinterpret_cast<AsyncLocalValueChangedArgs_1_t1180157334 *>(__this + 1);
	AsyncLocalValueChangedArgs_1_set_PreviousValue_m4097363970(_thisAdjusted, ___value0, method);
}
// T System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CCurrentValueU3Ek__BackingField_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AsyncLocalValueChangedArgs_1_t1180157334 * _thisAdjusted = reinterpret_cast<AsyncLocalValueChangedArgs_1_t1180157334 *>(__this + 1);
	return AsyncLocalValueChangedArgs_1_get_CurrentValue_m105549739(_thisAdjusted, method);
}
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_CurrentValue(T)
extern "C"  void AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CCurrentValueU3Ek__BackingField_1(L_0);
		return;
	}
}
extern "C"  void AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	AsyncLocalValueChangedArgs_1_t1180157334 * _thisAdjusted = reinterpret_cast<AsyncLocalValueChangedArgs_1_t1180157334 *>(__this + 1);
	AsyncLocalValueChangedArgs_1_set_CurrentValue_m2223160672(_thisAdjusted, ___value0, method);
}
// System.Void System.Threading.AsyncLocalValueChangedArgs`1<System.Object>::set_ThreadContextChanged(System.Boolean)
extern "C"  void AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772_gshared (AsyncLocalValueChangedArgs_1_t1180157334 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CThreadContextChangedU3Ek__BackingField_2(L_0);
		return;
	}
}
extern "C"  void AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	AsyncLocalValueChangedArgs_1_t1180157334 * _thisAdjusted = reinterpret_cast<AsyncLocalValueChangedArgs_1_t1180157334 *>(__this + 1);
	AsyncLocalValueChangedArgs_1_set_ThreadContextChanged_m3459436772(_thisAdjusted, ___value0, method);
}
// System.Void System.Threading.SparselyPopulatedArray`1<System.Object>::.ctor(System.Int32)
extern "C"  void SparselyPopulatedArray_1__ctor_m2366719019_gshared (SparselyPopulatedArray_1_t1092078389 * __this, int32_t ___initialSize0, const MethodInfo* method)
{
	SparselyPopulatedArrayFragment_1_t3870933437 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialSize0;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_1 = (SparselyPopulatedArrayFragment_1_t3870933437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (SparselyPopulatedArrayFragment_1_t3870933437 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		SparselyPopulatedArrayFragment_1_t3870933437 * L_2 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_1;
		V_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_2;
		il2cpp_codegen_memory_barrier();
		__this->set_m_tail_1(L_2);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_3 = V_0;
		__this->set_m_head_0(L_3);
		return;
	}
}
// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArray`1<System.Object>::get_Tail()
extern "C"  SparselyPopulatedArrayFragment_1_t3870933437 * SparselyPopulatedArray_1_get_Tail_m4282444654_gshared (SparselyPopulatedArray_1_t1092078389 * __this, const MethodInfo* method)
{
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)__this->get_m_tail_1();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// System.Threading.SparselyPopulatedArrayAddInfo`1<T> System.Threading.SparselyPopulatedArray`1<System.Object>::Add(T)
extern "C"  SparselyPopulatedArrayAddInfo_1_t810080232  SparselyPopulatedArray_1_Add_m2826913691_gshared (SparselyPopulatedArray_1_t1092078389 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	SparselyPopulatedArrayFragment_1_t3870933437 * V_0 = NULL;
	SparselyPopulatedArrayFragment_1_t3870933437 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	SparselyPopulatedArrayFragment_1_t3870933437 * V_7 = NULL;
	SparselyPopulatedArrayFragment_1_t3870933437 * G_B15_0 = NULL;
	SparselyPopulatedArrayFragment_1_t3870933437 * G_B14_0 = NULL;
	int32_t G_B16_0 = 0;
	SparselyPopulatedArrayFragment_1_t3870933437 * G_B16_1 = NULL;
	int32_t G_B24_0 = 0;

IL_0000:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)__this->get_m_tail_1();
		il2cpp_codegen_memory_barrier();
		V_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_0;
		goto IL_0020;
	}

IL_000e:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_1 = V_0;
		NullCheck(L_1);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_2 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_1->get_m_next_2();
		il2cpp_codegen_memory_barrier();
		SparselyPopulatedArrayFragment_1_t3870933437 * L_3 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_2;
		V_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_3;
		il2cpp_codegen_memory_barrier();
		__this->set_m_tail_1(L_3);
	}

IL_0020:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_4 = V_0;
		NullCheck(L_4);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_5 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_4->get_m_next_2();
		il2cpp_codegen_memory_barrier();
		if (L_5)
		{
			goto IL_000e;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_6 = V_0;
		V_1 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_6;
		goto IL_012e;
	}

IL_0034:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_8) >= ((int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_9 = V_1;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_10 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_9;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_10);
		il2cpp_codegen_memory_barrier();
		L_10->set_m_freeCount_1(((int32_t)((int32_t)L_11-(int32_t)1)));
	}

IL_0054:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)L_14->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_15) >= ((int32_t)((int32_t)-10))))
		{
			goto IL_0125;
		}
	}

IL_0071:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_16 = V_1;
		NullCheck((SparselyPopulatedArrayFragment_1_t3870933437 *)L_16);
		int32_t L_17 = ((  int32_t (*) (SparselyPopulatedArrayFragment_1_t3870933437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((SparselyPopulatedArrayFragment_1_t3870933437 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_2 = (int32_t)L_17;
		int32_t L_18 = V_2;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = (int32_t)L_19->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		int32_t L_21 = V_2;
		V_3 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18-(int32_t)L_20))%(int32_t)L_21));
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_00a0;
		}
	}
	{
		V_3 = (int32_t)0;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_23 = V_1;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_24 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_23;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)L_24->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_24);
		il2cpp_codegen_memory_barrier();
		L_24->set_m_freeCount_1(((int32_t)((int32_t)L_25-(int32_t)1)));
	}

IL_00a0:
	{
		V_4 = (int32_t)0;
		goto IL_011d;
	}

IL_00a8:
	{
		int32_t L_26 = V_3;
		int32_t L_27 = V_4;
		int32_t L_28 = V_2;
		V_5 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26+(int32_t)L_27))%(int32_t)L_28));
		SparselyPopulatedArrayFragment_1_t3870933437 * L_29 = V_1;
		NullCheck(L_29);
		ObjectU5BU5D_t3614634134* L_30 = (ObjectU5BU5D_t3614634134*)L_29->get_m_elements_0();
		int32_t L_31 = V_5;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		if (L_33)
		{
			goto IL_0117;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_34 = V_1;
		NullCheck(L_34);
		ObjectU5BU5D_t3614634134* L_35 = (ObjectU5BU5D_t3614634134*)L_34->get_m_elements_0();
		int32_t L_36 = V_5;
		NullCheck(L_35);
		Il2CppObject * L_37 = ___element0;
		Il2CppObject * L_38 = InterlockedCompareExchangeImpl<Il2CppObject *>((Il2CppObject **)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36))), (Il2CppObject *)L_37, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		if (L_38)
		{
			goto IL_0117;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_39 = V_1;
		NullCheck(L_39);
		int32_t L_40 = (int32_t)L_39->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		V_6 = (int32_t)((int32_t)((int32_t)L_40-(int32_t)1));
		SparselyPopulatedArrayFragment_1_t3870933437 * L_41 = V_1;
		int32_t L_42 = V_6;
		G_B14_0 = L_41;
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			G_B15_0 = L_41;
			goto IL_0106;
		}
	}
	{
		int32_t L_43 = V_6;
		G_B16_0 = L_43;
		G_B16_1 = G_B14_0;
		goto IL_0107;
	}

IL_0106:
	{
		G_B16_0 = 0;
		G_B16_1 = G_B15_0;
	}

IL_0107:
	{
		NullCheck(G_B16_1);
		il2cpp_codegen_memory_barrier();
		G_B16_1->set_m_freeCount_1(G_B16_0);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_44 = V_1;
		int32_t L_45 = V_5;
		SparselyPopulatedArrayAddInfo_1_t810080232  L_46;
		memset(&L_46, 0, sizeof(L_46));
		SparselyPopulatedArrayAddInfo_1__ctor_m1128745264(&L_46, (SparselyPopulatedArrayFragment_1_t3870933437 *)L_44, (int32_t)L_45, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_46;
	}

IL_0117:
	{
		int32_t L_47 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_011d:
	{
		int32_t L_48 = V_4;
		int32_t L_49 = V_2;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_00a8;
		}
	}

IL_0125:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_50 = V_1;
		NullCheck(L_50);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_51 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_50->get_m_prev_3();
		il2cpp_codegen_memory_barrier();
		V_1 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_51;
	}

IL_012e:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_52 = V_1;
		if (L_52)
		{
			goto IL_0034;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_53 = V_0;
		NullCheck(L_53);
		ObjectU5BU5D_t3614634134* L_54 = (ObjectU5BU5D_t3614634134*)L_53->get_m_elements_0();
		NullCheck(L_54);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length))))) == ((uint32_t)((int32_t)4096)))))
		{
			goto IL_0150;
		}
	}
	{
		G_B24_0 = ((int32_t)4096);
		goto IL_015a;
	}

IL_0150:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_55 = V_0;
		NullCheck(L_55);
		ObjectU5BU5D_t3614634134* L_56 = (ObjectU5BU5D_t3614634134*)L_55->get_m_elements_0();
		NullCheck(L_56);
		G_B24_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length))))*(int32_t)2));
	}

IL_015a:
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_57 = V_0;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_58 = (SparselyPopulatedArrayFragment_1_t3870933437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (SparselyPopulatedArrayFragment_1_t3870933437 *, int32_t, SparselyPopulatedArrayFragment_1_t3870933437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(L_58, (int32_t)G_B24_0, (SparselyPopulatedArrayFragment_1_t3870933437 *)L_57, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_7 = (SparselyPopulatedArrayFragment_1_t3870933437 *)L_58;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_59 = V_0;
		NullCheck(L_59);
		SparselyPopulatedArrayFragment_1_t3870933437 ** L_60 = (SparselyPopulatedArrayFragment_1_t3870933437 **)L_59->get_address_of_m_next_2();
		il2cpp_codegen_memory_barrier();
		SparselyPopulatedArrayFragment_1_t3870933437 * L_61 = V_7;
		SparselyPopulatedArrayFragment_1_t3870933437 * L_62 = InterlockedCompareExchangeImpl<SparselyPopulatedArrayFragment_1_t3870933437 *>((SparselyPopulatedArrayFragment_1_t3870933437 **)L_60, (SparselyPopulatedArrayFragment_1_t3870933437 *)L_61, (SparselyPopulatedArrayFragment_1_t3870933437 *)NULL);
		if (L_62)
		{
			goto IL_017f;
		}
	}
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_63 = V_7;
		il2cpp_codegen_memory_barrier();
		__this->set_m_tail_1(L_63);
	}

IL_017f:
	{
		goto IL_0000;
	}
}
// System.Void System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::.ctor(System.Threading.SparselyPopulatedArrayFragment`1<T>,System.Int32)
extern "C"  void SparselyPopulatedArrayAddInfo_1__ctor_m1128745264_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, SparselyPopulatedArrayFragment_1_t3870933437 * ___source0, int32_t ___index1, const MethodInfo* method)
{
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_0 = ___source0;
		__this->set_m_source_0(L_0);
		int32_t L_1 = ___index1;
		__this->set_m_index_1(L_1);
		return;
	}
}
extern "C"  void SparselyPopulatedArrayAddInfo_1__ctor_m1128745264_AdjustorThunk (Il2CppObject * __this, SparselyPopulatedArrayFragment_1_t3870933437 * ___source0, int32_t ___index1, const MethodInfo* method)
{
	SparselyPopulatedArrayAddInfo_1_t810080232 * _thisAdjusted = reinterpret_cast<SparselyPopulatedArrayAddInfo_1_t810080232 *>(__this + 1);
	SparselyPopulatedArrayAddInfo_1__ctor_m1128745264(_thisAdjusted, ___source0, ___index1, method);
}
// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Source()
extern "C"  SparselyPopulatedArrayFragment_1_t3870933437 * SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, const MethodInfo* method)
{
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)__this->get_m_source_0();
		return L_0;
	}
}
extern "C"  SparselyPopulatedArrayFragment_1_t3870933437 * SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SparselyPopulatedArrayAddInfo_1_t810080232 * _thisAdjusted = reinterpret_cast<SparselyPopulatedArrayAddInfo_1_t810080232 *>(__this + 1);
	return SparselyPopulatedArrayAddInfo_1_get_Source_m3275823194(_thisAdjusted, method);
}
// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1<System.Object>::get_Index()
extern "C"  int32_t SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712_gshared (SparselyPopulatedArrayAddInfo_1_t810080232 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_index_1();
		return L_0;
	}
}
extern "C"  int32_t SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SparselyPopulatedArrayAddInfo_1_t810080232 * _thisAdjusted = reinterpret_cast<SparselyPopulatedArrayAddInfo_1_t810080232 *>(__this + 1);
	return SparselyPopulatedArrayAddInfo_1_get_Index_m2079572712(_thisAdjusted, method);
}
// System.Void System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::.ctor(System.Int32)
extern "C"  void SparselyPopulatedArrayFragment_1__ctor_m875100827_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, int32_t ___size0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___size0;
		NullCheck((SparselyPopulatedArrayFragment_1_t3870933437 *)__this);
		((  void (*) (SparselyPopulatedArrayFragment_1_t3870933437 *, int32_t, SparselyPopulatedArrayFragment_1_t3870933437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SparselyPopulatedArrayFragment_1_t3870933437 *)__this, (int32_t)L_0, (SparselyPopulatedArrayFragment_1_t3870933437 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::.ctor(System.Int32,System.Threading.SparselyPopulatedArrayFragment`1<T>)
extern "C"  void SparselyPopulatedArrayFragment_1__ctor_m3720000803_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, int32_t ___size0, SparselyPopulatedArrayFragment_1_t3870933437 * ___prev1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		__this->set_m_elements_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		il2cpp_codegen_memory_barrier();
		__this->set_m_freeCount_1(L_1);
		SparselyPopulatedArrayFragment_1_t3870933437 * L_2 = ___prev1;
		il2cpp_codegen_memory_barrier();
		__this->set_m_prev_3(L_2);
		return;
	}
}
// T System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * SparselyPopulatedArrayFragment_1_get_Item_m2316076924_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_elements_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject **)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_2;
	}
}
// System.Int32 System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::get_Length()
extern "C"  int32_t SparselyPopulatedArrayFragment_1_get_Length_m391571445_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_elements_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::get_Prev()
extern "C"  SparselyPopulatedArrayFragment_1_t3870933437 * SparselyPopulatedArrayFragment_1_get_Prev_m115711905_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, const MethodInfo* method)
{
	{
		SparselyPopulatedArrayFragment_1_t3870933437 * L_0 = (SparselyPopulatedArrayFragment_1_t3870933437 *)__this->get_m_prev_3();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// T System.Threading.SparselyPopulatedArrayFragment`1<System.Object>::SafeAtomicRemove(System.Int32,T)
extern "C"  Il2CppObject * SparselyPopulatedArrayFragment_1_SafeAtomicRemove_m1336043978_gshared (SparselyPopulatedArrayFragment_1_t3870933437 * __this, int32_t ___index0, Il2CppObject * ___expectedElement1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_elements_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = ___expectedElement1;
		Il2CppObject * L_3 = InterlockedCompareExchangeImpl<Il2CppObject *>((Il2CppObject **)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1))), (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)L_2);
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = (int32_t)__this->get_m_freeCount_1();
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		__this->set_m_freeCount_1(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_0036:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void System.Threading.Tasks.Shared`1<System.Object>::.ctor(T)
extern "C"  void Shared_1__ctor_m684043315_gshared (Shared_1_t3839187714 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value0;
		__this->set_Value_0(L_0);
		return;
	}
}
// System.Void System.Threading.Tasks.Shared`1<System.Threading.CancellationTokenRegistration>::.ctor(T)
extern "C"  void Shared_1__ctor_m3983423627_gshared (Shared_1_t2858597776 * __this, CancellationTokenRegistration_t1708859357  ___value0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CancellationTokenRegistration_t1708859357  L_0 = ___value0;
		__this->set_Value_0(L_0);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor()
extern "C"  void Task_1__ctor_m314248096_gshared (Task_1_t2945603725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m314248096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m3731960308((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  void Task_1__ctor_m2571213091_gshared (Task_1_t2945603725 * __this, Il2CppObject * ___state0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2571213091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___state0;
		int32_t L_1 = ___options1;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m922749962((Task_t1843236107 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(TResult)
extern "C"  void Task_1__ctor_m4222636859_gshared (Task_1_t2945603725 * __this, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m4222636859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)0, (int32_t)0, (CancellationToken_t1851405782 )L_0, /*hidden argument*/NULL);
		bool L_1 = ___result0;
		__this->set_m_result_24(L_1);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Boolean,TResult,System.Threading.Tasks.TaskCreationOptions,System.Threading.CancellationToken)
extern "C"  void Task_1__ctor_m1487818250_gshared (Task_1_t2945603725 * __this, bool ___canceled0, bool ___result1, int32_t ___creationOptions2, CancellationToken_t1851405782  ___ct3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1487818250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___canceled0;
		int32_t L_1 = ___creationOptions2;
		CancellationToken_t1851405782  L_2 = ___ct3;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)L_0, (int32_t)L_1, (CancellationToken_t1851405782 )L_2, /*hidden argument*/NULL);
		bool L_3 = ___canceled0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		bool L_4 = ___result1;
		__this->set_m_result_24(L_4);
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Func`2<System.Object,TResult>,System.Object,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions)
extern "C" IL2CPP_NO_INLINE void Task_1__ctor_m4001507066_gshared (Task_1_t2945603725 * __this, Func_2_t3961629604 * ___function0, Il2CppObject * ___state1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m4001507066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Func_2_t3961629604 * L_0 = ___function0;
		Il2CppObject * L_1 = ___state1;
		int32_t L_2 = ___creationOptions3;
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_t1843236107 * L_3 = Task_InternalCurrentIfAttached_m1086187635(NULL /*static, unused*/, (int32_t)L_2, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_4 = ___cancellationToken2;
		int32_t L_5 = ___creationOptions3;
		NullCheck((Task_1_t2945603725 *)__this);
		((  void (*) (Task_1_t2945603725 *, Delegate_t3022476291 *, Il2CppObject *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t2945603725 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_3, (CancellationToken_t1851405782 )L_4, (int32_t)L_5, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (int32_t)1;
		NullCheck((Task_t1843236107 *)__this);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  void Task_1__ctor_m4131560672_gshared (Task_1_t2945603725 * __this, Func_1_t1485000104 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m4131560672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t1485000104 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_1_t2945603725 *)__this);
		((  void (*) (Task_1_t2945603725 *, Func_1_t1485000104 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Task_1_t2945603725 *)__this, (Func_1_t1485000104 *)L_0, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t* L_6 = ___stackMark6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m3631457423_gshared (Task_1_t2945603725 * __this, Func_1_t1485000104 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3631457423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t1485000104 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)NULL, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_6&(int32_t)((int32_t)2048))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_7 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, (String_t*)_stringLiteral1588292733, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.ctor(System.Delegate,System.Object,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m2769197254_gshared (Task_1_t2945603725 * __this, Delegate_t3022476291 * ___valueSelector0, Il2CppObject * ___state1, Task_t1843236107 * ___parent2, CancellationToken_t1851405782  ___cancellationToken3, int32_t ___creationOptions4, int32_t ___internalOptions5, TaskScheduler_t3932792796 * ___scheduler6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2769197254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Delegate_t3022476291 * L_0 = ___valueSelector0;
		Il2CppObject * L_1 = ___state1;
		Task_t1843236107 * L_2 = ___parent2;
		CancellationToken_t1851405782  L_3 = ___cancellationToken3;
		int32_t L_4 = ___creationOptions4;
		int32_t L_5 = ___internalOptions5;
		TaskScheduler_t3932792796 * L_6 = ___scheduler6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_2, (CancellationToken_t1851405782 )L_3, (int32_t)L_4, (int32_t)L_5, (TaskScheduler_t3932792796 *)L_6, /*hidden argument*/NULL);
		int32_t L_7 = ___internalOptions5;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)2048))))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_8 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, (String_t*)_stringLiteral1588292733, (String_t*)L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0033:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Boolean>::StartNew(System.Threading.Tasks.Task,System.Func`1<TResult>,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  Task_1_t2945603725 * Task_1_StartNew_m2115868853_gshared (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___parent0, Func_1_t1485000104 * ___function1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_StartNew_m2115868853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t2945603725 * V_0 = NULL;
	{
		Func_1_t1485000104 * L_0 = ___function1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral878805882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		TaskScheduler_t3932792796 * L_2 = ___scheduler5;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3323468611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)2048))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_5 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_6 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_6, (String_t*)_stringLiteral1588292733, (String_t*)L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0045:
	{
		Func_1_t1485000104 * L_7 = ___function1;
		Task_t1843236107 * L_8 = ___parent0;
		CancellationToken_t1851405782  L_9 = ___cancellationToken2;
		int32_t L_10 = ___creationOptions3;
		int32_t L_11 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_12 = ___scheduler5;
		int32_t* L_13 = ___stackMark6;
		Task_1_t2945603725 * L_14 = (Task_1_t2945603725 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Task_1_t2945603725 *, Func_1_t1485000104 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_14, (Func_1_t1485000104 *)L_7, (Task_t1843236107 *)L_8, (CancellationToken_t1851405782 )L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11|(int32_t)((int32_t)8192))), (TaskScheduler_t3932792796 *)L_12, (int32_t*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (Task_1_t2945603725 *)L_14;
		Task_1_t2945603725 * L_15 = V_0;
		NullCheck((Task_t1843236107 *)L_15);
		Task_ScheduleAndStart_m2476171743((Task_t1843236107 *)L_15, (bool)0, /*hidden argument*/NULL);
		Task_1_t2945603725 * L_16 = V_0;
		return L_16;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Boolean>::TrySetResult(TResult)
extern "C"  bool Task_1_TrySetResult_m1373840627_gshared (Task_1_t2945603725 * __this, bool ___result0, const MethodInfo* method)
{
	ContingentProperties_t606988207 * V_0 = NULL;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_1 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		bool L_2 = ___result0;
		__this->set_m_result_24(L_2);
		int32_t* L_3 = (int32_t*)((Task_t1843236107 *)__this)->get_address_of_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		Interlocked_Exchange_m4103465028(NULL /*static, unused*/, (int32_t*)L_3, (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))), /*hidden argument*/NULL);
		ContingentProperties_t606988207 * L_5 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)__this)->get_m_contingentProperties_15();
		il2cpp_codegen_memory_barrier();
		V_0 = (ContingentProperties_t606988207 *)L_5;
		ContingentProperties_t606988207 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ContingentProperties_t606988207 * L_7 = V_0;
		NullCheck((ContingentProperties_t606988207 *)L_7);
		ContingentProperties_SetCompleted_m2487364104((ContingentProperties_t606988207 *)L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_FinishStageThree_m4010028243((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0060:
	{
		return (bool)0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::DangerousSetResult(TResult)
extern "C"  void Task_1_DangerousSetResult_m3322214992_gshared (Task_1_t2945603725 * __this, bool ___result0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Task_t1843236107 * L_0 = (Task_t1843236107 *)((Task_t1843236107 *)__this)->get_m_parent_8();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = ___result0;
		NullCheck((Task_1_t2945603725 *)__this);
		bool L_2 = ((  bool (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Task_1_t2945603725 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (bool)L_2;
		goto IL_0035;
	}

IL_0018:
	{
		bool L_3 = ___result0;
		__this->set_m_result_24(L_3);
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		((Task_t1843236107 *)__this)->set_m_stateFlags_9(((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))));
	}

IL_0035:
	{
		return;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Boolean>::get_Result()
extern "C"  bool Task_1_get_Result_m3103676244_gshared (Task_1_t2945603725 * __this, const MethodInfo* method)
{
	bool G_B3_0 = false;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsWaitNotificationEnabledOrNotRanToCompletion_m2720866512((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((Task_1_t2945603725 *)__this);
		bool L_1 = ((  bool (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Task_1_t2945603725 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0017:
	{
		bool L_2 = (bool)__this->get_m_result_24();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Boolean>::get_ResultOnSuccess()
extern "C"  bool Task_1_get_ResultOnSuccess_m3635745454_gshared (Task_1_t2945603725 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_m_result_24();
		return L_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Boolean>::GetResultCore(System.Boolean)
extern "C"  bool Task_1_GetResultCore_m410448687_gshared (Task_1_t2945603725 * __this, bool ___waitCompletionNotification0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_GetResultCore_m410448687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_1 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		Task_InternalWait_m945475364((Task_t1843236107 *)__this, (int32_t)(-1), (CancellationToken_t1851405782 )L_1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_2 = ___waitCompletionNotification0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_NotifyDebuggerOfWaitCompletionIfNecessary_m4037125766((Task_t1843236107 *)__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_3 = Task_get_IsRanToCompletion_m3601607431((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_ThrowIfExceptional_m2165180494((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		bool L_4 = (bool)__this->get_m_result_24();
		return L_4;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Boolean>::TrySetException(System.Object)
extern "C"  bool Task_1_TrySetException_m248865808_gshared (Task_1_t2945603725 * __this, Il2CppObject * ___exceptionObject0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		Task_EnsureContingentPropertiesInitialized_m3371331877((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___exceptionObject0;
		NullCheck((Task_t1843236107 *)__this);
		Task_AddException_m3383991754((Task_t1843236107 *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_Finish_m2698939980((Task_t1843236107 *)__this, (bool)0, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_002f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Boolean>::TrySetCanceled(System.Threading.CancellationToken)
extern "C"  bool Task_1_TrySetCanceled_m307174891_gshared (Task_1_t2945603725 * __this, CancellationToken_t1851405782  ___tokenToRecord0, const MethodInfo* method)
{
	{
		CancellationToken_t1851405782  L_0 = ___tokenToRecord0;
		NullCheck((Task_1_t2945603725 *)__this);
		bool L_1 = ((  bool (*) (Task_1_t2945603725 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Task_1_t2945603725 *)__this, (CancellationToken_t1851405782 )L_0, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Boolean>::TrySetCanceled(System.Threading.CancellationToken,System.Object)
extern "C"  bool Task_1_TrySetCanceled_m1998444381_gshared (Task_1_t2945603725 * __this, CancellationToken_t1851405782  ___tokenToRecord0, Il2CppObject * ___cancellationException1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		CancellationToken_t1851405782  L_1 = ___tokenToRecord0;
		Il2CppObject * L_2 = ___cancellationException1;
		NullCheck((Task_t1843236107 *)__this);
		Task_RecordInternalCancellationRequest_m3918176011((Task_t1843236107 *)__this, (CancellationToken_t1851405782 )L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_CancellationCleanupLogic_m2712825363((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0027:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1<System.Boolean>::get_Factory()
extern "C"  TaskFactory_1_t220925529 * Task_1_get_Factory_m1877348428_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TaskFactory_1_t220925529 * L_0 = ((Task_1_t2945603725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->get_s_Factory_25();
		return L_0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::InnerInvoke()
extern "C"  void Task_1_InnerInvoke_m1084170280_gshared (Task_1_t2945603725 * __this, const MethodInfo* method)
{
	Func_1_t1485000104 * V_0 = NULL;
	Func_2_t3961629604 * V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_0 = (Func_1_t1485000104 *)((Func_1_t1485000104 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8)));
		Func_1_t1485000104 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Func_1_t1485000104 * L_2 = V_0;
		NullCheck((Func_1_t1485000104 *)L_2);
		bool L_3 = ((  bool (*) (Func_1_t1485000104 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_1_t1485000104 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_m_result_24(L_3);
		return;
	}

IL_001f:
	{
		Il2CppObject * L_4 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_1 = (Func_2_t3961629604 *)((Func_2_t3961629604 *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)));
		Func_2_t3961629604 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		Func_2_t3961629604 * L_6 = V_1;
		Il2CppObject * L_7 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_stateObject_6();
		NullCheck((Func_2_t3961629604 *)L_6);
		bool L_8 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Func_2_t3961629604 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		__this->set_m_result_24(L_8);
		return;
	}

IL_0044:
	{
		return;
	}
}
// System.Runtime.CompilerServices.TaskAwaiter`1<TResult> System.Threading.Tasks.Task`1<System.Boolean>::GetAwaiter()
extern "C"  TaskAwaiter_1_t899200082  Task_1_GetAwaiter_m1503534718_gshared (Task_1_t2945603725 * __this, const MethodInfo* method)
{
	{
		TaskAwaiter_1_t899200082  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TaskAwaiter_1__ctor_m3987458582(&L_0, (Task_1_t2945603725 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<TResult> System.Threading.Tasks.Task`1<System.Boolean>::ConfigureAwait(System.Boolean)
extern "C"  ConfiguredTaskAwaitable_1_t34161435  Task_1_ConfigureAwait_m3843117537_gshared (Task_1_t2945603725 * __this, bool ___continueOnCapturedContext0, const MethodInfo* method)
{
	{
		bool L_0 = ___continueOnCapturedContext0;
		ConfiguredTaskAwaitable_1_t34161435  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ConfiguredTaskAwaitable_1__ctor_m1905790602(&L_1, (Task_1_t2945603725 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Boolean>::.cctor()
extern "C"  void Task_1__cctor_m3006750161_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		TaskFactory_1_t220925529 * L_0 = (TaskFactory_1_t220925529 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((  void (*) (TaskFactory_1_t220925529 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((Task_1_t2945603725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_s_Factory_25(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		Func_2_t3758303068 * L_2 = (Func_2_t3758303068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((  void (*) (Func_2_t3758303068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Task_1_t2945603725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_TaskWhenAnyCast_26(L_2);
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Boolean>::<TaskWhenAnyCast>m__0(System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>)
extern "C"  Task_1_t2945603725 * Task_1_U3CTaskWhenAnyCastU3Em__0_m3063195731_gshared (Il2CppObject * __this /* static, unused */, Task_1_t963265114 * ___completed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_U3CTaskWhenAnyCastU3Em__0_m3063195731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Task_1_t963265114 * L_0 = ___completed0;
		NullCheck((Task_1_t963265114 *)L_0);
		Task_t1843236107 * L_1 = Task_1_get_Result_m1316373833((Task_1_t963265114 *)L_0, /*hidden argument*/Task_1_get_Result_m1316373833_MethodInfo_var);
		return ((Task_1_t2945603725 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor()
extern "C"  void Task_1__ctor_m172046308_gshared (Task_1_t1191906455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m172046308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m3731960308((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  void Task_1__ctor_m1030422093_gshared (Task_1_t1191906455 * __this, Il2CppObject * ___state0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1030422093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___state0;
		int32_t L_1 = ___options1;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m922749962((Task_t1843236107 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(TResult)
extern "C"  void Task_1__ctor_m2344439049_gshared (Task_1_t1191906455 * __this, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2344439049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)0, (int32_t)0, (CancellationToken_t1851405782 )L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___result0;
		__this->set_m_result_24(L_1);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Boolean,TResult,System.Threading.Tasks.TaskCreationOptions,System.Threading.CancellationToken)
extern "C"  void Task_1__ctor_m2745384578_gshared (Task_1_t1191906455 * __this, bool ___canceled0, int32_t ___result1, int32_t ___creationOptions2, CancellationToken_t1851405782  ___ct3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2745384578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___canceled0;
		int32_t L_1 = ___creationOptions2;
		CancellationToken_t1851405782  L_2 = ___ct3;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)L_0, (int32_t)L_1, (CancellationToken_t1851405782 )L_2, /*hidden argument*/NULL);
		bool L_3 = ___canceled0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_4 = ___result1;
		__this->set_m_result_24(L_4);
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Func`2<System.Object,TResult>,System.Object,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions)
extern "C" IL2CPP_NO_INLINE void Task_1__ctor_m298167690_gshared (Task_1_t1191906455 * __this, Func_2_t2207932334 * ___function0, Il2CppObject * ___state1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m298167690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Func_2_t2207932334 * L_0 = ___function0;
		Il2CppObject * L_1 = ___state1;
		int32_t L_2 = ___creationOptions3;
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_t1843236107 * L_3 = Task_InternalCurrentIfAttached_m1086187635(NULL /*static, unused*/, (int32_t)L_2, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_4 = ___cancellationToken2;
		int32_t L_5 = ___creationOptions3;
		NullCheck((Task_1_t1191906455 *)__this);
		((  void (*) (Task_1_t1191906455 *, Delegate_t3022476291 *, Il2CppObject *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t1191906455 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_3, (CancellationToken_t1851405782 )L_4, (int32_t)L_5, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (int32_t)1;
		NullCheck((Task_t1843236107 *)__this);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  void Task_1__ctor_m1777040968_gshared (Task_1_t1191906455 * __this, Func_1_t4026270130 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1777040968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t4026270130 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_1_t1191906455 *)__this);
		((  void (*) (Task_1_t1191906455 *, Func_1_t4026270130 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Task_1_t1191906455 *)__this, (Func_1_t4026270130 *)L_0, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t* L_6 = ___stackMark6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m2408210937_gshared (Task_1_t1191906455 * __this, Func_1_t4026270130 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2408210937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t4026270130 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)NULL, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_6&(int32_t)((int32_t)2048))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_7 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, (String_t*)_stringLiteral1588292733, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.ctor(System.Delegate,System.Object,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m968257538_gshared (Task_1_t1191906455 * __this, Delegate_t3022476291 * ___valueSelector0, Il2CppObject * ___state1, Task_t1843236107 * ___parent2, CancellationToken_t1851405782  ___cancellationToken3, int32_t ___creationOptions4, int32_t ___internalOptions5, TaskScheduler_t3932792796 * ___scheduler6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m968257538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Delegate_t3022476291 * L_0 = ___valueSelector0;
		Il2CppObject * L_1 = ___state1;
		Task_t1843236107 * L_2 = ___parent2;
		CancellationToken_t1851405782  L_3 = ___cancellationToken3;
		int32_t L_4 = ___creationOptions4;
		int32_t L_5 = ___internalOptions5;
		TaskScheduler_t3932792796 * L_6 = ___scheduler6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_2, (CancellationToken_t1851405782 )L_3, (int32_t)L_4, (int32_t)L_5, (TaskScheduler_t3932792796 *)L_6, /*hidden argument*/NULL);
		int32_t L_7 = ___internalOptions5;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)2048))))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_8 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, (String_t*)_stringLiteral1588292733, (String_t*)L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0033:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Int32>::StartNew(System.Threading.Tasks.Task,System.Func`1<TResult>,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  Task_1_t1191906455 * Task_1_StartNew_m870754347_gshared (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___parent0, Func_1_t4026270130 * ___function1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_StartNew_m870754347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t1191906455 * V_0 = NULL;
	{
		Func_1_t4026270130 * L_0 = ___function1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral878805882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		TaskScheduler_t3932792796 * L_2 = ___scheduler5;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3323468611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)2048))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_5 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_6 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_6, (String_t*)_stringLiteral1588292733, (String_t*)L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0045:
	{
		Func_1_t4026270130 * L_7 = ___function1;
		Task_t1843236107 * L_8 = ___parent0;
		CancellationToken_t1851405782  L_9 = ___cancellationToken2;
		int32_t L_10 = ___creationOptions3;
		int32_t L_11 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_12 = ___scheduler5;
		int32_t* L_13 = ___stackMark6;
		Task_1_t1191906455 * L_14 = (Task_1_t1191906455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Task_1_t1191906455 *, Func_1_t4026270130 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_14, (Func_1_t4026270130 *)L_7, (Task_t1843236107 *)L_8, (CancellationToken_t1851405782 )L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11|(int32_t)((int32_t)8192))), (TaskScheduler_t3932792796 *)L_12, (int32_t*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (Task_1_t1191906455 *)L_14;
		Task_1_t1191906455 * L_15 = V_0;
		NullCheck((Task_t1843236107 *)L_15);
		Task_ScheduleAndStart_m2476171743((Task_t1843236107 *)L_15, (bool)0, /*hidden argument*/NULL);
		Task_1_t1191906455 * L_16 = V_0;
		return L_16;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Int32>::TrySetResult(TResult)
extern "C"  bool Task_1_TrySetResult_m1453779105_gshared (Task_1_t1191906455 * __this, int32_t ___result0, const MethodInfo* method)
{
	ContingentProperties_t606988207 * V_0 = NULL;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_1 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_2 = ___result0;
		__this->set_m_result_24(L_2);
		int32_t* L_3 = (int32_t*)((Task_t1843236107 *)__this)->get_address_of_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		Interlocked_Exchange_m4103465028(NULL /*static, unused*/, (int32_t*)L_3, (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))), /*hidden argument*/NULL);
		ContingentProperties_t606988207 * L_5 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)__this)->get_m_contingentProperties_15();
		il2cpp_codegen_memory_barrier();
		V_0 = (ContingentProperties_t606988207 *)L_5;
		ContingentProperties_t606988207 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ContingentProperties_t606988207 * L_7 = V_0;
		NullCheck((ContingentProperties_t606988207 *)L_7);
		ContingentProperties_SetCompleted_m2487364104((ContingentProperties_t606988207 *)L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_FinishStageThree_m4010028243((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0060:
	{
		return (bool)0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::DangerousSetResult(TResult)
extern "C"  void Task_1_DangerousSetResult_m2436546400_gshared (Task_1_t1191906455 * __this, int32_t ___result0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Task_t1843236107 * L_0 = (Task_t1843236107 *)((Task_t1843236107 *)__this)->get_m_parent_8();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___result0;
		NullCheck((Task_1_t1191906455 *)__this);
		bool L_2 = ((  bool (*) (Task_1_t1191906455 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Task_1_t1191906455 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (bool)L_2;
		goto IL_0035;
	}

IL_0018:
	{
		int32_t L_3 = ___result0;
		__this->set_m_result_24(L_3);
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		((Task_t1843236107 *)__this)->set_m_stateFlags_9(((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))));
	}

IL_0035:
	{
		return;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Int32>::get_Result()
extern "C"  int32_t Task_1_get_Result_m53117464_gshared (Task_1_t1191906455 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsWaitNotificationEnabledOrNotRanToCompletion_m2720866512((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((Task_1_t1191906455 *)__this);
		int32_t L_1 = ((  int32_t (*) (Task_1_t1191906455 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Task_1_t1191906455 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0017:
	{
		int32_t L_2 = (int32_t)__this->get_m_result_24();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Int32>::get_ResultOnSuccess()
extern "C"  int32_t Task_1_get_ResultOnSuccess_m1982941662_gshared (Task_1_t1191906455 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_result_24();
		return L_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Int32>::GetResultCore(System.Boolean)
extern "C"  int32_t Task_1_GetResultCore_m88725665_gshared (Task_1_t1191906455 * __this, bool ___waitCompletionNotification0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_GetResultCore_m88725665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_1 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		Task_InternalWait_m945475364((Task_t1843236107 *)__this, (int32_t)(-1), (CancellationToken_t1851405782 )L_1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_2 = ___waitCompletionNotification0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_NotifyDebuggerOfWaitCompletionIfNecessary_m4037125766((Task_t1843236107 *)__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_3 = Task_get_IsRanToCompletion_m3601607431((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_ThrowIfExceptional_m2165180494((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_4 = (int32_t)__this->get_m_result_24();
		return L_4;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Int32>::TrySetException(System.Object)
extern "C"  bool Task_1_TrySetException_m735967204_gshared (Task_1_t1191906455 * __this, Il2CppObject * ___exceptionObject0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		Task_EnsureContingentPropertiesInitialized_m3371331877((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___exceptionObject0;
		NullCheck((Task_t1843236107 *)__this);
		Task_AddException_m3383991754((Task_t1843236107 *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_Finish_m2698939980((Task_t1843236107 *)__this, (bool)0, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_002f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Int32>::TrySetCanceled(System.Threading.CancellationToken)
extern "C"  bool Task_1_TrySetCanceled_m3305819613_gshared (Task_1_t1191906455 * __this, CancellationToken_t1851405782  ___tokenToRecord0, const MethodInfo* method)
{
	{
		CancellationToken_t1851405782  L_0 = ___tokenToRecord0;
		NullCheck((Task_1_t1191906455 *)__this);
		bool L_1 = ((  bool (*) (Task_1_t1191906455 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Task_1_t1191906455 *)__this, (CancellationToken_t1851405782 )L_0, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Int32>::TrySetCanceled(System.Threading.CancellationToken,System.Object)
extern "C"  bool Task_1_TrySetCanceled_m259876683_gshared (Task_1_t1191906455 * __this, CancellationToken_t1851405782  ___tokenToRecord0, Il2CppObject * ___cancellationException1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		CancellationToken_t1851405782  L_1 = ___tokenToRecord0;
		Il2CppObject * L_2 = ___cancellationException1;
		NullCheck((Task_t1843236107 *)__this);
		Task_RecordInternalCancellationRequest_m3918176011((Task_t1843236107 *)__this, (CancellationToken_t1851405782 )L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_CancellationCleanupLogic_m2712825363((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0027:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1<System.Int32>::get_Factory()
extern "C"  TaskFactory_1_t2762195555 * Task_1_get_Factory_m2174754308_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TaskFactory_1_t2762195555 * L_0 = ((Task_1_t1191906455_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->get_s_Factory_25();
		return L_0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::InnerInvoke()
extern "C"  void Task_1_InnerInvoke_m3119510844_gshared (Task_1_t1191906455 * __this, const MethodInfo* method)
{
	Func_1_t4026270130 * V_0 = NULL;
	Func_2_t2207932334 * V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_0 = (Func_1_t4026270130 *)((Func_1_t4026270130 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8)));
		Func_1_t4026270130 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Func_1_t4026270130 * L_2 = V_0;
		NullCheck((Func_1_t4026270130 *)L_2);
		int32_t L_3 = ((  int32_t (*) (Func_1_t4026270130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_1_t4026270130 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_m_result_24(L_3);
		return;
	}

IL_001f:
	{
		Il2CppObject * L_4 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_1 = (Func_2_t2207932334 *)((Func_2_t2207932334 *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)));
		Func_2_t2207932334 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		Func_2_t2207932334 * L_6 = V_1;
		Il2CppObject * L_7 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_stateObject_6();
		NullCheck((Func_2_t2207932334 *)L_6);
		int32_t L_8 = ((  int32_t (*) (Func_2_t2207932334 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Func_2_t2207932334 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		__this->set_m_result_24(L_8);
		return;
	}

IL_0044:
	{
		return;
	}
}
// System.Runtime.CompilerServices.TaskAwaiter`1<TResult> System.Threading.Tasks.Task`1<System.Int32>::GetAwaiter()
extern "C"  TaskAwaiter_1_t3440470108  Task_1_GetAwaiter_m1859403882_gshared (Task_1_t1191906455 * __this, const MethodInfo* method)
{
	{
		TaskAwaiter_1_t3440470108  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TaskAwaiter_1__ctor_m3485680026(&L_0, (Task_1_t1191906455 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<TResult> System.Threading.Tasks.Task`1<System.Int32>::ConfigureAwait(System.Boolean)
extern "C"  ConfiguredTaskAwaitable_1_t2575431461  Task_1_ConfigureAwait_m1543112255_gshared (Task_1_t1191906455 * __this, bool ___continueOnCapturedContext0, const MethodInfo* method)
{
	{
		bool L_0 = ___continueOnCapturedContext0;
		ConfiguredTaskAwaitable_1_t2575431461  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ConfiguredTaskAwaitable_1__ctor_m2687722690(&L_1, (Task_1_t1191906455 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Int32>::.cctor()
extern "C"  void Task_1__cctor_m3789805399_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		TaskFactory_1_t2762195555 * L_0 = (TaskFactory_1_t2762195555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((  void (*) (TaskFactory_1_t2762195555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((Task_1_t1191906455_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_s_Factory_25(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		Func_2_t2004605798 * L_2 = (Func_2_t2004605798 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((  void (*) (Func_2_t2004605798 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Task_1_t1191906455_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_TaskWhenAnyCast_26(L_2);
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Int32>::<TaskWhenAnyCast>m__0(System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>)
extern "C"  Task_1_t1191906455 * Task_1_U3CTaskWhenAnyCastU3Em__0_m4259118645_gshared (Il2CppObject * __this /* static, unused */, Task_1_t963265114 * ___completed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_U3CTaskWhenAnyCastU3Em__0_m4259118645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Task_1_t963265114 * L_0 = ___completed0;
		NullCheck((Task_1_t963265114 *)L_0);
		Task_t1843236107 * L_1 = Task_1_get_Result_m1316373833((Task_1_t963265114 *)L_0, /*hidden argument*/Task_1_get_Result_m1316373833_MethodInfo_var);
		return ((Task_1_t1191906455 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor()
extern "C"  void Task_1__ctor_m213817015_gshared (Task_1_t1809478302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m213817015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m3731960308((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  void Task_1__ctor_m3678853734_gshared (Task_1_t1809478302 * __this, Il2CppObject * ___state0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3678853734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___state0;
		int32_t L_1 = ___options1;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m922749962((Task_t1843236107 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(TResult)
extern "C"  void Task_1__ctor_m3150838094_gshared (Task_1_t1809478302 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3150838094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)0, (int32_t)0, (CancellationToken_t1851405782 )L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___result0;
		__this->set_m_result_24(L_1);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Boolean,TResult,System.Threading.Tasks.TaskCreationOptions,System.Threading.CancellationToken)
extern "C"  void Task_1__ctor_m1204176217_gshared (Task_1_t1809478302 * __this, bool ___canceled0, Il2CppObject * ___result1, int32_t ___creationOptions2, CancellationToken_t1851405782  ___ct3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1204176217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___canceled0;
		int32_t L_1 = ___creationOptions2;
		CancellationToken_t1851405782  L_2 = ___ct3;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)L_0, (int32_t)L_1, (CancellationToken_t1851405782 )L_2, /*hidden argument*/NULL);
		bool L_3 = ___canceled0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_4 = ___result1;
		__this->set_m_result_24(L_4);
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Func`2<System.Object,TResult>,System.Object,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions)
extern "C" IL2CPP_NO_INLINE void Task_1__ctor_m3239156755_gshared (Task_1_t1809478302 * __this, Func_2_t2825504181 * ___function0, Il2CppObject * ___state1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3239156755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Func_2_t2825504181 * L_0 = ___function0;
		Il2CppObject * L_1 = ___state1;
		int32_t L_2 = ___creationOptions3;
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_t1843236107 * L_3 = Task_InternalCurrentIfAttached_m1086187635(NULL /*static, unused*/, (int32_t)L_2, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_4 = ___cancellationToken2;
		int32_t L_5 = ___creationOptions3;
		NullCheck((Task_1_t1809478302 *)__this);
		((  void (*) (Task_1_t1809478302 *, Delegate_t3022476291 *, Il2CppObject *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t1809478302 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_3, (CancellationToken_t1851405782 )L_4, (int32_t)L_5, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (int32_t)1;
		NullCheck((Task_t1843236107 *)__this);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  void Task_1__ctor_m1658954423_gshared (Task_1_t1809478302 * __this, Func_1_t348874681 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1658954423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t348874681 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_1_t1809478302 *)__this);
		((  void (*) (Task_1_t1809478302 *, Func_1_t348874681 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Task_1_t1809478302 *)__this, (Func_1_t348874681 *)L_0, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t* L_6 = ___stackMark6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m3126613762_gshared (Task_1_t1809478302 * __this, Func_1_t348874681 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3126613762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t348874681 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)NULL, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_6&(int32_t)((int32_t)2048))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_7 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, (String_t*)_stringLiteral1588292733, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.ctor(System.Delegate,System.Object,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m3688860927_gshared (Task_1_t1809478302 * __this, Delegate_t3022476291 * ___valueSelector0, Il2CppObject * ___state1, Task_t1843236107 * ___parent2, CancellationToken_t1851405782  ___cancellationToken3, int32_t ___creationOptions4, int32_t ___internalOptions5, TaskScheduler_t3932792796 * ___scheduler6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3688860927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Delegate_t3022476291 * L_0 = ___valueSelector0;
		Il2CppObject * L_1 = ___state1;
		Task_t1843236107 * L_2 = ___parent2;
		CancellationToken_t1851405782  L_3 = ___cancellationToken3;
		int32_t L_4 = ___creationOptions4;
		int32_t L_5 = ___internalOptions5;
		TaskScheduler_t3932792796 * L_6 = ___scheduler6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_2, (CancellationToken_t1851405782 )L_3, (int32_t)L_4, (int32_t)L_5, (TaskScheduler_t3932792796 *)L_6, /*hidden argument*/NULL);
		int32_t L_7 = ___internalOptions5;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)2048))))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_8 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, (String_t*)_stringLiteral1588292733, (String_t*)L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0033:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Object>::StartNew(System.Threading.Tasks.Task,System.Func`1<TResult>,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  Task_1_t1809478302 * Task_1_StartNew_m3808576068_gshared (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___parent0, Func_1_t348874681 * ___function1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_StartNew_m3808576068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t1809478302 * V_0 = NULL;
	{
		Func_1_t348874681 * L_0 = ___function1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral878805882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		TaskScheduler_t3932792796 * L_2 = ___scheduler5;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3323468611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)2048))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_5 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_6 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_6, (String_t*)_stringLiteral1588292733, (String_t*)L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0045:
	{
		Func_1_t348874681 * L_7 = ___function1;
		Task_t1843236107 * L_8 = ___parent0;
		CancellationToken_t1851405782  L_9 = ___cancellationToken2;
		int32_t L_10 = ___creationOptions3;
		int32_t L_11 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_12 = ___scheduler5;
		int32_t* L_13 = ___stackMark6;
		Task_1_t1809478302 * L_14 = (Task_1_t1809478302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Task_1_t1809478302 *, Func_1_t348874681 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_14, (Func_1_t348874681 *)L_7, (Task_t1843236107 *)L_8, (CancellationToken_t1851405782 )L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11|(int32_t)((int32_t)8192))), (TaskScheduler_t3932792796 *)L_12, (int32_t*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (Task_1_t1809478302 *)L_14;
		Task_1_t1809478302 * L_15 = V_0;
		NullCheck((Task_t1843236107 *)L_15);
		Task_ScheduleAndStart_m2476171743((Task_t1843236107 *)L_15, (bool)0, /*hidden argument*/NULL);
		Task_1_t1809478302 * L_16 = V_0;
		return L_16;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Object>::TrySetResult(TResult)
extern "C"  bool Task_1_TrySetResult_m1398503260_gshared (Task_1_t1809478302 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	ContingentProperties_t606988207 * V_0 = NULL;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_1 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject * L_2 = ___result0;
		__this->set_m_result_24(L_2);
		int32_t* L_3 = (int32_t*)((Task_t1843236107 *)__this)->get_address_of_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		Interlocked_Exchange_m4103465028(NULL /*static, unused*/, (int32_t*)L_3, (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))), /*hidden argument*/NULL);
		ContingentProperties_t606988207 * L_5 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)__this)->get_m_contingentProperties_15();
		il2cpp_codegen_memory_barrier();
		V_0 = (ContingentProperties_t606988207 *)L_5;
		ContingentProperties_t606988207 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ContingentProperties_t606988207 * L_7 = V_0;
		NullCheck((ContingentProperties_t606988207 *)L_7);
		ContingentProperties_SetCompleted_m2487364104((ContingentProperties_t606988207 *)L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_FinishStageThree_m4010028243((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0060:
	{
		return (bool)0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::DangerousSetResult(TResult)
extern "C"  void Task_1_DangerousSetResult_m2974375575_gshared (Task_1_t1809478302 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Task_t1843236107 * L_0 = (Task_t1843236107 *)((Task_t1843236107 *)__this)->get_m_parent_8();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___result0;
		NullCheck((Task_1_t1809478302 *)__this);
		bool L_2 = ((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Task_1_t1809478302 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (bool)L_2;
		goto IL_0035;
	}

IL_0018:
	{
		Il2CppObject * L_3 = ___result0;
		__this->set_m_result_24(L_3);
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		((Task_t1843236107 *)__this)->set_m_stateFlags_9(((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))));
	}

IL_0035:
	{
		return;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Object>::get_Result()
extern "C"  Il2CppObject * Task_1_get_Result_m2758788574_gshared (Task_1_t1809478302 * __this, const MethodInfo* method)
{
	Il2CppObject * G_B3_0 = NULL;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsWaitNotificationEnabledOrNotRanToCompletion_m2720866512((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((Task_1_t1809478302 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Task_1_t1809478302 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Task_1_t1809478302 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0017:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_result_24();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Object>::get_ResultOnSuccess()
extern "C"  Il2CppObject * Task_1_get_ResultOnSuccess_m763524127_gshared (Task_1_t1809478302 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_result_24();
		return L_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Object>::GetResultCore(System.Boolean)
extern "C"  Il2CppObject * Task_1_GetResultCore_m2608934740_gshared (Task_1_t1809478302 * __this, bool ___waitCompletionNotification0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_GetResultCore_m2608934740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_1 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		Task_InternalWait_m945475364((Task_t1843236107 *)__this, (int32_t)(-1), (CancellationToken_t1851405782 )L_1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_2 = ___waitCompletionNotification0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_NotifyDebuggerOfWaitCompletionIfNecessary_m4037125766((Task_t1843236107 *)__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_3 = Task_get_IsRanToCompletion_m3601607431((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_ThrowIfExceptional_m2165180494((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_result_24();
		return L_4;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Object>::TrySetException(System.Object)
extern "C"  bool Task_1_TrySetException_m3273413011_gshared (Task_1_t1809478302 * __this, Il2CppObject * ___exceptionObject0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		Task_EnsureContingentPropertiesInitialized_m3371331877((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___exceptionObject0;
		NullCheck((Task_t1843236107 *)__this);
		Task_AddException_m3383991754((Task_t1843236107 *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_Finish_m2698939980((Task_t1843236107 *)__this, (bool)0, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_002f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Object>::TrySetCanceled(System.Threading.CancellationToken)
extern "C"  bool Task_1_TrySetCanceled_m3779559794_gshared (Task_1_t1809478302 * __this, CancellationToken_t1851405782  ___tokenToRecord0, const MethodInfo* method)
{
	{
		CancellationToken_t1851405782  L_0 = ___tokenToRecord0;
		NullCheck((Task_1_t1809478302 *)__this);
		bool L_1 = ((  bool (*) (Task_1_t1809478302 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Task_1_t1809478302 *)__this, (CancellationToken_t1851405782 )L_0, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Object>::TrySetCanceled(System.Threading.CancellationToken,System.Object)
extern "C"  bool Task_1_TrySetCanceled_m4074270508_gshared (Task_1_t1809478302 * __this, CancellationToken_t1851405782  ___tokenToRecord0, Il2CppObject * ___cancellationException1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		CancellationToken_t1851405782  L_1 = ___tokenToRecord0;
		Il2CppObject * L_2 = ___cancellationException1;
		NullCheck((Task_t1843236107 *)__this);
		Task_RecordInternalCancellationRequest_m3918176011((Task_t1843236107 *)__this, (CancellationToken_t1851405782 )L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_CancellationCleanupLogic_m2712825363((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0027:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1<System.Object>::get_Factory()
extern "C"  TaskFactory_1_t3379767402 * Task_1_get_Factory_m201481449_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TaskFactory_1_t3379767402 * L_0 = ((Task_1_t1809478302_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->get_s_Factory_25();
		return L_0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::InnerInvoke()
extern "C"  void Task_1_InnerInvoke_m604044665_gshared (Task_1_t1809478302 * __this, const MethodInfo* method)
{
	Func_1_t348874681 * V_0 = NULL;
	Func_2_t2825504181 * V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_0 = (Func_1_t348874681 *)((Func_1_t348874681 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8)));
		Func_1_t348874681 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Func_1_t348874681 * L_2 = V_0;
		NullCheck((Func_1_t348874681 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_1_t348874681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_1_t348874681 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_m_result_24(L_3);
		return;
	}

IL_001f:
	{
		Il2CppObject * L_4 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_1 = (Func_2_t2825504181 *)((Func_2_t2825504181 *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)));
		Func_2_t2825504181 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		Func_2_t2825504181 * L_6 = V_1;
		Il2CppObject * L_7 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_stateObject_6();
		NullCheck((Func_2_t2825504181 *)L_6);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Func_2_t2825504181 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		__this->set_m_result_24(L_8);
		return;
	}

IL_0044:
	{
		return;
	}
}
// System.Runtime.CompilerServices.TaskAwaiter`1<TResult> System.Threading.Tasks.Task`1<System.Object>::GetAwaiter()
extern "C"  TaskAwaiter_1_t4058041955  Task_1_GetAwaiter_m1854680895_gshared (Task_1_t1809478302 * __this, const MethodInfo* method)
{
	{
		TaskAwaiter_1_t4058041955  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TaskAwaiter_1__ctor_m882453359(&L_0, (Task_1_t1809478302 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<TResult> System.Threading.Tasks.Task`1<System.Object>::ConfigureAwait(System.Boolean)
extern "C"  ConfiguredTaskAwaitable_1_t3193003308  Task_1_ConfigureAwait_m2068188614_gshared (Task_1_t1809478302 * __this, bool ___continueOnCapturedContext0, const MethodInfo* method)
{
	{
		bool L_0 = ___continueOnCapturedContext0;
		ConfiguredTaskAwaitable_1_t3193003308  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ConfiguredTaskAwaitable_1__ctor_m740361621(&L_1, (Task_1_t1809478302 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Object>::.cctor()
extern "C"  void Task_1__cctor_m844286092_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		TaskFactory_1_t3379767402 * L_0 = (TaskFactory_1_t3379767402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((  void (*) (TaskFactory_1_t3379767402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((Task_1_t1809478302_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_s_Factory_25(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		Func_2_t2622177645 * L_2 = (Func_2_t2622177645 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((  void (*) (Func_2_t2622177645 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Task_1_t1809478302_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_TaskWhenAnyCast_26(L_2);
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Object>::<TaskWhenAnyCast>m__0(System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>)
extern "C"  Task_1_t1809478302 * Task_1_U3CTaskWhenAnyCastU3Em__0_m1495298862_gshared (Il2CppObject * __this /* static, unused */, Task_1_t963265114 * ___completed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_U3CTaskWhenAnyCastU3Em__0_m1495298862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Task_1_t963265114 * L_0 = ___completed0;
		NullCheck((Task_1_t963265114 *)L_0);
		Task_t1843236107 * L_1 = Task_1_get_Result_m1316373833((Task_1_t963265114 *)L_0, /*hidden argument*/Task_1_get_Result_m1316373833_MethodInfo_var);
		return ((Task_1_t1809478302 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor()
extern "C"  void Task_1__ctor_m3445550078_gshared (Task_1_t2445339805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3445550078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m3731960308((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  void Task_1__ctor_m3175258807_gshared (Task_1_t2445339805 * __this, Il2CppObject * ___state0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3175258807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___state0;
		int32_t L_1 = ___options1;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m922749962((Task_t1843236107 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(TResult)
extern "C"  void Task_1__ctor_m2127647879_gshared (Task_1_t2445339805 * __this, VoidTaskResult_t3325310798  ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2127647879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)0, (int32_t)0, (CancellationToken_t1851405782 )L_0, /*hidden argument*/NULL);
		VoidTaskResult_t3325310798  L_1 = ___result0;
		__this->set_m_result_24(L_1);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Boolean,TResult,System.Threading.Tasks.TaskCreationOptions,System.Threading.CancellationToken)
extern "C"  void Task_1__ctor_m3068577956_gshared (Task_1_t2445339805 * __this, bool ___canceled0, VoidTaskResult_t3325310798  ___result1, int32_t ___creationOptions2, CancellationToken_t1851405782  ___ct3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m3068577956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___canceled0;
		int32_t L_1 = ___creationOptions2;
		CancellationToken_t1851405782  L_2 = ___ct3;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m2563468577((Task_t1843236107 *)__this, (bool)L_0, (int32_t)L_1, (CancellationToken_t1851405782 )L_2, /*hidden argument*/NULL);
		bool L_3 = ___canceled0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		VoidTaskResult_t3325310798  L_4 = ___result1;
		__this->set_m_result_24(L_4);
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Func`2<System.Object,TResult>,System.Object,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions)
extern "C" IL2CPP_NO_INLINE void Task_1__ctor_m2082199556_gshared (Task_1_t2445339805 * __this, Func_2_t3461365684 * ___function0, Il2CppObject * ___state1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m2082199556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Func_2_t3461365684 * L_0 = ___function0;
		Il2CppObject * L_1 = ___state1;
		int32_t L_2 = ___creationOptions3;
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_t1843236107 * L_3 = Task_InternalCurrentIfAttached_m1086187635(NULL /*static, unused*/, (int32_t)L_2, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_4 = ___cancellationToken2;
		int32_t L_5 = ___creationOptions3;
		NullCheck((Task_1_t2445339805 *)__this);
		((  void (*) (Task_1_t2445339805 *, Delegate_t3022476291 *, Il2CppObject *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t2445339805 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_3, (CancellationToken_t1851405782 )L_4, (int32_t)L_5, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (int32_t)1;
		NullCheck((Task_t1843236107 *)__this);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  void Task_1__ctor_m1861180062_gshared (Task_1_t2445339805 * __this, Func_1_t984736184 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1861180062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t984736184 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_1_t2445339805 *)__this);
		((  void (*) (Task_1_t2445339805 *, Func_1_t984736184 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Task_1_t2445339805 *)__this, (Func_1_t984736184 *)L_0, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t* L_6 = ___stackMark6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_PossiblyCaptureContext_m3657345937((Task_t1843236107 *)__this, (int32_t*)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Func`1<TResult>,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m1693928371_gshared (Task_1_t2445339805 * __this, Func_1_t984736184 * ___valueSelector0, Task_t1843236107 * ___parent1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1693928371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_t984736184 * L_0 = ___valueSelector0;
		Task_t1843236107 * L_1 = ___parent1;
		CancellationToken_t1851405782  L_2 = ___cancellationToken2;
		int32_t L_3 = ___creationOptions3;
		int32_t L_4 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_5 = ___scheduler5;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)NULL, (Task_t1843236107 *)L_1, (CancellationToken_t1851405782 )L_2, (int32_t)L_3, (int32_t)L_4, (TaskScheduler_t3932792796 *)L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_6&(int32_t)((int32_t)2048))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_7 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, (String_t*)_stringLiteral1588292733, (String_t*)L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Delegate,System.Object,System.Threading.Tasks.Task,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void Task_1__ctor_m1335966936_gshared (Task_1_t2445339805 * __this, Delegate_t3022476291 * ___valueSelector0, Il2CppObject * ___state1, Task_t1843236107 * ___parent2, CancellationToken_t1851405782  ___cancellationToken3, int32_t ___creationOptions4, int32_t ___internalOptions5, TaskScheduler_t3932792796 * ___scheduler6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1335966936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Delegate_t3022476291 * L_0 = ___valueSelector0;
		Il2CppObject * L_1 = ___state1;
		Task_t1843236107 * L_2 = ___parent2;
		CancellationToken_t1851405782  L_3 = ___cancellationToken3;
		int32_t L_4 = ___creationOptions4;
		int32_t L_5 = ___internalOptions5;
		TaskScheduler_t3932792796 * L_6 = ___scheduler6;
		NullCheck((Task_t1843236107 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task__ctor_m4014766898((Task_t1843236107 *)__this, (Delegate_t3022476291 *)L_0, (Il2CppObject *)L_1, (Task_t1843236107 *)L_2, (CancellationToken_t1851405782 )L_3, (int32_t)L_4, (int32_t)L_5, (TaskScheduler_t3932792796 *)L_6, /*hidden argument*/NULL);
		int32_t L_7 = ___internalOptions5;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)2048))))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_8 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_9 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_9, (String_t*)_stringLiteral1588292733, (String_t*)L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0033:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::StartNew(System.Threading.Tasks.Task,System.Func`1<TResult>,System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.InternalTaskOptions,System.Threading.Tasks.TaskScheduler,System.Threading.StackCrawlMark&)
extern "C"  Task_1_t2445339805 * Task_1_StartNew_m3556824229_gshared (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___parent0, Func_1_t984736184 * ___function1, CancellationToken_t1851405782  ___cancellationToken2, int32_t ___creationOptions3, int32_t ___internalOptions4, TaskScheduler_t3932792796 * ___scheduler5, int32_t* ___stackMark6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_StartNew_m3556824229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t2445339805 * V_0 = NULL;
	{
		Func_1_t984736184 * L_0 = ___function1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral878805882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		TaskScheduler_t3932792796 * L_2 = ___scheduler5;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, (String_t*)_stringLiteral3323468611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___internalOptions4;
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)2048))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_5 = Environment_GetResourceString_m2533878090(NULL /*static, unused*/, (String_t*)_stringLiteral2943658594, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_6 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_6, (String_t*)_stringLiteral1588292733, (String_t*)L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0045:
	{
		Func_1_t984736184 * L_7 = ___function1;
		Task_t1843236107 * L_8 = ___parent0;
		CancellationToken_t1851405782  L_9 = ___cancellationToken2;
		int32_t L_10 = ___creationOptions3;
		int32_t L_11 = ___internalOptions4;
		TaskScheduler_t3932792796 * L_12 = ___scheduler5;
		int32_t* L_13 = ___stackMark6;
		Task_1_t2445339805 * L_14 = (Task_1_t2445339805 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Task_1_t2445339805 *, Func_1_t984736184 *, Task_t1843236107 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_14, (Func_1_t984736184 *)L_7, (Task_t1843236107 *)L_8, (CancellationToken_t1851405782 )L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11|(int32_t)((int32_t)8192))), (TaskScheduler_t3932792796 *)L_12, (int32_t*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (Task_1_t2445339805 *)L_14;
		Task_1_t2445339805 * L_15 = V_0;
		NullCheck((Task_t1843236107 *)L_15);
		Task_ScheduleAndStart_m2476171743((Task_t1843236107 *)L_15, (bool)0, /*hidden argument*/NULL);
		Task_1_t2445339805 * L_16 = V_0;
		return L_16;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::TrySetResult(TResult)
extern "C"  bool Task_1_TrySetResult_m1603354423_gshared (Task_1_t2445339805 * __this, VoidTaskResult_t3325310798  ___result0, const MethodInfo* method)
{
	ContingentProperties_t606988207 * V_0 = NULL;
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_1 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		VoidTaskResult_t3325310798  L_2 = ___result0;
		__this->set_m_result_24(L_2);
		int32_t* L_3 = (int32_t*)((Task_t1843236107 *)__this)->get_address_of_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		Interlocked_Exchange_m4103465028(NULL /*static, unused*/, (int32_t*)L_3, (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))), /*hidden argument*/NULL);
		ContingentProperties_t606988207 * L_5 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)__this)->get_m_contingentProperties_15();
		il2cpp_codegen_memory_barrier();
		V_0 = (ContingentProperties_t606988207 *)L_5;
		ContingentProperties_t606988207 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ContingentProperties_t606988207 * L_7 = V_0;
		NullCheck((ContingentProperties_t606988207 *)L_7);
		ContingentProperties_SetCompleted_m2487364104((ContingentProperties_t606988207 *)L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_FinishStageThree_m4010028243((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0060:
	{
		return (bool)0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::DangerousSetResult(TResult)
extern "C"  void Task_1_DangerousSetResult_m2661651686_gshared (Task_1_t2445339805 * __this, VoidTaskResult_t3325310798  ___result0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Task_t1843236107 * L_0 = (Task_t1843236107 *)((Task_t1843236107 *)__this)->get_m_parent_8();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		VoidTaskResult_t3325310798  L_1 = ___result0;
		NullCheck((Task_1_t2445339805 *)__this);
		bool L_2 = ((  bool (*) (Task_1_t2445339805 *, VoidTaskResult_t3325310798 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Task_1_t2445339805 *)__this, (VoidTaskResult_t3325310798 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (bool)L_2;
		goto IL_0035;
	}

IL_0018:
	{
		VoidTaskResult_t3325310798  L_3 = ___result0;
		__this->set_m_result_24(L_3);
		int32_t L_4 = (int32_t)((Task_t1843236107 *)__this)->get_m_stateFlags_9();
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		((Task_t1843236107 *)__this)->set_m_stateFlags_9(((int32_t)((int32_t)L_4|(int32_t)((int32_t)16777216))));
	}

IL_0035:
	{
		return;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::get_Result()
extern "C"  VoidTaskResult_t3325310798  Task_1_get_Result_m2869475246_gshared (Task_1_t2445339805 * __this, const MethodInfo* method)
{
	VoidTaskResult_t3325310798  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsWaitNotificationEnabledOrNotRanToCompletion_m2720866512((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((Task_1_t2445339805 *)__this);
		VoidTaskResult_t3325310798  L_1 = ((  VoidTaskResult_t3325310798  (*) (Task_1_t2445339805 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Task_1_t2445339805 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0017:
	{
		VoidTaskResult_t3325310798  L_2 = (VoidTaskResult_t3325310798 )__this->get_m_result_24();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::get_ResultOnSuccess()
extern "C"  VoidTaskResult_t3325310798  Task_1_get_ResultOnSuccess_m186620704_gshared (Task_1_t2445339805 * __this, const MethodInfo* method)
{
	{
		VoidTaskResult_t3325310798  L_0 = (VoidTaskResult_t3325310798 )__this->get_m_result_24();
		return L_0;
	}
}
// TResult System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::GetResultCore(System.Boolean)
extern "C"  VoidTaskResult_t3325310798  Task_1_GetResultCore_m2672380779_gshared (Task_1_t2445339805 * __this, bool ___waitCompletionNotification0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_GetResultCore_m2672380779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_get_IsCompleted_m669578966((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_1 = V_0;
		NullCheck((Task_t1843236107 *)__this);
		Task_InternalWait_m945475364((Task_t1843236107 *)__this, (int32_t)(-1), (CancellationToken_t1851405782 )L_1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_2 = ___waitCompletionNotification0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_NotifyDebuggerOfWaitCompletionIfNecessary_m4037125766((Task_t1843236107 *)__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		NullCheck((Task_t1843236107 *)__this);
		bool L_3 = Task_get_IsRanToCompletion_m3601607431((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		NullCheck((Task_t1843236107 *)__this);
		Task_ThrowIfExceptional_m2165180494((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		VoidTaskResult_t3325310798  L_4 = (VoidTaskResult_t3325310798 )__this->get_m_result_24();
		return L_4;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::TrySetException(System.Object)
extern "C"  bool Task_1_TrySetException_m2380105302_gshared (Task_1_t2445339805 * __this, Il2CppObject * ___exceptionObject0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		Task_EnsureContingentPropertiesInitialized_m3371331877((Task_t1843236107 *)__this, (bool)1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___exceptionObject0;
		NullCheck((Task_t1843236107 *)__this);
		Task_AddException_m3383991754((Task_t1843236107 *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_Finish_m2698939980((Task_t1843236107 *)__this, (bool)0, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_002f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::TrySetCanceled(System.Threading.CancellationToken)
extern "C"  bool Task_1_TrySetCanceled_m2314571191_gshared (Task_1_t2445339805 * __this, CancellationToken_t1851405782  ___tokenToRecord0, const MethodInfo* method)
{
	{
		CancellationToken_t1851405782  L_0 = ___tokenToRecord0;
		NullCheck((Task_1_t2445339805 *)__this);
		bool L_1 = ((  bool (*) (Task_1_t2445339805 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Task_1_t2445339805 *)__this, (CancellationToken_t1851405782 )L_0, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Boolean System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::TrySetCanceled(System.Threading.CancellationToken,System.Object)
extern "C"  bool Task_1_TrySetCanceled_m3419761117_gshared (Task_1_t2445339805 * __this, CancellationToken_t1851405782  ___tokenToRecord0, Il2CppObject * ___cancellationException1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		NullCheck((Task_t1843236107 *)__this);
		bool L_0 = Task_AtomicStateUpdate_m4068350973((Task_t1843236107 *)__this, (int32_t)((int32_t)67108864), (int32_t)((int32_t)90177536), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		CancellationToken_t1851405782  L_1 = ___tokenToRecord0;
		Il2CppObject * L_2 = ___cancellationException1;
		NullCheck((Task_t1843236107 *)__this);
		Task_RecordInternalCancellationRequest_m3918176011((Task_t1843236107 *)__this, (CancellationToken_t1851405782 )L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Task_t1843236107 *)__this);
		Task_CancellationCleanupLogic_m2712825363((Task_t1843236107 *)__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0027:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::get_Factory()
extern "C"  TaskFactory_1_t4015628905 * Task_1_get_Factory_m4079909032_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TaskFactory_1_t4015628905 * L_0 = ((Task_1_t2445339805_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->get_s_Factory_25();
		return L_0;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::InnerInvoke()
extern "C"  void Task_1_InnerInvoke_m3206152574_gshared (Task_1_t2445339805 * __this, const MethodInfo* method)
{
	Func_1_t984736184 * V_0 = NULL;
	Func_2_t3461365684 * V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_0 = (Func_1_t984736184 *)((Func_1_t984736184 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8)));
		Func_1_t984736184 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Func_1_t984736184 * L_2 = V_0;
		NullCheck((Func_1_t984736184 *)L_2);
		VoidTaskResult_t3325310798  L_3 = ((  VoidTaskResult_t3325310798  (*) (Func_1_t984736184 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_1_t984736184 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_m_result_24(L_3);
		return;
	}

IL_001f:
	{
		Il2CppObject * L_4 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_action_5();
		V_1 = (Func_2_t3461365684 *)((Func_2_t3461365684 *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)));
		Func_2_t3461365684 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		Func_2_t3461365684 * L_6 = V_1;
		Il2CppObject * L_7 = (Il2CppObject *)((Task_t1843236107 *)__this)->get_m_stateObject_6();
		NullCheck((Func_2_t3461365684 *)L_6);
		VoidTaskResult_t3325310798  L_8 = ((  VoidTaskResult_t3325310798  (*) (Func_2_t3461365684 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Func_2_t3461365684 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		__this->set_m_result_24(L_8);
		return;
	}

IL_0044:
	{
		return;
	}
}
// System.Runtime.CompilerServices.TaskAwaiter`1<TResult> System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::GetAwaiter()
extern "C"  TaskAwaiter_1_t398936162  Task_1_GetAwaiter_m2141083242_gshared (Task_1_t2445339805 * __this, const MethodInfo* method)
{
	{
		TaskAwaiter_1_t398936162  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TaskAwaiter_1__ctor_m4174446952(&L_0, (Task_1_t2445339805 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1<TResult> System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::ConfigureAwait(System.Boolean)
extern "C"  ConfiguredTaskAwaitable_1_t3828864811  Task_1_ConfigureAwait_m3005673541_gshared (Task_1_t2445339805 * __this, bool ___continueOnCapturedContext0, const MethodInfo* method)
{
	{
		bool L_0 = ___continueOnCapturedContext0;
		ConfiguredTaskAwaitable_1_t3828864811  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ConfiguredTaskAwaitable_1__ctor_m2250484098(&L_1, (Task_1_t2445339805 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::.cctor()
extern "C"  void Task_1__cctor_m184562537_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		TaskFactory_1_t4015628905 * L_0 = (TaskFactory_1_t4015628905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((  void (*) (TaskFactory_1_t4015628905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((Task_1_t2445339805_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_s_Factory_25(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		Func_2_t3258039148 * L_2 = (Func_2_t3258039148 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((  void (*) (Func_2_t3258039148 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Task_1_t2445339805_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->static_fields)->set_TaskWhenAnyCast_26(L_2);
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>::<TaskWhenAnyCast>m__0(System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>)
extern "C"  Task_1_t2445339805 * Task_1_U3CTaskWhenAnyCastU3Em__0_m214963539_gshared (Il2CppObject * __this /* static, unused */, Task_1_t963265114 * ___completed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_U3CTaskWhenAnyCastU3Em__0_m214963539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Task_1_t963265114 * L_0 = ___completed0;
		NullCheck((Task_1_t963265114 *)L_0);
		Task_t1843236107 * L_1 = Task_1_get_Result_m1316373833((Task_1_t963265114 *)L_0, /*hidden argument*/Task_1_get_Result_m1316373833_MethodInfo_var);
		return ((Task_1_t2445339805 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Boolean>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1__ctor_m3991003317_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Boolean>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1_U3CU3Em__0_m4272393811_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		AtomicBoolean_t379413895 * L_0 = (AtomicBoolean_t379413895 *)__this->get_invoked_0();
		NullCheck((AtomicBoolean_t379413895 *)L_0);
		bool L_1 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_2 = ___iar0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_3 = (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_3);
		Func_2_t3480198991 * L_4 = (Func_2_t3480198991 *)L_3->get_endFunction_0();
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_5 = (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_5);
		Action_1_t1801450390 * L_6 = (Action_1_t1801450390 *)L_5->get_endAction_1();
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_7 = (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_7);
		Task_1_t2945603725 * L_8 = (Task_1_t2945603725 *)L_7->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t3480198991 *, Action_1_t1801450390 *, Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, (Func_2_t3480198991 *)L_4, (Action_1_t1801450390 *)L_6, (Task_1_t2945603725 *)L_8, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Int32>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1__ctor_m2561241195_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Int32>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1_U3CU3Em__0_m3880772609_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		AtomicBoolean_t379413895 * L_0 = (AtomicBoolean_t379413895 *)__this->get_invoked_0();
		NullCheck((AtomicBoolean_t379413895 *)L_0);
		bool L_1 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_2 = ___iar0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_3 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_3);
		Func_2_t1726501721 * L_4 = (Func_2_t1726501721 *)L_3->get_endFunction_0();
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_5 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_5);
		Action_1_t1801450390 * L_6 = (Action_1_t1801450390 *)L_5->get_endAction_1();
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_7 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_7);
		Task_1_t1191906455 * L_8 = (Task_1_t1191906455 *)L_7->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t1726501721 *, Action_1_t1801450390 *, Task_1_t1191906455 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, (Func_2_t1726501721 *)L_4, (Action_1_t1801450390 *)L_6, (Task_1_t1191906455 *)L_8, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1__ctor_m901580190_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Object>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1_U3CU3Em__0_m635830190_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		AtomicBoolean_t379413895 * L_0 = (AtomicBoolean_t379413895 *)__this->get_invoked_0();
		NullCheck((AtomicBoolean_t379413895 *)L_0);
		bool L_1 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_2 = ___iar0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_3 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_3);
		Func_2_t2344073568 * L_4 = (Func_2_t2344073568 *)L_3->get_endFunction_0();
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_5 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_5);
		Action_1_t1801450390 * L_6 = (Action_1_t1801450390 *)L_5->get_endAction_1();
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_7 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_7);
		Task_1_t1809478302 * L_8 = (Task_1_t1809478302 *)L_7->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2344073568 *, Action_1_t1801450390 *, Task_1_t1809478302 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, (Func_2_t2344073568 *)L_4, (Action_1_t1801450390 *)L_6, (Task_1_t1809478302 *)L_8, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Threading.Tasks.VoidTaskResult>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1__ctor_m3147071245_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Threading.Tasks.VoidTaskResult>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey1_U3CU3Em__0_m354182971_gshared (U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		AtomicBoolean_t379413895 * L_0 = (AtomicBoolean_t379413895 *)__this->get_invoked_0();
		NullCheck((AtomicBoolean_t379413895 *)L_0);
		bool L_1 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_2 = ___iar0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_3 = (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_3);
		Func_2_t2979935071 * L_4 = (Func_2_t2979935071 *)L_3->get_endFunction_0();
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_5 = (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_5);
		Action_1_t1801450390 * L_6 = (Action_1_t1801450390 *)L_5->get_endAction_1();
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_7 = (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *)__this->get_U3CU3Ef__refU242_1();
		NullCheck(L_7);
		Task_1_t2445339805 * L_8 = (Task_1_t2445339805 *)L_7->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2979935071 *, Action_1_t1801450390 *, Task_1_t2445339805 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, (Func_2_t2979935071 *)L_4, (Action_1_t1801450390 *)L_6, (Task_1_t2445339805 *)L_8, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Boolean>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2__ctor_m1661185458_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Boolean>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2_U3CU3Em__0_m1445659858_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iar0;
		Func_2_t3480198991 * L_1 = (Func_2_t3480198991 *)__this->get_endFunction_0();
		Action_1_t1801450390 * L_2 = (Action_1_t1801450390 *)__this->get_endAction_1();
		Task_1_t2945603725 * L_3 = (Task_1_t2945603725 *)__this->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t3480198991 *, Action_1_t1801450390 *, Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Func_2_t3480198991 *)L_1, (Action_1_t1801450390 *)L_2, (Task_1_t2945603725 *)L_3, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Int32>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2__ctor_m2786452238_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Int32>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2_U3CU3Em__0_m2855477986_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iar0;
		Func_2_t1726501721 * L_1 = (Func_2_t1726501721 *)__this->get_endFunction_0();
		Action_1_t1801450390 * L_2 = (Action_1_t1801450390 *)__this->get_endAction_1();
		Task_1_t1191906455 * L_3 = (Task_1_t1191906455 *)__this->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t1726501721 *, Action_1_t1801450390 *, Task_1_t1191906455 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Func_2_t1726501721 *)L_1, (Action_1_t1801450390 *)L_2, (Task_1_t1191906455 *)L_3, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Object>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2__ctor_m1847278815_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Object>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2_U3CU3Em__0_m1163491949_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iar0;
		Func_2_t2344073568 * L_1 = (Func_2_t2344073568 *)__this->get_endFunction_0();
		Action_1_t1801450390 * L_2 = (Action_1_t1801450390 *)__this->get_endAction_1();
		Task_1_t1809478302 * L_3 = (Task_1_t1809478302 *)__this->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2344073568 *, Action_1_t1801450390 *, Task_1_t1809478302 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Func_2_t2344073568 *)L_1, (Action_1_t1801450390 *)L_2, (Task_1_t1809478302 *)L_3, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Threading.Tasks.VoidTaskResult>::.ctor()
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2__ctor_m924310604_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Threading.Tasks.VoidTaskResult>::<>m__0(System.IAsyncResult)
extern "C"  void U3CFromAsyncImplU3Ec__AnonStorey2_U3CU3Em__0_m4260241660_gshared (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iar0;
		Func_2_t2979935071 * L_1 = (Func_2_t2979935071 *)__this->get_endFunction_0();
		Action_1_t1801450390 * L_2 = (Action_1_t1801450390 *)__this->get_endAction_1();
		Task_1_t2445339805 * L_3 = (Task_1_t2445339805 *)__this->get_promise_2();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2979935071 *, Action_1_t1801450390 *, Task_1_t2445339805 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Func_2_t2979935071 *)L_1, (Action_1_t1801450390 *)L_2, (Task_1_t2445339805 *)L_3, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Boolean>::.ctor()
extern "C"  void TaskFactory_1__ctor_m1536387660_gshared (TaskFactory_1_t220925529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1__ctor_m1536387660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((TaskFactory_1_t220925529 *)__this);
		((  void (*) (TaskFactory_1_t220925529 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TaskFactory_1_t220925529 *)__this, (CancellationToken_t1851405782 )L_0, (int32_t)0, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Boolean>::.ctor(System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.TaskContinuationOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void TaskFactory_1__ctor_m3752158382_gshared (TaskFactory_1_t220925529 * __this, CancellationToken_t1851405782  ___cancellationToken0, int32_t ___creationOptions1, int32_t ___continuationOptions2, TaskScheduler_t3932792796 * ___scheduler3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___continuationOptions2;
		TaskFactory_CheckMultiTaskContinuationOptions_m717392640(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___creationOptions1;
		TaskFactory_CheckCreationOptions_m1907973182(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_2 = ___cancellationToken0;
		__this->set_m_defaultCancellationToken_0(L_2);
		TaskScheduler_t3932792796 * L_3 = ___scheduler3;
		__this->set_m_defaultScheduler_1(L_3);
		int32_t L_4 = ___creationOptions1;
		__this->set_m_defaultCreationOptions_2(L_4);
		int32_t L_5 = ___continuationOptions2;
		__this->set_m_defaultContinuationOptions_3(L_5);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Boolean>::FromAsyncCoreLogic(System.IAsyncResult,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void TaskFactory_1_FromAsyncCoreLogic_m3757763575_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___iar0, Func_2_t3480198991 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Task_1_t2945603725 * ___promise3, bool ___requiresSynchronization4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncCoreLogic_m3757763575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	OperationCanceledException_t2897400967 * V_4 = NULL;
	Exception_t1927440687 * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		V_1 = (OperationCanceledException_t2897400967 *)NULL;
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_3));
		bool L_0 = V_3;
		V_2 = (bool)L_0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Func_2_t3480198991 * L_1 = ___endFunction1;
				if (!L_1)
				{
					goto IL_0021;
				}
			}

IL_0014:
			{
				Func_2_t3480198991 * L_2 = ___endFunction1;
				Il2CppObject * L_3 = ___iar0;
				NullCheck((Func_2_t3480198991 *)L_2);
				bool L_4 = ((  bool (*) (Func_2_t3480198991 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((Func_2_t3480198991 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				V_2 = (bool)L_4;
				goto IL_0028;
			}

IL_0021:
			{
				Action_1_t1801450390 * L_5 = ___endAction2;
				Il2CppObject * L_6 = ___iar0;
				NullCheck((Action_1_t1801450390 *)L_5);
				Action_1_Invoke_m2407548955((Action_1_t1801450390 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/Action_1_Invoke_m2407548955_MethodInfo_var);
			}

IL_0028:
			{
				IL2CPP_LEAVE(0xDD, FINALLY_0041);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (OperationCanceledException_t2897400967_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_002d;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0037;
			throw e;
		}

CATCH_002d:
		{ // begin catch(System.OperationCanceledException)
			V_4 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)__exception_local);
			OperationCanceledException_t2897400967 * L_7 = V_4;
			V_1 = (OperationCanceledException_t2897400967 *)L_7;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)

CATCH_0037:
		{ // begin catch(System.Exception)
			V_5 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_8 = V_5;
			V_0 = (Exception_t1927440687 *)L_8;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			OperationCanceledException_t2897400967 * L_9 = V_1;
			if (!L_9)
			{
				goto IL_005a;
			}
		}

IL_0047:
		{
			Task_1_t2945603725 * L_10 = ___promise3;
			OperationCanceledException_t2897400967 * L_11 = V_1;
			NullCheck((OperationCanceledException_t2897400967 *)L_11);
			CancellationToken_t1851405782  L_12 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_11, /*hidden argument*/NULL);
			OperationCanceledException_t2897400967 * L_13 = V_1;
			NullCheck((Task_1_t2945603725 *)L_10);
			((  bool (*) (Task_1_t2945603725 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Task_1_t2945603725 *)L_10, (CancellationToken_t1851405782 )L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			goto IL_00dc;
		}

IL_005a:
		{
			Exception_t1927440687 * L_14 = V_0;
			if (!L_14)
			{
				goto IL_0095;
			}
		}

IL_0060:
		{
			Task_1_t2945603725 * L_15 = ___promise3;
			Exception_t1927440687 * L_16 = V_0;
			NullCheck((Task_1_t2945603725 *)L_15);
			bool L_17 = ((  bool (*) (Task_1_t2945603725 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((Task_1_t2945603725 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_6 = (bool)L_17;
			bool L_18 = V_6;
			if (!L_18)
			{
				goto IL_0090;
			}
		}

IL_0070:
		{
			Exception_t1927440687 * L_19 = V_0;
			if (!((ThreadAbortException_t1150575753 *)IsInst(L_19, ThreadAbortException_t1150575753_il2cpp_TypeInfo_var)))
			{
				goto IL_0090;
			}
		}

IL_007b:
		{
			Task_1_t2945603725 * L_20 = ___promise3;
			NullCheck(L_20);
			ContingentProperties_t606988207 * L_21 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)L_20)->get_m_contingentProperties_15();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_21);
			TaskExceptionHolder_t2208677448 * L_22 = (TaskExceptionHolder_t2208677448 *)L_21->get_m_exceptionsHolder_2();
			il2cpp_codegen_memory_barrier();
			NullCheck((TaskExceptionHolder_t2208677448 *)L_22);
			TaskExceptionHolder_MarkAsHandled_m698250467((TaskExceptionHolder_t2208677448 *)L_22, (bool)0, /*hidden argument*/NULL);
		}

IL_0090:
		{
			goto IL_00dc;
		}

IL_0095:
		{
			bool L_23 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00ac;
			}
		}

IL_009f:
		{
			Task_1_t2945603725 * L_24 = ___promise3;
			NullCheck((Task_t1843236107 *)L_24);
			int32_t L_25 = Task_get_Id_m4106115082((Task_t1843236107 *)L_24, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_25, (int32_t)1, /*hidden argument*/NULL);
		}

IL_00ac:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_26 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_26)
			{
				goto IL_00c1;
			}
		}

IL_00b6:
		{
			Task_1_t2945603725 * L_27 = ___promise3;
			NullCheck((Task_t1843236107 *)L_27);
			int32_t L_28 = Task_get_Id_m4106115082((Task_t1843236107 *)L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_28, /*hidden argument*/NULL);
		}

IL_00c1:
		{
			bool L_29 = ___requiresSynchronization4;
			if (!L_29)
			{
				goto IL_00d5;
			}
		}

IL_00c8:
		{
			Task_1_t2945603725 * L_30 = ___promise3;
			bool L_31 = V_2;
			NullCheck((Task_1_t2945603725 *)L_30);
			((  bool (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t2945603725 *)L_30, (bool)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			goto IL_00dc;
		}

IL_00d5:
		{
			Task_1_t2945603725 * L_32 = ___promise3;
			bool L_33 = V_2;
			NullCheck((Task_1_t2945603725 *)L_32);
			((  void (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t2945603725 *)L_32, (bool)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00dc:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00dd:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Boolean>::FromAsync(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Object)
extern "C"  Task_1_t2945603725 * TaskFactory_1_FromAsync_m246363337_gshared (TaskFactory_1_t220925529 * __this, Func_3_t1604491142 * ___beginMethod0, Func_2_t3480198991 * ___endMethod1, Il2CppObject * ___state2, const MethodInfo* method)
{
	{
		Func_3_t1604491142 * L_0 = ___beginMethod0;
		Func_2_t3480198991 * L_1 = ___endMethod1;
		Il2CppObject * L_2 = ___state2;
		int32_t L_3 = (int32_t)__this->get_m_defaultCreationOptions_2();
		Task_1_t2945603725 * L_4 = ((  Task_1_t2945603725 * (*) (Il2CppObject * /* static, unused */, Func_3_t1604491142 *, Func_2_t3480198991 *, Action_1_t1801450390 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Func_3_t1604491142 *)L_0, (Func_2_t3480198991 *)L_1, (Action_1_t1801450390 *)NULL, (Il2CppObject *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_4;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Boolean>::FromAsyncImpl(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  Task_1_t2945603725 * TaskFactory_1_FromAsyncImpl_m1220570521_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1604491142 * ___beginMethod0, Func_2_t3480198991 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Il2CppObject * ___state3, int32_t ___creationOptions4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncImpl_m1220570521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * V_0 = NULL;
	U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	bool V_4 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 *)L_0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_1 = V_0;
		Func_2_t3480198991 * L_2 = ___endFunction1;
		NullCheck(L_1);
		L_1->set_endFunction_0(L_2);
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_3 = V_0;
		Action_1_t1801450390 * L_4 = ___endAction2;
		NullCheck(L_3);
		L_3->set_endAction_1(L_4);
		Func_3_t1604491142 * L_5 = ___beginMethod0;
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		ArgumentNullException_t628810857 * L_6 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_6, (String_t*)_stringLiteral784393026, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0025:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_7 = V_0;
		NullCheck(L_7);
		Func_2_t3480198991 * L_8 = (Func_2_t3480198991 *)L_7->get_endFunction_0();
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t1801450390 * L_10 = (Action_1_t1801450390 *)L_9->get_endAction_1();
		if (L_10)
		{
			goto IL_0046;
		}
	}
	{
		ArgumentNullException_t628810857 * L_11 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_11, (String_t*)_stringLiteral3599838238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0046:
	{
		int32_t L_12 = ___creationOptions4;
		TaskFactory_CheckFromAsyncOptions_m238110038(NULL /*static, unused*/, (int32_t)L_12, (bool)1, /*hidden argument*/NULL);
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_13 = V_0;
		Il2CppObject * L_14 = ___state3;
		int32_t L_15 = ___creationOptions4;
		Task_1_t2945603725 * L_16 = (Task_1_t2945603725 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Task_1_t2945603725 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_16, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_13);
		L_13->set_promise_2(L_16);
		bool L_17 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_18 = V_0;
		NullCheck(L_18);
		Task_1_t2945603725 * L_19 = (Task_1_t2945603725 *)L_18->get_promise_2();
		NullCheck((Task_t1843236107 *)L_19);
		int32_t L_20 = Task_get_Id_m4106115082((Task_t1843236107 *)L_19, /*hidden argument*/NULL);
		Func_3_t1604491142 * L_21 = ___beginMethod0;
		NullCheck((Delegate_t3022476291 *)L_21);
		MethodInfo_t * L_22 = Delegate_get_Method_m2968370506((Delegate_t3022476291 *)L_21, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_22);
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2115721401, (String_t*)L_23, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCreation_m3376453187(NULL /*static, unused*/, (int32_t)0, (int32_t)L_20, (String_t*)L_24, (uint64_t)(((int64_t)((int64_t)0))), /*hidden argument*/NULL);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_25 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_25)
		{
			goto IL_00a4;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_26 = V_0;
		NullCheck(L_26);
		Task_1_t2945603725 * L_27 = (Task_1_t2945603725 *)L_26->get_promise_2();
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_AddToActiveTasks_m2680726720(NULL /*static, unused*/, (Task_t1843236107 *)L_27, /*hidden argument*/NULL);
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var);
			bool L_28 = BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_0119;
			}
		}

IL_00ae:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * L_29 = (U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			V_1 = (U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 *)L_29;
			U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * L_30 = V_1;
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_31 = V_0;
			NullCheck(L_30);
			L_30->set_U3CU3Ef__refU242_1(L_31);
			U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * L_32 = V_1;
			AtomicBoolean_t379413895 * L_33 = (AtomicBoolean_t379413895 *)il2cpp_codegen_object_new(AtomicBoolean_t379413895_il2cpp_TypeInfo_var);
			AtomicBoolean__ctor_m845874490(L_33, /*hidden argument*/NULL);
			NullCheck(L_32);
			L_32->set_invoked_0(L_33);
			Func_3_t1604491142 * L_34 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * L_35 = V_1;
			IntPtr_t L_36;
			L_36.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			AsyncCallback_t163412349 * L_37 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_37, (Il2CppObject *)L_35, (IntPtr_t)L_36, /*hidden argument*/NULL);
			Il2CppObject * L_38 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_34);
			Il2CppObject * L_39 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_34, (AsyncCallback_t163412349 *)L_37, (Il2CppObject *)L_38, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_2 = (Il2CppObject *)L_39;
			Il2CppObject * L_40 = V_2;
			if (!L_40)
			{
				goto IL_0114;
			}
		}

IL_00e0:
		{
			Il2CppObject * L_41 = V_2;
			NullCheck((Il2CppObject *)L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t1999651008_il2cpp_TypeInfo_var, (Il2CppObject *)L_41);
			if (!L_42)
			{
				goto IL_0114;
			}
		}

IL_00eb:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311 * L_43 = V_1;
			NullCheck(L_43);
			AtomicBoolean_t379413895 * L_44 = (AtomicBoolean_t379413895 *)L_43->get_invoked_0();
			NullCheck((AtomicBoolean_t379413895 *)L_44);
			bool L_45 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_44, /*hidden argument*/NULL);
			if (!L_45)
			{
				goto IL_0114;
			}
		}

IL_00fb:
		{
			Il2CppObject * L_46 = V_2;
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_47 = V_0;
			NullCheck(L_47);
			Func_2_t3480198991 * L_48 = (Func_2_t3480198991 *)L_47->get_endFunction_0();
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_49 = V_0;
			NullCheck(L_49);
			Action_1_t1801450390 * L_50 = (Action_1_t1801450390 *)L_49->get_endAction_1();
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_51 = V_0;
			NullCheck(L_51);
			Task_1_t2945603725 * L_52 = (Task_1_t2945603725 *)L_51->get_promise_2();
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t3480198991 *, Action_1_t1801450390 *, Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_46, (Func_2_t3480198991 *)L_48, (Action_1_t1801450390 *)L_50, (Task_1_t2945603725 *)L_52, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		}

IL_0114:
		{
			goto IL_012d;
		}

IL_0119:
		{
			Func_3_t1604491142 * L_53 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_54 = V_0;
			IntPtr_t L_55;
			L_55.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			AsyncCallback_t163412349 * L_56 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_56, (Il2CppObject *)L_54, (IntPtr_t)L_55, /*hidden argument*/NULL);
			Il2CppObject * L_57 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_53);
			Il2CppObject * L_58 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_53, (AsyncCallback_t163412349 *)L_56, (Il2CppObject *)L_57, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_3 = (Il2CppObject *)L_58;
		}

IL_012d:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0132;
		throw e;
	}

CATCH_0132:
	{ // begin catch(System.Object)
		{
			bool L_59 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_59)
			{
				goto IL_014f;
			}
		}

IL_013d:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_60 = V_0;
			NullCheck(L_60);
			Task_1_t2945603725 * L_61 = (Task_1_t2945603725 *)L_60->get_promise_2();
			NullCheck((Task_t1843236107 *)L_61);
			int32_t L_62 = Task_get_Id_m4106115082((Task_t1843236107 *)L_61, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_62, (int32_t)3, /*hidden argument*/NULL);
		}

IL_014f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_63 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_63)
			{
				goto IL_0169;
			}
		}

IL_0159:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_64 = V_0;
			NullCheck(L_64);
			Task_1_t2945603725 * L_65 = (Task_1_t2945603725 *)L_64->get_promise_2();
			NullCheck((Task_t1843236107 *)L_65);
			int32_t L_66 = Task_get_Id_m4106115082((Task_t1843236107 *)L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_66, /*hidden argument*/NULL);
		}

IL_0169:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_67 = V_0;
			NullCheck(L_67);
			Task_1_t2945603725 * L_68 = (Task_1_t2945603725 *)L_67->get_promise_2();
			Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_4));
			bool L_69 = V_4;
			NullCheck((Task_1_t2945603725 *)L_68);
			((  bool (*) (Task_1_t2945603725 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t2945603725 *)L_68, (bool)L_69, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}
	} // end catch (depth: 1)

IL_0181:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * L_70 = V_0;
		NullCheck(L_70);
		Task_1_t2945603725 * L_71 = (Task_1_t2945603725 *)L_70->get_promise_2();
		return L_71;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Int32>::.ctor()
extern "C"  void TaskFactory_1__ctor_m3007216488_gshared (TaskFactory_1_t2762195555 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1__ctor_m3007216488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((TaskFactory_1_t2762195555 *)__this);
		((  void (*) (TaskFactory_1_t2762195555 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TaskFactory_1_t2762195555 *)__this, (CancellationToken_t1851405782 )L_0, (int32_t)0, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Int32>::.ctor(System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.TaskContinuationOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void TaskFactory_1__ctor_m232645722_gshared (TaskFactory_1_t2762195555 * __this, CancellationToken_t1851405782  ___cancellationToken0, int32_t ___creationOptions1, int32_t ___continuationOptions2, TaskScheduler_t3932792796 * ___scheduler3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___continuationOptions2;
		TaskFactory_CheckMultiTaskContinuationOptions_m717392640(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___creationOptions1;
		TaskFactory_CheckCreationOptions_m1907973182(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_2 = ___cancellationToken0;
		__this->set_m_defaultCancellationToken_0(L_2);
		TaskScheduler_t3932792796 * L_3 = ___scheduler3;
		__this->set_m_defaultScheduler_1(L_3);
		int32_t L_4 = ___creationOptions1;
		__this->set_m_defaultCreationOptions_2(L_4);
		int32_t L_5 = ___continuationOptions2;
		__this->set_m_defaultContinuationOptions_3(L_5);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Int32>::FromAsyncCoreLogic(System.IAsyncResult,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void TaskFactory_1_FromAsyncCoreLogic_m4081154385_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___iar0, Func_2_t1726501721 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Task_1_t1191906455 * ___promise3, bool ___requiresSynchronization4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncCoreLogic_m4081154385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	OperationCanceledException_t2897400967 * V_4 = NULL;
	Exception_t1927440687 * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		V_1 = (OperationCanceledException_t2897400967 *)NULL;
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_0 = V_3;
		V_2 = (int32_t)L_0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Func_2_t1726501721 * L_1 = ___endFunction1;
				if (!L_1)
				{
					goto IL_0021;
				}
			}

IL_0014:
			{
				Func_2_t1726501721 * L_2 = ___endFunction1;
				Il2CppObject * L_3 = ___iar0;
				NullCheck((Func_2_t1726501721 *)L_2);
				int32_t L_4 = ((  int32_t (*) (Func_2_t1726501721 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((Func_2_t1726501721 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				V_2 = (int32_t)L_4;
				goto IL_0028;
			}

IL_0021:
			{
				Action_1_t1801450390 * L_5 = ___endAction2;
				Il2CppObject * L_6 = ___iar0;
				NullCheck((Action_1_t1801450390 *)L_5);
				Action_1_Invoke_m2407548955((Action_1_t1801450390 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/Action_1_Invoke_m2407548955_MethodInfo_var);
			}

IL_0028:
			{
				IL2CPP_LEAVE(0xDD, FINALLY_0041);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (OperationCanceledException_t2897400967_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_002d;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0037;
			throw e;
		}

CATCH_002d:
		{ // begin catch(System.OperationCanceledException)
			V_4 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)__exception_local);
			OperationCanceledException_t2897400967 * L_7 = V_4;
			V_1 = (OperationCanceledException_t2897400967 *)L_7;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)

CATCH_0037:
		{ // begin catch(System.Exception)
			V_5 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_8 = V_5;
			V_0 = (Exception_t1927440687 *)L_8;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			OperationCanceledException_t2897400967 * L_9 = V_1;
			if (!L_9)
			{
				goto IL_005a;
			}
		}

IL_0047:
		{
			Task_1_t1191906455 * L_10 = ___promise3;
			OperationCanceledException_t2897400967 * L_11 = V_1;
			NullCheck((OperationCanceledException_t2897400967 *)L_11);
			CancellationToken_t1851405782  L_12 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_11, /*hidden argument*/NULL);
			OperationCanceledException_t2897400967 * L_13 = V_1;
			NullCheck((Task_1_t1191906455 *)L_10);
			((  bool (*) (Task_1_t1191906455 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Task_1_t1191906455 *)L_10, (CancellationToken_t1851405782 )L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			goto IL_00dc;
		}

IL_005a:
		{
			Exception_t1927440687 * L_14 = V_0;
			if (!L_14)
			{
				goto IL_0095;
			}
		}

IL_0060:
		{
			Task_1_t1191906455 * L_15 = ___promise3;
			Exception_t1927440687 * L_16 = V_0;
			NullCheck((Task_1_t1191906455 *)L_15);
			bool L_17 = ((  bool (*) (Task_1_t1191906455 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((Task_1_t1191906455 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_6 = (bool)L_17;
			bool L_18 = V_6;
			if (!L_18)
			{
				goto IL_0090;
			}
		}

IL_0070:
		{
			Exception_t1927440687 * L_19 = V_0;
			if (!((ThreadAbortException_t1150575753 *)IsInst(L_19, ThreadAbortException_t1150575753_il2cpp_TypeInfo_var)))
			{
				goto IL_0090;
			}
		}

IL_007b:
		{
			Task_1_t1191906455 * L_20 = ___promise3;
			NullCheck(L_20);
			ContingentProperties_t606988207 * L_21 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)L_20)->get_m_contingentProperties_15();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_21);
			TaskExceptionHolder_t2208677448 * L_22 = (TaskExceptionHolder_t2208677448 *)L_21->get_m_exceptionsHolder_2();
			il2cpp_codegen_memory_barrier();
			NullCheck((TaskExceptionHolder_t2208677448 *)L_22);
			TaskExceptionHolder_MarkAsHandled_m698250467((TaskExceptionHolder_t2208677448 *)L_22, (bool)0, /*hidden argument*/NULL);
		}

IL_0090:
		{
			goto IL_00dc;
		}

IL_0095:
		{
			bool L_23 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00ac;
			}
		}

IL_009f:
		{
			Task_1_t1191906455 * L_24 = ___promise3;
			NullCheck((Task_t1843236107 *)L_24);
			int32_t L_25 = Task_get_Id_m4106115082((Task_t1843236107 *)L_24, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_25, (int32_t)1, /*hidden argument*/NULL);
		}

IL_00ac:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_26 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_26)
			{
				goto IL_00c1;
			}
		}

IL_00b6:
		{
			Task_1_t1191906455 * L_27 = ___promise3;
			NullCheck((Task_t1843236107 *)L_27);
			int32_t L_28 = Task_get_Id_m4106115082((Task_t1843236107 *)L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_28, /*hidden argument*/NULL);
		}

IL_00c1:
		{
			bool L_29 = ___requiresSynchronization4;
			if (!L_29)
			{
				goto IL_00d5;
			}
		}

IL_00c8:
		{
			Task_1_t1191906455 * L_30 = ___promise3;
			int32_t L_31 = V_2;
			NullCheck((Task_1_t1191906455 *)L_30);
			((  bool (*) (Task_1_t1191906455 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t1191906455 *)L_30, (int32_t)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			goto IL_00dc;
		}

IL_00d5:
		{
			Task_1_t1191906455 * L_32 = ___promise3;
			int32_t L_33 = V_2;
			NullCheck((Task_1_t1191906455 *)L_32);
			((  void (*) (Task_1_t1191906455 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t1191906455 *)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00dc:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00dd:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Int32>::FromAsync(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Object)
extern "C"  Task_1_t1191906455 * TaskFactory_1_FromAsync_m3277180679_gshared (TaskFactory_1_t2762195555 * __this, Func_3_t1604491142 * ___beginMethod0, Func_2_t1726501721 * ___endMethod1, Il2CppObject * ___state2, const MethodInfo* method)
{
	{
		Func_3_t1604491142 * L_0 = ___beginMethod0;
		Func_2_t1726501721 * L_1 = ___endMethod1;
		Il2CppObject * L_2 = ___state2;
		int32_t L_3 = (int32_t)__this->get_m_defaultCreationOptions_2();
		Task_1_t1191906455 * L_4 = ((  Task_1_t1191906455 * (*) (Il2CppObject * /* static, unused */, Func_3_t1604491142 *, Func_2_t1726501721 *, Action_1_t1801450390 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Func_3_t1604491142 *)L_0, (Func_2_t1726501721 *)L_1, (Action_1_t1801450390 *)NULL, (Il2CppObject *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_4;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Int32>::FromAsyncImpl(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  Task_1_t1191906455 * TaskFactory_1_FromAsyncImpl_m2198391319_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1604491142 * ___beginMethod0, Func_2_t1726501721 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Il2CppObject * ___state3, int32_t ___creationOptions4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncImpl_m2198391319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * V_0 = NULL;
	U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 *)L_0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_1 = V_0;
		Func_2_t1726501721 * L_2 = ___endFunction1;
		NullCheck(L_1);
		L_1->set_endFunction_0(L_2);
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_3 = V_0;
		Action_1_t1801450390 * L_4 = ___endAction2;
		NullCheck(L_3);
		L_3->set_endAction_1(L_4);
		Func_3_t1604491142 * L_5 = ___beginMethod0;
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		ArgumentNullException_t628810857 * L_6 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_6, (String_t*)_stringLiteral784393026, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0025:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_7 = V_0;
		NullCheck(L_7);
		Func_2_t1726501721 * L_8 = (Func_2_t1726501721 *)L_7->get_endFunction_0();
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t1801450390 * L_10 = (Action_1_t1801450390 *)L_9->get_endAction_1();
		if (L_10)
		{
			goto IL_0046;
		}
	}
	{
		ArgumentNullException_t628810857 * L_11 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_11, (String_t*)_stringLiteral3599838238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0046:
	{
		int32_t L_12 = ___creationOptions4;
		TaskFactory_CheckFromAsyncOptions_m238110038(NULL /*static, unused*/, (int32_t)L_12, (bool)1, /*hidden argument*/NULL);
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_13 = V_0;
		Il2CppObject * L_14 = ___state3;
		int32_t L_15 = ___creationOptions4;
		Task_1_t1191906455 * L_16 = (Task_1_t1191906455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Task_1_t1191906455 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_16, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_13);
		L_13->set_promise_2(L_16);
		bool L_17 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_18 = V_0;
		NullCheck(L_18);
		Task_1_t1191906455 * L_19 = (Task_1_t1191906455 *)L_18->get_promise_2();
		NullCheck((Task_t1843236107 *)L_19);
		int32_t L_20 = Task_get_Id_m4106115082((Task_t1843236107 *)L_19, /*hidden argument*/NULL);
		Func_3_t1604491142 * L_21 = ___beginMethod0;
		NullCheck((Delegate_t3022476291 *)L_21);
		MethodInfo_t * L_22 = Delegate_get_Method_m2968370506((Delegate_t3022476291 *)L_21, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_22);
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2115721401, (String_t*)L_23, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCreation_m3376453187(NULL /*static, unused*/, (int32_t)0, (int32_t)L_20, (String_t*)L_24, (uint64_t)(((int64_t)((int64_t)0))), /*hidden argument*/NULL);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_25 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_25)
		{
			goto IL_00a4;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_26 = V_0;
		NullCheck(L_26);
		Task_1_t1191906455 * L_27 = (Task_1_t1191906455 *)L_26->get_promise_2();
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_AddToActiveTasks_m2680726720(NULL /*static, unused*/, (Task_t1843236107 *)L_27, /*hidden argument*/NULL);
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var);
			bool L_28 = BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_0119;
			}
		}

IL_00ae:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * L_29 = (U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			V_1 = (U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 *)L_29;
			U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * L_30 = V_1;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_31 = V_0;
			NullCheck(L_30);
			L_30->set_U3CU3Ef__refU242_1(L_31);
			U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * L_32 = V_1;
			AtomicBoolean_t379413895 * L_33 = (AtomicBoolean_t379413895 *)il2cpp_codegen_object_new(AtomicBoolean_t379413895_il2cpp_TypeInfo_var);
			AtomicBoolean__ctor_m845874490(L_33, /*hidden argument*/NULL);
			NullCheck(L_32);
			L_32->set_invoked_0(L_33);
			Func_3_t1604491142 * L_34 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * L_35 = V_1;
			IntPtr_t L_36;
			L_36.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			AsyncCallback_t163412349 * L_37 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_37, (Il2CppObject *)L_35, (IntPtr_t)L_36, /*hidden argument*/NULL);
			Il2CppObject * L_38 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_34);
			Il2CppObject * L_39 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_34, (AsyncCallback_t163412349 *)L_37, (Il2CppObject *)L_38, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_2 = (Il2CppObject *)L_39;
			Il2CppObject * L_40 = V_2;
			if (!L_40)
			{
				goto IL_0114;
			}
		}

IL_00e0:
		{
			Il2CppObject * L_41 = V_2;
			NullCheck((Il2CppObject *)L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t1999651008_il2cpp_TypeInfo_var, (Il2CppObject *)L_41);
			if (!L_42)
			{
				goto IL_0114;
			}
		}

IL_00eb:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t338974041 * L_43 = V_1;
			NullCheck(L_43);
			AtomicBoolean_t379413895 * L_44 = (AtomicBoolean_t379413895 *)L_43->get_invoked_0();
			NullCheck((AtomicBoolean_t379413895 *)L_44);
			bool L_45 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_44, /*hidden argument*/NULL);
			if (!L_45)
			{
				goto IL_0114;
			}
		}

IL_00fb:
		{
			Il2CppObject * L_46 = V_2;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_47 = V_0;
			NullCheck(L_47);
			Func_2_t1726501721 * L_48 = (Func_2_t1726501721 *)L_47->get_endFunction_0();
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_49 = V_0;
			NullCheck(L_49);
			Action_1_t1801450390 * L_50 = (Action_1_t1801450390 *)L_49->get_endAction_1();
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_51 = V_0;
			NullCheck(L_51);
			Task_1_t1191906455 * L_52 = (Task_1_t1191906455 *)L_51->get_promise_2();
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t1726501721 *, Action_1_t1801450390 *, Task_1_t1191906455 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_46, (Func_2_t1726501721 *)L_48, (Action_1_t1801450390 *)L_50, (Task_1_t1191906455 *)L_52, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		}

IL_0114:
		{
			goto IL_012d;
		}

IL_0119:
		{
			Func_3_t1604491142 * L_53 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_54 = V_0;
			IntPtr_t L_55;
			L_55.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			AsyncCallback_t163412349 * L_56 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_56, (Il2CppObject *)L_54, (IntPtr_t)L_55, /*hidden argument*/NULL);
			Il2CppObject * L_57 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_53);
			Il2CppObject * L_58 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_53, (AsyncCallback_t163412349 *)L_56, (Il2CppObject *)L_57, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_3 = (Il2CppObject *)L_58;
		}

IL_012d:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0132;
		throw e;
	}

CATCH_0132:
	{ // begin catch(System.Object)
		{
			bool L_59 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_59)
			{
				goto IL_014f;
			}
		}

IL_013d:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_60 = V_0;
			NullCheck(L_60);
			Task_1_t1191906455 * L_61 = (Task_1_t1191906455 *)L_60->get_promise_2();
			NullCheck((Task_t1843236107 *)L_61);
			int32_t L_62 = Task_get_Id_m4106115082((Task_t1843236107 *)L_61, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_62, (int32_t)3, /*hidden argument*/NULL);
		}

IL_014f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_63 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_63)
			{
				goto IL_0169;
			}
		}

IL_0159:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_64 = V_0;
			NullCheck(L_64);
			Task_1_t1191906455 * L_65 = (Task_1_t1191906455 *)L_64->get_promise_2();
			NullCheck((Task_t1843236107 *)L_65);
			int32_t L_66 = Task_get_Id_m4106115082((Task_t1843236107 *)L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_66, /*hidden argument*/NULL);
		}

IL_0169:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_67 = V_0;
			NullCheck(L_67);
			Task_1_t1191906455 * L_68 = (Task_1_t1191906455 *)L_67->get_promise_2();
			Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_4));
			int32_t L_69 = V_4;
			NullCheck((Task_1_t1191906455 *)L_68);
			((  bool (*) (Task_1_t1191906455 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t1191906455 *)L_68, (int32_t)L_69, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}
	} // end catch (depth: 1)

IL_0181:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3310218870 * L_70 = V_0;
		NullCheck(L_70);
		Task_1_t1191906455 * L_71 = (Task_1_t1191906455 *)L_70->get_promise_2();
		return L_71;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Object>::.ctor()
extern "C"  void TaskFactory_1__ctor_m2717008271_gshared (TaskFactory_1_t3379767402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1__ctor_m2717008271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((TaskFactory_1_t3379767402 *)__this);
		((  void (*) (TaskFactory_1_t3379767402 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TaskFactory_1_t3379767402 *)__this, (CancellationToken_t1851405782 )L_0, (int32_t)0, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Object>::.ctor(System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.TaskContinuationOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void TaskFactory_1__ctor_m2934220153_gshared (TaskFactory_1_t3379767402 * __this, CancellationToken_t1851405782  ___cancellationToken0, int32_t ___creationOptions1, int32_t ___continuationOptions2, TaskScheduler_t3932792796 * ___scheduler3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___continuationOptions2;
		TaskFactory_CheckMultiTaskContinuationOptions_m717392640(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___creationOptions1;
		TaskFactory_CheckCreationOptions_m1907973182(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_2 = ___cancellationToken0;
		__this->set_m_defaultCancellationToken_0(L_2);
		TaskScheduler_t3932792796 * L_3 = ___scheduler3;
		__this->set_m_defaultScheduler_1(L_3);
		int32_t L_4 = ___creationOptions1;
		__this->set_m_defaultCreationOptions_2(L_4);
		int32_t L_5 = ___continuationOptions2;
		__this->set_m_defaultContinuationOptions_3(L_5);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Object>::FromAsyncCoreLogic(System.IAsyncResult,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void TaskFactory_1_FromAsyncCoreLogic_m3841454974_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___iar0, Func_2_t2344073568 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Task_1_t1809478302 * ___promise3, bool ___requiresSynchronization4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncCoreLogic_m3841454974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	OperationCanceledException_t2897400967 * V_4 = NULL;
	Exception_t1927440687 * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		V_1 = (OperationCanceledException_t2897400967 *)NULL;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_0 = V_3;
		V_2 = (Il2CppObject *)L_0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Func_2_t2344073568 * L_1 = ___endFunction1;
				if (!L_1)
				{
					goto IL_0021;
				}
			}

IL_0014:
			{
				Func_2_t2344073568 * L_2 = ___endFunction1;
				Il2CppObject * L_3 = ___iar0;
				NullCheck((Func_2_t2344073568 *)L_2);
				Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2344073568 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((Func_2_t2344073568 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				V_2 = (Il2CppObject *)L_4;
				goto IL_0028;
			}

IL_0021:
			{
				Action_1_t1801450390 * L_5 = ___endAction2;
				Il2CppObject * L_6 = ___iar0;
				NullCheck((Action_1_t1801450390 *)L_5);
				Action_1_Invoke_m2407548955((Action_1_t1801450390 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/Action_1_Invoke_m2407548955_MethodInfo_var);
			}

IL_0028:
			{
				IL2CPP_LEAVE(0xDD, FINALLY_0041);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (OperationCanceledException_t2897400967_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_002d;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0037;
			throw e;
		}

CATCH_002d:
		{ // begin catch(System.OperationCanceledException)
			V_4 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)__exception_local);
			OperationCanceledException_t2897400967 * L_7 = V_4;
			V_1 = (OperationCanceledException_t2897400967 *)L_7;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)

CATCH_0037:
		{ // begin catch(System.Exception)
			V_5 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_8 = V_5;
			V_0 = (Exception_t1927440687 *)L_8;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			OperationCanceledException_t2897400967 * L_9 = V_1;
			if (!L_9)
			{
				goto IL_005a;
			}
		}

IL_0047:
		{
			Task_1_t1809478302 * L_10 = ___promise3;
			OperationCanceledException_t2897400967 * L_11 = V_1;
			NullCheck((OperationCanceledException_t2897400967 *)L_11);
			CancellationToken_t1851405782  L_12 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_11, /*hidden argument*/NULL);
			OperationCanceledException_t2897400967 * L_13 = V_1;
			NullCheck((Task_1_t1809478302 *)L_10);
			((  bool (*) (Task_1_t1809478302 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Task_1_t1809478302 *)L_10, (CancellationToken_t1851405782 )L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			goto IL_00dc;
		}

IL_005a:
		{
			Exception_t1927440687 * L_14 = V_0;
			if (!L_14)
			{
				goto IL_0095;
			}
		}

IL_0060:
		{
			Task_1_t1809478302 * L_15 = ___promise3;
			Exception_t1927440687 * L_16 = V_0;
			NullCheck((Task_1_t1809478302 *)L_15);
			bool L_17 = ((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((Task_1_t1809478302 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_6 = (bool)L_17;
			bool L_18 = V_6;
			if (!L_18)
			{
				goto IL_0090;
			}
		}

IL_0070:
		{
			Exception_t1927440687 * L_19 = V_0;
			if (!((ThreadAbortException_t1150575753 *)IsInst(L_19, ThreadAbortException_t1150575753_il2cpp_TypeInfo_var)))
			{
				goto IL_0090;
			}
		}

IL_007b:
		{
			Task_1_t1809478302 * L_20 = ___promise3;
			NullCheck(L_20);
			ContingentProperties_t606988207 * L_21 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)L_20)->get_m_contingentProperties_15();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_21);
			TaskExceptionHolder_t2208677448 * L_22 = (TaskExceptionHolder_t2208677448 *)L_21->get_m_exceptionsHolder_2();
			il2cpp_codegen_memory_barrier();
			NullCheck((TaskExceptionHolder_t2208677448 *)L_22);
			TaskExceptionHolder_MarkAsHandled_m698250467((TaskExceptionHolder_t2208677448 *)L_22, (bool)0, /*hidden argument*/NULL);
		}

IL_0090:
		{
			goto IL_00dc;
		}

IL_0095:
		{
			bool L_23 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00ac;
			}
		}

IL_009f:
		{
			Task_1_t1809478302 * L_24 = ___promise3;
			NullCheck((Task_t1843236107 *)L_24);
			int32_t L_25 = Task_get_Id_m4106115082((Task_t1843236107 *)L_24, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_25, (int32_t)1, /*hidden argument*/NULL);
		}

IL_00ac:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_26 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_26)
			{
				goto IL_00c1;
			}
		}

IL_00b6:
		{
			Task_1_t1809478302 * L_27 = ___promise3;
			NullCheck((Task_t1843236107 *)L_27);
			int32_t L_28 = Task_get_Id_m4106115082((Task_t1843236107 *)L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_28, /*hidden argument*/NULL);
		}

IL_00c1:
		{
			bool L_29 = ___requiresSynchronization4;
			if (!L_29)
			{
				goto IL_00d5;
			}
		}

IL_00c8:
		{
			Task_1_t1809478302 * L_30 = ___promise3;
			Il2CppObject * L_31 = V_2;
			NullCheck((Task_1_t1809478302 *)L_30);
			((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t1809478302 *)L_30, (Il2CppObject *)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			goto IL_00dc;
		}

IL_00d5:
		{
			Task_1_t1809478302 * L_32 = ___promise3;
			Il2CppObject * L_33 = V_2;
			NullCheck((Task_1_t1809478302 *)L_32);
			((  void (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t1809478302 *)L_32, (Il2CppObject *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00dc:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00dd:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Object>::FromAsync(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Object)
extern "C"  Task_1_t1809478302 * TaskFactory_1_FromAsync_m117452016_gshared (TaskFactory_1_t3379767402 * __this, Func_3_t1604491142 * ___beginMethod0, Func_2_t2344073568 * ___endMethod1, Il2CppObject * ___state2, const MethodInfo* method)
{
	{
		Func_3_t1604491142 * L_0 = ___beginMethod0;
		Func_2_t2344073568 * L_1 = ___endMethod1;
		Il2CppObject * L_2 = ___state2;
		int32_t L_3 = (int32_t)__this->get_m_defaultCreationOptions_2();
		Task_1_t1809478302 * L_4 = ((  Task_1_t1809478302 * (*) (Il2CppObject * /* static, unused */, Func_3_t1604491142 *, Func_2_t2344073568 *, Action_1_t1801450390 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Func_3_t1604491142 *)L_0, (Func_2_t2344073568 *)L_1, (Action_1_t1801450390 *)NULL, (Il2CppObject *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_4;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Object>::FromAsyncImpl(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  Task_1_t1809478302 * TaskFactory_1_FromAsyncImpl_m4043021740_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1604491142 * ___beginMethod0, Func_2_t2344073568 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Il2CppObject * ___state3, int32_t ___creationOptions4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncImpl_m4043021740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * V_0 = NULL;
	U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 *)L_0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_1 = V_0;
		Func_2_t2344073568 * L_2 = ___endFunction1;
		NullCheck(L_1);
		L_1->set_endFunction_0(L_2);
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_3 = V_0;
		Action_1_t1801450390 * L_4 = ___endAction2;
		NullCheck(L_3);
		L_3->set_endAction_1(L_4);
		Func_3_t1604491142 * L_5 = ___beginMethod0;
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		ArgumentNullException_t628810857 * L_6 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_6, (String_t*)_stringLiteral784393026, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0025:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_7 = V_0;
		NullCheck(L_7);
		Func_2_t2344073568 * L_8 = (Func_2_t2344073568 *)L_7->get_endFunction_0();
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t1801450390 * L_10 = (Action_1_t1801450390 *)L_9->get_endAction_1();
		if (L_10)
		{
			goto IL_0046;
		}
	}
	{
		ArgumentNullException_t628810857 * L_11 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_11, (String_t*)_stringLiteral3599838238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0046:
	{
		int32_t L_12 = ___creationOptions4;
		TaskFactory_CheckFromAsyncOptions_m238110038(NULL /*static, unused*/, (int32_t)L_12, (bool)1, /*hidden argument*/NULL);
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_13 = V_0;
		Il2CppObject * L_14 = ___state3;
		int32_t L_15 = ___creationOptions4;
		Task_1_t1809478302 * L_16 = (Task_1_t1809478302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Task_1_t1809478302 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_16, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_13);
		L_13->set_promise_2(L_16);
		bool L_17 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_18 = V_0;
		NullCheck(L_18);
		Task_1_t1809478302 * L_19 = (Task_1_t1809478302 *)L_18->get_promise_2();
		NullCheck((Task_t1843236107 *)L_19);
		int32_t L_20 = Task_get_Id_m4106115082((Task_t1843236107 *)L_19, /*hidden argument*/NULL);
		Func_3_t1604491142 * L_21 = ___beginMethod0;
		NullCheck((Delegate_t3022476291 *)L_21);
		MethodInfo_t * L_22 = Delegate_get_Method_m2968370506((Delegate_t3022476291 *)L_21, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_22);
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2115721401, (String_t*)L_23, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCreation_m3376453187(NULL /*static, unused*/, (int32_t)0, (int32_t)L_20, (String_t*)L_24, (uint64_t)(((int64_t)((int64_t)0))), /*hidden argument*/NULL);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_25 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_25)
		{
			goto IL_00a4;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_26 = V_0;
		NullCheck(L_26);
		Task_1_t1809478302 * L_27 = (Task_1_t1809478302 *)L_26->get_promise_2();
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_AddToActiveTasks_m2680726720(NULL /*static, unused*/, (Task_t1843236107 *)L_27, /*hidden argument*/NULL);
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var);
			bool L_28 = BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_0119;
			}
		}

IL_00ae:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * L_29 = (U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			V_1 = (U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 *)L_29;
			U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * L_30 = V_1;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_31 = V_0;
			NullCheck(L_30);
			L_30->set_U3CU3Ef__refU242_1(L_31);
			U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * L_32 = V_1;
			AtomicBoolean_t379413895 * L_33 = (AtomicBoolean_t379413895 *)il2cpp_codegen_object_new(AtomicBoolean_t379413895_il2cpp_TypeInfo_var);
			AtomicBoolean__ctor_m845874490(L_33, /*hidden argument*/NULL);
			NullCheck(L_32);
			L_32->set_invoked_0(L_33);
			Func_3_t1604491142 * L_34 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * L_35 = V_1;
			IntPtr_t L_36;
			L_36.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			AsyncCallback_t163412349 * L_37 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_37, (Il2CppObject *)L_35, (IntPtr_t)L_36, /*hidden argument*/NULL);
			Il2CppObject * L_38 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_34);
			Il2CppObject * L_39 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_34, (AsyncCallback_t163412349 *)L_37, (Il2CppObject *)L_38, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_2 = (Il2CppObject *)L_39;
			Il2CppObject * L_40 = V_2;
			if (!L_40)
			{
				goto IL_0114;
			}
		}

IL_00e0:
		{
			Il2CppObject * L_41 = V_2;
			NullCheck((Il2CppObject *)L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t1999651008_il2cpp_TypeInfo_var, (Il2CppObject *)L_41);
			if (!L_42)
			{
				goto IL_0114;
			}
		}

IL_00eb:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t956545888 * L_43 = V_1;
			NullCheck(L_43);
			AtomicBoolean_t379413895 * L_44 = (AtomicBoolean_t379413895 *)L_43->get_invoked_0();
			NullCheck((AtomicBoolean_t379413895 *)L_44);
			bool L_45 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_44, /*hidden argument*/NULL);
			if (!L_45)
			{
				goto IL_0114;
			}
		}

IL_00fb:
		{
			Il2CppObject * L_46 = V_2;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_47 = V_0;
			NullCheck(L_47);
			Func_2_t2344073568 * L_48 = (Func_2_t2344073568 *)L_47->get_endFunction_0();
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_49 = V_0;
			NullCheck(L_49);
			Action_1_t1801450390 * L_50 = (Action_1_t1801450390 *)L_49->get_endAction_1();
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_51 = V_0;
			NullCheck(L_51);
			Task_1_t1809478302 * L_52 = (Task_1_t1809478302 *)L_51->get_promise_2();
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2344073568 *, Action_1_t1801450390 *, Task_1_t1809478302 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_46, (Func_2_t2344073568 *)L_48, (Action_1_t1801450390 *)L_50, (Task_1_t1809478302 *)L_52, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		}

IL_0114:
		{
			goto IL_012d;
		}

IL_0119:
		{
			Func_3_t1604491142 * L_53 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_54 = V_0;
			IntPtr_t L_55;
			L_55.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			AsyncCallback_t163412349 * L_56 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_56, (Il2CppObject *)L_54, (IntPtr_t)L_55, /*hidden argument*/NULL);
			Il2CppObject * L_57 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_53);
			Il2CppObject * L_58 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_53, (AsyncCallback_t163412349 *)L_56, (Il2CppObject *)L_57, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_3 = (Il2CppObject *)L_58;
		}

IL_012d:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0132;
		throw e;
	}

CATCH_0132:
	{ // begin catch(System.Object)
		{
			bool L_59 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_59)
			{
				goto IL_014f;
			}
		}

IL_013d:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_60 = V_0;
			NullCheck(L_60);
			Task_1_t1809478302 * L_61 = (Task_1_t1809478302 *)L_60->get_promise_2();
			NullCheck((Task_t1843236107 *)L_61);
			int32_t L_62 = Task_get_Id_m4106115082((Task_t1843236107 *)L_61, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_62, (int32_t)3, /*hidden argument*/NULL);
		}

IL_014f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_63 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_63)
			{
				goto IL_0169;
			}
		}

IL_0159:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_64 = V_0;
			NullCheck(L_64);
			Task_1_t1809478302 * L_65 = (Task_1_t1809478302 *)L_64->get_promise_2();
			NullCheck((Task_t1843236107 *)L_65);
			int32_t L_66 = Task_get_Id_m4106115082((Task_t1843236107 *)L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_66, /*hidden argument*/NULL);
		}

IL_0169:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_67 = V_0;
			NullCheck(L_67);
			Task_1_t1809478302 * L_68 = (Task_1_t1809478302 *)L_67->get_promise_2();
			Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
			Il2CppObject * L_69 = V_4;
			NullCheck((Task_1_t1809478302 *)L_68);
			((  bool (*) (Task_1_t1809478302 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t1809478302 *)L_68, (Il2CppObject *)L_69, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}
	} // end catch (depth: 1)

IL_0181:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717 * L_70 = V_0;
		NullCheck(L_70);
		Task_1_t1809478302 * L_71 = (Task_1_t1809478302 *)L_70->get_promise_2();
		return L_71;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
