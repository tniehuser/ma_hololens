﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_AwaitTaskContinuat2160930432.h"

// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t296893742;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t3857790437;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.SynchronizationContextAwaitTaskContinuation
struct  SynchronizationContextAwaitTaskContinuation_t2253979809  : public AwaitTaskContinuation_t2160930432
{
public:
	// System.Threading.SynchronizationContext System.Threading.Tasks.SynchronizationContextAwaitTaskContinuation::m_syncContext
	SynchronizationContext_t3857790437 * ___m_syncContext_7;

public:
	inline static int32_t get_offset_of_m_syncContext_7() { return static_cast<int32_t>(offsetof(SynchronizationContextAwaitTaskContinuation_t2253979809, ___m_syncContext_7)); }
	inline SynchronizationContext_t3857790437 * get_m_syncContext_7() const { return ___m_syncContext_7; }
	inline SynchronizationContext_t3857790437 ** get_address_of_m_syncContext_7() { return &___m_syncContext_7; }
	inline void set_m_syncContext_7(SynchronizationContext_t3857790437 * value)
	{
		___m_syncContext_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_syncContext_7, value);
	}
};

struct SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields
{
public:
	// System.Threading.SendOrPostCallback System.Threading.Tasks.SynchronizationContextAwaitTaskContinuation::s_postCallback
	SendOrPostCallback_t296893742 * ___s_postCallback_5;
	// System.Threading.ContextCallback System.Threading.Tasks.SynchronizationContextAwaitTaskContinuation::s_postActionCallback
	ContextCallback_t2287130692 * ___s_postActionCallback_6;
	// System.Threading.ContextCallback System.Threading.Tasks.SynchronizationContextAwaitTaskContinuation::<>f__mg$cache0
	ContextCallback_t2287130692 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_s_postCallback_5() { return static_cast<int32_t>(offsetof(SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields, ___s_postCallback_5)); }
	inline SendOrPostCallback_t296893742 * get_s_postCallback_5() const { return ___s_postCallback_5; }
	inline SendOrPostCallback_t296893742 ** get_address_of_s_postCallback_5() { return &___s_postCallback_5; }
	inline void set_s_postCallback_5(SendOrPostCallback_t296893742 * value)
	{
		___s_postCallback_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_postCallback_5, value);
	}

	inline static int32_t get_offset_of_s_postActionCallback_6() { return static_cast<int32_t>(offsetof(SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields, ___s_postActionCallback_6)); }
	inline ContextCallback_t2287130692 * get_s_postActionCallback_6() const { return ___s_postActionCallback_6; }
	inline ContextCallback_t2287130692 ** get_address_of_s_postActionCallback_6() { return &___s_postActionCallback_6; }
	inline void set_s_postActionCallback_6(ContextCallback_t2287130692 * value)
	{
		___s_postActionCallback_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_postActionCallback_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline ContextCallback_t2287130692 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline ContextCallback_t2287130692 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(ContextCallback_t2287130692 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
