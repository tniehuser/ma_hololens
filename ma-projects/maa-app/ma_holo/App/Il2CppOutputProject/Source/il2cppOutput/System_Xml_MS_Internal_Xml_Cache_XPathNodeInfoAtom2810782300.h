﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t339325318;
// System.Xml.XPath.XPathDocument
struct XPathDocument_t1328191420;
// MS.Internal.Xml.Cache.XPathNodePageInfo
struct XPathNodePageInfo_t617838178;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct  XPathNodeInfoAtom_t2810782300  : public Il2CppObject
{
public:
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::localName
	String_t* ___localName_0;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.Cache.XPathNodeInfoAtom::baseUri
	String_t* ___baseUri_3;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageParent
	XPathNodeU5BU5D_t339325318* ___pageParent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSibling
	XPathNodeU5BU5D_t339325318* ___pageSibling_5;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageSimilar
	XPathNodeU5BU5D_t339325318* ___pageSimilar_6;
	// System.Xml.XPath.XPathDocument MS.Internal.Xml.Cache.XPathNodeInfoAtom::doc
	XPathDocument_t1328191420 * ___doc_7;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::lineNumBase
	int32_t ___lineNumBase_8;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::linePosBase
	int32_t ___linePosBase_9;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::hashCode
	int32_t ___hashCode_10;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodeInfoAtom::localNameHash
	int32_t ___localNameHash_11;
	// MS.Internal.Xml.Cache.XPathNodePageInfo MS.Internal.Xml.Cache.XPathNodeInfoAtom::pageInfo
	XPathNodePageInfo_t617838178 * ___pageInfo_12;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier(&___localName_0, value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceUri_1, value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_2, value);
	}

	inline static int32_t get_offset_of_baseUri_3() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___baseUri_3)); }
	inline String_t* get_baseUri_3() const { return ___baseUri_3; }
	inline String_t** get_address_of_baseUri_3() { return &___baseUri_3; }
	inline void set_baseUri_3(String_t* value)
	{
		___baseUri_3 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_3, value);
	}

	inline static int32_t get_offset_of_pageParent_4() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___pageParent_4)); }
	inline XPathNodeU5BU5D_t339325318* get_pageParent_4() const { return ___pageParent_4; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageParent_4() { return &___pageParent_4; }
	inline void set_pageParent_4(XPathNodeU5BU5D_t339325318* value)
	{
		___pageParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___pageParent_4, value);
	}

	inline static int32_t get_offset_of_pageSibling_5() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___pageSibling_5)); }
	inline XPathNodeU5BU5D_t339325318* get_pageSibling_5() const { return ___pageSibling_5; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageSibling_5() { return &___pageSibling_5; }
	inline void set_pageSibling_5(XPathNodeU5BU5D_t339325318* value)
	{
		___pageSibling_5 = value;
		Il2CppCodeGenWriteBarrier(&___pageSibling_5, value);
	}

	inline static int32_t get_offset_of_pageSimilar_6() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___pageSimilar_6)); }
	inline XPathNodeU5BU5D_t339325318* get_pageSimilar_6() const { return ___pageSimilar_6; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageSimilar_6() { return &___pageSimilar_6; }
	inline void set_pageSimilar_6(XPathNodeU5BU5D_t339325318* value)
	{
		___pageSimilar_6 = value;
		Il2CppCodeGenWriteBarrier(&___pageSimilar_6, value);
	}

	inline static int32_t get_offset_of_doc_7() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___doc_7)); }
	inline XPathDocument_t1328191420 * get_doc_7() const { return ___doc_7; }
	inline XPathDocument_t1328191420 ** get_address_of_doc_7() { return &___doc_7; }
	inline void set_doc_7(XPathDocument_t1328191420 * value)
	{
		___doc_7 = value;
		Il2CppCodeGenWriteBarrier(&___doc_7, value);
	}

	inline static int32_t get_offset_of_lineNumBase_8() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___lineNumBase_8)); }
	inline int32_t get_lineNumBase_8() const { return ___lineNumBase_8; }
	inline int32_t* get_address_of_lineNumBase_8() { return &___lineNumBase_8; }
	inline void set_lineNumBase_8(int32_t value)
	{
		___lineNumBase_8 = value;
	}

	inline static int32_t get_offset_of_linePosBase_9() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___linePosBase_9)); }
	inline int32_t get_linePosBase_9() const { return ___linePosBase_9; }
	inline int32_t* get_address_of_linePosBase_9() { return &___linePosBase_9; }
	inline void set_linePosBase_9(int32_t value)
	{
		___linePosBase_9 = value;
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_localNameHash_11() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___localNameHash_11)); }
	inline int32_t get_localNameHash_11() const { return ___localNameHash_11; }
	inline int32_t* get_address_of_localNameHash_11() { return &___localNameHash_11; }
	inline void set_localNameHash_11(int32_t value)
	{
		___localNameHash_11 = value;
	}

	inline static int32_t get_offset_of_pageInfo_12() { return static_cast<int32_t>(offsetof(XPathNodeInfoAtom_t2810782300, ___pageInfo_12)); }
	inline XPathNodePageInfo_t617838178 * get_pageInfo_12() const { return ___pageInfo_12; }
	inline XPathNodePageInfo_t617838178 ** get_address_of_pageInfo_12() { return &___pageInfo_12; }
	inline void set_pageInfo_12(XPathNodePageInfo_t617838178 * value)
	{
		___pageInfo_12 = value;
		Il2CppCodeGenWriteBarrier(&___pageInfo_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
