﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"

// System.Xml.XmlNodeReaderNavigator
struct XmlNodeReaderNavigator_t4234558191;
// System.Xml.ReadContentAsBinaryHelper
struct ReadContentAsBinaryHelper_t1064946902;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReader
struct  XmlNodeReader_t1022603664  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlNodeReaderNavigator System.Xml.XmlNodeReader::readerNav
	XmlNodeReaderNavigator_t4234558191 * ___readerNav_3;
	// System.Xml.XmlNodeType System.Xml.XmlNodeReader::nodeType
	int32_t ___nodeType_4;
	// System.Int32 System.Xml.XmlNodeReader::curDepth
	int32_t ___curDepth_5;
	// System.Xml.ReadState System.Xml.XmlNodeReader::readState
	int32_t ___readState_6;
	// System.Boolean System.Xml.XmlNodeReader::fEOF
	bool ___fEOF_7;
	// System.Boolean System.Xml.XmlNodeReader::bResolveEntity
	bool ___bResolveEntity_8;
	// System.Boolean System.Xml.XmlNodeReader::bStartFromDocument
	bool ___bStartFromDocument_9;
	// System.Boolean System.Xml.XmlNodeReader::bInReadBinary
	bool ___bInReadBinary_10;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XmlNodeReader::readBinaryHelper
	ReadContentAsBinaryHelper_t1064946902 * ___readBinaryHelper_11;

public:
	inline static int32_t get_offset_of_readerNav_3() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___readerNav_3)); }
	inline XmlNodeReaderNavigator_t4234558191 * get_readerNav_3() const { return ___readerNav_3; }
	inline XmlNodeReaderNavigator_t4234558191 ** get_address_of_readerNav_3() { return &___readerNav_3; }
	inline void set_readerNav_3(XmlNodeReaderNavigator_t4234558191 * value)
	{
		___readerNav_3 = value;
		Il2CppCodeGenWriteBarrier(&___readerNav_3, value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_curDepth_5() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___curDepth_5)); }
	inline int32_t get_curDepth_5() const { return ___curDepth_5; }
	inline int32_t* get_address_of_curDepth_5() { return &___curDepth_5; }
	inline void set_curDepth_5(int32_t value)
	{
		___curDepth_5 = value;
	}

	inline static int32_t get_offset_of_readState_6() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___readState_6)); }
	inline int32_t get_readState_6() const { return ___readState_6; }
	inline int32_t* get_address_of_readState_6() { return &___readState_6; }
	inline void set_readState_6(int32_t value)
	{
		___readState_6 = value;
	}

	inline static int32_t get_offset_of_fEOF_7() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___fEOF_7)); }
	inline bool get_fEOF_7() const { return ___fEOF_7; }
	inline bool* get_address_of_fEOF_7() { return &___fEOF_7; }
	inline void set_fEOF_7(bool value)
	{
		___fEOF_7 = value;
	}

	inline static int32_t get_offset_of_bResolveEntity_8() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___bResolveEntity_8)); }
	inline bool get_bResolveEntity_8() const { return ___bResolveEntity_8; }
	inline bool* get_address_of_bResolveEntity_8() { return &___bResolveEntity_8; }
	inline void set_bResolveEntity_8(bool value)
	{
		___bResolveEntity_8 = value;
	}

	inline static int32_t get_offset_of_bStartFromDocument_9() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___bStartFromDocument_9)); }
	inline bool get_bStartFromDocument_9() const { return ___bStartFromDocument_9; }
	inline bool* get_address_of_bStartFromDocument_9() { return &___bStartFromDocument_9; }
	inline void set_bStartFromDocument_9(bool value)
	{
		___bStartFromDocument_9 = value;
	}

	inline static int32_t get_offset_of_bInReadBinary_10() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___bInReadBinary_10)); }
	inline bool get_bInReadBinary_10() const { return ___bInReadBinary_10; }
	inline bool* get_address_of_bInReadBinary_10() { return &___bInReadBinary_10; }
	inline void set_bInReadBinary_10(bool value)
	{
		___bInReadBinary_10 = value;
	}

	inline static int32_t get_offset_of_readBinaryHelper_11() { return static_cast<int32_t>(offsetof(XmlNodeReader_t1022603664, ___readBinaryHelper_11)); }
	inline ReadContentAsBinaryHelper_t1064946902 * get_readBinaryHelper_11() const { return ___readBinaryHelper_11; }
	inline ReadContentAsBinaryHelper_t1064946902 ** get_address_of_readBinaryHelper_11() { return &___readBinaryHelper_11; }
	inline void set_readBinaryHelper_11(ReadContentAsBinaryHelper_t1064946902 * value)
	{
		___readBinaryHelper_11 = value;
		Il2CppCodeGenWriteBarrier(&___readBinaryHelper_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
