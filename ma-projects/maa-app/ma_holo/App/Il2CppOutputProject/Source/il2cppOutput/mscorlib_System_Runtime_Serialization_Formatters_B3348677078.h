﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4279057407.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[]
struct BinaryTypeEnumU5BU5D_t1351797255;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped
struct  BinaryObjectWithMapTyped_t3348677078  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::objectId
	int32_t ___objectId_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::name
	String_t* ___name_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::numMembers
	int32_t ___numMembers_3;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::memberNames
	StringU5BU5D_t1642385972* ___memberNames_4;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::binaryTypeEnumA
	BinaryTypeEnumU5BU5D_t1351797255* ___binaryTypeEnumA_5;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::typeInformationA
	ObjectU5BU5D_t3614634134* ___typeInformationA_6;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::memberAssemIds
	Int32U5BU5D_t3030399641* ___memberAssemIds_7;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::assemId
	int32_t ___assemId_8;

public:
	inline static int32_t get_offset_of_binaryHeaderEnum_0() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___binaryHeaderEnum_0)); }
	inline int32_t get_binaryHeaderEnum_0() const { return ___binaryHeaderEnum_0; }
	inline int32_t* get_address_of_binaryHeaderEnum_0() { return &___binaryHeaderEnum_0; }
	inline void set_binaryHeaderEnum_0(int32_t value)
	{
		___binaryHeaderEnum_0 = value;
	}

	inline static int32_t get_offset_of_objectId_1() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___objectId_1)); }
	inline int32_t get_objectId_1() const { return ___objectId_1; }
	inline int32_t* get_address_of_objectId_1() { return &___objectId_1; }
	inline void set_objectId_1(int32_t value)
	{
		___objectId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_numMembers_3() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___numMembers_3)); }
	inline int32_t get_numMembers_3() const { return ___numMembers_3; }
	inline int32_t* get_address_of_numMembers_3() { return &___numMembers_3; }
	inline void set_numMembers_3(int32_t value)
	{
		___numMembers_3 = value;
	}

	inline static int32_t get_offset_of_memberNames_4() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___memberNames_4)); }
	inline StringU5BU5D_t1642385972* get_memberNames_4() const { return ___memberNames_4; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_4() { return &___memberNames_4; }
	inline void set_memberNames_4(StringU5BU5D_t1642385972* value)
	{
		___memberNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_4, value);
	}

	inline static int32_t get_offset_of_binaryTypeEnumA_5() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___binaryTypeEnumA_5)); }
	inline BinaryTypeEnumU5BU5D_t1351797255* get_binaryTypeEnumA_5() const { return ___binaryTypeEnumA_5; }
	inline BinaryTypeEnumU5BU5D_t1351797255** get_address_of_binaryTypeEnumA_5() { return &___binaryTypeEnumA_5; }
	inline void set_binaryTypeEnumA_5(BinaryTypeEnumU5BU5D_t1351797255* value)
	{
		___binaryTypeEnumA_5 = value;
		Il2CppCodeGenWriteBarrier(&___binaryTypeEnumA_5, value);
	}

	inline static int32_t get_offset_of_typeInformationA_6() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___typeInformationA_6)); }
	inline ObjectU5BU5D_t3614634134* get_typeInformationA_6() const { return ___typeInformationA_6; }
	inline ObjectU5BU5D_t3614634134** get_address_of_typeInformationA_6() { return &___typeInformationA_6; }
	inline void set_typeInformationA_6(ObjectU5BU5D_t3614634134* value)
	{
		___typeInformationA_6 = value;
		Il2CppCodeGenWriteBarrier(&___typeInformationA_6, value);
	}

	inline static int32_t get_offset_of_memberAssemIds_7() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___memberAssemIds_7)); }
	inline Int32U5BU5D_t3030399641* get_memberAssemIds_7() const { return ___memberAssemIds_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_memberAssemIds_7() { return &___memberAssemIds_7; }
	inline void set_memberAssemIds_7(Int32U5BU5D_t3030399641* value)
	{
		___memberAssemIds_7 = value;
		Il2CppCodeGenWriteBarrier(&___memberAssemIds_7, value);
	}

	inline static int32_t get_offset_of_assemId_8() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t3348677078, ___assemId_8)); }
	inline int32_t get_assemId_8() const { return ___assemId_8; }
	inline int32_t* get_address_of_assemId_8() { return &___assemId_8; }
	inline void set_assemId_8(int32_t value)
	{
		___assemId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
