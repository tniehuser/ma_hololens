﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Timeout
struct  Timeout_t35240459  : public Il2CppObject
{
public:

public:
};

struct Timeout_t35240459_StaticFields
{
public:
	// System.TimeSpan System.Threading.Timeout::InfiniteTimeSpan
	TimeSpan_t3430258949  ___InfiniteTimeSpan_0;

public:
	inline static int32_t get_offset_of_InfiniteTimeSpan_0() { return static_cast<int32_t>(offsetof(Timeout_t35240459_StaticFields, ___InfiniteTimeSpan_0)); }
	inline TimeSpan_t3430258949  get_InfiniteTimeSpan_0() const { return ___InfiniteTimeSpan_0; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteTimeSpan_0() { return &___InfiniteTimeSpan_0; }
	inline void set_InfiniteTimeSpan_0(TimeSpan_t3430258949  value)
	{
		___InfiniteTimeSpan_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
