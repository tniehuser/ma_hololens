﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList/SyncArrayList
struct  SyncArrayList_t249867678  : public ArrayList_t4252133567
{
public:
	// System.Collections.ArrayList System.Collections.ArrayList/SyncArrayList::_list
	ArrayList_t4252133567 * ____list_5;
	// System.Object System.Collections.ArrayList/SyncArrayList::_root
	Il2CppObject * ____root_6;

public:
	inline static int32_t get_offset_of__list_5() { return static_cast<int32_t>(offsetof(SyncArrayList_t249867678, ____list_5)); }
	inline ArrayList_t4252133567 * get__list_5() const { return ____list_5; }
	inline ArrayList_t4252133567 ** get_address_of__list_5() { return &____list_5; }
	inline void set__list_5(ArrayList_t4252133567 * value)
	{
		____list_5 = value;
		Il2CppCodeGenWriteBarrier(&____list_5, value);
	}

	inline static int32_t get_offset_of__root_6() { return static_cast<int32_t>(offsetof(SyncArrayList_t249867678, ____root_6)); }
	inline Il2CppObject * get__root_6() const { return ____root_6; }
	inline Il2CppObject ** get_address_of__root_6() { return &____root_6; }
	inline void set__root_6(Il2CppObject * value)
	{
		____root_6 = value;
		Il2CppCodeGenWriteBarrier(&____root_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
