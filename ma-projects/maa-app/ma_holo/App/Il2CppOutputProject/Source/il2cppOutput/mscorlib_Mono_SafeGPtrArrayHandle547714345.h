﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeGPtrArrayHandle1303258952.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.SafeGPtrArrayHandle
struct  SafeGPtrArrayHandle_t547714345 
{
public:
	// Mono.RuntimeGPtrArrayHandle Mono.SafeGPtrArrayHandle::handle
	RuntimeGPtrArrayHandle_t1303258952  ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(SafeGPtrArrayHandle_t547714345, ___handle_0)); }
	inline RuntimeGPtrArrayHandle_t1303258952  get_handle_0() const { return ___handle_0; }
	inline RuntimeGPtrArrayHandle_t1303258952 * get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(RuntimeGPtrArrayHandle_t1303258952  value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.SafeGPtrArrayHandle
struct SafeGPtrArrayHandle_t547714345_marshaled_pinvoke
{
	RuntimeGPtrArrayHandle_t1303258952_marshaled_pinvoke ___handle_0;
};
// Native definition for COM marshalling of Mono.SafeGPtrArrayHandle
struct SafeGPtrArrayHandle_t547714345_marshaled_com
{
	RuntimeGPtrArrayHandle_t1303258952_marshaled_com ___handle_0;
};
