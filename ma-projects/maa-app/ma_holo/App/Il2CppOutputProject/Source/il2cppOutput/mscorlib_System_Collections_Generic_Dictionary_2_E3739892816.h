﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Resources_ResourceLocator2156390884.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>
struct  Entry_t3739892816 
{
public:
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::hashCode
	int32_t ___hashCode_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Entry::next
	int32_t ___next_1;
	// TKey System.Collections.Generic.Dictionary`2/Entry::key
	Il2CppObject * ___key_2;
	// TValue System.Collections.Generic.Dictionary`2/Entry::value
	ResourceLocator_t2156390884  ___value_3;

public:
	inline static int32_t get_offset_of_hashCode_0() { return static_cast<int32_t>(offsetof(Entry_t3739892816, ___hashCode_0)); }
	inline int32_t get_hashCode_0() const { return ___hashCode_0; }
	inline int32_t* get_address_of_hashCode_0() { return &___hashCode_0; }
	inline void set_hashCode_0(int32_t value)
	{
		___hashCode_0 = value;
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Entry_t3739892816, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(Entry_t3739892816, ___key_2)); }
	inline Il2CppObject * get_key_2() const { return ___key_2; }
	inline Il2CppObject ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(Il2CppObject * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier(&___key_2, value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(Entry_t3739892816, ___value_3)); }
	inline ResourceLocator_t2156390884  get_value_3() const { return ___value_3; }
	inline ResourceLocator_t2156390884 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(ResourceLocator_t2156390884  value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
