﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.LinkedList`1<System.WeakReference>
struct LinkedList_1_t1382113796;
// System.Collections.Generic.LinkedListNode`1<System.WeakReference>
struct LinkedListNode_1_t4268478776;
// System.WeakReference
struct WeakReference_t1077405567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedListNode`1<System.WeakReference>
struct  LinkedListNode_1_t4268478776  : public Il2CppObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t1382113796 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t4268478776 * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t4268478776 * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	WeakReference_t1077405567 * ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t4268478776, ___list_0)); }
	inline LinkedList_1_t1382113796 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t1382113796 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t1382113796 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t4268478776, ___next_1)); }
	inline LinkedListNode_1_t4268478776 * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_t4268478776 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_t4268478776 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier(&___next_1, value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t4268478776, ___prev_2)); }
	inline LinkedListNode_1_t4268478776 * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_t4268478776 ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_t4268478776 * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier(&___prev_2, value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t4268478776, ___item_3)); }
	inline WeakReference_t1077405567 * get_item_3() const { return ___item_3; }
	inline WeakReference_t1077405567 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(WeakReference_t1077405567 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
