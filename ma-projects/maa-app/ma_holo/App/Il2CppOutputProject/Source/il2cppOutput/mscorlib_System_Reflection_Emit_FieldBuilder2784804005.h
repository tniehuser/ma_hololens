﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_FieldAttributes1122705193.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t4270021860;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.FieldBuilder
struct  FieldBuilder_t2784804005  : public FieldInfo_t
{
public:
	// System.Reflection.FieldAttributes System.Reflection.Emit.FieldBuilder::attrs
	int32_t ___attrs_0;
	// System.Type System.Reflection.Emit.FieldBuilder::type
	Type_t * ___type_1;
	// System.String System.Reflection.Emit.FieldBuilder::name
	String_t* ___name_2;
	// System.Object System.Reflection.Emit.FieldBuilder::def_value
	Il2CppObject * ___def_value_3;
	// System.Int32 System.Reflection.Emit.FieldBuilder::offset
	int32_t ___offset_4;
	// System.Int32 System.Reflection.Emit.FieldBuilder::table_idx
	int32_t ___table_idx_5;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.FieldBuilder::typeb
	TypeBuilder_t3308873219 * ___typeb_6;
	// System.Byte[] System.Reflection.Emit.FieldBuilder::rva_data
	ByteU5BU5D_t3397334013* ___rva_data_7;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.FieldBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_8;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.FieldBuilder::marshal_info
	UnmanagedMarshal_t4270021860 * ___marshal_info_9;
	// System.RuntimeFieldHandle System.Reflection.Emit.FieldBuilder::handle
	RuntimeFieldHandle_t2331729674  ___handle_10;
	// System.Type[] System.Reflection.Emit.FieldBuilder::modReq
	TypeU5BU5D_t1664964607* ___modReq_11;
	// System.Type[] System.Reflection.Emit.FieldBuilder::modOpt
	TypeU5BU5D_t1664964607* ___modOpt_12;

public:
	inline static int32_t get_offset_of_attrs_0() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___attrs_0)); }
	inline int32_t get_attrs_0() const { return ___attrs_0; }
	inline int32_t* get_address_of_attrs_0() { return &___attrs_0; }
	inline void set_attrs_0(int32_t value)
	{
		___attrs_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_def_value_3() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___def_value_3)); }
	inline Il2CppObject * get_def_value_3() const { return ___def_value_3; }
	inline Il2CppObject ** get_address_of_def_value_3() { return &___def_value_3; }
	inline void set_def_value_3(Il2CppObject * value)
	{
		___def_value_3 = value;
		Il2CppCodeGenWriteBarrier(&___def_value_3, value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_table_idx_5() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___table_idx_5)); }
	inline int32_t get_table_idx_5() const { return ___table_idx_5; }
	inline int32_t* get_address_of_table_idx_5() { return &___table_idx_5; }
	inline void set_table_idx_5(int32_t value)
	{
		___table_idx_5 = value;
	}

	inline static int32_t get_offset_of_typeb_6() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___typeb_6)); }
	inline TypeBuilder_t3308873219 * get_typeb_6() const { return ___typeb_6; }
	inline TypeBuilder_t3308873219 ** get_address_of_typeb_6() { return &___typeb_6; }
	inline void set_typeb_6(TypeBuilder_t3308873219 * value)
	{
		___typeb_6 = value;
		Il2CppCodeGenWriteBarrier(&___typeb_6, value);
	}

	inline static int32_t get_offset_of_rva_data_7() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___rva_data_7)); }
	inline ByteU5BU5D_t3397334013* get_rva_data_7() const { return ___rva_data_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_rva_data_7() { return &___rva_data_7; }
	inline void set_rva_data_7(ByteU5BU5D_t3397334013* value)
	{
		___rva_data_7 = value;
		Il2CppCodeGenWriteBarrier(&___rva_data_7, value);
	}

	inline static int32_t get_offset_of_cattrs_8() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___cattrs_8)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_8() const { return ___cattrs_8; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_8() { return &___cattrs_8; }
	inline void set_cattrs_8(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_8 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_8, value);
	}

	inline static int32_t get_offset_of_marshal_info_9() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___marshal_info_9)); }
	inline UnmanagedMarshal_t4270021860 * get_marshal_info_9() const { return ___marshal_info_9; }
	inline UnmanagedMarshal_t4270021860 ** get_address_of_marshal_info_9() { return &___marshal_info_9; }
	inline void set_marshal_info_9(UnmanagedMarshal_t4270021860 * value)
	{
		___marshal_info_9 = value;
		Il2CppCodeGenWriteBarrier(&___marshal_info_9, value);
	}

	inline static int32_t get_offset_of_handle_10() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___handle_10)); }
	inline RuntimeFieldHandle_t2331729674  get_handle_10() const { return ___handle_10; }
	inline RuntimeFieldHandle_t2331729674 * get_address_of_handle_10() { return &___handle_10; }
	inline void set_handle_10(RuntimeFieldHandle_t2331729674  value)
	{
		___handle_10 = value;
	}

	inline static int32_t get_offset_of_modReq_11() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___modReq_11)); }
	inline TypeU5BU5D_t1664964607* get_modReq_11() const { return ___modReq_11; }
	inline TypeU5BU5D_t1664964607** get_address_of_modReq_11() { return &___modReq_11; }
	inline void set_modReq_11(TypeU5BU5D_t1664964607* value)
	{
		___modReq_11 = value;
		Il2CppCodeGenWriteBarrier(&___modReq_11, value);
	}

	inline static int32_t get_offset_of_modOpt_12() { return static_cast<int32_t>(offsetof(FieldBuilder_t2784804005, ___modOpt_12)); }
	inline TypeU5BU5D_t1664964607* get_modOpt_12() const { return ___modOpt_12; }
	inline TypeU5BU5D_t1664964607** get_address_of_modOpt_12() { return &___modOpt_12; }
	inline void set_modOpt_12(TypeU5BU5D_t1664964607* value)
	{
		___modOpt_12 = value;
		Il2CppCodeGenWriteBarrier(&___modOpt_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
