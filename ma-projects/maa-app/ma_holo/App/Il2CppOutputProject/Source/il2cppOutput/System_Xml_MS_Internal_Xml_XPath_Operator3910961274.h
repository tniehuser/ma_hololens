﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"
#include "System_Xml_MS_Internal_Xml_XPath_Operator_Op1831748991.h"

// MS.Internal.Xml.XPath.Operator/Op[]
struct OpU5BU5D_t1888866726;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2002670936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Operator
struct  Operator_t3910961274  : public AstNode_t2002670936
{
public:
	// MS.Internal.Xml.XPath.Operator/Op MS.Internal.Xml.XPath.Operator::opType
	int32_t ___opType_1;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd1
	AstNode_t2002670936 * ___opnd1_2;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Operator::opnd2
	AstNode_t2002670936 * ___opnd2_3;

public:
	inline static int32_t get_offset_of_opType_1() { return static_cast<int32_t>(offsetof(Operator_t3910961274, ___opType_1)); }
	inline int32_t get_opType_1() const { return ___opType_1; }
	inline int32_t* get_address_of_opType_1() { return &___opType_1; }
	inline void set_opType_1(int32_t value)
	{
		___opType_1 = value;
	}

	inline static int32_t get_offset_of_opnd1_2() { return static_cast<int32_t>(offsetof(Operator_t3910961274, ___opnd1_2)); }
	inline AstNode_t2002670936 * get_opnd1_2() const { return ___opnd1_2; }
	inline AstNode_t2002670936 ** get_address_of_opnd1_2() { return &___opnd1_2; }
	inline void set_opnd1_2(AstNode_t2002670936 * value)
	{
		___opnd1_2 = value;
		Il2CppCodeGenWriteBarrier(&___opnd1_2, value);
	}

	inline static int32_t get_offset_of_opnd2_3() { return static_cast<int32_t>(offsetof(Operator_t3910961274, ___opnd2_3)); }
	inline AstNode_t2002670936 * get_opnd2_3() const { return ___opnd2_3; }
	inline AstNode_t2002670936 ** get_address_of_opnd2_3() { return &___opnd2_3; }
	inline void set_opnd2_3(AstNode_t2002670936 * value)
	{
		___opnd2_3 = value;
		Il2CppCodeGenWriteBarrier(&___opnd2_3, value);
	}
};

struct Operator_t3910961274_StaticFields
{
public:
	// MS.Internal.Xml.XPath.Operator/Op[] MS.Internal.Xml.XPath.Operator::invertOp
	OpU5BU5D_t1888866726* ___invertOp_0;

public:
	inline static int32_t get_offset_of_invertOp_0() { return static_cast<int32_t>(offsetof(Operator_t3910961274_StaticFields, ___invertOp_0)); }
	inline OpU5BU5D_t1888866726* get_invertOp_0() const { return ___invertOp_0; }
	inline OpU5BU5D_t1888866726** get_address_of_invertOp_0() { return &___invertOp_0; }
	inline void set_invertOp_0(OpU5BU5D_t1888866726* value)
	{
		___invertOp_0 = value;
		Il2CppCodeGenWriteBarrier(&___invertOp_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
