﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Tasks_TaskCreationOptions547302442.h"
#include "mscorlib_System_Threading_Tasks_InternalTaskOption4086034348.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Action`1<System.Object>
struct Action_1_t2491248677;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0
struct  U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861  : public Il2CppObject
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0::root
	Task_t1843236107 * ___root_0;
	// System.Boolean System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0::replicasAreQuitting
	bool ___replicasAreQuitting_1;
	// System.Action`1<System.Object> System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0::taskReplicaDelegate
	Action_1_t2491248677 * ___taskReplicaDelegate_2;
	// System.Threading.Tasks.TaskCreationOptions System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0::creationOptionsForReplicas
	int32_t ___creationOptionsForReplicas_3;
	// System.Threading.Tasks.InternalTaskOptions System.Threading.Tasks.Task/<ExecuteSelfReplicating>c__AnonStorey0::internalOptionsForReplicas
	int32_t ___internalOptionsForReplicas_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861, ___root_0)); }
	inline Task_t1843236107 * get_root_0() const { return ___root_0; }
	inline Task_t1843236107 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(Task_t1843236107 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}

	inline static int32_t get_offset_of_replicasAreQuitting_1() { return static_cast<int32_t>(offsetof(U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861, ___replicasAreQuitting_1)); }
	inline bool get_replicasAreQuitting_1() const { return ___replicasAreQuitting_1; }
	inline bool* get_address_of_replicasAreQuitting_1() { return &___replicasAreQuitting_1; }
	inline void set_replicasAreQuitting_1(bool value)
	{
		___replicasAreQuitting_1 = value;
	}

	inline static int32_t get_offset_of_taskReplicaDelegate_2() { return static_cast<int32_t>(offsetof(U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861, ___taskReplicaDelegate_2)); }
	inline Action_1_t2491248677 * get_taskReplicaDelegate_2() const { return ___taskReplicaDelegate_2; }
	inline Action_1_t2491248677 ** get_address_of_taskReplicaDelegate_2() { return &___taskReplicaDelegate_2; }
	inline void set_taskReplicaDelegate_2(Action_1_t2491248677 * value)
	{
		___taskReplicaDelegate_2 = value;
		Il2CppCodeGenWriteBarrier(&___taskReplicaDelegate_2, value);
	}

	inline static int32_t get_offset_of_creationOptionsForReplicas_3() { return static_cast<int32_t>(offsetof(U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861, ___creationOptionsForReplicas_3)); }
	inline int32_t get_creationOptionsForReplicas_3() const { return ___creationOptionsForReplicas_3; }
	inline int32_t* get_address_of_creationOptionsForReplicas_3() { return &___creationOptionsForReplicas_3; }
	inline void set_creationOptionsForReplicas_3(int32_t value)
	{
		___creationOptionsForReplicas_3 = value;
	}

	inline static int32_t get_offset_of_internalOptionsForReplicas_4() { return static_cast<int32_t>(offsetof(U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861, ___internalOptionsForReplicas_4)); }
	inline int32_t get_internalOptionsForReplicas_4() const { return ___internalOptionsForReplicas_4; }
	inline int32_t* get_address_of_internalOptionsForReplicas_4() { return &___internalOptionsForReplicas_4; }
	inline void set_internalOptionsForReplicas_4(int32_t value)
	{
		___internalOptionsForReplicas_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
