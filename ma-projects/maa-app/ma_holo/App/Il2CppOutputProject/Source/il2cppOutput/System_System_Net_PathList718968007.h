﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.SortedList
struct SortedList_t3004938869;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList
struct  PathList_t718968007  : public Il2CppObject
{
public:
	// System.Collections.SortedList System.Net.PathList::m_list
	SortedList_t3004938869 * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(PathList_t718968007, ___m_list_0)); }
	inline SortedList_t3004938869 * get_m_list_0() const { return ___m_list_0; }
	inline SortedList_t3004938869 ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(SortedList_t3004938869 * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_list_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
