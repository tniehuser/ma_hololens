﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectIDGenerator
struct  ObjectIDGenerator_t3070747799  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.ObjectIDGenerator::m_currentCount
	int32_t ___m_currentCount_0;
	// System.Int32 System.Runtime.Serialization.ObjectIDGenerator::m_currentSize
	int32_t ___m_currentSize_1;
	// System.Int64[] System.Runtime.Serialization.ObjectIDGenerator::m_ids
	Int64U5BU5D_t717125112* ___m_ids_2;
	// System.Object[] System.Runtime.Serialization.ObjectIDGenerator::m_objs
	ObjectU5BU5D_t3614634134* ___m_objs_3;

public:
	inline static int32_t get_offset_of_m_currentCount_0() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t3070747799, ___m_currentCount_0)); }
	inline int32_t get_m_currentCount_0() const { return ___m_currentCount_0; }
	inline int32_t* get_address_of_m_currentCount_0() { return &___m_currentCount_0; }
	inline void set_m_currentCount_0(int32_t value)
	{
		___m_currentCount_0 = value;
	}

	inline static int32_t get_offset_of_m_currentSize_1() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t3070747799, ___m_currentSize_1)); }
	inline int32_t get_m_currentSize_1() const { return ___m_currentSize_1; }
	inline int32_t* get_address_of_m_currentSize_1() { return &___m_currentSize_1; }
	inline void set_m_currentSize_1(int32_t value)
	{
		___m_currentSize_1 = value;
	}

	inline static int32_t get_offset_of_m_ids_2() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t3070747799, ___m_ids_2)); }
	inline Int64U5BU5D_t717125112* get_m_ids_2() const { return ___m_ids_2; }
	inline Int64U5BU5D_t717125112** get_address_of_m_ids_2() { return &___m_ids_2; }
	inline void set_m_ids_2(Int64U5BU5D_t717125112* value)
	{
		___m_ids_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ids_2, value);
	}

	inline static int32_t get_offset_of_m_objs_3() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t3070747799, ___m_objs_3)); }
	inline ObjectU5BU5D_t3614634134* get_m_objs_3() const { return ___m_objs_3; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_objs_3() { return &___m_objs_3; }
	inline void set_m_objs_3(ObjectU5BU5D_t3614634134* value)
	{
		___m_objs_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_objs_3, value);
	}
};

struct ObjectIDGenerator_t3070747799_StaticFields
{
public:
	// System.Int32[] System.Runtime.Serialization.ObjectIDGenerator::sizes
	Int32U5BU5D_t3030399641* ___sizes_4;

public:
	inline static int32_t get_offset_of_sizes_4() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t3070747799_StaticFields, ___sizes_4)); }
	inline Int32U5BU5D_t3030399641* get_sizes_4() const { return ___sizes_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_sizes_4() { return &___sizes_4; }
	inline void set_sizes_4(Int32U5BU5D_t3030399641* value)
	{
		___sizes_4 = value;
		Il2CppCodeGenWriteBarrier(&___sizes_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
