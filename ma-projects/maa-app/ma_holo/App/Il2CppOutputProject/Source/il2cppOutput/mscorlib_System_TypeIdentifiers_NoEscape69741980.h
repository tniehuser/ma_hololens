﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_TypeNames_ATypeName2751073424.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeIdentifiers/NoEscape
struct  NoEscape_t69741980  : public ATypeName_t2751073424
{
public:
	// System.String System.TypeIdentifiers/NoEscape::simpleName
	String_t* ___simpleName_0;

public:
	inline static int32_t get_offset_of_simpleName_0() { return static_cast<int32_t>(offsetof(NoEscape_t69741980, ___simpleName_0)); }
	inline String_t* get_simpleName_0() const { return ___simpleName_0; }
	inline String_t** get_address_of_simpleName_0() { return &___simpleName_0; }
	inline void set_simpleName_0(String_t* value)
	{
		___simpleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___simpleName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
