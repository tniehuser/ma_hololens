﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Collections.Concurrent.ConcurrentDictionary`2/Node<System.Object,System.Object>
struct Node_t1921535786;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentDictionary`2/Node<System.Object,System.Object>
struct  Node_t1921535786  : public Il2CppObject
{
public:
	// TKey System.Collections.Concurrent.ConcurrentDictionary`2/Node::m_key
	Il2CppObject * ___m_key_0;
	// TValue System.Collections.Concurrent.ConcurrentDictionary`2/Node::m_value
	Il2CppObject * ___m_value_1;
	// System.Collections.Concurrent.ConcurrentDictionary`2/Node<TKey,TValue> modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Concurrent.ConcurrentDictionary`2/Node::m_next
	Node_t1921535786 * ___m_next_2;
	// System.Int32 System.Collections.Concurrent.ConcurrentDictionary`2/Node::m_hashcode
	int32_t ___m_hashcode_3;

public:
	inline static int32_t get_offset_of_m_key_0() { return static_cast<int32_t>(offsetof(Node_t1921535786, ___m_key_0)); }
	inline Il2CppObject * get_m_key_0() const { return ___m_key_0; }
	inline Il2CppObject ** get_address_of_m_key_0() { return &___m_key_0; }
	inline void set_m_key_0(Il2CppObject * value)
	{
		___m_key_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_key_0, value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(Node_t1921535786, ___m_value_1)); }
	inline Il2CppObject * get_m_value_1() const { return ___m_value_1; }
	inline Il2CppObject ** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(Il2CppObject * value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_value_1, value);
	}

	inline static int32_t get_offset_of_m_next_2() { return static_cast<int32_t>(offsetof(Node_t1921535786, ___m_next_2)); }
	inline Node_t1921535786 * get_m_next_2() const { return ___m_next_2; }
	inline Node_t1921535786 ** get_address_of_m_next_2() { return &___m_next_2; }
	inline void set_m_next_2(Node_t1921535786 * value)
	{
		___m_next_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_next_2, value);
	}

	inline static int32_t get_offset_of_m_hashcode_3() { return static_cast<int32_t>(offsetof(Node_t1921535786, ___m_hashcode_3)); }
	inline int32_t get_m_hashcode_3() const { return ___m_hashcode_3; }
	inline int32_t* get_address_of_m_hashcode_3() { return &___m_hashcode_3; }
	inline void set_m_hashcode_3(int32_t value)
	{
		___m_hashcode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
