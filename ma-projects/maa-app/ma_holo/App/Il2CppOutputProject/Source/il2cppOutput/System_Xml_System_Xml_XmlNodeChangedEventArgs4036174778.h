﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t4036174778  : public EventArgs_t3289624707
{
public:
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::action
	int32_t ___action_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::node
	XmlNode_t616554813 * ___node_2;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::oldParent
	XmlNode_t616554813 * ___oldParent_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::newParent
	XmlNode_t616554813 * ___newParent_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::oldValue
	String_t* ___oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::newValue
	String_t* ___newValue_6;

public:
	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_node_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___node_2)); }
	inline XmlNode_t616554813 * get_node_2() const { return ___node_2; }
	inline XmlNode_t616554813 ** get_address_of_node_2() { return &___node_2; }
	inline void set_node_2(XmlNode_t616554813 * value)
	{
		___node_2 = value;
		Il2CppCodeGenWriteBarrier(&___node_2, value);
	}

	inline static int32_t get_offset_of_oldParent_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___oldParent_3)); }
	inline XmlNode_t616554813 * get_oldParent_3() const { return ___oldParent_3; }
	inline XmlNode_t616554813 ** get_address_of_oldParent_3() { return &___oldParent_3; }
	inline void set_oldParent_3(XmlNode_t616554813 * value)
	{
		___oldParent_3 = value;
		Il2CppCodeGenWriteBarrier(&___oldParent_3, value);
	}

	inline static int32_t get_offset_of_newParent_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___newParent_4)); }
	inline XmlNode_t616554813 * get_newParent_4() const { return ___newParent_4; }
	inline XmlNode_t616554813 ** get_address_of_newParent_4() { return &___newParent_4; }
	inline void set_newParent_4(XmlNode_t616554813 * value)
	{
		___newParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___newParent_4, value);
	}

	inline static int32_t get_offset_of_oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___oldValue_5)); }
	inline String_t* get_oldValue_5() const { return ___oldValue_5; }
	inline String_t** get_address_of_oldValue_5() { return &___oldValue_5; }
	inline void set_oldValue_5(String_t* value)
	{
		___oldValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___oldValue_5, value);
	}

	inline static int32_t get_offset_of_newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t4036174778, ___newValue_6)); }
	inline String_t* get_newValue_6() const { return ___newValue_6; }
	inline String_t** get_address_of_newValue_6() { return &___newValue_6; }
	inline void set_newValue_6(String_t* value)
	{
		___newValue_6 = value;
		Il2CppCodeGenWriteBarrier(&___newValue_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
