﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate
struct ErrorDelegate_t3690655641;
// UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate
struct StatusDelegate_t2739717665;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Windows.Speech.PhraseRecognitionSystem
struct  PhraseRecognitionSystem_t1642125421  : public Il2CppObject
{
public:

public:
};

struct PhraseRecognitionSystem_t1642125421_StaticFields
{
public:
	// UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate UnityEngine.Windows.Speech.PhraseRecognitionSystem::OnError
	ErrorDelegate_t3690655641 * ___OnError_0;
	// UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate UnityEngine.Windows.Speech.PhraseRecognitionSystem::OnStatusChanged
	StatusDelegate_t2739717665 * ___OnStatusChanged_1;

public:
	inline static int32_t get_offset_of_OnError_0() { return static_cast<int32_t>(offsetof(PhraseRecognitionSystem_t1642125421_StaticFields, ___OnError_0)); }
	inline ErrorDelegate_t3690655641 * get_OnError_0() const { return ___OnError_0; }
	inline ErrorDelegate_t3690655641 ** get_address_of_OnError_0() { return &___OnError_0; }
	inline void set_OnError_0(ErrorDelegate_t3690655641 * value)
	{
		___OnError_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnError_0, value);
	}

	inline static int32_t get_offset_of_OnStatusChanged_1() { return static_cast<int32_t>(offsetof(PhraseRecognitionSystem_t1642125421_StaticFields, ___OnStatusChanged_1)); }
	inline StatusDelegate_t2739717665 * get_OnStatusChanged_1() const { return ___OnStatusChanged_1; }
	inline StatusDelegate_t2739717665 ** get_address_of_OnStatusChanged_1() { return &___OnStatusChanged_1; }
	inline void set_OnStatusChanged_1(StatusDelegate_t2739717665 * value)
	{
		___OnStatusChanged_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnStatusChanged_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
