﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.NameTable/Entry
struct Entry_t2583369454;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_t2583369454  : public Il2CppObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hashCode
	int32_t ___hashCode_1;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_t2583369454 * ___next_2;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier(&___str_0, value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___next_2)); }
	inline Entry_t2583369454 * get_next_2() const { return ___next_2; }
	inline Entry_t2583369454 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(Entry_t2583369454 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier(&___next_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
