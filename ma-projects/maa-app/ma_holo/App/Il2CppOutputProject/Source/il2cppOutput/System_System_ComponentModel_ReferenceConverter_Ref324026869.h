﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.ReferenceConverter
struct ReferenceConverter_t3131270729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReferenceConverter/ReferenceComparer
struct  ReferenceComparer_t324026869  : public Il2CppObject
{
public:
	// System.ComponentModel.ReferenceConverter System.ComponentModel.ReferenceConverter/ReferenceComparer::converter
	ReferenceConverter_t3131270729 * ___converter_0;

public:
	inline static int32_t get_offset_of_converter_0() { return static_cast<int32_t>(offsetof(ReferenceComparer_t324026869, ___converter_0)); }
	inline ReferenceConverter_t3131270729 * get_converter_0() const { return ___converter_0; }
	inline ReferenceConverter_t3131270729 ** get_address_of_converter_0() { return &___converter_0; }
	inline void set_converter_0(ReferenceConverter_t3131270729 * value)
	{
		___converter_0 = value;
		Il2CppCodeGenWriteBarrier(&___converter_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
