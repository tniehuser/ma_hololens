﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Diagnostics_StackFrame2050294881.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597.h"
#include "mscorlib_System_Diagnostics_StackTrace_TraceFormat4280692789.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Globalization_CultureInfo_Data1401534395.h"
#include "mscorlib_System_Globalization_IdnMapping1950908047.h"
#include "mscorlib_System_Globalization_Bootstring702872283.h"
#include "mscorlib_System_Globalization_Punycode3653496603.h"
#include "mscorlib_System_IO_Directory3318511961.h"
#include "mscorlib_System_IO_Directory_U3CEnumerateKindU3Ec_3216484634.h"
#include "mscorlib_System_IO_DirectoryInfo1934446453.h"
#include "mscorlib_System_IO_File1930543328.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"
#include "mscorlib_System_IO_FileAttributes3843045335.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "mscorlib_System_IO_FileOptions3144759768.h"
#include "mscorlib_System_IO_FileShare3362491215.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "mscorlib_System_IO_FileStream_ReadDelegate3184826381.h"
#include "mscorlib_System_IO_FileStream_WriteDelegate489908132.h"
#include "mscorlib_System_IO_FileStreamAsyncResult2252849197.h"
#include "mscorlib_System_IO_MonoFileType3095218325.h"
#include "mscorlib_System_IO_MonoIO2170088987.h"
#include "mscorlib_System_IO_MonoIOError733012845.h"
#include "mscorlib_System_IO_MonoIOStat1621921065.h"
#include "mscorlib_System_IO_Path41728875.h"
#include "mscorlib_System_IO_SearchOption3245573.h"
#include "mscorlib_System_IO_SearchPattern1107849040.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "mscorlib_System_IO_UnexceptionalStreamReader683371910.h"
#include "mscorlib_System_IO_UnexceptionalStreamWriter1485343520.h"
#include "mscorlib_System_Reflection_Emit_NativeResourceType548430477.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS2708608433.h"
#include "mscorlib_System_Reflection_Emit_MonoResource3127387157.h"
#include "mscorlib_System_Reflection_Emit_MonoWin32Resource2467306218.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder1646117627.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAcc1210911821.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder700974433.h"
#include "mscorlib_System_Reflection_Emit_CustomAttributeBui2970303184.h"
#include "mscorlib_System_Reflection_Emit_ArrayType3579558425.h"
#include "mscorlib_System_Reflection_Emit_ByRefType1587086384.h"
#include "mscorlib_System_Reflection_Emit_PointerType3290473669.h"
#include "mscorlib_System_Reflection_Emit_DynamicILInfo2616052200.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod3307743052.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod_Anon4101997074.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethodTokenG435001088.h"
#include "mscorlib_System_Reflection_Emit_EventBuilder2927243889.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder2784804005.h"
#include "mscorlib_System_Reflection_Emit_GenericTypeParamet1370236603.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionBlock2042475189.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionInfo1490154598.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo149559338.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator99948092.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelF4090909514.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3712112744.h"
#include "mscorlib_System_Reflection_Emit_SequencePointList411423549.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder2116499186.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder644187984.h"
#include "mscorlib_System_Reflection_Emit_MethodOnTypeBuilde3128326659.h"
#include "mscorlib_System_Reflection_Emit_MethodToken3991686330.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder4156028127.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenG578872653.h"
#include "mscorlib_System_Reflection_Emit_OpCode2247480392.h"
#include "mscorlib_System_Reflection_Emit_OpCodeNames1907134268.h"
#include "mscorlib_System_Reflection_Emit_OpCodes3494785031.h"
#include "mscorlib_System_Reflection_Emit_OperandType22140925.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds4139237606.h"
#include "mscorlib_System_Reflection_Emit_PackingSize4013171414.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3344728474.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder3694255912.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour1390406961.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder3308873219.h"
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal4270021860.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "mscorlib_System_Reflection_Assembly_ResolveEventHo1761494505.h"
#include "mscorlib_System_Reflection_Assembly_UnmanagedMemor2398216387.h"
#include "mscorlib_System_Reflection_AssemblyName894705941.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "mscorlib_System_Reflection_CustomAttributeData3093286891.h"
#include "mscorlib_System_Reflection_CustomAttributeData_Laz2380698664.h"
#include "mscorlib_System_Reflection_CustomAttributeFormatEx3538197047.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Reflection_EventInfo4258285342.h"
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapt1766862959.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_ImageFileMachine128670190.h"
#include "mscorlib_System_Reflection_LocalVariableInfo1749284021.h"
#include "mscorlib_System_Reflection_ResolveTokenError3542011731.h"
#include "mscorlib_System_Reflection_Module4282841206.h"
#include "mscorlib_System_Reflection_RuntimeAssembly1913607566.h"
#include "mscorlib_System_Reflection_MonoAssembly183727305.h"
#include "mscorlib_System_Reflection_MonoEventInfo2190036573.h"
#include "mscorlib_System_Reflection_RuntimeEventInfo3861156374.h"
#include "mscorlib_System_Reflection_MonoEvent2188687691.h"
#include "mscorlib_System_Reflection_RuntimeFieldInfo1687134186.h"
#include "mscorlib_System_Reflection_RtFieldInfo999161580.h"
#include "mscorlib_System_Reflection_MonoField3600053525.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (StackFrame_t2050294881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1000[10] = 
{
	0,
	StackFrame_t2050294881::get_offset_of_ilOffset_1(),
	StackFrame_t2050294881::get_offset_of_nativeOffset_2(),
	StackFrame_t2050294881::get_offset_of_methodAddress_3(),
	StackFrame_t2050294881::get_offset_of_methodIndex_4(),
	StackFrame_t2050294881::get_offset_of_methodBase_5(),
	StackFrame_t2050294881::get_offset_of_fileName_6(),
	StackFrame_t2050294881::get_offset_of_lineNumber_7(),
	StackFrame_t2050294881::get_offset_of_columnNumber_8(),
	StackFrame_t2050294881::get_offset_of_internalMethodName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (StackTrace_t2500644597), -1, sizeof(StackTrace_t2500644597_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1001[7] = 
{
	0,
	StackTrace_t2500644597::get_offset_of_frames_1(),
	StackTrace_t2500644597::get_offset_of_captured_traces_2(),
	StackTrace_t2500644597::get_offset_of_debug_info_3(),
	StackTrace_t2500644597_StaticFields::get_offset_of_metadataHandlers_4(),
	StackTrace_t2500644597_StaticFields::get_offset_of_isAotidSet_5(),
	StackTrace_t2500644597_StaticFields::get_offset_of_aotid_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (TraceFormat_t4280692789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1002[4] = 
{
	TraceFormat_t4280692789::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (CultureInfo_t3500843524), -1, sizeof(CultureInfo_t3500843524_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1003[40] = 
{
	CultureInfo_t3500843524_StaticFields::get_offset_of_invariant_culture_info_0(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_shared_table_lock_1(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_default_current_culture_2(),
	CultureInfo_t3500843524::get_offset_of_m_isReadOnly_3(),
	CultureInfo_t3500843524::get_offset_of_cultureID_4(),
	CultureInfo_t3500843524::get_offset_of_parent_lcid_5(),
	CultureInfo_t3500843524::get_offset_of_datetime_index_6(),
	CultureInfo_t3500843524::get_offset_of_number_index_7(),
	CultureInfo_t3500843524::get_offset_of_default_calendar_type_8(),
	CultureInfo_t3500843524::get_offset_of_m_useUserOverride_9(),
	CultureInfo_t3500843524::get_offset_of_numInfo_10(),
	CultureInfo_t3500843524::get_offset_of_dateTimeInfo_11(),
	CultureInfo_t3500843524::get_offset_of_textInfo_12(),
	CultureInfo_t3500843524::get_offset_of_m_name_13(),
	CultureInfo_t3500843524::get_offset_of_englishname_14(),
	CultureInfo_t3500843524::get_offset_of_nativename_15(),
	CultureInfo_t3500843524::get_offset_of_iso3lang_16(),
	CultureInfo_t3500843524::get_offset_of_iso2lang_17(),
	CultureInfo_t3500843524::get_offset_of_win3lang_18(),
	CultureInfo_t3500843524::get_offset_of_territory_19(),
	CultureInfo_t3500843524::get_offset_of_native_calendar_names_20(),
	CultureInfo_t3500843524::get_offset_of_compareInfo_21(),
	CultureInfo_t3500843524::get_offset_of_textinfo_data_22(),
	CultureInfo_t3500843524::get_offset_of_m_dataItem_23(),
	CultureInfo_t3500843524::get_offset_of_calendar_24(),
	CultureInfo_t3500843524::get_offset_of_parent_culture_25(),
	CultureInfo_t3500843524::get_offset_of_constructed_26(),
	CultureInfo_t3500843524::get_offset_of_cached_serialized_form_27(),
	CultureInfo_t3500843524::get_offset_of_m_cultureData_28(),
	CultureInfo_t3500843524::get_offset_of_m_isInherited_29(),
	0,
	0,
	0,
	CultureInfo_t3500843524_StaticFields::get_offset_of_s_DefaultThreadCurrentUICulture_33(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_s_DefaultThreadCurrentCulture_34(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_shared_by_number_35(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_shared_by_name_36(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_IsTaiwanSku_37(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_38(),
	CultureInfo_t3500843524_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (Data_t1401534395)+ sizeof (Il2CppObject), sizeof(Data_t1401534395_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1004[6] = 
{
	Data_t1401534395::get_offset_of_ansi_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1401534395::get_offset_of_ebcdic_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1401534395::get_offset_of_mac_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1401534395::get_offset_of_oem_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1401534395::get_offset_of_right_to_left_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t1401534395::get_offset_of_list_sep_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (IdnMapping_t1950908047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1005[3] = 
{
	IdnMapping_t1950908047::get_offset_of_allow_unassigned_0(),
	IdnMapping_t1950908047::get_offset_of_use_std3_1(),
	IdnMapping_t1950908047::get_offset_of_puny_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (Bootstring_t702872283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1006[8] = 
{
	Bootstring_t702872283::get_offset_of_delimiter_0(),
	Bootstring_t702872283::get_offset_of_base_num_1(),
	Bootstring_t702872283::get_offset_of_tmin_2(),
	Bootstring_t702872283::get_offset_of_tmax_3(),
	Bootstring_t702872283::get_offset_of_skew_4(),
	Bootstring_t702872283::get_offset_of_damp_5(),
	Bootstring_t702872283::get_offset_of_initial_bias_6(),
	Bootstring_t702872283::get_offset_of_initial_n_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (Punycode_t3653496603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (Directory_t3318511961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (U3CEnumerateKindU3Ec__Iterator0_t3216484634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1009[15] = 
{
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_searchPattern_0(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_path_1(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3CstopU3E__0_2(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3Cpath_with_patternU3E__0_3(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3CrattrU3E__0_4(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3CerrorU3E__0_5(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3ChandleU3E__0_6(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3CsU3E__0_7(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_kind_8(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_searchOption_9(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U24locvar0_10(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U3CchildU3E__1_11(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U24current_12(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U24disposing_13(),
	U3CEnumerateKindU3Ec__Iterator0_t3216484634::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (DirectoryInfo_t1934446453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1010[2] = 
{
	DirectoryInfo_t1934446453::get_offset_of_current_6(),
	DirectoryInfo_t1934446453::get_offset_of_parent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (File_t1930543328), -1, sizeof(File_t1930543328_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1011[1] = 
{
	File_t1930543328_StaticFields::get_offset_of_defaultLocalFileTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (FileAccess_t4282042064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1012[4] = 
{
	FileAccess_t4282042064::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (FileAttributes_t3843045335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1013[17] = 
{
	FileAttributes_t3843045335::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (FileMode_t236403845)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1014[7] = 
{
	FileMode_t236403845::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (FileOptions_t3144759768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1015[8] = 
{
	FileOptions_t3144759768::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (FileShare_t3362491215)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1016[7] = 
{
	FileShare_t3362491215::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (FileStream_t1695958676), -1, sizeof(FileStream_t1695958676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1017[17] = 
{
	FileStream_t1695958676_StaticFields::get_offset_of_buf_recycle_8(),
	FileStream_t1695958676_StaticFields::get_offset_of_buf_recycle_lock_9(),
	FileStream_t1695958676::get_offset_of_buf_10(),
	FileStream_t1695958676::get_offset_of_name_11(),
	FileStream_t1695958676::get_offset_of_safeHandle_12(),
	FileStream_t1695958676::get_offset_of_isExposed_13(),
	FileStream_t1695958676::get_offset_of_append_startpos_14(),
	FileStream_t1695958676::get_offset_of_access_15(),
	FileStream_t1695958676::get_offset_of_owner_16(),
	FileStream_t1695958676::get_offset_of_async_17(),
	FileStream_t1695958676::get_offset_of_canseek_18(),
	FileStream_t1695958676::get_offset_of_anonymous_19(),
	FileStream_t1695958676::get_offset_of_buf_dirty_20(),
	FileStream_t1695958676::get_offset_of_buf_size_21(),
	FileStream_t1695958676::get_offset_of_buf_length_22(),
	FileStream_t1695958676::get_offset_of_buf_offset_23(),
	FileStream_t1695958676::get_offset_of_buf_start_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (ReadDelegate_t3184826381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (WriteDelegate_t489908132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (FileStreamAsyncResult_t2252849197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1020[9] = 
{
	FileStreamAsyncResult_t2252849197::get_offset_of_state_0(),
	FileStreamAsyncResult_t2252849197::get_offset_of_completed_1(),
	FileStreamAsyncResult_t2252849197::get_offset_of_wh_2(),
	FileStreamAsyncResult_t2252849197::get_offset_of_cb_3(),
	FileStreamAsyncResult_t2252849197::get_offset_of_completedSynch_4(),
	FileStreamAsyncResult_t2252849197::get_offset_of_Count_5(),
	FileStreamAsyncResult_t2252849197::get_offset_of_OriginalCount_6(),
	FileStreamAsyncResult_t2252849197::get_offset_of_BytesRead_7(),
	FileStreamAsyncResult_t2252849197::get_offset_of_realcb_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (MonoFileType_t3095218325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1021[6] = 
{
	MonoFileType_t3095218325::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (MonoIO_t2170088987), -1, sizeof(MonoIO_t2170088987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1022[2] = 
{
	MonoIO_t2170088987_StaticFields::get_offset_of_InvalidHandle_0(),
	MonoIO_t2170088987_StaticFields::get_offset_of_dump_handles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (MonoIOError_t733012845)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1023[27] = 
{
	MonoIOError_t733012845::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (MonoIOStat_t1621921065)+ sizeof (Il2CppObject), sizeof(MonoIOStat_t1621921065 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1024[5] = 
{
	MonoIOStat_t1621921065::get_offset_of_fileAttributes_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoIOStat_t1621921065::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoIOStat_t1621921065::get_offset_of_CreationTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoIOStat_t1621921065::get_offset_of_LastAccessTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoIOStat_t1621921065::get_offset_of_LastWriteTime_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (Path_t41728875), -1, sizeof(Path_t41728875_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1025[8] = 
{
	Path_t41728875_StaticFields::get_offset_of_InvalidPathChars_0(),
	Path_t41728875_StaticFields::get_offset_of_AltDirectorySeparatorChar_1(),
	Path_t41728875_StaticFields::get_offset_of_DirectorySeparatorChar_2(),
	Path_t41728875_StaticFields::get_offset_of_PathSeparator_3(),
	Path_t41728875_StaticFields::get_offset_of_DirectorySeparatorStr_4(),
	Path_t41728875_StaticFields::get_offset_of_VolumeSeparatorChar_5(),
	Path_t41728875_StaticFields::get_offset_of_PathSeparatorChars_6(),
	Path_t41728875_StaticFields::get_offset_of_dirEqualsVolume_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (SearchOption_t3245573)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1026[3] = 
{
	SearchOption_t3245573::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (SearchPattern_t1107849040), -1, sizeof(SearchPattern_t1107849040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1027[1] = 
{
	SearchPattern_t1107849040_StaticFields::get_offset_of_WildcardChars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (SeekOrigin_t2475945306)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1028[4] = 
{
	SeekOrigin_t2475945306::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (UnexceptionalStreamReader_t683371910), -1, sizeof(UnexceptionalStreamReader_t683371910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1029[2] = 
{
	UnexceptionalStreamReader_t683371910_StaticFields::get_offset_of_newline_21(),
	UnexceptionalStreamReader_t683371910_StaticFields::get_offset_of_newlineChar_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (UnexceptionalStreamWriter_t1485343520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (NativeResourceType_t548430477)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1031[5] = 
{
	NativeResourceType_t548430477::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (RefEmitPermissionSet_t2708608433)+ sizeof (Il2CppObject), sizeof(RefEmitPermissionSet_t2708608433_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1032[2] = 
{
	RefEmitPermissionSet_t2708608433::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RefEmitPermissionSet_t2708608433::get_offset_of_pset_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (MonoResource_t3127387157)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1033[6] = 
{
	MonoResource_t3127387157::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t3127387157::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t3127387157::get_offset_of_filename_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t3127387157::get_offset_of_attrs_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t3127387157::get_offset_of_offset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoResource_t3127387157::get_offset_of_stream_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { sizeof (MonoWin32Resource_t2467306218)+ sizeof (Il2CppObject), sizeof(MonoWin32Resource_t2467306218_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1034[4] = 
{
	MonoWin32Resource_t2467306218::get_offset_of_res_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoWin32Resource_t2467306218::get_offset_of_res_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoWin32Resource_t2467306218::get_offset_of_lang_id_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoWin32Resource_t2467306218::get_offset_of_data_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { sizeof (AssemblyBuilder_t1646117627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1035[37] = 
{
	AssemblyBuilder_t1646117627::get_offset_of_dynamic_assembly_10(),
	AssemblyBuilder_t1646117627::get_offset_of_entry_point_11(),
	AssemblyBuilder_t1646117627::get_offset_of_modules_12(),
	AssemblyBuilder_t1646117627::get_offset_of_name_13(),
	AssemblyBuilder_t1646117627::get_offset_of_dir_14(),
	AssemblyBuilder_t1646117627::get_offset_of_cattrs_15(),
	AssemblyBuilder_t1646117627::get_offset_of_resources_16(),
	AssemblyBuilder_t1646117627::get_offset_of_public_key_17(),
	AssemblyBuilder_t1646117627::get_offset_of_version_18(),
	AssemblyBuilder_t1646117627::get_offset_of_culture_19(),
	AssemblyBuilder_t1646117627::get_offset_of_algid_20(),
	AssemblyBuilder_t1646117627::get_offset_of_flags_21(),
	AssemblyBuilder_t1646117627::get_offset_of_pekind_22(),
	AssemblyBuilder_t1646117627::get_offset_of_delay_sign_23(),
	AssemblyBuilder_t1646117627::get_offset_of_access_24(),
	AssemblyBuilder_t1646117627::get_offset_of_loaded_modules_25(),
	AssemblyBuilder_t1646117627::get_offset_of_win32_resources_26(),
	AssemblyBuilder_t1646117627::get_offset_of_permissions_minimum_27(),
	AssemblyBuilder_t1646117627::get_offset_of_permissions_optional_28(),
	AssemblyBuilder_t1646117627::get_offset_of_permissions_refused_29(),
	AssemblyBuilder_t1646117627::get_offset_of_peKind_30(),
	AssemblyBuilder_t1646117627::get_offset_of_machine_31(),
	AssemblyBuilder_t1646117627::get_offset_of_corlib_internal_32(),
	AssemblyBuilder_t1646117627::get_offset_of_type_forwarders_33(),
	AssemblyBuilder_t1646117627::get_offset_of_pktoken_34(),
	AssemblyBuilder_t1646117627::get_offset_of_corlib_object_type_35(),
	AssemblyBuilder_t1646117627::get_offset_of_corlib_value_type_36(),
	AssemblyBuilder_t1646117627::get_offset_of_corlib_enum_type_37(),
	AssemblyBuilder_t1646117627::get_offset_of_corlib_void_type_38(),
	AssemblyBuilder_t1646117627::get_offset_of_resource_writers_39(),
	AssemblyBuilder_t1646117627::get_offset_of_version_res_40(),
	AssemblyBuilder_t1646117627::get_offset_of_created_41(),
	AssemblyBuilder_t1646117627::get_offset_of_is_module_only_42(),
	AssemblyBuilder_t1646117627::get_offset_of_sn_43(),
	AssemblyBuilder_t1646117627::get_offset_of_native_resource_44(),
	AssemblyBuilder_t1646117627::get_offset_of_versioninfo_culture_45(),
	AssemblyBuilder_t1646117627::get_offset_of_manifest_module_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { sizeof (AssemblyBuilderAccess_t1210911821)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1036[6] = 
{
	AssemblyBuilderAccess_t1210911821::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (ConstructorBuilder_t700974433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1037[14] = 
{
	ConstructorBuilder_t700974433::get_offset_of_mhandle_2(),
	ConstructorBuilder_t700974433::get_offset_of_ilgen_3(),
	ConstructorBuilder_t700974433::get_offset_of_parameters_4(),
	ConstructorBuilder_t700974433::get_offset_of_attrs_5(),
	ConstructorBuilder_t700974433::get_offset_of_iattrs_6(),
	ConstructorBuilder_t700974433::get_offset_of_table_idx_7(),
	ConstructorBuilder_t700974433::get_offset_of_call_conv_8(),
	ConstructorBuilder_t700974433::get_offset_of_type_9(),
	ConstructorBuilder_t700974433::get_offset_of_pinfo_10(),
	ConstructorBuilder_t700974433::get_offset_of_cattrs_11(),
	ConstructorBuilder_t700974433::get_offset_of_init_locals_12(),
	ConstructorBuilder_t700974433::get_offset_of_paramModReq_13(),
	ConstructorBuilder_t700974433::get_offset_of_paramModOpt_14(),
	ConstructorBuilder_t700974433::get_offset_of_permissions_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (CustomAttributeBuilder_t2970303184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1038[2] = 
{
	CustomAttributeBuilder_t2970303184::get_offset_of_ctor_0(),
	CustomAttributeBuilder_t2970303184::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (ArrayType_t3579558425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1039[1] = 
{
	ArrayType_t3579558425::get_offset_of_rank_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (ByRefType_t1587086384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (PointerType_t3290473669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (DynamicILInfo_t2616052200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (DynamicMethod_t3307743052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1043[19] = 
{
	DynamicMethod_t3307743052::get_offset_of_mhandle_0(),
	DynamicMethod_t3307743052::get_offset_of_name_1(),
	DynamicMethod_t3307743052::get_offset_of_returnType_2(),
	DynamicMethod_t3307743052::get_offset_of_parameters_3(),
	DynamicMethod_t3307743052::get_offset_of_attributes_4(),
	DynamicMethod_t3307743052::get_offset_of_callingConvention_5(),
	DynamicMethod_t3307743052::get_offset_of_module_6(),
	DynamicMethod_t3307743052::get_offset_of_skipVisibility_7(),
	DynamicMethod_t3307743052::get_offset_of_init_locals_8(),
	DynamicMethod_t3307743052::get_offset_of_ilgen_9(),
	DynamicMethod_t3307743052::get_offset_of_nrefs_10(),
	DynamicMethod_t3307743052::get_offset_of_refs_11(),
	DynamicMethod_t3307743052::get_offset_of_referenced_by_12(),
	DynamicMethod_t3307743052::get_offset_of_owner_13(),
	DynamicMethod_t3307743052::get_offset_of_deleg_14(),
	DynamicMethod_t3307743052::get_offset_of_method_15(),
	DynamicMethod_t3307743052::get_offset_of_pinfo_16(),
	DynamicMethod_t3307743052::get_offset_of_creating_17(),
	DynamicMethod_t3307743052::get_offset_of_il_info_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (AnonHostModuleHolder_t4101997074), -1, sizeof(AnonHostModuleHolder_t4101997074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1044[1] = 
{
	AnonHostModuleHolder_t4101997074_StaticFields::get_offset_of_anon_host_module_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (DynamicMethodTokenGenerator_t435001088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1045[1] = 
{
	DynamicMethodTokenGenerator_t435001088::get_offset_of_m_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (EventBuilder_t2927243889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1046[10] = 
{
	EventBuilder_t2927243889::get_offset_of_name_0(),
	EventBuilder_t2927243889::get_offset_of_type_1(),
	EventBuilder_t2927243889::get_offset_of_typeb_2(),
	EventBuilder_t2927243889::get_offset_of_cattrs_3(),
	EventBuilder_t2927243889::get_offset_of_add_method_4(),
	EventBuilder_t2927243889::get_offset_of_remove_method_5(),
	EventBuilder_t2927243889::get_offset_of_raise_method_6(),
	EventBuilder_t2927243889::get_offset_of_other_methods_7(),
	EventBuilder_t2927243889::get_offset_of_attrs_8(),
	EventBuilder_t2927243889::get_offset_of_table_idx_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (FieldBuilder_t2784804005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1047[13] = 
{
	FieldBuilder_t2784804005::get_offset_of_attrs_0(),
	FieldBuilder_t2784804005::get_offset_of_type_1(),
	FieldBuilder_t2784804005::get_offset_of_name_2(),
	FieldBuilder_t2784804005::get_offset_of_def_value_3(),
	FieldBuilder_t2784804005::get_offset_of_offset_4(),
	FieldBuilder_t2784804005::get_offset_of_table_idx_5(),
	FieldBuilder_t2784804005::get_offset_of_typeb_6(),
	FieldBuilder_t2784804005::get_offset_of_rva_data_7(),
	FieldBuilder_t2784804005::get_offset_of_cattrs_8(),
	FieldBuilder_t2784804005::get_offset_of_marshal_info_9(),
	FieldBuilder_t2784804005::get_offset_of_handle_10(),
	FieldBuilder_t2784804005::get_offset_of_modReq_11(),
	FieldBuilder_t2784804005::get_offset_of_modOpt_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (GenericTypeParameterBuilder_t1370236603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1048[8] = 
{
	GenericTypeParameterBuilder_t1370236603::get_offset_of_tbuilder_8(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_mbuilder_9(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_name_10(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_index_11(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_base_type_12(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_iface_constraints_13(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_cattrs_14(),
	GenericTypeParameterBuilder_t1370236603::get_offset_of_attrs_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (ILExceptionBlock_t2042475189)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1049[5] = 
{
	ILExceptionBlock_t2042475189::get_offset_of_extype_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionBlock_t2042475189::get_offset_of_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionBlock_t2042475189::get_offset_of_start_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionBlock_t2042475189::get_offset_of_len_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionBlock_t2042475189::get_offset_of_filter_offset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (ILExceptionInfo_t1490154598)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1050[4] = 
{
	ILExceptionInfo_t1490154598::get_offset_of_handlers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionInfo_t1490154598::get_offset_of_start_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionInfo_t1490154598::get_offset_of_len_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILExceptionInfo_t1490154598::get_offset_of_end_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (ILTokenInfo_t149559338)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1051[2] = 
{
	ILTokenInfo_t149559338::get_offset_of_member_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ILTokenInfo_t149559338::get_offset_of_code_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (ILGenerator_t99948092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1053[18] = 
{
	ILGenerator_t99948092::get_offset_of_code_0(),
	ILGenerator_t99948092::get_offset_of_code_len_1(),
	ILGenerator_t99948092::get_offset_of_max_stack_2(),
	ILGenerator_t99948092::get_offset_of_cur_stack_3(),
	ILGenerator_t99948092::get_offset_of_locals_4(),
	ILGenerator_t99948092::get_offset_of_ex_handlers_5(),
	ILGenerator_t99948092::get_offset_of_num_token_fixups_6(),
	ILGenerator_t99948092::get_offset_of_token_fixups_7(),
	ILGenerator_t99948092::get_offset_of_labels_8(),
	ILGenerator_t99948092::get_offset_of_num_labels_9(),
	ILGenerator_t99948092::get_offset_of_fixups_10(),
	ILGenerator_t99948092::get_offset_of_num_fixups_11(),
	ILGenerator_t99948092::get_offset_of_module_12(),
	ILGenerator_t99948092::get_offset_of_cur_block_13(),
	ILGenerator_t99948092::get_offset_of_open_blocks_14(),
	ILGenerator_t99948092::get_offset_of_token_gen_15(),
	ILGenerator_t99948092::get_offset_of_sequencePointLists_16(),
	ILGenerator_t99948092::get_offset_of_currentSequence_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (LabelFixup_t4090909514)+ sizeof (Il2CppObject), sizeof(LabelFixup_t4090909514 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1054[3] = 
{
	LabelFixup_t4090909514::get_offset_of_offset_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelFixup_t4090909514::get_offset_of_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelFixup_t4090909514::get_offset_of_label_idx_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (LabelData_t3712112744)+ sizeof (Il2CppObject), sizeof(LabelData_t3712112744 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1055[2] = 
{
	LabelData_t3712112744::get_offset_of_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LabelData_t3712112744::get_offset_of_maxStack_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (SequencePointList_t411423549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (Label_t4243202660)+ sizeof (Il2CppObject), sizeof(Label_t4243202660 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1057[1] = 
{
	Label_t4243202660::get_offset_of_label_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (LocalBuilder_t2116499186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1058[4] = 
{
	LocalBuilder_t2116499186::get_offset_of_name_3(),
	LocalBuilder_t2116499186::get_offset_of_ilgen_4(),
	LocalBuilder_t2116499186::get_offset_of_startOffset_5(),
	LocalBuilder_t2116499186::get_offset_of_endOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (MethodBuilder_t644187984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1059[27] = 
{
	MethodBuilder_t644187984::get_offset_of_mhandle_0(),
	MethodBuilder_t644187984::get_offset_of_rtype_1(),
	MethodBuilder_t644187984::get_offset_of_parameters_2(),
	MethodBuilder_t644187984::get_offset_of_attrs_3(),
	MethodBuilder_t644187984::get_offset_of_iattrs_4(),
	MethodBuilder_t644187984::get_offset_of_name_5(),
	MethodBuilder_t644187984::get_offset_of_table_idx_6(),
	MethodBuilder_t644187984::get_offset_of_code_7(),
	MethodBuilder_t644187984::get_offset_of_ilgen_8(),
	MethodBuilder_t644187984::get_offset_of_type_9(),
	MethodBuilder_t644187984::get_offset_of_pinfo_10(),
	MethodBuilder_t644187984::get_offset_of_cattrs_11(),
	MethodBuilder_t644187984::get_offset_of_override_methods_12(),
	MethodBuilder_t644187984::get_offset_of_pi_dll_13(),
	MethodBuilder_t644187984::get_offset_of_pi_entry_14(),
	MethodBuilder_t644187984::get_offset_of_charset_15(),
	MethodBuilder_t644187984::get_offset_of_extra_flags_16(),
	MethodBuilder_t644187984::get_offset_of_native_cc_17(),
	MethodBuilder_t644187984::get_offset_of_call_conv_18(),
	MethodBuilder_t644187984::get_offset_of_init_locals_19(),
	MethodBuilder_t644187984::get_offset_of_generic_container_20(),
	MethodBuilder_t644187984::get_offset_of_generic_params_21(),
	MethodBuilder_t644187984::get_offset_of_returnModReq_22(),
	MethodBuilder_t644187984::get_offset_of_returnModOpt_23(),
	MethodBuilder_t644187984::get_offset_of_paramModReq_24(),
	MethodBuilder_t644187984::get_offset_of_paramModOpt_25(),
	MethodBuilder_t644187984::get_offset_of_permissions_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (MethodOnTypeBuilderInst_t3128326659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1060[4] = 
{
	MethodOnTypeBuilderInst_t3128326659::get_offset_of_instantiation_0(),
	MethodOnTypeBuilderInst_t3128326659::get_offset_of_base_method_1(),
	MethodOnTypeBuilderInst_t3128326659::get_offset_of_method_arguments_2(),
	MethodOnTypeBuilderInst_t3128326659::get_offset_of_generic_method_definition_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (MethodToken_t3991686330)+ sizeof (Il2CppObject), sizeof(MethodToken_t3991686330 ), sizeof(MethodToken_t3991686330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1061[2] = 
{
	MethodToken_t3991686330::get_offset_of_tokValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MethodToken_t3991686330_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (ModuleBuilder_t4156028127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1062[20] = 
{
	ModuleBuilder_t4156028127::get_offset_of_dynamic_image_10(),
	ModuleBuilder_t4156028127::get_offset_of_num_types_11(),
	ModuleBuilder_t4156028127::get_offset_of_types_12(),
	ModuleBuilder_t4156028127::get_offset_of_cattrs_13(),
	ModuleBuilder_t4156028127::get_offset_of_guid_14(),
	ModuleBuilder_t4156028127::get_offset_of_table_idx_15(),
	ModuleBuilder_t4156028127::get_offset_of_assemblyb_16(),
	ModuleBuilder_t4156028127::get_offset_of_global_methods_17(),
	ModuleBuilder_t4156028127::get_offset_of_global_fields_18(),
	ModuleBuilder_t4156028127::get_offset_of_is_main_19(),
	ModuleBuilder_t4156028127::get_offset_of_resources_20(),
	ModuleBuilder_t4156028127::get_offset_of_global_type_21(),
	ModuleBuilder_t4156028127::get_offset_of_global_type_created_22(),
	ModuleBuilder_t4156028127::get_offset_of_name_cache_23(),
	ModuleBuilder_t4156028127::get_offset_of_us_string_cache_24(),
	ModuleBuilder_t4156028127::get_offset_of_table_indexes_25(),
	ModuleBuilder_t4156028127::get_offset_of_transient_26(),
	ModuleBuilder_t4156028127::get_offset_of_token_gen_27(),
	ModuleBuilder_t4156028127::get_offset_of_resource_writers_28(),
	ModuleBuilder_t4156028127::get_offset_of_symbolWriter_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (ModuleBuilderTokenGenerator_t578872653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1063[1] = 
{
	ModuleBuilderTokenGenerator_t578872653::get_offset_of_mb_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (OpCode_t2247480392)+ sizeof (Il2CppObject), sizeof(OpCode_t2247480392 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1064[8] = 
{
	OpCode_t2247480392::get_offset_of_op1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_op2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_push_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_pop_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_type_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_args_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t2247480392::get_offset_of_flow_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (OpCodeNames_t1907134268), -1, sizeof(OpCodeNames_t1907134268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1065[1] = 
{
	OpCodeNames_t1907134268_StaticFields::get_offset_of_names_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (OpCodes_t3494785031), -1, sizeof(OpCodes_t3494785031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1066[226] = 
{
	OpCodes_t3494785031_StaticFields::get_offset_of_Nop_0(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Break_1(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_0_2(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_1_3(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_2_4(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_3_5(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_0_6(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_1_7(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_2_8(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_3_9(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_0_10(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_1_11(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_2_12(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_3_13(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_S_14(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarga_S_15(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Starg_S_16(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_S_17(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloca_S_18(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_S_19(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldnull_20(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_M1_21(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_0_22(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_1_23(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_2_24(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_3_25(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_4_26(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_5_27(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_6_28(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_7_29(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_8_30(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_S_31(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I4_32(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_I8_33(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_R4_34(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldc_R8_35(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Dup_36(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Pop_37(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Jmp_38(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Call_39(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Calli_40(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ret_41(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Br_S_42(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Brfalse_S_43(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Brtrue_S_44(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Beq_S_45(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bge_S_46(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bgt_S_47(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ble_S_48(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Blt_S_49(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bne_Un_S_50(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bge_Un_S_51(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bgt_Un_S_52(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ble_Un_S_53(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Blt_Un_S_54(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Br_55(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Brfalse_56(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Brtrue_57(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Beq_58(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bge_59(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bgt_60(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ble_61(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Blt_62(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bne_Un_63(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bge_Un_64(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Bgt_Un_65(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ble_Un_66(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Blt_Un_67(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Switch_68(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_I1_69(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_U1_70(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_I2_71(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_U2_72(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_I4_73(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_U4_74(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_I8_75(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_I_76(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_R4_77(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_R8_78(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldind_Ref_79(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_Ref_80(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_I1_81(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_I2_82(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_I4_83(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_I8_84(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_R4_85(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_R8_86(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Add_87(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Sub_88(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Mul_89(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Div_90(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Div_Un_91(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Rem_92(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Rem_Un_93(),
	OpCodes_t3494785031_StaticFields::get_offset_of_And_94(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Or_95(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Xor_96(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Shl_97(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Shr_98(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Shr_Un_99(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Neg_100(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Not_101(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_I1_102(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_I2_103(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_I4_104(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_I8_105(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_R4_106(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_R8_107(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_U4_108(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_U8_109(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Callvirt_110(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Cpobj_111(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldobj_112(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldstr_113(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Newobj_114(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Castclass_115(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Isinst_116(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_R_Un_117(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Unbox_118(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Throw_119(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldfld_120(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldflda_121(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stfld_122(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldsfld_123(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldsflda_124(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stsfld_125(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stobj_126(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I1_Un_127(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I2_Un_128(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I4_Un_129(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I8_Un_130(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U1_Un_131(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U2_Un_132(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U4_Un_133(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U8_Un_134(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I_Un_135(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U_Un_136(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Box_137(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Newarr_138(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldlen_139(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelema_140(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_I1_141(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_U1_142(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_I2_143(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_U2_144(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_I4_145(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_U4_146(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_I8_147(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_I_148(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_R4_149(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_R8_150(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_Ref_151(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_I_152(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_I1_153(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_I2_154(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_I4_155(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_I8_156(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_R4_157(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_R8_158(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_Ref_159(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldelem_160(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stelem_161(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Unbox_Any_162(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I1_163(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U1_164(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I2_165(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U2_166(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I4_167(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U4_168(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I8_169(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U8_170(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Refanyval_171(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ckfinite_172(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Mkrefany_173(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldtoken_174(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_U2_175(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_U1_176(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_I_177(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_I_178(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_Ovf_U_179(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Add_Ovf_180(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Add_Ovf_Un_181(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Mul_Ovf_182(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Mul_Ovf_Un_183(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Sub_Ovf_184(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Sub_Ovf_Un_185(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Endfinally_186(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Leave_187(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Leave_S_188(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stind_I_189(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Conv_U_190(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix7_191(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix6_192(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix5_193(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix4_194(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix3_195(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix2_196(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefix1_197(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Prefixref_198(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Arglist_199(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ceq_200(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Cgt_201(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Cgt_Un_202(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Clt_203(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Clt_Un_204(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldftn_205(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldvirtftn_206(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarg_207(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldarga_208(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Starg_209(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloc_210(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Ldloca_211(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Stloc_212(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Localloc_213(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Endfilter_214(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Unaligned_215(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Volatile_216(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Tailcall_217(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Initobj_218(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Constrained_219(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Cpblk_220(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Initblk_221(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Rethrow_222(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Sizeof_223(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Refanytype_224(),
	OpCodes_t3494785031_StaticFields::get_offset_of_Readonly_225(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (OperandType_t22140925)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1067[19] = 
{
	OperandType_t22140925::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (PEFileKinds_t4139237606)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1068[4] = 
{
	PEFileKinds_t4139237606::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (PackingSize_t4013171414)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1069[10] = 
{
	PackingSize_t4013171414::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (ParameterBuilder_t3344728474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1070[8] = 
{
	ParameterBuilder_t3344728474::get_offset_of_methodb_0(),
	ParameterBuilder_t3344728474::get_offset_of_name_1(),
	ParameterBuilder_t3344728474::get_offset_of_cattrs_2(),
	ParameterBuilder_t3344728474::get_offset_of_marshal_info_3(),
	ParameterBuilder_t3344728474::get_offset_of_attrs_4(),
	ParameterBuilder_t3344728474::get_offset_of_position_5(),
	ParameterBuilder_t3344728474::get_offset_of_table_idx_6(),
	ParameterBuilder_t3344728474::get_offset_of_def_value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (PropertyBuilder_t3694255912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1071[15] = 
{
	PropertyBuilder_t3694255912::get_offset_of_attrs_0(),
	PropertyBuilder_t3694255912::get_offset_of_name_1(),
	PropertyBuilder_t3694255912::get_offset_of_type_2(),
	PropertyBuilder_t3694255912::get_offset_of_parameters_3(),
	PropertyBuilder_t3694255912::get_offset_of_cattrs_4(),
	PropertyBuilder_t3694255912::get_offset_of_def_value_5(),
	PropertyBuilder_t3694255912::get_offset_of_set_method_6(),
	PropertyBuilder_t3694255912::get_offset_of_get_method_7(),
	PropertyBuilder_t3694255912::get_offset_of_table_idx_8(),
	PropertyBuilder_t3694255912::get_offset_of_typeb_9(),
	PropertyBuilder_t3694255912::get_offset_of_returnModReq_10(),
	PropertyBuilder_t3694255912::get_offset_of_returnModOpt_11(),
	PropertyBuilder_t3694255912::get_offset_of_paramModReq_12(),
	PropertyBuilder_t3694255912::get_offset_of_paramModOpt_13(),
	PropertyBuilder_t3694255912::get_offset_of_callingConvention_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (StackBehaviour_t1390406961)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1072[30] = 
{
	StackBehaviour_t1390406961::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (TypeBuilder_t3308873219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1073[26] = 
{
	TypeBuilder_t3308873219::get_offset_of_tname_8(),
	TypeBuilder_t3308873219::get_offset_of_nspace_9(),
	TypeBuilder_t3308873219::get_offset_of_parent_10(),
	TypeBuilder_t3308873219::get_offset_of_nesting_type_11(),
	TypeBuilder_t3308873219::get_offset_of_interfaces_12(),
	TypeBuilder_t3308873219::get_offset_of_num_methods_13(),
	TypeBuilder_t3308873219::get_offset_of_methods_14(),
	TypeBuilder_t3308873219::get_offset_of_ctors_15(),
	TypeBuilder_t3308873219::get_offset_of_properties_16(),
	TypeBuilder_t3308873219::get_offset_of_num_fields_17(),
	TypeBuilder_t3308873219::get_offset_of_fields_18(),
	TypeBuilder_t3308873219::get_offset_of_events_19(),
	TypeBuilder_t3308873219::get_offset_of_cattrs_20(),
	TypeBuilder_t3308873219::get_offset_of_subtypes_21(),
	TypeBuilder_t3308873219::get_offset_of_attrs_22(),
	TypeBuilder_t3308873219::get_offset_of_table_idx_23(),
	TypeBuilder_t3308873219::get_offset_of_pmodule_24(),
	TypeBuilder_t3308873219::get_offset_of_class_size_25(),
	TypeBuilder_t3308873219::get_offset_of_packing_size_26(),
	TypeBuilder_t3308873219::get_offset_of_generic_container_27(),
	TypeBuilder_t3308873219::get_offset_of_generic_params_28(),
	TypeBuilder_t3308873219::get_offset_of_permissions_29(),
	TypeBuilder_t3308873219::get_offset_of_created_30(),
	TypeBuilder_t3308873219::get_offset_of_fullname_31(),
	TypeBuilder_t3308873219::get_offset_of_createTypeCalled_32(),
	TypeBuilder_t3308873219::get_offset_of_underlying_type_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (UnmanagedMarshal_t4270021860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1074[9] = 
{
	UnmanagedMarshal_t4270021860::get_offset_of_count_0(),
	UnmanagedMarshal_t4270021860::get_offset_of_t_1(),
	UnmanagedMarshal_t4270021860::get_offset_of_tbase_2(),
	UnmanagedMarshal_t4270021860::get_offset_of_guid_3(),
	UnmanagedMarshal_t4270021860::get_offset_of_mcookie_4(),
	UnmanagedMarshal_t4270021860::get_offset_of_marshaltype_5(),
	UnmanagedMarshal_t4270021860::get_offset_of_marshaltyperef_6(),
	UnmanagedMarshal_t4270021860::get_offset_of_param_num_7(),
	UnmanagedMarshal_t4270021860::get_offset_of_has_size_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (Assembly_t4268412390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1075[10] = 
{
	Assembly_t4268412390::get_offset_of__mono_assembly_0(),
	Assembly_t4268412390::get_offset_of_resolve_event_holder_1(),
	Assembly_t4268412390::get_offset_of__evidence_2(),
	Assembly_t4268412390::get_offset_of__minimum_3(),
	Assembly_t4268412390::get_offset_of__optional_4(),
	Assembly_t4268412390::get_offset_of__refuse_5(),
	Assembly_t4268412390::get_offset_of__granted_6(),
	Assembly_t4268412390::get_offset_of__denied_7(),
	Assembly_t4268412390::get_offset_of_fromByteArray_8(),
	Assembly_t4268412390::get_offset_of_assemblyName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (ResolveEventHolder_t1761494505), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (UnmanagedMemoryStreamForModule_t2398216387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1077[1] = 
{
	UnmanagedMemoryStreamForModule_t2398216387::get_offset_of_module_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (AssemblyName_t894705941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1078[16] = 
{
	AssemblyName_t894705941::get_offset_of_name_0(),
	AssemblyName_t894705941::get_offset_of_codebase_1(),
	AssemblyName_t894705941::get_offset_of_major_2(),
	AssemblyName_t894705941::get_offset_of_minor_3(),
	AssemblyName_t894705941::get_offset_of_build_4(),
	AssemblyName_t894705941::get_offset_of_revision_5(),
	AssemblyName_t894705941::get_offset_of_cultureinfo_6(),
	AssemblyName_t894705941::get_offset_of_flags_7(),
	AssemblyName_t894705941::get_offset_of_hashalg_8(),
	AssemblyName_t894705941::get_offset_of_keypair_9(),
	AssemblyName_t894705941::get_offset_of_publicKey_10(),
	AssemblyName_t894705941::get_offset_of_keyToken_11(),
	AssemblyName_t894705941::get_offset_of_versioncompat_12(),
	AssemblyName_t894705941::get_offset_of_version_13(),
	AssemblyName_t894705941::get_offset_of_processor_architecture_14(),
	AssemblyName_t894705941::get_offset_of_contentType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (ConstructorInfo_t2851816542), -1, sizeof(ConstructorInfo_t2851816542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1079[2] = 
{
	ConstructorInfo_t2851816542_StaticFields::get_offset_of_ConstructorName_0(),
	ConstructorInfo_t2851816542_StaticFields::get_offset_of_TypeConstructorName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (CustomAttributeData_t3093286891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1080[4] = 
{
	CustomAttributeData_t3093286891::get_offset_of_ctorInfo_0(),
	CustomAttributeData_t3093286891::get_offset_of_ctorArgs_1(),
	CustomAttributeData_t3093286891::get_offset_of_namedArgs_2(),
	CustomAttributeData_t3093286891::get_offset_of_lazyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (LazyCAttrData_t2380698664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1081[3] = 
{
	LazyCAttrData_t2380698664::get_offset_of_assembly_0(),
	LazyCAttrData_t2380698664::get_offset_of_data_1(),
	LazyCAttrData_t2380698664::get_offset_of_data_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (CustomAttributeFormatException_t3538197047), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (CustomAttributeNamedArgument_t94157543)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[2] = 
{
	CustomAttributeNamedArgument_t94157543::get_offset_of_typedArgument_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CustomAttributeNamedArgument_t94157543::get_offset_of_memberInfo_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (CustomAttributeTypedArgument_t1498197914)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1084[2] = 
{
	CustomAttributeTypedArgument_t1498197914::get_offset_of_argumentType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CustomAttributeTypedArgument_t1498197914::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (EventInfo_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1085[1] = 
{
	EventInfo_t::get_offset_of_cached_add_event_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (AddEventAdapter_t1766862959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (FieldInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (ImageFileMachine_t128670190)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1088[5] = 
{
	ImageFileMachine_t128670190::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (LocalVariableInfo_t1749284021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1089[3] = 
{
	LocalVariableInfo_t1749284021::get_offset_of_type_0(),
	LocalVariableInfo_t1749284021::get_offset_of_is_pinned_1(),
	LocalVariableInfo_t1749284021::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (ResolveTokenError_t3542011731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1090[4] = 
{
	ResolveTokenError_t3542011731::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (Module_t4282841206), -1, sizeof(Module_t4282841206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1091[10] = 
{
	Module_t4282841206_StaticFields::get_offset_of_FilterTypeName_0(),
	Module_t4282841206_StaticFields::get_offset_of_FilterTypeNameIgnoreCase_1(),
	Module_t4282841206::get_offset_of__impl_2(),
	Module_t4282841206::get_offset_of_assembly_3(),
	Module_t4282841206::get_offset_of_fqname_4(),
	Module_t4282841206::get_offset_of_name_5(),
	Module_t4282841206::get_offset_of_scopename_6(),
	Module_t4282841206::get_offset_of_is_resource_7(),
	Module_t4282841206::get_offset_of_token_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (RuntimeAssembly_t1913607566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (MonoAssembly_t183727305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (MonoEventInfo_t2190036573)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1094[8] = 
{
	MonoEventInfo_t2190036573::get_offset_of_declaring_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_reflected_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_add_method_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_remove_method_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_raise_method_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_attrs_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoEventInfo_t2190036573::get_offset_of_other_methods_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (RuntimeEventInfo_t3861156374), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (MonoEvent_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1096[2] = 
{
	MonoEvent_t::get_offset_of_klass_1(),
	MonoEvent_t::get_offset_of_handle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (RuntimeFieldInfo_t1687134186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (RtFieldInfo_t999161580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (MonoField_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1099[5] = 
{
	MonoField_t::get_offset_of_klass_0(),
	MonoField_t::get_offset_of_fhandle_1(),
	MonoField_t::get_offset_of_name_2(),
	MonoField_t::get_offset_of_type_3(),
	MonoField_t::get_offset_of_attrs_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
