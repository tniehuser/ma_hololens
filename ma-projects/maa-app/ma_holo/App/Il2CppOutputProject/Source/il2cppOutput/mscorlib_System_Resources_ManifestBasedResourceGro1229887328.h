﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Resources.ResourceManager/ResourceManagerMediator
struct ResourceManagerMediator_t1155374454;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ManifestBasedResourceGroveler
struct  ManifestBasedResourceGroveler_t1229887328  : public Il2CppObject
{
public:
	// System.Resources.ResourceManager/ResourceManagerMediator System.Resources.ManifestBasedResourceGroveler::_mediator
	ResourceManagerMediator_t1155374454 * ____mediator_0;

public:
	inline static int32_t get_offset_of__mediator_0() { return static_cast<int32_t>(offsetof(ManifestBasedResourceGroveler_t1229887328, ____mediator_0)); }
	inline ResourceManagerMediator_t1155374454 * get__mediator_0() const { return ____mediator_0; }
	inline ResourceManagerMediator_t1155374454 ** get_address_of__mediator_0() { return &____mediator_0; }
	inline void set__mediator_0(ResourceManagerMediator_t1155374454 * value)
	{
		____mediator_0 = value;
		Il2CppCodeGenWriteBarrier(&____mediator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
