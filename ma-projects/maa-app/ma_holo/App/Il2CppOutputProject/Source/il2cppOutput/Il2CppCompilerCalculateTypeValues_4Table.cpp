﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CallingConventions1097349142.h"
#include "mscorlib_System_Reflection_DefaultMemberAttribute889804479.h"
#include "mscorlib_System_Reflection_Emit_SymbolType2424957512.h"
#include "mscorlib_System_Reflection_EventAttributes2989788983.h"
#include "mscorlib_System_Reflection_FieldAttributes1122705193.h"
#include "mscorlib_System_Reflection_GenericParameterAttribu1251179291.h"
#include "mscorlib_System_Reflection_IntrospectionExtensions329995609.h"
#include "mscorlib_System_Reflection_InvalidFilterCriteriaExc998833949.h"
#include "mscorlib_System_Reflection_ManifestResourceInfo2035432027.h"
#include "mscorlib_System_Reflection_ResourceLocation999505379.h"
#include "mscorlib_System_Reflection_PInvokeAttributes2345979563.h"
#include "mscorlib_System_Reflection_MemberFilter3405857066.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Reflection_MemberInfoSerialization2799051170.h"
#include "mscorlib_System_Reflection_MemberTypes3343038963.h"
#include "mscorlib_System_Reflection_MethodAttributes790385034.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_MethodImplAttributes1541361196.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_Missing1033855606.h"
#include "mscorlib_System_Reflection_ParameterAttributes1266705348.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Reflection_Pointer937075087.h"
#include "mscorlib_System_Reflection_PropertyAttributes883448530.h"
#include "mscorlib_System_Reflection_ResourceAttributes2389678029.h"
#include "mscorlib_System_Reflection_TargetException1572104820.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti4098620458.h"
#include "mscorlib_System_Reflection_TargetParameterCountExc1554451430.h"
#include "mscorlib_System_Reflection_TypeAttributes2229518203.h"
#include "mscorlib_System_Reflection_TypeDelegator1357031879.h"
#include "mscorlib_System_Reflection_TypeFilter2905709404.h"
#include "mscorlib_System_Reflection_TypeInfo3822613806.h"
#include "mscorlib_System_Resources_FastResourceComparer3927788191.h"
#include "mscorlib_System_Resources_ManifestBasedResourceGro1229887328.h"
#include "mscorlib_System_Resources_MissingManifestResourceE1579693920.h"
#include "mscorlib_System_Resources_NeutralResourcesLanguage3267676636.h"
#include "mscorlib_System_Resources_ResourceManager264715885.h"
#include "mscorlib_System_Resources_ResourceManager_CultureNa736438917.h"
#include "mscorlib_System_Resources_ResourceManager_Resource1155374454.h"
#include "mscorlib_System_Resources_ResourceLocator2156390884.h"
#include "mscorlib_System_Resources_ResourceReader2463923611.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceE2665690338.h"
#include "mscorlib_System_Resources_ResourceSet1348327650.h"
#include "mscorlib_System_Resources_ResourceTypeCode3230235143.h"
#include "mscorlib_System_Resources_RuntimeResourceSet1442459318.h"
#include "mscorlib_System_Resources_SatelliteContractVersion2989984391.h"
#include "mscorlib_System_Resources_UltimateResourceFallback2904517644.h"
#include "mscorlib_System_TypeNameFormatFlags3292475613.h"
#include "mscorlib_System_TypeNameKind180927137.h"
#include "mscorlib_System_RuntimeType2836228502.h"
#include "mscorlib_System_RuntimeType_MemberListType1305728218.h"
#include "mscorlib_System_ReflectionOnlyType2913395635.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask3842594813.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncMeth2485284745.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncMetho781250562.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncMeth1692486082.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncMeth2123198672.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncStat3135501664.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCo2430705542.h"
#include "mscorlib_System_Runtime_CompilerServices_StateMachi815992224.h"
#include "mscorlib_System_Runtime_CompilerServices_TaskAwait3341738712.h"
#include "mscorlib_System_Runtime_CompilerServices_TypeForward68050954.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint3781660191.h"
#include "mscorlib_System_Runtime_CompilerServices_DefaultDe3858269114.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati4211964247.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilatio238494011.h"
#include "mscorlib_System_Runtime_CompilerServices_CompilerGe497097752.h"
#include "mscorlib_System_Runtime_CompilerServices_CustomCon2797584351.h"
#include "mscorlib_System_Runtime_CompilerServices_DateTimeC4275890373.h"
#include "mscorlib_System_Runtime_CompilerServices_DecimalCon569828555.h"
#include "mscorlib_System_Runtime_CompilerServices_Extension1840441203.h"
#include "mscorlib_System_Runtime_CompilerServices_FixedBuffe594856158.h"
#include "mscorlib_System_Runtime_CompilerServices_Internals1037732567.h"
#include "mscorlib_System_Runtime_CompilerServices_FriendAcc3329485784.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile700755342.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImp1849369677.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImp4056101785.h"
#include "mscorlib_System_Runtime_CompilerServices_TypeDepend220819457.h"
#include "mscorlib_System_Runtime_CompilerServices_UnsafeVal1767622487.h"
#include "mscorlib_System_Runtime_ExceptionServices_HandlePro813236102.h"
#include "mscorlib_System_Runtime_ExceptionServices_FirstCha3799922078.h"
#include "mscorlib_System_Runtime_ExceptionServices_Exceptio3341954301.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedF1696564987.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttri607560947.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfa1898221498.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceT4113096249.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultI347642415.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize400 = { sizeof (CallingConventions_t1097349142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable400[6] = 
{
	CallingConventions_t1097349142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize401 = { sizeof (DefaultMemberAttribute_t889804479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable401[1] = 
{
	DefaultMemberAttribute_t889804479::get_offset_of_m_memberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize402 = { sizeof (SymbolType_t2424957512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable402[1] = 
{
	SymbolType_t2424957512::get_offset_of_m_baseType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize403 = { sizeof (EventAttributes_t2989788983)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable403[5] = 
{
	EventAttributes_t2989788983::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize404 = { sizeof (FieldAttributes_t1122705193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable404[20] = 
{
	FieldAttributes_t1122705193::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize405 = { sizeof (GenericParameterAttributes_t1251179291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable405[9] = 
{
	GenericParameterAttributes_t1251179291::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize406 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize407 = { sizeof (IntrospectionExtensions_t329995609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize408 = { sizeof (InvalidFilterCriteriaException_t998833949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize409 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize411 = { sizeof (ManifestResourceInfo_t2035432027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable411[3] = 
{
	ManifestResourceInfo_t2035432027::get_offset_of__containingAssembly_0(),
	ManifestResourceInfo_t2035432027::get_offset_of__containingFileName_1(),
	ManifestResourceInfo_t2035432027::get_offset_of__resourceLocation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize412 = { sizeof (ResourceLocation_t999505379)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable412[4] = 
{
	ResourceLocation_t999505379::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize413 = { sizeof (PInvokeAttributes_t2345979563)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable413[23] = 
{
	PInvokeAttributes_t2345979563::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize414 = { sizeof (MemberFilter_t3405857066), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize415 = { sizeof (MemberInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize416 = { sizeof (MemberInfoSerializationHolder_t2799051170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable416[6] = 
{
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_memberName_0(),
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_reflectedType_1(),
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_signature_2(),
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_signature2_3(),
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_memberType_4(),
	MemberInfoSerializationHolder_t2799051170::get_offset_of_m_info_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize417 = { sizeof (MemberTypes_t3343038963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable417[10] = 
{
	MemberTypes_t3343038963::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize418 = { sizeof (MethodAttributes_t790385034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable418[25] = 
{
	MethodAttributes_t790385034::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize419 = { sizeof (MethodBase_t904190842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize420 = { sizeof (MethodImplAttributes_t1541361196)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable420[17] = 
{
	MethodImplAttributes_t1541361196::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize421 = { sizeof (MethodInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize422 = { sizeof (Missing_t1033855606), -1, sizeof(Missing_t1033855606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable422[1] = 
{
	Missing_t1033855606_StaticFields::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize423 = { sizeof (ParameterAttributes_t1266705348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable423[12] = 
{
	ParameterAttributes_t1266705348::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize424 = { sizeof (ParameterModifier_t1820634920)+ sizeof (Il2CppObject), sizeof(ParameterModifier_t1820634920_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable424[1] = 
{
	ParameterModifier_t1820634920::get_offset_of__byRef_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize425 = { sizeof (Pointer_t937075087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable425[2] = 
{
	Pointer_t937075087::get_offset_of__ptr_0(),
	Pointer_t937075087::get_offset_of__ptrType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize426 = { sizeof (PropertyAttributes_t883448530)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable426[9] = 
{
	PropertyAttributes_t883448530::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize427 = { sizeof (ResourceAttributes_t2389678029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable427[3] = 
{
	ResourceAttributes_t2389678029::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize428 = { sizeof (TargetException_t1572104820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize429 = { sizeof (TargetInvocationException_t4098620458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize430 = { sizeof (TargetParameterCountException_t1554451430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize431 = { sizeof (TypeAttributes_t2229518203)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable431[33] = 
{
	TypeAttributes_t2229518203::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize432 = { sizeof (TypeDelegator_t1357031879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable432[1] = 
{
	TypeDelegator_t1357031879::get_offset_of_typeImpl_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize433 = { sizeof (TypeFilter_t2905709404), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize434 = { sizeof (TypeInfo_t3822613806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize435 = { sizeof (FastResourceComparer_t3927788191), -1, sizeof(FastResourceComparer_t3927788191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable435[1] = 
{
	FastResourceComparer_t3927788191_StaticFields::get_offset_of_Default_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize436 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize437 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize438 = { sizeof (ManifestBasedResourceGroveler_t1229887328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable438[1] = 
{
	ManifestBasedResourceGroveler_t1229887328::get_offset_of__mediator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize439 = { sizeof (MissingManifestResourceException_t1579693920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize440 = { sizeof (NeutralResourcesLanguageAttribute_t3267676636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable440[2] = 
{
	NeutralResourcesLanguageAttribute_t3267676636::get_offset_of__culture_0(),
	NeutralResourcesLanguageAttribute_t3267676636::get_offset_of__fallbackLoc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize441 = { sizeof (ResourceManager_t264715885), -1, sizeof(ResourceManager_t264715885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable441[10] = 
{
	ResourceManager_t264715885::get_offset_of__lastUsedResourceCache_0(),
	ResourceManager_t264715885::get_offset_of_m_callingAssembly_1(),
	ResourceManager_t264715885::get_offset_of_resourceGroveler_2(),
	ResourceManager_t264715885_StaticFields::get_offset_of_MagicNumber_3(),
	ResourceManager_t264715885_StaticFields::get_offset_of_HeaderVersionNumber_4(),
	ResourceManager_t264715885_StaticFields::get_offset_of__minResourceSet_5(),
	ResourceManager_t264715885_StaticFields::get_offset_of_ResReaderTypeName_6(),
	ResourceManager_t264715885_StaticFields::get_offset_of_ResSetTypeName_7(),
	ResourceManager_t264715885_StaticFields::get_offset_of_MscorlibName_8(),
	ResourceManager_t264715885_StaticFields::get_offset_of_DEBUG_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize442 = { sizeof (CultureNameResourceSetPair_t736438917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize443 = { sizeof (ResourceManagerMediator_t1155374454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable443[1] = 
{
	ResourceManagerMediator_t1155374454::get_offset_of__rm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize444 = { sizeof (ResourceLocator_t2156390884)+ sizeof (Il2CppObject), sizeof(ResourceLocator_t2156390884_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable444[2] = 
{
	ResourceLocator_t2156390884::get_offset_of__value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceLocator_t2156390884::get_offset_of__dataPos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize445 = { sizeof (ResourceReader_t2463923611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable445[14] = 
{
	ResourceReader_t2463923611::get_offset_of__store_0(),
	ResourceReader_t2463923611::get_offset_of__resCache_1(),
	ResourceReader_t2463923611::get_offset_of__nameSectionOffset_2(),
	ResourceReader_t2463923611::get_offset_of__dataSectionOffset_3(),
	ResourceReader_t2463923611::get_offset_of__nameHashes_4(),
	ResourceReader_t2463923611::get_offset_of__nameHashesPtr_5(),
	ResourceReader_t2463923611::get_offset_of__namePositions_6(),
	ResourceReader_t2463923611::get_offset_of__namePositionsPtr_7(),
	ResourceReader_t2463923611::get_offset_of__typeTable_8(),
	ResourceReader_t2463923611::get_offset_of__typeNamePositions_9(),
	ResourceReader_t2463923611::get_offset_of__objFormatter_10(),
	ResourceReader_t2463923611::get_offset_of__numResources_11(),
	ResourceReader_t2463923611::get_offset_of__ums_12(),
	ResourceReader_t2463923611::get_offset_of__version_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize446 = { sizeof (ResourceEnumerator_t2665690338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable446[4] = 
{
	ResourceEnumerator_t2665690338::get_offset_of__reader_0(),
	ResourceEnumerator_t2665690338::get_offset_of__currentIsValid_1(),
	ResourceEnumerator_t2665690338::get_offset_of__currentName_2(),
	ResourceEnumerator_t2665690338::get_offset_of__dataPosition_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize447 = { sizeof (ResourceSet_t1348327650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable447[3] = 
{
	ResourceSet_t1348327650::get_offset_of_Reader_0(),
	ResourceSet_t1348327650::get_offset_of_Table_1(),
	ResourceSet_t1348327650::get_offset_of__caseInsensitiveTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize448 = { sizeof (ResourceTypeCode_t3230235143)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable448[22] = 
{
	ResourceTypeCode_t3230235143::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize449 = { sizeof (RuntimeResourceSet_t1442459318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable449[5] = 
{
	0,
	RuntimeResourceSet_t1442459318::get_offset_of__resCache_4(),
	RuntimeResourceSet_t1442459318::get_offset_of__defaultReader_5(),
	RuntimeResourceSet_t1442459318::get_offset_of__caseInsensitiveTable_6(),
	RuntimeResourceSet_t1442459318::get_offset_of__haveReadFromReader_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize450 = { sizeof (SatelliteContractVersionAttribute_t2989984391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable450[1] = 
{
	SatelliteContractVersionAttribute_t2989984391::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize451 = { sizeof (UltimateResourceFallbackLocation_t2904517644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable451[3] = 
{
	UltimateResourceFallbackLocation_t2904517644::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize452 = { sizeof (TypeNameFormatFlags_t3292475613)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable452[11] = 
{
	TypeNameFormatFlags_t3292475613::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize453 = { sizeof (TypeNameKind_t180927137)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable453[5] = 
{
	TypeNameKind_t180927137::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize454 = { sizeof (RuntimeType_t2836228502), -1, sizeof(RuntimeType_t2836228502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable454[21] = 
{
	RuntimeType_t2836228502_StaticFields::get_offset_of_ValueType_8(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_EnumType_9(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_ObjectType_10(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_StringType_11(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_DelegateType_12(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_s_SICtorParamTypes_13(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RuntimeType_t2836228502_StaticFields::get_offset_of_s_typedRef_23(),
	RuntimeType_t2836228502::get_offset_of_type_info_24(),
	RuntimeType_t2836228502::get_offset_of_GenericCache_25(),
	RuntimeType_t2836228502::get_offset_of_m_serializationCtor_26(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_clsid_types_27(),
	RuntimeType_t2836228502_StaticFields::get_offset_of_clsid_assemblybuilder_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize455 = { sizeof (MemberListType_t1305728218)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable455[5] = 
{
	MemberListType_t1305728218::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize456 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable456[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize457 = { sizeof (ReflectionOnlyType_t2913395635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize458 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable458[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize459 = { sizeof (AsyncTaskCache_t3842594813), -1, sizeof(AsyncTaskCache_t3842594813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable459[3] = 
{
	AsyncTaskCache_t3842594813_StaticFields::get_offset_of_TrueTask_0(),
	AsyncTaskCache_t3842594813_StaticFields::get_offset_of_FalseTask_1(),
	AsyncTaskCache_t3842594813_StaticFields::get_offset_of_Int32Tasks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize460 = { sizeof (AsyncMethodBuilderCore_t2485284745)+ sizeof (Il2CppObject), -1, sizeof(AsyncMethodBuilderCore_t2485284745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable460[4] = 
{
	AsyncMethodBuilderCore_t2485284745::get_offset_of_m_stateMachine_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AsyncMethodBuilderCore_t2485284745::get_offset_of_m_defaultContextAction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AsyncMethodBuilderCore_t2485284745_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	AsyncMethodBuilderCore_t2485284745_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize461 = { sizeof (MoveNextRunner_t781250562), -1, sizeof(MoveNextRunner_t781250562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable461[4] = 
{
	MoveNextRunner_t781250562::get_offset_of_m_context_0(),
	MoveNextRunner_t781250562::get_offset_of_m_stateMachine_1(),
	MoveNextRunner_t781250562_StaticFields::get_offset_of_s_invokeMoveNext_2(),
	MoveNextRunner_t781250562_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize462 = { sizeof (ContinuationWrapper_t1692486082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable462[3] = 
{
	ContinuationWrapper_t1692486082::get_offset_of_m_continuation_0(),
	ContinuationWrapper_t1692486082::get_offset_of_m_invokeAction_1(),
	ContinuationWrapper_t1692486082::get_offset_of_m_innerTask_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize463 = { sizeof (U3COutputAsyncCausalityEventsU3Ec__AnonStorey0_t2123198672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable463[2] = 
{
	U3COutputAsyncCausalityEventsU3Ec__AnonStorey0_t2123198672::get_offset_of_innerTask_0(),
	U3COutputAsyncCausalityEventsU3Ec__AnonStorey0_t2123198672::get_offset_of_continuation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize464 = { sizeof (AsyncStateMachineAttribute_t3135501664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize467 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize468 = { sizeof (RuntimeCompatibilityAttribute_t2430705542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable468[1] = 
{
	RuntimeCompatibilityAttribute_t2430705542::get_offset_of_m_wrapNonExceptionThrows_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize469 = { sizeof (StateMachineAttribute_t815992224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable469[1] = 
{
	StateMachineAttribute_t815992224::get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize470 = { sizeof (TaskAwaiter_t3341738712)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable470[1] = 
{
	TaskAwaiter_t3341738712::get_offset_of_m_task_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize471 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable471[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize472 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable472[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize473 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable473[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize474 = { sizeof (TypeForwardedFromAttribute_t68050954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable474[1] = 
{
	TypeForwardedFromAttribute_t68050954::get_offset_of_assemblyFullName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize475 = { sizeof (LoadHint_t3781660191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable475[4] = 
{
	LoadHint_t3781660191::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize476 = { sizeof (DefaultDependencyAttribute_t3858269114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable476[1] = 
{
	DefaultDependencyAttribute_t3858269114::get_offset_of_loadHint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize477 = { sizeof (CompilationRelaxations_t4211964247)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable477[2] = 
{
	CompilationRelaxations_t4211964247::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize478 = { sizeof (CompilationRelaxationsAttribute_t238494011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable478[1] = 
{
	CompilationRelaxationsAttribute_t238494011::get_offset_of_m_relaxations_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize479 = { sizeof (CompilerGeneratedAttribute_t497097752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize480 = { sizeof (CustomConstantAttribute_t2797584351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize481 = { sizeof (DateTimeConstantAttribute_t4275890373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable481[1] = 
{
	DateTimeConstantAttribute_t4275890373::get_offset_of_date_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize482 = { sizeof (DecimalConstantAttribute_t569828555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable482[1] = 
{
	DecimalConstantAttribute_t569828555::get_offset_of_dec_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize483 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize484 = { sizeof (FixedBufferAttribute_t594856158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable484[2] = 
{
	FixedBufferAttribute_t594856158::get_offset_of_elementType_0(),
	FixedBufferAttribute_t594856158::get_offset_of_length_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize485 = { sizeof (InternalsVisibleToAttribute_t1037732567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable485[2] = 
{
	InternalsVisibleToAttribute_t1037732567::get_offset_of__assemblyName_0(),
	InternalsVisibleToAttribute_t1037732567::get_offset_of__allInternalsVisible_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize486 = { sizeof (FriendAccessAllowedAttribute_t3329485784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize487 = { sizeof (IsVolatile_t700755342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize488 = { sizeof (MethodImplOptions_t1849369677)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable488[9] = 
{
	MethodImplOptions_t1849369677::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize489 = { sizeof (MethodImplAttribute_t4056101785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable489[1] = 
{
	MethodImplAttribute_t4056101785::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize490 = { sizeof (TypeDependencyAttribute_t220819457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable490[1] = 
{
	TypeDependencyAttribute_t220819457::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize491 = { sizeof (UnsafeValueTypeAttribute_t1767622487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize492 = { sizeof (HandleProcessCorruptedStateExceptionsAttribute_t813236102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize493 = { sizeof (FirstChanceExceptionEventArgs_t3799922078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize494 = { sizeof (ExceptionDispatchInfo_t3341954301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable494[2] = 
{
	ExceptionDispatchInfo_t3341954301::get_offset_of_m_Exception_0(),
	ExceptionDispatchInfo_t3341954301::get_offset_of_m_stackTrace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize495 = { sizeof (UnmanagedFunctionPointerAttribute_t1696564987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable495[1] = 
{
	UnmanagedFunctionPointerAttribute_t1696564987::get_offset_of_m_callingConvention_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize496 = { sizeof (DispIdAttribute_t607560947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable496[1] = 
{
	DispIdAttribute_t607560947::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize497 = { sizeof (ComInterfaceType_t1898221498)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable497[5] = 
{
	ComInterfaceType_t1898221498::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize498 = { sizeof (InterfaceTypeAttribute_t4113096249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable498[1] = 
{
	InterfaceTypeAttribute_t4113096249::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize499 = { sizeof (ComDefaultInterfaceAttribute_t347642415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable499[1] = 
{
	ComDefaultInterfaceAttribute_t347642415::get_offset_of__val_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
