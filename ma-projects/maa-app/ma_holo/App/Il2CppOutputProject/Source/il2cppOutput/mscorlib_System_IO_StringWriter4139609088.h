﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter4027217640.h"

// System.Text.UnicodeEncoding
struct UnicodeEncoding_t4081757012;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StringWriter
struct  StringWriter_t4139609088  : public TextWriter_t4027217640
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::_sb
	StringBuilder_t1221177846 * ____sb_12;
	// System.Boolean System.IO.StringWriter::_isOpen
	bool ____isOpen_13;

public:
	inline static int32_t get_offset_of__sb_12() { return static_cast<int32_t>(offsetof(StringWriter_t4139609088, ____sb_12)); }
	inline StringBuilder_t1221177846 * get__sb_12() const { return ____sb_12; }
	inline StringBuilder_t1221177846 ** get_address_of__sb_12() { return &____sb_12; }
	inline void set__sb_12(StringBuilder_t1221177846 * value)
	{
		____sb_12 = value;
		Il2CppCodeGenWriteBarrier(&____sb_12, value);
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(StringWriter_t4139609088, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}
};

struct StringWriter_t4139609088_StaticFields
{
public:
	// System.Text.UnicodeEncoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StringWriter::m_encoding
	UnicodeEncoding_t4081757012 * ___m_encoding_11;

public:
	inline static int32_t get_offset_of_m_encoding_11() { return static_cast<int32_t>(offsetof(StringWriter_t4139609088_StaticFields, ___m_encoding_11)); }
	inline UnicodeEncoding_t4081757012 * get_m_encoding_11() const { return ___m_encoding_11; }
	inline UnicodeEncoding_t4081757012 ** get_address_of_m_encoding_11() { return &___m_encoding_11; }
	inline void set_m_encoding_11(UnicodeEncoding_t4081757012 * value)
	{
		___m_encoding_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_encoding_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
