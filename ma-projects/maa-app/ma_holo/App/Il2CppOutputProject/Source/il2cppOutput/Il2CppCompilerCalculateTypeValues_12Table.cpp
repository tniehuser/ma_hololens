﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchron3073724998.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchron3779986825.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchroni462987365.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease3663008028.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_Ren194360041.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseMan1025868639.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSin3007073869.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState83447469.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lifetime2939669377.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo3252846202.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo688271106.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe2232356043.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientC3236389774.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru1254994451.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru2993650247.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTe3043186997.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header2756440555.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHa324204131.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodC2461541281.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MCMDicti159052147.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Message4157710479.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Message4235333696.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodR1456661140.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodRe981009581.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMeth771543475.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallTyp2486906258.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin3248446683.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefS3912784830.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin2821375126.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnM3411975905.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerC1054294306.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerO4261369100.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBu1613771438.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttr1982224933.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFiel3073759685.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMeth2381910676.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapPara2780084514.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapType3444503085.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttr4031752430.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_Transpare3836393972.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy298428346.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingP2419155897.h"
#include "mscorlib_System_Runtime_Remoting_Services_Tracking3722365321.h"
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTy4060499430.h"
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceT3934090848.h"
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo815109115.h"
#include "mscorlib_System_Runtime_Remoting_Identity3647548000.h"
#include "mscorlib_System_Runtime_Remoting_ClientIdentity2254682501.h"
#include "mscorlib_System_Runtime_Remoting_InternalRemotingS3953136710.h"
#include "mscorlib_System_Runtime_Remoting_ObjRef318414488.h"
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurat438177651.h"
#include "mscorlib_System_Runtime_Remoting_ConfigHandler2180714860.h"
#include "mscorlib_System_Runtime_Remoting_ChannelData1489610737.h"
#include "mscorlib_System_Runtime_Remoting_ProviderData2518653487.h"
#include "mscorlib_System_Runtime_Remoting_FormatterData12176916.h"
#include "mscorlib_System_Runtime_Remoting_RemotingException109604560.h"
#include "mscorlib_System_Runtime_Remoting_RemotingServices2399536837.h"
#include "mscorlib_System_Runtime_Remoting_ServerIdentity1656058977.h"
#include "mscorlib_System_Runtime_Remoting_ClientActivatedId1467784146.h"
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity164722255.h"
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentit3377680076.h"
#include "mscorlib_System_Runtime_Remoting_SoapServices3397513225.h"
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeIn59877052.h"
#include "mscorlib_System_Runtime_Remoting_TypeEntry3321373506.h"
#include "mscorlib_System_Runtime_Remoting_TypeInfo942537562.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTy3314744170.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMo2630225581.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceT1712728956.h"
#include "mscorlib_System_Runtime_Versioning_CompatibilitySw1185775942.h"
#include "mscorlib_System_Security_AccessControl_RegistryRigh427223974.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3842064707.h"
#include "mscorlib_System_Security_Cryptography_X509Certific2148381192.h"
#include "mscorlib_System_Security_Cryptography_X509Certific1751628948.h"
#include "mscorlib_System_Security_Cryptography_X509Certific1216946873.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig896479599.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig2096418190.h"
#include "mscorlib_System_Security_Cryptography_DESTransform179750796.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoSer2915171657.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (SynchronizationAttribute_t3073724998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1205[5] = 
{
	SynchronizationAttribute_t3073724998::get_offset_of__bReEntrant_1(),
	SynchronizationAttribute_t3073724998::get_offset_of__flavor_2(),
	SynchronizationAttribute_t3073724998::get_offset_of__lockCount_3(),
	SynchronizationAttribute_t3073724998::get_offset_of__mutex_4(),
	SynchronizationAttribute_t3073724998::get_offset_of__ownerThread_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (SynchronizedClientContextSink_t3779986825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[2] = 
{
	SynchronizedClientContextSink_t3779986825::get_offset_of__next_0(),
	SynchronizedClientContextSink_t3779986825::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (SynchronizedServerContextSink_t462987365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1207[2] = 
{
	SynchronizedServerContextSink_t462987365::get_offset_of__next_0(),
	SynchronizedServerContextSink_t462987365::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (Lease_t3663008028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1210[8] = 
{
	Lease_t3663008028::get_offset_of__leaseExpireTime_1(),
	Lease_t3663008028::get_offset_of__currentState_2(),
	Lease_t3663008028::get_offset_of__initialLeaseTime_3(),
	Lease_t3663008028::get_offset_of__renewOnCallTime_4(),
	Lease_t3663008028::get_offset_of__sponsorshipTimeout_5(),
	Lease_t3663008028::get_offset_of__sponsors_6(),
	Lease_t3663008028::get_offset_of__renewingSponsors_7(),
	Lease_t3663008028::get_offset_of__renewalDelegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (RenewalDelegate_t194360041), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (LeaseManager_t1025868639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[2] = 
{
	LeaseManager_t1025868639::get_offset_of__objects_0(),
	LeaseManager_t1025868639::get_offset_of__timer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (LeaseSink_t3007073869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1213[1] = 
{
	LeaseSink_t3007073869::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (LeaseState_t83447469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1214[6] = 
{
	LeaseState_t83447469::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (LifetimeServices_t2939669377), -1, sizeof(LifetimeServices_t2939669377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1215[5] = 
{
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseManagerPollTime_0(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseTime_1(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__renewOnCallTime_2(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__sponsorshipTimeout_3(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (ArgInfoType_t3252846202)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1216[3] = 
{
	ArgInfoType_t3252846202::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (ArgInfo_t688271106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[3] = 
{
	ArgInfo_t688271106::get_offset_of__paramMap_0(),
	ArgInfo_t688271106::get_offset_of__inoutArgCount_1(),
	ArgInfo_t688271106::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (AsyncResult_t2232356043), -1, sizeof(AsyncResult_t2232356043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1218[17] = 
{
	AsyncResult_t2232356043::get_offset_of_async_state_0(),
	AsyncResult_t2232356043::get_offset_of_handle_1(),
	AsyncResult_t2232356043::get_offset_of_async_delegate_2(),
	AsyncResult_t2232356043::get_offset_of_data_3(),
	AsyncResult_t2232356043::get_offset_of_object_data_4(),
	AsyncResult_t2232356043::get_offset_of_sync_completed_5(),
	AsyncResult_t2232356043::get_offset_of_completed_6(),
	AsyncResult_t2232356043::get_offset_of_endinvoke_called_7(),
	AsyncResult_t2232356043::get_offset_of_async_callback_8(),
	AsyncResult_t2232356043::get_offset_of_current_9(),
	AsyncResult_t2232356043::get_offset_of_original_10(),
	AsyncResult_t2232356043::get_offset_of_add_time_11(),
	AsyncResult_t2232356043::get_offset_of_call_message_12(),
	AsyncResult_t2232356043::get_offset_of_message_ctrl_13(),
	AsyncResult_t2232356043::get_offset_of_reply_message_14(),
	AsyncResult_t2232356043::get_offset_of_orig_cb_15(),
	AsyncResult_t2232356043_StaticFields::get_offset_of_ccb_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (ClientContextTerminatorSink_t3236389774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[1] = 
{
	ClientContextTerminatorSink_t3236389774::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (ConstructionCall_t1254994451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1220[6] = 
{
	ConstructionCall_t1254994451::get_offset_of__activator_11(),
	ConstructionCall_t1254994451::get_offset_of__activationAttributes_12(),
	ConstructionCall_t1254994451::get_offset_of__contextProperties_13(),
	ConstructionCall_t1254994451::get_offset_of__activationType_14(),
	ConstructionCall_t1254994451::get_offset_of__activationTypeName_15(),
	ConstructionCall_t1254994451::get_offset_of__isContextOk_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (ConstructionCallDictionary_t2993650247), -1, sizeof(ConstructionCallDictionary_t2993650247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1221[1] = 
{
	ConstructionCallDictionary_t2993650247_StaticFields::get_offset_of_InternalKeys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (EnvoyTerminatorSink_t3043186997), -1, sizeof(EnvoyTerminatorSink_t3043186997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1222[1] = 
{
	EnvoyTerminatorSink_t3043186997_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (Header_t2756440555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (HeaderHandler_t324204131), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (MethodCall_t2461541281), -1, sizeof(MethodCall_t2461541281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1234[11] = 
{
	MethodCall_t2461541281::get_offset_of__uri_0(),
	MethodCall_t2461541281::get_offset_of__typeName_1(),
	MethodCall_t2461541281::get_offset_of__methodName_2(),
	MethodCall_t2461541281::get_offset_of__args_3(),
	MethodCall_t2461541281::get_offset_of__methodSignature_4(),
	MethodCall_t2461541281::get_offset_of__methodBase_5(),
	MethodCall_t2461541281::get_offset_of__callContext_6(),
	MethodCall_t2461541281::get_offset_of__genericArguments_7(),
	MethodCall_t2461541281::get_offset_of_ExternalProperties_8(),
	MethodCall_t2461541281::get_offset_of_InternalProperties_9(),
	MethodCall_t2461541281_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (MCMDictionary_t159052147), -1, sizeof(MCMDictionary_t159052147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1235[1] = 
{
	MCMDictionary_t159052147_StaticFields::get_offset_of_InternalKeys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (MessageDictionary_t4157710479), -1, sizeof(MessageDictionary_t4157710479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1236[6] = 
{
	MessageDictionary_t4157710479::get_offset_of__internalProperties_0(),
	MessageDictionary_t4157710479::get_offset_of__message_1(),
	MessageDictionary_t4157710479::get_offset_of__methodKeys_2(),
	MessageDictionary_t4157710479::get_offset_of__ownProperties_3(),
	MessageDictionary_t4157710479_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_4(),
	MessageDictionary_t4157710479_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (DictionaryEnumerator_t4235333696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1237[3] = 
{
	DictionaryEnumerator_t4235333696::get_offset_of__methodDictionary_0(),
	DictionaryEnumerator_t4235333696::get_offset_of__hashtableEnum_1(),
	DictionaryEnumerator_t4235333696::get_offset_of__posMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (MethodResponse_t1456661140), -1, sizeof(MethodResponse_t1456661140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1238[15] = 
{
	MethodResponse_t1456661140::get_offset_of__methodName_0(),
	MethodResponse_t1456661140::get_offset_of__uri_1(),
	MethodResponse_t1456661140::get_offset_of__typeName_2(),
	MethodResponse_t1456661140::get_offset_of__methodBase_3(),
	MethodResponse_t1456661140::get_offset_of__returnValue_4(),
	MethodResponse_t1456661140::get_offset_of__exception_5(),
	MethodResponse_t1456661140::get_offset_of__methodSignature_6(),
	MethodResponse_t1456661140::get_offset_of__inArgInfo_7(),
	MethodResponse_t1456661140::get_offset_of__args_8(),
	MethodResponse_t1456661140::get_offset_of__outArgs_9(),
	MethodResponse_t1456661140::get_offset_of__callMsg_10(),
	MethodResponse_t1456661140::get_offset_of__callContext_11(),
	MethodResponse_t1456661140::get_offset_of_ExternalProperties_12(),
	MethodResponse_t1456661140::get_offset_of_InternalProperties_13(),
	MethodResponse_t1456661140_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (MethodReturnDictionary_t981009581), -1, sizeof(MethodReturnDictionary_t981009581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1239[2] = 
{
	MethodReturnDictionary_t981009581_StaticFields::get_offset_of_InternalReturnKeys_6(),
	MethodReturnDictionary_t981009581_StaticFields::get_offset_of_InternalExceptionKeys_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (MonoMethodMessage_t771543475), -1, sizeof(MonoMethodMessage_t771543475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1240[15] = 
{
	MonoMethodMessage_t771543475::get_offset_of_method_0(),
	MonoMethodMessage_t771543475::get_offset_of_args_1(),
	MonoMethodMessage_t771543475::get_offset_of_names_2(),
	MonoMethodMessage_t771543475::get_offset_of_arg_types_3(),
	MonoMethodMessage_t771543475::get_offset_of_ctx_4(),
	MonoMethodMessage_t771543475::get_offset_of_rval_5(),
	MonoMethodMessage_t771543475::get_offset_of_exc_6(),
	MonoMethodMessage_t771543475::get_offset_of_asyncResult_7(),
	MonoMethodMessage_t771543475::get_offset_of_call_type_8(),
	MonoMethodMessage_t771543475::get_offset_of_uri_9(),
	MonoMethodMessage_t771543475::get_offset_of_properties_10(),
	MonoMethodMessage_t771543475::get_offset_of_methodSignature_11(),
	MonoMethodMessage_t771543475::get_offset_of_identity_12(),
	MonoMethodMessage_t771543475_StaticFields::get_offset_of_CallContextKey_13(),
	MonoMethodMessage_t771543475_StaticFields::get_offset_of_UriKey_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (CallType_t2486906258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1241[5] = 
{
	CallType_t2486906258::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (RemotingSurrogate_t3248446683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (ObjRefSurrogate_t3912784830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (RemotingSurrogateSelector_t2821375126), -1, sizeof(RemotingSurrogateSelector_t2821375126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1244[4] = 
{
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of_s_cachedTypeObjRef_0(),
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of__objRefSurrogate_1(),
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of__objRemotingSurrogate_2(),
	RemotingSurrogateSelector_t2821375126::get_offset_of__next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (ReturnMessage_t3411975905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1245[12] = 
{
	ReturnMessage_t3411975905::get_offset_of__outArgs_0(),
	ReturnMessage_t3411975905::get_offset_of__args_1(),
	ReturnMessage_t3411975905::get_offset_of__callCtx_2(),
	ReturnMessage_t3411975905::get_offset_of__returnValue_3(),
	ReturnMessage_t3411975905::get_offset_of__uri_4(),
	ReturnMessage_t3411975905::get_offset_of__exception_5(),
	ReturnMessage_t3411975905::get_offset_of__methodBase_6(),
	ReturnMessage_t3411975905::get_offset_of__methodName_7(),
	ReturnMessage_t3411975905::get_offset_of__methodSignature_8(),
	ReturnMessage_t3411975905::get_offset_of__typeName_9(),
	ReturnMessage_t3411975905::get_offset_of__properties_10(),
	ReturnMessage_t3411975905::get_offset_of__inArgInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (ServerContextTerminatorSink_t1054294306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (ServerObjectTerminatorSink_t4261369100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1247[1] = 
{
	ServerObjectTerminatorSink_t4261369100::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (StackBuilderSink_t1613771438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1248[2] = 
{
	StackBuilderSink_t1613771438::get_offset_of__target_0(),
	StackBuilderSink_t1613771438::get_offset_of__rp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (SoapAttribute_t1982224933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1249[3] = 
{
	SoapAttribute_t1982224933::get_offset_of__useAttribute_0(),
	SoapAttribute_t1982224933::get_offset_of_ProtXmlNamespace_1(),
	SoapAttribute_t1982224933::get_offset_of_ReflectInfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (SoapFieldAttribute_t3073759685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1250[2] = 
{
	SoapFieldAttribute_t3073759685::get_offset_of__elementName_3(),
	SoapFieldAttribute_t3073759685::get_offset_of__isElement_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (SoapMethodAttribute_t2381910676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1251[6] = 
{
	SoapMethodAttribute_t2381910676::get_offset_of__responseElement_3(),
	SoapMethodAttribute_t2381910676::get_offset_of__responseNamespace_4(),
	SoapMethodAttribute_t2381910676::get_offset_of__returnElement_5(),
	SoapMethodAttribute_t2381910676::get_offset_of__soapAction_6(),
	SoapMethodAttribute_t2381910676::get_offset_of__useAttribute_7(),
	SoapMethodAttribute_t2381910676::get_offset_of__namespace_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (SoapParameterAttribute_t2780084514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (SoapTypeAttribute_t3444503085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1253[7] = 
{
	SoapTypeAttribute_t3444503085::get_offset_of__useAttribute_3(),
	SoapTypeAttribute_t3444503085::get_offset_of__xmlElementName_4(),
	SoapTypeAttribute_t3444503085::get_offset_of__xmlNamespace_5(),
	SoapTypeAttribute_t3444503085::get_offset_of__xmlTypeName_6(),
	SoapTypeAttribute_t3444503085::get_offset_of__xmlTypeNamespace_7(),
	SoapTypeAttribute_t3444503085::get_offset_of__isType_8(),
	SoapTypeAttribute_t3444503085::get_offset_of__isElement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (ProxyAttribute_t4031752430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (TransparentProxy_t3836393972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1255[3] = 
{
	TransparentProxy_t3836393972::get_offset_of__rp_0(),
	TransparentProxy_t3836393972::get_offset_of__class_1(),
	TransparentProxy_t3836393972::get_offset_of__custom_type_info_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (RealProxy_t298428346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1256[8] = 
{
	RealProxy_t298428346::get_offset_of_class_to_proxy_0(),
	RealProxy_t298428346::get_offset_of__targetContext_1(),
	RealProxy_t298428346::get_offset_of__server_2(),
	RealProxy_t298428346::get_offset_of__targetDomainId_3(),
	RealProxy_t298428346::get_offset_of__targetUri_4(),
	RealProxy_t298428346::get_offset_of__objectIdentity_5(),
	RealProxy_t298428346::get_offset_of__objTP_6(),
	RealProxy_t298428346::get_offset_of__stubData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (RemotingProxy_t2419155897), -1, sizeof(RemotingProxy_t2419155897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1257[5] = 
{
	RemotingProxy_t2419155897_StaticFields::get_offset_of__cache_GetTypeMethod_8(),
	RemotingProxy_t2419155897_StaticFields::get_offset_of__cache_GetHashCodeMethod_9(),
	RemotingProxy_t2419155897::get_offset_of__sink_10(),
	RemotingProxy_t2419155897::get_offset_of__hasEnvoySink_11(),
	RemotingProxy_t2419155897::get_offset_of__ctorCall_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (TrackingServices_t3722365321), -1, sizeof(TrackingServices_t3722365321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1259[1] = 
{
	TrackingServices_t3722365321_StaticFields::get_offset_of__handlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (ActivatedClientTypeEntry_t4060499430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1260[2] = 
{
	ActivatedClientTypeEntry_t4060499430::get_offset_of_applicationUrl_2(),
	ActivatedClientTypeEntry_t4060499430::get_offset_of_obj_type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (ActivatedServiceTypeEntry_t3934090848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1261[1] = 
{
	ActivatedServiceTypeEntry_t3934090848::get_offset_of_obj_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (EnvoyInfo_t815109115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1262[1] = 
{
	EnvoyInfo_t815109115::get_offset_of_envoySinks_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (Identity_t3647548000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1266[7] = 
{
	Identity_t3647548000::get_offset_of__objectUri_0(),
	Identity_t3647548000::get_offset_of__channelSink_1(),
	Identity_t3647548000::get_offset_of__envoySink_2(),
	Identity_t3647548000::get_offset_of__clientDynamicProperties_3(),
	Identity_t3647548000::get_offset_of__serverDynamicProperties_4(),
	Identity_t3647548000::get_offset_of__objRef_5(),
	Identity_t3647548000::get_offset_of__disposed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (ClientIdentity_t2254682501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1267[1] = 
{
	ClientIdentity_t2254682501::get_offset_of__proxyReference_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (InternalRemotingServices_t3953136710), -1, sizeof(InternalRemotingServices_t3953136710_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1268[1] = 
{
	InternalRemotingServices_t3953136710_StaticFields::get_offset_of__soapAttributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (ObjRef_t318414488), -1, sizeof(ObjRef_t318414488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1269[8] = 
{
	ObjRef_t318414488::get_offset_of_channel_info_0(),
	ObjRef_t318414488::get_offset_of_uri_1(),
	ObjRef_t318414488::get_offset_of_typeInfo_2(),
	ObjRef_t318414488::get_offset_of_envoyInfo_3(),
	ObjRef_t318414488::get_offset_of_flags_4(),
	ObjRef_t318414488::get_offset_of__serverType_5(),
	ObjRef_t318414488_StaticFields::get_offset_of_MarshalledObjectRef_6(),
	ObjRef_t318414488_StaticFields::get_offset_of_WellKnowObjectRef_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (RemotingConfiguration_t438177651), -1, sizeof(RemotingConfiguration_t438177651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1270[13] = 
{
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_applicationID_0(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_applicationName_1(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_processGuid_2(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_defaultConfigRead_3(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_defaultDelayedConfigRead_4(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of__errorMode_5(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_wellKnownClientEntries_6(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_activatedClientEntries_7(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_wellKnownServiceEntries_8(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_activatedServiceEntries_9(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_channelTemplates_10(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_clientProviderTemplates_11(),
	RemotingConfiguration_t438177651_StaticFields::get_offset_of_serverProviderTemplates_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (ConfigHandler_t2180714860), -1, sizeof(ConfigHandler_t2180714860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1271[9] = 
{
	ConfigHandler_t2180714860::get_offset_of_typeEntries_0(),
	ConfigHandler_t2180714860::get_offset_of_channelInstances_1(),
	ConfigHandler_t2180714860::get_offset_of_currentChannel_2(),
	ConfigHandler_t2180714860::get_offset_of_currentProviderData_3(),
	ConfigHandler_t2180714860::get_offset_of_currentClientUrl_4(),
	ConfigHandler_t2180714860::get_offset_of_appName_5(),
	ConfigHandler_t2180714860::get_offset_of_currentXmlPath_6(),
	ConfigHandler_t2180714860::get_offset_of_onlyDelayedChannels_7(),
	ConfigHandler_t2180714860_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (ChannelData_t1489610737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[7] = 
{
	ChannelData_t1489610737::get_offset_of_Ref_0(),
	ChannelData_t1489610737::get_offset_of_Type_1(),
	ChannelData_t1489610737::get_offset_of_Id_2(),
	ChannelData_t1489610737::get_offset_of_DelayLoadAsClientChannel_3(),
	ChannelData_t1489610737::get_offset_of__serverProviders_4(),
	ChannelData_t1489610737::get_offset_of__clientProviders_5(),
	ChannelData_t1489610737::get_offset_of__customProperties_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (ProviderData_t2518653487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1273[5] = 
{
	ProviderData_t2518653487::get_offset_of_Ref_0(),
	ProviderData_t2518653487::get_offset_of_Type_1(),
	ProviderData_t2518653487::get_offset_of_Id_2(),
	ProviderData_t2518653487::get_offset_of_CustomProperties_3(),
	ProviderData_t2518653487::get_offset_of_CustomData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (FormatterData_t12176916), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (RemotingException_t109604560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (RemotingServices_t2399536837), -1, sizeof(RemotingServices_t2399536837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1276[8] = 
{
	RemotingServices_t2399536837_StaticFields::get_offset_of_uri_hash_0(),
	RemotingServices_t2399536837_StaticFields::get_offset_of__serializationFormatter_1(),
	RemotingServices_t2399536837_StaticFields::get_offset_of__deserializationFormatter_2(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_app_id_3(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_app_id_lock_4(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_next_id_5(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_FieldSetterMethod_6(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_FieldGetterMethod_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (ServerIdentity_t1656058977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1277[5] = 
{
	ServerIdentity_t1656058977::get_offset_of__objectType_7(),
	ServerIdentity_t1656058977::get_offset_of__serverObject_8(),
	ServerIdentity_t1656058977::get_offset_of__serverSink_9(),
	ServerIdentity_t1656058977::get_offset_of__context_10(),
	ServerIdentity_t1656058977::get_offset_of__lease_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (ClientActivatedIdentity_t1467784146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (SingletonIdentity_t164722255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (SingleCallIdentity_t3377680076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (SoapServices_t3397513225), -1, sizeof(SoapServices_t3397513225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1281[5] = 
{
	SoapServices_t3397513225_StaticFields::get_offset_of__xmlTypes_0(),
	SoapServices_t3397513225_StaticFields::get_offset_of__xmlElements_1(),
	SoapServices_t3397513225_StaticFields::get_offset_of__soapActions_2(),
	SoapServices_t3397513225_StaticFields::get_offset_of__soapActionsMethods_3(),
	SoapServices_t3397513225_StaticFields::get_offset_of__typeInfos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (TypeInfo_t59877052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[2] = 
{
	TypeInfo_t59877052::get_offset_of_Attributes_0(),
	TypeInfo_t59877052::get_offset_of_Elements_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (TypeEntry_t3321373506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1283[2] = 
{
	TypeEntry_t3321373506::get_offset_of_assembly_name_0(),
	TypeEntry_t3321373506::get_offset_of_type_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (TypeInfo_t942537562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[3] = 
{
	TypeInfo_t942537562::get_offset_of_serverType_0(),
	TypeInfo_t942537562::get_offset_of_serverHierarchy_1(),
	TypeInfo_t942537562::get_offset_of_interfacesImplemented_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (WellKnownClientTypeEntry_t3314744170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1285[3] = 
{
	WellKnownClientTypeEntry_t3314744170::get_offset_of_obj_type_2(),
	WellKnownClientTypeEntry_t3314744170::get_offset_of_obj_url_3(),
	WellKnownClientTypeEntry_t3314744170::get_offset_of_app_url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (WellKnownObjectMode_t2630225581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1286[3] = 
{
	WellKnownObjectMode_t2630225581::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (WellKnownServiceTypeEntry_t1712728956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[3] = 
{
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_type_2(),
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_uri_3(),
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_mode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (CompatibilitySwitch_t1185775942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (RegistryRights_t427223974)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1289[15] = 
{
	RegistryRights_t427223974::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (X509Certificate_t283079845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[4] = 
{
	X509Certificate_t283079845::get_offset_of_impl_0(),
	X509Certificate_t283079845::get_offset_of_hideDates_1(),
	X509Certificate_t283079845::get_offset_of_issuer_name_2(),
	X509Certificate_t283079845::get_offset_of_subject_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (X509CertificateImpl_t3842064707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1292[1] = 
{
	X509CertificateImpl_t3842064707::get_offset_of_cachedCertificateHash_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (X509CertificateImplMono_t2148381192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[1] = 
{
	X509CertificateImplMono_t2148381192::get_offset_of_x509_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (X509Helper_t1751628948), -1, sizeof(X509Helper_t1751628948_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1294[1] = 
{
	X509Helper_t1751628948_StaticFields::get_offset_of_nativeHelper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (X509KeyStorageFlags_t1216946873)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1295[7] = 
{
	X509KeyStorageFlags_t1216946873::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (CryptoConfig_t896479599), -1, sizeof(CryptoConfig_t896479599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1296[29] = 
{
	CryptoConfig_t896479599_StaticFields::get_offset_of_lockObject_0(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_algorithms_1(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_unresolved_algorithms_2(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_oids_3(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultSHA1_4(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultMD5_5(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultSHA256_6(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultSHA384_7(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultSHA512_8(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRSA_9(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultDSA_10(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultDES_11(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_default3DES_12(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRC2_13(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultAES_14(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRNG_15(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMAC_16(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultMAC3DES_17(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultDSASigDesc_18(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRSAPKCS1SHA1SigDesc_19(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRSAPKCS1SHA256SigDesc_20(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRSAPKCS1SHA384SigDesc_21(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRSAPKCS1SHA512SigDesc_22(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultRIPEMD160_23(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMACMD5_24(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMACRIPEMD160_25(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMACSHA256_26(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMACSHA384_27(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_defaultHMACSHA512_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (CryptoHandler_t2096418190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1297[5] = 
{
	CryptoHandler_t2096418190::get_offset_of_algorithms_0(),
	CryptoHandler_t2096418190::get_offset_of_oid_1(),
	CryptoHandler_t2096418190::get_offset_of_names_2(),
	CryptoHandler_t2096418190::get_offset_of_classnames_3(),
	CryptoHandler_t2096418190::get_offset_of_level_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (DESTransform_t179750796), -1, sizeof(DESTransform_t179750796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1298[13] = 
{
	DESTransform_t179750796_StaticFields::get_offset_of_KEY_BIT_SIZE_12(),
	DESTransform_t179750796_StaticFields::get_offset_of_KEY_BYTE_SIZE_13(),
	DESTransform_t179750796_StaticFields::get_offset_of_BLOCK_BIT_SIZE_14(),
	DESTransform_t179750796_StaticFields::get_offset_of_BLOCK_BYTE_SIZE_15(),
	DESTransform_t179750796::get_offset_of_keySchedule_16(),
	DESTransform_t179750796::get_offset_of_byteBuff_17(),
	DESTransform_t179750796::get_offset_of_dwordBuff_18(),
	DESTransform_t179750796_StaticFields::get_offset_of_spBoxes_19(),
	DESTransform_t179750796_StaticFields::get_offset_of_PC1_20(),
	DESTransform_t179750796_StaticFields::get_offset_of_leftRotTotal_21(),
	DESTransform_t179750796_StaticFields::get_offset_of_PC2_22(),
	DESTransform_t179750796_StaticFields::get_offset_of_ipTab_23(),
	DESTransform_t179750796_StaticFields::get_offset_of_fpTab_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (DSACryptoServiceProvider_t2915171657), -1, sizeof(DSACryptoServiceProvider_t2915171657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1299[7] = 
{
	DSACryptoServiceProvider_t2915171657::get_offset_of_store_2(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_persistKey_3(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_persisted_4(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_privateKeyExportable_5(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_m_disposed_6(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_dsa_7(),
	DSACryptoServiceProvider_t2915171657_StaticFields::get_offset_of_useMachineKeyStore_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
