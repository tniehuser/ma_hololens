﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.BinaryWriter
struct BinaryWriter_t3179371318;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Text.Encoder
struct Encoder_t751367874;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.BinaryWriter
struct  BinaryWriter_t3179371318  : public Il2CppObject
{
public:
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_t3255436806 * ___OutStream_1;
	// System.Byte[] System.IO.BinaryWriter::_buffer
	ByteU5BU5D_t3397334013* ____buffer_2;
	// System.Text.Encoding System.IO.BinaryWriter::_encoding
	Encoding_t663144255 * ____encoding_3;
	// System.Text.Encoder System.IO.BinaryWriter::_encoder
	Encoder_t751367874 * ____encoder_4;
	// System.Boolean System.IO.BinaryWriter::_leaveOpen
	bool ____leaveOpen_5;
	// System.Byte[] System.IO.BinaryWriter::_largeByteBuffer
	ByteU5BU5D_t3397334013* ____largeByteBuffer_6;
	// System.Int32 System.IO.BinaryWriter::_maxChars
	int32_t ____maxChars_7;

public:
	inline static int32_t get_offset_of_OutStream_1() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ___OutStream_1)); }
	inline Stream_t3255436806 * get_OutStream_1() const { return ___OutStream_1; }
	inline Stream_t3255436806 ** get_address_of_OutStream_1() { return &___OutStream_1; }
	inline void set_OutStream_1(Stream_t3255436806 * value)
	{
		___OutStream_1 = value;
		Il2CppCodeGenWriteBarrier(&___OutStream_1, value);
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____buffer_2)); }
	inline ByteU5BU5D_t3397334013* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t3397334013* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_2, value);
	}

	inline static int32_t get_offset_of__encoding_3() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____encoding_3)); }
	inline Encoding_t663144255 * get__encoding_3() const { return ____encoding_3; }
	inline Encoding_t663144255 ** get_address_of__encoding_3() { return &____encoding_3; }
	inline void set__encoding_3(Encoding_t663144255 * value)
	{
		____encoding_3 = value;
		Il2CppCodeGenWriteBarrier(&____encoding_3, value);
	}

	inline static int32_t get_offset_of__encoder_4() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____encoder_4)); }
	inline Encoder_t751367874 * get__encoder_4() const { return ____encoder_4; }
	inline Encoder_t751367874 ** get_address_of__encoder_4() { return &____encoder_4; }
	inline void set__encoder_4(Encoder_t751367874 * value)
	{
		____encoder_4 = value;
		Il2CppCodeGenWriteBarrier(&____encoder_4, value);
	}

	inline static int32_t get_offset_of__leaveOpen_5() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____leaveOpen_5)); }
	inline bool get__leaveOpen_5() const { return ____leaveOpen_5; }
	inline bool* get_address_of__leaveOpen_5() { return &____leaveOpen_5; }
	inline void set__leaveOpen_5(bool value)
	{
		____leaveOpen_5 = value;
	}

	inline static int32_t get_offset_of__largeByteBuffer_6() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____largeByteBuffer_6)); }
	inline ByteU5BU5D_t3397334013* get__largeByteBuffer_6() const { return ____largeByteBuffer_6; }
	inline ByteU5BU5D_t3397334013** get_address_of__largeByteBuffer_6() { return &____largeByteBuffer_6; }
	inline void set__largeByteBuffer_6(ByteU5BU5D_t3397334013* value)
	{
		____largeByteBuffer_6 = value;
		Il2CppCodeGenWriteBarrier(&____largeByteBuffer_6, value);
	}

	inline static int32_t get_offset_of__maxChars_7() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318, ____maxChars_7)); }
	inline int32_t get__maxChars_7() const { return ____maxChars_7; }
	inline int32_t* get_address_of__maxChars_7() { return &____maxChars_7; }
	inline void set__maxChars_7(int32_t value)
	{
		____maxChars_7 = value;
	}
};

struct BinaryWriter_t3179371318_StaticFields
{
public:
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_t3179371318 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(BinaryWriter_t3179371318_StaticFields, ___Null_0)); }
	inline BinaryWriter_t3179371318 * get_Null_0() const { return ___Null_0; }
	inline BinaryWriter_t3179371318 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(BinaryWriter_t3179371318 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier(&___Null_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
