﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t3950677696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket/<BeginMConnect>c__AnonStorey0
struct  U3CBeginMConnectU3Ec__AnonStorey0_t586789679  : public Il2CppObject
{
public:
	// System.AsyncCallback System.Net.Sockets.Socket/<BeginMConnect>c__AnonStorey0::callback
	AsyncCallback_t163412349 * ___callback_0;
	// System.Net.Sockets.SocketAsyncResult System.Net.Sockets.Socket/<BeginMConnect>c__AnonStorey0::ares
	SocketAsyncResult_t3950677696 * ___ares_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CBeginMConnectU3Ec__AnonStorey0_t586789679, ___callback_0)); }
	inline AsyncCallback_t163412349 * get_callback_0() const { return ___callback_0; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AsyncCallback_t163412349 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_ares_1() { return static_cast<int32_t>(offsetof(U3CBeginMConnectU3Ec__AnonStorey0_t586789679, ___ares_1)); }
	inline SocketAsyncResult_t3950677696 * get_ares_1() const { return ___ares_1; }
	inline SocketAsyncResult_t3950677696 ** get_address_of_ares_1() { return &___ares_1; }
	inline void set_ares_1(SocketAsyncResult_t3950677696 * value)
	{
		___ares_1 = value;
		Il2CppCodeGenWriteBarrier(&___ares_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
