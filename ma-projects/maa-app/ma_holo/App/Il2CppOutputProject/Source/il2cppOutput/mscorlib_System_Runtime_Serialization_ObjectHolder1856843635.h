﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.Serialization.ObjectHolder[]
struct ObjectHolderU5BU5D_t2337234454;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectHolderList
struct  ObjectHolderList_t1856843635  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.ObjectHolder[] System.Runtime.Serialization.ObjectHolderList::m_values
	ObjectHolderU5BU5D_t2337234454* ___m_values_0;
	// System.Int32 System.Runtime.Serialization.ObjectHolderList::m_count
	int32_t ___m_count_1;

public:
	inline static int32_t get_offset_of_m_values_0() { return static_cast<int32_t>(offsetof(ObjectHolderList_t1856843635, ___m_values_0)); }
	inline ObjectHolderU5BU5D_t2337234454* get_m_values_0() const { return ___m_values_0; }
	inline ObjectHolderU5BU5D_t2337234454** get_address_of_m_values_0() { return &___m_values_0; }
	inline void set_m_values_0(ObjectHolderU5BU5D_t2337234454* value)
	{
		___m_values_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_values_0, value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(ObjectHolderList_t1856843635, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
