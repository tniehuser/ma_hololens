﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathNavigator3981235968.h"

// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t339325318;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathDocumentNavigator
struct  XPathDocumentNavigator_t4190236647  : public XPathNavigator_t3981235968
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageCurrent
	XPathNodeU5BU5D_t339325318* ___pageCurrent_4;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathDocumentNavigator::pageParent
	XPathNodeU5BU5D_t339325318* ___pageParent_5;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxCurrent
	int32_t ___idxCurrent_6;
	// System.Int32 MS.Internal.Xml.Cache.XPathDocumentNavigator::idxParent
	int32_t ___idxParent_7;

public:
	inline static int32_t get_offset_of_pageCurrent_4() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4190236647, ___pageCurrent_4)); }
	inline XPathNodeU5BU5D_t339325318* get_pageCurrent_4() const { return ___pageCurrent_4; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageCurrent_4() { return &___pageCurrent_4; }
	inline void set_pageCurrent_4(XPathNodeU5BU5D_t339325318* value)
	{
		___pageCurrent_4 = value;
		Il2CppCodeGenWriteBarrier(&___pageCurrent_4, value);
	}

	inline static int32_t get_offset_of_pageParent_5() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4190236647, ___pageParent_5)); }
	inline XPathNodeU5BU5D_t339325318* get_pageParent_5() const { return ___pageParent_5; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageParent_5() { return &___pageParent_5; }
	inline void set_pageParent_5(XPathNodeU5BU5D_t339325318* value)
	{
		___pageParent_5 = value;
		Il2CppCodeGenWriteBarrier(&___pageParent_5, value);
	}

	inline static int32_t get_offset_of_idxCurrent_6() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4190236647, ___idxCurrent_6)); }
	inline int32_t get_idxCurrent_6() const { return ___idxCurrent_6; }
	inline int32_t* get_address_of_idxCurrent_6() { return &___idxCurrent_6; }
	inline void set_idxCurrent_6(int32_t value)
	{
		___idxCurrent_6 = value;
	}

	inline static int32_t get_offset_of_idxParent_7() { return static_cast<int32_t>(offsetof(XPathDocumentNavigator_t4190236647, ___idxParent_7)); }
	inline int32_t get_idxParent_7() const { return ___idxParent_7; }
	inline int32_t* get_address_of_idxParent_7() { return &___idxParent_7; }
	inline void set_idxParent_7(int32_t value)
	{
		___idxParent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
