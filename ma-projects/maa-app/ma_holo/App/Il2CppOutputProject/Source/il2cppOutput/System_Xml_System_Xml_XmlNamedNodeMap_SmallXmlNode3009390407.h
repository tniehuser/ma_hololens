﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator
struct  SingleObjectEnumerator_t3009390407  : public Il2CppObject
{
public:
	// System.Object System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator::loneValue
	Il2CppObject * ___loneValue_0;
	// System.Int32 System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_loneValue_0() { return static_cast<int32_t>(offsetof(SingleObjectEnumerator_t3009390407, ___loneValue_0)); }
	inline Il2CppObject * get_loneValue_0() const { return ___loneValue_0; }
	inline Il2CppObject ** get_address_of_loneValue_0() { return &___loneValue_0; }
	inline void set_loneValue_0(Il2CppObject * value)
	{
		___loneValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___loneValue_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(SingleObjectEnumerator_t3009390407, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
