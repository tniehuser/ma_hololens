﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_EndPoint4156119363.h"

// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t2615413766  : public EndPoint_t4156119363
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::m_Address
	IPAddress_t1399971723 * ___m_Address_0;
	// System.Int32 System.Net.IPEndPoint::m_Port
	int32_t ___m_Port_1;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___m_Address_0)); }
	inline IPAddress_t1399971723 * get_m_Address_0() const { return ___m_Address_0; }
	inline IPAddress_t1399971723 ** get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(IPAddress_t1399971723 * value)
	{
		___m_Address_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Address_0, value);
	}

	inline static int32_t get_offset_of_m_Port_1() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___m_Port_1)); }
	inline int32_t get_m_Port_1() const { return ___m_Port_1; }
	inline int32_t* get_address_of_m_Port_1() { return &___m_Port_1; }
	inline void set_m_Port_1(int32_t value)
	{
		___m_Port_1 = value;
	}
};

struct IPEndPoint_t2615413766_StaticFields
{
public:
	// System.Net.IPEndPoint System.Net.IPEndPoint::Any
	IPEndPoint_t2615413766 * ___Any_2;
	// System.Net.IPEndPoint System.Net.IPEndPoint::IPv6Any
	IPEndPoint_t2615413766 * ___IPv6Any_3;

public:
	inline static int32_t get_offset_of_Any_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766_StaticFields, ___Any_2)); }
	inline IPEndPoint_t2615413766 * get_Any_2() const { return ___Any_2; }
	inline IPEndPoint_t2615413766 ** get_address_of_Any_2() { return &___Any_2; }
	inline void set_Any_2(IPEndPoint_t2615413766 * value)
	{
		___Any_2 = value;
		Il2CppCodeGenWriteBarrier(&___Any_2, value);
	}

	inline static int32_t get_offset_of_IPv6Any_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766_StaticFields, ___IPv6Any_3)); }
	inline IPEndPoint_t2615413766 * get_IPv6Any_3() const { return ___IPv6Any_3; }
	inline IPEndPoint_t2615413766 ** get_address_of_IPv6Any_3() { return &___IPv6Any_3; }
	inline void set_IPv6Any_3(IPEndPoint_t2615413766 * value)
	{
		___IPv6Any_3 = value;
		Il2CppCodeGenWriteBarrier(&___IPv6Any_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
