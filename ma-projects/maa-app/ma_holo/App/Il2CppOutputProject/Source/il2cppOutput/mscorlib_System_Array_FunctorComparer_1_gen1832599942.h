﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Comparison`1<System.Xml.Schema.XmlAtomicValue>
struct Comparison_1_t2014608222;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array/FunctorComparer`1<System.Xml.Schema.XmlAtomicValue>
struct  FunctorComparer_1_t1832599942  : public Il2CppObject
{
public:
	// System.Comparison`1<T> System.Array/FunctorComparer`1::comparison
	Comparison_1_t2014608222 * ___comparison_0;

public:
	inline static int32_t get_offset_of_comparison_0() { return static_cast<int32_t>(offsetof(FunctorComparer_1_t1832599942, ___comparison_0)); }
	inline Comparison_1_t2014608222 * get_comparison_0() const { return ___comparison_0; }
	inline Comparison_1_t2014608222 ** get_address_of_comparison_0() { return &___comparison_0; }
	inline void set_comparison_0(Comparison_1_t2014608222 * value)
	{
		___comparison_0 = value;
		Il2CppCodeGenWriteBarrier(&___comparison_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
