﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SyntaxTreeNode2397191729.h"

// System.Xml.Schema.SyntaxTreeNode
struct SyntaxTreeNode_t2397191729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.InteriorNode
struct  InteriorNode_t2716368958  : public SyntaxTreeNode_t2397191729
{
public:
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::leftChild
	SyntaxTreeNode_t2397191729 * ___leftChild_0;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::rightChild
	SyntaxTreeNode_t2397191729 * ___rightChild_1;

public:
	inline static int32_t get_offset_of_leftChild_0() { return static_cast<int32_t>(offsetof(InteriorNode_t2716368958, ___leftChild_0)); }
	inline SyntaxTreeNode_t2397191729 * get_leftChild_0() const { return ___leftChild_0; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_leftChild_0() { return &___leftChild_0; }
	inline void set_leftChild_0(SyntaxTreeNode_t2397191729 * value)
	{
		___leftChild_0 = value;
		Il2CppCodeGenWriteBarrier(&___leftChild_0, value);
	}

	inline static int32_t get_offset_of_rightChild_1() { return static_cast<int32_t>(offsetof(InteriorNode_t2716368958, ___rightChild_1)); }
	inline SyntaxTreeNode_t2397191729 * get_rightChild_1() const { return ___rightChild_1; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_rightChild_1() { return &___rightChild_1; }
	inline void set_rightChild_1(SyntaxTreeNode_t2397191729 * value)
	{
		___rightChild_1 = value;
		Il2CppCodeGenWriteBarrier(&___rightChild_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
