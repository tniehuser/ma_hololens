﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.UInt16[]
struct UInt16U5BU5D_t2527266722;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6AddressFormatter
struct  IPv6AddressFormatter_t3116172695 
{
public:
	// System.UInt16[] System.Net.IPv6AddressFormatter::address
	UInt16U5BU5D_t2527266722* ___address_0;
	// System.Int64 System.Net.IPv6AddressFormatter::scopeId
	int64_t ___scopeId_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6AddressFormatter_t3116172695, ___address_0)); }
	inline UInt16U5BU5D_t2527266722* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_t2527266722** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_t2527266722* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier(&___address_0, value);
	}

	inline static int32_t get_offset_of_scopeId_1() { return static_cast<int32_t>(offsetof(IPv6AddressFormatter_t3116172695, ___scopeId_1)); }
	inline int64_t get_scopeId_1() const { return ___scopeId_1; }
	inline int64_t* get_address_of_scopeId_1() { return &___scopeId_1; }
	inline void set_scopeId_1(int64_t value)
	{
		___scopeId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.IPv6AddressFormatter
struct IPv6AddressFormatter_t3116172695_marshaled_pinvoke
{
	uint16_t* ___address_0;
	int64_t ___scopeId_1;
};
// Native definition for COM marshalling of System.Net.IPv6AddressFormatter
struct IPv6AddressFormatter_t3116172695_marshaled_com
{
	uint16_t* ___address_0;
	int64_t ___scopeId_1;
};
