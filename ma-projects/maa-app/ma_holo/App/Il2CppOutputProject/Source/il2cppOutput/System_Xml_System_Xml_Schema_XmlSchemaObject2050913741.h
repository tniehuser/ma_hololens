﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t3063656491;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_t2050913741  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNum
	int32_t ___lineNum_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePos
	int32_t ___linePos_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t3063656491 * ___namespaces_3;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_t2050913741 * ___parent_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isProcessing
	bool ___isProcessing_5;

public:
	inline static int32_t get_offset_of_lineNum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___lineNum_0)); }
	inline int32_t get_lineNum_0() const { return ___lineNum_0; }
	inline int32_t* get_address_of_lineNum_0() { return &___lineNum_0; }
	inline void set_lineNum_0(int32_t value)
	{
		___lineNum_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier(&___sourceUri_2, value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___namespaces_3)); }
	inline XmlSerializerNamespaces_t3063656491 * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_t3063656491 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_t3063656491 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_3, value);
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___parent_4)); }
	inline XmlSchemaObject_t2050913741 * get_parent_4() const { return ___parent_4; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(XmlSchemaObject_t2050913741 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___parent_4, value);
	}

	inline static int32_t get_offset_of_isProcessing_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isProcessing_5)); }
	inline bool get_isProcessing_5() const { return ___isProcessing_5; }
	inline bool* get_address_of_isProcessing_5() { return &___isProcessing_5; }
	inline void set_isProcessing_5(bool value)
	{
		___isProcessing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
