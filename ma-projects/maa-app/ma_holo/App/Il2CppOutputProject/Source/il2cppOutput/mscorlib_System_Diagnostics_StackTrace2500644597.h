﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t4208599292;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t2217612696;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<System.Diagnostics.StackTrace,System.String>>
struct Dictionary_2_t2683032943;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackTrace
struct  StackTrace_t2500644597  : public Il2CppObject
{
public:
	// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::frames
	StackFrameU5BU5D_t4208599292* ___frames_1;
	// System.Diagnostics.StackTrace[] System.Diagnostics.StackTrace::captured_traces
	StackTraceU5BU5D_t2217612696* ___captured_traces_2;
	// System.Boolean System.Diagnostics.StackTrace::debug_info
	bool ___debug_info_3;

public:
	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597, ___frames_1)); }
	inline StackFrameU5BU5D_t4208599292* get_frames_1() const { return ___frames_1; }
	inline StackFrameU5BU5D_t4208599292** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(StackFrameU5BU5D_t4208599292* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier(&___frames_1, value);
	}

	inline static int32_t get_offset_of_captured_traces_2() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597, ___captured_traces_2)); }
	inline StackTraceU5BU5D_t2217612696* get_captured_traces_2() const { return ___captured_traces_2; }
	inline StackTraceU5BU5D_t2217612696** get_address_of_captured_traces_2() { return &___captured_traces_2; }
	inline void set_captured_traces_2(StackTraceU5BU5D_t2217612696* value)
	{
		___captured_traces_2 = value;
		Il2CppCodeGenWriteBarrier(&___captured_traces_2, value);
	}

	inline static int32_t get_offset_of_debug_info_3() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597, ___debug_info_3)); }
	inline bool get_debug_info_3() const { return ___debug_info_3; }
	inline bool* get_address_of_debug_info_3() { return &___debug_info_3; }
	inline void set_debug_info_3(bool value)
	{
		___debug_info_3 = value;
	}
};

struct StackTrace_t2500644597_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<System.Diagnostics.StackTrace,System.String>> System.Diagnostics.StackTrace::metadataHandlers
	Dictionary_2_t2683032943 * ___metadataHandlers_4;
	// System.Boolean System.Diagnostics.StackTrace::isAotidSet
	bool ___isAotidSet_5;
	// System.String System.Diagnostics.StackTrace::aotid
	String_t* ___aotid_6;

public:
	inline static int32_t get_offset_of_metadataHandlers_4() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597_StaticFields, ___metadataHandlers_4)); }
	inline Dictionary_2_t2683032943 * get_metadataHandlers_4() const { return ___metadataHandlers_4; }
	inline Dictionary_2_t2683032943 ** get_address_of_metadataHandlers_4() { return &___metadataHandlers_4; }
	inline void set_metadataHandlers_4(Dictionary_2_t2683032943 * value)
	{
		___metadataHandlers_4 = value;
		Il2CppCodeGenWriteBarrier(&___metadataHandlers_4, value);
	}

	inline static int32_t get_offset_of_isAotidSet_5() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597_StaticFields, ___isAotidSet_5)); }
	inline bool get_isAotidSet_5() const { return ___isAotidSet_5; }
	inline bool* get_address_of_isAotidSet_5() { return &___isAotidSet_5; }
	inline void set_isAotidSet_5(bool value)
	{
		___isAotidSet_5 = value;
	}

	inline static int32_t get_offset_of_aotid_6() { return static_cast<int32_t>(offsetof(StackTrace_t2500644597_StaticFields, ___aotid_6)); }
	inline String_t* get_aotid_6() const { return ___aotid_6; }
	inline String_t** get_address_of_aotid_6() { return &___aotid_6; }
	inline void set_aotid_6(String_t* value)
	{
		___aotid_6 = value;
		Il2CppCodeGenWriteBarrier(&___aotid_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
