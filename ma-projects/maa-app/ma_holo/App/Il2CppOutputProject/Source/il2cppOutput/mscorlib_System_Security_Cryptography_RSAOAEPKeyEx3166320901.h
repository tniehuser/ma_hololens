﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_AsymmetricKe3339648384.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Security.Cryptography.RSA
struct RSA_t3719518354;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAOAEPKeyExchangeFormatter
struct  RSAOAEPKeyExchangeFormatter_t3166320901  : public AsymmetricKeyExchangeFormatter_t3339648384
{
public:
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::_rsaKey
	RSA_t3719518354 * ____rsaKey_0;
	// System.Nullable`1<System.Boolean> System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::_rsaOverridesEncrypt
	Nullable_1_t2088641033  ____rsaOverridesEncrypt_1;

public:
	inline static int32_t get_offset_of__rsaKey_0() { return static_cast<int32_t>(offsetof(RSAOAEPKeyExchangeFormatter_t3166320901, ____rsaKey_0)); }
	inline RSA_t3719518354 * get__rsaKey_0() const { return ____rsaKey_0; }
	inline RSA_t3719518354 ** get_address_of__rsaKey_0() { return &____rsaKey_0; }
	inline void set__rsaKey_0(RSA_t3719518354 * value)
	{
		____rsaKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____rsaKey_0, value);
	}

	inline static int32_t get_offset_of__rsaOverridesEncrypt_1() { return static_cast<int32_t>(offsetof(RSAOAEPKeyExchangeFormatter_t3166320901, ____rsaOverridesEncrypt_1)); }
	inline Nullable_1_t2088641033  get__rsaOverridesEncrypt_1() const { return ____rsaOverridesEncrypt_1; }
	inline Nullable_1_t2088641033 * get_address_of__rsaOverridesEncrypt_1() { return &____rsaOverridesEncrypt_1; }
	inline void set__rsaOverridesEncrypt_1(Nullable_1_t2088641033  value)
	{
		____rsaOverridesEncrypt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
