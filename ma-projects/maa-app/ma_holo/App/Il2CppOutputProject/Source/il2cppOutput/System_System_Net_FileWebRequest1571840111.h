﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_WebRequest1365124353.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"

// System.Threading.WaitCallback
struct WaitCallback_t2798937288;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;
// System.String
struct String_t;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Net.WebResponse
struct WebResponse_t1895226051;
// System.IO.Stream
struct Stream_t3255436806;
// System.Uri
struct Uri_t19570940;
// System.Net.LazyAsyncResult
struct LazyAsyncResult_t698912221;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest
struct  FileWebRequest_t1571840111  : public WebRequest_t1365124353
{
public:
	// System.String System.Net.FileWebRequest::m_connectionGroupName
	String_t* ___m_connectionGroupName_16;
	// System.Int64 System.Net.FileWebRequest::m_contentLength
	int64_t ___m_contentLength_17;
	// System.Net.ICredentials System.Net.FileWebRequest::m_credentials
	Il2CppObject * ___m_credentials_18;
	// System.IO.FileAccess System.Net.FileWebRequest::m_fileAccess
	int32_t ___m_fileAccess_19;
	// System.Net.WebHeaderCollection System.Net.FileWebRequest::m_headers
	WebHeaderCollection_t3028142837 * ___m_headers_20;
	// System.String System.Net.FileWebRequest::m_method
	String_t* ___m_method_21;
	// System.Net.IWebProxy System.Net.FileWebRequest::m_proxy
	Il2CppObject * ___m_proxy_22;
	// System.Threading.ManualResetEvent System.Net.FileWebRequest::m_readerEvent
	ManualResetEvent_t926074657 * ___m_readerEvent_23;
	// System.Boolean System.Net.FileWebRequest::m_readPending
	bool ___m_readPending_24;
	// System.Net.WebResponse System.Net.FileWebRequest::m_response
	WebResponse_t1895226051 * ___m_response_25;
	// System.IO.Stream System.Net.FileWebRequest::m_stream
	Stream_t3255436806 * ___m_stream_26;
	// System.Boolean System.Net.FileWebRequest::m_syncHint
	bool ___m_syncHint_27;
	// System.Int32 System.Net.FileWebRequest::m_timeout
	int32_t ___m_timeout_28;
	// System.Uri System.Net.FileWebRequest::m_uri
	Uri_t19570940 * ___m_uri_29;
	// System.Boolean System.Net.FileWebRequest::m_writePending
	bool ___m_writePending_30;
	// System.Boolean System.Net.FileWebRequest::m_writing
	bool ___m_writing_31;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_WriteAResult
	LazyAsyncResult_t698912221 * ___m_WriteAResult_32;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_ReadAResult
	LazyAsyncResult_t698912221 * ___m_ReadAResult_33;
	// System.Int32 System.Net.FileWebRequest::m_Aborted
	int32_t ___m_Aborted_34;

public:
	inline static int32_t get_offset_of_m_connectionGroupName_16() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_connectionGroupName_16)); }
	inline String_t* get_m_connectionGroupName_16() const { return ___m_connectionGroupName_16; }
	inline String_t** get_address_of_m_connectionGroupName_16() { return &___m_connectionGroupName_16; }
	inline void set_m_connectionGroupName_16(String_t* value)
	{
		___m_connectionGroupName_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_connectionGroupName_16, value);
	}

	inline static int32_t get_offset_of_m_contentLength_17() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_contentLength_17)); }
	inline int64_t get_m_contentLength_17() const { return ___m_contentLength_17; }
	inline int64_t* get_address_of_m_contentLength_17() { return &___m_contentLength_17; }
	inline void set_m_contentLength_17(int64_t value)
	{
		___m_contentLength_17 = value;
	}

	inline static int32_t get_offset_of_m_credentials_18() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_credentials_18)); }
	inline Il2CppObject * get_m_credentials_18() const { return ___m_credentials_18; }
	inline Il2CppObject ** get_address_of_m_credentials_18() { return &___m_credentials_18; }
	inline void set_m_credentials_18(Il2CppObject * value)
	{
		___m_credentials_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_credentials_18, value);
	}

	inline static int32_t get_offset_of_m_fileAccess_19() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_fileAccess_19)); }
	inline int32_t get_m_fileAccess_19() const { return ___m_fileAccess_19; }
	inline int32_t* get_address_of_m_fileAccess_19() { return &___m_fileAccess_19; }
	inline void set_m_fileAccess_19(int32_t value)
	{
		___m_fileAccess_19 = value;
	}

	inline static int32_t get_offset_of_m_headers_20() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_headers_20)); }
	inline WebHeaderCollection_t3028142837 * get_m_headers_20() const { return ___m_headers_20; }
	inline WebHeaderCollection_t3028142837 ** get_address_of_m_headers_20() { return &___m_headers_20; }
	inline void set_m_headers_20(WebHeaderCollection_t3028142837 * value)
	{
		___m_headers_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_headers_20, value);
	}

	inline static int32_t get_offset_of_m_method_21() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_method_21)); }
	inline String_t* get_m_method_21() const { return ___m_method_21; }
	inline String_t** get_address_of_m_method_21() { return &___m_method_21; }
	inline void set_m_method_21(String_t* value)
	{
		___m_method_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_method_21, value);
	}

	inline static int32_t get_offset_of_m_proxy_22() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_proxy_22)); }
	inline Il2CppObject * get_m_proxy_22() const { return ___m_proxy_22; }
	inline Il2CppObject ** get_address_of_m_proxy_22() { return &___m_proxy_22; }
	inline void set_m_proxy_22(Il2CppObject * value)
	{
		___m_proxy_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_proxy_22, value);
	}

	inline static int32_t get_offset_of_m_readerEvent_23() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_readerEvent_23)); }
	inline ManualResetEvent_t926074657 * get_m_readerEvent_23() const { return ___m_readerEvent_23; }
	inline ManualResetEvent_t926074657 ** get_address_of_m_readerEvent_23() { return &___m_readerEvent_23; }
	inline void set_m_readerEvent_23(ManualResetEvent_t926074657 * value)
	{
		___m_readerEvent_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_readerEvent_23, value);
	}

	inline static int32_t get_offset_of_m_readPending_24() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_readPending_24)); }
	inline bool get_m_readPending_24() const { return ___m_readPending_24; }
	inline bool* get_address_of_m_readPending_24() { return &___m_readPending_24; }
	inline void set_m_readPending_24(bool value)
	{
		___m_readPending_24 = value;
	}

	inline static int32_t get_offset_of_m_response_25() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_response_25)); }
	inline WebResponse_t1895226051 * get_m_response_25() const { return ___m_response_25; }
	inline WebResponse_t1895226051 ** get_address_of_m_response_25() { return &___m_response_25; }
	inline void set_m_response_25(WebResponse_t1895226051 * value)
	{
		___m_response_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_response_25, value);
	}

	inline static int32_t get_offset_of_m_stream_26() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_stream_26)); }
	inline Stream_t3255436806 * get_m_stream_26() const { return ___m_stream_26; }
	inline Stream_t3255436806 ** get_address_of_m_stream_26() { return &___m_stream_26; }
	inline void set_m_stream_26(Stream_t3255436806 * value)
	{
		___m_stream_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_stream_26, value);
	}

	inline static int32_t get_offset_of_m_syncHint_27() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_syncHint_27)); }
	inline bool get_m_syncHint_27() const { return ___m_syncHint_27; }
	inline bool* get_address_of_m_syncHint_27() { return &___m_syncHint_27; }
	inline void set_m_syncHint_27(bool value)
	{
		___m_syncHint_27 = value;
	}

	inline static int32_t get_offset_of_m_timeout_28() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_timeout_28)); }
	inline int32_t get_m_timeout_28() const { return ___m_timeout_28; }
	inline int32_t* get_address_of_m_timeout_28() { return &___m_timeout_28; }
	inline void set_m_timeout_28(int32_t value)
	{
		___m_timeout_28 = value;
	}

	inline static int32_t get_offset_of_m_uri_29() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_uri_29)); }
	inline Uri_t19570940 * get_m_uri_29() const { return ___m_uri_29; }
	inline Uri_t19570940 ** get_address_of_m_uri_29() { return &___m_uri_29; }
	inline void set_m_uri_29(Uri_t19570940 * value)
	{
		___m_uri_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_uri_29, value);
	}

	inline static int32_t get_offset_of_m_writePending_30() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_writePending_30)); }
	inline bool get_m_writePending_30() const { return ___m_writePending_30; }
	inline bool* get_address_of_m_writePending_30() { return &___m_writePending_30; }
	inline void set_m_writePending_30(bool value)
	{
		___m_writePending_30 = value;
	}

	inline static int32_t get_offset_of_m_writing_31() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_writing_31)); }
	inline bool get_m_writing_31() const { return ___m_writing_31; }
	inline bool* get_address_of_m_writing_31() { return &___m_writing_31; }
	inline void set_m_writing_31(bool value)
	{
		___m_writing_31 = value;
	}

	inline static int32_t get_offset_of_m_WriteAResult_32() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_WriteAResult_32)); }
	inline LazyAsyncResult_t698912221 * get_m_WriteAResult_32() const { return ___m_WriteAResult_32; }
	inline LazyAsyncResult_t698912221 ** get_address_of_m_WriteAResult_32() { return &___m_WriteAResult_32; }
	inline void set_m_WriteAResult_32(LazyAsyncResult_t698912221 * value)
	{
		___m_WriteAResult_32 = value;
		Il2CppCodeGenWriteBarrier(&___m_WriteAResult_32, value);
	}

	inline static int32_t get_offset_of_m_ReadAResult_33() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_ReadAResult_33)); }
	inline LazyAsyncResult_t698912221 * get_m_ReadAResult_33() const { return ___m_ReadAResult_33; }
	inline LazyAsyncResult_t698912221 ** get_address_of_m_ReadAResult_33() { return &___m_ReadAResult_33; }
	inline void set_m_ReadAResult_33(LazyAsyncResult_t698912221 * value)
	{
		___m_ReadAResult_33 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReadAResult_33, value);
	}

	inline static int32_t get_offset_of_m_Aborted_34() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111, ___m_Aborted_34)); }
	inline int32_t get_m_Aborted_34() const { return ___m_Aborted_34; }
	inline int32_t* get_address_of_m_Aborted_34() { return &___m_Aborted_34; }
	inline void set_m_Aborted_34(int32_t value)
	{
		___m_Aborted_34 = value;
	}
};

struct FileWebRequest_t1571840111_StaticFields
{
public:
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetRequestStreamCallback
	WaitCallback_t2798937288 * ___s_GetRequestStreamCallback_12;
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetResponseCallback
	WaitCallback_t2798937288 * ___s_GetResponseCallback_13;
	// System.Threading.ContextCallback System.Net.FileWebRequest::s_WrappedGetRequestStreamCallback
	ContextCallback_t2287130692 * ___s_WrappedGetRequestStreamCallback_14;
	// System.Threading.ContextCallback System.Net.FileWebRequest::s_WrappedResponseCallback
	ContextCallback_t2287130692 * ___s_WrappedResponseCallback_15;

public:
	inline static int32_t get_offset_of_s_GetRequestStreamCallback_12() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111_StaticFields, ___s_GetRequestStreamCallback_12)); }
	inline WaitCallback_t2798937288 * get_s_GetRequestStreamCallback_12() const { return ___s_GetRequestStreamCallback_12; }
	inline WaitCallback_t2798937288 ** get_address_of_s_GetRequestStreamCallback_12() { return &___s_GetRequestStreamCallback_12; }
	inline void set_s_GetRequestStreamCallback_12(WaitCallback_t2798937288 * value)
	{
		___s_GetRequestStreamCallback_12 = value;
		Il2CppCodeGenWriteBarrier(&___s_GetRequestStreamCallback_12, value);
	}

	inline static int32_t get_offset_of_s_GetResponseCallback_13() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111_StaticFields, ___s_GetResponseCallback_13)); }
	inline WaitCallback_t2798937288 * get_s_GetResponseCallback_13() const { return ___s_GetResponseCallback_13; }
	inline WaitCallback_t2798937288 ** get_address_of_s_GetResponseCallback_13() { return &___s_GetResponseCallback_13; }
	inline void set_s_GetResponseCallback_13(WaitCallback_t2798937288 * value)
	{
		___s_GetResponseCallback_13 = value;
		Il2CppCodeGenWriteBarrier(&___s_GetResponseCallback_13, value);
	}

	inline static int32_t get_offset_of_s_WrappedGetRequestStreamCallback_14() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111_StaticFields, ___s_WrappedGetRequestStreamCallback_14)); }
	inline ContextCallback_t2287130692 * get_s_WrappedGetRequestStreamCallback_14() const { return ___s_WrappedGetRequestStreamCallback_14; }
	inline ContextCallback_t2287130692 ** get_address_of_s_WrappedGetRequestStreamCallback_14() { return &___s_WrappedGetRequestStreamCallback_14; }
	inline void set_s_WrappedGetRequestStreamCallback_14(ContextCallback_t2287130692 * value)
	{
		___s_WrappedGetRequestStreamCallback_14 = value;
		Il2CppCodeGenWriteBarrier(&___s_WrappedGetRequestStreamCallback_14, value);
	}

	inline static int32_t get_offset_of_s_WrappedResponseCallback_15() { return static_cast<int32_t>(offsetof(FileWebRequest_t1571840111_StaticFields, ___s_WrappedResponseCallback_15)); }
	inline ContextCallback_t2287130692 * get_s_WrappedResponseCallback_15() const { return ___s_WrappedResponseCallback_15; }
	inline ContextCallback_t2287130692 ** get_address_of_s_WrappedResponseCallback_15() { return &___s_WrappedResponseCallback_15; }
	inline void set_s_WrappedResponseCallback_15(ContextCallback_t2287130692 * value)
	{
		___s_WrappedResponseCallback_15 = value;
		Il2CppCodeGenWriteBarrier(&___s_WrappedResponseCallback_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
