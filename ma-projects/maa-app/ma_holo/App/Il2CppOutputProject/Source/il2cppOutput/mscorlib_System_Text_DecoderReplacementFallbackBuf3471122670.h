﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_DecoderFallbackBuffer4206371382.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.DecoderReplacementFallbackBuffer
struct  DecoderReplacementFallbackBuffer_t3471122670  : public DecoderFallbackBuffer_t4206371382
{
public:
	// System.String System.Text.DecoderReplacementFallbackBuffer::strDefault
	String_t* ___strDefault_2;
	// System.Int32 System.Text.DecoderReplacementFallbackBuffer::fallbackCount
	int32_t ___fallbackCount_3;
	// System.Int32 System.Text.DecoderReplacementFallbackBuffer::fallbackIndex
	int32_t ___fallbackIndex_4;

public:
	inline static int32_t get_offset_of_strDefault_2() { return static_cast<int32_t>(offsetof(DecoderReplacementFallbackBuffer_t3471122670, ___strDefault_2)); }
	inline String_t* get_strDefault_2() const { return ___strDefault_2; }
	inline String_t** get_address_of_strDefault_2() { return &___strDefault_2; }
	inline void set_strDefault_2(String_t* value)
	{
		___strDefault_2 = value;
		Il2CppCodeGenWriteBarrier(&___strDefault_2, value);
	}

	inline static int32_t get_offset_of_fallbackCount_3() { return static_cast<int32_t>(offsetof(DecoderReplacementFallbackBuffer_t3471122670, ___fallbackCount_3)); }
	inline int32_t get_fallbackCount_3() const { return ___fallbackCount_3; }
	inline int32_t* get_address_of_fallbackCount_3() { return &___fallbackCount_3; }
	inline void set_fallbackCount_3(int32_t value)
	{
		___fallbackCount_3 = value;
	}

	inline static int32_t get_offset_of_fallbackIndex_4() { return static_cast<int32_t>(offsetof(DecoderReplacementFallbackBuffer_t3471122670, ___fallbackIndex_4)); }
	inline int32_t get_fallbackIndex_4() const { return ___fallbackIndex_4; }
	inline int32_t* get_address_of_fallbackIndex_4() { return &___fallbackIndex_4; }
	inline void set_fallbackIndex_4(int32_t value)
	{
		___fallbackIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
