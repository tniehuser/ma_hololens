﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Xml.XmlDownloadManager
struct XmlDownloadManager_t830495394;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRegisteredNonCachedStream
struct  XmlRegisteredNonCachedStream_t1825156356  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.Xml.XmlRegisteredNonCachedStream::stream
	Stream_t3255436806 * ___stream_8;
	// System.Xml.XmlDownloadManager System.Xml.XmlRegisteredNonCachedStream::downloadManager
	XmlDownloadManager_t830495394 * ___downloadManager_9;
	// System.String System.Xml.XmlRegisteredNonCachedStream::host
	String_t* ___host_10;

public:
	inline static int32_t get_offset_of_stream_8() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___stream_8)); }
	inline Stream_t3255436806 * get_stream_8() const { return ___stream_8; }
	inline Stream_t3255436806 ** get_address_of_stream_8() { return &___stream_8; }
	inline void set_stream_8(Stream_t3255436806 * value)
	{
		___stream_8 = value;
		Il2CppCodeGenWriteBarrier(&___stream_8, value);
	}

	inline static int32_t get_offset_of_downloadManager_9() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___downloadManager_9)); }
	inline XmlDownloadManager_t830495394 * get_downloadManager_9() const { return ___downloadManager_9; }
	inline XmlDownloadManager_t830495394 ** get_address_of_downloadManager_9() { return &___downloadManager_9; }
	inline void set_downloadManager_9(XmlDownloadManager_t830495394 * value)
	{
		___downloadManager_9 = value;
		Il2CppCodeGenWriteBarrier(&___downloadManager_9, value);
	}

	inline static int32_t get_offset_of_host_10() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___host_10)); }
	inline String_t* get_host_10() const { return ___host_10; }
	inline String_t** get_address_of_host_10() { return &___host_10; }
	inline void set_host_10(String_t* value)
	{
		___host_10 = value;
		Il2CppCodeGenWriteBarrier(&___host_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
