﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t4238939941;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache
struct  SerObjectInfoCache_t4068137215  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::fullTypeName
	String_t* ___fullTypeName_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::assemblyString
	String_t* ___assemblyString_1;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::hasTypeForwardedFrom
	bool ___hasTypeForwardedFrom_2;
	// System.Reflection.MemberInfo[] System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::memberInfos
	MemberInfoU5BU5D_t4238939941* ___memberInfos_3;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::memberNames
	StringU5BU5D_t1642385972* ___memberNames_4;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache::memberTypes
	TypeU5BU5D_t1664964607* ___memberTypes_5;

public:
	inline static int32_t get_offset_of_fullTypeName_0() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___fullTypeName_0)); }
	inline String_t* get_fullTypeName_0() const { return ___fullTypeName_0; }
	inline String_t** get_address_of_fullTypeName_0() { return &___fullTypeName_0; }
	inline void set_fullTypeName_0(String_t* value)
	{
		___fullTypeName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fullTypeName_0, value);
	}

	inline static int32_t get_offset_of_assemblyString_1() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___assemblyString_1)); }
	inline String_t* get_assemblyString_1() const { return ___assemblyString_1; }
	inline String_t** get_address_of_assemblyString_1() { return &___assemblyString_1; }
	inline void set_assemblyString_1(String_t* value)
	{
		___assemblyString_1 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyString_1, value);
	}

	inline static int32_t get_offset_of_hasTypeForwardedFrom_2() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___hasTypeForwardedFrom_2)); }
	inline bool get_hasTypeForwardedFrom_2() const { return ___hasTypeForwardedFrom_2; }
	inline bool* get_address_of_hasTypeForwardedFrom_2() { return &___hasTypeForwardedFrom_2; }
	inline void set_hasTypeForwardedFrom_2(bool value)
	{
		___hasTypeForwardedFrom_2 = value;
	}

	inline static int32_t get_offset_of_memberInfos_3() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___memberInfos_3)); }
	inline MemberInfoU5BU5D_t4238939941* get_memberInfos_3() const { return ___memberInfos_3; }
	inline MemberInfoU5BU5D_t4238939941** get_address_of_memberInfos_3() { return &___memberInfos_3; }
	inline void set_memberInfos_3(MemberInfoU5BU5D_t4238939941* value)
	{
		___memberInfos_3 = value;
		Il2CppCodeGenWriteBarrier(&___memberInfos_3, value);
	}

	inline static int32_t get_offset_of_memberNames_4() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___memberNames_4)); }
	inline StringU5BU5D_t1642385972* get_memberNames_4() const { return ___memberNames_4; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_4() { return &___memberNames_4; }
	inline void set_memberNames_4(StringU5BU5D_t1642385972* value)
	{
		___memberNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_4, value);
	}

	inline static int32_t get_offset_of_memberTypes_5() { return static_cast<int32_t>(offsetof(SerObjectInfoCache_t4068137215, ___memberTypes_5)); }
	inline TypeU5BU5D_t1664964607* get_memberTypes_5() const { return ___memberTypes_5; }
	inline TypeU5BU5D_t1664964607** get_address_of_memberTypes_5() { return &___memberTypes_5; }
	inline void set_memberTypes_5(TypeU5BU5D_t1664964607* value)
	{
		___memberTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
