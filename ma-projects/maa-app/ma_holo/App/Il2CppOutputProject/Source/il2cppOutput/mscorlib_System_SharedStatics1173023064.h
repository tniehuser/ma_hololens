﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.SharedStatics
struct SharedStatics_t1173023064;
// System.Security.Util.Tokenizer/StringMaker
struct StringMaker_t3542918456;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SharedStatics
struct  SharedStatics_t1173023064  : public Il2CppObject
{
public:
	// System.Security.Util.Tokenizer/StringMaker System.SharedStatics::_maker
	StringMaker_t3542918456 * ____maker_1;

public:
	inline static int32_t get_offset_of__maker_1() { return static_cast<int32_t>(offsetof(SharedStatics_t1173023064, ____maker_1)); }
	inline StringMaker_t3542918456 * get__maker_1() const { return ____maker_1; }
	inline StringMaker_t3542918456 ** get_address_of__maker_1() { return &____maker_1; }
	inline void set__maker_1(StringMaker_t3542918456 * value)
	{
		____maker_1 = value;
		Il2CppCodeGenWriteBarrier(&____maker_1, value);
	}
};

struct SharedStatics_t1173023064_StaticFields
{
public:
	// System.SharedStatics System.SharedStatics::_sharedStatics
	SharedStatics_t1173023064 * ____sharedStatics_0;

public:
	inline static int32_t get_offset_of__sharedStatics_0() { return static_cast<int32_t>(offsetof(SharedStatics_t1173023064_StaticFields, ____sharedStatics_0)); }
	inline SharedStatics_t1173023064 * get__sharedStatics_0() const { return ____sharedStatics_0; }
	inline SharedStatics_t1173023064 ** get_address_of__sharedStatics_0() { return &____sharedStatics_0; }
	inline void set__sharedStatics_0(SharedStatics_t1173023064 * value)
	{
		____sharedStatics_0 = value;
		Il2CppCodeGenWriteBarrier(&____sharedStatics_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
