﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.EventDescriptor[]
struct EventDescriptorU5BU5D_t2599422448;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.ComponentModel.EventDescriptorCollection
struct EventDescriptorCollection_t3053042509;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventDescriptorCollection
struct  EventDescriptorCollection_t3053042509  : public Il2CppObject
{
public:
	// System.ComponentModel.EventDescriptor[] System.ComponentModel.EventDescriptorCollection::events
	EventDescriptorU5BU5D_t2599422448* ___events_0;
	// System.String[] System.ComponentModel.EventDescriptorCollection::namedSort
	StringU5BU5D_t1642385972* ___namedSort_1;
	// System.Collections.IComparer System.ComponentModel.EventDescriptorCollection::comparer
	Il2CppObject * ___comparer_2;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::eventsOwned
	bool ___eventsOwned_3;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::needSort
	bool ___needSort_4;
	// System.Int32 System.ComponentModel.EventDescriptorCollection::eventCount
	int32_t ___eventCount_5;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::readOnly
	bool ___readOnly_6;

public:
	inline static int32_t get_offset_of_events_0() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___events_0)); }
	inline EventDescriptorU5BU5D_t2599422448* get_events_0() const { return ___events_0; }
	inline EventDescriptorU5BU5D_t2599422448** get_address_of_events_0() { return &___events_0; }
	inline void set_events_0(EventDescriptorU5BU5D_t2599422448* value)
	{
		___events_0 = value;
		Il2CppCodeGenWriteBarrier(&___events_0, value);
	}

	inline static int32_t get_offset_of_namedSort_1() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___namedSort_1)); }
	inline StringU5BU5D_t1642385972* get_namedSort_1() const { return ___namedSort_1; }
	inline StringU5BU5D_t1642385972** get_address_of_namedSort_1() { return &___namedSort_1; }
	inline void set_namedSort_1(StringU5BU5D_t1642385972* value)
	{
		___namedSort_1 = value;
		Il2CppCodeGenWriteBarrier(&___namedSort_1, value);
	}

	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___comparer_2)); }
	inline Il2CppObject * get_comparer_2() const { return ___comparer_2; }
	inline Il2CppObject ** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(Il2CppObject * value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_2, value);
	}

	inline static int32_t get_offset_of_eventsOwned_3() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___eventsOwned_3)); }
	inline bool get_eventsOwned_3() const { return ___eventsOwned_3; }
	inline bool* get_address_of_eventsOwned_3() { return &___eventsOwned_3; }
	inline void set_eventsOwned_3(bool value)
	{
		___eventsOwned_3 = value;
	}

	inline static int32_t get_offset_of_needSort_4() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___needSort_4)); }
	inline bool get_needSort_4() const { return ___needSort_4; }
	inline bool* get_address_of_needSort_4() { return &___needSort_4; }
	inline void set_needSort_4(bool value)
	{
		___needSort_4 = value;
	}

	inline static int32_t get_offset_of_eventCount_5() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___eventCount_5)); }
	inline int32_t get_eventCount_5() const { return ___eventCount_5; }
	inline int32_t* get_address_of_eventCount_5() { return &___eventCount_5; }
	inline void set_eventCount_5(int32_t value)
	{
		___eventCount_5 = value;
	}

	inline static int32_t get_offset_of_readOnly_6() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509, ___readOnly_6)); }
	inline bool get_readOnly_6() const { return ___readOnly_6; }
	inline bool* get_address_of_readOnly_6() { return &___readOnly_6; }
	inline void set_readOnly_6(bool value)
	{
		___readOnly_6 = value;
	}
};

struct EventDescriptorCollection_t3053042509_StaticFields
{
public:
	// System.ComponentModel.EventDescriptorCollection System.ComponentModel.EventDescriptorCollection::Empty
	EventDescriptorCollection_t3053042509 * ___Empty_7;

public:
	inline static int32_t get_offset_of_Empty_7() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_t3053042509_StaticFields, ___Empty_7)); }
	inline EventDescriptorCollection_t3053042509 * get_Empty_7() const { return ___Empty_7; }
	inline EventDescriptorCollection_t3053042509 ** get_address_of_Empty_7() { return &___Empty_7; }
	inline void set_Empty_7(EventDescriptorCollection_t3053042509 * value)
	{
		___Empty_7 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
