﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask1272420930.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure4171467654.h"

// System.Type
struct Type_t;
// System.Uri
struct Uri_t19570940;
// System.Xml.XmlUrlResolver
struct XmlUrlResolver_t896669594;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0
struct  U3CGetEntityAsyncU3Ec__async0_t381354349 
{
public:
	// System.Type System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::ofObjectToReturn
	Type_t * ___ofObjectToReturn_0;
	// System.Uri System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::absoluteUri
	Uri_t19570940 * ___absoluteUri_1;
	// System.Xml.XmlUrlResolver System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::$this
	XmlUrlResolver_t896669594 * ___U24this_2;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object> System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::$builder
	AsyncTaskMethodBuilder_1_t1272420930  ___U24builder_3;
	// System.Int32 System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::$PC
	int32_t ___U24PC_4;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.IO.Stream> System.Xml.XmlUrlResolver/<GetEntityAsync>c__async0::$awaiter0
	ConfiguredTaskAwaiter_t4171467654  ___U24awaiter0_5;

public:
	inline static int32_t get_offset_of_ofObjectToReturn_0() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___ofObjectToReturn_0)); }
	inline Type_t * get_ofObjectToReturn_0() const { return ___ofObjectToReturn_0; }
	inline Type_t ** get_address_of_ofObjectToReturn_0() { return &___ofObjectToReturn_0; }
	inline void set_ofObjectToReturn_0(Type_t * value)
	{
		___ofObjectToReturn_0 = value;
		Il2CppCodeGenWriteBarrier(&___ofObjectToReturn_0, value);
	}

	inline static int32_t get_offset_of_absoluteUri_1() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___absoluteUri_1)); }
	inline Uri_t19570940 * get_absoluteUri_1() const { return ___absoluteUri_1; }
	inline Uri_t19570940 ** get_address_of_absoluteUri_1() { return &___absoluteUri_1; }
	inline void set_absoluteUri_1(Uri_t19570940 * value)
	{
		___absoluteUri_1 = value;
		Il2CppCodeGenWriteBarrier(&___absoluteUri_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___U24this_2)); }
	inline XmlUrlResolver_t896669594 * get_U24this_2() const { return ___U24this_2; }
	inline XmlUrlResolver_t896669594 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(XmlUrlResolver_t896669594 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24builder_3() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___U24builder_3)); }
	inline AsyncTaskMethodBuilder_1_t1272420930  get_U24builder_3() const { return ___U24builder_3; }
	inline AsyncTaskMethodBuilder_1_t1272420930 * get_address_of_U24builder_3() { return &___U24builder_3; }
	inline void set_U24builder_3(AsyncTaskMethodBuilder_1_t1272420930  value)
	{
		___U24builder_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24awaiter0_5() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ec__async0_t381354349, ___U24awaiter0_5)); }
	inline ConfiguredTaskAwaiter_t4171467654  get_U24awaiter0_5() const { return ___U24awaiter0_5; }
	inline ConfiguredTaskAwaiter_t4171467654 * get_address_of_U24awaiter0_5() { return &___U24awaiter0_5; }
	inline void set_U24awaiter0_5(ConfiguredTaskAwaiter_t4171467654  value)
	{
		___U24awaiter0_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
