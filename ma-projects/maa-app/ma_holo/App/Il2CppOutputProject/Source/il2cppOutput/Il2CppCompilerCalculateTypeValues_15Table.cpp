﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_InputRecord3971050487.h"
#include "mscorlib_System_Coord151819767.h"
#include "mscorlib_System_SmallRect2523714067.h"
#include "mscorlib_System_ConsoleScreenBufferInfo211837289.h"
#include "mscorlib_System_Handles2032702205.h"
#include "mscorlib_System_WindowsConsoleDriver276624544.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2882533465.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3066379751.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2731437132.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1568637719.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2375206766.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra762068660.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3894236545.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1459944470.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1568637717.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3113433916.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3872948716.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1568637713.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2731437126.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra762068664.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1568637715.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2306864765.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra762068654.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2306864773.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2217126741.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1500295684.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra762068658.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3894236547.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1459944466.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2358289439.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1054327435.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra337496371.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3894236543.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1903580244.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra740780927.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1195490030.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1054327437.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3894236537.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra337496398.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra698097168.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3894236541.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra337496305.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra415772071.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr4045332913.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3621845308.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1500295816.h"
#include "mscorlib_System___Il2CppComObject4064417062.h"
#include "mscorlib_System___Il2CppComDelegate3311706788.h"
#include "System_Security_U3CModuleU3E3783534214.h"
#include "Mono_Security_U3CModuleU3E3783534214.h"
#include "Mono_Security_Locale4255929014.h"
#include "Mono_Security_Mono_Math_BigInteger925946152.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign874893935.h"
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing80355991.h"
#include "Mono_Security_Mono_Math_BigInteger_Kernel1353186455.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor1997037801.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest572679901.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTests3283102398.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGener1053438167.h"
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialS463670656.h"
#include "Mono_Security_Mono_Security_ASN1924533535.h"
#include "Mono_Security_Mono_Security_ASN1Convert3301846396.h"
#include "Mono_Security_Mono_Security_BitConverterLE2825370260.h"
#include "Mono_Security_Mono_Security_PKCS73223261922.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo1443605387.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData2656813772.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag3379271383.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoCon4146607874.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilde3965881084.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyPairPe3637935872.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS13312870480.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS82103016899.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Priva92917103.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Enc1722354997.h"
#include "Mono_Security_Mono_Security_Cryptography_RC42789934315.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage3034748747.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged108853709.h"
#include "Mono_Security_Mono_Security_X509_SafeBag2166702855.h"
#include "Mono_Security_Mono_Security_X509_PKCS121362584794.h"
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveByte1740753016.h"
#include "Mono_Security_Mono_Security_X509_X501349661534.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3592472865.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3487770522.h"
#include "Mono_Security_Mono_Security_X509_X509Chain1938971907.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl2843686920.h"
#include "Mono_Security_Mono_Security_X509_X509Crl1699034837.h"
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEnt743353844.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1439760127.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl1640144839.h"
#include "Mono_Security_Mono_Security_X509_X509Store4028973563.h"
#include "Mono_Security_Mono_Security_X509_X509StoreManager1740460066.h"
#include "Mono_Security_Mono_Security_X509_X509Stores3001420398.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Authori795428182.h"
#include "Mono_Security_Mono_Security_X509_Extensions_BasicC3608227951.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Extend3816993686.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Genera2355256240.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsag530589947.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsa1909787375.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (InputRecord_t3971050487)+ sizeof (Il2CppObject), sizeof(InputRecord_t3971050487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1500[9] = 
{
	InputRecord_t3971050487::get_offset_of_EventType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_KeyDown_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_RepeatCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_VirtualKeyCode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_VirtualScanCode_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_Character_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_ControlKeyState_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_pad1_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputRecord_t3971050487::get_offset_of_pad2_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (Coord_t151819767)+ sizeof (Il2CppObject), sizeof(Coord_t151819767 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1501[2] = 
{
	Coord_t151819767::get_offset_of_X_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Coord_t151819767::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (SmallRect_t2523714067)+ sizeof (Il2CppObject), sizeof(SmallRect_t2523714067 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1502[4] = 
{
	SmallRect_t2523714067::get_offset_of_Left_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmallRect_t2523714067::get_offset_of_Top_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmallRect_t2523714067::get_offset_of_Right_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmallRect_t2523714067::get_offset_of_Bottom_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (ConsoleScreenBufferInfo_t211837289)+ sizeof (Il2CppObject), sizeof(ConsoleScreenBufferInfo_t211837289 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1503[5] = 
{
	ConsoleScreenBufferInfo_t211837289::get_offset_of_Size_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleScreenBufferInfo_t211837289::get_offset_of_CursorPosition_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleScreenBufferInfo_t211837289::get_offset_of_Attribute_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleScreenBufferInfo_t211837289::get_offset_of_Window_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleScreenBufferInfo_t211837289::get_offset_of_MaxWindowSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (Handles_t2032702205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1504[4] = 
{
	Handles_t2032702205::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (WindowsConsoleDriver_t276624544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[3] = 
{
	WindowsConsoleDriver_t276624544::get_offset_of_inputHandle_0(),
	WindowsConsoleDriver_t276624544::get_offset_of_outputHandle_1(),
	WindowsConsoleDriver_t276624544::get_offset_of_defaultAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305137), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1506[136] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D6E5DC824F803F8565AF31B42199DAE39FE7F4EA9_0(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DBE1BDEC0AA74B4DCB079943E70528096CCA985F8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DD28E8ABDBD777A482CE0EE5C24814ACAE52AABFE_2(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D4E3B533C39447AAEB59A8E48FABD7E15B5B5D195_3(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DE75835D001C843F156FBA01B001DFE1B8029AC17_4(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D320B018758ECE3752FFEDBAEB1A6DB67C80B9359_5(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DCF0B42666EF5E37EDEA0AB8E173E42C196D03814_6(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5_7(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DDF2F233DCDBB3568E0A055EB44EF8664BDAC9B0A_8(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DDDC2ED880C539C7F33DCD753EFC6935C3B89FBE3_9(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DC52D8C8A03F8882B47882CF64D7CEB5B16ABCB6F_10(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D479E6F4B309C1271A72CE31C4B974CBD1E46BF65_11(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7C2A9CB288780591A4DD199ED7FDF1C472107480_12(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D37B9BCADB3EB6095AB8C927DAE9A0E9A441DAF18_13(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1A5C53512C2C03B33C159F7812B37583471A8D03_14(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D44AF584FD19CC6DF2385F293ADA6E0019E7BE183_15(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DCC15284E6A49EF7DB9D2B39A26B096384F569476_16(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DA721C62955A330286E96B479DB7541A41C4983D7_17(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DE8AA9C2F35723FDE82408FCF60820DFBB7EDCBE7_18(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D23A65DEDCF5EE72DA424C5523A346EA9D72E35AC_19(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D5D033C1A00E415600D96B834A19DE753D0F86B39_20(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1708D3021987F34D391566AA230CDFE0413CED85_21(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D3443C5AD82F84896DC3D0BE60E26BB64179F0E3C_22(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D3B357C7DF6D4E1233422B95EE0BA35C67A4AF17B_23(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB53A2C6DF21FC88B17AEFC40EB895B8D63210CDF_24(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D5BCD21C341BE6DDF8FFFAE1A23ABA24DCBB612BF_25(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D98B4ABF6739017F1DE287ECB905CD31267242EC1_26(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DE92B39D8233061927D9ACDE54665E68E7535635A_27(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DDD3AEFEADB1CD615F3017763F1568179FEE640B0_28(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D704939CD172085D1295FCE3F1D92431D685D7AA2_29(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D3F9D18A6806D6DFFE913298361C2352DBE1FA632_30(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D94841DD2F330CCB1089BF413E4FA9B04505152E2_31(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D973417296623D8DC6961B09664E54039E44CA5D8_32(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DF37E34BEADB04F34FCC31078A59F49856CA83D5B_33(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D34476C29F6F81C989CFCA42F7C06E84C66236834_34(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D95C48758CAE1715783472FB073AB158AB8A0AB2A_35(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DBEBC9ECC660A13EFC359BA3383411F698CFF25DB_36(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D5BFE2819B4778217C56416C7585FF0E56EBACD89_37(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DAC791C4F39504D1184B73478943D0636258DA7B1_38(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D3FE6C283BCF384FD2C8789880DFF59664E2AB4A1_39(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2F71D2DA12F3CD0A6A112F5A5A75B4FDC6FE8547_40(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D67EEAD805D708D9AA4E14BF747E44CED801744F3_41(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D536422B321459B242ADED7240B7447E904E083E3_42(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D0588059ACBD52F7EA2835882F977A9CF72EB9775_43(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D39C9CE73C7B0619D409EF28344F687C1B5C130FE_44(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7341C933A70EAE383CC50C4B945ADB8E08F06737_45(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DA6732F8E7FC23766AB329B492D6BF82E3B33233F_46(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DEC89C317EA2BF49A70EFF5E89C691E34733D7C37_47(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D4858DB4AA76D3933F1CA9E6712D4FDB16903F628_48(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2C840AFA48C27B9C05593E468C1232CA1CC74AFD_49(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB9B670F134A59FB1107AF01A9FE8F8E3980B3093_50(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D4F7A8890F332B22B8DE0BD29D36FA7364748D76A_51(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D65E32B4E150FD8D24B93B0D42A17F1DAD146162B_52(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D823566DA642D6EA356E15585921F2A4CA23D6760_53(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D40E7C49413D261F3F38AD3A870C0AC69C8BDA048_54(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DF06E829E62F3AFBC045D064E10A4F5DF7C969612_55(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D8CAA092E783257106251246FF5C97F88D28517A6_56(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D25420D0055076FA8D3E4DD96BC53AE24DE6E619F_57(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D8E10AC2F34545DFBBF3FCBC06055D797A8C99991_58(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DBEE1CFE5DFAA408E14CE4AF4DCD824FA2E42DCB7_59(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D871B9CF85DB352BAADF12BAE8F19857683E385AC_60(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DA0074C15377C0C870B055927403EA9FA7A349D12_61(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D57F320D62696EC99727E0FE2045A05F1289CC0C6_62(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7FE820C9CF0F0B90445A71F1D262D22E4F0C4C68_63(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DE45BAB43F7D5D038672B3E3431F92E34A7AF2571_64(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB472ED77CB3B2A66D49D179F1EE2081B70A6AB61_65(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D609C0E8D8DA86A09D6013D301C86BA8782C16B8C_66(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D0A1ADB22C1D3E1F4B2448EE3F27DF9DE63329C4C_67(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_68(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D46232052BC757E030490D851F265FB47FA100902_69(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1AEF3D8DF416A46288C91C724CBF7B154D9E5BF3_70(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB6002BBF29B2704922EC3BBF0F9EE40ABF185D6B_71(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7F42F2EDC974BE29B2746957416ED1AEFA605F47_72(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D99E2E88877D14C7DDC4E957A0ED7079CA0E9EB24_73(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_74(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_75(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D0F5F33751392772515C71AC02BA5B693F1D12632_76(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DF8FAABB821300AA500C2CEC6091B3782A7FB44A4_77(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7088AAE49F0627B72729078DE6E3182DDCF8ED99_78(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1730F09044E91DB8371B849EFF5E6D17BDE4AED0_79(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D89A040451C8CC5C8FB268BE44BDD74964C104155_80(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2D1DA5BB407F0C11C3B5116196C0C6374D932B20_81(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1FE6CE411858B3D864679DE2139FB081F08BFACD_82(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2B33BEC8C30DFDC49DAFE20D3BDE19487850D717_83(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D93A63E90605400F34B49F0EB3361D23C89164BDA_84(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D3E823444D2DFECF0F90B436B88F02A533CB376F1_85(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D95264589E48F94B7857CFF398FB72A537E13EEE2_86(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DD78D08081C7A5AD6FBA7A8DC86BCD6D7A577C636_87(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D82C383F8E6E4D3D87AEBB986A5D0077E8AD157C4_88(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2BA840FF6020B8FF623DBCB7188248CF853FAF4F_89(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D5BBDF8058D4235C33F2E8DCF76004031B6187A2F_90(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D6C71197D228427B2864C69B357FEF73D8C9D59DF_91(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DF34B0E10653402E8F788F8BC3F7CD7090928A429_92(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D82C2A59850B2E85BCE1A45A479537A384DF6098D_93(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DEA9506959484C55CFE0C139C624DF6060E285866_94(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D81917F1E21F3C22B9F916994547A614FB03E968E_95(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DE1827270A5FE1C85F5352A66FD87BA747213D006_96(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DAFE25993FE067C90453601A90D14BD629149C2E0_97(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DF7803FC4163D22D8D1D4F19D2D35E788809A4AF5_98(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB29E0E3A81E1E581B1A077ACB1F579B16C1BCEEB_99(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DAFCD4E1211233E99373A3367B23105A3D624B1F2_100(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB8F87834C3597B2EEF22BA6D3A392CC925636401_101(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D6FC754859E4EC74E447048364B216D825C6F8FE7_102(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D46C15ECC6530B9BBB1CA075BF3BA7713B5533C9D_103(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D594A33A00BC4F785DFD43E3C6C44FBA1242CCAF3_104(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DCEBD8CCBE1ABD3679074BFC819878F36DF7D5A99_105(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DDA19DB47B583EFCF7825D2E39D661D2354F28219_106(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D8CC72CE51642811009902536B2E680DB083388DF_107(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DD117188BE8D4609C0D531C51B0BB911A4219DEBE_108(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D6770974FEF1E98B9C1864370E2B5B786EB0EA39E_109(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DCAF75B09D8CC258F09AAB5F71235C9E167C35E1B_110(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D96A9D5FE3077C2A211132CE03286DB96E4859315_111(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D7B23C087834C09BBCB09E8854A5366A1FD6249EA_112(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DA5444763673307F6828C748D4B9708CFC02B0959_113(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DA13AA52274D951A18029131A8DDECF76B569A15D_114(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D99B2D83E8D4A808B94E210BC991F9E70EBEDF9F3_115(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D421EC7E82F2967DF6CA8C3605514DC6F29EE5845_116(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DC1A1100642BA9685B30A84D97348484E14AA1865_117(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D6D797C11E1D4FB68B6570CF2A92B792433527065_118(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_119(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_120(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_121(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D1E41C4CD0767AEA21C00DEABA2EA9407F1E6CEA5_122(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D646036A65DECCD6835C914A46E6E44B729433B60_123(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DD002CBBE1FF33721AF7C4D1D3ECAD1B7DB5258B7_124(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D0392525BCB01691D1F319D89F2C12BF93A478467_125(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D179F932E5BBE33F97809B399A2623CFAD06B3012_126(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DEB237D93FA22EEE4936E8D363A0AD5117F5F3FB0_127(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D08F57E46A9814DEEB2371005EBA0B829CC0E925C_128(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D686246862DB9C9C770B2A0D0F65EB6FEFAC8A5C5_129(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D2A05421D1AF62C1EAC723F240BDB87234EAA97DC_130(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D15F9D5527DCECAD2159A8365536713ECB2DF6C06_131(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D8D231DD55FE1AD7631BBD0905A17D5EB616C2154_132(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D092A9763325508F994AF0408AFB6D4B9494D2F68_133(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2D121EC59E23F7559B28D338D562528F6299C2DE22_134(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24fieldU2DEBF68F411848D603D059DFDEA2321C5A5EA78044_135(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (U24ArrayTypeU3D3132_t2882533465)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D3132_t2882533465 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (U24ArrayTypeU3D256_t3066379751)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D256_t3066379751 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (U24ArrayTypeU3D64_t762068660)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t762068660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (U24ArrayTypeU3D4_t1459944470)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4_t1459944470 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (U24ArrayTypeU3D288_t3113433916)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D288_t3113433916 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (U24ArrayTypeU3D132_t3872948716)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D132_t3872948716 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (U24ArrayTypeU3D52_t1568637713)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D52_t1568637713 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (U24ArrayTypeU3D40_t2731437126)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D40_t2731437126 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (U24ArrayTypeU3D72_t1568637715)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D72_t1568637715 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (U24ArrayTypeU3D128_t2306864765)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D128_t2306864765 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (U24ArrayTypeU3D84_t762068654)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D84_t762068654 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (U24ArrayTypeU3D120_t2306864773)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D120_t2306864773 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (U24ArrayTypeU3D4096_t2217126741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4096_t2217126741 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (U24ArrayTypeU3D640_t1500295684)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D640_t1500295684 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (U24ArrayTypeU3D44_t762068658)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D44_t762068658 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (U24ArrayTypeU3D36_t3894236547)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t3894236547 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (U24ArrayTypeU3D8_t1459944466)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t1459944466 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (U24ArrayTypeU3D2384_t2358289439)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2384_t2358289439 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (U24ArrayTypeU3D1000_t1054327435)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1000_t1054327435 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (U24ArrayTypeU3D360_t337496371)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D360_t337496371 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (U24ArrayTypeU3D76_t3894236543)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D76_t3894236543 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (U24ArrayTypeU3D176_t1903580244)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D176_t1903580244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (U24ArrayTypeU3D212_t740780927)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D212_t740780927 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (U24ArrayTypeU3D2352_t1195490030)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2352_t1195490030 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (U24ArrayTypeU3D1020_t1054327437)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1020_t1054327437 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (U24ArrayTypeU3D264_t337496398)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D264_t337496398 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (U24ArrayTypeU3D2048_t698097168)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2048_t698097168 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (U24ArrayTypeU3D56_t3894236541)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D56_t3894236541 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (U24ArrayTypeU3D160_t337496305)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D160_t337496305 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (U24ArrayTypeU3D1668_t415772071)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1668_t415772071 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (U24ArrayTypeU3D2100_t4045332913)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2100_t4045332913 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (U24ArrayTypeU3D1452_t3621845308)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1452_t3621845308 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (U24ArrayTypeU3D240_t1500295816)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D240_t1500295816 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (__Il2CppComDelegate_t3311706788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (U3CModuleU3E_t3783534215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (Locale_t4255929015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (BigInteger_t925946153), -1, sizeof(BigInteger_t925946153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1552[4] = 
{
	BigInteger_t925946153::get_offset_of_length_0(),
	BigInteger_t925946153::get_offset_of_data_1(),
	BigInteger_t925946153_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t925946153_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (Sign_t874893936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1553[4] = 
{
	Sign_t874893936::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (ModulusRing_t80355992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[2] = 
{
	ModulusRing_t80355992::get_offset_of_mod_0(),
	ModulusRing_t80355992::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (Kernel_t1353186456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (ConfidenceFactor_t1997037802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1556[7] = 
{
	ConfidenceFactor_t1997037802::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (PrimalityTest_t572679902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (PrimalityTests_t3283102399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (PrimeGeneratorBase_t1053438168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (SequentialSearchPrimeGeneratorBase_t463670657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (ASN1_t924533536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[3] = 
{
	ASN1_t924533536::get_offset_of_m_nTag_0(),
	ASN1_t924533536::get_offset_of_m_aValue_1(),
	ASN1_t924533536::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (ASN1Convert_t3301846397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (BitConverterLE_t2825370261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (PKCS7_t3223261923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (ContentInfo_t1443605388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1565[2] = 
{
	ContentInfo_t1443605388::get_offset_of_contentType_0(),
	ContentInfo_t1443605388::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (EncryptedData_t2656813773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1566[4] = 
{
	EncryptedData_t2656813773::get_offset_of__version_0(),
	EncryptedData_t2656813773::get_offset_of__content_1(),
	EncryptedData_t2656813773::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2656813773::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (ARC4Managed_t3379271383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1567[5] = 
{
	ARC4Managed_t3379271383::get_offset_of_key_11(),
	ARC4Managed_t3379271383::get_offset_of_state_12(),
	ARC4Managed_t3379271383::get_offset_of_x_13(),
	ARC4Managed_t3379271383::get_offset_of_y_14(),
	ARC4Managed_t3379271383::get_offset_of_m_disposed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (CryptoConvert_t4146607875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (KeyBuilder_t3965881085), -1, sizeof(KeyBuilder_t3965881085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1569[1] = 
{
	KeyBuilder_t3965881085_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (KeyPairPersistence_t3637935873), -1, sizeof(KeyPairPersistence_t3637935873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1570[9] = 
{
	KeyPairPersistence_t3637935873_StaticFields::get_offset_of__userPathExists_0(),
	KeyPairPersistence_t3637935873_StaticFields::get_offset_of__userPath_1(),
	KeyPairPersistence_t3637935873_StaticFields::get_offset_of__machinePathExists_2(),
	KeyPairPersistence_t3637935873_StaticFields::get_offset_of__machinePath_3(),
	KeyPairPersistence_t3637935873::get_offset_of__params_4(),
	KeyPairPersistence_t3637935873::get_offset_of__keyvalue_5(),
	KeyPairPersistence_t3637935873::get_offset_of__filename_6(),
	KeyPairPersistence_t3637935873::get_offset_of__container_7(),
	KeyPairPersistence_t3637935873_StaticFields::get_offset_of_lockobj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (PKCS1_t3312870481), -1, sizeof(PKCS1_t3312870481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1571[5] = 
{
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA512_3(),
	PKCS1_t3312870481_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (PKCS8_t2103016900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (PrivateKeyInfo_t92917104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[4] = 
{
	PrivateKeyInfo_t92917104::get_offset_of__version_0(),
	PrivateKeyInfo_t92917104::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t92917104::get_offset_of__key_2(),
	PrivateKeyInfo_t92917104::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (EncryptedPrivateKeyInfo_t1722354998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[4] = 
{
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (RC4_t2789934315), -1, sizeof(RC4_t2789934315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1575[2] = 
{
	RC4_t2789934315_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	RC4_t2789934315_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (RSAManaged_t3034748748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[13] = 
{
	RSAManaged_t3034748748::get_offset_of_isCRTpossible_2(),
	RSAManaged_t3034748748::get_offset_of_keyBlinding_3(),
	RSAManaged_t3034748748::get_offset_of_keypairGenerated_4(),
	RSAManaged_t3034748748::get_offset_of_m_disposed_5(),
	RSAManaged_t3034748748::get_offset_of_d_6(),
	RSAManaged_t3034748748::get_offset_of_p_7(),
	RSAManaged_t3034748748::get_offset_of_q_8(),
	RSAManaged_t3034748748::get_offset_of_dp_9(),
	RSAManaged_t3034748748::get_offset_of_dq_10(),
	RSAManaged_t3034748748::get_offset_of_qInv_11(),
	RSAManaged_t3034748748::get_offset_of_n_12(),
	RSAManaged_t3034748748::get_offset_of_e_13(),
	RSAManaged_t3034748748::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (KeyGeneratedEventHandler_t108853710), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (SafeBag_t2166702856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[2] = 
{
	SafeBag_t2166702856::get_offset_of__bagOID_0(),
	SafeBag_t2166702856::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (PKCS12_t1362584795), -1, sizeof(PKCS12_t1362584795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1579[12] = 
{
	PKCS12_t1362584795::get_offset_of__password_0(),
	PKCS12_t1362584795::get_offset_of__keyBags_1(),
	PKCS12_t1362584795::get_offset_of__secretBags_2(),
	PKCS12_t1362584795::get_offset_of__certs_3(),
	PKCS12_t1362584795::get_offset_of__keyBagsChanged_4(),
	PKCS12_t1362584795::get_offset_of__secretBagsChanged_5(),
	PKCS12_t1362584795::get_offset_of__certsChanged_6(),
	PKCS12_t1362584795::get_offset_of__iterations_7(),
	PKCS12_t1362584795::get_offset_of__safeBags_8(),
	PKCS12_t1362584795::get_offset_of__rng_9(),
	PKCS12_t1362584795_StaticFields::get_offset_of_password_max_length_10(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (DeriveBytes_t1740753017), -1, sizeof(DeriveBytes_t1740753017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1580[7] = 
{
	DeriveBytes_t1740753017_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t1740753017_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t1740753017_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t1740753017::get_offset_of__hashName_3(),
	DeriveBytes_t1740753017::get_offset_of__iterations_4(),
	DeriveBytes_t1740753017::get_offset_of__password_5(),
	DeriveBytes_t1740753017::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (X501_t349661535), -1, sizeof(X501_t349661535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1581[15] = 
{
	X501_t349661535_StaticFields::get_offset_of_countryName_0(),
	X501_t349661535_StaticFields::get_offset_of_organizationName_1(),
	X501_t349661535_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_t349661535_StaticFields::get_offset_of_commonName_3(),
	X501_t349661535_StaticFields::get_offset_of_localityName_4(),
	X501_t349661535_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_t349661535_StaticFields::get_offset_of_streetAddress_6(),
	X501_t349661535_StaticFields::get_offset_of_domainComponent_7(),
	X501_t349661535_StaticFields::get_offset_of_userid_8(),
	X501_t349661535_StaticFields::get_offset_of_email_9(),
	X501_t349661535_StaticFields::get_offset_of_dnQualifier_10(),
	X501_t349661535_StaticFields::get_offset_of_title_11(),
	X501_t349661535_StaticFields::get_offset_of_surname_12(),
	X501_t349661535_StaticFields::get_offset_of_givenName_13(),
	X501_t349661535_StaticFields::get_offset_of_initial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (X509Certificate_t324051958), -1, sizeof(X509Certificate_t324051958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1582[24] = 
{
	X509Certificate_t324051958::get_offset_of_decoder_0(),
	X509Certificate_t324051958::get_offset_of_m_encodedcert_1(),
	X509Certificate_t324051958::get_offset_of_m_from_2(),
	X509Certificate_t324051958::get_offset_of_m_until_3(),
	X509Certificate_t324051958::get_offset_of_issuer_4(),
	X509Certificate_t324051958::get_offset_of_m_issuername_5(),
	X509Certificate_t324051958::get_offset_of_m_keyalgo_6(),
	X509Certificate_t324051958::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_t324051958::get_offset_of_subject_8(),
	X509Certificate_t324051958::get_offset_of_m_subject_9(),
	X509Certificate_t324051958::get_offset_of_m_publickey_10(),
	X509Certificate_t324051958::get_offset_of_signature_11(),
	X509Certificate_t324051958::get_offset_of_m_signaturealgo_12(),
	X509Certificate_t324051958::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_t324051958::get_offset_of_certhash_14(),
	X509Certificate_t324051958::get_offset_of__rsa_15(),
	X509Certificate_t324051958::get_offset_of__dsa_16(),
	X509Certificate_t324051958::get_offset_of_version_17(),
	X509Certificate_t324051958::get_offset_of_serialnumber_18(),
	X509Certificate_t324051958::get_offset_of_issuerUniqueID_19(),
	X509Certificate_t324051958::get_offset_of_subjectUniqueID_20(),
	X509Certificate_t324051958::get_offset_of_extensions_21(),
	X509Certificate_t324051958_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_t324051958_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (X509CertificateCollection_t3592472866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (X509CertificateEnumerator_t3487770523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[1] = 
{
	X509CertificateEnumerator_t3487770523::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (X509Chain_t1938971908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[5] = 
{
	X509Chain_t1938971908::get_offset_of_roots_0(),
	X509Chain_t1938971908::get_offset_of_certs_1(),
	X509Chain_t1938971908::get_offset_of__root_2(),
	X509Chain_t1938971908::get_offset_of__chain_3(),
	X509Chain_t1938971908::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (X509ChainStatusFlags_t2843686920)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1586[8] = 
{
	X509ChainStatusFlags_t2843686920::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (X509Crl_t1699034837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[10] = 
{
	X509Crl_t1699034837::get_offset_of_issuer_0(),
	X509Crl_t1699034837::get_offset_of_version_1(),
	X509Crl_t1699034837::get_offset_of_thisUpdate_2(),
	X509Crl_t1699034837::get_offset_of_nextUpdate_3(),
	X509Crl_t1699034837::get_offset_of_entries_4(),
	X509Crl_t1699034837::get_offset_of_signatureOID_5(),
	X509Crl_t1699034837::get_offset_of_signature_6(),
	X509Crl_t1699034837::get_offset_of_extensions_7(),
	X509Crl_t1699034837::get_offset_of_encoded_8(),
	X509Crl_t1699034837::get_offset_of_hash_value_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (X509CrlEntry_t743353844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[3] = 
{
	X509CrlEntry_t743353844::get_offset_of_sn_0(),
	X509CrlEntry_t743353844::get_offset_of_revocationDate_1(),
	X509CrlEntry_t743353844::get_offset_of_extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (X509Extension_t1439760128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[3] = 
{
	X509Extension_t1439760128::get_offset_of_extnOid_0(),
	X509Extension_t1439760128::get_offset_of_extnCritical_1(),
	X509Extension_t1439760128::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (X509ExtensionCollection_t1640144840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	X509ExtensionCollection_t1640144840::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (X509Store_t4028973564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[5] = 
{
	X509Store_t4028973564::get_offset_of__storePath_0(),
	X509Store_t4028973564::get_offset_of__certificates_1(),
	X509Store_t4028973564::get_offset_of__crls_2(),
	X509Store_t4028973564::get_offset_of__crl_3(),
	X509Store_t4028973564::get_offset_of__newFormat_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (X509StoreManager_t1740460067), -1, sizeof(X509StoreManager_t1740460067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1592[4] = 
{
	X509StoreManager_t1740460067_StaticFields::get_offset_of__userPath_0(),
	X509StoreManager_t1740460067_StaticFields::get_offset_of__localMachinePath_1(),
	X509StoreManager_t1740460067_StaticFields::get_offset_of__userStore_2(),
	X509StoreManager_t1740460067_StaticFields::get_offset_of__machineStore_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (X509Stores_t3001420399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[3] = 
{
	X509Stores_t3001420399::get_offset_of__storePath_0(),
	X509Stores_t3001420399::get_offset_of__newFormat_1(),
	X509Stores_t3001420399::get_offset_of__trusted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (AuthorityKeyIdentifierExtension_t795428182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[1] = 
{
	AuthorityKeyIdentifierExtension_t795428182::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (BasicConstraintsExtension_t3608227952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[2] = 
{
	BasicConstraintsExtension_t3608227952::get_offset_of_cA_3(),
	BasicConstraintsExtension_t3608227952::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (ExtendedKeyUsageExtension_t3816993686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[1] = 
{
	ExtendedKeyUsageExtension_t3816993686::get_offset_of_keyPurpose_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (GeneralNames_t2355256240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[5] = 
{
	GeneralNames_t2355256240::get_offset_of_rfc822Name_0(),
	GeneralNames_t2355256240::get_offset_of_dnsName_1(),
	GeneralNames_t2355256240::get_offset_of_directoryNames_2(),
	GeneralNames_t2355256240::get_offset_of_uris_3(),
	GeneralNames_t2355256240::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (KeyUsages_t530589947)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1598[11] = 
{
	KeyUsages_t530589947::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (KeyUsageExtension_t1909787375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[1] = 
{
	KeyUsageExtension_t1909787375::get_offset_of_kubits_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
