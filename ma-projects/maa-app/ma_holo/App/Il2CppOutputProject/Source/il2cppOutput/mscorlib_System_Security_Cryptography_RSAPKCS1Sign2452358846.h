﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_SignatureDescr89145500.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1SignatureDescription
struct  RSAPKCS1SignatureDescription_t2452358846  : public SignatureDescription_t89145500
{
public:
	// System.String System.Security.Cryptography.RSAPKCS1SignatureDescription::_hashAlgorithm
	String_t* ____hashAlgorithm_4;

public:
	inline static int32_t get_offset_of__hashAlgorithm_4() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureDescription_t2452358846, ____hashAlgorithm_4)); }
	inline String_t* get__hashAlgorithm_4() const { return ____hashAlgorithm_4; }
	inline String_t** get_address_of__hashAlgorithm_4() { return &____hashAlgorithm_4; }
	inline void set__hashAlgorithm_4(String_t* value)
	{
		____hashAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier(&____hashAlgorithm_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
