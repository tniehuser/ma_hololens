﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_Linq_Enumerable_Iterator_1_gen3157253607.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereListIterator`1<System.Object>
struct  WhereListIterator_1_t1069043000  : public Iterator_1_t3157253607
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereListIterator`1::source
	List_1_t2058570427 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereListIterator`1::predicate
	Func_2_t3961629604 * ___predicate_4;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereListIterator`1::enumerator
	Enumerator_t1593300101  ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t1069043000, ___source_3)); }
	inline List_1_t2058570427 * get_source_3() const { return ___source_3; }
	inline List_1_t2058570427 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t2058570427 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t1069043000, ___predicate_4)); }
	inline Func_2_t3961629604 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t3961629604 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t3961629604 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_4, value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t1069043000, ___enumerator_5)); }
	inline Enumerator_t1593300101  get_enumerator_5() const { return ___enumerator_5; }
	inline Enumerator_t1593300101 * get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(Enumerator_t1593300101  value)
	{
		___enumerator_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
