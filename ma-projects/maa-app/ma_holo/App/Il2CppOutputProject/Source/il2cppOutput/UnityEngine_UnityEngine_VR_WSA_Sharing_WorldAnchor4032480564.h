﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch
struct  WorldAnchorTransferBatch_t4032480564  : public Il2CppObject
{
public:
	// System.IntPtr UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch::m_NativePtr
	IntPtr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(WorldAnchorTransferBatch_t4032480564, ___m_NativePtr_0)); }
	inline IntPtr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline IntPtr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(IntPtr_t value)
	{
		___m_NativePtr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
