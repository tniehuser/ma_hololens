﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CharUnicodeInfo/UnicodeDataHeader
struct  UnicodeDataHeader_t2784996206 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Char System.Globalization.CharUnicodeInfo/UnicodeDataHeader::TableName
			Il2CppChar ___TableName_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Il2CppChar ___TableName_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___version_1_OffsetPadding[32];
			// System.UInt16 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::version
			uint16_t ___version_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___version_1_OffsetPadding_forAlignmentOnly[32];
			uint16_t ___version_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding[40];
			// System.UInt32 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::OffsetToCategoriesIndex
			uint32_t ___OffsetToCategoriesIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding_forAlignmentOnly[40];
			uint32_t ___OffsetToCategoriesIndex_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding[44];
			// System.UInt32 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::OffsetToCategoriesValue
			uint32_t ___OffsetToCategoriesValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding_forAlignmentOnly[44];
			uint32_t ___OffsetToCategoriesValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding[48];
			// System.UInt32 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::OffsetToNumbericIndex
			uint32_t ___OffsetToNumbericIndex_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding_forAlignmentOnly[48];
			uint32_t ___OffsetToNumbericIndex_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding[52];
			// System.UInt32 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::OffsetToDigitValue
			uint32_t ___OffsetToDigitValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding_forAlignmentOnly[52];
			uint32_t ___OffsetToDigitValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding[56];
			// System.UInt32 System.Globalization.CharUnicodeInfo/UnicodeDataHeader::OffsetToNumbericValue
			uint32_t ___OffsetToNumbericValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding_forAlignmentOnly[56];
			uint32_t ___OffsetToNumbericValue_6_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_TableName_0() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___TableName_0)); }
	inline Il2CppChar get_TableName_0() const { return ___TableName_0; }
	inline Il2CppChar* get_address_of_TableName_0() { return &___TableName_0; }
	inline void set_TableName_0(Il2CppChar value)
	{
		___TableName_0 = value;
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___version_1)); }
	inline uint16_t get_version_1() const { return ___version_1; }
	inline uint16_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint16_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_OffsetToCategoriesIndex_2() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___OffsetToCategoriesIndex_2)); }
	inline uint32_t get_OffsetToCategoriesIndex_2() const { return ___OffsetToCategoriesIndex_2; }
	inline uint32_t* get_address_of_OffsetToCategoriesIndex_2() { return &___OffsetToCategoriesIndex_2; }
	inline void set_OffsetToCategoriesIndex_2(uint32_t value)
	{
		___OffsetToCategoriesIndex_2 = value;
	}

	inline static int32_t get_offset_of_OffsetToCategoriesValue_3() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___OffsetToCategoriesValue_3)); }
	inline uint32_t get_OffsetToCategoriesValue_3() const { return ___OffsetToCategoriesValue_3; }
	inline uint32_t* get_address_of_OffsetToCategoriesValue_3() { return &___OffsetToCategoriesValue_3; }
	inline void set_OffsetToCategoriesValue_3(uint32_t value)
	{
		___OffsetToCategoriesValue_3 = value;
	}

	inline static int32_t get_offset_of_OffsetToNumbericIndex_4() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___OffsetToNumbericIndex_4)); }
	inline uint32_t get_OffsetToNumbericIndex_4() const { return ___OffsetToNumbericIndex_4; }
	inline uint32_t* get_address_of_OffsetToNumbericIndex_4() { return &___OffsetToNumbericIndex_4; }
	inline void set_OffsetToNumbericIndex_4(uint32_t value)
	{
		___OffsetToNumbericIndex_4 = value;
	}

	inline static int32_t get_offset_of_OffsetToDigitValue_5() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___OffsetToDigitValue_5)); }
	inline uint32_t get_OffsetToDigitValue_5() const { return ___OffsetToDigitValue_5; }
	inline uint32_t* get_address_of_OffsetToDigitValue_5() { return &___OffsetToDigitValue_5; }
	inline void set_OffsetToDigitValue_5(uint32_t value)
	{
		___OffsetToDigitValue_5 = value;
	}

	inline static int32_t get_offset_of_OffsetToNumbericValue_6() { return static_cast<int32_t>(offsetof(UnicodeDataHeader_t2784996206, ___OffsetToNumbericValue_6)); }
	inline uint32_t get_OffsetToNumbericValue_6() const { return ___OffsetToNumbericValue_6; }
	inline uint32_t* get_address_of_OffsetToNumbericValue_6() { return &___OffsetToNumbericValue_6; }
	inline void set_OffsetToNumbericValue_6(uint32_t value)
	{
		___OffsetToNumbericValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CharUnicodeInfo/UnicodeDataHeader
struct UnicodeDataHeader_t2784996206_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___TableName_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___TableName_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___version_1_OffsetPadding[32];
			uint16_t ___version_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___version_1_OffsetPadding_forAlignmentOnly[32];
			uint16_t ___version_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding[40];
			uint32_t ___OffsetToCategoriesIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding_forAlignmentOnly[40];
			uint32_t ___OffsetToCategoriesIndex_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding[44];
			uint32_t ___OffsetToCategoriesValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding_forAlignmentOnly[44];
			uint32_t ___OffsetToCategoriesValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding[48];
			uint32_t ___OffsetToNumbericIndex_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding_forAlignmentOnly[48];
			uint32_t ___OffsetToNumbericIndex_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding[52];
			uint32_t ___OffsetToDigitValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding_forAlignmentOnly[52];
			uint32_t ___OffsetToDigitValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding[56];
			uint32_t ___OffsetToNumbericValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding_forAlignmentOnly[56];
			uint32_t ___OffsetToNumbericValue_6_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of System.Globalization.CharUnicodeInfo/UnicodeDataHeader
struct UnicodeDataHeader_t2784996206_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___TableName_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___TableName_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___version_1_OffsetPadding[32];
			uint16_t ___version_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___version_1_OffsetPadding_forAlignmentOnly[32];
			uint16_t ___version_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding[40];
			uint32_t ___OffsetToCategoriesIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesIndex_2_OffsetPadding_forAlignmentOnly[40];
			uint32_t ___OffsetToCategoriesIndex_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding[44];
			uint32_t ___OffsetToCategoriesValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToCategoriesValue_3_OffsetPadding_forAlignmentOnly[44];
			uint32_t ___OffsetToCategoriesValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding[48];
			uint32_t ___OffsetToNumbericIndex_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericIndex_4_OffsetPadding_forAlignmentOnly[48];
			uint32_t ___OffsetToNumbericIndex_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding[52];
			uint32_t ___OffsetToDigitValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToDigitValue_5_OffsetPadding_forAlignmentOnly[52];
			uint32_t ___OffsetToDigitValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding[56];
			uint32_t ___OffsetToNumbericValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___OffsetToNumbericValue_6_OffsetPadding_forAlignmentOnly[56];
			uint32_t ___OffsetToNumbericValue_6_forAlignmentOnly;
		};
	};
};
