﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Diagnostics.TraceImplSettings
struct TraceImplSettings_t1186465586;
// System.Collections.IDictionary
struct IDictionary_t596158605;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DiagnosticsConfigurationHandler
struct  DiagnosticsConfigurationHandler_t610492850  : public Il2CppObject
{
public:
	// System.Diagnostics.TraceImplSettings System.Diagnostics.DiagnosticsConfigurationHandler::configValues
	TraceImplSettings_t1186465586 * ___configValues_0;
	// System.Collections.IDictionary System.Diagnostics.DiagnosticsConfigurationHandler::elementHandlers
	Il2CppObject * ___elementHandlers_1;

public:
	inline static int32_t get_offset_of_configValues_0() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t610492850, ___configValues_0)); }
	inline TraceImplSettings_t1186465586 * get_configValues_0() const { return ___configValues_0; }
	inline TraceImplSettings_t1186465586 ** get_address_of_configValues_0() { return &___configValues_0; }
	inline void set_configValues_0(TraceImplSettings_t1186465586 * value)
	{
		___configValues_0 = value;
		Il2CppCodeGenWriteBarrier(&___configValues_0, value);
	}

	inline static int32_t get_offset_of_elementHandlers_1() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t610492850, ___elementHandlers_1)); }
	inline Il2CppObject * get_elementHandlers_1() const { return ___elementHandlers_1; }
	inline Il2CppObject ** get_address_of_elementHandlers_1() { return &___elementHandlers_1; }
	inline void set_elementHandlers_1(Il2CppObject * value)
	{
		___elementHandlers_1 = value;
		Il2CppCodeGenWriteBarrier(&___elementHandlers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
