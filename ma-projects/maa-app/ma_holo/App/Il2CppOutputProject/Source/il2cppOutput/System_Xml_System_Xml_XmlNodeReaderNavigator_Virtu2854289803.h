﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct  VirtualAttribute_t2854289803 
{
public:
	// System.String System.Xml.XmlNodeReaderNavigator/VirtualAttribute::name
	String_t* ___name_0;
	// System.String System.Xml.XmlNodeReaderNavigator/VirtualAttribute::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(VirtualAttribute_t2854289803, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(VirtualAttribute_t2854289803, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct VirtualAttribute_t2854289803_marshaled_pinvoke
{
	char* ___name_0;
	char* ___value_1;
};
// Native definition for COM marshalling of System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct VirtualAttribute_t2854289803_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___value_1;
};
