﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int64[]
struct Int64U5BU5D_t717125112;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.LongList
struct  LongList_t2406893678  : public Il2CppObject
{
public:
	// System.Int64[] System.Runtime.Serialization.LongList::m_values
	Int64U5BU5D_t717125112* ___m_values_0;
	// System.Int32 System.Runtime.Serialization.LongList::m_count
	int32_t ___m_count_1;
	// System.Int32 System.Runtime.Serialization.LongList::m_totalItems
	int32_t ___m_totalItems_2;
	// System.Int32 System.Runtime.Serialization.LongList::m_currentItem
	int32_t ___m_currentItem_3;

public:
	inline static int32_t get_offset_of_m_values_0() { return static_cast<int32_t>(offsetof(LongList_t2406893678, ___m_values_0)); }
	inline Int64U5BU5D_t717125112* get_m_values_0() const { return ___m_values_0; }
	inline Int64U5BU5D_t717125112** get_address_of_m_values_0() { return &___m_values_0; }
	inline void set_m_values_0(Int64U5BU5D_t717125112* value)
	{
		___m_values_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_values_0, value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(LongList_t2406893678, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_totalItems_2() { return static_cast<int32_t>(offsetof(LongList_t2406893678, ___m_totalItems_2)); }
	inline int32_t get_m_totalItems_2() const { return ___m_totalItems_2; }
	inline int32_t* get_address_of_m_totalItems_2() { return &___m_totalItems_2; }
	inline void set_m_totalItems_2(int32_t value)
	{
		___m_totalItems_2 = value;
	}

	inline static int32_t get_offset_of_m_currentItem_3() { return static_cast<int32_t>(offsetof(LongList_t2406893678, ___m_currentItem_3)); }
	inline int32_t get_m_currentItem_3() const { return ___m_currentItem_3; }
	inline int32_t* get_address_of_m_currentItem_3() { return &___m_currentItem_3; }
	inline void set_m_currentItem_3(int32_t value)
	{
		___m_currentItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
