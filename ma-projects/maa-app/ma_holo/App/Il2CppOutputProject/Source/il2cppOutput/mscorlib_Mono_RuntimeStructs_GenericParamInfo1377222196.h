﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeStructs_MonoClass2595527713.h"
#include "mscorlib_System_IntPtr2504060609.h"

// Mono.RuntimeStructs/MonoClass
struct MonoClass_t2595527713;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeStructs/GenericParamInfo
struct  GenericParamInfo_t1377222196 
{
public:
	// Mono.RuntimeStructs/MonoClass* Mono.RuntimeStructs/GenericParamInfo::pklass
	MonoClass_t2595527713 * ___pklass_0;
	// System.IntPtr Mono.RuntimeStructs/GenericParamInfo::name
	IntPtr_t ___name_1;
	// System.UInt16 Mono.RuntimeStructs/GenericParamInfo::flags
	uint16_t ___flags_2;
	// System.UInt32 Mono.RuntimeStructs/GenericParamInfo::token
	uint32_t ___token_3;
	// Mono.RuntimeStructs/MonoClass** Mono.RuntimeStructs/GenericParamInfo::constraints
	MonoClass_t2595527713 ** ___constraints_4;

public:
	inline static int32_t get_offset_of_pklass_0() { return static_cast<int32_t>(offsetof(GenericParamInfo_t1377222196, ___pklass_0)); }
	inline MonoClass_t2595527713 * get_pklass_0() const { return ___pklass_0; }
	inline MonoClass_t2595527713 ** get_address_of_pklass_0() { return &___pklass_0; }
	inline void set_pklass_0(MonoClass_t2595527713 * value)
	{
		___pklass_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GenericParamInfo_t1377222196, ___name_1)); }
	inline IntPtr_t get_name_1() const { return ___name_1; }
	inline IntPtr_t* get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(IntPtr_t value)
	{
		___name_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(GenericParamInfo_t1377222196, ___flags_2)); }
	inline uint16_t get_flags_2() const { return ___flags_2; }
	inline uint16_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(uint16_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(GenericParamInfo_t1377222196, ___token_3)); }
	inline uint32_t get_token_3() const { return ___token_3; }
	inline uint32_t* get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(uint32_t value)
	{
		___token_3 = value;
	}

	inline static int32_t get_offset_of_constraints_4() { return static_cast<int32_t>(offsetof(GenericParamInfo_t1377222196, ___constraints_4)); }
	inline MonoClass_t2595527713 ** get_constraints_4() const { return ___constraints_4; }
	inline MonoClass_t2595527713 *** get_address_of_constraints_4() { return &___constraints_4; }
	inline void set_constraints_4(MonoClass_t2595527713 ** value)
	{
		___constraints_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeStructs/GenericParamInfo
struct GenericParamInfo_t1377222196_marshaled_pinvoke
{
	MonoClass_t2595527713 * ___pklass_0;
	intptr_t ___name_1;
	uint16_t ___flags_2;
	uint32_t ___token_3;
	MonoClass_t2595527713 ** ___constraints_4;
};
// Native definition for COM marshalling of Mono.RuntimeStructs/GenericParamInfo
struct GenericParamInfo_t1377222196_marshaled_com
{
	MonoClass_t2595527713 * ___pklass_0;
	intptr_t ___name_1;
	uint16_t ___flags_2;
	uint32_t ___token_3;
	MonoClass_t2595527713 ** ___constraints_4;
};
