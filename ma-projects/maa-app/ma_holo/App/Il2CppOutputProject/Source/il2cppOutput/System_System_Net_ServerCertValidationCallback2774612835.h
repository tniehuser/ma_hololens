﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback
struct  ServerCertValidationCallback_t2774612835  : public Il2CppObject
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServerCertValidationCallback::m_ValidationCallback
	RemoteCertificateValidationCallback_t2756269959 * ___m_ValidationCallback_0;
	// System.Threading.ExecutionContext System.Net.ServerCertValidationCallback::m_Context
	ExecutionContext_t1392266323 * ___m_Context_1;

public:
	inline static int32_t get_offset_of_m_ValidationCallback_0() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t2774612835, ___m_ValidationCallback_0)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_m_ValidationCallback_0() const { return ___m_ValidationCallback_0; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_m_ValidationCallback_0() { return &___m_ValidationCallback_0; }
	inline void set_m_ValidationCallback_0(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___m_ValidationCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_ValidationCallback_0, value);
	}

	inline static int32_t get_offset_of_m_Context_1() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t2774612835, ___m_Context_1)); }
	inline ExecutionContext_t1392266323 * get_m_Context_1() const { return ___m_Context_1; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_Context_1() { return &___m_Context_1; }
	inline void set_m_Context_1(ExecutionContext_t1392266323 * value)
	{
		___m_Context_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Context_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
