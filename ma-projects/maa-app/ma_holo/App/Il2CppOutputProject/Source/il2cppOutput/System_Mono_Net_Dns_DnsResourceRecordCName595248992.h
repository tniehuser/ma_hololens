﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_Net_Dns_DnsResourceRecord2943454412.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordCName
struct  DnsResourceRecordCName_t595248992  : public DnsResourceRecord_t2943454412
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordCName::cname
	String_t* ___cname_6;

public:
	inline static int32_t get_offset_of_cname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordCName_t595248992, ___cname_6)); }
	inline String_t* get_cname_6() const { return ___cname_6; }
	inline String_t** get_address_of_cname_6() { return &___cname_6; }
	inline void set_cname_6(String_t* value)
	{
		___cname_6 = value;
		Il2CppCodeGenWriteBarrier(&___cname_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
