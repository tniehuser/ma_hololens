﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Diagnostics_TraceListener3414949279.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DefaultTraceListener
struct  DefaultTraceListener_t1568159610  : public TraceListener_t3414949279
{
public:
	// System.String System.Diagnostics.DefaultTraceListener::logFileName
	String_t* ___logFileName_12;
	// System.Boolean System.Diagnostics.DefaultTraceListener::assertUiEnabled
	bool ___assertUiEnabled_13;

public:
	inline static int32_t get_offset_of_logFileName_12() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1568159610, ___logFileName_12)); }
	inline String_t* get_logFileName_12() const { return ___logFileName_12; }
	inline String_t** get_address_of_logFileName_12() { return &___logFileName_12; }
	inline void set_logFileName_12(String_t* value)
	{
		___logFileName_12 = value;
		Il2CppCodeGenWriteBarrier(&___logFileName_12, value);
	}

	inline static int32_t get_offset_of_assertUiEnabled_13() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1568159610, ___assertUiEnabled_13)); }
	inline bool get_assertUiEnabled_13() const { return ___assertUiEnabled_13; }
	inline bool* get_address_of_assertUiEnabled_13() { return &___assertUiEnabled_13; }
	inline void set_assertUiEnabled_13(bool value)
	{
		___assertUiEnabled_13 = value;
	}
};

struct DefaultTraceListener_t1568159610_StaticFields
{
public:
	// System.Boolean System.Diagnostics.DefaultTraceListener::OnWin32
	bool ___OnWin32_9;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTracePrefix
	String_t* ___MonoTracePrefix_10;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTraceFile
	String_t* ___MonoTraceFile_11;

public:
	inline static int32_t get_offset_of_OnWin32_9() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1568159610_StaticFields, ___OnWin32_9)); }
	inline bool get_OnWin32_9() const { return ___OnWin32_9; }
	inline bool* get_address_of_OnWin32_9() { return &___OnWin32_9; }
	inline void set_OnWin32_9(bool value)
	{
		___OnWin32_9 = value;
	}

	inline static int32_t get_offset_of_MonoTracePrefix_10() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1568159610_StaticFields, ___MonoTracePrefix_10)); }
	inline String_t* get_MonoTracePrefix_10() const { return ___MonoTracePrefix_10; }
	inline String_t** get_address_of_MonoTracePrefix_10() { return &___MonoTracePrefix_10; }
	inline void set_MonoTracePrefix_10(String_t* value)
	{
		___MonoTracePrefix_10 = value;
		Il2CppCodeGenWriteBarrier(&___MonoTracePrefix_10, value);
	}

	inline static int32_t get_offset_of_MonoTraceFile_11() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t1568159610_StaticFields, ___MonoTraceFile_11)); }
	inline String_t* get_MonoTraceFile_11() const { return ___MonoTraceFile_11; }
	inline String_t** get_address_of_MonoTraceFile_11() { return &___MonoTraceFile_11; }
	inline void set_MonoTraceFile_11(String_t* value)
	{
		___MonoTraceFile_11 = value;
		Il2CppCodeGenWriteBarrier(&___MonoTraceFile_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
