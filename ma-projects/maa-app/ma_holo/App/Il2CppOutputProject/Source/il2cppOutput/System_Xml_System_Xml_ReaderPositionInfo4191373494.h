﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_PositionInfo3273236083.h"

// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t135184468;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReaderPositionInfo
struct  ReaderPositionInfo_t4191373494  : public PositionInfo_t3273236083
{
public:
	// System.Xml.IXmlLineInfo System.Xml.ReaderPositionInfo::lineInfo
	Il2CppObject * ___lineInfo_0;

public:
	inline static int32_t get_offset_of_lineInfo_0() { return static_cast<int32_t>(offsetof(ReaderPositionInfo_t4191373494, ___lineInfo_0)); }
	inline Il2CppObject * get_lineInfo_0() const { return ___lineInfo_0; }
	inline Il2CppObject ** get_address_of_lineInfo_0() { return &___lineInfo_0; }
	inline void set_lineInfo_0(Il2CppObject * value)
	{
		___lineInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___lineInfo_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
