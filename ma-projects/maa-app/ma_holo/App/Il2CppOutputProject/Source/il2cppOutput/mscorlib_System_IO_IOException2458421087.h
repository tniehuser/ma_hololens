﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t2458421087  : public SystemException_t3877406272
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_16;

public:
	inline static int32_t get_offset_of__maybeFullPath_16() { return static_cast<int32_t>(offsetof(IOException_t2458421087, ____maybeFullPath_16)); }
	inline String_t* get__maybeFullPath_16() const { return ____maybeFullPath_16; }
	inline String_t** get_address_of__maybeFullPath_16() { return &____maybeFullPath_16; }
	inline void set__maybeFullPath_16(String_t* value)
	{
		____maybeFullPath_16 = value;
		Il2CppCodeGenWriteBarrier(&____maybeFullPath_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
