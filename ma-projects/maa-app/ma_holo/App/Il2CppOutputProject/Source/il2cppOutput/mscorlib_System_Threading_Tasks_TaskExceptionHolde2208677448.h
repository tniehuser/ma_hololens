﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.EventHandler
struct EventHandler_t277755526;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Collections.Generic.List`1<System.Runtime.ExceptionServices.ExceptionDispatchInfo>
struct List_1_t2711075433;
// System.Runtime.ExceptionServices.ExceptionDispatchInfo
struct ExceptionDispatchInfo_t3341954301;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskExceptionHolder
struct  TaskExceptionHolder_t2208677448  : public Il2CppObject
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.TaskExceptionHolder::m_task
	Task_t1843236107 * ___m_task_3;
	// System.Collections.Generic.List`1<System.Runtime.ExceptionServices.ExceptionDispatchInfo> modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.TaskExceptionHolder::m_faultExceptions
	List_1_t2711075433 * ___m_faultExceptions_4;
	// System.Runtime.ExceptionServices.ExceptionDispatchInfo System.Threading.Tasks.TaskExceptionHolder::m_cancellationException
	ExceptionDispatchInfo_t3341954301 * ___m_cancellationException_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.TaskExceptionHolder::m_isHandled
	bool ___m_isHandled_6;

public:
	inline static int32_t get_offset_of_m_task_3() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448, ___m_task_3)); }
	inline Task_t1843236107 * get_m_task_3() const { return ___m_task_3; }
	inline Task_t1843236107 ** get_address_of_m_task_3() { return &___m_task_3; }
	inline void set_m_task_3(Task_t1843236107 * value)
	{
		___m_task_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_task_3, value);
	}

	inline static int32_t get_offset_of_m_faultExceptions_4() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448, ___m_faultExceptions_4)); }
	inline List_1_t2711075433 * get_m_faultExceptions_4() const { return ___m_faultExceptions_4; }
	inline List_1_t2711075433 ** get_address_of_m_faultExceptions_4() { return &___m_faultExceptions_4; }
	inline void set_m_faultExceptions_4(List_1_t2711075433 * value)
	{
		___m_faultExceptions_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_faultExceptions_4, value);
	}

	inline static int32_t get_offset_of_m_cancellationException_5() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448, ___m_cancellationException_5)); }
	inline ExceptionDispatchInfo_t3341954301 * get_m_cancellationException_5() const { return ___m_cancellationException_5; }
	inline ExceptionDispatchInfo_t3341954301 ** get_address_of_m_cancellationException_5() { return &___m_cancellationException_5; }
	inline void set_m_cancellationException_5(ExceptionDispatchInfo_t3341954301 * value)
	{
		___m_cancellationException_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_cancellationException_5, value);
	}

	inline static int32_t get_offset_of_m_isHandled_6() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448, ___m_isHandled_6)); }
	inline bool get_m_isHandled_6() const { return ___m_isHandled_6; }
	inline bool* get_address_of_m_isHandled_6() { return &___m_isHandled_6; }
	inline void set_m_isHandled_6(bool value)
	{
		___m_isHandled_6 = value;
	}
};

struct TaskExceptionHolder_t2208677448_StaticFields
{
public:
	// System.Boolean System.Threading.Tasks.TaskExceptionHolder::s_failFastOnUnobservedException
	bool ___s_failFastOnUnobservedException_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.TaskExceptionHolder::s_domainUnloadStarted
	bool ___s_domainUnloadStarted_1;
	// System.EventHandler modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.TaskExceptionHolder::s_adUnloadEventHandler
	EventHandler_t277755526 * ___s_adUnloadEventHandler_2;
	// System.EventHandler System.Threading.Tasks.TaskExceptionHolder::<>f__mg$cache0
	EventHandler_t277755526 * ___U3CU3Ef__mgU24cache0_7;

public:
	inline static int32_t get_offset_of_s_failFastOnUnobservedException_0() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448_StaticFields, ___s_failFastOnUnobservedException_0)); }
	inline bool get_s_failFastOnUnobservedException_0() const { return ___s_failFastOnUnobservedException_0; }
	inline bool* get_address_of_s_failFastOnUnobservedException_0() { return &___s_failFastOnUnobservedException_0; }
	inline void set_s_failFastOnUnobservedException_0(bool value)
	{
		___s_failFastOnUnobservedException_0 = value;
	}

	inline static int32_t get_offset_of_s_domainUnloadStarted_1() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448_StaticFields, ___s_domainUnloadStarted_1)); }
	inline bool get_s_domainUnloadStarted_1() const { return ___s_domainUnloadStarted_1; }
	inline bool* get_address_of_s_domainUnloadStarted_1() { return &___s_domainUnloadStarted_1; }
	inline void set_s_domainUnloadStarted_1(bool value)
	{
		___s_domainUnloadStarted_1 = value;
	}

	inline static int32_t get_offset_of_s_adUnloadEventHandler_2() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448_StaticFields, ___s_adUnloadEventHandler_2)); }
	inline EventHandler_t277755526 * get_s_adUnloadEventHandler_2() const { return ___s_adUnloadEventHandler_2; }
	inline EventHandler_t277755526 ** get_address_of_s_adUnloadEventHandler_2() { return &___s_adUnloadEventHandler_2; }
	inline void set_s_adUnloadEventHandler_2(EventHandler_t277755526 * value)
	{
		___s_adUnloadEventHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_adUnloadEventHandler_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(TaskExceptionHolder_t2208677448_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline EventHandler_t277755526 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline EventHandler_t277755526 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(EventHandler_t277755526 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
