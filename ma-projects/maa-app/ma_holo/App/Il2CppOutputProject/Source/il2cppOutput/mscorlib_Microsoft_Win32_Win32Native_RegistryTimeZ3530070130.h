﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Interop_mincore_SYSTEMTIME2580015906.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.Win32Native/RegistryTimeZoneInformation
struct  RegistryTimeZoneInformation_t3530070130 
{
public:
	// System.Int32 Microsoft.Win32.Win32Native/RegistryTimeZoneInformation::Bias
	int32_t ___Bias_0;
	// System.Int32 Microsoft.Win32.Win32Native/RegistryTimeZoneInformation::StandardBias
	int32_t ___StandardBias_1;
	// System.Int32 Microsoft.Win32.Win32Native/RegistryTimeZoneInformation::DaylightBias
	int32_t ___DaylightBias_2;
	// Interop/mincore/SYSTEMTIME Microsoft.Win32.Win32Native/RegistryTimeZoneInformation::StandardDate
	SYSTEMTIME_t2580015906  ___StandardDate_3;
	// Interop/mincore/SYSTEMTIME Microsoft.Win32.Win32Native/RegistryTimeZoneInformation::DaylightDate
	SYSTEMTIME_t2580015906  ___DaylightDate_4;

public:
	inline static int32_t get_offset_of_Bias_0() { return static_cast<int32_t>(offsetof(RegistryTimeZoneInformation_t3530070130, ___Bias_0)); }
	inline int32_t get_Bias_0() const { return ___Bias_0; }
	inline int32_t* get_address_of_Bias_0() { return &___Bias_0; }
	inline void set_Bias_0(int32_t value)
	{
		___Bias_0 = value;
	}

	inline static int32_t get_offset_of_StandardBias_1() { return static_cast<int32_t>(offsetof(RegistryTimeZoneInformation_t3530070130, ___StandardBias_1)); }
	inline int32_t get_StandardBias_1() const { return ___StandardBias_1; }
	inline int32_t* get_address_of_StandardBias_1() { return &___StandardBias_1; }
	inline void set_StandardBias_1(int32_t value)
	{
		___StandardBias_1 = value;
	}

	inline static int32_t get_offset_of_DaylightBias_2() { return static_cast<int32_t>(offsetof(RegistryTimeZoneInformation_t3530070130, ___DaylightBias_2)); }
	inline int32_t get_DaylightBias_2() const { return ___DaylightBias_2; }
	inline int32_t* get_address_of_DaylightBias_2() { return &___DaylightBias_2; }
	inline void set_DaylightBias_2(int32_t value)
	{
		___DaylightBias_2 = value;
	}

	inline static int32_t get_offset_of_StandardDate_3() { return static_cast<int32_t>(offsetof(RegistryTimeZoneInformation_t3530070130, ___StandardDate_3)); }
	inline SYSTEMTIME_t2580015906  get_StandardDate_3() const { return ___StandardDate_3; }
	inline SYSTEMTIME_t2580015906 * get_address_of_StandardDate_3() { return &___StandardDate_3; }
	inline void set_StandardDate_3(SYSTEMTIME_t2580015906  value)
	{
		___StandardDate_3 = value;
	}

	inline static int32_t get_offset_of_DaylightDate_4() { return static_cast<int32_t>(offsetof(RegistryTimeZoneInformation_t3530070130, ___DaylightDate_4)); }
	inline SYSTEMTIME_t2580015906  get_DaylightDate_4() const { return ___DaylightDate_4; }
	inline SYSTEMTIME_t2580015906 * get_address_of_DaylightDate_4() { return &___DaylightDate_4; }
	inline void set_DaylightDate_4(SYSTEMTIME_t2580015906  value)
	{
		___DaylightDate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
