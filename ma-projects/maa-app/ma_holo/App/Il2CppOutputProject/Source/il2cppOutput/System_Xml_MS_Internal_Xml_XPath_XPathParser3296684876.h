﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MS.Internal.Xml.XPath.XPathScanner
struct XPathScanner_t1765956737;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser
struct  XPathParser_t3296684876  : public Il2CppObject
{
public:
	// MS.Internal.Xml.XPath.XPathScanner MS.Internal.Xml.XPath.XPathParser::scanner
	XPathScanner_t1765956737 * ___scanner_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser::parseDepth
	int32_t ___parseDepth_1;

public:
	inline static int32_t get_offset_of_scanner_0() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876, ___scanner_0)); }
	inline XPathScanner_t1765956737 * get_scanner_0() const { return ___scanner_0; }
	inline XPathScanner_t1765956737 ** get_address_of_scanner_0() { return &___scanner_0; }
	inline void set_scanner_0(XPathScanner_t1765956737 * value)
	{
		___scanner_0 = value;
		Il2CppCodeGenWriteBarrier(&___scanner_0, value);
	}

	inline static int32_t get_offset_of_parseDepth_1() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876, ___parseDepth_1)); }
	inline int32_t get_parseDepth_1() const { return ___parseDepth_1; }
	inline int32_t* get_address_of_parseDepth_1() { return &___parseDepth_1; }
	inline void set_parseDepth_1(int32_t value)
	{
		___parseDepth_1 = value;
	}
};

struct XPathParser_t3296684876_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray1
	XPathResultTypeU5BU5D_t2966113519* ___temparray1_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray2
	XPathResultTypeU5BU5D_t2966113519* ___temparray2_3;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray3
	XPathResultTypeU5BU5D_t2966113519* ___temparray3_4;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray4
	XPathResultTypeU5BU5D_t2966113519* ___temparray4_5;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray5
	XPathResultTypeU5BU5D_t2966113519* ___temparray5_6;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray6
	XPathResultTypeU5BU5D_t2966113519* ___temparray6_7;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray7
	XPathResultTypeU5BU5D_t2966113519* ___temparray7_8;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray8
	XPathResultTypeU5BU5D_t2966113519* ___temparray8_9;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser::temparray9
	XPathResultTypeU5BU5D_t2966113519* ___temparray9_10;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::functionTable
	Hashtable_t909839986 * ___functionTable_11;
	// System.Collections.Hashtable MS.Internal.Xml.XPath.XPathParser::AxesTable
	Hashtable_t909839986 * ___AxesTable_12;

public:
	inline static int32_t get_offset_of_temparray1_2() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray1_2)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray1_2() const { return ___temparray1_2; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray1_2() { return &___temparray1_2; }
	inline void set_temparray1_2(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray1_2 = value;
		Il2CppCodeGenWriteBarrier(&___temparray1_2, value);
	}

	inline static int32_t get_offset_of_temparray2_3() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray2_3)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray2_3() const { return ___temparray2_3; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray2_3() { return &___temparray2_3; }
	inline void set_temparray2_3(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray2_3 = value;
		Il2CppCodeGenWriteBarrier(&___temparray2_3, value);
	}

	inline static int32_t get_offset_of_temparray3_4() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray3_4)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray3_4() const { return ___temparray3_4; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray3_4() { return &___temparray3_4; }
	inline void set_temparray3_4(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray3_4 = value;
		Il2CppCodeGenWriteBarrier(&___temparray3_4, value);
	}

	inline static int32_t get_offset_of_temparray4_5() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray4_5)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray4_5() const { return ___temparray4_5; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray4_5() { return &___temparray4_5; }
	inline void set_temparray4_5(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray4_5 = value;
		Il2CppCodeGenWriteBarrier(&___temparray4_5, value);
	}

	inline static int32_t get_offset_of_temparray5_6() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray5_6)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray5_6() const { return ___temparray5_6; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray5_6() { return &___temparray5_6; }
	inline void set_temparray5_6(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray5_6 = value;
		Il2CppCodeGenWriteBarrier(&___temparray5_6, value);
	}

	inline static int32_t get_offset_of_temparray6_7() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray6_7)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray6_7() const { return ___temparray6_7; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray6_7() { return &___temparray6_7; }
	inline void set_temparray6_7(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray6_7 = value;
		Il2CppCodeGenWriteBarrier(&___temparray6_7, value);
	}

	inline static int32_t get_offset_of_temparray7_8() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray7_8)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray7_8() const { return ___temparray7_8; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray7_8() { return &___temparray7_8; }
	inline void set_temparray7_8(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray7_8 = value;
		Il2CppCodeGenWriteBarrier(&___temparray7_8, value);
	}

	inline static int32_t get_offset_of_temparray8_9() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray8_9)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray8_9() const { return ___temparray8_9; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray8_9() { return &___temparray8_9; }
	inline void set_temparray8_9(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray8_9 = value;
		Il2CppCodeGenWriteBarrier(&___temparray8_9, value);
	}

	inline static int32_t get_offset_of_temparray9_10() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___temparray9_10)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_temparray9_10() const { return ___temparray9_10; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_temparray9_10() { return &___temparray9_10; }
	inline void set_temparray9_10(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___temparray9_10 = value;
		Il2CppCodeGenWriteBarrier(&___temparray9_10, value);
	}

	inline static int32_t get_offset_of_functionTable_11() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___functionTable_11)); }
	inline Hashtable_t909839986 * get_functionTable_11() const { return ___functionTable_11; }
	inline Hashtable_t909839986 ** get_address_of_functionTable_11() { return &___functionTable_11; }
	inline void set_functionTable_11(Hashtable_t909839986 * value)
	{
		___functionTable_11 = value;
		Il2CppCodeGenWriteBarrier(&___functionTable_11, value);
	}

	inline static int32_t get_offset_of_AxesTable_12() { return static_cast<int32_t>(offsetof(XPathParser_t3296684876_StaticFields, ___AxesTable_12)); }
	inline Hashtable_t909839986 * get_AxesTable_12() const { return ___AxesTable_12; }
	inline Hashtable_t909839986 ** get_address_of_AxesTable_12() { return &___AxesTable_12; }
	inline void set_AxesTable_12(Hashtable_t909839986 * value)
	{
		___AxesTable_12 = value;
		Il2CppCodeGenWriteBarrier(&___AxesTable_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
