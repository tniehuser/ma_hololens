﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeStructs_MonoClass2595527713.h"

// Mono.RuntimeStructs/MonoClass
struct MonoClass_t2595527713;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeClassHandle
struct  RuntimeClassHandle_t2775506138 
{
public:
	// Mono.RuntimeStructs/MonoClass* Mono.RuntimeClassHandle::value
	MonoClass_t2595527713 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeClassHandle_t2775506138, ___value_0)); }
	inline MonoClass_t2595527713 * get_value_0() const { return ___value_0; }
	inline MonoClass_t2595527713 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(MonoClass_t2595527713 * value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeClassHandle
struct RuntimeClassHandle_t2775506138_marshaled_pinvoke
{
	MonoClass_t2595527713 * ___value_0;
};
// Native definition for COM marshalling of Mono.RuntimeClassHandle
struct RuntimeClassHandle_t2775506138_marshaled_com
{
	MonoClass_t2595527713 * ___value_0;
};
