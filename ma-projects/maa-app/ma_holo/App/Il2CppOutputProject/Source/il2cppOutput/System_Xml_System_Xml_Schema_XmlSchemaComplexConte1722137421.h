﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaContent3733871217.h"

// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct  XmlSchemaComplexContentRestriction_t1722137421  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentRestriction::particle
	XmlSchemaParticle_t3365045970 * ___particle_9;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentRestriction::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_10;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentRestriction::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentRestriction::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_12;

public:
	inline static int32_t get_offset_of_particle_9() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___particle_9)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_9() const { return ___particle_9; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_9() { return &___particle_9; }
	inline void set_particle_9(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_9 = value;
		Il2CppCodeGenWriteBarrier(&___particle_9, value);
	}

	inline static int32_t get_offset_of_attributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___attributes_10)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_10() const { return ___attributes_10; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_10() { return &___attributes_10; }
	inline void set_attributes_10(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_10, value);
	}

	inline static int32_t get_offset_of_anyAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___anyAttribute_11)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_11() const { return ___anyAttribute_11; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_11() { return &___anyAttribute_11; }
	inline void set_anyAttribute_11(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_11 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_11, value);
	}

	inline static int32_t get_offset_of_baseTypeName_12() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___baseTypeName_12)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_12() const { return ___baseTypeName_12; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_12() { return &___baseTypeName_12; }
	inline void set_baseTypeName_12(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_12 = value;
		Il2CppCodeGenWriteBarrier(&___baseTypeName_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
