﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.AssertSection
struct  AssertSection_t2202838995  : public ConfigurationElement_t1776195828
{
public:

public:
};

struct AssertSection_t2202838995_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.AssertSection::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.AssertSection::_propAssertUIEnabled
	ConfigurationProperty_t2048066811 * ____propAssertUIEnabled_16;
	// System.Configuration.ConfigurationProperty System.Diagnostics.AssertSection::_propLogFile
	ConfigurationProperty_t2048066811 * ____propLogFile_17;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(AssertSection_t2202838995_StaticFields, ____properties_15)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}

	inline static int32_t get_offset_of__propAssertUIEnabled_16() { return static_cast<int32_t>(offsetof(AssertSection_t2202838995_StaticFields, ____propAssertUIEnabled_16)); }
	inline ConfigurationProperty_t2048066811 * get__propAssertUIEnabled_16() const { return ____propAssertUIEnabled_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propAssertUIEnabled_16() { return &____propAssertUIEnabled_16; }
	inline void set__propAssertUIEnabled_16(ConfigurationProperty_t2048066811 * value)
	{
		____propAssertUIEnabled_16 = value;
		Il2CppCodeGenWriteBarrier(&____propAssertUIEnabled_16, value);
	}

	inline static int32_t get_offset_of__propLogFile_17() { return static_cast<int32_t>(offsetof(AssertSection_t2202838995_StaticFields, ____propLogFile_17)); }
	inline ConfigurationProperty_t2048066811 * get__propLogFile_17() const { return ____propLogFile_17; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propLogFile_17() { return &____propLogFile_17; }
	inline void set__propLogFile_17(ConfigurationProperty_t2048066811 * value)
	{
		____propLogFile_17 = value;
		Il2CppCodeGenWriteBarrier(&____propLogFile_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
