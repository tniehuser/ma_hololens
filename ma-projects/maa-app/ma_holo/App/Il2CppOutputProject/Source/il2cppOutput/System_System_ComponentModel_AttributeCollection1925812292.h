﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.AttributeCollection
struct AttributeCollection_t1925812292;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Attribute[]
struct AttributeU5BU5D_t4255796347;
// System.Object
struct Il2CppObject;
// System.ComponentModel.AttributeCollection/AttributeEntry[]
struct AttributeEntryU5BU5D_t2297172821;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AttributeCollection
struct  AttributeCollection_t1925812292  : public Il2CppObject
{
public:
	// System.Attribute[] System.ComponentModel.AttributeCollection::_attributes
	AttributeU5BU5D_t4255796347* ____attributes_2;
	// System.ComponentModel.AttributeCollection/AttributeEntry[] System.ComponentModel.AttributeCollection::_foundAttributeTypes
	AttributeEntryU5BU5D_t2297172821* ____foundAttributeTypes_4;
	// System.Int32 System.ComponentModel.AttributeCollection::_index
	int32_t ____index_5;

public:
	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292, ____attributes_2)); }
	inline AttributeU5BU5D_t4255796347* get__attributes_2() const { return ____attributes_2; }
	inline AttributeU5BU5D_t4255796347** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(AttributeU5BU5D_t4255796347* value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_2, value);
	}

	inline static int32_t get_offset_of__foundAttributeTypes_4() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292, ____foundAttributeTypes_4)); }
	inline AttributeEntryU5BU5D_t2297172821* get__foundAttributeTypes_4() const { return ____foundAttributeTypes_4; }
	inline AttributeEntryU5BU5D_t2297172821** get_address_of__foundAttributeTypes_4() { return &____foundAttributeTypes_4; }
	inline void set__foundAttributeTypes_4(AttributeEntryU5BU5D_t2297172821* value)
	{
		____foundAttributeTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&____foundAttributeTypes_4, value);
	}

	inline static int32_t get_offset_of__index_5() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292, ____index_5)); }
	inline int32_t get__index_5() const { return ____index_5; }
	inline int32_t* get_address_of__index_5() { return &____index_5; }
	inline void set__index_5(int32_t value)
	{
		____index_5 = value;
	}
};

struct AttributeCollection_t1925812292_StaticFields
{
public:
	// System.ComponentModel.AttributeCollection System.ComponentModel.AttributeCollection::Empty
	AttributeCollection_t1925812292 * ___Empty_0;
	// System.Collections.Hashtable System.ComponentModel.AttributeCollection::_defaultAttributes
	Hashtable_t909839986 * ____defaultAttributes_1;
	// System.Object System.ComponentModel.AttributeCollection::internalSyncObject
	Il2CppObject * ___internalSyncObject_3;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292_StaticFields, ___Empty_0)); }
	inline AttributeCollection_t1925812292 * get_Empty_0() const { return ___Empty_0; }
	inline AttributeCollection_t1925812292 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(AttributeCollection_t1925812292 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}

	inline static int32_t get_offset_of__defaultAttributes_1() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292_StaticFields, ____defaultAttributes_1)); }
	inline Hashtable_t909839986 * get__defaultAttributes_1() const { return ____defaultAttributes_1; }
	inline Hashtable_t909839986 ** get_address_of__defaultAttributes_1() { return &____defaultAttributes_1; }
	inline void set__defaultAttributes_1(Hashtable_t909839986 * value)
	{
		____defaultAttributes_1 = value;
		Il2CppCodeGenWriteBarrier(&____defaultAttributes_1, value);
	}

	inline static int32_t get_offset_of_internalSyncObject_3() { return static_cast<int32_t>(offsetof(AttributeCollection_t1925812292_StaticFields, ___internalSyncObject_3)); }
	inline Il2CppObject * get_internalSyncObject_3() const { return ___internalSyncObject_3; }
	inline Il2CppObject ** get_address_of_internalSyncObject_3() { return &___internalSyncObject_3; }
	inline void set_internalSyncObject_3(Il2CppObject * value)
	{
		___internalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___internalSyncObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
