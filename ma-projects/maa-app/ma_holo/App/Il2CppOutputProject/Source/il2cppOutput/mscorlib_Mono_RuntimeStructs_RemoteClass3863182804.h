﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_Mono_RuntimeStructs_MonoClass2595527713.h"

// Mono.RuntimeStructs/MonoClass
struct MonoClass_t2595527713;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeStructs/RemoteClass
struct  RemoteClass_t3863182804 
{
public:
	// System.IntPtr Mono.RuntimeStructs/RemoteClass::default_vtable
	IntPtr_t ___default_vtable_0;
	// System.IntPtr Mono.RuntimeStructs/RemoteClass::xdomain_vtable
	IntPtr_t ___xdomain_vtable_1;
	// Mono.RuntimeStructs/MonoClass* Mono.RuntimeStructs/RemoteClass::proxy_class
	MonoClass_t2595527713 * ___proxy_class_2;
	// System.IntPtr Mono.RuntimeStructs/RemoteClass::proxy_class_name
	IntPtr_t ___proxy_class_name_3;
	// System.UInt32 Mono.RuntimeStructs/RemoteClass::interface_count
	uint32_t ___interface_count_4;

public:
	inline static int32_t get_offset_of_default_vtable_0() { return static_cast<int32_t>(offsetof(RemoteClass_t3863182804, ___default_vtable_0)); }
	inline IntPtr_t get_default_vtable_0() const { return ___default_vtable_0; }
	inline IntPtr_t* get_address_of_default_vtable_0() { return &___default_vtable_0; }
	inline void set_default_vtable_0(IntPtr_t value)
	{
		___default_vtable_0 = value;
	}

	inline static int32_t get_offset_of_xdomain_vtable_1() { return static_cast<int32_t>(offsetof(RemoteClass_t3863182804, ___xdomain_vtable_1)); }
	inline IntPtr_t get_xdomain_vtable_1() const { return ___xdomain_vtable_1; }
	inline IntPtr_t* get_address_of_xdomain_vtable_1() { return &___xdomain_vtable_1; }
	inline void set_xdomain_vtable_1(IntPtr_t value)
	{
		___xdomain_vtable_1 = value;
	}

	inline static int32_t get_offset_of_proxy_class_2() { return static_cast<int32_t>(offsetof(RemoteClass_t3863182804, ___proxy_class_2)); }
	inline MonoClass_t2595527713 * get_proxy_class_2() const { return ___proxy_class_2; }
	inline MonoClass_t2595527713 ** get_address_of_proxy_class_2() { return &___proxy_class_2; }
	inline void set_proxy_class_2(MonoClass_t2595527713 * value)
	{
		___proxy_class_2 = value;
	}

	inline static int32_t get_offset_of_proxy_class_name_3() { return static_cast<int32_t>(offsetof(RemoteClass_t3863182804, ___proxy_class_name_3)); }
	inline IntPtr_t get_proxy_class_name_3() const { return ___proxy_class_name_3; }
	inline IntPtr_t* get_address_of_proxy_class_name_3() { return &___proxy_class_name_3; }
	inline void set_proxy_class_name_3(IntPtr_t value)
	{
		___proxy_class_name_3 = value;
	}

	inline static int32_t get_offset_of_interface_count_4() { return static_cast<int32_t>(offsetof(RemoteClass_t3863182804, ___interface_count_4)); }
	inline uint32_t get_interface_count_4() const { return ___interface_count_4; }
	inline uint32_t* get_address_of_interface_count_4() { return &___interface_count_4; }
	inline void set_interface_count_4(uint32_t value)
	{
		___interface_count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeStructs/RemoteClass
struct RemoteClass_t3863182804_marshaled_pinvoke
{
	intptr_t ___default_vtable_0;
	intptr_t ___xdomain_vtable_1;
	MonoClass_t2595527713 * ___proxy_class_2;
	intptr_t ___proxy_class_name_3;
	uint32_t ___interface_count_4;
};
// Native definition for COM marshalling of Mono.RuntimeStructs/RemoteClass
struct RemoteClass_t3863182804_marshaled_com
{
	intptr_t ___default_vtable_0;
	intptr_t ___xdomain_vtable_1;
	MonoClass_t2595527713 * ___proxy_class_2;
	intptr_t ___proxy_class_name_3;
	uint32_t ___interface_count_4;
};
