﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.BadImageFormatException
struct  BadImageFormatException_t4102491222  : public SystemException_t3877406272
{
public:
	// System.String System.BadImageFormatException::_fileName
	String_t* ____fileName_16;
	// System.String System.BadImageFormatException::_fusionLog
	String_t* ____fusionLog_17;

public:
	inline static int32_t get_offset_of__fileName_16() { return static_cast<int32_t>(offsetof(BadImageFormatException_t4102491222, ____fileName_16)); }
	inline String_t* get__fileName_16() const { return ____fileName_16; }
	inline String_t** get_address_of__fileName_16() { return &____fileName_16; }
	inline void set__fileName_16(String_t* value)
	{
		____fileName_16 = value;
		Il2CppCodeGenWriteBarrier(&____fileName_16, value);
	}

	inline static int32_t get_offset_of__fusionLog_17() { return static_cast<int32_t>(offsetof(BadImageFormatException_t4102491222, ____fusionLog_17)); }
	inline String_t* get__fusionLog_17() const { return ____fusionLog_17; }
	inline String_t** get_address_of__fusionLog_17() { return &____fusionLog_17; }
	inline void set__fusionLog_17(String_t* value)
	{
		____fusionLog_17 = value;
		Il2CppCodeGenWriteBarrier(&____fusionLog_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
