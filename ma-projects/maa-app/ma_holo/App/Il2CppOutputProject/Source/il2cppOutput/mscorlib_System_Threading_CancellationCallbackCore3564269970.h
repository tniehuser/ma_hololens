﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayFragment_1_t2654867320;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationCallbackCoreWorkArguments
struct  CancellationCallbackCoreWorkArguments_t3564269970 
{
public:
	// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo> System.Threading.CancellationCallbackCoreWorkArguments::m_currArrayFragment
	SparselyPopulatedArrayFragment_1_t2654867320 * ___m_currArrayFragment_0;
	// System.Int32 System.Threading.CancellationCallbackCoreWorkArguments::m_currArrayIndex
	int32_t ___m_currArrayIndex_1;

public:
	inline static int32_t get_offset_of_m_currArrayFragment_0() { return static_cast<int32_t>(offsetof(CancellationCallbackCoreWorkArguments_t3564269970, ___m_currArrayFragment_0)); }
	inline SparselyPopulatedArrayFragment_1_t2654867320 * get_m_currArrayFragment_0() const { return ___m_currArrayFragment_0; }
	inline SparselyPopulatedArrayFragment_1_t2654867320 ** get_address_of_m_currArrayFragment_0() { return &___m_currArrayFragment_0; }
	inline void set_m_currArrayFragment_0(SparselyPopulatedArrayFragment_1_t2654867320 * value)
	{
		___m_currArrayFragment_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_currArrayFragment_0, value);
	}

	inline static int32_t get_offset_of_m_currArrayIndex_1() { return static_cast<int32_t>(offsetof(CancellationCallbackCoreWorkArguments_t3564269970, ___m_currArrayIndex_1)); }
	inline int32_t get_m_currArrayIndex_1() const { return ___m_currArrayIndex_1; }
	inline int32_t* get_address_of_m_currArrayIndex_1() { return &___m_currArrayIndex_1; }
	inline void set_m_currArrayIndex_1(int32_t value)
	{
		___m_currArrayIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationCallbackCoreWorkArguments
struct CancellationCallbackCoreWorkArguments_t3564269970_marshaled_pinvoke
{
	SparselyPopulatedArrayFragment_1_t2654867320 * ___m_currArrayFragment_0;
	int32_t ___m_currArrayIndex_1;
};
// Native definition for COM marshalling of System.Threading.CancellationCallbackCoreWorkArguments
struct CancellationCallbackCoreWorkArguments_t3564269970_marshaled_com
{
	SparselyPopulatedArrayFragment_1_t2654867320 * ___m_currArrayFragment_0;
	int32_t ___m_currArrayIndex_1;
};
