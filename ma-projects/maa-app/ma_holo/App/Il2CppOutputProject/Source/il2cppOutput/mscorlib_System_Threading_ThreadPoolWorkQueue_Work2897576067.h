﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Threading_SpinLock2136334209.h"

// System.Threading.IThreadPoolWorkItem[]
struct IThreadPoolWorkItemU5BU5D_t2101682158;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueue/WorkStealingQueue
struct  WorkStealingQueue_t2897576067  : public Il2CppObject
{
public:
	// System.Threading.IThreadPoolWorkItem[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/WorkStealingQueue::m_array
	IThreadPoolWorkItemU5BU5D_t2101682158* ___m_array_0;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/WorkStealingQueue::m_mask
	int32_t ___m_mask_1;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/WorkStealingQueue::m_headIndex
	int32_t ___m_headIndex_2;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/WorkStealingQueue::m_tailIndex
	int32_t ___m_tailIndex_3;
	// System.Threading.SpinLock System.Threading.ThreadPoolWorkQueue/WorkStealingQueue::m_foreignLock
	SpinLock_t2136334209  ___m_foreignLock_4;

public:
	inline static int32_t get_offset_of_m_array_0() { return static_cast<int32_t>(offsetof(WorkStealingQueue_t2897576067, ___m_array_0)); }
	inline IThreadPoolWorkItemU5BU5D_t2101682158* get_m_array_0() const { return ___m_array_0; }
	inline IThreadPoolWorkItemU5BU5D_t2101682158** get_address_of_m_array_0() { return &___m_array_0; }
	inline void set_m_array_0(IThreadPoolWorkItemU5BU5D_t2101682158* value)
	{
		___m_array_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_array_0, value);
	}

	inline static int32_t get_offset_of_m_mask_1() { return static_cast<int32_t>(offsetof(WorkStealingQueue_t2897576067, ___m_mask_1)); }
	inline int32_t get_m_mask_1() const { return ___m_mask_1; }
	inline int32_t* get_address_of_m_mask_1() { return &___m_mask_1; }
	inline void set_m_mask_1(int32_t value)
	{
		___m_mask_1 = value;
	}

	inline static int32_t get_offset_of_m_headIndex_2() { return static_cast<int32_t>(offsetof(WorkStealingQueue_t2897576067, ___m_headIndex_2)); }
	inline int32_t get_m_headIndex_2() const { return ___m_headIndex_2; }
	inline int32_t* get_address_of_m_headIndex_2() { return &___m_headIndex_2; }
	inline void set_m_headIndex_2(int32_t value)
	{
		___m_headIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_tailIndex_3() { return static_cast<int32_t>(offsetof(WorkStealingQueue_t2897576067, ___m_tailIndex_3)); }
	inline int32_t get_m_tailIndex_3() const { return ___m_tailIndex_3; }
	inline int32_t* get_address_of_m_tailIndex_3() { return &___m_tailIndex_3; }
	inline void set_m_tailIndex_3(int32_t value)
	{
		___m_tailIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_foreignLock_4() { return static_cast<int32_t>(offsetof(WorkStealingQueue_t2897576067, ___m_foreignLock_4)); }
	inline SpinLock_t2136334209  get_m_foreignLock_4() const { return ___m_foreignLock_4; }
	inline SpinLock_t2136334209 * get_address_of_m_foreignLock_4() { return &___m_foreignLock_4; }
	inline void set_m_foreignLock_4(SpinLock_t2136334209  value)
	{
		___m_foreignLock_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
