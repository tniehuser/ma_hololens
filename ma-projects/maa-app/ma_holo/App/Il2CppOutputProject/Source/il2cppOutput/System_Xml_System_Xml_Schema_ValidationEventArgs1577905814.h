﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType3547578624.h"

// System.Xml.Schema.XmlSchemaException
struct XmlSchemaException_t4082200141;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventArgs
struct  ValidationEventArgs_t1577905814  : public EventArgs_t3289624707
{
public:
	// System.Xml.Schema.XmlSchemaException System.Xml.Schema.ValidationEventArgs::ex
	XmlSchemaException_t4082200141 * ___ex_1;
	// System.Xml.Schema.XmlSeverityType System.Xml.Schema.ValidationEventArgs::severity
	int32_t ___severity_2;

public:
	inline static int32_t get_offset_of_ex_1() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t1577905814, ___ex_1)); }
	inline XmlSchemaException_t4082200141 * get_ex_1() const { return ___ex_1; }
	inline XmlSchemaException_t4082200141 ** get_address_of_ex_1() { return &___ex_1; }
	inline void set_ex_1(XmlSchemaException_t4082200141 * value)
	{
		___ex_1 = value;
		Il2CppCodeGenWriteBarrier(&___ex_1, value);
	}

	inline static int32_t get_offset_of_severity_2() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t1577905814, ___severity_2)); }
	inline int32_t get_severity_2() const { return ___severity_2; }
	inline int32_t* get_address_of_severity_2() { return &___severity_2; }
	inline void set_severity_2(int32_t value)
	{
		___severity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
