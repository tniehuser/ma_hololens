﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_unsignedShor2975932210.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedByte
struct  Datatype_unsignedByte_t322972434  : public Datatype_unsignedShort_t2975932210
{
public:

public:
};

struct Datatype_unsignedByte_t322972434_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedByte::atomicValueType
	Type_t * ___atomicValueType_106;
	// System.Type System.Xml.Schema.Datatype_unsignedByte::listValueType
	Type_t * ___listValueType_107;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedByte::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_108;

public:
	inline static int32_t get_offset_of_atomicValueType_106() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___atomicValueType_106)); }
	inline Type_t * get_atomicValueType_106() const { return ___atomicValueType_106; }
	inline Type_t ** get_address_of_atomicValueType_106() { return &___atomicValueType_106; }
	inline void set_atomicValueType_106(Type_t * value)
	{
		___atomicValueType_106 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_106, value);
	}

	inline static int32_t get_offset_of_listValueType_107() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___listValueType_107)); }
	inline Type_t * get_listValueType_107() const { return ___listValueType_107; }
	inline Type_t ** get_address_of_listValueType_107() { return &___listValueType_107; }
	inline void set_listValueType_107(Type_t * value)
	{
		___listValueType_107 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_107, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_108() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___numeric10FacetsChecker_108)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_108() const { return ___numeric10FacetsChecker_108; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_108() { return &___numeric10FacetsChecker_108; }
	inline void set_numeric10FacetsChecker_108(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_108 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_108, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
