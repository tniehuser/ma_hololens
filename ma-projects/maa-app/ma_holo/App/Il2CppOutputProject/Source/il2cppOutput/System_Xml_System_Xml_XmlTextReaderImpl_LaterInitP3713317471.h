﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_InitInputTyp41671750.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Xml.XmlParserContext
struct XmlParserContext_t2728039553;
// System.IO.TextReader
struct TextReader_t1561828458;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/LaterInitParam
struct  LaterInitParam_t3713317471  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.XmlTextReaderImpl/LaterInitParam::useAsync
	bool ___useAsync_0;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/LaterInitParam::inputStream
	Stream_t3255436806 * ___inputStream_1;
	// System.Byte[] System.Xml.XmlTextReaderImpl/LaterInitParam::inputBytes
	ByteU5BU5D_t3397334013* ___inputBytes_2;
	// System.Int32 System.Xml.XmlTextReaderImpl/LaterInitParam::inputByteCount
	int32_t ___inputByteCount_3;
	// System.Uri System.Xml.XmlTextReaderImpl/LaterInitParam::inputbaseUri
	Uri_t19570940 * ___inputbaseUri_4;
	// System.String System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriStr
	String_t* ___inputUriStr_5;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriResolver
	XmlResolver_t2024571559 * ___inputUriResolver_6;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl/LaterInitParam::inputContext
	XmlParserContext_t2728039553 * ___inputContext_7;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/LaterInitParam::inputTextReader
	TextReader_t1561828458 * ___inputTextReader_8;
	// System.Xml.XmlTextReaderImpl/InitInputType System.Xml.XmlTextReaderImpl/LaterInitParam::initType
	int32_t ___initType_9;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_inputStream_1() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputStream_1)); }
	inline Stream_t3255436806 * get_inputStream_1() const { return ___inputStream_1; }
	inline Stream_t3255436806 ** get_address_of_inputStream_1() { return &___inputStream_1; }
	inline void set_inputStream_1(Stream_t3255436806 * value)
	{
		___inputStream_1 = value;
		Il2CppCodeGenWriteBarrier(&___inputStream_1, value);
	}

	inline static int32_t get_offset_of_inputBytes_2() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputBytes_2)); }
	inline ByteU5BU5D_t3397334013* get_inputBytes_2() const { return ___inputBytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_inputBytes_2() { return &___inputBytes_2; }
	inline void set_inputBytes_2(ByteU5BU5D_t3397334013* value)
	{
		___inputBytes_2 = value;
		Il2CppCodeGenWriteBarrier(&___inputBytes_2, value);
	}

	inline static int32_t get_offset_of_inputByteCount_3() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputByteCount_3)); }
	inline int32_t get_inputByteCount_3() const { return ___inputByteCount_3; }
	inline int32_t* get_address_of_inputByteCount_3() { return &___inputByteCount_3; }
	inline void set_inputByteCount_3(int32_t value)
	{
		___inputByteCount_3 = value;
	}

	inline static int32_t get_offset_of_inputbaseUri_4() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputbaseUri_4)); }
	inline Uri_t19570940 * get_inputbaseUri_4() const { return ___inputbaseUri_4; }
	inline Uri_t19570940 ** get_address_of_inputbaseUri_4() { return &___inputbaseUri_4; }
	inline void set_inputbaseUri_4(Uri_t19570940 * value)
	{
		___inputbaseUri_4 = value;
		Il2CppCodeGenWriteBarrier(&___inputbaseUri_4, value);
	}

	inline static int32_t get_offset_of_inputUriStr_5() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputUriStr_5)); }
	inline String_t* get_inputUriStr_5() const { return ___inputUriStr_5; }
	inline String_t** get_address_of_inputUriStr_5() { return &___inputUriStr_5; }
	inline void set_inputUriStr_5(String_t* value)
	{
		___inputUriStr_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputUriStr_5, value);
	}

	inline static int32_t get_offset_of_inputUriResolver_6() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputUriResolver_6)); }
	inline XmlResolver_t2024571559 * get_inputUriResolver_6() const { return ___inputUriResolver_6; }
	inline XmlResolver_t2024571559 ** get_address_of_inputUriResolver_6() { return &___inputUriResolver_6; }
	inline void set_inputUriResolver_6(XmlResolver_t2024571559 * value)
	{
		___inputUriResolver_6 = value;
		Il2CppCodeGenWriteBarrier(&___inputUriResolver_6, value);
	}

	inline static int32_t get_offset_of_inputContext_7() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputContext_7)); }
	inline XmlParserContext_t2728039553 * get_inputContext_7() const { return ___inputContext_7; }
	inline XmlParserContext_t2728039553 ** get_address_of_inputContext_7() { return &___inputContext_7; }
	inline void set_inputContext_7(XmlParserContext_t2728039553 * value)
	{
		___inputContext_7 = value;
		Il2CppCodeGenWriteBarrier(&___inputContext_7, value);
	}

	inline static int32_t get_offset_of_inputTextReader_8() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___inputTextReader_8)); }
	inline TextReader_t1561828458 * get_inputTextReader_8() const { return ___inputTextReader_8; }
	inline TextReader_t1561828458 ** get_address_of_inputTextReader_8() { return &___inputTextReader_8; }
	inline void set_inputTextReader_8(TextReader_t1561828458 * value)
	{
		___inputTextReader_8 = value;
		Il2CppCodeGenWriteBarrier(&___inputTextReader_8, value);
	}

	inline static int32_t get_offset_of_initType_9() { return static_cast<int32_t>(offsetof(LaterInitParam_t3713317471, ___initType_9)); }
	inline int32_t get_initType_9() const { return ___initType_9; }
	inline int32_t* get_address_of_initType_9() { return &___initType_9; }
	inline void set_initType_9(int32_t value)
	{
		___initType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
