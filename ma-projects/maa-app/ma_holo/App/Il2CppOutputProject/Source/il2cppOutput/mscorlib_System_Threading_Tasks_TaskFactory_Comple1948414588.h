﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task_1_gen963265114.h"

// System.Collections.Generic.IList`1<System.Threading.Tasks.Task>
struct IList_1_t2384176708;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskFactory/CompleteOnInvokePromise
struct  CompleteOnInvokePromise_t1948414588  : public Task_1_t963265114
{
public:
	// System.Collections.Generic.IList`1<System.Threading.Tasks.Task> System.Threading.Tasks.TaskFactory/CompleteOnInvokePromise::_tasks
	Il2CppObject* ____tasks_27;
	// System.Int32 System.Threading.Tasks.TaskFactory/CompleteOnInvokePromise::m_firstTaskAlreadyCompleted
	int32_t ___m_firstTaskAlreadyCompleted_28;

public:
	inline static int32_t get_offset_of__tasks_27() { return static_cast<int32_t>(offsetof(CompleteOnInvokePromise_t1948414588, ____tasks_27)); }
	inline Il2CppObject* get__tasks_27() const { return ____tasks_27; }
	inline Il2CppObject** get_address_of__tasks_27() { return &____tasks_27; }
	inline void set__tasks_27(Il2CppObject* value)
	{
		____tasks_27 = value;
		Il2CppCodeGenWriteBarrier(&____tasks_27, value);
	}

	inline static int32_t get_offset_of_m_firstTaskAlreadyCompleted_28() { return static_cast<int32_t>(offsetof(CompleteOnInvokePromise_t1948414588, ___m_firstTaskAlreadyCompleted_28)); }
	inline int32_t get_m_firstTaskAlreadyCompleted_28() const { return ___m_firstTaskAlreadyCompleted_28; }
	inline int32_t* get_address_of_m_firstTaskAlreadyCompleted_28() { return &___m_firstTaskAlreadyCompleted_28; }
	inline void set_m_firstTaskAlreadyCompleted_28(int32_t value)
	{
		___m_firstTaskAlreadyCompleted_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
