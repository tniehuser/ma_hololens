﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Serialization.Formatters.Binary.SerStack
struct SerStack_t3886188184;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit
struct  SerObjectInfoInit_t4094458531  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit::seenBeforeTable
	Hashtable_t909839986 * ___seenBeforeTable_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit::objectInfoIdCount
	int32_t ___objectInfoIdCount_1;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit::oiPool
	SerStack_t3886188184 * ___oiPool_2;

public:
	inline static int32_t get_offset_of_seenBeforeTable_0() { return static_cast<int32_t>(offsetof(SerObjectInfoInit_t4094458531, ___seenBeforeTable_0)); }
	inline Hashtable_t909839986 * get_seenBeforeTable_0() const { return ___seenBeforeTable_0; }
	inline Hashtable_t909839986 ** get_address_of_seenBeforeTable_0() { return &___seenBeforeTable_0; }
	inline void set_seenBeforeTable_0(Hashtable_t909839986 * value)
	{
		___seenBeforeTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___seenBeforeTable_0, value);
	}

	inline static int32_t get_offset_of_objectInfoIdCount_1() { return static_cast<int32_t>(offsetof(SerObjectInfoInit_t4094458531, ___objectInfoIdCount_1)); }
	inline int32_t get_objectInfoIdCount_1() const { return ___objectInfoIdCount_1; }
	inline int32_t* get_address_of_objectInfoIdCount_1() { return &___objectInfoIdCount_1; }
	inline void set_objectInfoIdCount_1(int32_t value)
	{
		___objectInfoIdCount_1 = value;
	}

	inline static int32_t get_offset_of_oiPool_2() { return static_cast<int32_t>(offsetof(SerObjectInfoInit_t4094458531, ___oiPool_2)); }
	inline SerStack_t3886188184 * get_oiPool_2() const { return ___oiPool_2; }
	inline SerStack_t3886188184 ** get_address_of_oiPool_2() { return &___oiPool_2; }
	inline void set_oiPool_2(SerStack_t3886188184 * value)
	{
		___oiPool_2 = value;
		Il2CppCodeGenWriteBarrier(&___oiPool_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
