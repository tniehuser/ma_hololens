﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeNAssembly
struct  TypeNAssembly_t2965287612  : public Il2CppObject
{
public:
	// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeNAssembly::type
	Type_t * ___type_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeNAssembly::assemblyName
	String_t* ___assemblyName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeNAssembly_t2965287612, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_assemblyName_1() { return static_cast<int32_t>(offsetof(TypeNAssembly_t2965287612, ___assemblyName_1)); }
	inline String_t* get_assemblyName_1() const { return ___assemblyName_1; }
	inline String_t** get_address_of_assemblyName_1() { return &___assemblyName_1; }
	inline void set_assemblyName_1(String_t* value)
	{
		___assemblyName_1 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
