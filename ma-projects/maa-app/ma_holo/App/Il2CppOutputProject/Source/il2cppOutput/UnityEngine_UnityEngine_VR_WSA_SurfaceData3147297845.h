﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_VR_WSA_SurfaceId4207907960.h"

// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.VR.WSA.WorldAnchor
struct WorldAnchor_t100028935;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.SurfaceData
struct  SurfaceData_t3147297845 
{
public:
	// UnityEngine.VR.WSA.SurfaceId UnityEngine.VR.WSA.SurfaceData::id
	SurfaceId_t4207907960  ___id_0;
	// UnityEngine.MeshFilter UnityEngine.VR.WSA.SurfaceData::outputMesh
	MeshFilter_t3026937449 * ___outputMesh_1;
	// UnityEngine.VR.WSA.WorldAnchor UnityEngine.VR.WSA.SurfaceData::outputAnchor
	WorldAnchor_t100028935 * ___outputAnchor_2;
	// UnityEngine.MeshCollider UnityEngine.VR.WSA.SurfaceData::outputCollider
	MeshCollider_t2718867283 * ___outputCollider_3;
	// System.Single UnityEngine.VR.WSA.SurfaceData::trianglesPerCubicMeter
	float ___trianglesPerCubicMeter_4;
	// System.Boolean UnityEngine.VR.WSA.SurfaceData::bakeCollider
	bool ___bakeCollider_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___id_0)); }
	inline SurfaceId_t4207907960  get_id_0() const { return ___id_0; }
	inline SurfaceId_t4207907960 * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(SurfaceId_t4207907960  value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_outputMesh_1() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___outputMesh_1)); }
	inline MeshFilter_t3026937449 * get_outputMesh_1() const { return ___outputMesh_1; }
	inline MeshFilter_t3026937449 ** get_address_of_outputMesh_1() { return &___outputMesh_1; }
	inline void set_outputMesh_1(MeshFilter_t3026937449 * value)
	{
		___outputMesh_1 = value;
		Il2CppCodeGenWriteBarrier(&___outputMesh_1, value);
	}

	inline static int32_t get_offset_of_outputAnchor_2() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___outputAnchor_2)); }
	inline WorldAnchor_t100028935 * get_outputAnchor_2() const { return ___outputAnchor_2; }
	inline WorldAnchor_t100028935 ** get_address_of_outputAnchor_2() { return &___outputAnchor_2; }
	inline void set_outputAnchor_2(WorldAnchor_t100028935 * value)
	{
		___outputAnchor_2 = value;
		Il2CppCodeGenWriteBarrier(&___outputAnchor_2, value);
	}

	inline static int32_t get_offset_of_outputCollider_3() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___outputCollider_3)); }
	inline MeshCollider_t2718867283 * get_outputCollider_3() const { return ___outputCollider_3; }
	inline MeshCollider_t2718867283 ** get_address_of_outputCollider_3() { return &___outputCollider_3; }
	inline void set_outputCollider_3(MeshCollider_t2718867283 * value)
	{
		___outputCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___outputCollider_3, value);
	}

	inline static int32_t get_offset_of_trianglesPerCubicMeter_4() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___trianglesPerCubicMeter_4)); }
	inline float get_trianglesPerCubicMeter_4() const { return ___trianglesPerCubicMeter_4; }
	inline float* get_address_of_trianglesPerCubicMeter_4() { return &___trianglesPerCubicMeter_4; }
	inline void set_trianglesPerCubicMeter_4(float value)
	{
		___trianglesPerCubicMeter_4 = value;
	}

	inline static int32_t get_offset_of_bakeCollider_5() { return static_cast<int32_t>(offsetof(SurfaceData_t3147297845, ___bakeCollider_5)); }
	inline bool get_bakeCollider_5() const { return ___bakeCollider_5; }
	inline bool* get_address_of_bakeCollider_5() { return &___bakeCollider_5; }
	inline void set_bakeCollider_5(bool value)
	{
		___bakeCollider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.VR.WSA.SurfaceData
struct SurfaceData_t3147297845_marshaled_pinvoke
{
	SurfaceId_t4207907960  ___id_0;
	MeshFilter_t3026937449 * ___outputMesh_1;
	WorldAnchor_t100028935 * ___outputAnchor_2;
	MeshCollider_t2718867283 * ___outputCollider_3;
	float ___trianglesPerCubicMeter_4;
	int32_t ___bakeCollider_5;
};
// Native definition for COM marshalling of UnityEngine.VR.WSA.SurfaceData
struct SurfaceData_t3147297845_marshaled_com
{
	SurfaceId_t4207907960  ___id_0;
	MeshFilter_t3026937449 * ___outputMesh_1;
	WorldAnchor_t100028935 * ___outputAnchor_2;
	MeshCollider_t2718867283 * ___outputCollider_3;
	float ___trianglesPerCubicMeter_4;
	int32_t ___bakeCollider_5;
};
