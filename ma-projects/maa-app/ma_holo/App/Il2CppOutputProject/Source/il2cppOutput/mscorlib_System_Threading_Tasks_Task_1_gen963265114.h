﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task1843236107.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.Task>
struct TaskFactory_1_t2533554214;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>>
struct Func_2_t1775964457;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>
struct  Task_1_t963265114  : public Task_t1843236107
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	Task_t1843236107 * ___m_result_24;

public:
	inline static int32_t get_offset_of_m_result_24() { return static_cast<int32_t>(offsetof(Task_1_t963265114, ___m_result_24)); }
	inline Task_t1843236107 * get_m_result_24() const { return ___m_result_24; }
	inline Task_t1843236107 ** get_address_of_m_result_24() { return &___m_result_24; }
	inline void set_m_result_24(Task_t1843236107 * value)
	{
		___m_result_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_result_24, value);
	}
};

struct Task_1_t963265114_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t2533554214 * ___s_Factory_25;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t1775964457 * ___TaskWhenAnyCast_26;

public:
	inline static int32_t get_offset_of_s_Factory_25() { return static_cast<int32_t>(offsetof(Task_1_t963265114_StaticFields, ___s_Factory_25)); }
	inline TaskFactory_1_t2533554214 * get_s_Factory_25() const { return ___s_Factory_25; }
	inline TaskFactory_1_t2533554214 ** get_address_of_s_Factory_25() { return &___s_Factory_25; }
	inline void set_s_Factory_25(TaskFactory_1_t2533554214 * value)
	{
		___s_Factory_25 = value;
		Il2CppCodeGenWriteBarrier(&___s_Factory_25, value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_26() { return static_cast<int32_t>(offsetof(Task_1_t963265114_StaticFields, ___TaskWhenAnyCast_26)); }
	inline Func_2_t1775964457 * get_TaskWhenAnyCast_26() const { return ___TaskWhenAnyCast_26; }
	inline Func_2_t1775964457 ** get_address_of_TaskWhenAnyCast_26() { return &___TaskWhenAnyCast_26; }
	inline void set_TaskWhenAnyCast_26(Func_2_t1775964457 * value)
	{
		___TaskWhenAnyCast_26 = value;
		Il2CppCodeGenWriteBarrier(&___TaskWhenAnyCast_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
