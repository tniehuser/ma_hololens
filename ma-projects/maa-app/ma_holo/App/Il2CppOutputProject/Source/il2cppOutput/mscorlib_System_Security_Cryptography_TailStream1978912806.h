﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.TailStream
struct  TailStream_t1978912806  : public Stream_t3255436806
{
public:
	// System.Byte[] System.Security.Cryptography.TailStream::_Buffer
	ByteU5BU5D_t3397334013* ____Buffer_8;
	// System.Int32 System.Security.Cryptography.TailStream::_BufferSize
	int32_t ____BufferSize_9;
	// System.Int32 System.Security.Cryptography.TailStream::_BufferIndex
	int32_t ____BufferIndex_10;
	// System.Boolean System.Security.Cryptography.TailStream::_BufferFull
	bool ____BufferFull_11;

public:
	inline static int32_t get_offset_of__Buffer_8() { return static_cast<int32_t>(offsetof(TailStream_t1978912806, ____Buffer_8)); }
	inline ByteU5BU5D_t3397334013* get__Buffer_8() const { return ____Buffer_8; }
	inline ByteU5BU5D_t3397334013** get_address_of__Buffer_8() { return &____Buffer_8; }
	inline void set__Buffer_8(ByteU5BU5D_t3397334013* value)
	{
		____Buffer_8 = value;
		Il2CppCodeGenWriteBarrier(&____Buffer_8, value);
	}

	inline static int32_t get_offset_of__BufferSize_9() { return static_cast<int32_t>(offsetof(TailStream_t1978912806, ____BufferSize_9)); }
	inline int32_t get__BufferSize_9() const { return ____BufferSize_9; }
	inline int32_t* get_address_of__BufferSize_9() { return &____BufferSize_9; }
	inline void set__BufferSize_9(int32_t value)
	{
		____BufferSize_9 = value;
	}

	inline static int32_t get_offset_of__BufferIndex_10() { return static_cast<int32_t>(offsetof(TailStream_t1978912806, ____BufferIndex_10)); }
	inline int32_t get__BufferIndex_10() const { return ____BufferIndex_10; }
	inline int32_t* get_address_of__BufferIndex_10() { return &____BufferIndex_10; }
	inline void set__BufferIndex_10(int32_t value)
	{
		____BufferIndex_10 = value;
	}

	inline static int32_t get_offset_of__BufferFull_11() { return static_cast<int32_t>(offsetof(TailStream_t1978912806, ____BufferFull_11)); }
	inline bool get__BufferFull_11() const { return ____BufferFull_11; }
	inline bool* get_address_of__BufferFull_11() { return &____BufferFull_11; }
	inline void set__BufferFull_11(bool value)
	{
		____BufferFull_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
