﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "System_Xml_System_Xml_Schema_XmlAtomicValue_Union69719246.h"

// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName
struct NamespacePrefixForQName_t3069403619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue
struct  XmlAtomicValue_t752869371  : public XPathItem_t3130801258
{
public:
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlAtomicValue::xmlType
	XmlSchemaType_t1795078578 * ___xmlType_0;
	// System.Object System.Xml.Schema.XmlAtomicValue::objVal
	Il2CppObject * ___objVal_1;
	// System.TypeCode System.Xml.Schema.XmlAtomicValue::clrType
	int32_t ___clrType_2;
	// System.Xml.Schema.XmlAtomicValue/Union System.Xml.Schema.XmlAtomicValue::unionVal
	Union_t69719246  ___unionVal_3;
	// System.Xml.Schema.XmlAtomicValue/NamespacePrefixForQName System.Xml.Schema.XmlAtomicValue::nsPrefix
	NamespacePrefixForQName_t3069403619 * ___nsPrefix_4;

public:
	inline static int32_t get_offset_of_xmlType_0() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___xmlType_0)); }
	inline XmlSchemaType_t1795078578 * get_xmlType_0() const { return ___xmlType_0; }
	inline XmlSchemaType_t1795078578 ** get_address_of_xmlType_0() { return &___xmlType_0; }
	inline void set_xmlType_0(XmlSchemaType_t1795078578 * value)
	{
		___xmlType_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlType_0, value);
	}

	inline static int32_t get_offset_of_objVal_1() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___objVal_1)); }
	inline Il2CppObject * get_objVal_1() const { return ___objVal_1; }
	inline Il2CppObject ** get_address_of_objVal_1() { return &___objVal_1; }
	inline void set_objVal_1(Il2CppObject * value)
	{
		___objVal_1 = value;
		Il2CppCodeGenWriteBarrier(&___objVal_1, value);
	}

	inline static int32_t get_offset_of_clrType_2() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___clrType_2)); }
	inline int32_t get_clrType_2() const { return ___clrType_2; }
	inline int32_t* get_address_of_clrType_2() { return &___clrType_2; }
	inline void set_clrType_2(int32_t value)
	{
		___clrType_2 = value;
	}

	inline static int32_t get_offset_of_unionVal_3() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___unionVal_3)); }
	inline Union_t69719246  get_unionVal_3() const { return ___unionVal_3; }
	inline Union_t69719246 * get_address_of_unionVal_3() { return &___unionVal_3; }
	inline void set_unionVal_3(Union_t69719246  value)
	{
		___unionVal_3 = value;
	}

	inline static int32_t get_offset_of_nsPrefix_4() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___nsPrefix_4)); }
	inline NamespacePrefixForQName_t3069403619 * get_nsPrefix_4() const { return ___nsPrefix_4; }
	inline NamespacePrefixForQName_t3069403619 ** get_address_of_nsPrefix_4() { return &___nsPrefix_4; }
	inline void set_nsPrefix_4(NamespacePrefixForQName_t3069403619 * value)
	{
		___nsPrefix_4 = value;
		Il2CppCodeGenWriteBarrier(&___nsPrefix_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
