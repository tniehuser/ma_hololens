﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_OperationCanceledException2897400967.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskCanceledException
struct  TaskCanceledException_t772081275  : public OperationCanceledException_t2897400967
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.TaskCanceledException::m_canceledTask
	Task_t1843236107 * ___m_canceledTask_17;

public:
	inline static int32_t get_offset_of_m_canceledTask_17() { return static_cast<int32_t>(offsetof(TaskCanceledException_t772081275, ___m_canceledTask_17)); }
	inline Task_t1843236107 * get_m_canceledTask_17() const { return ___m_canceledTask_17; }
	inline Task_t1843236107 ** get_address_of_m_canceledTask_17() { return &___m_canceledTask_17; }
	inline void set_m_canceledTask_17(Task_t1843236107 * value)
	{
		___m_canceledTask_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_canceledTask_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
