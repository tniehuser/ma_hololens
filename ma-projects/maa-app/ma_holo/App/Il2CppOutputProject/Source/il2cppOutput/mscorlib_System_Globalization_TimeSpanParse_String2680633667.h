﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TimeSpanParse/StringParser
struct  StringParser_t2680633667 
{
public:
	// System.String System.Globalization.TimeSpanParse/StringParser::str
	String_t* ___str_0;
	// System.Char System.Globalization.TimeSpanParse/StringParser::ch
	Il2CppChar ___ch_1;
	// System.Int32 System.Globalization.TimeSpanParse/StringParser::pos
	int32_t ___pos_2;
	// System.Int32 System.Globalization.TimeSpanParse/StringParser::len
	int32_t ___len_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(StringParser_t2680633667, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier(&___str_0, value);
	}

	inline static int32_t get_offset_of_ch_1() { return static_cast<int32_t>(offsetof(StringParser_t2680633667, ___ch_1)); }
	inline Il2CppChar get_ch_1() const { return ___ch_1; }
	inline Il2CppChar* get_address_of_ch_1() { return &___ch_1; }
	inline void set_ch_1(Il2CppChar value)
	{
		___ch_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(StringParser_t2680633667, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_len_3() { return static_cast<int32_t>(offsetof(StringParser_t2680633667, ___len_3)); }
	inline int32_t get_len_3() const { return ___len_3; }
	inline int32_t* get_address_of_len_3() { return &___len_3; }
	inline void set_len_3(int32_t value)
	{
		___len_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.TimeSpanParse/StringParser
struct StringParser_t2680633667_marshaled_pinvoke
{
	char* ___str_0;
	uint8_t ___ch_1;
	int32_t ___pos_2;
	int32_t ___len_3;
};
// Native definition for COM marshalling of System.Globalization.TimeSpanParse/StringParser
struct StringParser_t2680633667_marshaled_com
{
	Il2CppChar* ___str_0;
	uint8_t ___ch_1;
	int32_t ___pos_2;
	int32_t ___len_3;
};
