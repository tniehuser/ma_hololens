﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Runtime.ExceptionServices.ExceptionDispatchInfo
struct ExceptionDispatchInfo_t3341954301;
// System.Func`1<System.Threading.ManualResetEvent>
struct Func_1_t2880467339;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream/SynchronousAsyncResult
struct  SynchronousAsyncResult_t2050900148  : public Il2CppObject
{
public:
	// System.Object System.IO.Stream/SynchronousAsyncResult::_stateObject
	Il2CppObject * ____stateObject_0;
	// System.Boolean System.IO.Stream/SynchronousAsyncResult::_isWrite
	bool ____isWrite_1;
	// System.Threading.ManualResetEvent System.IO.Stream/SynchronousAsyncResult::_waitHandle
	ManualResetEvent_t926074657 * ____waitHandle_2;
	// System.Runtime.ExceptionServices.ExceptionDispatchInfo System.IO.Stream/SynchronousAsyncResult::_exceptionInfo
	ExceptionDispatchInfo_t3341954301 * ____exceptionInfo_3;
	// System.Boolean System.IO.Stream/SynchronousAsyncResult::_endXxxCalled
	bool ____endXxxCalled_4;
	// System.Int32 System.IO.Stream/SynchronousAsyncResult::_bytesRead
	int32_t ____bytesRead_5;

public:
	inline static int32_t get_offset_of__stateObject_0() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____stateObject_0)); }
	inline Il2CppObject * get__stateObject_0() const { return ____stateObject_0; }
	inline Il2CppObject ** get_address_of__stateObject_0() { return &____stateObject_0; }
	inline void set__stateObject_0(Il2CppObject * value)
	{
		____stateObject_0 = value;
		Il2CppCodeGenWriteBarrier(&____stateObject_0, value);
	}

	inline static int32_t get_offset_of__isWrite_1() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____isWrite_1)); }
	inline bool get__isWrite_1() const { return ____isWrite_1; }
	inline bool* get_address_of__isWrite_1() { return &____isWrite_1; }
	inline void set__isWrite_1(bool value)
	{
		____isWrite_1 = value;
	}

	inline static int32_t get_offset_of__waitHandle_2() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____waitHandle_2)); }
	inline ManualResetEvent_t926074657 * get__waitHandle_2() const { return ____waitHandle_2; }
	inline ManualResetEvent_t926074657 ** get_address_of__waitHandle_2() { return &____waitHandle_2; }
	inline void set__waitHandle_2(ManualResetEvent_t926074657 * value)
	{
		____waitHandle_2 = value;
		Il2CppCodeGenWriteBarrier(&____waitHandle_2, value);
	}

	inline static int32_t get_offset_of__exceptionInfo_3() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____exceptionInfo_3)); }
	inline ExceptionDispatchInfo_t3341954301 * get__exceptionInfo_3() const { return ____exceptionInfo_3; }
	inline ExceptionDispatchInfo_t3341954301 ** get_address_of__exceptionInfo_3() { return &____exceptionInfo_3; }
	inline void set__exceptionInfo_3(ExceptionDispatchInfo_t3341954301 * value)
	{
		____exceptionInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&____exceptionInfo_3, value);
	}

	inline static int32_t get_offset_of__endXxxCalled_4() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____endXxxCalled_4)); }
	inline bool get__endXxxCalled_4() const { return ____endXxxCalled_4; }
	inline bool* get_address_of__endXxxCalled_4() { return &____endXxxCalled_4; }
	inline void set__endXxxCalled_4(bool value)
	{
		____endXxxCalled_4 = value;
	}

	inline static int32_t get_offset_of__bytesRead_5() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148, ____bytesRead_5)); }
	inline int32_t get__bytesRead_5() const { return ____bytesRead_5; }
	inline int32_t* get_address_of__bytesRead_5() { return &____bytesRead_5; }
	inline void set__bytesRead_5(int32_t value)
	{
		____bytesRead_5 = value;
	}
};

struct SynchronousAsyncResult_t2050900148_StaticFields
{
public:
	// System.Func`1<System.Threading.ManualResetEvent> System.IO.Stream/SynchronousAsyncResult::<>f__am$cache0
	Func_1_t2880467339 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(SynchronousAsyncResult_t2050900148_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_1_t2880467339 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_1_t2880467339 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_1_t2880467339 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
