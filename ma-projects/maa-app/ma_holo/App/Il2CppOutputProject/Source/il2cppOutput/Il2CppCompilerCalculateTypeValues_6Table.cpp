﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_MemberHolder3720434074.h"
#include "mscorlib_System_Runtime_Serialization_ObjectIDGene3070747799.h"
#include "mscorlib_System_Runtime_Serialization_ObjectManage2645893724.h"
#include "mscorlib_System_Runtime_Serialization_ObjectHolder2992553423.h"
#include "mscorlib_System_Runtime_Serialization_FixupHolder2028025012.h"
#include "mscorlib_System_Runtime_Serialization_FixupHolderLi438393606.h"
#include "mscorlib_System_Runtime_Serialization_LongList2406893678.h"
#include "mscorlib_System_Runtime_Serialization_ObjectHolder1856843635.h"
#include "mscorlib_System_Runtime_Serialization_ObjectHolder1434580149.h"
#include "mscorlib_System_Runtime_Serialization_TypeLoadExcep427439951.h"
#include "mscorlib_System_Runtime_Serialization_SafeSerializ1088103422.h"
#include "mscorlib_System_Runtime_Serialization_SafeSerializ1975884510.h"
#include "mscorlib_System_Runtime_Serialization_OptionalField124318366.h"
#include "mscorlib_System_Runtime_Serialization_OnSerializin2011372116.h"
#include "mscorlib_System_Runtime_Serialization_OnSerialized3742956097.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializi484921187.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializ3172265744.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3985864818.h"
#include "mscorlib_System_Runtime_Serialization_SerializationE30413825.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio1398947579.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2472586292.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3485203212.h"
#include "mscorlib_System_Runtime_Serialization_Serialization589103770.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio4052555190.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon4264247603.h"
#include "mscorlib_System_Runtime_Serialization_ValueTypeFix1676793155.h"
#include "mscorlib_System_Runtime_Versioning_BinaryCompatibi1303671145.h"
#include "mscorlib_System_Runtime_Versioning_BinaryCompatibi3841228664.h"
#include "mscorlib_System_Runtime_Versioning_TargetFramework2072570862.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Security_DynamicSecurityMethodAttri500039186.h"
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecuri39244474.h"
#include "mscorlib_System_Security_UnverifiableCodeAttribute765455733.h"
#include "mscorlib_System_Security_AllowPartiallyTrustedCalle843922670.h"
#include "mscorlib_System_Security_SecurityCriticalAttribute2312050089.h"
#include "mscorlib_System_Security_SecuritySafeCriticalAttrib372031554.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith1682985260.h"
#include "mscorlib_System_Security_Cryptography_RSAEncryptio1083150860.h"
#include "mscorlib_System_Security_Cryptography_RSAEncryptio4275626723.h"
#include "mscorlib_System_Security_Cryptography_Aes2354947465.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKe3339648384.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi3580832979.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi4058014248.h"
#include "mscorlib_System_Security_Cryptography_CipherMode162592484.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode3032142640.h"
#include "mscorlib_System_Security_Cryptography_KeySizes3144736271.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3349726436.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi4184064416.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFl105264000.h"
#include "mscorlib_System_Security_Cryptography_CspParameters46065560.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937.h"
#include "mscorlib_System_Security_Cryptography_DES1353513560.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoServ933603253.h"
#include "mscorlib_System_Security_Cryptography_DSAParameter1872138834.h"
#include "mscorlib_System_Security_Cryptography_DSA903174880.h"
#include "mscorlib_System_Security_Cryptography_DSASignature2187578719.h"
#include "mscorlib_System_Security_Cryptography_DSASignature1065727064.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259.h"
#include "mscorlib_System_Security_Cryptography_HMAC130461695.h"
#include "mscorlib_System_Security_Cryptography_HMACMD52214610803.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160131410643.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA11958407246.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA2562622794722.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA3843026079344.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA512653426285.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlg1374150027.h"
#include "mscorlib_System_Security_Cryptography_MACTripleDES442445873.h"
#include "mscorlib_System_Security_Cryptography_TailStream1978912806.h"
#include "mscorlib_System_Security_Cryptography_MaskGeneratio649190171.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490.h"
#include "mscorlib_System_Security_Cryptography_PKCS1MaskGen3159630323.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2510243513.h"
#include "mscorlib_System_Security_Cryptography_RC23410342145.h"
#include "mscorlib_System_Security_Cryptography_RC2CryptoServ663781682.h"
#include "mscorlib_System_Security_Cryptography_Rijndael2154803531.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana1034060848.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana4113989319.h"
#include "mscorlib_System_Security_Cryptography_RijndaelManag135163252.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD1601732039966.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD160Man1613307429.h"
#include "mscorlib_System_Security_Cryptography_RSAParameter1462703416.h"
#include "mscorlib_System_Security_Cryptography_RSA3719518354.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967.h"
#include "mscorlib_System_Security_Cryptography_RSAOAEPKeyEx3166320901.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyE4167037264.h"
#include "mscorlib_System_Security_Cryptography_SHA13336793149.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed7268864.h"
#include "mscorlib_System_Security_Cryptography_SHA256582564463.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize604 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize605 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize606 = { sizeof (MemberHolder_t3720434074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable606[2] = 
{
	MemberHolder_t3720434074::get_offset_of_memberType_0(),
	MemberHolder_t3720434074::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize607 = { sizeof (ObjectIDGenerator_t3070747799), -1, sizeof(ObjectIDGenerator_t3070747799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable607[5] = 
{
	ObjectIDGenerator_t3070747799::get_offset_of_m_currentCount_0(),
	ObjectIDGenerator_t3070747799::get_offset_of_m_currentSize_1(),
	ObjectIDGenerator_t3070747799::get_offset_of_m_ids_2(),
	ObjectIDGenerator_t3070747799::get_offset_of_m_objs_3(),
	ObjectIDGenerator_t3070747799_StaticFields::get_offset_of_sizes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize608 = { sizeof (ObjectManager_t2645893724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable608[9] = 
{
	ObjectManager_t2645893724::get_offset_of_m_onDeserializationHandler_0(),
	ObjectManager_t2645893724::get_offset_of_m_onDeserializedHandler_1(),
	ObjectManager_t2645893724::get_offset_of_m_objects_2(),
	ObjectManager_t2645893724::get_offset_of_m_topObject_3(),
	ObjectManager_t2645893724::get_offset_of_m_specialFixupObjects_4(),
	ObjectManager_t2645893724::get_offset_of_m_fixupCount_5(),
	ObjectManager_t2645893724::get_offset_of_m_selector_6(),
	ObjectManager_t2645893724::get_offset_of_m_context_7(),
	ObjectManager_t2645893724::get_offset_of_m_isCrossAppDomain_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize609 = { sizeof (ObjectHolder_t2992553423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable609[14] = 
{
	ObjectHolder_t2992553423::get_offset_of_m_object_0(),
	ObjectHolder_t2992553423::get_offset_of_m_id_1(),
	ObjectHolder_t2992553423::get_offset_of_m_missingElementsRemaining_2(),
	ObjectHolder_t2992553423::get_offset_of_m_missingDecendents_3(),
	ObjectHolder_t2992553423::get_offset_of_m_serInfo_4(),
	ObjectHolder_t2992553423::get_offset_of_m_surrogate_5(),
	ObjectHolder_t2992553423::get_offset_of_m_missingElements_6(),
	ObjectHolder_t2992553423::get_offset_of_m_dependentObjects_7(),
	ObjectHolder_t2992553423::get_offset_of_m_next_8(),
	ObjectHolder_t2992553423::get_offset_of_m_flags_9(),
	ObjectHolder_t2992553423::get_offset_of_m_markForFixupWhenAvailable_10(),
	ObjectHolder_t2992553423::get_offset_of_m_valueFixup_11(),
	ObjectHolder_t2992553423::get_offset_of_m_typeLoad_12(),
	ObjectHolder_t2992553423::get_offset_of_m_reachable_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize610 = { sizeof (FixupHolder_t2028025012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable610[3] = 
{
	FixupHolder_t2028025012::get_offset_of_m_id_0(),
	FixupHolder_t2028025012::get_offset_of_m_fixupInfo_1(),
	FixupHolder_t2028025012::get_offset_of_m_fixupType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize611 = { sizeof (FixupHolderList_t438393606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable611[2] = 
{
	FixupHolderList_t438393606::get_offset_of_m_values_0(),
	FixupHolderList_t438393606::get_offset_of_m_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize612 = { sizeof (LongList_t2406893678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable612[4] = 
{
	LongList_t2406893678::get_offset_of_m_values_0(),
	LongList_t2406893678::get_offset_of_m_count_1(),
	LongList_t2406893678::get_offset_of_m_totalItems_2(),
	LongList_t2406893678::get_offset_of_m_currentItem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize613 = { sizeof (ObjectHolderList_t1856843635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable613[2] = 
{
	ObjectHolderList_t1856843635::get_offset_of_m_values_0(),
	ObjectHolderList_t1856843635::get_offset_of_m_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize614 = { sizeof (ObjectHolderListEnumerator_t1434580149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable614[4] = 
{
	ObjectHolderListEnumerator_t1434580149::get_offset_of_m_isFixupEnumerator_0(),
	ObjectHolderListEnumerator_t1434580149::get_offset_of_m_list_1(),
	ObjectHolderListEnumerator_t1434580149::get_offset_of_m_startingVersion_2(),
	ObjectHolderListEnumerator_t1434580149::get_offset_of_m_currPos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize615 = { sizeof (TypeLoadExceptionHolder_t427439951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable615[1] = 
{
	TypeLoadExceptionHolder_t427439951::get_offset_of_m_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize616 = { sizeof (SafeSerializationEventArgs_t1088103422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable616[2] = 
{
	SafeSerializationEventArgs_t1088103422::get_offset_of_m_streamingContext_1(),
	SafeSerializationEventArgs_t1088103422::get_offset_of_m_serializedStates_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize617 = { sizeof (SafeSerializationManager_t1975884510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable617[5] = 
{
	SafeSerializationManager_t1975884510::get_offset_of_m_serializedStates_0(),
	SafeSerializationManager_t1975884510::get_offset_of_m_savedSerializationInfo_1(),
	SafeSerializationManager_t1975884510::get_offset_of_m_realObject_2(),
	SafeSerializationManager_t1975884510::get_offset_of_m_realType_3(),
	SafeSerializationManager_t1975884510::get_offset_of_SerializeObjectState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize618 = { sizeof (OptionalFieldAttribute_t124318366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable618[1] = 
{
	OptionalFieldAttribute_t124318366::get_offset_of_versionAdded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize619 = { sizeof (OnSerializingAttribute_t2011372116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize620 = { sizeof (OnSerializedAttribute_t3742956097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize621 = { sizeof (OnDeserializingAttribute_t484921187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize622 = { sizeof (OnDeserializedAttribute_t3172265744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize623 = { sizeof (SerializationBinder_t3985864818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize624 = { sizeof (SerializationEvents_t30413825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable624[4] = 
{
	SerializationEvents_t30413825::get_offset_of_m_OnSerializingMethods_0(),
	SerializationEvents_t30413825::get_offset_of_m_OnSerializedMethods_1(),
	SerializationEvents_t30413825::get_offset_of_m_OnDeserializingMethods_2(),
	SerializationEvents_t30413825::get_offset_of_m_OnDeserializedMethods_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize625 = { sizeof (SerializationEventsCache_t1398947579), -1, sizeof(SerializationEventsCache_t1398947579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable625[1] = 
{
	SerializationEventsCache_t1398947579_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize626 = { sizeof (SerializationException_t753258759), -1, sizeof(SerializationException_t753258759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable626[1] = 
{
	SerializationException_t753258759_StaticFields::get_offset_of__nullMessage_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize627 = { sizeof (SerializationFieldInfo_t2472586292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable627[2] = 
{
	SerializationFieldInfo_t2472586292::get_offset_of_m_field_0(),
	SerializationFieldInfo_t2472586292::get_offset_of_m_serializationName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize628 = { sizeof (SerializationInfo_t228987430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable628[12] = 
{
	SerializationInfo_t228987430::get_offset_of_m_members_0(),
	SerializationInfo_t228987430::get_offset_of_m_data_1(),
	SerializationInfo_t228987430::get_offset_of_m_types_2(),
	SerializationInfo_t228987430::get_offset_of_m_nameToIndex_3(),
	SerializationInfo_t228987430::get_offset_of_m_currMember_4(),
	SerializationInfo_t228987430::get_offset_of_m_converter_5(),
	SerializationInfo_t228987430::get_offset_of_m_fullTypeName_6(),
	SerializationInfo_t228987430::get_offset_of_m_assemName_7(),
	SerializationInfo_t228987430::get_offset_of_objectType_8(),
	SerializationInfo_t228987430::get_offset_of_isFullTypeNameSetExplicit_9(),
	SerializationInfo_t228987430::get_offset_of_isAssemblyNameSetExplicit_10(),
	SerializationInfo_t228987430::get_offset_of_requireSameTokenInPartialTrust_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize629 = { sizeof (SerializationEntry_t3485203212)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable629[3] = 
{
	SerializationEntry_t3485203212::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t3485203212::get_offset_of_m_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t3485203212::get_offset_of_m_name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize630 = { sizeof (SerializationInfoEnumerator_t589103770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable630[6] = 
{
	SerializationInfoEnumerator_t589103770::get_offset_of_m_members_0(),
	SerializationInfoEnumerator_t589103770::get_offset_of_m_data_1(),
	SerializationInfoEnumerator_t589103770::get_offset_of_m_types_2(),
	SerializationInfoEnumerator_t589103770::get_offset_of_m_numItems_3(),
	SerializationInfoEnumerator_t589103770::get_offset_of_m_currItem_4(),
	SerializationInfoEnumerator_t589103770::get_offset_of_m_current_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize631 = { sizeof (SerializationObjectManager_t4052555190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable631[3] = 
{
	SerializationObjectManager_t4052555190::get_offset_of_m_objectSeenTable_0(),
	SerializationObjectManager_t4052555190::get_offset_of_m_onSerializedHandler_1(),
	SerializationObjectManager_t4052555190::get_offset_of_m_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize632 = { sizeof (StreamingContext_t1417235061)+ sizeof (Il2CppObject), sizeof(StreamingContext_t1417235061_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable632[2] = 
{
	StreamingContext_t1417235061::get_offset_of_m_additionalContext_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StreamingContext_t1417235061::get_offset_of_m_state_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize633 = { sizeof (StreamingContextStates_t4264247603)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable633[10] = 
{
	StreamingContextStates_t4264247603::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize634 = { sizeof (ValueTypeFixupInfo_t1676793155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable634[3] = 
{
	ValueTypeFixupInfo_t1676793155::get_offset_of_m_containerID_0(),
	ValueTypeFixupInfo_t1676793155::get_offset_of_m_parentField_1(),
	ValueTypeFixupInfo_t1676793155::get_offset_of_m_parentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize635 = { sizeof (BinaryCompatibility_t1303671145), -1, sizeof(BinaryCompatibility_t1303671145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable635[3] = 
{
	BinaryCompatibility_t1303671145_StaticFields::get_offset_of_s_AppWasBuiltForFramework_0(),
	BinaryCompatibility_t1303671145_StaticFields::get_offset_of_s_AppWasBuiltForVersion_1(),
	BinaryCompatibility_t1303671145_StaticFields::get_offset_of_s_map_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize636 = { sizeof (BinaryCompatibilityMap_t3841228664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable636[11] = 
{
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Phone_V7_1_0(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Phone_V8_0_1(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V4_5_2(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V4_5_1_3(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V4_5_2_4(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V4_5_3_5(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V4_5_4_6(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Desktop_V5_0_7(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Silverlight_V4_8(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Silverlight_V5_9(),
	BinaryCompatibilityMap_t3841228664::get_offset_of_TargetsAtLeast_Silverlight_V6_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize637 = { sizeof (TargetFrameworkId_t2072570862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable637[9] = 
{
	TargetFrameworkId_t2072570862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize638 = { sizeof (SByte_t454417549)+ sizeof (Il2CppObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable638[1] = 
{
	SByte_t454417549::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize639 = { sizeof (DynamicSecurityMethodAttribute_t500039186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize640 = { sizeof (SuppressUnmanagedCodeSecurityAttribute_t39244474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize641 = { sizeof (UnverifiableCodeAttribute_t765455733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize642 = { sizeof (AllowPartiallyTrustedCallersAttribute_t843922670), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize643 = { sizeof (SecurityCriticalAttribute_t2312050089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize644 = { sizeof (SecuritySafeCriticalAttribute_t372031554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize645 = { sizeof (HashAlgorithmName_t1682985260)+ sizeof (Il2CppObject), sizeof(HashAlgorithmName_t1682985260_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable645[1] = 
{
	HashAlgorithmName_t1682985260::get_offset_of__name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize646 = { sizeof (RSAEncryptionPadding_t1083150860), -1, sizeof(RSAEncryptionPadding_t1083150860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable646[7] = 
{
	RSAEncryptionPadding_t1083150860_StaticFields::get_offset_of_s_pkcs1_0(),
	RSAEncryptionPadding_t1083150860_StaticFields::get_offset_of_s_oaepSHA1_1(),
	RSAEncryptionPadding_t1083150860_StaticFields::get_offset_of_s_oaepSHA256_2(),
	RSAEncryptionPadding_t1083150860_StaticFields::get_offset_of_s_oaepSHA384_3(),
	RSAEncryptionPadding_t1083150860_StaticFields::get_offset_of_s_oaepSHA512_4(),
	RSAEncryptionPadding_t1083150860::get_offset_of__mode_5(),
	RSAEncryptionPadding_t1083150860::get_offset_of__oaepHashAlgorithm_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize647 = { sizeof (RSAEncryptionPaddingMode_t4275626723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable647[3] = 
{
	RSAEncryptionPaddingMode_t4275626723::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize648 = { sizeof (Aes_t2354947465), -1, sizeof(Aes_t2354947465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable648[2] = 
{
	Aes_t2354947465_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	Aes_t2354947465_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize649 = { sizeof (AsymmetricAlgorithm_t784058677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable649[2] = 
{
	AsymmetricAlgorithm_t784058677::get_offset_of_KeySizeValue_0(),
	AsymmetricAlgorithm_t784058677::get_offset_of_LegalKeySizesValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize650 = { sizeof (AsymmetricKeyExchangeFormatter_t3339648384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize651 = { sizeof (AsymmetricSignatureDeformatter_t3580832979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize652 = { sizeof (AsymmetricSignatureFormatter_t4058014248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize653 = { sizeof (CipherMode_t162592484)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable653[6] = 
{
	CipherMode_t162592484::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize654 = { sizeof (PaddingMode_t3032142640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable654[6] = 
{
	PaddingMode_t3032142640::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize655 = { sizeof (KeySizes_t3144736271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable655[3] = 
{
	KeySizes_t3144736271::get_offset_of_m_minSize_0(),
	KeySizes_t3144736271::get_offset_of_m_maxSize_1(),
	KeySizes_t3144736271::get_offset_of_m_skipSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize656 = { sizeof (CryptographicException_t3349726436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize657 = { sizeof (CryptographicUnexpectedOperationException_t4184064416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize658 = { sizeof (CspProviderFlags_t105264000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable658[10] = 
{
	CspProviderFlags_t105264000::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize659 = { sizeof (CspParameters_t46065560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable659[5] = 
{
	CspParameters_t46065560::get_offset_of_ProviderType_0(),
	CspParameters_t46065560::get_offset_of_ProviderName_1(),
	CspParameters_t46065560::get_offset_of_KeyContainerName_2(),
	CspParameters_t46065560::get_offset_of_KeyNumber_3(),
	CspParameters_t46065560::get_offset_of_m_flags_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize660 = { sizeof (CryptoStreamMode_t1337713182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable660[3] = 
{
	CryptoStreamMode_t1337713182::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize661 = { sizeof (CryptoStream_t3531341937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable661[12] = 
{
	CryptoStream_t3531341937::get_offset_of__stream_8(),
	CryptoStream_t3531341937::get_offset_of__Transform_9(),
	CryptoStream_t3531341937::get_offset_of__InputBuffer_10(),
	CryptoStream_t3531341937::get_offset_of__InputBufferIndex_11(),
	CryptoStream_t3531341937::get_offset_of__InputBlockSize_12(),
	CryptoStream_t3531341937::get_offset_of__OutputBuffer_13(),
	CryptoStream_t3531341937::get_offset_of__OutputBufferIndex_14(),
	CryptoStream_t3531341937::get_offset_of__OutputBlockSize_15(),
	CryptoStream_t3531341937::get_offset_of__transformMode_16(),
	CryptoStream_t3531341937::get_offset_of__canRead_17(),
	CryptoStream_t3531341937::get_offset_of__canWrite_18(),
	CryptoStream_t3531341937::get_offset_of__finalBlockTransformed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize662 = { sizeof (DES_t1353513560), -1, sizeof(DES_t1353513560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable662[2] = 
{
	DES_t1353513560_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	DES_t1353513560_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize663 = { sizeof (DESCryptoServiceProvider_t933603253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize664 = { sizeof (DSAParameters_t1872138834)+ sizeof (Il2CppObject), sizeof(DSAParameters_t1872138834_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable664[8] = 
{
	DSAParameters_t1872138834::get_offset_of_P_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Q_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_G_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Y_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_J_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_X_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Seed_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Counter_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize665 = { sizeof (DSA_t903174880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize666 = { sizeof (DSASignatureDeformatter_t2187578719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable666[2] = 
{
	DSASignatureDeformatter_t2187578719::get_offset_of__dsaKey_0(),
	DSASignatureDeformatter_t2187578719::get_offset_of__oid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize667 = { sizeof (DSASignatureFormatter_t1065727064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable667[2] = 
{
	DSASignatureFormatter_t1065727064::get_offset_of__dsaKey_0(),
	DSASignatureFormatter_t1065727064::get_offset_of__oid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize668 = { sizeof (HashAlgorithm_t2624936259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable668[4] = 
{
	HashAlgorithm_t2624936259::get_offset_of_HashSizeValue_0(),
	HashAlgorithm_t2624936259::get_offset_of_HashValue_1(),
	HashAlgorithm_t2624936259::get_offset_of_State_2(),
	HashAlgorithm_t2624936259::get_offset_of_m_bDisposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize669 = { sizeof (HMAC_t130461695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable669[7] = 
{
	HMAC_t130461695::get_offset_of_blockSizeValue_5(),
	HMAC_t130461695::get_offset_of_m_hashName_6(),
	HMAC_t130461695::get_offset_of_m_hash1_7(),
	HMAC_t130461695::get_offset_of_m_hash2_8(),
	HMAC_t130461695::get_offset_of_m_inner_9(),
	HMAC_t130461695::get_offset_of_m_outer_10(),
	HMAC_t130461695::get_offset_of_m_hashing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize670 = { sizeof (HMACMD5_t2214610803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize671 = { sizeof (HMACRIPEMD160_t131410643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize672 = { sizeof (HMACSHA1_t1958407246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize673 = { sizeof (HMACSHA256_t2622794722), -1, sizeof(HMACSHA256_t2622794722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable673[4] = 
{
	HMACSHA256_t2622794722_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	HMACSHA256_t2622794722_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
	HMACSHA256_t2622794722_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_14(),
	HMACSHA256_t2622794722_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize674 = { sizeof (HMACSHA384_t3026079344), -1, sizeof(HMACSHA384_t3026079344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable674[5] = 
{
	HMACSHA384_t3026079344::get_offset_of_m_useLegacyBlockSize_12(),
	HMACSHA384_t3026079344_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	HMACSHA384_t3026079344_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	HMACSHA384_t3026079344_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	HMACSHA384_t3026079344_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize675 = { sizeof (HMACSHA512_t653426285), -1, sizeof(HMACSHA512_t653426285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable675[5] = 
{
	HMACSHA512_t653426285::get_offset_of_m_useLegacyBlockSize_12(),
	HMACSHA512_t653426285_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	HMACSHA512_t653426285_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	HMACSHA512_t653426285_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	HMACSHA512_t653426285_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize677 = { sizeof (KeyedHashAlgorithm_t1374150027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable677[1] = 
{
	KeyedHashAlgorithm_t1374150027::get_offset_of_KeyValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize678 = { sizeof (MACTripleDES_t442445873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable678[5] = 
{
	MACTripleDES_t442445873::get_offset_of_m_encryptor_5(),
	MACTripleDES_t442445873::get_offset_of__cs_6(),
	MACTripleDES_t442445873::get_offset_of__ts_7(),
	MACTripleDES_t442445873::get_offset_of_m_bytesPerBlock_8(),
	MACTripleDES_t442445873::get_offset_of_des_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize679 = { sizeof (TailStream_t1978912806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable679[4] = 
{
	TailStream_t1978912806::get_offset_of__Buffer_8(),
	TailStream_t1978912806::get_offset_of__BufferSize_9(),
	TailStream_t1978912806::get_offset_of__BufferIndex_10(),
	TailStream_t1978912806::get_offset_of__BufferFull_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize680 = { sizeof (MaskGenerationMethod_t649190171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize681 = { sizeof (MD5_t1507972490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize682 = { sizeof (PKCS1MaskGenerationMethod_t3159630323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable682[1] = 
{
	PKCS1MaskGenerationMethod_t3159630323::get_offset_of_HashNameValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize683 = { sizeof (RandomNumberGenerator_t2510243513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize684 = { sizeof (RC2_t3410342145), -1, sizeof(RC2_t3410342145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable684[3] = 
{
	RC2_t3410342145::get_offset_of_EffectiveKeySizeValue_9(),
	RC2_t3410342145_StaticFields::get_offset_of_s_legalBlockSizes_10(),
	RC2_t3410342145_StaticFields::get_offset_of_s_legalKeySizes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize685 = { sizeof (RC2CryptoServiceProvider_t663781682), -1, sizeof(RC2CryptoServiceProvider_t663781682_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable685[2] = 
{
	RC2CryptoServiceProvider_t663781682::get_offset_of_m_use40bitSalt_12(),
	RC2CryptoServiceProvider_t663781682_StaticFields::get_offset_of_s_legalKeySizes_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize686 = { sizeof (Rijndael_t2154803531), -1, sizeof(Rijndael_t2154803531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable686[2] = 
{
	Rijndael_t2154803531_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	Rijndael_t2154803531_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize687 = { sizeof (RijndaelManaged_t1034060848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize688 = { sizeof (RijndaelManagedTransformMode_t4113989319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable688[3] = 
{
	RijndaelManagedTransformMode_t4113989319::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize689 = { sizeof (RijndaelManagedTransform_t135163252), -1, sizeof(RijndaelManagedTransform_t135163252_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable689[24] = 
{
	RijndaelManagedTransform_t135163252::get_offset_of_m_cipherMode_0(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_paddingValue_1(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_transformMode_2(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_blockSizeBits_3(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_blockSizeBytes_4(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_inputBlockSize_5(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_outputBlockSize_6(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_encryptKeyExpansion_7(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_decryptKeyExpansion_8(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_Nr_9(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_Nb_10(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_Nk_11(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_encryptindex_12(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_decryptindex_13(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_IV_14(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_lastBlockBuffer_15(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_depadBuffer_16(),
	RijndaelManagedTransform_t135163252::get_offset_of_m_shiftRegister_17(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_Sbox_18(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_Rcon_19(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_T_20(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_TF_21(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_iT_22(),
	RijndaelManagedTransform_t135163252_StaticFields::get_offset_of_s_iTF_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize690 = { sizeof (RIPEMD160_t1732039966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize691 = { sizeof (RIPEMD160Managed_t1613307429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable691[4] = 
{
	RIPEMD160Managed_t1613307429::get_offset_of__buffer_4(),
	RIPEMD160Managed_t1613307429::get_offset_of__count_5(),
	RIPEMD160Managed_t1613307429::get_offset_of__stateMD160_6(),
	RIPEMD160Managed_t1613307429::get_offset_of__blockDWords_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize692 = { sizeof (RSAParameters_t1462703416)+ sizeof (Il2CppObject), sizeof(RSAParameters_t1462703416_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable692[8] = 
{
	RSAParameters_t1462703416::get_offset_of_Exponent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_Modulus_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_P_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_Q_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_DP_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_DQ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_InverseQ_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_D_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize693 = { sizeof (RSA_t3719518354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize694 = { sizeof (RSACryptoServiceProvider_t4229286967), -1, sizeof(RSACryptoServiceProvider_t4229286967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable694[7] = 
{
	RSACryptoServiceProvider_t4229286967_StaticFields::get_offset_of_s_UseMachineKeyStore_2(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_store_3(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_persistKey_4(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_persisted_5(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_privateKeyExportable_6(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_m_disposed_7(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_rsa_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize695 = { sizeof (RSAOAEPKeyExchangeFormatter_t3166320901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable695[2] = 
{
	RSAOAEPKeyExchangeFormatter_t3166320901::get_offset_of__rsaKey_0(),
	RSAOAEPKeyExchangeFormatter_t3166320901::get_offset_of__rsaOverridesEncrypt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize696 = { sizeof (RSAPKCS1KeyExchangeFormatter_t4167037264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable696[3] = 
{
	RSAPKCS1KeyExchangeFormatter_t4167037264::get_offset_of_RngValue_0(),
	RSAPKCS1KeyExchangeFormatter_t4167037264::get_offset_of__rsaKey_1(),
	RSAPKCS1KeyExchangeFormatter_t4167037264::get_offset_of__rsaOverridesEncrypt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize697 = { sizeof (SHA1_t3336793149), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize698 = { sizeof (SHA1Managed_t7268864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable698[4] = 
{
	SHA1Managed_t7268864::get_offset_of__buffer_4(),
	SHA1Managed_t7268864::get_offset_of__count_5(),
	SHA1Managed_t7268864::get_offset_of__stateSHA1_6(),
	SHA1Managed_t7268864::get_offset_of__expandedBuffer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize699 = { sizeof (SHA256_t582564463), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
