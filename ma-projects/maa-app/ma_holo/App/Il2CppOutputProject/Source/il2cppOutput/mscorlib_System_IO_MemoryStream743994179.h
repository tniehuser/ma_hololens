﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t1191906455;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t743994179  : public Stream_t3255436806
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_t3397334013* ____buffer_8;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_9;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_10;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_11;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_12;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_13;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_14;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_15;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_16;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t1191906455 * ____lastReadTask_17;

public:
	inline static int32_t get_offset_of__buffer_8() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____buffer_8)); }
	inline ByteU5BU5D_t3397334013* get__buffer_8() const { return ____buffer_8; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_8() { return &____buffer_8; }
	inline void set__buffer_8(ByteU5BU5D_t3397334013* value)
	{
		____buffer_8 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_8, value);
	}

	inline static int32_t get_offset_of__origin_9() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____origin_9)); }
	inline int32_t get__origin_9() const { return ____origin_9; }
	inline int32_t* get_address_of__origin_9() { return &____origin_9; }
	inline void set__origin_9(int32_t value)
	{
		____origin_9 = value;
	}

	inline static int32_t get_offset_of__position_10() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____position_10)); }
	inline int32_t get__position_10() const { return ____position_10; }
	inline int32_t* get_address_of__position_10() { return &____position_10; }
	inline void set__position_10(int32_t value)
	{
		____position_10 = value;
	}

	inline static int32_t get_offset_of__length_11() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____length_11)); }
	inline int32_t get__length_11() const { return ____length_11; }
	inline int32_t* get_address_of__length_11() { return &____length_11; }
	inline void set__length_11(int32_t value)
	{
		____length_11 = value;
	}

	inline static int32_t get_offset_of__capacity_12() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____capacity_12)); }
	inline int32_t get__capacity_12() const { return ____capacity_12; }
	inline int32_t* get_address_of__capacity_12() { return &____capacity_12; }
	inline void set__capacity_12(int32_t value)
	{
		____capacity_12 = value;
	}

	inline static int32_t get_offset_of__expandable_13() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____expandable_13)); }
	inline bool get__expandable_13() const { return ____expandable_13; }
	inline bool* get_address_of__expandable_13() { return &____expandable_13; }
	inline void set__expandable_13(bool value)
	{
		____expandable_13 = value;
	}

	inline static int32_t get_offset_of__writable_14() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____writable_14)); }
	inline bool get__writable_14() const { return ____writable_14; }
	inline bool* get_address_of__writable_14() { return &____writable_14; }
	inline void set__writable_14(bool value)
	{
		____writable_14 = value;
	}

	inline static int32_t get_offset_of__exposable_15() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____exposable_15)); }
	inline bool get__exposable_15() const { return ____exposable_15; }
	inline bool* get_address_of__exposable_15() { return &____exposable_15; }
	inline void set__exposable_15(bool value)
	{
		____exposable_15 = value;
	}

	inline static int32_t get_offset_of__isOpen_16() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____isOpen_16)); }
	inline bool get__isOpen_16() const { return ____isOpen_16; }
	inline bool* get_address_of__isOpen_16() { return &____isOpen_16; }
	inline void set__isOpen_16(bool value)
	{
		____isOpen_16 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_17() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____lastReadTask_17)); }
	inline Task_1_t1191906455 * get__lastReadTask_17() const { return ____lastReadTask_17; }
	inline Task_1_t1191906455 ** get_address_of__lastReadTask_17() { return &____lastReadTask_17; }
	inline void set__lastReadTask_17(Task_1_t1191906455 * value)
	{
		____lastReadTask_17 = value;
		Il2CppCodeGenWriteBarrier(&____lastReadTask_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
