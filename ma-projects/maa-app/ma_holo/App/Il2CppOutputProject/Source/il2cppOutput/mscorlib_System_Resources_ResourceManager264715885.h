﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Resources.ResourceManager/CultureNameResourceSetPair
struct CultureNameResourceSetPair_t736438917;
// System.Reflection.RuntimeAssembly
struct RuntimeAssembly_t1913607566;
// System.Resources.IResourceGroveler
struct IResourceGroveler_t361643957;
// System.Type
struct Type_t;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceManager
struct  ResourceManager_t264715885  : public Il2CppObject
{
public:
	// System.Resources.ResourceManager/CultureNameResourceSetPair System.Resources.ResourceManager::_lastUsedResourceCache
	CultureNameResourceSetPair_t736438917 * ____lastUsedResourceCache_0;
	// System.Reflection.RuntimeAssembly System.Resources.ResourceManager::m_callingAssembly
	RuntimeAssembly_t1913607566 * ___m_callingAssembly_1;
	// System.Resources.IResourceGroveler System.Resources.ResourceManager::resourceGroveler
	Il2CppObject * ___resourceGroveler_2;

public:
	inline static int32_t get_offset_of__lastUsedResourceCache_0() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885, ____lastUsedResourceCache_0)); }
	inline CultureNameResourceSetPair_t736438917 * get__lastUsedResourceCache_0() const { return ____lastUsedResourceCache_0; }
	inline CultureNameResourceSetPair_t736438917 ** get_address_of__lastUsedResourceCache_0() { return &____lastUsedResourceCache_0; }
	inline void set__lastUsedResourceCache_0(CultureNameResourceSetPair_t736438917 * value)
	{
		____lastUsedResourceCache_0 = value;
		Il2CppCodeGenWriteBarrier(&____lastUsedResourceCache_0, value);
	}

	inline static int32_t get_offset_of_m_callingAssembly_1() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885, ___m_callingAssembly_1)); }
	inline RuntimeAssembly_t1913607566 * get_m_callingAssembly_1() const { return ___m_callingAssembly_1; }
	inline RuntimeAssembly_t1913607566 ** get_address_of_m_callingAssembly_1() { return &___m_callingAssembly_1; }
	inline void set_m_callingAssembly_1(RuntimeAssembly_t1913607566 * value)
	{
		___m_callingAssembly_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_callingAssembly_1, value);
	}

	inline static int32_t get_offset_of_resourceGroveler_2() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885, ___resourceGroveler_2)); }
	inline Il2CppObject * get_resourceGroveler_2() const { return ___resourceGroveler_2; }
	inline Il2CppObject ** get_address_of_resourceGroveler_2() { return &___resourceGroveler_2; }
	inline void set_resourceGroveler_2(Il2CppObject * value)
	{
		___resourceGroveler_2 = value;
		Il2CppCodeGenWriteBarrier(&___resourceGroveler_2, value);
	}
};

struct ResourceManager_t264715885_StaticFields
{
public:
	// System.Int32 System.Resources.ResourceManager::MagicNumber
	int32_t ___MagicNumber_3;
	// System.Int32 System.Resources.ResourceManager::HeaderVersionNumber
	int32_t ___HeaderVersionNumber_4;
	// System.Type System.Resources.ResourceManager::_minResourceSet
	Type_t * ____minResourceSet_5;
	// System.String System.Resources.ResourceManager::ResReaderTypeName
	String_t* ___ResReaderTypeName_6;
	// System.String System.Resources.ResourceManager::ResSetTypeName
	String_t* ___ResSetTypeName_7;
	// System.String System.Resources.ResourceManager::MscorlibName
	String_t* ___MscorlibName_8;
	// System.Int32 System.Resources.ResourceManager::DEBUG
	int32_t ___DEBUG_9;

public:
	inline static int32_t get_offset_of_MagicNumber_3() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___MagicNumber_3)); }
	inline int32_t get_MagicNumber_3() const { return ___MagicNumber_3; }
	inline int32_t* get_address_of_MagicNumber_3() { return &___MagicNumber_3; }
	inline void set_MagicNumber_3(int32_t value)
	{
		___MagicNumber_3 = value;
	}

	inline static int32_t get_offset_of_HeaderVersionNumber_4() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___HeaderVersionNumber_4)); }
	inline int32_t get_HeaderVersionNumber_4() const { return ___HeaderVersionNumber_4; }
	inline int32_t* get_address_of_HeaderVersionNumber_4() { return &___HeaderVersionNumber_4; }
	inline void set_HeaderVersionNumber_4(int32_t value)
	{
		___HeaderVersionNumber_4 = value;
	}

	inline static int32_t get_offset_of__minResourceSet_5() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ____minResourceSet_5)); }
	inline Type_t * get__minResourceSet_5() const { return ____minResourceSet_5; }
	inline Type_t ** get_address_of__minResourceSet_5() { return &____minResourceSet_5; }
	inline void set__minResourceSet_5(Type_t * value)
	{
		____minResourceSet_5 = value;
		Il2CppCodeGenWriteBarrier(&____minResourceSet_5, value);
	}

	inline static int32_t get_offset_of_ResReaderTypeName_6() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___ResReaderTypeName_6)); }
	inline String_t* get_ResReaderTypeName_6() const { return ___ResReaderTypeName_6; }
	inline String_t** get_address_of_ResReaderTypeName_6() { return &___ResReaderTypeName_6; }
	inline void set_ResReaderTypeName_6(String_t* value)
	{
		___ResReaderTypeName_6 = value;
		Il2CppCodeGenWriteBarrier(&___ResReaderTypeName_6, value);
	}

	inline static int32_t get_offset_of_ResSetTypeName_7() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___ResSetTypeName_7)); }
	inline String_t* get_ResSetTypeName_7() const { return ___ResSetTypeName_7; }
	inline String_t** get_address_of_ResSetTypeName_7() { return &___ResSetTypeName_7; }
	inline void set_ResSetTypeName_7(String_t* value)
	{
		___ResSetTypeName_7 = value;
		Il2CppCodeGenWriteBarrier(&___ResSetTypeName_7, value);
	}

	inline static int32_t get_offset_of_MscorlibName_8() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___MscorlibName_8)); }
	inline String_t* get_MscorlibName_8() const { return ___MscorlibName_8; }
	inline String_t** get_address_of_MscorlibName_8() { return &___MscorlibName_8; }
	inline void set_MscorlibName_8(String_t* value)
	{
		___MscorlibName_8 = value;
		Il2CppCodeGenWriteBarrier(&___MscorlibName_8, value);
	}

	inline static int32_t get_offset_of_DEBUG_9() { return static_cast<int32_t>(offsetof(ResourceManager_t264715885_StaticFields, ___DEBUG_9)); }
	inline int32_t get_DEBUG_9() const { return ___DEBUG_9; }
	inline int32_t* get_address_of_DEBUG_9() { return &___DEBUG_9; }
	inline void set_DEBUG_9(int32_t value)
	{
		___DEBUG_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
