﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_Net_Dns_DnsPacket413343383.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord>
struct ReadOnlyCollection_1_t3129240104;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion>
struct ReadOnlyCollection_1_t3276628651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResponse
struct  DnsResponse_t3880332876  : public DnsPacket_t413343383
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::question
	ReadOnlyCollection_1_t3276628651 * ___question_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::answer
	ReadOnlyCollection_1_t3129240104 * ___answer_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::authority
	ReadOnlyCollection_1_t3129240104 * ___authority_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::additional
	ReadOnlyCollection_1_t3129240104 * ___additional_8;
	// System.Int32 Mono.Net.Dns.DnsResponse::offset
	int32_t ___offset_9;

public:
	inline static int32_t get_offset_of_question_5() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___question_5)); }
	inline ReadOnlyCollection_1_t3276628651 * get_question_5() const { return ___question_5; }
	inline ReadOnlyCollection_1_t3276628651 ** get_address_of_question_5() { return &___question_5; }
	inline void set_question_5(ReadOnlyCollection_1_t3276628651 * value)
	{
		___question_5 = value;
		Il2CppCodeGenWriteBarrier(&___question_5, value);
	}

	inline static int32_t get_offset_of_answer_6() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___answer_6)); }
	inline ReadOnlyCollection_1_t3129240104 * get_answer_6() const { return ___answer_6; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_answer_6() { return &___answer_6; }
	inline void set_answer_6(ReadOnlyCollection_1_t3129240104 * value)
	{
		___answer_6 = value;
		Il2CppCodeGenWriteBarrier(&___answer_6, value);
	}

	inline static int32_t get_offset_of_authority_7() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___authority_7)); }
	inline ReadOnlyCollection_1_t3129240104 * get_authority_7() const { return ___authority_7; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_authority_7() { return &___authority_7; }
	inline void set_authority_7(ReadOnlyCollection_1_t3129240104 * value)
	{
		___authority_7 = value;
		Il2CppCodeGenWriteBarrier(&___authority_7, value);
	}

	inline static int32_t get_offset_of_additional_8() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___additional_8)); }
	inline ReadOnlyCollection_1_t3129240104 * get_additional_8() const { return ___additional_8; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_additional_8() { return &___additional_8; }
	inline void set_additional_8(ReadOnlyCollection_1_t3129240104 * value)
	{
		___additional_8 = value;
		Il2CppCodeGenWriteBarrier(&___additional_8, value);
	}

	inline static int32_t get_offset_of_offset_9() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___offset_9)); }
	inline int32_t get_offset_9() const { return ___offset_9; }
	inline int32_t* get_address_of_offset_9() { return &___offset_9; }
	inline void set_offset_9(int32_t value)
	{
		___offset_9 = value;
	}
};

struct DnsResponse_t3880332876_StaticFields
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::EmptyRR
	ReadOnlyCollection_1_t3129240104 * ___EmptyRR_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::EmptyQS
	ReadOnlyCollection_1_t3276628651 * ___EmptyQS_4;

public:
	inline static int32_t get_offset_of_EmptyRR_3() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876_StaticFields, ___EmptyRR_3)); }
	inline ReadOnlyCollection_1_t3129240104 * get_EmptyRR_3() const { return ___EmptyRR_3; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_EmptyRR_3() { return &___EmptyRR_3; }
	inline void set_EmptyRR_3(ReadOnlyCollection_1_t3129240104 * value)
	{
		___EmptyRR_3 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyRR_3, value);
	}

	inline static int32_t get_offset_of_EmptyQS_4() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876_StaticFields, ___EmptyQS_4)); }
	inline ReadOnlyCollection_1_t3276628651 * get_EmptyQS_4() const { return ___EmptyQS_4; }
	inline ReadOnlyCollection_1_t3276628651 ** get_address_of_EmptyQS_4() { return &___EmptyQS_4; }
	inline void set_EmptyQS_4(ReadOnlyCollection_1_t3276628651 * value)
	{
		___EmptyQS_4 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyQS_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
