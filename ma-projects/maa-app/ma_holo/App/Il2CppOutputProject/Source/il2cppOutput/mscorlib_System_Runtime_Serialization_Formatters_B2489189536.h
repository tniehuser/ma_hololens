﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache
struct SerObjectInfoCache_t4068137215;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t1282780357;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit
struct SerObjectInfoInit_t4094458531;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo
struct  WriteObjectInfo_t2489189536  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::objectInfoId
	int32_t ___objectInfoId_0;
	// System.Object System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::obj
	Il2CppObject * ___obj_1;
	// System.Type System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::objectType
	Type_t * ___objectType_2;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::isSi
	bool ___isSi_3;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::isNamed
	bool ___isNamed_4;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::isTyped
	bool ___isTyped_5;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::isArray
	bool ___isArray_6;
	// System.Runtime.Serialization.SerializationInfo System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::si
	SerializationInfo_t228987430 * ___si_7;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::cache
	SerObjectInfoCache_t4068137215 * ___cache_8;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::memberData
	ObjectU5BU5D_t3614634134* ___memberData_9;
	// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::serializationSurrogate
	Il2CppObject * ___serializationSurrogate_10;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::context
	StreamingContext_t1417235061  ___context_11;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::serObjectInfoInit
	SerObjectInfoInit_t4094458531 * ___serObjectInfoInit_12;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::objectId
	int64_t ___objectId_13;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::assemId
	int64_t ___assemId_14;
	// System.String System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::binderTypeName
	String_t* ___binderTypeName_15;
	// System.String System.Runtime.Serialization.Formatters.Binary.WriteObjectInfo::binderAssemblyString
	String_t* ___binderAssemblyString_16;

public:
	inline static int32_t get_offset_of_objectInfoId_0() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___objectInfoId_0)); }
	inline int32_t get_objectInfoId_0() const { return ___objectInfoId_0; }
	inline int32_t* get_address_of_objectInfoId_0() { return &___objectInfoId_0; }
	inline void set_objectInfoId_0(int32_t value)
	{
		___objectInfoId_0 = value;
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___obj_1)); }
	inline Il2CppObject * get_obj_1() const { return ___obj_1; }
	inline Il2CppObject ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(Il2CppObject * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier(&___obj_1, value);
	}

	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___objectType_2)); }
	inline Type_t * get_objectType_2() const { return ___objectType_2; }
	inline Type_t ** get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(Type_t * value)
	{
		___objectType_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectType_2, value);
	}

	inline static int32_t get_offset_of_isSi_3() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___isSi_3)); }
	inline bool get_isSi_3() const { return ___isSi_3; }
	inline bool* get_address_of_isSi_3() { return &___isSi_3; }
	inline void set_isSi_3(bool value)
	{
		___isSi_3 = value;
	}

	inline static int32_t get_offset_of_isNamed_4() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___isNamed_4)); }
	inline bool get_isNamed_4() const { return ___isNamed_4; }
	inline bool* get_address_of_isNamed_4() { return &___isNamed_4; }
	inline void set_isNamed_4(bool value)
	{
		___isNamed_4 = value;
	}

	inline static int32_t get_offset_of_isTyped_5() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___isTyped_5)); }
	inline bool get_isTyped_5() const { return ___isTyped_5; }
	inline bool* get_address_of_isTyped_5() { return &___isTyped_5; }
	inline void set_isTyped_5(bool value)
	{
		___isTyped_5 = value;
	}

	inline static int32_t get_offset_of_isArray_6() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___isArray_6)); }
	inline bool get_isArray_6() const { return ___isArray_6; }
	inline bool* get_address_of_isArray_6() { return &___isArray_6; }
	inline void set_isArray_6(bool value)
	{
		___isArray_6 = value;
	}

	inline static int32_t get_offset_of_si_7() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___si_7)); }
	inline SerializationInfo_t228987430 * get_si_7() const { return ___si_7; }
	inline SerializationInfo_t228987430 ** get_address_of_si_7() { return &___si_7; }
	inline void set_si_7(SerializationInfo_t228987430 * value)
	{
		___si_7 = value;
		Il2CppCodeGenWriteBarrier(&___si_7, value);
	}

	inline static int32_t get_offset_of_cache_8() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___cache_8)); }
	inline SerObjectInfoCache_t4068137215 * get_cache_8() const { return ___cache_8; }
	inline SerObjectInfoCache_t4068137215 ** get_address_of_cache_8() { return &___cache_8; }
	inline void set_cache_8(SerObjectInfoCache_t4068137215 * value)
	{
		___cache_8 = value;
		Il2CppCodeGenWriteBarrier(&___cache_8, value);
	}

	inline static int32_t get_offset_of_memberData_9() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___memberData_9)); }
	inline ObjectU5BU5D_t3614634134* get_memberData_9() const { return ___memberData_9; }
	inline ObjectU5BU5D_t3614634134** get_address_of_memberData_9() { return &___memberData_9; }
	inline void set_memberData_9(ObjectU5BU5D_t3614634134* value)
	{
		___memberData_9 = value;
		Il2CppCodeGenWriteBarrier(&___memberData_9, value);
	}

	inline static int32_t get_offset_of_serializationSurrogate_10() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___serializationSurrogate_10)); }
	inline Il2CppObject * get_serializationSurrogate_10() const { return ___serializationSurrogate_10; }
	inline Il2CppObject ** get_address_of_serializationSurrogate_10() { return &___serializationSurrogate_10; }
	inline void set_serializationSurrogate_10(Il2CppObject * value)
	{
		___serializationSurrogate_10 = value;
		Il2CppCodeGenWriteBarrier(&___serializationSurrogate_10, value);
	}

	inline static int32_t get_offset_of_context_11() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___context_11)); }
	inline StreamingContext_t1417235061  get_context_11() const { return ___context_11; }
	inline StreamingContext_t1417235061 * get_address_of_context_11() { return &___context_11; }
	inline void set_context_11(StreamingContext_t1417235061  value)
	{
		___context_11 = value;
	}

	inline static int32_t get_offset_of_serObjectInfoInit_12() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___serObjectInfoInit_12)); }
	inline SerObjectInfoInit_t4094458531 * get_serObjectInfoInit_12() const { return ___serObjectInfoInit_12; }
	inline SerObjectInfoInit_t4094458531 ** get_address_of_serObjectInfoInit_12() { return &___serObjectInfoInit_12; }
	inline void set_serObjectInfoInit_12(SerObjectInfoInit_t4094458531 * value)
	{
		___serObjectInfoInit_12 = value;
		Il2CppCodeGenWriteBarrier(&___serObjectInfoInit_12, value);
	}

	inline static int32_t get_offset_of_objectId_13() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___objectId_13)); }
	inline int64_t get_objectId_13() const { return ___objectId_13; }
	inline int64_t* get_address_of_objectId_13() { return &___objectId_13; }
	inline void set_objectId_13(int64_t value)
	{
		___objectId_13 = value;
	}

	inline static int32_t get_offset_of_assemId_14() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___assemId_14)); }
	inline int64_t get_assemId_14() const { return ___assemId_14; }
	inline int64_t* get_address_of_assemId_14() { return &___assemId_14; }
	inline void set_assemId_14(int64_t value)
	{
		___assemId_14 = value;
	}

	inline static int32_t get_offset_of_binderTypeName_15() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___binderTypeName_15)); }
	inline String_t* get_binderTypeName_15() const { return ___binderTypeName_15; }
	inline String_t** get_address_of_binderTypeName_15() { return &___binderTypeName_15; }
	inline void set_binderTypeName_15(String_t* value)
	{
		___binderTypeName_15 = value;
		Il2CppCodeGenWriteBarrier(&___binderTypeName_15, value);
	}

	inline static int32_t get_offset_of_binderAssemblyString_16() { return static_cast<int32_t>(offsetof(WriteObjectInfo_t2489189536, ___binderAssemblyString_16)); }
	inline String_t* get_binderAssemblyString_16() const { return ___binderAssemblyString_16; }
	inline String_t** get_address_of_binderAssemblyString_16() { return &___binderAssemblyString_16; }
	inline void set_binderAssemblyString_16(String_t* value)
	{
		___binderAssemblyString_16 = value;
		Il2CppCodeGenWriteBarrier(&___binderAssemblyString_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
