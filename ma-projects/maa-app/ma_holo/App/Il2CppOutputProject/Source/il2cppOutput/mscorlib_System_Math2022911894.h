﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Double[]
struct DoubleU5BU5D_t1889952540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Math
struct  Math_t2022911894  : public Il2CppObject
{
public:

public:
};

struct Math_t2022911894_StaticFields
{
public:
	// System.Double System.Math::doubleRoundLimit
	double ___doubleRoundLimit_0;
	// System.Double[] System.Math::roundPower10Double
	DoubleU5BU5D_t1889952540* ___roundPower10Double_1;

public:
	inline static int32_t get_offset_of_doubleRoundLimit_0() { return static_cast<int32_t>(offsetof(Math_t2022911894_StaticFields, ___doubleRoundLimit_0)); }
	inline double get_doubleRoundLimit_0() const { return ___doubleRoundLimit_0; }
	inline double* get_address_of_doubleRoundLimit_0() { return &___doubleRoundLimit_0; }
	inline void set_doubleRoundLimit_0(double value)
	{
		___doubleRoundLimit_0 = value;
	}

	inline static int32_t get_offset_of_roundPower10Double_1() { return static_cast<int32_t>(offsetof(Math_t2022911894_StaticFields, ___roundPower10Double_1)); }
	inline DoubleU5BU5D_t1889952540* get_roundPower10Double_1() const { return ___roundPower10Double_1; }
	inline DoubleU5BU5D_t1889952540** get_address_of_roundPower10Double_1() { return &___roundPower10Double_1; }
	inline void set_roundPower10Double_1(DoubleU5BU5D_t1889952540* value)
	{
		___roundPower10Double_1 = value;
		Il2CppCodeGenWriteBarrier(&___roundPower10Double_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
