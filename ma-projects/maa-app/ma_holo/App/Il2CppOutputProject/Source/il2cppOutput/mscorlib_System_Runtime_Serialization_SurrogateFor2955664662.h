﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t1282780357;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SurrogateForCyclicalReference
struct  SurrogateForCyclicalReference_t2955664662  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.SurrogateForCyclicalReference::innerSurrogate
	Il2CppObject * ___innerSurrogate_0;

public:
	inline static int32_t get_offset_of_innerSurrogate_0() { return static_cast<int32_t>(offsetof(SurrogateForCyclicalReference_t2955664662, ___innerSurrogate_0)); }
	inline Il2CppObject * get_innerSurrogate_0() const { return ___innerSurrogate_0; }
	inline Il2CppObject ** get_address_of_innerSurrogate_0() { return &___innerSurrogate_0; }
	inline void set_innerSurrogate_0(Il2CppObject * value)
	{
		___innerSurrogate_0 = value;
		Il2CppCodeGenWriteBarrier(&___innerSurrogate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
