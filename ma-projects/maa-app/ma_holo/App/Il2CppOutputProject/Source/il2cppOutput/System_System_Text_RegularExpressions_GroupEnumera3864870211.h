﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t939014605;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupEnumerator
struct  GroupEnumerator_t3864870211  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.GroupEnumerator::_rgc
	GroupCollection_t939014605 * ____rgc_0;
	// System.Int32 System.Text.RegularExpressions.GroupEnumerator::_curindex
	int32_t ____curindex_1;

public:
	inline static int32_t get_offset_of__rgc_0() { return static_cast<int32_t>(offsetof(GroupEnumerator_t3864870211, ____rgc_0)); }
	inline GroupCollection_t939014605 * get__rgc_0() const { return ____rgc_0; }
	inline GroupCollection_t939014605 ** get_address_of__rgc_0() { return &____rgc_0; }
	inline void set__rgc_0(GroupCollection_t939014605 * value)
	{
		____rgc_0 = value;
		Il2CppCodeGenWriteBarrier(&____rgc_0, value);
	}

	inline static int32_t get_offset_of__curindex_1() { return static_cast<int32_t>(offsetof(GroupEnumerator_t3864870211, ____curindex_1)); }
	inline int32_t get__curindex_1() const { return ____curindex_1; }
	inline int32_t* get_address_of__curindex_1() { return &____curindex_1; }
	inline void set__curindex_1(int32_t value)
	{
		____curindex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
