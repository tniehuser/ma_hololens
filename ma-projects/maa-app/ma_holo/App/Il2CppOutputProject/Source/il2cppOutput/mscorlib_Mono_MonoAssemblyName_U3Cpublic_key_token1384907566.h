﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.MonoAssemblyName/<public_key_token>__FixedBuffer5
struct  U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566 
{
public:
	union
	{
		struct
		{
			// System.Byte Mono.MonoAssemblyName/<public_key_token>__FixedBuffer5::FixedElementField
			uint8_t ___FixedElementField_0;
		};
		uint8_t U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566__padding[17];
	};

public:
	inline static int32_t get_offset_of_FixedElementField_0() { return static_cast<int32_t>(offsetof(U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566, ___FixedElementField_0)); }
	inline uint8_t get_FixedElementField_0() const { return ___FixedElementField_0; }
	inline uint8_t* get_address_of_FixedElementField_0() { return &___FixedElementField_0; }
	inline void set_FixedElementField_0(uint8_t value)
	{
		___FixedElementField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
