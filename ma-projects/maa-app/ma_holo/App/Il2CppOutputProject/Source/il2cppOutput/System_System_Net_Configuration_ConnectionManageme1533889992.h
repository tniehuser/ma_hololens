﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur2600766927.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementSection
struct  ConnectionManagementSection_t1533889992  : public ConfigurationSection_t2600766927
{
public:

public:
};

struct ConnectionManagementSection_t1533889992_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ConnectionManagementSection::connectionManagementProp
	ConfigurationProperty_t2048066811 * ___connectionManagementProp_19;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ConnectionManagementSection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_20;

public:
	inline static int32_t get_offset_of_connectionManagementProp_19() { return static_cast<int32_t>(offsetof(ConnectionManagementSection_t1533889992_StaticFields, ___connectionManagementProp_19)); }
	inline ConfigurationProperty_t2048066811 * get_connectionManagementProp_19() const { return ___connectionManagementProp_19; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_connectionManagementProp_19() { return &___connectionManagementProp_19; }
	inline void set_connectionManagementProp_19(ConfigurationProperty_t2048066811 * value)
	{
		___connectionManagementProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___connectionManagementProp_19, value);
	}

	inline static int32_t get_offset_of_properties_20() { return static_cast<int32_t>(offsetof(ConnectionManagementSection_t1533889992_StaticFields, ___properties_20)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_20() const { return ___properties_20; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_20() { return &___properties_20; }
	inline void set_properties_20(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_20 = value;
		Il2CppCodeGenWriteBarrier(&___properties_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
