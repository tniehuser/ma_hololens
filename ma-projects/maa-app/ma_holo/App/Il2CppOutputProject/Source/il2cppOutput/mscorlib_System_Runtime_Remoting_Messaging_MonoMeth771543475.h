﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallTyp2486906258.h"

// System.Reflection.MonoMethod
struct MonoMethod_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1927440687;
// System.Runtime.Remoting.Messaging.AsyncResult
struct AsyncResult_t2232356043;
// System.String
struct String_t;
// System.Runtime.Remoting.Messaging.MCMDictionary
struct MCMDictionary_t159052147;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Runtime.Remoting.Identity
struct Identity_t3647548000;
struct Exception_t1927440687_marshaled_pinvoke;
struct AsyncResult_t2232356043_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;
struct AsyncResult_t2232356043_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.MonoMethodMessage
struct  MonoMethodMessage_t771543475  : public Il2CppObject
{
public:
	// System.Reflection.MonoMethod System.Runtime.Remoting.Messaging.MonoMethodMessage::method
	MonoMethod_t * ___method_0;
	// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::args
	ObjectU5BU5D_t3614634134* ___args_1;
	// System.String[] System.Runtime.Remoting.Messaging.MonoMethodMessage::names
	StringU5BU5D_t1642385972* ___names_2;
	// System.Byte[] System.Runtime.Remoting.Messaging.MonoMethodMessage::arg_types
	ByteU5BU5D_t3397334013* ___arg_types_3;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::ctx
	LogicalCallContext_t725724420 * ___ctx_4;
	// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::rval
	Il2CppObject * ___rval_5;
	// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::exc
	Exception_t1927440687 * ___exc_6;
	// System.Runtime.Remoting.Messaging.AsyncResult System.Runtime.Remoting.Messaging.MonoMethodMessage::asyncResult
	AsyncResult_t2232356043 * ___asyncResult_7;
	// System.Runtime.Remoting.Messaging.CallType System.Runtime.Remoting.Messaging.MonoMethodMessage::call_type
	int32_t ___call_type_8;
	// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::uri
	String_t* ___uri_9;
	// System.Runtime.Remoting.Messaging.MCMDictionary System.Runtime.Remoting.Messaging.MonoMethodMessage::properties
	MCMDictionary_t159052147 * ___properties_10;
	// System.Type[] System.Runtime.Remoting.Messaging.MonoMethodMessage::methodSignature
	TypeU5BU5D_t1664964607* ___methodSignature_11;
	// System.Runtime.Remoting.Identity System.Runtime.Remoting.Messaging.MonoMethodMessage::identity
	Identity_t3647548000 * ___identity_12;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___method_0)); }
	inline MonoMethod_t * get_method_0() const { return ___method_0; }
	inline MonoMethod_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MonoMethod_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___args_1)); }
	inline ObjectU5BU5D_t3614634134* get_args_1() const { return ___args_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(ObjectU5BU5D_t3614634134* value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier(&___args_1, value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___names_2)); }
	inline StringU5BU5D_t1642385972* get_names_2() const { return ___names_2; }
	inline StringU5BU5D_t1642385972** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(StringU5BU5D_t1642385972* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier(&___names_2, value);
	}

	inline static int32_t get_offset_of_arg_types_3() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___arg_types_3)); }
	inline ByteU5BU5D_t3397334013* get_arg_types_3() const { return ___arg_types_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_arg_types_3() { return &___arg_types_3; }
	inline void set_arg_types_3(ByteU5BU5D_t3397334013* value)
	{
		___arg_types_3 = value;
		Il2CppCodeGenWriteBarrier(&___arg_types_3, value);
	}

	inline static int32_t get_offset_of_ctx_4() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___ctx_4)); }
	inline LogicalCallContext_t725724420 * get_ctx_4() const { return ___ctx_4; }
	inline LogicalCallContext_t725724420 ** get_address_of_ctx_4() { return &___ctx_4; }
	inline void set_ctx_4(LogicalCallContext_t725724420 * value)
	{
		___ctx_4 = value;
		Il2CppCodeGenWriteBarrier(&___ctx_4, value);
	}

	inline static int32_t get_offset_of_rval_5() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___rval_5)); }
	inline Il2CppObject * get_rval_5() const { return ___rval_5; }
	inline Il2CppObject ** get_address_of_rval_5() { return &___rval_5; }
	inline void set_rval_5(Il2CppObject * value)
	{
		___rval_5 = value;
		Il2CppCodeGenWriteBarrier(&___rval_5, value);
	}

	inline static int32_t get_offset_of_exc_6() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___exc_6)); }
	inline Exception_t1927440687 * get_exc_6() const { return ___exc_6; }
	inline Exception_t1927440687 ** get_address_of_exc_6() { return &___exc_6; }
	inline void set_exc_6(Exception_t1927440687 * value)
	{
		___exc_6 = value;
		Il2CppCodeGenWriteBarrier(&___exc_6, value);
	}

	inline static int32_t get_offset_of_asyncResult_7() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___asyncResult_7)); }
	inline AsyncResult_t2232356043 * get_asyncResult_7() const { return ___asyncResult_7; }
	inline AsyncResult_t2232356043 ** get_address_of_asyncResult_7() { return &___asyncResult_7; }
	inline void set_asyncResult_7(AsyncResult_t2232356043 * value)
	{
		___asyncResult_7 = value;
		Il2CppCodeGenWriteBarrier(&___asyncResult_7, value);
	}

	inline static int32_t get_offset_of_call_type_8() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___call_type_8)); }
	inline int32_t get_call_type_8() const { return ___call_type_8; }
	inline int32_t* get_address_of_call_type_8() { return &___call_type_8; }
	inline void set_call_type_8(int32_t value)
	{
		___call_type_8 = value;
	}

	inline static int32_t get_offset_of_uri_9() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___uri_9)); }
	inline String_t* get_uri_9() const { return ___uri_9; }
	inline String_t** get_address_of_uri_9() { return &___uri_9; }
	inline void set_uri_9(String_t* value)
	{
		___uri_9 = value;
		Il2CppCodeGenWriteBarrier(&___uri_9, value);
	}

	inline static int32_t get_offset_of_properties_10() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___properties_10)); }
	inline MCMDictionary_t159052147 * get_properties_10() const { return ___properties_10; }
	inline MCMDictionary_t159052147 ** get_address_of_properties_10() { return &___properties_10; }
	inline void set_properties_10(MCMDictionary_t159052147 * value)
	{
		___properties_10 = value;
		Il2CppCodeGenWriteBarrier(&___properties_10, value);
	}

	inline static int32_t get_offset_of_methodSignature_11() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___methodSignature_11)); }
	inline TypeU5BU5D_t1664964607* get_methodSignature_11() const { return ___methodSignature_11; }
	inline TypeU5BU5D_t1664964607** get_address_of_methodSignature_11() { return &___methodSignature_11; }
	inline void set_methodSignature_11(TypeU5BU5D_t1664964607* value)
	{
		___methodSignature_11 = value;
		Il2CppCodeGenWriteBarrier(&___methodSignature_11, value);
	}

	inline static int32_t get_offset_of_identity_12() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475, ___identity_12)); }
	inline Identity_t3647548000 * get_identity_12() const { return ___identity_12; }
	inline Identity_t3647548000 ** get_address_of_identity_12() { return &___identity_12; }
	inline void set_identity_12(Identity_t3647548000 * value)
	{
		___identity_12 = value;
		Il2CppCodeGenWriteBarrier(&___identity_12, value);
	}
};

struct MonoMethodMessage_t771543475_StaticFields
{
public:
	// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::CallContextKey
	String_t* ___CallContextKey_13;
	// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::UriKey
	String_t* ___UriKey_14;

public:
	inline static int32_t get_offset_of_CallContextKey_13() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475_StaticFields, ___CallContextKey_13)); }
	inline String_t* get_CallContextKey_13() const { return ___CallContextKey_13; }
	inline String_t** get_address_of_CallContextKey_13() { return &___CallContextKey_13; }
	inline void set_CallContextKey_13(String_t* value)
	{
		___CallContextKey_13 = value;
		Il2CppCodeGenWriteBarrier(&___CallContextKey_13, value);
	}

	inline static int32_t get_offset_of_UriKey_14() { return static_cast<int32_t>(offsetof(MonoMethodMessage_t771543475_StaticFields, ___UriKey_14)); }
	inline String_t* get_UriKey_14() const { return ___UriKey_14; }
	inline String_t** get_address_of_UriKey_14() { return &___UriKey_14; }
	inline void set_UriKey_14(String_t* value)
	{
		___UriKey_14 = value;
		Il2CppCodeGenWriteBarrier(&___UriKey_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Remoting.Messaging.MonoMethodMessage
struct MonoMethodMessage_t771543475_marshaled_pinvoke
{
	MonoMethod_t * ___method_0;
	ObjectU5BU5D_t3614634134* ___args_1;
	char** ___names_2;
	uint8_t* ___arg_types_3;
	LogicalCallContext_t725724420 * ___ctx_4;
	Il2CppIUnknown* ___rval_5;
	Exception_t1927440687_marshaled_pinvoke* ___exc_6;
	AsyncResult_t2232356043_marshaled_pinvoke* ___asyncResult_7;
	int32_t ___call_type_8;
	char* ___uri_9;
	MCMDictionary_t159052147 * ___properties_10;
	TypeU5BU5D_t1664964607* ___methodSignature_11;
	Identity_t3647548000 * ___identity_12;
};
// Native definition for COM marshalling of System.Runtime.Remoting.Messaging.MonoMethodMessage
struct MonoMethodMessage_t771543475_marshaled_com
{
	MonoMethod_t * ___method_0;
	ObjectU5BU5D_t3614634134* ___args_1;
	Il2CppChar** ___names_2;
	uint8_t* ___arg_types_3;
	LogicalCallContext_t725724420 * ___ctx_4;
	Il2CppIUnknown* ___rval_5;
	Exception_t1927440687_marshaled_com* ___exc_6;
	AsyncResult_t2232356043_marshaled_com* ___asyncResult_7;
	int32_t ___call_type_8;
	Il2CppChar* ___uri_9;
	MCMDictionary_t159052147 * ___properties_10;
	TypeU5BU5D_t1664964607* ___methodSignature_11;
	Identity_t3647548000 * ___identity_12;
};
