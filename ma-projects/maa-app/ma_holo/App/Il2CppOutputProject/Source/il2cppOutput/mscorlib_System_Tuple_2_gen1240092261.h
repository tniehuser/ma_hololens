﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t2745753060;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`2<System.IO.Stream,System.IO.Stream/ReadWriteTask>
struct  Tuple_2_t1240092261  : public Il2CppObject
{
public:
	// T1 System.Tuple`2::m_Item1
	Stream_t3255436806 * ___m_Item1_0;
	// T2 System.Tuple`2::m_Item2
	ReadWriteTask_t2745753060 * ___m_Item2_1;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t1240092261, ___m_Item1_0)); }
	inline Stream_t3255436806 * get_m_Item1_0() const { return ___m_Item1_0; }
	inline Stream_t3255436806 ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(Stream_t3255436806 * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t1240092261, ___m_Item2_1)); }
	inline ReadWriteTask_t2745753060 * get_m_Item2_1() const { return ___m_Item2_1; }
	inline ReadWriteTask_t2745753060 ** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(ReadWriteTask_t2745753060 * value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
