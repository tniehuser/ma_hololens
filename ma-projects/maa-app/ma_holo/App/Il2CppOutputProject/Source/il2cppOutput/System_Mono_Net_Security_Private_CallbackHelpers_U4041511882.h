﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t4080334132;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<MonoToInternal>c__AnonStorey8
struct  U3CMonoToInternalU3Ec__AnonStorey8_t4041511882  : public Il2CppObject
{
public:
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Net.Security.Private.CallbackHelpers/<MonoToInternal>c__AnonStorey8::callback
	MonoLocalCertificateSelectionCallback_t4080334132 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CMonoToInternalU3Ec__AnonStorey8_t4041511882, ___callback_0)); }
	inline MonoLocalCertificateSelectionCallback_t4080334132 * get_callback_0() const { return ___callback_0; }
	inline MonoLocalCertificateSelectionCallback_t4080334132 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoLocalCertificateSelectionCallback_t4080334132 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
