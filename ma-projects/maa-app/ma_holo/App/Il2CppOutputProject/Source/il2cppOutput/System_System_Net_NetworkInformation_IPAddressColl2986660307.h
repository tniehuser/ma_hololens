﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ObjectModel.Collection`1<System.Net.IPAddress>
struct Collection_1_t941716477;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.IPAddressCollection
struct  IPAddressCollection_t2986660307  : public Il2CppObject
{
public:
	// System.Collections.ObjectModel.Collection`1<System.Net.IPAddress> System.Net.NetworkInformation.IPAddressCollection::addresses
	Collection_1_t941716477 * ___addresses_0;

public:
	inline static int32_t get_offset_of_addresses_0() { return static_cast<int32_t>(offsetof(IPAddressCollection_t2986660307, ___addresses_0)); }
	inline Collection_1_t941716477 * get_addresses_0() const { return ___addresses_0; }
	inline Collection_1_t941716477 ** get_address_of_addresses_0() { return &___addresses_0; }
	inline void set_addresses_0(Collection_1_t941716477 * value)
	{
		___addresses_0 = value;
		Il2CppCodeGenWriteBarrier(&___addresses_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
