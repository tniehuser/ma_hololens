﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaType3522160305.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.Schema.SchemaBuilder
struct SchemaBuilder_t908297946;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Parser
struct  Parser_t1940171737  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaType System.Xml.Schema.Parser::schemaType
	int32_t ___schemaType_0;
	// System.Xml.XmlNameTable System.Xml.Schema.Parser::nameTable
	XmlNameTable_t1345805268 * ___nameTable_1;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.Parser::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.Parser::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_3;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::namespaceManager
	XmlNamespaceManager_t486731501 * ___namespaceManager_4;
	// System.Xml.XmlReader System.Xml.Schema.Parser::reader
	XmlReader_t3675626668 * ___reader_5;
	// System.Xml.PositionInfo System.Xml.Schema.Parser::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_6;
	// System.Boolean System.Xml.Schema.Parser::isProcessNamespaces
	bool ___isProcessNamespaces_7;
	// System.Int32 System.Xml.Schema.Parser::schemaXmlDepth
	int32_t ___schemaXmlDepth_8;
	// System.Int32 System.Xml.Schema.Parser::markupDepth
	int32_t ___markupDepth_9;
	// System.Xml.Schema.SchemaBuilder System.Xml.Schema.Parser::builder
	SchemaBuilder_t908297946 * ___builder_10;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Parser::schema
	XmlSchema_t880472818 * ___schema_11;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.Parser::xdrSchema
	SchemaInfo_t87206461 * ___xdrSchema_12;
	// System.Xml.XmlResolver System.Xml.Schema.Parser::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_13;
	// System.Xml.XmlDocument System.Xml.Schema.Parser::dummyDocument
	XmlDocument_t3649534162 * ___dummyDocument_14;
	// System.Boolean System.Xml.Schema.Parser::processMarkup
	bool ___processMarkup_15;
	// System.Xml.XmlNode System.Xml.Schema.Parser::parentNode
	XmlNode_t616554813 * ___parentNode_16;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::annotationNSManager
	XmlNamespaceManager_t486731501 * ___annotationNSManager_17;
	// System.String System.Xml.Schema.Parser::xmlns
	String_t* ___xmlns_18;
	// System.Xml.XmlCharType System.Xml.Schema.Parser::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_19;

public:
	inline static int32_t get_offset_of_schemaType_0() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaType_0)); }
	inline int32_t get_schemaType_0() const { return ___schemaType_0; }
	inline int32_t* get_address_of_schemaType_0() { return &___schemaType_0; }
	inline void set_schemaType_0(int32_t value)
	{
		___schemaType_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___nameTable_1)); }
	inline XmlNameTable_t1345805268 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t1345805268 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_1, value);
	}

	inline static int32_t get_offset_of_schemaNames_2() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaNames_2)); }
	inline SchemaNames_t1619962557 * get_schemaNames_2() const { return ___schemaNames_2; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_2() { return &___schemaNames_2; }
	inline void set_schemaNames_2(SchemaNames_t1619962557 * value)
	{
		___schemaNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___schemaNames_2, value);
	}

	inline static int32_t get_offset_of_eventHandler_3() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___eventHandler_3)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_3() const { return ___eventHandler_3; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_3() { return &___eventHandler_3; }
	inline void set_eventHandler_3(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandler_3, value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t486731501 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t486731501 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceManager_4, value);
	}

	inline static int32_t get_offset_of_reader_5() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___reader_5)); }
	inline XmlReader_t3675626668 * get_reader_5() const { return ___reader_5; }
	inline XmlReader_t3675626668 ** get_address_of_reader_5() { return &___reader_5; }
	inline void set_reader_5(XmlReader_t3675626668 * value)
	{
		___reader_5 = value;
		Il2CppCodeGenWriteBarrier(&___reader_5, value);
	}

	inline static int32_t get_offset_of_positionInfo_6() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___positionInfo_6)); }
	inline PositionInfo_t3273236083 * get_positionInfo_6() const { return ___positionInfo_6; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_6() { return &___positionInfo_6; }
	inline void set_positionInfo_6(PositionInfo_t3273236083 * value)
	{
		___positionInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___positionInfo_6, value);
	}

	inline static int32_t get_offset_of_isProcessNamespaces_7() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___isProcessNamespaces_7)); }
	inline bool get_isProcessNamespaces_7() const { return ___isProcessNamespaces_7; }
	inline bool* get_address_of_isProcessNamespaces_7() { return &___isProcessNamespaces_7; }
	inline void set_isProcessNamespaces_7(bool value)
	{
		___isProcessNamespaces_7 = value;
	}

	inline static int32_t get_offset_of_schemaXmlDepth_8() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaXmlDepth_8)); }
	inline int32_t get_schemaXmlDepth_8() const { return ___schemaXmlDepth_8; }
	inline int32_t* get_address_of_schemaXmlDepth_8() { return &___schemaXmlDepth_8; }
	inline void set_schemaXmlDepth_8(int32_t value)
	{
		___schemaXmlDepth_8 = value;
	}

	inline static int32_t get_offset_of_markupDepth_9() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___markupDepth_9)); }
	inline int32_t get_markupDepth_9() const { return ___markupDepth_9; }
	inline int32_t* get_address_of_markupDepth_9() { return &___markupDepth_9; }
	inline void set_markupDepth_9(int32_t value)
	{
		___markupDepth_9 = value;
	}

	inline static int32_t get_offset_of_builder_10() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___builder_10)); }
	inline SchemaBuilder_t908297946 * get_builder_10() const { return ___builder_10; }
	inline SchemaBuilder_t908297946 ** get_address_of_builder_10() { return &___builder_10; }
	inline void set_builder_10(SchemaBuilder_t908297946 * value)
	{
		___builder_10 = value;
		Il2CppCodeGenWriteBarrier(&___builder_10, value);
	}

	inline static int32_t get_offset_of_schema_11() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schema_11)); }
	inline XmlSchema_t880472818 * get_schema_11() const { return ___schema_11; }
	inline XmlSchema_t880472818 ** get_address_of_schema_11() { return &___schema_11; }
	inline void set_schema_11(XmlSchema_t880472818 * value)
	{
		___schema_11 = value;
		Il2CppCodeGenWriteBarrier(&___schema_11, value);
	}

	inline static int32_t get_offset_of_xdrSchema_12() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xdrSchema_12)); }
	inline SchemaInfo_t87206461 * get_xdrSchema_12() const { return ___xdrSchema_12; }
	inline SchemaInfo_t87206461 ** get_address_of_xdrSchema_12() { return &___xdrSchema_12; }
	inline void set_xdrSchema_12(SchemaInfo_t87206461 * value)
	{
		___xdrSchema_12 = value;
		Il2CppCodeGenWriteBarrier(&___xdrSchema_12, value);
	}

	inline static int32_t get_offset_of_xmlResolver_13() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlResolver_13)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_13() const { return ___xmlResolver_13; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_13() { return &___xmlResolver_13; }
	inline void set_xmlResolver_13(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_13 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_13, value);
	}

	inline static int32_t get_offset_of_dummyDocument_14() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___dummyDocument_14)); }
	inline XmlDocument_t3649534162 * get_dummyDocument_14() const { return ___dummyDocument_14; }
	inline XmlDocument_t3649534162 ** get_address_of_dummyDocument_14() { return &___dummyDocument_14; }
	inline void set_dummyDocument_14(XmlDocument_t3649534162 * value)
	{
		___dummyDocument_14 = value;
		Il2CppCodeGenWriteBarrier(&___dummyDocument_14, value);
	}

	inline static int32_t get_offset_of_processMarkup_15() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___processMarkup_15)); }
	inline bool get_processMarkup_15() const { return ___processMarkup_15; }
	inline bool* get_address_of_processMarkup_15() { return &___processMarkup_15; }
	inline void set_processMarkup_15(bool value)
	{
		___processMarkup_15 = value;
	}

	inline static int32_t get_offset_of_parentNode_16() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___parentNode_16)); }
	inline XmlNode_t616554813 * get_parentNode_16() const { return ___parentNode_16; }
	inline XmlNode_t616554813 ** get_address_of_parentNode_16() { return &___parentNode_16; }
	inline void set_parentNode_16(XmlNode_t616554813 * value)
	{
		___parentNode_16 = value;
		Il2CppCodeGenWriteBarrier(&___parentNode_16, value);
	}

	inline static int32_t get_offset_of_annotationNSManager_17() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___annotationNSManager_17)); }
	inline XmlNamespaceManager_t486731501 * get_annotationNSManager_17() const { return ___annotationNSManager_17; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_annotationNSManager_17() { return &___annotationNSManager_17; }
	inline void set_annotationNSManager_17(XmlNamespaceManager_t486731501 * value)
	{
		___annotationNSManager_17 = value;
		Il2CppCodeGenWriteBarrier(&___annotationNSManager_17, value);
	}

	inline static int32_t get_offset_of_xmlns_18() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlns_18)); }
	inline String_t* get_xmlns_18() const { return ___xmlns_18; }
	inline String_t** get_address_of_xmlns_18() { return &___xmlns_18; }
	inline void set_xmlns_18(String_t* value)
	{
		___xmlns_18 = value;
		Il2CppCodeGenWriteBarrier(&___xmlns_18, value);
	}

	inline static int32_t get_offset_of_xmlCharType_19() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlCharType_19)); }
	inline XmlCharType_t1050521405  get_xmlCharType_19() const { return ___xmlCharType_19; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_19() { return &___xmlCharType_19; }
	inline void set_xmlCharType_19(XmlCharType_t1050521405  value)
	{
		___xmlCharType_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
