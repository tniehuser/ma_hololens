﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.String
struct String_t;
// System.Globalization.CultureData
struct CultureData_t3400086592;
// System.Globalization.TextInfo
struct TextInfo_t3620182823;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TextInfo
struct  TextInfo_t3620182823  : public Il2CppObject
{
public:
	// System.Boolean System.Globalization.TextInfo::m_isReadOnly
	bool ___m_isReadOnly_0;
	// System.String System.Globalization.TextInfo::m_cultureName
	String_t* ___m_cultureName_1;
	// System.Globalization.CultureData System.Globalization.TextInfo::m_cultureData
	CultureData_t3400086592 * ___m_cultureData_2;
	// System.String System.Globalization.TextInfo::m_textInfoName
	String_t* ___m_textInfoName_3;
	// System.Nullable`1<System.Boolean> System.Globalization.TextInfo::m_IsAsciiCasingSameAsInvariant
	Nullable_1_t2088641033  ___m_IsAsciiCasingSameAsInvariant_4;
	// System.String System.Globalization.TextInfo::customCultureName
	String_t* ___customCultureName_6;
	// System.Int32 System.Globalization.TextInfo::m_win32LangID
	int32_t ___m_win32LangID_7;

public:
	inline static int32_t get_offset_of_m_isReadOnly_0() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_isReadOnly_0)); }
	inline bool get_m_isReadOnly_0() const { return ___m_isReadOnly_0; }
	inline bool* get_address_of_m_isReadOnly_0() { return &___m_isReadOnly_0; }
	inline void set_m_isReadOnly_0(bool value)
	{
		___m_isReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_m_cultureName_1() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_cultureName_1)); }
	inline String_t* get_m_cultureName_1() const { return ___m_cultureName_1; }
	inline String_t** get_address_of_m_cultureName_1() { return &___m_cultureName_1; }
	inline void set_m_cultureName_1(String_t* value)
	{
		___m_cultureName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_cultureName_1, value);
	}

	inline static int32_t get_offset_of_m_cultureData_2() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_cultureData_2)); }
	inline CultureData_t3400086592 * get_m_cultureData_2() const { return ___m_cultureData_2; }
	inline CultureData_t3400086592 ** get_address_of_m_cultureData_2() { return &___m_cultureData_2; }
	inline void set_m_cultureData_2(CultureData_t3400086592 * value)
	{
		___m_cultureData_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cultureData_2, value);
	}

	inline static int32_t get_offset_of_m_textInfoName_3() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_textInfoName_3)); }
	inline String_t* get_m_textInfoName_3() const { return ___m_textInfoName_3; }
	inline String_t** get_address_of_m_textInfoName_3() { return &___m_textInfoName_3; }
	inline void set_m_textInfoName_3(String_t* value)
	{
		___m_textInfoName_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_textInfoName_3, value);
	}

	inline static int32_t get_offset_of_m_IsAsciiCasingSameAsInvariant_4() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_IsAsciiCasingSameAsInvariant_4)); }
	inline Nullable_1_t2088641033  get_m_IsAsciiCasingSameAsInvariant_4() const { return ___m_IsAsciiCasingSameAsInvariant_4; }
	inline Nullable_1_t2088641033 * get_address_of_m_IsAsciiCasingSameAsInvariant_4() { return &___m_IsAsciiCasingSameAsInvariant_4; }
	inline void set_m_IsAsciiCasingSameAsInvariant_4(Nullable_1_t2088641033  value)
	{
		___m_IsAsciiCasingSameAsInvariant_4 = value;
	}

	inline static int32_t get_offset_of_customCultureName_6() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___customCultureName_6)); }
	inline String_t* get_customCultureName_6() const { return ___customCultureName_6; }
	inline String_t** get_address_of_customCultureName_6() { return &___customCultureName_6; }
	inline void set_customCultureName_6(String_t* value)
	{
		___customCultureName_6 = value;
		Il2CppCodeGenWriteBarrier(&___customCultureName_6, value);
	}

	inline static int32_t get_offset_of_m_win32LangID_7() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823, ___m_win32LangID_7)); }
	inline int32_t get_m_win32LangID_7() const { return ___m_win32LangID_7; }
	inline int32_t* get_address_of_m_win32LangID_7() { return &___m_win32LangID_7; }
	inline void set_m_win32LangID_7(int32_t value)
	{
		___m_win32LangID_7 = value;
	}
};

struct TextInfo_t3620182823_StaticFields
{
public:
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.TextInfo::s_Invariant
	TextInfo_t3620182823 * ___s_Invariant_5;

public:
	inline static int32_t get_offset_of_s_Invariant_5() { return static_cast<int32_t>(offsetof(TextInfo_t3620182823_StaticFields, ___s_Invariant_5)); }
	inline TextInfo_t3620182823 * get_s_Invariant_5() const { return ___s_Invariant_5; }
	inline TextInfo_t3620182823 ** get_address_of_s_Invariant_5() { return &___s_Invariant_5; }
	inline void set_s_Invariant_5(TextInfo_t3620182823 * value)
	{
		___s_Invariant_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_Invariant_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
