﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Type
struct Type_t;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2645893724;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache
struct SerObjectInfoCache_t4068137215;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t1282780357;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t672924358;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit
struct SerObjectInfoInit_t4094458531;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1473156697;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo
struct  ReadObjectInfo_t1645661573  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::objectInfoId
	int32_t ___objectInfoId_0;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::objectType
	Type_t * ___objectType_2;
	// System.Runtime.Serialization.ObjectManager System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::objectManager
	ObjectManager_t2645893724 * ___objectManager_3;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::count
	int32_t ___count_4;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::isSi
	bool ___isSi_5;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::isNamed
	bool ___isNamed_6;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::isTyped
	bool ___isTyped_7;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::bSimpleAssembly
	bool ___bSimpleAssembly_8;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoCache System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::cache
	SerObjectInfoCache_t4068137215 * ___cache_9;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::wireMemberNames
	StringU5BU5D_t1642385972* ___wireMemberNames_10;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::wireMemberTypes
	TypeU5BU5D_t1664964607* ___wireMemberTypes_11;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::lastPosition
	int32_t ___lastPosition_12;
	// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::serializationSurrogate
	Il2CppObject * ___serializationSurrogate_13;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::context
	StreamingContext_t1417235061  ___context_14;
	// System.Collections.Generic.List`1<System.Type> System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::memberTypesList
	List_1_t672924358 * ___memberTypesList_15;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::serObjectInfoInit
	SerObjectInfoInit_t4094458531 * ___serObjectInfoInit_16;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::formatterConverter
	Il2CppObject * ___formatterConverter_17;

public:
	inline static int32_t get_offset_of_objectInfoId_0() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___objectInfoId_0)); }
	inline int32_t get_objectInfoId_0() const { return ___objectInfoId_0; }
	inline int32_t* get_address_of_objectInfoId_0() { return &___objectInfoId_0; }
	inline void set_objectInfoId_0(int32_t value)
	{
		___objectInfoId_0 = value;
	}

	inline static int32_t get_offset_of_objectType_2() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___objectType_2)); }
	inline Type_t * get_objectType_2() const { return ___objectType_2; }
	inline Type_t ** get_address_of_objectType_2() { return &___objectType_2; }
	inline void set_objectType_2(Type_t * value)
	{
		___objectType_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectType_2, value);
	}

	inline static int32_t get_offset_of_objectManager_3() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___objectManager_3)); }
	inline ObjectManager_t2645893724 * get_objectManager_3() const { return ___objectManager_3; }
	inline ObjectManager_t2645893724 ** get_address_of_objectManager_3() { return &___objectManager_3; }
	inline void set_objectManager_3(ObjectManager_t2645893724 * value)
	{
		___objectManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectManager_3, value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_isSi_5() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___isSi_5)); }
	inline bool get_isSi_5() const { return ___isSi_5; }
	inline bool* get_address_of_isSi_5() { return &___isSi_5; }
	inline void set_isSi_5(bool value)
	{
		___isSi_5 = value;
	}

	inline static int32_t get_offset_of_isNamed_6() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___isNamed_6)); }
	inline bool get_isNamed_6() const { return ___isNamed_6; }
	inline bool* get_address_of_isNamed_6() { return &___isNamed_6; }
	inline void set_isNamed_6(bool value)
	{
		___isNamed_6 = value;
	}

	inline static int32_t get_offset_of_isTyped_7() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___isTyped_7)); }
	inline bool get_isTyped_7() const { return ___isTyped_7; }
	inline bool* get_address_of_isTyped_7() { return &___isTyped_7; }
	inline void set_isTyped_7(bool value)
	{
		___isTyped_7 = value;
	}

	inline static int32_t get_offset_of_bSimpleAssembly_8() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___bSimpleAssembly_8)); }
	inline bool get_bSimpleAssembly_8() const { return ___bSimpleAssembly_8; }
	inline bool* get_address_of_bSimpleAssembly_8() { return &___bSimpleAssembly_8; }
	inline void set_bSimpleAssembly_8(bool value)
	{
		___bSimpleAssembly_8 = value;
	}

	inline static int32_t get_offset_of_cache_9() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___cache_9)); }
	inline SerObjectInfoCache_t4068137215 * get_cache_9() const { return ___cache_9; }
	inline SerObjectInfoCache_t4068137215 ** get_address_of_cache_9() { return &___cache_9; }
	inline void set_cache_9(SerObjectInfoCache_t4068137215 * value)
	{
		___cache_9 = value;
		Il2CppCodeGenWriteBarrier(&___cache_9, value);
	}

	inline static int32_t get_offset_of_wireMemberNames_10() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___wireMemberNames_10)); }
	inline StringU5BU5D_t1642385972* get_wireMemberNames_10() const { return ___wireMemberNames_10; }
	inline StringU5BU5D_t1642385972** get_address_of_wireMemberNames_10() { return &___wireMemberNames_10; }
	inline void set_wireMemberNames_10(StringU5BU5D_t1642385972* value)
	{
		___wireMemberNames_10 = value;
		Il2CppCodeGenWriteBarrier(&___wireMemberNames_10, value);
	}

	inline static int32_t get_offset_of_wireMemberTypes_11() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___wireMemberTypes_11)); }
	inline TypeU5BU5D_t1664964607* get_wireMemberTypes_11() const { return ___wireMemberTypes_11; }
	inline TypeU5BU5D_t1664964607** get_address_of_wireMemberTypes_11() { return &___wireMemberTypes_11; }
	inline void set_wireMemberTypes_11(TypeU5BU5D_t1664964607* value)
	{
		___wireMemberTypes_11 = value;
		Il2CppCodeGenWriteBarrier(&___wireMemberTypes_11, value);
	}

	inline static int32_t get_offset_of_lastPosition_12() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___lastPosition_12)); }
	inline int32_t get_lastPosition_12() const { return ___lastPosition_12; }
	inline int32_t* get_address_of_lastPosition_12() { return &___lastPosition_12; }
	inline void set_lastPosition_12(int32_t value)
	{
		___lastPosition_12 = value;
	}

	inline static int32_t get_offset_of_serializationSurrogate_13() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___serializationSurrogate_13)); }
	inline Il2CppObject * get_serializationSurrogate_13() const { return ___serializationSurrogate_13; }
	inline Il2CppObject ** get_address_of_serializationSurrogate_13() { return &___serializationSurrogate_13; }
	inline void set_serializationSurrogate_13(Il2CppObject * value)
	{
		___serializationSurrogate_13 = value;
		Il2CppCodeGenWriteBarrier(&___serializationSurrogate_13, value);
	}

	inline static int32_t get_offset_of_context_14() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___context_14)); }
	inline StreamingContext_t1417235061  get_context_14() const { return ___context_14; }
	inline StreamingContext_t1417235061 * get_address_of_context_14() { return &___context_14; }
	inline void set_context_14(StreamingContext_t1417235061  value)
	{
		___context_14 = value;
	}

	inline static int32_t get_offset_of_memberTypesList_15() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___memberTypesList_15)); }
	inline List_1_t672924358 * get_memberTypesList_15() const { return ___memberTypesList_15; }
	inline List_1_t672924358 ** get_address_of_memberTypesList_15() { return &___memberTypesList_15; }
	inline void set_memberTypesList_15(List_1_t672924358 * value)
	{
		___memberTypesList_15 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypesList_15, value);
	}

	inline static int32_t get_offset_of_serObjectInfoInit_16() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___serObjectInfoInit_16)); }
	inline SerObjectInfoInit_t4094458531 * get_serObjectInfoInit_16() const { return ___serObjectInfoInit_16; }
	inline SerObjectInfoInit_t4094458531 ** get_address_of_serObjectInfoInit_16() { return &___serObjectInfoInit_16; }
	inline void set_serObjectInfoInit_16(SerObjectInfoInit_t4094458531 * value)
	{
		___serObjectInfoInit_16 = value;
		Il2CppCodeGenWriteBarrier(&___serObjectInfoInit_16, value);
	}

	inline static int32_t get_offset_of_formatterConverter_17() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573, ___formatterConverter_17)); }
	inline Il2CppObject * get_formatterConverter_17() const { return ___formatterConverter_17; }
	inline Il2CppObject ** get_address_of_formatterConverter_17() { return &___formatterConverter_17; }
	inline void set_formatterConverter_17(Il2CppObject * value)
	{
		___formatterConverter_17 = value;
		Il2CppCodeGenWriteBarrier(&___formatterConverter_17, value);
	}
};

struct ReadObjectInfo_t1645661573_StaticFields
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo::readObjectInfoCounter
	int32_t ___readObjectInfoCounter_1;

public:
	inline static int32_t get_offset_of_readObjectInfoCounter_1() { return static_cast<int32_t>(offsetof(ReadObjectInfo_t1645661573_StaticFields, ___readObjectInfoCounter_1)); }
	inline int32_t get_readObjectInfoCounter_1() const { return ___readObjectInfoCounter_1; }
	inline int32_t* get_address_of_readObjectInfoCounter_1() { return &___readObjectInfoCounter_1; }
	inline void set_readObjectInfoCounter_1(int32_t value)
	{
		___readObjectInfoCounter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
