﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct List_1_t1879707642;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection
struct  ValuesCollection_t1365611359  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection::entries
	List_1_t1879707642 * ___entries_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/ValuesCollection::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(ValuesCollection_t1365611359, ___entries_0)); }
	inline List_1_t1879707642 * get_entries_0() const { return ___entries_0; }
	inline List_1_t1879707642 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(List_1_t1879707642 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ValuesCollection_t1365611359, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
