﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Threading.Tasks.TaskScheduler,System.Object>
struct ConditionalWeakTable_2_t3845457102;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;
// System.EventHandler`1<System.Threading.Tasks.UnobservedTaskExceptionEventArgs>
struct EventHandler_1_t3973315282;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskScheduler
struct  TaskScheduler_t3932792796  : public Il2CppObject
{
public:

public:
};

struct TaskScheduler_t3932792796_StaticFields
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Threading.Tasks.TaskScheduler,System.Object> System.Threading.Tasks.TaskScheduler::s_activeTaskSchedulers
	ConditionalWeakTable_2_t3845457102 * ___s_activeTaskSchedulers_0;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.TaskScheduler::s_defaultTaskScheduler
	TaskScheduler_t3932792796 * ___s_defaultTaskScheduler_1;
	// System.EventHandler`1<System.Threading.Tasks.UnobservedTaskExceptionEventArgs> System.Threading.Tasks.TaskScheduler::_unobservedTaskException
	EventHandler_1_t3973315282 * ____unobservedTaskException_2;
	// System.Object System.Threading.Tasks.TaskScheduler::_unobservedTaskExceptionLockObject
	Il2CppObject * ____unobservedTaskExceptionLockObject_3;

public:
	inline static int32_t get_offset_of_s_activeTaskSchedulers_0() { return static_cast<int32_t>(offsetof(TaskScheduler_t3932792796_StaticFields, ___s_activeTaskSchedulers_0)); }
	inline ConditionalWeakTable_2_t3845457102 * get_s_activeTaskSchedulers_0() const { return ___s_activeTaskSchedulers_0; }
	inline ConditionalWeakTable_2_t3845457102 ** get_address_of_s_activeTaskSchedulers_0() { return &___s_activeTaskSchedulers_0; }
	inline void set_s_activeTaskSchedulers_0(ConditionalWeakTable_2_t3845457102 * value)
	{
		___s_activeTaskSchedulers_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_activeTaskSchedulers_0, value);
	}

	inline static int32_t get_offset_of_s_defaultTaskScheduler_1() { return static_cast<int32_t>(offsetof(TaskScheduler_t3932792796_StaticFields, ___s_defaultTaskScheduler_1)); }
	inline TaskScheduler_t3932792796 * get_s_defaultTaskScheduler_1() const { return ___s_defaultTaskScheduler_1; }
	inline TaskScheduler_t3932792796 ** get_address_of_s_defaultTaskScheduler_1() { return &___s_defaultTaskScheduler_1; }
	inline void set_s_defaultTaskScheduler_1(TaskScheduler_t3932792796 * value)
	{
		___s_defaultTaskScheduler_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_defaultTaskScheduler_1, value);
	}

	inline static int32_t get_offset_of__unobservedTaskException_2() { return static_cast<int32_t>(offsetof(TaskScheduler_t3932792796_StaticFields, ____unobservedTaskException_2)); }
	inline EventHandler_1_t3973315282 * get__unobservedTaskException_2() const { return ____unobservedTaskException_2; }
	inline EventHandler_1_t3973315282 ** get_address_of__unobservedTaskException_2() { return &____unobservedTaskException_2; }
	inline void set__unobservedTaskException_2(EventHandler_1_t3973315282 * value)
	{
		____unobservedTaskException_2 = value;
		Il2CppCodeGenWriteBarrier(&____unobservedTaskException_2, value);
	}

	inline static int32_t get_offset_of__unobservedTaskExceptionLockObject_3() { return static_cast<int32_t>(offsetof(TaskScheduler_t3932792796_StaticFields, ____unobservedTaskExceptionLockObject_3)); }
	inline Il2CppObject * get__unobservedTaskExceptionLockObject_3() const { return ____unobservedTaskExceptionLockObject_3; }
	inline Il2CppObject ** get_address_of__unobservedTaskExceptionLockObject_3() { return &____unobservedTaskExceptionLockObject_3; }
	inline void set__unobservedTaskExceptionLockObject_3(Il2CppObject * value)
	{
		____unobservedTaskExceptionLockObject_3 = value;
		Il2CppCodeGenWriteBarrier(&____unobservedTaskExceptionLockObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
