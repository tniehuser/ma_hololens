﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_WebResponse1895226051.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"

// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.IO.Stream
struct Stream_t3255436806;
// System.Uri
struct Uri_t19570940;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebResponse
struct  FileWebResponse_t1934981865  : public WebResponse_t1895226051
{
public:
	// System.Boolean System.Net.FileWebResponse::m_closed
	bool ___m_closed_1;
	// System.Int64 System.Net.FileWebResponse::m_contentLength
	int64_t ___m_contentLength_2;
	// System.IO.FileAccess System.Net.FileWebResponse::m_fileAccess
	int32_t ___m_fileAccess_3;
	// System.Net.WebHeaderCollection System.Net.FileWebResponse::m_headers
	WebHeaderCollection_t3028142837 * ___m_headers_4;
	// System.IO.Stream System.Net.FileWebResponse::m_stream
	Stream_t3255436806 * ___m_stream_5;
	// System.Uri System.Net.FileWebResponse::m_uri
	Uri_t19570940 * ___m_uri_6;

public:
	inline static int32_t get_offset_of_m_closed_1() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_closed_1)); }
	inline bool get_m_closed_1() const { return ___m_closed_1; }
	inline bool* get_address_of_m_closed_1() { return &___m_closed_1; }
	inline void set_m_closed_1(bool value)
	{
		___m_closed_1 = value;
	}

	inline static int32_t get_offset_of_m_contentLength_2() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_contentLength_2)); }
	inline int64_t get_m_contentLength_2() const { return ___m_contentLength_2; }
	inline int64_t* get_address_of_m_contentLength_2() { return &___m_contentLength_2; }
	inline void set_m_contentLength_2(int64_t value)
	{
		___m_contentLength_2 = value;
	}

	inline static int32_t get_offset_of_m_fileAccess_3() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_fileAccess_3)); }
	inline int32_t get_m_fileAccess_3() const { return ___m_fileAccess_3; }
	inline int32_t* get_address_of_m_fileAccess_3() { return &___m_fileAccess_3; }
	inline void set_m_fileAccess_3(int32_t value)
	{
		___m_fileAccess_3 = value;
	}

	inline static int32_t get_offset_of_m_headers_4() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_headers_4)); }
	inline WebHeaderCollection_t3028142837 * get_m_headers_4() const { return ___m_headers_4; }
	inline WebHeaderCollection_t3028142837 ** get_address_of_m_headers_4() { return &___m_headers_4; }
	inline void set_m_headers_4(WebHeaderCollection_t3028142837 * value)
	{
		___m_headers_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_headers_4, value);
	}

	inline static int32_t get_offset_of_m_stream_5() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_stream_5)); }
	inline Stream_t3255436806 * get_m_stream_5() const { return ___m_stream_5; }
	inline Stream_t3255436806 ** get_address_of_m_stream_5() { return &___m_stream_5; }
	inline void set_m_stream_5(Stream_t3255436806 * value)
	{
		___m_stream_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_stream_5, value);
	}

	inline static int32_t get_offset_of_m_uri_6() { return static_cast<int32_t>(offsetof(FileWebResponse_t1934981865, ___m_uri_6)); }
	inline Uri_t19570940 * get_m_uri_6() const { return ___m_uri_6; }
	inline Uri_t19570940 ** get_address_of_m_uri_6() { return &___m_uri_6; }
	inline void set_m_uri_6(Uri_t19570940 * value)
	{
		___m_uri_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_uri_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
