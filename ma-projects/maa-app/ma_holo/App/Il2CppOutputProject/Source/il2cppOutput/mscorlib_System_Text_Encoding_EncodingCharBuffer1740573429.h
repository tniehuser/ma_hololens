﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.Text.DecoderNLS
struct DecoderNLS_t1749238319;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t4206371382;
// System.Char
struct Char_t3454481338;
// System.Byte
struct Byte_t3683104436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding/EncodingCharBuffer
struct  EncodingCharBuffer_t1740573429  : public Il2CppObject
{
public:
	// System.Char* System.Text.Encoding/EncodingCharBuffer::chars
	Il2CppChar* ___chars_0;
	// System.Char* System.Text.Encoding/EncodingCharBuffer::charStart
	Il2CppChar* ___charStart_1;
	// System.Char* System.Text.Encoding/EncodingCharBuffer::charEnd
	Il2CppChar* ___charEnd_2;
	// System.Int32 System.Text.Encoding/EncodingCharBuffer::charCountResult
	int32_t ___charCountResult_3;
	// System.Text.Encoding System.Text.Encoding/EncodingCharBuffer::enc
	Encoding_t663144255 * ___enc_4;
	// System.Text.DecoderNLS System.Text.Encoding/EncodingCharBuffer::decoder
	DecoderNLS_t1749238319 * ___decoder_5;
	// System.Byte* System.Text.Encoding/EncodingCharBuffer::byteStart
	uint8_t* ___byteStart_6;
	// System.Byte* System.Text.Encoding/EncodingCharBuffer::byteEnd
	uint8_t* ___byteEnd_7;
	// System.Byte* System.Text.Encoding/EncodingCharBuffer::bytes
	uint8_t* ___bytes_8;
	// System.Text.DecoderFallbackBuffer System.Text.Encoding/EncodingCharBuffer::fallbackBuffer
	DecoderFallbackBuffer_t4206371382 * ___fallbackBuffer_9;

public:
	inline static int32_t get_offset_of_chars_0() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___chars_0)); }
	inline Il2CppChar* get_chars_0() const { return ___chars_0; }
	inline Il2CppChar** get_address_of_chars_0() { return &___chars_0; }
	inline void set_chars_0(Il2CppChar* value)
	{
		___chars_0 = value;
	}

	inline static int32_t get_offset_of_charStart_1() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___charStart_1)); }
	inline Il2CppChar* get_charStart_1() const { return ___charStart_1; }
	inline Il2CppChar** get_address_of_charStart_1() { return &___charStart_1; }
	inline void set_charStart_1(Il2CppChar* value)
	{
		___charStart_1 = value;
	}

	inline static int32_t get_offset_of_charEnd_2() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___charEnd_2)); }
	inline Il2CppChar* get_charEnd_2() const { return ___charEnd_2; }
	inline Il2CppChar** get_address_of_charEnd_2() { return &___charEnd_2; }
	inline void set_charEnd_2(Il2CppChar* value)
	{
		___charEnd_2 = value;
	}

	inline static int32_t get_offset_of_charCountResult_3() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___charCountResult_3)); }
	inline int32_t get_charCountResult_3() const { return ___charCountResult_3; }
	inline int32_t* get_address_of_charCountResult_3() { return &___charCountResult_3; }
	inline void set_charCountResult_3(int32_t value)
	{
		___charCountResult_3 = value;
	}

	inline static int32_t get_offset_of_enc_4() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___enc_4)); }
	inline Encoding_t663144255 * get_enc_4() const { return ___enc_4; }
	inline Encoding_t663144255 ** get_address_of_enc_4() { return &___enc_4; }
	inline void set_enc_4(Encoding_t663144255 * value)
	{
		___enc_4 = value;
		Il2CppCodeGenWriteBarrier(&___enc_4, value);
	}

	inline static int32_t get_offset_of_decoder_5() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___decoder_5)); }
	inline DecoderNLS_t1749238319 * get_decoder_5() const { return ___decoder_5; }
	inline DecoderNLS_t1749238319 ** get_address_of_decoder_5() { return &___decoder_5; }
	inline void set_decoder_5(DecoderNLS_t1749238319 * value)
	{
		___decoder_5 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_5, value);
	}

	inline static int32_t get_offset_of_byteStart_6() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___byteStart_6)); }
	inline uint8_t* get_byteStart_6() const { return ___byteStart_6; }
	inline uint8_t** get_address_of_byteStart_6() { return &___byteStart_6; }
	inline void set_byteStart_6(uint8_t* value)
	{
		___byteStart_6 = value;
	}

	inline static int32_t get_offset_of_byteEnd_7() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___byteEnd_7)); }
	inline uint8_t* get_byteEnd_7() const { return ___byteEnd_7; }
	inline uint8_t** get_address_of_byteEnd_7() { return &___byteEnd_7; }
	inline void set_byteEnd_7(uint8_t* value)
	{
		___byteEnd_7 = value;
	}

	inline static int32_t get_offset_of_bytes_8() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___bytes_8)); }
	inline uint8_t* get_bytes_8() const { return ___bytes_8; }
	inline uint8_t** get_address_of_bytes_8() { return &___bytes_8; }
	inline void set_bytes_8(uint8_t* value)
	{
		___bytes_8 = value;
	}

	inline static int32_t get_offset_of_fallbackBuffer_9() { return static_cast<int32_t>(offsetof(EncodingCharBuffer_t1740573429, ___fallbackBuffer_9)); }
	inline DecoderFallbackBuffer_t4206371382 * get_fallbackBuffer_9() const { return ___fallbackBuffer_9; }
	inline DecoderFallbackBuffer_t4206371382 ** get_address_of_fallbackBuffer_9() { return &___fallbackBuffer_9; }
	inline void set_fallbackBuffer_9(DecoderFallbackBuffer_t4206371382 * value)
	{
		___fallbackBuffer_9 = value;
		Il2CppCodeGenWriteBarrier(&___fallbackBuffer_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
