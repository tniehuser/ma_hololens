﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SchemaBuilder908297946.h"

// System.Xml.Schema.XsdBuilder/State[]
struct StateU5BU5D_t717950118;
// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[]
struct XsdAttributeEntryU5BU5D_t3765641446;
// System.Xml.Schema.XsdBuilder/XsdEntry[]
struct XsdEntryU5BU5D_t842229578;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.Schema.XsdBuilder/XsdEntry
struct XsdEntry_t2653064619;
// System.Xml.HWStack
struct HWStack_t738999989;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_t3277730824;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4015859774;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t4086789226;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.XmlSchemaComplexContent
struct XmlSchemaComplexContent_t2065934415;
// System.Xml.Schema.XmlSchemaComplexContentExtension
struct XmlSchemaComplexContentExtension_t655218998;
// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct XmlSchemaComplexContentRestriction_t1722137421;
// System.Xml.Schema.XmlSchemaSimpleContent
struct XmlSchemaSimpleContent_t2303138587;
// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct XmlSchemaSimpleContentExtension_t3718357176;
// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct XmlSchemaSimpleContentRestriction_t2728776481;
// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct XmlSchemaSimpleTypeUnion_t91327365;
// System.Xml.Schema.XmlSchemaSimpleTypeList
struct XmlSchemaSimpleTypeList_t2170323082;
// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct XmlSchemaSimpleTypeRestriction_t1099506232;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t4189650927;
// System.Xml.Schema.XmlSchemaGroupRef
struct XmlSchemaGroupRef_t3082205844;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_t1805755215;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t654568461;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_t728414063;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;
// System.Xml.Schema.XmlSchemaAttributeGroup
struct XmlSchemaAttributeGroup_t491156493;
// System.Xml.Schema.XmlSchemaAttributeGroupRef
struct XmlSchemaAttributeGroupRef_t825996660;
// System.Xml.Schema.XmlSchemaNotation
struct XmlSchemaNotation_t346281646;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t1058613623;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t604820427;
// System.Xml.Schema.XmlSchemaInclude
struct XmlSchemaInclude_t2752556710;
// System.Xml.Schema.XmlSchemaImport
struct XmlSchemaImport_t250324363;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t2400301303;
// System.Xml.Schema.XmlSchemaAppInfo
struct XmlSchemaAppInfo_t2033489551;
// System.Xml.Schema.XmlSchemaDocumentation
struct XmlSchemaDocumentation_t3832803992;
// System.Xml.Schema.XmlSchemaFacet
struct XmlSchemaFacet_t614309579;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t2118142256;
// System.Xml.Schema.XmlSchemaRedefine
struct XmlSchemaRedefine_t3478619248;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder
struct  XsdBuilder_t2784092428  : public SchemaBuilder_t908297946
{
public:
	// System.Xml.XmlReader System.Xml.Schema.XsdBuilder::reader
	XmlReader_t3675626668 * ___reader_61;
	// System.Xml.PositionInfo System.Xml.Schema.XsdBuilder::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_62;
	// System.Xml.Schema.XsdBuilder/XsdEntry System.Xml.Schema.XsdBuilder::currentEntry
	XsdEntry_t2653064619 * ___currentEntry_63;
	// System.Xml.Schema.XsdBuilder/XsdEntry System.Xml.Schema.XsdBuilder::nextEntry
	XsdEntry_t2653064619 * ___nextEntry_64;
	// System.Boolean System.Xml.Schema.XsdBuilder::hasChild
	bool ___hasChild_65;
	// System.Xml.HWStack System.Xml.Schema.XsdBuilder::stateHistory
	HWStack_t738999989 * ___stateHistory_66;
	// System.Collections.Stack System.Xml.Schema.XsdBuilder::containerStack
	Stack_t1043988394 * ___containerStack_67;
	// System.Xml.XmlNameTable System.Xml.Schema.XsdBuilder::nameTable
	XmlNameTable_t1345805268 * ___nameTable_68;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XsdBuilder::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_69;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdBuilder::namespaceManager
	XmlNamespaceManager_t486731501 * ___namespaceManager_70;
	// System.Boolean System.Xml.Schema.XsdBuilder::canIncludeImport
	bool ___canIncludeImport_71;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XsdBuilder::schema
	XmlSchema_t880472818 * ___schema_72;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XsdBuilder::xso
	XmlSchemaObject_t2050913741 * ___xso_73;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XsdBuilder::element
	XmlSchemaElement_t2433337156 * ___element_74;
	// System.Xml.Schema.XmlSchemaAny System.Xml.Schema.XsdBuilder::anyElement
	XmlSchemaAny_t3277730824 * ___anyElement_75;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XsdBuilder::attribute
	XmlSchemaAttribute_t4015859774 * ___attribute_76;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XsdBuilder::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_77;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XsdBuilder::complexType
	XmlSchemaComplexType_t4086789226 * ___complexType_78;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XsdBuilder::simpleType
	XmlSchemaSimpleType_t248156492 * ___simpleType_79;
	// System.Xml.Schema.XmlSchemaComplexContent System.Xml.Schema.XsdBuilder::complexContent
	XmlSchemaComplexContent_t2065934415 * ___complexContent_80;
	// System.Xml.Schema.XmlSchemaComplexContentExtension System.Xml.Schema.XsdBuilder::complexContentExtension
	XmlSchemaComplexContentExtension_t655218998 * ___complexContentExtension_81;
	// System.Xml.Schema.XmlSchemaComplexContentRestriction System.Xml.Schema.XsdBuilder::complexContentRestriction
	XmlSchemaComplexContentRestriction_t1722137421 * ___complexContentRestriction_82;
	// System.Xml.Schema.XmlSchemaSimpleContent System.Xml.Schema.XsdBuilder::simpleContent
	XmlSchemaSimpleContent_t2303138587 * ___simpleContent_83;
	// System.Xml.Schema.XmlSchemaSimpleContentExtension System.Xml.Schema.XsdBuilder::simpleContentExtension
	XmlSchemaSimpleContentExtension_t3718357176 * ___simpleContentExtension_84;
	// System.Xml.Schema.XmlSchemaSimpleContentRestriction System.Xml.Schema.XsdBuilder::simpleContentRestriction
	XmlSchemaSimpleContentRestriction_t2728776481 * ___simpleContentRestriction_85;
	// System.Xml.Schema.XmlSchemaSimpleTypeUnion System.Xml.Schema.XsdBuilder::simpleTypeUnion
	XmlSchemaSimpleTypeUnion_t91327365 * ___simpleTypeUnion_86;
	// System.Xml.Schema.XmlSchemaSimpleTypeList System.Xml.Schema.XsdBuilder::simpleTypeList
	XmlSchemaSimpleTypeList_t2170323082 * ___simpleTypeList_87;
	// System.Xml.Schema.XmlSchemaSimpleTypeRestriction System.Xml.Schema.XsdBuilder::simpleTypeRestriction
	XmlSchemaSimpleTypeRestriction_t1099506232 * ___simpleTypeRestriction_88;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XsdBuilder::group
	XmlSchemaGroup_t4189650927 * ___group_89;
	// System.Xml.Schema.XmlSchemaGroupRef System.Xml.Schema.XsdBuilder::groupRef
	XmlSchemaGroupRef_t3082205844 * ___groupRef_90;
	// System.Xml.Schema.XmlSchemaAll System.Xml.Schema.XsdBuilder::all
	XmlSchemaAll_t1805755215 * ___all_91;
	// System.Xml.Schema.XmlSchemaChoice System.Xml.Schema.XsdBuilder::choice
	XmlSchemaChoice_t654568461 * ___choice_92;
	// System.Xml.Schema.XmlSchemaSequence System.Xml.Schema.XsdBuilder::sequence
	XmlSchemaSequence_t728414063 * ___sequence_93;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XsdBuilder::particle
	XmlSchemaParticle_t3365045970 * ___particle_94;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XsdBuilder::attributeGroup
	XmlSchemaAttributeGroup_t491156493 * ___attributeGroup_95;
	// System.Xml.Schema.XmlSchemaAttributeGroupRef System.Xml.Schema.XsdBuilder::attributeGroupRef
	XmlSchemaAttributeGroupRef_t825996660 * ___attributeGroupRef_96;
	// System.Xml.Schema.XmlSchemaNotation System.Xml.Schema.XsdBuilder::notation
	XmlSchemaNotation_t346281646 * ___notation_97;
	// System.Xml.Schema.XmlSchemaIdentityConstraint System.Xml.Schema.XsdBuilder::identityConstraint
	XmlSchemaIdentityConstraint_t1058613623 * ___identityConstraint_98;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XsdBuilder::xpath
	XmlSchemaXPath_t604820427 * ___xpath_99;
	// System.Xml.Schema.XmlSchemaInclude System.Xml.Schema.XsdBuilder::include
	XmlSchemaInclude_t2752556710 * ___include_100;
	// System.Xml.Schema.XmlSchemaImport System.Xml.Schema.XsdBuilder::import
	XmlSchemaImport_t250324363 * ___import_101;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XsdBuilder::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_102;
	// System.Xml.Schema.XmlSchemaAppInfo System.Xml.Schema.XsdBuilder::appInfo
	XmlSchemaAppInfo_t2033489551 * ___appInfo_103;
	// System.Xml.Schema.XmlSchemaDocumentation System.Xml.Schema.XsdBuilder::documentation
	XmlSchemaDocumentation_t3832803992 * ___documentation_104;
	// System.Xml.Schema.XmlSchemaFacet System.Xml.Schema.XsdBuilder::facet
	XmlSchemaFacet_t614309579 * ___facet_105;
	// System.Xml.XmlNode[] System.Xml.Schema.XsdBuilder::markup
	XmlNodeU5BU5D_t2118142256* ___markup_106;
	// System.Xml.Schema.XmlSchemaRedefine System.Xml.Schema.XsdBuilder::redefine
	XmlSchemaRedefine_t3478619248 * ___redefine_107;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XsdBuilder::validationEventHandler
	ValidationEventHandler_t1580700381 * ___validationEventHandler_108;
	// System.Collections.ArrayList System.Xml.Schema.XsdBuilder::unhandledAttributes
	ArrayList_t4252133567 * ___unhandledAttributes_109;
	// System.Collections.Hashtable System.Xml.Schema.XsdBuilder::namespaces
	Hashtable_t909839986 * ___namespaces_110;

public:
	inline static int32_t get_offset_of_reader_61() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___reader_61)); }
	inline XmlReader_t3675626668 * get_reader_61() const { return ___reader_61; }
	inline XmlReader_t3675626668 ** get_address_of_reader_61() { return &___reader_61; }
	inline void set_reader_61(XmlReader_t3675626668 * value)
	{
		___reader_61 = value;
		Il2CppCodeGenWriteBarrier(&___reader_61, value);
	}

	inline static int32_t get_offset_of_positionInfo_62() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___positionInfo_62)); }
	inline PositionInfo_t3273236083 * get_positionInfo_62() const { return ___positionInfo_62; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_62() { return &___positionInfo_62; }
	inline void set_positionInfo_62(PositionInfo_t3273236083 * value)
	{
		___positionInfo_62 = value;
		Il2CppCodeGenWriteBarrier(&___positionInfo_62, value);
	}

	inline static int32_t get_offset_of_currentEntry_63() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___currentEntry_63)); }
	inline XsdEntry_t2653064619 * get_currentEntry_63() const { return ___currentEntry_63; }
	inline XsdEntry_t2653064619 ** get_address_of_currentEntry_63() { return &___currentEntry_63; }
	inline void set_currentEntry_63(XsdEntry_t2653064619 * value)
	{
		___currentEntry_63 = value;
		Il2CppCodeGenWriteBarrier(&___currentEntry_63, value);
	}

	inline static int32_t get_offset_of_nextEntry_64() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___nextEntry_64)); }
	inline XsdEntry_t2653064619 * get_nextEntry_64() const { return ___nextEntry_64; }
	inline XsdEntry_t2653064619 ** get_address_of_nextEntry_64() { return &___nextEntry_64; }
	inline void set_nextEntry_64(XsdEntry_t2653064619 * value)
	{
		___nextEntry_64 = value;
		Il2CppCodeGenWriteBarrier(&___nextEntry_64, value);
	}

	inline static int32_t get_offset_of_hasChild_65() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___hasChild_65)); }
	inline bool get_hasChild_65() const { return ___hasChild_65; }
	inline bool* get_address_of_hasChild_65() { return &___hasChild_65; }
	inline void set_hasChild_65(bool value)
	{
		___hasChild_65 = value;
	}

	inline static int32_t get_offset_of_stateHistory_66() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___stateHistory_66)); }
	inline HWStack_t738999989 * get_stateHistory_66() const { return ___stateHistory_66; }
	inline HWStack_t738999989 ** get_address_of_stateHistory_66() { return &___stateHistory_66; }
	inline void set_stateHistory_66(HWStack_t738999989 * value)
	{
		___stateHistory_66 = value;
		Il2CppCodeGenWriteBarrier(&___stateHistory_66, value);
	}

	inline static int32_t get_offset_of_containerStack_67() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___containerStack_67)); }
	inline Stack_t1043988394 * get_containerStack_67() const { return ___containerStack_67; }
	inline Stack_t1043988394 ** get_address_of_containerStack_67() { return &___containerStack_67; }
	inline void set_containerStack_67(Stack_t1043988394 * value)
	{
		___containerStack_67 = value;
		Il2CppCodeGenWriteBarrier(&___containerStack_67, value);
	}

	inline static int32_t get_offset_of_nameTable_68() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___nameTable_68)); }
	inline XmlNameTable_t1345805268 * get_nameTable_68() const { return ___nameTable_68; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_68() { return &___nameTable_68; }
	inline void set_nameTable_68(XmlNameTable_t1345805268 * value)
	{
		___nameTable_68 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_68, value);
	}

	inline static int32_t get_offset_of_schemaNames_69() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___schemaNames_69)); }
	inline SchemaNames_t1619962557 * get_schemaNames_69() const { return ___schemaNames_69; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_69() { return &___schemaNames_69; }
	inline void set_schemaNames_69(SchemaNames_t1619962557 * value)
	{
		___schemaNames_69 = value;
		Il2CppCodeGenWriteBarrier(&___schemaNames_69, value);
	}

	inline static int32_t get_offset_of_namespaceManager_70() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___namespaceManager_70)); }
	inline XmlNamespaceManager_t486731501 * get_namespaceManager_70() const { return ___namespaceManager_70; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_namespaceManager_70() { return &___namespaceManager_70; }
	inline void set_namespaceManager_70(XmlNamespaceManager_t486731501 * value)
	{
		___namespaceManager_70 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceManager_70, value);
	}

	inline static int32_t get_offset_of_canIncludeImport_71() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___canIncludeImport_71)); }
	inline bool get_canIncludeImport_71() const { return ___canIncludeImport_71; }
	inline bool* get_address_of_canIncludeImport_71() { return &___canIncludeImport_71; }
	inline void set_canIncludeImport_71(bool value)
	{
		___canIncludeImport_71 = value;
	}

	inline static int32_t get_offset_of_schema_72() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___schema_72)); }
	inline XmlSchema_t880472818 * get_schema_72() const { return ___schema_72; }
	inline XmlSchema_t880472818 ** get_address_of_schema_72() { return &___schema_72; }
	inline void set_schema_72(XmlSchema_t880472818 * value)
	{
		___schema_72 = value;
		Il2CppCodeGenWriteBarrier(&___schema_72, value);
	}

	inline static int32_t get_offset_of_xso_73() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___xso_73)); }
	inline XmlSchemaObject_t2050913741 * get_xso_73() const { return ___xso_73; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_xso_73() { return &___xso_73; }
	inline void set_xso_73(XmlSchemaObject_t2050913741 * value)
	{
		___xso_73 = value;
		Il2CppCodeGenWriteBarrier(&___xso_73, value);
	}

	inline static int32_t get_offset_of_element_74() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___element_74)); }
	inline XmlSchemaElement_t2433337156 * get_element_74() const { return ___element_74; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_element_74() { return &___element_74; }
	inline void set_element_74(XmlSchemaElement_t2433337156 * value)
	{
		___element_74 = value;
		Il2CppCodeGenWriteBarrier(&___element_74, value);
	}

	inline static int32_t get_offset_of_anyElement_75() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___anyElement_75)); }
	inline XmlSchemaAny_t3277730824 * get_anyElement_75() const { return ___anyElement_75; }
	inline XmlSchemaAny_t3277730824 ** get_address_of_anyElement_75() { return &___anyElement_75; }
	inline void set_anyElement_75(XmlSchemaAny_t3277730824 * value)
	{
		___anyElement_75 = value;
		Il2CppCodeGenWriteBarrier(&___anyElement_75, value);
	}

	inline static int32_t get_offset_of_attribute_76() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___attribute_76)); }
	inline XmlSchemaAttribute_t4015859774 * get_attribute_76() const { return ___attribute_76; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_attribute_76() { return &___attribute_76; }
	inline void set_attribute_76(XmlSchemaAttribute_t4015859774 * value)
	{
		___attribute_76 = value;
		Il2CppCodeGenWriteBarrier(&___attribute_76, value);
	}

	inline static int32_t get_offset_of_anyAttribute_77() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___anyAttribute_77)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_77() const { return ___anyAttribute_77; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_77() { return &___anyAttribute_77; }
	inline void set_anyAttribute_77(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_77 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_77, value);
	}

	inline static int32_t get_offset_of_complexType_78() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___complexType_78)); }
	inline XmlSchemaComplexType_t4086789226 * get_complexType_78() const { return ___complexType_78; }
	inline XmlSchemaComplexType_t4086789226 ** get_address_of_complexType_78() { return &___complexType_78; }
	inline void set_complexType_78(XmlSchemaComplexType_t4086789226 * value)
	{
		___complexType_78 = value;
		Il2CppCodeGenWriteBarrier(&___complexType_78, value);
	}

	inline static int32_t get_offset_of_simpleType_79() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleType_79)); }
	inline XmlSchemaSimpleType_t248156492 * get_simpleType_79() const { return ___simpleType_79; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_simpleType_79() { return &___simpleType_79; }
	inline void set_simpleType_79(XmlSchemaSimpleType_t248156492 * value)
	{
		___simpleType_79 = value;
		Il2CppCodeGenWriteBarrier(&___simpleType_79, value);
	}

	inline static int32_t get_offset_of_complexContent_80() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___complexContent_80)); }
	inline XmlSchemaComplexContent_t2065934415 * get_complexContent_80() const { return ___complexContent_80; }
	inline XmlSchemaComplexContent_t2065934415 ** get_address_of_complexContent_80() { return &___complexContent_80; }
	inline void set_complexContent_80(XmlSchemaComplexContent_t2065934415 * value)
	{
		___complexContent_80 = value;
		Il2CppCodeGenWriteBarrier(&___complexContent_80, value);
	}

	inline static int32_t get_offset_of_complexContentExtension_81() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___complexContentExtension_81)); }
	inline XmlSchemaComplexContentExtension_t655218998 * get_complexContentExtension_81() const { return ___complexContentExtension_81; }
	inline XmlSchemaComplexContentExtension_t655218998 ** get_address_of_complexContentExtension_81() { return &___complexContentExtension_81; }
	inline void set_complexContentExtension_81(XmlSchemaComplexContentExtension_t655218998 * value)
	{
		___complexContentExtension_81 = value;
		Il2CppCodeGenWriteBarrier(&___complexContentExtension_81, value);
	}

	inline static int32_t get_offset_of_complexContentRestriction_82() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___complexContentRestriction_82)); }
	inline XmlSchemaComplexContentRestriction_t1722137421 * get_complexContentRestriction_82() const { return ___complexContentRestriction_82; }
	inline XmlSchemaComplexContentRestriction_t1722137421 ** get_address_of_complexContentRestriction_82() { return &___complexContentRestriction_82; }
	inline void set_complexContentRestriction_82(XmlSchemaComplexContentRestriction_t1722137421 * value)
	{
		___complexContentRestriction_82 = value;
		Il2CppCodeGenWriteBarrier(&___complexContentRestriction_82, value);
	}

	inline static int32_t get_offset_of_simpleContent_83() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleContent_83)); }
	inline XmlSchemaSimpleContent_t2303138587 * get_simpleContent_83() const { return ___simpleContent_83; }
	inline XmlSchemaSimpleContent_t2303138587 ** get_address_of_simpleContent_83() { return &___simpleContent_83; }
	inline void set_simpleContent_83(XmlSchemaSimpleContent_t2303138587 * value)
	{
		___simpleContent_83 = value;
		Il2CppCodeGenWriteBarrier(&___simpleContent_83, value);
	}

	inline static int32_t get_offset_of_simpleContentExtension_84() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleContentExtension_84)); }
	inline XmlSchemaSimpleContentExtension_t3718357176 * get_simpleContentExtension_84() const { return ___simpleContentExtension_84; }
	inline XmlSchemaSimpleContentExtension_t3718357176 ** get_address_of_simpleContentExtension_84() { return &___simpleContentExtension_84; }
	inline void set_simpleContentExtension_84(XmlSchemaSimpleContentExtension_t3718357176 * value)
	{
		___simpleContentExtension_84 = value;
		Il2CppCodeGenWriteBarrier(&___simpleContentExtension_84, value);
	}

	inline static int32_t get_offset_of_simpleContentRestriction_85() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleContentRestriction_85)); }
	inline XmlSchemaSimpleContentRestriction_t2728776481 * get_simpleContentRestriction_85() const { return ___simpleContentRestriction_85; }
	inline XmlSchemaSimpleContentRestriction_t2728776481 ** get_address_of_simpleContentRestriction_85() { return &___simpleContentRestriction_85; }
	inline void set_simpleContentRestriction_85(XmlSchemaSimpleContentRestriction_t2728776481 * value)
	{
		___simpleContentRestriction_85 = value;
		Il2CppCodeGenWriteBarrier(&___simpleContentRestriction_85, value);
	}

	inline static int32_t get_offset_of_simpleTypeUnion_86() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleTypeUnion_86)); }
	inline XmlSchemaSimpleTypeUnion_t91327365 * get_simpleTypeUnion_86() const { return ___simpleTypeUnion_86; }
	inline XmlSchemaSimpleTypeUnion_t91327365 ** get_address_of_simpleTypeUnion_86() { return &___simpleTypeUnion_86; }
	inline void set_simpleTypeUnion_86(XmlSchemaSimpleTypeUnion_t91327365 * value)
	{
		___simpleTypeUnion_86 = value;
		Il2CppCodeGenWriteBarrier(&___simpleTypeUnion_86, value);
	}

	inline static int32_t get_offset_of_simpleTypeList_87() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleTypeList_87)); }
	inline XmlSchemaSimpleTypeList_t2170323082 * get_simpleTypeList_87() const { return ___simpleTypeList_87; }
	inline XmlSchemaSimpleTypeList_t2170323082 ** get_address_of_simpleTypeList_87() { return &___simpleTypeList_87; }
	inline void set_simpleTypeList_87(XmlSchemaSimpleTypeList_t2170323082 * value)
	{
		___simpleTypeList_87 = value;
		Il2CppCodeGenWriteBarrier(&___simpleTypeList_87, value);
	}

	inline static int32_t get_offset_of_simpleTypeRestriction_88() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___simpleTypeRestriction_88)); }
	inline XmlSchemaSimpleTypeRestriction_t1099506232 * get_simpleTypeRestriction_88() const { return ___simpleTypeRestriction_88; }
	inline XmlSchemaSimpleTypeRestriction_t1099506232 ** get_address_of_simpleTypeRestriction_88() { return &___simpleTypeRestriction_88; }
	inline void set_simpleTypeRestriction_88(XmlSchemaSimpleTypeRestriction_t1099506232 * value)
	{
		___simpleTypeRestriction_88 = value;
		Il2CppCodeGenWriteBarrier(&___simpleTypeRestriction_88, value);
	}

	inline static int32_t get_offset_of_group_89() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___group_89)); }
	inline XmlSchemaGroup_t4189650927 * get_group_89() const { return ___group_89; }
	inline XmlSchemaGroup_t4189650927 ** get_address_of_group_89() { return &___group_89; }
	inline void set_group_89(XmlSchemaGroup_t4189650927 * value)
	{
		___group_89 = value;
		Il2CppCodeGenWriteBarrier(&___group_89, value);
	}

	inline static int32_t get_offset_of_groupRef_90() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___groupRef_90)); }
	inline XmlSchemaGroupRef_t3082205844 * get_groupRef_90() const { return ___groupRef_90; }
	inline XmlSchemaGroupRef_t3082205844 ** get_address_of_groupRef_90() { return &___groupRef_90; }
	inline void set_groupRef_90(XmlSchemaGroupRef_t3082205844 * value)
	{
		___groupRef_90 = value;
		Il2CppCodeGenWriteBarrier(&___groupRef_90, value);
	}

	inline static int32_t get_offset_of_all_91() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___all_91)); }
	inline XmlSchemaAll_t1805755215 * get_all_91() const { return ___all_91; }
	inline XmlSchemaAll_t1805755215 ** get_address_of_all_91() { return &___all_91; }
	inline void set_all_91(XmlSchemaAll_t1805755215 * value)
	{
		___all_91 = value;
		Il2CppCodeGenWriteBarrier(&___all_91, value);
	}

	inline static int32_t get_offset_of_choice_92() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___choice_92)); }
	inline XmlSchemaChoice_t654568461 * get_choice_92() const { return ___choice_92; }
	inline XmlSchemaChoice_t654568461 ** get_address_of_choice_92() { return &___choice_92; }
	inline void set_choice_92(XmlSchemaChoice_t654568461 * value)
	{
		___choice_92 = value;
		Il2CppCodeGenWriteBarrier(&___choice_92, value);
	}

	inline static int32_t get_offset_of_sequence_93() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___sequence_93)); }
	inline XmlSchemaSequence_t728414063 * get_sequence_93() const { return ___sequence_93; }
	inline XmlSchemaSequence_t728414063 ** get_address_of_sequence_93() { return &___sequence_93; }
	inline void set_sequence_93(XmlSchemaSequence_t728414063 * value)
	{
		___sequence_93 = value;
		Il2CppCodeGenWriteBarrier(&___sequence_93, value);
	}

	inline static int32_t get_offset_of_particle_94() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___particle_94)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_94() const { return ___particle_94; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_94() { return &___particle_94; }
	inline void set_particle_94(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_94 = value;
		Il2CppCodeGenWriteBarrier(&___particle_94, value);
	}

	inline static int32_t get_offset_of_attributeGroup_95() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___attributeGroup_95)); }
	inline XmlSchemaAttributeGroup_t491156493 * get_attributeGroup_95() const { return ___attributeGroup_95; }
	inline XmlSchemaAttributeGroup_t491156493 ** get_address_of_attributeGroup_95() { return &___attributeGroup_95; }
	inline void set_attributeGroup_95(XmlSchemaAttributeGroup_t491156493 * value)
	{
		___attributeGroup_95 = value;
		Il2CppCodeGenWriteBarrier(&___attributeGroup_95, value);
	}

	inline static int32_t get_offset_of_attributeGroupRef_96() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___attributeGroupRef_96)); }
	inline XmlSchemaAttributeGroupRef_t825996660 * get_attributeGroupRef_96() const { return ___attributeGroupRef_96; }
	inline XmlSchemaAttributeGroupRef_t825996660 ** get_address_of_attributeGroupRef_96() { return &___attributeGroupRef_96; }
	inline void set_attributeGroupRef_96(XmlSchemaAttributeGroupRef_t825996660 * value)
	{
		___attributeGroupRef_96 = value;
		Il2CppCodeGenWriteBarrier(&___attributeGroupRef_96, value);
	}

	inline static int32_t get_offset_of_notation_97() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___notation_97)); }
	inline XmlSchemaNotation_t346281646 * get_notation_97() const { return ___notation_97; }
	inline XmlSchemaNotation_t346281646 ** get_address_of_notation_97() { return &___notation_97; }
	inline void set_notation_97(XmlSchemaNotation_t346281646 * value)
	{
		___notation_97 = value;
		Il2CppCodeGenWriteBarrier(&___notation_97, value);
	}

	inline static int32_t get_offset_of_identityConstraint_98() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___identityConstraint_98)); }
	inline XmlSchemaIdentityConstraint_t1058613623 * get_identityConstraint_98() const { return ___identityConstraint_98; }
	inline XmlSchemaIdentityConstraint_t1058613623 ** get_address_of_identityConstraint_98() { return &___identityConstraint_98; }
	inline void set_identityConstraint_98(XmlSchemaIdentityConstraint_t1058613623 * value)
	{
		___identityConstraint_98 = value;
		Il2CppCodeGenWriteBarrier(&___identityConstraint_98, value);
	}

	inline static int32_t get_offset_of_xpath_99() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___xpath_99)); }
	inline XmlSchemaXPath_t604820427 * get_xpath_99() const { return ___xpath_99; }
	inline XmlSchemaXPath_t604820427 ** get_address_of_xpath_99() { return &___xpath_99; }
	inline void set_xpath_99(XmlSchemaXPath_t604820427 * value)
	{
		___xpath_99 = value;
		Il2CppCodeGenWriteBarrier(&___xpath_99, value);
	}

	inline static int32_t get_offset_of_include_100() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___include_100)); }
	inline XmlSchemaInclude_t2752556710 * get_include_100() const { return ___include_100; }
	inline XmlSchemaInclude_t2752556710 ** get_address_of_include_100() { return &___include_100; }
	inline void set_include_100(XmlSchemaInclude_t2752556710 * value)
	{
		___include_100 = value;
		Il2CppCodeGenWriteBarrier(&___include_100, value);
	}

	inline static int32_t get_offset_of_import_101() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___import_101)); }
	inline XmlSchemaImport_t250324363 * get_import_101() const { return ___import_101; }
	inline XmlSchemaImport_t250324363 ** get_address_of_import_101() { return &___import_101; }
	inline void set_import_101(XmlSchemaImport_t250324363 * value)
	{
		___import_101 = value;
		Il2CppCodeGenWriteBarrier(&___import_101, value);
	}

	inline static int32_t get_offset_of_annotation_102() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___annotation_102)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_102() const { return ___annotation_102; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_102() { return &___annotation_102; }
	inline void set_annotation_102(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_102 = value;
		Il2CppCodeGenWriteBarrier(&___annotation_102, value);
	}

	inline static int32_t get_offset_of_appInfo_103() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___appInfo_103)); }
	inline XmlSchemaAppInfo_t2033489551 * get_appInfo_103() const { return ___appInfo_103; }
	inline XmlSchemaAppInfo_t2033489551 ** get_address_of_appInfo_103() { return &___appInfo_103; }
	inline void set_appInfo_103(XmlSchemaAppInfo_t2033489551 * value)
	{
		___appInfo_103 = value;
		Il2CppCodeGenWriteBarrier(&___appInfo_103, value);
	}

	inline static int32_t get_offset_of_documentation_104() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___documentation_104)); }
	inline XmlSchemaDocumentation_t3832803992 * get_documentation_104() const { return ___documentation_104; }
	inline XmlSchemaDocumentation_t3832803992 ** get_address_of_documentation_104() { return &___documentation_104; }
	inline void set_documentation_104(XmlSchemaDocumentation_t3832803992 * value)
	{
		___documentation_104 = value;
		Il2CppCodeGenWriteBarrier(&___documentation_104, value);
	}

	inline static int32_t get_offset_of_facet_105() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___facet_105)); }
	inline XmlSchemaFacet_t614309579 * get_facet_105() const { return ___facet_105; }
	inline XmlSchemaFacet_t614309579 ** get_address_of_facet_105() { return &___facet_105; }
	inline void set_facet_105(XmlSchemaFacet_t614309579 * value)
	{
		___facet_105 = value;
		Il2CppCodeGenWriteBarrier(&___facet_105, value);
	}

	inline static int32_t get_offset_of_markup_106() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___markup_106)); }
	inline XmlNodeU5BU5D_t2118142256* get_markup_106() const { return ___markup_106; }
	inline XmlNodeU5BU5D_t2118142256** get_address_of_markup_106() { return &___markup_106; }
	inline void set_markup_106(XmlNodeU5BU5D_t2118142256* value)
	{
		___markup_106 = value;
		Il2CppCodeGenWriteBarrier(&___markup_106, value);
	}

	inline static int32_t get_offset_of_redefine_107() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___redefine_107)); }
	inline XmlSchemaRedefine_t3478619248 * get_redefine_107() const { return ___redefine_107; }
	inline XmlSchemaRedefine_t3478619248 ** get_address_of_redefine_107() { return &___redefine_107; }
	inline void set_redefine_107(XmlSchemaRedefine_t3478619248 * value)
	{
		___redefine_107 = value;
		Il2CppCodeGenWriteBarrier(&___redefine_107, value);
	}

	inline static int32_t get_offset_of_validationEventHandler_108() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___validationEventHandler_108)); }
	inline ValidationEventHandler_t1580700381 * get_validationEventHandler_108() const { return ___validationEventHandler_108; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_validationEventHandler_108() { return &___validationEventHandler_108; }
	inline void set_validationEventHandler_108(ValidationEventHandler_t1580700381 * value)
	{
		___validationEventHandler_108 = value;
		Il2CppCodeGenWriteBarrier(&___validationEventHandler_108, value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_109() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___unhandledAttributes_109)); }
	inline ArrayList_t4252133567 * get_unhandledAttributes_109() const { return ___unhandledAttributes_109; }
	inline ArrayList_t4252133567 ** get_address_of_unhandledAttributes_109() { return &___unhandledAttributes_109; }
	inline void set_unhandledAttributes_109(ArrayList_t4252133567 * value)
	{
		___unhandledAttributes_109 = value;
		Il2CppCodeGenWriteBarrier(&___unhandledAttributes_109, value);
	}

	inline static int32_t get_offset_of_namespaces_110() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428, ___namespaces_110)); }
	inline Hashtable_t909839986 * get_namespaces_110() const { return ___namespaces_110; }
	inline Hashtable_t909839986 ** get_address_of_namespaces_110() { return &___namespaces_110; }
	inline void set_namespaces_110(Hashtable_t909839986 * value)
	{
		___namespaces_110 = value;
		Il2CppCodeGenWriteBarrier(&___namespaces_110, value);
	}
};

struct XsdBuilder_t2784092428_StaticFields
{
public:
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SchemaElement
	StateU5BU5D_t717950118* ___SchemaElement_0;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SchemaSubelements
	StateU5BU5D_t717950118* ___SchemaSubelements_1;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AttributeSubelements
	StateU5BU5D_t717950118* ___AttributeSubelements_2;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ElementSubelements
	StateU5BU5D_t717950118* ___ElementSubelements_3;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexTypeSubelements
	StateU5BU5D_t717950118* ___ComplexTypeSubelements_4;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentSubelements
	StateU5BU5D_t717950118* ___SimpleContentSubelements_5;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentExtensionSubelements
	StateU5BU5D_t717950118* ___SimpleContentExtensionSubelements_6;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleContentRestrictionSubelements
	StateU5BU5D_t717950118* ___SimpleContentRestrictionSubelements_7;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentSubelements
	StateU5BU5D_t717950118* ___ComplexContentSubelements_8;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentExtensionSubelements
	StateU5BU5D_t717950118* ___ComplexContentExtensionSubelements_9;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ComplexContentRestrictionSubelements
	StateU5BU5D_t717950118* ___ComplexContentRestrictionSubelements_10;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeSubelements
	StateU5BU5D_t717950118* ___SimpleTypeSubelements_11;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeRestrictionSubelements
	StateU5BU5D_t717950118* ___SimpleTypeRestrictionSubelements_12;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeListSubelements
	StateU5BU5D_t717950118* ___SimpleTypeListSubelements_13;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::SimpleTypeUnionSubelements
	StateU5BU5D_t717950118* ___SimpleTypeUnionSubelements_14;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::RedefineSubelements
	StateU5BU5D_t717950118* ___RedefineSubelements_15;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AttributeGroupSubelements
	StateU5BU5D_t717950118* ___AttributeGroupSubelements_16;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::GroupSubelements
	StateU5BU5D_t717950118* ___GroupSubelements_17;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AllSubelements
	StateU5BU5D_t717950118* ___AllSubelements_18;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::ChoiceSequenceSubelements
	StateU5BU5D_t717950118* ___ChoiceSequenceSubelements_19;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::IdentityConstraintSubelements
	StateU5BU5D_t717950118* ___IdentityConstraintSubelements_20;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AnnotationSubelements
	StateU5BU5D_t717950118* ___AnnotationSubelements_21;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder::AnnotatedSubelements
	StateU5BU5D_t717950118* ___AnnotatedSubelements_22;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SchemaAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SchemaAttributes_23;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AttributeAttributes_24;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ElementAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ElementAttributes_25;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexTypeAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ComplexTypeAttributes_26;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleContentAttributes_27;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentExtensionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleContentExtensionAttributes_28;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleContentRestrictionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleContentRestrictionAttributes_29;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ComplexContentAttributes_30;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentExtensionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ComplexContentExtensionAttributes_31;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ComplexContentRestrictionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ComplexContentRestrictionAttributes_32;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleTypeAttributes_33;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeRestrictionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleTypeRestrictionAttributes_34;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeUnionAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleTypeUnionAttributes_35;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SimpleTypeListAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SimpleTypeListAttributes_36;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeGroupAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AttributeGroupAttributes_37;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AttributeGroupRefAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AttributeGroupRefAttributes_38;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::GroupAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___GroupAttributes_39;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::GroupRefAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___GroupRefAttributes_40;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ParticleAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ParticleAttributes_41;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnyAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AnyAttributes_42;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::IdentityConstraintAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___IdentityConstraintAttributes_43;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::SelectorAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___SelectorAttributes_44;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::FieldAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___FieldAttributes_45;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::NotationAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___NotationAttributes_46;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::IncludeAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___IncludeAttributes_47;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::ImportAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___ImportAttributes_48;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::FacetAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___FacetAttributes_49;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnyAttributeAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AnyAttributeAttributes_50;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::DocumentationAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___DocumentationAttributes_51;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AppinfoAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AppinfoAttributes_52;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::RedefineAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___RedefineAttributes_53;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder::AnnotationAttributes
	XsdAttributeEntryU5BU5D_t3765641446* ___AnnotationAttributes_54;
	// System.Xml.Schema.XsdBuilder/XsdEntry[] System.Xml.Schema.XsdBuilder::SchemaEntries
	XsdEntryU5BU5D_t842229578* ___SchemaEntries_55;
	// System.Int32[] System.Xml.Schema.XsdBuilder::DerivationMethodValues
	Int32U5BU5D_t3030399641* ___DerivationMethodValues_56;
	// System.String[] System.Xml.Schema.XsdBuilder::DerivationMethodStrings
	StringU5BU5D_t1642385972* ___DerivationMethodStrings_57;
	// System.String[] System.Xml.Schema.XsdBuilder::FormStringValues
	StringU5BU5D_t1642385972* ___FormStringValues_58;
	// System.String[] System.Xml.Schema.XsdBuilder::UseStringValues
	StringU5BU5D_t1642385972* ___UseStringValues_59;
	// System.String[] System.Xml.Schema.XsdBuilder::ProcessContentsStringValues
	StringU5BU5D_t1642385972* ___ProcessContentsStringValues_60;

public:
	inline static int32_t get_offset_of_SchemaElement_0() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SchemaElement_0)); }
	inline StateU5BU5D_t717950118* get_SchemaElement_0() const { return ___SchemaElement_0; }
	inline StateU5BU5D_t717950118** get_address_of_SchemaElement_0() { return &___SchemaElement_0; }
	inline void set_SchemaElement_0(StateU5BU5D_t717950118* value)
	{
		___SchemaElement_0 = value;
		Il2CppCodeGenWriteBarrier(&___SchemaElement_0, value);
	}

	inline static int32_t get_offset_of_SchemaSubelements_1() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SchemaSubelements_1)); }
	inline StateU5BU5D_t717950118* get_SchemaSubelements_1() const { return ___SchemaSubelements_1; }
	inline StateU5BU5D_t717950118** get_address_of_SchemaSubelements_1() { return &___SchemaSubelements_1; }
	inline void set_SchemaSubelements_1(StateU5BU5D_t717950118* value)
	{
		___SchemaSubelements_1 = value;
		Il2CppCodeGenWriteBarrier(&___SchemaSubelements_1, value);
	}

	inline static int32_t get_offset_of_AttributeSubelements_2() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AttributeSubelements_2)); }
	inline StateU5BU5D_t717950118* get_AttributeSubelements_2() const { return ___AttributeSubelements_2; }
	inline StateU5BU5D_t717950118** get_address_of_AttributeSubelements_2() { return &___AttributeSubelements_2; }
	inline void set_AttributeSubelements_2(StateU5BU5D_t717950118* value)
	{
		___AttributeSubelements_2 = value;
		Il2CppCodeGenWriteBarrier(&___AttributeSubelements_2, value);
	}

	inline static int32_t get_offset_of_ElementSubelements_3() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ElementSubelements_3)); }
	inline StateU5BU5D_t717950118* get_ElementSubelements_3() const { return ___ElementSubelements_3; }
	inline StateU5BU5D_t717950118** get_address_of_ElementSubelements_3() { return &___ElementSubelements_3; }
	inline void set_ElementSubelements_3(StateU5BU5D_t717950118* value)
	{
		___ElementSubelements_3 = value;
		Il2CppCodeGenWriteBarrier(&___ElementSubelements_3, value);
	}

	inline static int32_t get_offset_of_ComplexTypeSubelements_4() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexTypeSubelements_4)); }
	inline StateU5BU5D_t717950118* get_ComplexTypeSubelements_4() const { return ___ComplexTypeSubelements_4; }
	inline StateU5BU5D_t717950118** get_address_of_ComplexTypeSubelements_4() { return &___ComplexTypeSubelements_4; }
	inline void set_ComplexTypeSubelements_4(StateU5BU5D_t717950118* value)
	{
		___ComplexTypeSubelements_4 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexTypeSubelements_4, value);
	}

	inline static int32_t get_offset_of_SimpleContentSubelements_5() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentSubelements_5)); }
	inline StateU5BU5D_t717950118* get_SimpleContentSubelements_5() const { return ___SimpleContentSubelements_5; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleContentSubelements_5() { return &___SimpleContentSubelements_5; }
	inline void set_SimpleContentSubelements_5(StateU5BU5D_t717950118* value)
	{
		___SimpleContentSubelements_5 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentSubelements_5, value);
	}

	inline static int32_t get_offset_of_SimpleContentExtensionSubelements_6() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentExtensionSubelements_6)); }
	inline StateU5BU5D_t717950118* get_SimpleContentExtensionSubelements_6() const { return ___SimpleContentExtensionSubelements_6; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleContentExtensionSubelements_6() { return &___SimpleContentExtensionSubelements_6; }
	inline void set_SimpleContentExtensionSubelements_6(StateU5BU5D_t717950118* value)
	{
		___SimpleContentExtensionSubelements_6 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentExtensionSubelements_6, value);
	}

	inline static int32_t get_offset_of_SimpleContentRestrictionSubelements_7() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentRestrictionSubelements_7)); }
	inline StateU5BU5D_t717950118* get_SimpleContentRestrictionSubelements_7() const { return ___SimpleContentRestrictionSubelements_7; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleContentRestrictionSubelements_7() { return &___SimpleContentRestrictionSubelements_7; }
	inline void set_SimpleContentRestrictionSubelements_7(StateU5BU5D_t717950118* value)
	{
		___SimpleContentRestrictionSubelements_7 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentRestrictionSubelements_7, value);
	}

	inline static int32_t get_offset_of_ComplexContentSubelements_8() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentSubelements_8)); }
	inline StateU5BU5D_t717950118* get_ComplexContentSubelements_8() const { return ___ComplexContentSubelements_8; }
	inline StateU5BU5D_t717950118** get_address_of_ComplexContentSubelements_8() { return &___ComplexContentSubelements_8; }
	inline void set_ComplexContentSubelements_8(StateU5BU5D_t717950118* value)
	{
		___ComplexContentSubelements_8 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentSubelements_8, value);
	}

	inline static int32_t get_offset_of_ComplexContentExtensionSubelements_9() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentExtensionSubelements_9)); }
	inline StateU5BU5D_t717950118* get_ComplexContentExtensionSubelements_9() const { return ___ComplexContentExtensionSubelements_9; }
	inline StateU5BU5D_t717950118** get_address_of_ComplexContentExtensionSubelements_9() { return &___ComplexContentExtensionSubelements_9; }
	inline void set_ComplexContentExtensionSubelements_9(StateU5BU5D_t717950118* value)
	{
		___ComplexContentExtensionSubelements_9 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentExtensionSubelements_9, value);
	}

	inline static int32_t get_offset_of_ComplexContentRestrictionSubelements_10() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentRestrictionSubelements_10)); }
	inline StateU5BU5D_t717950118* get_ComplexContentRestrictionSubelements_10() const { return ___ComplexContentRestrictionSubelements_10; }
	inline StateU5BU5D_t717950118** get_address_of_ComplexContentRestrictionSubelements_10() { return &___ComplexContentRestrictionSubelements_10; }
	inline void set_ComplexContentRestrictionSubelements_10(StateU5BU5D_t717950118* value)
	{
		___ComplexContentRestrictionSubelements_10 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentRestrictionSubelements_10, value);
	}

	inline static int32_t get_offset_of_SimpleTypeSubelements_11() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeSubelements_11)); }
	inline StateU5BU5D_t717950118* get_SimpleTypeSubelements_11() const { return ___SimpleTypeSubelements_11; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleTypeSubelements_11() { return &___SimpleTypeSubelements_11; }
	inline void set_SimpleTypeSubelements_11(StateU5BU5D_t717950118* value)
	{
		___SimpleTypeSubelements_11 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeSubelements_11, value);
	}

	inline static int32_t get_offset_of_SimpleTypeRestrictionSubelements_12() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeRestrictionSubelements_12)); }
	inline StateU5BU5D_t717950118* get_SimpleTypeRestrictionSubelements_12() const { return ___SimpleTypeRestrictionSubelements_12; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleTypeRestrictionSubelements_12() { return &___SimpleTypeRestrictionSubelements_12; }
	inline void set_SimpleTypeRestrictionSubelements_12(StateU5BU5D_t717950118* value)
	{
		___SimpleTypeRestrictionSubelements_12 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeRestrictionSubelements_12, value);
	}

	inline static int32_t get_offset_of_SimpleTypeListSubelements_13() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeListSubelements_13)); }
	inline StateU5BU5D_t717950118* get_SimpleTypeListSubelements_13() const { return ___SimpleTypeListSubelements_13; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleTypeListSubelements_13() { return &___SimpleTypeListSubelements_13; }
	inline void set_SimpleTypeListSubelements_13(StateU5BU5D_t717950118* value)
	{
		___SimpleTypeListSubelements_13 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeListSubelements_13, value);
	}

	inline static int32_t get_offset_of_SimpleTypeUnionSubelements_14() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeUnionSubelements_14)); }
	inline StateU5BU5D_t717950118* get_SimpleTypeUnionSubelements_14() const { return ___SimpleTypeUnionSubelements_14; }
	inline StateU5BU5D_t717950118** get_address_of_SimpleTypeUnionSubelements_14() { return &___SimpleTypeUnionSubelements_14; }
	inline void set_SimpleTypeUnionSubelements_14(StateU5BU5D_t717950118* value)
	{
		___SimpleTypeUnionSubelements_14 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeUnionSubelements_14, value);
	}

	inline static int32_t get_offset_of_RedefineSubelements_15() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___RedefineSubelements_15)); }
	inline StateU5BU5D_t717950118* get_RedefineSubelements_15() const { return ___RedefineSubelements_15; }
	inline StateU5BU5D_t717950118** get_address_of_RedefineSubelements_15() { return &___RedefineSubelements_15; }
	inline void set_RedefineSubelements_15(StateU5BU5D_t717950118* value)
	{
		___RedefineSubelements_15 = value;
		Il2CppCodeGenWriteBarrier(&___RedefineSubelements_15, value);
	}

	inline static int32_t get_offset_of_AttributeGroupSubelements_16() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AttributeGroupSubelements_16)); }
	inline StateU5BU5D_t717950118* get_AttributeGroupSubelements_16() const { return ___AttributeGroupSubelements_16; }
	inline StateU5BU5D_t717950118** get_address_of_AttributeGroupSubelements_16() { return &___AttributeGroupSubelements_16; }
	inline void set_AttributeGroupSubelements_16(StateU5BU5D_t717950118* value)
	{
		___AttributeGroupSubelements_16 = value;
		Il2CppCodeGenWriteBarrier(&___AttributeGroupSubelements_16, value);
	}

	inline static int32_t get_offset_of_GroupSubelements_17() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___GroupSubelements_17)); }
	inline StateU5BU5D_t717950118* get_GroupSubelements_17() const { return ___GroupSubelements_17; }
	inline StateU5BU5D_t717950118** get_address_of_GroupSubelements_17() { return &___GroupSubelements_17; }
	inline void set_GroupSubelements_17(StateU5BU5D_t717950118* value)
	{
		___GroupSubelements_17 = value;
		Il2CppCodeGenWriteBarrier(&___GroupSubelements_17, value);
	}

	inline static int32_t get_offset_of_AllSubelements_18() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AllSubelements_18)); }
	inline StateU5BU5D_t717950118* get_AllSubelements_18() const { return ___AllSubelements_18; }
	inline StateU5BU5D_t717950118** get_address_of_AllSubelements_18() { return &___AllSubelements_18; }
	inline void set_AllSubelements_18(StateU5BU5D_t717950118* value)
	{
		___AllSubelements_18 = value;
		Il2CppCodeGenWriteBarrier(&___AllSubelements_18, value);
	}

	inline static int32_t get_offset_of_ChoiceSequenceSubelements_19() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ChoiceSequenceSubelements_19)); }
	inline StateU5BU5D_t717950118* get_ChoiceSequenceSubelements_19() const { return ___ChoiceSequenceSubelements_19; }
	inline StateU5BU5D_t717950118** get_address_of_ChoiceSequenceSubelements_19() { return &___ChoiceSequenceSubelements_19; }
	inline void set_ChoiceSequenceSubelements_19(StateU5BU5D_t717950118* value)
	{
		___ChoiceSequenceSubelements_19 = value;
		Il2CppCodeGenWriteBarrier(&___ChoiceSequenceSubelements_19, value);
	}

	inline static int32_t get_offset_of_IdentityConstraintSubelements_20() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___IdentityConstraintSubelements_20)); }
	inline StateU5BU5D_t717950118* get_IdentityConstraintSubelements_20() const { return ___IdentityConstraintSubelements_20; }
	inline StateU5BU5D_t717950118** get_address_of_IdentityConstraintSubelements_20() { return &___IdentityConstraintSubelements_20; }
	inline void set_IdentityConstraintSubelements_20(StateU5BU5D_t717950118* value)
	{
		___IdentityConstraintSubelements_20 = value;
		Il2CppCodeGenWriteBarrier(&___IdentityConstraintSubelements_20, value);
	}

	inline static int32_t get_offset_of_AnnotationSubelements_21() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AnnotationSubelements_21)); }
	inline StateU5BU5D_t717950118* get_AnnotationSubelements_21() const { return ___AnnotationSubelements_21; }
	inline StateU5BU5D_t717950118** get_address_of_AnnotationSubelements_21() { return &___AnnotationSubelements_21; }
	inline void set_AnnotationSubelements_21(StateU5BU5D_t717950118* value)
	{
		___AnnotationSubelements_21 = value;
		Il2CppCodeGenWriteBarrier(&___AnnotationSubelements_21, value);
	}

	inline static int32_t get_offset_of_AnnotatedSubelements_22() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AnnotatedSubelements_22)); }
	inline StateU5BU5D_t717950118* get_AnnotatedSubelements_22() const { return ___AnnotatedSubelements_22; }
	inline StateU5BU5D_t717950118** get_address_of_AnnotatedSubelements_22() { return &___AnnotatedSubelements_22; }
	inline void set_AnnotatedSubelements_22(StateU5BU5D_t717950118* value)
	{
		___AnnotatedSubelements_22 = value;
		Il2CppCodeGenWriteBarrier(&___AnnotatedSubelements_22, value);
	}

	inline static int32_t get_offset_of_SchemaAttributes_23() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SchemaAttributes_23)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SchemaAttributes_23() const { return ___SchemaAttributes_23; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SchemaAttributes_23() { return &___SchemaAttributes_23; }
	inline void set_SchemaAttributes_23(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SchemaAttributes_23 = value;
		Il2CppCodeGenWriteBarrier(&___SchemaAttributes_23, value);
	}

	inline static int32_t get_offset_of_AttributeAttributes_24() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AttributeAttributes_24)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AttributeAttributes_24() const { return ___AttributeAttributes_24; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AttributeAttributes_24() { return &___AttributeAttributes_24; }
	inline void set_AttributeAttributes_24(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AttributeAttributes_24 = value;
		Il2CppCodeGenWriteBarrier(&___AttributeAttributes_24, value);
	}

	inline static int32_t get_offset_of_ElementAttributes_25() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ElementAttributes_25)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ElementAttributes_25() const { return ___ElementAttributes_25; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ElementAttributes_25() { return &___ElementAttributes_25; }
	inline void set_ElementAttributes_25(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ElementAttributes_25 = value;
		Il2CppCodeGenWriteBarrier(&___ElementAttributes_25, value);
	}

	inline static int32_t get_offset_of_ComplexTypeAttributes_26() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexTypeAttributes_26)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ComplexTypeAttributes_26() const { return ___ComplexTypeAttributes_26; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ComplexTypeAttributes_26() { return &___ComplexTypeAttributes_26; }
	inline void set_ComplexTypeAttributes_26(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ComplexTypeAttributes_26 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexTypeAttributes_26, value);
	}

	inline static int32_t get_offset_of_SimpleContentAttributes_27() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentAttributes_27)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleContentAttributes_27() const { return ___SimpleContentAttributes_27; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleContentAttributes_27() { return &___SimpleContentAttributes_27; }
	inline void set_SimpleContentAttributes_27(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleContentAttributes_27 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentAttributes_27, value);
	}

	inline static int32_t get_offset_of_SimpleContentExtensionAttributes_28() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentExtensionAttributes_28)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleContentExtensionAttributes_28() const { return ___SimpleContentExtensionAttributes_28; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleContentExtensionAttributes_28() { return &___SimpleContentExtensionAttributes_28; }
	inline void set_SimpleContentExtensionAttributes_28(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleContentExtensionAttributes_28 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentExtensionAttributes_28, value);
	}

	inline static int32_t get_offset_of_SimpleContentRestrictionAttributes_29() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleContentRestrictionAttributes_29)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleContentRestrictionAttributes_29() const { return ___SimpleContentRestrictionAttributes_29; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleContentRestrictionAttributes_29() { return &___SimpleContentRestrictionAttributes_29; }
	inline void set_SimpleContentRestrictionAttributes_29(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleContentRestrictionAttributes_29 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleContentRestrictionAttributes_29, value);
	}

	inline static int32_t get_offset_of_ComplexContentAttributes_30() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentAttributes_30)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ComplexContentAttributes_30() const { return ___ComplexContentAttributes_30; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ComplexContentAttributes_30() { return &___ComplexContentAttributes_30; }
	inline void set_ComplexContentAttributes_30(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ComplexContentAttributes_30 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentAttributes_30, value);
	}

	inline static int32_t get_offset_of_ComplexContentExtensionAttributes_31() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentExtensionAttributes_31)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ComplexContentExtensionAttributes_31() const { return ___ComplexContentExtensionAttributes_31; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ComplexContentExtensionAttributes_31() { return &___ComplexContentExtensionAttributes_31; }
	inline void set_ComplexContentExtensionAttributes_31(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ComplexContentExtensionAttributes_31 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentExtensionAttributes_31, value);
	}

	inline static int32_t get_offset_of_ComplexContentRestrictionAttributes_32() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ComplexContentRestrictionAttributes_32)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ComplexContentRestrictionAttributes_32() const { return ___ComplexContentRestrictionAttributes_32; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ComplexContentRestrictionAttributes_32() { return &___ComplexContentRestrictionAttributes_32; }
	inline void set_ComplexContentRestrictionAttributes_32(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ComplexContentRestrictionAttributes_32 = value;
		Il2CppCodeGenWriteBarrier(&___ComplexContentRestrictionAttributes_32, value);
	}

	inline static int32_t get_offset_of_SimpleTypeAttributes_33() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeAttributes_33)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleTypeAttributes_33() const { return ___SimpleTypeAttributes_33; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleTypeAttributes_33() { return &___SimpleTypeAttributes_33; }
	inline void set_SimpleTypeAttributes_33(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleTypeAttributes_33 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeAttributes_33, value);
	}

	inline static int32_t get_offset_of_SimpleTypeRestrictionAttributes_34() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeRestrictionAttributes_34)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleTypeRestrictionAttributes_34() const { return ___SimpleTypeRestrictionAttributes_34; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleTypeRestrictionAttributes_34() { return &___SimpleTypeRestrictionAttributes_34; }
	inline void set_SimpleTypeRestrictionAttributes_34(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleTypeRestrictionAttributes_34 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeRestrictionAttributes_34, value);
	}

	inline static int32_t get_offset_of_SimpleTypeUnionAttributes_35() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeUnionAttributes_35)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleTypeUnionAttributes_35() const { return ___SimpleTypeUnionAttributes_35; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleTypeUnionAttributes_35() { return &___SimpleTypeUnionAttributes_35; }
	inline void set_SimpleTypeUnionAttributes_35(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleTypeUnionAttributes_35 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeUnionAttributes_35, value);
	}

	inline static int32_t get_offset_of_SimpleTypeListAttributes_36() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SimpleTypeListAttributes_36)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SimpleTypeListAttributes_36() const { return ___SimpleTypeListAttributes_36; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SimpleTypeListAttributes_36() { return &___SimpleTypeListAttributes_36; }
	inline void set_SimpleTypeListAttributes_36(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SimpleTypeListAttributes_36 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleTypeListAttributes_36, value);
	}

	inline static int32_t get_offset_of_AttributeGroupAttributes_37() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AttributeGroupAttributes_37)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AttributeGroupAttributes_37() const { return ___AttributeGroupAttributes_37; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AttributeGroupAttributes_37() { return &___AttributeGroupAttributes_37; }
	inline void set_AttributeGroupAttributes_37(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AttributeGroupAttributes_37 = value;
		Il2CppCodeGenWriteBarrier(&___AttributeGroupAttributes_37, value);
	}

	inline static int32_t get_offset_of_AttributeGroupRefAttributes_38() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AttributeGroupRefAttributes_38)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AttributeGroupRefAttributes_38() const { return ___AttributeGroupRefAttributes_38; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AttributeGroupRefAttributes_38() { return &___AttributeGroupRefAttributes_38; }
	inline void set_AttributeGroupRefAttributes_38(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AttributeGroupRefAttributes_38 = value;
		Il2CppCodeGenWriteBarrier(&___AttributeGroupRefAttributes_38, value);
	}

	inline static int32_t get_offset_of_GroupAttributes_39() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___GroupAttributes_39)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_GroupAttributes_39() const { return ___GroupAttributes_39; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_GroupAttributes_39() { return &___GroupAttributes_39; }
	inline void set_GroupAttributes_39(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___GroupAttributes_39 = value;
		Il2CppCodeGenWriteBarrier(&___GroupAttributes_39, value);
	}

	inline static int32_t get_offset_of_GroupRefAttributes_40() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___GroupRefAttributes_40)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_GroupRefAttributes_40() const { return ___GroupRefAttributes_40; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_GroupRefAttributes_40() { return &___GroupRefAttributes_40; }
	inline void set_GroupRefAttributes_40(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___GroupRefAttributes_40 = value;
		Il2CppCodeGenWriteBarrier(&___GroupRefAttributes_40, value);
	}

	inline static int32_t get_offset_of_ParticleAttributes_41() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ParticleAttributes_41)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ParticleAttributes_41() const { return ___ParticleAttributes_41; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ParticleAttributes_41() { return &___ParticleAttributes_41; }
	inline void set_ParticleAttributes_41(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ParticleAttributes_41 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleAttributes_41, value);
	}

	inline static int32_t get_offset_of_AnyAttributes_42() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AnyAttributes_42)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AnyAttributes_42() const { return ___AnyAttributes_42; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AnyAttributes_42() { return &___AnyAttributes_42; }
	inline void set_AnyAttributes_42(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AnyAttributes_42 = value;
		Il2CppCodeGenWriteBarrier(&___AnyAttributes_42, value);
	}

	inline static int32_t get_offset_of_IdentityConstraintAttributes_43() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___IdentityConstraintAttributes_43)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_IdentityConstraintAttributes_43() const { return ___IdentityConstraintAttributes_43; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_IdentityConstraintAttributes_43() { return &___IdentityConstraintAttributes_43; }
	inline void set_IdentityConstraintAttributes_43(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___IdentityConstraintAttributes_43 = value;
		Il2CppCodeGenWriteBarrier(&___IdentityConstraintAttributes_43, value);
	}

	inline static int32_t get_offset_of_SelectorAttributes_44() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SelectorAttributes_44)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_SelectorAttributes_44() const { return ___SelectorAttributes_44; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_SelectorAttributes_44() { return &___SelectorAttributes_44; }
	inline void set_SelectorAttributes_44(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___SelectorAttributes_44 = value;
		Il2CppCodeGenWriteBarrier(&___SelectorAttributes_44, value);
	}

	inline static int32_t get_offset_of_FieldAttributes_45() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___FieldAttributes_45)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_FieldAttributes_45() const { return ___FieldAttributes_45; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_FieldAttributes_45() { return &___FieldAttributes_45; }
	inline void set_FieldAttributes_45(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___FieldAttributes_45 = value;
		Il2CppCodeGenWriteBarrier(&___FieldAttributes_45, value);
	}

	inline static int32_t get_offset_of_NotationAttributes_46() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___NotationAttributes_46)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_NotationAttributes_46() const { return ___NotationAttributes_46; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_NotationAttributes_46() { return &___NotationAttributes_46; }
	inline void set_NotationAttributes_46(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___NotationAttributes_46 = value;
		Il2CppCodeGenWriteBarrier(&___NotationAttributes_46, value);
	}

	inline static int32_t get_offset_of_IncludeAttributes_47() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___IncludeAttributes_47)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_IncludeAttributes_47() const { return ___IncludeAttributes_47; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_IncludeAttributes_47() { return &___IncludeAttributes_47; }
	inline void set_IncludeAttributes_47(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___IncludeAttributes_47 = value;
		Il2CppCodeGenWriteBarrier(&___IncludeAttributes_47, value);
	}

	inline static int32_t get_offset_of_ImportAttributes_48() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ImportAttributes_48)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_ImportAttributes_48() const { return ___ImportAttributes_48; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_ImportAttributes_48() { return &___ImportAttributes_48; }
	inline void set_ImportAttributes_48(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___ImportAttributes_48 = value;
		Il2CppCodeGenWriteBarrier(&___ImportAttributes_48, value);
	}

	inline static int32_t get_offset_of_FacetAttributes_49() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___FacetAttributes_49)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_FacetAttributes_49() const { return ___FacetAttributes_49; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_FacetAttributes_49() { return &___FacetAttributes_49; }
	inline void set_FacetAttributes_49(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___FacetAttributes_49 = value;
		Il2CppCodeGenWriteBarrier(&___FacetAttributes_49, value);
	}

	inline static int32_t get_offset_of_AnyAttributeAttributes_50() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AnyAttributeAttributes_50)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AnyAttributeAttributes_50() const { return ___AnyAttributeAttributes_50; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AnyAttributeAttributes_50() { return &___AnyAttributeAttributes_50; }
	inline void set_AnyAttributeAttributes_50(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AnyAttributeAttributes_50 = value;
		Il2CppCodeGenWriteBarrier(&___AnyAttributeAttributes_50, value);
	}

	inline static int32_t get_offset_of_DocumentationAttributes_51() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___DocumentationAttributes_51)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_DocumentationAttributes_51() const { return ___DocumentationAttributes_51; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_DocumentationAttributes_51() { return &___DocumentationAttributes_51; }
	inline void set_DocumentationAttributes_51(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___DocumentationAttributes_51 = value;
		Il2CppCodeGenWriteBarrier(&___DocumentationAttributes_51, value);
	}

	inline static int32_t get_offset_of_AppinfoAttributes_52() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AppinfoAttributes_52)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AppinfoAttributes_52() const { return ___AppinfoAttributes_52; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AppinfoAttributes_52() { return &___AppinfoAttributes_52; }
	inline void set_AppinfoAttributes_52(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AppinfoAttributes_52 = value;
		Il2CppCodeGenWriteBarrier(&___AppinfoAttributes_52, value);
	}

	inline static int32_t get_offset_of_RedefineAttributes_53() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___RedefineAttributes_53)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_RedefineAttributes_53() const { return ___RedefineAttributes_53; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_RedefineAttributes_53() { return &___RedefineAttributes_53; }
	inline void set_RedefineAttributes_53(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___RedefineAttributes_53 = value;
		Il2CppCodeGenWriteBarrier(&___RedefineAttributes_53, value);
	}

	inline static int32_t get_offset_of_AnnotationAttributes_54() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___AnnotationAttributes_54)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_AnnotationAttributes_54() const { return ___AnnotationAttributes_54; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_AnnotationAttributes_54() { return &___AnnotationAttributes_54; }
	inline void set_AnnotationAttributes_54(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___AnnotationAttributes_54 = value;
		Il2CppCodeGenWriteBarrier(&___AnnotationAttributes_54, value);
	}

	inline static int32_t get_offset_of_SchemaEntries_55() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___SchemaEntries_55)); }
	inline XsdEntryU5BU5D_t842229578* get_SchemaEntries_55() const { return ___SchemaEntries_55; }
	inline XsdEntryU5BU5D_t842229578** get_address_of_SchemaEntries_55() { return &___SchemaEntries_55; }
	inline void set_SchemaEntries_55(XsdEntryU5BU5D_t842229578* value)
	{
		___SchemaEntries_55 = value;
		Il2CppCodeGenWriteBarrier(&___SchemaEntries_55, value);
	}

	inline static int32_t get_offset_of_DerivationMethodValues_56() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___DerivationMethodValues_56)); }
	inline Int32U5BU5D_t3030399641* get_DerivationMethodValues_56() const { return ___DerivationMethodValues_56; }
	inline Int32U5BU5D_t3030399641** get_address_of_DerivationMethodValues_56() { return &___DerivationMethodValues_56; }
	inline void set_DerivationMethodValues_56(Int32U5BU5D_t3030399641* value)
	{
		___DerivationMethodValues_56 = value;
		Il2CppCodeGenWriteBarrier(&___DerivationMethodValues_56, value);
	}

	inline static int32_t get_offset_of_DerivationMethodStrings_57() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___DerivationMethodStrings_57)); }
	inline StringU5BU5D_t1642385972* get_DerivationMethodStrings_57() const { return ___DerivationMethodStrings_57; }
	inline StringU5BU5D_t1642385972** get_address_of_DerivationMethodStrings_57() { return &___DerivationMethodStrings_57; }
	inline void set_DerivationMethodStrings_57(StringU5BU5D_t1642385972* value)
	{
		___DerivationMethodStrings_57 = value;
		Il2CppCodeGenWriteBarrier(&___DerivationMethodStrings_57, value);
	}

	inline static int32_t get_offset_of_FormStringValues_58() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___FormStringValues_58)); }
	inline StringU5BU5D_t1642385972* get_FormStringValues_58() const { return ___FormStringValues_58; }
	inline StringU5BU5D_t1642385972** get_address_of_FormStringValues_58() { return &___FormStringValues_58; }
	inline void set_FormStringValues_58(StringU5BU5D_t1642385972* value)
	{
		___FormStringValues_58 = value;
		Il2CppCodeGenWriteBarrier(&___FormStringValues_58, value);
	}

	inline static int32_t get_offset_of_UseStringValues_59() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___UseStringValues_59)); }
	inline StringU5BU5D_t1642385972* get_UseStringValues_59() const { return ___UseStringValues_59; }
	inline StringU5BU5D_t1642385972** get_address_of_UseStringValues_59() { return &___UseStringValues_59; }
	inline void set_UseStringValues_59(StringU5BU5D_t1642385972* value)
	{
		___UseStringValues_59 = value;
		Il2CppCodeGenWriteBarrier(&___UseStringValues_59, value);
	}

	inline static int32_t get_offset_of_ProcessContentsStringValues_60() { return static_cast<int32_t>(offsetof(XsdBuilder_t2784092428_StaticFields, ___ProcessContentsStringValues_60)); }
	inline StringU5BU5D_t1642385972* get_ProcessContentsStringValues_60() const { return ___ProcessContentsStringValues_60; }
	inline StringU5BU5D_t1642385972** get_address_of_ProcessContentsStringValues_60() { return &___ProcessContentsStringValues_60; }
	inline void set_ProcessContentsStringValues_60(StringU5BU5D_t1642385972* value)
	{
		___ProcessContentsStringValues_60 = value;
		Il2CppCodeGenWriteBarrier(&___ProcessContentsStringValues_60, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
