﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEntry
struct  SerializationEntry_t3485203212 
{
public:
	// System.Type System.Runtime.Serialization.SerializationEntry::m_type
	Type_t * ___m_type_0;
	// System.Object System.Runtime.Serialization.SerializationEntry::m_value
	Il2CppObject * ___m_value_1;
	// System.String System.Runtime.Serialization.SerializationEntry::m_name
	String_t* ___m_name_2;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(SerializationEntry_t3485203212, ___m_type_0)); }
	inline Type_t * get_m_type_0() const { return ___m_type_0; }
	inline Type_t ** get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(Type_t * value)
	{
		___m_type_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_type_0, value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(SerializationEntry_t3485203212, ___m_value_1)); }
	inline Il2CppObject * get_m_value_1() const { return ___m_value_1; }
	inline Il2CppObject ** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(Il2CppObject * value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_value_1, value);
	}

	inline static int32_t get_offset_of_m_name_2() { return static_cast<int32_t>(offsetof(SerializationEntry_t3485203212, ___m_name_2)); }
	inline String_t* get_m_name_2() const { return ___m_name_2; }
	inline String_t** get_address_of_m_name_2() { return &___m_name_2; }
	inline void set_m_name_2(String_t* value)
	{
		___m_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t3485203212_marshaled_pinvoke
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	char* ___m_name_2;
};
// Native definition for COM marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t3485203212_marshaled_com
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	Il2CppChar* ___m_name_2;
};
