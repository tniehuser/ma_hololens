﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<System.Net.SimpleAsyncResult,System.Boolean>
struct Func_2_t4158077014;
// System.Object
struct Il2CppObject;
// System.Net.SimpleAsyncCallback
struct SimpleAsyncCallback_t3151114241;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult/<RunWithLock>c__AnonStorey1
struct  U3CRunWithLockU3Ec__AnonStorey1_t660223254  : public Il2CppObject
{
public:
	// System.Func`2<System.Net.SimpleAsyncResult,System.Boolean> System.Net.SimpleAsyncResult/<RunWithLock>c__AnonStorey1::func
	Func_2_t4158077014 * ___func_0;
	// System.Object System.Net.SimpleAsyncResult/<RunWithLock>c__AnonStorey1::locker
	Il2CppObject * ___locker_1;
	// System.Net.SimpleAsyncCallback System.Net.SimpleAsyncResult/<RunWithLock>c__AnonStorey1::callback
	SimpleAsyncCallback_t3151114241 * ___callback_2;

public:
	inline static int32_t get_offset_of_func_0() { return static_cast<int32_t>(offsetof(U3CRunWithLockU3Ec__AnonStorey1_t660223254, ___func_0)); }
	inline Func_2_t4158077014 * get_func_0() const { return ___func_0; }
	inline Func_2_t4158077014 ** get_address_of_func_0() { return &___func_0; }
	inline void set_func_0(Func_2_t4158077014 * value)
	{
		___func_0 = value;
		Il2CppCodeGenWriteBarrier(&___func_0, value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(U3CRunWithLockU3Ec__AnonStorey1_t660223254, ___locker_1)); }
	inline Il2CppObject * get_locker_1() const { return ___locker_1; }
	inline Il2CppObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(Il2CppObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier(&___locker_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CRunWithLockU3Ec__AnonStorey1_t660223254, ___callback_2)); }
	inline SimpleAsyncCallback_t3151114241 * get_callback_2() const { return ___callback_2; }
	inline SimpleAsyncCallback_t3151114241 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(SimpleAsyncCallback_t3151114241 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
