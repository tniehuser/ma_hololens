﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Mono_Net_Dns_DnsQType1279797630.h"
#include "System_Mono_Net_Dns_DnsQClass2376709280.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQuestion
struct  DnsQuestion_t3090842959  : public Il2CppObject
{
public:
	// System.String Mono.Net.Dns.DnsQuestion::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsQType Mono.Net.Dns.DnsQuestion::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsQClass Mono.Net.Dns.DnsQuestion::_class
	uint16_t ____class_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of__class_2() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ____class_2)); }
	inline uint16_t get__class_2() const { return ____class_2; }
	inline uint16_t* get_address_of__class_2() { return &____class_2; }
	inline void set__class_2(uint16_t value)
	{
		____class_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
