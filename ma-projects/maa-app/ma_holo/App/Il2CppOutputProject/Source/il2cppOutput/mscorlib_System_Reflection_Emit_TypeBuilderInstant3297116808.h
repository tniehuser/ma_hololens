﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_TypeInfo3822613806.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.TypeBuilderInstantiation
struct  TypeBuilderInstantiation_t3297116808  : public TypeInfo_t3822613806
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
