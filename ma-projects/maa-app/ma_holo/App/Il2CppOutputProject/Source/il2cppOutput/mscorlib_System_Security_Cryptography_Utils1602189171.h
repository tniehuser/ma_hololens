﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Cryptography.RNGCryptoServiceProvider
struct RNGCryptoServiceProvider_t2688843926;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Utils
struct  Utils_t1602189171  : public Il2CppObject
{
public:

public:
};

struct Utils_t1602189171_StaticFields
{
public:
	// System.Security.Cryptography.RNGCryptoServiceProvider modreq(System.Runtime.CompilerServices.IsVolatile) System.Security.Cryptography.Utils::_rng
	RNGCryptoServiceProvider_t2688843926 * ____rng_0;

public:
	inline static int32_t get_offset_of__rng_0() { return static_cast<int32_t>(offsetof(Utils_t1602189171_StaticFields, ____rng_0)); }
	inline RNGCryptoServiceProvider_t2688843926 * get__rng_0() const { return ____rng_0; }
	inline RNGCryptoServiceProvider_t2688843926 ** get_address_of__rng_0() { return &____rng_0; }
	inline void set__rng_0(RNGCryptoServiceProvider_t2688843926 * value)
	{
		____rng_0 = value;
		Il2CppCodeGenWriteBarrier(&____rng_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
