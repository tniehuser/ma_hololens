﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Globalization_Calendar585061108.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Globalization.EraInfo[]
struct EraInfoU5BU5D_t1865950449;
// System.Globalization.Calendar
struct Calendar_t585061108;
// System.Globalization.GregorianCalendarHelper
struct GregorianCalendarHelper_t3151146692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.JapaneseCalendar
struct  JapaneseCalendar_t3073160889  : public Calendar_t585061108
{
public:
	// System.Globalization.GregorianCalendarHelper System.Globalization.JapaneseCalendar::helper
	GregorianCalendarHelper_t3151146692 * ___helper_6;

public:
	inline static int32_t get_offset_of_helper_6() { return static_cast<int32_t>(offsetof(JapaneseCalendar_t3073160889, ___helper_6)); }
	inline GregorianCalendarHelper_t3151146692 * get_helper_6() const { return ___helper_6; }
	inline GregorianCalendarHelper_t3151146692 ** get_address_of_helper_6() { return &___helper_6; }
	inline void set_helper_6(GregorianCalendarHelper_t3151146692 * value)
	{
		___helper_6 = value;
		Il2CppCodeGenWriteBarrier(&___helper_6, value);
	}
};

struct JapaneseCalendar_t3073160889_StaticFields
{
public:
	// System.DateTime System.Globalization.JapaneseCalendar::calendarMinValue
	DateTime_t693205669  ___calendarMinValue_3;
	// System.Globalization.EraInfo[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.JapaneseCalendar::japaneseEraInfo
	EraInfoU5BU5D_t1865950449* ___japaneseEraInfo_4;
	// System.Globalization.Calendar modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.JapaneseCalendar::s_defaultInstance
	Calendar_t585061108 * ___s_defaultInstance_5;

public:
	inline static int32_t get_offset_of_calendarMinValue_3() { return static_cast<int32_t>(offsetof(JapaneseCalendar_t3073160889_StaticFields, ___calendarMinValue_3)); }
	inline DateTime_t693205669  get_calendarMinValue_3() const { return ___calendarMinValue_3; }
	inline DateTime_t693205669 * get_address_of_calendarMinValue_3() { return &___calendarMinValue_3; }
	inline void set_calendarMinValue_3(DateTime_t693205669  value)
	{
		___calendarMinValue_3 = value;
	}

	inline static int32_t get_offset_of_japaneseEraInfo_4() { return static_cast<int32_t>(offsetof(JapaneseCalendar_t3073160889_StaticFields, ___japaneseEraInfo_4)); }
	inline EraInfoU5BU5D_t1865950449* get_japaneseEraInfo_4() const { return ___japaneseEraInfo_4; }
	inline EraInfoU5BU5D_t1865950449** get_address_of_japaneseEraInfo_4() { return &___japaneseEraInfo_4; }
	inline void set_japaneseEraInfo_4(EraInfoU5BU5D_t1865950449* value)
	{
		___japaneseEraInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___japaneseEraInfo_4, value);
	}

	inline static int32_t get_offset_of_s_defaultInstance_5() { return static_cast<int32_t>(offsetof(JapaneseCalendar_t3073160889_StaticFields, ___s_defaultInstance_5)); }
	inline Calendar_t585061108 * get_s_defaultInstance_5() const { return ___s_defaultInstance_5; }
	inline Calendar_t585061108 ** get_address_of_s_defaultInstance_5() { return &___s_defaultInstance_5; }
	inline void set_s_defaultInstance_5(Calendar_t585061108 * value)
	{
		___s_defaultInstance_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_defaultInstance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
