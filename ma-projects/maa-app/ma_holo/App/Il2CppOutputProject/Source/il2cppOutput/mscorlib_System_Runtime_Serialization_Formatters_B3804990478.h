﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.TypeInformation
struct  TypeInformation_t3804990478  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.TypeInformation::fullTypeName
	String_t* ___fullTypeName_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.TypeInformation::assemblyString
	String_t* ___assemblyString_1;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.TypeInformation::hasTypeForwardedFrom
	bool ___hasTypeForwardedFrom_2;

public:
	inline static int32_t get_offset_of_fullTypeName_0() { return static_cast<int32_t>(offsetof(TypeInformation_t3804990478, ___fullTypeName_0)); }
	inline String_t* get_fullTypeName_0() const { return ___fullTypeName_0; }
	inline String_t** get_address_of_fullTypeName_0() { return &___fullTypeName_0; }
	inline void set_fullTypeName_0(String_t* value)
	{
		___fullTypeName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fullTypeName_0, value);
	}

	inline static int32_t get_offset_of_assemblyString_1() { return static_cast<int32_t>(offsetof(TypeInformation_t3804990478, ___assemblyString_1)); }
	inline String_t* get_assemblyString_1() const { return ___assemblyString_1; }
	inline String_t** get_address_of_assemblyString_1() { return &___assemblyString_1; }
	inline void set_assemblyString_1(String_t* value)
	{
		___assemblyString_1 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyString_1, value);
	}

	inline static int32_t get_offset_of_hasTypeForwardedFrom_2() { return static_cast<int32_t>(offsetof(TypeInformation_t3804990478, ___hasTypeForwardedFrom_2)); }
	inline bool get_hasTypeForwardedFrom_2() const { return ___hasTypeForwardedFrom_2; }
	inline bool* get_address_of_hasTypeForwardedFrom_2() { return &___hasTypeForwardedFrom_2; }
	inline void set_hasTypeForwardedFrom_2(bool value)
	{
		___hasTypeForwardedFrom_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
