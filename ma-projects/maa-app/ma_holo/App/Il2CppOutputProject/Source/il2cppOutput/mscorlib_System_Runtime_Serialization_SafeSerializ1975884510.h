﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.RuntimeType
struct RuntimeType_t2836228502;
// System.EventHandler`1<System.Runtime.Serialization.SafeSerializationEventArgs>
struct EventHandler_1_t3974377890;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SafeSerializationManager
struct  SafeSerializationManager_t1975884510  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<System.Object> System.Runtime.Serialization.SafeSerializationManager::m_serializedStates
	Il2CppObject* ___m_serializedStates_0;
	// System.Runtime.Serialization.SerializationInfo System.Runtime.Serialization.SafeSerializationManager::m_savedSerializationInfo
	SerializationInfo_t228987430 * ___m_savedSerializationInfo_1;
	// System.Object System.Runtime.Serialization.SafeSerializationManager::m_realObject
	Il2CppObject * ___m_realObject_2;
	// System.RuntimeType System.Runtime.Serialization.SafeSerializationManager::m_realType
	RuntimeType_t2836228502 * ___m_realType_3;
	// System.EventHandler`1<System.Runtime.Serialization.SafeSerializationEventArgs> System.Runtime.Serialization.SafeSerializationManager::SerializeObjectState
	EventHandler_1_t3974377890 * ___SerializeObjectState_4;

public:
	inline static int32_t get_offset_of_m_serializedStates_0() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t1975884510, ___m_serializedStates_0)); }
	inline Il2CppObject* get_m_serializedStates_0() const { return ___m_serializedStates_0; }
	inline Il2CppObject** get_address_of_m_serializedStates_0() { return &___m_serializedStates_0; }
	inline void set_m_serializedStates_0(Il2CppObject* value)
	{
		___m_serializedStates_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_serializedStates_0, value);
	}

	inline static int32_t get_offset_of_m_savedSerializationInfo_1() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t1975884510, ___m_savedSerializationInfo_1)); }
	inline SerializationInfo_t228987430 * get_m_savedSerializationInfo_1() const { return ___m_savedSerializationInfo_1; }
	inline SerializationInfo_t228987430 ** get_address_of_m_savedSerializationInfo_1() { return &___m_savedSerializationInfo_1; }
	inline void set_m_savedSerializationInfo_1(SerializationInfo_t228987430 * value)
	{
		___m_savedSerializationInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_savedSerializationInfo_1, value);
	}

	inline static int32_t get_offset_of_m_realObject_2() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t1975884510, ___m_realObject_2)); }
	inline Il2CppObject * get_m_realObject_2() const { return ___m_realObject_2; }
	inline Il2CppObject ** get_address_of_m_realObject_2() { return &___m_realObject_2; }
	inline void set_m_realObject_2(Il2CppObject * value)
	{
		___m_realObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_realObject_2, value);
	}

	inline static int32_t get_offset_of_m_realType_3() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t1975884510, ___m_realType_3)); }
	inline RuntimeType_t2836228502 * get_m_realType_3() const { return ___m_realType_3; }
	inline RuntimeType_t2836228502 ** get_address_of_m_realType_3() { return &___m_realType_3; }
	inline void set_m_realType_3(RuntimeType_t2836228502 * value)
	{
		___m_realType_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_realType_3, value);
	}

	inline static int32_t get_offset_of_SerializeObjectState_4() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t1975884510, ___SerializeObjectState_4)); }
	inline EventHandler_1_t3974377890 * get_SerializeObjectState_4() const { return ___SerializeObjectState_4; }
	inline EventHandler_1_t3974377890 ** get_address_of_SerializeObjectState_4() { return &___SerializeObjectState_4; }
	inline void set_SerializeObjectState_4(EventHandler_1_t3974377890 * value)
	{
		___SerializeObjectState_4 = value;
		Il2CppCodeGenWriteBarrier(&___SerializeObjectState_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
