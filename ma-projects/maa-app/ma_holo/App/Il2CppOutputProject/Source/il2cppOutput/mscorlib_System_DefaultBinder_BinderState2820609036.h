﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DefaultBinder/BinderState
struct  BinderState_t2820609036  : public Il2CppObject
{
public:
	// System.Int32[] System.DefaultBinder/BinderState::m_argsMap
	Int32U5BU5D_t3030399641* ___m_argsMap_0;
	// System.Int32 System.DefaultBinder/BinderState::m_originalSize
	int32_t ___m_originalSize_1;
	// System.Boolean System.DefaultBinder/BinderState::m_isParamArray
	bool ___m_isParamArray_2;

public:
	inline static int32_t get_offset_of_m_argsMap_0() { return static_cast<int32_t>(offsetof(BinderState_t2820609036, ___m_argsMap_0)); }
	inline Int32U5BU5D_t3030399641* get_m_argsMap_0() const { return ___m_argsMap_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_argsMap_0() { return &___m_argsMap_0; }
	inline void set_m_argsMap_0(Int32U5BU5D_t3030399641* value)
	{
		___m_argsMap_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_argsMap_0, value);
	}

	inline static int32_t get_offset_of_m_originalSize_1() { return static_cast<int32_t>(offsetof(BinderState_t2820609036, ___m_originalSize_1)); }
	inline int32_t get_m_originalSize_1() const { return ___m_originalSize_1; }
	inline int32_t* get_address_of_m_originalSize_1() { return &___m_originalSize_1; }
	inline void set_m_originalSize_1(int32_t value)
	{
		___m_originalSize_1 = value;
	}

	inline static int32_t get_offset_of_m_isParamArray_2() { return static_cast<int32_t>(offsetof(BinderState_t2820609036, ___m_isParamArray_2)); }
	inline bool get_m_isParamArray_2() const { return ___m_isParamArray_2; }
	inline bool* get_address_of_m_isParamArray_2() { return &___m_isParamArray_2; }
	inline void set_m_isParamArray_2(bool value)
	{
		___m_isParamArray_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
