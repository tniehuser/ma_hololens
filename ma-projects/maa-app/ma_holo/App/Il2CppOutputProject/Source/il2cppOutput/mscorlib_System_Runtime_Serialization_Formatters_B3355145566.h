﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo943306207.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo999493661.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_T1182459634.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1212206240.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.InternalFE
struct  InternalFE_t3355145566  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.InternalFE::FEtypeFormat
	int32_t ___FEtypeFormat_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle System.Runtime.Serialization.Formatters.Binary.InternalFE::FEassemblyFormat
	int32_t ___FEassemblyFormat_1;
	// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.InternalFE::FEsecurityLevel
	int32_t ___FEsecurityLevel_2;
	// System.Runtime.Serialization.Formatters.Binary.InternalSerializerTypeE System.Runtime.Serialization.Formatters.Binary.InternalFE::FEserializerTypeEnum
	int32_t ___FEserializerTypeEnum_3;

public:
	inline static int32_t get_offset_of_FEtypeFormat_0() { return static_cast<int32_t>(offsetof(InternalFE_t3355145566, ___FEtypeFormat_0)); }
	inline int32_t get_FEtypeFormat_0() const { return ___FEtypeFormat_0; }
	inline int32_t* get_address_of_FEtypeFormat_0() { return &___FEtypeFormat_0; }
	inline void set_FEtypeFormat_0(int32_t value)
	{
		___FEtypeFormat_0 = value;
	}

	inline static int32_t get_offset_of_FEassemblyFormat_1() { return static_cast<int32_t>(offsetof(InternalFE_t3355145566, ___FEassemblyFormat_1)); }
	inline int32_t get_FEassemblyFormat_1() const { return ___FEassemblyFormat_1; }
	inline int32_t* get_address_of_FEassemblyFormat_1() { return &___FEassemblyFormat_1; }
	inline void set_FEassemblyFormat_1(int32_t value)
	{
		___FEassemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of_FEsecurityLevel_2() { return static_cast<int32_t>(offsetof(InternalFE_t3355145566, ___FEsecurityLevel_2)); }
	inline int32_t get_FEsecurityLevel_2() const { return ___FEsecurityLevel_2; }
	inline int32_t* get_address_of_FEsecurityLevel_2() { return &___FEsecurityLevel_2; }
	inline void set_FEsecurityLevel_2(int32_t value)
	{
		___FEsecurityLevel_2 = value;
	}

	inline static int32_t get_offset_of_FEserializerTypeEnum_3() { return static_cast<int32_t>(offsetof(InternalFE_t3355145566, ___FEserializerTypeEnum_3)); }
	inline int32_t get_FEserializerTypeEnum_3() const { return ___FEserializerTypeEnum_3; }
	inline int32_t* get_address_of_FEserializerTypeEnum_3() { return &___FEserializerTypeEnum_3; }
	inline void set_FEserializerTypeEnum_3(int32_t value)
	{
		___FEserializerTypeEnum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
