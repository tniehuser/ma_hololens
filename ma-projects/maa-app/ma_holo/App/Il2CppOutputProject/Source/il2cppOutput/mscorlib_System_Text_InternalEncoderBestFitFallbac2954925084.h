﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_EncoderFallback1756452756.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.InternalEncoderBestFitFallback
struct  InternalEncoderBestFitFallback_t2954925084  : public EncoderFallback_t1756452756
{
public:
	// System.Text.Encoding System.Text.InternalEncoderBestFitFallback::encoding
	Encoding_t663144255 * ___encoding_4;
	// System.Char[] System.Text.InternalEncoderBestFitFallback::arrayBestFit
	CharU5BU5D_t1328083999* ___arrayBestFit_5;

public:
	inline static int32_t get_offset_of_encoding_4() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallback_t2954925084, ___encoding_4)); }
	inline Encoding_t663144255 * get_encoding_4() const { return ___encoding_4; }
	inline Encoding_t663144255 ** get_address_of_encoding_4() { return &___encoding_4; }
	inline void set_encoding_4(Encoding_t663144255 * value)
	{
		___encoding_4 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_4, value);
	}

	inline static int32_t get_offset_of_arrayBestFit_5() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallback_t2954925084, ___arrayBestFit_5)); }
	inline CharU5BU5D_t1328083999* get_arrayBestFit_5() const { return ___arrayBestFit_5; }
	inline CharU5BU5D_t1328083999** get_address_of_arrayBestFit_5() { return &___arrayBestFit_5; }
	inline void set_arrayBestFit_5(CharU5BU5D_t1328083999* value)
	{
		___arrayBestFit_5 = value;
		Il2CppCodeGenWriteBarrier(&___arrayBestFit_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
