﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_CompareOptions2829943955.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.SortKey
struct  SortKey_t1270563137  : public Il2CppObject
{
public:
	// System.String System.Globalization.SortKey::source
	String_t* ___source_0;
	// System.Byte[] System.Globalization.SortKey::key
	ByteU5BU5D_t3397334013* ___key_1;
	// System.Globalization.CompareOptions System.Globalization.SortKey::options
	int32_t ___options_2;
	// System.Int32 System.Globalization.SortKey::lcid
	int32_t ___lcid_3;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(SortKey_t1270563137, ___source_0)); }
	inline String_t* get_source_0() const { return ___source_0; }
	inline String_t** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(String_t* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(SortKey_t1270563137, ___key_1)); }
	inline ByteU5BU5D_t3397334013* get_key_1() const { return ___key_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(ByteU5BU5D_t3397334013* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_options_2() { return static_cast<int32_t>(offsetof(SortKey_t1270563137, ___options_2)); }
	inline int32_t get_options_2() const { return ___options_2; }
	inline int32_t* get_address_of_options_2() { return &___options_2; }
	inline void set_options_2(int32_t value)
	{
		___options_2 = value;
	}

	inline static int32_t get_offset_of_lcid_3() { return static_cast<int32_t>(offsetof(SortKey_t1270563137, ___lcid_3)); }
	inline int32_t get_lcid_3() const { return ___lcid_3; }
	inline int32_t* get_address_of_lcid_3() { return &___lcid_3; }
	inline void set_lcid_3(int32_t value)
	{
		___lcid_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.SortKey
struct SortKey_t1270563137_marshaled_pinvoke
{
	char* ___source_0;
	uint8_t* ___key_1;
	int32_t ___options_2;
	int32_t ___lcid_3;
};
// Native definition for COM marshalling of System.Globalization.SortKey
struct SortKey_t1270563137_marshaled_com
{
	Il2CppChar* ___source_0;
	uint8_t* ___key_1;
	int32_t ___options_2;
	int32_t ___lcid_3;
};
