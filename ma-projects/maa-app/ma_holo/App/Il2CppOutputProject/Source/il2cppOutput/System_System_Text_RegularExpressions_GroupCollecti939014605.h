﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.RegularExpressions.Match
struct Match_t3164245899;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t2272681992;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t939014605  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.GroupCollection::_match
	Match_t3164245899 * ____match_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.GroupCollection::_captureMap
	Hashtable_t909839986 * ____captureMap_1;
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::_groups
	GroupU5BU5D_t2272681992* ____groups_2;

public:
	inline static int32_t get_offset_of__match_0() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____match_0)); }
	inline Match_t3164245899 * get__match_0() const { return ____match_0; }
	inline Match_t3164245899 ** get_address_of__match_0() { return &____match_0; }
	inline void set__match_0(Match_t3164245899 * value)
	{
		____match_0 = value;
		Il2CppCodeGenWriteBarrier(&____match_0, value);
	}

	inline static int32_t get_offset_of__captureMap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____captureMap_1)); }
	inline Hashtable_t909839986 * get__captureMap_1() const { return ____captureMap_1; }
	inline Hashtable_t909839986 ** get_address_of__captureMap_1() { return &____captureMap_1; }
	inline void set__captureMap_1(Hashtable_t909839986 * value)
	{
		____captureMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____captureMap_1, value);
	}

	inline static int32_t get_offset_of__groups_2() { return static_cast<int32_t>(offsetof(GroupCollection_t939014605, ____groups_2)); }
	inline GroupU5BU5D_t2272681992* get__groups_2() const { return ____groups_2; }
	inline GroupU5BU5D_t2272681992** get_address_of__groups_2() { return &____groups_2; }
	inline void set__groups_2(GroupU5BU5D_t2272681992* value)
	{
		____groups_2 = value;
		Il2CppCodeGenWriteBarrier(&____groups_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
