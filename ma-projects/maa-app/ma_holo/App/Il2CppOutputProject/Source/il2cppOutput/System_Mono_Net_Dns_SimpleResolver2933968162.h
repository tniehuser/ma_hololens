﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Net.IPEndPoint[]
struct IPEndPointU5BU5D_t294524195;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs>
struct Dictionary_2_t3302389772;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Threading.TimerCallback
struct TimerCallback_t1684927372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.SimpleResolver
struct  SimpleResolver_t2933968162  : public Il2CppObject
{
public:
	// System.Net.IPEndPoint[] Mono.Net.Dns.SimpleResolver::endpoints
	IPEndPointU5BU5D_t294524195* ___endpoints_2;
	// System.Net.Sockets.Socket Mono.Net.Dns.SimpleResolver::client
	Socket_t3821512045 * ___client_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs> Mono.Net.Dns.SimpleResolver::queries
	Dictionary_2_t3302389772 * ___queries_4;
	// System.AsyncCallback Mono.Net.Dns.SimpleResolver::receive_cb
	AsyncCallback_t163412349 * ___receive_cb_5;
	// System.Threading.TimerCallback Mono.Net.Dns.SimpleResolver::timeout_cb
	TimerCallback_t1684927372 * ___timeout_cb_6;
	// System.Boolean Mono.Net.Dns.SimpleResolver::disposed
	bool ___disposed_7;

public:
	inline static int32_t get_offset_of_endpoints_2() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___endpoints_2)); }
	inline IPEndPointU5BU5D_t294524195* get_endpoints_2() const { return ___endpoints_2; }
	inline IPEndPointU5BU5D_t294524195** get_address_of_endpoints_2() { return &___endpoints_2; }
	inline void set_endpoints_2(IPEndPointU5BU5D_t294524195* value)
	{
		___endpoints_2 = value;
		Il2CppCodeGenWriteBarrier(&___endpoints_2, value);
	}

	inline static int32_t get_offset_of_client_3() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___client_3)); }
	inline Socket_t3821512045 * get_client_3() const { return ___client_3; }
	inline Socket_t3821512045 ** get_address_of_client_3() { return &___client_3; }
	inline void set_client_3(Socket_t3821512045 * value)
	{
		___client_3 = value;
		Il2CppCodeGenWriteBarrier(&___client_3, value);
	}

	inline static int32_t get_offset_of_queries_4() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___queries_4)); }
	inline Dictionary_2_t3302389772 * get_queries_4() const { return ___queries_4; }
	inline Dictionary_2_t3302389772 ** get_address_of_queries_4() { return &___queries_4; }
	inline void set_queries_4(Dictionary_2_t3302389772 * value)
	{
		___queries_4 = value;
		Il2CppCodeGenWriteBarrier(&___queries_4, value);
	}

	inline static int32_t get_offset_of_receive_cb_5() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___receive_cb_5)); }
	inline AsyncCallback_t163412349 * get_receive_cb_5() const { return ___receive_cb_5; }
	inline AsyncCallback_t163412349 ** get_address_of_receive_cb_5() { return &___receive_cb_5; }
	inline void set_receive_cb_5(AsyncCallback_t163412349 * value)
	{
		___receive_cb_5 = value;
		Il2CppCodeGenWriteBarrier(&___receive_cb_5, value);
	}

	inline static int32_t get_offset_of_timeout_cb_6() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___timeout_cb_6)); }
	inline TimerCallback_t1684927372 * get_timeout_cb_6() const { return ___timeout_cb_6; }
	inline TimerCallback_t1684927372 ** get_address_of_timeout_cb_6() { return &___timeout_cb_6; }
	inline void set_timeout_cb_6(TimerCallback_t1684927372 * value)
	{
		___timeout_cb_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeout_cb_6, value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}
};

struct SimpleResolver_t2933968162_StaticFields
{
public:
	// System.String[] Mono.Net.Dns.SimpleResolver::EmptyStrings
	StringU5BU5D_t1642385972* ___EmptyStrings_0;
	// System.Net.IPAddress[] Mono.Net.Dns.SimpleResolver::EmptyAddresses
	IPAddressU5BU5D_t4087230954* ___EmptyAddresses_1;

public:
	inline static int32_t get_offset_of_EmptyStrings_0() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162_StaticFields, ___EmptyStrings_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyStrings_0() const { return ___EmptyStrings_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyStrings_0() { return &___EmptyStrings_0; }
	inline void set_EmptyStrings_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyStrings_0 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyStrings_0, value);
	}

	inline static int32_t get_offset_of_EmptyAddresses_1() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162_StaticFields, ___EmptyAddresses_1)); }
	inline IPAddressU5BU5D_t4087230954* get_EmptyAddresses_1() const { return ___EmptyAddresses_1; }
	inline IPAddressU5BU5D_t4087230954** get_address_of_EmptyAddresses_1() { return &___EmptyAddresses_1; }
	inline void set_EmptyAddresses_1(IPAddressU5BU5D_t4087230954* value)
	{
		___EmptyAddresses_1 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyAddresses_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
