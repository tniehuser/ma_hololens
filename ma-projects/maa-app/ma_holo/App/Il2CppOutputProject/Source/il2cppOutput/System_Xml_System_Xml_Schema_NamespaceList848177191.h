﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_NamespaceList_ListTyp2879180877.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceList
struct  NamespaceList_t848177191  : public Il2CppObject
{
public:
	// System.Xml.Schema.NamespaceList/ListType System.Xml.Schema.NamespaceList::type
	int32_t ___type_0;
	// System.Collections.Hashtable System.Xml.Schema.NamespaceList::set
	Hashtable_t909839986 * ___set_1;
	// System.String System.Xml.Schema.NamespaceList::targetNamespace
	String_t* ___targetNamespace_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_set_1() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___set_1)); }
	inline Hashtable_t909839986 * get_set_1() const { return ___set_1; }
	inline Hashtable_t909839986 ** get_address_of_set_1() { return &___set_1; }
	inline void set_set_1(Hashtable_t909839986 * value)
	{
		___set_1 = value;
		Il2CppCodeGenWriteBarrier(&___set_1, value);
	}

	inline static int32_t get_offset_of_targetNamespace_2() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___targetNamespace_2)); }
	inline String_t* get_targetNamespace_2() const { return ___targetNamespace_2; }
	inline String_t** get_address_of_targetNamespace_2() { return &___targetNamespace_2; }
	inline void set_targetNamespace_2(String_t* value)
	{
		___targetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetNamespace_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
