﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsStream
struct  TlsStream_t4089752859  : public Stream_t3255436806
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canRead
	bool ___canRead_8;
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canWrite
	bool ___canWrite_9;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.TlsStream::buffer
	MemoryStream_t743994179 * ___buffer_10;
	// System.Byte[] Mono.Security.Protocol.Tls.TlsStream::temp
	ByteU5BU5D_t3397334013* ___temp_11;

public:
	inline static int32_t get_offset_of_canRead_8() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canRead_8)); }
	inline bool get_canRead_8() const { return ___canRead_8; }
	inline bool* get_address_of_canRead_8() { return &___canRead_8; }
	inline void set_canRead_8(bool value)
	{
		___canRead_8 = value;
	}

	inline static int32_t get_offset_of_canWrite_9() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canWrite_9)); }
	inline bool get_canWrite_9() const { return ___canWrite_9; }
	inline bool* get_address_of_canWrite_9() { return &___canWrite_9; }
	inline void set_canWrite_9(bool value)
	{
		___canWrite_9 = value;
	}

	inline static int32_t get_offset_of_buffer_10() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___buffer_10)); }
	inline MemoryStream_t743994179 * get_buffer_10() const { return ___buffer_10; }
	inline MemoryStream_t743994179 ** get_address_of_buffer_10() { return &___buffer_10; }
	inline void set_buffer_10(MemoryStream_t743994179 * value)
	{
		___buffer_10 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_10, value);
	}

	inline static int32_t get_offset_of_temp_11() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___temp_11)); }
	inline ByteU5BU5D_t3397334013* get_temp_11() const { return ___temp_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_temp_11() { return &___temp_11; }
	inline void set_temp_11(ByteU5BU5D_t3397334013* value)
	{
		___temp_11 = value;
		Il2CppCodeGenWriteBarrier(&___temp_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
