﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.OrderedDictionary
struct  OrderedDictionary_t14805809  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Collections.Specialized.OrderedDictionary::_objectsArray
	ArrayList_t4252133567 * ____objectsArray_0;
	// System.Collections.Hashtable System.Collections.Specialized.OrderedDictionary::_objectsTable
	Hashtable_t909839986 * ____objectsTable_1;
	// System.Int32 System.Collections.Specialized.OrderedDictionary::_initialCapacity
	int32_t ____initialCapacity_2;
	// System.Collections.IEqualityComparer System.Collections.Specialized.OrderedDictionary::_comparer
	Il2CppObject * ____comparer_3;
	// System.Boolean System.Collections.Specialized.OrderedDictionary::_readOnly
	bool ____readOnly_4;
	// System.Object System.Collections.Specialized.OrderedDictionary::_syncRoot
	Il2CppObject * ____syncRoot_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.OrderedDictionary::_siInfo
	SerializationInfo_t228987430 * ____siInfo_6;

public:
	inline static int32_t get_offset_of__objectsArray_0() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____objectsArray_0)); }
	inline ArrayList_t4252133567 * get__objectsArray_0() const { return ____objectsArray_0; }
	inline ArrayList_t4252133567 ** get_address_of__objectsArray_0() { return &____objectsArray_0; }
	inline void set__objectsArray_0(ArrayList_t4252133567 * value)
	{
		____objectsArray_0 = value;
		Il2CppCodeGenWriteBarrier(&____objectsArray_0, value);
	}

	inline static int32_t get_offset_of__objectsTable_1() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____objectsTable_1)); }
	inline Hashtable_t909839986 * get__objectsTable_1() const { return ____objectsTable_1; }
	inline Hashtable_t909839986 ** get_address_of__objectsTable_1() { return &____objectsTable_1; }
	inline void set__objectsTable_1(Hashtable_t909839986 * value)
	{
		____objectsTable_1 = value;
		Il2CppCodeGenWriteBarrier(&____objectsTable_1, value);
	}

	inline static int32_t get_offset_of__initialCapacity_2() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____initialCapacity_2)); }
	inline int32_t get__initialCapacity_2() const { return ____initialCapacity_2; }
	inline int32_t* get_address_of__initialCapacity_2() { return &____initialCapacity_2; }
	inline void set__initialCapacity_2(int32_t value)
	{
		____initialCapacity_2 = value;
	}

	inline static int32_t get_offset_of__comparer_3() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____comparer_3)); }
	inline Il2CppObject * get__comparer_3() const { return ____comparer_3; }
	inline Il2CppObject ** get_address_of__comparer_3() { return &____comparer_3; }
	inline void set__comparer_3(Il2CppObject * value)
	{
		____comparer_3 = value;
		Il2CppCodeGenWriteBarrier(&____comparer_3, value);
	}

	inline static int32_t get_offset_of__readOnly_4() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____readOnly_4)); }
	inline bool get__readOnly_4() const { return ____readOnly_4; }
	inline bool* get_address_of__readOnly_4() { return &____readOnly_4; }
	inline void set__readOnly_4(bool value)
	{
		____readOnly_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____syncRoot_5)); }
	inline Il2CppObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline Il2CppObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(Il2CppObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_5, value);
	}

	inline static int32_t get_offset_of__siInfo_6() { return static_cast<int32_t>(offsetof(OrderedDictionary_t14805809, ____siInfo_6)); }
	inline SerializationInfo_t228987430 * get__siInfo_6() const { return ____siInfo_6; }
	inline SerializationInfo_t228987430 ** get_address_of__siInfo_6() { return &____siInfo_6; }
	inline void set__siInfo_6(SerializationInfo_t228987430 * value)
	{
		____siInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&____siInfo_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
