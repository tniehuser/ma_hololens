﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_Module4282841206.h"
#include "mscorlib_System_UIntPtr1549717846.h"

// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t4254476946;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1646117627;
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t4238041457;
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t867683112;
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t3865978872;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.TypeName,System.Reflection.Emit.TypeBuilder>
struct Dictionary_2_t2316212319;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Reflection.Emit.ModuleBuilderTokenGenerator
struct ModuleBuilderTokenGenerator_t578872653;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t920863394;
struct AssemblyBuilder_t1646117627_marshaled_pinvoke;
struct MonoResource_t3127387157_marshaled_pinvoke;
struct AssemblyBuilder_t1646117627_marshaled_com;
struct MonoResource_t3127387157_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ModuleBuilder
struct  ModuleBuilder_t4156028127  : public Module_t4282841206
{
public:
	// System.UIntPtr System.Reflection.Emit.ModuleBuilder::dynamic_image
	UIntPtr_t  ___dynamic_image_10;
	// System.Int32 System.Reflection.Emit.ModuleBuilder::num_types
	int32_t ___num_types_11;
	// System.Reflection.Emit.TypeBuilder[] System.Reflection.Emit.ModuleBuilder::types
	TypeBuilderU5BU5D_t4254476946* ___types_12;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.ModuleBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_13;
	// System.Byte[] System.Reflection.Emit.ModuleBuilder::guid
	ByteU5BU5D_t3397334013* ___guid_14;
	// System.Int32 System.Reflection.Emit.ModuleBuilder::table_idx
	int32_t ___table_idx_15;
	// System.Reflection.Emit.AssemblyBuilder System.Reflection.Emit.ModuleBuilder::assemblyb
	AssemblyBuilder_t1646117627 * ___assemblyb_16;
	// System.Reflection.Emit.MethodBuilder[] System.Reflection.Emit.ModuleBuilder::global_methods
	MethodBuilderU5BU5D_t4238041457* ___global_methods_17;
	// System.Reflection.Emit.FieldBuilder[] System.Reflection.Emit.ModuleBuilder::global_fields
	FieldBuilderU5BU5D_t867683112* ___global_fields_18;
	// System.Boolean System.Reflection.Emit.ModuleBuilder::is_main
	bool ___is_main_19;
	// System.Reflection.Emit.MonoResource[] System.Reflection.Emit.ModuleBuilder::resources
	MonoResourceU5BU5D_t3865978872* ___resources_20;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::global_type
	TypeBuilder_t3308873219 * ___global_type_21;
	// System.Type System.Reflection.Emit.ModuleBuilder::global_type_created
	Type_t * ___global_type_created_22;
	// System.Collections.Generic.Dictionary`2<System.TypeName,System.Reflection.Emit.TypeBuilder> System.Reflection.Emit.ModuleBuilder::name_cache
	Dictionary_2_t2316212319 * ___name_cache_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Reflection.Emit.ModuleBuilder::us_string_cache
	Dictionary_2_t3986656710 * ___us_string_cache_24;
	// System.Int32[] System.Reflection.Emit.ModuleBuilder::table_indexes
	Int32U5BU5D_t3030399641* ___table_indexes_25;
	// System.Boolean System.Reflection.Emit.ModuleBuilder::transient
	bool ___transient_26;
	// System.Reflection.Emit.ModuleBuilderTokenGenerator System.Reflection.Emit.ModuleBuilder::token_gen
	ModuleBuilderTokenGenerator_t578872653 * ___token_gen_27;
	// System.Collections.Hashtable System.Reflection.Emit.ModuleBuilder::resource_writers
	Hashtable_t909839986 * ___resource_writers_28;
	// System.Diagnostics.SymbolStore.ISymbolWriter System.Reflection.Emit.ModuleBuilder::symbolWriter
	Il2CppObject * ___symbolWriter_29;

public:
	inline static int32_t get_offset_of_dynamic_image_10() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___dynamic_image_10)); }
	inline UIntPtr_t  get_dynamic_image_10() const { return ___dynamic_image_10; }
	inline UIntPtr_t * get_address_of_dynamic_image_10() { return &___dynamic_image_10; }
	inline void set_dynamic_image_10(UIntPtr_t  value)
	{
		___dynamic_image_10 = value;
	}

	inline static int32_t get_offset_of_num_types_11() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___num_types_11)); }
	inline int32_t get_num_types_11() const { return ___num_types_11; }
	inline int32_t* get_address_of_num_types_11() { return &___num_types_11; }
	inline void set_num_types_11(int32_t value)
	{
		___num_types_11 = value;
	}

	inline static int32_t get_offset_of_types_12() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___types_12)); }
	inline TypeBuilderU5BU5D_t4254476946* get_types_12() const { return ___types_12; }
	inline TypeBuilderU5BU5D_t4254476946** get_address_of_types_12() { return &___types_12; }
	inline void set_types_12(TypeBuilderU5BU5D_t4254476946* value)
	{
		___types_12 = value;
		Il2CppCodeGenWriteBarrier(&___types_12, value);
	}

	inline static int32_t get_offset_of_cattrs_13() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___cattrs_13)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_13() const { return ___cattrs_13; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_13() { return &___cattrs_13; }
	inline void set_cattrs_13(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_13 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_13, value);
	}

	inline static int32_t get_offset_of_guid_14() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___guid_14)); }
	inline ByteU5BU5D_t3397334013* get_guid_14() const { return ___guid_14; }
	inline ByteU5BU5D_t3397334013** get_address_of_guid_14() { return &___guid_14; }
	inline void set_guid_14(ByteU5BU5D_t3397334013* value)
	{
		___guid_14 = value;
		Il2CppCodeGenWriteBarrier(&___guid_14, value);
	}

	inline static int32_t get_offset_of_table_idx_15() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___table_idx_15)); }
	inline int32_t get_table_idx_15() const { return ___table_idx_15; }
	inline int32_t* get_address_of_table_idx_15() { return &___table_idx_15; }
	inline void set_table_idx_15(int32_t value)
	{
		___table_idx_15 = value;
	}

	inline static int32_t get_offset_of_assemblyb_16() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___assemblyb_16)); }
	inline AssemblyBuilder_t1646117627 * get_assemblyb_16() const { return ___assemblyb_16; }
	inline AssemblyBuilder_t1646117627 ** get_address_of_assemblyb_16() { return &___assemblyb_16; }
	inline void set_assemblyb_16(AssemblyBuilder_t1646117627 * value)
	{
		___assemblyb_16 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyb_16, value);
	}

	inline static int32_t get_offset_of_global_methods_17() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___global_methods_17)); }
	inline MethodBuilderU5BU5D_t4238041457* get_global_methods_17() const { return ___global_methods_17; }
	inline MethodBuilderU5BU5D_t4238041457** get_address_of_global_methods_17() { return &___global_methods_17; }
	inline void set_global_methods_17(MethodBuilderU5BU5D_t4238041457* value)
	{
		___global_methods_17 = value;
		Il2CppCodeGenWriteBarrier(&___global_methods_17, value);
	}

	inline static int32_t get_offset_of_global_fields_18() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___global_fields_18)); }
	inline FieldBuilderU5BU5D_t867683112* get_global_fields_18() const { return ___global_fields_18; }
	inline FieldBuilderU5BU5D_t867683112** get_address_of_global_fields_18() { return &___global_fields_18; }
	inline void set_global_fields_18(FieldBuilderU5BU5D_t867683112* value)
	{
		___global_fields_18 = value;
		Il2CppCodeGenWriteBarrier(&___global_fields_18, value);
	}

	inline static int32_t get_offset_of_is_main_19() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___is_main_19)); }
	inline bool get_is_main_19() const { return ___is_main_19; }
	inline bool* get_address_of_is_main_19() { return &___is_main_19; }
	inline void set_is_main_19(bool value)
	{
		___is_main_19 = value;
	}

	inline static int32_t get_offset_of_resources_20() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___resources_20)); }
	inline MonoResourceU5BU5D_t3865978872* get_resources_20() const { return ___resources_20; }
	inline MonoResourceU5BU5D_t3865978872** get_address_of_resources_20() { return &___resources_20; }
	inline void set_resources_20(MonoResourceU5BU5D_t3865978872* value)
	{
		___resources_20 = value;
		Il2CppCodeGenWriteBarrier(&___resources_20, value);
	}

	inline static int32_t get_offset_of_global_type_21() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___global_type_21)); }
	inline TypeBuilder_t3308873219 * get_global_type_21() const { return ___global_type_21; }
	inline TypeBuilder_t3308873219 ** get_address_of_global_type_21() { return &___global_type_21; }
	inline void set_global_type_21(TypeBuilder_t3308873219 * value)
	{
		___global_type_21 = value;
		Il2CppCodeGenWriteBarrier(&___global_type_21, value);
	}

	inline static int32_t get_offset_of_global_type_created_22() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___global_type_created_22)); }
	inline Type_t * get_global_type_created_22() const { return ___global_type_created_22; }
	inline Type_t ** get_address_of_global_type_created_22() { return &___global_type_created_22; }
	inline void set_global_type_created_22(Type_t * value)
	{
		___global_type_created_22 = value;
		Il2CppCodeGenWriteBarrier(&___global_type_created_22, value);
	}

	inline static int32_t get_offset_of_name_cache_23() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___name_cache_23)); }
	inline Dictionary_2_t2316212319 * get_name_cache_23() const { return ___name_cache_23; }
	inline Dictionary_2_t2316212319 ** get_address_of_name_cache_23() { return &___name_cache_23; }
	inline void set_name_cache_23(Dictionary_2_t2316212319 * value)
	{
		___name_cache_23 = value;
		Il2CppCodeGenWriteBarrier(&___name_cache_23, value);
	}

	inline static int32_t get_offset_of_us_string_cache_24() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___us_string_cache_24)); }
	inline Dictionary_2_t3986656710 * get_us_string_cache_24() const { return ___us_string_cache_24; }
	inline Dictionary_2_t3986656710 ** get_address_of_us_string_cache_24() { return &___us_string_cache_24; }
	inline void set_us_string_cache_24(Dictionary_2_t3986656710 * value)
	{
		___us_string_cache_24 = value;
		Il2CppCodeGenWriteBarrier(&___us_string_cache_24, value);
	}

	inline static int32_t get_offset_of_table_indexes_25() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___table_indexes_25)); }
	inline Int32U5BU5D_t3030399641* get_table_indexes_25() const { return ___table_indexes_25; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_indexes_25() { return &___table_indexes_25; }
	inline void set_table_indexes_25(Int32U5BU5D_t3030399641* value)
	{
		___table_indexes_25 = value;
		Il2CppCodeGenWriteBarrier(&___table_indexes_25, value);
	}

	inline static int32_t get_offset_of_transient_26() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___transient_26)); }
	inline bool get_transient_26() const { return ___transient_26; }
	inline bool* get_address_of_transient_26() { return &___transient_26; }
	inline void set_transient_26(bool value)
	{
		___transient_26 = value;
	}

	inline static int32_t get_offset_of_token_gen_27() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___token_gen_27)); }
	inline ModuleBuilderTokenGenerator_t578872653 * get_token_gen_27() const { return ___token_gen_27; }
	inline ModuleBuilderTokenGenerator_t578872653 ** get_address_of_token_gen_27() { return &___token_gen_27; }
	inline void set_token_gen_27(ModuleBuilderTokenGenerator_t578872653 * value)
	{
		___token_gen_27 = value;
		Il2CppCodeGenWriteBarrier(&___token_gen_27, value);
	}

	inline static int32_t get_offset_of_resource_writers_28() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___resource_writers_28)); }
	inline Hashtable_t909839986 * get_resource_writers_28() const { return ___resource_writers_28; }
	inline Hashtable_t909839986 ** get_address_of_resource_writers_28() { return &___resource_writers_28; }
	inline void set_resource_writers_28(Hashtable_t909839986 * value)
	{
		___resource_writers_28 = value;
		Il2CppCodeGenWriteBarrier(&___resource_writers_28, value);
	}

	inline static int32_t get_offset_of_symbolWriter_29() { return static_cast<int32_t>(offsetof(ModuleBuilder_t4156028127, ___symbolWriter_29)); }
	inline Il2CppObject * get_symbolWriter_29() const { return ___symbolWriter_29; }
	inline Il2CppObject ** get_address_of_symbolWriter_29() { return &___symbolWriter_29; }
	inline void set_symbolWriter_29(Il2CppObject * value)
	{
		___symbolWriter_29 = value;
		Il2CppCodeGenWriteBarrier(&___symbolWriter_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t4156028127_marshaled_pinvoke : public Module_t4282841206_marshaled_pinvoke
{
	uintptr_t ___dynamic_image_10;
	int32_t ___num_types_11;
	TypeBuilderU5BU5D_t4254476946* ___types_12;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_13;
	uint8_t* ___guid_14;
	int32_t ___table_idx_15;
	AssemblyBuilder_t1646117627_marshaled_pinvoke* ___assemblyb_16;
	MethodBuilderU5BU5D_t4238041457* ___global_methods_17;
	FieldBuilderU5BU5D_t867683112* ___global_fields_18;
	int32_t ___is_main_19;
	MonoResource_t3127387157_marshaled_pinvoke* ___resources_20;
	TypeBuilder_t3308873219 * ___global_type_21;
	Type_t * ___global_type_created_22;
	Dictionary_2_t2316212319 * ___name_cache_23;
	Dictionary_2_t3986656710 * ___us_string_cache_24;
	int32_t* ___table_indexes_25;
	int32_t ___transient_26;
	ModuleBuilderTokenGenerator_t578872653 * ___token_gen_27;
	Hashtable_t909839986 * ___resource_writers_28;
	Il2CppObject * ___symbolWriter_29;
};
// Native definition for COM marshalling of System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t4156028127_marshaled_com : public Module_t4282841206_marshaled_com
{
	uintptr_t ___dynamic_image_10;
	int32_t ___num_types_11;
	TypeBuilderU5BU5D_t4254476946* ___types_12;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_13;
	uint8_t* ___guid_14;
	int32_t ___table_idx_15;
	AssemblyBuilder_t1646117627_marshaled_com* ___assemblyb_16;
	MethodBuilderU5BU5D_t4238041457* ___global_methods_17;
	FieldBuilderU5BU5D_t867683112* ___global_fields_18;
	int32_t ___is_main_19;
	MonoResource_t3127387157_marshaled_com* ___resources_20;
	TypeBuilder_t3308873219 * ___global_type_21;
	Type_t * ___global_type_created_22;
	Dictionary_2_t2316212319 * ___name_cache_23;
	Dictionary_2_t3986656710 * ___us_string_cache_24;
	int32_t* ___table_indexes_25;
	int32_t ___transient_26;
	ModuleBuilderTokenGenerator_t578872653 * ___token_gen_27;
	Hashtable_t909839986 * ___resource_writers_28;
	Il2CppObject * ___symbolWriter_29;
};
