﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.Serialization.ObjectHolderList
struct ObjectHolderList_t1856843635;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectHolderListEnumerator
struct  ObjectHolderListEnumerator_t1434580149  : public Il2CppObject
{
public:
	// System.Boolean System.Runtime.Serialization.ObjectHolderListEnumerator::m_isFixupEnumerator
	bool ___m_isFixupEnumerator_0;
	// System.Runtime.Serialization.ObjectHolderList System.Runtime.Serialization.ObjectHolderListEnumerator::m_list
	ObjectHolderList_t1856843635 * ___m_list_1;
	// System.Int32 System.Runtime.Serialization.ObjectHolderListEnumerator::m_startingVersion
	int32_t ___m_startingVersion_2;
	// System.Int32 System.Runtime.Serialization.ObjectHolderListEnumerator::m_currPos
	int32_t ___m_currPos_3;

public:
	inline static int32_t get_offset_of_m_isFixupEnumerator_0() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t1434580149, ___m_isFixupEnumerator_0)); }
	inline bool get_m_isFixupEnumerator_0() const { return ___m_isFixupEnumerator_0; }
	inline bool* get_address_of_m_isFixupEnumerator_0() { return &___m_isFixupEnumerator_0; }
	inline void set_m_isFixupEnumerator_0(bool value)
	{
		___m_isFixupEnumerator_0 = value;
	}

	inline static int32_t get_offset_of_m_list_1() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t1434580149, ___m_list_1)); }
	inline ObjectHolderList_t1856843635 * get_m_list_1() const { return ___m_list_1; }
	inline ObjectHolderList_t1856843635 ** get_address_of_m_list_1() { return &___m_list_1; }
	inline void set_m_list_1(ObjectHolderList_t1856843635 * value)
	{
		___m_list_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_list_1, value);
	}

	inline static int32_t get_offset_of_m_startingVersion_2() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t1434580149, ___m_startingVersion_2)); }
	inline int32_t get_m_startingVersion_2() const { return ___m_startingVersion_2; }
	inline int32_t* get_address_of_m_startingVersion_2() { return &___m_startingVersion_2; }
	inline void set_m_startingVersion_2(int32_t value)
	{
		___m_startingVersion_2 = value;
	}

	inline static int32_t get_offset_of_m_currPos_3() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t1434580149, ___m_currPos_3)); }
	inline int32_t get_m_currPos_3() const { return ___m_currPos_3; }
	inline int32_t* get_address_of_m_currPos_3() { return &___m_currPos_3; }
	inline void set_m_currPos_3(int32_t value)
	{
		___m_currPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
