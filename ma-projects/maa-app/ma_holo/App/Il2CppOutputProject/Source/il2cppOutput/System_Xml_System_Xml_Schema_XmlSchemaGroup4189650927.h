﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_t3811767860;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t4189650927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroup
struct  XmlSchemaGroup_t4189650927  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaGroup::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroup::particle
	XmlSchemaGroupBase_t3811767860 * ___particle_10;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaGroup::canonicalParticle
	XmlSchemaParticle_t3365045970 * ___canonicalParticle_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroup::qname
	XmlQualifiedName_t1944712516 * ___qname_12;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroup::redefined
	XmlSchemaGroup_t4189650927 * ___redefined_13;
	// System.Int32 System.Xml.Schema.XmlSchemaGroup::selfReferenceCount
	int32_t ___selfReferenceCount_14;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_particle_10() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___particle_10)); }
	inline XmlSchemaGroupBase_t3811767860 * get_particle_10() const { return ___particle_10; }
	inline XmlSchemaGroupBase_t3811767860 ** get_address_of_particle_10() { return &___particle_10; }
	inline void set_particle_10(XmlSchemaGroupBase_t3811767860 * value)
	{
		___particle_10 = value;
		Il2CppCodeGenWriteBarrier(&___particle_10, value);
	}

	inline static int32_t get_offset_of_canonicalParticle_11() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___canonicalParticle_11)); }
	inline XmlSchemaParticle_t3365045970 * get_canonicalParticle_11() const { return ___canonicalParticle_11; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_canonicalParticle_11() { return &___canonicalParticle_11; }
	inline void set_canonicalParticle_11(XmlSchemaParticle_t3365045970 * value)
	{
		___canonicalParticle_11 = value;
		Il2CppCodeGenWriteBarrier(&___canonicalParticle_11, value);
	}

	inline static int32_t get_offset_of_qname_12() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___qname_12)); }
	inline XmlQualifiedName_t1944712516 * get_qname_12() const { return ___qname_12; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_12() { return &___qname_12; }
	inline void set_qname_12(XmlQualifiedName_t1944712516 * value)
	{
		___qname_12 = value;
		Il2CppCodeGenWriteBarrier(&___qname_12, value);
	}

	inline static int32_t get_offset_of_redefined_13() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___redefined_13)); }
	inline XmlSchemaGroup_t4189650927 * get_redefined_13() const { return ___redefined_13; }
	inline XmlSchemaGroup_t4189650927 ** get_address_of_redefined_13() { return &___redefined_13; }
	inline void set_redefined_13(XmlSchemaGroup_t4189650927 * value)
	{
		___redefined_13 = value;
		Il2CppCodeGenWriteBarrier(&___redefined_13, value);
	}

	inline static int32_t get_offset_of_selfReferenceCount_14() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___selfReferenceCount_14)); }
	inline int32_t get_selfReferenceCount_14() const { return ___selfReferenceCount_14; }
	inline int32_t* get_address_of_selfReferenceCount_14() { return &___selfReferenceCount_14; }
	inline void set_selfReferenceCount_14(int32_t value)
	{
		___selfReferenceCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
