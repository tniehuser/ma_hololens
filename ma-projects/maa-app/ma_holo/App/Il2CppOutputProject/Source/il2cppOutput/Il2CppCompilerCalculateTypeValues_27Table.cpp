﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaException4082200141.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaExternal3943748629.h"
#include "System_Xml_System_Xml_Schema_FacetType1331259443.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet614309579.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNumericFacet3887766756.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaLengthFacet430394943.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinLengthFac1871324785.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxLengthFac3225833099.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaPatternFacet2024909611.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaEnumerationF3989738874.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinExclusive3225721695.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinInclusiveF708563817.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxExclusive2593226405.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxInclusiveF663510947.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTotalDigitsF2939281405.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFractionDigi2708215647.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpaceFa2007056012.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase3811767860.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroup4189650927.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupRef3082205844.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons1058613623.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath604820427.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUnique403169513.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKey1946917723.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref1894386400.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaImport250324363.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInclude2752556710.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo2864028808.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNotation346281646.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollect395083109.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable3364835593.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_1126777061.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2510586510.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_1365611359.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_1879684909.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2783747869.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Occu200023993.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Empt446815059.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaRedefine3478619248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSequence728414063.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet313318308.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2303138587.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten3718357176.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2728776481.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSubstitution3854467138.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSubstitution1277095323.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse3553149267.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationEx2387044366.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"
#include "System_Xml_System_Xml_Schema_IdRefNode224554150.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator1978690704.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType3547578624.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode58293802.h"
#include "System_Xml_System_Xml_Schema_XmlValueConverter68179724.h"
#include "System_Xml_System_Xml_Schema_XmlBaseConverter217670330.h"
#include "System_Xml_System_Xml_Schema_XmlNumeric10Converter652743775.h"
#include "System_Xml_System_Xml_Schema_XmlNumeric2Converter429464530.h"
#include "System_Xml_System_Xml_Schema_XmlDateTimeConverter3001450694.h"
#include "System_Xml_System_Xml_Schema_XmlBooleanConverter375919885.h"
#include "System_Xml_System_Xml_Schema_XmlMiscConverter2996460861.h"
#include "System_Xml_System_Xml_Schema_XmlStringConverter2927339906.h"
#include "System_Xml_System_Xml_Schema_XmlUntypedConverter3998072860.h"
#include "System_Xml_System_Xml_Schema_XmlAnyConverter1207259383.h"
#include "System_Xml_System_Xml_Schema_XmlAnyListConverter2170868107.h"
#include "System_Xml_System_Xml_Schema_XmlListConverter888285589.h"
#include "System_Xml_System_Xml_Schema_XmlUnionConverter2065897640.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder2784092428.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_State2667800191.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_XsdBuildFu2636231335.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_XsdInitFunc862750143.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_XsdEndChild839604892.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_XsdAttribu2279094079.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_XsdEntry2653064619.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_BuilderNames73376825.h"
#include "System_Xml_System_Xml_Schema_XsdDateTimeFlags2172197783.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime2434467484.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime_DateTimeTy563167880.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime_XsdDateTi2077819616.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime_Parser2170101039.h"
#include "System_Xml_System_Xml_Schema_XsdDuration2624036407.h"
#include "System_Xml_System_Xml_Schema_XsdDuration_Parts3593783327.h"
#include "System_Xml_System_Xml_Schema_XsdDuration_DurationT1846793513.h"
#include "System_Xml_System_Xml_Schema_XsdValidator535151321.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribute713947513.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA2502375235.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (XmlSchemaException_t4082200141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[7] = 
{
	XmlSchemaException_t4082200141::get_offset_of_res_16(),
	XmlSchemaException_t4082200141::get_offset_of_args_17(),
	XmlSchemaException_t4082200141::get_offset_of_sourceUri_18(),
	XmlSchemaException_t4082200141::get_offset_of_lineNumber_19(),
	XmlSchemaException_t4082200141::get_offset_of_linePosition_20(),
	XmlSchemaException_t4082200141::get_offset_of_sourceSchemaObject_21(),
	XmlSchemaException_t4082200141::get_offset_of_message_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (XmlSchemaExternal_t3943748629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[6] = 
{
	XmlSchemaExternal_t3943748629::get_offset_of_location_6(),
	XmlSchemaExternal_t3943748629::get_offset_of_baseUri_7(),
	XmlSchemaExternal_t3943748629::get_offset_of_schema_8(),
	XmlSchemaExternal_t3943748629::get_offset_of_id_9(),
	XmlSchemaExternal_t3943748629::get_offset_of_moreAttributes_10(),
	XmlSchemaExternal_t3943748629::get_offset_of_compositor_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (FacetType_t1331259443)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2702[14] = 
{
	FacetType_t1331259443::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (XmlSchemaFacet_t614309579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[3] = 
{
	XmlSchemaFacet_t614309579::get_offset_of_value_9(),
	XmlSchemaFacet_t614309579::get_offset_of_isFixed_10(),
	XmlSchemaFacet_t614309579::get_offset_of_facetType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (XmlSchemaNumericFacet_t3887766756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (XmlSchemaLengthFacet_t430394943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (XmlSchemaMinLengthFacet_t1871324785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (XmlSchemaMaxLengthFacet_t3225833099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (XmlSchemaPatternFacet_t2024909611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (XmlSchemaEnumerationFacet_t3989738874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (XmlSchemaMinExclusiveFacet_t3225721695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (XmlSchemaMinInclusiveFacet_t708563817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (XmlSchemaMaxExclusiveFacet_t2593226405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (XmlSchemaMaxInclusiveFacet_t663510947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (XmlSchemaTotalDigitsFacet_t2939281405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (XmlSchemaFractionDigitsFacet_t2708215647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (XmlSchemaWhiteSpaceFacet_t2007056012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (XmlSchemaForm_t1143227640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	XmlSchemaForm_t1143227640::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (XmlSchemaGroupBase_t3811767860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (XmlSchemaGroup_t4189650927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[6] = 
{
	XmlSchemaGroup_t4189650927::get_offset_of_name_9(),
	XmlSchemaGroup_t4189650927::get_offset_of_particle_10(),
	XmlSchemaGroup_t4189650927::get_offset_of_canonicalParticle_11(),
	XmlSchemaGroup_t4189650927::get_offset_of_qname_12(),
	XmlSchemaGroup_t4189650927::get_offset_of_redefined_13(),
	XmlSchemaGroup_t4189650927::get_offset_of_selfReferenceCount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (XmlSchemaGroupRef_t3082205844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[3] = 
{
	XmlSchemaGroupRef_t3082205844::get_offset_of_refName_13(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_particle_14(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_refined_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (XmlSchemaIdentityConstraint_t1058613623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[5] = 
{
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_name_9(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_selector_10(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_fields_11(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_qualifiedName_12(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_compiledConstraint_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (XmlSchemaXPath_t604820427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[1] = 
{
	XmlSchemaXPath_t604820427::get_offset_of_xpath_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (XmlSchemaUnique_t403169513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (XmlSchemaKey_t1946917723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (XmlSchemaKeyref_t1894386400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[1] = 
{
	XmlSchemaKeyref_t1894386400::get_offset_of_refer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (XmlSchemaImport_t250324363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[2] = 
{
	XmlSchemaImport_t250324363::get_offset_of_ns_12(),
	XmlSchemaImport_t250324363::get_offset_of_annotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (XmlSchemaInclude_t2752556710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	XmlSchemaInclude_t2752556710::get_offset_of_annotation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[8] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_schemaElement_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_schemaAttribute_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_schemaType_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
	XmlSchemaInfo_t2864028808::get_offset_of_contentType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (XmlSchemaNotation_t346281646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[4] = 
{
	XmlSchemaNotation_t346281646::get_offset_of_name_9(),
	XmlSchemaNotation_t346281646::get_offset_of_publicId_10(),
	XmlSchemaNotation_t346281646::get_offset_of_systemId_11(),
	XmlSchemaNotation_t346281646::get_offset_of_qname_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (XmlSchemaObjectCollection_t395083109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[1] = 
{
	XmlSchemaObjectCollection_t395083109::get_offset_of_parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[6] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_lineNum_0(),
	XmlSchemaObject_t2050913741::get_offset_of_linePos_1(),
	XmlSchemaObject_t2050913741::get_offset_of_sourceUri_2(),
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_3(),
	XmlSchemaObject_t2050913741::get_offset_of_parent_4(),
	XmlSchemaObject_t2050913741::get_offset_of_isProcessing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (XmlSchemaObjectTable_t3364835593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[2] = 
{
	XmlSchemaObjectTable_t3364835593::get_offset_of_table_0(),
	XmlSchemaObjectTable_t3364835593::get_offset_of_entries_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (EnumeratorType_t1126777061)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2733[4] = 
{
	EnumeratorType_t1126777061::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (XmlSchemaObjectEntry_t2510586510)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	XmlSchemaObjectEntry_t2510586510::get_offset_of_qname_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XmlSchemaObjectEntry_t2510586510::get_offset_of_xso_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (ValuesCollection_t1365611359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[2] = 
{
	ValuesCollection_t1365611359::get_offset_of_entries_0(),
	ValuesCollection_t1365611359::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (XSOEnumerator_t1879684909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[6] = 
{
	XSOEnumerator_t1879684909::get_offset_of_entries_0(),
	XSOEnumerator_t1879684909::get_offset_of_enumType_1(),
	XSOEnumerator_t1879684909::get_offset_of_currentIndex_2(),
	XSOEnumerator_t1879684909::get_offset_of_size_3(),
	XSOEnumerator_t1879684909::get_offset_of_currentKey_4(),
	XSOEnumerator_t1879684909::get_offset_of_currentValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (XSODictionaryEnumerator_t2783747869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (XmlSchemaParticle_t3365045970), -1, sizeof(XmlSchemaParticle_t3365045970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[4] = 
{
	XmlSchemaParticle_t3365045970::get_offset_of_minOccurs_9(),
	XmlSchemaParticle_t3365045970::get_offset_of_maxOccurs_10(),
	XmlSchemaParticle_t3365045970::get_offset_of_flags_11(),
	XmlSchemaParticle_t3365045970_StaticFields::get_offset_of_Empty_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (Occurs_t200023993)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2739[4] = 
{
	Occurs_t200023993::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (EmptyParticle_t446815059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (XmlSchemaRedefine_t3478619248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[4] = 
{
	XmlSchemaRedefine_t3478619248::get_offset_of_items_12(),
	XmlSchemaRedefine_t3478619248::get_offset_of_attributeGroups_13(),
	XmlSchemaRedefine_t3478619248::get_offset_of_types_14(),
	XmlSchemaRedefine_t3478619248::get_offset_of_groups_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (XmlSchemaSequence_t728414063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[1] = 
{
	XmlSchemaSequence_t728414063::get_offset_of_items_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (XmlSchemaSet_t313318308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[12] = 
{
	XmlSchemaSet_t313318308::get_offset_of_nameTable_0(),
	XmlSchemaSet_t313318308::get_offset_of_schemas_1(),
	XmlSchemaSet_t313318308::get_offset_of_internalEventHandler_2(),
	XmlSchemaSet_t313318308::get_offset_of_eventHandler_3(),
	XmlSchemaSet_t313318308::get_offset_of_schemaLocations_4(),
	XmlSchemaSet_t313318308::get_offset_of_chameleonSchemas_5(),
	XmlSchemaSet_t313318308::get_offset_of_targetNamespaces_6(),
	XmlSchemaSet_t313318308::get_offset_of_compileAll_7(),
	XmlSchemaSet_t313318308::get_offset_of_cachedCompiledInfo_8(),
	XmlSchemaSet_t313318308::get_offset_of_readerSettings_9(),
	XmlSchemaSet_t313318308::get_offset_of_compilationSettings_10(),
	XmlSchemaSet_t313318308::get_offset_of_substitutionGroups_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (XmlSchemaSimpleContent_t2303138587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[1] = 
{
	XmlSchemaSimpleContent_t2303138587::get_offset_of_content_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (XmlSchemaSimpleContentExtension_t3718357176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[3] = 
{
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_attributes_9(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_anyAttribute_10(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_baseTypeName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (XmlSchemaSimpleContentRestriction_t2728776481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[5] = 
{
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseTypeName_9(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseType_10(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_facets_11(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_attributes_12(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_anyAttribute_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (XmlSchemaSimpleType_t248156492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[1] = 
{
	XmlSchemaSimpleType_t248156492::get_offset_of_content_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[3] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_9(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_10(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_baseItemType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[3] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseTypeName_9(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseType_10(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_facets_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[3] = 
{
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_baseTypes_9(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_memberTypes_10(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_baseMemberTypes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (XmlSchemaSubstitutionGroup_t3854467138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[2] = 
{
	XmlSchemaSubstitutionGroup_t3854467138::get_offset_of_membersList_6(),
	XmlSchemaSubstitutionGroup_t3854467138::get_offset_of_examplar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (XmlSchemaSubstitutionGroupV1Compat_t1277095323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	XmlSchemaSubstitutionGroupV1Compat_t1277095323::get_offset_of_choice_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (XmlSchemaType_t1795078578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[10] = 
{
	XmlSchemaType_t1795078578::get_offset_of_name_9(),
	XmlSchemaType_t1795078578::get_offset_of_final_10(),
	XmlSchemaType_t1795078578::get_offset_of_derivedBy_11(),
	XmlSchemaType_t1795078578::get_offset_of_baseSchemaType_12(),
	XmlSchemaType_t1795078578::get_offset_of_datatype_13(),
	XmlSchemaType_t1795078578::get_offset_of_finalResolved_14(),
	XmlSchemaType_t1795078578::get_offset_of_elementDecl_15(),
	XmlSchemaType_t1795078578::get_offset_of_qname_16(),
	XmlSchemaType_t1795078578::get_offset_of_redefined_17(),
	XmlSchemaType_t1795078578::get_offset_of_contentType_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (XmlSchemaUse_t3553149267)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2755[5] = 
{
	XmlSchemaUse_t3553149267::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (XmlSchemaValidationException_t2387044366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (XmlSchemaValidationFlags_t910489930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[7] = 
{
	XmlSchemaValidationFlags_t910489930::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (IdRefNode_t224554150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[4] = 
{
	IdRefNode_t224554150::get_offset_of_Id_0(),
	IdRefNode_t224554150::get_offset_of_LineNo_1(),
	IdRefNode_t224554150::get_offset_of_LinePos_2(),
	IdRefNode_t224554150::get_offset_of_Next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (XmlSchemaValidator_t1978690704), -1, sizeof(XmlSchemaValidator_t1978690704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2759[7] = 
{
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_dtQName_0(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_dtCDATA_1(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_dtStringArray_2(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_EmptyParticleArray_3(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_EmptyAttributeArray_4(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_ValidStates_5(),
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_MethodNames_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2760[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (XmlSeverityType_t3547578624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	XmlSeverityType_t3547578624::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2762[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (XmlTypeCode_t58293802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2763[56] = 
{
	XmlTypeCode_t58293802::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (XmlValueConverter_t68179724), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (XmlBaseConverter_t217670330), -1, sizeof(XmlBaseConverter_t217670330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2765[32] = 
{
	XmlBaseConverter_t217670330::get_offset_of_schemaType_0(),
	XmlBaseConverter_t217670330::get_offset_of_typeCode_1(),
	XmlBaseConverter_t217670330::get_offset_of_clrTypeDefault_2(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_ICollectionType_3(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_IEnumerableType_4(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_IListType_5(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_ObjectArrayType_6(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_StringArrayType_7(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_XmlAtomicValueArrayType_8(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_DecimalType_9(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_Int32Type_10(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_Int64Type_11(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_StringType_12(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_XmlAtomicValueType_13(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_ObjectType_14(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_ByteType_15(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_Int16Type_16(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_SByteType_17(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_UInt16Type_18(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_UInt32Type_19(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_UInt64Type_20(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_XPathItemType_21(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_DoubleType_22(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_SingleType_23(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_DateTimeType_24(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_DateTimeOffsetType_25(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_BooleanType_26(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_ByteArrayType_27(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_XmlQualifiedNameType_28(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_UriType_29(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_TimeSpanType_30(),
	XmlBaseConverter_t217670330_StaticFields::get_offset_of_XPathNavigatorType_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (XmlNumeric10Converter_t652743775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (XmlNumeric2Converter_t429464530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (XmlDateTimeConverter_t3001450694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (XmlBooleanConverter_t375919885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (XmlMiscConverter_t2996460861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (XmlStringConverter_t2927339906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (XmlUntypedConverter_t3998072860), -1, sizeof(XmlUntypedConverter_t3998072860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[3] = 
{
	XmlUntypedConverter_t3998072860::get_offset_of_allowListToList_33(),
	XmlUntypedConverter_t3998072860_StaticFields::get_offset_of_Untyped_34(),
	XmlUntypedConverter_t3998072860_StaticFields::get_offset_of_UntypedList_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (XmlAnyConverter_t1207259383), -1, sizeof(XmlAnyConverter_t1207259383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2773[2] = 
{
	XmlAnyConverter_t1207259383_StaticFields::get_offset_of_Item_32(),
	XmlAnyConverter_t1207259383_StaticFields::get_offset_of_AnyAtomic_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (XmlAnyListConverter_t2170868107), -1, sizeof(XmlAnyListConverter_t2170868107_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2774[2] = 
{
	XmlAnyListConverter_t2170868107_StaticFields::get_offset_of_ItemList_33(),
	XmlAnyListConverter_t2170868107_StaticFields::get_offset_of_AnyAtomicList_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (XmlListConverter_t888285589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[1] = 
{
	XmlListConverter_t888285589::get_offset_of_atomicConverter_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (XmlUnionConverter_t2065897640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[3] = 
{
	XmlUnionConverter_t2065897640::get_offset_of_converters_32(),
	XmlUnionConverter_t2065897640::get_offset_of_hasAtomicMember_33(),
	XmlUnionConverter_t2065897640::get_offset_of_hasListMember_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (XsdBuilder_t2784092428), -1, sizeof(XsdBuilder_t2784092428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[111] = 
{
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SchemaElement_0(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SchemaSubelements_1(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AttributeSubelements_2(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ElementSubelements_3(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexTypeSubelements_4(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentSubelements_5(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentExtensionSubelements_6(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentRestrictionSubelements_7(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentSubelements_8(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentExtensionSubelements_9(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentRestrictionSubelements_10(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeSubelements_11(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeRestrictionSubelements_12(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeListSubelements_13(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeUnionSubelements_14(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_RedefineSubelements_15(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AttributeGroupSubelements_16(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_GroupSubelements_17(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AllSubelements_18(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ChoiceSequenceSubelements_19(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_IdentityConstraintSubelements_20(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AnnotationSubelements_21(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AnnotatedSubelements_22(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SchemaAttributes_23(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AttributeAttributes_24(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ElementAttributes_25(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexTypeAttributes_26(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentAttributes_27(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentExtensionAttributes_28(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleContentRestrictionAttributes_29(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentAttributes_30(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentExtensionAttributes_31(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ComplexContentRestrictionAttributes_32(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeAttributes_33(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeRestrictionAttributes_34(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeUnionAttributes_35(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SimpleTypeListAttributes_36(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AttributeGroupAttributes_37(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AttributeGroupRefAttributes_38(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_GroupAttributes_39(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_GroupRefAttributes_40(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ParticleAttributes_41(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AnyAttributes_42(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_IdentityConstraintAttributes_43(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SelectorAttributes_44(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_FieldAttributes_45(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_NotationAttributes_46(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_IncludeAttributes_47(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ImportAttributes_48(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_FacetAttributes_49(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AnyAttributeAttributes_50(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_DocumentationAttributes_51(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AppinfoAttributes_52(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_RedefineAttributes_53(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_AnnotationAttributes_54(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_SchemaEntries_55(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_DerivationMethodValues_56(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_DerivationMethodStrings_57(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_FormStringValues_58(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_UseStringValues_59(),
	XsdBuilder_t2784092428_StaticFields::get_offset_of_ProcessContentsStringValues_60(),
	XsdBuilder_t2784092428::get_offset_of_reader_61(),
	XsdBuilder_t2784092428::get_offset_of_positionInfo_62(),
	XsdBuilder_t2784092428::get_offset_of_currentEntry_63(),
	XsdBuilder_t2784092428::get_offset_of_nextEntry_64(),
	XsdBuilder_t2784092428::get_offset_of_hasChild_65(),
	XsdBuilder_t2784092428::get_offset_of_stateHistory_66(),
	XsdBuilder_t2784092428::get_offset_of_containerStack_67(),
	XsdBuilder_t2784092428::get_offset_of_nameTable_68(),
	XsdBuilder_t2784092428::get_offset_of_schemaNames_69(),
	XsdBuilder_t2784092428::get_offset_of_namespaceManager_70(),
	XsdBuilder_t2784092428::get_offset_of_canIncludeImport_71(),
	XsdBuilder_t2784092428::get_offset_of_schema_72(),
	XsdBuilder_t2784092428::get_offset_of_xso_73(),
	XsdBuilder_t2784092428::get_offset_of_element_74(),
	XsdBuilder_t2784092428::get_offset_of_anyElement_75(),
	XsdBuilder_t2784092428::get_offset_of_attribute_76(),
	XsdBuilder_t2784092428::get_offset_of_anyAttribute_77(),
	XsdBuilder_t2784092428::get_offset_of_complexType_78(),
	XsdBuilder_t2784092428::get_offset_of_simpleType_79(),
	XsdBuilder_t2784092428::get_offset_of_complexContent_80(),
	XsdBuilder_t2784092428::get_offset_of_complexContentExtension_81(),
	XsdBuilder_t2784092428::get_offset_of_complexContentRestriction_82(),
	XsdBuilder_t2784092428::get_offset_of_simpleContent_83(),
	XsdBuilder_t2784092428::get_offset_of_simpleContentExtension_84(),
	XsdBuilder_t2784092428::get_offset_of_simpleContentRestriction_85(),
	XsdBuilder_t2784092428::get_offset_of_simpleTypeUnion_86(),
	XsdBuilder_t2784092428::get_offset_of_simpleTypeList_87(),
	XsdBuilder_t2784092428::get_offset_of_simpleTypeRestriction_88(),
	XsdBuilder_t2784092428::get_offset_of_group_89(),
	XsdBuilder_t2784092428::get_offset_of_groupRef_90(),
	XsdBuilder_t2784092428::get_offset_of_all_91(),
	XsdBuilder_t2784092428::get_offset_of_choice_92(),
	XsdBuilder_t2784092428::get_offset_of_sequence_93(),
	XsdBuilder_t2784092428::get_offset_of_particle_94(),
	XsdBuilder_t2784092428::get_offset_of_attributeGroup_95(),
	XsdBuilder_t2784092428::get_offset_of_attributeGroupRef_96(),
	XsdBuilder_t2784092428::get_offset_of_notation_97(),
	XsdBuilder_t2784092428::get_offset_of_identityConstraint_98(),
	XsdBuilder_t2784092428::get_offset_of_xpath_99(),
	XsdBuilder_t2784092428::get_offset_of_include_100(),
	XsdBuilder_t2784092428::get_offset_of_import_101(),
	XsdBuilder_t2784092428::get_offset_of_annotation_102(),
	XsdBuilder_t2784092428::get_offset_of_appInfo_103(),
	XsdBuilder_t2784092428::get_offset_of_documentation_104(),
	XsdBuilder_t2784092428::get_offset_of_facet_105(),
	XsdBuilder_t2784092428::get_offset_of_markup_106(),
	XsdBuilder_t2784092428::get_offset_of_redefine_107(),
	XsdBuilder_t2784092428::get_offset_of_validationEventHandler_108(),
	XsdBuilder_t2784092428::get_offset_of_unhandledAttributes_109(),
	XsdBuilder_t2784092428::get_offset_of_namespaces_110(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (State_t2667800191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2778[49] = 
{
	State_t2667800191::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (XsdBuildFunction_t2636231335), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (XsdInitFunction_t862750143), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (XsdEndChildFunction_t839604892), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (XsdAttributeEntry_t2279094079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[2] = 
{
	XsdAttributeEntry_t2279094079::get_offset_of_Attribute_0(),
	XsdAttributeEntry_t2279094079::get_offset_of_BuildFunc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (XsdEntry_t2653064619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[7] = 
{
	XsdEntry_t2653064619::get_offset_of_Name_0(),
	XsdEntry_t2653064619::get_offset_of_CurrentState_1(),
	XsdEntry_t2653064619::get_offset_of_NextStates_2(),
	XsdEntry_t2653064619::get_offset_of_Attributes_3(),
	XsdEntry_t2653064619::get_offset_of_InitFunc_4(),
	XsdEntry_t2653064619::get_offset_of_EndChildFunc_5(),
	XsdEntry_t2653064619::get_offset_of_ParseContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (BuilderNamespaceManager_t73376825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[2] = 
{
	BuilderNamespaceManager_t73376825::get_offset_of_nsMgr_8(),
	BuilderNamespaceManager_t73376825::get_offset_of_reader_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (XsdDateTimeFlags_t2172197783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2785[13] = 
{
	XsdDateTimeFlags_t2172197783::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (XsdDateTime_t2434467484)+ sizeof (Il2CppObject), -1, sizeof(XsdDateTime_t2434467484_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2786[25] = 
{
	XsdDateTime_t2434467484::get_offset_of_dt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDateTime_t2434467484::get_offset_of_extra_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy_2(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy__3(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy_MM_4(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy_MM__5(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy_MM_dd_6(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lzyyyy_MM_ddT_7(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_LzHH_8(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_LzHH__9(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_LzHH_mm_10(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_LzHH_mm__11(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_LzHH_mm_ss_12(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz__13(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz_zz_14(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz_zz__15(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz_zz_zz_16(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz___17(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz__mm_18(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz__mm__19(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz__mm___20(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz__mm_dd_21(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz____22(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_Lz___dd_23(),
	XsdDateTime_t2434467484_StaticFields::get_offset_of_typeCodes_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (DateTimeTypeCode_t563167880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2787[10] = 
{
	DateTimeTypeCode_t563167880::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (XsdDateTimeKind_t2077819616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[5] = 
{
	XsdDateTimeKind_t2077819616::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (Parser_t2170101039)+ sizeof (Il2CppObject), sizeof(Parser_t2170101039_marshaled_pinvoke), sizeof(Parser_t2170101039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[14] = 
{
	Parser_t2170101039::get_offset_of_typeCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_year_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_month_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_day_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_hour_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_minute_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_second_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_fraction_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_kind_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_zoneHour_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_zoneMinute_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_text_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039::get_offset_of_length_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Parser_t2170101039_StaticFields::get_offset_of_Power10_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (XsdDuration_t2624036407)+ sizeof (Il2CppObject), sizeof(XsdDuration_t2624036407 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[7] = 
{
	XsdDuration_t2624036407::get_offset_of_years_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_months_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_days_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_hours_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_minutes_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_seconds_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XsdDuration_t2624036407::get_offset_of_nanoseconds_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (Parts_t3593783327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[8] = 
{
	Parts_t3593783327::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (DurationType_t1846793513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[4] = 
{
	DurationType_t1846793513::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (XsdValidator_t535151321), -1, sizeof(XsdValidator_t535151321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2793[20] = 
{
	XsdValidator_t535151321::get_offset_of_startIDConstraint_15(),
	XsdValidator_t535151321::get_offset_of_validationStack_16(),
	XsdValidator_t535151321::get_offset_of_attPresence_17(),
	XsdValidator_t535151321::get_offset_of_nsManager_18(),
	XsdValidator_t535151321::get_offset_of_bManageNamespaces_19(),
	XsdValidator_t535151321::get_offset_of_IDs_20(),
	XsdValidator_t535151321::get_offset_of_idRefListHead_21(),
	XsdValidator_t535151321::get_offset_of_inlineSchemaParser_22(),
	XsdValidator_t535151321::get_offset_of_processContents_23(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtCDATA_24(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtQName_25(),
	XsdValidator_t535151321_StaticFields::get_offset_of_dtStringArray_26(),
	XsdValidator_t535151321::get_offset_of_NsXmlNs_27(),
	XsdValidator_t535151321::get_offset_of_NsXs_28(),
	XsdValidator_t535151321::get_offset_of_NsXsi_29(),
	XsdValidator_t535151321::get_offset_of_XsiType_30(),
	XsdValidator_t535151321::get_offset_of_XsiNil_31(),
	XsdValidator_t535151321::get_offset_of_XsiSchemaLocation_32(),
	XsdValidator_t535151321::get_offset_of_XsiNoNamespaceSchemaLocation_33(),
	XsdValidator_t535151321::get_offset_of_XsdSchema_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[1] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[2] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
