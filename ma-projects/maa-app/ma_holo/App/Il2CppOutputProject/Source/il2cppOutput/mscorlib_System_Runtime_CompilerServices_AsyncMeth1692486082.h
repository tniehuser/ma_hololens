﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action
struct Action_t3226471752;
// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore/ContinuationWrapper
struct  ContinuationWrapper_t1692486082  : public Il2CppObject
{
public:
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore/ContinuationWrapper::m_continuation
	Action_t3226471752 * ___m_continuation_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore/ContinuationWrapper::m_invokeAction
	Action_t3226471752 * ___m_invokeAction_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncMethodBuilderCore/ContinuationWrapper::m_innerTask
	Task_t1843236107 * ___m_innerTask_2;

public:
	inline static int32_t get_offset_of_m_continuation_0() { return static_cast<int32_t>(offsetof(ContinuationWrapper_t1692486082, ___m_continuation_0)); }
	inline Action_t3226471752 * get_m_continuation_0() const { return ___m_continuation_0; }
	inline Action_t3226471752 ** get_address_of_m_continuation_0() { return &___m_continuation_0; }
	inline void set_m_continuation_0(Action_t3226471752 * value)
	{
		___m_continuation_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_continuation_0, value);
	}

	inline static int32_t get_offset_of_m_invokeAction_1() { return static_cast<int32_t>(offsetof(ContinuationWrapper_t1692486082, ___m_invokeAction_1)); }
	inline Action_t3226471752 * get_m_invokeAction_1() const { return ___m_invokeAction_1; }
	inline Action_t3226471752 ** get_address_of_m_invokeAction_1() { return &___m_invokeAction_1; }
	inline void set_m_invokeAction_1(Action_t3226471752 * value)
	{
		___m_invokeAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_invokeAction_1, value);
	}

	inline static int32_t get_offset_of_m_innerTask_2() { return static_cast<int32_t>(offsetof(ContinuationWrapper_t1692486082, ___m_innerTask_2)); }
	inline Task_t1843236107 * get_m_innerTask_2() const { return ___m_innerTask_2; }
	inline Task_t1843236107 ** get_address_of_m_innerTask_2() { return &___m_innerTask_2; }
	inline void set_m_innerTask_2(Task_t1843236107 * value)
	{
		___m_innerTask_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_innerTask_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
