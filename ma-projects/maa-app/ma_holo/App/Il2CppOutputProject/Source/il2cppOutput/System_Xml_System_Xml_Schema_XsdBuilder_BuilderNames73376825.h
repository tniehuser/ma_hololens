﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"

// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.XmlReader
struct XmlReader_t3675626668;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/BuilderNamespaceManager
struct  BuilderNamespaceManager_t73376825  : public XmlNamespaceManager_t486731501
{
public:
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdBuilder/BuilderNamespaceManager::nsMgr
	XmlNamespaceManager_t486731501 * ___nsMgr_8;
	// System.Xml.XmlReader System.Xml.Schema.XsdBuilder/BuilderNamespaceManager::reader
	XmlReader_t3675626668 * ___reader_9;

public:
	inline static int32_t get_offset_of_nsMgr_8() { return static_cast<int32_t>(offsetof(BuilderNamespaceManager_t73376825, ___nsMgr_8)); }
	inline XmlNamespaceManager_t486731501 * get_nsMgr_8() const { return ___nsMgr_8; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsMgr_8() { return &___nsMgr_8; }
	inline void set_nsMgr_8(XmlNamespaceManager_t486731501 * value)
	{
		___nsMgr_8 = value;
		Il2CppCodeGenWriteBarrier(&___nsMgr_8, value);
	}

	inline static int32_t get_offset_of_reader_9() { return static_cast<int32_t>(offsetof(BuilderNamespaceManager_t73376825, ___reader_9)); }
	inline XmlReader_t3675626668 * get_reader_9() const { return ___reader_9; }
	inline XmlReader_t3675626668 ** get_address_of_reader_9() { return &___reader_9; }
	inline void set_reader_9(XmlReader_t3675626668 * value)
	{
		___reader_9 = value;
		Il2CppCodeGenWriteBarrier(&___reader_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
