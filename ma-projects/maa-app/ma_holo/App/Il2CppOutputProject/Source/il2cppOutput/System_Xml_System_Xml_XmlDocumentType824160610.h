﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"

// System.String
struct String_t;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t145210370;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t824160610  : public XmlLinkedNode_t1287616130
{
public:
	// System.String System.Xml.XmlDocumentType::name
	String_t* ___name_2;
	// System.String System.Xml.XmlDocumentType::publicId
	String_t* ___publicId_3;
	// System.String System.Xml.XmlDocumentType::systemId
	String_t* ___systemId_4;
	// System.String System.Xml.XmlDocumentType::internalSubset
	String_t* ___internalSubset_5;
	// System.Boolean System.Xml.XmlDocumentType::namespaces
	bool ___namespaces_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t145210370 * ___entities_7;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t145210370 * ___notations_8;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocumentType::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_9;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_publicId_3() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___publicId_3)); }
	inline String_t* get_publicId_3() const { return ___publicId_3; }
	inline String_t** get_address_of_publicId_3() { return &___publicId_3; }
	inline void set_publicId_3(String_t* value)
	{
		___publicId_3 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_3, value);
	}

	inline static int32_t get_offset_of_systemId_4() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___systemId_4)); }
	inline String_t* get_systemId_4() const { return ___systemId_4; }
	inline String_t** get_address_of_systemId_4() { return &___systemId_4; }
	inline void set_systemId_4(String_t* value)
	{
		___systemId_4 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_4, value);
	}

	inline static int32_t get_offset_of_internalSubset_5() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___internalSubset_5)); }
	inline String_t* get_internalSubset_5() const { return ___internalSubset_5; }
	inline String_t** get_address_of_internalSubset_5() { return &___internalSubset_5; }
	inline void set_internalSubset_5(String_t* value)
	{
		___internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier(&___internalSubset_5, value);
	}

	inline static int32_t get_offset_of_namespaces_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___namespaces_6)); }
	inline bool get_namespaces_6() const { return ___namespaces_6; }
	inline bool* get_address_of_namespaces_6() { return &___namespaces_6; }
	inline void set_namespaces_6(bool value)
	{
		___namespaces_6 = value;
	}

	inline static int32_t get_offset_of_entities_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___entities_7)); }
	inline XmlNamedNodeMap_t145210370 * get_entities_7() const { return ___entities_7; }
	inline XmlNamedNodeMap_t145210370 ** get_address_of_entities_7() { return &___entities_7; }
	inline void set_entities_7(XmlNamedNodeMap_t145210370 * value)
	{
		___entities_7 = value;
		Il2CppCodeGenWriteBarrier(&___entities_7, value);
	}

	inline static int32_t get_offset_of_notations_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___notations_8)); }
	inline XmlNamedNodeMap_t145210370 * get_notations_8() const { return ___notations_8; }
	inline XmlNamedNodeMap_t145210370 ** get_address_of_notations_8() { return &___notations_8; }
	inline void set_notations_8(XmlNamedNodeMap_t145210370 * value)
	{
		___notations_8 = value;
		Il2CppCodeGenWriteBarrier(&___notations_8, value);
	}

	inline static int32_t get_offset_of_schemaInfo_9() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___schemaInfo_9)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_9() const { return ___schemaInfo_9; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_9() { return &___schemaInfo_9; }
	inline void set_schemaInfo_9(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&___schemaInfo_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
