﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.Net.FtpWebRequest
struct FtpWebRequest_t3120721823;
// System.IO.Stream
struct Stream_t3255436806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpDataStream
struct  FtpDataStream_t3588258764  : public Stream_t3255436806
{
public:
	// System.Net.FtpWebRequest System.Net.FtpDataStream::request
	FtpWebRequest_t3120721823 * ___request_8;
	// System.IO.Stream System.Net.FtpDataStream::networkStream
	Stream_t3255436806 * ___networkStream_9;
	// System.Boolean System.Net.FtpDataStream::disposed
	bool ___disposed_10;
	// System.Boolean System.Net.FtpDataStream::isRead
	bool ___isRead_11;
	// System.Int32 System.Net.FtpDataStream::totalRead
	int32_t ___totalRead_12;

public:
	inline static int32_t get_offset_of_request_8() { return static_cast<int32_t>(offsetof(FtpDataStream_t3588258764, ___request_8)); }
	inline FtpWebRequest_t3120721823 * get_request_8() const { return ___request_8; }
	inline FtpWebRequest_t3120721823 ** get_address_of_request_8() { return &___request_8; }
	inline void set_request_8(FtpWebRequest_t3120721823 * value)
	{
		___request_8 = value;
		Il2CppCodeGenWriteBarrier(&___request_8, value);
	}

	inline static int32_t get_offset_of_networkStream_9() { return static_cast<int32_t>(offsetof(FtpDataStream_t3588258764, ___networkStream_9)); }
	inline Stream_t3255436806 * get_networkStream_9() const { return ___networkStream_9; }
	inline Stream_t3255436806 ** get_address_of_networkStream_9() { return &___networkStream_9; }
	inline void set_networkStream_9(Stream_t3255436806 * value)
	{
		___networkStream_9 = value;
		Il2CppCodeGenWriteBarrier(&___networkStream_9, value);
	}

	inline static int32_t get_offset_of_disposed_10() { return static_cast<int32_t>(offsetof(FtpDataStream_t3588258764, ___disposed_10)); }
	inline bool get_disposed_10() const { return ___disposed_10; }
	inline bool* get_address_of_disposed_10() { return &___disposed_10; }
	inline void set_disposed_10(bool value)
	{
		___disposed_10 = value;
	}

	inline static int32_t get_offset_of_isRead_11() { return static_cast<int32_t>(offsetof(FtpDataStream_t3588258764, ___isRead_11)); }
	inline bool get_isRead_11() const { return ___isRead_11; }
	inline bool* get_address_of_isRead_11() { return &___isRead_11; }
	inline void set_isRead_11(bool value)
	{
		___isRead_11 = value;
	}

	inline static int32_t get_offset_of_totalRead_12() { return static_cast<int32_t>(offsetof(FtpDataStream_t3588258764, ___totalRead_12)); }
	inline int32_t get_totalRead_12() const { return ___totalRead_12; }
	inline int32_t* get_address_of_totalRead_12() { return &___totalRead_12; }
	inline void set_totalRead_12(int32_t value)
	{
		___totalRead_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
