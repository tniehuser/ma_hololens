﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.DateTimeParse/MatchNumberDelegate
struct MatchNumberDelegate_t2323488373;
// System.DateTimeParse/DS[][]
struct DSU5BU5DU5BU5D_t3400242417;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeParse
struct  DateTimeParse_t2729286702  : public Il2CppObject
{
public:

public:
};

struct DateTimeParse_t2729286702_StaticFields
{
public:
	// System.DateTimeParse/MatchNumberDelegate System.DateTimeParse::m_hebrewNumberParser
	MatchNumberDelegate_t2323488373 * ___m_hebrewNumberParser_0;
	// System.DateTimeParse/DS[][] System.DateTimeParse::dateParsingStates
	DSU5BU5DU5BU5D_t3400242417* ___dateParsingStates_1;

public:
	inline static int32_t get_offset_of_m_hebrewNumberParser_0() { return static_cast<int32_t>(offsetof(DateTimeParse_t2729286702_StaticFields, ___m_hebrewNumberParser_0)); }
	inline MatchNumberDelegate_t2323488373 * get_m_hebrewNumberParser_0() const { return ___m_hebrewNumberParser_0; }
	inline MatchNumberDelegate_t2323488373 ** get_address_of_m_hebrewNumberParser_0() { return &___m_hebrewNumberParser_0; }
	inline void set_m_hebrewNumberParser_0(MatchNumberDelegate_t2323488373 * value)
	{
		___m_hebrewNumberParser_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_hebrewNumberParser_0, value);
	}

	inline static int32_t get_offset_of_dateParsingStates_1() { return static_cast<int32_t>(offsetof(DateTimeParse_t2729286702_StaticFields, ___dateParsingStates_1)); }
	inline DSU5BU5DU5BU5D_t3400242417* get_dateParsingStates_1() const { return ___dateParsingStates_1; }
	inline DSU5BU5DU5BU5D_t3400242417** get_address_of_dateParsingStates_1() { return &___dateParsingStates_1; }
	inline void set_dateParsingStates_1(DSU5BU5DU5BU5D_t3400242417* value)
	{
		___dateParsingStates_1 = value;
		Il2CppCodeGenWriteBarrier(&___dateParsingStates_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
