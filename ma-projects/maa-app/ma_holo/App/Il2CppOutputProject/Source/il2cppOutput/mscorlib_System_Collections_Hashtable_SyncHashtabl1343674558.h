﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable/SyncHashtable
struct  SyncHashtable_t1343674558  : public Hashtable_t909839986
{
public:
	// System.Collections.Hashtable System.Collections.Hashtable/SyncHashtable::_table
	Hashtable_t909839986 * ____table_12;

public:
	inline static int32_t get_offset_of__table_12() { return static_cast<int32_t>(offsetof(SyncHashtable_t1343674558, ____table_12)); }
	inline Hashtable_t909839986 * get__table_12() const { return ____table_12; }
	inline Hashtable_t909839986 ** get_address_of__table_12() { return &____table_12; }
	inline void set__table_12(Hashtable_t909839986 * value)
	{
		____table_12 = value;
		Il2CppCodeGenWriteBarrier(&____table_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
