﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SerStack
struct  SerStack_t3886188184  : public Il2CppObject
{
public:
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.SerStack::objects
	ObjectU5BU5D_t3614634134* ___objects_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.SerStack::stackId
	String_t* ___stackId_1;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerStack::top
	int32_t ___top_2;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(SerStack_t3886188184, ___objects_0)); }
	inline ObjectU5BU5D_t3614634134* get_objects_0() const { return ___objects_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(ObjectU5BU5D_t3614634134* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier(&___objects_0, value);
	}

	inline static int32_t get_offset_of_stackId_1() { return static_cast<int32_t>(offsetof(SerStack_t3886188184, ___stackId_1)); }
	inline String_t* get_stackId_1() const { return ___stackId_1; }
	inline String_t** get_address_of_stackId_1() { return &___stackId_1; }
	inline void set_stackId_1(String_t* value)
	{
		___stackId_1 = value;
		Il2CppCodeGenWriteBarrier(&___stackId_1, value);
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(SerStack_t3886188184, ___top_2)); }
	inline int32_t get_top_2() const { return ___top_2; }
	inline int32_t* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(int32_t value)
	{
		___top_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
