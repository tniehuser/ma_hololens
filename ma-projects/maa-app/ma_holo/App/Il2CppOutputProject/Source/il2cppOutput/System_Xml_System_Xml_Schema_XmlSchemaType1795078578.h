﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2874429441.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_t1795078578  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaType::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_10;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::derivedBy
	int32_t ___derivedBy_11;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::baseSchemaType
	XmlSchemaType_t1795078578 * ___baseSchemaType_12;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::datatype
	XmlSchemaDatatype_t1195946242 * ___datatype_13;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::finalResolved
	int32_t ___finalResolved_14;
	// System.Xml.Schema.SchemaElementDecl modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.XmlSchemaType::elementDecl
	SchemaElementDecl_t1940851905 * ___elementDecl_15;
	// System.Xml.XmlQualifiedName modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.XmlSchemaType::qname
	XmlQualifiedName_t1944712516 * ___qname_16;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::redefined
	XmlSchemaType_t1795078578 * ___redefined_17;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaType::contentType
	int32_t ___contentType_18;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_final_10() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___final_10)); }
	inline int32_t get_final_10() const { return ___final_10; }
	inline int32_t* get_address_of_final_10() { return &___final_10; }
	inline void set_final_10(int32_t value)
	{
		___final_10 = value;
	}

	inline static int32_t get_offset_of_derivedBy_11() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___derivedBy_11)); }
	inline int32_t get_derivedBy_11() const { return ___derivedBy_11; }
	inline int32_t* get_address_of_derivedBy_11() { return &___derivedBy_11; }
	inline void set_derivedBy_11(int32_t value)
	{
		___derivedBy_11 = value;
	}

	inline static int32_t get_offset_of_baseSchemaType_12() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___baseSchemaType_12)); }
	inline XmlSchemaType_t1795078578 * get_baseSchemaType_12() const { return ___baseSchemaType_12; }
	inline XmlSchemaType_t1795078578 ** get_address_of_baseSchemaType_12() { return &___baseSchemaType_12; }
	inline void set_baseSchemaType_12(XmlSchemaType_t1795078578 * value)
	{
		___baseSchemaType_12 = value;
		Il2CppCodeGenWriteBarrier(&___baseSchemaType_12, value);
	}

	inline static int32_t get_offset_of_datatype_13() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___datatype_13)); }
	inline XmlSchemaDatatype_t1195946242 * get_datatype_13() const { return ___datatype_13; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_datatype_13() { return &___datatype_13; }
	inline void set_datatype_13(XmlSchemaDatatype_t1195946242 * value)
	{
		___datatype_13 = value;
		Il2CppCodeGenWriteBarrier(&___datatype_13, value);
	}

	inline static int32_t get_offset_of_finalResolved_14() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___finalResolved_14)); }
	inline int32_t get_finalResolved_14() const { return ___finalResolved_14; }
	inline int32_t* get_address_of_finalResolved_14() { return &___finalResolved_14; }
	inline void set_finalResolved_14(int32_t value)
	{
		___finalResolved_14 = value;
	}

	inline static int32_t get_offset_of_elementDecl_15() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___elementDecl_15)); }
	inline SchemaElementDecl_t1940851905 * get_elementDecl_15() const { return ___elementDecl_15; }
	inline SchemaElementDecl_t1940851905 ** get_address_of_elementDecl_15() { return &___elementDecl_15; }
	inline void set_elementDecl_15(SchemaElementDecl_t1940851905 * value)
	{
		___elementDecl_15 = value;
		Il2CppCodeGenWriteBarrier(&___elementDecl_15, value);
	}

	inline static int32_t get_offset_of_qname_16() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___qname_16)); }
	inline XmlQualifiedName_t1944712516 * get_qname_16() const { return ___qname_16; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_16() { return &___qname_16; }
	inline void set_qname_16(XmlQualifiedName_t1944712516 * value)
	{
		___qname_16 = value;
		Il2CppCodeGenWriteBarrier(&___qname_16, value);
	}

	inline static int32_t get_offset_of_redefined_17() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___redefined_17)); }
	inline XmlSchemaType_t1795078578 * get_redefined_17() const { return ___redefined_17; }
	inline XmlSchemaType_t1795078578 ** get_address_of_redefined_17() { return &___redefined_17; }
	inline void set_redefined_17(XmlSchemaType_t1795078578 * value)
	{
		___redefined_17 = value;
		Il2CppCodeGenWriteBarrier(&___redefined_17, value);
	}

	inline static int32_t get_offset_of_contentType_18() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___contentType_18)); }
	inline int32_t get_contentType_18() const { return ___contentType_18; }
	inline int32_t* get_address_of_contentType_18() { return &___contentType_18; }
	inline void set_contentType_18(int32_t value)
	{
		___contentType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
