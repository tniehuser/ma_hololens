﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1287616130  : public XmlNode_t616554813
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_t1287616130 * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1287616130, ___next_1)); }
	inline XmlLinkedNode_t1287616130 * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_t1287616130 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier(&___next_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
