﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.SmallXmlParserException
struct  SmallXmlParserException_t2094031034  : public SystemException_t3877406272
{
public:
	// System.Int32 Mono.Xml.SmallXmlParserException::line
	int32_t ___line_16;
	// System.Int32 Mono.Xml.SmallXmlParserException::column
	int32_t ___column_17;

public:
	inline static int32_t get_offset_of_line_16() { return static_cast<int32_t>(offsetof(SmallXmlParserException_t2094031034, ___line_16)); }
	inline int32_t get_line_16() const { return ___line_16; }
	inline int32_t* get_address_of_line_16() { return &___line_16; }
	inline void set_line_16(int32_t value)
	{
		___line_16 = value;
	}

	inline static int32_t get_offset_of_column_17() { return static_cast<int32_t>(offsetof(SmallXmlParserException_t2094031034, ___column_17)); }
	inline int32_t get_column_17() const { return ___column_17; }
	inline int32_t* get_address_of_column_17() { return &___column_17; }
	inline void set_column_17(int32_t value)
	{
		___column_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
