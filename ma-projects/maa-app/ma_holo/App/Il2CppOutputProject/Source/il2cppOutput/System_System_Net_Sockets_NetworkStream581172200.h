﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Net.Sockets.Socket
struct Socket_t3821512045;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.NetworkStream
struct  NetworkStream_t581172200  : public Stream_t3255436806
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.NetworkStream::m_StreamSocket
	Socket_t3821512045 * ___m_StreamSocket_8;
	// System.Boolean System.Net.Sockets.NetworkStream::m_Readable
	bool ___m_Readable_9;
	// System.Boolean System.Net.Sockets.NetworkStream::m_Writeable
	bool ___m_Writeable_10;
	// System.Boolean System.Net.Sockets.NetworkStream::m_OwnsSocket
	bool ___m_OwnsSocket_11;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CloseTimeout
	int32_t ___m_CloseTimeout_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.NetworkStream::m_CleanedUp
	bool ___m_CleanedUp_13;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CurrentReadTimeout
	int32_t ___m_CurrentReadTimeout_14;
	// System.Int32 System.Net.Sockets.NetworkStream::m_CurrentWriteTimeout
	int32_t ___m_CurrentWriteTimeout_15;

public:
	inline static int32_t get_offset_of_m_StreamSocket_8() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_StreamSocket_8)); }
	inline Socket_t3821512045 * get_m_StreamSocket_8() const { return ___m_StreamSocket_8; }
	inline Socket_t3821512045 ** get_address_of_m_StreamSocket_8() { return &___m_StreamSocket_8; }
	inline void set_m_StreamSocket_8(Socket_t3821512045 * value)
	{
		___m_StreamSocket_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_StreamSocket_8, value);
	}

	inline static int32_t get_offset_of_m_Readable_9() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_Readable_9)); }
	inline bool get_m_Readable_9() const { return ___m_Readable_9; }
	inline bool* get_address_of_m_Readable_9() { return &___m_Readable_9; }
	inline void set_m_Readable_9(bool value)
	{
		___m_Readable_9 = value;
	}

	inline static int32_t get_offset_of_m_Writeable_10() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_Writeable_10)); }
	inline bool get_m_Writeable_10() const { return ___m_Writeable_10; }
	inline bool* get_address_of_m_Writeable_10() { return &___m_Writeable_10; }
	inline void set_m_Writeable_10(bool value)
	{
		___m_Writeable_10 = value;
	}

	inline static int32_t get_offset_of_m_OwnsSocket_11() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_OwnsSocket_11)); }
	inline bool get_m_OwnsSocket_11() const { return ___m_OwnsSocket_11; }
	inline bool* get_address_of_m_OwnsSocket_11() { return &___m_OwnsSocket_11; }
	inline void set_m_OwnsSocket_11(bool value)
	{
		___m_OwnsSocket_11 = value;
	}

	inline static int32_t get_offset_of_m_CloseTimeout_12() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_CloseTimeout_12)); }
	inline int32_t get_m_CloseTimeout_12() const { return ___m_CloseTimeout_12; }
	inline int32_t* get_address_of_m_CloseTimeout_12() { return &___m_CloseTimeout_12; }
	inline void set_m_CloseTimeout_12(int32_t value)
	{
		___m_CloseTimeout_12 = value;
	}

	inline static int32_t get_offset_of_m_CleanedUp_13() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_CleanedUp_13)); }
	inline bool get_m_CleanedUp_13() const { return ___m_CleanedUp_13; }
	inline bool* get_address_of_m_CleanedUp_13() { return &___m_CleanedUp_13; }
	inline void set_m_CleanedUp_13(bool value)
	{
		___m_CleanedUp_13 = value;
	}

	inline static int32_t get_offset_of_m_CurrentReadTimeout_14() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_CurrentReadTimeout_14)); }
	inline int32_t get_m_CurrentReadTimeout_14() const { return ___m_CurrentReadTimeout_14; }
	inline int32_t* get_address_of_m_CurrentReadTimeout_14() { return &___m_CurrentReadTimeout_14; }
	inline void set_m_CurrentReadTimeout_14(int32_t value)
	{
		___m_CurrentReadTimeout_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentWriteTimeout_15() { return static_cast<int32_t>(offsetof(NetworkStream_t581172200, ___m_CurrentWriteTimeout_15)); }
	inline int32_t get_m_CurrentWriteTimeout_15() const { return ___m_CurrentWriteTimeout_15; }
	inline int32_t* get_address_of_m_CurrentWriteTimeout_15() { return &___m_CurrentWriteTimeout_15; }
	inline void set_m_CurrentWriteTimeout_15(int32_t value)
	{
		___m_CurrentWriteTimeout_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
