﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_Net_Dns_DnsResourceRecord2943454412.h"

// System.Net.IPAddress
struct IPAddress_t1399971723;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordIPAddress
struct  DnsResourceRecordIPAddress_t2549622903  : public DnsResourceRecord_t2943454412
{
public:
	// System.Net.IPAddress Mono.Net.Dns.DnsResourceRecordIPAddress::address
	IPAddress_t1399971723 * ___address_6;

public:
	inline static int32_t get_offset_of_address_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordIPAddress_t2549622903, ___address_6)); }
	inline IPAddress_t1399971723 * get_address_6() const { return ___address_6; }
	inline IPAddress_t1399971723 ** get_address_of_address_6() { return &___address_6; }
	inline void set_address_6(IPAddress_t1399971723 * value)
	{
		___address_6 = value;
		Il2CppCodeGenWriteBarrier(&___address_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
