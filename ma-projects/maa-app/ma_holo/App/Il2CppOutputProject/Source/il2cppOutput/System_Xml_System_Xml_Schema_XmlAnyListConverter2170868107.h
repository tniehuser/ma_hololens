﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlListConverter888285589.h"

// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAnyListConverter
struct  XmlAnyListConverter_t2170868107  : public XmlListConverter_t888285589
{
public:

public:
};

struct XmlAnyListConverter_t2170868107_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyListConverter::ItemList
	XmlValueConverter_t68179724 * ___ItemList_33;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyListConverter::AnyAtomicList
	XmlValueConverter_t68179724 * ___AnyAtomicList_34;

public:
	inline static int32_t get_offset_of_ItemList_33() { return static_cast<int32_t>(offsetof(XmlAnyListConverter_t2170868107_StaticFields, ___ItemList_33)); }
	inline XmlValueConverter_t68179724 * get_ItemList_33() const { return ___ItemList_33; }
	inline XmlValueConverter_t68179724 ** get_address_of_ItemList_33() { return &___ItemList_33; }
	inline void set_ItemList_33(XmlValueConverter_t68179724 * value)
	{
		___ItemList_33 = value;
		Il2CppCodeGenWriteBarrier(&___ItemList_33, value);
	}

	inline static int32_t get_offset_of_AnyAtomicList_34() { return static_cast<int32_t>(offsetof(XmlAnyListConverter_t2170868107_StaticFields, ___AnyAtomicList_34)); }
	inline XmlValueConverter_t68179724 * get_AnyAtomicList_34() const { return ___AnyAtomicList_34; }
	inline XmlValueConverter_t68179724 ** get_address_of_AnyAtomicList_34() { return &___AnyAtomicList_34; }
	inline void set_AnyAtomicList_34(XmlValueConverter_t68179724 * value)
	{
		___AnyAtomicList_34 = value;
		Il2CppCodeGenWriteBarrier(&___AnyAtomicList_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
