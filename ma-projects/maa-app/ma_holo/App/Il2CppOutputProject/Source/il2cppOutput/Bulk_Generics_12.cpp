﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_gen4015628905.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_Threading_Tasks_TaskCreationOptions547302442.h"
#include "mscorlib_System_Threading_Tasks_TaskContinuationOp2826278702.h"
#include "mscorlib_System_Threading_Tasks_TaskScheduler3932792796.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Func_2_gen2979935071.h"
#include "mscorlib_System_Action_1_gen1801450390.h"
#include "mscorlib_System_Threading_Tasks_Task_1_gen2445339805.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Threading_Tasks_CausalityTraceLevel536076440.h"
#include "mscorlib_System_Threading_Tasks_AsyncCausalityStat2986644407.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_OperationCanceledException2897400967.h"
#include "mscorlib_System_Threading_Tasks_VoidTaskResult3325310798.h"
#include "mscorlib_System_Threading_ThreadAbortException1150575753.h"
#include "mscorlib_System_Threading_Tasks_Task1843236107.h"
#include "mscorlib_System_Threading_Tasks_Task_ContingentProp606988207.h"
#include "mscorlib_System_Threading_Tasks_TaskExceptionHolde2208677448.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Func_3_gen1604491142.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CFr268684924.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_1_U3CF1592407391.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Threading_AtomicBoolean379413895.h"
#include "mscorlib_System_Threading_ThreadPoolWorkQueue_Spars935185730.h"
#include "mscorlib_System_Tuple_2_gen1801232814.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1263084566.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Tuple_2_gen1036200771.h"
#include "mscorlib_System_Tuple_3_gen1136823221.h"
#include "mscorlib_System_Tuple_4_gen3284226035.h"
#include "mscorlib_System_Tuple_4_gen1664179553.h"
#include "mscorlib_System_Tuple_4_gen2865985893.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2019901575.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen266204305.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1482999186.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen883776152.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen270836789.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3799696166.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen2191335654.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2955480072.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen1666603240.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1372135904.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen3149477088.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2935245934.h"

// System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>
struct TaskFactory_1_t4015628905;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t3932792796;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Func`2<System.IAsyncResult,System.Threading.Tasks.VoidTaskResult>
struct Func_2_t2979935071;
// System.Action`1<System.IAsyncResult>
struct Action_1_t1801450390;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t2445339805;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.OperationCanceledException
struct OperationCanceledException_t2897400967;
// System.Threading.Tasks.TaskExceptionHolder
struct TaskExceptionHolder_t2208677448;
// System.Threading.Tasks.Task
struct Task_t1843236107;
// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t1604491142;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3022476291;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Threading.AtomicBoolean
struct AtomicBoolean_t379413895;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>
struct SparseArray_1_t935185730;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Array
struct Il2CppArray;
// System.Tuple`2<System.Object,System.Char>
struct Tuple_2_t1801232814;
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t1263084566;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1579458414;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Type
struct Type_t;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Tuple`2<System.Object,System.Object>
struct Tuple_2_t1036200771;
// System.Tuple`3<System.Object,System.Object,System.Object>
struct Tuple_3_t1136823221;
// System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>
struct Tuple_4_t3284226035;
// System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>
struct Tuple_4_t1664179553;
// System.Tuple`4<System.Object,System.Object,System.Object,System.Object>
struct Tuple_4_t2865985893;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2619124609;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t865427339;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t1482999186;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t870059823;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2019901575;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t266204305;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3438463199;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t883776152;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t270836789;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3799696166;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t2191335654;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2955480072;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3051495417;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t1903595547;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t606618774;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t1666603240;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t828812576;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t1372135904;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t3149477088;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t2935245934;
extern Il2CppClass* CancellationToken_t1851405782_il2cpp_TypeInfo_var;
extern const uint32_t TaskFactory_1__ctor_m4181434098_MetadataUsageId;
extern Il2CppClass* VoidTaskResult_t3325310798_il2cpp_TypeInfo_var;
extern Il2CppClass* OperationCanceledException_t2897400967_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadAbortException_t1150575753_il2cpp_TypeInfo_var;
extern Il2CppClass* Task_t1843236107_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2407548955_MethodInfo_var;
extern const uint32_t TaskFactory_1_FromAsyncCoreLogic_m3004652579_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var;
extern Il2CppClass* AtomicBoolean_t379413895_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern Il2CppClass* IAsyncResult_t1999651008_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_Invoke_m2184768394_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral784393026;
extern Il2CppCodeGenString* _stringLiteral3599838238;
extern Il2CppCodeGenString* _stringLiteral2115721401;
extern const uint32_t TaskFactory_1_FromAsyncImpl_m1352775473_MetadataUsageId;
extern Il2CppClass* IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var;
extern const MethodInfo* EqualityComparer_1_get_Default_m1577971315_MethodInfo_var;
extern const uint32_t Tuple_2_Equals_m732327059_MetadataUsageId;
extern Il2CppClass* IEqualityComparer_t2716208158_il2cpp_TypeInfo_var;
extern const uint32_t Tuple_2_System_Collections_IStructuralEquatable_Equals_m3661955903_MetadataUsageId;
extern Il2CppClass* IStructuralComparable_t881765570_il2cpp_TypeInfo_var;
extern const MethodInfo* Comparer_1_get_Default_m40106963_MethodInfo_var;
extern const uint32_t Tuple_2_System_IComparable_CompareTo_m141023310_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2734479056;
extern Il2CppCodeGenString* _stringLiteral3712335156;
extern const uint32_t Tuple_2_System_Collections_IStructuralComparable_CompareTo_m3600080012_MetadataUsageId;
extern const uint32_t Tuple_2_GetHashCode_m3704074721_MetadataUsageId;
extern const uint32_t Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m1235293593_MetadataUsageId;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* ITuple_t843925179_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern const uint32_t Tuple_2_ToString_m2248070625_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t Tuple_2_System_ITuple_ToString_m3268469601_MetadataUsageId;
extern const uint32_t Tuple_2_Equals_m248290730_MetadataUsageId;
extern const uint32_t Tuple_2_System_Collections_IStructuralEquatable_Equals_m984969174_MetadataUsageId;
extern const uint32_t Tuple_2_System_IComparable_CompareTo_m3606644887_MetadataUsageId;
extern const uint32_t Tuple_2_System_Collections_IStructuralComparable_CompareTo_m122580837_MetadataUsageId;
extern const uint32_t Tuple_2_GetHashCode_m3472475602_MetadataUsageId;
extern const uint32_t Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m2915229304_MetadataUsageId;
extern const uint32_t Tuple_2_ToString_m4162645436_MetadataUsageId;
extern const uint32_t Tuple_2_System_ITuple_ToString_m3694123798_MetadataUsageId;
extern const uint32_t Tuple_3_Equals_m4286593197_MetadataUsageId;
extern const uint32_t Tuple_3_System_Collections_IStructuralEquatable_Equals_m2717096461_MetadataUsageId;
extern const uint32_t Tuple_3_System_IComparable_CompareTo_m4004863408_MetadataUsageId;
extern const uint32_t Tuple_3_System_Collections_IStructuralComparable_CompareTo_m3210780030_MetadataUsageId;
extern const uint32_t Tuple_3_GetHashCode_m132921887_MetadataUsageId;
extern const uint32_t Tuple_3_System_Collections_IStructuralEquatable_GetHashCode_m2445616235_MetadataUsageId;
extern const uint32_t Tuple_3_ToString_m1145130135_MetadataUsageId;
extern const uint32_t Tuple_3_System_ITuple_ToString_m4112284619_MetadataUsageId;
extern const uint32_t Tuple_4_Equals_m4168359424_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_Equals_m3064214516_MetadataUsageId;
extern const uint32_t Tuple_4_System_IComparable_CompareTo_m4053149811_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m1720090693_MetadataUsageId;
extern const uint32_t Tuple_4_GetHashCode_m109059448_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m686082162_MetadataUsageId;
extern const uint32_t Tuple_4_ToString_m1586339854_MetadataUsageId;
extern const uint32_t Tuple_4_System_ITuple_ToString_m4156720916_MetadataUsageId;
extern const uint32_t Tuple_4_Equals_m293337750_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_Equals_m745313518_MetadataUsageId;
extern const uint32_t Tuple_4_System_IComparable_CompareTo_m2767899169_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3256184635_MetadataUsageId;
extern const uint32_t Tuple_4_GetHashCode_m1825267794_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m4281699352_MetadataUsageId;
extern const uint32_t Tuple_4_ToString_m1553484336_MetadataUsageId;
extern const uint32_t Tuple_4_System_ITuple_ToString_m2916912930_MetadataUsageId;
extern const uint32_t Tuple_4_Equals_m2553141920_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_Equals_m1873162208_MetadataUsageId;
extern const uint32_t Tuple_4_System_IComparable_CompareTo_m3304034037_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3378305607_MetadataUsageId;
extern const uint32_t Tuple_4_GetHashCode_m308938224_MetadataUsageId;
extern const uint32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m2517367258_MetadataUsageId;
extern const uint32_t Tuple_4_ToString_m715281218_MetadataUsageId;
extern const uint32_t Tuple_4_System_ITuple_ToString_m507032720_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m127496184_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m79259589_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m874046876_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m769918017_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m231935020_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m428957899_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m54675381_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m1715547918_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m4078762228_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m4090512311_MetadataUsageId;
extern const uint32_t InvokableCall_2__ctor_m974169948_MetadataUsageId;
extern const uint32_t InvokableCall_2_Invoke_m1071013389_MetadataUsageId;
extern const uint32_t InvokableCall_3__ctor_m3141607487_MetadataUsageId;
extern const uint32_t InvokableCall_3_Invoke_m74557124_MetadataUsageId;
extern const uint32_t InvokableCall_4__ctor_m1096399974_MetadataUsageId;
extern const uint32_t InvokableCall_4_Invoke_m1555001411_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m530778538_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId;
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId;
extern Il2CppClass* LoadSceneMode_t2981886439_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId;
extern const uint32_t UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m2948712401_MetadataUsageId;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m2073978020_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId;
extern const uint32_t UnityEvent_2__ctor_m3717034779_MetadataUsageId;
extern const uint32_t UnityEvent_2_FindMethod_Impl_m2783251718_MetadataUsageId;
extern const uint32_t UnityEvent_3__ctor_m3502631330_MetadataUsageId;
extern const uint32_t UnityEvent_3_FindMethod_Impl_m1889846153_MetadataUsageId;
extern const uint32_t UnityEvent_4__ctor_m3102731553_MetadataUsageId;
extern const uint32_t UnityEvent_4_FindMethod_Impl_m4079512420_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1606206610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3022476291 * m_Items[1];

public:
	inline Delegate_t3022476291 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t3022476291 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t1664964607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * p0, const MethodInfo* method);
// TResult System.Func`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1656275607_gshared (Func_3_t3369346583 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
extern "C"  EqualityComparer_1_t1263084566 * EqualityComparer_1_get_Default_m1577971315_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C"  Comparer_1_t1579458414 * Comparer_1_get_Default_m40106963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckMultiTaskContinuationOptions(System.Threading.Tasks.TaskContinuationOptions)
extern "C"  void TaskFactory_CheckMultiTaskContinuationOptions_m717392640 (Il2CppObject * __this /* static, unused */, int32_t ___continuationOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckCreationOptions(System.Threading.Tasks.TaskCreationOptions)
extern "C"  void TaskFactory_CheckCreationOptions_m1907973182 (Il2CppObject * __this /* static, unused */, int32_t ___creationOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.IAsyncResult>::Invoke(T)
#define Action_1_Invoke_m2407548955(__this, p0, method) ((  void (*) (Action_1_t1801450390 *, Il2CppObject *, const MethodInfo*))Action_1_Invoke_m1684652980_gshared)(__this, p0, method)
// System.Threading.CancellationToken System.OperationCanceledException::get_CancellationToken()
extern "C"  CancellationToken_t1851405782  OperationCanceledException_get_CancellationToken_m821612483 (OperationCanceledException_t2897400967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskExceptionHolder::MarkAsHandled(System.Boolean)
extern "C"  void TaskExceptionHolder_MarkAsHandled_m698250467 (TaskExceptionHolder_t2208677448 * __this, bool ___calledFromFinalizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.AsyncCausalityTracer::get_LoggingOn()
extern "C"  bool AsyncCausalityTracer_get_LoggingOn_m929857466 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Tasks.Task::get_Id()
extern "C"  int32_t Task_get_Id_m4106115082 (Task_t1843236107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.AsyncCausalityTracer::TraceOperationCompletion(System.Threading.Tasks.CausalityTraceLevel,System.Int32,System.Threading.Tasks.AsyncCausalityStatus)
extern "C" IL2CPP_NO_INLINE void AsyncCausalityTracer_TraceOperationCompletion_m1161622555 (Il2CppObject * __this /* static, unused */, int32_t ___traceLevel0, int32_t ___taskId1, int32_t ___status2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.Task::RemoveFromActiveTasks(System.Int32)
extern "C"  void Task_RemoveFromActiveTasks_m437324001 (Il2CppObject * __this /* static, unused */, int32_t ___taskId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskFactory::CheckFromAsyncOptions(System.Threading.Tasks.TaskCreationOptions,System.Boolean)
extern "C"  void TaskFactory_CheckFromAsyncOptions_m238110038 (Il2CppObject * __this /* static, unused */, int32_t ___creationOptions0, bool ___hasBeginMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Delegate::get_Method()
extern "C"  MethodInfo_t * Delegate_get_Method_m2968370506 (Delegate_t3022476291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* ___str00, String_t* ___str11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.AsyncCausalityTracer::TraceOperationCreation(System.Threading.Tasks.CausalityTraceLevel,System.Int32,System.String,System.UInt64)
extern "C" IL2CPP_NO_INLINE void AsyncCausalityTracer_TraceOperationCreation_m3376453187 (Il2CppObject * __this /* static, unused */, int32_t ___traceLevel0, int32_t ___taskId1, String_t* ___operationName2, uint64_t ___relatedContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Tasks.Task::AddToActiveTasks(System.Threading.Tasks.Task)
extern "C"  bool Task_AddToActiveTasks_m2680726720 (Il2CppObject * __this /* static, unused */, Task_t1843236107 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Versioning.BinaryCompatibility::get_TargetsAtLeast_Desktop_V4_5()
extern "C"  bool BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AtomicBoolean::.ctor()
extern "C"  void AtomicBoolean__ctor_m845874490 (AtomicBoolean_t379413895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AsyncCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCallback__ctor_m3071689932 (AsyncCallback_t163412349 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TResult System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>::Invoke(T1,T2)
#define Func_3_Invoke_m2184768394(__this, p0, p1, method) ((  Il2CppObject * (*) (Func_3_t1604491142 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_3_Invoke_m1656275607_gshared)(__this, p0, p1, method)
// System.Boolean System.Threading.AtomicBoolean::TryRelaxedSet()
extern "C"  bool AtomicBoolean_TryRelaxedSet_m1519556294 (AtomicBoolean_t379413895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C"  void Monitor_Enter_m2026616996 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, bool* ___lockTaken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
extern "C"  void Array_Copy_m2363740072 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___sourceArray0, Il2CppArray * ___destinationArray1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
#define EqualityComparer_1_get_Default_m1577971315(__this /* static, unused */, method) ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1577971315_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
#define Comparer_1_get_Default_m40106963(__this /* static, unused */, method) ((  Comparer_1_t1579458414 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m40106963_gshared)(__this /* static, unused */, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetResourceString(System.String,System.Object[])
extern "C"  String_t* Environment_GetResourceString_m3280810056 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ObjectU5BU5D_t3614634134* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m544251339 (ArgumentException_t3259014390 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Tuple::CombineHashCodes(System.Int32,System.Int32)
extern "C"  int32_t Tuple_CombineHashCodes_m3512177706 (Il2CppObject * __this /* static, unused */, int32_t ___h10, int32_t ___h21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3946851802 (StringBuilder_t1221177846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t1221177846 * StringBuilder_Append_m3636508479 (StringBuilder_t1221177846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
extern "C"  StringBuilder_t1221177846 * StringBuilder_Append_m3541816491 (StringBuilder_t1221177846 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Tuple::CombineHashCodes(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Tuple_CombineHashCodes_m514564413 (Il2CppObject * __this /* static, unused */, int32_t ___h10, int32_t ___h21, int32_t ___h32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Tuple::CombineHashCodes(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Tuple_CombineHashCodes_m1865303580 (Il2CppObject * __this /* static, unused */, int32_t ___h10, int32_t ___h21, int32_t ___h32, int32_t ___h43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2877580597 (BaseInvokableCall_t2229564840 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t3022476291 * NetFxCoreExtensions_CreateDelegate_m2492743074 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m88556325 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * ___delegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m4062111756 (UnityEventBase_t828812576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1834951552 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t1664964607* ___argumentTypes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m2706435282 (UnityEventBase_t828812576 * __this, ObjectU5BU5D_t3614634134* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>::.ctor()
extern "C"  void TaskFactory_1__ctor_m4181434098_gshared (TaskFactory_1_t4015628905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1__ctor_m4181434098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		NullCheck((TaskFactory_1_t4015628905 *)__this);
		((  void (*) (TaskFactory_1_t4015628905 *, CancellationToken_t1851405782 , int32_t, int32_t, TaskScheduler_t3932792796 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TaskFactory_1_t4015628905 *)__this, (CancellationToken_t1851405782 )L_0, (int32_t)0, (int32_t)0, (TaskScheduler_t3932792796 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>::.ctor(System.Threading.CancellationToken,System.Threading.Tasks.TaskCreationOptions,System.Threading.Tasks.TaskContinuationOptions,System.Threading.Tasks.TaskScheduler)
extern "C"  void TaskFactory_1__ctor_m948575816_gshared (TaskFactory_1_t4015628905 * __this, CancellationToken_t1851405782  ___cancellationToken0, int32_t ___creationOptions1, int32_t ___continuationOptions2, TaskScheduler_t3932792796 * ___scheduler3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___continuationOptions2;
		TaskFactory_CheckMultiTaskContinuationOptions_m717392640(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___creationOptions1;
		TaskFactory_CheckCreationOptions_m1907973182(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/NULL);
		CancellationToken_t1851405782  L_2 = ___cancellationToken0;
		__this->set_m_defaultCancellationToken_0(L_2);
		TaskScheduler_t3932792796 * L_3 = ___scheduler3;
		__this->set_m_defaultScheduler_1(L_3);
		int32_t L_4 = ___creationOptions1;
		__this->set_m_defaultCreationOptions_2(L_4);
		int32_t L_5 = ___continuationOptions2;
		__this->set_m_defaultContinuationOptions_3(L_5);
		return;
	}
}
// System.Void System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>::FromAsyncCoreLogic(System.IAsyncResult,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Threading.Tasks.Task`1<TResult>,System.Boolean)
extern "C"  void TaskFactory_1_FromAsyncCoreLogic_m3004652579_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___iar0, Func_2_t2979935071 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Task_1_t2445339805 * ___promise3, bool ___requiresSynchronization4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncCoreLogic_m3004652579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	OperationCanceledException_t2897400967 * V_1 = NULL;
	VoidTaskResult_t3325310798  V_2;
	memset(&V_2, 0, sizeof(V_2));
	VoidTaskResult_t3325310798  V_3;
	memset(&V_3, 0, sizeof(V_3));
	OperationCanceledException_t2897400967 * V_4 = NULL;
	Exception_t1927440687 * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		V_1 = (OperationCanceledException_t2897400967 *)NULL;
		Initobj (VoidTaskResult_t3325310798_il2cpp_TypeInfo_var, (&V_3));
		VoidTaskResult_t3325310798  L_0 = V_3;
		V_2 = (VoidTaskResult_t3325310798 )L_0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Func_2_t2979935071 * L_1 = ___endFunction1;
				if (!L_1)
				{
					goto IL_0021;
				}
			}

IL_0014:
			{
				Func_2_t2979935071 * L_2 = ___endFunction1;
				Il2CppObject * L_3 = ___iar0;
				NullCheck((Func_2_t2979935071 *)L_2);
				VoidTaskResult_t3325310798  L_4 = ((  VoidTaskResult_t3325310798  (*) (Func_2_t2979935071 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((Func_2_t2979935071 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				V_2 = (VoidTaskResult_t3325310798 )L_4;
				goto IL_0028;
			}

IL_0021:
			{
				Action_1_t1801450390 * L_5 = ___endAction2;
				Il2CppObject * L_6 = ___iar0;
				NullCheck((Action_1_t1801450390 *)L_5);
				Action_1_Invoke_m2407548955((Action_1_t1801450390 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/Action_1_Invoke_m2407548955_MethodInfo_var);
			}

IL_0028:
			{
				IL2CPP_LEAVE(0xDD, FINALLY_0041);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (OperationCanceledException_t2897400967_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_002d;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
				goto CATCH_0037;
			throw e;
		}

CATCH_002d:
		{ // begin catch(System.OperationCanceledException)
			V_4 = (OperationCanceledException_t2897400967 *)((OperationCanceledException_t2897400967 *)__exception_local);
			OperationCanceledException_t2897400967 * L_7 = V_4;
			V_1 = (OperationCanceledException_t2897400967 *)L_7;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)

CATCH_0037:
		{ // begin catch(System.Exception)
			V_5 = (Exception_t1927440687 *)((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_8 = V_5;
			V_0 = (Exception_t1927440687 *)L_8;
			IL2CPP_LEAVE(0xDD, FINALLY_0041);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			OperationCanceledException_t2897400967 * L_9 = V_1;
			if (!L_9)
			{
				goto IL_005a;
			}
		}

IL_0047:
		{
			Task_1_t2445339805 * L_10 = ___promise3;
			OperationCanceledException_t2897400967 * L_11 = V_1;
			NullCheck((OperationCanceledException_t2897400967 *)L_11);
			CancellationToken_t1851405782  L_12 = OperationCanceledException_get_CancellationToken_m821612483((OperationCanceledException_t2897400967 *)L_11, /*hidden argument*/NULL);
			OperationCanceledException_t2897400967 * L_13 = V_1;
			NullCheck((Task_1_t2445339805 *)L_10);
			((  bool (*) (Task_1_t2445339805 *, CancellationToken_t1851405782 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Task_1_t2445339805 *)L_10, (CancellationToken_t1851405782 )L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			goto IL_00dc;
		}

IL_005a:
		{
			Exception_t1927440687 * L_14 = V_0;
			if (!L_14)
			{
				goto IL_0095;
			}
		}

IL_0060:
		{
			Task_1_t2445339805 * L_15 = ___promise3;
			Exception_t1927440687 * L_16 = V_0;
			NullCheck((Task_1_t2445339805 *)L_15);
			bool L_17 = ((  bool (*) (Task_1_t2445339805 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((Task_1_t2445339805 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_6 = (bool)L_17;
			bool L_18 = V_6;
			if (!L_18)
			{
				goto IL_0090;
			}
		}

IL_0070:
		{
			Exception_t1927440687 * L_19 = V_0;
			if (!((ThreadAbortException_t1150575753 *)IsInst(L_19, ThreadAbortException_t1150575753_il2cpp_TypeInfo_var)))
			{
				goto IL_0090;
			}
		}

IL_007b:
		{
			Task_1_t2445339805 * L_20 = ___promise3;
			NullCheck(L_20);
			ContingentProperties_t606988207 * L_21 = (ContingentProperties_t606988207 *)((Task_t1843236107 *)L_20)->get_m_contingentProperties_15();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_21);
			TaskExceptionHolder_t2208677448 * L_22 = (TaskExceptionHolder_t2208677448 *)L_21->get_m_exceptionsHolder_2();
			il2cpp_codegen_memory_barrier();
			NullCheck((TaskExceptionHolder_t2208677448 *)L_22);
			TaskExceptionHolder_MarkAsHandled_m698250467((TaskExceptionHolder_t2208677448 *)L_22, (bool)0, /*hidden argument*/NULL);
		}

IL_0090:
		{
			goto IL_00dc;
		}

IL_0095:
		{
			bool L_23 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00ac;
			}
		}

IL_009f:
		{
			Task_1_t2445339805 * L_24 = ___promise3;
			NullCheck((Task_t1843236107 *)L_24);
			int32_t L_25 = Task_get_Id_m4106115082((Task_t1843236107 *)L_24, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_25, (int32_t)1, /*hidden argument*/NULL);
		}

IL_00ac:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_26 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_26)
			{
				goto IL_00c1;
			}
		}

IL_00b6:
		{
			Task_1_t2445339805 * L_27 = ___promise3;
			NullCheck((Task_t1843236107 *)L_27);
			int32_t L_28 = Task_get_Id_m4106115082((Task_t1843236107 *)L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_28, /*hidden argument*/NULL);
		}

IL_00c1:
		{
			bool L_29 = ___requiresSynchronization4;
			if (!L_29)
			{
				goto IL_00d5;
			}
		}

IL_00c8:
		{
			Task_1_t2445339805 * L_30 = ___promise3;
			VoidTaskResult_t3325310798  L_31 = V_2;
			NullCheck((Task_1_t2445339805 *)L_30);
			((  bool (*) (Task_1_t2445339805 *, VoidTaskResult_t3325310798 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t2445339805 *)L_30, (VoidTaskResult_t3325310798 )L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			goto IL_00dc;
		}

IL_00d5:
		{
			Task_1_t2445339805 * L_32 = ___promise3;
			VoidTaskResult_t3325310798  L_33 = V_2;
			NullCheck((Task_1_t2445339805 *)L_32);
			((  void (*) (Task_1_t2445339805 *, VoidTaskResult_t3325310798 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((Task_1_t2445339805 *)L_32, (VoidTaskResult_t3325310798 )L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_00dc:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00dd:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>::FromAsync(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Object)
extern "C"  Task_1_t2445339805 * TaskFactory_1_FromAsync_m3609684705_gshared (TaskFactory_1_t4015628905 * __this, Func_3_t1604491142 * ___beginMethod0, Func_2_t2979935071 * ___endMethod1, Il2CppObject * ___state2, const MethodInfo* method)
{
	{
		Func_3_t1604491142 * L_0 = ___beginMethod0;
		Func_2_t2979935071 * L_1 = ___endMethod1;
		Il2CppObject * L_2 = ___state2;
		int32_t L_3 = (int32_t)__this->get_m_defaultCreationOptions_2();
		Task_1_t2445339805 * L_4 = ((  Task_1_t2445339805 * (*) (Il2CppObject * /* static, unused */, Func_3_t1604491142 *, Func_2_t2979935071 *, Action_1_t1801450390 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Func_3_t1604491142 *)L_0, (Func_2_t2979935071 *)L_1, (Action_1_t1801450390 *)NULL, (Il2CppObject *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_4;
	}
}
// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1<System.Threading.Tasks.VoidTaskResult>::FromAsyncImpl(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,TResult>,System.Action`1<System.IAsyncResult>,System.Object,System.Threading.Tasks.TaskCreationOptions)
extern "C"  Task_1_t2445339805 * TaskFactory_1_FromAsyncImpl_m1352775473_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1604491142 * ___beginMethod0, Func_2_t2979935071 * ___endFunction1, Action_1_t1801450390 * ___endAction2, Il2CppObject * ___state3, int32_t ___creationOptions4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskFactory_1_FromAsyncImpl_m1352775473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * V_0 = NULL;
	U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	VoidTaskResult_t3325310798  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 *)L_0;
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_1 = V_0;
		Func_2_t2979935071 * L_2 = ___endFunction1;
		NullCheck(L_1);
		L_1->set_endFunction_0(L_2);
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_3 = V_0;
		Action_1_t1801450390 * L_4 = ___endAction2;
		NullCheck(L_3);
		L_3->set_endAction_1(L_4);
		Func_3_t1604491142 * L_5 = ___beginMethod0;
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		ArgumentNullException_t628810857 * L_6 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_6, (String_t*)_stringLiteral784393026, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0025:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_7 = V_0;
		NullCheck(L_7);
		Func_2_t2979935071 * L_8 = (Func_2_t2979935071 *)L_7->get_endFunction_0();
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_9 = V_0;
		NullCheck(L_9);
		Action_1_t1801450390 * L_10 = (Action_1_t1801450390 *)L_9->get_endAction_1();
		if (L_10)
		{
			goto IL_0046;
		}
	}
	{
		ArgumentNullException_t628810857 * L_11 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_11, (String_t*)_stringLiteral3599838238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0046:
	{
		int32_t L_12 = ___creationOptions4;
		TaskFactory_CheckFromAsyncOptions_m238110038(NULL /*static, unused*/, (int32_t)L_12, (bool)1, /*hidden argument*/NULL);
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_13 = V_0;
		Il2CppObject * L_14 = ___state3;
		int32_t L_15 = ___creationOptions4;
		Task_1_t2445339805 * L_16 = (Task_1_t2445339805 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Task_1_t2445339805 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_16, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_13);
		L_13->set_promise_2(L_16);
		bool L_17 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_18 = V_0;
		NullCheck(L_18);
		Task_1_t2445339805 * L_19 = (Task_1_t2445339805 *)L_18->get_promise_2();
		NullCheck((Task_t1843236107 *)L_19);
		int32_t L_20 = Task_get_Id_m4106115082((Task_t1843236107 *)L_19, /*hidden argument*/NULL);
		Func_3_t1604491142 * L_21 = ___beginMethod0;
		NullCheck((Delegate_t3022476291 *)L_21);
		MethodInfo_t * L_22 = Delegate_get_Method_m2968370506((Delegate_t3022476291 *)L_21, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_22);
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral2115721401, (String_t*)L_23, /*hidden argument*/NULL);
		AsyncCausalityTracer_TraceOperationCreation_m3376453187(NULL /*static, unused*/, (int32_t)0, (int32_t)L_20, (String_t*)L_24, (uint64_t)(((int64_t)((int64_t)0))), /*hidden argument*/NULL);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		bool L_25 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
		if (!L_25)
		{
			goto IL_00a4;
		}
	}
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_26 = V_0;
		NullCheck(L_26);
		Task_1_t2445339805 * L_27 = (Task_1_t2445339805 *)L_26->get_promise_2();
		IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
		Task_AddToActiveTasks_m2680726720(NULL /*static, unused*/, (Task_t1843236107 *)L_27, /*hidden argument*/NULL);
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(BinaryCompatibility_t1303671145_il2cpp_TypeInfo_var);
			bool L_28 = BinaryCompatibility_get_TargetsAtLeast_Desktop_V4_5_m4051793983(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_0119;
			}
		}

IL_00ae:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * L_29 = (U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			((  void (*) (U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			V_1 = (U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 *)L_29;
			U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * L_30 = V_1;
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_31 = V_0;
			NullCheck(L_30);
			L_30->set_U3CU3Ef__refU242_1(L_31);
			U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * L_32 = V_1;
			AtomicBoolean_t379413895 * L_33 = (AtomicBoolean_t379413895 *)il2cpp_codegen_object_new(AtomicBoolean_t379413895_il2cpp_TypeInfo_var);
			AtomicBoolean__ctor_m845874490(L_33, /*hidden argument*/NULL);
			NullCheck(L_32);
			L_32->set_invoked_0(L_33);
			Func_3_t1604491142 * L_34 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * L_35 = V_1;
			IntPtr_t L_36;
			L_36.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			AsyncCallback_t163412349 * L_37 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_37, (Il2CppObject *)L_35, (IntPtr_t)L_36, /*hidden argument*/NULL);
			Il2CppObject * L_38 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_34);
			Il2CppObject * L_39 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_34, (AsyncCallback_t163412349 *)L_37, (Il2CppObject *)L_38, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_2 = (Il2CppObject *)L_39;
			Il2CppObject * L_40 = V_2;
			if (!L_40)
			{
				goto IL_0114;
			}
		}

IL_00e0:
		{
			Il2CppObject * L_41 = V_2;
			NullCheck((Il2CppObject *)L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t1999651008_il2cpp_TypeInfo_var, (Il2CppObject *)L_41);
			if (!L_42)
			{
				goto IL_0114;
			}
		}

IL_00eb:
		{
			U3CFromAsyncImplU3Ec__AnonStorey1_t1592407391 * L_43 = V_1;
			NullCheck(L_43);
			AtomicBoolean_t379413895 * L_44 = (AtomicBoolean_t379413895 *)L_43->get_invoked_0();
			NullCheck((AtomicBoolean_t379413895 *)L_44);
			bool L_45 = AtomicBoolean_TryRelaxedSet_m1519556294((AtomicBoolean_t379413895 *)L_44, /*hidden argument*/NULL);
			if (!L_45)
			{
				goto IL_0114;
			}
		}

IL_00fb:
		{
			Il2CppObject * L_46 = V_2;
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_47 = V_0;
			NullCheck(L_47);
			Func_2_t2979935071 * L_48 = (Func_2_t2979935071 *)L_47->get_endFunction_0();
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_49 = V_0;
			NullCheck(L_49);
			Action_1_t1801450390 * L_50 = (Action_1_t1801450390 *)L_49->get_endAction_1();
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_51 = V_0;
			NullCheck(L_51);
			Task_1_t2445339805 * L_52 = (Task_1_t2445339805 *)L_51->get_promise_2();
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2979935071 *, Action_1_t1801450390 *, Task_1_t2445339805 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_46, (Func_2_t2979935071 *)L_48, (Action_1_t1801450390 *)L_50, (Task_1_t2445339805 *)L_52, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		}

IL_0114:
		{
			goto IL_012d;
		}

IL_0119:
		{
			Func_3_t1604491142 * L_53 = ___beginMethod0;
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_54 = V_0;
			IntPtr_t L_55;
			L_55.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
			AsyncCallback_t163412349 * L_56 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m3071689932(L_56, (Il2CppObject *)L_54, (IntPtr_t)L_55, /*hidden argument*/NULL);
			Il2CppObject * L_57 = ___state3;
			NullCheck((Func_3_t1604491142 *)L_53);
			Il2CppObject * L_58 = Func_3_Invoke_m2184768394((Func_3_t1604491142 *)L_53, (AsyncCallback_t163412349 *)L_56, (Il2CppObject *)L_57, /*hidden argument*/Func_3_Invoke_m2184768394_MethodInfo_var);
			V_3 = (Il2CppObject *)L_58;
		}

IL_012d:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0132;
		throw e;
	}

CATCH_0132:
	{ // begin catch(System.Object)
		{
			bool L_59 = AsyncCausalityTracer_get_LoggingOn_m929857466(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_59)
			{
				goto IL_014f;
			}
		}

IL_013d:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_60 = V_0;
			NullCheck(L_60);
			Task_1_t2445339805 * L_61 = (Task_1_t2445339805 *)L_60->get_promise_2();
			NullCheck((Task_t1843236107 *)L_61);
			int32_t L_62 = Task_get_Id_m4106115082((Task_t1843236107 *)L_61, /*hidden argument*/NULL);
			AsyncCausalityTracer_TraceOperationCompletion_m1161622555(NULL /*static, unused*/, (int32_t)0, (int32_t)L_62, (int32_t)3, /*hidden argument*/NULL);
		}

IL_014f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			bool L_63 = ((Task_t1843236107_StaticFields*)Task_t1843236107_il2cpp_TypeInfo_var->static_fields)->get_s_asyncDebuggingEnabled_12();
			if (!L_63)
			{
				goto IL_0169;
			}
		}

IL_0159:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_64 = V_0;
			NullCheck(L_64);
			Task_1_t2445339805 * L_65 = (Task_1_t2445339805 *)L_64->get_promise_2();
			NullCheck((Task_t1843236107 *)L_65);
			int32_t L_66 = Task_get_Id_m4106115082((Task_t1843236107 *)L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t1843236107_il2cpp_TypeInfo_var);
			Task_RemoveFromActiveTasks_m437324001(NULL /*static, unused*/, (int32_t)L_66, /*hidden argument*/NULL);
		}

IL_0169:
		{
			U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_67 = V_0;
			NullCheck(L_67);
			Task_1_t2445339805 * L_68 = (Task_1_t2445339805 *)L_67->get_promise_2();
			Initobj (VoidTaskResult_t3325310798_il2cpp_TypeInfo_var, (&V_4));
			VoidTaskResult_t3325310798  L_69 = V_4;
			NullCheck((Task_1_t2445339805 *)L_68);
			((  bool (*) (Task_1_t2445339805 *, VoidTaskResult_t3325310798 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Task_1_t2445339805 *)L_68, (VoidTaskResult_t3325310798 )L_69, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}
	} // end catch (depth: 1)

IL_0181:
	{
		U3CFromAsyncImplU3Ec__AnonStorey2_t268684924 * L_70 = V_0;
		NullCheck(L_70);
		Task_1_t2445339805 * L_71 = (Task_1_t2445339805 *)L_70->get_promise_2();
		return L_71;
	}
}
// System.Void System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>::.ctor(System.Int32)
extern "C"  void SparseArray_1__ctor_m946292937_gshared (SparseArray_1_t935185730 * __this, int32_t ___initialSize0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialSize0;
		il2cpp_codegen_memory_barrier();
		__this->set_m_array_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_0)));
		return;
	}
}
// T[] System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>::get_Current()
extern "C"  ObjectU5BU5D_t3614634134* SparseArray_1_get_Current_m1166481189_gshared (SparseArray_1_t935185730 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// System.Int32 System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>::Add(T)
extern "C"  int32_t SparseArray_1_Add_m2311537735_gshared (SparseArray_1_t935185730 * __this, Il2CppObject * ___e0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ObjectU5BU5D_t3614634134* V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (ObjectU5BU5D_t3614634134*)L_0;
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		V_2 = (bool)0;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = V_1;
			Monitor_Enter_m2026616996(NULL /*static, unused*/, (Il2CppObject *)L_2, (bool*)(&V_2), /*hidden argument*/NULL);
			V_3 = (int32_t)0;
			goto IL_009a;
		}

IL_001c:
		{
			ObjectU5BU5D_t3614634134* L_3 = V_0;
			int32_t L_4 = V_3;
			NullCheck(L_3);
			int32_t L_5 = L_4;
			Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
			if (L_6)
			{
				goto IL_0042;
			}
		}

IL_002d:
		{
			ObjectU5BU5D_t3614634134* L_7 = V_0;
			int32_t L_8 = V_3;
			NullCheck(L_7);
			Il2CppObject * L_9 = ___e0;
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject **)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))), (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			int32_t L_10 = V_3;
			V_4 = (int32_t)L_10;
			IL2CPP_LEAVE(0xB7, FINALLY_00a8);
		}

IL_0042:
		{
			int32_t L_11 = V_3;
			ObjectU5BU5D_t3614634134* L_12 = V_0;
			NullCheck(L_12);
			if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))-(int32_t)1))))))
			{
				goto IL_0096;
			}
		}

IL_004d:
		{
			ObjectU5BU5D_t3614634134* L_13 = V_0;
			ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
			il2cpp_codegen_memory_barrier();
			if ((((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_13) == ((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_14)))
			{
				goto IL_0060;
			}
		}

IL_005b:
		{
			goto IL_0096;
		}

IL_0060:
		{
			ObjectU5BU5D_t3614634134* L_15 = V_0;
			NullCheck(L_15);
			V_5 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))*(int32_t)2))));
			ObjectU5BU5D_t3614634134* L_16 = V_0;
			ObjectU5BU5D_t3614634134* L_17 = V_5;
			int32_t L_18 = V_3;
			Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, (Il2CppArray *)(Il2CppArray *)L_17, (int32_t)((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
			ObjectU5BU5D_t3614634134* L_19 = V_5;
			int32_t L_20 = V_3;
			Il2CppObject * L_21 = ___e0;
			NullCheck(L_19);
			(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_20+(int32_t)1))), (Il2CppObject *)L_21);
			ObjectU5BU5D_t3614634134* L_22 = V_5;
			il2cpp_codegen_memory_barrier();
			__this->set_m_array_0(L_22);
			int32_t L_23 = V_3;
			V_4 = (int32_t)((int32_t)((int32_t)L_23+(int32_t)1));
			IL2CPP_LEAVE(0xB7, FINALLY_00a8);
		}

IL_0096:
		{
			int32_t L_24 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		}

IL_009a:
		{
			int32_t L_25 = V_3;
			ObjectU5BU5D_t3614634134* L_26 = V_0;
			NullCheck(L_26);
			if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
			{
				goto IL_001c;
			}
		}

IL_00a3:
		{
			IL2CPP_LEAVE(0xB2, FINALLY_00a8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a8;
	}

FINALLY_00a8:
	{ // begin finally (depth: 1)
		{
			bool L_27 = V_2;
			if (!L_27)
			{
				goto IL_00b1;
			}
		}

IL_00ab:
		{
			Il2CppObject * L_28 = V_1;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_28, /*hidden argument*/NULL);
		}

IL_00b1:
		{
			IL2CPP_END_FINALLY(168)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(168)
	{
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b2:
	{
		goto IL_0000;
	}

IL_00b7:
	{
		int32_t L_29 = V_4;
		return L_29;
	}
}
// System.Void System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>::Remove(T)
extern "C"  void SparseArray_1_Remove_m1428540360_gshared (SparseArray_1_t935185730 * __this, Il2CppObject * ___e0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (ObjectU5BU5D_t3614634134*)L_0;
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		V_2 = (bool)0;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = V_1;
			Monitor_Enter_m2026616996(NULL /*static, unused*/, (Il2CppObject *)L_2, (bool*)(&V_2), /*hidden argument*/NULL);
			V_3 = (int32_t)0;
			goto IL_005c;
		}

IL_001c:
		{
			ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
			il2cpp_codegen_memory_barrier();
			int32_t L_4 = V_3;
			NullCheck(L_3);
			int32_t L_5 = L_4;
			Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
			Il2CppObject * L_7 = ___e0;
			if ((!(((Il2CppObject*)(Il2CppObject *)L_6) == ((Il2CppObject*)(Il2CppObject *)L_7))))
			{
				goto IL_0058;
			}
		}

IL_003a:
		{
			ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
			il2cpp_codegen_memory_barrier();
			int32_t L_9 = V_3;
			NullCheck(L_8);
			((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject **)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			goto IL_006c;
		}

IL_0058:
		{
			int32_t L_10 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_005c:
		{
			int32_t L_11 = V_3;
			ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)__this->get_m_array_0();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_12);
			if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
			{
				goto IL_001c;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_2;
			if (!L_13)
			{
				goto IL_007a;
			}
		}

IL_0074:
		{
			Il2CppObject * L_14 = V_1;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		}

IL_007a:
		{
			IL2CPP_END_FINALLY(113)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007b:
	{
		return;
	}
}
// System.Void System.Tuple`2<System.Object,System.Char>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m1750522201_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___item10, Il2CppChar ___item21, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		Il2CppChar L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		return;
	}
}
// T1 System.Tuple`2<System.Object,System.Char>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m3943324081_gshared (Tuple_2_t1801232814 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`2<System.Object,System.Char>::get_Item2()
extern "C"  Il2CppChar Tuple_2_get_Item2_m4176927457_gshared (Tuple_2_t1801232814 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_m_Item2_1();
		return L_0;
	}
}
// System.Boolean System.Tuple`2<System.Object,System.Char>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m732327059_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_Equals_m732327059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`2<System.Object,System.Char>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_2_System_Collections_IStructuralEquatable_Equals_m3661955903_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralEquatable_Equals_m3661955903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_2_t1801232814 * V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_2_t1801232814 *)((Tuple_2_t1801232814 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_2_t1801232814 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_2_t1801232814 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_7 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Il2CppObject * L_8 = ___comparer1;
		Il2CppChar L_9 = (Il2CppChar)__this->get_m_Item2_1();
		Il2CppChar L_10 = L_9;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_10);
		Tuple_2_t1801232814 * L_12 = V_0;
		NullCheck(L_12);
		Il2CppChar L_13 = (Il2CppChar)L_12->get_m_Item2_1();
		Il2CppChar L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_14);
		NullCheck((Il2CppObject *)L_8);
		bool L_16 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_11, (Il2CppObject *)L_15);
		G_B7_0 = ((int32_t)(L_16));
		goto IL_0057;
	}

IL_0056:
	{
		G_B7_0 = 0;
	}

IL_0057:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Char>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_2_System_IComparable_CompareTo_m141023310_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_IComparable_CompareTo_m141023310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Char>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_2_System_Collections_IStructuralComparable_CompareTo_m3600080012_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralComparable_CompareTo_m3600080012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_2_t1801232814 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_2_t1801232814 *)((Tuple_2_t1801232814 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_2_t1801232814 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_2_t1801232814 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_8);
		int32_t L_12 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		V_1 = (int32_t)L_12;
		int32_t L_13 = V_1;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0065:
	{
		Il2CppObject * L_15 = ___comparer1;
		Il2CppChar L_16 = (Il2CppChar)__this->get_m_Item2_1();
		Il2CppChar L_17 = L_16;
		Il2CppObject * L_18 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_17);
		Tuple_2_t1801232814 * L_19 = V_0;
		NullCheck(L_19);
		Il2CppChar L_20 = (Il2CppChar)L_19->get_m_Item2_1();
		Il2CppChar L_21 = L_20;
		Il2CppObject * L_22 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_21);
		NullCheck((Il2CppObject *)L_15);
		int32_t L_23 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_18, (Il2CppObject *)L_22);
		return L_23;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Char>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m3704074721_gshared (Tuple_2_t1801232814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_GetHashCode_m3704074721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Char>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m1235293593_gshared (Tuple_2_t1801232814 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m1235293593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		Il2CppObject * L_3 = ___comparer0;
		Il2CppChar L_4 = (Il2CppChar)__this->get_m_Item2_1();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		NullCheck((Il2CppObject *)L_3);
		int32_t L_7 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_6);
		int32_t L_8 = Tuple_CombineHashCodes_m3512177706(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String System.Tuple`2<System.Object,System.Char>::ToString()
extern "C"  String_t* Tuple_2_ToString_m2248070625_gshared (Tuple_2_t1801232814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_ToString_m2248070625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`2<System.Object,System.Char>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_2_System_ITuple_ToString_m3268469601_gshared (Tuple_2_t1801232814 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_ITuple_ToString_m3268469601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_2, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = ___sb0;
		Il2CppChar L_4 = (Il2CppChar)__this->get_m_Item2_1();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_5);
		NullCheck((StringBuilder_t1221177846 *)L_3);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_7 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_7);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_7, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_8 = ___sb0;
		NullCheck((Il2CppObject *)L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_8);
		return L_9;
	}
}
// System.Void System.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m926017102_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		Il2CppObject * L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		return;
	}
}
// T1 System.Tuple`2<System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m1631309290_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`2<System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_2_get_Item2_m761907184_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item2_1();
		return L_0;
	}
}
// System.Boolean System.Tuple`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m248290730_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_Equals_m248290730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`2<System.Object,System.Object>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_2_System_Collections_IStructuralEquatable_Equals_m984969174_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralEquatable_Equals_m984969174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_2_t1036200771 * V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_2_t1036200771 *)((Tuple_2_t1036200771 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_2_t1036200771 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_2_t1036200771 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_7 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_2_t1036200771 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_8);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0057;
	}

IL_0056:
	{
		G_B7_0 = 0;
	}

IL_0057:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_2_System_IComparable_CompareTo_m3606644887_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_IComparable_CompareTo_m3606644887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Object>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_2_System_Collections_IStructuralComparable_CompareTo_m122580837_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralComparable_CompareTo_m122580837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_2_t1036200771 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_2_t1036200771 *)((Tuple_2_t1036200771 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_2_t1036200771 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_2_t1036200771 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_8);
		int32_t L_12 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		V_1 = (int32_t)L_12;
		int32_t L_13 = V_1;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0065:
	{
		Il2CppObject * L_15 = ___comparer1;
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_2_t1036200771 * L_17 = V_0;
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_15);
		int32_t L_19 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_16, (Il2CppObject *)L_18);
		return L_19;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m3472475602_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_GetHashCode_m3472475602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`2<System.Object,System.Object>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m2915229304_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_Collections_IStructuralEquatable_GetHashCode_m2915229304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		Il2CppObject * L_3 = ___comparer0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		int32_t L_6 = Tuple_CombineHashCodes_m3512177706(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String System.Tuple`2<System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_2_ToString_m4162645436_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_ToString_m4162645436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`2<System.Object,System.Object>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_2_System_ITuple_ToString_m3694123798_gshared (Tuple_2_t1036200771 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2_System_ITuple_ToString_m3694123798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_2, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = ___sb0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((StringBuilder_t1221177846 *)L_3);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_5 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_5);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_5, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_6 = ___sb0;
		NullCheck((Il2CppObject *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_6);
		return L_7;
	}
}
// System.Void System.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m3862679166_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		Il2CppObject * L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		Il2CppObject * L_2 = ___item32;
		__this->set_m_Item3_2(L_2);
		return;
	}
}
// T1 System.Tuple`3<System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_3_get_Item1_m1627040007_gshared (Tuple_3_t1136823221 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`3<System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_3_get_Item2_m2105596403_gshared (Tuple_3_t1136823221 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item2_1();
		return L_0;
	}
}
// T3 System.Tuple`3<System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_3_get_Item3_m160071119_gshared (Tuple_3_t1136823221 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item3_2();
		return L_0;
	}
}
// System.Boolean System.Tuple`3<System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_3_Equals_m4286593197_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_Equals_m4286593197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`3<System.Object,System.Object,System.Object>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_3_System_Collections_IStructuralEquatable_Equals_m2717096461_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_System_Collections_IStructuralEquatable_Equals_m2717096461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_3_t1136823221 * V_0 = NULL;
	int32_t G_B8_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_3_t1136823221 *)((Tuple_3_t1136823221 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_3_t1136823221 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_3_t1136823221 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_7 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_3_t1136823221 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_8);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		if (!L_12)
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_13 = ___comparer1;
		Il2CppObject * L_14 = (Il2CppObject *)__this->get_m_Item3_2();
		Tuple_3_t1136823221 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_13);
		bool L_17 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_13, (Il2CppObject *)L_14, (Il2CppObject *)L_16);
		G_B8_0 = ((int32_t)(L_17));
		goto IL_0078;
	}

IL_0077:
	{
		G_B8_0 = 0;
	}

IL_0078:
	{
		return (bool)G_B8_0;
	}
}
// System.Int32 System.Tuple`3<System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_3_System_IComparable_CompareTo_m4004863408_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_System_IComparable_CompareTo_m4004863408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`3<System.Object,System.Object,System.Object>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_3_System_Collections_IStructuralComparable_CompareTo_m3210780030_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_System_Collections_IStructuralComparable_CompareTo_m3210780030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_3_t1136823221 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_3_t1136823221 *)((Tuple_3_t1136823221 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_3_t1136823221 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_3_t1136823221 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_8);
		int32_t L_12 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		V_1 = (int32_t)L_12;
		int32_t L_13 = V_1;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0065:
	{
		Il2CppObject * L_15 = ___comparer1;
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_3_t1136823221 * L_17 = V_0;
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_15);
		int32_t L_19 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_16, (Il2CppObject *)L_18);
		V_1 = (int32_t)L_19;
		int32_t L_20 = V_1;
		if (!L_20)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_21 = V_1;
		return L_21;
	}

IL_008a:
	{
		Il2CppObject * L_22 = ___comparer1;
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_m_Item3_2();
		Tuple_3_t1136823221 * L_24 = V_0;
		NullCheck(L_24);
		Il2CppObject * L_25 = (Il2CppObject *)L_24->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_22);
		int32_t L_26 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (Il2CppObject *)L_25);
		return L_26;
	}
}
// System.Int32 System.Tuple`3<System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_3_GetHashCode_m132921887_gshared (Tuple_3_t1136823221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_GetHashCode_m132921887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`3<System.Object,System.Object,System.Object>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_3_System_Collections_IStructuralEquatable_GetHashCode_m2445616235_gshared (Tuple_3_t1136823221 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_System_Collections_IStructuralEquatable_GetHashCode_m2445616235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		Il2CppObject * L_3 = ___comparer0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		Il2CppObject * L_6 = ___comparer0;
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_6);
		int32_t L_8 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		int32_t L_9 = Tuple_CombineHashCodes_m514564413(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_5, (int32_t)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.String System.Tuple`3<System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_3_ToString_m1145130135_gshared (Tuple_3_t1136823221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_ToString_m1145130135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`3<System.Object,System.Object,System.Object>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_3_System_ITuple_ToString_m4112284619_gshared (Tuple_3_t1136823221 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3_System_ITuple_ToString_m4112284619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_2, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = ___sb0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((StringBuilder_t1221177846 *)L_3);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_5 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_5);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_5, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_6 = ___sb0;
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_m_Item3_2();
		NullCheck((StringBuilder_t1221177846 *)L_6);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_8 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_8);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_8, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_9 = ___sb0;
		NullCheck((Il2CppObject *)L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_9);
		return L_10;
	}
}
// System.Void System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m1179645532_gshared (Tuple_4_t3284226035 * __this, int32_t ___item10, int32_t ___item21, int32_t ___item32, bool ___item43, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		int32_t L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		int32_t L_2 = ___item32;
		__this->set_m_Item3_2(L_2);
		bool L_3 = ___item43;
		__this->set_m_Item4_3(L_3);
		return;
	}
}
// T1 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::get_Item1()
extern "C"  int32_t Tuple_4_get_Item1_m1262997696_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::get_Item2()
extern "C"  int32_t Tuple_4_get_Item2_m313993498_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_Item2_1();
		return L_0;
	}
}
// T3 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::get_Item3()
extern "C"  int32_t Tuple_4_get_Item3_m3555904244_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_Item3_2();
		return L_0;
	}
}
// T4 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::get_Item4()
extern "C"  bool Tuple_4_get_Item4_m1594534006_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_m_Item4_3();
		return L_0;
	}
}
// System.Boolean System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::Equals(System.Object)
extern "C"  bool Tuple_4_Equals_m4168359424_gshared (Tuple_4_t3284226035 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_Equals_m4168359424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_4_System_Collections_IStructuralEquatable_Equals_m3064214516_gshared (Tuple_4_t3284226035 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_Equals_m3064214516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t3284226035 * V_0 = NULL;
	int32_t G_B9_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t3284226035 *)((Tuple_4_t3284226035 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t3284226035 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		int32_t L_4 = (int32_t)__this->get_m_Item1_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_5);
		Tuple_4_t3284226035 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get_m_Item1_0();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_9);
		NullCheck((Il2CppObject *)L_3);
		bool L_11 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_6, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_12 = ___comparer1;
		int32_t L_13 = (int32_t)__this->get_m_Item2_1();
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_14);
		Tuple_4_t3284226035 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)L_16->get_m_Item2_1();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)L_12);
		bool L_20 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_12, (Il2CppObject *)L_15, (Il2CppObject *)L_19);
		if (!L_20)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_21 = ___comparer1;
		int32_t L_22 = (int32_t)__this->get_m_Item3_2();
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_23);
		Tuple_4_t3284226035 * L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = (int32_t)L_25->get_m_Item3_2();
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_27);
		NullCheck((Il2CppObject *)L_21);
		bool L_29 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_21, (Il2CppObject *)L_24, (Il2CppObject *)L_28);
		if (!L_29)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_30 = ___comparer1;
		bool L_31 = (bool)__this->get_m_Item4_3();
		bool L_32 = L_31;
		Il2CppObject * L_33 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_32);
		Tuple_4_t3284226035 * L_34 = V_0;
		NullCheck(L_34);
		bool L_35 = (bool)L_34->get_m_Item4_3();
		bool L_36 = L_35;
		Il2CppObject * L_37 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_36);
		NullCheck((Il2CppObject *)L_30);
		bool L_38 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_30, (Il2CppObject *)L_33, (Il2CppObject *)L_37);
		G_B9_0 = ((int32_t)(L_38));
		goto IL_0099;
	}

IL_0098:
	{
		G_B9_0 = 0;
	}

IL_0099:
	{
		return (bool)G_B9_0;
	}
}
// System.Int32 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_4_System_IComparable_CompareTo_m4053149811_gshared (Tuple_4_t3284226035 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_IComparable_CompareTo_m4053149811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m1720090693_gshared (Tuple_4_t3284226035 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralComparable_CompareTo_m1720090693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t3284226035 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t3284226035 *)((Tuple_4_t3284226035 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t3284226035 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		int32_t L_9 = (int32_t)__this->get_m_Item1_0();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_10);
		Tuple_4_t3284226035 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_m_Item1_0();
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_14);
		NullCheck((Il2CppObject *)L_8);
		int32_t L_16 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_11, (Il2CppObject *)L_15);
		V_1 = (int32_t)L_16;
		int32_t L_17 = V_1;
		if (!L_17)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_18 = V_1;
		return L_18;
	}

IL_0065:
	{
		Il2CppObject * L_19 = ___comparer1;
		int32_t L_20 = (int32_t)__this->get_m_Item2_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_21);
		Tuple_4_t3284226035 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)L_23->get_m_Item2_1();
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_25);
		NullCheck((Il2CppObject *)L_19);
		int32_t L_27 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_22, (Il2CppObject *)L_26);
		V_1 = (int32_t)L_27;
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_29 = V_1;
		return L_29;
	}

IL_008a:
	{
		Il2CppObject * L_30 = ___comparer1;
		int32_t L_31 = (int32_t)__this->get_m_Item3_2();
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_32);
		Tuple_4_t3284226035 * L_34 = V_0;
		NullCheck(L_34);
		int32_t L_35 = (int32_t)L_34->get_m_Item3_2();
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_36);
		NullCheck((Il2CppObject *)L_30);
		int32_t L_38 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_30, (Il2CppObject *)L_33, (Il2CppObject *)L_37);
		V_1 = (int32_t)L_38;
		int32_t L_39 = V_1;
		if (!L_39)
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_40 = V_1;
		return L_40;
	}

IL_00af:
	{
		Il2CppObject * L_41 = ___comparer1;
		bool L_42 = (bool)__this->get_m_Item4_3();
		bool L_43 = L_42;
		Il2CppObject * L_44 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_43);
		Tuple_4_t3284226035 * L_45 = V_0;
		NullCheck(L_45);
		bool L_46 = (bool)L_45->get_m_Item4_3();
		bool L_47 = L_46;
		Il2CppObject * L_48 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_47);
		NullCheck((Il2CppObject *)L_41);
		int32_t L_49 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_41, (Il2CppObject *)L_44, (Il2CppObject *)L_48);
		return L_49;
	}
}
// System.Int32 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::GetHashCode()
extern "C"  int32_t Tuple_4_GetHashCode_m109059448_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_GetHashCode_m109059448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m686082162_gshared (Tuple_4_t3284226035 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m686082162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		int32_t L_1 = (int32_t)__this->get_m_Item1_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		NullCheck((Il2CppObject *)L_0);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_3);
		Il2CppObject * L_5 = ___comparer0;
		int32_t L_6 = (int32_t)__this->get_m_Item2_1();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		NullCheck((Il2CppObject *)L_5);
		int32_t L_9 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_5, (Il2CppObject *)L_8);
		Il2CppObject * L_10 = ___comparer0;
		int32_t L_11 = (int32_t)__this->get_m_Item3_2();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_12);
		NullCheck((Il2CppObject *)L_10);
		int32_t L_14 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_10, (Il2CppObject *)L_13);
		Il2CppObject * L_15 = ___comparer0;
		bool L_16 = (bool)__this->get_m_Item4_3();
		bool L_17 = L_16;
		Il2CppObject * L_18 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_17);
		NullCheck((Il2CppObject *)L_15);
		int32_t L_19 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_18);
		int32_t L_20 = Tuple_CombineHashCodes_m1865303580(NULL /*static, unused*/, (int32_t)L_4, (int32_t)L_9, (int32_t)L_14, (int32_t)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.String System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::ToString()
extern "C"  String_t* Tuple_4_ToString_m1586339854_gshared (Tuple_4_t3284226035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_ToString_m1586339854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_4_System_ITuple_ToString_m4156720916_gshared (Tuple_4_t3284226035 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_ITuple_ToString_m4156720916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		int32_t L_1 = (int32_t)__this->get_m_Item1_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_4 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_4);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_4, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_5 = ___sb0;
		int32_t L_6 = (int32_t)__this->get_m_Item2_1();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		NullCheck((StringBuilder_t1221177846 *)L_5);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_9 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_9);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_9, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_10 = ___sb0;
		int32_t L_11 = (int32_t)__this->get_m_Item3_2();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_12);
		NullCheck((StringBuilder_t1221177846 *)L_10);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_10, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_14 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_14);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_14, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_15 = ___sb0;
		bool L_16 = (bool)__this->get_m_Item4_3();
		bool L_17 = L_16;
		Il2CppObject * L_18 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_17);
		NullCheck((StringBuilder_t1221177846 *)L_15);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_15, (Il2CppObject *)L_18, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_19 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_19);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_19, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_20 = ___sb0;
		NullCheck((Il2CppObject *)L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_20);
		return L_21;
	}
}
// System.Void System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m4098087217_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, int32_t ___item32, int32_t ___item43, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		Il2CppObject * L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		int32_t L_2 = ___item32;
		__this->set_m_Item3_2(L_2);
		int32_t L_3 = ___item43;
		__this->set_m_Item4_3(L_3);
		return;
	}
}
// T1 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::get_Item1()
extern "C"  Il2CppObject * Tuple_4_get_Item1_m3474445202_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::get_Item2()
extern "C"  Il2CppObject * Tuple_4_get_Item2_m918849776_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item2_1();
		return L_0;
	}
}
// T3 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::get_Item3()
extern "C"  int32_t Tuple_4_get_Item3_m2952384586_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_Item3_2();
		return L_0;
	}
}
// T4 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::get_Item4()
extern "C"  int32_t Tuple_4_get_Item4_m1221904288_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_m_Item4_3();
		return L_0;
	}
}
// System.Boolean System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::Equals(System.Object)
extern "C"  bool Tuple_4_Equals_m293337750_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_Equals_m293337750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_4_System_Collections_IStructuralEquatable_Equals_m745313518_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_Equals_m745313518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t1664179553 * V_0 = NULL;
	int32_t G_B9_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t1664179553 *)((Tuple_4_t1664179553 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t1664179553 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_4_t1664179553 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_7 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_4_t1664179553 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_8);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		if (!L_12)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_13 = ___comparer1;
		int32_t L_14 = (int32_t)__this->get_m_Item3_2();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_15);
		Tuple_4_t1664179553 * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = (int32_t)L_17->get_m_Item3_2();
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_19);
		NullCheck((Il2CppObject *)L_13);
		bool L_21 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_13, (Il2CppObject *)L_16, (Il2CppObject *)L_20);
		if (!L_21)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_22 = ___comparer1;
		int32_t L_23 = (int32_t)__this->get_m_Item4_3();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_24);
		Tuple_4_t1664179553 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)L_26->get_m_Item4_3();
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_28);
		NullCheck((Il2CppObject *)L_22);
		bool L_30 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_22, (Il2CppObject *)L_25, (Il2CppObject *)L_29);
		G_B9_0 = ((int32_t)(L_30));
		goto IL_0099;
	}

IL_0098:
	{
		G_B9_0 = 0;
	}

IL_0099:
	{
		return (bool)G_B9_0;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_4_System_IComparable_CompareTo_m2767899169_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_IComparable_CompareTo_m2767899169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3256184635_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3256184635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t1664179553 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t1664179553 *)((Tuple_4_t1664179553 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t1664179553 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_4_t1664179553 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_8);
		int32_t L_12 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		V_1 = (int32_t)L_12;
		int32_t L_13 = V_1;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0065:
	{
		Il2CppObject * L_15 = ___comparer1;
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_4_t1664179553 * L_17 = V_0;
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_15);
		int32_t L_19 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_16, (Il2CppObject *)L_18);
		V_1 = (int32_t)L_19;
		int32_t L_20 = V_1;
		if (!L_20)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_21 = V_1;
		return L_21;
	}

IL_008a:
	{
		Il2CppObject * L_22 = ___comparer1;
		int32_t L_23 = (int32_t)__this->get_m_Item3_2();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_24);
		Tuple_4_t1664179553 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)L_26->get_m_Item3_2();
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_28);
		NullCheck((Il2CppObject *)L_22);
		int32_t L_30 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_22, (Il2CppObject *)L_25, (Il2CppObject *)L_29);
		V_1 = (int32_t)L_30;
		int32_t L_31 = V_1;
		if (!L_31)
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_32 = V_1;
		return L_32;
	}

IL_00af:
	{
		Il2CppObject * L_33 = ___comparer1;
		int32_t L_34 = (int32_t)__this->get_m_Item4_3();
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_35);
		Tuple_4_t1664179553 * L_37 = V_0;
		NullCheck(L_37);
		int32_t L_38 = (int32_t)L_37->get_m_Item4_3();
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_39);
		NullCheck((Il2CppObject *)L_33);
		int32_t L_41 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_33, (Il2CppObject *)L_36, (Il2CppObject *)L_40);
		return L_41;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::GetHashCode()
extern "C"  int32_t Tuple_4_GetHashCode_m1825267794_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_GetHashCode_m1825267794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m4281699352_gshared (Tuple_4_t1664179553 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m4281699352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		Il2CppObject * L_3 = ___comparer0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		Il2CppObject * L_6 = ___comparer0;
		int32_t L_7 = (int32_t)__this->get_m_Item3_2();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_8);
		NullCheck((Il2CppObject *)L_6);
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_6, (Il2CppObject *)L_9);
		Il2CppObject * L_11 = ___comparer0;
		int32_t L_12 = (int32_t)__this->get_m_Item4_3();
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_13);
		NullCheck((Il2CppObject *)L_11);
		int32_t L_15 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Il2CppObject *)L_14);
		int32_t L_16 = Tuple_CombineHashCodes_m1865303580(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_5, (int32_t)L_10, (int32_t)L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.String System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::ToString()
extern "C"  String_t* Tuple_4_ToString_m1553484336_gshared (Tuple_4_t1664179553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_ToString_m1553484336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_4_System_ITuple_ToString_m2916912930_gshared (Tuple_4_t1664179553 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_ITuple_ToString_m2916912930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_2, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = ___sb0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((StringBuilder_t1221177846 *)L_3);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_5 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_5);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_5, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_6 = ___sb0;
		int32_t L_7 = (int32_t)__this->get_m_Item3_2();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_8);
		NullCheck((StringBuilder_t1221177846 *)L_6);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_6, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_10 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_10);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_10, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_11 = ___sb0;
		int32_t L_12 = (int32_t)__this->get_m_Item4_3();
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_13);
		NullCheck((StringBuilder_t1221177846 *)L_11);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_11, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_15 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_15);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_15, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_16 = ___sb0;
		NullCheck((Il2CppObject *)L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_16);
		return L_17;
	}
}
// System.Void System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m261619317_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___item10;
		__this->set_m_Item1_0(L_0);
		Il2CppObject * L_1 = ___item21;
		__this->set_m_Item2_1(L_1);
		Il2CppObject * L_2 = ___item32;
		__this->set_m_Item3_2(L_2);
		Il2CppObject * L_3 = ___item43;
		__this->set_m_Item4_3(L_3);
		return;
	}
}
// T1 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_4_get_Item1_m446721184_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item1_0();
		return L_0;
	}
}
// T2 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_4_get_Item2_m3646646738_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item2_1();
		return L_0;
	}
}
// T3 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_4_get_Item3_m448360856_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item3_2();
		return L_0;
	}
}
// T4 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_4_get_Item4_m398643186_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Item4_3();
		return L_0;
	}
}
// System.Boolean System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_4_Equals_m2553141920_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_Equals_m2553141920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		EqualityComparer_1_t1263084566 * L_1 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IStructuralEquatable::Equals(System.Object,System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Boolean System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.Collections.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_4_System_Collections_IStructuralEquatable_Equals_m1873162208_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_Equals_m1873162208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t2865985893 * V_0 = NULL;
	int32_t G_B9_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t2865985893 *)((Tuple_4_t2865985893 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t2865985893 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___comparer1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_4_t2865985893 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_7 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_4_t2865985893 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_8);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		if (!L_12)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_13 = ___comparer1;
		Il2CppObject * L_14 = (Il2CppObject *)__this->get_m_Item3_2();
		Tuple_4_t2865985893 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_13);
		bool L_17 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_13, (Il2CppObject *)L_14, (Il2CppObject *)L_16);
		if (!L_17)
		{
			goto IL_0098;
		}
	}
	{
		Il2CppObject * L_18 = ___comparer1;
		Il2CppObject * L_19 = (Il2CppObject *)__this->get_m_Item4_3();
		Tuple_4_t2865985893 * L_20 = V_0;
		NullCheck(L_20);
		Il2CppObject * L_21 = (Il2CppObject *)L_20->get_m_Item4_3();
		NullCheck((Il2CppObject *)L_18);
		bool L_22 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.IEqualityComparer::Equals(System.Object,System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_18, (Il2CppObject *)L_19, (Il2CppObject *)L_21);
		G_B9_0 = ((int32_t)(L_22));
		goto IL_0099;
	}

IL_0098:
	{
		G_B9_0 = 0;
	}

IL_0099:
	{
		return (bool)G_B9_0;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_4_System_IComparable_CompareTo_m3304034037_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_IComparable_CompareTo_m3304034037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		Comparer_1_t1579458414 * L_1 = Comparer_1_get_Default_m40106963(NULL /*static, unused*/, /*hidden argument*/Comparer_1_get_Default_m40106963_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_2 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IStructuralComparable::CompareTo(System.Object,System.Collections.IComparer) */, IStructuralComparable_t881765570_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.Collections.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3378305607_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralComparable_CompareTo_m3378305607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tuple_4_t2865985893 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = (Tuple_4_t2865985893 *)((Tuple_4_t2865985893 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Tuple_4_t2865985893 * L_2 = V_0;
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck((Il2CppObject *)__this);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		String_t* L_6 = Environment_GetResourceString_m3280810056(NULL /*static, unused*/, (String_t*)_stringLiteral2734479056, (ObjectU5BU5D_t3614634134*)L_3, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_7, (String_t*)L_6, (String_t*)_stringLiteral3712335156, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003e:
	{
		V_1 = (int32_t)0;
		Il2CppObject * L_8 = ___comparer1;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_m_Item1_0();
		Tuple_4_t2865985893 * L_10 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_8);
		int32_t L_12 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_11);
		V_1 = (int32_t)L_12;
		int32_t L_13 = V_1;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_14 = V_1;
		return L_14;
	}

IL_0065:
	{
		Il2CppObject * L_15 = ___comparer1;
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_m_Item2_1();
		Tuple_4_t2865985893 * L_17 = V_0;
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_15);
		int32_t L_19 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_16, (Il2CppObject *)L_18);
		V_1 = (int32_t)L_19;
		int32_t L_20 = V_1;
		if (!L_20)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_21 = V_1;
		return L_21;
	}

IL_008a:
	{
		Il2CppObject * L_22 = ___comparer1;
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_m_Item3_2();
		Tuple_4_t2865985893 * L_24 = V_0;
		NullCheck(L_24);
		Il2CppObject * L_25 = (Il2CppObject *)L_24->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_22);
		int32_t L_26 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (Il2CppObject *)L_25);
		V_1 = (int32_t)L_26;
		int32_t L_27 = V_1;
		if (!L_27)
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_28 = V_1;
		return L_28;
	}

IL_00af:
	{
		Il2CppObject * L_29 = ___comparer1;
		Il2CppObject * L_30 = (Il2CppObject *)__this->get_m_Item4_3();
		Tuple_4_t2865985893 * L_31 = V_0;
		NullCheck(L_31);
		Il2CppObject * L_32 = (Il2CppObject *)L_31->get_m_Item4_3();
		NullCheck((Il2CppObject *)L_29);
		int32_t L_33 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, (Il2CppObject *)L_29, (Il2CppObject *)L_30, (Il2CppObject *)L_32);
		return L_33;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_4_GetHashCode_m308938224_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_GetHashCode_m308938224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EqualityComparer_1_t1263084566 * L_0 = EqualityComparer_1_get_Default_m1577971315(NULL /*static, unused*/, /*hidden argument*/EqualityComparer_1_get_Default_m1577971315_MethodInfo_var);
		NullCheck((Il2CppObject *)__this);
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IStructuralEquatable::GetHashCode(System.Collections.IEqualityComparer) */, IStructuralEquatable_t3751153838_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.Collections.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m2517367258_gshared (Tuple_4_t2865985893 * __this, Il2CppObject * ___comparer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_Collections_IStructuralEquatable_GetHashCode_m2517367258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___comparer0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		Il2CppObject * L_3 = ___comparer0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		Il2CppObject * L_6 = ___comparer0;
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_m_Item3_2();
		NullCheck((Il2CppObject *)L_6);
		int32_t L_8 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		Il2CppObject * L_9 = ___comparer0;
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_m_Item4_3();
		NullCheck((Il2CppObject *)L_9);
		int32_t L_11 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(1 /* System.Int32 System.Collections.IEqualityComparer::GetHashCode(System.Object) */, IEqualityComparer_t2716208158_il2cpp_TypeInfo_var, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		int32_t L_12 = Tuple_CombineHashCodes_m1865303580(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_5, (int32_t)L_8, (int32_t)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_4_ToString_m715281218_gshared (Tuple_4_t2865985893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_ToString_m715281218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((Il2CppObject *)__this);
		String_t* L_3 = InterfaceFuncInvoker1< String_t*, StringBuilder_t1221177846 * >::Invoke(0 /* System.String System.ITuple::ToString(System.Text.StringBuilder) */, ITuple_t843925179_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (StringBuilder_t1221177846 *)L_2);
		return L_3;
	}
}
// System.String System.Tuple`4<System.Object,System.Object,System.Object,System.Object>::System.ITuple.ToString(System.Text.StringBuilder)
extern "C"  String_t* Tuple_4_System_ITuple_ToString_m507032720_gshared (Tuple_4_t2865985893 * __this, StringBuilder_t1221177846 * ___sb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tuple_4_System_ITuple_ToString_m507032720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___sb0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_m_Item1_0();
		NullCheck((StringBuilder_t1221177846 *)L_0);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_2, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_3 = ___sb0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Item2_1();
		NullCheck((StringBuilder_t1221177846 *)L_3);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_5 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_5);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_5, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_6 = ___sb0;
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_m_Item3_2();
		NullCheck((StringBuilder_t1221177846 *)L_6);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_8 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_8);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_8, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_9 = ___sb0;
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_m_Item4_3();
		NullCheck((StringBuilder_t1221177846 *)L_9);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_11 = ___sb0;
		NullCheck((StringBuilder_t1221177846 *)L_11);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_11, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_12 = ___sb0;
		NullCheck((Il2CppObject *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_12);
		return L_13;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2563320212_gshared (CachedInvokableCall_1_t2619124609 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3247299909_gshared (CachedInvokableCall_1_t2619124609 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m127496184_gshared (CachedInvokableCall_1_t865427339 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m127496184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t266204305 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2815073919_gshared (CachedInvokableCall_1_t865427339 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t266204305 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m79259589_gshared (CachedInvokableCall_1_t1482999186 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m79259589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t883776152 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2401236944_gshared (CachedInvokableCall_1_t1482999186 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t883776152 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3238306320_gshared (CachedInvokableCall_1_t870059823 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t270836789 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4097553971_gshared (CachedInvokableCall_1_t870059823 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t270836789 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m874046876_gshared (InvokableCall_1_t2019901575 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m874046876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3048312905_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1481038152_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m769918017_gshared (InvokableCall_1_t2019901575 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m769918017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t897193173 * L_5 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t897193173 * L_7 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t897193173 *)L_7);
		((  void (*) (UnityAction_1_t897193173 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t897193173 *)L_7, (bool)((*(bool*)((bool*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m231935020_gshared (InvokableCall_1_t266204305 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m231935020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t266204305 *)__this, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3068046591_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3070410248_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m428957899_gshared (InvokableCall_1_t266204305 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m428957899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3438463199 * L_5 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3438463199 * L_7 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3438463199 *)L_7);
		((  void (*) (UnityAction_1_t3438463199 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3438463199 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m54675381_gshared (InvokableCall_1_t883776152 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m54675381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t883776152 *)__this, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m4009721884_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m527482931_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m1715547918_gshared (InvokableCall_1_t883776152 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1715547918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t4056035046 * L_5 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t4056035046 * L_7 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4056035046 *)L_7);
		((  void (*) (UnityAction_1_t4056035046 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t4056035046 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m4078762228_gshared (InvokableCall_1_t270836789 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m4078762228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t270836789 *)__this, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3251799843_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1744559252_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m4090512311_gshared (InvokableCall_1_t270836789 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4090512311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3443095683 * L_5 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3443095683 * L_7 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3443095683 *)L_7);
		((  void (*) (UnityAction_1_t3443095683 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3443095683 *)L_7, (float)((*(float*)((float*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m974169948_gshared (InvokableCall_2_t3799696166 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m974169948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3784905282 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m1071013389_gshared (InvokableCall_2_t3799696166 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1071013389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t3784905282 * L_8 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3784905282 * L_10 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3784905282 *)L_10);
		((  void (*) (UnityAction_2_t3784905282 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t3784905282 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_3__ctor_m3141607487_gshared (InvokableCall_3_t2191335654 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m3141607487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t3482433968 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_3_Invoke_m74557124_gshared (InvokableCall_3_t2191335654 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m74557124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t3482433968 * L_11 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t3482433968 * L_13 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t3482433968 *)L_13);
		((  void (*) (UnityAction_3_t3482433968 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t3482433968 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0060:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_4__ctor_m1096399974_gshared (InvokableCall_4_t2955480072 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m1096399974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t1666603240 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_4_Invoke_m1555001411_gshared (InvokableCall_4_t2955480072 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m1555001411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t1666603240 * L_14 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		UnityAction_4_t1666603240 * L_16 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t3614634134* L_23 = ___args0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t3614634134* L_26 = ___args0;
		NullCheck(L_26);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t1666603240 *)L_16);
		((  void (*) (UnityAction_4_t1666603240 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t1666603240 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0070:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2667547413_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3523417209_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2512011642_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3317901367_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m25541871_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563101999_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m530778538_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m530778538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1662218393_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2836997866_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1279804060_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3462722079_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2822290096_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3282722699_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563206587_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m4162767106_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3175338521_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2627946124_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3061904506_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2974933271_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3641222126_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m622153369_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1994351568_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m3203769083_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m4199296611_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2684626998_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1528820797_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2528278652_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(LoadSceneMode_t2981886439_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1593881300_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2892452633_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m670567184_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2733450299_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m234106915_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m3783439840_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m1498227613_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m160302482_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m1279075386_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2053485839_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m3312096275_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m3427746322_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m3887055469_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2948712401_gshared (UnityEvent_1_t2110227463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2948712401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m4083384818_gshared (UnityEvent_1_t2110227463 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3311025800_gshared (UnityEvent_1_t2110227463 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t266204305 * L_2 = (InvokableCall_1_t266204305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1903741765_gshared (UnityEvent_1_t2110227463 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2073978020_gshared (UnityEvent_1_t2727799310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2073978020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2223850067_gshared (UnityEvent_1_t2727799310 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m669290055_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t883776152 * L_2 = (InvokableCall_1_t883776152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m838874366_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		Il2CppObject * L_1 = ___arg00;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3717034779_gshared (UnityEvent_2_t1372135904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m3717034779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m2783251718_gshared (UnityEvent_2_t1372135904 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2_FindMethod_Impl_m2783251718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_4, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_6;
		goto IL_002e;
	}

IL_002e:
	{
		MethodInfo_t * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2147273130_gshared (UnityEvent_2_t1372135904 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_2_t3799696166 * L_2 = (InvokableCall_2_t3799696166 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (InvokableCall_2_t3799696166 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_3__ctor_m3502631330_gshared (UnityEvent_3_t3149477088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m3502631330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m1889846153_gshared (UnityEvent_3_t3149477088 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3_FindMethod_Impl_m1889846153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t1664964607* L_6 = (TypeU5BU5D_t1664964607*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		MethodInfo_t * L_8 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_6, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_8;
		goto IL_003b;
	}

IL_003b:
	{
		MethodInfo_t * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_3_GetDelegate_m338681277_gshared (UnityEvent_3_t3149477088 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_3_t2191335654 * L_2 = (InvokableCall_3_t2191335654 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_3_t2191335654 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_4__ctor_m3102731553_gshared (UnityEvent_4_t2935245934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m3102731553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_4_FindMethod_Impl_m4079512420_gshared (UnityEvent_4_t2935245934 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4_FindMethod_Impl_m4079512420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t1664964607* L_6 = (TypeU5BU5D_t1664964607*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		TypeU5BU5D_t1664964607* L_8 = (TypeU5BU5D_t1664964607*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_9);
		MethodInfo_t * L_10 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_8, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_10;
		goto IL_0048;
	}

IL_0048:
	{
		MethodInfo_t * L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_4_GetDelegate_m2704961864_gshared (UnityEvent_4_t2935245934 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_4_t2955480072 * L_2 = (InvokableCall_4_t2955480072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (InvokableCall_4_t2955480072 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
