﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t2919696197;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3260333366  : public Il2CppObject
{
public:
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1::predicate1
	Func_2_t2919696197 * ___predicate1_0;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/<CombinePredicates>c__AnonStorey1A`1::predicate2
	Func_2_t2919696197 * ___predicate2_1;

public:
	inline static int32_t get_offset_of_predicate1_0() { return static_cast<int32_t>(offsetof(U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3260333366, ___predicate1_0)); }
	inline Func_2_t2919696197 * get_predicate1_0() const { return ___predicate1_0; }
	inline Func_2_t2919696197 ** get_address_of_predicate1_0() { return &___predicate1_0; }
	inline void set_predicate1_0(Func_2_t2919696197 * value)
	{
		___predicate1_0 = value;
		Il2CppCodeGenWriteBarrier(&___predicate1_0, value);
	}

	inline static int32_t get_offset_of_predicate2_1() { return static_cast<int32_t>(offsetof(U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3260333366, ___predicate2_1)); }
	inline Func_2_t2919696197 * get_predicate2_1() const { return ___predicate2_1; }
	inline Func_2_t2919696197 ** get_address_of_predicate2_1() { return &___predicate2_1; }
	inline void set_predicate2_1(Func_2_t2919696197 * value)
	{
		___predicate2_1 = value;
		Il2CppCodeGenWriteBarrier(&___predicate2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
