﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"

// System.Func`2<System.Object,System.String>
struct Func_2_t2165275119;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.IO.TextReader
struct TextReader_t1561828458;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t1561828458  : public MarshalByRefObject_t1285298191
{
public:

public:
};

struct TextReader_t1561828458_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> System.IO.TextReader::_ReadLineDelegate
	Func_2_t2165275119 * ____ReadLineDelegate_1;
	// System.Func`2<System.Object,System.Int32> System.IO.TextReader::_ReadDelegate
	Func_2_t2207932334 * ____ReadDelegate_2;
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t1561828458 * ___Null_3;

public:
	inline static int32_t get_offset_of__ReadLineDelegate_1() { return static_cast<int32_t>(offsetof(TextReader_t1561828458_StaticFields, ____ReadLineDelegate_1)); }
	inline Func_2_t2165275119 * get__ReadLineDelegate_1() const { return ____ReadLineDelegate_1; }
	inline Func_2_t2165275119 ** get_address_of__ReadLineDelegate_1() { return &____ReadLineDelegate_1; }
	inline void set__ReadLineDelegate_1(Func_2_t2165275119 * value)
	{
		____ReadLineDelegate_1 = value;
		Il2CppCodeGenWriteBarrier(&____ReadLineDelegate_1, value);
	}

	inline static int32_t get_offset_of__ReadDelegate_2() { return static_cast<int32_t>(offsetof(TextReader_t1561828458_StaticFields, ____ReadDelegate_2)); }
	inline Func_2_t2207932334 * get__ReadDelegate_2() const { return ____ReadDelegate_2; }
	inline Func_2_t2207932334 ** get_address_of__ReadDelegate_2() { return &____ReadDelegate_2; }
	inline void set__ReadDelegate_2(Func_2_t2207932334 * value)
	{
		____ReadDelegate_2 = value;
		Il2CppCodeGenWriteBarrier(&____ReadDelegate_2, value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(TextReader_t1561828458_StaticFields, ___Null_3)); }
	inline TextReader_t1561828458 * get_Null_3() const { return ___Null_3; }
	inline TextReader_t1561828458 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(TextReader_t1561828458 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier(&___Null_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
