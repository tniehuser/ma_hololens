﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"

// System.Object
struct Il2CppObject;
// Microsoft.Win32.SafeHandles.SafeRegistryHandle
struct SafeRegistryHandle_t1955425892;
// System.String
struct String_t;
// Microsoft.Win32.IRegistryApi
struct IRegistryApi_t40222722;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.RegistryKey
struct  RegistryKey_t2287932784  : public MarshalByRefObject_t1285298191
{
public:
	// System.Object Microsoft.Win32.RegistryKey::handle
	Il2CppObject * ___handle_1;
	// Microsoft.Win32.SafeHandles.SafeRegistryHandle Microsoft.Win32.RegistryKey::safe_handle
	SafeRegistryHandle_t1955425892 * ___safe_handle_2;
	// System.Object Microsoft.Win32.RegistryKey::hive
	Il2CppObject * ___hive_3;
	// System.String Microsoft.Win32.RegistryKey::qname
	String_t* ___qname_4;
	// System.Boolean Microsoft.Win32.RegistryKey::isRemoteRoot
	bool ___isRemoteRoot_5;
	// System.Boolean Microsoft.Win32.RegistryKey::isWritable
	bool ___isWritable_6;

public:
	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___handle_1)); }
	inline Il2CppObject * get_handle_1() const { return ___handle_1; }
	inline Il2CppObject ** get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(Il2CppObject * value)
	{
		___handle_1 = value;
		Il2CppCodeGenWriteBarrier(&___handle_1, value);
	}

	inline static int32_t get_offset_of_safe_handle_2() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___safe_handle_2)); }
	inline SafeRegistryHandle_t1955425892 * get_safe_handle_2() const { return ___safe_handle_2; }
	inline SafeRegistryHandle_t1955425892 ** get_address_of_safe_handle_2() { return &___safe_handle_2; }
	inline void set_safe_handle_2(SafeRegistryHandle_t1955425892 * value)
	{
		___safe_handle_2 = value;
		Il2CppCodeGenWriteBarrier(&___safe_handle_2, value);
	}

	inline static int32_t get_offset_of_hive_3() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___hive_3)); }
	inline Il2CppObject * get_hive_3() const { return ___hive_3; }
	inline Il2CppObject ** get_address_of_hive_3() { return &___hive_3; }
	inline void set_hive_3(Il2CppObject * value)
	{
		___hive_3 = value;
		Il2CppCodeGenWriteBarrier(&___hive_3, value);
	}

	inline static int32_t get_offset_of_qname_4() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___qname_4)); }
	inline String_t* get_qname_4() const { return ___qname_4; }
	inline String_t** get_address_of_qname_4() { return &___qname_4; }
	inline void set_qname_4(String_t* value)
	{
		___qname_4 = value;
		Il2CppCodeGenWriteBarrier(&___qname_4, value);
	}

	inline static int32_t get_offset_of_isRemoteRoot_5() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___isRemoteRoot_5)); }
	inline bool get_isRemoteRoot_5() const { return ___isRemoteRoot_5; }
	inline bool* get_address_of_isRemoteRoot_5() { return &___isRemoteRoot_5; }
	inline void set_isRemoteRoot_5(bool value)
	{
		___isRemoteRoot_5 = value;
	}

	inline static int32_t get_offset_of_isWritable_6() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784, ___isWritable_6)); }
	inline bool get_isWritable_6() const { return ___isWritable_6; }
	inline bool* get_address_of_isWritable_6() { return &___isWritable_6; }
	inline void set_isWritable_6(bool value)
	{
		___isWritable_6 = value;
	}
};

struct RegistryKey_t2287932784_StaticFields
{
public:
	// Microsoft.Win32.IRegistryApi Microsoft.Win32.RegistryKey::RegistryApi
	Il2CppObject * ___RegistryApi_7;

public:
	inline static int32_t get_offset_of_RegistryApi_7() { return static_cast<int32_t>(offsetof(RegistryKey_t2287932784_StaticFields, ___RegistryApi_7)); }
	inline Il2CppObject * get_RegistryApi_7() const { return ___RegistryApi_7; }
	inline Il2CppObject ** get_address_of_RegistryApi_7() { return &___RegistryApi_7; }
	inline void set_RegistryApi_7(Il2CppObject * value)
	{
		___RegistryApi_7 = value;
		Il2CppCodeGenWriteBarrier(&___RegistryApi_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
