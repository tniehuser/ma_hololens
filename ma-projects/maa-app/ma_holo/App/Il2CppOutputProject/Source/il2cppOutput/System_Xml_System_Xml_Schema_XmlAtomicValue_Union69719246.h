﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_DateTime693205669.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue/Union
struct  Union_t69719246 
{
public:
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Boolean System.Xml.Schema.XmlAtomicValue/Union::boolVal
					bool ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					bool ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Double System.Xml.Schema.XmlAtomicValue/Union::dblVal
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Int64 System.Xml.Schema.XmlAtomicValue/Union::i64Val
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Int32 System.Xml.Schema.XmlAtomicValue/Union::i32Val
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					// System.DateTime System.Xml.Schema.XmlAtomicValue/Union::dtVal
					DateTime_t693205669  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t693205669  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t69719246__padding[8];
	};

public:
	inline static int32_t get_offset_of_boolVal_0() { return static_cast<int32_t>(offsetof(Union_t69719246, ___boolVal_0)); }
	inline bool get_boolVal_0() const { return ___boolVal_0; }
	inline bool* get_address_of_boolVal_0() { return &___boolVal_0; }
	inline void set_boolVal_0(bool value)
	{
		___boolVal_0 = value;
	}

	inline static int32_t get_offset_of_dblVal_1() { return static_cast<int32_t>(offsetof(Union_t69719246, ___dblVal_1)); }
	inline double get_dblVal_1() const { return ___dblVal_1; }
	inline double* get_address_of_dblVal_1() { return &___dblVal_1; }
	inline void set_dblVal_1(double value)
	{
		___dblVal_1 = value;
	}

	inline static int32_t get_offset_of_i64Val_2() { return static_cast<int32_t>(offsetof(Union_t69719246, ___i64Val_2)); }
	inline int64_t get_i64Val_2() const { return ___i64Val_2; }
	inline int64_t* get_address_of_i64Val_2() { return &___i64Val_2; }
	inline void set_i64Val_2(int64_t value)
	{
		___i64Val_2 = value;
	}

	inline static int32_t get_offset_of_i32Val_3() { return static_cast<int32_t>(offsetof(Union_t69719246, ___i32Val_3)); }
	inline int32_t get_i32Val_3() const { return ___i32Val_3; }
	inline int32_t* get_address_of_i32Val_3() { return &___i32Val_3; }
	inline void set_i32Val_3(int32_t value)
	{
		___i32Val_3 = value;
	}

	inline static int32_t get_offset_of_dtVal_4() { return static_cast<int32_t>(offsetof(Union_t69719246, ___dtVal_4)); }
	inline DateTime_t693205669  get_dtVal_4() const { return ___dtVal_4; }
	inline DateTime_t693205669 * get_address_of_dtVal_4() { return &___dtVal_4; }
	inline void set_dtVal_4(DateTime_t693205669  value)
	{
		___dtVal_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XmlAtomicValue/Union
struct Union_t69719246_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					DateTime_t693205669  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t693205669  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t69719246__padding[8];
	};
};
// Native definition for COM marshalling of System.Xml.Schema.XmlAtomicValue/Union
struct Union_t69719246_marshaled_com
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___boolVal_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___boolVal_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					double ___dblVal_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					double ___dblVal_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int64_t ___i64Val_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					int64_t ___i64Val_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___i32Val_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					int32_t ___i32Val_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					DateTime_t693205669  ___dtVal_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					DateTime_t693205669  ___dtVal_4_forAlignmentOnly;
				};
			};
		};
		uint8_t Union_t69719246__padding[8];
	};
};
