﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons1058613623.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKeyref
struct  XmlSchemaKeyref_t1894386400  : public XmlSchemaIdentityConstraint_t1058613623
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaKeyref::refer
	XmlQualifiedName_t1944712516 * ___refer_14;

public:
	inline static int32_t get_offset_of_refer_14() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_t1894386400, ___refer_14)); }
	inline XmlQualifiedName_t1944712516 * get_refer_14() const { return ___refer_14; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refer_14() { return &___refer_14; }
	inline void set_refer_14(XmlQualifiedName_t1944712516 * value)
	{
		___refer_14 = value;
		Il2CppCodeGenWriteBarrier(&___refer_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
