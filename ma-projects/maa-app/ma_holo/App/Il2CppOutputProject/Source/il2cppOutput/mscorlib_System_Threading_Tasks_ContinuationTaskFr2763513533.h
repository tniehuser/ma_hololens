﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task1843236107.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.ContinuationTaskFromTask
struct  ContinuationTaskFromTask_t2763513533  : public Task_t1843236107
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.ContinuationTaskFromTask::m_antecedent
	Task_t1843236107 * ___m_antecedent_24;

public:
	inline static int32_t get_offset_of_m_antecedent_24() { return static_cast<int32_t>(offsetof(ContinuationTaskFromTask_t2763513533, ___m_antecedent_24)); }
	inline Task_t1843236107 * get_m_antecedent_24() const { return ___m_antecedent_24; }
	inline Task_t1843236107 ** get_address_of_m_antecedent_24() { return &___m_antecedent_24; }
	inline void set_m_antecedent_24(Task_t1843236107 * value)
	{
		___m_antecedent_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_antecedent_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
