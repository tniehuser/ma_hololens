﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Collections.Generic.Dictionary`2<System.Type,System.AttributeUsageAttribute>
struct Dictionary_2_t2994793024;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1057435127;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoCustomAttrs
struct  MonoCustomAttrs_t2976585698  : public Il2CppObject
{
public:

public:
};

struct MonoCustomAttrs_t2976585698_StaticFields
{
public:
	// System.Reflection.Assembly System.MonoCustomAttrs::corlib
	Assembly_t4268412390 * ___corlib_0;
	// System.AttributeUsageAttribute System.MonoCustomAttrs::DefaultAttributeUsage
	AttributeUsageAttribute_t1057435127 * ___DefaultAttributeUsage_2;

public:
	inline static int32_t get_offset_of_corlib_0() { return static_cast<int32_t>(offsetof(MonoCustomAttrs_t2976585698_StaticFields, ___corlib_0)); }
	inline Assembly_t4268412390 * get_corlib_0() const { return ___corlib_0; }
	inline Assembly_t4268412390 ** get_address_of_corlib_0() { return &___corlib_0; }
	inline void set_corlib_0(Assembly_t4268412390 * value)
	{
		___corlib_0 = value;
		Il2CppCodeGenWriteBarrier(&___corlib_0, value);
	}

	inline static int32_t get_offset_of_DefaultAttributeUsage_2() { return static_cast<int32_t>(offsetof(MonoCustomAttrs_t2976585698_StaticFields, ___DefaultAttributeUsage_2)); }
	inline AttributeUsageAttribute_t1057435127 * get_DefaultAttributeUsage_2() const { return ___DefaultAttributeUsage_2; }
	inline AttributeUsageAttribute_t1057435127 ** get_address_of_DefaultAttributeUsage_2() { return &___DefaultAttributeUsage_2; }
	inline void set_DefaultAttributeUsage_2(AttributeUsageAttribute_t1057435127 * value)
	{
		___DefaultAttributeUsage_2 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultAttributeUsage_2, value);
	}
};

struct MonoCustomAttrs_t2976585698_ThreadStaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.AttributeUsageAttribute> System.MonoCustomAttrs::usage_cache
	Dictionary_2_t2994793024 * ___usage_cache_1;

public:
	inline static int32_t get_offset_of_usage_cache_1() { return static_cast<int32_t>(offsetof(MonoCustomAttrs_t2976585698_ThreadStaticFields, ___usage_cache_1)); }
	inline Dictionary_2_t2994793024 * get_usage_cache_1() const { return ___usage_cache_1; }
	inline Dictionary_2_t2994793024 ** get_address_of_usage_cache_1() { return &___usage_cache_1; }
	inline void set_usage_cache_1(Dictionary_2_t2994793024 * value)
	{
		___usage_cache_1 = value;
		Il2CppCodeGenWriteBarrier(&___usage_cache_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
