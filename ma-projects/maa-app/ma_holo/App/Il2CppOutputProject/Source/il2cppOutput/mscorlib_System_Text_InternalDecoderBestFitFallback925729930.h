﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_DecoderFallbackBuffer4206371382.h"

// System.Text.InternalDecoderBestFitFallback
struct InternalDecoderBestFitFallback_t1609214544;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.InternalDecoderBestFitFallbackBuffer
struct  InternalDecoderBestFitFallbackBuffer_t925729930  : public DecoderFallbackBuffer_t4206371382
{
public:
	// System.Char System.Text.InternalDecoderBestFitFallbackBuffer::cBestFit
	Il2CppChar ___cBestFit_2;
	// System.Int32 System.Text.InternalDecoderBestFitFallbackBuffer::iCount
	int32_t ___iCount_3;
	// System.Int32 System.Text.InternalDecoderBestFitFallbackBuffer::iSize
	int32_t ___iSize_4;
	// System.Text.InternalDecoderBestFitFallback System.Text.InternalDecoderBestFitFallbackBuffer::oFallback
	InternalDecoderBestFitFallback_t1609214544 * ___oFallback_5;

public:
	inline static int32_t get_offset_of_cBestFit_2() { return static_cast<int32_t>(offsetof(InternalDecoderBestFitFallbackBuffer_t925729930, ___cBestFit_2)); }
	inline Il2CppChar get_cBestFit_2() const { return ___cBestFit_2; }
	inline Il2CppChar* get_address_of_cBestFit_2() { return &___cBestFit_2; }
	inline void set_cBestFit_2(Il2CppChar value)
	{
		___cBestFit_2 = value;
	}

	inline static int32_t get_offset_of_iCount_3() { return static_cast<int32_t>(offsetof(InternalDecoderBestFitFallbackBuffer_t925729930, ___iCount_3)); }
	inline int32_t get_iCount_3() const { return ___iCount_3; }
	inline int32_t* get_address_of_iCount_3() { return &___iCount_3; }
	inline void set_iCount_3(int32_t value)
	{
		___iCount_3 = value;
	}

	inline static int32_t get_offset_of_iSize_4() { return static_cast<int32_t>(offsetof(InternalDecoderBestFitFallbackBuffer_t925729930, ___iSize_4)); }
	inline int32_t get_iSize_4() const { return ___iSize_4; }
	inline int32_t* get_address_of_iSize_4() { return &___iSize_4; }
	inline void set_iSize_4(int32_t value)
	{
		___iSize_4 = value;
	}

	inline static int32_t get_offset_of_oFallback_5() { return static_cast<int32_t>(offsetof(InternalDecoderBestFitFallbackBuffer_t925729930, ___oFallback_5)); }
	inline InternalDecoderBestFitFallback_t1609214544 * get_oFallback_5() const { return ___oFallback_5; }
	inline InternalDecoderBestFitFallback_t1609214544 ** get_address_of_oFallback_5() { return &___oFallback_5; }
	inline void set_oFallback_5(InternalDecoderBestFitFallback_t1609214544 * value)
	{
		___oFallback_5 = value;
		Il2CppCodeGenWriteBarrier(&___oFallback_5, value);
	}
};

struct InternalDecoderBestFitFallbackBuffer_t925729930_StaticFields
{
public:
	// System.Object System.Text.InternalDecoderBestFitFallbackBuffer::s_InternalSyncObject
	Il2CppObject * ___s_InternalSyncObject_6;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_6() { return static_cast<int32_t>(offsetof(InternalDecoderBestFitFallbackBuffer_t925729930_StaticFields, ___s_InternalSyncObject_6)); }
	inline Il2CppObject * get_s_InternalSyncObject_6() const { return ___s_InternalSyncObject_6; }
	inline Il2CppObject ** get_address_of_s_InternalSyncObject_6() { return &___s_InternalSyncObject_6; }
	inline void set_s_InternalSyncObject_6(Il2CppObject * value)
	{
		___s_InternalSyncObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_InternalSyncObject_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
