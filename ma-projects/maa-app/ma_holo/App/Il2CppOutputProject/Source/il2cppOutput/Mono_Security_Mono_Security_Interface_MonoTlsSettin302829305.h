﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen214512479.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t1929047274;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t4080334132;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Object
struct Il2CppObject;
// Mono.Security.Interface.CipherSuiteCode[]
struct CipherSuiteCodeU5BU5D_t4141109067;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t3067886638;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t302829305;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsSettings
struct  MonoTlsSettings_t302829305  : public Il2CppObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Security.Interface.MonoTlsSettings::<RemoteCertificateValidationCallback>k__BackingField
	MonoRemoteCertificateValidationCallback_t1929047274 * ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0;
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Security.Interface.MonoTlsSettings::<ClientCertificateSelectionCallback>k__BackingField
	MonoLocalCertificateSelectionCallback_t4080334132 * ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Interface.MonoTlsSettings::<TrustAnchors>k__BackingField
	X509CertificateCollection_t1197680765 * ___U3CTrustAnchorsU3Ek__BackingField_2;
	// System.Object Mono.Security.Interface.MonoTlsSettings::<UserSettings>k__BackingField
	Il2CppObject * ___U3CUserSettingsU3Ek__BackingField_3;
	// System.Nullable`1<Mono.Security.Interface.TlsProtocols> Mono.Security.Interface.MonoTlsSettings::<EnabledProtocols>k__BackingField
	Nullable_1_t214512479  ___U3CEnabledProtocolsU3Ek__BackingField_4;
	// Mono.Security.Interface.CipherSuiteCode[] Mono.Security.Interface.MonoTlsSettings::<EnabledCiphers>k__BackingField
	CipherSuiteCodeU5BU5D_t4141109067* ___U3CEnabledCiphersU3Ek__BackingField_5;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::cloned
	bool ___cloned_6;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertName
	bool ___checkCertName_7;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_8;
	// System.Nullable`1<System.Boolean> Mono.Security.Interface.MonoTlsSettings::useServicePointManagerCallback
	Nullable_1_t2088641033  ___useServicePointManagerCallback_9;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::skipSystemValidators
	bool ___skipSystemValidators_10;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::callbackNeedsChain
	bool ___callbackNeedsChain_11;
	// Mono.Security.Interface.ICertificateValidator Mono.Security.Interface.MonoTlsSettings::certificateValidator
	Il2CppObject * ___certificateValidator_12;

public:
	inline static int32_t get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0)); }
	inline MonoRemoteCertificateValidationCallback_t1929047274 * get_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() const { return ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline MonoRemoteCertificateValidationCallback_t1929047274 ** get_address_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return &___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline void set_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(MonoRemoteCertificateValidationCallback_t1929047274 * value)
	{
		___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1)); }
	inline MonoLocalCertificateSelectionCallback_t4080334132 * get_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() const { return ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline MonoLocalCertificateSelectionCallback_t4080334132 ** get_address_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return &___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline void set_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(MonoLocalCertificateSelectionCallback_t4080334132 * value)
	{
		___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTrustAnchorsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CTrustAnchorsU3Ek__BackingField_2)); }
	inline X509CertificateCollection_t1197680765 * get_U3CTrustAnchorsU3Ek__BackingField_2() const { return ___U3CTrustAnchorsU3Ek__BackingField_2; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_U3CTrustAnchorsU3Ek__BackingField_2() { return &___U3CTrustAnchorsU3Ek__BackingField_2; }
	inline void set_U3CTrustAnchorsU3Ek__BackingField_2(X509CertificateCollection_t1197680765 * value)
	{
		___U3CTrustAnchorsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTrustAnchorsU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUserSettingsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CUserSettingsU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CUserSettingsU3Ek__BackingField_3() const { return ___U3CUserSettingsU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CUserSettingsU3Ek__BackingField_3() { return &___U3CUserSettingsU3Ek__BackingField_3; }
	inline void set_U3CUserSettingsU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CUserSettingsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserSettingsU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CEnabledProtocolsU3Ek__BackingField_4)); }
	inline Nullable_1_t214512479  get_U3CEnabledProtocolsU3Ek__BackingField_4() const { return ___U3CEnabledProtocolsU3Ek__BackingField_4; }
	inline Nullable_1_t214512479 * get_address_of_U3CEnabledProtocolsU3Ek__BackingField_4() { return &___U3CEnabledProtocolsU3Ek__BackingField_4; }
	inline void set_U3CEnabledProtocolsU3Ek__BackingField_4(Nullable_1_t214512479  value)
	{
		___U3CEnabledProtocolsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledCiphersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___U3CEnabledCiphersU3Ek__BackingField_5)); }
	inline CipherSuiteCodeU5BU5D_t4141109067* get_U3CEnabledCiphersU3Ek__BackingField_5() const { return ___U3CEnabledCiphersU3Ek__BackingField_5; }
	inline CipherSuiteCodeU5BU5D_t4141109067** get_address_of_U3CEnabledCiphersU3Ek__BackingField_5() { return &___U3CEnabledCiphersU3Ek__BackingField_5; }
	inline void set_U3CEnabledCiphersU3Ek__BackingField_5(CipherSuiteCodeU5BU5D_t4141109067* value)
	{
		___U3CEnabledCiphersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEnabledCiphersU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_cloned_6() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___cloned_6)); }
	inline bool get_cloned_6() const { return ___cloned_6; }
	inline bool* get_address_of_cloned_6() { return &___cloned_6; }
	inline void set_cloned_6(bool value)
	{
		___cloned_6 = value;
	}

	inline static int32_t get_offset_of_checkCertName_7() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___checkCertName_7)); }
	inline bool get_checkCertName_7() const { return ___checkCertName_7; }
	inline bool* get_address_of_checkCertName_7() { return &___checkCertName_7; }
	inline void set_checkCertName_7(bool value)
	{
		___checkCertName_7 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_8() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___checkCertRevocationStatus_8)); }
	inline bool get_checkCertRevocationStatus_8() const { return ___checkCertRevocationStatus_8; }
	inline bool* get_address_of_checkCertRevocationStatus_8() { return &___checkCertRevocationStatus_8; }
	inline void set_checkCertRevocationStatus_8(bool value)
	{
		___checkCertRevocationStatus_8 = value;
	}

	inline static int32_t get_offset_of_useServicePointManagerCallback_9() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___useServicePointManagerCallback_9)); }
	inline Nullable_1_t2088641033  get_useServicePointManagerCallback_9() const { return ___useServicePointManagerCallback_9; }
	inline Nullable_1_t2088641033 * get_address_of_useServicePointManagerCallback_9() { return &___useServicePointManagerCallback_9; }
	inline void set_useServicePointManagerCallback_9(Nullable_1_t2088641033  value)
	{
		___useServicePointManagerCallback_9 = value;
	}

	inline static int32_t get_offset_of_skipSystemValidators_10() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___skipSystemValidators_10)); }
	inline bool get_skipSystemValidators_10() const { return ___skipSystemValidators_10; }
	inline bool* get_address_of_skipSystemValidators_10() { return &___skipSystemValidators_10; }
	inline void set_skipSystemValidators_10(bool value)
	{
		___skipSystemValidators_10 = value;
	}

	inline static int32_t get_offset_of_callbackNeedsChain_11() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___callbackNeedsChain_11)); }
	inline bool get_callbackNeedsChain_11() const { return ___callbackNeedsChain_11; }
	inline bool* get_address_of_callbackNeedsChain_11() { return &___callbackNeedsChain_11; }
	inline void set_callbackNeedsChain_11(bool value)
	{
		___callbackNeedsChain_11 = value;
	}

	inline static int32_t get_offset_of_certificateValidator_12() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305, ___certificateValidator_12)); }
	inline Il2CppObject * get_certificateValidator_12() const { return ___certificateValidator_12; }
	inline Il2CppObject ** get_address_of_certificateValidator_12() { return &___certificateValidator_12; }
	inline void set_certificateValidator_12(Il2CppObject * value)
	{
		___certificateValidator_12 = value;
		Il2CppCodeGenWriteBarrier(&___certificateValidator_12, value);
	}
};

struct MonoTlsSettings_t302829305_StaticFields
{
public:
	// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::defaultSettings
	MonoTlsSettings_t302829305 * ___defaultSettings_13;

public:
	inline static int32_t get_offset_of_defaultSettings_13() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t302829305_StaticFields, ___defaultSettings_13)); }
	inline MonoTlsSettings_t302829305 * get_defaultSettings_13() const { return ___defaultSettings_13; }
	inline MonoTlsSettings_t302829305 ** get_address_of_defaultSettings_13() { return &___defaultSettings_13; }
	inline void set_defaultSettings_13(MonoTlsSettings_t302829305 * value)
	{
		___defaultSettings_13 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSettings_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
