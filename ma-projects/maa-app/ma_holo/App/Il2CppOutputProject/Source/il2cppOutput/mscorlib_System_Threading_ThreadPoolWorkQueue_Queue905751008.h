﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"

// System.Threading.IThreadPoolWorkItem[]
struct IThreadPoolWorkItemU5BU5D_t2101682158;
// System.Threading.ThreadPoolWorkQueue/QueueSegment
struct QueueSegment_t905751008;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueue/QueueSegment
struct  QueueSegment_t905751008  : public Il2CppObject
{
public:
	// System.Threading.IThreadPoolWorkItem[] System.Threading.ThreadPoolWorkQueue/QueueSegment::nodes
	IThreadPoolWorkItemU5BU5D_t2101682158* ___nodes_0;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/QueueSegment::indexes
	int32_t ___indexes_1;
	// System.Threading.ThreadPoolWorkQueue/QueueSegment modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/QueueSegment::Next
	QueueSegment_t905751008 * ___Next_2;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(QueueSegment_t905751008, ___nodes_0)); }
	inline IThreadPoolWorkItemU5BU5D_t2101682158* get_nodes_0() const { return ___nodes_0; }
	inline IThreadPoolWorkItemU5BU5D_t2101682158** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(IThreadPoolWorkItemU5BU5D_t2101682158* value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_0, value);
	}

	inline static int32_t get_offset_of_indexes_1() { return static_cast<int32_t>(offsetof(QueueSegment_t905751008, ___indexes_1)); }
	inline int32_t get_indexes_1() const { return ___indexes_1; }
	inline int32_t* get_address_of_indexes_1() { return &___indexes_1; }
	inline void set_indexes_1(int32_t value)
	{
		___indexes_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(QueueSegment_t905751008, ___Next_2)); }
	inline QueueSegment_t905751008 * get_Next_2() const { return ___Next_2; }
	inline QueueSegment_t905751008 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(QueueSegment_t905751008 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier(&___Next_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
