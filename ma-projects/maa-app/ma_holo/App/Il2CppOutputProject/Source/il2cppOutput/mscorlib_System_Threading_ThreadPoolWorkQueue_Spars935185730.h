﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Object>
struct  SparseArray_1_t935185730  : public Il2CppObject
{
public:
	// T[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/SparseArray`1::m_array
	ObjectU5BU5D_t3614634134* ___m_array_0;

public:
	inline static int32_t get_offset_of_m_array_0() { return static_cast<int32_t>(offsetof(SparseArray_1_t935185730, ___m_array_0)); }
	inline ObjectU5BU5D_t3614634134* get_m_array_0() const { return ___m_array_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_array_0() { return &___m_array_0; }
	inline void set_m_array_0(ObjectU5BU5D_t3614634134* value)
	{
		___m_array_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_array_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
