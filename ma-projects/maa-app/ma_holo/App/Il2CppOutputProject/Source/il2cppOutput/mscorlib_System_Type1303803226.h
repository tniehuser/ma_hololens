﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"

// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.Binder
struct Binder_t3404612058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_7;

public:
	inline static int32_t get_offset_of__impl_7() { return static_cast<int32_t>(offsetof(Type_t, ____impl_7)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_7() const { return ____impl_7; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_7() { return &____impl_7; }
	inline void set__impl_7(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_7 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	Il2CppObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t3404612058 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier(&___FilterAttribute_0, value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t3405857066 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t3405857066 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier(&___FilterName_1, value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier(&___FilterNameIgnoreCase_2, value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline Il2CppObject * get_Missing_3() const { return ___Missing_3; }
	inline Il2CppObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(Il2CppObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier(&___Missing_3, value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyTypes_5, value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t3404612058 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t3404612058 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t3404612058 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier(&___defaultBinder_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
