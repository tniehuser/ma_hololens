﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Xml.Schema.XmlTypeCode[]
struct XmlTypeCodeU5BU5D_t2815001071;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime
struct  XsdDateTime_t2434467484 
{
public:
	// System.DateTime System.Xml.Schema.XsdDateTime::dt
	DateTime_t693205669  ___dt_0;
	// System.UInt32 System.Xml.Schema.XsdDateTime::extra
	uint32_t ___extra_1;

public:
	inline static int32_t get_offset_of_dt_0() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484, ___dt_0)); }
	inline DateTime_t693205669  get_dt_0() const { return ___dt_0; }
	inline DateTime_t693205669 * get_address_of_dt_0() { return &___dt_0; }
	inline void set_dt_0(DateTime_t693205669  value)
	{
		___dt_0 = value;
	}

	inline static int32_t get_offset_of_extra_1() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484, ___extra_1)); }
	inline uint32_t get_extra_1() const { return ___extra_1; }
	inline uint32_t* get_address_of_extra_1() { return &___extra_1; }
	inline void set_extra_1(uint32_t value)
	{
		___extra_1 = value;
	}
};

struct XsdDateTime_t2434467484_StaticFields
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy
	int32_t ___Lzyyyy_2;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_
	int32_t ___Lzyyyy__3;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_4;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__5;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_6;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_7;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH
	int32_t ___LzHH_8;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_
	int32_t ___LzHH__9;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm
	int32_t ___LzHH_mm_10;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm_
	int32_t ___LzHH_mm__11;
	// System.Int32 System.Xml.Schema.XsdDateTime::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_12;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_
	int32_t ___Lz__13;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz
	int32_t ___Lz_zz_14;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz_
	int32_t ___Lz_zz__15;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz_zz_zz
	int32_t ___Lz_zz_zz_16;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__
	int32_t ___Lz___17;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm
	int32_t ___Lz__mm_18;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm_
	int32_t ___Lz__mm__19;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm__
	int32_t ___Lz__mm___20;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz__mm_dd
	int32_t ___Lz__mm_dd_21;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz___
	int32_t ___Lz____22;
	// System.Int32 System.Xml.Schema.XsdDateTime::Lz___dd
	int32_t ___Lz___dd_23;
	// System.Xml.Schema.XmlTypeCode[] System.Xml.Schema.XsdDateTime::typeCodes
	XmlTypeCodeU5BU5D_t2815001071* ___typeCodes_24;

public:
	inline static int32_t get_offset_of_Lzyyyy_2() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy_2)); }
	inline int32_t get_Lzyyyy_2() const { return ___Lzyyyy_2; }
	inline int32_t* get_address_of_Lzyyyy_2() { return &___Lzyyyy_2; }
	inline void set_Lzyyyy_2(int32_t value)
	{
		___Lzyyyy_2 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__3() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy__3)); }
	inline int32_t get_Lzyyyy__3() const { return ___Lzyyyy__3; }
	inline int32_t* get_address_of_Lzyyyy__3() { return &___Lzyyyy__3; }
	inline void set_Lzyyyy__3(int32_t value)
	{
		___Lzyyyy__3 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_4() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy_MM_4)); }
	inline int32_t get_Lzyyyy_MM_4() const { return ___Lzyyyy_MM_4; }
	inline int32_t* get_address_of_Lzyyyy_MM_4() { return &___Lzyyyy_MM_4; }
	inline void set_Lzyyyy_MM_4(int32_t value)
	{
		___Lzyyyy_MM_4 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__5() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy_MM__5)); }
	inline int32_t get_Lzyyyy_MM__5() const { return ___Lzyyyy_MM__5; }
	inline int32_t* get_address_of_Lzyyyy_MM__5() { return &___Lzyyyy_MM__5; }
	inline void set_Lzyyyy_MM__5(int32_t value)
	{
		___Lzyyyy_MM__5 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_6() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy_MM_dd_6)); }
	inline int32_t get_Lzyyyy_MM_dd_6() const { return ___Lzyyyy_MM_dd_6; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_6() { return &___Lzyyyy_MM_dd_6; }
	inline void set_Lzyyyy_MM_dd_6(int32_t value)
	{
		___Lzyyyy_MM_dd_6 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_7() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lzyyyy_MM_ddT_7)); }
	inline int32_t get_Lzyyyy_MM_ddT_7() const { return ___Lzyyyy_MM_ddT_7; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_7() { return &___Lzyyyy_MM_ddT_7; }
	inline void set_Lzyyyy_MM_ddT_7(int32_t value)
	{
		___Lzyyyy_MM_ddT_7 = value;
	}

	inline static int32_t get_offset_of_LzHH_8() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___LzHH_8)); }
	inline int32_t get_LzHH_8() const { return ___LzHH_8; }
	inline int32_t* get_address_of_LzHH_8() { return &___LzHH_8; }
	inline void set_LzHH_8(int32_t value)
	{
		___LzHH_8 = value;
	}

	inline static int32_t get_offset_of_LzHH__9() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___LzHH__9)); }
	inline int32_t get_LzHH__9() const { return ___LzHH__9; }
	inline int32_t* get_address_of_LzHH__9() { return &___LzHH__9; }
	inline void set_LzHH__9(int32_t value)
	{
		___LzHH__9 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_10() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___LzHH_mm_10)); }
	inline int32_t get_LzHH_mm_10() const { return ___LzHH_mm_10; }
	inline int32_t* get_address_of_LzHH_mm_10() { return &___LzHH_mm_10; }
	inline void set_LzHH_mm_10(int32_t value)
	{
		___LzHH_mm_10 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__11() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___LzHH_mm__11)); }
	inline int32_t get_LzHH_mm__11() const { return ___LzHH_mm__11; }
	inline int32_t* get_address_of_LzHH_mm__11() { return &___LzHH_mm__11; }
	inline void set_LzHH_mm__11(int32_t value)
	{
		___LzHH_mm__11 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_12() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___LzHH_mm_ss_12)); }
	inline int32_t get_LzHH_mm_ss_12() const { return ___LzHH_mm_ss_12; }
	inline int32_t* get_address_of_LzHH_mm_ss_12() { return &___LzHH_mm_ss_12; }
	inline void set_LzHH_mm_ss_12(int32_t value)
	{
		___LzHH_mm_ss_12 = value;
	}

	inline static int32_t get_offset_of_Lz__13() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz__13)); }
	inline int32_t get_Lz__13() const { return ___Lz__13; }
	inline int32_t* get_address_of_Lz__13() { return &___Lz__13; }
	inline void set_Lz__13(int32_t value)
	{
		___Lz__13 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_14() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz_zz_14)); }
	inline int32_t get_Lz_zz_14() const { return ___Lz_zz_14; }
	inline int32_t* get_address_of_Lz_zz_14() { return &___Lz_zz_14; }
	inline void set_Lz_zz_14(int32_t value)
	{
		___Lz_zz_14 = value;
	}

	inline static int32_t get_offset_of_Lz_zz__15() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz_zz__15)); }
	inline int32_t get_Lz_zz__15() const { return ___Lz_zz__15; }
	inline int32_t* get_address_of_Lz_zz__15() { return &___Lz_zz__15; }
	inline void set_Lz_zz__15(int32_t value)
	{
		___Lz_zz__15 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_zz_16() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz_zz_zz_16)); }
	inline int32_t get_Lz_zz_zz_16() const { return ___Lz_zz_zz_16; }
	inline int32_t* get_address_of_Lz_zz_zz_16() { return &___Lz_zz_zz_16; }
	inline void set_Lz_zz_zz_16(int32_t value)
	{
		___Lz_zz_zz_16 = value;
	}

	inline static int32_t get_offset_of_Lz___17() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz___17)); }
	inline int32_t get_Lz___17() const { return ___Lz___17; }
	inline int32_t* get_address_of_Lz___17() { return &___Lz___17; }
	inline void set_Lz___17(int32_t value)
	{
		___Lz___17 = value;
	}

	inline static int32_t get_offset_of_Lz__mm_18() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz__mm_18)); }
	inline int32_t get_Lz__mm_18() const { return ___Lz__mm_18; }
	inline int32_t* get_address_of_Lz__mm_18() { return &___Lz__mm_18; }
	inline void set_Lz__mm_18(int32_t value)
	{
		___Lz__mm_18 = value;
	}

	inline static int32_t get_offset_of_Lz__mm__19() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz__mm__19)); }
	inline int32_t get_Lz__mm__19() const { return ___Lz__mm__19; }
	inline int32_t* get_address_of_Lz__mm__19() { return &___Lz__mm__19; }
	inline void set_Lz__mm__19(int32_t value)
	{
		___Lz__mm__19 = value;
	}

	inline static int32_t get_offset_of_Lz__mm___20() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz__mm___20)); }
	inline int32_t get_Lz__mm___20() const { return ___Lz__mm___20; }
	inline int32_t* get_address_of_Lz__mm___20() { return &___Lz__mm___20; }
	inline void set_Lz__mm___20(int32_t value)
	{
		___Lz__mm___20 = value;
	}

	inline static int32_t get_offset_of_Lz__mm_dd_21() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz__mm_dd_21)); }
	inline int32_t get_Lz__mm_dd_21() const { return ___Lz__mm_dd_21; }
	inline int32_t* get_address_of_Lz__mm_dd_21() { return &___Lz__mm_dd_21; }
	inline void set_Lz__mm_dd_21(int32_t value)
	{
		___Lz__mm_dd_21 = value;
	}

	inline static int32_t get_offset_of_Lz____22() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz____22)); }
	inline int32_t get_Lz____22() const { return ___Lz____22; }
	inline int32_t* get_address_of_Lz____22() { return &___Lz____22; }
	inline void set_Lz____22(int32_t value)
	{
		___Lz____22 = value;
	}

	inline static int32_t get_offset_of_Lz___dd_23() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___Lz___dd_23)); }
	inline int32_t get_Lz___dd_23() const { return ___Lz___dd_23; }
	inline int32_t* get_address_of_Lz___dd_23() { return &___Lz___dd_23; }
	inline void set_Lz___dd_23(int32_t value)
	{
		___Lz___dd_23 = value;
	}

	inline static int32_t get_offset_of_typeCodes_24() { return static_cast<int32_t>(offsetof(XsdDateTime_t2434467484_StaticFields, ___typeCodes_24)); }
	inline XmlTypeCodeU5BU5D_t2815001071* get_typeCodes_24() const { return ___typeCodes_24; }
	inline XmlTypeCodeU5BU5D_t2815001071** get_address_of_typeCodes_24() { return &___typeCodes_24; }
	inline void set_typeCodes_24(XmlTypeCodeU5BU5D_t2815001071* value)
	{
		___typeCodes_24 = value;
		Il2CppCodeGenWriteBarrier(&___typeCodes_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XsdDateTime
struct XsdDateTime_t2434467484_marshaled_pinvoke
{
	DateTime_t693205669  ___dt_0;
	uint32_t ___extra_1;
};
// Native definition for COM marshalling of System.Xml.Schema.XsdDateTime
struct XsdDateTime_t2434467484_marshaled_com
{
	DateTime_t693205669  ___dt_0;
	uint32_t ___extra_1;
};
