﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"

// Mono.Net.Security.IMonoTlsProvider
struct IMonoTlsProvider_t2506971578;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t581172200;
// Mono.Net.Security.IMonoSslStream
struct IMonoSslStream_t424679660;
// Mono.Net.Security.ChainValidationHelper
struct ChainValidationHelper_t3893280544;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t302829305;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsStream
struct  MonoTlsStream_t1249652258  : public Il2CppObject
{
public:
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsStream::provider
	Il2CppObject * ___provider_0;
	// System.Net.HttpWebRequest Mono.Net.Security.MonoTlsStream::request
	HttpWebRequest_t1951404513 * ___request_1;
	// System.Net.Sockets.NetworkStream Mono.Net.Security.MonoTlsStream::networkStream
	NetworkStream_t581172200 * ___networkStream_2;
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.MonoTlsStream::sslStream
	Il2CppObject * ___sslStream_3;
	// System.Net.WebExceptionStatus Mono.Net.Security.MonoTlsStream::status
	int32_t ___status_4;
	// System.Boolean Mono.Net.Security.MonoTlsStream::<CertificateValidationFailed>k__BackingField
	bool ___U3CCertificateValidationFailedU3Ek__BackingField_5;
	// Mono.Net.Security.ChainValidationHelper Mono.Net.Security.MonoTlsStream::validationHelper
	ChainValidationHelper_t3893280544 * ___validationHelper_6;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MonoTlsStream::settings
	MonoTlsSettings_t302829305 * ___settings_7;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___provider_0)); }
	inline Il2CppObject * get_provider_0() const { return ___provider_0; }
	inline Il2CppObject ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(Il2CppObject * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier(&___provider_0, value);
	}

	inline static int32_t get_offset_of_request_1() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___request_1)); }
	inline HttpWebRequest_t1951404513 * get_request_1() const { return ___request_1; }
	inline HttpWebRequest_t1951404513 ** get_address_of_request_1() { return &___request_1; }
	inline void set_request_1(HttpWebRequest_t1951404513 * value)
	{
		___request_1 = value;
		Il2CppCodeGenWriteBarrier(&___request_1, value);
	}

	inline static int32_t get_offset_of_networkStream_2() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___networkStream_2)); }
	inline NetworkStream_t581172200 * get_networkStream_2() const { return ___networkStream_2; }
	inline NetworkStream_t581172200 ** get_address_of_networkStream_2() { return &___networkStream_2; }
	inline void set_networkStream_2(NetworkStream_t581172200 * value)
	{
		___networkStream_2 = value;
		Il2CppCodeGenWriteBarrier(&___networkStream_2, value);
	}

	inline static int32_t get_offset_of_sslStream_3() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___sslStream_3)); }
	inline Il2CppObject * get_sslStream_3() const { return ___sslStream_3; }
	inline Il2CppObject ** get_address_of_sslStream_3() { return &___sslStream_3; }
	inline void set_sslStream_3(Il2CppObject * value)
	{
		___sslStream_3 = value;
		Il2CppCodeGenWriteBarrier(&___sslStream_3, value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___U3CCertificateValidationFailedU3Ek__BackingField_5)); }
	inline bool get_U3CCertificateValidationFailedU3Ek__BackingField_5() const { return ___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return &___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline void set_U3CCertificateValidationFailedU3Ek__BackingField_5(bool value)
	{
		___U3CCertificateValidationFailedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_validationHelper_6() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___validationHelper_6)); }
	inline ChainValidationHelper_t3893280544 * get_validationHelper_6() const { return ___validationHelper_6; }
	inline ChainValidationHelper_t3893280544 ** get_address_of_validationHelper_6() { return &___validationHelper_6; }
	inline void set_validationHelper_6(ChainValidationHelper_t3893280544 * value)
	{
		___validationHelper_6 = value;
		Il2CppCodeGenWriteBarrier(&___validationHelper_6, value);
	}

	inline static int32_t get_offset_of_settings_7() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___settings_7)); }
	inline MonoTlsSettings_t302829305 * get_settings_7() const { return ___settings_7; }
	inline MonoTlsSettings_t302829305 ** get_address_of_settings_7() { return &___settings_7; }
	inline void set_settings_7(MonoTlsSettings_t302829305 * value)
	{
		___settings_7 = value;
		Il2CppCodeGenWriteBarrier(&___settings_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
