﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.PathList/PathListComparer
struct PathListComparer_t2346409738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList/PathListComparer
struct  PathListComparer_t2346409738  : public Il2CppObject
{
public:

public:
};

struct PathListComparer_t2346409738_StaticFields
{
public:
	// System.Net.PathList/PathListComparer System.Net.PathList/PathListComparer::StaticInstance
	PathListComparer_t2346409738 * ___StaticInstance_0;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(PathListComparer_t2346409738_StaticFields, ___StaticInstance_0)); }
	inline PathListComparer_t2346409738 * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline PathListComparer_t2346409738 ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(PathListComparer_t2346409738 * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___StaticInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
