﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.TimeZoneInfo/AdjustmentRule
struct AdjustmentRule_t2179708818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/OffsetAndRule
struct  OffsetAndRule_t2067953655  : public Il2CppObject
{
public:
	// System.Int32 System.TimeZoneInfo/OffsetAndRule::year
	int32_t ___year_0;
	// System.TimeSpan System.TimeZoneInfo/OffsetAndRule::offset
	TimeSpan_t3430258949  ___offset_1;
	// System.TimeZoneInfo/AdjustmentRule System.TimeZoneInfo/OffsetAndRule::rule
	AdjustmentRule_t2179708818 * ___rule_2;

public:
	inline static int32_t get_offset_of_year_0() { return static_cast<int32_t>(offsetof(OffsetAndRule_t2067953655, ___year_0)); }
	inline int32_t get_year_0() const { return ___year_0; }
	inline int32_t* get_address_of_year_0() { return &___year_0; }
	inline void set_year_0(int32_t value)
	{
		___year_0 = value;
	}

	inline static int32_t get_offset_of_offset_1() { return static_cast<int32_t>(offsetof(OffsetAndRule_t2067953655, ___offset_1)); }
	inline TimeSpan_t3430258949  get_offset_1() const { return ___offset_1; }
	inline TimeSpan_t3430258949 * get_address_of_offset_1() { return &___offset_1; }
	inline void set_offset_1(TimeSpan_t3430258949  value)
	{
		___offset_1 = value;
	}

	inline static int32_t get_offset_of_rule_2() { return static_cast<int32_t>(offsetof(OffsetAndRule_t2067953655, ___rule_2)); }
	inline AdjustmentRule_t2179708818 * get_rule_2() const { return ___rule_2; }
	inline AdjustmentRule_t2179708818 ** get_address_of_rule_2() { return &___rule_2; }
	inline void set_rule_2(AdjustmentRule_t2179708818 * value)
	{
		___rule_2 = value;
		Il2CppCodeGenWriteBarrier(&___rule_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
