﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.Tasks.ITaskCompletionAction
struct ITaskCompletionAction_t2098045752;
// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.CompletionActionInvoker
struct  CompletionActionInvoker_t1583323542  : public Il2CppObject
{
public:
	// System.Threading.Tasks.ITaskCompletionAction System.Threading.Tasks.CompletionActionInvoker::m_action
	Il2CppObject * ___m_action_0;
	// System.Threading.Tasks.Task System.Threading.Tasks.CompletionActionInvoker::m_completingTask
	Task_t1843236107 * ___m_completingTask_1;

public:
	inline static int32_t get_offset_of_m_action_0() { return static_cast<int32_t>(offsetof(CompletionActionInvoker_t1583323542, ___m_action_0)); }
	inline Il2CppObject * get_m_action_0() const { return ___m_action_0; }
	inline Il2CppObject ** get_address_of_m_action_0() { return &___m_action_0; }
	inline void set_m_action_0(Il2CppObject * value)
	{
		___m_action_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_action_0, value);
	}

	inline static int32_t get_offset_of_m_completingTask_1() { return static_cast<int32_t>(offsetof(CompletionActionInvoker_t1583323542, ___m_completingTask_1)); }
	inline Task_t1843236107 * get_m_completingTask_1() const { return ___m_completingTask_1; }
	inline Task_t1843236107 ** get_address_of_m_completingTask_1() { return &___m_completingTask_1; }
	inline void set_m_completingTask_1(Task_t1843236107 * value)
	{
		___m_completingTask_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_completingTask_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
