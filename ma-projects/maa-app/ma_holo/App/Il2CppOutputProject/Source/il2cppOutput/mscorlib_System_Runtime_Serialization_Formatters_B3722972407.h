﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Concurrent.ConcurrentDictionary`2<System.String,System.Object>
struct ConcurrentDictionary_2_t2171449296;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.NameCache
struct  NameCache_t3722972407  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.NameCache::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(NameCache_t3722972407, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}
};

struct NameCache_t3722972407_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentDictionary`2<System.String,System.Object> System.Runtime.Serialization.Formatters.Binary.NameCache::ht
	ConcurrentDictionary_2_t2171449296 * ___ht_0;

public:
	inline static int32_t get_offset_of_ht_0() { return static_cast<int32_t>(offsetof(NameCache_t3722972407_StaticFields, ___ht_0)); }
	inline ConcurrentDictionary_2_t2171449296 * get_ht_0() const { return ___ht_0; }
	inline ConcurrentDictionary_2_t2171449296 ** get_address_of_ht_0() { return &___ht_0; }
	inline void set_ht_0(ConcurrentDictionary_2_t2171449296 * value)
	{
		___ht_0 = value;
		Il2CppCodeGenWriteBarrier(&___ht_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
