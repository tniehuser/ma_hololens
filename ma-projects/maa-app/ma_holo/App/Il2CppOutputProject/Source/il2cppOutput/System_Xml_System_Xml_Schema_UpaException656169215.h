﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Exception1927440687.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UpaException
struct  UpaException_t656169215  : public Exception_t1927440687
{
public:
	// System.Object System.Xml.Schema.UpaException::particle1
	Il2CppObject * ___particle1_16;
	// System.Object System.Xml.Schema.UpaException::particle2
	Il2CppObject * ___particle2_17;

public:
	inline static int32_t get_offset_of_particle1_16() { return static_cast<int32_t>(offsetof(UpaException_t656169215, ___particle1_16)); }
	inline Il2CppObject * get_particle1_16() const { return ___particle1_16; }
	inline Il2CppObject ** get_address_of_particle1_16() { return &___particle1_16; }
	inline void set_particle1_16(Il2CppObject * value)
	{
		___particle1_16 = value;
		Il2CppCodeGenWriteBarrier(&___particle1_16, value);
	}

	inline static int32_t get_offset_of_particle2_17() { return static_cast<int32_t>(offsetof(UpaException_t656169215, ___particle2_17)); }
	inline Il2CppObject * get_particle2_17() const { return ___particle2_17; }
	inline Il2CppObject ** get_address_of_particle2_17() { return &___particle2_17; }
	inline void set_particle2_17(Il2CppObject * value)
	{
		___particle2_17 = value;
		Il2CppCodeGenWriteBarrier(&___particle2_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
