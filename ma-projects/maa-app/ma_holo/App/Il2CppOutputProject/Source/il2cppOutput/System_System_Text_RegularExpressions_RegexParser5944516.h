﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"

// System.Text.RegularExpressions.RegexNode
struct RegexNode_t2469392321;
// System.String
struct String_t;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions>
struct List_1_t1787380859;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexParser
struct  RegexParser_t5944516  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_stack
	RegexNode_t2469392321 * ____stack_0;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_group
	RegexNode_t2469392321 * ____group_1;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_alternation
	RegexNode_t2469392321 * ____alternation_2;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_concatenation
	RegexNode_t2469392321 * ____concatenation_3;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_unit
	RegexNode_t2469392321 * ____unit_4;
	// System.String System.Text.RegularExpressions.RegexParser::_pattern
	String_t* ____pattern_5;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_currentPos
	int32_t ____currentPos_6;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexParser::_culture
	CultureInfo_t3500843524 * ____culture_7;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_autocap
	int32_t ____autocap_8;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capcount
	int32_t ____capcount_9;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_captop
	int32_t ____captop_10;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capsize
	int32_t ____capsize_11;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_caps
	Hashtable_t909839986 * ____caps_12;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_capnames
	Hashtable_t909839986 * ____capnames_13;
	// System.Int32[] System.Text.RegularExpressions.RegexParser::_capnumlist
	Int32U5BU5D_t3030399641* ____capnumlist_14;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexParser::_capnamelist
	List_1_t1398341365 * ____capnamelist_15;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexParser::_options
	int32_t ____options_16;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions> System.Text.RegularExpressions.RegexParser::_optionsStack
	List_1_t1787380859 * ____optionsStack_17;
	// System.Boolean System.Text.RegularExpressions.RegexParser::_ignoreNextParen
	bool ____ignoreNextParen_18;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____stack_0)); }
	inline RegexNode_t2469392321 * get__stack_0() const { return ____stack_0; }
	inline RegexNode_t2469392321 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(RegexNode_t2469392321 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier(&____stack_0, value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____group_1)); }
	inline RegexNode_t2469392321 * get__group_1() const { return ____group_1; }
	inline RegexNode_t2469392321 ** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RegexNode_t2469392321 * value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier(&____group_1, value);
	}

	inline static int32_t get_offset_of__alternation_2() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____alternation_2)); }
	inline RegexNode_t2469392321 * get__alternation_2() const { return ____alternation_2; }
	inline RegexNode_t2469392321 ** get_address_of__alternation_2() { return &____alternation_2; }
	inline void set__alternation_2(RegexNode_t2469392321 * value)
	{
		____alternation_2 = value;
		Il2CppCodeGenWriteBarrier(&____alternation_2, value);
	}

	inline static int32_t get_offset_of__concatenation_3() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____concatenation_3)); }
	inline RegexNode_t2469392321 * get__concatenation_3() const { return ____concatenation_3; }
	inline RegexNode_t2469392321 ** get_address_of__concatenation_3() { return &____concatenation_3; }
	inline void set__concatenation_3(RegexNode_t2469392321 * value)
	{
		____concatenation_3 = value;
		Il2CppCodeGenWriteBarrier(&____concatenation_3, value);
	}

	inline static int32_t get_offset_of__unit_4() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____unit_4)); }
	inline RegexNode_t2469392321 * get__unit_4() const { return ____unit_4; }
	inline RegexNode_t2469392321 ** get_address_of__unit_4() { return &____unit_4; }
	inline void set__unit_4(RegexNode_t2469392321 * value)
	{
		____unit_4 = value;
		Il2CppCodeGenWriteBarrier(&____unit_4, value);
	}

	inline static int32_t get_offset_of__pattern_5() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____pattern_5)); }
	inline String_t* get__pattern_5() const { return ____pattern_5; }
	inline String_t** get_address_of__pattern_5() { return &____pattern_5; }
	inline void set__pattern_5(String_t* value)
	{
		____pattern_5 = value;
		Il2CppCodeGenWriteBarrier(&____pattern_5, value);
	}

	inline static int32_t get_offset_of__currentPos_6() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____currentPos_6)); }
	inline int32_t get__currentPos_6() const { return ____currentPos_6; }
	inline int32_t* get_address_of__currentPos_6() { return &____currentPos_6; }
	inline void set__currentPos_6(int32_t value)
	{
		____currentPos_6 = value;
	}

	inline static int32_t get_offset_of__culture_7() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____culture_7)); }
	inline CultureInfo_t3500843524 * get__culture_7() const { return ____culture_7; }
	inline CultureInfo_t3500843524 ** get_address_of__culture_7() { return &____culture_7; }
	inline void set__culture_7(CultureInfo_t3500843524 * value)
	{
		____culture_7 = value;
		Il2CppCodeGenWriteBarrier(&____culture_7, value);
	}

	inline static int32_t get_offset_of__autocap_8() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____autocap_8)); }
	inline int32_t get__autocap_8() const { return ____autocap_8; }
	inline int32_t* get_address_of__autocap_8() { return &____autocap_8; }
	inline void set__autocap_8(int32_t value)
	{
		____autocap_8 = value;
	}

	inline static int32_t get_offset_of__capcount_9() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capcount_9)); }
	inline int32_t get__capcount_9() const { return ____capcount_9; }
	inline int32_t* get_address_of__capcount_9() { return &____capcount_9; }
	inline void set__capcount_9(int32_t value)
	{
		____capcount_9 = value;
	}

	inline static int32_t get_offset_of__captop_10() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____captop_10)); }
	inline int32_t get__captop_10() const { return ____captop_10; }
	inline int32_t* get_address_of__captop_10() { return &____captop_10; }
	inline void set__captop_10(int32_t value)
	{
		____captop_10 = value;
	}

	inline static int32_t get_offset_of__capsize_11() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capsize_11)); }
	inline int32_t get__capsize_11() const { return ____capsize_11; }
	inline int32_t* get_address_of__capsize_11() { return &____capsize_11; }
	inline void set__capsize_11(int32_t value)
	{
		____capsize_11 = value;
	}

	inline static int32_t get_offset_of__caps_12() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____caps_12)); }
	inline Hashtable_t909839986 * get__caps_12() const { return ____caps_12; }
	inline Hashtable_t909839986 ** get_address_of__caps_12() { return &____caps_12; }
	inline void set__caps_12(Hashtable_t909839986 * value)
	{
		____caps_12 = value;
		Il2CppCodeGenWriteBarrier(&____caps_12, value);
	}

	inline static int32_t get_offset_of__capnames_13() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnames_13)); }
	inline Hashtable_t909839986 * get__capnames_13() const { return ____capnames_13; }
	inline Hashtable_t909839986 ** get_address_of__capnames_13() { return &____capnames_13; }
	inline void set__capnames_13(Hashtable_t909839986 * value)
	{
		____capnames_13 = value;
		Il2CppCodeGenWriteBarrier(&____capnames_13, value);
	}

	inline static int32_t get_offset_of__capnumlist_14() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnumlist_14)); }
	inline Int32U5BU5D_t3030399641* get__capnumlist_14() const { return ____capnumlist_14; }
	inline Int32U5BU5D_t3030399641** get_address_of__capnumlist_14() { return &____capnumlist_14; }
	inline void set__capnumlist_14(Int32U5BU5D_t3030399641* value)
	{
		____capnumlist_14 = value;
		Il2CppCodeGenWriteBarrier(&____capnumlist_14, value);
	}

	inline static int32_t get_offset_of__capnamelist_15() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____capnamelist_15)); }
	inline List_1_t1398341365 * get__capnamelist_15() const { return ____capnamelist_15; }
	inline List_1_t1398341365 ** get_address_of__capnamelist_15() { return &____capnamelist_15; }
	inline void set__capnamelist_15(List_1_t1398341365 * value)
	{
		____capnamelist_15 = value;
		Il2CppCodeGenWriteBarrier(&____capnamelist_15, value);
	}

	inline static int32_t get_offset_of__options_16() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____options_16)); }
	inline int32_t get__options_16() const { return ____options_16; }
	inline int32_t* get_address_of__options_16() { return &____options_16; }
	inline void set__options_16(int32_t value)
	{
		____options_16 = value;
	}

	inline static int32_t get_offset_of__optionsStack_17() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____optionsStack_17)); }
	inline List_1_t1787380859 * get__optionsStack_17() const { return ____optionsStack_17; }
	inline List_1_t1787380859 ** get_address_of__optionsStack_17() { return &____optionsStack_17; }
	inline void set__optionsStack_17(List_1_t1787380859 * value)
	{
		____optionsStack_17 = value;
		Il2CppCodeGenWriteBarrier(&____optionsStack_17, value);
	}

	inline static int32_t get_offset_of__ignoreNextParen_18() { return static_cast<int32_t>(offsetof(RegexParser_t5944516, ____ignoreNextParen_18)); }
	inline bool get__ignoreNextParen_18() const { return ____ignoreNextParen_18; }
	inline bool* get_address_of__ignoreNextParen_18() { return &____ignoreNextParen_18; }
	inline void set__ignoreNextParen_18(bool value)
	{
		____ignoreNextParen_18 = value;
	}
};

struct RegexParser_t5944516_StaticFields
{
public:
	// System.Byte[] System.Text.RegularExpressions.RegexParser::_category
	ByteU5BU5D_t3397334013* ____category_19;

public:
	inline static int32_t get_offset_of__category_19() { return static_cast<int32_t>(offsetof(RegexParser_t5944516_StaticFields, ____category_19)); }
	inline ByteU5BU5D_t3397334013* get__category_19() const { return ____category_19; }
	inline ByteU5BU5D_t3397334013** get_address_of__category_19() { return &____category_19; }
	inline void set__category_19(ByteU5BU5D_t3397334013* value)
	{
		____category_19 = value;
		Il2CppCodeGenWriteBarrier(&____category_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
