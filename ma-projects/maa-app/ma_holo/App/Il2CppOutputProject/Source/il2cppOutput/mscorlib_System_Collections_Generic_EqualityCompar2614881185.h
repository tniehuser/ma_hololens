﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.EqualityComparer`1<System.Int16>
struct EqualityComparer_1_t2614881185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Int16>
struct  EqualityComparer_1_t2614881185  : public Il2CppObject
{
public:

public:
};

struct EqualityComparer_1_t2614881185_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Generic.EqualityComparer`1::defaultComparer
	EqualityComparer_1_t2614881185 * ___defaultComparer_0;

public:
	inline static int32_t get_offset_of_defaultComparer_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2614881185_StaticFields, ___defaultComparer_0)); }
	inline EqualityComparer_1_t2614881185 * get_defaultComparer_0() const { return ___defaultComparer_0; }
	inline EqualityComparer_1_t2614881185 ** get_address_of_defaultComparer_0() { return &___defaultComparer_0; }
	inline void set_defaultComparer_0(EqualityComparer_1_t2614881185 * value)
	{
		___defaultComparer_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultComparer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
