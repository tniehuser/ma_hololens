﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_Win32Exception1708275760.h"

// System.Net.EndPoint
struct EndPoint_t4156119363;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketException
struct  SocketException_t1618573604  : public Win32Exception_t1708275760
{
public:
	// System.Net.EndPoint System.Net.Sockets.SocketException::m_EndPoint
	EndPoint_t4156119363 * ___m_EndPoint_17;

public:
	inline static int32_t get_offset_of_m_EndPoint_17() { return static_cast<int32_t>(offsetof(SocketException_t1618573604, ___m_EndPoint_17)); }
	inline EndPoint_t4156119363 * get_m_EndPoint_17() const { return ___m_EndPoint_17; }
	inline EndPoint_t4156119363 ** get_address_of_m_EndPoint_17() { return &___m_EndPoint_17; }
	inline void set_m_EndPoint_17(EndPoint_t4156119363 * value)
	{
		___m_EndPoint_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_EndPoint_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
