﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation>[]
struct EntryU5BU5D_t2286645042;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t516436004;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation>
struct KeyCollection_t3930878850;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation>
struct ValueCollection_t150440922;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Runtime.Serialization.Formatters.Binary.TypeInformation>
struct  Dictionary_2_t1447381079  : public Il2CppObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t3030399641* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t2286645042* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	Il2CppObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t3930878850 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t150440922 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	Il2CppObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___buckets_0)); }
	inline Int32U5BU5D_t3030399641* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t3030399641* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier(&___buckets_0, value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___entries_1)); }
	inline EntryU5BU5D_t2286645042* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t2286645042** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t2286645042* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier(&___entries_1, value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___comparer_6)); }
	inline Il2CppObject* get_comparer_6() const { return ___comparer_6; }
	inline Il2CppObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(Il2CppObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_6, value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___keys_7)); }
	inline KeyCollection_t3930878850 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t3930878850 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t3930878850 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier(&___keys_7, value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ___values_8)); }
	inline ValueCollection_t150440922 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t150440922 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t150440922 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier(&___values_8, value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1447381079, ____syncRoot_9)); }
	inline Il2CppObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline Il2CppObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(Il2CppObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
