﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_TimeoutException3246754798.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexMatchTimeoutException
struct  RegexMatchTimeoutException_t197735700  : public TimeoutException_t3246754798
{
public:
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexInput
	String_t* ___regexInput_16;
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexPattern
	String_t* ___regexPattern_17;
	// System.TimeSpan System.Text.RegularExpressions.RegexMatchTimeoutException::matchTimeout
	TimeSpan_t3430258949  ___matchTimeout_18;

public:
	inline static int32_t get_offset_of_regexInput_16() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___regexInput_16)); }
	inline String_t* get_regexInput_16() const { return ___regexInput_16; }
	inline String_t** get_address_of_regexInput_16() { return &___regexInput_16; }
	inline void set_regexInput_16(String_t* value)
	{
		___regexInput_16 = value;
		Il2CppCodeGenWriteBarrier(&___regexInput_16, value);
	}

	inline static int32_t get_offset_of_regexPattern_17() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___regexPattern_17)); }
	inline String_t* get_regexPattern_17() const { return ___regexPattern_17; }
	inline String_t** get_address_of_regexPattern_17() { return &___regexPattern_17; }
	inline void set_regexPattern_17(String_t* value)
	{
		___regexPattern_17 = value;
		Il2CppCodeGenWriteBarrier(&___regexPattern_17, value);
	}

	inline static int32_t get_offset_of_matchTimeout_18() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t197735700, ___matchTimeout_18)); }
	inline TimeSpan_t3430258949  get_matchTimeout_18() const { return ___matchTimeout_18; }
	inline TimeSpan_t3430258949 * get_address_of_matchTimeout_18() { return &___matchTimeout_18; }
	inline void set_matchTimeout_18(TimeSpan_t3430258949  value)
	{
		___matchTimeout_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
