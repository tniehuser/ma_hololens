﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_XmlValidatingReaderImpl_Pars1774871414.h"

// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t3122949129;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Xml.Schema.BaseValidator
struct BaseValidator_t3557140249;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3518500204;
// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct ValidationEventHandling_t2798815538;
// System.Xml.XmlParserContext
struct XmlParserContext_t2728039553;
// System.Xml.ReadContentAsBinaryHelper
struct ReadContentAsBinaryHelper_t1064946902;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl
struct  XmlValidatingReaderImpl_t1507412803  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::coreReader
	XmlReader_t3675626668 * ___coreReader_3;
	// System.Xml.XmlTextReaderImpl System.Xml.XmlValidatingReaderImpl::coreReaderImpl
	XmlTextReaderImpl_t3122949129 * ___coreReaderImpl_4;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlValidatingReaderImpl::coreReaderNSResolver
	Il2CppObject * ___coreReaderNSResolver_5;
	// System.Xml.ValidationType System.Xml.XmlValidatingReaderImpl::validationType
	int32_t ___validationType_6;
	// System.Xml.Schema.BaseValidator System.Xml.XmlValidatingReaderImpl::validator
	BaseValidator_t3557140249 * ___validator_7;
	// System.Xml.Schema.XmlSchemaCollection System.Xml.XmlValidatingReaderImpl::schemaCollection
	XmlSchemaCollection_t3518500204 * ___schemaCollection_8;
	// System.Boolean System.Xml.XmlValidatingReaderImpl::processIdentityConstraints
	bool ___processIdentityConstraints_9;
	// System.Xml.XmlValidatingReaderImpl/ParsingFunction System.Xml.XmlValidatingReaderImpl::parsingFunction
	int32_t ___parsingFunction_10;
	// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling System.Xml.XmlValidatingReaderImpl::eventHandling
	ValidationEventHandling_t2798815538 * ___eventHandling_11;
	// System.Xml.XmlParserContext System.Xml.XmlValidatingReaderImpl::parserContext
	XmlParserContext_t2728039553 * ___parserContext_12;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XmlValidatingReaderImpl::readBinaryHelper
	ReadContentAsBinaryHelper_t1064946902 * ___readBinaryHelper_13;
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::outerReader
	XmlReader_t3675626668 * ___outerReader_14;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___coreReader_3)); }
	inline XmlReader_t3675626668 * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t3675626668 ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t3675626668 * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier(&___coreReader_3, value);
	}

	inline static int32_t get_offset_of_coreReaderImpl_4() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___coreReaderImpl_4)); }
	inline XmlTextReaderImpl_t3122949129 * get_coreReaderImpl_4() const { return ___coreReaderImpl_4; }
	inline XmlTextReaderImpl_t3122949129 ** get_address_of_coreReaderImpl_4() { return &___coreReaderImpl_4; }
	inline void set_coreReaderImpl_4(XmlTextReaderImpl_t3122949129 * value)
	{
		___coreReaderImpl_4 = value;
		Il2CppCodeGenWriteBarrier(&___coreReaderImpl_4, value);
	}

	inline static int32_t get_offset_of_coreReaderNSResolver_5() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___coreReaderNSResolver_5)); }
	inline Il2CppObject * get_coreReaderNSResolver_5() const { return ___coreReaderNSResolver_5; }
	inline Il2CppObject ** get_address_of_coreReaderNSResolver_5() { return &___coreReaderNSResolver_5; }
	inline void set_coreReaderNSResolver_5(Il2CppObject * value)
	{
		___coreReaderNSResolver_5 = value;
		Il2CppCodeGenWriteBarrier(&___coreReaderNSResolver_5, value);
	}

	inline static int32_t get_offset_of_validationType_6() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___validationType_6)); }
	inline int32_t get_validationType_6() const { return ___validationType_6; }
	inline int32_t* get_address_of_validationType_6() { return &___validationType_6; }
	inline void set_validationType_6(int32_t value)
	{
		___validationType_6 = value;
	}

	inline static int32_t get_offset_of_validator_7() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___validator_7)); }
	inline BaseValidator_t3557140249 * get_validator_7() const { return ___validator_7; }
	inline BaseValidator_t3557140249 ** get_address_of_validator_7() { return &___validator_7; }
	inline void set_validator_7(BaseValidator_t3557140249 * value)
	{
		___validator_7 = value;
		Il2CppCodeGenWriteBarrier(&___validator_7, value);
	}

	inline static int32_t get_offset_of_schemaCollection_8() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___schemaCollection_8)); }
	inline XmlSchemaCollection_t3518500204 * get_schemaCollection_8() const { return ___schemaCollection_8; }
	inline XmlSchemaCollection_t3518500204 ** get_address_of_schemaCollection_8() { return &___schemaCollection_8; }
	inline void set_schemaCollection_8(XmlSchemaCollection_t3518500204 * value)
	{
		___schemaCollection_8 = value;
		Il2CppCodeGenWriteBarrier(&___schemaCollection_8, value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_9() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___processIdentityConstraints_9)); }
	inline bool get_processIdentityConstraints_9() const { return ___processIdentityConstraints_9; }
	inline bool* get_address_of_processIdentityConstraints_9() { return &___processIdentityConstraints_9; }
	inline void set_processIdentityConstraints_9(bool value)
	{
		___processIdentityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_10() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___parsingFunction_10)); }
	inline int32_t get_parsingFunction_10() const { return ___parsingFunction_10; }
	inline int32_t* get_address_of_parsingFunction_10() { return &___parsingFunction_10; }
	inline void set_parsingFunction_10(int32_t value)
	{
		___parsingFunction_10 = value;
	}

	inline static int32_t get_offset_of_eventHandling_11() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___eventHandling_11)); }
	inline ValidationEventHandling_t2798815538 * get_eventHandling_11() const { return ___eventHandling_11; }
	inline ValidationEventHandling_t2798815538 ** get_address_of_eventHandling_11() { return &___eventHandling_11; }
	inline void set_eventHandling_11(ValidationEventHandling_t2798815538 * value)
	{
		___eventHandling_11 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandling_11, value);
	}

	inline static int32_t get_offset_of_parserContext_12() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___parserContext_12)); }
	inline XmlParserContext_t2728039553 * get_parserContext_12() const { return ___parserContext_12; }
	inline XmlParserContext_t2728039553 ** get_address_of_parserContext_12() { return &___parserContext_12; }
	inline void set_parserContext_12(XmlParserContext_t2728039553 * value)
	{
		___parserContext_12 = value;
		Il2CppCodeGenWriteBarrier(&___parserContext_12, value);
	}

	inline static int32_t get_offset_of_readBinaryHelper_13() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___readBinaryHelper_13)); }
	inline ReadContentAsBinaryHelper_t1064946902 * get_readBinaryHelper_13() const { return ___readBinaryHelper_13; }
	inline ReadContentAsBinaryHelper_t1064946902 ** get_address_of_readBinaryHelper_13() { return &___readBinaryHelper_13; }
	inline void set_readBinaryHelper_13(ReadContentAsBinaryHelper_t1064946902 * value)
	{
		___readBinaryHelper_13 = value;
		Il2CppCodeGenWriteBarrier(&___readBinaryHelper_13, value);
	}

	inline static int32_t get_offset_of_outerReader_14() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803, ___outerReader_14)); }
	inline XmlReader_t3675626668 * get_outerReader_14() const { return ___outerReader_14; }
	inline XmlReader_t3675626668 ** get_address_of_outerReader_14() { return &___outerReader_14; }
	inline void set_outerReader_14(XmlReader_t3675626668 * value)
	{
		___outerReader_14 = value;
		Il2CppCodeGenWriteBarrier(&___outerReader_14, value);
	}
};

struct XmlValidatingReaderImpl_t1507412803_StaticFields
{
public:
	// System.Xml.XmlResolver System.Xml.XmlValidatingReaderImpl::s_tempResolver
	XmlResolver_t2024571559 * ___s_tempResolver_15;

public:
	inline static int32_t get_offset_of_s_tempResolver_15() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t1507412803_StaticFields, ___s_tempResolver_15)); }
	inline XmlResolver_t2024571559 * get_s_tempResolver_15() const { return ___s_tempResolver_15; }
	inline XmlResolver_t2024571559 ** get_address_of_s_tempResolver_15() { return &___s_tempResolver_15; }
	inline void set_s_tempResolver_15(XmlResolver_t2024571559 * value)
	{
		___s_tempResolver_15 = value;
		Il2CppCodeGenWriteBarrier(&___s_tempResolver_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
