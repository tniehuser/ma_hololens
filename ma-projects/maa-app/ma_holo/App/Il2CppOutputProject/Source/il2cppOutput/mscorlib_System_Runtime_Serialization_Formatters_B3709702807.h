﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage
struct  BinaryMethodCallMessage_t3709702807  : public Il2CppObject
{
public:
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_inargs
	ObjectU5BU5D_t3614634134* ____inargs_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_methodName
	String_t* ____methodName_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_typeName
	String_t* ____typeName_2;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_methodSignature
	Il2CppObject * ____methodSignature_3;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_instArgs
	TypeU5BU5D_t1664964607* ____instArgs_4;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_args
	ObjectU5BU5D_t3614634134* ____args_5;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_logicalCallContext
	LogicalCallContext_t725724420 * ____logicalCallContext_6;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCallMessage::_properties
	ObjectU5BU5D_t3614634134* ____properties_7;

public:
	inline static int32_t get_offset_of__inargs_0() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____inargs_0)); }
	inline ObjectU5BU5D_t3614634134* get__inargs_0() const { return ____inargs_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of__inargs_0() { return &____inargs_0; }
	inline void set__inargs_0(ObjectU5BU5D_t3614634134* value)
	{
		____inargs_0 = value;
		Il2CppCodeGenWriteBarrier(&____inargs_0, value);
	}

	inline static int32_t get_offset_of__methodName_1() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____methodName_1)); }
	inline String_t* get__methodName_1() const { return ____methodName_1; }
	inline String_t** get_address_of__methodName_1() { return &____methodName_1; }
	inline void set__methodName_1(String_t* value)
	{
		____methodName_1 = value;
		Il2CppCodeGenWriteBarrier(&____methodName_1, value);
	}

	inline static int32_t get_offset_of__typeName_2() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____typeName_2)); }
	inline String_t* get__typeName_2() const { return ____typeName_2; }
	inline String_t** get_address_of__typeName_2() { return &____typeName_2; }
	inline void set__typeName_2(String_t* value)
	{
		____typeName_2 = value;
		Il2CppCodeGenWriteBarrier(&____typeName_2, value);
	}

	inline static int32_t get_offset_of__methodSignature_3() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____methodSignature_3)); }
	inline Il2CppObject * get__methodSignature_3() const { return ____methodSignature_3; }
	inline Il2CppObject ** get_address_of__methodSignature_3() { return &____methodSignature_3; }
	inline void set__methodSignature_3(Il2CppObject * value)
	{
		____methodSignature_3 = value;
		Il2CppCodeGenWriteBarrier(&____methodSignature_3, value);
	}

	inline static int32_t get_offset_of__instArgs_4() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____instArgs_4)); }
	inline TypeU5BU5D_t1664964607* get__instArgs_4() const { return ____instArgs_4; }
	inline TypeU5BU5D_t1664964607** get_address_of__instArgs_4() { return &____instArgs_4; }
	inline void set__instArgs_4(TypeU5BU5D_t1664964607* value)
	{
		____instArgs_4 = value;
		Il2CppCodeGenWriteBarrier(&____instArgs_4, value);
	}

	inline static int32_t get_offset_of__args_5() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____args_5)); }
	inline ObjectU5BU5D_t3614634134* get__args_5() const { return ____args_5; }
	inline ObjectU5BU5D_t3614634134** get_address_of__args_5() { return &____args_5; }
	inline void set__args_5(ObjectU5BU5D_t3614634134* value)
	{
		____args_5 = value;
		Il2CppCodeGenWriteBarrier(&____args_5, value);
	}

	inline static int32_t get_offset_of__logicalCallContext_6() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____logicalCallContext_6)); }
	inline LogicalCallContext_t725724420 * get__logicalCallContext_6() const { return ____logicalCallContext_6; }
	inline LogicalCallContext_t725724420 ** get_address_of__logicalCallContext_6() { return &____logicalCallContext_6; }
	inline void set__logicalCallContext_6(LogicalCallContext_t725724420 * value)
	{
		____logicalCallContext_6 = value;
		Il2CppCodeGenWriteBarrier(&____logicalCallContext_6, value);
	}

	inline static int32_t get_offset_of__properties_7() { return static_cast<int32_t>(offsetof(BinaryMethodCallMessage_t3709702807, ____properties_7)); }
	inline ObjectU5BU5D_t3614634134* get__properties_7() const { return ____properties_7; }
	inline ObjectU5BU5D_t3614634134** get_address_of__properties_7() { return &____properties_7; }
	inline void set__properties_7(ObjectU5BU5D_t3614634134* value)
	{
		____properties_7 = value;
		Il2CppCodeGenWriteBarrier(&____properties_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
