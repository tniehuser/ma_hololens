﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaNames_Token1005517746.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[]
struct XdrAttributeEntryU5BU5D_t4145474522;
// System.Xml.Schema.XdrBuilder/XdrInitFunction
struct XdrInitFunction_t2565742755;
// System.Xml.Schema.XdrBuilder/XdrBeginChildFunction
struct XdrBeginChildFunction_t218963458;
// System.Xml.Schema.XdrBuilder/XdrEndChildFunction
struct XdrEndChildFunction_t4290565954;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrEntry
struct  XdrEntry_t2813485863  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XdrBuilder/XdrEntry::_Name
	int32_t ____Name_0;
	// System.Int32[] System.Xml.Schema.XdrBuilder/XdrEntry::_NextStates
	Int32U5BU5D_t3030399641* ____NextStates_1;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder/XdrEntry::_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ____Attributes_2;
	// System.Xml.Schema.XdrBuilder/XdrInitFunction System.Xml.Schema.XdrBuilder/XdrEntry::_InitFunc
	XdrInitFunction_t2565742755 * ____InitFunc_3;
	// System.Xml.Schema.XdrBuilder/XdrBeginChildFunction System.Xml.Schema.XdrBuilder/XdrEntry::_BeginChildFunc
	XdrBeginChildFunction_t218963458 * ____BeginChildFunc_4;
	// System.Xml.Schema.XdrBuilder/XdrEndChildFunction System.Xml.Schema.XdrBuilder/XdrEntry::_EndChildFunc
	XdrEndChildFunction_t4290565954 * ____EndChildFunc_5;
	// System.Boolean System.Xml.Schema.XdrBuilder/XdrEntry::_AllowText
	bool ____AllowText_6;

public:
	inline static int32_t get_offset_of__Name_0() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____Name_0)); }
	inline int32_t get__Name_0() const { return ____Name_0; }
	inline int32_t* get_address_of__Name_0() { return &____Name_0; }
	inline void set__Name_0(int32_t value)
	{
		____Name_0 = value;
	}

	inline static int32_t get_offset_of__NextStates_1() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____NextStates_1)); }
	inline Int32U5BU5D_t3030399641* get__NextStates_1() const { return ____NextStates_1; }
	inline Int32U5BU5D_t3030399641** get_address_of__NextStates_1() { return &____NextStates_1; }
	inline void set__NextStates_1(Int32U5BU5D_t3030399641* value)
	{
		____NextStates_1 = value;
		Il2CppCodeGenWriteBarrier(&____NextStates_1, value);
	}

	inline static int32_t get_offset_of__Attributes_2() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____Attributes_2)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get__Attributes_2() const { return ____Attributes_2; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of__Attributes_2() { return &____Attributes_2; }
	inline void set__Attributes_2(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		____Attributes_2 = value;
		Il2CppCodeGenWriteBarrier(&____Attributes_2, value);
	}

	inline static int32_t get_offset_of__InitFunc_3() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____InitFunc_3)); }
	inline XdrInitFunction_t2565742755 * get__InitFunc_3() const { return ____InitFunc_3; }
	inline XdrInitFunction_t2565742755 ** get_address_of__InitFunc_3() { return &____InitFunc_3; }
	inline void set__InitFunc_3(XdrInitFunction_t2565742755 * value)
	{
		____InitFunc_3 = value;
		Il2CppCodeGenWriteBarrier(&____InitFunc_3, value);
	}

	inline static int32_t get_offset_of__BeginChildFunc_4() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____BeginChildFunc_4)); }
	inline XdrBeginChildFunction_t218963458 * get__BeginChildFunc_4() const { return ____BeginChildFunc_4; }
	inline XdrBeginChildFunction_t218963458 ** get_address_of__BeginChildFunc_4() { return &____BeginChildFunc_4; }
	inline void set__BeginChildFunc_4(XdrBeginChildFunction_t218963458 * value)
	{
		____BeginChildFunc_4 = value;
		Il2CppCodeGenWriteBarrier(&____BeginChildFunc_4, value);
	}

	inline static int32_t get_offset_of__EndChildFunc_5() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____EndChildFunc_5)); }
	inline XdrEndChildFunction_t4290565954 * get__EndChildFunc_5() const { return ____EndChildFunc_5; }
	inline XdrEndChildFunction_t4290565954 ** get_address_of__EndChildFunc_5() { return &____EndChildFunc_5; }
	inline void set__EndChildFunc_5(XdrEndChildFunction_t4290565954 * value)
	{
		____EndChildFunc_5 = value;
		Il2CppCodeGenWriteBarrier(&____EndChildFunc_5, value);
	}

	inline static int32_t get_offset_of__AllowText_6() { return static_cast<int32_t>(offsetof(XdrEntry_t2813485863, ____AllowText_6)); }
	inline bool get__AllowText_6() const { return ____AllowText_6; }
	inline bool* get_address_of__AllowText_6() { return &____AllowText_6; }
	inline void set__AllowText_6(bool value)
	{
		____AllowText_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
