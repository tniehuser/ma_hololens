﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFStreamClientContext
struct  CFStreamClientContext_t3245641555 
{
public:
	// System.IntPtr Mono.Net.CFStreamClientContext::Version
	IntPtr_t ___Version_0;
	// System.IntPtr Mono.Net.CFStreamClientContext::Info
	IntPtr_t ___Info_1;
	// System.IntPtr Mono.Net.CFStreamClientContext::Retain
	IntPtr_t ___Retain_2;
	// System.IntPtr Mono.Net.CFStreamClientContext::Release
	IntPtr_t ___Release_3;
	// System.IntPtr Mono.Net.CFStreamClientContext::CopyDescription
	IntPtr_t ___CopyDescription_4;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Version_0)); }
	inline IntPtr_t get_Version_0() const { return ___Version_0; }
	inline IntPtr_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(IntPtr_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Info_1() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Info_1)); }
	inline IntPtr_t get_Info_1() const { return ___Info_1; }
	inline IntPtr_t* get_address_of_Info_1() { return &___Info_1; }
	inline void set_Info_1(IntPtr_t value)
	{
		___Info_1 = value;
	}

	inline static int32_t get_offset_of_Retain_2() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Retain_2)); }
	inline IntPtr_t get_Retain_2() const { return ___Retain_2; }
	inline IntPtr_t* get_address_of_Retain_2() { return &___Retain_2; }
	inline void set_Retain_2(IntPtr_t value)
	{
		___Retain_2 = value;
	}

	inline static int32_t get_offset_of_Release_3() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Release_3)); }
	inline IntPtr_t get_Release_3() const { return ___Release_3; }
	inline IntPtr_t* get_address_of_Release_3() { return &___Release_3; }
	inline void set_Release_3(IntPtr_t value)
	{
		___Release_3 = value;
	}

	inline static int32_t get_offset_of_CopyDescription_4() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___CopyDescription_4)); }
	inline IntPtr_t get_CopyDescription_4() const { return ___CopyDescription_4; }
	inline IntPtr_t* get_address_of_CopyDescription_4() { return &___CopyDescription_4; }
	inline void set_CopyDescription_4(IntPtr_t value)
	{
		___CopyDescription_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
