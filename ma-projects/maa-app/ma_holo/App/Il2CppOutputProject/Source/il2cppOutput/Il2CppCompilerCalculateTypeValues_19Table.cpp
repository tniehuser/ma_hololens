﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Security_Cryptography_X509Certificat2968295413.h"
#include "System_System_Security_Cryptography_X509Certificat2085032772.h"
#include "System_System_Security_Cryptography_X509Certificat3452126517.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "System_System_Security_Cryptography_X509Certificate480677120.h"
#include "System_System_Security_Cryptography_X509Certificat2099881051.h"
#include "System_System_Security_Cryptography_X509Certificate650873211.h"
#include "System_System_Security_Cryptography_X509Certificat1320896183.h"
#include "System_System_Security_Cryptography_X509Certificat3763443773.h"
#include "System_System_Security_Cryptography_X509Certificat3221716179.h"
#include "System_System_Security_Cryptography_X509Certificat1000999714.h"
#include "System_System_Security_Cryptography_X509Certificat2161630009.h"
#include "System_System_Security_Cryptography_X509Certificat1038124237.h"
#include "System_System_Security_Cryptography_X509Certificat2461349531.h"
#include "System_System_Security_Cryptography_X509Certificat2669466891.h"
#include "System_System_Security_Cryptography_X509Certificat2166064554.h"
#include "System_System_Security_Cryptography_X509Certificat2065307963.h"
#include "System_System_Security_Cryptography_X509Certificat1617430119.h"
#include "System_System_Security_Cryptography_X509Certificat2508879999.h"
#include "System_System_Security_Cryptography_X509Certificate110301003.h"
#include "System_System_Security_Cryptography_X509Certificat2169036324.h"
#include "System_System_SRDescriptionAttribute431985547.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_Mono_Net_Dns_DnsClass3168085651.h"
#include "System_Mono_Net_Dns_DnsHeader2481892084.h"
#include "System_Mono_Net_Dns_DnsOpCode1353552885.h"
#include "System_Mono_Net_Dns_DnsPacket413343383.h"
#include "System_Mono_Net_Dns_DnsQClass2376709280.h"
#include "System_Mono_Net_Dns_DnsQType1279797630.h"
#include "System_Mono_Net_Dns_DnsQuery3483920911.h"
#include "System_Mono_Net_Dns_DnsQuestion3090842959.h"
#include "System_Mono_Net_Dns_DnsRCode977533636.h"
#include "System_Mono_Net_Dns_DnsResourceRecordA2533778347.h"
#include "System_Mono_Net_Dns_DnsResourceRecordAAAA4028376332.h"
#include "System_Mono_Net_Dns_DnsResourceRecordCName595248992.h"
#include "System_Mono_Net_Dns_DnsResourceRecord2943454412.h"
#include "System_Mono_Net_Dns_DnsResourceRecordIPAddress2549622903.h"
#include "System_Mono_Net_Dns_DnsResourceRecordPTR1722196618.h"
#include "System_Mono_Net_Dns_DnsResponse3880332876.h"
#include "System_Mono_Net_Dns_DnsType1822475631.h"
#include "System_Mono_Net_Dns_DnsUtil3812046859.h"
#include "System_Mono_Net_Dns_SimpleResolver2933968162.h"
#include "System_Mono_Net_Dns_ResolverError2494446112.h"
#include "System_Mono_Net_Dns_SimpleResolverEventArgs4294564137.h"
#include "System_System_IOOperation4250055067.h"
#include "System_System_IOAsyncCallback2427139621.h"
#include "System_System_IOAsyncResult1276329107.h"
#include "System_System_IOSelectorJob2021937086.h"
#include "System_System_IOSelector2669134661.h"
#include "System_Mono_Net_Security_Private_CallbackHelpers4210896810.h"
#include "System_Mono_Net_Security_Private_CallbackHelpers_U3927459951.h"
#include "System_Mono_Net_Security_Private_CallbackHelpers_U4041511882.h"
#include "System_Mono_Net_Security_ServerCertValidationCallbac93117366.h"
#include "System_Mono_Net_Security_ChainValidationHelper3893280544.h"
#include "System_Mono_Net_Security_Private_LegacySslStream83997747.h"
#include "System_Mono_Net_Security_Private_LegacySslStream_U3081887423.h"
#include "System_Mono_Net_Security_LegacyTlsProvider3024743.h"
#include "System_Mono_Net_Security_Private_MonoSslStreamImpl1549641033.h"
#include "System_Mono_Net_Security_Private_MonoSslStreamWrap4202517786.h"
#include "System_Mono_Net_Security_MonoTlsProviderFactory3928617369.h"
#include "System_Mono_Net_Security_Private_MonoTlsProviderWra210206708.h"
#include "System_Mono_Net_Security_MonoTlsStream1249652258.h"
#include "System_Mono_Net_Security_SystemCertificateValidato2641375168.h"
#include "System_System_Net_AutoWebProxyScriptEngine2702566410.h"
#include "System_System_Diagnostics_ConfigurationManagerInte3801022026.h"
#include "System_System_Diagnostics_ConfigurationManagerInte3259527665.h"
#include "System_System_Security_Cryptography_CAPI3094615495.h"
#include "System_System_Net_UnsafeNclNativeMethods667458916.h"
#include "System_System_Net_UnsafeNclNativeMethods_SecureStr3125888809.h"
#include "System_System_LocalAppContextSwitches4208498603.h"
#include "System_System_Net_Logging2453025863.h"
#include "System_System_Net_Cache_RequestCacheProtocol2110185277.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_Configuration_PrivilegedConfiguratio3772865982.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_CachedCodeEn3553821051.h"
#include "System_System_Text_RegularExpressions_ExclusiveRefe708182869.h"
#include "System_System_Text_RegularExpressions_SharedRefere2137668360.h"
#include "System_System_Text_RegularExpressions_RegexBoyerMo2204811018.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_RegexCharCla2441867401.h"
#include "System_System_Text_RegularExpressions_RegexCharCla2153935826.h"
#include "System_System_Text_RegularExpressions_RegexCharCla3640568023.h"
#include "System_System_Text_RegularExpressions_RegexCharCla3794243288.h"
#include "System_System_Text_RegularExpressions_RegexCode2469392150.h"
#include "System_System_Text_RegularExpressions_RegexCompile1714699756.h"
#include "System_System_Text_RegularExpressions_RegexCompile1775865058.h"
#include "System_System_Text_RegularExpressions_RegexLWCGCom1552814047.h"
#include "System_System_Text_RegularExpressions_RegexFCD150416822.h"
#include "System_System_Text_RegularExpressions_RegexFC2009854258.h"
#include "System_System_Text_RegularExpressions_RegexPrefix1013837165.h"
#include "System_System_Text_RegularExpressions_Group3761430853.h"
#include "System_System_Text_RegularExpressions_GroupCollecti939014605.h"
#include "System_System_Text_RegularExpressions_GroupEnumera3864870211.h"
#include "System_System_Text_RegularExpressions_RegexInterpre884291495.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"
#include "System_System_Text_RegularExpressions_MatchSparse4194398671.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (X509ChainImpl_t2968295413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (X509ChainImplMono_t2085032772), -1, sizeof(X509ChainImplMono_t2085032772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[16] = 
{
	X509ChainImplMono_t2085032772::get_offset_of_location_0(),
	X509ChainImplMono_t2085032772::get_offset_of_elements_1(),
	X509ChainImplMono_t2085032772::get_offset_of_policy_2(),
	X509ChainImplMono_t2085032772::get_offset_of_status_3(),
	X509ChainImplMono_t2085032772_StaticFields::get_offset_of_Empty_4(),
	X509ChainImplMono_t2085032772::get_offset_of_max_path_length_5(),
	X509ChainImplMono_t2085032772::get_offset_of_working_issuer_name_6(),
	X509ChainImplMono_t2085032772::get_offset_of_working_public_key_7(),
	X509ChainImplMono_t2085032772::get_offset_of_bce_restriction_8(),
	X509ChainImplMono_t2085032772::get_offset_of_roots_9(),
	X509ChainImplMono_t2085032772::get_offset_of_cas_10(),
	X509ChainImplMono_t2085032772::get_offset_of_root_store_11(),
	X509ChainImplMono_t2085032772::get_offset_of_ca_store_12(),
	X509ChainImplMono_t2085032772::get_offset_of_user_root_store_13(),
	X509ChainImplMono_t2085032772::get_offset_of_user_ca_store_14(),
	X509ChainImplMono_t2085032772::get_offset_of_collection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (X509ChainPolicy_t3452126517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[9] = 
{
	X509ChainPolicy_t3452126517::get_offset_of_apps_0(),
	X509ChainPolicy_t3452126517::get_offset_of_cert_1(),
	X509ChainPolicy_t3452126517::get_offset_of_store_2(),
	X509ChainPolicy_t3452126517::get_offset_of_store2_3(),
	X509ChainPolicy_t3452126517::get_offset_of_rflag_4(),
	X509ChainPolicy_t3452126517::get_offset_of_mode_5(),
	X509ChainPolicy_t3452126517::get_offset_of_timeout_6(),
	X509ChainPolicy_t3452126517::get_offset_of_vflags_7(),
	X509ChainPolicy_t3452126517::get_offset_of_vtime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (X509ChainStatus_t4278378721)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t4278378721_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[2] = 
{
	X509ChainStatus_t4278378721::get_offset_of_status_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509ChainStatus_t4278378721::get_offset_of_info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (X509ChainStatusFlags_t480677120)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[27] = 
{
	X509ChainStatusFlags_t480677120::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (X509EnhancedKeyUsageExtension_t2099881051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[2] = 
{
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__enhKeyUsage_3(),
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (X509ExtensionCollection_t650873211), -1, sizeof(X509ExtensionCollection_t650873211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	X509ExtensionCollection_t650873211_StaticFields::get_offset_of_Empty_0(),
	X509ExtensionCollection_t650873211::get_offset_of__list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (X509Extension_t1320896183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[1] = 
{
	X509Extension_t1320896183::get_offset_of__critical_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (X509FindType_t3221716179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1909[16] = 
{
	X509FindType_t3221716179::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (X509Helper2_t1000999714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (MyNativeHelper_t2161630009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (X509KeyUsageExtension_t1038124237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1038124237::get_offset_of__keyUsages_6(),
	X509KeyUsageExtension_t1038124237::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (X509KeyUsageFlags_t2461349531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[11] = 
{
	X509KeyUsageFlags_t2461349531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (X509NameType_t2669466891)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[7] = 
{
	X509NameType_t2669466891::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (X509RevocationFlag_t2166064554)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1915[4] = 
{
	X509RevocationFlag_t2166064554::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (X509RevocationMode_t2065307963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1916[4] = 
{
	X509RevocationMode_t2065307963::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (X509Store_t1617430119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[5] = 
{
	X509Store_t1617430119::get_offset_of__name_0(),
	X509Store_t1617430119::get_offset_of__location_1(),
	X509Store_t1617430119::get_offset_of_list_2(),
	X509Store_t1617430119::get_offset_of__flags_3(),
	X509Store_t1617430119::get_offset_of_store_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (X509SubjectKeyIdentifierExtension_t2508879999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__subjectKeyIdentifier_5(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__ski_6(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t110301003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t110301003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (X509VerificationFlags_t2169036324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[15] = 
{
	X509VerificationFlags_t2169036324::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (SRDescriptionAttribute_t431985547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[1] = 
{
	SRDescriptionAttribute_t431985547::get_offset_of_isReplaced_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (DnsClass_t3168085651)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[9] = 
{
	DnsClass_t3168085651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (DnsHeader_t2481892084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[1] = 
{
	DnsHeader_t2481892084::get_offset_of_bytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (DnsOpCode_t1353552885)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[6] = 
{
	DnsOpCode_t1353552885::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (DnsPacket_t413343383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[3] = 
{
	DnsPacket_t413343383::get_offset_of_packet_0(),
	DnsPacket_t413343383::get_offset_of_position_1(),
	DnsPacket_t413343383::get_offset_of_header_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (DnsQClass_t2376709280)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1927[11] = 
{
	DnsQClass_t2376709280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (DnsQType_t1279797630)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1928[71] = 
{
	DnsQType_t1279797630::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (DnsQuery_t3483920911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (DnsQuestion_t3090842959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[3] = 
{
	DnsQuestion_t3090842959::get_offset_of_name_0(),
	DnsQuestion_t3090842959::get_offset_of_type_1(),
	DnsQuestion_t3090842959::get_offset_of__class_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (DnsRCode_t977533636)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[20] = 
{
	DnsRCode_t977533636::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (DnsResourceRecordA_t2533778347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (DnsResourceRecordAAAA_t4028376332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (DnsResourceRecordCName_t595248992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	DnsResourceRecordCName_t595248992::get_offset_of_cname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (DnsResourceRecord_t2943454412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[6] = 
{
	DnsResourceRecord_t2943454412::get_offset_of_name_0(),
	DnsResourceRecord_t2943454412::get_offset_of_type_1(),
	DnsResourceRecord_t2943454412::get_offset_of_klass_2(),
	DnsResourceRecord_t2943454412::get_offset_of_ttl_3(),
	DnsResourceRecord_t2943454412::get_offset_of_rdlength_4(),
	DnsResourceRecord_t2943454412::get_offset_of_m_rdata_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (DnsResourceRecordIPAddress_t2549622903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[1] = 
{
	DnsResourceRecordIPAddress_t2549622903::get_offset_of_address_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (DnsResourceRecordPTR_t1722196618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	DnsResourceRecordPTR_t1722196618::get_offset_of_dname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (DnsResponse_t3880332876), -1, sizeof(DnsResponse_t3880332876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1938[7] = 
{
	DnsResponse_t3880332876_StaticFields::get_offset_of_EmptyRR_3(),
	DnsResponse_t3880332876_StaticFields::get_offset_of_EmptyQS_4(),
	DnsResponse_t3880332876::get_offset_of_question_5(),
	DnsResponse_t3880332876::get_offset_of_answer_6(),
	DnsResponse_t3880332876::get_offset_of_authority_7(),
	DnsResponse_t3880332876::get_offset_of_additional_8(),
	DnsResponse_t3880332876::get_offset_of_offset_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (DnsType_t1822475631)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[70] = 
{
	DnsType_t1822475631::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (DnsUtil_t3812046859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (SimpleResolver_t2933968162), -1, sizeof(SimpleResolver_t2933968162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1941[8] = 
{
	SimpleResolver_t2933968162_StaticFields::get_offset_of_EmptyStrings_0(),
	SimpleResolver_t2933968162_StaticFields::get_offset_of_EmptyAddresses_1(),
	SimpleResolver_t2933968162::get_offset_of_endpoints_2(),
	SimpleResolver_t2933968162::get_offset_of_client_3(),
	SimpleResolver_t2933968162::get_offset_of_queries_4(),
	SimpleResolver_t2933968162::get_offset_of_receive_cb_5(),
	SimpleResolver_t2933968162::get_offset_of_timeout_cb_6(),
	SimpleResolver_t2933968162::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (ResolverError_t2494446112)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[10] = 
{
	ResolverError_t2494446112::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (SimpleResolverEventArgs_t4294564137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[9] = 
{
	SimpleResolverEventArgs_t4294564137::get_offset_of_Completed_1(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CResolverErrorU3Ek__BackingField_2(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CErrorMessageU3Ek__BackingField_3(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CHostNameU3Ek__BackingField_4(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CHostEntryU3Ek__BackingField_5(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_QueryID_6(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_Retries_7(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_Timer_8(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_PTRAddress_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (IOOperation_t4250055067)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[3] = 
{
	IOOperation_t4250055067::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (IOAsyncCallback_t2427139621), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (IOAsyncResult_t1276329107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[5] = 
{
	IOAsyncResult_t1276329107::get_offset_of_async_callback_0(),
	IOAsyncResult_t1276329107::get_offset_of_async_state_1(),
	IOAsyncResult_t1276329107::get_offset_of_wait_handle_2(),
	IOAsyncResult_t1276329107::get_offset_of_completed_synchronously_3(),
	IOAsyncResult_t1276329107::get_offset_of_completed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (IOSelectorJob_t2021937086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[3] = 
{
	IOSelectorJob_t2021937086::get_offset_of_operation_0(),
	IOSelectorJob_t2021937086::get_offset_of_callback_1(),
	IOSelectorJob_t2021937086::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (IOSelector_t2669134661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CallbackHelpers_t4210896810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (U3CMonoToPublicU3Ec__AnonStorey5_t927459951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[1] = 
{
	U3CMonoToPublicU3Ec__AnonStorey5_t927459951::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (U3CMonoToInternalU3Ec__AnonStorey8_t4041511882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	U3CMonoToInternalU3Ec__AnonStorey8_t4041511882::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (ServerCertValidationCallbackWrapper_t93117366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (ChainValidationHelper_t3893280544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[8] = 
{
	ChainValidationHelper_t3893280544::get_offset_of_sender_0(),
	ChainValidationHelper_t3893280544::get_offset_of_settings_1(),
	ChainValidationHelper_t3893280544::get_offset_of_provider_2(),
	ChainValidationHelper_t3893280544::get_offset_of_certValidationCallback_3(),
	ChainValidationHelper_t3893280544::get_offset_of_certSelectionCallback_4(),
	ChainValidationHelper_t3893280544::get_offset_of_callbackWrapper_5(),
	ChainValidationHelper_t3893280544::get_offset_of_tlsStream_6(),
	ChainValidationHelper_t3893280544::get_offset_of_request_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (LegacySslStream_t83997747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[3] = 
{
	LegacySslStream_t83997747::get_offset_of_ssl_stream_10(),
	LegacySslStream_t83997747::get_offset_of_certificateValidator_11(),
	LegacySslStream_t83997747::get_offset_of_provider_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[3] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423::get_offset_of_targetHost_1(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (LegacyTlsProvider_t3024743), -1, sizeof(LegacyTlsProvider_t3024743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1958[1] = 
{
	LegacyTlsProvider_t3024743_StaticFields::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (MonoSslStreamImpl_t1549641033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[1] = 
{
	MonoSslStreamImpl_t1549641033::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (MonoSslStreamWrapper_t4202517786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	MonoSslStreamWrapper_t4202517786::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (MonoTlsProviderFactory_t3928617369), -1, sizeof(MonoTlsProviderFactory_t3928617369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[4] = 
{
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_locker_0(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_initialized_1(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_defaultProvider_2(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_providerRegistration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (MonoTlsProviderWrapper_t210206708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[1] = 
{
	MonoTlsProviderWrapper_t210206708::get_offset_of_provider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (MonoTlsStream_t1249652258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[8] = 
{
	MonoTlsStream_t1249652258::get_offset_of_provider_0(),
	MonoTlsStream_t1249652258::get_offset_of_request_1(),
	MonoTlsStream_t1249652258::get_offset_of_networkStream_2(),
	MonoTlsStream_t1249652258::get_offset_of_sslStream_3(),
	MonoTlsStream_t1249652258::get_offset_of_status_4(),
	MonoTlsStream_t1249652258::get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5(),
	MonoTlsStream_t1249652258::get_offset_of_validationHelper_6(),
	MonoTlsStream_t1249652258::get_offset_of_settings_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (SystemCertificateValidator_t2641375168), -1, sizeof(SystemCertificateValidator_t2641375168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1964[3] = 
{
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_is_macosx_0(),
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_revocation_mode_1(),
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_s_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (AutoWebProxyScriptEngine_t2702566410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[2] = 
{
	AutoWebProxyScriptEngine_t2702566410::get_offset_of_U3CAutomaticConfigurationScriptU3Ek__BackingField_0(),
	AutoWebProxyScriptEngine_t2702566410::get_offset_of_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (ConfigurationManagerInternalFactory_t3801022026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (Instance_t3259527665), -1, sizeof(Instance_t3259527665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1967[1] = 
{
	Instance_t3259527665_StaticFields::get_offset_of_SetConfigurationSystemInProgress_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (CAPI_t3094615495), -1, sizeof(CAPI_t3094615495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1968[2] = 
{
	CAPI_t3094615495_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_0(),
	CAPI_t3094615495_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (UnsafeNclNativeMethods_t667458916), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (SecureStringHelper_t3125888809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (LocalAppContextSwitches_t4208498603), -1, sizeof(LocalAppContextSwitches_t4208498603_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[1] = 
{
	LocalAppContextSwitches_t4208498603_StaticFields::get_offset_of_MemberDescriptorEqualsReturnsFalseIfEquivalent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (Logging_t2453025863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (RequestCacheProtocol_t2110185277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (Win32Exception_t1708275760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[1] = 
{
	Win32Exception_t1708275760::get_offset_of_nativeErrorCode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (PrivilegedConfigurationManager_t3772865982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1976[18] = 
{
	Regex_t1803876613::get_offset_of_pattern_0(),
	Regex_t1803876613::get_offset_of_factory_1(),
	Regex_t1803876613::get_offset_of_roptions_2(),
	Regex_t1803876613_StaticFields::get_offset_of_MaximumMatchTimeout_3(),
	Regex_t1803876613_StaticFields::get_offset_of_InfiniteMatchTimeout_4(),
	Regex_t1803876613::get_offset_of_internalMatchTimeout_5(),
	Regex_t1803876613_StaticFields::get_offset_of_FallbackDefaultMatchTimeout_6(),
	Regex_t1803876613_StaticFields::get_offset_of_DefaultMatchTimeout_7(),
	Regex_t1803876613::get_offset_of_caps_8(),
	Regex_t1803876613::get_offset_of_capnames_9(),
	Regex_t1803876613::get_offset_of_capslist_10(),
	Regex_t1803876613::get_offset_of_capsize_11(),
	Regex_t1803876613::get_offset_of_runnerref_12(),
	Regex_t1803876613::get_offset_of_replref_13(),
	Regex_t1803876613::get_offset_of_code_14(),
	Regex_t1803876613::get_offset_of_refsInitialized_15(),
	Regex_t1803876613_StaticFields::get_offset_of_livecode_16(),
	Regex_t1803876613_StaticFields::get_offset_of_cacheSize_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (CachedCodeEntry_t3553821051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[9] = 
{
	CachedCodeEntry_t3553821051::get_offset_of__key_0(),
	CachedCodeEntry_t3553821051::get_offset_of__code_1(),
	CachedCodeEntry_t3553821051::get_offset_of__caps_2(),
	CachedCodeEntry_t3553821051::get_offset_of__capnames_3(),
	CachedCodeEntry_t3553821051::get_offset_of__capslist_4(),
	CachedCodeEntry_t3553821051::get_offset_of__capsize_5(),
	CachedCodeEntry_t3553821051::get_offset_of__factory_6(),
	CachedCodeEntry_t3553821051::get_offset_of__runnerref_7(),
	CachedCodeEntry_t3553821051::get_offset_of__replref_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (ExclusiveReference_t708182869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[3] = 
{
	ExclusiveReference_t708182869::get_offset_of__ref_0(),
	ExclusiveReference_t708182869::get_offset_of__obj_1(),
	ExclusiveReference_t708182869::get_offset_of__locked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (SharedReference_t2137668360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[1] = 
{
	SharedReference_t2137668360::get_offset_of__ref_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (RegexBoyerMoore_t2204811018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[9] = 
{
	RegexBoyerMoore_t2204811018::get_offset_of__positive_0(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeASCII_1(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeUnicode_2(),
	RegexBoyerMoore_t2204811018::get_offset_of__pattern_3(),
	RegexBoyerMoore_t2204811018::get_offset_of__lowASCII_4(),
	RegexBoyerMoore_t2204811018::get_offset_of__highASCII_5(),
	RegexBoyerMoore_t2204811018::get_offset_of__rightToLeft_6(),
	RegexBoyerMoore_t2204811018::get_offset_of__caseInsensitive_7(),
	RegexBoyerMoore_t2204811018::get_offset_of__culture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[3] = 
{
	Capture_t4157900610::get_offset_of__text_0(),
	Capture_t4157900610::get_offset_of__index_1(),
	Capture_t4157900610::get_offset_of__length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (RegexCharClass_t2441867401), -1, sizeof(RegexCharClass_t2441867401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1982[19] = 
{
	RegexCharClass_t2441867401::get_offset_of__rangelist_0(),
	RegexCharClass_t2441867401::get_offset_of__categories_1(),
	RegexCharClass_t2441867401::get_offset_of__canonical_2(),
	RegexCharClass_t2441867401::get_offset_of__negate_3(),
	RegexCharClass_t2441867401::get_offset_of__subtractor_4(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_InternalRegexIgnoreCase_5(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Space_6(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpace_7(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Word_8(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWord_9(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_SpaceClass_10(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpaceClass_11(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_WordClass_12(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWordClass_13(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_DigitClass_14(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotDigitClass_15(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__definedCategories_16(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__propTable_17(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__lcTable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (LowerCaseMapping_t2153935826)+ sizeof (Il2CppObject), sizeof(LowerCaseMapping_t2153935826_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	LowerCaseMapping_t2153935826::get_offset_of__chMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LowerCaseMapping_t2153935826::get_offset_of__chMax_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LowerCaseMapping_t2153935826::get_offset_of__lcOp_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LowerCaseMapping_t2153935826::get_offset_of__data_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (SingleRangeComparer_t3640568023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (SingleRange_t3794243288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[2] = 
{
	SingleRange_t3794243288::get_offset_of__first_0(),
	SingleRange_t3794243288::get_offset_of__last_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (RegexCode_t2469392150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[9] = 
{
	RegexCode_t2469392150::get_offset_of__codes_0(),
	RegexCode_t2469392150::get_offset_of__strings_1(),
	RegexCode_t2469392150::get_offset_of__trackcount_2(),
	RegexCode_t2469392150::get_offset_of__caps_3(),
	RegexCode_t2469392150::get_offset_of__capsize_4(),
	RegexCode_t2469392150::get_offset_of__fcPrefix_5(),
	RegexCode_t2469392150::get_offset_of__bmPrefix_6(),
	RegexCode_t2469392150::get_offset_of__anchors_7(),
	RegexCode_t2469392150::get_offset_of__rightToLeft_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (RegexCompiler_t1714699756), -1, sizeof(RegexCompiler_t1714699756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1987[56] = 
{
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textbegF_0(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textendF_1(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textstartF_2(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textposF_3(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__textF_4(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackposF_5(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackF_6(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__stackposF_7(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__stackF_8(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__trackcountF_9(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__ensurestorageM_10(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__captureM_11(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__transferM_12(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__uncaptureM_13(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__ismatchedM_14(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__matchlengthM_15(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__matchindexM_16(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__isboundaryM_17(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__isECMABoundaryM_18(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__chartolowerM_19(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getcharM_20(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__crawlposM_21(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__charInSetM_22(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getCurrentCulture_23(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__getInvariantCulture_24(),
	RegexCompiler_t1714699756_StaticFields::get_offset_of__checkTimeoutM_25(),
	RegexCompiler_t1714699756::get_offset_of__ilg_26(),
	RegexCompiler_t1714699756::get_offset_of__textstartV_27(),
	RegexCompiler_t1714699756::get_offset_of__textbegV_28(),
	RegexCompiler_t1714699756::get_offset_of__textendV_29(),
	RegexCompiler_t1714699756::get_offset_of__textposV_30(),
	RegexCompiler_t1714699756::get_offset_of__textV_31(),
	RegexCompiler_t1714699756::get_offset_of__trackposV_32(),
	RegexCompiler_t1714699756::get_offset_of__trackV_33(),
	RegexCompiler_t1714699756::get_offset_of__stackposV_34(),
	RegexCompiler_t1714699756::get_offset_of__stackV_35(),
	RegexCompiler_t1714699756::get_offset_of__tempV_36(),
	RegexCompiler_t1714699756::get_offset_of__temp2V_37(),
	RegexCompiler_t1714699756::get_offset_of__temp3V_38(),
	RegexCompiler_t1714699756::get_offset_of__code_39(),
	RegexCompiler_t1714699756::get_offset_of__codes_40(),
	RegexCompiler_t1714699756::get_offset_of__strings_41(),
	RegexCompiler_t1714699756::get_offset_of__fcPrefix_42(),
	RegexCompiler_t1714699756::get_offset_of__bmPrefix_43(),
	RegexCompiler_t1714699756::get_offset_of__anchors_44(),
	RegexCompiler_t1714699756::get_offset_of__labels_45(),
	RegexCompiler_t1714699756::get_offset_of__notes_46(),
	RegexCompiler_t1714699756::get_offset_of__notecount_47(),
	RegexCompiler_t1714699756::get_offset_of__trackcount_48(),
	RegexCompiler_t1714699756::get_offset_of__backtrack_49(),
	RegexCompiler_t1714699756::get_offset_of__regexopcode_50(),
	RegexCompiler_t1714699756::get_offset_of__codepos_51(),
	RegexCompiler_t1714699756::get_offset_of__backpos_52(),
	RegexCompiler_t1714699756::get_offset_of__options_53(),
	RegexCompiler_t1714699756::get_offset_of__uniquenote_54(),
	RegexCompiler_t1714699756::get_offset_of__goto_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (BacktrackNote_t1775865058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[3] = 
{
	BacktrackNote_t1775865058::get_offset_of__codepos_0(),
	BacktrackNote_t1775865058::get_offset_of__flags_1(),
	BacktrackNote_t1775865058::get_offset_of__label_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (RegexLWCGCompiler_t1552814047), -1, sizeof(RegexLWCGCompiler_t1552814047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1989[2] = 
{
	RegexLWCGCompiler_t1552814047_StaticFields::get_offset_of__regexCount_56(),
	RegexLWCGCompiler_t1552814047_StaticFields::get_offset_of__paramTypes_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (RegexFCD_t150416822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[7] = 
{
	RegexFCD_t150416822::get_offset_of__intStack_0(),
	RegexFCD_t150416822::get_offset_of__intDepth_1(),
	RegexFCD_t150416822::get_offset_of__fcStack_2(),
	RegexFCD_t150416822::get_offset_of__fcDepth_3(),
	RegexFCD_t150416822::get_offset_of__skipAllChildren_4(),
	RegexFCD_t150416822::get_offset_of__skipchild_5(),
	RegexFCD_t150416822::get_offset_of__failed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (RegexFC_t2009854258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[3] = 
{
	RegexFC_t2009854258::get_offset_of__cc_0(),
	RegexFC_t2009854258::get_offset_of__nullable_1(),
	RegexFC_t2009854258::get_offset_of__caseInsensitive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (RegexPrefix_t1013837165), -1, sizeof(RegexPrefix_t1013837165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1992[3] = 
{
	RegexPrefix_t1013837165::get_offset_of__prefix_0(),
	RegexPrefix_t1013837165::get_offset_of__caseInsensitive_1(),
	RegexPrefix_t1013837165_StaticFields::get_offset_of__empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (Group_t3761430853), -1, sizeof(Group_t3761430853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[3] = 
{
	Group_t3761430853_StaticFields::get_offset_of__emptygroup_3(),
	Group_t3761430853::get_offset_of__caps_4(),
	Group_t3761430853::get_offset_of__capcount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (GroupCollection_t939014605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[3] = 
{
	GroupCollection_t939014605::get_offset_of__match_0(),
	GroupCollection_t939014605::get_offset_of__captureMap_1(),
	GroupCollection_t939014605::get_offset_of__groups_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (GroupEnumerator_t3864870211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[2] = 
{
	GroupEnumerator_t3864870211::get_offset_of__rgc_0(),
	GroupEnumerator_t3864870211::get_offset_of__curindex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (RegexInterpreter_t884291495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[11] = 
{
	RegexInterpreter_t884291495::get_offset_of_runoperator_18(),
	RegexInterpreter_t884291495::get_offset_of_runcodes_19(),
	RegexInterpreter_t884291495::get_offset_of_runcodepos_20(),
	RegexInterpreter_t884291495::get_offset_of_runstrings_21(),
	RegexInterpreter_t884291495::get_offset_of_runcode_22(),
	RegexInterpreter_t884291495::get_offset_of_runfcPrefix_23(),
	RegexInterpreter_t884291495::get_offset_of_runbmPrefix_24(),
	RegexInterpreter_t884291495::get_offset_of_runanchors_25(),
	RegexInterpreter_t884291495::get_offset_of_runrtl_26(),
	RegexInterpreter_t884291495::get_offset_of_runci_27(),
	RegexInterpreter_t884291495::get_offset_of_runculture_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (Match_t3164245899), -1, sizeof(Match_t3164245899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1997[10] = 
{
	Match_t3164245899_StaticFields::get_offset_of__empty_6(),
	Match_t3164245899::get_offset_of__groupcoll_7(),
	Match_t3164245899::get_offset_of__regex_8(),
	Match_t3164245899::get_offset_of__textbeg_9(),
	Match_t3164245899::get_offset_of__textpos_10(),
	Match_t3164245899::get_offset_of__textend_11(),
	Match_t3164245899::get_offset_of__textstart_12(),
	Match_t3164245899::get_offset_of__matches_13(),
	Match_t3164245899::get_offset_of__matchcount_14(),
	Match_t3164245899::get_offset_of__balancing_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (MatchSparse_t4194398671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	MatchSparse_t4194398671::get_offset_of__caps_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (MatchCollection_t3718216671), -1, sizeof(MatchCollection_t3718216671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1999[9] = 
{
	MatchCollection_t3718216671::get_offset_of__regex_0(),
	MatchCollection_t3718216671::get_offset_of__matches_1(),
	MatchCollection_t3718216671::get_offset_of__done_2(),
	MatchCollection_t3718216671::get_offset_of__input_3(),
	MatchCollection_t3718216671::get_offset_of__beginning_4(),
	MatchCollection_t3718216671::get_offset_of__length_5(),
	MatchCollection_t3718216671::get_offset_of__startat_6(),
	MatchCollection_t3718216671::get_offset_of__prevlen_7(),
	MatchCollection_t3718216671_StaticFields::get_offset_of_infinite_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
