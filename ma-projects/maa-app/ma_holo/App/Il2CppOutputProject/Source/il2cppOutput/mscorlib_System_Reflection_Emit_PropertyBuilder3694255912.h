﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_PropertyAttributes883448530.h"
#include "mscorlib_System_Reflection_CallingConventions1097349142.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Object
struct Il2CppObject;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t644187984;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2318378278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.PropertyBuilder
struct  PropertyBuilder_t3694255912  : public PropertyInfo_t
{
public:
	// System.Reflection.PropertyAttributes System.Reflection.Emit.PropertyBuilder::attrs
	int32_t ___attrs_0;
	// System.String System.Reflection.Emit.PropertyBuilder::name
	String_t* ___name_1;
	// System.Type System.Reflection.Emit.PropertyBuilder::type
	Type_t * ___type_2;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::parameters
	TypeU5BU5D_t1664964607* ___parameters_3;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.PropertyBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_4;
	// System.Object System.Reflection.Emit.PropertyBuilder::def_value
	Il2CppObject * ___def_value_5;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.PropertyBuilder::set_method
	MethodBuilder_t644187984 * ___set_method_6;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.PropertyBuilder::get_method
	MethodBuilder_t644187984 * ___get_method_7;
	// System.Int32 System.Reflection.Emit.PropertyBuilder::table_idx
	int32_t ___table_idx_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.PropertyBuilder::typeb
	TypeBuilder_t3308873219 * ___typeb_9;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::returnModReq
	TypeU5BU5D_t1664964607* ___returnModReq_10;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::returnModOpt
	TypeU5BU5D_t1664964607* ___returnModOpt_11;
	// System.Type[][] System.Reflection.Emit.PropertyBuilder::paramModReq
	TypeU5BU5DU5BU5D_t2318378278* ___paramModReq_12;
	// System.Type[][] System.Reflection.Emit.PropertyBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t2318378278* ___paramModOpt_13;
	// System.Reflection.CallingConventions System.Reflection.Emit.PropertyBuilder::callingConvention
	int32_t ___callingConvention_14;

public:
	inline static int32_t get_offset_of_attrs_0() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___attrs_0)); }
	inline int32_t get_attrs_0() const { return ___attrs_0; }
	inline int32_t* get_address_of_attrs_0() { return &___attrs_0; }
	inline void set_attrs_0(int32_t value)
	{
		___attrs_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___parameters_3)); }
	inline TypeU5BU5D_t1664964607* get_parameters_3() const { return ___parameters_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(TypeU5BU5D_t1664964607* value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_3, value);
	}

	inline static int32_t get_offset_of_cattrs_4() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___cattrs_4)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_4() const { return ___cattrs_4; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_4() { return &___cattrs_4; }
	inline void set_cattrs_4(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_4 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_4, value);
	}

	inline static int32_t get_offset_of_def_value_5() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___def_value_5)); }
	inline Il2CppObject * get_def_value_5() const { return ___def_value_5; }
	inline Il2CppObject ** get_address_of_def_value_5() { return &___def_value_5; }
	inline void set_def_value_5(Il2CppObject * value)
	{
		___def_value_5 = value;
		Il2CppCodeGenWriteBarrier(&___def_value_5, value);
	}

	inline static int32_t get_offset_of_set_method_6() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___set_method_6)); }
	inline MethodBuilder_t644187984 * get_set_method_6() const { return ___set_method_6; }
	inline MethodBuilder_t644187984 ** get_address_of_set_method_6() { return &___set_method_6; }
	inline void set_set_method_6(MethodBuilder_t644187984 * value)
	{
		___set_method_6 = value;
		Il2CppCodeGenWriteBarrier(&___set_method_6, value);
	}

	inline static int32_t get_offset_of_get_method_7() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___get_method_7)); }
	inline MethodBuilder_t644187984 * get_get_method_7() const { return ___get_method_7; }
	inline MethodBuilder_t644187984 ** get_address_of_get_method_7() { return &___get_method_7; }
	inline void set_get_method_7(MethodBuilder_t644187984 * value)
	{
		___get_method_7 = value;
		Il2CppCodeGenWriteBarrier(&___get_method_7, value);
	}

	inline static int32_t get_offset_of_table_idx_8() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___table_idx_8)); }
	inline int32_t get_table_idx_8() const { return ___table_idx_8; }
	inline int32_t* get_address_of_table_idx_8() { return &___table_idx_8; }
	inline void set_table_idx_8(int32_t value)
	{
		___table_idx_8 = value;
	}

	inline static int32_t get_offset_of_typeb_9() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___typeb_9)); }
	inline TypeBuilder_t3308873219 * get_typeb_9() const { return ___typeb_9; }
	inline TypeBuilder_t3308873219 ** get_address_of_typeb_9() { return &___typeb_9; }
	inline void set_typeb_9(TypeBuilder_t3308873219 * value)
	{
		___typeb_9 = value;
		Il2CppCodeGenWriteBarrier(&___typeb_9, value);
	}

	inline static int32_t get_offset_of_returnModReq_10() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___returnModReq_10)); }
	inline TypeU5BU5D_t1664964607* get_returnModReq_10() const { return ___returnModReq_10; }
	inline TypeU5BU5D_t1664964607** get_address_of_returnModReq_10() { return &___returnModReq_10; }
	inline void set_returnModReq_10(TypeU5BU5D_t1664964607* value)
	{
		___returnModReq_10 = value;
		Il2CppCodeGenWriteBarrier(&___returnModReq_10, value);
	}

	inline static int32_t get_offset_of_returnModOpt_11() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___returnModOpt_11)); }
	inline TypeU5BU5D_t1664964607* get_returnModOpt_11() const { return ___returnModOpt_11; }
	inline TypeU5BU5D_t1664964607** get_address_of_returnModOpt_11() { return &___returnModOpt_11; }
	inline void set_returnModOpt_11(TypeU5BU5D_t1664964607* value)
	{
		___returnModOpt_11 = value;
		Il2CppCodeGenWriteBarrier(&___returnModOpt_11, value);
	}

	inline static int32_t get_offset_of_paramModReq_12() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___paramModReq_12)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModReq_12() const { return ___paramModReq_12; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModReq_12() { return &___paramModReq_12; }
	inline void set_paramModReq_12(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModReq_12 = value;
		Il2CppCodeGenWriteBarrier(&___paramModReq_12, value);
	}

	inline static int32_t get_offset_of_paramModOpt_13() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___paramModOpt_13)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModOpt_13() const { return ___paramModOpt_13; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModOpt_13() { return &___paramModOpt_13; }
	inline void set_paramModOpt_13(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModOpt_13 = value;
		Il2CppCodeGenWriteBarrier(&___paramModOpt_13, value);
	}

	inline static int32_t get_offset_of_callingConvention_14() { return static_cast<int32_t>(offsetof(PropertyBuilder_t3694255912, ___callingConvention_14)); }
	inline int32_t get_callingConvention_14() const { return ___callingConvention_14; }
	inline int32_t* get_address_of_callingConvention_14() { return &___callingConvention_14; }
	inline void set_callingConvention_14(int32_t value)
	{
		___callingConvention_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
