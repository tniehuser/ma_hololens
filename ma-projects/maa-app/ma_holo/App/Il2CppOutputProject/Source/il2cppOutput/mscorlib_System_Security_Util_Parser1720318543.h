﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.SecurityDocument
struct SecurityDocument_t3709171241;
// System.Security.Util.Tokenizer
struct Tokenizer_t395826351;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.Parser
struct  Parser_t1720318543  : public Il2CppObject
{
public:
	// System.Security.SecurityDocument System.Security.Util.Parser::_doc
	SecurityDocument_t3709171241 * ____doc_0;
	// System.Security.Util.Tokenizer System.Security.Util.Parser::_t
	Tokenizer_t395826351 * ____t_1;

public:
	inline static int32_t get_offset_of__doc_0() { return static_cast<int32_t>(offsetof(Parser_t1720318543, ____doc_0)); }
	inline SecurityDocument_t3709171241 * get__doc_0() const { return ____doc_0; }
	inline SecurityDocument_t3709171241 ** get_address_of__doc_0() { return &____doc_0; }
	inline void set__doc_0(SecurityDocument_t3709171241 * value)
	{
		____doc_0 = value;
		Il2CppCodeGenWriteBarrier(&____doc_0, value);
	}

	inline static int32_t get_offset_of__t_1() { return static_cast<int32_t>(offsetof(Parser_t1720318543, ____t_1)); }
	inline Tokenizer_t395826351 * get__t_1() const { return ____t_1; }
	inline Tokenizer_t395826351 ** get_address_of__t_1() { return &____t_1; }
	inline void set__t_1(Tokenizer_t395826351 * value)
	{
		____t_1 = value;
		Il2CppCodeGenWriteBarrier(&____t_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
