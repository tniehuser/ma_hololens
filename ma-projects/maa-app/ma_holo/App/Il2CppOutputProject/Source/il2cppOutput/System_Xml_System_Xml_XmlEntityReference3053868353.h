﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"

// System.String
struct String_t;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t3053868353  : public XmlLinkedNode_t1287616130
{
public:
	// System.String System.Xml.XmlEntityReference::name
	String_t* ___name_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastChild
	XmlLinkedNode_t1287616130 * ___lastChild_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3053868353, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3053868353, ___lastChild_3)); }
	inline XmlLinkedNode_t1287616130 * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t1287616130 * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastChild_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
