﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryAssembly
struct  BinaryAssembly_t3375684671  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryAssembly::assemId
	int32_t ___assemId_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryAssembly::assemblyString
	String_t* ___assemblyString_1;

public:
	inline static int32_t get_offset_of_assemId_0() { return static_cast<int32_t>(offsetof(BinaryAssembly_t3375684671, ___assemId_0)); }
	inline int32_t get_assemId_0() const { return ___assemId_0; }
	inline int32_t* get_address_of_assemId_0() { return &___assemId_0; }
	inline void set_assemId_0(int32_t value)
	{
		___assemId_0 = value;
	}

	inline static int32_t get_offset_of_assemblyString_1() { return static_cast<int32_t>(offsetof(BinaryAssembly_t3375684671, ___assemblyString_1)); }
	inline String_t* get_assemblyString_1() const { return ___assemblyString_1; }
	inline String_t** get_address_of_assemblyString_1() { return &___assemblyString_1; }
	inline void set_assemblyString_1(String_t* value)
	{
		___assemblyString_1 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyString_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
