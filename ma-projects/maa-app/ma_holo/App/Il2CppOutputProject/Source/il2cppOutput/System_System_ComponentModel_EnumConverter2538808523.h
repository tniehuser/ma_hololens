﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter745995970.h"

// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t191679357;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EnumConverter
struct  EnumConverter_t2538808523  : public TypeConverter_t745995970
{
public:
	// System.ComponentModel.TypeConverter/StandardValuesCollection System.ComponentModel.EnumConverter::values
	StandardValuesCollection_t191679357 * ___values_4;
	// System.Type System.ComponentModel.EnumConverter::type
	Type_t * ___type_5;

public:
	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(EnumConverter_t2538808523, ___values_4)); }
	inline StandardValuesCollection_t191679357 * get_values_4() const { return ___values_4; }
	inline StandardValuesCollection_t191679357 ** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StandardValuesCollection_t191679357 * value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier(&___values_4, value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(EnumConverter_t2538808523, ___type_5)); }
	inline Type_t * get_type_5() const { return ___type_5; }
	inline Type_t ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(Type_t * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
