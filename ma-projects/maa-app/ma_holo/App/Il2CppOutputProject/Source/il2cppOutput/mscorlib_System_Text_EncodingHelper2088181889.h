﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.Object
struct Il2CppObject;
// System.Reflection.Assembly
struct Assembly_t4268412390;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncodingHelper
struct  EncodingHelper_t2088181889  : public Il2CppObject
{
public:

public:
};

struct EncodingHelper_t2088181889_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncodingHelper::utf8EncodingWithoutMarkers
	Encoding_t663144255 * ___utf8EncodingWithoutMarkers_0;
	// System.Object System.Text.EncodingHelper::lockobj
	Il2CppObject * ___lockobj_1;
	// System.Reflection.Assembly System.Text.EncodingHelper::i18nAssembly
	Assembly_t4268412390 * ___i18nAssembly_2;
	// System.Boolean System.Text.EncodingHelper::i18nDisabled
	bool ___i18nDisabled_3;

public:
	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_0() { return static_cast<int32_t>(offsetof(EncodingHelper_t2088181889_StaticFields, ___utf8EncodingWithoutMarkers_0)); }
	inline Encoding_t663144255 * get_utf8EncodingWithoutMarkers_0() const { return ___utf8EncodingWithoutMarkers_0; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingWithoutMarkers_0() { return &___utf8EncodingWithoutMarkers_0; }
	inline void set_utf8EncodingWithoutMarkers_0(Encoding_t663144255 * value)
	{
		___utf8EncodingWithoutMarkers_0 = value;
		Il2CppCodeGenWriteBarrier(&___utf8EncodingWithoutMarkers_0, value);
	}

	inline static int32_t get_offset_of_lockobj_1() { return static_cast<int32_t>(offsetof(EncodingHelper_t2088181889_StaticFields, ___lockobj_1)); }
	inline Il2CppObject * get_lockobj_1() const { return ___lockobj_1; }
	inline Il2CppObject ** get_address_of_lockobj_1() { return &___lockobj_1; }
	inline void set_lockobj_1(Il2CppObject * value)
	{
		___lockobj_1 = value;
		Il2CppCodeGenWriteBarrier(&___lockobj_1, value);
	}

	inline static int32_t get_offset_of_i18nAssembly_2() { return static_cast<int32_t>(offsetof(EncodingHelper_t2088181889_StaticFields, ___i18nAssembly_2)); }
	inline Assembly_t4268412390 * get_i18nAssembly_2() const { return ___i18nAssembly_2; }
	inline Assembly_t4268412390 ** get_address_of_i18nAssembly_2() { return &___i18nAssembly_2; }
	inline void set_i18nAssembly_2(Assembly_t4268412390 * value)
	{
		___i18nAssembly_2 = value;
		Il2CppCodeGenWriteBarrier(&___i18nAssembly_2, value);
	}

	inline static int32_t get_offset_of_i18nDisabled_3() { return static_cast<int32_t>(offsetof(EncodingHelper_t2088181889_StaticFields, ___i18nDisabled_3)); }
	inline bool get_i18nDisabled_3() const { return ___i18nDisabled_3; }
	inline bool* get_address_of_i18nDisabled_3() { return &___i18nDisabled_3; }
	inline void set_i18nDisabled_3(bool value)
	{
		___i18nDisabled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
