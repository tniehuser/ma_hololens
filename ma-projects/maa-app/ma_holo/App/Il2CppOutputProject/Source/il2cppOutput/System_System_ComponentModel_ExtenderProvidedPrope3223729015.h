﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"

// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t4250402154;
// System.ComponentModel.IExtenderProvider
struct IExtenderProvider_t791831889;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExtenderProvidedPropertyAttribute
struct  ExtenderProvidedPropertyAttribute_t3223729015  : public Attribute_t542643598
{
public:
	// System.ComponentModel.PropertyDescriptor System.ComponentModel.ExtenderProvidedPropertyAttribute::extenderProperty
	PropertyDescriptor_t4250402154 * ___extenderProperty_0;
	// System.ComponentModel.IExtenderProvider System.ComponentModel.ExtenderProvidedPropertyAttribute::provider
	Il2CppObject * ___provider_1;
	// System.Type System.ComponentModel.ExtenderProvidedPropertyAttribute::receiverType
	Type_t * ___receiverType_2;

public:
	inline static int32_t get_offset_of_extenderProperty_0() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t3223729015, ___extenderProperty_0)); }
	inline PropertyDescriptor_t4250402154 * get_extenderProperty_0() const { return ___extenderProperty_0; }
	inline PropertyDescriptor_t4250402154 ** get_address_of_extenderProperty_0() { return &___extenderProperty_0; }
	inline void set_extenderProperty_0(PropertyDescriptor_t4250402154 * value)
	{
		___extenderProperty_0 = value;
		Il2CppCodeGenWriteBarrier(&___extenderProperty_0, value);
	}

	inline static int32_t get_offset_of_provider_1() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t3223729015, ___provider_1)); }
	inline Il2CppObject * get_provider_1() const { return ___provider_1; }
	inline Il2CppObject ** get_address_of_provider_1() { return &___provider_1; }
	inline void set_provider_1(Il2CppObject * value)
	{
		___provider_1 = value;
		Il2CppCodeGenWriteBarrier(&___provider_1, value);
	}

	inline static int32_t get_offset_of_receiverType_2() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t3223729015, ___receiverType_2)); }
	inline Type_t * get_receiverType_2() const { return ___receiverType_2; }
	inline Type_t ** get_address_of_receiverType_2() { return &___receiverType_2; }
	inline void set_receiverType_2(Type_t * value)
	{
		___receiverType_2 = value;
		Il2CppCodeGenWriteBarrier(&___receiverType_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
