﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Object
struct Il2CppObject;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t3857790437;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t1795361321;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationCallbackInfo
struct  CancellationCallbackInfo_t1473383178  : public Il2CppObject
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationCallbackInfo::Callback
	Action_1_t2491248677 * ___Callback_0;
	// System.Object System.Threading.CancellationCallbackInfo::StateForCallback
	Il2CppObject * ___StateForCallback_1;
	// System.Threading.SynchronizationContext System.Threading.CancellationCallbackInfo::TargetSyncContext
	SynchronizationContext_t3857790437 * ___TargetSyncContext_2;
	// System.Threading.ExecutionContext System.Threading.CancellationCallbackInfo::TargetExecutionContext
	ExecutionContext_t1392266323 * ___TargetExecutionContext_3;
	// System.Threading.CancellationTokenSource System.Threading.CancellationCallbackInfo::CancellationTokenSource
	CancellationTokenSource_t1795361321 * ___CancellationTokenSource_4;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178, ___Callback_0)); }
	inline Action_1_t2491248677 * get_Callback_0() const { return ___Callback_0; }
	inline Action_1_t2491248677 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Action_1_t2491248677 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_0, value);
	}

	inline static int32_t get_offset_of_StateForCallback_1() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178, ___StateForCallback_1)); }
	inline Il2CppObject * get_StateForCallback_1() const { return ___StateForCallback_1; }
	inline Il2CppObject ** get_address_of_StateForCallback_1() { return &___StateForCallback_1; }
	inline void set_StateForCallback_1(Il2CppObject * value)
	{
		___StateForCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___StateForCallback_1, value);
	}

	inline static int32_t get_offset_of_TargetSyncContext_2() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178, ___TargetSyncContext_2)); }
	inline SynchronizationContext_t3857790437 * get_TargetSyncContext_2() const { return ___TargetSyncContext_2; }
	inline SynchronizationContext_t3857790437 ** get_address_of_TargetSyncContext_2() { return &___TargetSyncContext_2; }
	inline void set_TargetSyncContext_2(SynchronizationContext_t3857790437 * value)
	{
		___TargetSyncContext_2 = value;
		Il2CppCodeGenWriteBarrier(&___TargetSyncContext_2, value);
	}

	inline static int32_t get_offset_of_TargetExecutionContext_3() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178, ___TargetExecutionContext_3)); }
	inline ExecutionContext_t1392266323 * get_TargetExecutionContext_3() const { return ___TargetExecutionContext_3; }
	inline ExecutionContext_t1392266323 ** get_address_of_TargetExecutionContext_3() { return &___TargetExecutionContext_3; }
	inline void set_TargetExecutionContext_3(ExecutionContext_t1392266323 * value)
	{
		___TargetExecutionContext_3 = value;
		Il2CppCodeGenWriteBarrier(&___TargetExecutionContext_3, value);
	}

	inline static int32_t get_offset_of_CancellationTokenSource_4() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178, ___CancellationTokenSource_4)); }
	inline CancellationTokenSource_t1795361321 * get_CancellationTokenSource_4() const { return ___CancellationTokenSource_4; }
	inline CancellationTokenSource_t1795361321 ** get_address_of_CancellationTokenSource_4() { return &___CancellationTokenSource_4; }
	inline void set_CancellationTokenSource_4(CancellationTokenSource_t1795361321 * value)
	{
		___CancellationTokenSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___CancellationTokenSource_4, value);
	}
};

struct CancellationCallbackInfo_t1473383178_StaticFields
{
public:
	// System.Threading.ContextCallback System.Threading.CancellationCallbackInfo::s_executionContextCallback
	ContextCallback_t2287130692 * ___s_executionContextCallback_5;

public:
	inline static int32_t get_offset_of_s_executionContextCallback_5() { return static_cast<int32_t>(offsetof(CancellationCallbackInfo_t1473383178_StaticFields, ___s_executionContextCallback_5)); }
	inline ContextCallback_t2287130692 * get_s_executionContextCallback_5() const { return ___s_executionContextCallback_5; }
	inline ContextCallback_t2287130692 ** get_address_of_s_executionContextCallback_5() { return &___s_executionContextCallback_5; }
	inline void set_s_executionContextCallback_5(ContextCallback_t2287130692 * value)
	{
		___s_executionContextCallback_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_executionContextCallback_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
