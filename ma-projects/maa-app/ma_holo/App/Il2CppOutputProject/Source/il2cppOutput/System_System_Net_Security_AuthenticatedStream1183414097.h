﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.IO.Stream
struct Stream_t3255436806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticatedStream
struct  AuthenticatedStream_t1183414097  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.Net.Security.AuthenticatedStream::_InnerStream
	Stream_t3255436806 * ____InnerStream_8;
	// System.Boolean System.Net.Security.AuthenticatedStream::_LeaveStreamOpen
	bool ____LeaveStreamOpen_9;

public:
	inline static int32_t get_offset_of__InnerStream_8() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t1183414097, ____InnerStream_8)); }
	inline Stream_t3255436806 * get__InnerStream_8() const { return ____InnerStream_8; }
	inline Stream_t3255436806 ** get_address_of__InnerStream_8() { return &____InnerStream_8; }
	inline void set__InnerStream_8(Stream_t3255436806 * value)
	{
		____InnerStream_8 = value;
		Il2CppCodeGenWriteBarrier(&____InnerStream_8, value);
	}

	inline static int32_t get_offset_of__LeaveStreamOpen_9() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t1183414097, ____LeaveStreamOpen_9)); }
	inline bool get__LeaveStreamOpen_9() const { return ____LeaveStreamOpen_9; }
	inline bool* get_address_of__LeaveStreamOpen_9() { return &____LeaveStreamOpen_9; }
	inline void set__LeaveStreamOpen_9(bool value)
	{
		____LeaveStreamOpen_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
