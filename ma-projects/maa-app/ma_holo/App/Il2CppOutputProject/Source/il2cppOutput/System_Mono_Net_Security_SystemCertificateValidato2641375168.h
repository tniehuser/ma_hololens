﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Security_Cryptography_X509Certificat2065307963.h"
#include "System_System_Security_Cryptography_X509Certificat2461349531.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.SystemCertificateValidator
struct  SystemCertificateValidator_t2641375168  : public Il2CppObject
{
public:

public:
};

struct SystemCertificateValidator_t2641375168_StaticFields
{
public:
	// System.Boolean Mono.Net.Security.SystemCertificateValidator::is_macosx
	bool ___is_macosx_0;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode Mono.Net.Security.SystemCertificateValidator::revocation_mode
	int32_t ___revocation_mode_1;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags Mono.Net.Security.SystemCertificateValidator::s_flags
	int32_t ___s_flags_2;

public:
	inline static int32_t get_offset_of_is_macosx_0() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___is_macosx_0)); }
	inline bool get_is_macosx_0() const { return ___is_macosx_0; }
	inline bool* get_address_of_is_macosx_0() { return &___is_macosx_0; }
	inline void set_is_macosx_0(bool value)
	{
		___is_macosx_0 = value;
	}

	inline static int32_t get_offset_of_revocation_mode_1() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___revocation_mode_1)); }
	inline int32_t get_revocation_mode_1() const { return ___revocation_mode_1; }
	inline int32_t* get_address_of_revocation_mode_1() { return &___revocation_mode_1; }
	inline void set_revocation_mode_1(int32_t value)
	{
		___revocation_mode_1 = value;
	}

	inline static int32_t get_offset_of_s_flags_2() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___s_flags_2)); }
	inline int32_t get_s_flags_2() const { return ___s_flags_2; }
	inline int32_t* get_address_of_s_flags_2() { return &___s_flags_2; }
	inline void set_s_flags_2(int32_t value)
	{
		___s_flags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
