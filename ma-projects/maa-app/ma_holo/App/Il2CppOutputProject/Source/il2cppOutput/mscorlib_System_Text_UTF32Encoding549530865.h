﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Encoding663144255.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.UTF32Encoding
struct  UTF32Encoding_t549530865  : public Encoding_t663144255
{
public:
	// System.Boolean System.Text.UTF32Encoding::emitUTF32ByteOrderMark
	bool ___emitUTF32ByteOrderMark_15;
	// System.Boolean System.Text.UTF32Encoding::isThrowException
	bool ___isThrowException_16;
	// System.Boolean System.Text.UTF32Encoding::bigEndian
	bool ___bigEndian_17;

public:
	inline static int32_t get_offset_of_emitUTF32ByteOrderMark_15() { return static_cast<int32_t>(offsetof(UTF32Encoding_t549530865, ___emitUTF32ByteOrderMark_15)); }
	inline bool get_emitUTF32ByteOrderMark_15() const { return ___emitUTF32ByteOrderMark_15; }
	inline bool* get_address_of_emitUTF32ByteOrderMark_15() { return &___emitUTF32ByteOrderMark_15; }
	inline void set_emitUTF32ByteOrderMark_15(bool value)
	{
		___emitUTF32ByteOrderMark_15 = value;
	}

	inline static int32_t get_offset_of_isThrowException_16() { return static_cast<int32_t>(offsetof(UTF32Encoding_t549530865, ___isThrowException_16)); }
	inline bool get_isThrowException_16() const { return ___isThrowException_16; }
	inline bool* get_address_of_isThrowException_16() { return &___isThrowException_16; }
	inline void set_isThrowException_16(bool value)
	{
		___isThrowException_16 = value;
	}

	inline static int32_t get_offset_of_bigEndian_17() { return static_cast<int32_t>(offsetof(UTF32Encoding_t549530865, ___bigEndian_17)); }
	inline bool get_bigEndian_17() const { return ___bigEndian_17; }
	inline bool* get_address_of_bigEndian_17() { return &___bigEndian_17; }
	inline void set_bigEndian_17(bool value)
	{
		___bigEndian_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
