﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate
struct DictationHypothesisDelegate_t1495849926;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate
struct DictationResultDelegate_t1941514337;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate
struct DictationCompletedDelegate_t3326551541;
// UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler
struct DictationErrorHandler_t3730830311;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Windows.Speech.DictationRecognizer
struct  DictationRecognizer_t894834159  : public Il2CppObject
{
public:
	// System.IntPtr UnityEngine.Windows.Speech.DictationRecognizer::m_Recognizer
	IntPtr_t ___m_Recognizer_0;
	// UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate UnityEngine.Windows.Speech.DictationRecognizer::DictationHypothesis
	DictationHypothesisDelegate_t1495849926 * ___DictationHypothesis_1;
	// UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate UnityEngine.Windows.Speech.DictationRecognizer::DictationResult
	DictationResultDelegate_t1941514337 * ___DictationResult_2;
	// UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate UnityEngine.Windows.Speech.DictationRecognizer::DictationComplete
	DictationCompletedDelegate_t3326551541 * ___DictationComplete_3;
	// UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler UnityEngine.Windows.Speech.DictationRecognizer::DictationError
	DictationErrorHandler_t3730830311 * ___DictationError_4;

public:
	inline static int32_t get_offset_of_m_Recognizer_0() { return static_cast<int32_t>(offsetof(DictationRecognizer_t894834159, ___m_Recognizer_0)); }
	inline IntPtr_t get_m_Recognizer_0() const { return ___m_Recognizer_0; }
	inline IntPtr_t* get_address_of_m_Recognizer_0() { return &___m_Recognizer_0; }
	inline void set_m_Recognizer_0(IntPtr_t value)
	{
		___m_Recognizer_0 = value;
	}

	inline static int32_t get_offset_of_DictationHypothesis_1() { return static_cast<int32_t>(offsetof(DictationRecognizer_t894834159, ___DictationHypothesis_1)); }
	inline DictationHypothesisDelegate_t1495849926 * get_DictationHypothesis_1() const { return ___DictationHypothesis_1; }
	inline DictationHypothesisDelegate_t1495849926 ** get_address_of_DictationHypothesis_1() { return &___DictationHypothesis_1; }
	inline void set_DictationHypothesis_1(DictationHypothesisDelegate_t1495849926 * value)
	{
		___DictationHypothesis_1 = value;
		Il2CppCodeGenWriteBarrier(&___DictationHypothesis_1, value);
	}

	inline static int32_t get_offset_of_DictationResult_2() { return static_cast<int32_t>(offsetof(DictationRecognizer_t894834159, ___DictationResult_2)); }
	inline DictationResultDelegate_t1941514337 * get_DictationResult_2() const { return ___DictationResult_2; }
	inline DictationResultDelegate_t1941514337 ** get_address_of_DictationResult_2() { return &___DictationResult_2; }
	inline void set_DictationResult_2(DictationResultDelegate_t1941514337 * value)
	{
		___DictationResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___DictationResult_2, value);
	}

	inline static int32_t get_offset_of_DictationComplete_3() { return static_cast<int32_t>(offsetof(DictationRecognizer_t894834159, ___DictationComplete_3)); }
	inline DictationCompletedDelegate_t3326551541 * get_DictationComplete_3() const { return ___DictationComplete_3; }
	inline DictationCompletedDelegate_t3326551541 ** get_address_of_DictationComplete_3() { return &___DictationComplete_3; }
	inline void set_DictationComplete_3(DictationCompletedDelegate_t3326551541 * value)
	{
		___DictationComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___DictationComplete_3, value);
	}

	inline static int32_t get_offset_of_DictationError_4() { return static_cast<int32_t>(offsetof(DictationRecognizer_t894834159, ___DictationError_4)); }
	inline DictationErrorHandler_t3730830311 * get_DictationError_4() const { return ___DictationError_4; }
	inline DictationErrorHandler_t3730830311 ** get_address_of_DictationError_4() { return &___DictationError_4; }
	inline void set_DictationError_4(DictationErrorHandler_t3730830311 * value)
	{
		___DictationError_4 = value;
		Il2CppCodeGenWriteBarrier(&___DictationError_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
