﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlListConverter888285589.h"

// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlUntypedConverter
struct  XmlUntypedConverter_t3998072860  : public XmlListConverter_t888285589
{
public:
	// System.Boolean System.Xml.Schema.XmlUntypedConverter::allowListToList
	bool ___allowListToList_33;

public:
	inline static int32_t get_offset_of_allowListToList_33() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t3998072860, ___allowListToList_33)); }
	inline bool get_allowListToList_33() const { return ___allowListToList_33; }
	inline bool* get_address_of_allowListToList_33() { return &___allowListToList_33; }
	inline void set_allowListToList_33(bool value)
	{
		___allowListToList_33 = value;
	}
};

struct XmlUntypedConverter_t3998072860_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlUntypedConverter::Untyped
	XmlValueConverter_t68179724 * ___Untyped_34;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlUntypedConverter::UntypedList
	XmlValueConverter_t68179724 * ___UntypedList_35;

public:
	inline static int32_t get_offset_of_Untyped_34() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t3998072860_StaticFields, ___Untyped_34)); }
	inline XmlValueConverter_t68179724 * get_Untyped_34() const { return ___Untyped_34; }
	inline XmlValueConverter_t68179724 ** get_address_of_Untyped_34() { return &___Untyped_34; }
	inline void set_Untyped_34(XmlValueConverter_t68179724 * value)
	{
		___Untyped_34 = value;
		Il2CppCodeGenWriteBarrier(&___Untyped_34, value);
	}

	inline static int32_t get_offset_of_UntypedList_35() { return static_cast<int32_t>(offsetof(XmlUntypedConverter_t3998072860_StaticFields, ___UntypedList_35)); }
	inline XmlValueConverter_t68179724 * get_UntypedList_35() const { return ___UntypedList_35; }
	inline XmlValueConverter_t68179724 ** get_address_of_UntypedList_35() { return &___UntypedList_35; }
	inline void set_UntypedList_35(XmlValueConverter_t68179724 * value)
	{
		___UntypedList_35 = value;
		Il2CppCodeGenWriteBarrier(&___UntypedList_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
