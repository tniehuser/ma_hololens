﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.OperatingSystem
struct OperatingSystem_t290860502;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Environment
struct  Environment_t3662374671  : public Il2CppObject
{
public:

public:
};

struct Environment_t3662374671_StaticFields
{
public:
	// System.String System.Environment::nl
	String_t* ___nl_1;
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t290860502 * ___os_2;

public:
	inline static int32_t get_offset_of_nl_1() { return static_cast<int32_t>(offsetof(Environment_t3662374671_StaticFields, ___nl_1)); }
	inline String_t* get_nl_1() const { return ___nl_1; }
	inline String_t** get_address_of_nl_1() { return &___nl_1; }
	inline void set_nl_1(String_t* value)
	{
		___nl_1 = value;
		Il2CppCodeGenWriteBarrier(&___nl_1, value);
	}

	inline static int32_t get_offset_of_os_2() { return static_cast<int32_t>(offsetof(Environment_t3662374671_StaticFields, ___os_2)); }
	inline OperatingSystem_t290860502 * get_os_2() const { return ___os_2; }
	inline OperatingSystem_t290860502 ** get_address_of_os_2() { return &___os_2; }
	inline void set_os_2(OperatingSystem_t290860502 * value)
	{
		___os_2 = value;
		Il2CppCodeGenWriteBarrier(&___os_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
