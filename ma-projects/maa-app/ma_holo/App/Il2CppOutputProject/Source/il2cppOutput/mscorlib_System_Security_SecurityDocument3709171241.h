﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SecurityDocument
struct  SecurityDocument_t3709171241  : public Il2CppObject
{
public:
	// System.Byte[] System.Security.SecurityDocument::m_data
	ByteU5BU5D_t3397334013* ___m_data_0;

public:
	inline static int32_t get_offset_of_m_data_0() { return static_cast<int32_t>(offsetof(SecurityDocument_t3709171241, ___m_data_0)); }
	inline ByteU5BU5D_t3397334013* get_m_data_0() const { return ___m_data_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_data_0() { return &___m_data_0; }
	inline void set_m_data_0(ByteU5BU5D_t3397334013* value)
	{
		___m_data_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
