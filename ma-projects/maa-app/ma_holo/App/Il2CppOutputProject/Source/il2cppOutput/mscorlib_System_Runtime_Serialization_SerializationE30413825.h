﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t2699667469;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEvents
struct  SerializationEvents_t30413825  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnSerializingMethods
	List_1_t2699667469 * ___m_OnSerializingMethods_0;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnSerializedMethods
	List_1_t2699667469 * ___m_OnSerializedMethods_1;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnDeserializingMethods
	List_1_t2699667469 * ___m_OnDeserializingMethods_2;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnDeserializedMethods
	List_1_t2699667469 * ___m_OnDeserializedMethods_3;

public:
	inline static int32_t get_offset_of_m_OnSerializingMethods_0() { return static_cast<int32_t>(offsetof(SerializationEvents_t30413825, ___m_OnSerializingMethods_0)); }
	inline List_1_t2699667469 * get_m_OnSerializingMethods_0() const { return ___m_OnSerializingMethods_0; }
	inline List_1_t2699667469 ** get_address_of_m_OnSerializingMethods_0() { return &___m_OnSerializingMethods_0; }
	inline void set_m_OnSerializingMethods_0(List_1_t2699667469 * value)
	{
		___m_OnSerializingMethods_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnSerializingMethods_0, value);
	}

	inline static int32_t get_offset_of_m_OnSerializedMethods_1() { return static_cast<int32_t>(offsetof(SerializationEvents_t30413825, ___m_OnSerializedMethods_1)); }
	inline List_1_t2699667469 * get_m_OnSerializedMethods_1() const { return ___m_OnSerializedMethods_1; }
	inline List_1_t2699667469 ** get_address_of_m_OnSerializedMethods_1() { return &___m_OnSerializedMethods_1; }
	inline void set_m_OnSerializedMethods_1(List_1_t2699667469 * value)
	{
		___m_OnSerializedMethods_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnSerializedMethods_1, value);
	}

	inline static int32_t get_offset_of_m_OnDeserializingMethods_2() { return static_cast<int32_t>(offsetof(SerializationEvents_t30413825, ___m_OnDeserializingMethods_2)); }
	inline List_1_t2699667469 * get_m_OnDeserializingMethods_2() const { return ___m_OnDeserializingMethods_2; }
	inline List_1_t2699667469 ** get_address_of_m_OnDeserializingMethods_2() { return &___m_OnDeserializingMethods_2; }
	inline void set_m_OnDeserializingMethods_2(List_1_t2699667469 * value)
	{
		___m_OnDeserializingMethods_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnDeserializingMethods_2, value);
	}

	inline static int32_t get_offset_of_m_OnDeserializedMethods_3() { return static_cast<int32_t>(offsetof(SerializationEvents_t30413825, ___m_OnDeserializedMethods_3)); }
	inline List_1_t2699667469 * get_m_OnDeserializedMethods_3() const { return ___m_OnDeserializedMethods_3; }
	inline List_1_t2699667469 ** get_address_of_m_OnDeserializedMethods_3() { return &___m_OnDeserializedMethods_3; }
	inline void set_m_OnDeserializedMethods_3(List_1_t2699667469 * value)
	{
		___m_OnDeserializedMethods_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnDeserializedMethods_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
