﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeStructs_GenericParamInfo1377222196.h"

// Mono.RuntimeStructs/GenericParamInfo
struct GenericParamInfo_t1377222196;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeGenericParamInfoHandle
struct  RuntimeGenericParamInfoHandle_t580017048 
{
public:
	// Mono.RuntimeStructs/GenericParamInfo* Mono.RuntimeGenericParamInfoHandle::value
	GenericParamInfo_t1377222196 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeGenericParamInfoHandle_t580017048, ___value_0)); }
	inline GenericParamInfo_t1377222196 * get_value_0() const { return ___value_0; }
	inline GenericParamInfo_t1377222196 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(GenericParamInfo_t1377222196 * value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeGenericParamInfoHandle
struct RuntimeGenericParamInfoHandle_t580017048_marshaled_pinvoke
{
	GenericParamInfo_t1377222196 * ___value_0;
};
// Native definition for COM marshalling of Mono.RuntimeGenericParamInfoHandle
struct RuntimeGenericParamInfoHandle_t580017048_marshaled_com
{
	GenericParamInfo_t1377222196 * ___value_0;
};
