﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.PerfCounterSection
struct  PerfCounterSection_t3986468662  : public ConfigurationElement_t1776195828
{
public:

public:
};

struct PerfCounterSection_t3986468662_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.PerfCounterSection::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.PerfCounterSection::_propFileMappingSize
	ConfigurationProperty_t2048066811 * ____propFileMappingSize_16;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(PerfCounterSection_t3986468662_StaticFields, ____properties_15)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}

	inline static int32_t get_offset_of__propFileMappingSize_16() { return static_cast<int32_t>(offsetof(PerfCounterSection_t3986468662_StaticFields, ____propFileMappingSize_16)); }
	inline ConfigurationProperty_t2048066811 * get__propFileMappingSize_16() const { return ____propFileMappingSize_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propFileMappingSize_16() { return &____propFileMappingSize_16; }
	inline void set__propFileMappingSize_16(ConfigurationProperty_t2048066811 * value)
	{
		____propFileMappingSize_16 = value;
		Il2CppCodeGenWriteBarrier(&____propFileMappingSize_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
