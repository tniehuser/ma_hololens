﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDe2069522403.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu3527426713.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribu3321178844.h"
#include "System_Xml_System_Xml_ValidateNames208250372.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"
#include "System_Xml_System_Xml_XmlComplianceUtil976628708.h"
#include "System_Xml_System_Xml_ExceptionType1261777571.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlDownloadManager830495394.h"
#include "System_Xml_System_Xml_XmlDownloadManager_U3CGetStr4234648579.h"
#include "System_Xml_System_Xml_XmlDownloadManager_U3CGetNon1529316717.h"
#include "System_Xml_System_Xml_OpenedHost3322155821.h"
#include "System_Xml_System_Xml_XmlRegisteredNonCachedStream1825156356.h"
#include "System_Xml_System_Xml_XmlCachedStream235166513.h"
#include "System_Xml_System_Xml_UTF16Decoder2333661544.h"
#include "System_Xml_System_Xml_SafeAsciiDecoder3220120650.h"
#include "System_Xml_System_Xml_Ucs4Encoding650080996.h"
#include "System_Xml_System_Xml_Ucs4Encoding123431992440.h"
#include "System_Xml_System_Xml_Ucs4Encoding4321616845456.h"
#include "System_Xml_System_Xml_Ucs4Encoding2143576439440.h"
#include "System_Xml_System_Xml_Ucs4Encoding34122881835128.h"
#include "System_Xml_System_Xml_Ucs4Decoder2645160027.h"
#include "System_Xml_System_Xml_Ucs4Decoder43212511885999.h"
#include "System_Xml_System_Xml_Ucs4Decoder1234522314591.h"
#include "System_Xml_System_Xml_Ucs4Decoder21433957010607.h"
#include "System_Xml_System_Xml_Ucs4Decoder34123372157535.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_Namespac3577631811.h"
#include "System_Xml_System_Xml_XmlNamespaceScope1384947590.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlQualifiedName_HashCodeOfS3565421161.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlUrlResolver_U3CGetEntityAs381354349.h"
#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"
#include "System_Xml_MS_Internal_Xml_XPath_AstNode_AstType2242522236.h"
#include "System_Xml_MS_Internal_Xml_XPath_Axis1660922251.h"
#include "System_Xml_MS_Internal_Xml_XPath_Axis_AxisType301805468.h"
#include "System_Xml_MS_Internal_Xml_XPath_Filter3094301242.h"
#include "System_Xml_MS_Internal_Xml_XPath_Function2441061476.h"
#include "System_Xml_MS_Internal_Xml_XPath_Function_Function2662603618.h"
#include "System_Xml_MS_Internal_Xml_XPath_Group1321734799.h"
#include "System_Xml_MS_Internal_Xml_XPath_Operand3474020745.h"
#include "System_Xml_MS_Internal_Xml_XPath_Operator3910961274.h"
#include "System_Xml_MS_Internal_Xml_XPath_Operator_Op1831748991.h"
#include "System_Xml_MS_Internal_Xml_XPath_Root2474398224.h"
#include "System_Xml_MS_Internal_Xml_XPath_Variable3215317144.h"
#include "System_Xml_MS_Internal_Xml_XPath_XPathParser3296684876.h"
#include "System_Xml_MS_Internal_Xml_XPath_XPathParser_Param2302613227.h"
#include "System_Xml_MS_Internal_Xml_XPath_XPathScanner1765956737.h"
#include "System_Xml_MS_Internal_Xml_XPath_XPathScanner_LexK1109665186.h"
#include "System_Xml_System_Xml_XPath_XPathDocument1328191420.h"
#include "System_Xml_System_Xml_XPath_XPathException1503722168.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578.h"
#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope3601604274.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator3981235968.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorKeyCompa3055722314.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType817388867.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1568637719.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2731437126.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar762068660.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1568637717.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1459944466.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3894236547.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar740780737.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2375206768.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2731437132.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1500295715.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar762068658.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar762068664.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3894236545.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3894236541.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2375206772.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar740780834.h"
#include "Mono_Posix_U3CModuleU3E3783534214.h"
#include "System_Core_U3CModuleU3E3783534214.h"
#include "System_Core_SR2523137203.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Security_Cryptography_AesManage3721278648.h"
#include "System_Core_System_Linq_Error3199922586.h"
#include "System_Core_System_Security_Cryptography_AesCrypto1359258894.h"
#include "System_Core_System_Security_Cryptography_AesTransf3733702461.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242306864773.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U243066379751.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U243379926265.h"
#include "MetaArcade_Core_U3CModuleU3E3783534214.h"
#include "MetaArcade_Core_MetaArcade_Core_Signals_Signal1504483080.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[3] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_elementName_0(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_1(),
	XmlRootAttribute_t3527426713::get_offset_of_nullable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (ValidateNames_t208250372), -1, sizeof(ValidateNames_t208250372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2804[1] = 
{
	ValidateNames_t208250372_StaticFields::get_offset_of_xmlCharType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (XmlCharType_t1050521405)+ sizeof (Il2CppObject), sizeof(XmlCharType_t1050521405_marshaled_pinvoke), sizeof(XmlCharType_t1050521405_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2805[3] = 
{
	XmlCharType_t1050521405_StaticFields::get_offset_of_s_Lock_0(),
	XmlCharType_t1050521405_StaticFields::get_offset_of_s_CharProperties_1(),
	XmlCharType_t1050521405::get_offset_of_charProperties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (XmlComplianceUtil_t976628708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (ExceptionType_t1261777571)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2807[3] = 
{
	ExceptionType_t1261777571::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2808[4] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_xmlCharType_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_crt_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_c_EncodedCharLength_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_WhitespaceChars_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (XmlDownloadManager_t830495394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	XmlDownloadManager_t830495394::get_offset_of_connections_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (U3CGetStreamAsyncU3Ec__AnonStorey1_t4234648579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[1] = 
{
	U3CGetStreamAsyncU3Ec__AnonStorey1_t4234648579::get_offset_of_uri_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[11] = 
{
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_uri_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U3CreqU3E__0_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_credentials_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_proxy_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_cachePolicy_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U3CrespU3E__0_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U3CwebReqU3E__0_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U24this_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U24builder_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U24PC_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717::get_offset_of_U24awaiter0_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (OpenedHost_t3322155821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[1] = 
{
	OpenedHost_t3322155821::get_offset_of_nonCachedConnectionsCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (XmlRegisteredNonCachedStream_t1825156356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[3] = 
{
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_stream_8(),
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_downloadManager_9(),
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_host_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (XmlCachedStream_t235166513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[1] = 
{
	XmlCachedStream_t235166513::get_offset_of_uri_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (UTF16Decoder_t2333661544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[2] = 
{
	UTF16Decoder_t2333661544::get_offset_of_bigEndian_2(),
	UTF16Decoder_t2333661544::get_offset_of_lastByte_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (SafeAsciiDecoder_t3220120650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (Ucs4Encoding_t650080996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	Ucs4Encoding_t650080996::get_offset_of_ucs4Decoder_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Ucs4Encoding1234_t31992440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (Ucs4Encoding4321_t616845456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (Ucs4Encoding2143_t576439440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (Ucs4Encoding3412_t2881835128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (Ucs4Decoder_t2645160027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	Ucs4Decoder_t2645160027::get_offset_of_lastBytes_2(),
	Ucs4Decoder_t2645160027::get_offset_of_lastBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (Ucs4Decoder4321_t2511885999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (Ucs4Decoder1234_t522314591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (Ucs4Decoder2143_t3957010607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (Ucs4Decoder3412_t3372157535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[6] = 
{
	XmlException_t4188277960::get_offset_of_res_16(),
	XmlException_t4188277960::get_offset_of_args_17(),
	XmlException_t4188277960::get_offset_of_lineNumber_18(),
	XmlException_t4188277960::get_offset_of_linePosition_19(),
	XmlException_t4188277960::get_offset_of_sourceUri_20(),
	XmlException_t4188277960::get_offset_of_message_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (XmlNamespaceManager_t486731501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[8] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_nsdecls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_lastDecl_1(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopeId_3(),
	XmlNamespaceManager_t486731501::get_offset_of_hashTable_4(),
	XmlNamespaceManager_t486731501::get_offset_of_useHashtable_5(),
	XmlNamespaceManager_t486731501::get_offset_of_xml_6(),
	XmlNamespaceManager_t486731501::get_offset_of_xmlNs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (NamespaceDeclaration_t3577631811)+ sizeof (Il2CppObject), sizeof(NamespaceDeclaration_t3577631811_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2829[4] = 
{
	NamespaceDeclaration_t3577631811::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_scopeId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_previousNsIndex_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (XmlNamespaceScope_t1384947590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2830[4] = 
{
	XmlNamespaceScope_t1384947590::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2832[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[5] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_hashCodeDelegate_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (HashCodeOfStringDelegate_t3565421161), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (XmlUrlResolver_t896669594), -1, sizeof(XmlUrlResolver_t896669594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2836[4] = 
{
	XmlUrlResolver_t896669594_StaticFields::get_offset_of_s_DownloadManager_0(),
	XmlUrlResolver_t896669594::get_offset_of__credentials_1(),
	XmlUrlResolver_t896669594::get_offset_of__proxy_2(),
	XmlUrlResolver_t896669594::get_offset_of__cachePolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (U3CGetEntityAsyncU3Ec__async0_t381354349)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[6] = 
{
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_ofObjectToReturn_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_absoluteUri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_U24this_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_U24builder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_U24PC_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CGetEntityAsyncU3Ec__async0_t381354349::get_offset_of_U24awaiter0_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (AstNode_t2002670936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (AstType_t2242522236)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[10] = 
{
	AstType_t2242522236::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (Axis_t1660922251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[7] = 
{
	Axis_t1660922251::get_offset_of_axisType_0(),
	Axis_t1660922251::get_offset_of_input_1(),
	Axis_t1660922251::get_offset_of_prefix_2(),
	Axis_t1660922251::get_offset_of_name_3(),
	Axis_t1660922251::get_offset_of_nodeType_4(),
	Axis_t1660922251::get_offset_of_abbrAxis_5(),
	Axis_t1660922251::get_offset_of_urn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (AxisType_t301805468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2841[15] = 
{
	AxisType_t301805468::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (Filter_t3094301242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[2] = 
{
	Filter_t3094301242::get_offset_of_input_0(),
	Filter_t3094301242::get_offset_of_condition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (Function_t2441061476), -1, sizeof(Function_t2441061476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2843[5] = 
{
	Function_t2441061476::get_offset_of_functionType_0(),
	Function_t2441061476::get_offset_of_argumentList_1(),
	Function_t2441061476::get_offset_of_name_2(),
	Function_t2441061476::get_offset_of_prefix_3(),
	Function_t2441061476_StaticFields::get_offset_of_ReturnTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (FunctionType_t2662603618)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2844[29] = 
{
	FunctionType_t2662603618::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (Group_t1321734799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	Group_t1321734799::get_offset_of_groupNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (Operand_t3474020745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[2] = 
{
	Operand_t3474020745::get_offset_of_type_0(),
	Operand_t3474020745::get_offset_of_val_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (Operator_t3910961274), -1, sizeof(Operator_t3910961274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2847[4] = 
{
	Operator_t3910961274_StaticFields::get_offset_of_invertOp_0(),
	Operator_t3910961274::get_offset_of_opType_1(),
	Operator_t3910961274::get_offset_of_opnd1_2(),
	Operator_t3910961274::get_offset_of_opnd2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (Op_t1831748991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2848[16] = 
{
	Op_t1831748991::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (Root_t2474398224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (Variable_t3215317144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[2] = 
{
	Variable_t3215317144::get_offset_of_localname_0(),
	Variable_t3215317144::get_offset_of_prefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (XPathParser_t3296684876), -1, sizeof(XPathParser_t3296684876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2851[13] = 
{
	XPathParser_t3296684876::get_offset_of_scanner_0(),
	XPathParser_t3296684876::get_offset_of_parseDepth_1(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray1_2(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray2_3(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray3_4(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray4_5(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray5_6(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray6_7(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray7_8(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray8_9(),
	XPathParser_t3296684876_StaticFields::get_offset_of_temparray9_10(),
	XPathParser_t3296684876_StaticFields::get_offset_of_functionTable_11(),
	XPathParser_t3296684876_StaticFields::get_offset_of_AxesTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (ParamInfo_t2302613227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[4] = 
{
	ParamInfo_t2302613227::get_offset_of_ftype_0(),
	ParamInfo_t2302613227::get_offset_of_minargs_1(),
	ParamInfo_t2302613227::get_offset_of_maxargs_2(),
	ParamInfo_t2302613227::get_offset_of_argTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (XPathScanner_t1765956737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[10] = 
{
	XPathScanner_t1765956737::get_offset_of_xpathExpr_0(),
	XPathScanner_t1765956737::get_offset_of_xpathExprIndex_1(),
	XPathScanner_t1765956737::get_offset_of_kind_2(),
	XPathScanner_t1765956737::get_offset_of_currentChar_3(),
	XPathScanner_t1765956737::get_offset_of_name_4(),
	XPathScanner_t1765956737::get_offset_of_prefix_5(),
	XPathScanner_t1765956737::get_offset_of_stringValue_6(),
	XPathScanner_t1765956737::get_offset_of_numberValue_7(),
	XPathScanner_t1765956737::get_offset_of_canBeFunction_8(),
	XPathScanner_t1765956737::get_offset_of_xmlCharType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (LexKind_t1109665186)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2854[32] = 
{
	LexKind_t1109665186::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (XPathDocument_t1328191420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[4] = 
{
	XPathDocument_t1328191420::get_offset_of_pageXmlNmsp_0(),
	XPathDocument_t1328191420::get_offset_of_idxXmlNmsp_1(),
	XPathDocument_t1328191420::get_offset_of_nameTable_2(),
	XPathDocument_t1328191420::get_offset_of_mapNmsp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (XPathException_t1503722168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[3] = 
{
	XPathException_t1503722168::get_offset_of_res_16(),
	XPathException_t1503722168::get_offset_of_args_17(),
	XPathException_t1503722168::get_offset_of_message_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (XPathResultType_t1521569578)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2858[8] = 
{
	XPathResultType_t1521569578::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (XPathNamespaceScope_t3601604274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[4] = 
{
	XPathNamespaceScope_t3601604274::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (XPathNavigator_t3981235968), -1, sizeof(XPathNavigator_t3981235968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2861[4] = 
{
	XPathNavigator_t3981235968_StaticFields::get_offset_of_comparer_0(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_NodeTypeLetter_1(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_UniqueIdTbl_2(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_ContentKindMasks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (XPathNavigatorKeyComparer_t3055722314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (XPathNodeType_t817388867)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2863[11] = 
{
	XPathNodeType_t817388867::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2864[29] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DB368804F0C6DAB083B253A6B106D0783D5C32E90_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D6A0D50D692745A6663128CD315B71079584F3E59_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DFFE3F15642234E7FAD6951D432F1134D5AD15922_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D9E374D7263B2452E25DE3D6E617F6A728D98A439_3(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D221CE291CD044114B4369175B9B91177F5932876_4(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D702F6A3276CBE481D247A77C20B5459FB94D07D2_5(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D360487BE4278986419B568EFD887F6145383168A_6(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D0701435C4E2C38EFE43C51BD22C114AB8B80124D_7(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D8B4E5E81A88D29642679AFCE41DCA380F9000462_8(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1_9(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D161F91CE1721D8F16622810CBB39887D21C47031_10(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D485F43E332C2F7530815B17C08DAC169A8F697E0_11(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DDCF398750721AA7A27A6BA56E99350329B06E8B1_12(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D574B9D4E4C39F6E8004181E5765B627B75EB1AD1_13(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1_14(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DC2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C_15(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DAB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C_16(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D99F0664C2AC8464B51252D92FC24F3834C6FB90C_17(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D51E4CA1C2B009A2876C6E57D8E69E3502BCA3440_18(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D9E31F24F64765FCAA589F589324D17C9FCF6A06D_19(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D7A32E1A19C182315E4263A65A72066492550D8CD_20(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DB5A4CCF7112376BCFA66AF51D95F8EEA7AF47688_21(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DEBC658B067B5C785A3F0BB67D73755F6FEE7F70C_22(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D49C5BA13401986EC93E4677F52CBE2248184DFBD_23(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4_24(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2D5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD_25(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DBAD037B714E1CD1052149B51238A3D4351DD10B5_26(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DEE3413A2C088FF9432054D6E60A7CB6A498D25F0_27(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24fieldU2DB9F0004E3873FDDCABFDA6174EA18F0859B637B4_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (U24ArrayTypeU3D32_t1568637723)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637723 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (U24ArrayTypeU3D40_t2731437127)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D40_t2731437127 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U24ArrayTypeU3D64_t762068662)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t762068662 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (U24ArrayTypeU3D12_t1568637724)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637724 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (U24ArrayTypeU3D8_t1459944468)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t1459944468 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (U24ArrayTypeU3D36_t3894236549)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t3894236549 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (U24ArrayTypeU3D416_t740780737)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D416_t740780737 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (U24ArrayTypeU3D68_t2375206768)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D68_t2375206768 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (U24ArrayTypeU3D20_t2731437134)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437134 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (U24ArrayTypeU3D144_t1500295715)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D144_t1500295715 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (U24ArrayTypeU3D44_t762068663)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D44_t762068663 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (U24ArrayTypeU3D24_t762068665)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068665 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (U24ArrayTypeU3D16_t3894236550)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236550 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (U24ArrayTypeU3D56_t3894236542)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D56_t3894236542 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (U24ArrayTypeU3D112_t740780834)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D112_t740780834 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (SR_t2523137205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (Enumerable_t2148412300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (AesManaged_t3721278648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[1] = 
{
	AesManaged_t3721278648::get_offset_of_m_rijndael_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (Error_t3199922586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (AesCryptoServiceProvider_t1359258894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (AesTransform_t3733702461), -1, sizeof(AesTransform_t3733702461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2893[14] = 
{
	AesTransform_t3733702461::get_offset_of_expandedKey_12(),
	AesTransform_t3733702461::get_offset_of_Nk_13(),
	AesTransform_t3733702461::get_offset_of_Nr_14(),
	AesTransform_t3733702461_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t3733702461_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T0_18(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T1_19(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T2_20(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T3_21(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2894[11] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D0AA802CD6847EB893FE786B5EA5168B2FDCD7B93_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D30A0358B25B1372DD598BB4B1AC56AD6B8F08A47_3(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2DAE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9_4(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94_5(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D5B5DF5A459E902D96F7DB0FB235A25346CA85C5D_6(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2DA02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53_7(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D5BE411F1438EAEF33726D855E99011D5FECDDD4E_8(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D20733E1283D873EBE47133A95C233E11B76F5F11_9(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (U24ArrayTypeU3D120_t2306864774)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D120_t2306864774 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (U24ArrayTypeU3D256_t3066379753)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D256_t3066379753 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (U24ArrayTypeU3D1024_t3379926265)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1024_t3379926265 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (Signal_t1504483080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[3] = 
{
	Signal_t1504483080::get_offset_of_m_oneTimeListeners_0(),
	Signal_t1504483080::get_offset_of_m_listeners_1(),
	Signal_t1504483080::get_offset_of_m_weakListeners_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
