﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"
#include "System_Xml_System_Xml_Schema_StateUnion1440994946.h"

// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;
// System.String
struct String_t;
// System.Xml.Schema.ConstraintStruct[]
struct ConstraintStructU5BU5D_t2758837721;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_t2256991674;
// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Collections.Generic.List`1<System.Xml.Schema.RangePositionInfo>
struct List_1_t2149924054;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationState
struct  ValidationState_t3143048826  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Schema.ValidationState::IsNill
	bool ___IsNill_0;
	// System.Boolean System.Xml.Schema.ValidationState::NeedValidateChildren
	bool ___NeedValidateChildren_1;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.ValidationState::ProcessContents
	int32_t ___ProcessContents_2;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.ValidationState::ElementDecl
	SchemaElementDecl_t1940851905 * ___ElementDecl_3;
	// System.String System.Xml.Schema.ValidationState::LocalName
	String_t* ___LocalName_4;
	// System.String System.Xml.Schema.ValidationState::Namespace
	String_t* ___Namespace_5;
	// System.Xml.Schema.ConstraintStruct[] System.Xml.Schema.ValidationState::Constr
	ConstraintStructU5BU5D_t2758837721* ___Constr_6;
	// System.Xml.Schema.StateUnion System.Xml.Schema.ValidationState::CurrentState
	StateUnion_t1440994946  ___CurrentState_7;
	// System.Boolean System.Xml.Schema.ValidationState::HasMatched
	bool ___HasMatched_8;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.ValidationState::CurPos
	BitSetU5BU5D_t2256991674* ___CurPos_9;
	// System.Xml.Schema.BitSet System.Xml.Schema.ValidationState::AllElementsSet
	BitSet_t1062448123 * ___AllElementsSet_10;
	// System.Collections.Generic.List`1<System.Xml.Schema.RangePositionInfo> System.Xml.Schema.ValidationState::RunningPositions
	List_1_t2149924054 * ___RunningPositions_11;
	// System.Boolean System.Xml.Schema.ValidationState::TooComplex
	bool ___TooComplex_12;

public:
	inline static int32_t get_offset_of_IsNill_0() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___IsNill_0)); }
	inline bool get_IsNill_0() const { return ___IsNill_0; }
	inline bool* get_address_of_IsNill_0() { return &___IsNill_0; }
	inline void set_IsNill_0(bool value)
	{
		___IsNill_0 = value;
	}

	inline static int32_t get_offset_of_NeedValidateChildren_1() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___NeedValidateChildren_1)); }
	inline bool get_NeedValidateChildren_1() const { return ___NeedValidateChildren_1; }
	inline bool* get_address_of_NeedValidateChildren_1() { return &___NeedValidateChildren_1; }
	inline void set_NeedValidateChildren_1(bool value)
	{
		___NeedValidateChildren_1 = value;
	}

	inline static int32_t get_offset_of_ProcessContents_2() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___ProcessContents_2)); }
	inline int32_t get_ProcessContents_2() const { return ___ProcessContents_2; }
	inline int32_t* get_address_of_ProcessContents_2() { return &___ProcessContents_2; }
	inline void set_ProcessContents_2(int32_t value)
	{
		___ProcessContents_2 = value;
	}

	inline static int32_t get_offset_of_ElementDecl_3() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___ElementDecl_3)); }
	inline SchemaElementDecl_t1940851905 * get_ElementDecl_3() const { return ___ElementDecl_3; }
	inline SchemaElementDecl_t1940851905 ** get_address_of_ElementDecl_3() { return &___ElementDecl_3; }
	inline void set_ElementDecl_3(SchemaElementDecl_t1940851905 * value)
	{
		___ElementDecl_3 = value;
		Il2CppCodeGenWriteBarrier(&___ElementDecl_3, value);
	}

	inline static int32_t get_offset_of_LocalName_4() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___LocalName_4)); }
	inline String_t* get_LocalName_4() const { return ___LocalName_4; }
	inline String_t** get_address_of_LocalName_4() { return &___LocalName_4; }
	inline void set_LocalName_4(String_t* value)
	{
		___LocalName_4 = value;
		Il2CppCodeGenWriteBarrier(&___LocalName_4, value);
	}

	inline static int32_t get_offset_of_Namespace_5() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___Namespace_5)); }
	inline String_t* get_Namespace_5() const { return ___Namespace_5; }
	inline String_t** get_address_of_Namespace_5() { return &___Namespace_5; }
	inline void set_Namespace_5(String_t* value)
	{
		___Namespace_5 = value;
		Il2CppCodeGenWriteBarrier(&___Namespace_5, value);
	}

	inline static int32_t get_offset_of_Constr_6() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___Constr_6)); }
	inline ConstraintStructU5BU5D_t2758837721* get_Constr_6() const { return ___Constr_6; }
	inline ConstraintStructU5BU5D_t2758837721** get_address_of_Constr_6() { return &___Constr_6; }
	inline void set_Constr_6(ConstraintStructU5BU5D_t2758837721* value)
	{
		___Constr_6 = value;
		Il2CppCodeGenWriteBarrier(&___Constr_6, value);
	}

	inline static int32_t get_offset_of_CurrentState_7() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___CurrentState_7)); }
	inline StateUnion_t1440994946  get_CurrentState_7() const { return ___CurrentState_7; }
	inline StateUnion_t1440994946 * get_address_of_CurrentState_7() { return &___CurrentState_7; }
	inline void set_CurrentState_7(StateUnion_t1440994946  value)
	{
		___CurrentState_7 = value;
	}

	inline static int32_t get_offset_of_HasMatched_8() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___HasMatched_8)); }
	inline bool get_HasMatched_8() const { return ___HasMatched_8; }
	inline bool* get_address_of_HasMatched_8() { return &___HasMatched_8; }
	inline void set_HasMatched_8(bool value)
	{
		___HasMatched_8 = value;
	}

	inline static int32_t get_offset_of_CurPos_9() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___CurPos_9)); }
	inline BitSetU5BU5D_t2256991674* get_CurPos_9() const { return ___CurPos_9; }
	inline BitSetU5BU5D_t2256991674** get_address_of_CurPos_9() { return &___CurPos_9; }
	inline void set_CurPos_9(BitSetU5BU5D_t2256991674* value)
	{
		___CurPos_9 = value;
		Il2CppCodeGenWriteBarrier(&___CurPos_9, value);
	}

	inline static int32_t get_offset_of_AllElementsSet_10() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___AllElementsSet_10)); }
	inline BitSet_t1062448123 * get_AllElementsSet_10() const { return ___AllElementsSet_10; }
	inline BitSet_t1062448123 ** get_address_of_AllElementsSet_10() { return &___AllElementsSet_10; }
	inline void set_AllElementsSet_10(BitSet_t1062448123 * value)
	{
		___AllElementsSet_10 = value;
		Il2CppCodeGenWriteBarrier(&___AllElementsSet_10, value);
	}

	inline static int32_t get_offset_of_RunningPositions_11() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___RunningPositions_11)); }
	inline List_1_t2149924054 * get_RunningPositions_11() const { return ___RunningPositions_11; }
	inline List_1_t2149924054 ** get_address_of_RunningPositions_11() { return &___RunningPositions_11; }
	inline void set_RunningPositions_11(List_1_t2149924054 * value)
	{
		___RunningPositions_11 = value;
		Il2CppCodeGenWriteBarrier(&___RunningPositions_11, value);
	}

	inline static int32_t get_offset_of_TooComplex_12() { return static_cast<int32_t>(offsetof(ValidationState_t3143048826, ___TooComplex_12)); }
	inline bool get_TooComplex_12() const { return ___TooComplex_12; }
	inline bool* get_address_of_TooComplex_12() { return &___TooComplex_12; }
	inline void set_TooComplex_12(bool value)
	{
		___TooComplex_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
