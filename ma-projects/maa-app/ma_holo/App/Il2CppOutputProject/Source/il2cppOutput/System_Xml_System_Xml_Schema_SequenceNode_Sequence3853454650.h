﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Xml.Schema.SequenceNode
struct SequenceNode_t4039907291;
// System.Xml.Schema.BitSet
struct BitSet_t1062448123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct  SequenceConstructPosContext_t3853454650 
{
public:
	// System.Xml.Schema.SequenceNode System.Xml.Schema.SequenceNode/SequenceConstructPosContext::this_
	SequenceNode_t4039907291 * ___this__0;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstpos
	BitSet_t1062448123 * ___firstpos_1;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastpos
	BitSet_t1062448123 * ___lastpos_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastposLeft
	BitSet_t1062448123 * ___lastposLeft_3;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstposRight
	BitSet_t1062448123 * ___firstposRight_4;

public:
	inline static int32_t get_offset_of_this__0() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___this__0)); }
	inline SequenceNode_t4039907291 * get_this__0() const { return ___this__0; }
	inline SequenceNode_t4039907291 ** get_address_of_this__0() { return &___this__0; }
	inline void set_this__0(SequenceNode_t4039907291 * value)
	{
		___this__0 = value;
		Il2CppCodeGenWriteBarrier(&___this__0, value);
	}

	inline static int32_t get_offset_of_firstpos_1() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___firstpos_1)); }
	inline BitSet_t1062448123 * get_firstpos_1() const { return ___firstpos_1; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_1() { return &___firstpos_1; }
	inline void set_firstpos_1(BitSet_t1062448123 * value)
	{
		___firstpos_1 = value;
		Il2CppCodeGenWriteBarrier(&___firstpos_1, value);
	}

	inline static int32_t get_offset_of_lastpos_2() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___lastpos_2)); }
	inline BitSet_t1062448123 * get_lastpos_2() const { return ___lastpos_2; }
	inline BitSet_t1062448123 ** get_address_of_lastpos_2() { return &___lastpos_2; }
	inline void set_lastpos_2(BitSet_t1062448123 * value)
	{
		___lastpos_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastpos_2, value);
	}

	inline static int32_t get_offset_of_lastposLeft_3() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___lastposLeft_3)); }
	inline BitSet_t1062448123 * get_lastposLeft_3() const { return ___lastposLeft_3; }
	inline BitSet_t1062448123 ** get_address_of_lastposLeft_3() { return &___lastposLeft_3; }
	inline void set_lastposLeft_3(BitSet_t1062448123 * value)
	{
		___lastposLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastposLeft_3, value);
	}

	inline static int32_t get_offset_of_firstposRight_4() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___firstposRight_4)); }
	inline BitSet_t1062448123 * get_firstposRight_4() const { return ___firstposRight_4; }
	inline BitSet_t1062448123 ** get_address_of_firstposRight_4() { return &___firstposRight_4; }
	inline void set_firstposRight_4(BitSet_t1062448123 * value)
	{
		___firstposRight_4 = value;
		Il2CppCodeGenWriteBarrier(&___firstposRight_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t3853454650_marshaled_pinvoke
{
	SequenceNode_t4039907291 * ___this__0;
	BitSet_t1062448123 * ___firstpos_1;
	BitSet_t1062448123 * ___lastpos_2;
	BitSet_t1062448123 * ___lastposLeft_3;
	BitSet_t1062448123 * ___firstposRight_4;
};
// Native definition for COM marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t3853454650_marshaled_com
{
	SequenceNode_t4039907291 * ___this__0;
	BitSet_t1062448123 * ___firstpos_1;
	BitSet_t1062448123 * ___lastpos_2;
	BitSet_t1062448123 * ___lastposLeft_3;
	BitSet_t1062448123 * ___firstposRight_4;
};
