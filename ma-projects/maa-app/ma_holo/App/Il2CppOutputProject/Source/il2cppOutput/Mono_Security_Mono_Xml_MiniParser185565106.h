﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.UInt16[]
struct UInt16U5BU5D_t2527266722;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser
struct  MiniParser_t185565106  : public Il2CppObject
{
public:
	// System.Int32 Mono.Xml.MiniParser::line
	int32_t ___line_3;
	// System.Int32 Mono.Xml.MiniParser::col
	int32_t ___col_4;
	// System.Int32[] Mono.Xml.MiniParser::twoCharBuff
	Int32U5BU5D_t3030399641* ___twoCharBuff_5;
	// System.Boolean Mono.Xml.MiniParser::splitCData
	bool ___splitCData_6;

public:
	inline static int32_t get_offset_of_line_3() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___line_3)); }
	inline int32_t get_line_3() const { return ___line_3; }
	inline int32_t* get_address_of_line_3() { return &___line_3; }
	inline void set_line_3(int32_t value)
	{
		___line_3 = value;
	}

	inline static int32_t get_offset_of_col_4() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___col_4)); }
	inline int32_t get_col_4() const { return ___col_4; }
	inline int32_t* get_address_of_col_4() { return &___col_4; }
	inline void set_col_4(int32_t value)
	{
		___col_4 = value;
	}

	inline static int32_t get_offset_of_twoCharBuff_5() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___twoCharBuff_5)); }
	inline Int32U5BU5D_t3030399641* get_twoCharBuff_5() const { return ___twoCharBuff_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_twoCharBuff_5() { return &___twoCharBuff_5; }
	inline void set_twoCharBuff_5(Int32U5BU5D_t3030399641* value)
	{
		___twoCharBuff_5 = value;
		Il2CppCodeGenWriteBarrier(&___twoCharBuff_5, value);
	}

	inline static int32_t get_offset_of_splitCData_6() { return static_cast<int32_t>(offsetof(MiniParser_t185565106, ___splitCData_6)); }
	inline bool get_splitCData_6() const { return ___splitCData_6; }
	inline bool* get_address_of_splitCData_6() { return &___splitCData_6; }
	inline void set_splitCData_6(bool value)
	{
		___splitCData_6 = value;
	}
};

struct MiniParser_t185565106_StaticFields
{
public:
	// System.Int32 Mono.Xml.MiniParser::INPUT_RANGE
	int32_t ___INPUT_RANGE_0;
	// System.UInt16[] Mono.Xml.MiniParser::tbl
	UInt16U5BU5D_t2527266722* ___tbl_1;
	// System.String[] Mono.Xml.MiniParser::errors
	StringU5BU5D_t1642385972* ___errors_2;

public:
	inline static int32_t get_offset_of_INPUT_RANGE_0() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___INPUT_RANGE_0)); }
	inline int32_t get_INPUT_RANGE_0() const { return ___INPUT_RANGE_0; }
	inline int32_t* get_address_of_INPUT_RANGE_0() { return &___INPUT_RANGE_0; }
	inline void set_INPUT_RANGE_0(int32_t value)
	{
		___INPUT_RANGE_0 = value;
	}

	inline static int32_t get_offset_of_tbl_1() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___tbl_1)); }
	inline UInt16U5BU5D_t2527266722* get_tbl_1() const { return ___tbl_1; }
	inline UInt16U5BU5D_t2527266722** get_address_of_tbl_1() { return &___tbl_1; }
	inline void set_tbl_1(UInt16U5BU5D_t2527266722* value)
	{
		___tbl_1 = value;
		Il2CppCodeGenWriteBarrier(&___tbl_1, value);
	}

	inline static int32_t get_offset_of_errors_2() { return static_cast<int32_t>(offsetof(MiniParser_t185565106_StaticFields, ___errors_2)); }
	inline StringU5BU5D_t1642385972* get_errors_2() const { return ___errors_2; }
	inline StringU5BU5D_t1642385972** get_address_of_errors_2() { return &___errors_2; }
	inline void set_errors_2(StringU5BU5D_t1642385972* value)
	{
		___errors_2 = value;
		Il2CppCodeGenWriteBarrier(&___errors_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
