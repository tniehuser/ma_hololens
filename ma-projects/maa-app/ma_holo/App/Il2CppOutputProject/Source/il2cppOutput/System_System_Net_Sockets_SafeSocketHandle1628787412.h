﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZer1177681199.h"

// System.Collections.Generic.List`1<System.Threading.Thread>
struct List_1_t3905650040;
// System.Collections.Generic.Dictionary`2<System.Threading.Thread,System.Diagnostics.StackTrace>
struct Dictionary_2_t874325380;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SafeSocketHandle
struct  SafeSocketHandle_t1628787412  : public SafeHandleZeroOrMinusOneIsInvalid_t1177681199
{
public:
	// System.Collections.Generic.List`1<System.Threading.Thread> System.Net.Sockets.SafeSocketHandle::blocking_threads
	List_1_t3905650040 * ___blocking_threads_4;
	// System.Collections.Generic.Dictionary`2<System.Threading.Thread,System.Diagnostics.StackTrace> System.Net.Sockets.SafeSocketHandle::threads_stacktraces
	Dictionary_2_t874325380 * ___threads_stacktraces_5;
	// System.Boolean System.Net.Sockets.SafeSocketHandle::in_cleanup
	bool ___in_cleanup_6;

public:
	inline static int32_t get_offset_of_blocking_threads_4() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t1628787412, ___blocking_threads_4)); }
	inline List_1_t3905650040 * get_blocking_threads_4() const { return ___blocking_threads_4; }
	inline List_1_t3905650040 ** get_address_of_blocking_threads_4() { return &___blocking_threads_4; }
	inline void set_blocking_threads_4(List_1_t3905650040 * value)
	{
		___blocking_threads_4 = value;
		Il2CppCodeGenWriteBarrier(&___blocking_threads_4, value);
	}

	inline static int32_t get_offset_of_threads_stacktraces_5() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t1628787412, ___threads_stacktraces_5)); }
	inline Dictionary_2_t874325380 * get_threads_stacktraces_5() const { return ___threads_stacktraces_5; }
	inline Dictionary_2_t874325380 ** get_address_of_threads_stacktraces_5() { return &___threads_stacktraces_5; }
	inline void set_threads_stacktraces_5(Dictionary_2_t874325380 * value)
	{
		___threads_stacktraces_5 = value;
		Il2CppCodeGenWriteBarrier(&___threads_stacktraces_5, value);
	}

	inline static int32_t get_offset_of_in_cleanup_6() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t1628787412, ___in_cleanup_6)); }
	inline bool get_in_cleanup_6() const { return ___in_cleanup_6; }
	inline bool* get_address_of_in_cleanup_6() { return &___in_cleanup_6; }
	inline void set_in_cleanup_6(bool value)
	{
		___in_cleanup_6 = value;
	}
};

struct SafeSocketHandle_t1628787412_StaticFields
{
public:
	// System.Boolean System.Net.Sockets.SafeSocketHandle::THROW_ON_ABORT_RETRIES
	bool ___THROW_ON_ABORT_RETRIES_7;

public:
	inline static int32_t get_offset_of_THROW_ON_ABORT_RETRIES_7() { return static_cast<int32_t>(offsetof(SafeSocketHandle_t1628787412_StaticFields, ___THROW_ON_ABORT_RETRIES_7)); }
	inline bool get_THROW_ON_ABORT_RETRIES_7() const { return ___THROW_ON_ABORT_RETRIES_7; }
	inline bool* get_address_of_THROW_ON_ABORT_RETRIES_7() { return &___THROW_ON_ABORT_RETRIES_7; }
	inline void set_THROW_ON_ABORT_RETRIES_7(bool value)
	{
		___THROW_ON_ABORT_RETRIES_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
