﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_TypeInfo3822613806.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.SymbolType
struct  SymbolType_t2424957512  : public TypeInfo_t3822613806
{
public:
	// System.Type System.Reflection.Emit.SymbolType::m_baseType
	Type_t * ___m_baseType_8;

public:
	inline static int32_t get_offset_of_m_baseType_8() { return static_cast<int32_t>(offsetof(SymbolType_t2424957512, ___m_baseType_8)); }
	inline Type_t * get_m_baseType_8() const { return ___m_baseType_8; }
	inline Type_t ** get_address_of_m_baseType_8() { return &___m_baseType_8; }
	inline void set_m_baseType_8(Type_t * value)
	{
		___m_baseType_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_baseType_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
