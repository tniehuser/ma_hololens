﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.String
struct String_t;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t4027255380  : public XmlNode_t616554813
{
public:
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_1;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_2;
	// System.String System.Xml.XmlEntity::notationName
	String_t* ___notationName_3;
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_4;
	// System.String System.Xml.XmlEntity::unparsedReplacementStr
	String_t* ___unparsedReplacementStr_5;
	// System.String System.Xml.XmlEntity::baseURI
	String_t* ___baseURI_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastChild
	XmlLinkedNode_t1287616130 * ___lastChild_7;
	// System.Boolean System.Xml.XmlEntity::childrenFoliating
	bool ___childrenFoliating_8;

public:
	inline static int32_t get_offset_of_publicId_1() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___publicId_1)); }
	inline String_t* get_publicId_1() const { return ___publicId_1; }
	inline String_t** get_address_of_publicId_1() { return &___publicId_1; }
	inline void set_publicId_1(String_t* value)
	{
		___publicId_1 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_1, value);
	}

	inline static int32_t get_offset_of_systemId_2() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___systemId_2)); }
	inline String_t* get_systemId_2() const { return ___systemId_2; }
	inline String_t** get_address_of_systemId_2() { return &___systemId_2; }
	inline void set_systemId_2(String_t* value)
	{
		___systemId_2 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_2, value);
	}

	inline static int32_t get_offset_of_notationName_3() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___notationName_3)); }
	inline String_t* get_notationName_3() const { return ___notationName_3; }
	inline String_t** get_address_of_notationName_3() { return &___notationName_3; }
	inline void set_notationName_3(String_t* value)
	{
		___notationName_3 = value;
		Il2CppCodeGenWriteBarrier(&___notationName_3, value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_4, value);
	}

	inline static int32_t get_offset_of_unparsedReplacementStr_5() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___unparsedReplacementStr_5)); }
	inline String_t* get_unparsedReplacementStr_5() const { return ___unparsedReplacementStr_5; }
	inline String_t** get_address_of_unparsedReplacementStr_5() { return &___unparsedReplacementStr_5; }
	inline void set_unparsedReplacementStr_5(String_t* value)
	{
		___unparsedReplacementStr_5 = value;
		Il2CppCodeGenWriteBarrier(&___unparsedReplacementStr_5, value);
	}

	inline static int32_t get_offset_of_baseURI_6() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___baseURI_6)); }
	inline String_t* get_baseURI_6() const { return ___baseURI_6; }
	inline String_t** get_address_of_baseURI_6() { return &___baseURI_6; }
	inline void set_baseURI_6(String_t* value)
	{
		___baseURI_6 = value;
		Il2CppCodeGenWriteBarrier(&___baseURI_6, value);
	}

	inline static int32_t get_offset_of_lastChild_7() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___lastChild_7)); }
	inline XmlLinkedNode_t1287616130 * get_lastChild_7() const { return ___lastChild_7; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastChild_7() { return &___lastChild_7; }
	inline void set_lastChild_7(XmlLinkedNode_t1287616130 * value)
	{
		___lastChild_7 = value;
		Il2CppCodeGenWriteBarrier(&___lastChild_7, value);
	}

	inline static int32_t get_offset_of_childrenFoliating_8() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___childrenFoliating_8)); }
	inline bool get_childrenFoliating_8() const { return ___childrenFoliating_8; }
	inline bool* get_address_of_childrenFoliating_8() { return &___childrenFoliating_8; }
	inline void set_childrenFoliating_8(bool value)
	{
		___childrenFoliating_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
