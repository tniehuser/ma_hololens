﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`4<System.IO.TextWriter,System.Char[],System.Int32,System.Int32>
struct  Tuple_4_t184925852  : public Il2CppObject
{
public:
	// T1 System.Tuple`4::m_Item1
	TextWriter_t4027217640 * ___m_Item1_0;
	// T2 System.Tuple`4::m_Item2
	CharU5BU5D_t1328083999* ___m_Item2_1;
	// T3 System.Tuple`4::m_Item3
	int32_t ___m_Item3_2;
	// T4 System.Tuple`4::m_Item4
	int32_t ___m_Item4_3;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_4_t184925852, ___m_Item1_0)); }
	inline TextWriter_t4027217640 * get_m_Item1_0() const { return ___m_Item1_0; }
	inline TextWriter_t4027217640 ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(TextWriter_t4027217640 * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_4_t184925852, ___m_Item2_1)); }
	inline CharU5BU5D_t1328083999* get_m_Item2_1() const { return ___m_Item2_1; }
	inline CharU5BU5D_t1328083999** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(CharU5BU5D_t1328083999* value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}

	inline static int32_t get_offset_of_m_Item3_2() { return static_cast<int32_t>(offsetof(Tuple_4_t184925852, ___m_Item3_2)); }
	inline int32_t get_m_Item3_2() const { return ___m_Item3_2; }
	inline int32_t* get_address_of_m_Item3_2() { return &___m_Item3_2; }
	inline void set_m_Item3_2(int32_t value)
	{
		___m_Item3_2 = value;
	}

	inline static int32_t get_offset_of_m_Item4_3() { return static_cast<int32_t>(offsetof(Tuple_4_t184925852, ___m_Item4_3)); }
	inline int32_t get_m_Item4_3() const { return ___m_Item4_3; }
	inline int32_t* get_address_of_m_Item4_3() { return &___m_Item4_3; }
	inline void set_m_Item4_3(int32_t value)
	{
		___m_Item4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
