﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<MetaArcade.Core.Signals.IWeakActionHandler>
struct List_1_t2141515233;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MetaArcade.Core.Signals.WeakActionList
struct  WeakActionList_t119657226  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<MetaArcade.Core.Signals.IWeakActionHandler> MetaArcade.Core.Signals.WeakActionList::m_internalList
	List_1_t2141515233 * ___m_internalList_0;
	// System.Object MetaArcade.Core.Signals.WeakActionList::m_sync
	Il2CppObject * ___m_sync_1;

public:
	inline static int32_t get_offset_of_m_internalList_0() { return static_cast<int32_t>(offsetof(WeakActionList_t119657226, ___m_internalList_0)); }
	inline List_1_t2141515233 * get_m_internalList_0() const { return ___m_internalList_0; }
	inline List_1_t2141515233 ** get_address_of_m_internalList_0() { return &___m_internalList_0; }
	inline void set_m_internalList_0(List_1_t2141515233 * value)
	{
		___m_internalList_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_internalList_0, value);
	}

	inline static int32_t get_offset_of_m_sync_1() { return static_cast<int32_t>(offsetof(WeakActionList_t119657226, ___m_sync_1)); }
	inline Il2CppObject * get_m_sync_1() const { return ___m_sync_1; }
	inline Il2CppObject ** get_address_of_m_sync_1() { return &___m_sync_1; }
	inline void set_m_sync_1(Il2CppObject * value)
	{
		___m_sync_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_sync_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
