﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Decimal[]
struct DecimalU5BU5D_t624008824;
struct Decimal_t724701077 ;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangePositionInfo
struct  RangePositionInfo_t2780802922 
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangePositionInfo::curpos
	BitSet_t1062448123 * ___curpos_0;
	// System.Decimal[] System.Xml.Schema.RangePositionInfo::rangeCounters
	DecimalU5BU5D_t624008824* ___rangeCounters_1;

public:
	inline static int32_t get_offset_of_curpos_0() { return static_cast<int32_t>(offsetof(RangePositionInfo_t2780802922, ___curpos_0)); }
	inline BitSet_t1062448123 * get_curpos_0() const { return ___curpos_0; }
	inline BitSet_t1062448123 ** get_address_of_curpos_0() { return &___curpos_0; }
	inline void set_curpos_0(BitSet_t1062448123 * value)
	{
		___curpos_0 = value;
		Il2CppCodeGenWriteBarrier(&___curpos_0, value);
	}

	inline static int32_t get_offset_of_rangeCounters_1() { return static_cast<int32_t>(offsetof(RangePositionInfo_t2780802922, ___rangeCounters_1)); }
	inline DecimalU5BU5D_t624008824* get_rangeCounters_1() const { return ___rangeCounters_1; }
	inline DecimalU5BU5D_t624008824** get_address_of_rangeCounters_1() { return &___rangeCounters_1; }
	inline void set_rangeCounters_1(DecimalU5BU5D_t624008824* value)
	{
		___rangeCounters_1 = value;
		Il2CppCodeGenWriteBarrier(&___rangeCounters_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_t2780802922_marshaled_pinvoke
{
	BitSet_t1062448123 * ___curpos_0;
	Decimal_t724701077 * ___rangeCounters_1;
};
// Native definition for COM marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_t2780802922_marshaled_com
{
	BitSet_t1062448123 * ___curpos_0;
	Decimal_t724701077 * ___rangeCounters_1;
};
