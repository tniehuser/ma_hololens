﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_Group3761430853.h"

// System.Text.RegularExpressions.Match
struct Match_t3164245899;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t939014605;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3164245899  : public Group_t3761430853
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::_groupcoll
	GroupCollection_t939014605 * ____groupcoll_7;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::_regex
	Regex_t1803876613 * ____regex_8;
	// System.Int32 System.Text.RegularExpressions.Match::_textbeg
	int32_t ____textbeg_9;
	// System.Int32 System.Text.RegularExpressions.Match::_textpos
	int32_t ____textpos_10;
	// System.Int32 System.Text.RegularExpressions.Match::_textend
	int32_t ____textend_11;
	// System.Int32 System.Text.RegularExpressions.Match::_textstart
	int32_t ____textstart_12;
	// System.Int32[][] System.Text.RegularExpressions.Match::_matches
	Int32U5BU5DU5BU5D_t3750818532* ____matches_13;
	// System.Int32[] System.Text.RegularExpressions.Match::_matchcount
	Int32U5BU5D_t3030399641* ____matchcount_14;
	// System.Boolean System.Text.RegularExpressions.Match::_balancing
	bool ____balancing_15;

public:
	inline static int32_t get_offset_of__groupcoll_7() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____groupcoll_7)); }
	inline GroupCollection_t939014605 * get__groupcoll_7() const { return ____groupcoll_7; }
	inline GroupCollection_t939014605 ** get_address_of__groupcoll_7() { return &____groupcoll_7; }
	inline void set__groupcoll_7(GroupCollection_t939014605 * value)
	{
		____groupcoll_7 = value;
		Il2CppCodeGenWriteBarrier(&____groupcoll_7, value);
	}

	inline static int32_t get_offset_of__regex_8() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____regex_8)); }
	inline Regex_t1803876613 * get__regex_8() const { return ____regex_8; }
	inline Regex_t1803876613 ** get_address_of__regex_8() { return &____regex_8; }
	inline void set__regex_8(Regex_t1803876613 * value)
	{
		____regex_8 = value;
		Il2CppCodeGenWriteBarrier(&____regex_8, value);
	}

	inline static int32_t get_offset_of__textbeg_9() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textbeg_9)); }
	inline int32_t get__textbeg_9() const { return ____textbeg_9; }
	inline int32_t* get_address_of__textbeg_9() { return &____textbeg_9; }
	inline void set__textbeg_9(int32_t value)
	{
		____textbeg_9 = value;
	}

	inline static int32_t get_offset_of__textpos_10() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textpos_10)); }
	inline int32_t get__textpos_10() const { return ____textpos_10; }
	inline int32_t* get_address_of__textpos_10() { return &____textpos_10; }
	inline void set__textpos_10(int32_t value)
	{
		____textpos_10 = value;
	}

	inline static int32_t get_offset_of__textend_11() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textend_11)); }
	inline int32_t get__textend_11() const { return ____textend_11; }
	inline int32_t* get_address_of__textend_11() { return &____textend_11; }
	inline void set__textend_11(int32_t value)
	{
		____textend_11 = value;
	}

	inline static int32_t get_offset_of__textstart_12() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____textstart_12)); }
	inline int32_t get__textstart_12() const { return ____textstart_12; }
	inline int32_t* get_address_of__textstart_12() { return &____textstart_12; }
	inline void set__textstart_12(int32_t value)
	{
		____textstart_12 = value;
	}

	inline static int32_t get_offset_of__matches_13() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____matches_13)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__matches_13() const { return ____matches_13; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__matches_13() { return &____matches_13; }
	inline void set__matches_13(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____matches_13 = value;
		Il2CppCodeGenWriteBarrier(&____matches_13, value);
	}

	inline static int32_t get_offset_of__matchcount_14() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____matchcount_14)); }
	inline Int32U5BU5D_t3030399641* get__matchcount_14() const { return ____matchcount_14; }
	inline Int32U5BU5D_t3030399641** get_address_of__matchcount_14() { return &____matchcount_14; }
	inline void set__matchcount_14(Int32U5BU5D_t3030399641* value)
	{
		____matchcount_14 = value;
		Il2CppCodeGenWriteBarrier(&____matchcount_14, value);
	}

	inline static int32_t get_offset_of__balancing_15() { return static_cast<int32_t>(offsetof(Match_t3164245899, ____balancing_15)); }
	inline bool get__balancing_15() const { return ____balancing_15; }
	inline bool* get_address_of__balancing_15() { return &____balancing_15; }
	inline void set__balancing_15(bool value)
	{
		____balancing_15 = value;
	}
};

struct Match_t3164245899_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::_empty
	Match_t3164245899 * ____empty_6;

public:
	inline static int32_t get_offset_of__empty_6() { return static_cast<int32_t>(offsetof(Match_t3164245899_StaticFields, ____empty_6)); }
	inline Match_t3164245899 * get__empty_6() const { return ____empty_6; }
	inline Match_t3164245899 ** get_address_of__empty_6() { return &____empty_6; }
	inline void set__empty_6(Match_t3164245899 * value)
	{
		____empty_6 = value;
		Il2CppCodeGenWriteBarrier(&____empty_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
