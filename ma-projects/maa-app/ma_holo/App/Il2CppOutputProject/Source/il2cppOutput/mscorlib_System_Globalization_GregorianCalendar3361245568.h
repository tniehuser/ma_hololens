﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Globalization_Calendar585061108.h"
#include "mscorlib_System_Globalization_GregorianCalendarTyp3080789929.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Globalization.Calendar
struct Calendar_t585061108;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.GregorianCalendar
struct  GregorianCalendar_t3361245568  : public Calendar_t585061108
{
public:
	// System.Globalization.GregorianCalendarTypes System.Globalization.GregorianCalendar::m_type
	int32_t ___m_type_3;

public:
	inline static int32_t get_offset_of_m_type_3() { return static_cast<int32_t>(offsetof(GregorianCalendar_t3361245568, ___m_type_3)); }
	inline int32_t get_m_type_3() const { return ___m_type_3; }
	inline int32_t* get_address_of_m_type_3() { return &___m_type_3; }
	inline void set_m_type_3(int32_t value)
	{
		___m_type_3 = value;
	}
};

struct GregorianCalendar_t3361245568_StaticFields
{
public:
	// System.Int32[] System.Globalization.GregorianCalendar::DaysToMonth365
	Int32U5BU5D_t3030399641* ___DaysToMonth365_4;
	// System.Int32[] System.Globalization.GregorianCalendar::DaysToMonth366
	Int32U5BU5D_t3030399641* ___DaysToMonth366_5;
	// System.Globalization.Calendar modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.GregorianCalendar::s_defaultInstance
	Calendar_t585061108 * ___s_defaultInstance_6;

public:
	inline static int32_t get_offset_of_DaysToMonth365_4() { return static_cast<int32_t>(offsetof(GregorianCalendar_t3361245568_StaticFields, ___DaysToMonth365_4)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth365_4() const { return ___DaysToMonth365_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth365_4() { return &___DaysToMonth365_4; }
	inline void set_DaysToMonth365_4(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth365_4 = value;
		Il2CppCodeGenWriteBarrier(&___DaysToMonth365_4, value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_5() { return static_cast<int32_t>(offsetof(GregorianCalendar_t3361245568_StaticFields, ___DaysToMonth366_5)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth366_5() const { return ___DaysToMonth366_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth366_5() { return &___DaysToMonth366_5; }
	inline void set_DaysToMonth366_5(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth366_5 = value;
		Il2CppCodeGenWriteBarrier(&___DaysToMonth366_5, value);
	}

	inline static int32_t get_offset_of_s_defaultInstance_6() { return static_cast<int32_t>(offsetof(GregorianCalendar_t3361245568_StaticFields, ___s_defaultInstance_6)); }
	inline Calendar_t585061108 * get_s_defaultInstance_6() const { return ___s_defaultInstance_6; }
	inline Calendar_t585061108 ** get_address_of_s_defaultInstance_6() { return &___s_defaultInstance_6; }
	inline void set_s_defaultInstance_6(Calendar_t585061108 * value)
	{
		___s_defaultInstance_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_defaultInstance_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
