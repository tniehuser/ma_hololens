﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.SimpleAsyncResult
struct SimpleAsyncResult_t2937691397;
// System.Net.WebConnectionStream
struct WebConnectionStream_t1922483508;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream/<SetHeadersAsync>c__AnonStorey1
struct  U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317  : public Il2CppObject
{
public:
	// System.Net.SimpleAsyncResult System.Net.WebConnectionStream/<SetHeadersAsync>c__AnonStorey1::result
	SimpleAsyncResult_t2937691397 * ___result_0;
	// System.Net.WebConnectionStream System.Net.WebConnectionStream/<SetHeadersAsync>c__AnonStorey1::$this
	WebConnectionStream_t1922483508 * ___U24this_1;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317, ___result_0)); }
	inline SimpleAsyncResult_t2937691397 * get_result_0() const { return ___result_0; }
	inline SimpleAsyncResult_t2937691397 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(SimpleAsyncResult_t2937691397 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSetHeadersAsyncU3Ec__AnonStorey1_t1091057317, ___U24this_1)); }
	inline WebConnectionStream_t1922483508 * get_U24this_1() const { return ___U24this_1; }
	inline WebConnectionStream_t1922483508 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebConnectionStream_t1922483508 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
