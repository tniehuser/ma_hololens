﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfoSc2584404353.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeFormatInfoScanner
struct  DateTimeFormatInfoScanner_t2782759166  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> System.Globalization.DateTimeFormatInfoScanner::m_dateWords
	List_1_t1398341365 * ___m_dateWords_0;
	// System.Globalization.DateTimeFormatInfoScanner/FoundDatePattern System.Globalization.DateTimeFormatInfoScanner::m_ymdFlags
	int32_t ___m_ymdFlags_2;

public:
	inline static int32_t get_offset_of_m_dateWords_0() { return static_cast<int32_t>(offsetof(DateTimeFormatInfoScanner_t2782759166, ___m_dateWords_0)); }
	inline List_1_t1398341365 * get_m_dateWords_0() const { return ___m_dateWords_0; }
	inline List_1_t1398341365 ** get_address_of_m_dateWords_0() { return &___m_dateWords_0; }
	inline void set_m_dateWords_0(List_1_t1398341365 * value)
	{
		___m_dateWords_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_dateWords_0, value);
	}

	inline static int32_t get_offset_of_m_ymdFlags_2() { return static_cast<int32_t>(offsetof(DateTimeFormatInfoScanner_t2782759166, ___m_ymdFlags_2)); }
	inline int32_t get_m_ymdFlags_2() const { return ___m_ymdFlags_2; }
	inline int32_t* get_address_of_m_ymdFlags_2() { return &___m_ymdFlags_2; }
	inline void set_m_ymdFlags_2(int32_t value)
	{
		___m_ymdFlags_2 = value;
	}
};

struct DateTimeFormatInfoScanner_t2782759166_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.DateTimeFormatInfoScanner::s_knownWords
	Dictionary_2_t3943999495 * ___s_knownWords_1;

public:
	inline static int32_t get_offset_of_s_knownWords_1() { return static_cast<int32_t>(offsetof(DateTimeFormatInfoScanner_t2782759166_StaticFields, ___s_knownWords_1)); }
	inline Dictionary_2_t3943999495 * get_s_knownWords_1() const { return ___s_knownWords_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_s_knownWords_1() { return &___s_knownWords_1; }
	inline void set_s_knownWords_1(Dictionary_2_t3943999495 * value)
	{
		___s_knownWords_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_knownWords_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
