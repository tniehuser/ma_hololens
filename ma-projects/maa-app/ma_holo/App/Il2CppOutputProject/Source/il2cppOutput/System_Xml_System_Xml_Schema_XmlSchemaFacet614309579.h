﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_FacetType1331259443.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t614309579  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaFacet::value
	String_t* ___value_9;
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_10;
	// System.Xml.Schema.FacetType System.Xml.Schema.XmlSchemaFacet::facetType
	int32_t ___facetType_11;

public:
	inline static int32_t get_offset_of_value_9() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___value_9)); }
	inline String_t* get_value_9() const { return ___value_9; }
	inline String_t** get_address_of_value_9() { return &___value_9; }
	inline void set_value_9(String_t* value)
	{
		___value_9 = value;
		Il2CppCodeGenWriteBarrier(&___value_9, value);
	}

	inline static int32_t get_offset_of_isFixed_10() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___isFixed_10)); }
	inline bool get_isFixed_10() const { return ___isFixed_10; }
	inline bool* get_address_of_isFixed_10() { return &___isFixed_10; }
	inline void set_isFixed_10(bool value)
	{
		___isFixed_10 = value;
	}

	inline static int32_t get_offset_of_facetType_11() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___facetType_11)); }
	inline int32_t get_facetType_11() const { return ___facetType_11; }
	inline int32_t* get_address_of_facetType_11() { return &___facetType_11; }
	inline void set_facetType_11(int32_t value)
	{
		___facetType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
