﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t964629540;
// System.Xml.Schema.SelectorActiveAxis
struct SelectorActiveAxis_t789423304;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ConstraintStruct
struct  ConstraintStruct_t2462842120  : public Il2CppObject
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.ConstraintStruct::constraint
	CompiledIdentityConstraint_t964629540 * ___constraint_0;
	// System.Xml.Schema.SelectorActiveAxis System.Xml.Schema.ConstraintStruct::axisSelector
	SelectorActiveAxis_t789423304 * ___axisSelector_1;
	// System.Collections.ArrayList System.Xml.Schema.ConstraintStruct::axisFields
	ArrayList_t4252133567 * ___axisFields_2;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::qualifiedTable
	Hashtable_t909839986 * ___qualifiedTable_3;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::keyrefTable
	Hashtable_t909839986 * ___keyrefTable_4;
	// System.Int32 System.Xml.Schema.ConstraintStruct::tableDim
	int32_t ___tableDim_5;

public:
	inline static int32_t get_offset_of_constraint_0() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___constraint_0)); }
	inline CompiledIdentityConstraint_t964629540 * get_constraint_0() const { return ___constraint_0; }
	inline CompiledIdentityConstraint_t964629540 ** get_address_of_constraint_0() { return &___constraint_0; }
	inline void set_constraint_0(CompiledIdentityConstraint_t964629540 * value)
	{
		___constraint_0 = value;
		Il2CppCodeGenWriteBarrier(&___constraint_0, value);
	}

	inline static int32_t get_offset_of_axisSelector_1() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___axisSelector_1)); }
	inline SelectorActiveAxis_t789423304 * get_axisSelector_1() const { return ___axisSelector_1; }
	inline SelectorActiveAxis_t789423304 ** get_address_of_axisSelector_1() { return &___axisSelector_1; }
	inline void set_axisSelector_1(SelectorActiveAxis_t789423304 * value)
	{
		___axisSelector_1 = value;
		Il2CppCodeGenWriteBarrier(&___axisSelector_1, value);
	}

	inline static int32_t get_offset_of_axisFields_2() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___axisFields_2)); }
	inline ArrayList_t4252133567 * get_axisFields_2() const { return ___axisFields_2; }
	inline ArrayList_t4252133567 ** get_address_of_axisFields_2() { return &___axisFields_2; }
	inline void set_axisFields_2(ArrayList_t4252133567 * value)
	{
		___axisFields_2 = value;
		Il2CppCodeGenWriteBarrier(&___axisFields_2, value);
	}

	inline static int32_t get_offset_of_qualifiedTable_3() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___qualifiedTable_3)); }
	inline Hashtable_t909839986 * get_qualifiedTable_3() const { return ___qualifiedTable_3; }
	inline Hashtable_t909839986 ** get_address_of_qualifiedTable_3() { return &___qualifiedTable_3; }
	inline void set_qualifiedTable_3(Hashtable_t909839986 * value)
	{
		___qualifiedTable_3 = value;
		Il2CppCodeGenWriteBarrier(&___qualifiedTable_3, value);
	}

	inline static int32_t get_offset_of_keyrefTable_4() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___keyrefTable_4)); }
	inline Hashtable_t909839986 * get_keyrefTable_4() const { return ___keyrefTable_4; }
	inline Hashtable_t909839986 ** get_address_of_keyrefTable_4() { return &___keyrefTable_4; }
	inline void set_keyrefTable_4(Hashtable_t909839986 * value)
	{
		___keyrefTable_4 = value;
		Il2CppCodeGenWriteBarrier(&___keyrefTable_4, value);
	}

	inline static int32_t get_offset_of_tableDim_5() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___tableDim_5)); }
	inline int32_t get_tableDim_5() const { return ___tableDim_5; }
	inline int32_t* get_address_of_tableDim_5() { return &___tableDim_5; }
	inline void set_tableDim_5(int32_t value)
	{
		___tableDim_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
