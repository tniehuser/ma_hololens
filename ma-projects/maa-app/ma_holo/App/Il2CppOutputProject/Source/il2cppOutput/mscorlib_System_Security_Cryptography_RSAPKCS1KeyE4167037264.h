﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_AsymmetricKe3339648384.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Security.Cryptography.RSA
struct RSA_t3719518354;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
struct  RSAPKCS1KeyExchangeFormatter_t4167037264  : public AsymmetricKeyExchangeFormatter_t3339648384
{
public:
	// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::RngValue
	RandomNumberGenerator_t2510243513 * ___RngValue_0;
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::_rsaKey
	RSA_t3719518354 * ____rsaKey_1;
	// System.Nullable`1<System.Boolean> System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::_rsaOverridesEncrypt
	Nullable_1_t2088641033  ____rsaOverridesEncrypt_2;

public:
	inline static int32_t get_offset_of_RngValue_0() { return static_cast<int32_t>(offsetof(RSAPKCS1KeyExchangeFormatter_t4167037264, ___RngValue_0)); }
	inline RandomNumberGenerator_t2510243513 * get_RngValue_0() const { return ___RngValue_0; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of_RngValue_0() { return &___RngValue_0; }
	inline void set_RngValue_0(RandomNumberGenerator_t2510243513 * value)
	{
		___RngValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___RngValue_0, value);
	}

	inline static int32_t get_offset_of__rsaKey_1() { return static_cast<int32_t>(offsetof(RSAPKCS1KeyExchangeFormatter_t4167037264, ____rsaKey_1)); }
	inline RSA_t3719518354 * get__rsaKey_1() const { return ____rsaKey_1; }
	inline RSA_t3719518354 ** get_address_of__rsaKey_1() { return &____rsaKey_1; }
	inline void set__rsaKey_1(RSA_t3719518354 * value)
	{
		____rsaKey_1 = value;
		Il2CppCodeGenWriteBarrier(&____rsaKey_1, value);
	}

	inline static int32_t get_offset_of__rsaOverridesEncrypt_2() { return static_cast<int32_t>(offsetof(RSAPKCS1KeyExchangeFormatter_t4167037264, ____rsaOverridesEncrypt_2)); }
	inline Nullable_1_t2088641033  get__rsaOverridesEncrypt_2() const { return ____rsaOverridesEncrypt_2; }
	inline Nullable_1_t2088641033 * get_address_of__rsaOverridesEncrypt_2() { return &____rsaOverridesEncrypt_2; }
	inline void set__rsaOverridesEncrypt_2(Nullable_1_t2088641033  value)
	{
		____rsaOverridesEncrypt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
