﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ActiveAxis439376929.h"

// System.Xml.Schema.ConstraintStruct
struct ConstraintStruct_t2462842120;
// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SelectorActiveAxis
struct  SelectorActiveAxis_t789423304  : public ActiveAxis_t439376929
{
public:
	// System.Xml.Schema.ConstraintStruct System.Xml.Schema.SelectorActiveAxis::cs
	ConstraintStruct_t2462842120 * ___cs_4;
	// System.Collections.ArrayList System.Xml.Schema.SelectorActiveAxis::KSs
	ArrayList_t4252133567 * ___KSs_5;
	// System.Int32 System.Xml.Schema.SelectorActiveAxis::KSpointer
	int32_t ___KSpointer_6;

public:
	inline static int32_t get_offset_of_cs_4() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___cs_4)); }
	inline ConstraintStruct_t2462842120 * get_cs_4() const { return ___cs_4; }
	inline ConstraintStruct_t2462842120 ** get_address_of_cs_4() { return &___cs_4; }
	inline void set_cs_4(ConstraintStruct_t2462842120 * value)
	{
		___cs_4 = value;
		Il2CppCodeGenWriteBarrier(&___cs_4, value);
	}

	inline static int32_t get_offset_of_KSs_5() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___KSs_5)); }
	inline ArrayList_t4252133567 * get_KSs_5() const { return ___KSs_5; }
	inline ArrayList_t4252133567 ** get_address_of_KSs_5() { return &___KSs_5; }
	inline void set_KSs_5(ArrayList_t4252133567 * value)
	{
		___KSs_5 = value;
		Il2CppCodeGenWriteBarrier(&___KSs_5, value);
	}

	inline static int32_t get_offset_of_KSpointer_6() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___KSpointer_6)); }
	inline int32_t get_KSpointer_6() const { return ___KSpointer_6; }
	inline int32_t* get_address_of_KSpointer_6() { return &___KSpointer_6; }
	inline void set_KSpointer_6(int32_t value)
	{
		___KSpointer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
