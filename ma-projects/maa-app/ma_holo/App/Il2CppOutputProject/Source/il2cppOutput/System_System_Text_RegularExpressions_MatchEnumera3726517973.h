﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t3718216671;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchEnumerator
struct  MatchEnumerator_t3726517973  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchEnumerator::_matchcoll
	MatchCollection_t3718216671 * ____matchcoll_0;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchEnumerator::_match
	Match_t3164245899 * ____match_1;
	// System.Int32 System.Text.RegularExpressions.MatchEnumerator::_curindex
	int32_t ____curindex_2;
	// System.Boolean System.Text.RegularExpressions.MatchEnumerator::_done
	bool ____done_3;

public:
	inline static int32_t get_offset_of__matchcoll_0() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____matchcoll_0)); }
	inline MatchCollection_t3718216671 * get__matchcoll_0() const { return ____matchcoll_0; }
	inline MatchCollection_t3718216671 ** get_address_of__matchcoll_0() { return &____matchcoll_0; }
	inline void set__matchcoll_0(MatchCollection_t3718216671 * value)
	{
		____matchcoll_0 = value;
		Il2CppCodeGenWriteBarrier(&____matchcoll_0, value);
	}

	inline static int32_t get_offset_of__match_1() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____match_1)); }
	inline Match_t3164245899 * get__match_1() const { return ____match_1; }
	inline Match_t3164245899 ** get_address_of__match_1() { return &____match_1; }
	inline void set__match_1(Match_t3164245899 * value)
	{
		____match_1 = value;
		Il2CppCodeGenWriteBarrier(&____match_1, value);
	}

	inline static int32_t get_offset_of__curindex_2() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____curindex_2)); }
	inline int32_t get__curindex_2() const { return ____curindex_2; }
	inline int32_t* get_address_of__curindex_2() { return &____curindex_2; }
	inline void set__curindex_2(int32_t value)
	{
		____curindex_2 = value;
	}

	inline static int32_t get_offset_of__done_3() { return static_cast<int32_t>(offsetof(MatchEnumerator_t3726517973, ____done_3)); }
	inline bool get__done_3() const { return ____done_3; }
	inline bool* get_address_of__done_3() { return &____done_3; }
	inline void set__done_3(bool value)
	{
		____done_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
