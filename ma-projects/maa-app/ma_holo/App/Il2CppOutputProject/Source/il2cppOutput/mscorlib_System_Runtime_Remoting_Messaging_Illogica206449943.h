﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.IllogicalCallContext
struct  IllogicalCallContext_t206449943  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.IllogicalCallContext::m_Datastore
	Hashtable_t909839986 * ___m_Datastore_0;
	// System.Object System.Runtime.Remoting.Messaging.IllogicalCallContext::m_HostContext
	Il2CppObject * ___m_HostContext_1;

public:
	inline static int32_t get_offset_of_m_Datastore_0() { return static_cast<int32_t>(offsetof(IllogicalCallContext_t206449943, ___m_Datastore_0)); }
	inline Hashtable_t909839986 * get_m_Datastore_0() const { return ___m_Datastore_0; }
	inline Hashtable_t909839986 ** get_address_of_m_Datastore_0() { return &___m_Datastore_0; }
	inline void set_m_Datastore_0(Hashtable_t909839986 * value)
	{
		___m_Datastore_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Datastore_0, value);
	}

	inline static int32_t get_offset_of_m_HostContext_1() { return static_cast<int32_t>(offsetof(IllogicalCallContext_t206449943, ___m_HostContext_1)); }
	inline Il2CppObject * get_m_HostContext_1() const { return ___m_HostContext_1; }
	inline Il2CppObject ** get_address_of_m_HostContext_1() { return &___m_HostContext_1; }
	inline void set_m_HostContext_1(Il2CppObject * value)
	{
		___m_HostContext_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_HostContext_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
