﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_IncrementalReadDecoder403822042.h"
#include "System_Xml_System_Xml_IncrementalReadDummyDecoder1924434910.h"
#include "System_Xml_System_Xml_ReadContentAsBinaryHelper1064946902.h"
#include "System_Xml_System_Xml_ReadContentAsBinaryHelper_St4184127726.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_SecureStringHasher2961272395.h"
#include "System_Xml_System_Xml_SecureStringHasher_HashCodeO2463303350.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_XmlConfiguration_XmlConfigur3722747970.h"
#include "System_Xml_System_Xml_XmlConfiguration_XmlReaderSe3603186601.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlTextEncoder2806209204.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl3122949129.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingFunc788942260.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingMod3052286627.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_EntityType1332110843.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_EntityExpa3449258025.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_Incremental232776713.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_LaterInitP3713317471.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_InitInputTyp41671750.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_DtdParserPr633814591.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingSta1278724163.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_XmlContext938582012.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_NoNamespace796425321.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_NodeData2613273532.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_DtdDefault1068264103.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_OnDefaultA3539809886.h"
#include "System_Xml_System_Xml_Formatting1126649075.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_NamespaceState1247635686.h"
#include "System_Xml_System_Xml_XmlTextWriter_TagInfo1244462138.h"
#include "System_Xml_System_Xml_XmlTextWriter_Namespace1808381789.h"
#include "System_Xml_System_Xml_XmlTextWriter_SpecialAttr1516144018.h"
#include "System_Xml_System_Xml_XmlTextWriter_State563545493.h"
#include "System_Xml_System_Xml_XmlTextWriter_Token2485601873.h"
#include "System_Xml_System_Xml_XmlValidatingReader3416770767.h"
#include "System_Xml_System_Xml_XmlValidatingReaderImpl1507412803.h"
#include "System_Xml_System_Xml_XmlValidatingReaderImpl_Pars1774871414.h"
#include "System_Xml_System_Xml_XmlValidatingReaderImpl_Vali2798815538.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_DomNameTable3442363663.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlChildEnumerator1349548835.h"
#include "System_Xml_System_Xml_XmlChildNodes3248922980.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlLoader156301564.h"
#include "System_Xml_System_Xml_XmlName3016058992.h"
#include "System_Xml_System_Xml_XmlNameEx693073035.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap_SmallXmlNode4252813691.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap_SmallXmlNode3009390407.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeReaderNavigator4234558191.h"
#include "System_Xml_System_Xml_XmlNodeReaderNavigator_Virtu2854289803.h"
#include "System_Xml_System_Xml_XmlNodeReader1022603664.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_System_Xml_XmlUnspecifiedAttribute3928824946.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_EmptyEnumerator2328036233.h"
#include "System_Xml_System_Xml_HWStack738999989.h"
#include "System_Xml_System_Xml_PositionInfo3273236083.h"
#include "System_Xml_System_Xml_ReaderPositionInfo4191373494.h"
#include "System_Xml_System_Xml_LineInfo1429635508.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_Ref3802055625.h"
#include "System_Xml_System_Xml_Schema_AxisElement941837363.h"
#include "System_Xml_System_Xml_Schema_AxisStack3388994403.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (IncrementalReadDecoder_t403822042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (IncrementalReadDummyDecoder_t1924434910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (ReadContentAsBinaryHelper_t1064946902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	ReadContentAsBinaryHelper_t1064946902::get_offset_of_reader_0(),
	ReadContentAsBinaryHelper_t1064946902::get_offset_of_state_1(),
	ReadContentAsBinaryHelper_t1064946902::get_offset_of_valueOffset_2(),
	ReadContentAsBinaryHelper_t1064946902::get_offset_of_isEnd_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (State_t4184127726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[4] = 
{
	State_t4184127726::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2409[6] = 
{
	ReadState_t3138905245::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (SecureStringHasher_t2961272395), -1, sizeof(SecureStringHasher_t2961272395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2410[2] = 
{
	SecureStringHasher_t2961272395_StaticFields::get_offset_of_hashCodeDelegate_0(),
	SecureStringHasher_t2961272395::get_offset_of_hashCodeRandomizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (HashCodeOfStringDelegate_t2463303350), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (ValidationType_t1401987383)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[6] = 
{
	ValidationType_t1401987383::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (XmlConfigurationString_t3722747970), -1, sizeof(XmlConfigurationString_t3722747970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2414[2] = 
{
	XmlConfigurationString_t3722747970_StaticFields::get_offset_of_XmlReaderSectionPath_0(),
	XmlConfigurationString_t3722747970_StaticFields::get_offset_of_XsltSectionPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (XmlReaderSection_t3603186601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[10] = 
{
	XmlParserContext_t2728039553::get_offset_of__nt_0(),
	XmlParserContext_t2728039553::get_offset_of__nsMgr_1(),
	XmlParserContext_t2728039553::get_offset_of__docTypeName_2(),
	XmlParserContext_t2728039553::get_offset_of__pubId_3(),
	XmlParserContext_t2728039553::get_offset_of__sysId_4(),
	XmlParserContext_t2728039553::get_offset_of__internalSubset_5(),
	XmlParserContext_t2728039553::get_offset_of__xmlLang_6(),
	XmlParserContext_t2728039553::get_offset_of__xmlSpace_7(),
	XmlParserContext_t2728039553::get_offset_of__baseURI_8(),
	XmlParserContext_t2728039553::get_offset_of__encoding_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (XmlReader_t3675626668), -1, sizeof(XmlReader_t3675626668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2417[3] = 
{
	XmlReader_t3675626668_StaticFields::get_offset_of_IsTextualNodeBitmap_0(),
	XmlReader_t3675626668_StaticFields::get_offset_of_CanReadContentAsBitmap_1(),
	XmlReader_t3675626668_StaticFields::get_offset_of_HasValueBitmap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (XmlReaderSettings_t1578612233), -1, sizeof(XmlReaderSettings_t1578612233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2418[20] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_useAsync_0(),
	XmlReaderSettings_t1578612233::get_offset_of_nameTable_1(),
	XmlReaderSettings_t1578612233::get_offset_of_xmlResolver_2(),
	XmlReaderSettings_t1578612233::get_offset_of_lineNumberOffset_3(),
	XmlReaderSettings_t1578612233::get_offset_of_linePositionOffset_4(),
	XmlReaderSettings_t1578612233::get_offset_of_conformanceLevel_5(),
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_6(),
	XmlReaderSettings_t1578612233::get_offset_of_maxCharactersInDocument_7(),
	XmlReaderSettings_t1578612233::get_offset_of_maxCharactersFromEntities_8(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreWhitespace_9(),
	XmlReaderSettings_t1578612233::get_offset_of_ignorePIs_10(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreComments_11(),
	XmlReaderSettings_t1578612233::get_offset_of_dtdProcessing_12(),
	XmlReaderSettings_t1578612233::get_offset_of_validationType_13(),
	XmlReaderSettings_t1578612233::get_offset_of_validationFlags_14(),
	XmlReaderSettings_t1578612233::get_offset_of_schemas_15(),
	XmlReaderSettings_t1578612233::get_offset_of_closeInput_16(),
	XmlReaderSettings_t1578612233::get_offset_of_isReadOnly_17(),
	XmlReaderSettings_t1578612233::get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_18(),
	XmlReaderSettings_t1578612233_StaticFields::get_offset_of_s_enableLegacyXmlSettings_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2419[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (XmlTextEncoder_t2806209204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[6] = 
{
	XmlTextEncoder_t2806209204::get_offset_of_textWriter_0(),
	XmlTextEncoder_t2806209204::get_offset_of_inAttribute_1(),
	XmlTextEncoder_t2806209204::get_offset_of_quoteChar_2(),
	XmlTextEncoder_t2806209204::get_offset_of_attrValue_3(),
	XmlTextEncoder_t2806209204::get_offset_of_cacheAttrValue_4(),
	XmlTextEncoder_t2806209204::get_offset_of_xmlCharType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	XmlTextReader_t3514170725::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (XmlTextReaderImpl_t3122949129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[76] = 
{
	XmlTextReaderImpl_t3122949129::get_offset_of_parseText_dummyTask_3(),
	XmlTextReaderImpl_t3122949129::get_offset_of_laterInitParam_4(),
	XmlTextReaderImpl_t3122949129::get_offset_of_xmlCharType_5(),
	XmlTextReaderImpl_t3122949129::get_offset_of_ps_6(),
	XmlTextReaderImpl_t3122949129::get_offset_of_parsingFunction_7(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nextParsingFunction_8(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nextNextParsingFunction_9(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nodes_10(),
	XmlTextReaderImpl_t3122949129::get_offset_of_curNode_11(),
	XmlTextReaderImpl_t3122949129::get_offset_of_index_12(),
	XmlTextReaderImpl_t3122949129::get_offset_of_curAttrIndex_13(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attrCount_14(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attrHashtable_15(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attrDuplWalkCount_16(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attrNeedNamespaceLookup_17(),
	XmlTextReaderImpl_t3122949129::get_offset_of_fullAttrCleanup_18(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attrDuplSortingArray_19(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nameTable_20(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nameTableFromSettings_21(),
	XmlTextReaderImpl_t3122949129::get_offset_of_xmlResolver_22(),
	XmlTextReaderImpl_t3122949129::get_offset_of_url_23(),
	XmlTextReaderImpl_t3122949129::get_offset_of_normalize_24(),
	XmlTextReaderImpl_t3122949129::get_offset_of_supportNamespaces_25(),
	XmlTextReaderImpl_t3122949129::get_offset_of_whitespaceHandling_26(),
	XmlTextReaderImpl_t3122949129::get_offset_of_dtdProcessing_27(),
	XmlTextReaderImpl_t3122949129::get_offset_of_entityHandling_28(),
	XmlTextReaderImpl_t3122949129::get_offset_of_ignorePIs_29(),
	XmlTextReaderImpl_t3122949129::get_offset_of_ignoreComments_30(),
	XmlTextReaderImpl_t3122949129::get_offset_of_checkCharacters_31(),
	XmlTextReaderImpl_t3122949129::get_offset_of_lineNumberOffset_32(),
	XmlTextReaderImpl_t3122949129::get_offset_of_linePositionOffset_33(),
	XmlTextReaderImpl_t3122949129::get_offset_of_closeInput_34(),
	XmlTextReaderImpl_t3122949129::get_offset_of_maxCharactersInDocument_35(),
	XmlTextReaderImpl_t3122949129::get_offset_of_maxCharactersFromEntities_36(),
	XmlTextReaderImpl_t3122949129::get_offset_of_v1Compat_37(),
	XmlTextReaderImpl_t3122949129::get_offset_of_namespaceManager_38(),
	XmlTextReaderImpl_t3122949129::get_offset_of_lastPrefix_39(),
	XmlTextReaderImpl_t3122949129::get_offset_of_xmlContext_40(),
	XmlTextReaderImpl_t3122949129::get_offset_of_parsingStatesStack_41(),
	XmlTextReaderImpl_t3122949129::get_offset_of_parsingStatesStackTop_42(),
	XmlTextReaderImpl_t3122949129::get_offset_of_reportedBaseUri_43(),
	XmlTextReaderImpl_t3122949129::get_offset_of_reportedEncoding_44(),
	XmlTextReaderImpl_t3122949129::get_offset_of_dtdInfo_45(),
	XmlTextReaderImpl_t3122949129::get_offset_of_fragmentType_46(),
	XmlTextReaderImpl_t3122949129::get_offset_of_fragmentParserContext_47(),
	XmlTextReaderImpl_t3122949129::get_offset_of_fragment_48(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadDecoder_49(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadState_50(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadLineInfo_51(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadDepth_52(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadLeftStartPos_53(),
	XmlTextReaderImpl_t3122949129::get_offset_of_incReadLeftEndPos_54(),
	XmlTextReaderImpl_t3122949129::get_offset_of_attributeValueBaseEntityId_55(),
	XmlTextReaderImpl_t3122949129::get_offset_of_emptyEntityInAttributeResolved_56(),
	XmlTextReaderImpl_t3122949129::get_offset_of_validationEventHandling_57(),
	XmlTextReaderImpl_t3122949129::get_offset_of_onDefaultAttributeUse_58(),
	XmlTextReaderImpl_t3122949129::get_offset_of_validatingReaderCompatFlag_59(),
	XmlTextReaderImpl_t3122949129::get_offset_of_addDefaultAttributesAndNormalize_60(),
	XmlTextReaderImpl_t3122949129::get_offset_of_stringBuilder_61(),
	XmlTextReaderImpl_t3122949129::get_offset_of_rootElementParsed_62(),
	XmlTextReaderImpl_t3122949129::get_offset_of_standalone_63(),
	XmlTextReaderImpl_t3122949129::get_offset_of_nextEntityId_64(),
	XmlTextReaderImpl_t3122949129::get_offset_of_parsingMode_65(),
	XmlTextReaderImpl_t3122949129::get_offset_of_readState_66(),
	XmlTextReaderImpl_t3122949129::get_offset_of_lastEntity_67(),
	XmlTextReaderImpl_t3122949129::get_offset_of_afterResetState_68(),
	XmlTextReaderImpl_t3122949129::get_offset_of_documentStartBytePos_69(),
	XmlTextReaderImpl_t3122949129::get_offset_of_readValueOffset_70(),
	XmlTextReaderImpl_t3122949129::get_offset_of_charactersInDocument_71(),
	XmlTextReaderImpl_t3122949129::get_offset_of_charactersFromEntities_72(),
	XmlTextReaderImpl_t3122949129::get_offset_of_currentEntities_73(),
	XmlTextReaderImpl_t3122949129::get_offset_of_disableUndeclaredEntityCheck_74(),
	XmlTextReaderImpl_t3122949129::get_offset_of_outerReader_75(),
	XmlTextReaderImpl_t3122949129::get_offset_of_xmlResolverIsSet_76(),
	XmlTextReaderImpl_t3122949129::get_offset_of_Xml_77(),
	XmlTextReaderImpl_t3122949129::get_offset_of_XmlNs_78(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (ParsingFunction_t788942260)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2423[27] = 
{
	ParsingFunction_t788942260::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (ParsingMode_t3052286627)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	ParsingMode_t3052286627::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (EntityType_t1332110843)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2425[9] = 
{
	EntityType_t1332110843::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (EntityExpandType_t3449258025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2426[4] = 
{
	EntityExpandType_t3449258025::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (IncrementalReadState_t232776713)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2427[16] = 
{
	IncrementalReadState_t232776713::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (LaterInitParam_t3713317471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[10] = 
{
	LaterInitParam_t3713317471::get_offset_of_useAsync_0(),
	LaterInitParam_t3713317471::get_offset_of_inputStream_1(),
	LaterInitParam_t3713317471::get_offset_of_inputBytes_2(),
	LaterInitParam_t3713317471::get_offset_of_inputByteCount_3(),
	LaterInitParam_t3713317471::get_offset_of_inputbaseUri_4(),
	LaterInitParam_t3713317471::get_offset_of_inputUriStr_5(),
	LaterInitParam_t3713317471::get_offset_of_inputUriResolver_6(),
	LaterInitParam_t3713317471::get_offset_of_inputContext_7(),
	LaterInitParam_t3713317471::get_offset_of_inputTextReader_8(),
	LaterInitParam_t3713317471::get_offset_of_initType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (InitInputType_t41671750)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2429[5] = 
{
	InitInputType_t41671750::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (DtdParserProxy_t633814591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[1] = 
{
	DtdParserProxy_t633814591::get_offset_of_reader_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (ParsingState_t1278724163)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[21] = 
{
	ParsingState_t1278724163::get_offset_of_chars_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_charPos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_charsUsed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_encoding_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_appendMode_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_stream_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_decoder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_bytes_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_bytePos_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_bytesUsed_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_textReader_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_lineNo_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_lineStartPos_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_baseUriStr_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_baseUri_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_isEof_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_isStreamEof_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_entity_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_entityId_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_eolNormalized_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingState_t1278724163::get_offset_of_entityResolvedManually_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (XmlContext_t938582012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[4] = 
{
	XmlContext_t938582012::get_offset_of_xmlSpace_0(),
	XmlContext_t938582012::get_offset_of_xmlLang_1(),
	XmlContext_t938582012::get_offset_of_defaultNamespace_2(),
	XmlContext_t938582012::get_offset_of_previousContext_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (NoNamespaceManager_t796425321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (NodeData_t2613273532), -1, sizeof(NodeData_t2613273532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2434[20] = 
{
	NodeData_t2613273532_StaticFields::get_offset_of_s_None_0(),
	NodeData_t2613273532::get_offset_of_type_1(),
	NodeData_t2613273532::get_offset_of_localName_2(),
	NodeData_t2613273532::get_offset_of_prefix_3(),
	NodeData_t2613273532::get_offset_of_ns_4(),
	NodeData_t2613273532::get_offset_of_nameWPrefix_5(),
	NodeData_t2613273532::get_offset_of_value_6(),
	NodeData_t2613273532::get_offset_of_chars_7(),
	NodeData_t2613273532::get_offset_of_valueStartPos_8(),
	NodeData_t2613273532::get_offset_of_valueLength_9(),
	NodeData_t2613273532::get_offset_of_lineInfo_10(),
	NodeData_t2613273532::get_offset_of_lineInfo2_11(),
	NodeData_t2613273532::get_offset_of_quoteChar_12(),
	NodeData_t2613273532::get_offset_of_depth_13(),
	NodeData_t2613273532::get_offset_of_isEmptyOrDefault_14(),
	NodeData_t2613273532::get_offset_of_entityId_15(),
	NodeData_t2613273532::get_offset_of_xmlContextPushed_16(),
	NodeData_t2613273532::get_offset_of_nextAttrValueChunk_17(),
	NodeData_t2613273532::get_offset_of_schemaType_18(),
	NodeData_t2613273532::get_offset_of_typedValue_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (DtdDefaultAttributeInfoToNodeDataComparer_t1068264103), -1, sizeof(DtdDefaultAttributeInfoToNodeDataComparer_t1068264103_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[1] = 
{
	DtdDefaultAttributeInfoToNodeDataComparer_t1068264103_StaticFields::get_offset_of_s_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (OnDefaultAttributeUseDelegate_t3539809886), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (Formatting_t1126649075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2437[3] = 
{
	Formatting_t1126649075::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[28] = 
{
	XmlTextWriter_t2527250655::get_offset_of_textWriter_1(),
	XmlTextWriter_t2527250655::get_offset_of_xmlEncoder_2(),
	XmlTextWriter_t2527250655::get_offset_of_encoding_3(),
	XmlTextWriter_t2527250655::get_offset_of_formatting_4(),
	XmlTextWriter_t2527250655::get_offset_of_indented_5(),
	XmlTextWriter_t2527250655::get_offset_of_indentation_6(),
	XmlTextWriter_t2527250655::get_offset_of_indentChar_7(),
	XmlTextWriter_t2527250655::get_offset_of_stack_8(),
	XmlTextWriter_t2527250655::get_offset_of_top_9(),
	XmlTextWriter_t2527250655::get_offset_of_stateTable_10(),
	XmlTextWriter_t2527250655::get_offset_of_currentState_11(),
	XmlTextWriter_t2527250655::get_offset_of_lastToken_12(),
	XmlTextWriter_t2527250655::get_offset_of_base64Encoder_13(),
	XmlTextWriter_t2527250655::get_offset_of_quoteChar_14(),
	XmlTextWriter_t2527250655::get_offset_of_curQuoteChar_15(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_16(),
	XmlTextWriter_t2527250655::get_offset_of_specialAttr_17(),
	XmlTextWriter_t2527250655::get_offset_of_prefixForXmlNs_18(),
	XmlTextWriter_t2527250655::get_offset_of_flush_19(),
	XmlTextWriter_t2527250655::get_offset_of_nsStack_20(),
	XmlTextWriter_t2527250655::get_offset_of_nsTop_21(),
	XmlTextWriter_t2527250655::get_offset_of_nsHashtable_22(),
	XmlTextWriter_t2527250655::get_offset_of_useNsHashtable_23(),
	XmlTextWriter_t2527250655::get_offset_of_xmlCharType_24(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_stateName_25(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_tokenName_26(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_stateTableDefault_27(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_stateTableDocument_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (NamespaceState_t1247635686)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2439[5] = 
{
	NamespaceState_t1247635686::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (TagInfo_t1244462138)+ sizeof (Il2CppObject), sizeof(TagInfo_t1244462138_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2440[9] = 
{
	TagInfo_t1244462138::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_defaultNs_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_defaultNsState_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_prevNsTop_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_prefixCount_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagInfo_t1244462138::get_offset_of_mixed_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (Namespace_t1808381789)+ sizeof (Il2CppObject), sizeof(Namespace_t1808381789_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2441[4] = 
{
	Namespace_t1808381789::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Namespace_t1808381789::get_offset_of_ns_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Namespace_t1808381789::get_offset_of_declared_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Namespace_t1808381789::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (SpecialAttr_t1516144018)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2442[5] = 
{
	SpecialAttr_t1516144018::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (State_t563545493)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[11] = 
{
	State_t563545493::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (Token_t2485601873)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2444[15] = 
{
	Token_t2485601873::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (XmlValidatingReader_t3416770767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	XmlValidatingReader_t3416770767::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (XmlValidatingReaderImpl_t1507412803), -1, sizeof(XmlValidatingReaderImpl_t1507412803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2446[13] = 
{
	XmlValidatingReaderImpl_t1507412803::get_offset_of_coreReader_3(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_coreReaderImpl_4(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_coreReaderNSResolver_5(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_validationType_6(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_validator_7(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_schemaCollection_8(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_processIdentityConstraints_9(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_parsingFunction_10(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_eventHandling_11(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_parserContext_12(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_readBinaryHelper_13(),
	XmlValidatingReaderImpl_t1507412803::get_offset_of_outerReader_14(),
	XmlValidatingReaderImpl_t1507412803_StaticFields::get_offset_of_s_tempResolver_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (ParsingFunction_t1774871414)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[9] = 
{
	ParsingFunction_t1774871414::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (ValidationEventHandling_t2798815538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[2] = 
{
	ValidationEventHandling_t2798815538::get_offset_of_reader_0(),
	ValidationEventHandling_t2798815538::get_offset_of_eventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[1] = 
{
	XmlWriter_t1048088568::get_offset_of_writeNodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2450[8] = 
{
	WriteState_t1534871862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (DomNameTable_t3442363663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[5] = 
{
	DomNameTable_t3442363663::get_offset_of_entries_0(),
	DomNameTable_t3442363663::get_offset_of_count_1(),
	DomNameTable_t3442363663::get_offset_of_mask_2(),
	DomNameTable_t3442363663::get_offset_of_ownerDocument_3(),
	DomNameTable_t3442363663::get_offset_of_nameTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[2] = 
{
	XmlAttribute_t175731005::get_offset_of_name_1(),
	XmlAttribute_t175731005::get_offset_of_lastChild_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (XmlChildEnumerator_t1349548835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[3] = 
{
	XmlChildEnumerator_t1349548835::get_offset_of_container_0(),
	XmlChildEnumerator_t1349548835::get_offset_of_child_1(),
	XmlChildEnumerator_t1349548835::get_offset_of_isFirst_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (XmlChildNodes_t3248922980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	XmlChildNodes_t3248922980::get_offset_of_container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (XmlDeclaration_t1545359137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[3] = 
{
	XmlDeclaration_t1545359137::get_offset_of_version_2(),
	XmlDeclaration_t1545359137::get_offset_of_encoding_3(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2460[44] = 
{
	XmlDocument_t3649534162::get_offset_of_implementation_1(),
	XmlDocument_t3649534162::get_offset_of_domNameTable_2(),
	XmlDocument_t3649534162::get_offset_of_lastChild_3(),
	XmlDocument_t3649534162::get_offset_of_entities_4(),
	XmlDocument_t3649534162::get_offset_of_htElementIdMap_5(),
	XmlDocument_t3649534162::get_offset_of_htElementIDAttrDecl_6(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_7(),
	XmlDocument_t3649534162::get_offset_of_schemas_8(),
	XmlDocument_t3649534162::get_offset_of_reportValidity_9(),
	XmlDocument_t3649534162::get_offset_of_actualLoadingStatus_10(),
	XmlDocument_t3649534162::get_offset_of_onNodeInsertingDelegate_11(),
	XmlDocument_t3649534162::get_offset_of_onNodeInsertedDelegate_12(),
	XmlDocument_t3649534162::get_offset_of_onNodeRemovingDelegate_13(),
	XmlDocument_t3649534162::get_offset_of_onNodeRemovedDelegate_14(),
	XmlDocument_t3649534162::get_offset_of_onNodeChangingDelegate_15(),
	XmlDocument_t3649534162::get_offset_of_onNodeChangedDelegate_16(),
	XmlDocument_t3649534162::get_offset_of_fEntRefNodesPresent_17(),
	XmlDocument_t3649534162::get_offset_of_fCDataNodesPresent_18(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_19(),
	XmlDocument_t3649534162::get_offset_of_isLoading_20(),
	XmlDocument_t3649534162::get_offset_of_strDocumentName_21(),
	XmlDocument_t3649534162::get_offset_of_strDocumentFragmentName_22(),
	XmlDocument_t3649534162::get_offset_of_strCommentName_23(),
	XmlDocument_t3649534162::get_offset_of_strTextName_24(),
	XmlDocument_t3649534162::get_offset_of_strCDataSectionName_25(),
	XmlDocument_t3649534162::get_offset_of_strEntityName_26(),
	XmlDocument_t3649534162::get_offset_of_strID_27(),
	XmlDocument_t3649534162::get_offset_of_strXmlns_28(),
	XmlDocument_t3649534162::get_offset_of_strXml_29(),
	XmlDocument_t3649534162::get_offset_of_strSpace_30(),
	XmlDocument_t3649534162::get_offset_of_strLang_31(),
	XmlDocument_t3649534162::get_offset_of_strEmpty_32(),
	XmlDocument_t3649534162::get_offset_of_strNonSignificantWhitespaceName_33(),
	XmlDocument_t3649534162::get_offset_of_strSignificantWhitespaceName_34(),
	XmlDocument_t3649534162::get_offset_of_strReservedXmlns_35(),
	XmlDocument_t3649534162::get_offset_of_strReservedXml_36(),
	XmlDocument_t3649534162::get_offset_of_baseURI_37(),
	XmlDocument_t3649534162::get_offset_of_resolver_38(),
	XmlDocument_t3649534162::get_offset_of_bSetResolver_39(),
	XmlDocument_t3649534162::get_offset_of_objLock_40(),
	XmlDocument_t3649534162_StaticFields::get_offset_of_EmptyEnumerator_41(),
	XmlDocument_t3649534162_StaticFields::get_offset_of_NotKnownSchemaInfo_42(),
	XmlDocument_t3649534162_StaticFields::get_offset_of_ValidSchemaInfo_43(),
	XmlDocument_t3649534162_StaticFields::get_offset_of_InvalidSchemaInfo_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[8] = 
{
	XmlDocumentType_t824160610::get_offset_of_name_2(),
	XmlDocumentType_t824160610::get_offset_of_publicId_3(),
	XmlDocumentType_t824160610::get_offset_of_systemId_4(),
	XmlDocumentType_t824160610::get_offset_of_internalSubset_5(),
	XmlDocumentType_t824160610::get_offset_of_namespaces_6(),
	XmlDocumentType_t824160610::get_offset_of_entities_7(),
	XmlDocumentType_t824160610::get_offset_of_notations_8(),
	XmlDocumentType_t824160610::get_offset_of_schemaInfo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[3] = 
{
	XmlElement_t2877111883::get_offset_of_name_2(),
	XmlElement_t2877111883::get_offset_of_attributes_3(),
	XmlElement_t2877111883::get_offset_of_lastChild_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[8] = 
{
	XmlEntity_t4027255380::get_offset_of_publicId_1(),
	XmlEntity_t4027255380::get_offset_of_systemId_2(),
	XmlEntity_t4027255380::get_offset_of_notationName_3(),
	XmlEntity_t4027255380::get_offset_of_name_4(),
	XmlEntity_t4027255380::get_offset_of_unparsedReplacementStr_5(),
	XmlEntity_t4027255380::get_offset_of_baseURI_6(),
	XmlEntity_t4027255380::get_offset_of_lastChild_7(),
	XmlEntity_t4027255380::get_offset_of_childrenFoliating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_name_2(),
	XmlEntityReference_t3053868353::get_offset_of_lastChild_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2466[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_nameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (XmlLoader_t156301564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[3] = 
{
	XmlLoader_t156301564::get_offset_of_doc_0(),
	XmlLoader_t156301564::get_offset_of_reader_1(),
	XmlLoader_t156301564::get_offset_of_preserveWhitespace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (XmlName_t3016058992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[7] = 
{
	XmlName_t3016058992::get_offset_of_prefix_0(),
	XmlName_t3016058992::get_offset_of_localName_1(),
	XmlName_t3016058992::get_offset_of_ns_2(),
	XmlName_t3016058992::get_offset_of_name_3(),
	XmlName_t3016058992::get_offset_of_hashCode_4(),
	XmlName_t3016058992::get_offset_of_ownerDoc_5(),
	XmlName_t3016058992::get_offset_of_next_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (XmlNameEx_t693073035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[4] = 
{
	XmlNameEx_t693073035::get_offset_of_flags_7(),
	XmlNameEx_t693073035::get_offset_of_memberType_8(),
	XmlNameEx_t693073035::get_offset_of_schemaType_9(),
	XmlNameEx_t693073035::get_offset_of_decl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (XmlNamedNodeMap_t145210370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	XmlNamedNodeMap_t145210370::get_offset_of_parent_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (SmallXmlNodeList_t4252813691)+ sizeof (Il2CppObject), sizeof(SmallXmlNodeList_t4252813691_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2473[1] = 
{
	SmallXmlNodeList_t4252813691::get_offset_of_field_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (SingleObjectEnumerator_t3009390407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[2] = 
{
	SingleObjectEnumerator_t3009390407::get_offset_of_loneValue_0(),
	SingleObjectEnumerator_t3009390407::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_action_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_node_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_oldParent_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_newParent_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of_newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (XmlNode_t616554813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	XmlNode_t616554813::get_offset_of_parentNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (XmlNodeReaderNavigator_t4234558191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[17] = 
{
	XmlNodeReaderNavigator_t4234558191::get_offset_of_curNode_0(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_elemNode_1(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_logNode_2(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_attrIndex_3(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_logAttrIndex_4(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nameTable_5(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_doc_6(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nAttrInd_7(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nDeclarationAttrCount_8(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nDocTypeAttrCount_9(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nLogLevel_10(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_nLogAttrInd_11(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_bLogOnAttrVal_12(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_bCreatedOnAttribute_13(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_decNodeAttributes_14(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_docTypeNodeAttributes_15(),
	XmlNodeReaderNavigator_t4234558191::get_offset_of_bOnAttrVal_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (VirtualAttribute_t2854289803)+ sizeof (Il2CppObject), sizeof(VirtualAttribute_t2854289803_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	VirtualAttribute_t2854289803::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VirtualAttribute_t2854289803::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (XmlNodeReader_t1022603664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[9] = 
{
	XmlNodeReader_t1022603664::get_offset_of_readerNav_3(),
	XmlNodeReader_t1022603664::get_offset_of_nodeType_4(),
	XmlNodeReader_t1022603664::get_offset_of_curDepth_5(),
	XmlNodeReader_t1022603664::get_offset_of_readState_6(),
	XmlNodeReader_t1022603664::get_offset_of_fEOF_7(),
	XmlNodeReader_t1022603664::get_offset_of_bResolveEntity_8(),
	XmlNodeReader_t1022603664::get_offset_of_bStartFromDocument_9(),
	XmlNodeReader_t1022603664::get_offset_of_bInReadBinary_10(),
	XmlNodeReader_t1022603664::get_offset_of_readBinaryHelper_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[3] = 
{
	XmlNotation_t206561061::get_offset_of_publicId_1(),
	XmlNotation_t206561061::get_offset_of_systemId_2(),
	XmlNotation_t206561061::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_2(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (XmlUnspecifiedAttribute_t3928824946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	XmlUnspecifiedAttribute_t3928824946::get_offset_of_fSpecified_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (EmptyEnumerator_t2328036233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (HWStack_t738999989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[5] = 
{
	HWStack_t738999989::get_offset_of_stack_0(),
	HWStack_t738999989::get_offset_of_growthRate_1(),
	HWStack_t738999989::get_offset_of_used_2(),
	HWStack_t738999989::get_offset_of_size_3(),
	HWStack_t738999989::get_offset_of_limit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (PositionInfo_t3273236083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (ReaderPositionInfo_t4191373494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[1] = 
{
	ReaderPositionInfo_t4191373494::get_offset_of_lineInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (LineInfo_t1429635508)+ sizeof (Il2CppObject), sizeof(LineInfo_t1429635508 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2494[2] = 
{
	LineInfo_t1429635508::get_offset_of_lineNo_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LineInfo_t1429635508::get_offset_of_linePos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[4] = 
{
	NameTable_t594386929::get_offset_of_entries_0(),
	NameTable_t594386929::get_offset_of_count_1(),
	NameTable_t594386929::get_offset_of_mask_2(),
	NameTable_t594386929::get_offset_of_hashCodeRandomizer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[3] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hashCode_1(),
	Entry_t2583369454::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (Ref_t3802055625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (AxisElement_t941837363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[4] = 
{
	AxisElement_t941837363::get_offset_of_curNode_0(),
	AxisElement_t941837363::get_offset_of_rootDepth_1(),
	AxisElement_t941837363::get_offset_of_curDepth_2(),
	AxisElement_t941837363::get_offset_of_isMatch_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (AxisStack_t3388994403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[3] = 
{
	AxisStack_t3388994403::get_offset_of_stack_0(),
	AxisStack_t3388994403::get_offset_of_subtree_1(),
	AxisStack_t3388994403::get_offset_of_parent_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
