﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2874429441.h"

// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4015859774;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInfo
struct  XmlSchemaInfo_t2864028808  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isDefault
	bool ___isDefault_0;
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isNil
	bool ___isNil_1;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::schemaElement
	XmlSchemaElement_t2433337156 * ___schemaElement_2;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::schemaAttribute
	XmlSchemaAttribute_t4015859774 * ___schemaAttribute_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::schemaType
	XmlSchemaType_t1795078578 * ___schemaType_4;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::memberType
	XmlSchemaSimpleType_t248156492 * ___memberType_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::validity
	int32_t ___validity_6;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaInfo::contentType
	int32_t ___contentType_7;

public:
	inline static int32_t get_offset_of_isDefault_0() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___isDefault_0)); }
	inline bool get_isDefault_0() const { return ___isDefault_0; }
	inline bool* get_address_of_isDefault_0() { return &___isDefault_0; }
	inline void set_isDefault_0(bool value)
	{
		___isDefault_0 = value;
	}

	inline static int32_t get_offset_of_isNil_1() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___isNil_1)); }
	inline bool get_isNil_1() const { return ___isNil_1; }
	inline bool* get_address_of_isNil_1() { return &___isNil_1; }
	inline void set_isNil_1(bool value)
	{
		___isNil_1 = value;
	}

	inline static int32_t get_offset_of_schemaElement_2() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___schemaElement_2)); }
	inline XmlSchemaElement_t2433337156 * get_schemaElement_2() const { return ___schemaElement_2; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_schemaElement_2() { return &___schemaElement_2; }
	inline void set_schemaElement_2(XmlSchemaElement_t2433337156 * value)
	{
		___schemaElement_2 = value;
		Il2CppCodeGenWriteBarrier(&___schemaElement_2, value);
	}

	inline static int32_t get_offset_of_schemaAttribute_3() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___schemaAttribute_3)); }
	inline XmlSchemaAttribute_t4015859774 * get_schemaAttribute_3() const { return ___schemaAttribute_3; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_schemaAttribute_3() { return &___schemaAttribute_3; }
	inline void set_schemaAttribute_3(XmlSchemaAttribute_t4015859774 * value)
	{
		___schemaAttribute_3 = value;
		Il2CppCodeGenWriteBarrier(&___schemaAttribute_3, value);
	}

	inline static int32_t get_offset_of_schemaType_4() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___schemaType_4)); }
	inline XmlSchemaType_t1795078578 * get_schemaType_4() const { return ___schemaType_4; }
	inline XmlSchemaType_t1795078578 ** get_address_of_schemaType_4() { return &___schemaType_4; }
	inline void set_schemaType_4(XmlSchemaType_t1795078578 * value)
	{
		___schemaType_4 = value;
		Il2CppCodeGenWriteBarrier(&___schemaType_4, value);
	}

	inline static int32_t get_offset_of_memberType_5() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___memberType_5)); }
	inline XmlSchemaSimpleType_t248156492 * get_memberType_5() const { return ___memberType_5; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_memberType_5() { return &___memberType_5; }
	inline void set_memberType_5(XmlSchemaSimpleType_t248156492 * value)
	{
		___memberType_5 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_5, value);
	}

	inline static int32_t get_offset_of_validity_6() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___validity_6)); }
	inline int32_t get_validity_6() const { return ___validity_6; }
	inline int32_t* get_address_of_validity_6() { return &___validity_6; }
	inline void set_validity_6(int32_t value)
	{
		___validity_6 = value;
	}

	inline static int32_t get_offset_of_contentType_7() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___contentType_7)); }
	inline int32_t get_contentType_7() const { return ___contentType_7; }
	inline int32_t* get_address_of_contentType_7() { return &___contentType_7; }
	inline void set_contentType_7(int32_t value)
	{
		___contentType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
