﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ConfigurationManagerInternalFactory/Instance
struct  Instance_t3259527665  : public Il2CppObject
{
public:

public:
};

struct Instance_t3259527665_StaticFields
{
public:
	// System.Boolean System.Diagnostics.ConfigurationManagerInternalFactory/Instance::SetConfigurationSystemInProgress
	bool ___SetConfigurationSystemInProgress_0;

public:
	inline static int32_t get_offset_of_SetConfigurationSystemInProgress_0() { return static_cast<int32_t>(offsetof(Instance_t3259527665_StaticFields, ___SetConfigurationSystemInProgress_0)); }
	inline bool get_SetConfigurationSystemInProgress_0() const { return ___SetConfigurationSystemInProgress_0; }
	inline bool* get_address_of_SetConfigurationSystemInProgress_0() { return &___SetConfigurationSystemInProgress_0; }
	inline void set_SetConfigurationSystemInProgress_0(bool value)
	{
		___SetConfigurationSystemInProgress_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
