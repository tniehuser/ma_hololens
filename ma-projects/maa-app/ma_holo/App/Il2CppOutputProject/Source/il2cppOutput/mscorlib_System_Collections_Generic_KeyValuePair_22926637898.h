﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.Thread
struct Thread_t241561612;
// System.Diagnostics.StackTrace
struct StackTrace_t2500644597;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Threading.Thread,System.Diagnostics.StackTrace>
struct  KeyValuePair_2_t2926637898 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Thread_t241561612 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	StackTrace_t2500644597 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2926637898, ___key_0)); }
	inline Thread_t241561612 * get_key_0() const { return ___key_0; }
	inline Thread_t241561612 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Thread_t241561612 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2926637898, ___value_1)); }
	inline StackTrace_t2500644597 * get_value_1() const { return ___value_1; }
	inline StackTrace_t2500644597 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(StackTrace_t2500644597 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
