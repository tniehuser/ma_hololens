﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SafeSerializationEventArgs
struct  SafeSerializationEventArgs_t1088103422  : public EventArgs_t3289624707
{
public:
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.SafeSerializationEventArgs::m_streamingContext
	StreamingContext_t1417235061  ___m_streamingContext_1;
	// System.Collections.Generic.List`1<System.Object> System.Runtime.Serialization.SafeSerializationEventArgs::m_serializedStates
	List_1_t2058570427 * ___m_serializedStates_2;

public:
	inline static int32_t get_offset_of_m_streamingContext_1() { return static_cast<int32_t>(offsetof(SafeSerializationEventArgs_t1088103422, ___m_streamingContext_1)); }
	inline StreamingContext_t1417235061  get_m_streamingContext_1() const { return ___m_streamingContext_1; }
	inline StreamingContext_t1417235061 * get_address_of_m_streamingContext_1() { return &___m_streamingContext_1; }
	inline void set_m_streamingContext_1(StreamingContext_t1417235061  value)
	{
		___m_streamingContext_1 = value;
	}

	inline static int32_t get_offset_of_m_serializedStates_2() { return static_cast<int32_t>(offsetof(SafeSerializationEventArgs_t1088103422, ___m_serializedStates_2)); }
	inline List_1_t2058570427 * get_m_serializedStates_2() const { return ___m_serializedStates_2; }
	inline List_1_t2058570427 ** get_address_of_m_serializedStates_2() { return &___m_serializedStates_2; }
	inline void set_m_serializedStates_2(List_1_t2058570427 * value)
	{
		___m_serializedStates_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_serializedStates_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
