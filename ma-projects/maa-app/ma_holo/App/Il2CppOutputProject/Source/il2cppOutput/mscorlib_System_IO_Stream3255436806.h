﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t2745753060;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t461808439;
// System.Func`1<System.Threading.SemaphoreSlim>
struct Func_1_t2416201121;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.Action`2<System.Threading.Tasks.Task,System.Object>
struct Action_2_t3798078305;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public MarshalByRefObject_t1285298191
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t2745753060 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t461808439 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t2745753060 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t2745753060 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t2745753060 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier(&____activeReadWriteTask_2, value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t461808439 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t461808439 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t461808439 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier(&____asyncActiveSemaphore_3, value);
	}
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_1;
	// System.Func`1<System.Threading.SemaphoreSlim> System.IO.Stream::<>f__am$cache0
	Func_1_t2416201121 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<System.Object,System.Int32> System.IO.Stream::<>f__am$cache2
	Func_2_t2207932334 * ___U3CU3Ef__amU24cache2_5;
	// System.Func`2<System.Object,System.Int32> System.IO.Stream::<>f__am$cache5
	Func_2_t2207932334 * ___U3CU3Ef__amU24cache5_6;
	// System.Action`2<System.Threading.Tasks.Task,System.Object> System.IO.Stream::<>f__am$cache6
	Action_2_t3798078305 * ___U3CU3Ef__amU24cache6_7;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_1)); }
	inline Stream_t3255436806 * get_Null_1() const { return ___Null_1; }
	inline Stream_t3255436806 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t3255436806 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier(&___Null_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_1_t2416201121 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_1_t2416201121 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_1_t2416201121 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_5() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___U3CU3Ef__amU24cache2_5)); }
	inline Func_2_t2207932334 * get_U3CU3Ef__amU24cache2_5() const { return ___U3CU3Ef__amU24cache2_5; }
	inline Func_2_t2207932334 ** get_address_of_U3CU3Ef__amU24cache2_5() { return &___U3CU3Ef__amU24cache2_5; }
	inline void set_U3CU3Ef__amU24cache2_5(Func_2_t2207932334 * value)
	{
		___U3CU3Ef__amU24cache2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_6() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___U3CU3Ef__amU24cache5_6)); }
	inline Func_2_t2207932334 * get_U3CU3Ef__amU24cache5_6() const { return ___U3CU3Ef__amU24cache5_6; }
	inline Func_2_t2207932334 ** get_address_of_U3CU3Ef__amU24cache5_6() { return &___U3CU3Ef__amU24cache5_6; }
	inline void set_U3CU3Ef__amU24cache5_6(Func_2_t2207932334 * value)
	{
		___U3CU3Ef__amU24cache5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_7() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___U3CU3Ef__amU24cache6_7)); }
	inline Action_2_t3798078305 * get_U3CU3Ef__amU24cache6_7() const { return ___U3CU3Ef__amU24cache6_7; }
	inline Action_2_t3798078305 ** get_address_of_U3CU3Ef__amU24cache6_7() { return &___U3CU3Ef__amU24cache6_7; }
	inline void set_U3CU3Ef__amU24cache6_7(Action_2_t3798078305 * value)
	{
		___U3CU3Ef__amU24cache6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
