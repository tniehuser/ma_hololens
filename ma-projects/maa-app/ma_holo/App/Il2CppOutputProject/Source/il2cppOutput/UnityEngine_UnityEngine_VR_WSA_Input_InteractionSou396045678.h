﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1509183288.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.InteractionSourceProperties
struct  InteractionSourceProperties_t396045678 
{
public:
	// System.Double UnityEngine.VR.WSA.Input.InteractionSourceProperties::m_sourceLossRisk
	double ___m_sourceLossRisk_0;
	// UnityEngine.Vector3 UnityEngine.VR.WSA.Input.InteractionSourceProperties::m_sourceLossMitigationDirection
	Vector3_t2243707580  ___m_sourceLossMitigationDirection_1;
	// UnityEngine.VR.WSA.Input.InteractionSourceLocation UnityEngine.VR.WSA.Input.InteractionSourceProperties::m_location
	InteractionSourceLocation_t1509183288  ___m_location_2;

public:
	inline static int32_t get_offset_of_m_sourceLossRisk_0() { return static_cast<int32_t>(offsetof(InteractionSourceProperties_t396045678, ___m_sourceLossRisk_0)); }
	inline double get_m_sourceLossRisk_0() const { return ___m_sourceLossRisk_0; }
	inline double* get_address_of_m_sourceLossRisk_0() { return &___m_sourceLossRisk_0; }
	inline void set_m_sourceLossRisk_0(double value)
	{
		___m_sourceLossRisk_0 = value;
	}

	inline static int32_t get_offset_of_m_sourceLossMitigationDirection_1() { return static_cast<int32_t>(offsetof(InteractionSourceProperties_t396045678, ___m_sourceLossMitigationDirection_1)); }
	inline Vector3_t2243707580  get_m_sourceLossMitigationDirection_1() const { return ___m_sourceLossMitigationDirection_1; }
	inline Vector3_t2243707580 * get_address_of_m_sourceLossMitigationDirection_1() { return &___m_sourceLossMitigationDirection_1; }
	inline void set_m_sourceLossMitigationDirection_1(Vector3_t2243707580  value)
	{
		___m_sourceLossMitigationDirection_1 = value;
	}

	inline static int32_t get_offset_of_m_location_2() { return static_cast<int32_t>(offsetof(InteractionSourceProperties_t396045678, ___m_location_2)); }
	inline InteractionSourceLocation_t1509183288  get_m_location_2() const { return ___m_location_2; }
	inline InteractionSourceLocation_t1509183288 * get_address_of_m_location_2() { return &___m_location_2; }
	inline void set_m_location_2(InteractionSourceLocation_t1509183288  value)
	{
		___m_location_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
