﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"

// System.String
struct String_t;
// System.Xml.Schema.NamespaceList
struct NamespaceList_t848177191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAny
struct  XmlSchemaAny_t3277730824  : public XmlSchemaParticle_t3365045970
{
public:
	// System.String System.Xml.Schema.XmlSchemaAny::ns
	String_t* ___ns_13;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAny::processContents
	int32_t ___processContents_14;
	// System.Xml.Schema.NamespaceList System.Xml.Schema.XmlSchemaAny::namespaceList
	NamespaceList_t848177191 * ___namespaceList_15;

public:
	inline static int32_t get_offset_of_ns_13() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___ns_13)); }
	inline String_t* get_ns_13() const { return ___ns_13; }
	inline String_t** get_address_of_ns_13() { return &___ns_13; }
	inline void set_ns_13(String_t* value)
	{
		___ns_13 = value;
		Il2CppCodeGenWriteBarrier(&___ns_13, value);
	}

	inline static int32_t get_offset_of_processContents_14() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___processContents_14)); }
	inline int32_t get_processContents_14() const { return ___processContents_14; }
	inline int32_t* get_address_of_processContents_14() { return &___processContents_14; }
	inline void set_processContents_14(int32_t value)
	{
		___processContents_14 = value;
	}

	inline static int32_t get_offset_of_namespaceList_15() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___namespaceList_15)); }
	inline NamespaceList_t848177191 * get_namespaceList_15() const { return ___namespaceList_15; }
	inline NamespaceList_t848177191 ** get_address_of_namespaceList_15() { return &___namespaceList_15; }
	inline void set_namespaceList_15(NamespaceList_t848177191 * value)
	{
		___namespaceList_15 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceList_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
