﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_nonNegativeI1851861419.h"

// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_positiveInteger
struct  Datatype_positiveInteger_t3806316496  : public Datatype_nonNegativeInteger_t1851861419
{
public:

public:
};

struct Datatype_positiveInteger_t3806316496_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_positiveInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_97;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_97() { return static_cast<int32_t>(offsetof(Datatype_positiveInteger_t3806316496_StaticFields, ___numeric10FacetsChecker_97)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_97() const { return ___numeric10FacetsChecker_97; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_97() { return &___numeric10FacetsChecker_97; }
	inline void set_numeric10FacetsChecker_97(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_97 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_97, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
