﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "System_System_Net_CookieVariant839969627.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderVariantInfo
struct  HeaderVariantInfo_t2058796272 
{
public:
	// System.String System.Net.HeaderVariantInfo::m_name
	String_t* ___m_name_0;
	// System.Net.CookieVariant System.Net.HeaderVariantInfo::m_variant
	int32_t ___m_variant_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_t2058796272, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_0, value);
	}

	inline static int32_t get_offset_of_m_variant_1() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_t2058796272, ___m_variant_1)); }
	inline int32_t get_m_variant_1() const { return ___m_variant_1; }
	inline int32_t* get_address_of_m_variant_1() { return &___m_variant_1; }
	inline void set_m_variant_1(int32_t value)
	{
		___m_variant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_t2058796272_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_variant_1;
};
// Native definition for COM marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_t2058796272_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_variant_1;
};
