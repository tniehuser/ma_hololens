﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectNull
struct  ObjectNull_t1089955196  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectNull::nullCount
	int32_t ___nullCount_0;

public:
	inline static int32_t get_offset_of_nullCount_0() { return static_cast<int32_t>(offsetof(ObjectNull_t1089955196, ___nullCount_0)); }
	inline int32_t get_nullCount_0() const { return ___nullCount_0; }
	inline int32_t* get_address_of_nullCount_0() { return &___nullCount_0; }
	inline void set_nullCount_0(int32_t value)
	{
		___nullCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
