﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SchemaBuilder908297946.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[]
struct XdrAttributeEntryU5BU5D_t4145474522;
// System.Xml.Schema.XdrBuilder/XdrEntry[]
struct XdrEntryU5BU5D_t1886355550;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.String
struct String_t;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.Schema.ParticleContentValidator
struct ParticleContentValidator_t1341047977;
// System.Xml.Schema.XdrBuilder/XdrEntry
struct XdrEntry_t2813485863;
// System.Xml.HWStack
struct HWStack_t738999989;
// System.Xml.Schema.XdrBuilder/ElementContent
struct ElementContent_t3043903854;
// System.Xml.Schema.XdrBuilder/GroupContent
struct GroupContent_t1687431939;
// System.Xml.Schema.XdrBuilder/AttributeContent
struct AttributeContent_t2961854454;
// System.Xml.Schema.XdrBuilder/DeclBaseInfo
struct DeclBaseInfo_t1711216790;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder
struct  XdrBuilder_t1520335019  : public SchemaBuilder_t908297946
{
public:
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XdrBuilder::_SchemaInfo
	SchemaInfo_t87206461 * ____SchemaInfo_14;
	// System.String System.Xml.Schema.XdrBuilder::_TargetNamespace
	String_t* ____TargetNamespace_15;
	// System.Xml.XmlReader System.Xml.Schema.XdrBuilder::_reader
	XmlReader_t3675626668 * ____reader_16;
	// System.Xml.PositionInfo System.Xml.Schema.XdrBuilder::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_17;
	// System.Xml.Schema.ParticleContentValidator System.Xml.Schema.XdrBuilder::_contentValidator
	ParticleContentValidator_t1341047977 * ____contentValidator_18;
	// System.Xml.Schema.XdrBuilder/XdrEntry System.Xml.Schema.XdrBuilder::_CurState
	XdrEntry_t2813485863 * ____CurState_19;
	// System.Xml.Schema.XdrBuilder/XdrEntry System.Xml.Schema.XdrBuilder::_NextState
	XdrEntry_t2813485863 * ____NextState_20;
	// System.Xml.HWStack System.Xml.Schema.XdrBuilder::_StateHistory
	HWStack_t738999989 * ____StateHistory_21;
	// System.Xml.HWStack System.Xml.Schema.XdrBuilder::_GroupStack
	HWStack_t738999989 * ____GroupStack_22;
	// System.String System.Xml.Schema.XdrBuilder::_XdrName
	String_t* ____XdrName_23;
	// System.String System.Xml.Schema.XdrBuilder::_XdrPrefix
	String_t* ____XdrPrefix_24;
	// System.Xml.Schema.XdrBuilder/ElementContent System.Xml.Schema.XdrBuilder::_ElementDef
	ElementContent_t3043903854 * ____ElementDef_25;
	// System.Xml.Schema.XdrBuilder/GroupContent System.Xml.Schema.XdrBuilder::_GroupDef
	GroupContent_t1687431939 * ____GroupDef_26;
	// System.Xml.Schema.XdrBuilder/AttributeContent System.Xml.Schema.XdrBuilder::_AttributeDef
	AttributeContent_t2961854454 * ____AttributeDef_27;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder::_UndefinedAttributeTypes
	DeclBaseInfo_t1711216790 * ____UndefinedAttributeTypes_28;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder::_BaseDecl
	DeclBaseInfo_t1711216790 * ____BaseDecl_29;
	// System.Xml.XmlNameTable System.Xml.Schema.XdrBuilder::_NameTable
	XmlNameTable_t1345805268 * ____NameTable_30;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XdrBuilder::_SchemaNames
	SchemaNames_t1619962557 * ____SchemaNames_31;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XdrBuilder::_CurNsMgr
	XmlNamespaceManager_t486731501 * ____CurNsMgr_32;
	// System.String System.Xml.Schema.XdrBuilder::_Text
	String_t* ____Text_33;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XdrBuilder::validationEventHandler
	ValidationEventHandler_t1580700381 * ___validationEventHandler_34;
	// System.Collections.Hashtable System.Xml.Schema.XdrBuilder::_UndeclaredElements
	Hashtable_t909839986 * ____UndeclaredElements_35;
	// System.Xml.XmlResolver System.Xml.Schema.XdrBuilder::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_36;

public:
	inline static int32_t get_offset_of__SchemaInfo_14() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____SchemaInfo_14)); }
	inline SchemaInfo_t87206461 * get__SchemaInfo_14() const { return ____SchemaInfo_14; }
	inline SchemaInfo_t87206461 ** get_address_of__SchemaInfo_14() { return &____SchemaInfo_14; }
	inline void set__SchemaInfo_14(SchemaInfo_t87206461 * value)
	{
		____SchemaInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&____SchemaInfo_14, value);
	}

	inline static int32_t get_offset_of__TargetNamespace_15() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____TargetNamespace_15)); }
	inline String_t* get__TargetNamespace_15() const { return ____TargetNamespace_15; }
	inline String_t** get_address_of__TargetNamespace_15() { return &____TargetNamespace_15; }
	inline void set__TargetNamespace_15(String_t* value)
	{
		____TargetNamespace_15 = value;
		Il2CppCodeGenWriteBarrier(&____TargetNamespace_15, value);
	}

	inline static int32_t get_offset_of__reader_16() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____reader_16)); }
	inline XmlReader_t3675626668 * get__reader_16() const { return ____reader_16; }
	inline XmlReader_t3675626668 ** get_address_of__reader_16() { return &____reader_16; }
	inline void set__reader_16(XmlReader_t3675626668 * value)
	{
		____reader_16 = value;
		Il2CppCodeGenWriteBarrier(&____reader_16, value);
	}

	inline static int32_t get_offset_of_positionInfo_17() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ___positionInfo_17)); }
	inline PositionInfo_t3273236083 * get_positionInfo_17() const { return ___positionInfo_17; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_17() { return &___positionInfo_17; }
	inline void set_positionInfo_17(PositionInfo_t3273236083 * value)
	{
		___positionInfo_17 = value;
		Il2CppCodeGenWriteBarrier(&___positionInfo_17, value);
	}

	inline static int32_t get_offset_of__contentValidator_18() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____contentValidator_18)); }
	inline ParticleContentValidator_t1341047977 * get__contentValidator_18() const { return ____contentValidator_18; }
	inline ParticleContentValidator_t1341047977 ** get_address_of__contentValidator_18() { return &____contentValidator_18; }
	inline void set__contentValidator_18(ParticleContentValidator_t1341047977 * value)
	{
		____contentValidator_18 = value;
		Il2CppCodeGenWriteBarrier(&____contentValidator_18, value);
	}

	inline static int32_t get_offset_of__CurState_19() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____CurState_19)); }
	inline XdrEntry_t2813485863 * get__CurState_19() const { return ____CurState_19; }
	inline XdrEntry_t2813485863 ** get_address_of__CurState_19() { return &____CurState_19; }
	inline void set__CurState_19(XdrEntry_t2813485863 * value)
	{
		____CurState_19 = value;
		Il2CppCodeGenWriteBarrier(&____CurState_19, value);
	}

	inline static int32_t get_offset_of__NextState_20() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____NextState_20)); }
	inline XdrEntry_t2813485863 * get__NextState_20() const { return ____NextState_20; }
	inline XdrEntry_t2813485863 ** get_address_of__NextState_20() { return &____NextState_20; }
	inline void set__NextState_20(XdrEntry_t2813485863 * value)
	{
		____NextState_20 = value;
		Il2CppCodeGenWriteBarrier(&____NextState_20, value);
	}

	inline static int32_t get_offset_of__StateHistory_21() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____StateHistory_21)); }
	inline HWStack_t738999989 * get__StateHistory_21() const { return ____StateHistory_21; }
	inline HWStack_t738999989 ** get_address_of__StateHistory_21() { return &____StateHistory_21; }
	inline void set__StateHistory_21(HWStack_t738999989 * value)
	{
		____StateHistory_21 = value;
		Il2CppCodeGenWriteBarrier(&____StateHistory_21, value);
	}

	inline static int32_t get_offset_of__GroupStack_22() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____GroupStack_22)); }
	inline HWStack_t738999989 * get__GroupStack_22() const { return ____GroupStack_22; }
	inline HWStack_t738999989 ** get_address_of__GroupStack_22() { return &____GroupStack_22; }
	inline void set__GroupStack_22(HWStack_t738999989 * value)
	{
		____GroupStack_22 = value;
		Il2CppCodeGenWriteBarrier(&____GroupStack_22, value);
	}

	inline static int32_t get_offset_of__XdrName_23() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____XdrName_23)); }
	inline String_t* get__XdrName_23() const { return ____XdrName_23; }
	inline String_t** get_address_of__XdrName_23() { return &____XdrName_23; }
	inline void set__XdrName_23(String_t* value)
	{
		____XdrName_23 = value;
		Il2CppCodeGenWriteBarrier(&____XdrName_23, value);
	}

	inline static int32_t get_offset_of__XdrPrefix_24() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____XdrPrefix_24)); }
	inline String_t* get__XdrPrefix_24() const { return ____XdrPrefix_24; }
	inline String_t** get_address_of__XdrPrefix_24() { return &____XdrPrefix_24; }
	inline void set__XdrPrefix_24(String_t* value)
	{
		____XdrPrefix_24 = value;
		Il2CppCodeGenWriteBarrier(&____XdrPrefix_24, value);
	}

	inline static int32_t get_offset_of__ElementDef_25() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____ElementDef_25)); }
	inline ElementContent_t3043903854 * get__ElementDef_25() const { return ____ElementDef_25; }
	inline ElementContent_t3043903854 ** get_address_of__ElementDef_25() { return &____ElementDef_25; }
	inline void set__ElementDef_25(ElementContent_t3043903854 * value)
	{
		____ElementDef_25 = value;
		Il2CppCodeGenWriteBarrier(&____ElementDef_25, value);
	}

	inline static int32_t get_offset_of__GroupDef_26() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____GroupDef_26)); }
	inline GroupContent_t1687431939 * get__GroupDef_26() const { return ____GroupDef_26; }
	inline GroupContent_t1687431939 ** get_address_of__GroupDef_26() { return &____GroupDef_26; }
	inline void set__GroupDef_26(GroupContent_t1687431939 * value)
	{
		____GroupDef_26 = value;
		Il2CppCodeGenWriteBarrier(&____GroupDef_26, value);
	}

	inline static int32_t get_offset_of__AttributeDef_27() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____AttributeDef_27)); }
	inline AttributeContent_t2961854454 * get__AttributeDef_27() const { return ____AttributeDef_27; }
	inline AttributeContent_t2961854454 ** get_address_of__AttributeDef_27() { return &____AttributeDef_27; }
	inline void set__AttributeDef_27(AttributeContent_t2961854454 * value)
	{
		____AttributeDef_27 = value;
		Il2CppCodeGenWriteBarrier(&____AttributeDef_27, value);
	}

	inline static int32_t get_offset_of__UndefinedAttributeTypes_28() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____UndefinedAttributeTypes_28)); }
	inline DeclBaseInfo_t1711216790 * get__UndefinedAttributeTypes_28() const { return ____UndefinedAttributeTypes_28; }
	inline DeclBaseInfo_t1711216790 ** get_address_of__UndefinedAttributeTypes_28() { return &____UndefinedAttributeTypes_28; }
	inline void set__UndefinedAttributeTypes_28(DeclBaseInfo_t1711216790 * value)
	{
		____UndefinedAttributeTypes_28 = value;
		Il2CppCodeGenWriteBarrier(&____UndefinedAttributeTypes_28, value);
	}

	inline static int32_t get_offset_of__BaseDecl_29() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____BaseDecl_29)); }
	inline DeclBaseInfo_t1711216790 * get__BaseDecl_29() const { return ____BaseDecl_29; }
	inline DeclBaseInfo_t1711216790 ** get_address_of__BaseDecl_29() { return &____BaseDecl_29; }
	inline void set__BaseDecl_29(DeclBaseInfo_t1711216790 * value)
	{
		____BaseDecl_29 = value;
		Il2CppCodeGenWriteBarrier(&____BaseDecl_29, value);
	}

	inline static int32_t get_offset_of__NameTable_30() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____NameTable_30)); }
	inline XmlNameTable_t1345805268 * get__NameTable_30() const { return ____NameTable_30; }
	inline XmlNameTable_t1345805268 ** get_address_of__NameTable_30() { return &____NameTable_30; }
	inline void set__NameTable_30(XmlNameTable_t1345805268 * value)
	{
		____NameTable_30 = value;
		Il2CppCodeGenWriteBarrier(&____NameTable_30, value);
	}

	inline static int32_t get_offset_of__SchemaNames_31() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____SchemaNames_31)); }
	inline SchemaNames_t1619962557 * get__SchemaNames_31() const { return ____SchemaNames_31; }
	inline SchemaNames_t1619962557 ** get_address_of__SchemaNames_31() { return &____SchemaNames_31; }
	inline void set__SchemaNames_31(SchemaNames_t1619962557 * value)
	{
		____SchemaNames_31 = value;
		Il2CppCodeGenWriteBarrier(&____SchemaNames_31, value);
	}

	inline static int32_t get_offset_of__CurNsMgr_32() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____CurNsMgr_32)); }
	inline XmlNamespaceManager_t486731501 * get__CurNsMgr_32() const { return ____CurNsMgr_32; }
	inline XmlNamespaceManager_t486731501 ** get_address_of__CurNsMgr_32() { return &____CurNsMgr_32; }
	inline void set__CurNsMgr_32(XmlNamespaceManager_t486731501 * value)
	{
		____CurNsMgr_32 = value;
		Il2CppCodeGenWriteBarrier(&____CurNsMgr_32, value);
	}

	inline static int32_t get_offset_of__Text_33() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____Text_33)); }
	inline String_t* get__Text_33() const { return ____Text_33; }
	inline String_t** get_address_of__Text_33() { return &____Text_33; }
	inline void set__Text_33(String_t* value)
	{
		____Text_33 = value;
		Il2CppCodeGenWriteBarrier(&____Text_33, value);
	}

	inline static int32_t get_offset_of_validationEventHandler_34() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ___validationEventHandler_34)); }
	inline ValidationEventHandler_t1580700381 * get_validationEventHandler_34() const { return ___validationEventHandler_34; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_validationEventHandler_34() { return &___validationEventHandler_34; }
	inline void set_validationEventHandler_34(ValidationEventHandler_t1580700381 * value)
	{
		___validationEventHandler_34 = value;
		Il2CppCodeGenWriteBarrier(&___validationEventHandler_34, value);
	}

	inline static int32_t get_offset_of__UndeclaredElements_35() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ____UndeclaredElements_35)); }
	inline Hashtable_t909839986 * get__UndeclaredElements_35() const { return ____UndeclaredElements_35; }
	inline Hashtable_t909839986 ** get_address_of__UndeclaredElements_35() { return &____UndeclaredElements_35; }
	inline void set__UndeclaredElements_35(Hashtable_t909839986 * value)
	{
		____UndeclaredElements_35 = value;
		Il2CppCodeGenWriteBarrier(&____UndeclaredElements_35, value);
	}

	inline static int32_t get_offset_of_xmlResolver_36() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019, ___xmlResolver_36)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_36() const { return ___xmlResolver_36; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_36() { return &___xmlResolver_36; }
	inline void set_xmlResolver_36(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_36 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_36, value);
	}
};

struct XdrBuilder_t1520335019_StaticFields
{
public:
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Root_Element
	Int32U5BU5D_t3030399641* ___S_XDR_Root_Element_0;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Root_SubElements
	Int32U5BU5D_t3030399641* ___S_XDR_Root_SubElements_1;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_ElementType_SubElements
	Int32U5BU5D_t3030399641* ___S_XDR_ElementType_SubElements_2;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeType_SubElements
	Int32U5BU5D_t3030399641* ___S_XDR_AttributeType_SubElements_3;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Group_SubElements
	Int32U5BU5D_t3030399641* ___S_XDR_Group_SubElements_4;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Root_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_Root_Attributes_5;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_ElementType_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_ElementType_Attributes_6;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeType_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_AttributeType_Attributes_7;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Element_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_Element_Attributes_8;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Attribute_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_Attribute_Attributes_9;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Group_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_Group_Attributes_10;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_ElementDataType_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_ElementDataType_Attributes_11;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeDataType_Attributes
	XdrAttributeEntryU5BU5D_t4145474522* ___S_XDR_AttributeDataType_Attributes_12;
	// System.Xml.Schema.XdrBuilder/XdrEntry[] System.Xml.Schema.XdrBuilder::S_SchemaEntries
	XdrEntryU5BU5D_t1886355550* ___S_SchemaEntries_13;

public:
	inline static int32_t get_offset_of_S_XDR_Root_Element_0() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Root_Element_0)); }
	inline Int32U5BU5D_t3030399641* get_S_XDR_Root_Element_0() const { return ___S_XDR_Root_Element_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_S_XDR_Root_Element_0() { return &___S_XDR_Root_Element_0; }
	inline void set_S_XDR_Root_Element_0(Int32U5BU5D_t3030399641* value)
	{
		___S_XDR_Root_Element_0 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Root_Element_0, value);
	}

	inline static int32_t get_offset_of_S_XDR_Root_SubElements_1() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Root_SubElements_1)); }
	inline Int32U5BU5D_t3030399641* get_S_XDR_Root_SubElements_1() const { return ___S_XDR_Root_SubElements_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_S_XDR_Root_SubElements_1() { return &___S_XDR_Root_SubElements_1; }
	inline void set_S_XDR_Root_SubElements_1(Int32U5BU5D_t3030399641* value)
	{
		___S_XDR_Root_SubElements_1 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Root_SubElements_1, value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementType_SubElements_2() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_ElementType_SubElements_2)); }
	inline Int32U5BU5D_t3030399641* get_S_XDR_ElementType_SubElements_2() const { return ___S_XDR_ElementType_SubElements_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_S_XDR_ElementType_SubElements_2() { return &___S_XDR_ElementType_SubElements_2; }
	inline void set_S_XDR_ElementType_SubElements_2(Int32U5BU5D_t3030399641* value)
	{
		___S_XDR_ElementType_SubElements_2 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_ElementType_SubElements_2, value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeType_SubElements_3() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_AttributeType_SubElements_3)); }
	inline Int32U5BU5D_t3030399641* get_S_XDR_AttributeType_SubElements_3() const { return ___S_XDR_AttributeType_SubElements_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_S_XDR_AttributeType_SubElements_3() { return &___S_XDR_AttributeType_SubElements_3; }
	inline void set_S_XDR_AttributeType_SubElements_3(Int32U5BU5D_t3030399641* value)
	{
		___S_XDR_AttributeType_SubElements_3 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_AttributeType_SubElements_3, value);
	}

	inline static int32_t get_offset_of_S_XDR_Group_SubElements_4() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Group_SubElements_4)); }
	inline Int32U5BU5D_t3030399641* get_S_XDR_Group_SubElements_4() const { return ___S_XDR_Group_SubElements_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_S_XDR_Group_SubElements_4() { return &___S_XDR_Group_SubElements_4; }
	inline void set_S_XDR_Group_SubElements_4(Int32U5BU5D_t3030399641* value)
	{
		___S_XDR_Group_SubElements_4 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Group_SubElements_4, value);
	}

	inline static int32_t get_offset_of_S_XDR_Root_Attributes_5() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Root_Attributes_5)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_Root_Attributes_5() const { return ___S_XDR_Root_Attributes_5; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_Root_Attributes_5() { return &___S_XDR_Root_Attributes_5; }
	inline void set_S_XDR_Root_Attributes_5(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_Root_Attributes_5 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Root_Attributes_5, value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementType_Attributes_6() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_ElementType_Attributes_6)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_ElementType_Attributes_6() const { return ___S_XDR_ElementType_Attributes_6; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_ElementType_Attributes_6() { return &___S_XDR_ElementType_Attributes_6; }
	inline void set_S_XDR_ElementType_Attributes_6(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_ElementType_Attributes_6 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_ElementType_Attributes_6, value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeType_Attributes_7() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_AttributeType_Attributes_7)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_AttributeType_Attributes_7() const { return ___S_XDR_AttributeType_Attributes_7; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_AttributeType_Attributes_7() { return &___S_XDR_AttributeType_Attributes_7; }
	inline void set_S_XDR_AttributeType_Attributes_7(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_AttributeType_Attributes_7 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_AttributeType_Attributes_7, value);
	}

	inline static int32_t get_offset_of_S_XDR_Element_Attributes_8() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Element_Attributes_8)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_Element_Attributes_8() const { return ___S_XDR_Element_Attributes_8; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_Element_Attributes_8() { return &___S_XDR_Element_Attributes_8; }
	inline void set_S_XDR_Element_Attributes_8(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_Element_Attributes_8 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Element_Attributes_8, value);
	}

	inline static int32_t get_offset_of_S_XDR_Attribute_Attributes_9() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Attribute_Attributes_9)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_Attribute_Attributes_9() const { return ___S_XDR_Attribute_Attributes_9; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_Attribute_Attributes_9() { return &___S_XDR_Attribute_Attributes_9; }
	inline void set_S_XDR_Attribute_Attributes_9(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_Attribute_Attributes_9 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Attribute_Attributes_9, value);
	}

	inline static int32_t get_offset_of_S_XDR_Group_Attributes_10() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_Group_Attributes_10)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_Group_Attributes_10() const { return ___S_XDR_Group_Attributes_10; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_Group_Attributes_10() { return &___S_XDR_Group_Attributes_10; }
	inline void set_S_XDR_Group_Attributes_10(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_Group_Attributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_Group_Attributes_10, value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementDataType_Attributes_11() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_ElementDataType_Attributes_11)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_ElementDataType_Attributes_11() const { return ___S_XDR_ElementDataType_Attributes_11; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_ElementDataType_Attributes_11() { return &___S_XDR_ElementDataType_Attributes_11; }
	inline void set_S_XDR_ElementDataType_Attributes_11(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_ElementDataType_Attributes_11 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_ElementDataType_Attributes_11, value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeDataType_Attributes_12() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_XDR_AttributeDataType_Attributes_12)); }
	inline XdrAttributeEntryU5BU5D_t4145474522* get_S_XDR_AttributeDataType_Attributes_12() const { return ___S_XDR_AttributeDataType_Attributes_12; }
	inline XdrAttributeEntryU5BU5D_t4145474522** get_address_of_S_XDR_AttributeDataType_Attributes_12() { return &___S_XDR_AttributeDataType_Attributes_12; }
	inline void set_S_XDR_AttributeDataType_Attributes_12(XdrAttributeEntryU5BU5D_t4145474522* value)
	{
		___S_XDR_AttributeDataType_Attributes_12 = value;
		Il2CppCodeGenWriteBarrier(&___S_XDR_AttributeDataType_Attributes_12, value);
	}

	inline static int32_t get_offset_of_S_SchemaEntries_13() { return static_cast<int32_t>(offsetof(XdrBuilder_t1520335019_StaticFields, ___S_SchemaEntries_13)); }
	inline XdrEntryU5BU5D_t1886355550* get_S_SchemaEntries_13() const { return ___S_SchemaEntries_13; }
	inline XdrEntryU5BU5D_t1886355550** get_address_of_S_SchemaEntries_13() { return &___S_SchemaEntries_13; }
	inline void set_S_SchemaEntries_13(XdrEntryU5BU5D_t1886355550* value)
	{
		___S_SchemaEntries_13 = value;
		Il2CppCodeGenWriteBarrier(&___S_SchemaEntries_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
