﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_Mono_RuntimeRemoteClassHandle2923639406.h"

// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t298428346;
struct RealProxy_t298428346_marshaled_pinvoke;
struct RealProxy_t298428346_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Proxies.TransparentProxy
struct  TransparentProxy_t3836393972  : public Il2CppObject
{
public:
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.TransparentProxy::_rp
	RealProxy_t298428346 * ____rp_0;
	// Mono.RuntimeRemoteClassHandle System.Runtime.Remoting.Proxies.TransparentProxy::_class
	RuntimeRemoteClassHandle_t2923639406  ____class_1;
	// System.Boolean System.Runtime.Remoting.Proxies.TransparentProxy::_custom_type_info
	bool ____custom_type_info_2;

public:
	inline static int32_t get_offset_of__rp_0() { return static_cast<int32_t>(offsetof(TransparentProxy_t3836393972, ____rp_0)); }
	inline RealProxy_t298428346 * get__rp_0() const { return ____rp_0; }
	inline RealProxy_t298428346 ** get_address_of__rp_0() { return &____rp_0; }
	inline void set__rp_0(RealProxy_t298428346 * value)
	{
		____rp_0 = value;
		Il2CppCodeGenWriteBarrier(&____rp_0, value);
	}

	inline static int32_t get_offset_of__class_1() { return static_cast<int32_t>(offsetof(TransparentProxy_t3836393972, ____class_1)); }
	inline RuntimeRemoteClassHandle_t2923639406  get__class_1() const { return ____class_1; }
	inline RuntimeRemoteClassHandle_t2923639406 * get_address_of__class_1() { return &____class_1; }
	inline void set__class_1(RuntimeRemoteClassHandle_t2923639406  value)
	{
		____class_1 = value;
	}

	inline static int32_t get_offset_of__custom_type_info_2() { return static_cast<int32_t>(offsetof(TransparentProxy_t3836393972, ____custom_type_info_2)); }
	inline bool get__custom_type_info_2() const { return ____custom_type_info_2; }
	inline bool* get_address_of__custom_type_info_2() { return &____custom_type_info_2; }
	inline void set__custom_type_info_2(bool value)
	{
		____custom_type_info_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Remoting.Proxies.TransparentProxy
struct TransparentProxy_t3836393972_marshaled_pinvoke
{
	RealProxy_t298428346_marshaled_pinvoke* ____rp_0;
	RuntimeRemoteClassHandle_t2923639406_marshaled_pinvoke ____class_1;
	int32_t ____custom_type_info_2;
};
// Native definition for COM marshalling of System.Runtime.Remoting.Proxies.TransparentProxy
struct TransparentProxy_t3836393972_marshaled_com
{
	RealProxy_t298428346_marshaled_com* ____rp_0;
	RuntimeRemoteClassHandle_t2923639406_marshaled_com ____class_1;
	int32_t ____custom_type_info_2;
};
