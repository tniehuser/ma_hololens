﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "MetaArcade_Core_MetaArcade_Core_Signals_WeakActionL119657226.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application_LowMemoryCallba642977590.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Screen786852042.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard601950206.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817.h"
#include "UnityEngine_UnityEngine_Input1785128008.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Time31991979.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3250302433.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1502856514.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog1642125421.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog3690655641.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog2739717665.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecog1627945097.h"
#include "UnityEngine_UnityEngine_Windows_Speech_PhraseRecogn438723648.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRec894834159.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe1495849926.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe1941514337.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe3326551541.h"
#include "UnityEngine_UnityEngine_Windows_Speech_DictationRe3730830311.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Script4271526.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_ContactFilter2D1672660996.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_AI_NavMesh1481227028.h"
#include "UnityEngine_UnityEngine_AI_NavMesh_OnNavMeshPreUpd2039022291.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (WeakActionList_t119657226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[2] = 
{
	WeakActionList_t119657226::get_offset_of_m_internalList_0(),
	WeakActionList_t119657226::get_offset_of_m_sync_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (Application_t354826772), -1, sizeof(Application_t354826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[3] = 
{
	Application_t354826772_StaticFields::get_offset_of_lowMemory_0(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandler_1(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (LowMemoryCallback_t642977590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (LogCallback_t1867914413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (AssetBundleCreateRequest_t1038783543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (AssetBundleRequest_t2674559435), sizeof(AssetBundleRequest_t2674559435_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (AssetBundle_t2054978754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (AsyncOperation_t3814632279), sizeof(AsyncOperation_t3814632279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2909[1] = 
{
	AsyncOperation_t3814632279::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (WaitForSeconds_t3839502067), sizeof(WaitForSeconds_t3839502067_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2910[1] = 
{
	WaitForSeconds_t3839502067::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (WaitForFixedUpdate_t3968615785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (WaitForEndOfFrame_t1785723201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (Coroutine_t2299508840), sizeof(Coroutine_t2299508840_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2913[1] = 
{
	Coroutine_t2299508840::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (ScriptableObject_t1975622470), sizeof(ScriptableObject_t1975622470_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (Behaviour_t955675639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (Camera_t189460977), -1, sizeof(Camera_t189460977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2916[3] = 
{
	Camera_t189460977_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t189460977_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t189460977_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (CameraCallback_t834278767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (Component_t3819376471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (CullingGroupEvent_t1057617917)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t1057617917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2919[3] = 
{
	CullingGroupEvent_t1057617917::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (CullingGroup_t1091689465), sizeof(CullingGroup_t1091689465_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2920[2] = 
{
	CullingGroup_t1091689465::get_offset_of_m_Ptr_0(),
	CullingGroup_t1091689465::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (StateChanged_t2480912210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (DebugLogHandler_t865810509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (Debug_t1368543263), -1, sizeof(Debug_t1368543263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2923[1] = 
{
	Debug_t1368543263_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (Display_t3666191348), -1, sizeof(Display_t3666191348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	Display_t3666191348::get_offset_of_nativeDisplay_0(),
	Display_t3666191348_StaticFields::get_offset_of_displays_1(),
	Display_t3666191348_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t3666191348_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (DisplaysUpdatedDelegate_t3423469815), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (GameObject_t1756533147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (Gradient_t3600583008), sizeof(Gradient_t3600583008_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2927[1] = 
{
	Gradient_t3600583008::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (MeshFilter_t3026937449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (Screen_t786852042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (RectOffset_t3387826427), sizeof(RectOffset_t3387826427_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[2] = 
{
	RectOffset_t3387826427::get_offset_of_m_Ptr_0(),
	RectOffset_t3387826427::get_offset_of_m_SourceStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (GUIElement_t3381083099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (GUILayer_t3254902478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (TouchScreenKeyboard_t601950206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (Gyroscope_t1705362817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (Input_t1785128008), -1, sizeof(Input_t1785128008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2935[1] = 
{
	Input_t1785128008_StaticFields::get_offset_of_m_MainGyro_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (LayerMask_t3188175821)+ sizeof (Il2CppObject), sizeof(LayerMask_t3188175821 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2936[1] = 
{
	LayerMask_t3188175821::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (Vector3_t2243707580)+ sizeof (Il2CppObject), sizeof(Vector3_t2243707580 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2937[3] = 
{
	Vector3_t2243707580::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (Quaternion_t4030073918)+ sizeof (Il2CppObject), sizeof(Quaternion_t4030073918 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2938[4] = 
{
	Quaternion_t4030073918::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (Bounds_t3033363703)+ sizeof (Il2CppObject), sizeof(Bounds_t3033363703 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	Bounds_t3033363703::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t3033363703::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (Mathf_t2336485820)+ sizeof (Il2CppObject), sizeof(Mathf_t2336485820 ), sizeof(Mathf_t2336485820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	Mathf_t2336485820_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (Keyframe_t1449471340)+ sizeof (Il2CppObject), sizeof(Keyframe_t1449471340 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2941[4] = 
{
	Keyframe_t1449471340::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (AnimationCurve_t3306541151), sizeof(AnimationCurve_t3306541151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2942[1] = 
{
	AnimationCurve_t3306541151::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (MonoBehaviour_t1158329972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (ResourceRequest_t2560315377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[2] = 
{
	ResourceRequest_t2560315377::get_offset_of_m_Path_1(),
	ResourceRequest_t2560315377::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (Resources_t339470017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (Texture_t2243626319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (Texture2D_t3542995729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (RenderTexture_t2666733923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (Time_t31991979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (HideFlags_t1434274199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2950[10] = 
{
	HideFlags_t1434274199::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (Object_t1021602117), sizeof(Object_t1021602117_marshaled_pinvoke), sizeof(Object_t1021602117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2951[2] = 
{
	Object_t1021602117::get_offset_of_m_CachedPtr_0(),
	Object_t1021602117_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (AudioType_t4076847944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2952[14] = 
{
	AudioType_t4076847944::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (YieldInstruction_t3462875981), sizeof(YieldInstruction_t3462875981_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (PlayState_t3250302433)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2954[3] = 
{
	PlayState_t3250302433::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (PlayableHandle_t1502856514)+ sizeof (Il2CppObject), sizeof(PlayableHandle_t1502856514 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2955[2] = 
{
	PlayableHandle_t1502856514::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayableHandle_t1502856514::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (Playable_t3667545548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[1] = 
{
	Playable_t3667545548::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (ScriptPlayable_t4067966717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (Scene_t1684909666)+ sizeof (Il2CppObject), sizeof(Scene_t1684909666 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2958[1] = 
{
	Scene_t1684909666::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (LoadSceneMode_t2981886439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2959[3] = 
{
	LoadSceneMode_t2981886439::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (SceneManager_t90660965), -1, sizeof(SceneManager_t90660965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2960[3] = 
{
	SceneManager_t90660965_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t90660965_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t90660965_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (PhraseRecognitionSystem_t1642125421), -1, sizeof(PhraseRecognitionSystem_t1642125421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2961[2] = 
{
	PhraseRecognitionSystem_t1642125421_StaticFields::get_offset_of_OnError_0(),
	PhraseRecognitionSystem_t1642125421_StaticFields::get_offset_of_OnStatusChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (ErrorDelegate_t3690655641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (StatusDelegate_t2739717665), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (PhraseRecognizer_t1627945097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[2] = 
{
	PhraseRecognizer_t1627945097::get_offset_of_m_Recognizer_0(),
	PhraseRecognizer_t1627945097::get_offset_of_OnPhraseRecognized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (PhraseRecognizedDelegate_t438723648), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (DictationRecognizer_t894834159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[5] = 
{
	DictationRecognizer_t894834159::get_offset_of_m_Recognizer_0(),
	DictationRecognizer_t894834159::get_offset_of_DictationHypothesis_1(),
	DictationRecognizer_t894834159::get_offset_of_DictationResult_2(),
	DictationRecognizer_t894834159::get_offset_of_DictationComplete_3(),
	DictationRecognizer_t894834159::get_offset_of_DictationError_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (DictationHypothesisDelegate_t1495849926), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (DictationResultDelegate_t1941514337), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (DictationCompletedDelegate_t3326551541), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (DictationErrorHandler_t3730830311), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (ScriptableRenderContext_t4271526)+ sizeof (Il2CppObject), sizeof(ScriptableRenderContext_t4271526 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	ScriptableRenderContext_t4271526::get_offset_of_m_Ptr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (Transform_t3275118058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (Enumerator_t1251553160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[2] = 
{
	Enumerator_t1251553160::get_offset_of_outer_0(),
	Enumerator_t1251553160::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (RectTransform_t3349966182), -1, sizeof(RectTransform_t3349966182_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2974[1] = 
{
	RectTransform_t3349966182_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (ReapplyDrivenProperties_t2020713228), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (ControllerColliderHit_t4070855101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[7] = 
{
	ControllerColliderHit_t4070855101::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Point_2(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (Collision_t2876846408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[5] = 
{
	Collision_t2876846408::get_offset_of_m_Impulse_0(),
	Collision_t2876846408::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2876846408::get_offset_of_m_Rigidbody_2(),
	Collision_t2876846408::get_offset_of_m_Collider_3(),
	Collision_t2876846408::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (QueryTriggerInteraction_t478029726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2978[4] = 
{
	QueryTriggerInteraction_t478029726::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (Physics_t634932869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (ContactPoint_t1376425630)+ sizeof (Il2CppObject), sizeof(ContactPoint_t1376425630 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2980[5] = 
{
	ContactPoint_t1376425630::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (Rigidbody_t4233889191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (Collider_t3497673348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (MeshCollider_t2718867283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (RaycastHit_t87180320)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[6] = 
{
	RaycastHit_t87180320::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (CharacterController_t4094781467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (RaycastHit2D_t4063908774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[6] = 
{
	RaycastHit2D_t4063908774::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (Physics2D_t2540166467), -1, sizeof(Physics2D_t2540166467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2987[1] = 
{
	Physics2D_t2540166467_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (ContactFilter2D_t1672660996)+ sizeof (Il2CppObject), sizeof(ContactFilter2D_t1672660996_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2988[11] = 
{
	ContactFilter2D_t1672660996::get_offset_of_useTriggers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useLayerMask_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useDepth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useOutsideDepth_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useNormalAngle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useOutsideNormalAngle_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_layerMask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_minDepth_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_maxDepth_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_minNormalAngle_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_maxNormalAngle_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (Rigidbody2D_t502193897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (Collider2D_t646061738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (ContactPoint2D_t3659330976)+ sizeof (Il2CppObject), sizeof(ContactPoint2D_t3659330976 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2991[11] = 
{
	ContactPoint2D_t3659330976::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_RelativeVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_NormalImpulse_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_TangentImpulse_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Collider_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherCollider_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Rigidbody_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherRigidbody_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Enabled_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (Collision2D_t1539500754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[7] = 
{
	Collision2D_t1539500754::get_offset_of_m_Collider_0(),
	Collision2D_t1539500754::get_offset_of_m_OtherCollider_1(),
	Collision2D_t1539500754::get_offset_of_m_Rigidbody_2(),
	Collision2D_t1539500754::get_offset_of_m_OtherRigidbody_3(),
	Collision2D_t1539500754::get_offset_of_m_Contacts_4(),
	Collision2D_t1539500754::get_offset_of_m_RelativeVelocity_5(),
	Collision2D_t1539500754::get_offset_of_m_Enabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (NavMesh_t1481227028), -1, sizeof(NavMesh_t1481227028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2993[1] = 
{
	NavMesh_t1481227028_StaticFields::get_offset_of_onPreUpdate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (OnNavMeshPreUpdate_t2039022291), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (AudioSettings_t3144015719), -1, sizeof(AudioSettings_t3144015719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2995[1] = 
{
	AudioSettings_t3144015719_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (AudioConfigurationChangeHandler_t3743753033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (AudioClip_t1932558630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[2] = 
{
	AudioClip_t1932558630::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t1932558630::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (PCMReaderCallback_t3007145346), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (PCMSetPositionCallback_t421863554), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
