﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.ICredentials
struct ICredentials_t3855617113;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/CFWebProxy
struct  CFWebProxy_t1432115055  : public Il2CppObject
{
public:
	// System.Net.ICredentials Mono.Net.CFNetwork/CFWebProxy::credentials
	Il2CppObject * ___credentials_0;
	// System.Boolean Mono.Net.CFNetwork/CFWebProxy::userSpecified
	bool ___userSpecified_1;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(CFWebProxy_t1432115055, ___credentials_0)); }
	inline Il2CppObject * get_credentials_0() const { return ___credentials_0; }
	inline Il2CppObject ** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(Il2CppObject * value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_0, value);
	}

	inline static int32_t get_offset_of_userSpecified_1() { return static_cast<int32_t>(offsetof(CFWebProxy_t1432115055, ___userSpecified_1)); }
	inline bool get_userSpecified_1() const { return ___userSpecified_1; }
	inline bool* get_address_of_userSpecified_1() { return &___userSpecified_1; }
	inline void set_userSpecified_1(bool value)
	{
		___userSpecified_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
