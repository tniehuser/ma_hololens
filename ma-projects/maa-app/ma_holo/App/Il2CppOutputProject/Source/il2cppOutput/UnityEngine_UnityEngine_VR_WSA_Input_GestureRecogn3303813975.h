﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate
struct HoldCanceledEventDelegate_t1855824201;
// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate
struct HoldCompletedEventDelegate_t3501137495;
// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate
struct HoldStartedEventDelegate_t3415544547;
// UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate
struct TappedEventDelegate_t2437856093;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate
struct ManipulationCanceledEventDelegate_t2553296249;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate
struct ManipulationCompletedEventDelegate_t2759974023;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate
struct ManipulationStartedEventDelegate_t2487153075;
// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate
struct ManipulationUpdatedEventDelegate_t2394416487;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate
struct NavigationCanceledEventDelegate_t1045620518;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate
struct NavigationCompletedEventDelegate_t1982950364;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate
struct NavigationStartedEventDelegate_t3123342528;
// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate
struct NavigationUpdatedEventDelegate_t3960847450;
// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate
struct RecognitionEndedEventDelegate_t831649826;
// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate
struct RecognitionStartedEventDelegate_t3116179703;
// UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate
struct GestureErrorDelegate_t2343902086;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.GestureRecognizer
struct  GestureRecognizer_t3303813975  : public Il2CppObject
{
public:
	// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::HoldCanceledEvent
	HoldCanceledEventDelegate_t1855824201 * ___HoldCanceledEvent_0;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::HoldCompletedEvent
	HoldCompletedEventDelegate_t3501137495 * ___HoldCompletedEvent_1;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::HoldStartedEvent
	HoldStartedEventDelegate_t3415544547 * ___HoldStartedEvent_2;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::TappedEvent
	TappedEventDelegate_t2437856093 * ___TappedEvent_3;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::ManipulationCanceledEvent
	ManipulationCanceledEventDelegate_t2553296249 * ___ManipulationCanceledEvent_4;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::ManipulationCompletedEvent
	ManipulationCompletedEventDelegate_t2759974023 * ___ManipulationCompletedEvent_5;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::ManipulationStartedEvent
	ManipulationStartedEventDelegate_t2487153075 * ___ManipulationStartedEvent_6;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::ManipulationUpdatedEvent
	ManipulationUpdatedEventDelegate_t2394416487 * ___ManipulationUpdatedEvent_7;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::NavigationCanceledEvent
	NavigationCanceledEventDelegate_t1045620518 * ___NavigationCanceledEvent_8;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::NavigationCompletedEvent
	NavigationCompletedEventDelegate_t1982950364 * ___NavigationCompletedEvent_9;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::NavigationStartedEvent
	NavigationStartedEventDelegate_t3123342528 * ___NavigationStartedEvent_10;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::NavigationUpdatedEvent
	NavigationUpdatedEventDelegate_t3960847450 * ___NavigationUpdatedEvent_11;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::RecognitionEndedEvent
	RecognitionEndedEventDelegate_t831649826 * ___RecognitionEndedEvent_12;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::RecognitionStartedEvent
	RecognitionStartedEventDelegate_t3116179703 * ___RecognitionStartedEvent_13;
	// UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate UnityEngine.VR.WSA.Input.GestureRecognizer::GestureErrorEvent
	GestureErrorDelegate_t2343902086 * ___GestureErrorEvent_14;
	// System.IntPtr UnityEngine.VR.WSA.Input.GestureRecognizer::m_Recognizer
	IntPtr_t ___m_Recognizer_15;

public:
	inline static int32_t get_offset_of_HoldCanceledEvent_0() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___HoldCanceledEvent_0)); }
	inline HoldCanceledEventDelegate_t1855824201 * get_HoldCanceledEvent_0() const { return ___HoldCanceledEvent_0; }
	inline HoldCanceledEventDelegate_t1855824201 ** get_address_of_HoldCanceledEvent_0() { return &___HoldCanceledEvent_0; }
	inline void set_HoldCanceledEvent_0(HoldCanceledEventDelegate_t1855824201 * value)
	{
		___HoldCanceledEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___HoldCanceledEvent_0, value);
	}

	inline static int32_t get_offset_of_HoldCompletedEvent_1() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___HoldCompletedEvent_1)); }
	inline HoldCompletedEventDelegate_t3501137495 * get_HoldCompletedEvent_1() const { return ___HoldCompletedEvent_1; }
	inline HoldCompletedEventDelegate_t3501137495 ** get_address_of_HoldCompletedEvent_1() { return &___HoldCompletedEvent_1; }
	inline void set_HoldCompletedEvent_1(HoldCompletedEventDelegate_t3501137495 * value)
	{
		___HoldCompletedEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___HoldCompletedEvent_1, value);
	}

	inline static int32_t get_offset_of_HoldStartedEvent_2() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___HoldStartedEvent_2)); }
	inline HoldStartedEventDelegate_t3415544547 * get_HoldStartedEvent_2() const { return ___HoldStartedEvent_2; }
	inline HoldStartedEventDelegate_t3415544547 ** get_address_of_HoldStartedEvent_2() { return &___HoldStartedEvent_2; }
	inline void set_HoldStartedEvent_2(HoldStartedEventDelegate_t3415544547 * value)
	{
		___HoldStartedEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___HoldStartedEvent_2, value);
	}

	inline static int32_t get_offset_of_TappedEvent_3() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___TappedEvent_3)); }
	inline TappedEventDelegate_t2437856093 * get_TappedEvent_3() const { return ___TappedEvent_3; }
	inline TappedEventDelegate_t2437856093 ** get_address_of_TappedEvent_3() { return &___TappedEvent_3; }
	inline void set_TappedEvent_3(TappedEventDelegate_t2437856093 * value)
	{
		___TappedEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___TappedEvent_3, value);
	}

	inline static int32_t get_offset_of_ManipulationCanceledEvent_4() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___ManipulationCanceledEvent_4)); }
	inline ManipulationCanceledEventDelegate_t2553296249 * get_ManipulationCanceledEvent_4() const { return ___ManipulationCanceledEvent_4; }
	inline ManipulationCanceledEventDelegate_t2553296249 ** get_address_of_ManipulationCanceledEvent_4() { return &___ManipulationCanceledEvent_4; }
	inline void set_ManipulationCanceledEvent_4(ManipulationCanceledEventDelegate_t2553296249 * value)
	{
		___ManipulationCanceledEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___ManipulationCanceledEvent_4, value);
	}

	inline static int32_t get_offset_of_ManipulationCompletedEvent_5() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___ManipulationCompletedEvent_5)); }
	inline ManipulationCompletedEventDelegate_t2759974023 * get_ManipulationCompletedEvent_5() const { return ___ManipulationCompletedEvent_5; }
	inline ManipulationCompletedEventDelegate_t2759974023 ** get_address_of_ManipulationCompletedEvent_5() { return &___ManipulationCompletedEvent_5; }
	inline void set_ManipulationCompletedEvent_5(ManipulationCompletedEventDelegate_t2759974023 * value)
	{
		___ManipulationCompletedEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___ManipulationCompletedEvent_5, value);
	}

	inline static int32_t get_offset_of_ManipulationStartedEvent_6() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___ManipulationStartedEvent_6)); }
	inline ManipulationStartedEventDelegate_t2487153075 * get_ManipulationStartedEvent_6() const { return ___ManipulationStartedEvent_6; }
	inline ManipulationStartedEventDelegate_t2487153075 ** get_address_of_ManipulationStartedEvent_6() { return &___ManipulationStartedEvent_6; }
	inline void set_ManipulationStartedEvent_6(ManipulationStartedEventDelegate_t2487153075 * value)
	{
		___ManipulationStartedEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___ManipulationStartedEvent_6, value);
	}

	inline static int32_t get_offset_of_ManipulationUpdatedEvent_7() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___ManipulationUpdatedEvent_7)); }
	inline ManipulationUpdatedEventDelegate_t2394416487 * get_ManipulationUpdatedEvent_7() const { return ___ManipulationUpdatedEvent_7; }
	inline ManipulationUpdatedEventDelegate_t2394416487 ** get_address_of_ManipulationUpdatedEvent_7() { return &___ManipulationUpdatedEvent_7; }
	inline void set_ManipulationUpdatedEvent_7(ManipulationUpdatedEventDelegate_t2394416487 * value)
	{
		___ManipulationUpdatedEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___ManipulationUpdatedEvent_7, value);
	}

	inline static int32_t get_offset_of_NavigationCanceledEvent_8() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___NavigationCanceledEvent_8)); }
	inline NavigationCanceledEventDelegate_t1045620518 * get_NavigationCanceledEvent_8() const { return ___NavigationCanceledEvent_8; }
	inline NavigationCanceledEventDelegate_t1045620518 ** get_address_of_NavigationCanceledEvent_8() { return &___NavigationCanceledEvent_8; }
	inline void set_NavigationCanceledEvent_8(NavigationCanceledEventDelegate_t1045620518 * value)
	{
		___NavigationCanceledEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___NavigationCanceledEvent_8, value);
	}

	inline static int32_t get_offset_of_NavigationCompletedEvent_9() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___NavigationCompletedEvent_9)); }
	inline NavigationCompletedEventDelegate_t1982950364 * get_NavigationCompletedEvent_9() const { return ___NavigationCompletedEvent_9; }
	inline NavigationCompletedEventDelegate_t1982950364 ** get_address_of_NavigationCompletedEvent_9() { return &___NavigationCompletedEvent_9; }
	inline void set_NavigationCompletedEvent_9(NavigationCompletedEventDelegate_t1982950364 * value)
	{
		___NavigationCompletedEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___NavigationCompletedEvent_9, value);
	}

	inline static int32_t get_offset_of_NavigationStartedEvent_10() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___NavigationStartedEvent_10)); }
	inline NavigationStartedEventDelegate_t3123342528 * get_NavigationStartedEvent_10() const { return ___NavigationStartedEvent_10; }
	inline NavigationStartedEventDelegate_t3123342528 ** get_address_of_NavigationStartedEvent_10() { return &___NavigationStartedEvent_10; }
	inline void set_NavigationStartedEvent_10(NavigationStartedEventDelegate_t3123342528 * value)
	{
		___NavigationStartedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___NavigationStartedEvent_10, value);
	}

	inline static int32_t get_offset_of_NavigationUpdatedEvent_11() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___NavigationUpdatedEvent_11)); }
	inline NavigationUpdatedEventDelegate_t3960847450 * get_NavigationUpdatedEvent_11() const { return ___NavigationUpdatedEvent_11; }
	inline NavigationUpdatedEventDelegate_t3960847450 ** get_address_of_NavigationUpdatedEvent_11() { return &___NavigationUpdatedEvent_11; }
	inline void set_NavigationUpdatedEvent_11(NavigationUpdatedEventDelegate_t3960847450 * value)
	{
		___NavigationUpdatedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___NavigationUpdatedEvent_11, value);
	}

	inline static int32_t get_offset_of_RecognitionEndedEvent_12() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___RecognitionEndedEvent_12)); }
	inline RecognitionEndedEventDelegate_t831649826 * get_RecognitionEndedEvent_12() const { return ___RecognitionEndedEvent_12; }
	inline RecognitionEndedEventDelegate_t831649826 ** get_address_of_RecognitionEndedEvent_12() { return &___RecognitionEndedEvent_12; }
	inline void set_RecognitionEndedEvent_12(RecognitionEndedEventDelegate_t831649826 * value)
	{
		___RecognitionEndedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___RecognitionEndedEvent_12, value);
	}

	inline static int32_t get_offset_of_RecognitionStartedEvent_13() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___RecognitionStartedEvent_13)); }
	inline RecognitionStartedEventDelegate_t3116179703 * get_RecognitionStartedEvent_13() const { return ___RecognitionStartedEvent_13; }
	inline RecognitionStartedEventDelegate_t3116179703 ** get_address_of_RecognitionStartedEvent_13() { return &___RecognitionStartedEvent_13; }
	inline void set_RecognitionStartedEvent_13(RecognitionStartedEventDelegate_t3116179703 * value)
	{
		___RecognitionStartedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___RecognitionStartedEvent_13, value);
	}

	inline static int32_t get_offset_of_GestureErrorEvent_14() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___GestureErrorEvent_14)); }
	inline GestureErrorDelegate_t2343902086 * get_GestureErrorEvent_14() const { return ___GestureErrorEvent_14; }
	inline GestureErrorDelegate_t2343902086 ** get_address_of_GestureErrorEvent_14() { return &___GestureErrorEvent_14; }
	inline void set_GestureErrorEvent_14(GestureErrorDelegate_t2343902086 * value)
	{
		___GestureErrorEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___GestureErrorEvent_14, value);
	}

	inline static int32_t get_offset_of_m_Recognizer_15() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3303813975, ___m_Recognizer_15)); }
	inline IntPtr_t get_m_Recognizer_15() const { return ___m_Recognizer_15; }
	inline IntPtr_t* get_address_of_m_Recognizer_15() { return &___m_Recognizer_15; }
	inline void set_m_Recognizer_15(IntPtr_t value)
	{
		___m_Recognizer_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
