﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppContextSwitches
struct  AppContextSwitches_t4151153076  : public Il2CppObject
{
public:

public:
};

struct AppContextSwitches_t4151153076_StaticFields
{
public:
	// System.Boolean System.AppContextSwitches::ThrowExceptionIfDisposedCancellationTokenSource
	bool ___ThrowExceptionIfDisposedCancellationTokenSource_0;
	// System.Boolean System.AppContextSwitches::NoAsyncCurrentCulture
	bool ___NoAsyncCurrentCulture_1;

public:
	inline static int32_t get_offset_of_ThrowExceptionIfDisposedCancellationTokenSource_0() { return static_cast<int32_t>(offsetof(AppContextSwitches_t4151153076_StaticFields, ___ThrowExceptionIfDisposedCancellationTokenSource_0)); }
	inline bool get_ThrowExceptionIfDisposedCancellationTokenSource_0() const { return ___ThrowExceptionIfDisposedCancellationTokenSource_0; }
	inline bool* get_address_of_ThrowExceptionIfDisposedCancellationTokenSource_0() { return &___ThrowExceptionIfDisposedCancellationTokenSource_0; }
	inline void set_ThrowExceptionIfDisposedCancellationTokenSource_0(bool value)
	{
		___ThrowExceptionIfDisposedCancellationTokenSource_0 = value;
	}

	inline static int32_t get_offset_of_NoAsyncCurrentCulture_1() { return static_cast<int32_t>(offsetof(AppContextSwitches_t4151153076_StaticFields, ___NoAsyncCurrentCulture_1)); }
	inline bool get_NoAsyncCurrentCulture_1() const { return ___NoAsyncCurrentCulture_1; }
	inline bool* get_address_of_NoAsyncCurrentCulture_1() { return &___NoAsyncCurrentCulture_1; }
	inline void set_NoAsyncCurrentCulture_1(bool value)
	{
		___NoAsyncCurrentCulture_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
