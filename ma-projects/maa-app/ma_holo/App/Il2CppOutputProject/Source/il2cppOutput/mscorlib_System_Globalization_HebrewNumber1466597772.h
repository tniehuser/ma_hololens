﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Globalization.HebrewNumber/HebrewValue[]
struct HebrewValueU5BU5D_t804438456;
// System.Globalization.HebrewNumber/HS[][]
struct HSU5BU5DU5BU5D_t274427794;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.HebrewNumber
struct  HebrewNumber_t1466597772  : public Il2CppObject
{
public:

public:
};

struct HebrewNumber_t1466597772_StaticFields
{
public:
	// System.Globalization.HebrewNumber/HebrewValue[] System.Globalization.HebrewNumber::HebrewValues
	HebrewValueU5BU5D_t804438456* ___HebrewValues_0;
	// System.Char System.Globalization.HebrewNumber::maxHebrewNumberCh
	Il2CppChar ___maxHebrewNumberCh_1;
	// System.Globalization.HebrewNumber/HS[][] System.Globalization.HebrewNumber::NumberPasingState
	HSU5BU5DU5BU5D_t274427794* ___NumberPasingState_2;

public:
	inline static int32_t get_offset_of_HebrewValues_0() { return static_cast<int32_t>(offsetof(HebrewNumber_t1466597772_StaticFields, ___HebrewValues_0)); }
	inline HebrewValueU5BU5D_t804438456* get_HebrewValues_0() const { return ___HebrewValues_0; }
	inline HebrewValueU5BU5D_t804438456** get_address_of_HebrewValues_0() { return &___HebrewValues_0; }
	inline void set_HebrewValues_0(HebrewValueU5BU5D_t804438456* value)
	{
		___HebrewValues_0 = value;
		Il2CppCodeGenWriteBarrier(&___HebrewValues_0, value);
	}

	inline static int32_t get_offset_of_maxHebrewNumberCh_1() { return static_cast<int32_t>(offsetof(HebrewNumber_t1466597772_StaticFields, ___maxHebrewNumberCh_1)); }
	inline Il2CppChar get_maxHebrewNumberCh_1() const { return ___maxHebrewNumberCh_1; }
	inline Il2CppChar* get_address_of_maxHebrewNumberCh_1() { return &___maxHebrewNumberCh_1; }
	inline void set_maxHebrewNumberCh_1(Il2CppChar value)
	{
		___maxHebrewNumberCh_1 = value;
	}

	inline static int32_t get_offset_of_NumberPasingState_2() { return static_cast<int32_t>(offsetof(HebrewNumber_t1466597772_StaticFields, ___NumberPasingState_2)); }
	inline HSU5BU5DU5BU5D_t274427794* get_NumberPasingState_2() const { return ___NumberPasingState_2; }
	inline HSU5BU5DU5BU5D_t274427794** get_address_of_NumberPasingState_2() { return &___NumberPasingState_2; }
	inline void set_NumberPasingState_2(HSU5BU5DU5BU5D_t274427794* value)
	{
		___NumberPasingState_2 = value;
		Il2CppCodeGenWriteBarrier(&___NumberPasingState_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
