﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3719433431.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.NameInfo
struct  NameInfo_t2407316929  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.NameInfo::NIFullName
	String_t* ___NIFullName_0;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.NameInfo::NIobjectId
	int64_t ___NIobjectId_1;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.NameInfo::NIassemId
	int64_t ___NIassemId_2;
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.NameInfo::NIprimitiveTypeEnum
	int32_t ___NIprimitiveTypeEnum_3;
	// System.Type System.Runtime.Serialization.Formatters.Binary.NameInfo::NItype
	Type_t * ___NItype_4;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NIisSealed
	bool ___NIisSealed_5;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NIisArray
	bool ___NIisArray_6;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NIisArrayItem
	bool ___NIisArrayItem_7;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NItransmitTypeOnObject
	bool ___NItransmitTypeOnObject_8;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NItransmitTypeOnMember
	bool ___NItransmitTypeOnMember_9;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NIisParentTypeOnObject
	bool ___NIisParentTypeOnObject_10;
	// System.Runtime.Serialization.Formatters.Binary.InternalArrayTypeE System.Runtime.Serialization.Formatters.Binary.NameInfo::NIarrayEnum
	int32_t ___NIarrayEnum_11;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.NameInfo::NIsealedStatusChecked
	bool ___NIsealedStatusChecked_12;

public:
	inline static int32_t get_offset_of_NIFullName_0() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIFullName_0)); }
	inline String_t* get_NIFullName_0() const { return ___NIFullName_0; }
	inline String_t** get_address_of_NIFullName_0() { return &___NIFullName_0; }
	inline void set_NIFullName_0(String_t* value)
	{
		___NIFullName_0 = value;
		Il2CppCodeGenWriteBarrier(&___NIFullName_0, value);
	}

	inline static int32_t get_offset_of_NIobjectId_1() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIobjectId_1)); }
	inline int64_t get_NIobjectId_1() const { return ___NIobjectId_1; }
	inline int64_t* get_address_of_NIobjectId_1() { return &___NIobjectId_1; }
	inline void set_NIobjectId_1(int64_t value)
	{
		___NIobjectId_1 = value;
	}

	inline static int32_t get_offset_of_NIassemId_2() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIassemId_2)); }
	inline int64_t get_NIassemId_2() const { return ___NIassemId_2; }
	inline int64_t* get_address_of_NIassemId_2() { return &___NIassemId_2; }
	inline void set_NIassemId_2(int64_t value)
	{
		___NIassemId_2 = value;
	}

	inline static int32_t get_offset_of_NIprimitiveTypeEnum_3() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIprimitiveTypeEnum_3)); }
	inline int32_t get_NIprimitiveTypeEnum_3() const { return ___NIprimitiveTypeEnum_3; }
	inline int32_t* get_address_of_NIprimitiveTypeEnum_3() { return &___NIprimitiveTypeEnum_3; }
	inline void set_NIprimitiveTypeEnum_3(int32_t value)
	{
		___NIprimitiveTypeEnum_3 = value;
	}

	inline static int32_t get_offset_of_NItype_4() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NItype_4)); }
	inline Type_t * get_NItype_4() const { return ___NItype_4; }
	inline Type_t ** get_address_of_NItype_4() { return &___NItype_4; }
	inline void set_NItype_4(Type_t * value)
	{
		___NItype_4 = value;
		Il2CppCodeGenWriteBarrier(&___NItype_4, value);
	}

	inline static int32_t get_offset_of_NIisSealed_5() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIisSealed_5)); }
	inline bool get_NIisSealed_5() const { return ___NIisSealed_5; }
	inline bool* get_address_of_NIisSealed_5() { return &___NIisSealed_5; }
	inline void set_NIisSealed_5(bool value)
	{
		___NIisSealed_5 = value;
	}

	inline static int32_t get_offset_of_NIisArray_6() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIisArray_6)); }
	inline bool get_NIisArray_6() const { return ___NIisArray_6; }
	inline bool* get_address_of_NIisArray_6() { return &___NIisArray_6; }
	inline void set_NIisArray_6(bool value)
	{
		___NIisArray_6 = value;
	}

	inline static int32_t get_offset_of_NIisArrayItem_7() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIisArrayItem_7)); }
	inline bool get_NIisArrayItem_7() const { return ___NIisArrayItem_7; }
	inline bool* get_address_of_NIisArrayItem_7() { return &___NIisArrayItem_7; }
	inline void set_NIisArrayItem_7(bool value)
	{
		___NIisArrayItem_7 = value;
	}

	inline static int32_t get_offset_of_NItransmitTypeOnObject_8() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NItransmitTypeOnObject_8)); }
	inline bool get_NItransmitTypeOnObject_8() const { return ___NItransmitTypeOnObject_8; }
	inline bool* get_address_of_NItransmitTypeOnObject_8() { return &___NItransmitTypeOnObject_8; }
	inline void set_NItransmitTypeOnObject_8(bool value)
	{
		___NItransmitTypeOnObject_8 = value;
	}

	inline static int32_t get_offset_of_NItransmitTypeOnMember_9() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NItransmitTypeOnMember_9)); }
	inline bool get_NItransmitTypeOnMember_9() const { return ___NItransmitTypeOnMember_9; }
	inline bool* get_address_of_NItransmitTypeOnMember_9() { return &___NItransmitTypeOnMember_9; }
	inline void set_NItransmitTypeOnMember_9(bool value)
	{
		___NItransmitTypeOnMember_9 = value;
	}

	inline static int32_t get_offset_of_NIisParentTypeOnObject_10() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIisParentTypeOnObject_10)); }
	inline bool get_NIisParentTypeOnObject_10() const { return ___NIisParentTypeOnObject_10; }
	inline bool* get_address_of_NIisParentTypeOnObject_10() { return &___NIisParentTypeOnObject_10; }
	inline void set_NIisParentTypeOnObject_10(bool value)
	{
		___NIisParentTypeOnObject_10 = value;
	}

	inline static int32_t get_offset_of_NIarrayEnum_11() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIarrayEnum_11)); }
	inline int32_t get_NIarrayEnum_11() const { return ___NIarrayEnum_11; }
	inline int32_t* get_address_of_NIarrayEnum_11() { return &___NIarrayEnum_11; }
	inline void set_NIarrayEnum_11(int32_t value)
	{
		___NIarrayEnum_11 = value;
	}

	inline static int32_t get_offset_of_NIsealedStatusChecked_12() { return static_cast<int32_t>(offsetof(NameInfo_t2407316929, ___NIsealedStatusChecked_12)); }
	inline bool get_NIsealedStatusChecked_12() const { return ___NIsealedStatusChecked_12; }
	inline bool* get_address_of_NIsealedStatusChecked_12() { return &___NIsealedStatusChecked_12; }
	inline void set_NIsealedStatusChecked_12(bool value)
	{
		___NIsealedStatusChecked_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
