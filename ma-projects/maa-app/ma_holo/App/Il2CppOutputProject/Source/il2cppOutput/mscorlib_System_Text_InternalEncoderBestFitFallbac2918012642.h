﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_EncoderFallbackBuffer3883615514.h"

// System.Text.InternalEncoderBestFitFallback
struct InternalEncoderBestFitFallback_t2954925084;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.InternalEncoderBestFitFallbackBuffer
struct  InternalEncoderBestFitFallbackBuffer_t2918012642  : public EncoderFallbackBuffer_t3883615514
{
public:
	// System.Char System.Text.InternalEncoderBestFitFallbackBuffer::cBestFit
	Il2CppChar ___cBestFit_7;
	// System.Text.InternalEncoderBestFitFallback System.Text.InternalEncoderBestFitFallbackBuffer::oFallback
	InternalEncoderBestFitFallback_t2954925084 * ___oFallback_8;
	// System.Int32 System.Text.InternalEncoderBestFitFallbackBuffer::iCount
	int32_t ___iCount_9;
	// System.Int32 System.Text.InternalEncoderBestFitFallbackBuffer::iSize
	int32_t ___iSize_10;

public:
	inline static int32_t get_offset_of_cBestFit_7() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallbackBuffer_t2918012642, ___cBestFit_7)); }
	inline Il2CppChar get_cBestFit_7() const { return ___cBestFit_7; }
	inline Il2CppChar* get_address_of_cBestFit_7() { return &___cBestFit_7; }
	inline void set_cBestFit_7(Il2CppChar value)
	{
		___cBestFit_7 = value;
	}

	inline static int32_t get_offset_of_oFallback_8() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallbackBuffer_t2918012642, ___oFallback_8)); }
	inline InternalEncoderBestFitFallback_t2954925084 * get_oFallback_8() const { return ___oFallback_8; }
	inline InternalEncoderBestFitFallback_t2954925084 ** get_address_of_oFallback_8() { return &___oFallback_8; }
	inline void set_oFallback_8(InternalEncoderBestFitFallback_t2954925084 * value)
	{
		___oFallback_8 = value;
		Il2CppCodeGenWriteBarrier(&___oFallback_8, value);
	}

	inline static int32_t get_offset_of_iCount_9() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallbackBuffer_t2918012642, ___iCount_9)); }
	inline int32_t get_iCount_9() const { return ___iCount_9; }
	inline int32_t* get_address_of_iCount_9() { return &___iCount_9; }
	inline void set_iCount_9(int32_t value)
	{
		___iCount_9 = value;
	}

	inline static int32_t get_offset_of_iSize_10() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallbackBuffer_t2918012642, ___iSize_10)); }
	inline int32_t get_iSize_10() const { return ___iSize_10; }
	inline int32_t* get_address_of_iSize_10() { return &___iSize_10; }
	inline void set_iSize_10(int32_t value)
	{
		___iSize_10 = value;
	}
};

struct InternalEncoderBestFitFallbackBuffer_t2918012642_StaticFields
{
public:
	// System.Object System.Text.InternalEncoderBestFitFallbackBuffer::s_InternalSyncObject
	Il2CppObject * ___s_InternalSyncObject_11;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_11() { return static_cast<int32_t>(offsetof(InternalEncoderBestFitFallbackBuffer_t2918012642_StaticFields, ___s_InternalSyncObject_11)); }
	inline Il2CppObject * get_s_InternalSyncObject_11() const { return ___s_InternalSyncObject_11; }
	inline Il2CppObject ** get_address_of_s_InternalSyncObject_11() { return &___s_InternalSyncObject_11; }
	inline void set_s_InternalSyncObject_11(Il2CppObject * value)
	{
		___s_InternalSyncObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___s_InternalSyncObject_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
