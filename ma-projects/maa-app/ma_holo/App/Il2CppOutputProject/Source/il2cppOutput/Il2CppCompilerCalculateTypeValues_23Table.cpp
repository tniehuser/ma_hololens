﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1568637719.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3066379751.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3894236545.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1568637717.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1459944470.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array2306864765.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT762068658.h"
#include "System_Configuration_U3CModuleU3E3783534214.h"
#include "System_Configuration_System_Configuration_Provider2882126354.h"
#include "System_Configuration_System_Configuration_Provider2548499159.h"
#include "System_Configuration_System_Configuration_ClientCo4294641134.h"
#include "System_Configuration_System_Configuration_ConfigNa2395569530.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"
#include "System_Configuration_System_Configuration_Configur3670001516.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "System_Configuration_System_Configuration_Configur2811353736.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "System_Configuration_System_Configuration_Configur3996373180.h"
#include "System_Configuration_System_Configuration_ElementMa997038224.h"
#include "System_Configuration_System_Configuration_Configur1911180302.h"
#include "System_Configuration_System_Configuration_Configur3305291330.h"
#include "System_Configuration_System_Configuration_Configur1806001494.h"
#include "System_Configuration_System_Configuration_Configur1362721126.h"
#include "System_Configuration_System_Configuration_Configur2625210096.h"
#include "System_Configuration_System_Configuration_Configur1895107553.h"
#include "System_Configuration_System_Configuration_Configur1903842989.h"
#include "System_Configuration_System_Configuration_Configura131834733.h"
#include "System_Configuration_System_Configuration_Configur1011762925.h"
#include "System_Configuration_System_Configuration_Configur2608608455.h"
#include "System_Configuration_System_Configuration_Configur2048066811.h"
#include "System_Configuration_System_Configuration_Configur3655647199.h"
#include "System_Configuration_System_Configuration_Configur3473514151.h"
#include "System_Configuration_System_Configuration_Configur3219689025.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"
#include "System_Configuration_System_Configuration_Configur2600766927.h"
#include "System_Configuration_System_Configuration_Configur4261113299.h"
#include "System_Configuration_System_Configuration_Configur1795270620.h"
#include "System_Configuration_System_Configuration_Configur2230982736.h"
#include "System_Configuration_System_Configuration_Configura575145286.h"
#include "System_Configuration_System_Configuration_Configur1204907851.h"
#include "System_Configuration_System_Configuration_Configur1007519140.h"
#include "System_Configuration_System_Configuration_Configura210547623.h"
#include "System_Configuration_ConfigXmlTextReader3212066157.h"
#include "System_Configuration_System_Configuration_DefaultS3840532724.h"
#include "System_Configuration_System_Configuration_DefaultVa300527515.h"
#include "System_Configuration_System_Configuration_ElementI3165583784.h"
#include "System_Configuration_System_Configuration_ExeConfi1419586304.h"
#include "System_Configuration_System_Configuration_IgnoreSec681509237.h"
#include "System_Configuration_System_Configuration_Internal3846641927.h"
#include "System_Configuration_System_Configuration_Internal2108740756.h"
#include "System_Configuration_System_Configuration_InternalC547577555.h"
#include "System_Configuration_System_Configuration_ExeConfi2778769322.h"
#include "System_Configuration_System_Configuration_InternalC547578517.h"
#include "System_Configuration_System_Configuration_Property2089433965.h"
#include "System_Configuration_System_Configuration_PropertyI954922393.h"
#include "System_Configuration_System_Configuration_Property1453501302.h"
#include "System_Configuration_System_Configuration_Property1217826846.h"
#include "System_Configuration_System_Configuration_Protecte1807950812.h"
#include "System_Configuration_System_Configuration_Protecte3971982415.h"
#include "System_Configuration_System_Configuration_Protected388338823.h"
#include "System_Configuration_System_Configuration_Protecte3541826375.h"
#include "System_Configuration_System_Configuration_ProviderS873049714.h"
#include "System_Configuration_System_Configuration_ProviderS585304908.h"
#include "System_Configuration_System_Configuration_SectionI1739019515.h"
#include "System_Configuration_System_Configuration_SectionG2346323570.h"
#include "System_Configuration_System_Configuration_ConfigIn3264723080.h"
#include "System_Configuration_System_Configuration_SectionI2754609709.h"
#include "System_Configuration_System_MonoTODOAttribute3487514019.h"
#include "System_Configuration_System_MonoInternalNoteAttrib4192790486.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_SR2523137203.h"
#include "System_Xml_System_Xml_Res3849109792.h"
#include "System_Xml_System_Xml_Base64Encoder1608613663.h"
#include "System_Xml_System_Xml_XmlTextWriterBase64Encoder365185068.h"
#include "System_Xml_System_Xml_BinHexDecoder3180637936.h"
#include "System_Xml_System_Xml_BinHexEncoder3517319200.h"
#include "System_Xml_System_Xml_Bits2854626690.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathDocumentNavi4190236647.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNode3118381855.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeRef2092605142.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeHelper2323461313.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodePageInfo617838178.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeInfoAtom2810782300.h"
#include "System_Xml_System_Xml_BinaryCompatibility3641193253.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_System_Xml_DtdProcessing2734370679.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U24ArrayTypeU3D32_t1568637721)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637721 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U24ArrayTypeU3D256_t3066379752)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D256_t3066379752 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U24ArrayTypeU3D16_t3894236548)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236548 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U24ArrayTypeU3D12_t1568637722)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637722 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (U24ArrayTypeU3D4_t1459944472)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4_t1459944472 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U24ArrayTypeU3D128_t2306864766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D128_t2306864766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (U24ArrayTypeU3D44_t762068659)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D44_t762068659 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (ProviderBase_t2882126354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[3] = 
{
	ProviderBase_t2882126354::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2882126354::get_offset_of__description_1(),
	ProviderBase_t2882126354::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (ProviderCollection_t2548499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[3] = 
{
	ProviderCollection_t2548499159::get_offset_of_lookup_0(),
	ProviderCollection_t2548499159::get_offset_of_readOnly_1(),
	ProviderCollection_t2548499159::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ClientConfigurationSystem_t4294641134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[1] = 
{
	ClientConfigurationSystem_t4294641134::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (ConfigNameValueCollection_t2395569530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	ConfigNameValueCollection_t2395569530::get_offset_of_modified_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (ConfigInfo_t546730838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[6] = 
{
	ConfigInfo_t546730838::get_offset_of_Name_0(),
	ConfigInfo_t546730838::get_offset_of_TypeName_1(),
	ConfigInfo_t546730838::get_offset_of_Type_2(),
	ConfigInfo_t546730838::get_offset_of_streamName_3(),
	ConfigInfo_t546730838::get_offset_of_Parent_4(),
	ConfigInfo_t546730838::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ConfigurationXmlDocument_t3670001516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (Configuration_t3335372970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[12] = 
{
	Configuration_t3335372970::get_offset_of_parent_0(),
	Configuration_t3335372970::get_offset_of_elementData_1(),
	Configuration_t3335372970::get_offset_of_streamName_2(),
	Configuration_t3335372970::get_offset_of_rootSectionGroup_3(),
	Configuration_t3335372970::get_offset_of_locations_4(),
	Configuration_t3335372970::get_offset_of_rootGroup_5(),
	Configuration_t3335372970::get_offset_of_system_6(),
	Configuration_t3335372970::get_offset_of_hasFile_7(),
	Configuration_t3335372970::get_offset_of_rootNamespace_8(),
	Configuration_t3335372970::get_offset_of_configPath_9(),
	Configuration_t3335372970::get_offset_of_locationConfigPath_10(),
	Configuration_t3335372970::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (ConfigurationAllowDefinition_t3250313246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2321[5] = 
{
	ConfigurationAllowDefinition_t3250313246::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (ConfigurationAllowExeDefinition_t3860111898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2322[5] = 
{
	ConfigurationAllowExeDefinition_t3860111898::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ConfigurationCollectionAttribute_t2811353736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[5] = 
{
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ConfigurationElement_t1776195828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[15] = 
{
	ConfigurationElement_t1776195828::get_offset_of_rawXml_0(),
	ConfigurationElement_t1776195828::get_offset_of_modified_1(),
	ConfigurationElement_t1776195828::get_offset_of_map_2(),
	ConfigurationElement_t1776195828::get_offset_of_keyProps_3(),
	ConfigurationElement_t1776195828::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t1776195828::get_offset_of_readOnly_5(),
	ConfigurationElement_t1776195828::get_offset_of_elementInfo_6(),
	ConfigurationElement_t1776195828::get_offset_of__configuration_7(),
	ConfigurationElement_t1776195828::get_offset_of_elementPresent_8(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllAttributesExcept_9(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllElementsExcept_10(),
	ConfigurationElement_t1776195828::get_offset_of_lockAttributes_11(),
	ConfigurationElement_t1776195828::get_offset_of_lockElements_12(),
	ConfigurationElement_t1776195828::get_offset_of_lockItem_13(),
	ConfigurationElement_t1776195828::get_offset_of_saveContext_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (SaveContext_t3996373180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[3] = 
{
	SaveContext_t3996373180::get_offset_of_Element_0(),
	SaveContext_t3996373180::get_offset_of_Parent_1(),
	SaveContext_t3996373180::get_offset_of_Mode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (ElementMap_t997038224), -1, sizeof(ElementMap_t997038224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2326[3] = 
{
	ElementMap_t997038224_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t997038224::get_offset_of_properties_1(),
	ElementMap_t997038224::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (ConfigurationElementCollection_t1911180302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[10] = 
{
	ConfigurationElementCollection_t1911180302::get_offset_of_list_15(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removed_16(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inherited_17(),
	ConfigurationElementCollection_t1911180302::get_offset_of_emitClear_18(),
	ConfigurationElementCollection_t1911180302::get_offset_of_modified_19(),
	ConfigurationElementCollection_t1911180302::get_offset_of_comparer_20(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inheritedLimitIndex_21(),
	ConfigurationElementCollection_t1911180302::get_offset_of_addElementName_22(),
	ConfigurationElementCollection_t1911180302::get_offset_of_clearElementName_23(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removeElementName_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (ConfigurationRemoveElement_t3305291330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[3] = 
{
	ConfigurationRemoveElement_t3305291330::get_offset_of_properties_15(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origElement_16(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origCollection_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (ConfigurationElementCollectionType_t1806001494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2329[5] = 
{
	ConfigurationElementCollectionType_t1806001494::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (ConfigurationErrorsException_t1362721126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[2] = 
{
	ConfigurationErrorsException_t1362721126::get_offset_of_filename_18(),
	ConfigurationErrorsException_t1362721126::get_offset_of_line_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (ConfigurationFileMap_t2625210096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[1] = 
{
	ConfigurationFileMap_t2625210096::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (ConfigurationLocation_t1895107553), -1, sizeof(ConfigurationLocation_t1895107553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[7] = 
{
	ConfigurationLocation_t1895107553_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1895107553::get_offset_of_path_1(),
	ConfigurationLocation_t1895107553::get_offset_of_configuration_2(),
	ConfigurationLocation_t1895107553::get_offset_of_parent_3(),
	ConfigurationLocation_t1895107553::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1895107553::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1895107553::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (ConfigurationLocationCollection_t1903842989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (ConfigurationLockType_t131834733)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[4] = 
{
	ConfigurationLockType_t131834733::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (ConfigurationLockCollection_t1011762925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[6] = 
{
	ConfigurationLockCollection_t1011762925::get_offset_of_names_0(),
	ConfigurationLockCollection_t1011762925::get_offset_of_element_1(),
	ConfigurationLockCollection_t1011762925::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t1011762925::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (ConfigurationManager_t2608608455), -1, sizeof(ConfigurationManager_t2608608455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (ConfigurationProperty_t2048066811), -1, sizeof(ConfigurationProperty_t2048066811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2337[9] = 
{
	ConfigurationProperty_t2048066811_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t2048066811::get_offset_of_name_1(),
	ConfigurationProperty_t2048066811::get_offset_of_type_2(),
	ConfigurationProperty_t2048066811::get_offset_of_default_value_3(),
	ConfigurationProperty_t2048066811::get_offset_of_converter_4(),
	ConfigurationProperty_t2048066811::get_offset_of_validation_5(),
	ConfigurationProperty_t2048066811::get_offset_of_flags_6(),
	ConfigurationProperty_t2048066811::get_offset_of_description_7(),
	ConfigurationProperty_t2048066811::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (ConfigurationPropertyAttribute_t3655647199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[3] = 
{
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (ConfigurationPropertyCollection_t3473514151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[1] = 
{
	ConfigurationPropertyCollection_t3473514151::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (ConfigurationPropertyOptions_t3219689025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2340[8] = 
{
	ConfigurationPropertyOptions_t3219689025::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (ConfigurationSaveMode_t700320212)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2341[4] = 
{
	ConfigurationSaveMode_t700320212::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (ConfigurationSection_t2600766927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[4] = 
{
	ConfigurationSection_t2600766927::get_offset_of_sectionInformation_15(),
	ConfigurationSection_t2600766927::get_offset_of_section_handler_16(),
	ConfigurationSection_t2600766927::get_offset_of_externalDataXml_17(),
	ConfigurationSection_t2600766927::get_offset_of__configContext_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (ConfigurationSectionCollection_t4261113299), -1, sizeof(ConfigurationSectionCollection_t4261113299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2343[3] = 
{
	ConfigurationSectionCollection_t4261113299::get_offset_of_group_11(),
	ConfigurationSectionCollection_t4261113299::get_offset_of_config_12(),
	ConfigurationSectionCollection_t4261113299_StaticFields::get_offset_of_lockObject_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t1795270620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24locvar1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (ConfigurationSectionGroup_t2230982736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[5] = 
{
	ConfigurationSectionGroup_t2230982736::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_config_2(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_group_3(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (ConfigurationSectionGroupCollection_t575145286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_group_11(),
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_config_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (ConfigurationUserLevel_t1204907851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	ConfigurationUserLevel_t1204907851::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (ConfigurationValidatorAttribute_t1007519140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[2] = 
{
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (ConfigurationValidatorBase_t210547623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (ConfigXmlTextReader_t3212066157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[1] = 
{
	ConfigXmlTextReader_t3212066157::get_offset_of_fileName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (DefaultSection_t3840532724), -1, sizeof(DefaultSection_t3840532724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2351[1] = 
{
	DefaultSection_t3840532724_StaticFields::get_offset_of_properties_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (DefaultValidator_t300527515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ElementInformation_t3165583784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[3] = 
{
	ElementInformation_t3165583784::get_offset_of_propertyInfo_0(),
	ElementInformation_t3165583784::get_offset_of_owner_1(),
	ElementInformation_t3165583784::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (ExeConfigurationFileMap_t1419586304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	ExeConfigurationFileMap_t1419586304::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (IgnoreSection_t681509237), -1, sizeof(IgnoreSection_t681509237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	IgnoreSection_t681509237::get_offset_of_xml_19(),
	IgnoreSection_t681509237_StaticFields::get_offset_of_properties_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (InternalConfigurationFactory_t3846641927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (InternalConfigurationSystem_t2108740756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[3] = 
{
	InternalConfigurationSystem_t2108740756::get_offset_of_host_0(),
	InternalConfigurationSystem_t2108740756::get_offset_of_root_1(),
	InternalConfigurationSystem_t2108740756::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (InternalConfigurationHost_t547577555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ExeConfigurationHost_t2778769322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	ExeConfigurationHost_t2778769322::get_offset_of_map_0(),
	ExeConfigurationHost_t2778769322::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (InternalConfigurationRoot_t547578517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[2] = 
{
	InternalConfigurationRoot_t547578517::get_offset_of_host_0(),
	InternalConfigurationRoot_t547578517::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (PropertyInformation_t2089433965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[7] = 
{
	PropertyInformation_t2089433965::get_offset_of_isModified_0(),
	PropertyInformation_t2089433965::get_offset_of_lineNumber_1(),
	PropertyInformation_t2089433965::get_offset_of_source_2(),
	PropertyInformation_t2089433965::get_offset_of_val_3(),
	PropertyInformation_t2089433965::get_offset_of_origin_4(),
	PropertyInformation_t2089433965::get_offset_of_owner_5(),
	PropertyInformation_t2089433965::get_offset_of_property_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (PropertyInformationCollection_t954922393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (PropertyInformationEnumerator_t1453501302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[2] = 
{
	PropertyInformationEnumerator_t1453501302::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t1453501302::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (PropertyValueOrigin_t1217826846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2364[4] = 
{
	PropertyValueOrigin_t1217826846::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ProtectedConfiguration_t1807950812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ProtectedConfigurationProvider_t3971982415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ProtectedConfigurationProviderCollection_t388338823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ProtectedConfigurationSection_t3541826375), -1, sizeof(ProtectedConfigurationSection_t3541826375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2368[4] = 
{
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_defaultProviderProp_19(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_providersProp_20(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_properties_21(),
	ProtectedConfigurationSection_t3541826375::get_offset_of_providers_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ProviderSettings_t873049714), -1, sizeof(ProviderSettings_t873049714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	ProviderSettings_t873049714::get_offset_of_parameters_15(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_nameProp_16(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_typeProp_17(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ProviderSettingsCollection_t585304908), -1, sizeof(ProviderSettingsCollection_t585304908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2370[1] = 
{
	ProviderSettingsCollection_t585304908_StaticFields::get_offset_of_props_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (SectionInfo_t1739019515), -1, sizeof(SectionInfo_t1739019515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2371[6] = 
{
	SectionInfo_t1739019515::get_offset_of_allowLocation_6(),
	SectionInfo_t1739019515::get_offset_of_requirePermission_7(),
	SectionInfo_t1739019515::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t1739019515::get_offset_of_allowDefinition_9(),
	SectionInfo_t1739019515::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t1739019515_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (SectionGroupInfo_t2346323570), -1, sizeof(SectionGroupInfo_t2346323570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[4] = 
{
	SectionGroupInfo_t2346323570::get_offset_of_modified_6(),
	SectionGroupInfo_t2346323570::get_offset_of_sections_7(),
	SectionGroupInfo_t2346323570::get_offset_of_groups_8(),
	SectionGroupInfo_t2346323570_StaticFields::get_offset_of_emptyList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ConfigInfoCollection_t3264723080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (SectionInformation_t2754609709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[13] = 
{
	SectionInformation_t2754609709::get_offset_of_parent_0(),
	SectionInformation_t2754609709::get_offset_of_allow_definition_1(),
	SectionInformation_t2754609709::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2754609709::get_offset_of_allow_location_3(),
	SectionInformation_t2754609709::get_offset_of_allow_override_4(),
	SectionInformation_t2754609709::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2754609709::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2754609709::get_offset_of_require_permission_7(),
	SectionInformation_t2754609709::get_offset_of_config_source_8(),
	SectionInformation_t2754609709::get_offset_of_name_9(),
	SectionInformation_t2754609709::get_offset_of_raw_xml_10(),
	SectionInformation_t2754609709::get_offset_of_protection_provider_11(),
	SectionInformation_t2754609709::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (MonoTODOAttribute_t3487514020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[1] = 
{
	MonoTODOAttribute_t3487514020::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (MonoInternalNoteAttribute_t4192790486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (SR_t2523137204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (Res_t3849109792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (Base64Encoder_t1608613663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[3] = 
{
	Base64Encoder_t1608613663::get_offset_of_leftOverBytes_0(),
	Base64Encoder_t1608613663::get_offset_of_leftOverBytesCount_1(),
	Base64Encoder_t1608613663::get_offset_of_charsLine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (XmlTextWriterBase64Encoder_t365185068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	XmlTextWriterBase64Encoder_t365185068::get_offset_of_xmlTextEncoder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (BinHexDecoder_t3180637936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	BinHexDecoder_t3180637936::get_offset_of_buffer_0(),
	BinHexDecoder_t3180637936::get_offset_of_curIndex_1(),
	BinHexDecoder_t3180637936::get_offset_of_endIndex_2(),
	BinHexDecoder_t3180637936::get_offset_of_hasHalfByteCached_3(),
	BinHexDecoder_t3180637936::get_offset_of_cachedHalfByte_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (BinHexEncoder_t3517319200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (Bits_t2854626690), -1, sizeof(Bits_t2854626690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2384[5] = 
{
	Bits_t2854626690_StaticFields::get_offset_of_MASK_0101010101010101_0(),
	Bits_t2854626690_StaticFields::get_offset_of_MASK_0011001100110011_1(),
	Bits_t2854626690_StaticFields::get_offset_of_MASK_0000111100001111_2(),
	Bits_t2854626690_StaticFields::get_offset_of_MASK_0000000011111111_3(),
	Bits_t2854626690_StaticFields::get_offset_of_MASK_1111111111111111_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (XPathDocumentNavigator_t4190236647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[4] = 
{
	XPathDocumentNavigator_t4190236647::get_offset_of_pageCurrent_4(),
	XPathDocumentNavigator_t4190236647::get_offset_of_pageParent_5(),
	XPathDocumentNavigator_t4190236647::get_offset_of_idxCurrent_6(),
	XPathDocumentNavigator_t4190236647::get_offset_of_idxParent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (XPathNode_t3118381855)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[7] = 
{
	XPathNode_t3118381855::get_offset_of_info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_idxSibling_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_idxParent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_idxSimilar_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_posOffset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_props_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNode_t3118381855::get_offset_of_value_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (XPathNodeRef_t2092605142)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	XPathNodeRef_t2092605142::get_offset_of_page_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XPathNodeRef_t2092605142::get_offset_of_idx_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (XPathNodeHelper_t2323461313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (XPathNodePageInfo_t617838178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[3] = 
{
	XPathNodePageInfo_t617838178::get_offset_of_pageNum_0(),
	XPathNodePageInfo_t617838178::get_offset_of_nodeCount_1(),
	XPathNodePageInfo_t617838178::get_offset_of_pageNext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (XPathNodeInfoAtom_t2810782300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[13] = 
{
	XPathNodeInfoAtom_t2810782300::get_offset_of_localName_0(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_namespaceUri_1(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_prefix_2(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_baseUri_3(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_pageParent_4(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_pageSibling_5(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_pageSimilar_6(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_doc_7(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_lineNumBase_8(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_linePosBase_9(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_hashCode_10(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_localNameHash_11(),
	XPathNodeInfoAtom_t2810782300::get_offset_of_pageInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (BinaryCompatibility_t3641193253), -1, sizeof(BinaryCompatibility_t3641193253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[1] = 
{
	BinaryCompatibility_t3641193253_StaticFields::get_offset_of__targetsAtLeast_Desktop_V4_5_2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2392[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (DtdProcessing_t2734370679)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2393[4] = 
{
	DtdProcessing_t2734370679::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
