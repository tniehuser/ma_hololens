﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidateNames
struct  ValidateNames_t208250372  : public Il2CppObject
{
public:

public:
};

struct ValidateNames_t208250372_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.ValidateNames::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_0;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(ValidateNames_t208250372_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t1050521405  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t1050521405  value)
	{
		___xmlCharType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
