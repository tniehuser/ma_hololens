﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MemberTypes3343038963.h"

// System.String
struct String_t;
// System.RuntimeType
struct RuntimeType_t2836228502;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfoSerializationHolder
struct  MemberInfoSerializationHolder_t2799051170  : public Il2CppObject
{
public:
	// System.String System.Reflection.MemberInfoSerializationHolder::m_memberName
	String_t* ___m_memberName_0;
	// System.RuntimeType System.Reflection.MemberInfoSerializationHolder::m_reflectedType
	RuntimeType_t2836228502 * ___m_reflectedType_1;
	// System.String System.Reflection.MemberInfoSerializationHolder::m_signature
	String_t* ___m_signature_2;
	// System.String System.Reflection.MemberInfoSerializationHolder::m_signature2
	String_t* ___m_signature2_3;
	// System.Reflection.MemberTypes System.Reflection.MemberInfoSerializationHolder::m_memberType
	int32_t ___m_memberType_4;
	// System.Runtime.Serialization.SerializationInfo System.Reflection.MemberInfoSerializationHolder::m_info
	SerializationInfo_t228987430 * ___m_info_5;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_memberName_0, value);
	}

	inline static int32_t get_offset_of_m_reflectedType_1() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_reflectedType_1)); }
	inline RuntimeType_t2836228502 * get_m_reflectedType_1() const { return ___m_reflectedType_1; }
	inline RuntimeType_t2836228502 ** get_address_of_m_reflectedType_1() { return &___m_reflectedType_1; }
	inline void set_m_reflectedType_1(RuntimeType_t2836228502 * value)
	{
		___m_reflectedType_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_reflectedType_1, value);
	}

	inline static int32_t get_offset_of_m_signature_2() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_signature_2)); }
	inline String_t* get_m_signature_2() const { return ___m_signature_2; }
	inline String_t** get_address_of_m_signature_2() { return &___m_signature_2; }
	inline void set_m_signature_2(String_t* value)
	{
		___m_signature_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_signature_2, value);
	}

	inline static int32_t get_offset_of_m_signature2_3() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_signature2_3)); }
	inline String_t* get_m_signature2_3() const { return ___m_signature2_3; }
	inline String_t** get_address_of_m_signature2_3() { return &___m_signature2_3; }
	inline void set_m_signature2_3(String_t* value)
	{
		___m_signature2_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_signature2_3, value);
	}

	inline static int32_t get_offset_of_m_memberType_4() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_memberType_4)); }
	inline int32_t get_m_memberType_4() const { return ___m_memberType_4; }
	inline int32_t* get_address_of_m_memberType_4() { return &___m_memberType_4; }
	inline void set_m_memberType_4(int32_t value)
	{
		___m_memberType_4 = value;
	}

	inline static int32_t get_offset_of_m_info_5() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t2799051170, ___m_info_5)); }
	inline SerializationInfo_t228987430 * get_m_info_5() const { return ___m_info_5; }
	inline SerializationInfo_t228987430 ** get_address_of_m_info_5() { return &___m_info_5; }
	inline void set_m_info_5(SerializationInfo_t228987430 * value)
	{
		___m_info_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_info_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
