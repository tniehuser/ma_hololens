﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SyntaxTreeNode2397191729.h"

// System.Xml.Schema.NamespaceList
struct NamespaceList_t848177191;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceListNode
struct  NamespaceListNode_t2509262495  : public SyntaxTreeNode_t2397191729
{
public:
	// System.Xml.Schema.NamespaceList System.Xml.Schema.NamespaceListNode::namespaceList
	NamespaceList_t848177191 * ___namespaceList_0;
	// System.Object System.Xml.Schema.NamespaceListNode::particle
	Il2CppObject * ___particle_1;

public:
	inline static int32_t get_offset_of_namespaceList_0() { return static_cast<int32_t>(offsetof(NamespaceListNode_t2509262495, ___namespaceList_0)); }
	inline NamespaceList_t848177191 * get_namespaceList_0() const { return ___namespaceList_0; }
	inline NamespaceList_t848177191 ** get_address_of_namespaceList_0() { return &___namespaceList_0; }
	inline void set_namespaceList_0(NamespaceList_t848177191 * value)
	{
		___namespaceList_0 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceList_0, value);
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(NamespaceListNode_t2509262495, ___particle_1)); }
	inline Il2CppObject * get_particle_1() const { return ___particle_1; }
	inline Il2CppObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(Il2CppObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier(&___particle_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
