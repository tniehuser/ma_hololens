﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Interop_mincore_TIME_ZONE_INFORMATION_U3C1060306023.h"
#include "mscorlib_Interop_mincore_SYSTEMTIME2580015906.h"
#include "mscorlib_Interop_mincore_TIME_ZONE_INFORMATION_U3CD186285493.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interop/mincore/TIME_ZONE_INFORMATION
struct  TIME_ZONE_INFORMATION_t2855177307 
{
public:
	// System.Int32 Interop/mincore/TIME_ZONE_INFORMATION::Bias
	int32_t ___Bias_0;
	// Interop/mincore/TIME_ZONE_INFORMATION/<StandardName>__FixedBuffer3 Interop/mincore/TIME_ZONE_INFORMATION::StandardName
	U3CStandardNameU3E__FixedBuffer3_t1060306023  ___StandardName_1;
	// Interop/mincore/SYSTEMTIME Interop/mincore/TIME_ZONE_INFORMATION::StandardDate
	SYSTEMTIME_t2580015906  ___StandardDate_2;
	// System.Int32 Interop/mincore/TIME_ZONE_INFORMATION::StandardBias
	int32_t ___StandardBias_3;
	// Interop/mincore/TIME_ZONE_INFORMATION/<DaylightName>__FixedBuffer4 Interop/mincore/TIME_ZONE_INFORMATION::DaylightName
	U3CDaylightNameU3E__FixedBuffer4_t186285493  ___DaylightName_4;
	// Interop/mincore/SYSTEMTIME Interop/mincore/TIME_ZONE_INFORMATION::DaylightDate
	SYSTEMTIME_t2580015906  ___DaylightDate_5;
	// System.Int32 Interop/mincore/TIME_ZONE_INFORMATION::DaylightBias
	int32_t ___DaylightBias_6;

public:
	inline static int32_t get_offset_of_Bias_0() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___Bias_0)); }
	inline int32_t get_Bias_0() const { return ___Bias_0; }
	inline int32_t* get_address_of_Bias_0() { return &___Bias_0; }
	inline void set_Bias_0(int32_t value)
	{
		___Bias_0 = value;
	}

	inline static int32_t get_offset_of_StandardName_1() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___StandardName_1)); }
	inline U3CStandardNameU3E__FixedBuffer3_t1060306023  get_StandardName_1() const { return ___StandardName_1; }
	inline U3CStandardNameU3E__FixedBuffer3_t1060306023 * get_address_of_StandardName_1() { return &___StandardName_1; }
	inline void set_StandardName_1(U3CStandardNameU3E__FixedBuffer3_t1060306023  value)
	{
		___StandardName_1 = value;
	}

	inline static int32_t get_offset_of_StandardDate_2() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___StandardDate_2)); }
	inline SYSTEMTIME_t2580015906  get_StandardDate_2() const { return ___StandardDate_2; }
	inline SYSTEMTIME_t2580015906 * get_address_of_StandardDate_2() { return &___StandardDate_2; }
	inline void set_StandardDate_2(SYSTEMTIME_t2580015906  value)
	{
		___StandardDate_2 = value;
	}

	inline static int32_t get_offset_of_StandardBias_3() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___StandardBias_3)); }
	inline int32_t get_StandardBias_3() const { return ___StandardBias_3; }
	inline int32_t* get_address_of_StandardBias_3() { return &___StandardBias_3; }
	inline void set_StandardBias_3(int32_t value)
	{
		___StandardBias_3 = value;
	}

	inline static int32_t get_offset_of_DaylightName_4() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___DaylightName_4)); }
	inline U3CDaylightNameU3E__FixedBuffer4_t186285493  get_DaylightName_4() const { return ___DaylightName_4; }
	inline U3CDaylightNameU3E__FixedBuffer4_t186285493 * get_address_of_DaylightName_4() { return &___DaylightName_4; }
	inline void set_DaylightName_4(U3CDaylightNameU3E__FixedBuffer4_t186285493  value)
	{
		___DaylightName_4 = value;
	}

	inline static int32_t get_offset_of_DaylightDate_5() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___DaylightDate_5)); }
	inline SYSTEMTIME_t2580015906  get_DaylightDate_5() const { return ___DaylightDate_5; }
	inline SYSTEMTIME_t2580015906 * get_address_of_DaylightDate_5() { return &___DaylightDate_5; }
	inline void set_DaylightDate_5(SYSTEMTIME_t2580015906  value)
	{
		___DaylightDate_5 = value;
	}

	inline static int32_t get_offset_of_DaylightBias_6() { return static_cast<int32_t>(offsetof(TIME_ZONE_INFORMATION_t2855177307, ___DaylightBias_6)); }
	inline int32_t get_DaylightBias_6() const { return ___DaylightBias_6; }
	inline int32_t* get_address_of_DaylightBias_6() { return &___DaylightBias_6; }
	inline void set_DaylightBias_6(int32_t value)
	{
		___DaylightBias_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
