﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_1126777061.h"

// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct List_1_t1879707642;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator
struct  XSOEnumerator_t1879684909  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::entries
	List_1_t1879707642 * ___entries_0;
	// System.Xml.Schema.XmlSchemaObjectTable/EnumeratorType System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::enumType
	int32_t ___enumType_1;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentIndex
	int32_t ___currentIndex_2;
	// System.Int32 System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::size
	int32_t ___size_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentKey
	XmlQualifiedName_t1944712516 * ___currentKey_4;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectTable/XSOEnumerator::currentValue
	XmlSchemaObject_t2050913741 * ___currentValue_5;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___entries_0)); }
	inline List_1_t1879707642 * get_entries_0() const { return ___entries_0; }
	inline List_1_t1879707642 ** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(List_1_t1879707642 * value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}

	inline static int32_t get_offset_of_enumType_1() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___enumType_1)); }
	inline int32_t get_enumType_1() const { return ___enumType_1; }
	inline int32_t* get_address_of_enumType_1() { return &___enumType_1; }
	inline void set_enumType_1(int32_t value)
	{
		___enumType_1 = value;
	}

	inline static int32_t get_offset_of_currentIndex_2() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___currentIndex_2)); }
	inline int32_t get_currentIndex_2() const { return ___currentIndex_2; }
	inline int32_t* get_address_of_currentIndex_2() { return &___currentIndex_2; }
	inline void set_currentIndex_2(int32_t value)
	{
		___currentIndex_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_currentKey_4() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___currentKey_4)); }
	inline XmlQualifiedName_t1944712516 * get_currentKey_4() const { return ___currentKey_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_currentKey_4() { return &___currentKey_4; }
	inline void set_currentKey_4(XmlQualifiedName_t1944712516 * value)
	{
		___currentKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentKey_4, value);
	}

	inline static int32_t get_offset_of_currentValue_5() { return static_cast<int32_t>(offsetof(XSOEnumerator_t1879684909, ___currentValue_5)); }
	inline XmlSchemaObject_t2050913741 * get_currentValue_5() const { return ___currentValue_5; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_currentValue_5() { return &___currentValue_5; }
	inline void set_currentValue_5(XmlSchemaObject_t2050913741 * value)
	{
		___currentValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentValue_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
