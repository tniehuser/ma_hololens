﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t3218582488;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoConfig
struct  CryptoConfig_t896479599  : public Il2CppObject
{
public:

public:
};

struct CryptoConfig_t896479599_StaticFields
{
public:
	// System.Object System.Security.Cryptography.CryptoConfig::lockObject
	Il2CppObject * ___lockObject_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> System.Security.Cryptography.CryptoConfig::algorithms
	Dictionary_2_t3218582488 * ___algorithms_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Security.Cryptography.CryptoConfig::unresolved_algorithms
	Dictionary_2_t3943999495 * ___unresolved_algorithms_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Security.Cryptography.CryptoConfig::oids
	Dictionary_2_t3943999495 * ___oids_3;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultSHA1
	Type_t * ___defaultSHA1_4;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultMD5
	Type_t * ___defaultMD5_5;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultSHA256
	Type_t * ___defaultSHA256_6;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultSHA384
	Type_t * ___defaultSHA384_7;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultSHA512
	Type_t * ___defaultSHA512_8;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRSA
	Type_t * ___defaultRSA_9;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultDSA
	Type_t * ___defaultDSA_10;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultDES
	Type_t * ___defaultDES_11;
	// System.Type System.Security.Cryptography.CryptoConfig::default3DES
	Type_t * ___default3DES_12;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRC2
	Type_t * ___defaultRC2_13;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultAES
	Type_t * ___defaultAES_14;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRNG
	Type_t * ___defaultRNG_15;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMAC
	Type_t * ___defaultHMAC_16;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultMAC3DES
	Type_t * ___defaultMAC3DES_17;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultDSASigDesc
	Type_t * ___defaultDSASigDesc_18;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRSAPKCS1SHA1SigDesc
	Type_t * ___defaultRSAPKCS1SHA1SigDesc_19;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRSAPKCS1SHA256SigDesc
	Type_t * ___defaultRSAPKCS1SHA256SigDesc_20;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRSAPKCS1SHA384SigDesc
	Type_t * ___defaultRSAPKCS1SHA384SigDesc_21;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRSAPKCS1SHA512SigDesc
	Type_t * ___defaultRSAPKCS1SHA512SigDesc_22;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultRIPEMD160
	Type_t * ___defaultRIPEMD160_23;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMACMD5
	Type_t * ___defaultHMACMD5_24;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMACRIPEMD160
	Type_t * ___defaultHMACRIPEMD160_25;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMACSHA256
	Type_t * ___defaultHMACSHA256_26;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMACSHA384
	Type_t * ___defaultHMACSHA384_27;
	// System.Type System.Security.Cryptography.CryptoConfig::defaultHMACSHA512
	Type_t * ___defaultHMACSHA512_28;

public:
	inline static int32_t get_offset_of_lockObject_0() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___lockObject_0)); }
	inline Il2CppObject * get_lockObject_0() const { return ___lockObject_0; }
	inline Il2CppObject ** get_address_of_lockObject_0() { return &___lockObject_0; }
	inline void set_lockObject_0(Il2CppObject * value)
	{
		___lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___lockObject_0, value);
	}

	inline static int32_t get_offset_of_algorithms_1() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___algorithms_1)); }
	inline Dictionary_2_t3218582488 * get_algorithms_1() const { return ___algorithms_1; }
	inline Dictionary_2_t3218582488 ** get_address_of_algorithms_1() { return &___algorithms_1; }
	inline void set_algorithms_1(Dictionary_2_t3218582488 * value)
	{
		___algorithms_1 = value;
		Il2CppCodeGenWriteBarrier(&___algorithms_1, value);
	}

	inline static int32_t get_offset_of_unresolved_algorithms_2() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___unresolved_algorithms_2)); }
	inline Dictionary_2_t3943999495 * get_unresolved_algorithms_2() const { return ___unresolved_algorithms_2; }
	inline Dictionary_2_t3943999495 ** get_address_of_unresolved_algorithms_2() { return &___unresolved_algorithms_2; }
	inline void set_unresolved_algorithms_2(Dictionary_2_t3943999495 * value)
	{
		___unresolved_algorithms_2 = value;
		Il2CppCodeGenWriteBarrier(&___unresolved_algorithms_2, value);
	}

	inline static int32_t get_offset_of_oids_3() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___oids_3)); }
	inline Dictionary_2_t3943999495 * get_oids_3() const { return ___oids_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_oids_3() { return &___oids_3; }
	inline void set_oids_3(Dictionary_2_t3943999495 * value)
	{
		___oids_3 = value;
		Il2CppCodeGenWriteBarrier(&___oids_3, value);
	}

	inline static int32_t get_offset_of_defaultSHA1_4() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultSHA1_4)); }
	inline Type_t * get_defaultSHA1_4() const { return ___defaultSHA1_4; }
	inline Type_t ** get_address_of_defaultSHA1_4() { return &___defaultSHA1_4; }
	inline void set_defaultSHA1_4(Type_t * value)
	{
		___defaultSHA1_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSHA1_4, value);
	}

	inline static int32_t get_offset_of_defaultMD5_5() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultMD5_5)); }
	inline Type_t * get_defaultMD5_5() const { return ___defaultMD5_5; }
	inline Type_t ** get_address_of_defaultMD5_5() { return &___defaultMD5_5; }
	inline void set_defaultMD5_5(Type_t * value)
	{
		___defaultMD5_5 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMD5_5, value);
	}

	inline static int32_t get_offset_of_defaultSHA256_6() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultSHA256_6)); }
	inline Type_t * get_defaultSHA256_6() const { return ___defaultSHA256_6; }
	inline Type_t ** get_address_of_defaultSHA256_6() { return &___defaultSHA256_6; }
	inline void set_defaultSHA256_6(Type_t * value)
	{
		___defaultSHA256_6 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSHA256_6, value);
	}

	inline static int32_t get_offset_of_defaultSHA384_7() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultSHA384_7)); }
	inline Type_t * get_defaultSHA384_7() const { return ___defaultSHA384_7; }
	inline Type_t ** get_address_of_defaultSHA384_7() { return &___defaultSHA384_7; }
	inline void set_defaultSHA384_7(Type_t * value)
	{
		___defaultSHA384_7 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSHA384_7, value);
	}

	inline static int32_t get_offset_of_defaultSHA512_8() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultSHA512_8)); }
	inline Type_t * get_defaultSHA512_8() const { return ___defaultSHA512_8; }
	inline Type_t ** get_address_of_defaultSHA512_8() { return &___defaultSHA512_8; }
	inline void set_defaultSHA512_8(Type_t * value)
	{
		___defaultSHA512_8 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSHA512_8, value);
	}

	inline static int32_t get_offset_of_defaultRSA_9() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRSA_9)); }
	inline Type_t * get_defaultRSA_9() const { return ___defaultRSA_9; }
	inline Type_t ** get_address_of_defaultRSA_9() { return &___defaultRSA_9; }
	inline void set_defaultRSA_9(Type_t * value)
	{
		___defaultRSA_9 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRSA_9, value);
	}

	inline static int32_t get_offset_of_defaultDSA_10() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultDSA_10)); }
	inline Type_t * get_defaultDSA_10() const { return ___defaultDSA_10; }
	inline Type_t ** get_address_of_defaultDSA_10() { return &___defaultDSA_10; }
	inline void set_defaultDSA_10(Type_t * value)
	{
		___defaultDSA_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultDSA_10, value);
	}

	inline static int32_t get_offset_of_defaultDES_11() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultDES_11)); }
	inline Type_t * get_defaultDES_11() const { return ___defaultDES_11; }
	inline Type_t ** get_address_of_defaultDES_11() { return &___defaultDES_11; }
	inline void set_defaultDES_11(Type_t * value)
	{
		___defaultDES_11 = value;
		Il2CppCodeGenWriteBarrier(&___defaultDES_11, value);
	}

	inline static int32_t get_offset_of_default3DES_12() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___default3DES_12)); }
	inline Type_t * get_default3DES_12() const { return ___default3DES_12; }
	inline Type_t ** get_address_of_default3DES_12() { return &___default3DES_12; }
	inline void set_default3DES_12(Type_t * value)
	{
		___default3DES_12 = value;
		Il2CppCodeGenWriteBarrier(&___default3DES_12, value);
	}

	inline static int32_t get_offset_of_defaultRC2_13() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRC2_13)); }
	inline Type_t * get_defaultRC2_13() const { return ___defaultRC2_13; }
	inline Type_t ** get_address_of_defaultRC2_13() { return &___defaultRC2_13; }
	inline void set_defaultRC2_13(Type_t * value)
	{
		___defaultRC2_13 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRC2_13, value);
	}

	inline static int32_t get_offset_of_defaultAES_14() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultAES_14)); }
	inline Type_t * get_defaultAES_14() const { return ___defaultAES_14; }
	inline Type_t ** get_address_of_defaultAES_14() { return &___defaultAES_14; }
	inline void set_defaultAES_14(Type_t * value)
	{
		___defaultAES_14 = value;
		Il2CppCodeGenWriteBarrier(&___defaultAES_14, value);
	}

	inline static int32_t get_offset_of_defaultRNG_15() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRNG_15)); }
	inline Type_t * get_defaultRNG_15() const { return ___defaultRNG_15; }
	inline Type_t ** get_address_of_defaultRNG_15() { return &___defaultRNG_15; }
	inline void set_defaultRNG_15(Type_t * value)
	{
		___defaultRNG_15 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRNG_15, value);
	}

	inline static int32_t get_offset_of_defaultHMAC_16() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMAC_16)); }
	inline Type_t * get_defaultHMAC_16() const { return ___defaultHMAC_16; }
	inline Type_t ** get_address_of_defaultHMAC_16() { return &___defaultHMAC_16; }
	inline void set_defaultHMAC_16(Type_t * value)
	{
		___defaultHMAC_16 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMAC_16, value);
	}

	inline static int32_t get_offset_of_defaultMAC3DES_17() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultMAC3DES_17)); }
	inline Type_t * get_defaultMAC3DES_17() const { return ___defaultMAC3DES_17; }
	inline Type_t ** get_address_of_defaultMAC3DES_17() { return &___defaultMAC3DES_17; }
	inline void set_defaultMAC3DES_17(Type_t * value)
	{
		___defaultMAC3DES_17 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMAC3DES_17, value);
	}

	inline static int32_t get_offset_of_defaultDSASigDesc_18() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultDSASigDesc_18)); }
	inline Type_t * get_defaultDSASigDesc_18() const { return ___defaultDSASigDesc_18; }
	inline Type_t ** get_address_of_defaultDSASigDesc_18() { return &___defaultDSASigDesc_18; }
	inline void set_defaultDSASigDesc_18(Type_t * value)
	{
		___defaultDSASigDesc_18 = value;
		Il2CppCodeGenWriteBarrier(&___defaultDSASigDesc_18, value);
	}

	inline static int32_t get_offset_of_defaultRSAPKCS1SHA1SigDesc_19() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRSAPKCS1SHA1SigDesc_19)); }
	inline Type_t * get_defaultRSAPKCS1SHA1SigDesc_19() const { return ___defaultRSAPKCS1SHA1SigDesc_19; }
	inline Type_t ** get_address_of_defaultRSAPKCS1SHA1SigDesc_19() { return &___defaultRSAPKCS1SHA1SigDesc_19; }
	inline void set_defaultRSAPKCS1SHA1SigDesc_19(Type_t * value)
	{
		___defaultRSAPKCS1SHA1SigDesc_19 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRSAPKCS1SHA1SigDesc_19, value);
	}

	inline static int32_t get_offset_of_defaultRSAPKCS1SHA256SigDesc_20() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRSAPKCS1SHA256SigDesc_20)); }
	inline Type_t * get_defaultRSAPKCS1SHA256SigDesc_20() const { return ___defaultRSAPKCS1SHA256SigDesc_20; }
	inline Type_t ** get_address_of_defaultRSAPKCS1SHA256SigDesc_20() { return &___defaultRSAPKCS1SHA256SigDesc_20; }
	inline void set_defaultRSAPKCS1SHA256SigDesc_20(Type_t * value)
	{
		___defaultRSAPKCS1SHA256SigDesc_20 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRSAPKCS1SHA256SigDesc_20, value);
	}

	inline static int32_t get_offset_of_defaultRSAPKCS1SHA384SigDesc_21() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRSAPKCS1SHA384SigDesc_21)); }
	inline Type_t * get_defaultRSAPKCS1SHA384SigDesc_21() const { return ___defaultRSAPKCS1SHA384SigDesc_21; }
	inline Type_t ** get_address_of_defaultRSAPKCS1SHA384SigDesc_21() { return &___defaultRSAPKCS1SHA384SigDesc_21; }
	inline void set_defaultRSAPKCS1SHA384SigDesc_21(Type_t * value)
	{
		___defaultRSAPKCS1SHA384SigDesc_21 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRSAPKCS1SHA384SigDesc_21, value);
	}

	inline static int32_t get_offset_of_defaultRSAPKCS1SHA512SigDesc_22() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRSAPKCS1SHA512SigDesc_22)); }
	inline Type_t * get_defaultRSAPKCS1SHA512SigDesc_22() const { return ___defaultRSAPKCS1SHA512SigDesc_22; }
	inline Type_t ** get_address_of_defaultRSAPKCS1SHA512SigDesc_22() { return &___defaultRSAPKCS1SHA512SigDesc_22; }
	inline void set_defaultRSAPKCS1SHA512SigDesc_22(Type_t * value)
	{
		___defaultRSAPKCS1SHA512SigDesc_22 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRSAPKCS1SHA512SigDesc_22, value);
	}

	inline static int32_t get_offset_of_defaultRIPEMD160_23() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultRIPEMD160_23)); }
	inline Type_t * get_defaultRIPEMD160_23() const { return ___defaultRIPEMD160_23; }
	inline Type_t ** get_address_of_defaultRIPEMD160_23() { return &___defaultRIPEMD160_23; }
	inline void set_defaultRIPEMD160_23(Type_t * value)
	{
		___defaultRIPEMD160_23 = value;
		Il2CppCodeGenWriteBarrier(&___defaultRIPEMD160_23, value);
	}

	inline static int32_t get_offset_of_defaultHMACMD5_24() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMACMD5_24)); }
	inline Type_t * get_defaultHMACMD5_24() const { return ___defaultHMACMD5_24; }
	inline Type_t ** get_address_of_defaultHMACMD5_24() { return &___defaultHMACMD5_24; }
	inline void set_defaultHMACMD5_24(Type_t * value)
	{
		___defaultHMACMD5_24 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMACMD5_24, value);
	}

	inline static int32_t get_offset_of_defaultHMACRIPEMD160_25() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMACRIPEMD160_25)); }
	inline Type_t * get_defaultHMACRIPEMD160_25() const { return ___defaultHMACRIPEMD160_25; }
	inline Type_t ** get_address_of_defaultHMACRIPEMD160_25() { return &___defaultHMACRIPEMD160_25; }
	inline void set_defaultHMACRIPEMD160_25(Type_t * value)
	{
		___defaultHMACRIPEMD160_25 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMACRIPEMD160_25, value);
	}

	inline static int32_t get_offset_of_defaultHMACSHA256_26() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMACSHA256_26)); }
	inline Type_t * get_defaultHMACSHA256_26() const { return ___defaultHMACSHA256_26; }
	inline Type_t ** get_address_of_defaultHMACSHA256_26() { return &___defaultHMACSHA256_26; }
	inline void set_defaultHMACSHA256_26(Type_t * value)
	{
		___defaultHMACSHA256_26 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMACSHA256_26, value);
	}

	inline static int32_t get_offset_of_defaultHMACSHA384_27() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMACSHA384_27)); }
	inline Type_t * get_defaultHMACSHA384_27() const { return ___defaultHMACSHA384_27; }
	inline Type_t ** get_address_of_defaultHMACSHA384_27() { return &___defaultHMACSHA384_27; }
	inline void set_defaultHMACSHA384_27(Type_t * value)
	{
		___defaultHMACSHA384_27 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMACSHA384_27, value);
	}

	inline static int32_t get_offset_of_defaultHMACSHA512_28() { return static_cast<int32_t>(offsetof(CryptoConfig_t896479599_StaticFields, ___defaultHMACSHA512_28)); }
	inline Type_t * get_defaultHMACSHA512_28() const { return ___defaultHMACSHA512_28; }
	inline Type_t ** get_address_of_defaultHMACSHA512_28() { return &___defaultHMACSHA512_28; }
	inline void set_defaultHMACSHA512_28(Type_t * value)
	{
		___defaultHMACSHA512_28 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHMACSHA512_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
