﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Net.HeaderInfo
struct HeaderInfo_t3044570949;
// System.Net.HeaderParser
struct HeaderParser_t3706119548;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfoTable
struct  HeaderInfoTable_t2462863175  : public Il2CppObject
{
public:

public:
};

struct HeaderInfoTable_t2462863175_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.HeaderInfoTable::HeaderHashTable
	Hashtable_t909839986 * ___HeaderHashTable_0;
	// System.Net.HeaderInfo System.Net.HeaderInfoTable::UnknownHeaderInfo
	HeaderInfo_t3044570949 * ___UnknownHeaderInfo_1;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::SingleParser
	HeaderParser_t3706119548 * ___SingleParser_2;
	// System.Net.HeaderParser System.Net.HeaderInfoTable::MultiParser
	HeaderParser_t3706119548 * ___MultiParser_3;

public:
	inline static int32_t get_offset_of_HeaderHashTable_0() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___HeaderHashTable_0)); }
	inline Hashtable_t909839986 * get_HeaderHashTable_0() const { return ___HeaderHashTable_0; }
	inline Hashtable_t909839986 ** get_address_of_HeaderHashTable_0() { return &___HeaderHashTable_0; }
	inline void set_HeaderHashTable_0(Hashtable_t909839986 * value)
	{
		___HeaderHashTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderHashTable_0, value);
	}

	inline static int32_t get_offset_of_UnknownHeaderInfo_1() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___UnknownHeaderInfo_1)); }
	inline HeaderInfo_t3044570949 * get_UnknownHeaderInfo_1() const { return ___UnknownHeaderInfo_1; }
	inline HeaderInfo_t3044570949 ** get_address_of_UnknownHeaderInfo_1() { return &___UnknownHeaderInfo_1; }
	inline void set_UnknownHeaderInfo_1(HeaderInfo_t3044570949 * value)
	{
		___UnknownHeaderInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___UnknownHeaderInfo_1, value);
	}

	inline static int32_t get_offset_of_SingleParser_2() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___SingleParser_2)); }
	inline HeaderParser_t3706119548 * get_SingleParser_2() const { return ___SingleParser_2; }
	inline HeaderParser_t3706119548 ** get_address_of_SingleParser_2() { return &___SingleParser_2; }
	inline void set_SingleParser_2(HeaderParser_t3706119548 * value)
	{
		___SingleParser_2 = value;
		Il2CppCodeGenWriteBarrier(&___SingleParser_2, value);
	}

	inline static int32_t get_offset_of_MultiParser_3() { return static_cast<int32_t>(offsetof(HeaderInfoTable_t2462863175_StaticFields, ___MultiParser_3)); }
	inline HeaderParser_t3706119548 * get_MultiParser_3() const { return ___MultiParser_3; }
	inline HeaderParser_t3706119548 ** get_address_of_MultiParser_3() { return &___MultiParser_3; }
	inline void set_MultiParser_3(HeaderParser_t3706119548 * value)
	{
		___MultiParser_3 = value;
		Il2CppCodeGenWriteBarrier(&___MultiParser_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
