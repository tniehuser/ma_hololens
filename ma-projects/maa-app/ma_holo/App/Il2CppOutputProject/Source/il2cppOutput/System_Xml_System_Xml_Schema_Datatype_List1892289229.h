﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_anySimpleTyp4012795865.h"

// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_t1152094268;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_List
struct  Datatype_List_t1892289229  : public Datatype_anySimpleType_t4012795865
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.Datatype_List::itemType
	DatatypeImplementation_t1152094268 * ___itemType_93;
	// System.Int32 System.Xml.Schema.Datatype_List::minListSize
	int32_t ___minListSize_94;

public:
	inline static int32_t get_offset_of_itemType_93() { return static_cast<int32_t>(offsetof(Datatype_List_t1892289229, ___itemType_93)); }
	inline DatatypeImplementation_t1152094268 * get_itemType_93() const { return ___itemType_93; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_itemType_93() { return &___itemType_93; }
	inline void set_itemType_93(DatatypeImplementation_t1152094268 * value)
	{
		___itemType_93 = value;
		Il2CppCodeGenWriteBarrier(&___itemType_93, value);
	}

	inline static int32_t get_offset_of_minListSize_94() { return static_cast<int32_t>(offsetof(Datatype_List_t1892289229, ___minListSize_94)); }
	inline int32_t get_minListSize_94() const { return ___minListSize_94; }
	inline int32_t* get_address_of_minListSize_94() { return &___minListSize_94; }
	inline void set_minListSize_94(int32_t value)
	{
		___minListSize_94 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
