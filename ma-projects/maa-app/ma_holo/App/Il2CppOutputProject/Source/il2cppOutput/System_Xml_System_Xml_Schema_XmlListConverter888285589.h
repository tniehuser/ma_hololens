﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlBaseConverter217670330.h"

// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlListConverter
struct  XmlListConverter_t888285589  : public XmlBaseConverter_t217670330
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlListConverter::atomicConverter
	XmlValueConverter_t68179724 * ___atomicConverter_32;

public:
	inline static int32_t get_offset_of_atomicConverter_32() { return static_cast<int32_t>(offsetof(XmlListConverter_t888285589, ___atomicConverter_32)); }
	inline XmlValueConverter_t68179724 * get_atomicConverter_32() const { return ___atomicConverter_32; }
	inline XmlValueConverter_t68179724 ** get_address_of_atomicConverter_32() { return &___atomicConverter_32; }
	inline void set_atomicConverter_32(XmlValueConverter_t68179724 * value)
	{
		___atomicConverter_32 = value;
		Il2CppCodeGenWriteBarrier(&___atomicConverter_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
