﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_AsymmetricSi4058014248.h"

// System.Security.Cryptography.DSA
struct DSA_t903174880;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t1065727064  : public AsymmetricSignatureFormatter_t4058014248
{
public:
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::_dsaKey
	DSA_t903174880 * ____dsaKey_0;
	// System.String System.Security.Cryptography.DSASignatureFormatter::_oid
	String_t* ____oid_1;

public:
	inline static int32_t get_offset_of__dsaKey_0() { return static_cast<int32_t>(offsetof(DSASignatureFormatter_t1065727064, ____dsaKey_0)); }
	inline DSA_t903174880 * get__dsaKey_0() const { return ____dsaKey_0; }
	inline DSA_t903174880 ** get_address_of__dsaKey_0() { return &____dsaKey_0; }
	inline void set__dsaKey_0(DSA_t903174880 * value)
	{
		____dsaKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____dsaKey_0, value);
	}

	inline static int32_t get_offset_of__oid_1() { return static_cast<int32_t>(offsetof(DSASignatureFormatter_t1065727064, ____oid_1)); }
	inline String_t* get__oid_1() const { return ____oid_1; }
	inline String_t** get_address_of__oid_1() { return &____oid_1; }
	inline void set__oid_1(String_t* value)
	{
		____oid_1 = value;
		Il2CppCodeGenWriteBarrier(&____oid_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
