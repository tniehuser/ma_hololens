﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_FacetsChecker1235574227.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Numeric10FacetsChecker
struct  Numeric10FacetsChecker_t3431899547  : public FacetsChecker_t1235574227
{
public:
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::maxValue
	Decimal_t724701077  ___maxValue_1;
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::minValue
	Decimal_t724701077  ___minValue_2;

public:
	inline static int32_t get_offset_of_maxValue_1() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547, ___maxValue_1)); }
	inline Decimal_t724701077  get_maxValue_1() const { return ___maxValue_1; }
	inline Decimal_t724701077 * get_address_of_maxValue_1() { return &___maxValue_1; }
	inline void set_maxValue_1(Decimal_t724701077  value)
	{
		___maxValue_1 = value;
	}

	inline static int32_t get_offset_of_minValue_2() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547, ___minValue_2)); }
	inline Decimal_t724701077  get_minValue_2() const { return ___minValue_2; }
	inline Decimal_t724701077 * get_address_of_minValue_2() { return &___minValue_2; }
	inline void set_minValue_2(Decimal_t724701077  value)
	{
		___minValue_2 = value;
	}
};

struct Numeric10FacetsChecker_t3431899547_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.Numeric10FacetsChecker::signs
	CharU5BU5D_t1328083999* ___signs_0;

public:
	inline static int32_t get_offset_of_signs_0() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547_StaticFields, ___signs_0)); }
	inline CharU5BU5D_t1328083999* get_signs_0() const { return ___signs_0; }
	inline CharU5BU5D_t1328083999** get_address_of_signs_0() { return &___signs_0; }
	inline void set_signs_0(CharU5BU5D_t1328083999* value)
	{
		___signs_0 = value;
		Il2CppCodeGenWriteBarrier(&___signs_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
