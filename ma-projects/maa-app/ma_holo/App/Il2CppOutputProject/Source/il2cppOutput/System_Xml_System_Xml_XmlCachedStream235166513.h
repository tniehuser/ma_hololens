﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_MemoryStream743994179.h"

// System.Uri
struct Uri_t19570940;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCachedStream
struct  XmlCachedStream_t235166513  : public MemoryStream_t743994179
{
public:
	// System.Uri System.Xml.XmlCachedStream::uri
	Uri_t19570940 * ___uri_18;

public:
	inline static int32_t get_offset_of_uri_18() { return static_cast<int32_t>(offsetof(XmlCachedStream_t235166513, ___uri_18)); }
	inline Uri_t19570940 * get_uri_18() const { return ___uri_18; }
	inline Uri_t19570940 ** get_address_of_uri_18() { return &___uri_18; }
	inline void set_uri_18(Uri_t19570940 * value)
	{
		___uri_18 = value;
		Il2CppCodeGenWriteBarrier(&___uri_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
