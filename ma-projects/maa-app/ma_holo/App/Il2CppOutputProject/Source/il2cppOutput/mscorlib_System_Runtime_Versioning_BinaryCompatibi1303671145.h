﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Versioning_TargetFramework2072570862.h"

// System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap
struct BinaryCompatibilityMap_t3841228664;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Versioning.BinaryCompatibility
struct  BinaryCompatibility_t1303671145  : public Il2CppObject
{
public:

public:
};

struct BinaryCompatibility_t1303671145_StaticFields
{
public:
	// System.Runtime.Versioning.TargetFrameworkId System.Runtime.Versioning.BinaryCompatibility::s_AppWasBuiltForFramework
	int32_t ___s_AppWasBuiltForFramework_0;
	// System.Int32 System.Runtime.Versioning.BinaryCompatibility::s_AppWasBuiltForVersion
	int32_t ___s_AppWasBuiltForVersion_1;
	// System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap System.Runtime.Versioning.BinaryCompatibility::s_map
	BinaryCompatibilityMap_t3841228664 * ___s_map_2;

public:
	inline static int32_t get_offset_of_s_AppWasBuiltForFramework_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t1303671145_StaticFields, ___s_AppWasBuiltForFramework_0)); }
	inline int32_t get_s_AppWasBuiltForFramework_0() const { return ___s_AppWasBuiltForFramework_0; }
	inline int32_t* get_address_of_s_AppWasBuiltForFramework_0() { return &___s_AppWasBuiltForFramework_0; }
	inline void set_s_AppWasBuiltForFramework_0(int32_t value)
	{
		___s_AppWasBuiltForFramework_0 = value;
	}

	inline static int32_t get_offset_of_s_AppWasBuiltForVersion_1() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t1303671145_StaticFields, ___s_AppWasBuiltForVersion_1)); }
	inline int32_t get_s_AppWasBuiltForVersion_1() const { return ___s_AppWasBuiltForVersion_1; }
	inline int32_t* get_address_of_s_AppWasBuiltForVersion_1() { return &___s_AppWasBuiltForVersion_1; }
	inline void set_s_AppWasBuiltForVersion_1(int32_t value)
	{
		___s_AppWasBuiltForVersion_1 = value;
	}

	inline static int32_t get_offset_of_s_map_2() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t1303671145_StaticFields, ___s_map_2)); }
	inline BinaryCompatibilityMap_t3841228664 * get_s_map_2() const { return ___s_map_2; }
	inline BinaryCompatibilityMap_t3841228664 ** get_address_of_s_map_2() { return &___s_map_2; }
	inline void set_s_map_2(BinaryCompatibilityMap_t3841228664 * value)
	{
		___s_map_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_map_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
