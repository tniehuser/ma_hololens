﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t1980576455;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CompatibleComparer
struct  CompatibleComparer_t2934985531  : public Il2CppObject
{
public:
	// System.Collections.IComparer System.Collections.CompatibleComparer::_comparer
	Il2CppObject * ____comparer_0;
	// System.Collections.IHashCodeProvider System.Collections.CompatibleComparer::_hcp
	Il2CppObject * ____hcp_1;

public:
	inline static int32_t get_offset_of__comparer_0() { return static_cast<int32_t>(offsetof(CompatibleComparer_t2934985531, ____comparer_0)); }
	inline Il2CppObject * get__comparer_0() const { return ____comparer_0; }
	inline Il2CppObject ** get_address_of__comparer_0() { return &____comparer_0; }
	inline void set__comparer_0(Il2CppObject * value)
	{
		____comparer_0 = value;
		Il2CppCodeGenWriteBarrier(&____comparer_0, value);
	}

	inline static int32_t get_offset_of__hcp_1() { return static_cast<int32_t>(offsetof(CompatibleComparer_t2934985531, ____hcp_1)); }
	inline Il2CppObject * get__hcp_1() const { return ____hcp_1; }
	inline Il2CppObject ** get_address_of__hcp_1() { return &____hcp_1; }
	inline void set__hcp_1(Il2CppObject * value)
	{
		____hcp_1 = value;
		Il2CppCodeGenWriteBarrier(&____hcp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
