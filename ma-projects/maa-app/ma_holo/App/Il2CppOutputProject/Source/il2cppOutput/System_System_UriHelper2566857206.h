﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHelper
struct  UriHelper_t2566857206  : public Il2CppObject
{
public:

public:
};

struct UriHelper_t2566857206_StaticFields
{
public:
	// System.Char[] System.UriHelper::HexUpperChars
	CharU5BU5D_t1328083999* ___HexUpperChars_0;

public:
	inline static int32_t get_offset_of_HexUpperChars_0() { return static_cast<int32_t>(offsetof(UriHelper_t2566857206_StaticFields, ___HexUpperChars_0)); }
	inline CharU5BU5D_t1328083999* get_HexUpperChars_0() const { return ___HexUpperChars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_HexUpperChars_0() { return &___HexUpperChars_0; }
	inline void set_HexUpperChars_0(CharU5BU5D_t1328083999* value)
	{
		___HexUpperChars_0 = value;
		Il2CppCodeGenWriteBarrier(&___HexUpperChars_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
