﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EmptyArray`1<System.Char>
struct  EmptyArray_1_t3790661640  : public Il2CppObject
{
public:

public:
};

struct EmptyArray_1_t3790661640_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	CharU5BU5D_t1328083999* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_t3790661640_StaticFields, ___Value_0)); }
	inline CharU5BU5D_t1328083999* get_Value_0() const { return ___Value_0; }
	inline CharU5BU5D_t1328083999** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(CharU5BU5D_t1328083999* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
