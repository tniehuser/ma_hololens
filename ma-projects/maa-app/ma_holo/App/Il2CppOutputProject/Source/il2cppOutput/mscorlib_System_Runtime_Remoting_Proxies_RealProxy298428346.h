﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type
struct Type_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t502196753;
// System.MarshalByRefObject
struct MarshalByRefObject_t1285298191;
// System.String
struct String_t;
// System.Runtime.Remoting.Identity
struct Identity_t3647548000;
// System.Object
struct Il2CppObject;
struct Context_t502196753_marshaled_pinvoke;
struct MarshalByRefObject_t1285298191_marshaled_pinvoke;
struct Context_t502196753_marshaled_com;
struct MarshalByRefObject_t1285298191_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Proxies.RealProxy
struct  RealProxy_t298428346  : public Il2CppObject
{
public:
	// System.Type System.Runtime.Remoting.Proxies.RealProxy::class_to_proxy
	Type_t * ___class_to_proxy_0;
	// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Proxies.RealProxy::_targetContext
	Context_t502196753 * ____targetContext_1;
	// System.MarshalByRefObject System.Runtime.Remoting.Proxies.RealProxy::_server
	MarshalByRefObject_t1285298191 * ____server_2;
	// System.Int32 System.Runtime.Remoting.Proxies.RealProxy::_targetDomainId
	int32_t ____targetDomainId_3;
	// System.String System.Runtime.Remoting.Proxies.RealProxy::_targetUri
	String_t* ____targetUri_4;
	// System.Runtime.Remoting.Identity System.Runtime.Remoting.Proxies.RealProxy::_objectIdentity
	Identity_t3647548000 * ____objectIdentity_5;
	// System.Object System.Runtime.Remoting.Proxies.RealProxy::_objTP
	Il2CppObject * ____objTP_6;
	// System.Object System.Runtime.Remoting.Proxies.RealProxy::_stubData
	Il2CppObject * ____stubData_7;

public:
	inline static int32_t get_offset_of_class_to_proxy_0() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ___class_to_proxy_0)); }
	inline Type_t * get_class_to_proxy_0() const { return ___class_to_proxy_0; }
	inline Type_t ** get_address_of_class_to_proxy_0() { return &___class_to_proxy_0; }
	inline void set_class_to_proxy_0(Type_t * value)
	{
		___class_to_proxy_0 = value;
		Il2CppCodeGenWriteBarrier(&___class_to_proxy_0, value);
	}

	inline static int32_t get_offset_of__targetContext_1() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____targetContext_1)); }
	inline Context_t502196753 * get__targetContext_1() const { return ____targetContext_1; }
	inline Context_t502196753 ** get_address_of__targetContext_1() { return &____targetContext_1; }
	inline void set__targetContext_1(Context_t502196753 * value)
	{
		____targetContext_1 = value;
		Il2CppCodeGenWriteBarrier(&____targetContext_1, value);
	}

	inline static int32_t get_offset_of__server_2() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____server_2)); }
	inline MarshalByRefObject_t1285298191 * get__server_2() const { return ____server_2; }
	inline MarshalByRefObject_t1285298191 ** get_address_of__server_2() { return &____server_2; }
	inline void set__server_2(MarshalByRefObject_t1285298191 * value)
	{
		____server_2 = value;
		Il2CppCodeGenWriteBarrier(&____server_2, value);
	}

	inline static int32_t get_offset_of__targetDomainId_3() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____targetDomainId_3)); }
	inline int32_t get__targetDomainId_3() const { return ____targetDomainId_3; }
	inline int32_t* get_address_of__targetDomainId_3() { return &____targetDomainId_3; }
	inline void set__targetDomainId_3(int32_t value)
	{
		____targetDomainId_3 = value;
	}

	inline static int32_t get_offset_of__targetUri_4() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____targetUri_4)); }
	inline String_t* get__targetUri_4() const { return ____targetUri_4; }
	inline String_t** get_address_of__targetUri_4() { return &____targetUri_4; }
	inline void set__targetUri_4(String_t* value)
	{
		____targetUri_4 = value;
		Il2CppCodeGenWriteBarrier(&____targetUri_4, value);
	}

	inline static int32_t get_offset_of__objectIdentity_5() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____objectIdentity_5)); }
	inline Identity_t3647548000 * get__objectIdentity_5() const { return ____objectIdentity_5; }
	inline Identity_t3647548000 ** get_address_of__objectIdentity_5() { return &____objectIdentity_5; }
	inline void set__objectIdentity_5(Identity_t3647548000 * value)
	{
		____objectIdentity_5 = value;
		Il2CppCodeGenWriteBarrier(&____objectIdentity_5, value);
	}

	inline static int32_t get_offset_of__objTP_6() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____objTP_6)); }
	inline Il2CppObject * get__objTP_6() const { return ____objTP_6; }
	inline Il2CppObject ** get_address_of__objTP_6() { return &____objTP_6; }
	inline void set__objTP_6(Il2CppObject * value)
	{
		____objTP_6 = value;
		Il2CppCodeGenWriteBarrier(&____objTP_6, value);
	}

	inline static int32_t get_offset_of__stubData_7() { return static_cast<int32_t>(offsetof(RealProxy_t298428346, ____stubData_7)); }
	inline Il2CppObject * get__stubData_7() const { return ____stubData_7; }
	inline Il2CppObject ** get_address_of__stubData_7() { return &____stubData_7; }
	inline void set__stubData_7(Il2CppObject * value)
	{
		____stubData_7 = value;
		Il2CppCodeGenWriteBarrier(&____stubData_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t298428346_marshaled_pinvoke
{
	Type_t * ___class_to_proxy_0;
	Context_t502196753_marshaled_pinvoke* ____targetContext_1;
	MarshalByRefObject_t1285298191_marshaled_pinvoke* ____server_2;
	int32_t ____targetDomainId_3;
	char* ____targetUri_4;
	Identity_t3647548000 * ____objectIdentity_5;
	Il2CppIUnknown* ____objTP_6;
	Il2CppIUnknown* ____stubData_7;
};
// Native definition for COM marshalling of System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t298428346_marshaled_com
{
	Type_t * ___class_to_proxy_0;
	Context_t502196753_marshaled_com* ____targetContext_1;
	MarshalByRefObject_t1285298191_marshaled_com* ____server_2;
	int32_t ____targetDomainId_3;
	Il2CppChar* ____targetUri_4;
	Identity_t3647548000 * ____objectIdentity_5;
	Il2CppIUnknown* ____objTP_6;
	Il2CppIUnknown* ____stubData_7;
};
