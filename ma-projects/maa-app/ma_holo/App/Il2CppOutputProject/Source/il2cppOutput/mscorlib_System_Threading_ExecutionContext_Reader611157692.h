﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ExecutionContext/Reader
struct  Reader_t611157692 
{
public:
	// System.Threading.ExecutionContext System.Threading.ExecutionContext/Reader::m_ec
	ExecutionContext_t1392266323 * ___m_ec_0;

public:
	inline static int32_t get_offset_of_m_ec_0() { return static_cast<int32_t>(offsetof(Reader_t611157692, ___m_ec_0)); }
	inline ExecutionContext_t1392266323 * get_m_ec_0() const { return ___m_ec_0; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_ec_0() { return &___m_ec_0; }
	inline void set_m_ec_0(ExecutionContext_t1392266323 * value)
	{
		___m_ec_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_ec_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.ExecutionContext/Reader
struct Reader_t611157692_marshaled_pinvoke
{
	ExecutionContext_t1392266323 * ___m_ec_0;
};
// Native definition for COM marshalling of System.Threading.ExecutionContext/Reader
struct Reader_t611157692_marshaled_com
{
	ExecutionContext_t1392266323 * ___m_ec_0;
};
