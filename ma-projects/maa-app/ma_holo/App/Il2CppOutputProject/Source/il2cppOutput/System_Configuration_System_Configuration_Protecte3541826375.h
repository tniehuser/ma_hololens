﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur2600766927.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ProtectedConfigurationProviderCollection
struct ProtectedConfigurationProviderCollection_t388338823;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationSection
struct  ProtectedConfigurationSection_t3541826375  : public ConfigurationSection_t2600766927
{
public:
	// System.Configuration.ProtectedConfigurationProviderCollection System.Configuration.ProtectedConfigurationSection::providers
	ProtectedConfigurationProviderCollection_t388338823 * ___providers_22;

public:
	inline static int32_t get_offset_of_providers_22() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375, ___providers_22)); }
	inline ProtectedConfigurationProviderCollection_t388338823 * get_providers_22() const { return ___providers_22; }
	inline ProtectedConfigurationProviderCollection_t388338823 ** get_address_of_providers_22() { return &___providers_22; }
	inline void set_providers_22(ProtectedConfigurationProviderCollection_t388338823 * value)
	{
		___providers_22 = value;
		Il2CppCodeGenWriteBarrier(&___providers_22, value);
	}
};

struct ProtectedConfigurationSection_t3541826375_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::defaultProviderProp
	ConfigurationProperty_t2048066811 * ___defaultProviderProp_19;
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::providersProp
	ConfigurationProperty_t2048066811 * ___providersProp_20;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedConfigurationSection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_21;

public:
	inline static int32_t get_offset_of_defaultProviderProp_19() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___defaultProviderProp_19)); }
	inline ConfigurationProperty_t2048066811 * get_defaultProviderProp_19() const { return ___defaultProviderProp_19; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_defaultProviderProp_19() { return &___defaultProviderProp_19; }
	inline void set_defaultProviderProp_19(ConfigurationProperty_t2048066811 * value)
	{
		___defaultProviderProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___defaultProviderProp_19, value);
	}

	inline static int32_t get_offset_of_providersProp_20() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___providersProp_20)); }
	inline ConfigurationProperty_t2048066811 * get_providersProp_20() const { return ___providersProp_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_providersProp_20() { return &___providersProp_20; }
	inline void set_providersProp_20(ConfigurationProperty_t2048066811 * value)
	{
		___providersProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___providersProp_20, value);
	}

	inline static int32_t get_offset_of_properties_21() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___properties_21)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_21() const { return ___properties_21; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_21() { return &___properties_21; }
	inline void set_properties_21(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_21 = value;
		Il2CppCodeGenWriteBarrier(&___properties_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
