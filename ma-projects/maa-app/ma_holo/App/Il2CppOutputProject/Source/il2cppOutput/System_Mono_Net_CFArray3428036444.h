﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Mono_Net_CFObject3730145744.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFArray
struct  CFArray_t3428036444  : public CFObject_t3730145744
{
public:

public:
};

struct CFArray_t3428036444_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFArray::kCFTypeArrayCallbacks
	IntPtr_t ___kCFTypeArrayCallbacks_1;

public:
	inline static int32_t get_offset_of_kCFTypeArrayCallbacks_1() { return static_cast<int32_t>(offsetof(CFArray_t3428036444_StaticFields, ___kCFTypeArrayCallbacks_1)); }
	inline IntPtr_t get_kCFTypeArrayCallbacks_1() const { return ___kCFTypeArrayCallbacks_1; }
	inline IntPtr_t* get_address_of_kCFTypeArrayCallbacks_1() { return &___kCFTypeArrayCallbacks_1; }
	inline void set_kCFTypeArrayCallbacks_1(IntPtr_t value)
	{
		___kCFTypeArrayCallbacks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
