﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"

// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback/CallbackContext
struct  CallbackContext_t3155372874  : public Il2CppObject
{
public:
	// System.Object System.Net.ServerCertValidationCallback/CallbackContext::request
	Il2CppObject * ___request_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServerCertValidationCallback/CallbackContext::certificate
	X509Certificate_t283079845 * ___certificate_1;
	// System.Security.Cryptography.X509Certificates.X509Chain System.Net.ServerCertValidationCallback/CallbackContext::chain
	X509Chain_t777637347 * ___chain_2;
	// System.Net.Security.SslPolicyErrors System.Net.ServerCertValidationCallback/CallbackContext::sslPolicyErrors
	int32_t ___sslPolicyErrors_3;
	// System.Boolean System.Net.ServerCertValidationCallback/CallbackContext::result
	bool ___result_4;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___request_0)); }
	inline Il2CppObject * get_request_0() const { return ___request_0; }
	inline Il2CppObject ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(Il2CppObject * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_0, value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___certificate_1)); }
	inline X509Certificate_t283079845 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate_t283079845 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate_t283079845 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier(&___certificate_1, value);
	}

	inline static int32_t get_offset_of_chain_2() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___chain_2)); }
	inline X509Chain_t777637347 * get_chain_2() const { return ___chain_2; }
	inline X509Chain_t777637347 ** get_address_of_chain_2() { return &___chain_2; }
	inline void set_chain_2(X509Chain_t777637347 * value)
	{
		___chain_2 = value;
		Il2CppCodeGenWriteBarrier(&___chain_2, value);
	}

	inline static int32_t get_offset_of_sslPolicyErrors_3() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___sslPolicyErrors_3)); }
	inline int32_t get_sslPolicyErrors_3() const { return ___sslPolicyErrors_3; }
	inline int32_t* get_address_of_sslPolicyErrors_3() { return &___sslPolicyErrors_3; }
	inline void set_sslPolicyErrors_3(int32_t value)
	{
		___sslPolicyErrors_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(CallbackContext_t3155372874, ___result_4)); }
	inline bool get_result_4() const { return ___result_4; }
	inline bool* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(bool value)
	{
		___result_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
