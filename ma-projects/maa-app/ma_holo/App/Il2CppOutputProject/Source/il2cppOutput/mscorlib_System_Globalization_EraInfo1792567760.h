﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.EraInfo
struct  EraInfo_t1792567760  : public Il2CppObject
{
public:
	// System.Int32 System.Globalization.EraInfo::era
	int32_t ___era_0;
	// System.Int64 System.Globalization.EraInfo::ticks
	int64_t ___ticks_1;
	// System.Int32 System.Globalization.EraInfo::yearOffset
	int32_t ___yearOffset_2;
	// System.Int32 System.Globalization.EraInfo::minEraYear
	int32_t ___minEraYear_3;
	// System.Int32 System.Globalization.EraInfo::maxEraYear
	int32_t ___maxEraYear_4;
	// System.String System.Globalization.EraInfo::eraName
	String_t* ___eraName_5;
	// System.String System.Globalization.EraInfo::abbrevEraName
	String_t* ___abbrevEraName_6;
	// System.String System.Globalization.EraInfo::englishEraName
	String_t* ___englishEraName_7;

public:
	inline static int32_t get_offset_of_era_0() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___era_0)); }
	inline int32_t get_era_0() const { return ___era_0; }
	inline int32_t* get_address_of_era_0() { return &___era_0; }
	inline void set_era_0(int32_t value)
	{
		___era_0 = value;
	}

	inline static int32_t get_offset_of_ticks_1() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___ticks_1)); }
	inline int64_t get_ticks_1() const { return ___ticks_1; }
	inline int64_t* get_address_of_ticks_1() { return &___ticks_1; }
	inline void set_ticks_1(int64_t value)
	{
		___ticks_1 = value;
	}

	inline static int32_t get_offset_of_yearOffset_2() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___yearOffset_2)); }
	inline int32_t get_yearOffset_2() const { return ___yearOffset_2; }
	inline int32_t* get_address_of_yearOffset_2() { return &___yearOffset_2; }
	inline void set_yearOffset_2(int32_t value)
	{
		___yearOffset_2 = value;
	}

	inline static int32_t get_offset_of_minEraYear_3() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___minEraYear_3)); }
	inline int32_t get_minEraYear_3() const { return ___minEraYear_3; }
	inline int32_t* get_address_of_minEraYear_3() { return &___minEraYear_3; }
	inline void set_minEraYear_3(int32_t value)
	{
		___minEraYear_3 = value;
	}

	inline static int32_t get_offset_of_maxEraYear_4() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___maxEraYear_4)); }
	inline int32_t get_maxEraYear_4() const { return ___maxEraYear_4; }
	inline int32_t* get_address_of_maxEraYear_4() { return &___maxEraYear_4; }
	inline void set_maxEraYear_4(int32_t value)
	{
		___maxEraYear_4 = value;
	}

	inline static int32_t get_offset_of_eraName_5() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___eraName_5)); }
	inline String_t* get_eraName_5() const { return ___eraName_5; }
	inline String_t** get_address_of_eraName_5() { return &___eraName_5; }
	inline void set_eraName_5(String_t* value)
	{
		___eraName_5 = value;
		Il2CppCodeGenWriteBarrier(&___eraName_5, value);
	}

	inline static int32_t get_offset_of_abbrevEraName_6() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___abbrevEraName_6)); }
	inline String_t* get_abbrevEraName_6() const { return ___abbrevEraName_6; }
	inline String_t** get_address_of_abbrevEraName_6() { return &___abbrevEraName_6; }
	inline void set_abbrevEraName_6(String_t* value)
	{
		___abbrevEraName_6 = value;
		Il2CppCodeGenWriteBarrier(&___abbrevEraName_6, value);
	}

	inline static int32_t get_offset_of_englishEraName_7() { return static_cast<int32_t>(offsetof(EraInfo_t1792567760, ___englishEraName_7)); }
	inline String_t* get_englishEraName_7() const { return ___englishEraName_7; }
	inline String_t** get_address_of_englishEraName_7() { return &___englishEraName_7; }
	inline void set_englishEraName_7(String_t* value)
	{
		___englishEraName_7 = value;
		Il2CppCodeGenWriteBarrier(&___englishEraName_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
