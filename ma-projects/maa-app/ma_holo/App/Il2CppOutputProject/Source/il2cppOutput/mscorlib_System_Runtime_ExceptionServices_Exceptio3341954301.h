﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ExceptionServices.ExceptionDispatchInfo
struct  ExceptionDispatchInfo_t3341954301  : public Il2CppObject
{
public:
	// System.Exception System.Runtime.ExceptionServices.ExceptionDispatchInfo::m_Exception
	Exception_t1927440687 * ___m_Exception_0;
	// System.Object System.Runtime.ExceptionServices.ExceptionDispatchInfo::m_stackTrace
	Il2CppObject * ___m_stackTrace_1;

public:
	inline static int32_t get_offset_of_m_Exception_0() { return static_cast<int32_t>(offsetof(ExceptionDispatchInfo_t3341954301, ___m_Exception_0)); }
	inline Exception_t1927440687 * get_m_Exception_0() const { return ___m_Exception_0; }
	inline Exception_t1927440687 ** get_address_of_m_Exception_0() { return &___m_Exception_0; }
	inline void set_m_Exception_0(Exception_t1927440687 * value)
	{
		___m_Exception_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Exception_0, value);
	}

	inline static int32_t get_offset_of_m_stackTrace_1() { return static_cast<int32_t>(offsetof(ExceptionDispatchInfo_t3341954301, ___m_stackTrace_1)); }
	inline Il2CppObject * get_m_stackTrace_1() const { return ___m_stackTrace_1; }
	inline Il2CppObject ** get_address_of_m_stackTrace_1() { return &___m_stackTrace_1; }
	inline void set_m_stackTrace_1(Il2CppObject * value)
	{
		___m_stackTrace_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_stackTrace_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
