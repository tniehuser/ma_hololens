﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.SwitchElement
struct  SwitchElement_t4125397718  : public ConfigurationElement_t1776195828
{
public:
	// System.Collections.Hashtable System.Diagnostics.SwitchElement::_attributes
	Hashtable_t909839986 * ____attributes_18;

public:
	inline static int32_t get_offset_of__attributes_18() { return static_cast<int32_t>(offsetof(SwitchElement_t4125397718, ____attributes_18)); }
	inline Hashtable_t909839986 * get__attributes_18() const { return ____attributes_18; }
	inline Hashtable_t909839986 ** get_address_of__attributes_18() { return &____attributes_18; }
	inline void set__attributes_18(Hashtable_t909839986 * value)
	{
		____attributes_18 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_18, value);
	}
};

struct SwitchElement_t4125397718_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.SwitchElement::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SwitchElement::_propName
	ConfigurationProperty_t2048066811 * ____propName_16;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SwitchElement::_propValue
	ConfigurationProperty_t2048066811 * ____propValue_17;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(SwitchElement_t4125397718_StaticFields, ____properties_15)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}

	inline static int32_t get_offset_of__propName_16() { return static_cast<int32_t>(offsetof(SwitchElement_t4125397718_StaticFields, ____propName_16)); }
	inline ConfigurationProperty_t2048066811 * get__propName_16() const { return ____propName_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propName_16() { return &____propName_16; }
	inline void set__propName_16(ConfigurationProperty_t2048066811 * value)
	{
		____propName_16 = value;
		Il2CppCodeGenWriteBarrier(&____propName_16, value);
	}

	inline static int32_t get_offset_of__propValue_17() { return static_cast<int32_t>(offsetof(SwitchElement_t4125397718_StaticFields, ____propValue_17)); }
	inline ConfigurationProperty_t2048066811 * get__propValue_17() const { return ____propValue_17; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propValue_17() { return &____propValue_17; }
	inline void set__propValue_17(ConfigurationProperty_t2048066811 * value)
	{
		____propValue_17 = value;
		Il2CppCodeGenWriteBarrier(&____propValue_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
