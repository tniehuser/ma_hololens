﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Runtime_ConstrainedExecution_Criti1920899984.h"

// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1152954092;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_t2240136856;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t842524053;
// System.Threading.InternalThread
struct InternalThread_t4118012807;
// System.Object
struct Il2CppObject;
// System.Security.Principal.IPrincipal
struct IPrincipal_t783141777;
// System.Threading.Thread
struct Thread_t241561612;
// System.MulticastDelegate
struct MulticastDelegate_t3201952435;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Globalization.CultureInfo>>
struct Action_1_t1793350945;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t241561612  : public CriticalFinalizerObject_t1920899984
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_t4118012807 * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	Il2CppObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	Il2CppObject * ___pending_exception_8;
	// System.Int32 System.Threading.Thread::priority
	int32_t ___priority_9;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	Il2CppObject * ___principal_10;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_11;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t3201952435 * ___m_Delegate_13;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t1392266323 * ___m_ExecutionContext_14;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_15;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___internal_thread_6)); }
	inline InternalThread_t4118012807 * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_t4118012807 ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_t4118012807 * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier(&___internal_thread_6, value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___m_ThreadStartArg_7)); }
	inline Il2CppObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline Il2CppObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(Il2CppObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_ThreadStartArg_7, value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___pending_exception_8)); }
	inline Il2CppObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline Il2CppObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(Il2CppObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier(&___pending_exception_8, value);
	}

	inline static int32_t get_offset_of_priority_9() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___priority_9)); }
	inline int32_t get_priority_9() const { return ___priority_9; }
	inline int32_t* get_address_of_priority_9() { return &___priority_9; }
	inline void set_priority_9(int32_t value)
	{
		___priority_9 = value;
	}

	inline static int32_t get_offset_of_principal_10() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___principal_10)); }
	inline Il2CppObject * get_principal_10() const { return ___principal_10; }
	inline Il2CppObject ** get_address_of_principal_10() { return &___principal_10; }
	inline void set_principal_10(Il2CppObject * value)
	{
		___principal_10 = value;
		Il2CppCodeGenWriteBarrier(&___principal_10, value);
	}

	inline static int32_t get_offset_of_principal_version_11() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___principal_version_11)); }
	inline int32_t get_principal_version_11() const { return ___principal_version_11; }
	inline int32_t* get_address_of_principal_version_11() { return &___principal_version_11; }
	inline void set_principal_version_11(int32_t value)
	{
		___principal_version_11 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_13() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___m_Delegate_13)); }
	inline MulticastDelegate_t3201952435 * get_m_Delegate_13() const { return ___m_Delegate_13; }
	inline MulticastDelegate_t3201952435 ** get_address_of_m_Delegate_13() { return &___m_Delegate_13; }
	inline void set_m_Delegate_13(MulticastDelegate_t3201952435 * value)
	{
		___m_Delegate_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Delegate_13, value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_14() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___m_ExecutionContext_14)); }
	inline ExecutionContext_t1392266323 * get_m_ExecutionContext_14() const { return ___m_ExecutionContext_14; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_ExecutionContext_14() { return &___m_ExecutionContext_14; }
	inline void set_m_ExecutionContext_14(ExecutionContext_t1392266323 * value)
	{
		___m_ExecutionContext_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_ExecutionContext_14, value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_15() { return static_cast<int32_t>(offsetof(Thread_t241561612, ___m_ExecutionContextBelongsToOuterScope_15)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_15() const { return ___m_ExecutionContextBelongsToOuterScope_15; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_15() { return &___m_ExecutionContextBelongsToOuterScope_15; }
	inline void set_m_ExecutionContextBelongsToOuterScope_15(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_15 = value;
	}
};

struct Thread_t241561612_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1152954092 * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t842524053 * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t842524053 * ___s_asyncLocalCurrentUICulture_5;
	// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Globalization.CultureInfo>> System.Threading.Thread::<>f__mg$cache0
	Action_1_t1793350945 * ___U3CU3Ef__mgU24cache0_16;
	// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Globalization.CultureInfo>> System.Threading.Thread::<>f__mg$cache1
	Action_1_t1793350945 * ___U3CU3Ef__mgU24cache1_17;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_t241561612_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t1152954092 * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t1152954092 ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t1152954092 * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_LocalDataStoreMgr_0, value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_t241561612_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_t842524053 * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_t842524053 ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_t842524053 * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_asyncLocalCurrentCulture_4, value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_t241561612_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_t842524053 * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_t842524053 ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_t842524053 * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_asyncLocalCurrentUICulture_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_16() { return static_cast<int32_t>(offsetof(Thread_t241561612_StaticFields, ___U3CU3Ef__mgU24cache0_16)); }
	inline Action_1_t1793350945 * get_U3CU3Ef__mgU24cache0_16() const { return ___U3CU3Ef__mgU24cache0_16; }
	inline Action_1_t1793350945 ** get_address_of_U3CU3Ef__mgU24cache0_16() { return &___U3CU3Ef__mgU24cache0_16; }
	inline void set_U3CU3Ef__mgU24cache0_16(Action_1_t1793350945 * value)
	{
		___U3CU3Ef__mgU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_17() { return static_cast<int32_t>(offsetof(Thread_t241561612_StaticFields, ___U3CU3Ef__mgU24cache1_17)); }
	inline Action_1_t1793350945 * get_U3CU3Ef__mgU24cache1_17() const { return ___U3CU3Ef__mgU24cache1_17; }
	inline Action_1_t1793350945 ** get_address_of_U3CU3Ef__mgU24cache1_17() { return &___U3CU3Ef__mgU24cache1_17; }
	inline void set_U3CU3Ef__mgU24cache1_17(Action_1_t1793350945 * value)
	{
		___U3CU3Ef__mgU24cache1_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_17, value);
	}
};

struct Thread_t241561612_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_t2240136856 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t3500843524 * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t3500843524 * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_t241561612 * ___current_thread_12;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_t241561612_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_t2240136856 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_t2240136856 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_t2240136856 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_LocalDataStore_1, value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_t241561612_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t3500843524 * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t3500843524 ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t3500843524 * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentCulture_2, value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_t241561612_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t3500843524 * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t3500843524 ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t3500843524 * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentUICulture_3, value);
	}

	inline static int32_t get_offset_of_current_thread_12() { return static_cast<int32_t>(offsetof(Thread_t241561612_ThreadStaticFields, ___current_thread_12)); }
	inline Thread_t241561612 * get_current_thread_12() const { return ___current_thread_12; }
	inline Thread_t241561612 ** get_address_of_current_thread_12() { return &___current_thread_12; }
	inline void set_current_thread_12(Thread_t241561612 * value)
	{
		___current_thread_12 = value;
		Il2CppCodeGenWriteBarrier(&___current_thread_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
