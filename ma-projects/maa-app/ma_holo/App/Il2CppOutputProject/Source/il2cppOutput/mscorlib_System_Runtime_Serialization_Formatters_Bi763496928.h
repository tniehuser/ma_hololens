﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObject
struct  BinaryObject_t763496928  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObject::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObject::mapId
	int32_t ___mapId_1;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryObject_t763496928, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_mapId_1() { return static_cast<int32_t>(offsetof(BinaryObject_t763496928, ___mapId_1)); }
	inline int32_t get_mapId_1() const { return ___mapId_1; }
	inline int32_t* get_address_of_mapId_1() { return &___mapId_1; }
	inline void set_mapId_1(int32_t value)
	{
		___mapId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
