﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"

// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_t2256991674;
// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_t1753655453;
// System.Xml.Schema.Positions
struct Positions_t3593914952;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NfaContentValidator
struct  NfaContentValidator_t1842871216  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.NfaContentValidator::firstpos
	BitSet_t1062448123 * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.NfaContentValidator::followpos
	BitSetU5BU5D_t2256991674* ___followpos_8;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.NfaContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_9;
	// System.Xml.Schema.Positions System.Xml.Schema.NfaContentValidator::positions
	Positions_t3593914952 * ___positions_10;
	// System.Int32 System.Xml.Schema.NfaContentValidator::endMarkerPos
	int32_t ___endMarkerPos_11;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___firstpos_7)); }
	inline BitSet_t1062448123 * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t1062448123 * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier(&___firstpos_7, value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___followpos_8)); }
	inline BitSetU5BU5D_t2256991674* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_t2256991674** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_t2256991674* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier(&___followpos_8, value);
	}

	inline static int32_t get_offset_of_symbols_9() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___symbols_9)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_9() const { return ___symbols_9; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_9() { return &___symbols_9; }
	inline void set_symbols_9(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_9 = value;
		Il2CppCodeGenWriteBarrier(&___symbols_9, value);
	}

	inline static int32_t get_offset_of_positions_10() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___positions_10)); }
	inline Positions_t3593914952 * get_positions_10() const { return ___positions_10; }
	inline Positions_t3593914952 ** get_address_of_positions_10() { return &___positions_10; }
	inline void set_positions_10(Positions_t3593914952 * value)
	{
		___positions_10 = value;
		Il2CppCodeGenWriteBarrier(&___positions_10, value);
	}

	inline static int32_t get_offset_of_endMarkerPos_11() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___endMarkerPos_11)); }
	inline int32_t get_endMarkerPos_11() const { return ___endMarkerPos_11; }
	inline int32_t* get_address_of_endMarkerPos_11() { return &___endMarkerPos_11; }
	inline void set_endMarkerPos_11(int32_t value)
	{
		___endMarkerPos_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
