﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_X509Certific3842064707.h"

// Mono.Security.X509.X509Certificate
struct X509Certificate_t324051957;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateImplMono
struct  X509CertificateImplMono_t2148381192  : public X509CertificateImpl_t3842064707
{
public:
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509CertificateImplMono::x509
	X509Certificate_t324051957 * ___x509_1;

public:
	inline static int32_t get_offset_of_x509_1() { return static_cast<int32_t>(offsetof(X509CertificateImplMono_t2148381192, ___x509_1)); }
	inline X509Certificate_t324051957 * get_x509_1() const { return ___x509_1; }
	inline X509Certificate_t324051957 ** get_address_of_x509_1() { return &___x509_1; }
	inline void set_x509_1(X509Certificate_t324051957 * value)
	{
		___x509_1 = value;
		Il2CppCodeGenWriteBarrier(&___x509_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
