﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"

// System.Collections.Queue
struct Queue_t1288490777;
// System.Runtime.Serialization.ObjectIDGenerator
struct ObjectIDGenerator_t3070747799;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1912587528;
// System.Runtime.Serialization.Formatters.Binary.__BinaryWriter
struct __BinaryWriter_t2912249390;
// System.Runtime.Serialization.SerializationObjectManager
struct SerializationObjectManager_t4052555190;
// System.String
struct String_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2408360458;
// System.Runtime.Serialization.Formatters.Binary.InternalFE
struct InternalFE_t3355145566;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t3985864818;
// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit
struct SerObjectInfoInit_t4094458531;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1473156697;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Serialization.Formatters.Binary.SerStack
struct SerStack_t3886188184;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectWriter
struct  ObjectWriter_t4293742132  : public Il2CppObject
{
public:
	// System.Collections.Queue System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_objectQueue
	Queue_t1288490777 * ___m_objectQueue_0;
	// System.Runtime.Serialization.ObjectIDGenerator System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_idGenerator
	ObjectIDGenerator_t3070747799 * ___m_idGenerator_1;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_currentId
	int32_t ___m_currentId_2;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_surrogates
	Il2CppObject * ___m_surrogates_3;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_context
	StreamingContext_t1417235061  ___m_context_4;
	// System.Runtime.Serialization.Formatters.Binary.__BinaryWriter System.Runtime.Serialization.Formatters.Binary.ObjectWriter::serWriter
	__BinaryWriter_t2912249390 * ___serWriter_5;
	// System.Runtime.Serialization.SerializationObjectManager System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_objectManager
	SerializationObjectManager_t4052555190 * ___m_objectManager_6;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::topId
	int64_t ___topId_7;
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectWriter::topName
	String_t* ___topName_8;
	// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Serialization.Formatters.Binary.ObjectWriter::headers
	HeaderU5BU5D_t2408360458* ___headers_9;
	// System.Runtime.Serialization.Formatters.Binary.InternalFE System.Runtime.Serialization.Formatters.Binary.ObjectWriter::formatterEnums
	InternalFE_t3355145566 * ___formatterEnums_10;
	// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_binder
	SerializationBinder_t3985864818 * ___m_binder_11;
	// System.Runtime.Serialization.Formatters.Binary.SerObjectInfoInit System.Runtime.Serialization.Formatters.Binary.ObjectWriter::serObjectInfoInit
	SerObjectInfoInit_t4094458531 * ___serObjectInfoInit_12;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.Formatters.Binary.ObjectWriter::m_formatterConverter
	Il2CppObject * ___m_formatterConverter_13;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ObjectWriter::crossAppDomainArray
	ObjectU5BU5D_t3614634134* ___crossAppDomainArray_14;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectWriter::previousObj
	Il2CppObject * ___previousObj_15;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::previousId
	int64_t ___previousId_16;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectWriter::previousType
	Type_t * ___previousType_17;
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.ObjectWriter::previousCode
	int32_t ___previousCode_18;
	// System.Collections.Hashtable System.Runtime.Serialization.Formatters.Binary.ObjectWriter::assemblyToIdTable
	Hashtable_t909839986 * ___assemblyToIdTable_19;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.ObjectWriter::niPool
	SerStack_t3886188184 * ___niPool_20;

public:
	inline static int32_t get_offset_of_m_objectQueue_0() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_objectQueue_0)); }
	inline Queue_t1288490777 * get_m_objectQueue_0() const { return ___m_objectQueue_0; }
	inline Queue_t1288490777 ** get_address_of_m_objectQueue_0() { return &___m_objectQueue_0; }
	inline void set_m_objectQueue_0(Queue_t1288490777 * value)
	{
		___m_objectQueue_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_objectQueue_0, value);
	}

	inline static int32_t get_offset_of_m_idGenerator_1() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_idGenerator_1)); }
	inline ObjectIDGenerator_t3070747799 * get_m_idGenerator_1() const { return ___m_idGenerator_1; }
	inline ObjectIDGenerator_t3070747799 ** get_address_of_m_idGenerator_1() { return &___m_idGenerator_1; }
	inline void set_m_idGenerator_1(ObjectIDGenerator_t3070747799 * value)
	{
		___m_idGenerator_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_idGenerator_1, value);
	}

	inline static int32_t get_offset_of_m_currentId_2() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_currentId_2)); }
	inline int32_t get_m_currentId_2() const { return ___m_currentId_2; }
	inline int32_t* get_address_of_m_currentId_2() { return &___m_currentId_2; }
	inline void set_m_currentId_2(int32_t value)
	{
		___m_currentId_2 = value;
	}

	inline static int32_t get_offset_of_m_surrogates_3() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_surrogates_3)); }
	inline Il2CppObject * get_m_surrogates_3() const { return ___m_surrogates_3; }
	inline Il2CppObject ** get_address_of_m_surrogates_3() { return &___m_surrogates_3; }
	inline void set_m_surrogates_3(Il2CppObject * value)
	{
		___m_surrogates_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_surrogates_3, value);
	}

	inline static int32_t get_offset_of_m_context_4() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_context_4)); }
	inline StreamingContext_t1417235061  get_m_context_4() const { return ___m_context_4; }
	inline StreamingContext_t1417235061 * get_address_of_m_context_4() { return &___m_context_4; }
	inline void set_m_context_4(StreamingContext_t1417235061  value)
	{
		___m_context_4 = value;
	}

	inline static int32_t get_offset_of_serWriter_5() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___serWriter_5)); }
	inline __BinaryWriter_t2912249390 * get_serWriter_5() const { return ___serWriter_5; }
	inline __BinaryWriter_t2912249390 ** get_address_of_serWriter_5() { return &___serWriter_5; }
	inline void set_serWriter_5(__BinaryWriter_t2912249390 * value)
	{
		___serWriter_5 = value;
		Il2CppCodeGenWriteBarrier(&___serWriter_5, value);
	}

	inline static int32_t get_offset_of_m_objectManager_6() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_objectManager_6)); }
	inline SerializationObjectManager_t4052555190 * get_m_objectManager_6() const { return ___m_objectManager_6; }
	inline SerializationObjectManager_t4052555190 ** get_address_of_m_objectManager_6() { return &___m_objectManager_6; }
	inline void set_m_objectManager_6(SerializationObjectManager_t4052555190 * value)
	{
		___m_objectManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_objectManager_6, value);
	}

	inline static int32_t get_offset_of_topId_7() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___topId_7)); }
	inline int64_t get_topId_7() const { return ___topId_7; }
	inline int64_t* get_address_of_topId_7() { return &___topId_7; }
	inline void set_topId_7(int64_t value)
	{
		___topId_7 = value;
	}

	inline static int32_t get_offset_of_topName_8() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___topName_8)); }
	inline String_t* get_topName_8() const { return ___topName_8; }
	inline String_t** get_address_of_topName_8() { return &___topName_8; }
	inline void set_topName_8(String_t* value)
	{
		___topName_8 = value;
		Il2CppCodeGenWriteBarrier(&___topName_8, value);
	}

	inline static int32_t get_offset_of_headers_9() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___headers_9)); }
	inline HeaderU5BU5D_t2408360458* get_headers_9() const { return ___headers_9; }
	inline HeaderU5BU5D_t2408360458** get_address_of_headers_9() { return &___headers_9; }
	inline void set_headers_9(HeaderU5BU5D_t2408360458* value)
	{
		___headers_9 = value;
		Il2CppCodeGenWriteBarrier(&___headers_9, value);
	}

	inline static int32_t get_offset_of_formatterEnums_10() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___formatterEnums_10)); }
	inline InternalFE_t3355145566 * get_formatterEnums_10() const { return ___formatterEnums_10; }
	inline InternalFE_t3355145566 ** get_address_of_formatterEnums_10() { return &___formatterEnums_10; }
	inline void set_formatterEnums_10(InternalFE_t3355145566 * value)
	{
		___formatterEnums_10 = value;
		Il2CppCodeGenWriteBarrier(&___formatterEnums_10, value);
	}

	inline static int32_t get_offset_of_m_binder_11() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_binder_11)); }
	inline SerializationBinder_t3985864818 * get_m_binder_11() const { return ___m_binder_11; }
	inline SerializationBinder_t3985864818 ** get_address_of_m_binder_11() { return &___m_binder_11; }
	inline void set_m_binder_11(SerializationBinder_t3985864818 * value)
	{
		___m_binder_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_binder_11, value);
	}

	inline static int32_t get_offset_of_serObjectInfoInit_12() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___serObjectInfoInit_12)); }
	inline SerObjectInfoInit_t4094458531 * get_serObjectInfoInit_12() const { return ___serObjectInfoInit_12; }
	inline SerObjectInfoInit_t4094458531 ** get_address_of_serObjectInfoInit_12() { return &___serObjectInfoInit_12; }
	inline void set_serObjectInfoInit_12(SerObjectInfoInit_t4094458531 * value)
	{
		___serObjectInfoInit_12 = value;
		Il2CppCodeGenWriteBarrier(&___serObjectInfoInit_12, value);
	}

	inline static int32_t get_offset_of_m_formatterConverter_13() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___m_formatterConverter_13)); }
	inline Il2CppObject * get_m_formatterConverter_13() const { return ___m_formatterConverter_13; }
	inline Il2CppObject ** get_address_of_m_formatterConverter_13() { return &___m_formatterConverter_13; }
	inline void set_m_formatterConverter_13(Il2CppObject * value)
	{
		___m_formatterConverter_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_formatterConverter_13, value);
	}

	inline static int32_t get_offset_of_crossAppDomainArray_14() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___crossAppDomainArray_14)); }
	inline ObjectU5BU5D_t3614634134* get_crossAppDomainArray_14() const { return ___crossAppDomainArray_14; }
	inline ObjectU5BU5D_t3614634134** get_address_of_crossAppDomainArray_14() { return &___crossAppDomainArray_14; }
	inline void set_crossAppDomainArray_14(ObjectU5BU5D_t3614634134* value)
	{
		___crossAppDomainArray_14 = value;
		Il2CppCodeGenWriteBarrier(&___crossAppDomainArray_14, value);
	}

	inline static int32_t get_offset_of_previousObj_15() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___previousObj_15)); }
	inline Il2CppObject * get_previousObj_15() const { return ___previousObj_15; }
	inline Il2CppObject ** get_address_of_previousObj_15() { return &___previousObj_15; }
	inline void set_previousObj_15(Il2CppObject * value)
	{
		___previousObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___previousObj_15, value);
	}

	inline static int32_t get_offset_of_previousId_16() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___previousId_16)); }
	inline int64_t get_previousId_16() const { return ___previousId_16; }
	inline int64_t* get_address_of_previousId_16() { return &___previousId_16; }
	inline void set_previousId_16(int64_t value)
	{
		___previousId_16 = value;
	}

	inline static int32_t get_offset_of_previousType_17() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___previousType_17)); }
	inline Type_t * get_previousType_17() const { return ___previousType_17; }
	inline Type_t ** get_address_of_previousType_17() { return &___previousType_17; }
	inline void set_previousType_17(Type_t * value)
	{
		___previousType_17 = value;
		Il2CppCodeGenWriteBarrier(&___previousType_17, value);
	}

	inline static int32_t get_offset_of_previousCode_18() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___previousCode_18)); }
	inline int32_t get_previousCode_18() const { return ___previousCode_18; }
	inline int32_t* get_address_of_previousCode_18() { return &___previousCode_18; }
	inline void set_previousCode_18(int32_t value)
	{
		___previousCode_18 = value;
	}

	inline static int32_t get_offset_of_assemblyToIdTable_19() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___assemblyToIdTable_19)); }
	inline Hashtable_t909839986 * get_assemblyToIdTable_19() const { return ___assemblyToIdTable_19; }
	inline Hashtable_t909839986 ** get_address_of_assemblyToIdTable_19() { return &___assemblyToIdTable_19; }
	inline void set_assemblyToIdTable_19(Hashtable_t909839986 * value)
	{
		___assemblyToIdTable_19 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyToIdTable_19, value);
	}

	inline static int32_t get_offset_of_niPool_20() { return static_cast<int32_t>(offsetof(ObjectWriter_t4293742132, ___niPool_20)); }
	inline SerStack_t3886188184 * get_niPool_20() const { return ___niPool_20; }
	inline SerStack_t3886188184 ** get_address_of_niPool_20() { return &___niPool_20; }
	inline void set_niPool_20(SerStack_t3886188184 * value)
	{
		___niPool_20 = value;
		Il2CppCodeGenWriteBarrier(&___niPool_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
