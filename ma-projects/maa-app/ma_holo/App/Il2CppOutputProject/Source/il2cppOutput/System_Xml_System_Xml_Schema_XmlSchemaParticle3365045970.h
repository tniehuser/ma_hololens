﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "mscorlib_System_Decimal724701077.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Occu200023993.h"

// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_t3365045970  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minOccurs
	Decimal_t724701077  ___minOccurs_9;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::maxOccurs
	Decimal_t724701077  ___maxOccurs_10;
	// System.Xml.Schema.XmlSchemaParticle/Occurs System.Xml.Schema.XmlSchemaParticle::flags
	int32_t ___flags_11;

public:
	inline static int32_t get_offset_of_minOccurs_9() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___minOccurs_9)); }
	inline Decimal_t724701077  get_minOccurs_9() const { return ___minOccurs_9; }
	inline Decimal_t724701077 * get_address_of_minOccurs_9() { return &___minOccurs_9; }
	inline void set_minOccurs_9(Decimal_t724701077  value)
	{
		___minOccurs_9 = value;
	}

	inline static int32_t get_offset_of_maxOccurs_10() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___maxOccurs_10)); }
	inline Decimal_t724701077  get_maxOccurs_10() const { return ___maxOccurs_10; }
	inline Decimal_t724701077 * get_address_of_maxOccurs_10() { return &___maxOccurs_10; }
	inline void set_maxOccurs_10(Decimal_t724701077  value)
	{
		___maxOccurs_10 = value;
	}

	inline static int32_t get_offset_of_flags_11() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___flags_11)); }
	inline int32_t get_flags_11() const { return ___flags_11; }
	inline int32_t* get_address_of_flags_11() { return &___flags_11; }
	inline void set_flags_11(int32_t value)
	{
		___flags_11 = value;
	}
};

struct XmlSchemaParticle_t3365045970_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::Empty
	XmlSchemaParticle_t3365045970 * ___Empty_12;

public:
	inline static int32_t get_offset_of_Empty_12() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970_StaticFields, ___Empty_12)); }
	inline XmlSchemaParticle_t3365045970 * get_Empty_12() const { return ___Empty_12; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_Empty_12() { return &___Empty_12; }
	inline void set_Empty_12(XmlSchemaParticle_t3365045970 * value)
	{
		___Empty_12 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
