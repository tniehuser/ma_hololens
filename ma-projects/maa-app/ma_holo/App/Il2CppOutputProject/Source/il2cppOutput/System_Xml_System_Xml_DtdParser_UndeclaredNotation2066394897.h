﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.DtdParser/UndeclaredNotation
struct UndeclaredNotation_t2066394897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/UndeclaredNotation
struct  UndeclaredNotation_t2066394897  : public Il2CppObject
{
public:
	// System.String System.Xml.DtdParser/UndeclaredNotation::name
	String_t* ___name_0;
	// System.Int32 System.Xml.DtdParser/UndeclaredNotation::lineNo
	int32_t ___lineNo_1;
	// System.Int32 System.Xml.DtdParser/UndeclaredNotation::linePos
	int32_t ___linePos_2;
	// System.Xml.DtdParser/UndeclaredNotation System.Xml.DtdParser/UndeclaredNotation::next
	UndeclaredNotation_t2066394897 * ___next_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_lineNo_1() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___lineNo_1)); }
	inline int32_t get_lineNo_1() const { return ___lineNo_1; }
	inline int32_t* get_address_of_lineNo_1() { return &___lineNo_1; }
	inline void set_lineNo_1(int32_t value)
	{
		___lineNo_1 = value;
	}

	inline static int32_t get_offset_of_linePos_2() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___linePos_2)); }
	inline int32_t get_linePos_2() const { return ___linePos_2; }
	inline int32_t* get_address_of_linePos_2() { return &___linePos_2; }
	inline void set_linePos_2(int32_t value)
	{
		___linePos_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___next_3)); }
	inline UndeclaredNotation_t2066394897 * get_next_3() const { return ___next_3; }
	inline UndeclaredNotation_t2066394897 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(UndeclaredNotation_t2066394897 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier(&___next_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
