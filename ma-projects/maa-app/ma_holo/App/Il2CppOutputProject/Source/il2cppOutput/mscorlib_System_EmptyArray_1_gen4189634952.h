﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.SequenceNode/SequenceConstructPosContext[]
struct SequenceConstructPosContextU5BU5D_t2244150431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EmptyArray`1<System.Xml.Schema.SequenceNode/SequenceConstructPosContext>
struct  EmptyArray_1_t4189634952  : public Il2CppObject
{
public:

public:
};

struct EmptyArray_1_t4189634952_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	SequenceConstructPosContextU5BU5D_t2244150431* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_t4189634952_StaticFields, ___Value_0)); }
	inline SequenceConstructPosContextU5BU5D_t2244150431* get_Value_0() const { return ___Value_0; }
	inline SequenceConstructPosContextU5BU5D_t2244150431** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(SequenceConstructPosContextU5BU5D_t2244150431* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
