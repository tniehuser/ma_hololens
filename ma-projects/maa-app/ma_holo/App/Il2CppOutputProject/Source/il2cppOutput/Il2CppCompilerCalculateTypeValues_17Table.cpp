﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Diagnostics_DiagnosticsConfigurationH610492850.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration1009623312.h"
#include "System_System_Diagnostics_EventLog681067562.h"
#include "System_System_Diagnostics_EventLogImpl3456077238.h"
#include "System_System_Diagnostics_EventLogInstaller4025365612.h"
#include "System_System_Diagnostics_EventLogTraceListener2981657285.h"
#include "System_System_Diagnostics_MonitoringDescriptionAtt1660295144.h"
#include "System_System_Diagnostics_TraceImplSettings1186465586.h"
#include "System_System_Diagnostics_TraceSourceInfo8795084.h"
#include "System_System_Platform3160142327.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"
#include "System_System_IO_Compression_DeflateStream3198596725.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet3362229488.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe1894833619.h"
#include "System_System_IO_Compression_DeflateStreamNative76712276.h"
#include "System_System_IO_Compression_DeflateStreamNative_U4047001824.h"
#include "System_System_IO_Compression_GZipStream2274754946.h"
#include "System_System_Net_AuthenticationManager3410876775.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_ChunkStream91719323.h"
#include "System_System_Net_ChunkStream_State4001596355.h"
#include "System_System_Net_ChunkStream_Chunk3860501603.h"
#include "System_System_Net_Configuration_AuthenticationModu1084300802.h"
#include "System_System_Net_Configuration_AuthenticationModu1426459758.h"
#include "System_System_Net_Configuration_AuthenticationModu1750570770.h"
#include "System_System_Net_Configuration_BypassElementColle3411512674.h"
#include "System_System_Net_Configuration_BypassElement4253212366.h"
#include "System_System_Net_Configuration_ConnectionManageme3244012643.h"
#include "System_System_Net_Configuration_ConnectionManageme1734801665.h"
#include "System_System_Net_Configuration_ConnectionManageme2657447783.h"
#include "System_System_Net_Configuration_ConnectionManageme1533889992.h"
#include "System_System_Net_Configuration_DefaultProxySectio2916409848.h"
#include "System_System_Net_Configuration_HttpWebRequestElem2106051069.h"
#include "System_System_Net_Configuration_Ipv6Element977943121.h"
#include "System_System_Net_Configuration_ModuleElement3031348726.h"
#include "System_System_Net_Configuration_NetSectionGroup2546413291.h"
#include "System_System_Net_Configuration_PerformanceCounter1606215027.h"
#include "System_System_Net_Configuration_ProxyElement1414493002.h"
#include "System_System_Net_Configuration_ProxyElement_Bypas3271087145.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy1457033350.h"
#include "System_System_Net_Configuration_ProxyElement_AutoDe735769531.h"
#include "System_System_Net_Configuration_ServicePointManage1132364388.h"
#include "System_System_Net_Configuration_SettingsSection2300716058.h"
#include "System_System_Net_Configuration_SocketElement792962077.h"
#include "System_System_Net_Configuration_WebProxyScriptElem1017943775.h"
#include "System_System_Net_Configuration_WebRequestModuleEl2218695785.h"
#include "System_System_Net_Configuration_WebRequestModuleEl4070853259.h"
#include "System_System_Net_Configuration_WebRequestModulesS3717257007.h"
#include "System_System_Net_DecompressionMethods2530166567.h"
#include "System_System_Net_DefaultCertificatePolicy2545332216.h"
#include "System_System_Net_Dns1335526197.h"
#include "System_System_Net_FtpAsyncResult770082413.h"
#include "System_System_Net_FtpDataStream3588258764.h"
#include "System_System_Net_FtpDataStream_WriteDelegate888270799.h"
#include "System_System_Net_FtpDataStream_ReadDelegate1559754630.h"
#include "System_System_Net_FtpRequestCreator3711983251.h"
#include "System_System_Net_FtpWebRequest3120721823.h"
#include "System_System_Net_FtpWebRequest_RequestState4256633122.h"
#include "System_System_Net_FtpStatus3714482970.h"
#include "System_System_Net_FtpWebResponse2609078769.h"
#include "System_System_Net_HttpRequestCreator1416559589.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_HttpWebRequest_NtlmAuthState3242387638.h"
#include "System_System_Net_HttpWebRequest_AuthorizationStat3879143100.h"
#include "System_System_Net_HttpWebRequest_U3CBeginGetRespon2626749757.h"
#include "System_System_Net_HttpWebResponse2828383075.h"
#include "System_System_Net_IPv6AddressFormatter3116172695.h"
#include "System_Mono_Net_CFObject3730145744.h"
#include "System_Mono_Net_CFArray3428036444.h"
#include "System_Mono_Net_CFNumber2174524538.h"
#include "System_Mono_Net_CFRange2990408436.h"
#include "System_Mono_Net_CFStreamClientContext3245641555.h"
#include "System_Mono_Net_CFString3358817686.h"
#include "System_Mono_Net_CFDictionary3548969133.h"
#include "System_Mono_Net_CFUrl2608898728.h"
#include "System_Mono_Net_CFRunLoop1302886598.h"
#include "System_Mono_Net_CFProxyType2264861409.h"
#include "System_Mono_Net_CFProxy537977545.h"
#include "System_Mono_Net_CFProxySettings4092929580.h"
#include "System_Mono_Net_CFNetwork2805084167.h"
#include "System_Mono_Net_CFNetwork_GetProxyData1386489386.h"
#include "System_Mono_Net_CFNetwork_CFProxyAutoConfiguration3537096558.h"
#include "System_Mono_Net_CFNetwork_CFWebProxy1432115055.h"
#include "System_Mono_Net_CFNetwork_U3CExecuteProxyAutoConfigu35768992.h"
#include "System_System_Net_NetConfig1609737885.h"
#include "System_System_Net_NetworkInformation_Win32IPAddres2117998352.h"
#include "System_System_Net_NetworkInformation_CommonUnixIPGlo83484526.h"
#include "System_System_Net_NetworkInformation_UnixIPGlobalP2823347355.h"
#include "System_System_Net_NetworkInformation_MibIPGlobalPr4104985615.h"
#include "System_System_Net_NetworkInformation_Win32IPGlobal2393043454.h"
#include "System_System_Net_NetworkInformation_UnixIPInterfa2418091637.h"
#include "System_System_Net_NetworkInformation_LinuxIPInterf3318447793.h"
#include "System_System_Net_NetworkInformation_MacOsIPInterf3101377296.h"
#include "System_System_Net_NetworkInformation_Win32IPInterf3641679752.h"
#include "System_System_Net_NetworkInformation_Win32IPv4Inte2652275954.h"
#include "System_System_Net_NetworkInformation_ifa_ifu602722385.h"
#include "System_System_Net_NetworkInformation_ifaddrs2532459533.h"
#include "System_System_Net_NetworkInformation_sockaddr_in1277740973.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (DiagnosticsConfigurationHandler_t610492850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[2] = 
{
	DiagnosticsConfigurationHandler_t610492850::get_offset_of_configValues_0(),
	DiagnosticsConfigurationHandler_t610492850::get_offset_of_elementHandlers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (ElementHandler_t1009623312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (EventLog_t681067562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[3] = 
{
	EventLog_t681067562::get_offset_of_source_4(),
	EventLog_t681067562::get_offset_of_doRaiseEvents_5(),
	EventLog_t681067562::get_offset_of_Impl_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (EventLogImpl_t3456077238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (EventLogInstaller_t4025365612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (EventLogTraceListener_t2981657285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[2] = 
{
	EventLogTraceListener_t2981657285::get_offset_of_event_log_9(),
	EventLogTraceListener_t2981657285::get_offset_of_name_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (MonitoringDescriptionAttribute_t1660295144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (TraceImplSettings_t1186465586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[3] = 
{
	TraceImplSettings_t1186465586::get_offset_of_AutoFlush_0(),
	TraceImplSettings_t1186465586::get_offset_of_IndentSize_1(),
	TraceImplSettings_t1186465586::get_offset_of_Listeners_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (TraceSourceInfo_t8795084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[3] = 
{
	TraceSourceInfo_t8795084::get_offset_of_name_0(),
	TraceSourceInfo_t8795084::get_offset_of_levels_1(),
	TraceSourceInfo_t8795084::get_offset_of_listeners_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (Platform_t3160142327), -1, sizeof(Platform_t3160142327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[3] = 
{
	Platform_t3160142327_StaticFields::get_offset_of_checkedOS_0(),
	Platform_t3160142327_StaticFields::get_offset_of_isMacOS_1(),
	Platform_t3160142327_StaticFields::get_offset_of_isFreeBSD_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (CompressionMode_t1471062003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1710[3] = 
{
	CompressionMode_t1471062003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (DeflateStream_t3198596725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[5] = 
{
	DeflateStream_t3198596725::get_offset_of_base_stream_8(),
	DeflateStream_t3198596725::get_offset_of_mode_9(),
	DeflateStream_t3198596725::get_offset_of_leaveOpen_10(),
	DeflateStream_t3198596725::get_offset_of_disposed_11(),
	DeflateStream_t3198596725::get_offset_of_native_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (ReadMethod_t3362229488), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (WriteMethod_t1894833619), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (DeflateStreamNative_t76712276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[6] = 
{
	DeflateStreamNative_t76712276::get_offset_of_feeder_0(),
	DeflateStreamNative_t76712276::get_offset_of_base_stream_1(),
	DeflateStreamNative_t76712276::get_offset_of_z_stream_2(),
	DeflateStreamNative_t76712276::get_offset_of_data_3(),
	DeflateStreamNative_t76712276::get_offset_of_disposed_4(),
	DeflateStreamNative_t76712276::get_offset_of_io_buffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (UnmanagedReadOrWrite_t4047001824), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (GZipStream_t2274754946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	GZipStream_t2274754946::get_offset_of_deflateStream_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (AuthenticationManager_t3410876775), -1, sizeof(AuthenticationManager_t3410876775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (BindIPEndPoint_t635820671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (ChunkStream_t91719323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[10] = 
{
	ChunkStream_t91719323::get_offset_of_headers_0(),
	ChunkStream_t91719323::get_offset_of_chunkSize_1(),
	ChunkStream_t91719323::get_offset_of_chunkRead_2(),
	ChunkStream_t91719323::get_offset_of_totalWritten_3(),
	ChunkStream_t91719323::get_offset_of_state_4(),
	ChunkStream_t91719323::get_offset_of_saved_5(),
	ChunkStream_t91719323::get_offset_of_sawCR_6(),
	ChunkStream_t91719323::get_offset_of_gotit_7(),
	ChunkStream_t91719323::get_offset_of_trailerState_8(),
	ChunkStream_t91719323::get_offset_of_chunks_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (State_t4001596355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[6] = 
{
	State_t4001596355::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (Chunk_t3860501603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[2] = 
{
	Chunk_t3860501603::get_offset_of_Bytes_0(),
	Chunk_t3860501603::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (AuthenticationModuleElementCollection_t1084300802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (AuthenticationModuleElement_t1426459758), -1, sizeof(AuthenticationModuleElement_t1426459758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_properties_15(),
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_typeProp_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (AuthenticationModulesSection_t1750570770), -1, sizeof(AuthenticationModulesSection_t1750570770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1724[2] = 
{
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_properties_19(),
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_authenticationModulesProp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (BypassElementCollection_t3411512674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (BypassElement_t4253212366), -1, sizeof(BypassElement_t4253212366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[2] = 
{
	BypassElement_t4253212366_StaticFields::get_offset_of_properties_15(),
	BypassElement_t4253212366_StaticFields::get_offset_of_addressProp_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (ConnectionManagementElementCollection_t3244012643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (ConnectionManagementElement_t1734801665), -1, sizeof(ConnectionManagementElement_t1734801665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[3] = 
{
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_properties_15(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_addressProp_16(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_maxConnectionProp_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (ConnectionManagementData_t2657447783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	ConnectionManagementData_t2657447783::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (ConnectionManagementSection_t1533889992), -1, sizeof(ConnectionManagementSection_t1533889992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[2] = 
{
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_connectionManagementProp_19(),
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_properties_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (DefaultProxySection_t2916409848), -1, sizeof(DefaultProxySection_t2916409848_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1731[6] = 
{
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_properties_19(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_bypassListProp_20(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_enabledProp_21(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_moduleProp_22(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_proxyProp_23(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_useDefaultCredentialsProp_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (HttpWebRequestElement_t2106051069), -1, sizeof(HttpWebRequestElement_t2106051069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[5] = 
{
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumErrorResponseLengthProp_15(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumResponseHeadersLengthProp_16(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumUnauthorizedUploadLengthProp_17(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_useUnsafeHeaderParsingProp_18(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_properties_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Ipv6Element_t977943121), -1, sizeof(Ipv6Element_t977943121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[2] = 
{
	Ipv6Element_t977943121_StaticFields::get_offset_of_properties_15(),
	Ipv6Element_t977943121_StaticFields::get_offset_of_enabledProp_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (ModuleElement_t3031348726), -1, sizeof(ModuleElement_t3031348726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1734[2] = 
{
	ModuleElement_t3031348726_StaticFields::get_offset_of_properties_15(),
	ModuleElement_t3031348726_StaticFields::get_offset_of_typeProp_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (NetSectionGroup_t2546413291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (PerformanceCountersElement_t1606215027), -1, sizeof(PerformanceCountersElement_t1606215027_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[2] = 
{
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_enabledProp_15(),
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (ProxyElement_t1414493002), -1, sizeof(ProxyElement_t1414493002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1737[6] = 
{
	ProxyElement_t1414493002_StaticFields::get_offset_of_properties_15(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_autoDetectProp_16(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_bypassOnLocalProp_17(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_proxyAddressProp_18(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_scriptLocationProp_19(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_useSystemDefaultProp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (BypassOnLocalValues_t3271087145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[4] = 
{
	BypassOnLocalValues_t3271087145::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (UseSystemDefaultValues_t1457033350)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1739[4] = 
{
	UseSystemDefaultValues_t1457033350::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (AutoDetectValues_t735769531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1740[4] = 
{
	AutoDetectValues_t735769531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (ServicePointManagerElement_t1132364388), -1, sizeof(ServicePointManagerElement_t1132364388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1741[7] = 
{
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_properties_15(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateNameProp_16(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateRevocationListProp_17(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_dnsRefreshTimeoutProp_18(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_enableDnsRoundRobinProp_19(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_expect100ContinueProp_20(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_useNagleAlgorithmProp_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (SettingsSection_t2300716058), -1, sizeof(SettingsSection_t2300716058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1742[7] = 
{
	SettingsSection_t2300716058_StaticFields::get_offset_of_properties_19(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_httpWebRequestProp_20(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_ipv6Prop_21(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_performanceCountersProp_22(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_servicePointManagerProp_23(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_webProxyScriptProp_24(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_socketProp_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (SocketElement_t792962077), -1, sizeof(SocketElement_t792962077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1743[3] = 
{
	SocketElement_t792962077_StaticFields::get_offset_of_properties_15(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_16(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (WebProxyScriptElement_t1017943775), -1, sizeof(WebProxyScriptElement_t1017943775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1744[2] = 
{
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_downloadTimeoutProp_15(),
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (WebRequestModuleElementCollection_t2218695785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (WebRequestModuleElement_t4070853259), -1, sizeof(WebRequestModuleElement_t4070853259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1746[3] = 
{
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_properties_15(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_prefixProp_16(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_typeProp_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (WebRequestModulesSection_t3717257007), -1, sizeof(WebRequestModulesSection_t3717257007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1747[2] = 
{
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_properties_19(),
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_webRequestModulesProp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (DecompressionMethods_t2530166567)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1748[4] = 
{
	DecompressionMethods_t2530166567::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (DefaultCertificatePolicy_t2545332216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (Dns_t1335526197), -1, sizeof(Dns_t1335526197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1750[2] = 
{
	Dns_t1335526197_StaticFields::get_offset_of_use_mono_dns_0(),
	Dns_t1335526197_StaticFields::get_offset_of_resolver_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (FtpAsyncResult_t770082413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[9] = 
{
	FtpAsyncResult_t770082413::get_offset_of_response_0(),
	FtpAsyncResult_t770082413::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t770082413::get_offset_of_exception_2(),
	FtpAsyncResult_t770082413::get_offset_of_callback_3(),
	FtpAsyncResult_t770082413::get_offset_of_stream_4(),
	FtpAsyncResult_t770082413::get_offset_of_state_5(),
	FtpAsyncResult_t770082413::get_offset_of_completed_6(),
	FtpAsyncResult_t770082413::get_offset_of_synch_7(),
	FtpAsyncResult_t770082413::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (FtpDataStream_t3588258764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[5] = 
{
	FtpDataStream_t3588258764::get_offset_of_request_8(),
	FtpDataStream_t3588258764::get_offset_of_networkStream_9(),
	FtpDataStream_t3588258764::get_offset_of_disposed_10(),
	FtpDataStream_t3588258764::get_offset_of_isRead_11(),
	FtpDataStream_t3588258764::get_offset_of_totalRead_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (WriteDelegate_t888270799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (ReadDelegate_t1559754630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (FtpRequestCreator_t3711983251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (FtpWebRequest_t3120721823), -1, sizeof(FtpWebRequest_t3120721823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1756[30] = 
{
	FtpWebRequest_t3120721823::get_offset_of_requestUri_12(),
	FtpWebRequest_t3120721823::get_offset_of_file_name_13(),
	FtpWebRequest_t3120721823::get_offset_of_servicePoint_14(),
	FtpWebRequest_t3120721823::get_offset_of_origDataStream_15(),
	FtpWebRequest_t3120721823::get_offset_of_dataStream_16(),
	FtpWebRequest_t3120721823::get_offset_of_controlStream_17(),
	FtpWebRequest_t3120721823::get_offset_of_controlReader_18(),
	FtpWebRequest_t3120721823::get_offset_of_credentials_19(),
	FtpWebRequest_t3120721823::get_offset_of_hostEntry_20(),
	FtpWebRequest_t3120721823::get_offset_of_localEndPoint_21(),
	FtpWebRequest_t3120721823::get_offset_of_remoteEndPoint_22(),
	FtpWebRequest_t3120721823::get_offset_of_proxy_23(),
	FtpWebRequest_t3120721823::get_offset_of_timeout_24(),
	FtpWebRequest_t3120721823::get_offset_of_rwTimeout_25(),
	FtpWebRequest_t3120721823::get_offset_of_offset_26(),
	FtpWebRequest_t3120721823::get_offset_of_binary_27(),
	FtpWebRequest_t3120721823::get_offset_of_enableSsl_28(),
	FtpWebRequest_t3120721823::get_offset_of_usePassive_29(),
	FtpWebRequest_t3120721823::get_offset_of_keepAlive_30(),
	FtpWebRequest_t3120721823::get_offset_of_method_31(),
	FtpWebRequest_t3120721823::get_offset_of_renameTo_32(),
	FtpWebRequest_t3120721823::get_offset_of_locker_33(),
	FtpWebRequest_t3120721823::get_offset_of_requestState_34(),
	FtpWebRequest_t3120721823::get_offset_of_asyncResult_35(),
	FtpWebRequest_t3120721823::get_offset_of_ftpResponse_36(),
	FtpWebRequest_t3120721823::get_offset_of_requestStream_37(),
	FtpWebRequest_t3120721823::get_offset_of_initial_path_38(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_supportedCommands_39(),
	FtpWebRequest_t3120721823::get_offset_of_dataEncoding_40(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (RequestState_t4256633122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1757[10] = 
{
	RequestState_t4256633122::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (FtpStatus_t3714482970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[2] = 
{
	FtpStatus_t3714482970::get_offset_of_statusCode_0(),
	FtpStatus_t3714482970::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (FtpWebResponse_t2609078769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[12] = 
{
	FtpWebResponse_t2609078769::get_offset_of_stream_1(),
	FtpWebResponse_t2609078769::get_offset_of_uri_2(),
	FtpWebResponse_t2609078769::get_offset_of_statusCode_3(),
	FtpWebResponse_t2609078769::get_offset_of_lastModified_4(),
	FtpWebResponse_t2609078769::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2609078769::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2609078769::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2609078769::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2609078769::get_offset_of_method_9(),
	FtpWebResponse_t2609078769::get_offset_of_disposed_10(),
	FtpWebResponse_t2609078769::get_offset_of_request_11(),
	FtpWebResponse_t2609078769::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (HttpRequestCreator_t1416559589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (HttpWebRequest_t1951404513), -1, sizeof(HttpWebRequest_t1951404513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1761[61] = 
{
	HttpWebRequest_t1951404513::get_offset_of_requestUri_12(),
	HttpWebRequest_t1951404513::get_offset_of_actualUri_13(),
	HttpWebRequest_t1951404513::get_offset_of_hostChanged_14(),
	HttpWebRequest_t1951404513::get_offset_of_allowAutoRedirect_15(),
	HttpWebRequest_t1951404513::get_offset_of_allowBuffering_16(),
	HttpWebRequest_t1951404513::get_offset_of_certificates_17(),
	HttpWebRequest_t1951404513::get_offset_of_connectionGroup_18(),
	HttpWebRequest_t1951404513::get_offset_of_haveContentLength_19(),
	HttpWebRequest_t1951404513::get_offset_of_contentLength_20(),
	HttpWebRequest_t1951404513::get_offset_of_continueDelegate_21(),
	HttpWebRequest_t1951404513::get_offset_of_cookieContainer_22(),
	HttpWebRequest_t1951404513::get_offset_of_credentials_23(),
	HttpWebRequest_t1951404513::get_offset_of_haveResponse_24(),
	HttpWebRequest_t1951404513::get_offset_of_haveRequest_25(),
	HttpWebRequest_t1951404513::get_offset_of_requestSent_26(),
	HttpWebRequest_t1951404513::get_offset_of_webHeaders_27(),
	HttpWebRequest_t1951404513::get_offset_of_keepAlive_28(),
	HttpWebRequest_t1951404513::get_offset_of_maxAutoRedirect_29(),
	HttpWebRequest_t1951404513::get_offset_of_mediaType_30(),
	HttpWebRequest_t1951404513::get_offset_of_method_31(),
	HttpWebRequest_t1951404513::get_offset_of_initialMethod_32(),
	HttpWebRequest_t1951404513::get_offset_of_pipelined_33(),
	HttpWebRequest_t1951404513::get_offset_of_preAuthenticate_34(),
	HttpWebRequest_t1951404513::get_offset_of_usedPreAuth_35(),
	HttpWebRequest_t1951404513::get_offset_of_version_36(),
	HttpWebRequest_t1951404513::get_offset_of_force_version_37(),
	HttpWebRequest_t1951404513::get_offset_of_actualVersion_38(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_39(),
	HttpWebRequest_t1951404513::get_offset_of_sendChunked_40(),
	HttpWebRequest_t1951404513::get_offset_of_servicePoint_41(),
	HttpWebRequest_t1951404513::get_offset_of_timeout_42(),
	HttpWebRequest_t1951404513::get_offset_of_writeStream_43(),
	HttpWebRequest_t1951404513::get_offset_of_webResponse_44(),
	HttpWebRequest_t1951404513::get_offset_of_asyncWrite_45(),
	HttpWebRequest_t1951404513::get_offset_of_asyncRead_46(),
	HttpWebRequest_t1951404513::get_offset_of_abortHandler_47(),
	HttpWebRequest_t1951404513::get_offset_of_aborted_48(),
	HttpWebRequest_t1951404513::get_offset_of_gotRequestStream_49(),
	HttpWebRequest_t1951404513::get_offset_of_redirects_50(),
	HttpWebRequest_t1951404513::get_offset_of_expectContinue_51(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBuffer_52(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBufferLength_53(),
	HttpWebRequest_t1951404513::get_offset_of_getResponseCalled_54(),
	HttpWebRequest_t1951404513::get_offset_of_saved_exc_55(),
	HttpWebRequest_t1951404513::get_offset_of_locker_56(),
	HttpWebRequest_t1951404513::get_offset_of_finished_reading_57(),
	HttpWebRequest_t1951404513::get_offset_of_WebConnection_58(),
	HttpWebRequest_t1951404513::get_offset_of_auto_decomp_59(),
	HttpWebRequest_t1951404513_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_60(),
	HttpWebRequest_t1951404513::get_offset_of_readWriteTimeout_61(),
	HttpWebRequest_t1951404513::get_offset_of_tlsProvider_62(),
	HttpWebRequest_t1951404513::get_offset_of_tlsSettings_63(),
	HttpWebRequest_t1951404513::get_offset_of_certValidationCallback_64(),
	HttpWebRequest_t1951404513::get_offset_of_auth_state_65(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_auth_state_66(),
	HttpWebRequest_t1951404513::get_offset_of_host_67(),
	HttpWebRequest_t1951404513::get_offset_of_ResendContentFactory_68(),
	HttpWebRequest_t1951404513::get_offset_of_U3CThrowOnErrorU3Ek__BackingField_69(),
	HttpWebRequest_t1951404513::get_offset_of_unsafe_auth_blah_70(),
	HttpWebRequest_t1951404513::get_offset_of_U3CReuseConnectionU3Ek__BackingField_71(),
	HttpWebRequest_t1951404513::get_offset_of_StoredConnection_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (NtlmAuthState_t3242387638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1762[4] = 
{
	NtlmAuthState_t3242387638::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (AuthorizationState_t3879143100)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[4] = 
{
	AuthorizationState_t3879143100::get_offset_of_request_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AuthorizationState_t3879143100::get_offset_of_isProxy_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AuthorizationState_t3879143100::get_offset_of_isCompleted_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AuthorizationState_t3879143100::get_offset_of_ntlm_auth_state_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757::get_offset_of_aread_0(),
	U3CBeginGetResponseU3Ec__AnonStorey0_t2626749757::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (HttpWebResponse_t2828383075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[12] = 
{
	HttpWebResponse_t2828383075::get_offset_of_uri_1(),
	HttpWebResponse_t2828383075::get_offset_of_webHeaders_2(),
	HttpWebResponse_t2828383075::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t2828383075::get_offset_of_method_4(),
	HttpWebResponse_t2828383075::get_offset_of_version_5(),
	HttpWebResponse_t2828383075::get_offset_of_statusCode_6(),
	HttpWebResponse_t2828383075::get_offset_of_statusDescription_7(),
	HttpWebResponse_t2828383075::get_offset_of_contentLength_8(),
	HttpWebResponse_t2828383075::get_offset_of_contentType_9(),
	HttpWebResponse_t2828383075::get_offset_of_cookie_container_10(),
	HttpWebResponse_t2828383075::get_offset_of_disposed_11(),
	HttpWebResponse_t2828383075::get_offset_of_stream_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (IPv6AddressFormatter_t3116172695)+ sizeof (Il2CppObject), sizeof(IPv6AddressFormatter_t3116172695_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1767[2] = 
{
	IPv6AddressFormatter_t3116172695::get_offset_of_address_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IPv6AddressFormatter_t3116172695::get_offset_of_scopeId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (CFObject_t3730145744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[1] = 
{
	CFObject_t3730145744::get_offset_of_U3CHandleU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (CFArray_t3428036444), -1, sizeof(CFArray_t3428036444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	CFArray_t3428036444_StaticFields::get_offset_of_kCFTypeArrayCallbacks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (CFNumber_t2174524538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (CFRange_t2990408436)+ sizeof (Il2CppObject), sizeof(CFRange_t2990408436 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[2] = 
{
	CFRange_t2990408436::get_offset_of_Location_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CFRange_t2990408436::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (CFStreamClientContext_t3245641555)+ sizeof (Il2CppObject), sizeof(CFStreamClientContext_t3245641555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[5] = 
{
	CFStreamClientContext_t3245641555::get_offset_of_Version_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Retain_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Release_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CFStreamClientContext_t3245641555::get_offset_of_CopyDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (CFString_t3358817686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	CFString_t3358817686::get_offset_of_str_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (CFDictionary_t3548969133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (CFUrl_t2608898728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (CFRunLoop_t1302886598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (CFProxyType_t2264861409)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[8] = 
{
	CFProxyType_t2264861409::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (CFProxy_t537977545), -1, sizeof(CFProxy_t537977545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1779[14] = 
{
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyAutoConfigurationJavaScriptKey_0(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyAutoConfigurationURLKey_1(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyHostNameKey_2(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyPasswordKey_3(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyPortNumberKey_4(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeKey_5(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyUsernameKey_6(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationURL_7(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationJavaScript_8(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeFTP_9(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeHTTP_10(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeHTTPS_11(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeSOCKS_12(),
	CFProxy_t537977545::get_offset_of_settings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (CFProxySettings_t4092929580), -1, sizeof(CFProxySettings_t4092929580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1780[7] = 
{
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPEnable_0(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPPort_1(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPProxy_2(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5(),
	CFProxySettings_t4092929580::get_offset_of_settings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (CFNetwork_t2805084167), -1, sizeof(CFNetwork_t2805084167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1781[4] = 
{
	CFNetwork_t2805084167_StaticFields::get_offset_of_lock_obj_0(),
	CFNetwork_t2805084167_StaticFields::get_offset_of_get_proxy_queue_1(),
	CFNetwork_t2805084167_StaticFields::get_offset_of_proxy_event_2(),
	CFNetwork_t2805084167_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (GetProxyData_t1386489386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[5] = 
{
	GetProxyData_t1386489386::get_offset_of_script_0(),
	GetProxyData_t1386489386::get_offset_of_targetUri_1(),
	GetProxyData_t1386489386::get_offset_of_error_2(),
	GetProxyData_t1386489386::get_offset_of_result_3(),
	GetProxyData_t1386489386::get_offset_of_evt_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (CFProxyAutoConfigurationResultCallback_t3537096558), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (CFWebProxy_t1432115055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[2] = 
{
	CFWebProxy_t1432115055::get_offset_of_credentials_0(),
	CFWebProxy_t1432115055::get_offset_of_userSpecified_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992::get_offset_of_proxies_0(),
	U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992::get_offset_of_runLoop_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (NetConfig_t1609737885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[1] = 
{
	NetConfig_t1609737885::get_offset_of_MaxResponseHeadersLength_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (Win32IPAddressCollection_t2117998352), -1, sizeof(Win32IPAddressCollection_t2117998352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[2] = 
{
	Win32IPAddressCollection_t2117998352_StaticFields::get_offset_of_Empty_1(),
	Win32IPAddressCollection_t2117998352::get_offset_of_is_readonly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (CommonUnixIPGlobalProperties_t83484526), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (UnixIPGlobalProperties_t2823347355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (MibIPGlobalProperties_t4104985615), -1, sizeof(MibIPGlobalProperties_t4104985615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1790[7] = 
{
	MibIPGlobalProperties_t4104985615::get_offset_of_StatisticsFile_0(),
	MibIPGlobalProperties_t4104985615::get_offset_of_StatisticsFileIPv6_1(),
	MibIPGlobalProperties_t4104985615::get_offset_of_TcpFile_2(),
	MibIPGlobalProperties_t4104985615::get_offset_of_Tcp6File_3(),
	MibIPGlobalProperties_t4104985615::get_offset_of_UdpFile_4(),
	MibIPGlobalProperties_t4104985615::get_offset_of_Udp6File_5(),
	MibIPGlobalProperties_t4104985615_StaticFields::get_offset_of_wsChars_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (Win32IPGlobalProperties_t2393043454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (UnixIPInterfaceProperties_t2418091637), -1, sizeof(UnixIPInterfaceProperties_t2418091637_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1792[7] = 
{
	UnixIPInterfaceProperties_t2418091637::get_offset_of_iface_0(),
	UnixIPInterfaceProperties_t2418091637::get_offset_of_addresses_1(),
	UnixIPInterfaceProperties_t2418091637::get_offset_of_dns_servers_2(),
	UnixIPInterfaceProperties_t2418091637::get_offset_of_dns_suffix_3(),
	UnixIPInterfaceProperties_t2418091637::get_offset_of_last_parse_4(),
	UnixIPInterfaceProperties_t2418091637_StaticFields::get_offset_of_ns_5(),
	UnixIPInterfaceProperties_t2418091637_StaticFields::get_offset_of_search_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (LinuxIPInterfaceProperties_t3318447793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (MacOsIPInterfaceProperties_t3101377296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (Win32IPInterfaceProperties2_t3641679752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[3] = 
{
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_addr_0(),
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_mib4_1(),
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_mib6_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (Win32IPv4InterfaceStatistics_t2652275954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	Win32IPv4InterfaceStatistics_t2652275954::get_offset_of_info_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (ifa_ifu_t602722385)+ sizeof (Il2CppObject), sizeof(ifa_ifu_t602722385 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1797[2] = 
{
	ifa_ifu_t602722385::get_offset_of_ifu_broadaddr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifa_ifu_t602722385::get_offset_of_ifu_dstaddr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ifaddrs_t2532459533)+ sizeof (Il2CppObject), sizeof(ifaddrs_t2532459533_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1798[7] = 
{
	ifaddrs_t2532459533::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_ifu_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (sockaddr_in_t1277740973)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t1277740973 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[3] = 
{
	sockaddr_in_t1277740973::get_offset_of_sin_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t1277740973::get_offset_of_sin_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t1277740973::get_offset_of_sin_addr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
