﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Threading_ExecutionContext_Reader611157692.h"

// System.Object
struct Il2CppObject;
// System.Threading.Thread
struct Thread_t241561612;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ExecutionContextSwitcher
struct  ExecutionContextSwitcher_t3304742894 
{
public:
	// System.Threading.ExecutionContext/Reader System.Threading.ExecutionContextSwitcher::outerEC
	Reader_t611157692  ___outerEC_0;
	// System.Boolean System.Threading.ExecutionContextSwitcher::outerECBelongsToScope
	bool ___outerECBelongsToScope_1;
	// System.Object System.Threading.ExecutionContextSwitcher::hecsw
	Il2CppObject * ___hecsw_2;
	// System.Threading.Thread System.Threading.ExecutionContextSwitcher::thread
	Thread_t241561612 * ___thread_3;

public:
	inline static int32_t get_offset_of_outerEC_0() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t3304742894, ___outerEC_0)); }
	inline Reader_t611157692  get_outerEC_0() const { return ___outerEC_0; }
	inline Reader_t611157692 * get_address_of_outerEC_0() { return &___outerEC_0; }
	inline void set_outerEC_0(Reader_t611157692  value)
	{
		___outerEC_0 = value;
	}

	inline static int32_t get_offset_of_outerECBelongsToScope_1() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t3304742894, ___outerECBelongsToScope_1)); }
	inline bool get_outerECBelongsToScope_1() const { return ___outerECBelongsToScope_1; }
	inline bool* get_address_of_outerECBelongsToScope_1() { return &___outerECBelongsToScope_1; }
	inline void set_outerECBelongsToScope_1(bool value)
	{
		___outerECBelongsToScope_1 = value;
	}

	inline static int32_t get_offset_of_hecsw_2() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t3304742894, ___hecsw_2)); }
	inline Il2CppObject * get_hecsw_2() const { return ___hecsw_2; }
	inline Il2CppObject ** get_address_of_hecsw_2() { return &___hecsw_2; }
	inline void set_hecsw_2(Il2CppObject * value)
	{
		___hecsw_2 = value;
		Il2CppCodeGenWriteBarrier(&___hecsw_2, value);
	}

	inline static int32_t get_offset_of_thread_3() { return static_cast<int32_t>(offsetof(ExecutionContextSwitcher_t3304742894, ___thread_3)); }
	inline Thread_t241561612 * get_thread_3() const { return ___thread_3; }
	inline Thread_t241561612 ** get_address_of_thread_3() { return &___thread_3; }
	inline void set_thread_3(Thread_t241561612 * value)
	{
		___thread_3 = value;
		Il2CppCodeGenWriteBarrier(&___thread_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.ExecutionContextSwitcher
struct ExecutionContextSwitcher_t3304742894_marshaled_pinvoke
{
	Reader_t611157692_marshaled_pinvoke ___outerEC_0;
	int32_t ___outerECBelongsToScope_1;
	Il2CppIUnknown* ___hecsw_2;
	Thread_t241561612 * ___thread_3;
};
// Native definition for COM marshalling of System.Threading.ExecutionContextSwitcher
struct ExecutionContextSwitcher_t3304742894_marshaled_com
{
	Reader_t611157692_marshaled_com ___outerEC_0;
	int32_t ___outerECBelongsToScope_1;
	Il2CppIUnknown* ___hecsw_2;
	Thread_t241561612 * ___thread_3;
};
