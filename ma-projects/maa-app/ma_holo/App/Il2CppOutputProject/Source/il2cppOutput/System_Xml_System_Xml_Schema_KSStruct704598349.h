﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.KeySequence
struct KeySequence_t746093258;
// System.Xml.Schema.LocatedActiveAxis[]
struct LocatedActiveAxisU5BU5D_t2201939280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KSStruct
struct  KSStruct_t704598349  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.Schema.KSStruct::depth
	int32_t ___depth_0;
	// System.Xml.Schema.KeySequence System.Xml.Schema.KSStruct::ks
	KeySequence_t746093258 * ___ks_1;
	// System.Xml.Schema.LocatedActiveAxis[] System.Xml.Schema.KSStruct::fields
	LocatedActiveAxisU5BU5D_t2201939280* ___fields_2;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_ks_1() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___ks_1)); }
	inline KeySequence_t746093258 * get_ks_1() const { return ___ks_1; }
	inline KeySequence_t746093258 ** get_address_of_ks_1() { return &___ks_1; }
	inline void set_ks_1(KeySequence_t746093258 * value)
	{
		___ks_1 = value;
		Il2CppCodeGenWriteBarrier(&___ks_1, value);
	}

	inline static int32_t get_offset_of_fields_2() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___fields_2)); }
	inline LocatedActiveAxisU5BU5D_t2201939280* get_fields_2() const { return ___fields_2; }
	inline LocatedActiveAxisU5BU5D_t2201939280** get_address_of_fields_2() { return &___fields_2; }
	inline void set_fields_2(LocatedActiveAxisU5BU5D_t2201939280* value)
	{
		___fields_2 = value;
		Il2CppCodeGenWriteBarrier(&___fields_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
