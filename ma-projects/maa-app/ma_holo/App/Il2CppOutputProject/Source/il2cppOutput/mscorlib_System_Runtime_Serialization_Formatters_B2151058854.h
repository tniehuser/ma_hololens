﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SizedArray
struct  SizedArray_t2151058854  : public Il2CppObject
{
public:
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.SizedArray::objects
	ObjectU5BU5D_t3614634134* ___objects_0;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.SizedArray::negObjects
	ObjectU5BU5D_t3614634134* ___negObjects_1;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(SizedArray_t2151058854, ___objects_0)); }
	inline ObjectU5BU5D_t3614634134* get_objects_0() const { return ___objects_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(ObjectU5BU5D_t3614634134* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier(&___objects_0, value);
	}

	inline static int32_t get_offset_of_negObjects_1() { return static_cast<int32_t>(offsetof(SizedArray_t2151058854, ___negObjects_1)); }
	inline ObjectU5BU5D_t3614634134* get_negObjects_1() const { return ___negObjects_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_negObjects_1() { return &___negObjects_1; }
	inline void set_negObjects_1(ObjectU5BU5D_t3614634134* value)
	{
		___negObjects_1 = value;
		Il2CppCodeGenWriteBarrier(&___negObjects_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
