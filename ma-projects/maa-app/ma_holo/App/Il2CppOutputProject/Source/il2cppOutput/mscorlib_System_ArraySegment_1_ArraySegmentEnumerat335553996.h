﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1/ArraySegmentEnumerator<System.Byte>
struct  ArraySegmentEnumerator_t335553996  : public Il2CppObject
{
public:
	// T[] System.ArraySegment`1/ArraySegmentEnumerator::_array
	ByteU5BU5D_t3397334013* ____array_0;
	// System.Int32 System.ArraySegment`1/ArraySegmentEnumerator::_start
	int32_t ____start_1;
	// System.Int32 System.ArraySegment`1/ArraySegmentEnumerator::_end
	int32_t ____end_2;
	// System.Int32 System.ArraySegment`1/ArraySegmentEnumerator::_current
	int32_t ____current_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegmentEnumerator_t335553996, ____array_0)); }
	inline ByteU5BU5D_t3397334013* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_t3397334013* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier(&____array_0, value);
	}

	inline static int32_t get_offset_of__start_1() { return static_cast<int32_t>(offsetof(ArraySegmentEnumerator_t335553996, ____start_1)); }
	inline int32_t get__start_1() const { return ____start_1; }
	inline int32_t* get_address_of__start_1() { return &____start_1; }
	inline void set__start_1(int32_t value)
	{
		____start_1 = value;
	}

	inline static int32_t get_offset_of__end_2() { return static_cast<int32_t>(offsetof(ArraySegmentEnumerator_t335553996, ____end_2)); }
	inline int32_t get__end_2() const { return ____end_2; }
	inline int32_t* get_address_of__end_2() { return &____end_2; }
	inline void set__end_2(int32_t value)
	{
		____end_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(ArraySegmentEnumerator_t335553996, ____current_3)); }
	inline int32_t get__current_3() const { return ____current_3; }
	inline int32_t* get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(int32_t value)
	{
		____current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
