﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Net.CFProxy[]
struct CFProxyU5BU5D_t2453615604;
// Mono.Net.CFRunLoop
struct CFRunLoop_t1302886598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/<ExecuteProxyAutoConfigurationURL>c__AnonStorey0
struct  U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992  : public Il2CppObject
{
public:
	// Mono.Net.CFProxy[] Mono.Net.CFNetwork/<ExecuteProxyAutoConfigurationURL>c__AnonStorey0::proxies
	CFProxyU5BU5D_t2453615604* ___proxies_0;
	// Mono.Net.CFRunLoop Mono.Net.CFNetwork/<ExecuteProxyAutoConfigurationURL>c__AnonStorey0::runLoop
	CFRunLoop_t1302886598 * ___runLoop_1;

public:
	inline static int32_t get_offset_of_proxies_0() { return static_cast<int32_t>(offsetof(U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992, ___proxies_0)); }
	inline CFProxyU5BU5D_t2453615604* get_proxies_0() const { return ___proxies_0; }
	inline CFProxyU5BU5D_t2453615604** get_address_of_proxies_0() { return &___proxies_0; }
	inline void set_proxies_0(CFProxyU5BU5D_t2453615604* value)
	{
		___proxies_0 = value;
		Il2CppCodeGenWriteBarrier(&___proxies_0, value);
	}

	inline static int32_t get_offset_of_runLoop_1() { return static_cast<int32_t>(offsetof(U3CExecuteProxyAutoConfigurationURLU3Ec__AnonStorey0_t35768992, ___runLoop_1)); }
	inline CFRunLoop_t1302886598 * get_runLoop_1() const { return ___runLoop_1; }
	inline CFRunLoop_t1302886598 ** get_address_of_runLoop_1() { return &___runLoop_1; }
	inline void set_runLoop_1(CFRunLoop_t1302886598 * value)
	{
		___runLoop_1 = value;
		Il2CppCodeGenWriteBarrier(&___runLoop_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
