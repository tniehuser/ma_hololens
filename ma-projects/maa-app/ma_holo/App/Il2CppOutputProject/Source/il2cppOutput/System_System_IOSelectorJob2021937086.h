﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_IOOperation4250055067.h"

// System.IOAsyncCallback
struct IOAsyncCallback_t2427139621;
// System.IOAsyncResult
struct IOAsyncResult_t1276329107;
struct IOAsyncResult_t1276329107_marshaled_pinvoke;
struct IOAsyncResult_t1276329107_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOSelectorJob
struct  IOSelectorJob_t2021937086  : public Il2CppObject
{
public:
	// System.IOOperation System.IOSelectorJob::operation
	int32_t ___operation_0;
	// System.IOAsyncCallback System.IOSelectorJob::callback
	IOAsyncCallback_t2427139621 * ___callback_1;
	// System.IOAsyncResult System.IOSelectorJob::state
	IOAsyncResult_t1276329107 * ___state_2;

public:
	inline static int32_t get_offset_of_operation_0() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___operation_0)); }
	inline int32_t get_operation_0() const { return ___operation_0; }
	inline int32_t* get_address_of_operation_0() { return &___operation_0; }
	inline void set_operation_0(int32_t value)
	{
		___operation_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___callback_1)); }
	inline IOAsyncCallback_t2427139621 * get_callback_1() const { return ___callback_1; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(IOAsyncCallback_t2427139621 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___state_2)); }
	inline IOAsyncResult_t1276329107 * get_state_2() const { return ___state_2; }
	inline IOAsyncResult_t1276329107 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(IOAsyncResult_t1276329107 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier(&___state_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOSelectorJob
struct IOSelectorJob_t2021937086_marshaled_pinvoke
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t1276329107_marshaled_pinvoke* ___state_2;
};
// Native definition for COM marshalling of System.IOSelectorJob
struct IOSelectorJob_t2021937086_marshaled_com
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t1276329107_marshaled_com* ___state_2;
};
