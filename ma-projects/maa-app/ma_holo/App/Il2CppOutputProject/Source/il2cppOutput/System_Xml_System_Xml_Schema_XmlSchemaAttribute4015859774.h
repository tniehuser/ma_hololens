﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse3553149267.h"

// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t1510907267;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_t4015859774  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaAttribute::defaultValue
	String_t* ___defaultValue_9;
	// System.String System.Xml.Schema.XmlSchemaAttribute::fixedValue
	String_t* ___fixedValue_10;
	// System.String System.Xml.Schema.XmlSchemaAttribute::name
	String_t* ___name_11;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaAttribute::form
	int32_t ___form_12;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::use
	int32_t ___use_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::refName
	XmlQualifiedName_t1944712516 * ___refName_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::typeName
	XmlQualifiedName_t1944712516 * ___typeName_15;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::type
	XmlSchemaSimpleType_t248156492 * ___type_17;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::attributeType
	XmlSchemaSimpleType_t248156492 * ___attributeType_18;
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XmlSchemaAttribute::attDef
	SchemaAttDef_t1510907267 * ___attDef_19;

public:
	inline static int32_t get_offset_of_defaultValue_9() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___defaultValue_9)); }
	inline String_t* get_defaultValue_9() const { return ___defaultValue_9; }
	inline String_t** get_address_of_defaultValue_9() { return &___defaultValue_9; }
	inline void set_defaultValue_9(String_t* value)
	{
		___defaultValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___defaultValue_9, value);
	}

	inline static int32_t get_offset_of_fixedValue_10() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___fixedValue_10)); }
	inline String_t* get_fixedValue_10() const { return ___fixedValue_10; }
	inline String_t** get_address_of_fixedValue_10() { return &___fixedValue_10; }
	inline void set_fixedValue_10(String_t* value)
	{
		___fixedValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___fixedValue_10, value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier(&___name_11, value);
	}

	inline static int32_t get_offset_of_form_12() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___form_12)); }
	inline int32_t get_form_12() const { return ___form_12; }
	inline int32_t* get_address_of_form_12() { return &___form_12; }
	inline void set_form_12(int32_t value)
	{
		___form_12 = value;
	}

	inline static int32_t get_offset_of_use_13() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___use_13)); }
	inline int32_t get_use_13() const { return ___use_13; }
	inline int32_t* get_address_of_use_13() { return &___use_13; }
	inline void set_use_13(int32_t value)
	{
		___use_13 = value;
	}

	inline static int32_t get_offset_of_refName_14() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___refName_14)); }
	inline XmlQualifiedName_t1944712516 * get_refName_14() const { return ___refName_14; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_14() { return &___refName_14; }
	inline void set_refName_14(XmlQualifiedName_t1944712516 * value)
	{
		___refName_14 = value;
		Il2CppCodeGenWriteBarrier(&___refName_14, value);
	}

	inline static int32_t get_offset_of_typeName_15() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___typeName_15)); }
	inline XmlQualifiedName_t1944712516 * get_typeName_15() const { return ___typeName_15; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_typeName_15() { return &___typeName_15; }
	inline void set_typeName_15(XmlQualifiedName_t1944712516 * value)
	{
		___typeName_15 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_15, value);
	}

	inline static int32_t get_offset_of_qualifiedName_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___qualifiedName_16)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_16() const { return ___qualifiedName_16; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_16() { return &___qualifiedName_16; }
	inline void set_qualifiedName_16(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_16 = value;
		Il2CppCodeGenWriteBarrier(&___qualifiedName_16, value);
	}

	inline static int32_t get_offset_of_type_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___type_17)); }
	inline XmlSchemaSimpleType_t248156492 * get_type_17() const { return ___type_17; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_type_17() { return &___type_17; }
	inline void set_type_17(XmlSchemaSimpleType_t248156492 * value)
	{
		___type_17 = value;
		Il2CppCodeGenWriteBarrier(&___type_17, value);
	}

	inline static int32_t get_offset_of_attributeType_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___attributeType_18)); }
	inline XmlSchemaSimpleType_t248156492 * get_attributeType_18() const { return ___attributeType_18; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_attributeType_18() { return &___attributeType_18; }
	inline void set_attributeType_18(XmlSchemaSimpleType_t248156492 * value)
	{
		___attributeType_18 = value;
		Il2CppCodeGenWriteBarrier(&___attributeType_18, value);
	}

	inline static int32_t get_offset_of_attDef_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___attDef_19)); }
	inline SchemaAttDef_t1510907267 * get_attDef_19() const { return ___attDef_19; }
	inline SchemaAttDef_t1510907267 ** get_address_of_attDef_19() { return &___attDef_19; }
	inline void set_attDef_19(SchemaAttDef_t1510907267 * value)
	{
		___attDef_19 = value;
		Il2CppCodeGenWriteBarrier(&___attDef_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
