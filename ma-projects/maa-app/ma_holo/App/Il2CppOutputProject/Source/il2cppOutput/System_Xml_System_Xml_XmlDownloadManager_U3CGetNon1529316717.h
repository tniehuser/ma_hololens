﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask1838408441.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure2811256899.h"

// System.Uri
struct Uri_t19570940;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Net.WebResponse
struct WebResponse_t1895226051;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Xml.XmlDownloadManager
struct XmlDownloadManager_t830495394;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0
struct  U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717 
{
public:
	// System.Uri System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::uri
	Uri_t19570940 * ___uri_0;
	// System.Net.WebRequest System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::<req>__0
	WebRequest_t1365124353 * ___U3CreqU3E__0_1;
	// System.Net.ICredentials System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::credentials
	Il2CppObject * ___credentials_2;
	// System.Net.IWebProxy System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::proxy
	Il2CppObject * ___proxy_3;
	// System.Net.Cache.RequestCachePolicy System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::cachePolicy
	RequestCachePolicy_t2663429579 * ___cachePolicy_4;
	// System.Net.WebResponse System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::<resp>__0
	WebResponse_t1895226051 * ___U3CrespU3E__0_5;
	// System.Net.HttpWebRequest System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::<webReq>__0
	HttpWebRequest_t1951404513 * ___U3CwebReqU3E__0_6;
	// System.Xml.XmlDownloadManager System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::$this
	XmlDownloadManager_t830495394 * ___U24this_7;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.IO.Stream> System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::$builder
	AsyncTaskMethodBuilder_1_t1838408441  ___U24builder_8;
	// System.Int32 System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::$PC
	int32_t ___U24PC_9;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.WebResponse> System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>c__async0::$awaiter0
	ConfiguredTaskAwaiter_t2811256899  ___U24awaiter0_10;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier(&___uri_0, value);
	}

	inline static int32_t get_offset_of_U3CreqU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U3CreqU3E__0_1)); }
	inline WebRequest_t1365124353 * get_U3CreqU3E__0_1() const { return ___U3CreqU3E__0_1; }
	inline WebRequest_t1365124353 ** get_address_of_U3CreqU3E__0_1() { return &___U3CreqU3E__0_1; }
	inline void set_U3CreqU3E__0_1(WebRequest_t1365124353 * value)
	{
		___U3CreqU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreqU3E__0_1, value);
	}

	inline static int32_t get_offset_of_credentials_2() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___credentials_2)); }
	inline Il2CppObject * get_credentials_2() const { return ___credentials_2; }
	inline Il2CppObject ** get_address_of_credentials_2() { return &___credentials_2; }
	inline void set_credentials_2(Il2CppObject * value)
	{
		___credentials_2 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_2, value);
	}

	inline static int32_t get_offset_of_proxy_3() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___proxy_3)); }
	inline Il2CppObject * get_proxy_3() const { return ___proxy_3; }
	inline Il2CppObject ** get_address_of_proxy_3() { return &___proxy_3; }
	inline void set_proxy_3(Il2CppObject * value)
	{
		___proxy_3 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_3, value);
	}

	inline static int32_t get_offset_of_cachePolicy_4() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___cachePolicy_4)); }
	inline RequestCachePolicy_t2663429579 * get_cachePolicy_4() const { return ___cachePolicy_4; }
	inline RequestCachePolicy_t2663429579 ** get_address_of_cachePolicy_4() { return &___cachePolicy_4; }
	inline void set_cachePolicy_4(RequestCachePolicy_t2663429579 * value)
	{
		___cachePolicy_4 = value;
		Il2CppCodeGenWriteBarrier(&___cachePolicy_4, value);
	}

	inline static int32_t get_offset_of_U3CrespU3E__0_5() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U3CrespU3E__0_5)); }
	inline WebResponse_t1895226051 * get_U3CrespU3E__0_5() const { return ___U3CrespU3E__0_5; }
	inline WebResponse_t1895226051 ** get_address_of_U3CrespU3E__0_5() { return &___U3CrespU3E__0_5; }
	inline void set_U3CrespU3E__0_5(WebResponse_t1895226051 * value)
	{
		___U3CrespU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrespU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U3CwebReqU3E__0_6() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U3CwebReqU3E__0_6)); }
	inline HttpWebRequest_t1951404513 * get_U3CwebReqU3E__0_6() const { return ___U3CwebReqU3E__0_6; }
	inline HttpWebRequest_t1951404513 ** get_address_of_U3CwebReqU3E__0_6() { return &___U3CwebReqU3E__0_6; }
	inline void set_U3CwebReqU3E__0_6(HttpWebRequest_t1951404513 * value)
	{
		___U3CwebReqU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwebReqU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U24this_7)); }
	inline XmlDownloadManager_t830495394 * get_U24this_7() const { return ___U24this_7; }
	inline XmlDownloadManager_t830495394 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(XmlDownloadManager_t830495394 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24builder_8() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U24builder_8)); }
	inline AsyncTaskMethodBuilder_1_t1838408441  get_U24builder_8() const { return ___U24builder_8; }
	inline AsyncTaskMethodBuilder_1_t1838408441 * get_address_of_U24builder_8() { return &___U24builder_8; }
	inline void set_U24builder_8(AsyncTaskMethodBuilder_1_t1838408441  value)
	{
		___U24builder_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24awaiter0_10() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717, ___U24awaiter0_10)); }
	inline ConfiguredTaskAwaiter_t2811256899  get_U24awaiter0_10() const { return ___U24awaiter0_10; }
	inline ConfiguredTaskAwaiter_t2811256899 * get_address_of_U24awaiter0_10() { return &___U24awaiter0_10; }
	inline void set_U24awaiter0_10(ConfiguredTaskAwaiter_t2811256899  value)
	{
		___U24awaiter0_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
