﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Net_WebHeaderCollectionType1212469221.h"

// System.Net.HeaderInfoTable
struct HeaderInfoTable_t2462863175;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.SByte[]
struct SByteU5BU5D_t3472287392;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Net.WebHeaderCollection/RfcChar[]
struct RfcCharU5BU5D_t2287831188;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_t3028142837  : public NameValueCollection_t3047564564
{
public:
	// System.String[] System.Net.WebHeaderCollection::m_CommonHeaders
	StringU5BU5D_t1642385972* ___m_CommonHeaders_14;
	// System.Int32 System.Net.WebHeaderCollection::m_NumCommonHeaders
	int32_t ___m_NumCommonHeaders_15;
	// System.Collections.Specialized.NameValueCollection System.Net.WebHeaderCollection::m_InnerCollection
	NameValueCollection_t3047564564 * ___m_InnerCollection_18;
	// System.Net.WebHeaderCollectionType System.Net.WebHeaderCollection::m_Type
	uint16_t ___m_Type_19;

public:
	inline static int32_t get_offset_of_m_CommonHeaders_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_CommonHeaders_14)); }
	inline StringU5BU5D_t1642385972* get_m_CommonHeaders_14() const { return ___m_CommonHeaders_14; }
	inline StringU5BU5D_t1642385972** get_address_of_m_CommonHeaders_14() { return &___m_CommonHeaders_14; }
	inline void set_m_CommonHeaders_14(StringU5BU5D_t1642385972* value)
	{
		___m_CommonHeaders_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_CommonHeaders_14, value);
	}

	inline static int32_t get_offset_of_m_NumCommonHeaders_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_NumCommonHeaders_15)); }
	inline int32_t get_m_NumCommonHeaders_15() const { return ___m_NumCommonHeaders_15; }
	inline int32_t* get_address_of_m_NumCommonHeaders_15() { return &___m_NumCommonHeaders_15; }
	inline void set_m_NumCommonHeaders_15(int32_t value)
	{
		___m_NumCommonHeaders_15 = value;
	}

	inline static int32_t get_offset_of_m_InnerCollection_18() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_InnerCollection_18)); }
	inline NameValueCollection_t3047564564 * get_m_InnerCollection_18() const { return ___m_InnerCollection_18; }
	inline NameValueCollection_t3047564564 ** get_address_of_m_InnerCollection_18() { return &___m_InnerCollection_18; }
	inline void set_m_InnerCollection_18(NameValueCollection_t3047564564 * value)
	{
		___m_InnerCollection_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_InnerCollection_18, value);
	}

	inline static int32_t get_offset_of_m_Type_19() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837, ___m_Type_19)); }
	inline uint16_t get_m_Type_19() const { return ___m_Type_19; }
	inline uint16_t* get_address_of_m_Type_19() { return &___m_Type_19; }
	inline void set_m_Type_19(uint16_t value)
	{
		___m_Type_19 = value;
	}
};

struct WebHeaderCollection_t3028142837_StaticFields
{
public:
	// System.Net.HeaderInfoTable System.Net.WebHeaderCollection::HInfo
	HeaderInfoTable_t2462863175 * ___HInfo_13;
	// System.String[] System.Net.WebHeaderCollection::s_CommonHeaderNames
	StringU5BU5D_t1642385972* ___s_CommonHeaderNames_16;
	// System.SByte[] System.Net.WebHeaderCollection::s_CommonHeaderHints
	SByteU5BU5D_t3472287392* ___s_CommonHeaderHints_17;
	// System.Char[] System.Net.WebHeaderCollection::HttpTrimCharacters
	CharU5BU5D_t1328083999* ___HttpTrimCharacters_20;
	// System.Net.WebHeaderCollection/RfcChar[] System.Net.WebHeaderCollection::RfcCharMap
	RfcCharU5BU5D_t2287831188* ___RfcCharMap_21;

public:
	inline static int32_t get_offset_of_HInfo_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___HInfo_13)); }
	inline HeaderInfoTable_t2462863175 * get_HInfo_13() const { return ___HInfo_13; }
	inline HeaderInfoTable_t2462863175 ** get_address_of_HInfo_13() { return &___HInfo_13; }
	inline void set_HInfo_13(HeaderInfoTable_t2462863175 * value)
	{
		___HInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&___HInfo_13, value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderNames_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___s_CommonHeaderNames_16)); }
	inline StringU5BU5D_t1642385972* get_s_CommonHeaderNames_16() const { return ___s_CommonHeaderNames_16; }
	inline StringU5BU5D_t1642385972** get_address_of_s_CommonHeaderNames_16() { return &___s_CommonHeaderNames_16; }
	inline void set_s_CommonHeaderNames_16(StringU5BU5D_t1642385972* value)
	{
		___s_CommonHeaderNames_16 = value;
		Il2CppCodeGenWriteBarrier(&___s_CommonHeaderNames_16, value);
	}

	inline static int32_t get_offset_of_s_CommonHeaderHints_17() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___s_CommonHeaderHints_17)); }
	inline SByteU5BU5D_t3472287392* get_s_CommonHeaderHints_17() const { return ___s_CommonHeaderHints_17; }
	inline SByteU5BU5D_t3472287392** get_address_of_s_CommonHeaderHints_17() { return &___s_CommonHeaderHints_17; }
	inline void set_s_CommonHeaderHints_17(SByteU5BU5D_t3472287392* value)
	{
		___s_CommonHeaderHints_17 = value;
		Il2CppCodeGenWriteBarrier(&___s_CommonHeaderHints_17, value);
	}

	inline static int32_t get_offset_of_HttpTrimCharacters_20() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___HttpTrimCharacters_20)); }
	inline CharU5BU5D_t1328083999* get_HttpTrimCharacters_20() const { return ___HttpTrimCharacters_20; }
	inline CharU5BU5D_t1328083999** get_address_of_HttpTrimCharacters_20() { return &___HttpTrimCharacters_20; }
	inline void set_HttpTrimCharacters_20(CharU5BU5D_t1328083999* value)
	{
		___HttpTrimCharacters_20 = value;
		Il2CppCodeGenWriteBarrier(&___HttpTrimCharacters_20, value);
	}

	inline static int32_t get_offset_of_RfcCharMap_21() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t3028142837_StaticFields, ___RfcCharMap_21)); }
	inline RfcCharU5BU5D_t2287831188* get_RfcCharMap_21() const { return ___RfcCharMap_21; }
	inline RfcCharU5BU5D_t2287831188** get_address_of_RfcCharMap_21() { return &___RfcCharMap_21; }
	inline void set_RfcCharMap_21(RfcCharU5BU5D_t2287831188* value)
	{
		___RfcCharMap_21 = value;
		Il2CppCodeGenWriteBarrier(&___RfcCharMap_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
