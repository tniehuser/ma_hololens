﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Globalization.InternalEncodingDataItem[]
struct InternalEncodingDataItemU5BU5D_t2088713692;
// System.Globalization.InternalCodePageDataItem[]
struct InternalCodePageDataItemU5BU5D_t1272849843;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.EncodingTable
struct  EncodingTable_t1040637825  : public Il2CppObject
{
public:

public:
};

struct EncodingTable_t1040637825_StaticFields
{
public:
	// System.Globalization.InternalEncodingDataItem[] System.Globalization.EncodingTable::encodingDataPtr
	InternalEncodingDataItemU5BU5D_t2088713692* ___encodingDataPtr_0;
	// System.Globalization.InternalCodePageDataItem[] System.Globalization.EncodingTable::codePageDataPtr
	InternalCodePageDataItemU5BU5D_t1272849843* ___codePageDataPtr_1;
	// System.Int32 System.Globalization.EncodingTable::lastEncodingItem
	int32_t ___lastEncodingItem_2;
	// System.Collections.Hashtable System.Globalization.EncodingTable::hashByName
	Hashtable_t909839986 * ___hashByName_3;
	// System.Collections.Hashtable System.Globalization.EncodingTable::hashByCodePage
	Hashtable_t909839986 * ___hashByCodePage_4;

public:
	inline static int32_t get_offset_of_encodingDataPtr_0() { return static_cast<int32_t>(offsetof(EncodingTable_t1040637825_StaticFields, ___encodingDataPtr_0)); }
	inline InternalEncodingDataItemU5BU5D_t2088713692* get_encodingDataPtr_0() const { return ___encodingDataPtr_0; }
	inline InternalEncodingDataItemU5BU5D_t2088713692** get_address_of_encodingDataPtr_0() { return &___encodingDataPtr_0; }
	inline void set_encodingDataPtr_0(InternalEncodingDataItemU5BU5D_t2088713692* value)
	{
		___encodingDataPtr_0 = value;
		Il2CppCodeGenWriteBarrier(&___encodingDataPtr_0, value);
	}

	inline static int32_t get_offset_of_codePageDataPtr_1() { return static_cast<int32_t>(offsetof(EncodingTable_t1040637825_StaticFields, ___codePageDataPtr_1)); }
	inline InternalCodePageDataItemU5BU5D_t1272849843* get_codePageDataPtr_1() const { return ___codePageDataPtr_1; }
	inline InternalCodePageDataItemU5BU5D_t1272849843** get_address_of_codePageDataPtr_1() { return &___codePageDataPtr_1; }
	inline void set_codePageDataPtr_1(InternalCodePageDataItemU5BU5D_t1272849843* value)
	{
		___codePageDataPtr_1 = value;
		Il2CppCodeGenWriteBarrier(&___codePageDataPtr_1, value);
	}

	inline static int32_t get_offset_of_lastEncodingItem_2() { return static_cast<int32_t>(offsetof(EncodingTable_t1040637825_StaticFields, ___lastEncodingItem_2)); }
	inline int32_t get_lastEncodingItem_2() const { return ___lastEncodingItem_2; }
	inline int32_t* get_address_of_lastEncodingItem_2() { return &___lastEncodingItem_2; }
	inline void set_lastEncodingItem_2(int32_t value)
	{
		___lastEncodingItem_2 = value;
	}

	inline static int32_t get_offset_of_hashByName_3() { return static_cast<int32_t>(offsetof(EncodingTable_t1040637825_StaticFields, ___hashByName_3)); }
	inline Hashtable_t909839986 * get_hashByName_3() const { return ___hashByName_3; }
	inline Hashtable_t909839986 ** get_address_of_hashByName_3() { return &___hashByName_3; }
	inline void set_hashByName_3(Hashtable_t909839986 * value)
	{
		___hashByName_3 = value;
		Il2CppCodeGenWriteBarrier(&___hashByName_3, value);
	}

	inline static int32_t get_offset_of_hashByCodePage_4() { return static_cast<int32_t>(offsetof(EncodingTable_t1040637825_StaticFields, ___hashByCodePage_4)); }
	inline Hashtable_t909839986 * get_hashByCodePage_4() const { return ___hashByCodePage_4; }
	inline Hashtable_t909839986 ** get_address_of_hashByCodePage_4() { return &___hashByCodePage_4; }
	inline void set_hashByCodePage_4(Hashtable_t909839986 * value)
	{
		___hashByCodePage_4 = value;
		Il2CppCodeGenWriteBarrier(&___hashByCodePage_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
