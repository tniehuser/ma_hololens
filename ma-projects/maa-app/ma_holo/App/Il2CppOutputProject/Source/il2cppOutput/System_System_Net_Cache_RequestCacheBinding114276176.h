﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.Cache.RequestCache
struct RequestCache_t1417804387;
// System.Net.Cache.RequestCacheValidator
struct RequestCacheValidator_t1766318073;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cache.RequestCacheBinding
struct  RequestCacheBinding_t114276176  : public Il2CppObject
{
public:
	// System.Net.Cache.RequestCache System.Net.Cache.RequestCacheBinding::m_RequestCache
	RequestCache_t1417804387 * ___m_RequestCache_0;
	// System.Net.Cache.RequestCacheValidator System.Net.Cache.RequestCacheBinding::m_CacheValidator
	RequestCacheValidator_t1766318073 * ___m_CacheValidator_1;

public:
	inline static int32_t get_offset_of_m_RequestCache_0() { return static_cast<int32_t>(offsetof(RequestCacheBinding_t114276176, ___m_RequestCache_0)); }
	inline RequestCache_t1417804387 * get_m_RequestCache_0() const { return ___m_RequestCache_0; }
	inline RequestCache_t1417804387 ** get_address_of_m_RequestCache_0() { return &___m_RequestCache_0; }
	inline void set_m_RequestCache_0(RequestCache_t1417804387 * value)
	{
		___m_RequestCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_RequestCache_0, value);
	}

	inline static int32_t get_offset_of_m_CacheValidator_1() { return static_cast<int32_t>(offsetof(RequestCacheBinding_t114276176, ___m_CacheValidator_1)); }
	inline RequestCacheValidator_t1766318073 * get_m_CacheValidator_1() const { return ___m_CacheValidator_1; }
	inline RequestCacheValidator_t1766318073 ** get_address_of_m_CacheValidator_1() { return &___m_CacheValidator_1; }
	inline void set_m_CacheValidator_1(RequestCacheValidator_t1766318073 * value)
	{
		___m_CacheValidator_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_CacheValidator_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
