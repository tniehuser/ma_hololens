﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Collections.SortedList/KeyList
struct KeyList_t1496669825;
// System.Collections.SortedList/ValueList
struct ValueList_t1155029929;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.SortedList
struct  SortedList_t3004938869  : public Il2CppObject
{
public:
	// System.Object[] System.Collections.SortedList::keys
	ObjectU5BU5D_t3614634134* ___keys_0;
	// System.Object[] System.Collections.SortedList::values
	ObjectU5BU5D_t3614634134* ___values_1;
	// System.Int32 System.Collections.SortedList::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.SortedList::version
	int32_t ___version_3;
	// System.Collections.IComparer System.Collections.SortedList::comparer
	Il2CppObject * ___comparer_4;
	// System.Collections.SortedList/KeyList System.Collections.SortedList::keyList
	KeyList_t1496669825 * ___keyList_5;
	// System.Collections.SortedList/ValueList System.Collections.SortedList::valueList
	ValueList_t1155029929 * ___valueList_6;
	// System.Object System.Collections.SortedList::_syncRoot
	Il2CppObject * ____syncRoot_7;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___keys_0)); }
	inline ObjectU5BU5D_t3614634134* get_keys_0() const { return ___keys_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(ObjectU5BU5D_t3614634134* value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier(&___keys_0, value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___values_1)); }
	inline ObjectU5BU5D_t3614634134* get_values_1() const { return ___values_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ObjectU5BU5D_t3614634134* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier(&___values_1, value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_comparer_4() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___comparer_4)); }
	inline Il2CppObject * get_comparer_4() const { return ___comparer_4; }
	inline Il2CppObject ** get_address_of_comparer_4() { return &___comparer_4; }
	inline void set_comparer_4(Il2CppObject * value)
	{
		___comparer_4 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_4, value);
	}

	inline static int32_t get_offset_of_keyList_5() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___keyList_5)); }
	inline KeyList_t1496669825 * get_keyList_5() const { return ___keyList_5; }
	inline KeyList_t1496669825 ** get_address_of_keyList_5() { return &___keyList_5; }
	inline void set_keyList_5(KeyList_t1496669825 * value)
	{
		___keyList_5 = value;
		Il2CppCodeGenWriteBarrier(&___keyList_5, value);
	}

	inline static int32_t get_offset_of_valueList_6() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ___valueList_6)); }
	inline ValueList_t1155029929 * get_valueList_6() const { return ___valueList_6; }
	inline ValueList_t1155029929 ** get_address_of_valueList_6() { return &___valueList_6; }
	inline void set_valueList_6(ValueList_t1155029929 * value)
	{
		___valueList_6 = value;
		Il2CppCodeGenWriteBarrier(&___valueList_6, value);
	}

	inline static int32_t get_offset_of__syncRoot_7() { return static_cast<int32_t>(offsetof(SortedList_t3004938869, ____syncRoot_7)); }
	inline Il2CppObject * get__syncRoot_7() const { return ____syncRoot_7; }
	inline Il2CppObject ** get_address_of__syncRoot_7() { return &____syncRoot_7; }
	inline void set__syncRoot_7(Il2CppObject * value)
	{
		____syncRoot_7 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_7, value);
	}
};

struct SortedList_t3004938869_StaticFields
{
public:
	// System.Object[] System.Collections.SortedList::emptyArray
	ObjectU5BU5D_t3614634134* ___emptyArray_8;

public:
	inline static int32_t get_offset_of_emptyArray_8() { return static_cast<int32_t>(offsetof(SortedList_t3004938869_StaticFields, ___emptyArray_8)); }
	inline ObjectU5BU5D_t3614634134* get_emptyArray_8() const { return ___emptyArray_8; }
	inline ObjectU5BU5D_t3614634134** get_address_of_emptyArray_8() { return &___emptyArray_8; }
	inline void set_emptyArray_8(ObjectU5BU5D_t3614634134* value)
	{
		___emptyArray_8 = value;
		Il2CppCodeGenWriteBarrier(&___emptyArray_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
