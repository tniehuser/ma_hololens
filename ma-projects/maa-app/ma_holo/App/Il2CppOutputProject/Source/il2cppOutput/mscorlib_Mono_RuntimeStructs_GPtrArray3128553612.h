﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.IntPtr
struct IntPtr_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeStructs/GPtrArray
struct  GPtrArray_t3128553612 
{
public:
	// System.IntPtr* Mono.RuntimeStructs/GPtrArray::data
	IntPtr_t* ___data_0;
	// System.Int32 Mono.RuntimeStructs/GPtrArray::len
	int32_t ___len_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(GPtrArray_t3128553612, ___data_0)); }
	inline IntPtr_t* get_data_0() const { return ___data_0; }
	inline IntPtr_t** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IntPtr_t* value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_len_1() { return static_cast<int32_t>(offsetof(GPtrArray_t3128553612, ___len_1)); }
	inline int32_t get_len_1() const { return ___len_1; }
	inline int32_t* get_address_of_len_1() { return &___len_1; }
	inline void set_len_1(int32_t value)
	{
		___len_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeStructs/GPtrArray
struct GPtrArray_t3128553612_marshaled_pinvoke
{
	IntPtr_t* ___data_0;
	int32_t ___len_1;
};
// Native definition for COM marshalling of Mono.RuntimeStructs/GPtrArray
struct GPtrArray_t3128553612_marshaled_com
{
	IntPtr_t* ___data_0;
	int32_t ___len_1;
};
