﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_SHA256Manage2029745292.h"
#include "mscorlib_System_Security_Cryptography_SHA384535510267.h"
#include "mscorlib_System_Security_Cryptography_SHA384Managed741627254.h"
#include "mscorlib_System_Security_Cryptography_SHA5122908163326.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage3949709369.h"
#include "mscorlib_System_Security_Cryptography_SignatureDescr89145500.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign2452358846.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA12477284625.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA21667091043.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA33714880827.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA52743264430.h"
#include "mscorlib_System_Security_Cryptography_DSASignature1998527418.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522.h"
#include "mscorlib_System_Security_Cryptography_TripleDES243950698.h"
#include "mscorlib_System_Security_Cryptography_TripleDESCry2380467305.h"
#include "mscorlib_System_Security_Cryptography_Utils1602189171.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3737064127.h"
#include "mscorlib_System_Security_SecurityDocument3709171241.h"
#include "mscorlib_System_Security_Util_Parser1720318543.h"
#include "mscorlib_System_Security_Util_Tokenizer395826351.h"
#include "mscorlib_System_Security_Util_Tokenizer_TokenSource403933261.h"
#include "mscorlib_System_Security_Util_Tokenizer_StringMake3542918456.h"
#include "mscorlib_System_Security_Util_Tokenizer_StreamToken424400117.h"
#include "mscorlib_System_Security_Util_TokenizerShortBlock1058612006.h"
#include "mscorlib_System_Security_Util_TokenizerStringBlock1488754793.h"
#include "mscorlib_System_Security_Util_TokenizerStream3223310371.h"
#include "mscorlib_System_SerializableAttribute2780967079.h"
#include "mscorlib_System_SharedStatics1173023064.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_StackOverflowException2044302119.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_StringSplitOptions2996162939.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "mscorlib_System_CultureAwareComparer1533343251.h"
#include "mscorlib_System_OrdinalComparer1018219584.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFre2691375565.h"
#include "mscorlib_System_SystemException3877406272.h"
#include "mscorlib_System_Text_ASCIIEncoding3022927988.h"
#include "mscorlib_System_Text_Decoder3792697818.h"
#include "mscorlib_System_Text_InternalDecoderBestFitFallbac1609214544.h"
#include "mscorlib_System_Text_InternalDecoderBestFitFallback925729930.h"
#include "mscorlib_System_Text_DecoderExceptionFallback944865245.h"
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffer40651649.h"
#include "mscorlib_System_Text_DecoderFallbackException561423693.h"
#include "mscorlib_System_Text_DecoderFallback1715117820.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer4206371382.h"
#include "mscorlib_System_Text_DecoderNLS1749238319.h"
#include "mscorlib_System_Text_DecoderReplacementFallback3042394152.h"
#include "mscorlib_System_Text_DecoderReplacementFallbackBuf3471122670.h"
#include "mscorlib_System_Text_Encoder751367874.h"
#include "mscorlib_System_Text_InternalEncoderBestFitFallbac2954925084.h"
#include "mscorlib_System_Text_InternalEncoderBestFitFallbac2918012642.h"
#include "mscorlib_System_Text_EncoderExceptionFallback1520212111.h"
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffe2027398699.h"
#include "mscorlib_System_Text_EncoderFallbackException1447984975.h"
#include "mscorlib_System_Text_EncoderFallback1756452756.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer3883615514.h"
#include "mscorlib_System_Text_EncoderNLS1944053629.h"
#include "mscorlib_System_Text_EncoderReplacementFallback4228544112.h"
#include "mscorlib_System_Text_EncoderReplacementFallbackBuf1313574570.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Text_Encoding_DefaultEncoder2222564313.h"
#include "mscorlib_System_Text_Encoding_DefaultDecoder780313315.h"
#include "mscorlib_System_Text_Encoding_EncodingCharBuffer1740573429.h"
#include "mscorlib_System_Text_Encoding_EncodingByteBuffer3578698359.h"
#include "mscorlib_System_Text_EncodingProvider2755886406.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Text_StringBuilderCache3694921148.h"
#include "mscorlib_System_Text_UnicodeEncoding4081757012.h"
#include "mscorlib_System_Text_UnicodeEncoding_Decoder1541418315.h"
#include "mscorlib_System_Text_UTF32Encoding549530865.h"
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder2654498546.h"
#include "mscorlib_System_Text_UTF7Encoding741406939.h"
#include "mscorlib_System_Text_UTF7Encoding_Decoder2984530476.h"
#include "mscorlib_System_Text_UTF7Encoding_Encoder130743804.h"
#include "mscorlib_System_Text_UTF7Encoding_DecoderUTF7Fallb3319501624.h"
#include "mscorlib_System_Text_UTF7Encoding_DecoderUTF7Fallb1271633598.h"
#include "mscorlib_System_Text_UTF8Encoding111055448.h"
#include "mscorlib_System_Text_UTF8Encoding_UTF8Encoder3863367316.h"
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder2447592404.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_Threading_CancellationTokenRegistr1708859357.h"
#include "mscorlib_System_Threading_CancellationTokenSource1795361321.h"
#include "mscorlib_System_Threading_CancellationCallbackCore3564269970.h"
#include "mscorlib_System_Threading_CancellationCallbackInfo1473383178.h"
#include "mscorlib_System_Threading_LazyInitializer3555830498.h"
#include "mscorlib_System_Threading_ManualResetEventSlim964365518.h"
#include "mscorlib_System_Threading_SemaphoreSlim461808439.h"
#include "mscorlib_System_Threading_SemaphoreSlim_TaskNode453295914.h"
#include "mscorlib_System_Threading_SemaphoreSlim_U3CWaitUnt2777167442.h"
#include "mscorlib_System_Threading_SpinLock2136334209.h"
#include "mscorlib_System_Threading_SpinLock_SystemThreading3340967512.h"
#include "mscorlib_System_Threading_SpinWait3991132955.h"
#include "mscorlib_System_Threading_PlatformHelper1972808937.h"
#include "mscorlib_System_Threading_TimeoutHelper2679076275.h"
#include "mscorlib_System_Threading_Tasks_CausalityTraceLevel536076440.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize700 = { sizeof (SHA256Managed_t2029745292), -1, sizeof(SHA256Managed_t2029745292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable700[5] = 
{
	SHA256Managed_t2029745292::get_offset_of__buffer_4(),
	SHA256Managed_t2029745292::get_offset_of__count_5(),
	SHA256Managed_t2029745292::get_offset_of__stateSHA256_6(),
	SHA256Managed_t2029745292::get_offset_of__W_7(),
	SHA256Managed_t2029745292_StaticFields::get_offset_of__K_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize701 = { sizeof (SHA384_t535510267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize702 = { sizeof (SHA384Managed_t741627254), -1, sizeof(SHA384Managed_t741627254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable702[5] = 
{
	SHA384Managed_t741627254::get_offset_of__buffer_4(),
	SHA384Managed_t741627254::get_offset_of__count_5(),
	SHA384Managed_t741627254::get_offset_of__stateSHA384_6(),
	SHA384Managed_t741627254::get_offset_of__W_7(),
	SHA384Managed_t741627254_StaticFields::get_offset_of__K_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize703 = { sizeof (SHA512_t2908163326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize704 = { sizeof (SHA512Managed_t3949709369), -1, sizeof(SHA512Managed_t3949709369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable704[5] = 
{
	SHA512Managed_t3949709369::get_offset_of__buffer_4(),
	SHA512Managed_t3949709369::get_offset_of__count_5(),
	SHA512Managed_t3949709369::get_offset_of__stateSHA512_6(),
	SHA512Managed_t3949709369::get_offset_of__W_7(),
	SHA512Managed_t3949709369_StaticFields::get_offset_of__K_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize705 = { sizeof (SignatureDescription_t89145500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable705[4] = 
{
	SignatureDescription_t89145500::get_offset_of__strKey_0(),
	SignatureDescription_t89145500::get_offset_of__strDigest_1(),
	SignatureDescription_t89145500::get_offset_of__strFormatter_2(),
	SignatureDescription_t89145500::get_offset_of__strDeformatter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize706 = { sizeof (RSAPKCS1SignatureDescription_t2452358846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable706[1] = 
{
	RSAPKCS1SignatureDescription_t2452358846::get_offset_of__hashAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize707 = { sizeof (RSAPKCS1SHA1SignatureDescription_t2477284625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize708 = { sizeof (RSAPKCS1SHA256SignatureDescription_t1667091043), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize709 = { sizeof (RSAPKCS1SHA384SignatureDescription_t3714880827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize710 = { sizeof (RSAPKCS1SHA512SignatureDescription_t2743264430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize711 = { sizeof (DSASignatureDescription_t1998527418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize712 = { sizeof (SymmetricAlgorithm_t1108166522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable712[9] = 
{
	SymmetricAlgorithm_t1108166522::get_offset_of_BlockSizeValue_0(),
	SymmetricAlgorithm_t1108166522::get_offset_of_FeedbackSizeValue_1(),
	SymmetricAlgorithm_t1108166522::get_offset_of_IVValue_2(),
	SymmetricAlgorithm_t1108166522::get_offset_of_KeyValue_3(),
	SymmetricAlgorithm_t1108166522::get_offset_of_LegalBlockSizesValue_4(),
	SymmetricAlgorithm_t1108166522::get_offset_of_LegalKeySizesValue_5(),
	SymmetricAlgorithm_t1108166522::get_offset_of_KeySizeValue_6(),
	SymmetricAlgorithm_t1108166522::get_offset_of_ModeValue_7(),
	SymmetricAlgorithm_t1108166522::get_offset_of_PaddingValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize713 = { sizeof (TripleDES_t243950698), -1, sizeof(TripleDES_t243950698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable713[2] = 
{
	TripleDES_t243950698_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	TripleDES_t243950698_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize714 = { sizeof (TripleDESCryptoServiceProvider_t2380467305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize715 = { sizeof (Utils_t1602189171), -1, sizeof(Utils_t1602189171_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable715[1] = 
{
	Utils_t1602189171_StaticFields::get_offset_of__rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize716 = { sizeof (OidGroup_t3737064127)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable716[13] = 
{
	OidGroup_t3737064127::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize717 = { sizeof (SecurityDocument_t3709171241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable717[1] = 
{
	SecurityDocument_t3709171241::get_offset_of_m_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize718 = { sizeof (Parser_t1720318543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable718[2] = 
{
	Parser_t1720318543::get_offset_of__doc_0(),
	Parser_t1720318543::get_offset_of__t_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize719 = { sizeof (Tokenizer_t395826351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable719[16] = 
{
	Tokenizer_t395826351::get_offset_of_LineNo_0(),
	Tokenizer_t395826351::get_offset_of__inProcessingTag_1(),
	Tokenizer_t395826351::get_offset_of__inBytes_2(),
	Tokenizer_t395826351::get_offset_of__inChars_3(),
	Tokenizer_t395826351::get_offset_of__inString_4(),
	Tokenizer_t395826351::get_offset_of__inIndex_5(),
	Tokenizer_t395826351::get_offset_of__inSize_6(),
	Tokenizer_t395826351::get_offset_of__inSavedCharacter_7(),
	Tokenizer_t395826351::get_offset_of__inTokenSource_8(),
	Tokenizer_t395826351::get_offset_of__inTokenReader_9(),
	Tokenizer_t395826351::get_offset_of__maker_10(),
	Tokenizer_t395826351::get_offset_of__searchStrings_11(),
	Tokenizer_t395826351::get_offset_of__replaceStrings_12(),
	Tokenizer_t395826351::get_offset_of__inNestedIndex_13(),
	Tokenizer_t395826351::get_offset_of__inNestedSize_14(),
	Tokenizer_t395826351::get_offset_of__inNestedString_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize720 = { sizeof (TokenSource_t403933261)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable720[8] = 
{
	TokenSource_t403933261::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize721 = { sizeof (StringMaker_t3542918456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable721[6] = 
{
	StringMaker_t3542918456::get_offset_of_aStrings_0(),
	StringMaker_t3542918456::get_offset_of_cStringsMax_1(),
	StringMaker_t3542918456::get_offset_of_cStringsUsed_2(),
	StringMaker_t3542918456::get_offset_of__outStringBuilder_3(),
	StringMaker_t3542918456::get_offset_of__outChars_4(),
	StringMaker_t3542918456::get_offset_of__outIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize722 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize723 = { sizeof (StreamTokenReader_t424400117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable723[2] = 
{
	StreamTokenReader_t424400117::get_offset_of__in_0(),
	StreamTokenReader_t424400117::get_offset_of__numCharRead_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize724 = { sizeof (TokenizerShortBlock_t1058612006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable724[2] = 
{
	TokenizerShortBlock_t1058612006::get_offset_of_m_block_0(),
	TokenizerShortBlock_t1058612006::get_offset_of_m_next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize725 = { sizeof (TokenizerStringBlock_t1488754793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable725[2] = 
{
	TokenizerStringBlock_t1488754793::get_offset_of_m_block_0(),
	TokenizerStringBlock_t1488754793::get_offset_of_m_next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize726 = { sizeof (TokenizerStream_t3223310371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable726[8] = 
{
	TokenizerStream_t3223310371::get_offset_of_m_countTokens_0(),
	TokenizerStream_t3223310371::get_offset_of_m_headTokens_1(),
	TokenizerStream_t3223310371::get_offset_of_m_lastTokens_2(),
	TokenizerStream_t3223310371::get_offset_of_m_currentTokens_3(),
	TokenizerStream_t3223310371::get_offset_of_m_indexTokens_4(),
	TokenizerStream_t3223310371::get_offset_of_m_headStrings_5(),
	TokenizerStream_t3223310371::get_offset_of_m_currentStrings_6(),
	TokenizerStream_t3223310371::get_offset_of_m_indexStrings_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize727 = { sizeof (SerializableAttribute_t2780967079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize728 = { sizeof (SharedStatics_t1173023064), -1, sizeof(SharedStatics_t1173023064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable728[2] = 
{
	SharedStatics_t1173023064_StaticFields::get_offset_of__sharedStatics_0(),
	SharedStatics_t1173023064::get_offset_of__maker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize729 = { sizeof (Single_t2076509932)+ sizeof (Il2CppObject), sizeof(float), 0, 0 };
extern const int32_t g_FieldOffsetTable729[7] = 
{
	Single_t2076509932::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize730 = { sizeof (StackOverflowException_t2044302119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize731 = { sizeof (String_t), sizeof(char*), sizeof(String_t_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable731[8] = 
{
	String_t::get_offset_of_m_stringLength_0(),
	String_t::get_offset_of_m_firstChar_1(),
	0,
	0,
	0,
	String_t_StaticFields::get_offset_of_Empty_5(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize732 = { sizeof (StringSplitOptions_t2996162939)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable732[3] = 
{
	StringSplitOptions_t2996162939::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize733 = { sizeof (StringComparer_t1574862926), -1, sizeof(StringComparer_t1574862926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable733[4] = 
{
	StringComparer_t1574862926_StaticFields::get_offset_of__invariantCulture_0(),
	StringComparer_t1574862926_StaticFields::get_offset_of__invariantCultureIgnoreCase_1(),
	StringComparer_t1574862926_StaticFields::get_offset_of__ordinal_2(),
	StringComparer_t1574862926_StaticFields::get_offset_of__ordinalIgnoreCase_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize734 = { sizeof (CultureAwareComparer_t1533343251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable734[2] = 
{
	CultureAwareComparer_t1533343251::get_offset_of__compareInfo_4(),
	CultureAwareComparer_t1533343251::get_offset_of__ignoreCase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize735 = { sizeof (OrdinalComparer_t1018219584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable735[1] = 
{
	OrdinalComparer_t1018219584::get_offset_of__ignoreCase_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize736 = { sizeof (StringFreezingAttribute_t2691375565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize737 = { sizeof (SystemException_t3877406272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize738 = { sizeof (ASCIIEncoding_t3022927988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize739 = { sizeof (Decoder_t3792697818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable739[2] = 
{
	Decoder_t3792697818::get_offset_of_m_fallback_0(),
	Decoder_t3792697818::get_offset_of_m_fallbackBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize740 = { sizeof (InternalDecoderBestFitFallback_t1609214544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable740[3] = 
{
	InternalDecoderBestFitFallback_t1609214544::get_offset_of_encoding_4(),
	InternalDecoderBestFitFallback_t1609214544::get_offset_of_arrayBestFit_5(),
	InternalDecoderBestFitFallback_t1609214544::get_offset_of_cReplacement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize741 = { sizeof (InternalDecoderBestFitFallbackBuffer_t925729930), -1, sizeof(InternalDecoderBestFitFallbackBuffer_t925729930_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable741[5] = 
{
	InternalDecoderBestFitFallbackBuffer_t925729930::get_offset_of_cBestFit_2(),
	InternalDecoderBestFitFallbackBuffer_t925729930::get_offset_of_iCount_3(),
	InternalDecoderBestFitFallbackBuffer_t925729930::get_offset_of_iSize_4(),
	InternalDecoderBestFitFallbackBuffer_t925729930::get_offset_of_oFallback_5(),
	InternalDecoderBestFitFallbackBuffer_t925729930_StaticFields::get_offset_of_s_InternalSyncObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize742 = { sizeof (DecoderExceptionFallback_t944865245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize743 = { sizeof (DecoderExceptionFallbackBuffer_t40651649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize744 = { sizeof (DecoderFallbackException_t561423693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable744[2] = 
{
	DecoderFallbackException_t561423693::get_offset_of_bytesUnknown_17(),
	DecoderFallbackException_t561423693::get_offset_of_index_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize745 = { sizeof (DecoderFallback_t1715117820), -1, sizeof(DecoderFallback_t1715117820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable745[4] = 
{
	DecoderFallback_t1715117820::get_offset_of_bIsMicrosoftBestFitFallback_0(),
	DecoderFallback_t1715117820_StaticFields::get_offset_of_replacementFallback_1(),
	DecoderFallback_t1715117820_StaticFields::get_offset_of_exceptionFallback_2(),
	DecoderFallback_t1715117820_StaticFields::get_offset_of_s_InternalSyncObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize746 = { sizeof (DecoderFallbackBuffer_t4206371382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable746[2] = 
{
	DecoderFallbackBuffer_t4206371382::get_offset_of_byteStart_0(),
	DecoderFallbackBuffer_t4206371382::get_offset_of_charEnd_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize747 = { sizeof (DecoderNLS_t1749238319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable747[4] = 
{
	DecoderNLS_t1749238319::get_offset_of_m_encoding_2(),
	DecoderNLS_t1749238319::get_offset_of_m_mustFlush_3(),
	DecoderNLS_t1749238319::get_offset_of_m_throwOnOverflow_4(),
	DecoderNLS_t1749238319::get_offset_of_m_bytesUsed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize748 = { sizeof (DecoderReplacementFallback_t3042394152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable748[1] = 
{
	DecoderReplacementFallback_t3042394152::get_offset_of_strDefault_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize749 = { sizeof (DecoderReplacementFallbackBuffer_t3471122670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable749[3] = 
{
	DecoderReplacementFallbackBuffer_t3471122670::get_offset_of_strDefault_2(),
	DecoderReplacementFallbackBuffer_t3471122670::get_offset_of_fallbackCount_3(),
	DecoderReplacementFallbackBuffer_t3471122670::get_offset_of_fallbackIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize750 = { sizeof (Encoder_t751367874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable750[2] = 
{
	Encoder_t751367874::get_offset_of_m_fallback_0(),
	Encoder_t751367874::get_offset_of_m_fallbackBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize751 = { sizeof (InternalEncoderBestFitFallback_t2954925084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable751[2] = 
{
	InternalEncoderBestFitFallback_t2954925084::get_offset_of_encoding_4(),
	InternalEncoderBestFitFallback_t2954925084::get_offset_of_arrayBestFit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize752 = { sizeof (InternalEncoderBestFitFallbackBuffer_t2918012642), -1, sizeof(InternalEncoderBestFitFallbackBuffer_t2918012642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable752[5] = 
{
	InternalEncoderBestFitFallbackBuffer_t2918012642::get_offset_of_cBestFit_7(),
	InternalEncoderBestFitFallbackBuffer_t2918012642::get_offset_of_oFallback_8(),
	InternalEncoderBestFitFallbackBuffer_t2918012642::get_offset_of_iCount_9(),
	InternalEncoderBestFitFallbackBuffer_t2918012642::get_offset_of_iSize_10(),
	InternalEncoderBestFitFallbackBuffer_t2918012642_StaticFields::get_offset_of_s_InternalSyncObject_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize753 = { sizeof (EncoderExceptionFallback_t1520212111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize754 = { sizeof (EncoderExceptionFallbackBuffer_t2027398699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize755 = { sizeof (EncoderFallbackException_t1447984975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable755[4] = 
{
	EncoderFallbackException_t1447984975::get_offset_of_charUnknown_17(),
	EncoderFallbackException_t1447984975::get_offset_of_charUnknownHigh_18(),
	EncoderFallbackException_t1447984975::get_offset_of_charUnknownLow_19(),
	EncoderFallbackException_t1447984975::get_offset_of_index_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize756 = { sizeof (EncoderFallback_t1756452756), -1, sizeof(EncoderFallback_t1756452756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable756[4] = 
{
	EncoderFallback_t1756452756::get_offset_of_bIsMicrosoftBestFitFallback_0(),
	EncoderFallback_t1756452756_StaticFields::get_offset_of_replacementFallback_1(),
	EncoderFallback_t1756452756_StaticFields::get_offset_of_exceptionFallback_2(),
	EncoderFallback_t1756452756_StaticFields::get_offset_of_s_InternalSyncObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize757 = { sizeof (EncoderFallbackBuffer_t3883615514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable757[7] = 
{
	EncoderFallbackBuffer_t3883615514::get_offset_of_charStart_0(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_charEnd_1(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_encoder_2(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_setEncoder_3(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_bUsedEncoder_4(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_bFallingBack_5(),
	EncoderFallbackBuffer_t3883615514::get_offset_of_iRecursionCount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize758 = { sizeof (EncoderNLS_t1944053629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable758[5] = 
{
	EncoderNLS_t1944053629::get_offset_of_charLeftOver_2(),
	EncoderNLS_t1944053629::get_offset_of_m_encoding_3(),
	EncoderNLS_t1944053629::get_offset_of_m_mustFlush_4(),
	EncoderNLS_t1944053629::get_offset_of_m_throwOnOverflow_5(),
	EncoderNLS_t1944053629::get_offset_of_m_charsUsed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize759 = { sizeof (EncoderReplacementFallback_t4228544112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable759[1] = 
{
	EncoderReplacementFallback_t4228544112::get_offset_of_strDefault_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize760 = { sizeof (EncoderReplacementFallbackBuffer_t1313574570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable760[3] = 
{
	EncoderReplacementFallbackBuffer_t1313574570::get_offset_of_strDefault_7(),
	EncoderReplacementFallbackBuffer_t1313574570::get_offset_of_fallbackCount_8(),
	EncoderReplacementFallbackBuffer_t1313574570::get_offset_of_fallbackIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize761 = { sizeof (Encoding_t663144255), -1, sizeof(Encoding_t663144255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable761[15] = 
{
	Encoding_t663144255_StaticFields::get_offset_of_defaultEncoding_0(),
	Encoding_t663144255_StaticFields::get_offset_of_unicodeEncoding_1(),
	Encoding_t663144255_StaticFields::get_offset_of_bigEndianUnicode_2(),
	Encoding_t663144255_StaticFields::get_offset_of_utf7Encoding_3(),
	Encoding_t663144255_StaticFields::get_offset_of_utf8Encoding_4(),
	Encoding_t663144255_StaticFields::get_offset_of_utf32Encoding_5(),
	Encoding_t663144255_StaticFields::get_offset_of_asciiEncoding_6(),
	Encoding_t663144255_StaticFields::get_offset_of_latin1Encoding_7(),
	Encoding_t663144255_StaticFields::get_offset_of_encodings_8(),
	Encoding_t663144255::get_offset_of_m_codePage_9(),
	Encoding_t663144255::get_offset_of_dataItem_10(),
	Encoding_t663144255::get_offset_of_m_isReadOnly_11(),
	Encoding_t663144255::get_offset_of_encoderFallback_12(),
	Encoding_t663144255::get_offset_of_decoderFallback_13(),
	Encoding_t663144255_StaticFields::get_offset_of_s_InternalSyncObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize762 = { sizeof (DefaultEncoder_t2222564313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable762[3] = 
{
	DefaultEncoder_t2222564313::get_offset_of_m_encoding_2(),
	DefaultEncoder_t2222564313::get_offset_of_m_hasInitializedEncoding_3(),
	DefaultEncoder_t2222564313::get_offset_of_charLeftOver_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize763 = { sizeof (DefaultDecoder_t780313315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable763[2] = 
{
	DefaultDecoder_t780313315::get_offset_of_m_encoding_2(),
	DefaultDecoder_t780313315::get_offset_of_m_hasInitializedEncoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize764 = { sizeof (EncodingCharBuffer_t1740573429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable764[10] = 
{
	EncodingCharBuffer_t1740573429::get_offset_of_chars_0(),
	EncodingCharBuffer_t1740573429::get_offset_of_charStart_1(),
	EncodingCharBuffer_t1740573429::get_offset_of_charEnd_2(),
	EncodingCharBuffer_t1740573429::get_offset_of_charCountResult_3(),
	EncodingCharBuffer_t1740573429::get_offset_of_enc_4(),
	EncodingCharBuffer_t1740573429::get_offset_of_decoder_5(),
	EncodingCharBuffer_t1740573429::get_offset_of_byteStart_6(),
	EncodingCharBuffer_t1740573429::get_offset_of_byteEnd_7(),
	EncodingCharBuffer_t1740573429::get_offset_of_bytes_8(),
	EncodingCharBuffer_t1740573429::get_offset_of_fallbackBuffer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize765 = { sizeof (EncodingByteBuffer_t3578698359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable765[10] = 
{
	EncodingByteBuffer_t3578698359::get_offset_of_bytes_0(),
	EncodingByteBuffer_t3578698359::get_offset_of_byteStart_1(),
	EncodingByteBuffer_t3578698359::get_offset_of_byteEnd_2(),
	EncodingByteBuffer_t3578698359::get_offset_of_chars_3(),
	EncodingByteBuffer_t3578698359::get_offset_of_charStart_4(),
	EncodingByteBuffer_t3578698359::get_offset_of_charEnd_5(),
	EncodingByteBuffer_t3578698359::get_offset_of_byteCountResult_6(),
	EncodingByteBuffer_t3578698359::get_offset_of_enc_7(),
	EncodingByteBuffer_t3578698359::get_offset_of_encoder_8(),
	EncodingByteBuffer_t3578698359::get_offset_of_fallbackBuffer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize766 = { sizeof (EncodingProvider_t2755886406), -1, sizeof(EncodingProvider_t2755886406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable766[2] = 
{
	EncodingProvider_t2755886406_StaticFields::get_offset_of_s_InternalSyncObject_0(),
	EncodingProvider_t2755886406_StaticFields::get_offset_of_s_providers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize767 = { sizeof (StringBuilder_t1221177846), sizeof(char*), 0, 0 };
extern const int32_t g_FieldOffsetTable767[11] = 
{
	StringBuilder_t1221177846::get_offset_of_m_ChunkChars_0(),
	StringBuilder_t1221177846::get_offset_of_m_ChunkPrevious_1(),
	StringBuilder_t1221177846::get_offset_of_m_ChunkLength_2(),
	StringBuilder_t1221177846::get_offset_of_m_ChunkOffset_3(),
	StringBuilder_t1221177846::get_offset_of_m_MaxCapacity_4(),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize768 = { sizeof (StringBuilderCache_t3694921148), -1, 0, sizeof(StringBuilderCache_t3694921148_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable768[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize769 = { sizeof (UnicodeEncoding_t4081757012), -1, sizeof(UnicodeEncoding_t4081757012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable769[4] = 
{
	UnicodeEncoding_t4081757012::get_offset_of_isThrowException_15(),
	UnicodeEncoding_t4081757012::get_offset_of_bigEndian_16(),
	UnicodeEncoding_t4081757012::get_offset_of_byteOrderMark_17(),
	UnicodeEncoding_t4081757012_StaticFields::get_offset_of_highLowPatternMask_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize770 = { sizeof (Decoder_t1541418315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable770[2] = 
{
	Decoder_t1541418315::get_offset_of_lastByte_6(),
	Decoder_t1541418315::get_offset_of_lastChar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize771 = { sizeof (UTF32Encoding_t549530865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable771[3] = 
{
	UTF32Encoding_t549530865::get_offset_of_emitUTF32ByteOrderMark_15(),
	UTF32Encoding_t549530865::get_offset_of_isThrowException_16(),
	UTF32Encoding_t549530865::get_offset_of_bigEndian_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize772 = { sizeof (UTF32Decoder_t2654498546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable772[2] = 
{
	UTF32Decoder_t2654498546::get_offset_of_iChar_6(),
	UTF32Decoder_t2654498546::get_offset_of_readByteCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize773 = { sizeof (UTF7Encoding_t741406939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable773[4] = 
{
	UTF7Encoding_t741406939::get_offset_of_base64Bytes_15(),
	UTF7Encoding_t741406939::get_offset_of_base64Values_16(),
	UTF7Encoding_t741406939::get_offset_of_directEncode_17(),
	UTF7Encoding_t741406939::get_offset_of_m_allowOptionals_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize774 = { sizeof (Decoder_t2984530476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable774[3] = 
{
	Decoder_t2984530476::get_offset_of_bits_6(),
	Decoder_t2984530476::get_offset_of_bitCount_7(),
	Decoder_t2984530476::get_offset_of_firstByte_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize775 = { sizeof (Encoder_t130743804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable775[2] = 
{
	Encoder_t130743804::get_offset_of_bits_7(),
	Encoder_t130743804::get_offset_of_bitCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize776 = { sizeof (DecoderUTF7Fallback_t3319501624), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize777 = { sizeof (DecoderUTF7FallbackBuffer_t1271633598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable777[3] = 
{
	DecoderUTF7FallbackBuffer_t1271633598::get_offset_of_cFallback_2(),
	DecoderUTF7FallbackBuffer_t1271633598::get_offset_of_iCount_3(),
	DecoderUTF7FallbackBuffer_t1271633598::get_offset_of_iSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize778 = { sizeof (UTF8Encoding_t111055448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable778[2] = 
{
	UTF8Encoding_t111055448::get_offset_of_emitUTF8Identifier_15(),
	UTF8Encoding_t111055448::get_offset_of_isThrowException_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize779 = { sizeof (UTF8Encoder_t3863367316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable779[1] = 
{
	UTF8Encoder_t3863367316::get_offset_of_surrogateChar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize780 = { sizeof (UTF8Decoder_t2447592404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable780[1] = 
{
	UTF8Decoder_t2447592404::get_offset_of_bits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize781 = { sizeof (CancellationToken_t1851405782)+ sizeof (Il2CppObject), -1, sizeof(CancellationToken_t1851405782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable781[2] = 
{
	CancellationToken_t1851405782::get_offset_of_m_source_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CancellationToken_t1851405782_StaticFields::get_offset_of_s_ActionToActionObjShunt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize782 = { sizeof (CancellationTokenRegistration_t1708859357)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable782[2] = 
{
	CancellationTokenRegistration_t1708859357::get_offset_of_m_callbackInfo_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CancellationTokenRegistration_t1708859357::get_offset_of_m_registrationInfo_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize783 = { sizeof (CancellationTokenSource_t1795361321), -1, sizeof(CancellationTokenSource_t1795361321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable783[13] = 
{
	CancellationTokenSource_t1795361321_StaticFields::get_offset_of__staticSource_Set_0(),
	CancellationTokenSource_t1795361321_StaticFields::get_offset_of__staticSource_NotCancelable_1(),
	CancellationTokenSource_t1795361321_StaticFields::get_offset_of_s_nLists_2(),
	CancellationTokenSource_t1795361321::get_offset_of_m_kernelEvent_3(),
	CancellationTokenSource_t1795361321::get_offset_of_m_registeredCallbacksLists_4(),
	CancellationTokenSource_t1795361321::get_offset_of_m_state_5(),
	CancellationTokenSource_t1795361321::get_offset_of_m_threadIDExecutingCallbacks_6(),
	CancellationTokenSource_t1795361321::get_offset_of_m_disposed_7(),
	CancellationTokenSource_t1795361321::get_offset_of_m_linkingRegistrations_8(),
	CancellationTokenSource_t1795361321_StaticFields::get_offset_of_s_LinkedTokenCancelDelegate_9(),
	CancellationTokenSource_t1795361321::get_offset_of_m_executingCallback_10(),
	CancellationTokenSource_t1795361321::get_offset_of_m_timer_11(),
	CancellationTokenSource_t1795361321_StaticFields::get_offset_of_s_timerCallback_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize784 = { sizeof (CancellationCallbackCoreWorkArguments_t3564269970)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable784[2] = 
{
	CancellationCallbackCoreWorkArguments_t3564269970::get_offset_of_m_currArrayFragment_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CancellationCallbackCoreWorkArguments_t3564269970::get_offset_of_m_currArrayIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize785 = { sizeof (CancellationCallbackInfo_t1473383178), -1, sizeof(CancellationCallbackInfo_t1473383178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable785[6] = 
{
	CancellationCallbackInfo_t1473383178::get_offset_of_Callback_0(),
	CancellationCallbackInfo_t1473383178::get_offset_of_StateForCallback_1(),
	CancellationCallbackInfo_t1473383178::get_offset_of_TargetSyncContext_2(),
	CancellationCallbackInfo_t1473383178::get_offset_of_TargetExecutionContext_3(),
	CancellationCallbackInfo_t1473383178::get_offset_of_CancellationTokenSource_4(),
	CancellationCallbackInfo_t1473383178_StaticFields::get_offset_of_s_executionContextCallback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize786 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable786[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize787 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable787[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize788 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable788[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize789 = { sizeof (LazyInitializer_t3555830498), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize790 = { sizeof (ManualResetEventSlim_t964365518), -1, sizeof(ManualResetEventSlim_t964365518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable790[4] = 
{
	ManualResetEventSlim_t964365518::get_offset_of_m_lock_0(),
	ManualResetEventSlim_t964365518::get_offset_of_m_eventObj_1(),
	ManualResetEventSlim_t964365518::get_offset_of_m_combinedState_2(),
	ManualResetEventSlim_t964365518_StaticFields::get_offset_of_s_cancellationTokenCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize791 = { sizeof (SemaphoreSlim_t461808439), -1, sizeof(SemaphoreSlim_t461808439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable791[9] = 
{
	SemaphoreSlim_t461808439::get_offset_of_m_currentCount_0(),
	SemaphoreSlim_t461808439::get_offset_of_m_maxCount_1(),
	SemaphoreSlim_t461808439::get_offset_of_m_waitCount_2(),
	SemaphoreSlim_t461808439::get_offset_of_m_lockObj_3(),
	SemaphoreSlim_t461808439::get_offset_of_m_waitHandle_4(),
	SemaphoreSlim_t461808439::get_offset_of_m_asyncHead_5(),
	SemaphoreSlim_t461808439::get_offset_of_m_asyncTail_6(),
	SemaphoreSlim_t461808439_StaticFields::get_offset_of_s_trueTask_7(),
	SemaphoreSlim_t461808439_StaticFields::get_offset_of_s_cancellationTokenCanceledEventHandler_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize792 = { sizeof (TaskNode_t453295914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable792[2] = 
{
	TaskNode_t453295914::get_offset_of_Prev_27(),
	TaskNode_t453295914::get_offset_of_Next_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize793 = { sizeof (U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable793[12] = 
{
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_asyncWaiter_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_cancellationToken_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U3CctsU3E__1_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_millisecondsTimeout_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U3CwaitCompletedU3E__2_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24locvar0_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24locvar1_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24this_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24builder_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24PC_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24awaiter0_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442::get_offset_of_U24awaiter1_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize794 = { sizeof (SpinLock_t2136334209)+ sizeof (Il2CppObject), -1, sizeof(SpinLock_t2136334209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable794[2] = 
{
	SpinLock_t2136334209::get_offset_of_m_owner_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpinLock_t2136334209_StaticFields::get_offset_of_MAXIMUM_WAITERS_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize795 = { sizeof (SystemThreading_SpinLockDebugView_t3340967512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize796 = { sizeof (SpinWait_t3991132955)+ sizeof (Il2CppObject), sizeof(SpinWait_t3991132955 ), 0, 0 };
extern const int32_t g_FieldOffsetTable796[1] = 
{
	SpinWait_t3991132955::get_offset_of_m_count_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize797 = { sizeof (PlatformHelper_t1972808937), -1, sizeof(PlatformHelper_t1972808937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable797[2] = 
{
	PlatformHelper_t1972808937_StaticFields::get_offset_of_s_processorCount_0(),
	PlatformHelper_t1972808937_StaticFields::get_offset_of_s_lastProcessorCountRefreshTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize798 = { sizeof (TimeoutHelper_t2679076275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize799 = { sizeof (CausalityTraceLevel_t536076440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable799[4] = 
{
	CausalityTraceLevel_t536076440::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
