﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/ElementContent
struct  ElementContent_t3043903854  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XdrBuilder/ElementContent::_ElementDecl
	SchemaElementDecl_t1940851905 * ____ElementDecl_0;
	// System.Int32 System.Xml.Schema.XdrBuilder/ElementContent::_ContentAttr
	int32_t ____ContentAttr_1;
	// System.Int32 System.Xml.Schema.XdrBuilder/ElementContent::_OrderAttr
	int32_t ____OrderAttr_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_MasterGroupRequired
	bool ____MasterGroupRequired_3;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_ExistTerminal
	bool ____ExistTerminal_4;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_AllowDataType
	bool ____AllowDataType_5;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_HasDataType
	bool ____HasDataType_6;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_HasType
	bool ____HasType_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/ElementContent::_EnumerationRequired
	bool ____EnumerationRequired_8;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MinVal
	uint32_t ____MinVal_9;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MaxVal
	uint32_t ____MaxVal_10;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MaxLength
	uint32_t ____MaxLength_11;
	// System.UInt32 System.Xml.Schema.XdrBuilder/ElementContent::_MinLength
	uint32_t ____MinLength_12;
	// System.Collections.Hashtable System.Xml.Schema.XdrBuilder/ElementContent::_AttDefList
	Hashtable_t909839986 * ____AttDefList_13;

public:
	inline static int32_t get_offset_of__ElementDecl_0() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____ElementDecl_0)); }
	inline SchemaElementDecl_t1940851905 * get__ElementDecl_0() const { return ____ElementDecl_0; }
	inline SchemaElementDecl_t1940851905 ** get_address_of__ElementDecl_0() { return &____ElementDecl_0; }
	inline void set__ElementDecl_0(SchemaElementDecl_t1940851905 * value)
	{
		____ElementDecl_0 = value;
		Il2CppCodeGenWriteBarrier(&____ElementDecl_0, value);
	}

	inline static int32_t get_offset_of__ContentAttr_1() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____ContentAttr_1)); }
	inline int32_t get__ContentAttr_1() const { return ____ContentAttr_1; }
	inline int32_t* get_address_of__ContentAttr_1() { return &____ContentAttr_1; }
	inline void set__ContentAttr_1(int32_t value)
	{
		____ContentAttr_1 = value;
	}

	inline static int32_t get_offset_of__OrderAttr_2() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____OrderAttr_2)); }
	inline int32_t get__OrderAttr_2() const { return ____OrderAttr_2; }
	inline int32_t* get_address_of__OrderAttr_2() { return &____OrderAttr_2; }
	inline void set__OrderAttr_2(int32_t value)
	{
		____OrderAttr_2 = value;
	}

	inline static int32_t get_offset_of__MasterGroupRequired_3() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____MasterGroupRequired_3)); }
	inline bool get__MasterGroupRequired_3() const { return ____MasterGroupRequired_3; }
	inline bool* get_address_of__MasterGroupRequired_3() { return &____MasterGroupRequired_3; }
	inline void set__MasterGroupRequired_3(bool value)
	{
		____MasterGroupRequired_3 = value;
	}

	inline static int32_t get_offset_of__ExistTerminal_4() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____ExistTerminal_4)); }
	inline bool get__ExistTerminal_4() const { return ____ExistTerminal_4; }
	inline bool* get_address_of__ExistTerminal_4() { return &____ExistTerminal_4; }
	inline void set__ExistTerminal_4(bool value)
	{
		____ExistTerminal_4 = value;
	}

	inline static int32_t get_offset_of__AllowDataType_5() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____AllowDataType_5)); }
	inline bool get__AllowDataType_5() const { return ____AllowDataType_5; }
	inline bool* get_address_of__AllowDataType_5() { return &____AllowDataType_5; }
	inline void set__AllowDataType_5(bool value)
	{
		____AllowDataType_5 = value;
	}

	inline static int32_t get_offset_of__HasDataType_6() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____HasDataType_6)); }
	inline bool get__HasDataType_6() const { return ____HasDataType_6; }
	inline bool* get_address_of__HasDataType_6() { return &____HasDataType_6; }
	inline void set__HasDataType_6(bool value)
	{
		____HasDataType_6 = value;
	}

	inline static int32_t get_offset_of__HasType_7() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____HasType_7)); }
	inline bool get__HasType_7() const { return ____HasType_7; }
	inline bool* get_address_of__HasType_7() { return &____HasType_7; }
	inline void set__HasType_7(bool value)
	{
		____HasType_7 = value;
	}

	inline static int32_t get_offset_of__EnumerationRequired_8() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____EnumerationRequired_8)); }
	inline bool get__EnumerationRequired_8() const { return ____EnumerationRequired_8; }
	inline bool* get_address_of__EnumerationRequired_8() { return &____EnumerationRequired_8; }
	inline void set__EnumerationRequired_8(bool value)
	{
		____EnumerationRequired_8 = value;
	}

	inline static int32_t get_offset_of__MinVal_9() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____MinVal_9)); }
	inline uint32_t get__MinVal_9() const { return ____MinVal_9; }
	inline uint32_t* get_address_of__MinVal_9() { return &____MinVal_9; }
	inline void set__MinVal_9(uint32_t value)
	{
		____MinVal_9 = value;
	}

	inline static int32_t get_offset_of__MaxVal_10() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____MaxVal_10)); }
	inline uint32_t get__MaxVal_10() const { return ____MaxVal_10; }
	inline uint32_t* get_address_of__MaxVal_10() { return &____MaxVal_10; }
	inline void set__MaxVal_10(uint32_t value)
	{
		____MaxVal_10 = value;
	}

	inline static int32_t get_offset_of__MaxLength_11() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____MaxLength_11)); }
	inline uint32_t get__MaxLength_11() const { return ____MaxLength_11; }
	inline uint32_t* get_address_of__MaxLength_11() { return &____MaxLength_11; }
	inline void set__MaxLength_11(uint32_t value)
	{
		____MaxLength_11 = value;
	}

	inline static int32_t get_offset_of__MinLength_12() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____MinLength_12)); }
	inline uint32_t get__MinLength_12() const { return ____MinLength_12; }
	inline uint32_t* get_address_of__MinLength_12() { return &____MinLength_12; }
	inline void set__MinLength_12(uint32_t value)
	{
		____MinLength_12 = value;
	}

	inline static int32_t get_offset_of__AttDefList_13() { return static_cast<int32_t>(offsetof(ElementContent_t3043903854, ____AttDefList_13)); }
	inline Hashtable_t909839986 * get__AttDefList_13() const { return ____AttDefList_13; }
	inline Hashtable_t909839986 ** get_address_of__AttDefList_13() { return &____AttDefList_13; }
	inline void set__AttDefList_13(Hashtable_t909839986 * value)
	{
		____AttDefList_13 = value;
		Il2CppCodeGenWriteBarrier(&____AttDefList_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
