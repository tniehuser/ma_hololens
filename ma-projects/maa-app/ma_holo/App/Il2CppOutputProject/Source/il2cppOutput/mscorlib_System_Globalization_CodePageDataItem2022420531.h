﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CodePageDataItem
struct  CodePageDataItem_t2022420531  : public Il2CppObject
{
public:
	// System.Int32 System.Globalization.CodePageDataItem::m_dataIndex
	int32_t ___m_dataIndex_0;
	// System.Int32 System.Globalization.CodePageDataItem::m_uiFamilyCodePage
	int32_t ___m_uiFamilyCodePage_1;
	// System.String System.Globalization.CodePageDataItem::m_webName
	String_t* ___m_webName_2;
	// System.UInt32 System.Globalization.CodePageDataItem::m_flags
	uint32_t ___m_flags_3;

public:
	inline static int32_t get_offset_of_m_dataIndex_0() { return static_cast<int32_t>(offsetof(CodePageDataItem_t2022420531, ___m_dataIndex_0)); }
	inline int32_t get_m_dataIndex_0() const { return ___m_dataIndex_0; }
	inline int32_t* get_address_of_m_dataIndex_0() { return &___m_dataIndex_0; }
	inline void set_m_dataIndex_0(int32_t value)
	{
		___m_dataIndex_0 = value;
	}

	inline static int32_t get_offset_of_m_uiFamilyCodePage_1() { return static_cast<int32_t>(offsetof(CodePageDataItem_t2022420531, ___m_uiFamilyCodePage_1)); }
	inline int32_t get_m_uiFamilyCodePage_1() const { return ___m_uiFamilyCodePage_1; }
	inline int32_t* get_address_of_m_uiFamilyCodePage_1() { return &___m_uiFamilyCodePage_1; }
	inline void set_m_uiFamilyCodePage_1(int32_t value)
	{
		___m_uiFamilyCodePage_1 = value;
	}

	inline static int32_t get_offset_of_m_webName_2() { return static_cast<int32_t>(offsetof(CodePageDataItem_t2022420531, ___m_webName_2)); }
	inline String_t* get_m_webName_2() const { return ___m_webName_2; }
	inline String_t** get_address_of_m_webName_2() { return &___m_webName_2; }
	inline void set_m_webName_2(String_t* value)
	{
		___m_webName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_webName_2, value);
	}

	inline static int32_t get_offset_of_m_flags_3() { return static_cast<int32_t>(offsetof(CodePageDataItem_t2022420531, ___m_flags_3)); }
	inline uint32_t get_m_flags_3() const { return ___m_flags_3; }
	inline uint32_t* get_address_of_m_flags_3() { return &___m_flags_3; }
	inline void set_m_flags_3(uint32_t value)
	{
		___m_flags_3 = value;
	}
};

struct CodePageDataItem_t2022420531_StaticFields
{
public:
	// System.Char[] System.Globalization.CodePageDataItem::sep
	CharU5BU5D_t1328083999* ___sep_4;

public:
	inline static int32_t get_offset_of_sep_4() { return static_cast<int32_t>(offsetof(CodePageDataItem_t2022420531_StaticFields, ___sep_4)); }
	inline CharU5BU5D_t1328083999* get_sep_4() const { return ___sep_4; }
	inline CharU5BU5D_t1328083999** get_address_of_sep_4() { return &___sep_4; }
	inline void set_sep_4(CharU5BU5D_t1328083999* value)
	{
		___sep_4 = value;
		Il2CppCodeGenWriteBarrier(&___sep_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
