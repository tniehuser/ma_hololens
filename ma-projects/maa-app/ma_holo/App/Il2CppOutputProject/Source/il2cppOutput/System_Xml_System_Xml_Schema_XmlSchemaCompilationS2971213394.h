﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCompilationSettings
struct  XmlSchemaCompilationSettings_t2971213394  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaCompilationSettings::enableUpaCheck
	bool ___enableUpaCheck_0;

public:
	inline static int32_t get_offset_of_enableUpaCheck_0() { return static_cast<int32_t>(offsetof(XmlSchemaCompilationSettings_t2971213394, ___enableUpaCheck_0)); }
	inline bool get_enableUpaCheck_0() const { return ___enableUpaCheck_0; }
	inline bool* get_address_of_enableUpaCheck_0() { return &___enableUpaCheck_0; }
	inline void set_enableUpaCheck_0(bool value)
	{
		___enableUpaCheck_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
