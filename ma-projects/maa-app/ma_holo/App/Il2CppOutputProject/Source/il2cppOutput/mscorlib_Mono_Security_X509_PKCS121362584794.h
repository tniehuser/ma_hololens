﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472865;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.PKCS12
struct  PKCS12_t1362584794  : public Il2CppObject
{
public:
	// System.Byte[] Mono.Security.X509.PKCS12::_password
	ByteU5BU5D_t3397334013* ____password_0;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_keyBags
	ArrayList_t4252133567 * ____keyBags_1;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_secretBags
	ArrayList_t4252133567 * ____secretBags_2;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.PKCS12::_certs
	X509CertificateCollection_t3592472865 * ____certs_3;
	// System.Boolean Mono.Security.X509.PKCS12::_keyBagsChanged
	bool ____keyBagsChanged_4;
	// System.Boolean Mono.Security.X509.PKCS12::_secretBagsChanged
	bool ____secretBagsChanged_5;
	// System.Boolean Mono.Security.X509.PKCS12::_certsChanged
	bool ____certsChanged_6;
	// System.Int32 Mono.Security.X509.PKCS12::_iterations
	int32_t ____iterations_7;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_safeBags
	ArrayList_t4252133567 * ____safeBags_8;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.X509.PKCS12::_rng
	RandomNumberGenerator_t2510243513 * ____rng_9;

public:
	inline static int32_t get_offset_of__password_0() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____password_0)); }
	inline ByteU5BU5D_t3397334013* get__password_0() const { return ____password_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__password_0() { return &____password_0; }
	inline void set__password_0(ByteU5BU5D_t3397334013* value)
	{
		____password_0 = value;
		Il2CppCodeGenWriteBarrier(&____password_0, value);
	}

	inline static int32_t get_offset_of__keyBags_1() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____keyBags_1)); }
	inline ArrayList_t4252133567 * get__keyBags_1() const { return ____keyBags_1; }
	inline ArrayList_t4252133567 ** get_address_of__keyBags_1() { return &____keyBags_1; }
	inline void set__keyBags_1(ArrayList_t4252133567 * value)
	{
		____keyBags_1 = value;
		Il2CppCodeGenWriteBarrier(&____keyBags_1, value);
	}

	inline static int32_t get_offset_of__secretBags_2() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____secretBags_2)); }
	inline ArrayList_t4252133567 * get__secretBags_2() const { return ____secretBags_2; }
	inline ArrayList_t4252133567 ** get_address_of__secretBags_2() { return &____secretBags_2; }
	inline void set__secretBags_2(ArrayList_t4252133567 * value)
	{
		____secretBags_2 = value;
		Il2CppCodeGenWriteBarrier(&____secretBags_2, value);
	}

	inline static int32_t get_offset_of__certs_3() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____certs_3)); }
	inline X509CertificateCollection_t3592472865 * get__certs_3() const { return ____certs_3; }
	inline X509CertificateCollection_t3592472865 ** get_address_of__certs_3() { return &____certs_3; }
	inline void set__certs_3(X509CertificateCollection_t3592472865 * value)
	{
		____certs_3 = value;
		Il2CppCodeGenWriteBarrier(&____certs_3, value);
	}

	inline static int32_t get_offset_of__keyBagsChanged_4() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____keyBagsChanged_4)); }
	inline bool get__keyBagsChanged_4() const { return ____keyBagsChanged_4; }
	inline bool* get_address_of__keyBagsChanged_4() { return &____keyBagsChanged_4; }
	inline void set__keyBagsChanged_4(bool value)
	{
		____keyBagsChanged_4 = value;
	}

	inline static int32_t get_offset_of__secretBagsChanged_5() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____secretBagsChanged_5)); }
	inline bool get__secretBagsChanged_5() const { return ____secretBagsChanged_5; }
	inline bool* get_address_of__secretBagsChanged_5() { return &____secretBagsChanged_5; }
	inline void set__secretBagsChanged_5(bool value)
	{
		____secretBagsChanged_5 = value;
	}

	inline static int32_t get_offset_of__certsChanged_6() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____certsChanged_6)); }
	inline bool get__certsChanged_6() const { return ____certsChanged_6; }
	inline bool* get_address_of__certsChanged_6() { return &____certsChanged_6; }
	inline void set__certsChanged_6(bool value)
	{
		____certsChanged_6 = value;
	}

	inline static int32_t get_offset_of__iterations_7() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____iterations_7)); }
	inline int32_t get__iterations_7() const { return ____iterations_7; }
	inline int32_t* get_address_of__iterations_7() { return &____iterations_7; }
	inline void set__iterations_7(int32_t value)
	{
		____iterations_7 = value;
	}

	inline static int32_t get_offset_of__safeBags_8() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____safeBags_8)); }
	inline ArrayList_t4252133567 * get__safeBags_8() const { return ____safeBags_8; }
	inline ArrayList_t4252133567 ** get_address_of__safeBags_8() { return &____safeBags_8; }
	inline void set__safeBags_8(ArrayList_t4252133567 * value)
	{
		____safeBags_8 = value;
		Il2CppCodeGenWriteBarrier(&____safeBags_8, value);
	}

	inline static int32_t get_offset_of__rng_9() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794, ____rng_9)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_9() const { return ____rng_9; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_9() { return &____rng_9; }
	inline void set__rng_9(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_9 = value;
		Il2CppCodeGenWriteBarrier(&____rng_9, value);
	}
};

struct PKCS12_t1362584794_StaticFields
{
public:
	// System.Int32 Mono.Security.X509.PKCS12::password_max_length
	int32_t ___password_max_length_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_11;

public:
	inline static int32_t get_offset_of_password_max_length_10() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794_StaticFields, ___password_max_length_10)); }
	inline int32_t get_password_max_length_10() const { return ___password_max_length_10; }
	inline int32_t* get_address_of_password_max_length_10() { return &___password_max_length_10; }
	inline void set_password_max_length_10(int32_t value)
	{
		___password_max_length_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_11() { return static_cast<int32_t>(offsetof(PKCS12_t1362584794_StaticFields, ___U3CU3Ef__switchU24map1_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_11() const { return ___U3CU3Ef__switchU24map1_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_11() { return &___U3CU3Ef__switchU24map1_11; }
	inline void set_U3CU3Ef__switchU24map1_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
