﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_UnmanagedMemoryStream822875729.h"

// System.Reflection.Module
struct Module_t4282841206;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Assembly/UnmanagedMemoryStreamForModule
struct  UnmanagedMemoryStreamForModule_t2398216387  : public UnmanagedMemoryStream_t822875729
{
public:
	// System.Reflection.Module System.Reflection.Assembly/UnmanagedMemoryStreamForModule::module
	Module_t4282841206 * ___module_16;

public:
	inline static int32_t get_offset_of_module_16() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStreamForModule_t2398216387, ___module_16)); }
	inline Module_t4282841206 * get_module_16() const { return ___module_16; }
	inline Module_t4282841206 ** get_address_of_module_16() { return &___module_16; }
	inline void set_module_16(Module_t4282841206 * value)
	{
		___module_16 = value;
		Il2CppCodeGenWriteBarrier(&___module_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
