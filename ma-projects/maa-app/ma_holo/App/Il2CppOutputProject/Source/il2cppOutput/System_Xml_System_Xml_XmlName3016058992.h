﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlName
struct XmlName_t3016058992;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlName
struct  XmlName_t3016058992  : public Il2CppObject
{
public:
	// System.String System.Xml.XmlName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlName::localName
	String_t* ___localName_1;
	// System.String System.Xml.XmlName::ns
	String_t* ___ns_2;
	// System.String System.Xml.XmlName::name
	String_t* ___name_3;
	// System.Int32 System.Xml.XmlName::hashCode
	int32_t ___hashCode_4;
	// System.Xml.XmlDocument System.Xml.XmlName::ownerDoc
	XmlDocument_t3649534162 * ___ownerDoc_5;
	// System.Xml.XmlName System.Xml.XmlName::next
	XmlName_t3016058992 * ___next_6;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_0, value);
	}

	inline static int32_t get_offset_of_localName_1() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___localName_1)); }
	inline String_t* get_localName_1() const { return ___localName_1; }
	inline String_t** get_address_of_localName_1() { return &___localName_1; }
	inline void set_localName_1(String_t* value)
	{
		___localName_1 = value;
		Il2CppCodeGenWriteBarrier(&___localName_1, value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier(&___ns_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_hashCode_4() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___hashCode_4)); }
	inline int32_t get_hashCode_4() const { return ___hashCode_4; }
	inline int32_t* get_address_of_hashCode_4() { return &___hashCode_4; }
	inline void set_hashCode_4(int32_t value)
	{
		___hashCode_4 = value;
	}

	inline static int32_t get_offset_of_ownerDoc_5() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___ownerDoc_5)); }
	inline XmlDocument_t3649534162 * get_ownerDoc_5() const { return ___ownerDoc_5; }
	inline XmlDocument_t3649534162 ** get_address_of_ownerDoc_5() { return &___ownerDoc_5; }
	inline void set_ownerDoc_5(XmlDocument_t3649534162 * value)
	{
		___ownerDoc_5 = value;
		Il2CppCodeGenWriteBarrier(&___ownerDoc_5, value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(XmlName_t3016058992, ___next_6)); }
	inline XmlName_t3016058992 * get_next_6() const { return ___next_6; }
	inline XmlName_t3016058992 ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(XmlName_t3016058992 * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier(&___next_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
