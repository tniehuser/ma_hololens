﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t302829305;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t823784021;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t2774612835;
// System.Net.Security.LocalCertSelectionCallback
struct LocalCertSelectionCallback_t2279727292;
// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct ServerCertValidationCallbackWrapper_t93117366;
// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t1249652258;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ChainValidationHelper
struct  ChainValidationHelper_t3893280544  : public Il2CppObject
{
public:
	// System.Object Mono.Net.Security.ChainValidationHelper::sender
	Il2CppObject * ___sender_0;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.ChainValidationHelper::settings
	MonoTlsSettings_t302829305 * ___settings_1;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.ChainValidationHelper::provider
	MonoTlsProvider_t823784021 * ___provider_2;
	// System.Net.ServerCertValidationCallback Mono.Net.Security.ChainValidationHelper::certValidationCallback
	ServerCertValidationCallback_t2774612835 * ___certValidationCallback_3;
	// System.Net.Security.LocalCertSelectionCallback Mono.Net.Security.ChainValidationHelper::certSelectionCallback
	LocalCertSelectionCallback_t2279727292 * ___certSelectionCallback_4;
	// Mono.Net.Security.ServerCertValidationCallbackWrapper Mono.Net.Security.ChainValidationHelper::callbackWrapper
	ServerCertValidationCallbackWrapper_t93117366 * ___callbackWrapper_5;
	// Mono.Net.Security.MonoTlsStream Mono.Net.Security.ChainValidationHelper::tlsStream
	MonoTlsStream_t1249652258 * ___tlsStream_6;
	// System.Net.HttpWebRequest Mono.Net.Security.ChainValidationHelper::request
	HttpWebRequest_t1951404513 * ___request_7;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___sender_0)); }
	inline Il2CppObject * get_sender_0() const { return ___sender_0; }
	inline Il2CppObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(Il2CppObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier(&___sender_0, value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___settings_1)); }
	inline MonoTlsSettings_t302829305 * get_settings_1() const { return ___settings_1; }
	inline MonoTlsSettings_t302829305 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(MonoTlsSettings_t302829305 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier(&___settings_1, value);
	}

	inline static int32_t get_offset_of_provider_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___provider_2)); }
	inline MonoTlsProvider_t823784021 * get_provider_2() const { return ___provider_2; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_2() { return &___provider_2; }
	inline void set_provider_2(MonoTlsProvider_t823784021 * value)
	{
		___provider_2 = value;
		Il2CppCodeGenWriteBarrier(&___provider_2, value);
	}

	inline static int32_t get_offset_of_certValidationCallback_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___certValidationCallback_3)); }
	inline ServerCertValidationCallback_t2774612835 * get_certValidationCallback_3() const { return ___certValidationCallback_3; }
	inline ServerCertValidationCallback_t2774612835 ** get_address_of_certValidationCallback_3() { return &___certValidationCallback_3; }
	inline void set_certValidationCallback_3(ServerCertValidationCallback_t2774612835 * value)
	{
		___certValidationCallback_3 = value;
		Il2CppCodeGenWriteBarrier(&___certValidationCallback_3, value);
	}

	inline static int32_t get_offset_of_certSelectionCallback_4() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___certSelectionCallback_4)); }
	inline LocalCertSelectionCallback_t2279727292 * get_certSelectionCallback_4() const { return ___certSelectionCallback_4; }
	inline LocalCertSelectionCallback_t2279727292 ** get_address_of_certSelectionCallback_4() { return &___certSelectionCallback_4; }
	inline void set_certSelectionCallback_4(LocalCertSelectionCallback_t2279727292 * value)
	{
		___certSelectionCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___certSelectionCallback_4, value);
	}

	inline static int32_t get_offset_of_callbackWrapper_5() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___callbackWrapper_5)); }
	inline ServerCertValidationCallbackWrapper_t93117366 * get_callbackWrapper_5() const { return ___callbackWrapper_5; }
	inline ServerCertValidationCallbackWrapper_t93117366 ** get_address_of_callbackWrapper_5() { return &___callbackWrapper_5; }
	inline void set_callbackWrapper_5(ServerCertValidationCallbackWrapper_t93117366 * value)
	{
		___callbackWrapper_5 = value;
		Il2CppCodeGenWriteBarrier(&___callbackWrapper_5, value);
	}

	inline static int32_t get_offset_of_tlsStream_6() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___tlsStream_6)); }
	inline MonoTlsStream_t1249652258 * get_tlsStream_6() const { return ___tlsStream_6; }
	inline MonoTlsStream_t1249652258 ** get_address_of_tlsStream_6() { return &___tlsStream_6; }
	inline void set_tlsStream_6(MonoTlsStream_t1249652258 * value)
	{
		___tlsStream_6 = value;
		Il2CppCodeGenWriteBarrier(&___tlsStream_6, value);
	}

	inline static int32_t get_offset_of_request_7() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___request_7)); }
	inline HttpWebRequest_t1951404513 * get_request_7() const { return ___request_7; }
	inline HttpWebRequest_t1951404513 ** get_address_of_request_7() { return &___request_7; }
	inline void set_request_7(HttpWebRequest_t1951404513 * value)
	{
		___request_7 = value;
		Il2CppCodeGenWriteBarrier(&___request_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
