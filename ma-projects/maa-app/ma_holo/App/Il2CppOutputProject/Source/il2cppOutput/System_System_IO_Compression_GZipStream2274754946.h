﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.IO.Compression.DeflateStream
struct DeflateStream_t3198596725;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.GZipStream
struct  GZipStream_t2274754946  : public Stream_t3255436806
{
public:
	// System.IO.Compression.DeflateStream System.IO.Compression.GZipStream::deflateStream
	DeflateStream_t3198596725 * ___deflateStream_8;

public:
	inline static int32_t get_offset_of_deflateStream_8() { return static_cast<int32_t>(offsetof(GZipStream_t2274754946, ___deflateStream_8)); }
	inline DeflateStream_t3198596725 * get_deflateStream_8() const { return ___deflateStream_8; }
	inline DeflateStream_t3198596725 ** get_address_of_deflateStream_8() { return &___deflateStream_8; }
	inline void set_deflateStream_8(DeflateStream_t3198596725 * value)
	{
		___deflateStream_8 = value;
		Il2CppCodeGenWriteBarrier(&___deflateStream_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
