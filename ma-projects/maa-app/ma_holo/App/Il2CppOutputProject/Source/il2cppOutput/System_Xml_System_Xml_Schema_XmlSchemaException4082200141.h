﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_t4082200141  : public SystemException_t3877406272
{
public:
	// System.String System.Xml.Schema.XmlSchemaException::res
	String_t* ___res_16;
	// System.String[] System.Xml.Schema.XmlSchemaException::args
	StringU5BU5D_t1642385972* ___args_17;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_18;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_19;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_20;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceSchemaObject
	XmlSchemaObject_t2050913741 * ___sourceSchemaObject_21;
	// System.String System.Xml.Schema.XmlSchemaException::message
	String_t* ___message_22;

public:
	inline static int32_t get_offset_of_res_16() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___res_16)); }
	inline String_t* get_res_16() const { return ___res_16; }
	inline String_t** get_address_of_res_16() { return &___res_16; }
	inline void set_res_16(String_t* value)
	{
		___res_16 = value;
		Il2CppCodeGenWriteBarrier(&___res_16, value);
	}

	inline static int32_t get_offset_of_args_17() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___args_17)); }
	inline StringU5BU5D_t1642385972* get_args_17() const { return ___args_17; }
	inline StringU5BU5D_t1642385972** get_address_of_args_17() { return &___args_17; }
	inline void set_args_17(StringU5BU5D_t1642385972* value)
	{
		___args_17 = value;
		Il2CppCodeGenWriteBarrier(&___args_17, value);
	}

	inline static int32_t get_offset_of_sourceUri_18() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceUri_18)); }
	inline String_t* get_sourceUri_18() const { return ___sourceUri_18; }
	inline String_t** get_address_of_sourceUri_18() { return &___sourceUri_18; }
	inline void set_sourceUri_18(String_t* value)
	{
		___sourceUri_18 = value;
		Il2CppCodeGenWriteBarrier(&___sourceUri_18, value);
	}

	inline static int32_t get_offset_of_lineNumber_19() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___lineNumber_19)); }
	inline int32_t get_lineNumber_19() const { return ___lineNumber_19; }
	inline int32_t* get_address_of_lineNumber_19() { return &___lineNumber_19; }
	inline void set_lineNumber_19(int32_t value)
	{
		___lineNumber_19 = value;
	}

	inline static int32_t get_offset_of_linePosition_20() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___linePosition_20)); }
	inline int32_t get_linePosition_20() const { return ___linePosition_20; }
	inline int32_t* get_address_of_linePosition_20() { return &___linePosition_20; }
	inline void set_linePosition_20(int32_t value)
	{
		___linePosition_20 = value;
	}

	inline static int32_t get_offset_of_sourceSchemaObject_21() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceSchemaObject_21)); }
	inline XmlSchemaObject_t2050913741 * get_sourceSchemaObject_21() const { return ___sourceSchemaObject_21; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_sourceSchemaObject_21() { return &___sourceSchemaObject_21; }
	inline void set_sourceSchemaObject_21(XmlSchemaObject_t2050913741 * value)
	{
		___sourceSchemaObject_21 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSchemaObject_21, value);
	}

	inline static int32_t get_offset_of_message_22() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___message_22)); }
	inline String_t* get_message_22() const { return ___message_22; }
	inline String_t** get_address_of_message_22() { return &___message_22; }
	inline void set_message_22(String_t* value)
	{
		___message_22 = value;
		Il2CppCodeGenWriteBarrier(&___message_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
