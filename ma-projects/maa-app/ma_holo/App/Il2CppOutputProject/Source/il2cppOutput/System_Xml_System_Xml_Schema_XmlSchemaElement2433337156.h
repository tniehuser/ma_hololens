﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"

// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_t2433337156  : public XmlSchemaParticle_t3365045970
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isAbstract
	bool ___isAbstract_13;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::hasAbstractAttribute
	bool ___hasAbstractAttribute_14;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isNillable
	bool ___isNillable_15;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::hasNillableAttribute
	bool ___hasNillableAttribute_16;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isLocalTypeDerivationChecked
	bool ___isLocalTypeDerivationChecked_17;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::block
	int32_t ___block_18;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::final
	int32_t ___final_19;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaElement::form
	int32_t ___form_20;
	// System.String System.Xml.Schema.XmlSchemaElement::defaultValue
	String_t* ___defaultValue_21;
	// System.String System.Xml.Schema.XmlSchemaElement::fixedValue
	String_t* ___fixedValue_22;
	// System.String System.Xml.Schema.XmlSchemaElement::name
	String_t* ___name_23;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::refName
	XmlQualifiedName_t1944712516 * ___refName_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::substitutionGroup
	XmlQualifiedName_t1944712516 * ___substitutionGroup_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::typeName
	XmlQualifiedName_t1944712516 * ___typeName_26;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::type
	XmlSchemaType_t1795078578 * ___type_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_28;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::elementType
	XmlSchemaType_t1795078578 * ___elementType_29;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::blockResolved
	int32_t ___blockResolved_30;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::finalResolved
	int32_t ___finalResolved_31;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaElement::constraints
	XmlSchemaObjectCollection_t395083109 * ___constraints_32;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XmlSchemaElement::elementDecl
	SchemaElementDecl_t1940851905 * ___elementDecl_33;

public:
	inline static int32_t get_offset_of_isAbstract_13() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___isAbstract_13)); }
	inline bool get_isAbstract_13() const { return ___isAbstract_13; }
	inline bool* get_address_of_isAbstract_13() { return &___isAbstract_13; }
	inline void set_isAbstract_13(bool value)
	{
		___isAbstract_13 = value;
	}

	inline static int32_t get_offset_of_hasAbstractAttribute_14() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___hasAbstractAttribute_14)); }
	inline bool get_hasAbstractAttribute_14() const { return ___hasAbstractAttribute_14; }
	inline bool* get_address_of_hasAbstractAttribute_14() { return &___hasAbstractAttribute_14; }
	inline void set_hasAbstractAttribute_14(bool value)
	{
		___hasAbstractAttribute_14 = value;
	}

	inline static int32_t get_offset_of_isNillable_15() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___isNillable_15)); }
	inline bool get_isNillable_15() const { return ___isNillable_15; }
	inline bool* get_address_of_isNillable_15() { return &___isNillable_15; }
	inline void set_isNillable_15(bool value)
	{
		___isNillable_15 = value;
	}

	inline static int32_t get_offset_of_hasNillableAttribute_16() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___hasNillableAttribute_16)); }
	inline bool get_hasNillableAttribute_16() const { return ___hasNillableAttribute_16; }
	inline bool* get_address_of_hasNillableAttribute_16() { return &___hasNillableAttribute_16; }
	inline void set_hasNillableAttribute_16(bool value)
	{
		___hasNillableAttribute_16 = value;
	}

	inline static int32_t get_offset_of_isLocalTypeDerivationChecked_17() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___isLocalTypeDerivationChecked_17)); }
	inline bool get_isLocalTypeDerivationChecked_17() const { return ___isLocalTypeDerivationChecked_17; }
	inline bool* get_address_of_isLocalTypeDerivationChecked_17() { return &___isLocalTypeDerivationChecked_17; }
	inline void set_isLocalTypeDerivationChecked_17(bool value)
	{
		___isLocalTypeDerivationChecked_17 = value;
	}

	inline static int32_t get_offset_of_block_18() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___block_18)); }
	inline int32_t get_block_18() const { return ___block_18; }
	inline int32_t* get_address_of_block_18() { return &___block_18; }
	inline void set_block_18(int32_t value)
	{
		___block_18 = value;
	}

	inline static int32_t get_offset_of_final_19() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___final_19)); }
	inline int32_t get_final_19() const { return ___final_19; }
	inline int32_t* get_address_of_final_19() { return &___final_19; }
	inline void set_final_19(int32_t value)
	{
		___final_19 = value;
	}

	inline static int32_t get_offset_of_form_20() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___form_20)); }
	inline int32_t get_form_20() const { return ___form_20; }
	inline int32_t* get_address_of_form_20() { return &___form_20; }
	inline void set_form_20(int32_t value)
	{
		___form_20 = value;
	}

	inline static int32_t get_offset_of_defaultValue_21() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___defaultValue_21)); }
	inline String_t* get_defaultValue_21() const { return ___defaultValue_21; }
	inline String_t** get_address_of_defaultValue_21() { return &___defaultValue_21; }
	inline void set_defaultValue_21(String_t* value)
	{
		___defaultValue_21 = value;
		Il2CppCodeGenWriteBarrier(&___defaultValue_21, value);
	}

	inline static int32_t get_offset_of_fixedValue_22() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___fixedValue_22)); }
	inline String_t* get_fixedValue_22() const { return ___fixedValue_22; }
	inline String_t** get_address_of_fixedValue_22() { return &___fixedValue_22; }
	inline void set_fixedValue_22(String_t* value)
	{
		___fixedValue_22 = value;
		Il2CppCodeGenWriteBarrier(&___fixedValue_22, value);
	}

	inline static int32_t get_offset_of_name_23() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___name_23)); }
	inline String_t* get_name_23() const { return ___name_23; }
	inline String_t** get_address_of_name_23() { return &___name_23; }
	inline void set_name_23(String_t* value)
	{
		___name_23 = value;
		Il2CppCodeGenWriteBarrier(&___name_23, value);
	}

	inline static int32_t get_offset_of_refName_24() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___refName_24)); }
	inline XmlQualifiedName_t1944712516 * get_refName_24() const { return ___refName_24; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_24() { return &___refName_24; }
	inline void set_refName_24(XmlQualifiedName_t1944712516 * value)
	{
		___refName_24 = value;
		Il2CppCodeGenWriteBarrier(&___refName_24, value);
	}

	inline static int32_t get_offset_of_substitutionGroup_25() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___substitutionGroup_25)); }
	inline XmlQualifiedName_t1944712516 * get_substitutionGroup_25() const { return ___substitutionGroup_25; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_substitutionGroup_25() { return &___substitutionGroup_25; }
	inline void set_substitutionGroup_25(XmlQualifiedName_t1944712516 * value)
	{
		___substitutionGroup_25 = value;
		Il2CppCodeGenWriteBarrier(&___substitutionGroup_25, value);
	}

	inline static int32_t get_offset_of_typeName_26() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___typeName_26)); }
	inline XmlQualifiedName_t1944712516 * get_typeName_26() const { return ___typeName_26; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_typeName_26() { return &___typeName_26; }
	inline void set_typeName_26(XmlQualifiedName_t1944712516 * value)
	{
		___typeName_26 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_26, value);
	}

	inline static int32_t get_offset_of_type_27() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___type_27)); }
	inline XmlSchemaType_t1795078578 * get_type_27() const { return ___type_27; }
	inline XmlSchemaType_t1795078578 ** get_address_of_type_27() { return &___type_27; }
	inline void set_type_27(XmlSchemaType_t1795078578 * value)
	{
		___type_27 = value;
		Il2CppCodeGenWriteBarrier(&___type_27, value);
	}

	inline static int32_t get_offset_of_qualifiedName_28() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___qualifiedName_28)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_28() const { return ___qualifiedName_28; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_28() { return &___qualifiedName_28; }
	inline void set_qualifiedName_28(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_28 = value;
		Il2CppCodeGenWriteBarrier(&___qualifiedName_28, value);
	}

	inline static int32_t get_offset_of_elementType_29() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___elementType_29)); }
	inline XmlSchemaType_t1795078578 * get_elementType_29() const { return ___elementType_29; }
	inline XmlSchemaType_t1795078578 ** get_address_of_elementType_29() { return &___elementType_29; }
	inline void set_elementType_29(XmlSchemaType_t1795078578 * value)
	{
		___elementType_29 = value;
		Il2CppCodeGenWriteBarrier(&___elementType_29, value);
	}

	inline static int32_t get_offset_of_blockResolved_30() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___blockResolved_30)); }
	inline int32_t get_blockResolved_30() const { return ___blockResolved_30; }
	inline int32_t* get_address_of_blockResolved_30() { return &___blockResolved_30; }
	inline void set_blockResolved_30(int32_t value)
	{
		___blockResolved_30 = value;
	}

	inline static int32_t get_offset_of_finalResolved_31() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___finalResolved_31)); }
	inline int32_t get_finalResolved_31() const { return ___finalResolved_31; }
	inline int32_t* get_address_of_finalResolved_31() { return &___finalResolved_31; }
	inline void set_finalResolved_31(int32_t value)
	{
		___finalResolved_31 = value;
	}

	inline static int32_t get_offset_of_constraints_32() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___constraints_32)); }
	inline XmlSchemaObjectCollection_t395083109 * get_constraints_32() const { return ___constraints_32; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_constraints_32() { return &___constraints_32; }
	inline void set_constraints_32(XmlSchemaObjectCollection_t395083109 * value)
	{
		___constraints_32 = value;
		Il2CppCodeGenWriteBarrier(&___constraints_32, value);
	}

	inline static int32_t get_offset_of_elementDecl_33() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___elementDecl_33)); }
	inline SchemaElementDecl_t1940851905 * get_elementDecl_33() const { return ___elementDecl_33; }
	inline SchemaElementDecl_t1940851905 ** get_address_of_elementDecl_33() { return &___elementDecl_33; }
	inline void set_elementDecl_33(SchemaElementDecl_t1940851905 * value)
	{
		___elementDecl_33 = value;
		Il2CppCodeGenWriteBarrier(&___elementDecl_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
