﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaSubstitution3854467138.h"

// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t654568461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSubstitutionGroupV1Compat
struct  XmlSchemaSubstitutionGroupV1Compat_t1277095323  : public XmlSchemaSubstitutionGroup_t3854467138
{
public:
	// System.Xml.Schema.XmlSchemaChoice System.Xml.Schema.XmlSchemaSubstitutionGroupV1Compat::choice
	XmlSchemaChoice_t654568461 * ___choice_8;

public:
	inline static int32_t get_offset_of_choice_8() { return static_cast<int32_t>(offsetof(XmlSchemaSubstitutionGroupV1Compat_t1277095323, ___choice_8)); }
	inline XmlSchemaChoice_t654568461 * get_choice_8() const { return ___choice_8; }
	inline XmlSchemaChoice_t654568461 ** get_address_of_choice_8() { return &___choice_8; }
	inline void set_choice_8(XmlSchemaChoice_t654568461 * value)
	{
		___choice_8 = value;
		Il2CppCodeGenWriteBarrier(&___choice_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
