﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_ResourceLocation999505379.h"

// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ManifestResourceInfo
struct  ManifestResourceInfo_t2035432027  : public Il2CppObject
{
public:
	// System.Reflection.Assembly System.Reflection.ManifestResourceInfo::_containingAssembly
	Assembly_t4268412390 * ____containingAssembly_0;
	// System.String System.Reflection.ManifestResourceInfo::_containingFileName
	String_t* ____containingFileName_1;
	// System.Reflection.ResourceLocation System.Reflection.ManifestResourceInfo::_resourceLocation
	int32_t ____resourceLocation_2;

public:
	inline static int32_t get_offset_of__containingAssembly_0() { return static_cast<int32_t>(offsetof(ManifestResourceInfo_t2035432027, ____containingAssembly_0)); }
	inline Assembly_t4268412390 * get__containingAssembly_0() const { return ____containingAssembly_0; }
	inline Assembly_t4268412390 ** get_address_of__containingAssembly_0() { return &____containingAssembly_0; }
	inline void set__containingAssembly_0(Assembly_t4268412390 * value)
	{
		____containingAssembly_0 = value;
		Il2CppCodeGenWriteBarrier(&____containingAssembly_0, value);
	}

	inline static int32_t get_offset_of__containingFileName_1() { return static_cast<int32_t>(offsetof(ManifestResourceInfo_t2035432027, ____containingFileName_1)); }
	inline String_t* get__containingFileName_1() const { return ____containingFileName_1; }
	inline String_t** get_address_of__containingFileName_1() { return &____containingFileName_1; }
	inline void set__containingFileName_1(String_t* value)
	{
		____containingFileName_1 = value;
		Il2CppCodeGenWriteBarrier(&____containingFileName_1, value);
	}

	inline static int32_t get_offset_of__resourceLocation_2() { return static_cast<int32_t>(offsetof(ManifestResourceInfo_t2035432027, ____resourceLocation_2)); }
	inline int32_t get__resourceLocation_2() const { return ____resourceLocation_2; }
	inline int32_t* get_address_of__resourceLocation_2() { return &____resourceLocation_2; }
	inline void set__resourceLocation_2(int32_t value)
	{
		____resourceLocation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
