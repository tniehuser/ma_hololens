﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.RegularExpressions.RegexCharClass
struct RegexCharClass_t2441867401;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFC
struct  RegexFC_t2009854258  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexFC::_cc
	RegexCharClass_t2441867401 * ____cc_0;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_nullable
	bool ____nullable_1;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_caseInsensitive
	bool ____caseInsensitive_2;

public:
	inline static int32_t get_offset_of__cc_0() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____cc_0)); }
	inline RegexCharClass_t2441867401 * get__cc_0() const { return ____cc_0; }
	inline RegexCharClass_t2441867401 ** get_address_of__cc_0() { return &____cc_0; }
	inline void set__cc_0(RegexCharClass_t2441867401 * value)
	{
		____cc_0 = value;
		Il2CppCodeGenWriteBarrier(&____cc_0, value);
	}

	inline static int32_t get_offset_of__nullable_1() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____nullable_1)); }
	inline bool get__nullable_1() const { return ____nullable_1; }
	inline bool* get_address_of__nullable_1() { return &____nullable_1; }
	inline void set__nullable_1(bool value)
	{
		____nullable_1 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_2() { return static_cast<int32_t>(offsetof(RegexFC_t2009854258, ____caseInsensitive_2)); }
	inline bool get__caseInsensitive_2() const { return ____caseInsensitive_2; }
	inline bool* get_address_of__caseInsensitive_2() { return &____caseInsensitive_2; }
	inline void set__caseInsensitive_2(bool value)
	{
		____caseInsensitive_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
