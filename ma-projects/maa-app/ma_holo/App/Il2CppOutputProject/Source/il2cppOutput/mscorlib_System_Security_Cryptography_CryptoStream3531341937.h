﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoStream
struct  CryptoStream_t3531341937  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.Security.Cryptography.CryptoStream::_stream
	Stream_t3255436806 * ____stream_8;
	// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.CryptoStream::_Transform
	Il2CppObject * ____Transform_9;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_InputBuffer
	ByteU5BU5D_t3397334013* ____InputBuffer_10;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBufferIndex
	int32_t ____InputBufferIndex_11;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBlockSize
	int32_t ____InputBlockSize_12;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_OutputBuffer
	ByteU5BU5D_t3397334013* ____OutputBuffer_13;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBufferIndex
	int32_t ____OutputBufferIndex_14;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBlockSize
	int32_t ____OutputBlockSize_15;
	// System.Security.Cryptography.CryptoStreamMode System.Security.Cryptography.CryptoStream::_transformMode
	int32_t ____transformMode_16;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canRead
	bool ____canRead_17;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canWrite
	bool ____canWrite_18;
	// System.Boolean System.Security.Cryptography.CryptoStream::_finalBlockTransformed
	bool ____finalBlockTransformed_19;

public:
	inline static int32_t get_offset_of__stream_8() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____stream_8)); }
	inline Stream_t3255436806 * get__stream_8() const { return ____stream_8; }
	inline Stream_t3255436806 ** get_address_of__stream_8() { return &____stream_8; }
	inline void set__stream_8(Stream_t3255436806 * value)
	{
		____stream_8 = value;
		Il2CppCodeGenWriteBarrier(&____stream_8, value);
	}

	inline static int32_t get_offset_of__Transform_9() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____Transform_9)); }
	inline Il2CppObject * get__Transform_9() const { return ____Transform_9; }
	inline Il2CppObject ** get_address_of__Transform_9() { return &____Transform_9; }
	inline void set__Transform_9(Il2CppObject * value)
	{
		____Transform_9 = value;
		Il2CppCodeGenWriteBarrier(&____Transform_9, value);
	}

	inline static int32_t get_offset_of__InputBuffer_10() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____InputBuffer_10)); }
	inline ByteU5BU5D_t3397334013* get__InputBuffer_10() const { return ____InputBuffer_10; }
	inline ByteU5BU5D_t3397334013** get_address_of__InputBuffer_10() { return &____InputBuffer_10; }
	inline void set__InputBuffer_10(ByteU5BU5D_t3397334013* value)
	{
		____InputBuffer_10 = value;
		Il2CppCodeGenWriteBarrier(&____InputBuffer_10, value);
	}

	inline static int32_t get_offset_of__InputBufferIndex_11() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____InputBufferIndex_11)); }
	inline int32_t get__InputBufferIndex_11() const { return ____InputBufferIndex_11; }
	inline int32_t* get_address_of__InputBufferIndex_11() { return &____InputBufferIndex_11; }
	inline void set__InputBufferIndex_11(int32_t value)
	{
		____InputBufferIndex_11 = value;
	}

	inline static int32_t get_offset_of__InputBlockSize_12() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____InputBlockSize_12)); }
	inline int32_t get__InputBlockSize_12() const { return ____InputBlockSize_12; }
	inline int32_t* get_address_of__InputBlockSize_12() { return &____InputBlockSize_12; }
	inline void set__InputBlockSize_12(int32_t value)
	{
		____InputBlockSize_12 = value;
	}

	inline static int32_t get_offset_of__OutputBuffer_13() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____OutputBuffer_13)); }
	inline ByteU5BU5D_t3397334013* get__OutputBuffer_13() const { return ____OutputBuffer_13; }
	inline ByteU5BU5D_t3397334013** get_address_of__OutputBuffer_13() { return &____OutputBuffer_13; }
	inline void set__OutputBuffer_13(ByteU5BU5D_t3397334013* value)
	{
		____OutputBuffer_13 = value;
		Il2CppCodeGenWriteBarrier(&____OutputBuffer_13, value);
	}

	inline static int32_t get_offset_of__OutputBufferIndex_14() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____OutputBufferIndex_14)); }
	inline int32_t get__OutputBufferIndex_14() const { return ____OutputBufferIndex_14; }
	inline int32_t* get_address_of__OutputBufferIndex_14() { return &____OutputBufferIndex_14; }
	inline void set__OutputBufferIndex_14(int32_t value)
	{
		____OutputBufferIndex_14 = value;
	}

	inline static int32_t get_offset_of__OutputBlockSize_15() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____OutputBlockSize_15)); }
	inline int32_t get__OutputBlockSize_15() const { return ____OutputBlockSize_15; }
	inline int32_t* get_address_of__OutputBlockSize_15() { return &____OutputBlockSize_15; }
	inline void set__OutputBlockSize_15(int32_t value)
	{
		____OutputBlockSize_15 = value;
	}

	inline static int32_t get_offset_of__transformMode_16() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____transformMode_16)); }
	inline int32_t get__transformMode_16() const { return ____transformMode_16; }
	inline int32_t* get_address_of__transformMode_16() { return &____transformMode_16; }
	inline void set__transformMode_16(int32_t value)
	{
		____transformMode_16 = value;
	}

	inline static int32_t get_offset_of__canRead_17() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____canRead_17)); }
	inline bool get__canRead_17() const { return ____canRead_17; }
	inline bool* get_address_of__canRead_17() { return &____canRead_17; }
	inline void set__canRead_17(bool value)
	{
		____canRead_17 = value;
	}

	inline static int32_t get_offset_of__canWrite_18() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____canWrite_18)); }
	inline bool get__canWrite_18() const { return ____canWrite_18; }
	inline bool* get_address_of__canWrite_18() { return &____canWrite_18; }
	inline void set__canWrite_18(bool value)
	{
		____canWrite_18 = value;
	}

	inline static int32_t get_offset_of__finalBlockTransformed_19() { return static_cast<int32_t>(offsetof(CryptoStream_t3531341937, ____finalBlockTransformed_19)); }
	inline bool get__finalBlockTransformed_19() const { return ____finalBlockTransformed_19; }
	inline bool* get_address_of__finalBlockTransformed_19() { return &____finalBlockTransformed_19; }
	inline void set__finalBlockTransformed_19(bool value)
	{
		____finalBlockTransformed_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
