﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "System_System_Net_CookieToken1256074727.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer/RecognizedAttribute
struct  RecognizedAttribute_t3930529114 
{
public:
	// System.String System.Net.CookieTokenizer/RecognizedAttribute::m_name
	String_t* ___m_name_0;
	// System.Net.CookieToken System.Net.CookieTokenizer/RecognizedAttribute::m_token
	int32_t ___m_token_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t3930529114, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_0, value);
	}

	inline static int32_t get_offset_of_m_token_1() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t3930529114, ___m_token_1)); }
	inline int32_t get_m_token_1() const { return ___m_token_1; }
	inline int32_t* get_address_of_m_token_1() { return &___m_token_1; }
	inline void set_m_token_1(int32_t value)
	{
		___m_token_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t3930529114_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_token_1;
};
// Native definition for COM marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t3930529114_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_token_1;
};
