﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task1843236107.h"

// System.Threading.Tasks.TaskFactory`1<System.Boolean>
struct TaskFactory_1_t220925529;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Boolean>>
struct Func_2_t3758303068;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task`1<System.Boolean>
struct  Task_1_t2945603725  : public Task_t1843236107
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	bool ___m_result_24;

public:
	inline static int32_t get_offset_of_m_result_24() { return static_cast<int32_t>(offsetof(Task_1_t2945603725, ___m_result_24)); }
	inline bool get_m_result_24() const { return ___m_result_24; }
	inline bool* get_address_of_m_result_24() { return &___m_result_24; }
	inline void set_m_result_24(bool value)
	{
		___m_result_24 = value;
	}
};

struct Task_1_t2945603725_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t220925529 * ___s_Factory_25;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t3758303068 * ___TaskWhenAnyCast_26;

public:
	inline static int32_t get_offset_of_s_Factory_25() { return static_cast<int32_t>(offsetof(Task_1_t2945603725_StaticFields, ___s_Factory_25)); }
	inline TaskFactory_1_t220925529 * get_s_Factory_25() const { return ___s_Factory_25; }
	inline TaskFactory_1_t220925529 ** get_address_of_s_Factory_25() { return &___s_Factory_25; }
	inline void set_s_Factory_25(TaskFactory_1_t220925529 * value)
	{
		___s_Factory_25 = value;
		Il2CppCodeGenWriteBarrier(&___s_Factory_25, value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_26() { return static_cast<int32_t>(offsetof(Task_1_t2945603725_StaticFields, ___TaskWhenAnyCast_26)); }
	inline Func_2_t3758303068 * get_TaskWhenAnyCast_26() const { return ___TaskWhenAnyCast_26; }
	inline Func_2_t3758303068 ** get_address_of_TaskWhenAnyCast_26() { return &___TaskWhenAnyCast_26; }
	inline void set_TaskWhenAnyCast_26(Func_2_t3758303068 * value)
	{
		___TaskWhenAnyCast_26 = value;
		Il2CppCodeGenWriteBarrier(&___TaskWhenAnyCast_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
