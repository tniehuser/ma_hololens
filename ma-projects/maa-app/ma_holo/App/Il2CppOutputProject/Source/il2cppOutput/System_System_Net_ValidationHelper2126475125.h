﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ValidationHelper
struct  ValidationHelper_t2126475125  : public Il2CppObject
{
public:

public:
};

struct ValidationHelper_t2126475125_StaticFields
{
public:
	// System.String[] System.Net.ValidationHelper::EmptyArray
	StringU5BU5D_t1642385972* ___EmptyArray_0;
	// System.Char[] System.Net.ValidationHelper::InvalidMethodChars
	CharU5BU5D_t1328083999* ___InvalidMethodChars_1;
	// System.Char[] System.Net.ValidationHelper::InvalidParamChars
	CharU5BU5D_t1328083999* ___InvalidParamChars_2;

public:
	inline static int32_t get_offset_of_EmptyArray_0() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___EmptyArray_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyArray_0() const { return ___EmptyArray_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyArray_0() { return &___EmptyArray_0; }
	inline void set_EmptyArray_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyArray_0 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyArray_0, value);
	}

	inline static int32_t get_offset_of_InvalidMethodChars_1() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___InvalidMethodChars_1)); }
	inline CharU5BU5D_t1328083999* get_InvalidMethodChars_1() const { return ___InvalidMethodChars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_InvalidMethodChars_1() { return &___InvalidMethodChars_1; }
	inline void set_InvalidMethodChars_1(CharU5BU5D_t1328083999* value)
	{
		___InvalidMethodChars_1 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidMethodChars_1, value);
	}

	inline static int32_t get_offset_of_InvalidParamChars_2() { return static_cast<int32_t>(offsetof(ValidationHelper_t2126475125_StaticFields, ___InvalidParamChars_2)); }
	inline CharU5BU5D_t1328083999* get_InvalidParamChars_2() const { return ___InvalidParamChars_2; }
	inline CharU5BU5D_t1328083999** get_address_of_InvalidParamChars_2() { return &___InvalidParamChars_2; }
	inline void set_InvalidParamChars_2(CharU5BU5D_t1328083999* value)
	{
		___InvalidParamChars_2 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidParamChars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
