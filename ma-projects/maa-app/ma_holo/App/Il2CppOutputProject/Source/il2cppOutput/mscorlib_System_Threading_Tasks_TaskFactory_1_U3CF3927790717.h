﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<System.IAsyncResult,System.Object>
struct Func_2_t2344073568;
// System.Action`1<System.IAsyncResult>
struct Action_1_t1801450390;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_t1809478302;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Object>
struct  U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717  : public Il2CppObject
{
public:
	// System.Func`2<System.IAsyncResult,TResult> System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2::endFunction
	Func_2_t2344073568 * ___endFunction_0;
	// System.Action`1<System.IAsyncResult> System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2::endAction
	Action_1_t1801450390 * ___endAction_1;
	// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2::promise
	Task_1_t1809478302 * ___promise_2;

public:
	inline static int32_t get_offset_of_endFunction_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717, ___endFunction_0)); }
	inline Func_2_t2344073568 * get_endFunction_0() const { return ___endFunction_0; }
	inline Func_2_t2344073568 ** get_address_of_endFunction_0() { return &___endFunction_0; }
	inline void set_endFunction_0(Func_2_t2344073568 * value)
	{
		___endFunction_0 = value;
		Il2CppCodeGenWriteBarrier(&___endFunction_0, value);
	}

	inline static int32_t get_offset_of_endAction_1() { return static_cast<int32_t>(offsetof(U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717, ___endAction_1)); }
	inline Action_1_t1801450390 * get_endAction_1() const { return ___endAction_1; }
	inline Action_1_t1801450390 ** get_address_of_endAction_1() { return &___endAction_1; }
	inline void set_endAction_1(Action_1_t1801450390 * value)
	{
		___endAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___endAction_1, value);
	}

	inline static int32_t get_offset_of_promise_2() { return static_cast<int32_t>(offsetof(U3CFromAsyncImplU3Ec__AnonStorey2_t3927790717, ___promise_2)); }
	inline Task_1_t1809478302 * get_promise_2() const { return ___promise_2; }
	inline Task_1_t1809478302 ** get_address_of_promise_2() { return &___promise_2; }
	inline void set_promise_2(Task_1_t1809478302 * value)
	{
		___promise_2 = value;
		Il2CppCodeGenWriteBarrier(&___promise_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
