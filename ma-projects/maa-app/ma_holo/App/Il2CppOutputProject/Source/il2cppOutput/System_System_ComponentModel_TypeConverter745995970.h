﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t745995970  : public Il2CppObject
{
public:

public:
};

struct TypeConverter_t745995970_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::firstLoadAppSetting
	bool ___firstLoadAppSetting_2;
	// System.Object System.ComponentModel.TypeConverter::loadAppSettingLock
	Il2CppObject * ___loadAppSettingLock_3;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t745995970_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}

	inline static int32_t get_offset_of_firstLoadAppSetting_2() { return static_cast<int32_t>(offsetof(TypeConverter_t745995970_StaticFields, ___firstLoadAppSetting_2)); }
	inline bool get_firstLoadAppSetting_2() const { return ___firstLoadAppSetting_2; }
	inline bool* get_address_of_firstLoadAppSetting_2() { return &___firstLoadAppSetting_2; }
	inline void set_firstLoadAppSetting_2(bool value)
	{
		___firstLoadAppSetting_2 = value;
	}

	inline static int32_t get_offset_of_loadAppSettingLock_3() { return static_cast<int32_t>(offsetof(TypeConverter_t745995970_StaticFields, ___loadAppSettingLock_3)); }
	inline Il2CppObject * get_loadAppSettingLock_3() const { return ___loadAppSettingLock_3; }
	inline Il2CppObject ** get_address_of_loadAppSettingLock_3() { return &___loadAppSettingLock_3; }
	inline void set_loadAppSettingLock_3(Il2CppObject * value)
	{
		___loadAppSettingLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___loadAppSettingLock_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
