﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_CapturePixel3370027235.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3470796049.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture2660512180.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3170939459.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture3210647042.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1811850718.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1862037614.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture2612745932.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_VideoCapture1976525720.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1414746886.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture3943606183.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture3813459671.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture2698966894.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture_522077320.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture2742193416.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1784202510.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCapture1623734756.h"
#include "UnityEngine_UnityEngine_VR_WSA_WebCam_PhotoCaptureF928993319.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo2135528789.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1972476489.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo1509183288.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSou396045678.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSou830383220.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldAnchor100028935.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldAnchor_OnTracki417897799.h"
#include "UnityEngine_UnityEngine_VR_WSA_PositionalLocatorSt3556528907.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldManager2948982693.h"
#include "UnityEngine_UnityEngine_VR_WSA_WorldManager_OnPosi3864636813.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer10059812.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_EventHan2685920451.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_ErrorEve3983973519.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_FrameRea2353988013.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_AddComponentMenu1099699699.h"
#include "UnityEngine_UnityEngine_ContextMenu2283362202.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder2717914595.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute1576243993.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin107439586.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe4226409784.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttribu689702060.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttri3403607913.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttribu14323075.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJob987733588.h"
#include "UnityEngine_UnityEngine_Collections_NativeContainer269240268.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3267933728.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1288953595.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Frame658788566.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (CapturePixelFormat_t3370027235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3100[5] = 
{
	CapturePixelFormat_t3370027235::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (VideoCapture_t3470796049), -1, sizeof(VideoCapture_t3470796049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3101[2] = 
{
	VideoCapture_t3470796049_StaticFields::get_offset_of_HR_SUCCESS_0(),
	VideoCapture_t3470796049::get_offset_of_m_NativePtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (CaptureResultType_t2660512180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3102[3] = 
{
	CaptureResultType_t2660512180::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (VideoCaptureResult_t3170939459)+ sizeof (Il2CppObject), sizeof(VideoCaptureResult_t3170939459 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3103[2] = 
{
	VideoCaptureResult_t3170939459::get_offset_of_resultType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoCaptureResult_t3170939459::get_offset_of_hResult_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (OnVideoCaptureResourceCreatedCallback_t3210647042), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (OnVideoModeStartedCallback_t1811850718), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (OnVideoModeStoppedCallback_t1862037614), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (OnStartedRecordingVideoCallback_t2612745932), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (OnStoppedRecordingVideoCallback_t1976525720), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (PhotoCapture_t1414746886), -1, sizeof(PhotoCapture_t1414746886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3109[2] = 
{
	PhotoCapture_t1414746886_StaticFields::get_offset_of_HR_SUCCESS_0(),
	PhotoCapture_t1414746886::get_offset_of_m_NativePtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (CaptureResultType_t3943606183)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3110[3] = 
{
	CaptureResultType_t3943606183::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (PhotoCaptureResult_t3813459671)+ sizeof (Il2CppObject), sizeof(PhotoCaptureResult_t3813459671 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3111[2] = 
{
	PhotoCaptureResult_t3813459671::get_offset_of_resultType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotoCaptureResult_t3813459671::get_offset_of_hResult_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (OnCaptureResourceCreatedCallback_t2698966894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (OnPhotoModeStartedCallback_t522077320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (OnPhotoModeStoppedCallback_t2742193416), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (OnCapturedToDiskCallback_t1784202510), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (OnCapturedToMemoryCallback_t1623734756), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (PhotoCaptureFrame_t928993319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[4] = 
{
	PhotoCaptureFrame_t928993319::get_offset_of_m_NativePtr_0(),
	PhotoCaptureFrame_t928993319::get_offset_of_U3CdataLengthU3Ek__BackingField_1(),
	PhotoCaptureFrame_t928993319::get_offset_of_U3ChasLocationDataU3Ek__BackingField_2(),
	PhotoCaptureFrame_t928993319::get_offset_of_U3CpixelFormatU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (InteractionSourceKind_t2135528789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3118[5] = 
{
	InteractionSourceKind_t2135528789::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (InteractionSource_t1972476489)+ sizeof (Il2CppObject), sizeof(InteractionSource_t1972476489 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	InteractionSource_t1972476489::get_offset_of_m_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSource_t1972476489::get_offset_of_m_kind_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (InteractionSourceLocation_t1509183288)+ sizeof (Il2CppObject), sizeof(InteractionSourceLocation_t1509183288 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3120[4] = 
{
	InteractionSourceLocation_t1509183288::get_offset_of_m_hasPosition_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceLocation_t1509183288::get_offset_of_m_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceLocation_t1509183288::get_offset_of_m_hasVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceLocation_t1509183288::get_offset_of_m_velocity_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (InteractionSourceProperties_t396045678)+ sizeof (Il2CppObject), sizeof(InteractionSourceProperties_t396045678 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3121[3] = 
{
	InteractionSourceProperties_t396045678::get_offset_of_m_sourceLossRisk_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceProperties_t396045678::get_offset_of_m_sourceLossMitigationDirection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceProperties_t396045678::get_offset_of_m_location_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (InteractionSourceState_t830383220)+ sizeof (Il2CppObject), sizeof(InteractionSourceState_t830383220 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3122[4] = 
{
	InteractionSourceState_t830383220::get_offset_of_m_pressed_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceState_t830383220::get_offset_of_m_properties_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceState_t830383220::get_offset_of_m_source_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InteractionSourceState_t830383220::get_offset_of_m_headRay_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (WorldAnchor_t100028935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[1] = 
{
	WorldAnchor_t100028935::get_offset_of_OnTrackingChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (OnTrackingChangedDelegate_t417897799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (PositionalLocatorState_t3556528907)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3125[6] = 
{
	PositionalLocatorState_t3556528907::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (WorldManager_t2948982693), -1, sizeof(WorldManager_t2948982693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3126[1] = 
{
	WorldManager_t2948982693_StaticFields::get_offset_of_OnPositionalLocatorStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (OnPositionalLocatorStateChangedDelegate_t3864636813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (VideoPlayer_t10059812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[7] = 
{
	VideoPlayer_t10059812::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t10059812::get_offset_of_loopPointReached_3(),
	VideoPlayer_t10059812::get_offset_of_started_4(),
	VideoPlayer_t10059812::get_offset_of_frameDropped_5(),
	VideoPlayer_t10059812::get_offset_of_errorReceived_6(),
	VideoPlayer_t10059812::get_offset_of_seekCompleted_7(),
	VideoPlayer_t10059812::get_offset_of_frameReady_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (EventHandler_t2685920451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (ErrorEventHandler_t3983973519), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (FrameReadyEventHandler_t2353988013), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (AttributeHelperEngine_t958797062), -1, sizeof(AttributeHelperEngine_t958797062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3132[3] = 
{
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (DisallowMultipleComponent_t2656950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (RequireComponent_t864575032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[3] = 
{
	RequireComponent_t864575032::get_offset_of_m_Type0_0(),
	RequireComponent_t864575032::get_offset_of_m_Type1_1(),
	RequireComponent_t864575032::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (AddComponentMenu_t1099699699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[2] = 
{
	AddComponentMenu_t1099699699::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t1099699699::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (ContextMenu_t2283362202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[3] = 
{
	ContextMenu_t2283362202::get_offset_of_menuItem_0(),
	ContextMenu_t2283362202::get_offset_of_validate_1(),
	ContextMenu_t2283362202::get_offset_of_priority_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (ExecuteInEditMode_t3043633143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (DefaultExecutionOrder_t2717914595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[1] = 
{
	DefaultExecutionOrder_t2717914595::get_offset_of_U3CorderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (NativeClassAttribute_t1576243993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[1] = 
{
	NativeClassAttribute_t1576243993::get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (GeneratedByOldBindingsGeneratorAttribute_t107439586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (SendMessageOptions_t1414041951)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3141[3] = 
{
	SendMessageOptions_t1414041951::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (LogType_t1559732862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3142[6] = 
{
	LogType_t1559732862::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (Color_t2020392075)+ sizeof (Il2CppObject), sizeof(Color_t2020392075 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3144[4] = 
{
	Color_t2020392075::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (SetupCoroutine_t3582942563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (WritableAttribute_t3715198420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (AssemblyIsEditorAssembly_t1557026495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (CameraClearFlags_t452084705)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3148[6] = 
{
	CameraClearFlags_t452084705::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (KeyCode_t2283395152)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3149[322] = 
{
	KeyCode_t2283395152::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (MathfInternal_t715669973)+ sizeof (Il2CppObject), sizeof(MathfInternal_t715669973 ), sizeof(MathfInternal_t715669973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3150[3] = 
{
	MathfInternal_t715669973_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t715669973_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t715669973_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SendMouseEvents_t3505065032), -1, sizeof(SendMouseEvents_t3505065032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3151[5] = 
{
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (HitInfo_t1761367055)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[2] = 
{
	HitInfo_t1761367055::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t1761367055::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (Ray_t2469606224)+ sizeof (Il2CppObject), sizeof(Ray_t2469606224 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3153[2] = 
{
	Ray_t2469606224::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t2469606224::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (Rect_t3681755626)+ sizeof (Il2CppObject), sizeof(Rect_t3681755626 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3154[4] = 
{
	Rect_t3681755626::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t3681755626::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (SerializePrivateVariables_t2241034664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (SerializeField_t3073427462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (PreferBinarySerialization_t2472773525), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (StackTraceUtility_t1881293839), -1, sizeof(StackTraceUtility_t1881293839_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3159[1] = 
{
	StackTraceUtility_t1881293839_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (UnityException_t2687879050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[2] = 
{
	0,
	UnityException_t2687879050::get_offset_of_unityStackTrace_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (TrackedReference_t1045890189), sizeof(TrackedReference_t1045890189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3161[1] = 
{
	TrackedReference_t1045890189::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3162[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3171[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3187[2] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3188[4] = 
{
	Vector4_t2243707581::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (ThreadAndSerializationSafeAttribute_t4226409784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (ReadOnlyAttribute_t689702060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (ReadWriteAttribute_t3403607913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (WriteOnlyAttribute_t14323075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (DeallocateOnJobCompletionAttribute_t987733588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (NativeContainerAttribute_t269240268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3267933728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1288953595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3197[6] = 
{
	FrameData_t1120735295::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (Flags_t658788566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3198[3] = 
{
	Flags_t658788566::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
