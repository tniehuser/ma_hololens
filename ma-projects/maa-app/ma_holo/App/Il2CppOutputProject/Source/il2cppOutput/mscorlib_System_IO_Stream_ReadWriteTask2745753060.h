﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task_1_gen1191906455.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream/ReadWriteTask
struct  ReadWriteTask_t2745753060  : public Task_1_t1191906455
{
public:
	// System.Boolean System.IO.Stream/ReadWriteTask::_isRead
	bool ____isRead_27;
	// System.IO.Stream System.IO.Stream/ReadWriteTask::_stream
	Stream_t3255436806 * ____stream_28;
	// System.Byte[] System.IO.Stream/ReadWriteTask::_buffer
	ByteU5BU5D_t3397334013* ____buffer_29;
	// System.Int32 System.IO.Stream/ReadWriteTask::_offset
	int32_t ____offset_30;
	// System.Int32 System.IO.Stream/ReadWriteTask::_count
	int32_t ____count_31;
	// System.AsyncCallback System.IO.Stream/ReadWriteTask::_callback
	AsyncCallback_t163412349 * ____callback_32;
	// System.Threading.ExecutionContext System.IO.Stream/ReadWriteTask::_context
	ExecutionContext_t1392266323 * ____context_33;

public:
	inline static int32_t get_offset_of__isRead_27() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____isRead_27)); }
	inline bool get__isRead_27() const { return ____isRead_27; }
	inline bool* get_address_of__isRead_27() { return &____isRead_27; }
	inline void set__isRead_27(bool value)
	{
		____isRead_27 = value;
	}

	inline static int32_t get_offset_of__stream_28() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____stream_28)); }
	inline Stream_t3255436806 * get__stream_28() const { return ____stream_28; }
	inline Stream_t3255436806 ** get_address_of__stream_28() { return &____stream_28; }
	inline void set__stream_28(Stream_t3255436806 * value)
	{
		____stream_28 = value;
		Il2CppCodeGenWriteBarrier(&____stream_28, value);
	}

	inline static int32_t get_offset_of__buffer_29() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____buffer_29)); }
	inline ByteU5BU5D_t3397334013* get__buffer_29() const { return ____buffer_29; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_29() { return &____buffer_29; }
	inline void set__buffer_29(ByteU5BU5D_t3397334013* value)
	{
		____buffer_29 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_29, value);
	}

	inline static int32_t get_offset_of__offset_30() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____offset_30)); }
	inline int32_t get__offset_30() const { return ____offset_30; }
	inline int32_t* get_address_of__offset_30() { return &____offset_30; }
	inline void set__offset_30(int32_t value)
	{
		____offset_30 = value;
	}

	inline static int32_t get_offset_of__count_31() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____count_31)); }
	inline int32_t get__count_31() const { return ____count_31; }
	inline int32_t* get_address_of__count_31() { return &____count_31; }
	inline void set__count_31(int32_t value)
	{
		____count_31 = value;
	}

	inline static int32_t get_offset_of__callback_32() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____callback_32)); }
	inline AsyncCallback_t163412349 * get__callback_32() const { return ____callback_32; }
	inline AsyncCallback_t163412349 ** get_address_of__callback_32() { return &____callback_32; }
	inline void set__callback_32(AsyncCallback_t163412349 * value)
	{
		____callback_32 = value;
		Il2CppCodeGenWriteBarrier(&____callback_32, value);
	}

	inline static int32_t get_offset_of__context_33() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060, ____context_33)); }
	inline ExecutionContext_t1392266323 * get__context_33() const { return ____context_33; }
	inline ExecutionContext_t1392266323 ** get_address_of__context_33() { return &____context_33; }
	inline void set__context_33(ExecutionContext_t1392266323 * value)
	{
		____context_33 = value;
		Il2CppCodeGenWriteBarrier(&____context_33, value);
	}
};

struct ReadWriteTask_t2745753060_StaticFields
{
public:
	// System.Threading.ContextCallback System.IO.Stream/ReadWriteTask::s_invokeAsyncCallback
	ContextCallback_t2287130692 * ___s_invokeAsyncCallback_34;
	// System.Threading.ContextCallback System.IO.Stream/ReadWriteTask::<>f__mg$cache0
	ContextCallback_t2287130692 * ___U3CU3Ef__mgU24cache0_35;

public:
	inline static int32_t get_offset_of_s_invokeAsyncCallback_34() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060_StaticFields, ___s_invokeAsyncCallback_34)); }
	inline ContextCallback_t2287130692 * get_s_invokeAsyncCallback_34() const { return ___s_invokeAsyncCallback_34; }
	inline ContextCallback_t2287130692 ** get_address_of_s_invokeAsyncCallback_34() { return &___s_invokeAsyncCallback_34; }
	inline void set_s_invokeAsyncCallback_34(ContextCallback_t2287130692 * value)
	{
		___s_invokeAsyncCallback_34 = value;
		Il2CppCodeGenWriteBarrier(&___s_invokeAsyncCallback_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_35() { return static_cast<int32_t>(offsetof(ReadWriteTask_t2745753060_StaticFields, ___U3CU3Ef__mgU24cache0_35)); }
	inline ContextCallback_t2287130692 * get_U3CU3Ef__mgU24cache0_35() const { return ___U3CU3Ef__mgU24cache0_35; }
	inline ContextCallback_t2287130692 ** get_address_of_U3CU3Ef__mgU24cache0_35() { return &___U3CU3Ef__mgU24cache0_35; }
	inline void set_U3CU3Ef__mgU24cache0_35(ContextCallback_t2287130692 * value)
	{
		___U3CU3Ef__mgU24cache0_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
