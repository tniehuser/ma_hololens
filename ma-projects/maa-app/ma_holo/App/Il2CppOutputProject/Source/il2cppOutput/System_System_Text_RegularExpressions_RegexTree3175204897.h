﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"

// System.Text.RegularExpressions.RegexNode
struct RegexNode_t2469392321;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexTree
struct  RegexTree_t3175204897  : public Il2CppObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexTree::_root
	RegexNode_t2469392321 * ____root_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_caps
	Hashtable_t909839986 * ____caps_1;
	// System.Int32[] System.Text.RegularExpressions.RegexTree::_capnumlist
	Int32U5BU5D_t3030399641* ____capnumlist_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_capnames
	Hashtable_t909839986 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.RegexTree::_capslist
	StringU5BU5D_t1642385972* ____capslist_4;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexTree::_options
	int32_t ____options_5;
	// System.Int32 System.Text.RegularExpressions.RegexTree::_captop
	int32_t ____captop_6;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____root_0)); }
	inline RegexNode_t2469392321 * get__root_0() const { return ____root_0; }
	inline RegexNode_t2469392321 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(RegexNode_t2469392321 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier(&____root_0, value);
	}

	inline static int32_t get_offset_of__caps_1() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____caps_1)); }
	inline Hashtable_t909839986 * get__caps_1() const { return ____caps_1; }
	inline Hashtable_t909839986 ** get_address_of__caps_1() { return &____caps_1; }
	inline void set__caps_1(Hashtable_t909839986 * value)
	{
		____caps_1 = value;
		Il2CppCodeGenWriteBarrier(&____caps_1, value);
	}

	inline static int32_t get_offset_of__capnumlist_2() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capnumlist_2)); }
	inline Int32U5BU5D_t3030399641* get__capnumlist_2() const { return ____capnumlist_2; }
	inline Int32U5BU5D_t3030399641** get_address_of__capnumlist_2() { return &____capnumlist_2; }
	inline void set__capnumlist_2(Int32U5BU5D_t3030399641* value)
	{
		____capnumlist_2 = value;
		Il2CppCodeGenWriteBarrier(&____capnumlist_2, value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capnames_3)); }
	inline Hashtable_t909839986 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t909839986 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t909839986 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier(&____capnames_3, value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____capslist_4)); }
	inline StringU5BU5D_t1642385972* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1642385972** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1642385972* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier(&____capslist_4, value);
	}

	inline static int32_t get_offset_of__options_5() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____options_5)); }
	inline int32_t get__options_5() const { return ____options_5; }
	inline int32_t* get_address_of__options_5() { return &____options_5; }
	inline void set__options_5(int32_t value)
	{
		____options_5 = value;
	}

	inline static int32_t get_offset_of__captop_6() { return static_cast<int32_t>(offsetof(RegexTree_t3175204897, ____captop_6)); }
	inline int32_t get__captop_6() const { return ____captop_6; }
	inline int32_t* get_address_of__captop_6() { return &____captop_6; }
	inline void set__captop_6(int32_t value)
	{
		____captop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
