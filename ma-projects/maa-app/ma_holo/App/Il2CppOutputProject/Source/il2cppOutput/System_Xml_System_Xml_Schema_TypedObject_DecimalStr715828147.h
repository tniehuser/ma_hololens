﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Decimal[]
struct DecimalU5BU5D_t624008824;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject/DecimalStruct
struct  DecimalStruct_t715828147  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.Schema.TypedObject/DecimalStruct::isDecimal
	bool ___isDecimal_0;
	// System.Decimal[] System.Xml.Schema.TypedObject/DecimalStruct::dvalue
	DecimalU5BU5D_t624008824* ___dvalue_1;

public:
	inline static int32_t get_offset_of_isDecimal_0() { return static_cast<int32_t>(offsetof(DecimalStruct_t715828147, ___isDecimal_0)); }
	inline bool get_isDecimal_0() const { return ___isDecimal_0; }
	inline bool* get_address_of_isDecimal_0() { return &___isDecimal_0; }
	inline void set_isDecimal_0(bool value)
	{
		___isDecimal_0 = value;
	}

	inline static int32_t get_offset_of_dvalue_1() { return static_cast<int32_t>(offsetof(DecimalStruct_t715828147, ___dvalue_1)); }
	inline DecimalU5BU5D_t624008824* get_dvalue_1() const { return ___dvalue_1; }
	inline DecimalU5BU5D_t624008824** get_address_of_dvalue_1() { return &___dvalue_1; }
	inline void set_dvalue_1(DecimalU5BU5D_t624008824* value)
	{
		___dvalue_1 = value;
		Il2CppCodeGenWriteBarrier(&___dvalue_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
