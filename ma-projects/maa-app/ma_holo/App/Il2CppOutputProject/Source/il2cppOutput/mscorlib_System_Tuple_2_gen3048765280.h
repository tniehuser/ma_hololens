﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`2<System.IO.TextWriter,System.String>
struct  Tuple_2_t3048765280  : public Il2CppObject
{
public:
	// T1 System.Tuple`2::m_Item1
	TextWriter_t4027217640 * ___m_Item1_0;
	// T2 System.Tuple`2::m_Item2
	String_t* ___m_Item2_1;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t3048765280, ___m_Item1_0)); }
	inline TextWriter_t4027217640 * get_m_Item1_0() const { return ___m_Item1_0; }
	inline TextWriter_t4027217640 ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(TextWriter_t4027217640 * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t3048765280, ___m_Item2_1)); }
	inline String_t* get_m_Item2_1() const { return ___m_Item2_1; }
	inline String_t** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(String_t* value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
