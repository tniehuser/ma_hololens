﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.UInt64[]
struct UInt64U5BU5D_t1668688775;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum/ValuesAndNames
struct  ValuesAndNames_t2142337900  : public Il2CppObject
{
public:
	// System.UInt64[] System.Enum/ValuesAndNames::Values
	UInt64U5BU5D_t1668688775* ___Values_0;
	// System.String[] System.Enum/ValuesAndNames::Names
	StringU5BU5D_t1642385972* ___Names_1;

public:
	inline static int32_t get_offset_of_Values_0() { return static_cast<int32_t>(offsetof(ValuesAndNames_t2142337900, ___Values_0)); }
	inline UInt64U5BU5D_t1668688775* get_Values_0() const { return ___Values_0; }
	inline UInt64U5BU5D_t1668688775** get_address_of_Values_0() { return &___Values_0; }
	inline void set_Values_0(UInt64U5BU5D_t1668688775* value)
	{
		___Values_0 = value;
		Il2CppCodeGenWriteBarrier(&___Values_0, value);
	}

	inline static int32_t get_offset_of_Names_1() { return static_cast<int32_t>(offsetof(ValuesAndNames_t2142337900, ___Names_1)); }
	inline StringU5BU5D_t1642385972* get_Names_1() const { return ___Names_1; }
	inline StringU5BU5D_t1642385972** get_address_of_Names_1() { return &___Names_1; }
	inline void set_Names_1(StringU5BU5D_t1642385972* value)
	{
		___Names_1 = value;
		Il2CppCodeGenWriteBarrier(&___Names_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
