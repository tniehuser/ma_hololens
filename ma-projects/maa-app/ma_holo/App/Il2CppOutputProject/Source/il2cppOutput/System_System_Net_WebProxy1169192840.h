﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Uri
struct Uri_t19570940;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t3677892936;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Net.AutoWebProxyScriptEngine
struct AutoWebProxyScriptEngine_t2702566410;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t1169192840  : public Il2CppObject
{
public:
	// System.Boolean System.Net.WebProxy::_UseRegistry
	bool ____UseRegistry_0;
	// System.Boolean System.Net.WebProxy::_BypassOnLocal
	bool ____BypassOnLocal_1;
	// System.Boolean System.Net.WebProxy::m_EnableAutoproxy
	bool ___m_EnableAutoproxy_2;
	// System.Uri System.Net.WebProxy::_ProxyAddress
	Uri_t19570940 * ____ProxyAddress_3;
	// System.Collections.ArrayList System.Net.WebProxy::_BypassList
	ArrayList_t4252133567 * ____BypassList_4;
	// System.Net.ICredentials System.Net.WebProxy::_Credentials
	Il2CppObject * ____Credentials_5;
	// System.Text.RegularExpressions.Regex[] System.Net.WebProxy::_RegExBypassList
	RegexU5BU5D_t3677892936* ____RegExBypassList_6;
	// System.Collections.Hashtable System.Net.WebProxy::_ProxyHostAddresses
	Hashtable_t909839986 * ____ProxyHostAddresses_7;
	// System.Net.AutoWebProxyScriptEngine System.Net.WebProxy::m_ScriptEngine
	AutoWebProxyScriptEngine_t2702566410 * ___m_ScriptEngine_8;

public:
	inline static int32_t get_offset_of__UseRegistry_0() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____UseRegistry_0)); }
	inline bool get__UseRegistry_0() const { return ____UseRegistry_0; }
	inline bool* get_address_of__UseRegistry_0() { return &____UseRegistry_0; }
	inline void set__UseRegistry_0(bool value)
	{
		____UseRegistry_0 = value;
	}

	inline static int32_t get_offset_of__BypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____BypassOnLocal_1)); }
	inline bool get__BypassOnLocal_1() const { return ____BypassOnLocal_1; }
	inline bool* get_address_of__BypassOnLocal_1() { return &____BypassOnLocal_1; }
	inline void set__BypassOnLocal_1(bool value)
	{
		____BypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_m_EnableAutoproxy_2() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ___m_EnableAutoproxy_2)); }
	inline bool get_m_EnableAutoproxy_2() const { return ___m_EnableAutoproxy_2; }
	inline bool* get_address_of_m_EnableAutoproxy_2() { return &___m_EnableAutoproxy_2; }
	inline void set_m_EnableAutoproxy_2(bool value)
	{
		___m_EnableAutoproxy_2 = value;
	}

	inline static int32_t get_offset_of__ProxyAddress_3() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____ProxyAddress_3)); }
	inline Uri_t19570940 * get__ProxyAddress_3() const { return ____ProxyAddress_3; }
	inline Uri_t19570940 ** get_address_of__ProxyAddress_3() { return &____ProxyAddress_3; }
	inline void set__ProxyAddress_3(Uri_t19570940 * value)
	{
		____ProxyAddress_3 = value;
		Il2CppCodeGenWriteBarrier(&____ProxyAddress_3, value);
	}

	inline static int32_t get_offset_of__BypassList_4() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____BypassList_4)); }
	inline ArrayList_t4252133567 * get__BypassList_4() const { return ____BypassList_4; }
	inline ArrayList_t4252133567 ** get_address_of__BypassList_4() { return &____BypassList_4; }
	inline void set__BypassList_4(ArrayList_t4252133567 * value)
	{
		____BypassList_4 = value;
		Il2CppCodeGenWriteBarrier(&____BypassList_4, value);
	}

	inline static int32_t get_offset_of__Credentials_5() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____Credentials_5)); }
	inline Il2CppObject * get__Credentials_5() const { return ____Credentials_5; }
	inline Il2CppObject ** get_address_of__Credentials_5() { return &____Credentials_5; }
	inline void set__Credentials_5(Il2CppObject * value)
	{
		____Credentials_5 = value;
		Il2CppCodeGenWriteBarrier(&____Credentials_5, value);
	}

	inline static int32_t get_offset_of__RegExBypassList_6() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____RegExBypassList_6)); }
	inline RegexU5BU5D_t3677892936* get__RegExBypassList_6() const { return ____RegExBypassList_6; }
	inline RegexU5BU5D_t3677892936** get_address_of__RegExBypassList_6() { return &____RegExBypassList_6; }
	inline void set__RegExBypassList_6(RegexU5BU5D_t3677892936* value)
	{
		____RegExBypassList_6 = value;
		Il2CppCodeGenWriteBarrier(&____RegExBypassList_6, value);
	}

	inline static int32_t get_offset_of__ProxyHostAddresses_7() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ____ProxyHostAddresses_7)); }
	inline Hashtable_t909839986 * get__ProxyHostAddresses_7() const { return ____ProxyHostAddresses_7; }
	inline Hashtable_t909839986 ** get_address_of__ProxyHostAddresses_7() { return &____ProxyHostAddresses_7; }
	inline void set__ProxyHostAddresses_7(Hashtable_t909839986 * value)
	{
		____ProxyHostAddresses_7 = value;
		Il2CppCodeGenWriteBarrier(&____ProxyHostAddresses_7, value);
	}

	inline static int32_t get_offset_of_m_ScriptEngine_8() { return static_cast<int32_t>(offsetof(WebProxy_t1169192840, ___m_ScriptEngine_8)); }
	inline AutoWebProxyScriptEngine_t2702566410 * get_m_ScriptEngine_8() const { return ___m_ScriptEngine_8; }
	inline AutoWebProxyScriptEngine_t2702566410 ** get_address_of_m_ScriptEngine_8() { return &___m_ScriptEngine_8; }
	inline void set_m_ScriptEngine_8(AutoWebProxyScriptEngine_t2702566410 * value)
	{
		___m_ScriptEngine_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_ScriptEngine_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
