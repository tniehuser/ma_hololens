﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t1510907267;
// System.Xml.Schema.XdrBuilder/DeclBaseInfo
struct DeclBaseInfo_t1711216790;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/DeclBaseInfo
struct  DeclBaseInfo_t1711216790  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Name
	XmlQualifiedName_t1944712516 * ____Name_0;
	// System.String System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Prefix
	String_t* ____Prefix_1;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/DeclBaseInfo::_TypeName
	XmlQualifiedName_t1944712516 * ____TypeName_2;
	// System.String System.Xml.Schema.XdrBuilder/DeclBaseInfo::_TypePrefix
	String_t* ____TypePrefix_3;
	// System.Object System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Default
	Il2CppObject * ____Default_4;
	// System.Object System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Revises
	Il2CppObject * ____Revises_5;
	// System.UInt32 System.Xml.Schema.XdrBuilder/DeclBaseInfo::_MaxOccurs
	uint32_t ____MaxOccurs_6;
	// System.UInt32 System.Xml.Schema.XdrBuilder/DeclBaseInfo::_MinOccurs
	uint32_t ____MinOccurs_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Checking
	bool ____Checking_8;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XdrBuilder/DeclBaseInfo::_ElementDecl
	SchemaElementDecl_t1940851905 * ____ElementDecl_9;
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Attdef
	SchemaAttDef_t1510907267 * ____Attdef_10;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Next
	DeclBaseInfo_t1711216790 * ____Next_11;

public:
	inline static int32_t get_offset_of__Name_0() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Name_0)); }
	inline XmlQualifiedName_t1944712516 * get__Name_0() const { return ____Name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__Name_0() { return &____Name_0; }
	inline void set__Name_0(XmlQualifiedName_t1944712516 * value)
	{
		____Name_0 = value;
		Il2CppCodeGenWriteBarrier(&____Name_0, value);
	}

	inline static int32_t get_offset_of__Prefix_1() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Prefix_1)); }
	inline String_t* get__Prefix_1() const { return ____Prefix_1; }
	inline String_t** get_address_of__Prefix_1() { return &____Prefix_1; }
	inline void set__Prefix_1(String_t* value)
	{
		____Prefix_1 = value;
		Il2CppCodeGenWriteBarrier(&____Prefix_1, value);
	}

	inline static int32_t get_offset_of__TypeName_2() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____TypeName_2)); }
	inline XmlQualifiedName_t1944712516 * get__TypeName_2() const { return ____TypeName_2; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__TypeName_2() { return &____TypeName_2; }
	inline void set__TypeName_2(XmlQualifiedName_t1944712516 * value)
	{
		____TypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&____TypeName_2, value);
	}

	inline static int32_t get_offset_of__TypePrefix_3() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____TypePrefix_3)); }
	inline String_t* get__TypePrefix_3() const { return ____TypePrefix_3; }
	inline String_t** get_address_of__TypePrefix_3() { return &____TypePrefix_3; }
	inline void set__TypePrefix_3(String_t* value)
	{
		____TypePrefix_3 = value;
		Il2CppCodeGenWriteBarrier(&____TypePrefix_3, value);
	}

	inline static int32_t get_offset_of__Default_4() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Default_4)); }
	inline Il2CppObject * get__Default_4() const { return ____Default_4; }
	inline Il2CppObject ** get_address_of__Default_4() { return &____Default_4; }
	inline void set__Default_4(Il2CppObject * value)
	{
		____Default_4 = value;
		Il2CppCodeGenWriteBarrier(&____Default_4, value);
	}

	inline static int32_t get_offset_of__Revises_5() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Revises_5)); }
	inline Il2CppObject * get__Revises_5() const { return ____Revises_5; }
	inline Il2CppObject ** get_address_of__Revises_5() { return &____Revises_5; }
	inline void set__Revises_5(Il2CppObject * value)
	{
		____Revises_5 = value;
		Il2CppCodeGenWriteBarrier(&____Revises_5, value);
	}

	inline static int32_t get_offset_of__MaxOccurs_6() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____MaxOccurs_6)); }
	inline uint32_t get__MaxOccurs_6() const { return ____MaxOccurs_6; }
	inline uint32_t* get_address_of__MaxOccurs_6() { return &____MaxOccurs_6; }
	inline void set__MaxOccurs_6(uint32_t value)
	{
		____MaxOccurs_6 = value;
	}

	inline static int32_t get_offset_of__MinOccurs_7() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____MinOccurs_7)); }
	inline uint32_t get__MinOccurs_7() const { return ____MinOccurs_7; }
	inline uint32_t* get_address_of__MinOccurs_7() { return &____MinOccurs_7; }
	inline void set__MinOccurs_7(uint32_t value)
	{
		____MinOccurs_7 = value;
	}

	inline static int32_t get_offset_of__Checking_8() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Checking_8)); }
	inline bool get__Checking_8() const { return ____Checking_8; }
	inline bool* get_address_of__Checking_8() { return &____Checking_8; }
	inline void set__Checking_8(bool value)
	{
		____Checking_8 = value;
	}

	inline static int32_t get_offset_of__ElementDecl_9() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____ElementDecl_9)); }
	inline SchemaElementDecl_t1940851905 * get__ElementDecl_9() const { return ____ElementDecl_9; }
	inline SchemaElementDecl_t1940851905 ** get_address_of__ElementDecl_9() { return &____ElementDecl_9; }
	inline void set__ElementDecl_9(SchemaElementDecl_t1940851905 * value)
	{
		____ElementDecl_9 = value;
		Il2CppCodeGenWriteBarrier(&____ElementDecl_9, value);
	}

	inline static int32_t get_offset_of__Attdef_10() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Attdef_10)); }
	inline SchemaAttDef_t1510907267 * get__Attdef_10() const { return ____Attdef_10; }
	inline SchemaAttDef_t1510907267 ** get_address_of__Attdef_10() { return &____Attdef_10; }
	inline void set__Attdef_10(SchemaAttDef_t1510907267 * value)
	{
		____Attdef_10 = value;
		Il2CppCodeGenWriteBarrier(&____Attdef_10, value);
	}

	inline static int32_t get_offset_of__Next_11() { return static_cast<int32_t>(offsetof(DeclBaseInfo_t1711216790, ____Next_11)); }
	inline DeclBaseInfo_t1711216790 * get__Next_11() const { return ____Next_11; }
	inline DeclBaseInfo_t1711216790 ** get_address_of__Next_11() { return &____Next_11; }
	inline void set__Next_11(DeclBaseInfo_t1711216790 * value)
	{
		____Next_11 = value;
		Il2CppCodeGenWriteBarrier(&____Next_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
