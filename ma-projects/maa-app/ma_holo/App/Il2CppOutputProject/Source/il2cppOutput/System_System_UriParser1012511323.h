﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_UriSyntaxFlags1242716474.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_System_UriParser_UriQuirksVersion4233729352.h"

// System.Collections.Generic.Dictionary`2<System.String,System.UriParser>
struct Dictionary_2_t2927290585;
// System.String
struct String_t;
// System.UriParser
struct UriParser_t1012511323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser
struct  UriParser_t1012511323  : public Il2CppObject
{
public:
	// System.UriSyntaxFlags System.UriParser::m_Flags
	int32_t ___m_Flags_2;
	// System.UriSyntaxFlags modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlags
	int32_t ___m_UpdatableFlags_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlagsUsed
	bool ___m_UpdatableFlagsUsed_4;
	// System.Int32 System.UriParser::m_Port
	int32_t ___m_Port_5;
	// System.String System.UriParser::m_Scheme
	String_t* ___m_Scheme_6;

public:
	inline static int32_t get_offset_of_m_Flags_2() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Flags_2)); }
	inline int32_t get_m_Flags_2() const { return ___m_Flags_2; }
	inline int32_t* get_address_of_m_Flags_2() { return &___m_Flags_2; }
	inline void set_m_Flags_2(int32_t value)
	{
		___m_Flags_2 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlags_3() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_UpdatableFlags_3)); }
	inline int32_t get_m_UpdatableFlags_3() const { return ___m_UpdatableFlags_3; }
	inline int32_t* get_address_of_m_UpdatableFlags_3() { return &___m_UpdatableFlags_3; }
	inline void set_m_UpdatableFlags_3(int32_t value)
	{
		___m_UpdatableFlags_3 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlagsUsed_4() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_UpdatableFlagsUsed_4)); }
	inline bool get_m_UpdatableFlagsUsed_4() const { return ___m_UpdatableFlagsUsed_4; }
	inline bool* get_address_of_m_UpdatableFlagsUsed_4() { return &___m_UpdatableFlagsUsed_4; }
	inline void set_m_UpdatableFlagsUsed_4(bool value)
	{
		___m_UpdatableFlagsUsed_4 = value;
	}

	inline static int32_t get_offset_of_m_Port_5() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Port_5)); }
	inline int32_t get_m_Port_5() const { return ___m_Port_5; }
	inline int32_t* get_address_of_m_Port_5() { return &___m_Port_5; }
	inline void set_m_Port_5(int32_t value)
	{
		___m_Port_5 = value;
	}

	inline static int32_t get_offset_of_m_Scheme_6() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Scheme_6)); }
	inline String_t* get_m_Scheme_6() const { return ___m_Scheme_6; }
	inline String_t** get_address_of_m_Scheme_6() { return &___m_Scheme_6; }
	inline void set_m_Scheme_6(String_t* value)
	{
		___m_Scheme_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Scheme_6, value);
	}
};

struct UriParser_t1012511323_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_Table
	Dictionary_2_t2927290585 * ___m_Table_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_TempTable
	Dictionary_2_t2927290585 * ___m_TempTable_1;
	// System.UriParser System.UriParser::HttpUri
	UriParser_t1012511323 * ___HttpUri_7;
	// System.UriParser System.UriParser::HttpsUri
	UriParser_t1012511323 * ___HttpsUri_8;
	// System.UriParser System.UriParser::WsUri
	UriParser_t1012511323 * ___WsUri_9;
	// System.UriParser System.UriParser::WssUri
	UriParser_t1012511323 * ___WssUri_10;
	// System.UriParser System.UriParser::FtpUri
	UriParser_t1012511323 * ___FtpUri_11;
	// System.UriParser System.UriParser::FileUri
	UriParser_t1012511323 * ___FileUri_12;
	// System.UriParser System.UriParser::GopherUri
	UriParser_t1012511323 * ___GopherUri_13;
	// System.UriParser System.UriParser::NntpUri
	UriParser_t1012511323 * ___NntpUri_14;
	// System.UriParser System.UriParser::NewsUri
	UriParser_t1012511323 * ___NewsUri_15;
	// System.UriParser System.UriParser::MailToUri
	UriParser_t1012511323 * ___MailToUri_16;
	// System.UriParser System.UriParser::UuidUri
	UriParser_t1012511323 * ___UuidUri_17;
	// System.UriParser System.UriParser::TelnetUri
	UriParser_t1012511323 * ___TelnetUri_18;
	// System.UriParser System.UriParser::LdapUri
	UriParser_t1012511323 * ___LdapUri_19;
	// System.UriParser System.UriParser::NetTcpUri
	UriParser_t1012511323 * ___NetTcpUri_20;
	// System.UriParser System.UriParser::NetPipeUri
	UriParser_t1012511323 * ___NetPipeUri_21;
	// System.UriParser System.UriParser::VsMacrosUri
	UriParser_t1012511323 * ___VsMacrosUri_22;
	// System.UriParser/UriQuirksVersion System.UriParser::s_QuirksVersion
	int32_t ___s_QuirksVersion_23;
	// System.UriSyntaxFlags System.UriParser::HttpSyntaxFlags
	int32_t ___HttpSyntaxFlags_24;
	// System.UriSyntaxFlags System.UriParser::FileSyntaxFlags
	int32_t ___FileSyntaxFlags_25;

public:
	inline static int32_t get_offset_of_m_Table_0() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___m_Table_0)); }
	inline Dictionary_2_t2927290585 * get_m_Table_0() const { return ___m_Table_0; }
	inline Dictionary_2_t2927290585 ** get_address_of_m_Table_0() { return &___m_Table_0; }
	inline void set_m_Table_0(Dictionary_2_t2927290585 * value)
	{
		___m_Table_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Table_0, value);
	}

	inline static int32_t get_offset_of_m_TempTable_1() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___m_TempTable_1)); }
	inline Dictionary_2_t2927290585 * get_m_TempTable_1() const { return ___m_TempTable_1; }
	inline Dictionary_2_t2927290585 ** get_address_of_m_TempTable_1() { return &___m_TempTable_1; }
	inline void set_m_TempTable_1(Dictionary_2_t2927290585 * value)
	{
		___m_TempTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_TempTable_1, value);
	}

	inline static int32_t get_offset_of_HttpUri_7() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpUri_7)); }
	inline UriParser_t1012511323 * get_HttpUri_7() const { return ___HttpUri_7; }
	inline UriParser_t1012511323 ** get_address_of_HttpUri_7() { return &___HttpUri_7; }
	inline void set_HttpUri_7(UriParser_t1012511323 * value)
	{
		___HttpUri_7 = value;
		Il2CppCodeGenWriteBarrier(&___HttpUri_7, value);
	}

	inline static int32_t get_offset_of_HttpsUri_8() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpsUri_8)); }
	inline UriParser_t1012511323 * get_HttpsUri_8() const { return ___HttpsUri_8; }
	inline UriParser_t1012511323 ** get_address_of_HttpsUri_8() { return &___HttpsUri_8; }
	inline void set_HttpsUri_8(UriParser_t1012511323 * value)
	{
		___HttpsUri_8 = value;
		Il2CppCodeGenWriteBarrier(&___HttpsUri_8, value);
	}

	inline static int32_t get_offset_of_WsUri_9() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___WsUri_9)); }
	inline UriParser_t1012511323 * get_WsUri_9() const { return ___WsUri_9; }
	inline UriParser_t1012511323 ** get_address_of_WsUri_9() { return &___WsUri_9; }
	inline void set_WsUri_9(UriParser_t1012511323 * value)
	{
		___WsUri_9 = value;
		Il2CppCodeGenWriteBarrier(&___WsUri_9, value);
	}

	inline static int32_t get_offset_of_WssUri_10() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___WssUri_10)); }
	inline UriParser_t1012511323 * get_WssUri_10() const { return ___WssUri_10; }
	inline UriParser_t1012511323 ** get_address_of_WssUri_10() { return &___WssUri_10; }
	inline void set_WssUri_10(UriParser_t1012511323 * value)
	{
		___WssUri_10 = value;
		Il2CppCodeGenWriteBarrier(&___WssUri_10, value);
	}

	inline static int32_t get_offset_of_FtpUri_11() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FtpUri_11)); }
	inline UriParser_t1012511323 * get_FtpUri_11() const { return ___FtpUri_11; }
	inline UriParser_t1012511323 ** get_address_of_FtpUri_11() { return &___FtpUri_11; }
	inline void set_FtpUri_11(UriParser_t1012511323 * value)
	{
		___FtpUri_11 = value;
		Il2CppCodeGenWriteBarrier(&___FtpUri_11, value);
	}

	inline static int32_t get_offset_of_FileUri_12() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FileUri_12)); }
	inline UriParser_t1012511323 * get_FileUri_12() const { return ___FileUri_12; }
	inline UriParser_t1012511323 ** get_address_of_FileUri_12() { return &___FileUri_12; }
	inline void set_FileUri_12(UriParser_t1012511323 * value)
	{
		___FileUri_12 = value;
		Il2CppCodeGenWriteBarrier(&___FileUri_12, value);
	}

	inline static int32_t get_offset_of_GopherUri_13() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___GopherUri_13)); }
	inline UriParser_t1012511323 * get_GopherUri_13() const { return ___GopherUri_13; }
	inline UriParser_t1012511323 ** get_address_of_GopherUri_13() { return &___GopherUri_13; }
	inline void set_GopherUri_13(UriParser_t1012511323 * value)
	{
		___GopherUri_13 = value;
		Il2CppCodeGenWriteBarrier(&___GopherUri_13, value);
	}

	inline static int32_t get_offset_of_NntpUri_14() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NntpUri_14)); }
	inline UriParser_t1012511323 * get_NntpUri_14() const { return ___NntpUri_14; }
	inline UriParser_t1012511323 ** get_address_of_NntpUri_14() { return &___NntpUri_14; }
	inline void set_NntpUri_14(UriParser_t1012511323 * value)
	{
		___NntpUri_14 = value;
		Il2CppCodeGenWriteBarrier(&___NntpUri_14, value);
	}

	inline static int32_t get_offset_of_NewsUri_15() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NewsUri_15)); }
	inline UriParser_t1012511323 * get_NewsUri_15() const { return ___NewsUri_15; }
	inline UriParser_t1012511323 ** get_address_of_NewsUri_15() { return &___NewsUri_15; }
	inline void set_NewsUri_15(UriParser_t1012511323 * value)
	{
		___NewsUri_15 = value;
		Il2CppCodeGenWriteBarrier(&___NewsUri_15, value);
	}

	inline static int32_t get_offset_of_MailToUri_16() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___MailToUri_16)); }
	inline UriParser_t1012511323 * get_MailToUri_16() const { return ___MailToUri_16; }
	inline UriParser_t1012511323 ** get_address_of_MailToUri_16() { return &___MailToUri_16; }
	inline void set_MailToUri_16(UriParser_t1012511323 * value)
	{
		___MailToUri_16 = value;
		Il2CppCodeGenWriteBarrier(&___MailToUri_16, value);
	}

	inline static int32_t get_offset_of_UuidUri_17() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___UuidUri_17)); }
	inline UriParser_t1012511323 * get_UuidUri_17() const { return ___UuidUri_17; }
	inline UriParser_t1012511323 ** get_address_of_UuidUri_17() { return &___UuidUri_17; }
	inline void set_UuidUri_17(UriParser_t1012511323 * value)
	{
		___UuidUri_17 = value;
		Il2CppCodeGenWriteBarrier(&___UuidUri_17, value);
	}

	inline static int32_t get_offset_of_TelnetUri_18() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___TelnetUri_18)); }
	inline UriParser_t1012511323 * get_TelnetUri_18() const { return ___TelnetUri_18; }
	inline UriParser_t1012511323 ** get_address_of_TelnetUri_18() { return &___TelnetUri_18; }
	inline void set_TelnetUri_18(UriParser_t1012511323 * value)
	{
		___TelnetUri_18 = value;
		Il2CppCodeGenWriteBarrier(&___TelnetUri_18, value);
	}

	inline static int32_t get_offset_of_LdapUri_19() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___LdapUri_19)); }
	inline UriParser_t1012511323 * get_LdapUri_19() const { return ___LdapUri_19; }
	inline UriParser_t1012511323 ** get_address_of_LdapUri_19() { return &___LdapUri_19; }
	inline void set_LdapUri_19(UriParser_t1012511323 * value)
	{
		___LdapUri_19 = value;
		Il2CppCodeGenWriteBarrier(&___LdapUri_19, value);
	}

	inline static int32_t get_offset_of_NetTcpUri_20() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NetTcpUri_20)); }
	inline UriParser_t1012511323 * get_NetTcpUri_20() const { return ___NetTcpUri_20; }
	inline UriParser_t1012511323 ** get_address_of_NetTcpUri_20() { return &___NetTcpUri_20; }
	inline void set_NetTcpUri_20(UriParser_t1012511323 * value)
	{
		___NetTcpUri_20 = value;
		Il2CppCodeGenWriteBarrier(&___NetTcpUri_20, value);
	}

	inline static int32_t get_offset_of_NetPipeUri_21() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NetPipeUri_21)); }
	inline UriParser_t1012511323 * get_NetPipeUri_21() const { return ___NetPipeUri_21; }
	inline UriParser_t1012511323 ** get_address_of_NetPipeUri_21() { return &___NetPipeUri_21; }
	inline void set_NetPipeUri_21(UriParser_t1012511323 * value)
	{
		___NetPipeUri_21 = value;
		Il2CppCodeGenWriteBarrier(&___NetPipeUri_21, value);
	}

	inline static int32_t get_offset_of_VsMacrosUri_22() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___VsMacrosUri_22)); }
	inline UriParser_t1012511323 * get_VsMacrosUri_22() const { return ___VsMacrosUri_22; }
	inline UriParser_t1012511323 ** get_address_of_VsMacrosUri_22() { return &___VsMacrosUri_22; }
	inline void set_VsMacrosUri_22(UriParser_t1012511323 * value)
	{
		___VsMacrosUri_22 = value;
		Il2CppCodeGenWriteBarrier(&___VsMacrosUri_22, value);
	}

	inline static int32_t get_offset_of_s_QuirksVersion_23() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___s_QuirksVersion_23)); }
	inline int32_t get_s_QuirksVersion_23() const { return ___s_QuirksVersion_23; }
	inline int32_t* get_address_of_s_QuirksVersion_23() { return &___s_QuirksVersion_23; }
	inline void set_s_QuirksVersion_23(int32_t value)
	{
		___s_QuirksVersion_23 = value;
	}

	inline static int32_t get_offset_of_HttpSyntaxFlags_24() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpSyntaxFlags_24)); }
	inline int32_t get_HttpSyntaxFlags_24() const { return ___HttpSyntaxFlags_24; }
	inline int32_t* get_address_of_HttpSyntaxFlags_24() { return &___HttpSyntaxFlags_24; }
	inline void set_HttpSyntaxFlags_24(int32_t value)
	{
		___HttpSyntaxFlags_24 = value;
	}

	inline static int32_t get_offset_of_FileSyntaxFlags_25() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FileSyntaxFlags_25)); }
	inline int32_t get_FileSyntaxFlags_25() const { return ___FileSyntaxFlags_25; }
	inline int32_t* get_address_of_FileSyntaxFlags_25() { return &___FileSyntaxFlags_25; }
	inline void set_FileSyntaxFlags_25(int32_t value)
	{
		___FileSyntaxFlags_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
