﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_LineInfo1429635508.h"

// System.Xml.XmlTextReaderImpl/NodeData
struct NodeData_t2613273532;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/NodeData
struct  NodeData_t2613273532  : public Il2CppObject
{
public:
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl/NodeData::type
	int32_t ___type_1;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::prefix
	String_t* ___prefix_3;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::ns
	String_t* ___ns_4;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::nameWPrefix
	String_t* ___nameWPrefix_5;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::value
	String_t* ___value_6;
	// System.Char[] System.Xml.XmlTextReaderImpl/NodeData::chars
	CharU5BU5D_t1328083999* ___chars_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueStartPos
	int32_t ___valueStartPos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueLength
	int32_t ___valueLength_9;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo
	LineInfo_t1429635508  ___lineInfo_10;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo2
	LineInfo_t1429635508  ___lineInfo2_11;
	// System.Char System.Xml.XmlTextReaderImpl/NodeData::quoteChar
	Il2CppChar ___quoteChar_12;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::depth
	int32_t ___depth_13;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::isEmptyOrDefault
	bool ___isEmptyOrDefault_14;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::entityId
	int32_t ___entityId_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::xmlContextPushed
	bool ___xmlContextPushed_16;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl/NodeData::nextAttrValueChunk
	NodeData_t2613273532 * ___nextAttrValueChunk_17;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::schemaType
	Il2CppObject * ___schemaType_18;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::typedValue
	Il2CppObject * ___typedValue_19;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier(&___localName_2, value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_3, value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___ns_4)); }
	inline String_t* get_ns_4() const { return ___ns_4; }
	inline String_t** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(String_t* value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ns_4, value);
	}

	inline static int32_t get_offset_of_nameWPrefix_5() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___nameWPrefix_5)); }
	inline String_t* get_nameWPrefix_5() const { return ___nameWPrefix_5; }
	inline String_t** get_address_of_nameWPrefix_5() { return &___nameWPrefix_5; }
	inline void set_nameWPrefix_5(String_t* value)
	{
		___nameWPrefix_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameWPrefix_5, value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier(&___value_6, value);
	}

	inline static int32_t get_offset_of_chars_7() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___chars_7)); }
	inline CharU5BU5D_t1328083999* get_chars_7() const { return ___chars_7; }
	inline CharU5BU5D_t1328083999** get_address_of_chars_7() { return &___chars_7; }
	inline void set_chars_7(CharU5BU5D_t1328083999* value)
	{
		___chars_7 = value;
		Il2CppCodeGenWriteBarrier(&___chars_7, value);
	}

	inline static int32_t get_offset_of_valueStartPos_8() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___valueStartPos_8)); }
	inline int32_t get_valueStartPos_8() const { return ___valueStartPos_8; }
	inline int32_t* get_address_of_valueStartPos_8() { return &___valueStartPos_8; }
	inline void set_valueStartPos_8(int32_t value)
	{
		___valueStartPos_8 = value;
	}

	inline static int32_t get_offset_of_valueLength_9() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___valueLength_9)); }
	inline int32_t get_valueLength_9() const { return ___valueLength_9; }
	inline int32_t* get_address_of_valueLength_9() { return &___valueLength_9; }
	inline void set_valueLength_9(int32_t value)
	{
		___valueLength_9 = value;
	}

	inline static int32_t get_offset_of_lineInfo_10() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___lineInfo_10)); }
	inline LineInfo_t1429635508  get_lineInfo_10() const { return ___lineInfo_10; }
	inline LineInfo_t1429635508 * get_address_of_lineInfo_10() { return &___lineInfo_10; }
	inline void set_lineInfo_10(LineInfo_t1429635508  value)
	{
		___lineInfo_10 = value;
	}

	inline static int32_t get_offset_of_lineInfo2_11() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___lineInfo2_11)); }
	inline LineInfo_t1429635508  get_lineInfo2_11() const { return ___lineInfo2_11; }
	inline LineInfo_t1429635508 * get_address_of_lineInfo2_11() { return &___lineInfo2_11; }
	inline void set_lineInfo2_11(LineInfo_t1429635508  value)
	{
		___lineInfo2_11 = value;
	}

	inline static int32_t get_offset_of_quoteChar_12() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___quoteChar_12)); }
	inline Il2CppChar get_quoteChar_12() const { return ___quoteChar_12; }
	inline Il2CppChar* get_address_of_quoteChar_12() { return &___quoteChar_12; }
	inline void set_quoteChar_12(Il2CppChar value)
	{
		___quoteChar_12 = value;
	}

	inline static int32_t get_offset_of_depth_13() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___depth_13)); }
	inline int32_t get_depth_13() const { return ___depth_13; }
	inline int32_t* get_address_of_depth_13() { return &___depth_13; }
	inline void set_depth_13(int32_t value)
	{
		___depth_13 = value;
	}

	inline static int32_t get_offset_of_isEmptyOrDefault_14() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___isEmptyOrDefault_14)); }
	inline bool get_isEmptyOrDefault_14() const { return ___isEmptyOrDefault_14; }
	inline bool* get_address_of_isEmptyOrDefault_14() { return &___isEmptyOrDefault_14; }
	inline void set_isEmptyOrDefault_14(bool value)
	{
		___isEmptyOrDefault_14 = value;
	}

	inline static int32_t get_offset_of_entityId_15() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___entityId_15)); }
	inline int32_t get_entityId_15() const { return ___entityId_15; }
	inline int32_t* get_address_of_entityId_15() { return &___entityId_15; }
	inline void set_entityId_15(int32_t value)
	{
		___entityId_15 = value;
	}

	inline static int32_t get_offset_of_xmlContextPushed_16() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___xmlContextPushed_16)); }
	inline bool get_xmlContextPushed_16() const { return ___xmlContextPushed_16; }
	inline bool* get_address_of_xmlContextPushed_16() { return &___xmlContextPushed_16; }
	inline void set_xmlContextPushed_16(bool value)
	{
		___xmlContextPushed_16 = value;
	}

	inline static int32_t get_offset_of_nextAttrValueChunk_17() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___nextAttrValueChunk_17)); }
	inline NodeData_t2613273532 * get_nextAttrValueChunk_17() const { return ___nextAttrValueChunk_17; }
	inline NodeData_t2613273532 ** get_address_of_nextAttrValueChunk_17() { return &___nextAttrValueChunk_17; }
	inline void set_nextAttrValueChunk_17(NodeData_t2613273532 * value)
	{
		___nextAttrValueChunk_17 = value;
		Il2CppCodeGenWriteBarrier(&___nextAttrValueChunk_17, value);
	}

	inline static int32_t get_offset_of_schemaType_18() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___schemaType_18)); }
	inline Il2CppObject * get_schemaType_18() const { return ___schemaType_18; }
	inline Il2CppObject ** get_address_of_schemaType_18() { return &___schemaType_18; }
	inline void set_schemaType_18(Il2CppObject * value)
	{
		___schemaType_18 = value;
		Il2CppCodeGenWriteBarrier(&___schemaType_18, value);
	}

	inline static int32_t get_offset_of_typedValue_19() { return static_cast<int32_t>(offsetof(NodeData_t2613273532, ___typedValue_19)); }
	inline Il2CppObject * get_typedValue_19() const { return ___typedValue_19; }
	inline Il2CppObject ** get_address_of_typedValue_19() { return &___typedValue_19; }
	inline void set_typedValue_19(Il2CppObject * value)
	{
		___typedValue_19 = value;
		Il2CppCodeGenWriteBarrier(&___typedValue_19, value);
	}
};

struct NodeData_t2613273532_StaticFields
{
public:
	// System.Xml.XmlTextReaderImpl/NodeData modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlTextReaderImpl/NodeData::s_None
	NodeData_t2613273532 * ___s_None_0;

public:
	inline static int32_t get_offset_of_s_None_0() { return static_cast<int32_t>(offsetof(NodeData_t2613273532_StaticFields, ___s_None_0)); }
	inline NodeData_t2613273532 * get_s_None_0() const { return ___s_None_0; }
	inline NodeData_t2613273532 ** get_address_of_s_None_0() { return &___s_None_0; }
	inline void set_s_None_0(NodeData_t2613273532 * value)
	{
		___s_None_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_None_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
