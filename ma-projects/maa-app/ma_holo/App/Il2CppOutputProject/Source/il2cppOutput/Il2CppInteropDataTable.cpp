﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_WindowsRun3312105128.h"
#include "mscorlib_System_Runtime_InteropServices_IErrorInfo3966376335.h"





extern const Il2CppType IRestrictedErrorInfo_t3312105128_0_0_0;
extern const Il2CppType IErrorInfo_t3966376335_0_0_0;
extern "C" void Context_t2636657155_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Context_t2636657155_0_0_0;
extern "C" void Escape_t169451053_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Escape_t169451053_0_0_0;
extern "C" void PreviousInfo_t581002487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PreviousInfo_t581002487_0_0_0;
extern "C" void RuntimeClassHandle_t2775506138_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeClassHandle_t2775506138_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeClassHandle_t2775506138_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RuntimeClassHandle_t2775506138_0_0_0;
extern "C" void RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RuntimeGenericParamInfoHandle_t580017048_0_0_0;
extern "C" void RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RuntimeGPtrArrayHandle_t1303258952_0_0_0;
extern "C" void RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RuntimeRemoteClassHandle_t2923639406_0_0_0;
extern "C" void GenericParamInfo_t1377222196_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GenericParamInfo_t1377222196_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GenericParamInfo_t1377222196_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GenericParamInfo_t1377222196_0_0_0;
extern "C" void GPtrArray_t3128553612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GPtrArray_t3128553612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GPtrArray_t3128553612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GPtrArray_t3128553612_0_0_0;
extern "C" void RemoteClass_t3863182804_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RemoteClass_t3863182804_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RemoteClass_t3863182804_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RemoteClass_t3863182804_0_0_0;
extern "C" void SafeGPtrArrayHandle_t547714345_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SafeGPtrArrayHandle_t547714345_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SafeGPtrArrayHandle_t547714345_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SafeGPtrArrayHandle_t547714345_0_0_0;
extern "C" void SafeStringMarshal_t2486501886_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SafeStringMarshal_t2486501886_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SafeStringMarshal_t2486501886_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SafeStringMarshal_t2486501886_0_0_0;
extern "C" void UriScheme_t683497865_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t683497865_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t683497865_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriScheme_t683497865_0_0_0;
extern "C" void __DTString_t420639831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void __DTString_t420639831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void __DTString_t420639831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType __DTString_t420639831_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3226471752();
extern const Il2CppType Action_t3226471752_0_0_0;
extern "C" void AppDomain_t2719102437_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AppDomain_t2719102437_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AppDomain_t2719102437_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AppDomain_t2719102437_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613();
extern const Il2CppType AppDomainInitializer_t3898244613_0_0_0;
extern "C" void AppDomainSetup_t611332832_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AppDomainSetup_t611332832_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AppDomainSetup_t611332832_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AppDomainSetup_t611332832_0_0_0;
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
extern "C" void bucket_t976591655_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void bucket_t976591655_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void bucket_t976591655_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType bucket_t976591655_0_0_0;
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t2895223116();
extern const Il2CppType InternalCancelHandler_t2895223116_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowsCancelHandler_t1197082027();
extern const Il2CppType WindowsCancelHandler_t1197082027_0_0_0;
extern "C" void ConsoleKeyInfo_t3124575640_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConsoleKeyInfo_t3124575640_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConsoleKeyInfo_t3124575640_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ConsoleKeyInfo_t3124575640_0_0_0;
extern "C" void DateTimeRawInfo_t1887088787_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DateTimeRawInfo_t1887088787_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DateTimeRawInfo_t1887088787_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DateTimeRawInfo_t1887088787_0_0_0;
extern "C" void DateTimeResult_t824298922_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DateTimeResult_t824298922_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DateTimeResult_t824298922_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DateTimeResult_t824298922_0_0_0;
extern "C" void Delegate_t3022476291_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Delegate_t3022476291_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Delegate_t3022476291_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Delegate_t3022476291_0_0_0;
extern "C" void StackFrame_t2050294881_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StackFrame_t2050294881_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StackFrame_t2050294881_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StackFrame_t2050294881_0_0_0;
extern "C" void DTSubString_t4037085109_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DTSubString_t4037085109_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DTSubString_t4037085109_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DTSubString_t4037085109_0_0_0;
extern "C" void Enum_t2459695545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Enum_t2459695545_0_0_0;
extern "C" void EnumResult_t2872047947_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EnumResult_t2872047947_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EnumResult_t2872047947_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EnumResult_t2872047947_0_0_0;
extern "C" void Exception_t1927440687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Exception_t1927440687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Exception_t1927440687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Exception_t1927440687_0_0_0;
extern "C" void CalendarData_t275297348_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CalendarData_t275297348_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CalendarData_t275297348_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CalendarData_t275297348_0_0_0;
extern "C" void UnicodeDataHeader_t2784996206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnicodeDataHeader_t2784996206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnicodeDataHeader_t2784996206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnicodeDataHeader_t2784996206_0_0_0;
extern "C" void CultureData_t3400086592_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CultureData_t3400086592_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CultureData_t3400086592_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CultureData_t3400086592_0_0_0;
extern "C" void CultureInfo_t3500843524_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CultureInfo_t3500843524_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CultureInfo_t3500843524_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
extern "C" void Data_t1401534395_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Data_t1401534395_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Data_t1401534395_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Data_t1401534395_0_0_0;
extern "C" void InternalCodePageDataItem_t1742109366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InternalCodePageDataItem_t1742109366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InternalCodePageDataItem_t1742109366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InternalCodePageDataItem_t1742109366_0_0_0;
extern "C" void InternalEncodingDataItem_t82919681_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InternalEncodingDataItem_t82919681_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InternalEncodingDataItem_t82919681_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InternalEncodingDataItem_t82919681_0_0_0;
extern "C" void SortKey_t1270563137_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SortKey_t1270563137_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SortKey_t1270563137_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SortKey_t1270563137_0_0_0;
extern "C" void FormatLiterals_t2087080800_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatLiterals_t2087080800_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatLiterals_t2087080800_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FormatLiterals_t2087080800_0_0_0;
extern "C" void StringParser_t2680633667_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringParser_t2680633667_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringParser_t2680633667_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StringParser_t2680633667_0_0_0;
extern "C" void TimeSpanRawInfo_t1900124058_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TimeSpanRawInfo_t1900124058_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TimeSpanRawInfo_t1900124058_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TimeSpanRawInfo_t1900124058_0_0_0;
extern "C" void TimeSpanResult_t782244817_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TimeSpanResult_t782244817_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TimeSpanResult_t782244817_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TimeSpanResult_t782244817_0_0_0;
extern "C" void TimeSpanToken_t2031659367_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TimeSpanToken_t2031659367_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TimeSpanToken_t2031659367_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TimeSpanToken_t2031659367_0_0_0;
extern "C" void TimeSpanTokenizer_t1845673523_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TimeSpanTokenizer_t1845673523_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TimeSpanTokenizer_t1845673523_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TimeSpanTokenizer_t1845673523_0_0_0;
extern "C" void GuidResult_t2567604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GuidResult_t2567604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GuidResult_t2567604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GuidResult_t2567604379_0_0_0;
extern "C" void InputRecord_t3971050487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputRecord_t3971050487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputRecord_t3971050487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InputRecord_t3971050487_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381();
extern const Il2CppType ReadDelegate_t3184826381_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132();
extern const Il2CppType WriteDelegate_t489908132_0_0_0;
extern "C" void MarshalByRefObject_t1285298191_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MarshalByRefObject_t1285298191_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MarshalByRefObject_t1285298191_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
extern "C" void MonoAsyncCall_t3796687503_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoAsyncCall_t3796687503_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoAsyncCall_t3796687503_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoAsyncCall_t3796687503_0_0_0;
extern "C" void MonoTypeInfo_t1976057079_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoTypeInfo_t1976057079_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoTypeInfo_t1976057079_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoTypeInfo_t1976057079_0_0_0;
extern "C" void MulticastDelegate_t3201952435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MulticastDelegate_t3201952435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MulticastDelegate_t3201952435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MulticastDelegate_t3201952435_0_0_0;
extern "C" void NumberBuffer_t3547128574_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NumberBuffer_t3547128574_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NumberBuffer_t3547128574_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NumberBuffer_t3547128574_0_0_0;
extern "C" void FormatParam_t2947516451_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatParam_t2947516451_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatParam_t2947516451_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FormatParam_t2947516451_0_0_0;
extern "C" void ParamsArray_t2726825425_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParamsArray_t2726825425_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParamsArray_t2726825425_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParamsArray_t2726825425_0_0_0;
extern "C" void ParsingInfo_t442145320_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParsingInfo_t442145320_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParsingInfo_t442145320_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParsingInfo_t442145320_0_0_0;
extern "C" void Assembly_t4268412390_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Assembly_t4268412390_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Assembly_t4268412390_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Assembly_t4268412390_0_0_0;
extern "C" void AssemblyName_t894705941_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssemblyName_t894705941_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssemblyName_t894705941_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssemblyName_t894705941_0_0_0;
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
extern "C" void AssemblyBuilder_t1646117627_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssemblyBuilder_t1646117627_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssemblyBuilder_t1646117627_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssemblyBuilder_t1646117627_0_0_0;
extern "C" void CustomAttributeBuilder_t2970303184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeBuilder_t2970303184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeBuilder_t2970303184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeBuilder_t2970303184_0_0_0;
extern "C" void EventBuilder_t2927243889_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EventBuilder_t2927243889_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EventBuilder_t2927243889_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EventBuilder_t2927243889_0_0_0;
extern "C" void ILExceptionBlock_t2042475189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILExceptionBlock_t2042475189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILExceptionBlock_t2042475189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILExceptionBlock_t2042475189_0_0_0;
extern "C" void ILExceptionInfo_t1490154598_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILExceptionInfo_t1490154598_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILExceptionInfo_t1490154598_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILExceptionInfo_t1490154598_0_0_0;
extern "C" void ILGenerator_t99948092_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILGenerator_t99948092_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILGenerator_t99948092_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILGenerator_t99948092_0_0_0;
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
extern "C" void LocalBuilder_t2116499186_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LocalBuilder_t2116499186_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LocalBuilder_t2116499186_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
extern "C" void ModuleBuilder_t4156028127_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ModuleBuilder_t4156028127_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ModuleBuilder_t4156028127_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
extern "C" void MonoResource_t3127387157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t3127387157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t3127387157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoResource_t3127387157_0_0_0;
extern "C" void MonoWin32Resource_t2467306218_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t2467306218_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t2467306218_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoWin32Resource_t2467306218_0_0_0;
extern "C" void ParameterBuilder_t3344728474_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterBuilder_t3344728474_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterBuilder_t3344728474_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
extern "C" void RefEmitPermissionSet_t2708608433_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t2708608433_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t2708608433_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RefEmitPermissionSet_t2708608433_0_0_0;
extern "C" void UnmanagedMarshal_t4270021860_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnmanagedMarshal_t4270021860_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnmanagedMarshal_t4270021860_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnmanagedMarshal_t4270021860_0_0_0;
extern "C" void LocalVariableInfo_t1749284021_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LocalVariableInfo_t1749284021_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LocalVariableInfo_t1749284021_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
extern "C" void Module_t4282841206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Module_t4282841206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Module_t4282841206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Module_t4282841206_0_0_0;
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEventInfo_t2190036573_0_0_0;
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoMethodInfo_t3646562144_0_0_0;
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoPropertyInfo_t486106184_0_0_0;
extern "C" void ParameterInfo_t2249040075_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterInfo_t2249040075_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterInfo_t2249040075_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
extern "C" void ResourceLocator_t2156390884_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceLocator_t2156390884_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceLocator_t2156390884_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceLocator_t2156390884_0_0_0;
extern "C" void AsyncMethodBuilderCore_t2485284745_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncMethodBuilderCore_t2485284745_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncMethodBuilderCore_t2485284745_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncMethodBuilderCore_t2485284745_0_0_0;
extern "C" void Ephemeron_t1875076633_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Ephemeron_t1875076633_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Ephemeron_t1875076633_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Ephemeron_t1875076633_0_0_0;
extern "C" void TaskAwaiter_t3341738712_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TaskAwaiter_t3341738712_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TaskAwaiter_t3341738712_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TaskAwaiter_t3341738712_0_0_0;
extern "C" Il2CppIUnknown* CreateComCallableWrapperFor_ManagedErrorInfo_t914761495(Il2CppObject* obj);
extern const Il2CppType ManagedErrorInfo_t914761495_0_0_0;
extern "C" void Context_t502196753_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t502196753_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t502196753_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Context_t502196753_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990();
extern const Il2CppType CrossContextDelegate_t754146990_0_0_0;
extern "C" void AsyncResult_t2232356043_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncResult_t2232356043_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncResult_t2232356043_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncResult_t2232356043_0_0_0;
extern "C" void Reader_t1568751674_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Reader_t1568751674_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Reader_t1568751674_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Reader_t1568751674_0_0_0;
extern "C" void MonoMethodMessage_t771543475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodMessage_t771543475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodMessage_t771543475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoMethodMessage_t771543475_0_0_0;
extern "C" void RealProxy_t298428346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RealProxy_t298428346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RealProxy_t298428346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RealProxy_t298428346_0_0_0;
extern "C" void TransparentProxy_t3836393972_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransparentProxy_t3836393972_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransparentProxy_t3836393972_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TransparentProxy_t3836393972_0_0_0;
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SerializationEntry_t3485203212_0_0_0;
extern "C" void DelegatePInvokeWrapper_SerializationEventHandler_t2339848500();
extern const Il2CppType SerializationEventHandler_t2339848500_0_0_0;
extern "C" void StreamingContext_t1417235061_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StreamingContext_t1417235061_0_0_0;
extern "C" void DSAParameters_t1872138834_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DSAParameters_t1872138834_0_0_0;
extern "C" void HashAlgorithmName_t1682985260_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HashAlgorithmName_t1682985260_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HashAlgorithmName_t1682985260_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HashAlgorithmName_t1682985260_0_0_0;
extern "C" void RSAParameters_t1462703416_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RSAParameters_t1462703416_0_0_0;
extern "C" void SNIP_t4214504621_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SNIP_t4214504621_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SNIP_t4214504621_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SNIP_t4214504621_0_0_0;
extern "C" void CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CancellationCallbackCoreWorkArguments_t3564269970_0_0_0;
extern "C" void CancellationToken_t1851405782_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationToken_t1851405782_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationToken_t1851405782_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CancellationToken_t1851405782_0_0_0;
extern "C" void CancellationTokenRegistration_t1708859357_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationTokenRegistration_t1708859357_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationTokenRegistration_t1708859357_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CancellationTokenRegistration_t1708859357_0_0_0;
extern "C" void Reader_t611157692_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Reader_t611157692_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Reader_t611157692_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Reader_t611157692_0_0_0;
extern "C" void ExecutionContextSwitcher_t3304742894_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ExecutionContextSwitcher_t3304742894_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ExecutionContextSwitcher_t3304742894_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ExecutionContextSwitcher_t3304742894_0_0_0;
extern "C" void SpinLock_t2136334209_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpinLock_t2136334209_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpinLock_t2136334209_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SpinLock_t2136334209_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264();
extern const Il2CppType ThreadStart_t3437517264_0_0_0;
extern "C" void WaitHandle_t677569169_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitHandle_t677569169_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitHandle_t677569169_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitHandle_t677569169_0_0_0;
extern "C" void TransitionTime_t3441274853_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransitionTime_t3441274853_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransitionTime_t3441274853_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TransitionTime_t3441274853_0_0_0;
extern "C" void TZifType_t1855764066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TZifType_t1855764066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TZifType_t1855764066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TZifType_t1855764066_0_0_0;
extern "C" void UnSafeCharBuffer_t927766464_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnSafeCharBuffer_t927766464_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnSafeCharBuffer_t927766464_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnSafeCharBuffer_t927766464_0_0_0;
extern "C" void ValueType_t3507792607_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ValueType_t3507792607_0_0_0;
extern "C" void VersionResult_t785420859_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VersionResult_t785420859_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VersionResult_t785420859_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType VersionResult_t785420859_0_0_0;
extern "C" void DelegatePInvokeWrapper_CFProxyAutoConfigurationResultCallback_t3537096558();
extern const Il2CppType CFProxyAutoConfigurationResultCallback_t3537096558_0_0_0;
extern "C" void AttributeEntry_t168441916_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AttributeEntry_t168441916_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AttributeEntry_t168441916_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AttributeEntry_t168441916_0_0_0;
extern "C" void DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DefaultExtendedTypeDescriptor_t3612110989_0_0_0;
extern "C" void DefaultTypeDescriptor_t3334440910_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DefaultTypeDescriptor_t3334440910_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DefaultTypeDescriptor_t3334440910_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DefaultTypeDescriptor_t3334440910_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488();
extern const Il2CppType ReadMethod_t3362229488_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619();
extern const Il2CppType WriteMethod_t1894833619_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t4047001824();
extern const Il2CppType UnmanagedReadOrWrite_t4047001824_0_0_0;
extern "C" void IOAsyncResult_t1276329107_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IOAsyncResult_t1276329107_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IOAsyncResult_t1276329107_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IOAsyncResult_t1276329107_0_0_0;
extern "C" void IOSelectorJob_t2021937086_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IOSelectorJob_t2021937086_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IOSelectorJob_t2021937086_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IOSelectorJob_t2021937086_0_0_0;
extern "C" void RecognizedAttribute_t3930529114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RecognizedAttribute_t3930529114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RecognizedAttribute_t3930529114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RecognizedAttribute_t3930529114_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630();
extern const Il2CppType ReadDelegate_t1559754630_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799();
extern const Il2CppType WriteDelegate_t888270799_0_0_0;
extern "C" void DelegatePInvokeWrapper_HeaderParser_t3706119548();
extern const Il2CppType HeaderParser_t3706119548_0_0_0;
extern "C" void HeaderVariantInfo_t2058796272_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HeaderVariantInfo_t2058796272_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HeaderVariantInfo_t2058796272_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HeaderVariantInfo_t2058796272_0_0_0;
extern "C" void AuthorizationState_t3879143100_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AuthorizationState_t3879143100_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AuthorizationState_t3879143100_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AuthorizationState_t3879143100_0_0_0;
extern "C" void IPv6AddressFormatter_t3116172695_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IPv6AddressFormatter_t3116172695_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IPv6AddressFormatter_t3116172695_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IPv6AddressFormatter_t3116172695_0_0_0;
extern "C" void ifaddrs_t2532459533_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t2532459533_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t2532459533_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ifaddrs_t2532459533_0_0_0;
extern "C" void in6_addr_t4035827331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t4035827331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t4035827331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType in6_addr_t4035827331_0_0_0;
extern "C" void ifaddrs_t937751619_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t937751619_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t937751619_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ifaddrs_t937751619_0_0_0;
extern "C" void in6_addr_t1014645363_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t1014645363_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t1014645363_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType in6_addr_t1014645363_0_0_0;
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType sockaddr_dl_t3955242742_0_0_0;
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType sockaddr_in6_t834146887_0_0_0;
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType sockaddr_in6_t2899168071_0_0_0;
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType sockaddr_ll_t1681025498_0_0_0;
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_FIXED_INFO_t1371335919_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Win32_FIXED_INFO_t1371335919_0_0_0;
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0;
extern "C" void Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Win32_IP_ADDR_STRING_t2646152127_0_0_0;
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Win32_MIB_IFROW_t4215928996_0_0_0;
extern "C" void Win32_SOCKADDR_t1606057231_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_SOCKADDR_t1606057231_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_SOCKADDR_t1606057231_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Win32_SOCKADDR_t1606057231_0_0_0;
extern "C" void SocketAsyncResult_t3950677696_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t3950677696_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t3950677696_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SocketAsyncResult_t3950677696_0_0_0;
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
extern "C" void LowerCaseMapping_t2153935826_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LowerCaseMapping_t2153935826_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LowerCaseMapping_t2153935826_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LowerCaseMapping_t2153935826_0_0_0;
extern "C" void XPathNode_t3118381855_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XPathNode_t3118381855_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XPathNode_t3118381855_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType XPathNode_t3118381855_0_0_0;
extern "C" void XPathNodeRef_t2092605142_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XPathNodeRef_t2092605142_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XPathNodeRef_t2092605142_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType XPathNodeRef_t2092605142_0_0_0;
extern "C" void FacetsCompiler_t3565032206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FacetsCompiler_t3565032206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FacetsCompiler_t3565032206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FacetsCompiler_t3565032206_0_0_0;
extern "C" void Map_t2552390023_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Map_t2552390023_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Map_t2552390023_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Map_t2552390023_0_0_0;
extern "C" void Position_t1796812729_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Position_t1796812729_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Position_t1796812729_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Position_t1796812729_0_0_0;
extern "C" void RangePositionInfo_t2780802922_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RangePositionInfo_t2780802922_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RangePositionInfo_t2780802922_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RangePositionInfo_t2780802922_0_0_0;
extern "C" void SequenceConstructPosContext_t3853454650_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SequenceConstructPosContext_t3853454650_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SequenceConstructPosContext_t3853454650_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SequenceConstructPosContext_t3853454650_0_0_0;
extern "C" void Union_t69719246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Union_t69719246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Union_t69719246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Union_t69719246_0_0_0;
extern "C" void XmlSchemaObjectEntry_t2510586510_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XmlSchemaObjectEntry_t2510586510_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XmlSchemaObjectEntry_t2510586510_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType XmlSchemaObjectEntry_t2510586510_0_0_0;
extern "C" void XsdDateTime_t2434467484_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XsdDateTime_t2434467484_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XsdDateTime_t2434467484_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType XsdDateTime_t2434467484_0_0_0;
extern "C" void Parser_t2170101039_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Parser_t2170101039_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Parser_t2170101039_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Parser_t2170101039_0_0_0;
extern "C" void DelegatePInvokeWrapper_HashCodeOfStringDelegate_t2463303350();
extern const Il2CppType HashCodeOfStringDelegate_t2463303350_0_0_0;
extern "C" void XmlCharType_t1050521405_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XmlCharType_t1050521405_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XmlCharType_t1050521405_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType XmlCharType_t1050521405_0_0_0;
extern "C" void SmallXmlNodeList_t4252813691_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SmallXmlNodeList_t4252813691_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SmallXmlNodeList_t4252813691_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SmallXmlNodeList_t4252813691_0_0_0;
extern "C" void NamespaceDeclaration_t3577631811_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NamespaceDeclaration_t3577631811_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NamespaceDeclaration_t3577631811_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NamespaceDeclaration_t3577631811_0_0_0;
extern "C" void VirtualAttribute_t2854289803_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VirtualAttribute_t2854289803_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VirtualAttribute_t2854289803_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType VirtualAttribute_t2854289803_0_0_0;
extern "C" void DelegatePInvokeWrapper_HashCodeOfStringDelegate_t3565421161();
extern const Il2CppType HashCodeOfStringDelegate_t3565421161_0_0_0;
extern "C" void ParsingState_t1278724163_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParsingState_t1278724163_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParsingState_t1278724163_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParsingState_t1278724163_0_0_0;
extern "C" void Namespace_t1808381789_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Namespace_t1808381789_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Namespace_t1808381789_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Namespace_t1808381789_0_0_0;
extern "C" void TagInfo_t1244462138_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagInfo_t1244462138_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagInfo_t1244462138_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TagInfo_t1244462138_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnNavMeshPreUpdate_t2039022291();
extern const Il2CppType OnNavMeshPreUpdate_t2039022291_0_0_0;
extern "C" void CustomEventData_t1269126727_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t1269126727_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t1269126727_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomEventData_t1269126727_0_0_0;
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnityAnalyticsHandler_t3238795095_0_0_0;
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationCurve_t3306541151_0_0_0;
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationEvent_t2428323300_0_0_0;
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimatorTransitionInfo_t2410896200_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413();
extern const Il2CppType LogCallback_t1867914413_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t642977590();
extern const Il2CppType LowMemoryCallback_t642977590_0_0_0;
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssetBundleRequest_t2674559435_0_0_0;
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncOperation_t3814632279_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346();
extern const Il2CppType PCMReaderCallback_t3007145346_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554();
extern const Il2CppType PCMSetPositionCallback_t421863554_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033();
extern const Il2CppType AudioConfigurationChangeHandler_t3743753033_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132();
extern const Il2CppType WillRenderCanvases_t3522132132_0_0_0;
extern "C" void Collision_t2876846408_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision_t2876846408_0_0_0;
extern "C" void Collision2D_t1539500754_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t1539500754_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t1539500754_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision2D_t1539500754_0_0_0;
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ContactFilter2D_t1672660996_0_0_0;
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ControllerColliderHit_t4070855101_0_0_0;
extern "C" void Coroutine_t2299508840_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Coroutine_t2299508840_0_0_0;
extern "C" void CullingGroup_t1091689465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CullingGroup_t1091689465_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210();
extern const Il2CppType StateChanged_t2480912210_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815();
extern const Il2CppType DisplaysUpdatedDelegate_t3423469815_0_0_0;
extern "C" void Event_t3028476042_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Event_t3028476042_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511();
extern const Il2CppType UnityAction_t4025899511_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033();
extern const Il2CppType FontTextureRebuildCallback_t1272078033_0_0_0;
extern "C" void Gradient_t3600583008_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Gradient_t3600583008_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455();
extern const Il2CppType WindowFunction_t3486805455_0_0_0;
extern "C" void GUIContent_t4210063000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIContent_t4210063000_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336();
extern const Il2CppType SkinChangedDelegate_t3594822336_0_0_0;
extern "C" void GUIStyle_t1799908754_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyleState_t3801000545_0_0_0;
extern "C" void HumanBone_t1529896151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HumanBone_t1529896151_0_0_0;
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandler_t1216180266_0_0_0;
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandlerAudioClip_t1520641178_0_0_0;
extern "C" void Object_t1021602117_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Object_t1021602117_0_0_0;
extern "C" void RaycastHit_t87180320_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit_t87180320_0_0_0;
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
extern "C" void RectOffset_t3387826427_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RectOffset_t3387826427_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180();
extern const Il2CppType UpdatedEventHandler_t3033456180_0_0_0;
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceRequest_t2560315377_0_0_0;
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ScriptableObject_t1975622470_0_0_0;
extern "C" void HitInfo_t1761367055_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HitInfo_t1761367055_0_0_0;
extern "C" void SkeletonBone_t345082847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SkeletonBone_t345082847_0_0_0;
extern "C" void TrackedReference_t1045890189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TrackedReference_t1045890189_0_0_0;
extern "C" void DelegatePInvokeWrapper_GestureErrorDelegate_t2343902086();
extern const Il2CppType GestureErrorDelegate_t2343902086_0_0_0;
extern "C" void DelegatePInvokeWrapper_HoldCanceledEventDelegate_t1855824201();
extern const Il2CppType HoldCanceledEventDelegate_t1855824201_0_0_0;
extern "C" void DelegatePInvokeWrapper_HoldCompletedEventDelegate_t3501137495();
extern const Il2CppType HoldCompletedEventDelegate_t3501137495_0_0_0;
extern "C" void DelegatePInvokeWrapper_HoldStartedEventDelegate_t3415544547();
extern const Il2CppType HoldStartedEventDelegate_t3415544547_0_0_0;
extern "C" void DelegatePInvokeWrapper_ManipulationCanceledEventDelegate_t2553296249();
extern const Il2CppType ManipulationCanceledEventDelegate_t2553296249_0_0_0;
extern "C" void DelegatePInvokeWrapper_ManipulationCompletedEventDelegate_t2759974023();
extern const Il2CppType ManipulationCompletedEventDelegate_t2759974023_0_0_0;
extern "C" void DelegatePInvokeWrapper_ManipulationStartedEventDelegate_t2487153075();
extern const Il2CppType ManipulationStartedEventDelegate_t2487153075_0_0_0;
extern "C" void DelegatePInvokeWrapper_ManipulationUpdatedEventDelegate_t2394416487();
extern const Il2CppType ManipulationUpdatedEventDelegate_t2394416487_0_0_0;
extern "C" void DelegatePInvokeWrapper_NavigationCanceledEventDelegate_t1045620518();
extern const Il2CppType NavigationCanceledEventDelegate_t1045620518_0_0_0;
extern "C" void DelegatePInvokeWrapper_NavigationCompletedEventDelegate_t1982950364();
extern const Il2CppType NavigationCompletedEventDelegate_t1982950364_0_0_0;
extern "C" void DelegatePInvokeWrapper_NavigationStartedEventDelegate_t3123342528();
extern const Il2CppType NavigationStartedEventDelegate_t3123342528_0_0_0;
extern "C" void DelegatePInvokeWrapper_NavigationUpdatedEventDelegate_t3960847450();
extern const Il2CppType NavigationUpdatedEventDelegate_t3960847450_0_0_0;
extern "C" void DelegatePInvokeWrapper_RecognitionEndedEventDelegate_t831649826();
extern const Il2CppType RecognitionEndedEventDelegate_t831649826_0_0_0;
extern "C" void DelegatePInvokeWrapper_RecognitionStartedEventDelegate_t3116179703();
extern const Il2CppType RecognitionStartedEventDelegate_t3116179703_0_0_0;
extern "C" void DelegatePInvokeWrapper_TappedEventDelegate_t2437856093();
extern const Il2CppType TappedEventDelegate_t2437856093_0_0_0;
extern "C" void DelegatePInvokeWrapper_SerializationCompleteDelegate_t389490593();
extern const Il2CppType SerializationCompleteDelegate_t389490593_0_0_0;
extern "C" void DelegatePInvokeWrapper_SerializationDataAvailableDelegate_t2381700809();
extern const Il2CppType SerializationDataAvailableDelegate_t2381700809_0_0_0;
extern "C" void SurfaceData_t3147297845_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SurfaceData_t3147297845_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SurfaceData_t3147297845_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SurfaceData_t3147297845_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnCapturedToDiskCallback_t1784202510();
extern const Il2CppType OnCapturedToDiskCallback_t1784202510_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnPhotoModeStartedCallback_t522077320();
extern const Il2CppType OnPhotoModeStartedCallback_t522077320_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnPhotoModeStoppedCallback_t2742193416();
extern const Il2CppType OnPhotoModeStoppedCallback_t2742193416_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnStartedRecordingVideoCallback_t2612745932();
extern const Il2CppType OnStartedRecordingVideoCallback_t2612745932_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnStoppedRecordingVideoCallback_t1976525720();
extern const Il2CppType OnStoppedRecordingVideoCallback_t1976525720_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnVideoModeStartedCallback_t1811850718();
extern const Il2CppType OnVideoModeStartedCallback_t1811850718_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnVideoModeStoppedCallback_t1862037614();
extern const Il2CppType OnVideoModeStoppedCallback_t1862037614_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnPositionalLocatorStateChangedDelegate_t3864636813();
extern const Il2CppType OnPositionalLocatorStateChangedDelegate_t3864636813_0_0_0;
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitForSeconds_t3839502067_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictationCompletedDelegate_t3326551541();
extern const Il2CppType DictationCompletedDelegate_t3326551541_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictationErrorHandler_t3730830311();
extern const Il2CppType DictationErrorHandler_t3730830311_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictationHypothesisDelegate_t1495849926();
extern const Il2CppType DictationHypothesisDelegate_t1495849926_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictationResultDelegate_t1941514337();
extern const Il2CppType DictationResultDelegate_t1941514337_0_0_0;
extern "C" void DelegatePInvokeWrapper_ErrorDelegate_t3690655641();
extern const Il2CppType ErrorDelegate_t3690655641_0_0_0;
extern "C" void DelegatePInvokeWrapper_StatusDelegate_t2739717665();
extern const Il2CppType StatusDelegate_t2739717665_0_0_0;
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PhraseRecognizedEventArgs_t3185826360_0_0_0;
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SemanticMeaning_t2306793223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SemanticMeaning_t2306793223_0_0_0;
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType YieldInstruction_t3462875981_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[249] = 
{
	{ NULL, NULL, NULL, NULL, NULL, &IRestrictedErrorInfo_t3312105128::IID, &IRestrictedErrorInfo_t3312105128_0_0_0 } /* System.Runtime.InteropServices.WindowsRuntime.IRestrictedErrorInfo */,
	{ NULL, NULL, NULL, NULL, NULL, &IErrorInfo_t3966376335::IID, &IErrorInfo_t3966376335_0_0_0 } /* System.Runtime.InteropServices.IErrorInfo */,
	{ NULL, Context_t2636657155_marshal_pinvoke, Context_t2636657155_marshal_pinvoke_back, Context_t2636657155_marshal_pinvoke_cleanup, NULL, NULL, &Context_t2636657155_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t169451053_marshal_pinvoke, Escape_t169451053_marshal_pinvoke_back, Escape_t169451053_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t169451053_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t581002487_marshal_pinvoke, PreviousInfo_t581002487_marshal_pinvoke_back, PreviousInfo_t581002487_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t581002487_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ NULL, RuntimeClassHandle_t2775506138_marshal_pinvoke, RuntimeClassHandle_t2775506138_marshal_pinvoke_back, RuntimeClassHandle_t2775506138_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeClassHandle_t2775506138_0_0_0 } /* Mono.RuntimeClassHandle */,
	{ NULL, RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke, RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke_back, RuntimeGenericParamInfoHandle_t580017048_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeGenericParamInfoHandle_t580017048_0_0_0 } /* Mono.RuntimeGenericParamInfoHandle */,
	{ NULL, RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke, RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke_back, RuntimeGPtrArrayHandle_t1303258952_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeGPtrArrayHandle_t1303258952_0_0_0 } /* Mono.RuntimeGPtrArrayHandle */,
	{ NULL, RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke, RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke_back, RuntimeRemoteClassHandle_t2923639406_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeRemoteClassHandle_t2923639406_0_0_0 } /* Mono.RuntimeRemoteClassHandle */,
	{ NULL, GenericParamInfo_t1377222196_marshal_pinvoke, GenericParamInfo_t1377222196_marshal_pinvoke_back, GenericParamInfo_t1377222196_marshal_pinvoke_cleanup, NULL, NULL, &GenericParamInfo_t1377222196_0_0_0 } /* Mono.RuntimeStructs/GenericParamInfo */,
	{ NULL, GPtrArray_t3128553612_marshal_pinvoke, GPtrArray_t3128553612_marshal_pinvoke_back, GPtrArray_t3128553612_marshal_pinvoke_cleanup, NULL, NULL, &GPtrArray_t3128553612_0_0_0 } /* Mono.RuntimeStructs/GPtrArray */,
	{ NULL, RemoteClass_t3863182804_marshal_pinvoke, RemoteClass_t3863182804_marshal_pinvoke_back, RemoteClass_t3863182804_marshal_pinvoke_cleanup, NULL, NULL, &RemoteClass_t3863182804_0_0_0 } /* Mono.RuntimeStructs/RemoteClass */,
	{ NULL, SafeGPtrArrayHandle_t547714345_marshal_pinvoke, SafeGPtrArrayHandle_t547714345_marshal_pinvoke_back, SafeGPtrArrayHandle_t547714345_marshal_pinvoke_cleanup, NULL, NULL, &SafeGPtrArrayHandle_t547714345_0_0_0 } /* Mono.SafeGPtrArrayHandle */,
	{ NULL, SafeStringMarshal_t2486501886_marshal_pinvoke, SafeStringMarshal_t2486501886_marshal_pinvoke_back, SafeStringMarshal_t2486501886_marshal_pinvoke_cleanup, NULL, NULL, &SafeStringMarshal_t2486501886_0_0_0 } /* Mono.SafeStringMarshal */,
	{ NULL, UriScheme_t683497865_marshal_pinvoke, UriScheme_t683497865_marshal_pinvoke_back, UriScheme_t683497865_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t683497865_0_0_0 } /* Mono.Security.Uri/UriScheme */,
	{ NULL, __DTString_t420639831_marshal_pinvoke, __DTString_t420639831_marshal_pinvoke_back, __DTString_t420639831_marshal_pinvoke_cleanup, NULL, NULL, &__DTString_t420639831_0_0_0 } /* System.__DTString */,
	{ DelegatePInvokeWrapper_Action_t3226471752, NULL, NULL, NULL, NULL, NULL, &Action_t3226471752_0_0_0 } /* System.Action */,
	{ NULL, AppDomain_t2719102437_marshal_pinvoke, AppDomain_t2719102437_marshal_pinvoke_back, AppDomain_t2719102437_marshal_pinvoke_cleanup, NULL, NULL, &AppDomain_t2719102437_0_0_0 } /* System.AppDomain */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t3898244613, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t3898244613_0_0_0 } /* System.AppDomainInitializer */,
	{ NULL, AppDomainSetup_t611332832_marshal_pinvoke, AppDomainSetup_t611332832_marshal_pinvoke_back, AppDomainSetup_t611332832_marshal_pinvoke_cleanup, NULL, NULL, &AppDomainSetup_t611332832_0_0_0 } /* System.AppDomainSetup */,
	{ NULL, DictionaryEntry_t3048875398_marshal_pinvoke, DictionaryEntry_t3048875398_marshal_pinvoke_back, DictionaryEntry_t3048875398_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3048875398_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, bucket_t976591655_marshal_pinvoke, bucket_t976591655_marshal_pinvoke_back, bucket_t976591655_marshal_pinvoke_cleanup, NULL, NULL, &bucket_t976591655_0_0_0 } /* System.Collections.Hashtable/bucket */,
	{ DelegatePInvokeWrapper_InternalCancelHandler_t2895223116, NULL, NULL, NULL, NULL, NULL, &InternalCancelHandler_t2895223116_0_0_0 } /* System.Console/InternalCancelHandler */,
	{ DelegatePInvokeWrapper_WindowsCancelHandler_t1197082027, NULL, NULL, NULL, NULL, NULL, &WindowsCancelHandler_t1197082027_0_0_0 } /* System.Console/WindowsConsole/WindowsCancelHandler */,
	{ NULL, ConsoleKeyInfo_t3124575640_marshal_pinvoke, ConsoleKeyInfo_t3124575640_marshal_pinvoke_back, ConsoleKeyInfo_t3124575640_marshal_pinvoke_cleanup, NULL, NULL, &ConsoleKeyInfo_t3124575640_0_0_0 } /* System.ConsoleKeyInfo */,
	{ NULL, DateTimeRawInfo_t1887088787_marshal_pinvoke, DateTimeRawInfo_t1887088787_marshal_pinvoke_back, DateTimeRawInfo_t1887088787_marshal_pinvoke_cleanup, NULL, NULL, &DateTimeRawInfo_t1887088787_0_0_0 } /* System.DateTimeRawInfo */,
	{ NULL, DateTimeResult_t824298922_marshal_pinvoke, DateTimeResult_t824298922_marshal_pinvoke_back, DateTimeResult_t824298922_marshal_pinvoke_cleanup, NULL, NULL, &DateTimeResult_t824298922_0_0_0 } /* System.DateTimeResult */,
	{ NULL, Delegate_t3022476291_marshal_pinvoke, Delegate_t3022476291_marshal_pinvoke_back, Delegate_t3022476291_marshal_pinvoke_cleanup, NULL, NULL, &Delegate_t3022476291_0_0_0 } /* System.Delegate */,
	{ NULL, StackFrame_t2050294881_marshal_pinvoke, StackFrame_t2050294881_marshal_pinvoke_back, StackFrame_t2050294881_marshal_pinvoke_cleanup, NULL, NULL, &StackFrame_t2050294881_0_0_0 } /* System.Diagnostics.StackFrame */,
	{ NULL, DTSubString_t4037085109_marshal_pinvoke, DTSubString_t4037085109_marshal_pinvoke_back, DTSubString_t4037085109_marshal_pinvoke_cleanup, NULL, NULL, &DTSubString_t4037085109_0_0_0 } /* System.DTSubString */,
	{ NULL, Enum_t2459695545_marshal_pinvoke, Enum_t2459695545_marshal_pinvoke_back, Enum_t2459695545_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t2459695545_0_0_0 } /* System.Enum */,
	{ NULL, EnumResult_t2872047947_marshal_pinvoke, EnumResult_t2872047947_marshal_pinvoke_back, EnumResult_t2872047947_marshal_pinvoke_cleanup, NULL, NULL, &EnumResult_t2872047947_0_0_0 } /* System.Enum/EnumResult */,
	{ NULL, Exception_t1927440687_marshal_pinvoke, Exception_t1927440687_marshal_pinvoke_back, Exception_t1927440687_marshal_pinvoke_cleanup, NULL, NULL, &Exception_t1927440687_0_0_0 } /* System.Exception */,
	{ NULL, CalendarData_t275297348_marshal_pinvoke, CalendarData_t275297348_marshal_pinvoke_back, CalendarData_t275297348_marshal_pinvoke_cleanup, NULL, NULL, &CalendarData_t275297348_0_0_0 } /* System.Globalization.CalendarData */,
	{ NULL, UnicodeDataHeader_t2784996206_marshal_pinvoke, UnicodeDataHeader_t2784996206_marshal_pinvoke_back, UnicodeDataHeader_t2784996206_marshal_pinvoke_cleanup, NULL, NULL, &UnicodeDataHeader_t2784996206_0_0_0 } /* System.Globalization.CharUnicodeInfo/UnicodeDataHeader */,
	{ NULL, CultureData_t3400086592_marshal_pinvoke, CultureData_t3400086592_marshal_pinvoke_back, CultureData_t3400086592_marshal_pinvoke_cleanup, NULL, NULL, &CultureData_t3400086592_0_0_0 } /* System.Globalization.CultureData */,
	{ NULL, CultureInfo_t3500843524_marshal_pinvoke, CultureInfo_t3500843524_marshal_pinvoke_back, CultureInfo_t3500843524_marshal_pinvoke_cleanup, NULL, NULL, &CultureInfo_t3500843524_0_0_0 } /* System.Globalization.CultureInfo */,
	{ NULL, Data_t1401534395_marshal_pinvoke, Data_t1401534395_marshal_pinvoke_back, Data_t1401534395_marshal_pinvoke_cleanup, NULL, NULL, &Data_t1401534395_0_0_0 } /* System.Globalization.CultureInfo/Data */,
	{ NULL, InternalCodePageDataItem_t1742109366_marshal_pinvoke, InternalCodePageDataItem_t1742109366_marshal_pinvoke_back, InternalCodePageDataItem_t1742109366_marshal_pinvoke_cleanup, NULL, NULL, &InternalCodePageDataItem_t1742109366_0_0_0 } /* System.Globalization.InternalCodePageDataItem */,
	{ NULL, InternalEncodingDataItem_t82919681_marshal_pinvoke, InternalEncodingDataItem_t82919681_marshal_pinvoke_back, InternalEncodingDataItem_t82919681_marshal_pinvoke_cleanup, NULL, NULL, &InternalEncodingDataItem_t82919681_0_0_0 } /* System.Globalization.InternalEncodingDataItem */,
	{ NULL, SortKey_t1270563137_marshal_pinvoke, SortKey_t1270563137_marshal_pinvoke_back, SortKey_t1270563137_marshal_pinvoke_cleanup, NULL, NULL, &SortKey_t1270563137_0_0_0 } /* System.Globalization.SortKey */,
	{ NULL, FormatLiterals_t2087080800_marshal_pinvoke, FormatLiterals_t2087080800_marshal_pinvoke_back, FormatLiterals_t2087080800_marshal_pinvoke_cleanup, NULL, NULL, &FormatLiterals_t2087080800_0_0_0 } /* System.Globalization.TimeSpanFormat/FormatLiterals */,
	{ NULL, StringParser_t2680633667_marshal_pinvoke, StringParser_t2680633667_marshal_pinvoke_back, StringParser_t2680633667_marshal_pinvoke_cleanup, NULL, NULL, &StringParser_t2680633667_0_0_0 } /* System.Globalization.TimeSpanParse/StringParser */,
	{ NULL, TimeSpanRawInfo_t1900124058_marshal_pinvoke, TimeSpanRawInfo_t1900124058_marshal_pinvoke_back, TimeSpanRawInfo_t1900124058_marshal_pinvoke_cleanup, NULL, NULL, &TimeSpanRawInfo_t1900124058_0_0_0 } /* System.Globalization.TimeSpanParse/TimeSpanRawInfo */,
	{ NULL, TimeSpanResult_t782244817_marshal_pinvoke, TimeSpanResult_t782244817_marshal_pinvoke_back, TimeSpanResult_t782244817_marshal_pinvoke_cleanup, NULL, NULL, &TimeSpanResult_t782244817_0_0_0 } /* System.Globalization.TimeSpanParse/TimeSpanResult */,
	{ NULL, TimeSpanToken_t2031659367_marshal_pinvoke, TimeSpanToken_t2031659367_marshal_pinvoke_back, TimeSpanToken_t2031659367_marshal_pinvoke_cleanup, NULL, NULL, &TimeSpanToken_t2031659367_0_0_0 } /* System.Globalization.TimeSpanParse/TimeSpanToken */,
	{ NULL, TimeSpanTokenizer_t1845673523_marshal_pinvoke, TimeSpanTokenizer_t1845673523_marshal_pinvoke_back, TimeSpanTokenizer_t1845673523_marshal_pinvoke_cleanup, NULL, NULL, &TimeSpanTokenizer_t1845673523_0_0_0 } /* System.Globalization.TimeSpanParse/TimeSpanTokenizer */,
	{ NULL, GuidResult_t2567604379_marshal_pinvoke, GuidResult_t2567604379_marshal_pinvoke_back, GuidResult_t2567604379_marshal_pinvoke_cleanup, NULL, NULL, &GuidResult_t2567604379_0_0_0 } /* System.Guid/GuidResult */,
	{ NULL, InputRecord_t3971050487_marshal_pinvoke, InputRecord_t3971050487_marshal_pinvoke_back, InputRecord_t3971050487_marshal_pinvoke_cleanup, NULL, NULL, &InputRecord_t3971050487_0_0_0 } /* System.InputRecord */,
	{ DelegatePInvokeWrapper_ReadDelegate_t3184826381, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t3184826381_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t489908132, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t489908132_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MarshalByRefObject_t1285298191_marshal_pinvoke, MarshalByRefObject_t1285298191_marshal_pinvoke_back, MarshalByRefObject_t1285298191_marshal_pinvoke_cleanup, NULL, NULL, &MarshalByRefObject_t1285298191_0_0_0 } /* System.MarshalByRefObject */,
	{ NULL, MonoAsyncCall_t3796687503_marshal_pinvoke, MonoAsyncCall_t3796687503_marshal_pinvoke_back, MonoAsyncCall_t3796687503_marshal_pinvoke_cleanup, NULL, NULL, &MonoAsyncCall_t3796687503_0_0_0 } /* System.MonoAsyncCall */,
	{ NULL, MonoTypeInfo_t1976057079_marshal_pinvoke, MonoTypeInfo_t1976057079_marshal_pinvoke_back, MonoTypeInfo_t1976057079_marshal_pinvoke_cleanup, NULL, NULL, &MonoTypeInfo_t1976057079_0_0_0 } /* System.MonoTypeInfo */,
	{ NULL, MulticastDelegate_t3201952435_marshal_pinvoke, MulticastDelegate_t3201952435_marshal_pinvoke_back, MulticastDelegate_t3201952435_marshal_pinvoke_cleanup, NULL, NULL, &MulticastDelegate_t3201952435_0_0_0 } /* System.MulticastDelegate */,
	{ NULL, NumberBuffer_t3547128574_marshal_pinvoke, NumberBuffer_t3547128574_marshal_pinvoke_back, NumberBuffer_t3547128574_marshal_pinvoke_cleanup, NULL, NULL, &NumberBuffer_t3547128574_0_0_0 } /* System.Number/NumberBuffer */,
	{ NULL, FormatParam_t2947516451_marshal_pinvoke, FormatParam_t2947516451_marshal_pinvoke_back, FormatParam_t2947516451_marshal_pinvoke_cleanup, NULL, NULL, &FormatParam_t2947516451_0_0_0 } /* System.ParameterizedStrings/FormatParam */,
	{ NULL, ParamsArray_t2726825425_marshal_pinvoke, ParamsArray_t2726825425_marshal_pinvoke_back, ParamsArray_t2726825425_marshal_pinvoke_cleanup, NULL, NULL, &ParamsArray_t2726825425_0_0_0 } /* System.ParamsArray */,
	{ NULL, ParsingInfo_t442145320_marshal_pinvoke, ParsingInfo_t442145320_marshal_pinvoke_back, ParsingInfo_t442145320_marshal_pinvoke_cleanup, NULL, NULL, &ParsingInfo_t442145320_0_0_0 } /* System.ParsingInfo */,
	{ NULL, Assembly_t4268412390_marshal_pinvoke, Assembly_t4268412390_marshal_pinvoke_back, Assembly_t4268412390_marshal_pinvoke_cleanup, NULL, NULL, &Assembly_t4268412390_0_0_0 } /* System.Reflection.Assembly */,
	{ NULL, AssemblyName_t894705941_marshal_pinvoke, AssemblyName_t894705941_marshal_pinvoke_back, AssemblyName_t894705941_marshal_pinvoke_cleanup, NULL, NULL, &AssemblyName_t894705941_0_0_0 } /* System.Reflection.AssemblyName */,
	{ NULL, CustomAttributeNamedArgument_t94157543_marshal_pinvoke, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t94157543_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t1498197914_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, AssemblyBuilder_t1646117627_marshal_pinvoke, AssemblyBuilder_t1646117627_marshal_pinvoke_back, AssemblyBuilder_t1646117627_marshal_pinvoke_cleanup, NULL, NULL, &AssemblyBuilder_t1646117627_0_0_0 } /* System.Reflection.Emit.AssemblyBuilder */,
	{ NULL, CustomAttributeBuilder_t2970303184_marshal_pinvoke, CustomAttributeBuilder_t2970303184_marshal_pinvoke_back, CustomAttributeBuilder_t2970303184_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeBuilder_t2970303184_0_0_0 } /* System.Reflection.Emit.CustomAttributeBuilder */,
	{ NULL, EventBuilder_t2927243889_marshal_pinvoke, EventBuilder_t2927243889_marshal_pinvoke_back, EventBuilder_t2927243889_marshal_pinvoke_cleanup, NULL, NULL, &EventBuilder_t2927243889_0_0_0 } /* System.Reflection.Emit.EventBuilder */,
	{ NULL, ILExceptionBlock_t2042475189_marshal_pinvoke, ILExceptionBlock_t2042475189_marshal_pinvoke_back, ILExceptionBlock_t2042475189_marshal_pinvoke_cleanup, NULL, NULL, &ILExceptionBlock_t2042475189_0_0_0 } /* System.Reflection.Emit.ILExceptionBlock */,
	{ NULL, ILExceptionInfo_t1490154598_marshal_pinvoke, ILExceptionInfo_t1490154598_marshal_pinvoke_back, ILExceptionInfo_t1490154598_marshal_pinvoke_cleanup, NULL, NULL, &ILExceptionInfo_t1490154598_0_0_0 } /* System.Reflection.Emit.ILExceptionInfo */,
	{ NULL, ILGenerator_t99948092_marshal_pinvoke, ILGenerator_t99948092_marshal_pinvoke_back, ILGenerator_t99948092_marshal_pinvoke_cleanup, NULL, NULL, &ILGenerator_t99948092_0_0_0 } /* System.Reflection.Emit.ILGenerator */,
	{ NULL, ILTokenInfo_t149559338_marshal_pinvoke, ILTokenInfo_t149559338_marshal_pinvoke_back, ILTokenInfo_t149559338_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t149559338_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, LocalBuilder_t2116499186_marshal_pinvoke, LocalBuilder_t2116499186_marshal_pinvoke_back, LocalBuilder_t2116499186_marshal_pinvoke_cleanup, NULL, NULL, &LocalBuilder_t2116499186_0_0_0 } /* System.Reflection.Emit.LocalBuilder */,
	{ NULL, ModuleBuilder_t4156028127_marshal_pinvoke, ModuleBuilder_t4156028127_marshal_pinvoke_back, ModuleBuilder_t4156028127_marshal_pinvoke_cleanup, NULL, NULL, &ModuleBuilder_t4156028127_0_0_0 } /* System.Reflection.Emit.ModuleBuilder */,
	{ NULL, MonoResource_t3127387157_marshal_pinvoke, MonoResource_t3127387157_marshal_pinvoke_back, MonoResource_t3127387157_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t3127387157_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t2467306218_marshal_pinvoke, MonoWin32Resource_t2467306218_marshal_pinvoke_back, MonoWin32Resource_t2467306218_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t2467306218_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, ParameterBuilder_t3344728474_marshal_pinvoke, ParameterBuilder_t3344728474_marshal_pinvoke_back, ParameterBuilder_t3344728474_marshal_pinvoke_cleanup, NULL, NULL, &ParameterBuilder_t3344728474_0_0_0 } /* System.Reflection.Emit.ParameterBuilder */,
	{ NULL, RefEmitPermissionSet_t2708608433_marshal_pinvoke, RefEmitPermissionSet_t2708608433_marshal_pinvoke_back, RefEmitPermissionSet_t2708608433_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t2708608433_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, UnmanagedMarshal_t4270021860_marshal_pinvoke, UnmanagedMarshal_t4270021860_marshal_pinvoke_back, UnmanagedMarshal_t4270021860_marshal_pinvoke_cleanup, NULL, NULL, &UnmanagedMarshal_t4270021860_0_0_0 } /* System.Reflection.Emit.UnmanagedMarshal */,
	{ NULL, LocalVariableInfo_t1749284021_marshal_pinvoke, LocalVariableInfo_t1749284021_marshal_pinvoke_back, LocalVariableInfo_t1749284021_marshal_pinvoke_cleanup, NULL, NULL, &LocalVariableInfo_t1749284021_0_0_0 } /* System.Reflection.LocalVariableInfo */,
	{ NULL, Module_t4282841206_marshal_pinvoke, Module_t4282841206_marshal_pinvoke_back, Module_t4282841206_marshal_pinvoke_cleanup, NULL, NULL, &Module_t4282841206_0_0_0 } /* System.Reflection.Module */,
	{ NULL, MonoEventInfo_t2190036573_marshal_pinvoke, MonoEventInfo_t2190036573_marshal_pinvoke_back, MonoEventInfo_t2190036573_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t2190036573_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t3646562144_marshal_pinvoke, MonoMethodInfo_t3646562144_marshal_pinvoke_back, MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t3646562144_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t486106184_marshal_pinvoke, MonoPropertyInfo_t486106184_marshal_pinvoke_back, MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t486106184_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterInfo_t2249040075_marshal_pinvoke, ParameterInfo_t2249040075_marshal_pinvoke_back, ParameterInfo_t2249040075_marshal_pinvoke_cleanup, NULL, NULL, &ParameterInfo_t2249040075_0_0_0 } /* System.Reflection.ParameterInfo */,
	{ NULL, ParameterModifier_t1820634920_marshal_pinvoke, ParameterModifier_t1820634920_marshal_pinvoke_back, ParameterModifier_t1820634920_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1820634920_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceLocator_t2156390884_marshal_pinvoke, ResourceLocator_t2156390884_marshal_pinvoke_back, ResourceLocator_t2156390884_marshal_pinvoke_cleanup, NULL, NULL, &ResourceLocator_t2156390884_0_0_0 } /* System.Resources.ResourceLocator */,
	{ NULL, AsyncMethodBuilderCore_t2485284745_marshal_pinvoke, AsyncMethodBuilderCore_t2485284745_marshal_pinvoke_back, AsyncMethodBuilderCore_t2485284745_marshal_pinvoke_cleanup, NULL, NULL, &AsyncMethodBuilderCore_t2485284745_0_0_0 } /* System.Runtime.CompilerServices.AsyncMethodBuilderCore */,
	{ NULL, Ephemeron_t1875076633_marshal_pinvoke, Ephemeron_t1875076633_marshal_pinvoke_back, Ephemeron_t1875076633_marshal_pinvoke_cleanup, NULL, NULL, &Ephemeron_t1875076633_0_0_0 } /* System.Runtime.CompilerServices.Ephemeron */,
	{ NULL, TaskAwaiter_t3341738712_marshal_pinvoke, TaskAwaiter_t3341738712_marshal_pinvoke_back, TaskAwaiter_t3341738712_marshal_pinvoke_cleanup, NULL, NULL, &TaskAwaiter_t3341738712_0_0_0 } /* System.Runtime.CompilerServices.TaskAwaiter */,
	{ NULL, NULL, NULL, NULL, CreateComCallableWrapperFor_ManagedErrorInfo_t914761495, NULL, &ManagedErrorInfo_t914761495_0_0_0 } /* System.Runtime.InteropServices.ManagedErrorInfo */,
	{ NULL, Context_t502196753_marshal_pinvoke, Context_t502196753_marshal_pinvoke_back, Context_t502196753_marshal_pinvoke_cleanup, NULL, NULL, &Context_t502196753_0_0_0 } /* System.Runtime.Remoting.Contexts.Context */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t754146990, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t754146990_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ NULL, AsyncResult_t2232356043_marshal_pinvoke, AsyncResult_t2232356043_marshal_pinvoke_back, AsyncResult_t2232356043_marshal_pinvoke_cleanup, NULL, NULL, &AsyncResult_t2232356043_0_0_0 } /* System.Runtime.Remoting.Messaging.AsyncResult */,
	{ NULL, Reader_t1568751674_marshal_pinvoke, Reader_t1568751674_marshal_pinvoke_back, Reader_t1568751674_marshal_pinvoke_cleanup, NULL, NULL, &Reader_t1568751674_0_0_0 } /* System.Runtime.Remoting.Messaging.LogicalCallContext/Reader */,
	{ NULL, MonoMethodMessage_t771543475_marshal_pinvoke, MonoMethodMessage_t771543475_marshal_pinvoke_back, MonoMethodMessage_t771543475_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodMessage_t771543475_0_0_0 } /* System.Runtime.Remoting.Messaging.MonoMethodMessage */,
	{ NULL, RealProxy_t298428346_marshal_pinvoke, RealProxy_t298428346_marshal_pinvoke_back, RealProxy_t298428346_marshal_pinvoke_cleanup, NULL, NULL, &RealProxy_t298428346_0_0_0 } /* System.Runtime.Remoting.Proxies.RealProxy */,
	{ NULL, TransparentProxy_t3836393972_marshal_pinvoke, TransparentProxy_t3836393972_marshal_pinvoke_back, TransparentProxy_t3836393972_marshal_pinvoke_cleanup, NULL, NULL, &TransparentProxy_t3836393972_0_0_0 } /* System.Runtime.Remoting.Proxies.TransparentProxy */,
	{ NULL, SerializationEntry_t3485203212_marshal_pinvoke, SerializationEntry_t3485203212_marshal_pinvoke_back, SerializationEntry_t3485203212_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t3485203212_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ DelegatePInvokeWrapper_SerializationEventHandler_t2339848500, NULL, NULL, NULL, NULL, NULL, &SerializationEventHandler_t2339848500_0_0_0 } /* System.Runtime.Serialization.SerializationEventHandler */,
	{ NULL, StreamingContext_t1417235061_marshal_pinvoke, StreamingContext_t1417235061_marshal_pinvoke_back, StreamingContext_t1417235061_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t1417235061_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1872138834_marshal_pinvoke, DSAParameters_t1872138834_marshal_pinvoke_back, DSAParameters_t1872138834_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1872138834_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, HashAlgorithmName_t1682985260_marshal_pinvoke, HashAlgorithmName_t1682985260_marshal_pinvoke_back, HashAlgorithmName_t1682985260_marshal_pinvoke_cleanup, NULL, NULL, &HashAlgorithmName_t1682985260_0_0_0 } /* System.Security.Cryptography.HashAlgorithmName */,
	{ NULL, RSAParameters_t1462703416_marshal_pinvoke, RSAParameters_t1462703416_marshal_pinvoke_back, RSAParameters_t1462703416_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1462703416_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SNIP_t4214504621_marshal_pinvoke, SNIP_t4214504621_marshal_pinvoke_back, SNIP_t4214504621_marshal_pinvoke_cleanup, NULL, NULL, &SNIP_t4214504621_0_0_0 } /* System.Security.Permissions.StrongNameIdentityPermission/SNIP */,
	{ NULL, CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke, CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke_back, CancellationCallbackCoreWorkArguments_t3564269970_marshal_pinvoke_cleanup, NULL, NULL, &CancellationCallbackCoreWorkArguments_t3564269970_0_0_0 } /* System.Threading.CancellationCallbackCoreWorkArguments */,
	{ NULL, CancellationToken_t1851405782_marshal_pinvoke, CancellationToken_t1851405782_marshal_pinvoke_back, CancellationToken_t1851405782_marshal_pinvoke_cleanup, NULL, NULL, &CancellationToken_t1851405782_0_0_0 } /* System.Threading.CancellationToken */,
	{ NULL, CancellationTokenRegistration_t1708859357_marshal_pinvoke, CancellationTokenRegistration_t1708859357_marshal_pinvoke_back, CancellationTokenRegistration_t1708859357_marshal_pinvoke_cleanup, NULL, NULL, &CancellationTokenRegistration_t1708859357_0_0_0 } /* System.Threading.CancellationTokenRegistration */,
	{ NULL, Reader_t611157692_marshal_pinvoke, Reader_t611157692_marshal_pinvoke_back, Reader_t611157692_marshal_pinvoke_cleanup, NULL, NULL, &Reader_t611157692_0_0_0 } /* System.Threading.ExecutionContext/Reader */,
	{ NULL, ExecutionContextSwitcher_t3304742894_marshal_pinvoke, ExecutionContextSwitcher_t3304742894_marshal_pinvoke_back, ExecutionContextSwitcher_t3304742894_marshal_pinvoke_cleanup, NULL, NULL, &ExecutionContextSwitcher_t3304742894_0_0_0 } /* System.Threading.ExecutionContextSwitcher */,
	{ NULL, SpinLock_t2136334209_marshal_pinvoke, SpinLock_t2136334209_marshal_pinvoke_back, SpinLock_t2136334209_marshal_pinvoke_cleanup, NULL, NULL, &SpinLock_t2136334209_0_0_0 } /* System.Threading.SpinLock */,
	{ DelegatePInvokeWrapper_ThreadStart_t3437517264, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t3437517264_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, WaitHandle_t677569169_marshal_pinvoke, WaitHandle_t677569169_marshal_pinvoke_back, WaitHandle_t677569169_marshal_pinvoke_cleanup, NULL, NULL, &WaitHandle_t677569169_0_0_0 } /* System.Threading.WaitHandle */,
	{ NULL, TransitionTime_t3441274853_marshal_pinvoke, TransitionTime_t3441274853_marshal_pinvoke_back, TransitionTime_t3441274853_marshal_pinvoke_cleanup, NULL, NULL, &TransitionTime_t3441274853_0_0_0 } /* System.TimeZoneInfo/TransitionTime */,
	{ NULL, TZifType_t1855764066_marshal_pinvoke, TZifType_t1855764066_marshal_pinvoke_back, TZifType_t1855764066_marshal_pinvoke_cleanup, NULL, NULL, &TZifType_t1855764066_0_0_0 } /* System.TimeZoneInfo/TZifType */,
	{ NULL, UnSafeCharBuffer_t927766464_marshal_pinvoke, UnSafeCharBuffer_t927766464_marshal_pinvoke_back, UnSafeCharBuffer_t927766464_marshal_pinvoke_cleanup, NULL, NULL, &UnSafeCharBuffer_t927766464_0_0_0 } /* System.UnSafeCharBuffer */,
	{ NULL, ValueType_t3507792607_marshal_pinvoke, ValueType_t3507792607_marshal_pinvoke_back, ValueType_t3507792607_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3507792607_0_0_0 } /* System.ValueType */,
	{ NULL, VersionResult_t785420859_marshal_pinvoke, VersionResult_t785420859_marshal_pinvoke_back, VersionResult_t785420859_marshal_pinvoke_cleanup, NULL, NULL, &VersionResult_t785420859_0_0_0 } /* System.Version/VersionResult */,
	{ DelegatePInvokeWrapper_CFProxyAutoConfigurationResultCallback_t3537096558, NULL, NULL, NULL, NULL, NULL, &CFProxyAutoConfigurationResultCallback_t3537096558_0_0_0 } /* Mono.Net.CFNetwork/CFProxyAutoConfigurationResultCallback */,
	{ NULL, AttributeEntry_t168441916_marshal_pinvoke, AttributeEntry_t168441916_marshal_pinvoke_back, AttributeEntry_t168441916_marshal_pinvoke_cleanup, NULL, NULL, &AttributeEntry_t168441916_0_0_0 } /* System.ComponentModel.AttributeCollection/AttributeEntry */,
	{ NULL, DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke, DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke_back, DefaultExtendedTypeDescriptor_t3612110989_marshal_pinvoke_cleanup, NULL, NULL, &DefaultExtendedTypeDescriptor_t3612110989_0_0_0 } /* System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultExtendedTypeDescriptor */,
	{ NULL, DefaultTypeDescriptor_t3334440910_marshal_pinvoke, DefaultTypeDescriptor_t3334440910_marshal_pinvoke_back, DefaultTypeDescriptor_t3334440910_marshal_pinvoke_cleanup, NULL, NULL, &DefaultTypeDescriptor_t3334440910_0_0_0 } /* System.ComponentModel.TypeDescriptor/TypeDescriptionNode/DefaultTypeDescriptor */,
	{ DelegatePInvokeWrapper_ReadMethod_t3362229488, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t3362229488_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_WriteMethod_t1894833619, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t1894833619_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t4047001824, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t4047001824_0_0_0 } /* System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite */,
	{ NULL, IOAsyncResult_t1276329107_marshal_pinvoke, IOAsyncResult_t1276329107_marshal_pinvoke_back, IOAsyncResult_t1276329107_marshal_pinvoke_cleanup, NULL, NULL, &IOAsyncResult_t1276329107_0_0_0 } /* System.IOAsyncResult */,
	{ NULL, IOSelectorJob_t2021937086_marshal_pinvoke, IOSelectorJob_t2021937086_marshal_pinvoke_back, IOSelectorJob_t2021937086_marshal_pinvoke_cleanup, NULL, NULL, &IOSelectorJob_t2021937086_0_0_0 } /* System.IOSelectorJob */,
	{ NULL, RecognizedAttribute_t3930529114_marshal_pinvoke, RecognizedAttribute_t3930529114_marshal_pinvoke_back, RecognizedAttribute_t3930529114_marshal_pinvoke_cleanup, NULL, NULL, &RecognizedAttribute_t3930529114_0_0_0 } /* System.Net.CookieTokenizer/RecognizedAttribute */,
	{ DelegatePInvokeWrapper_ReadDelegate_t1559754630, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t1559754630_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t888270799, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t888270799_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_HeaderParser_t3706119548, NULL, NULL, NULL, NULL, NULL, &HeaderParser_t3706119548_0_0_0 } /* System.Net.HeaderParser */,
	{ NULL, HeaderVariantInfo_t2058796272_marshal_pinvoke, HeaderVariantInfo_t2058796272_marshal_pinvoke_back, HeaderVariantInfo_t2058796272_marshal_pinvoke_cleanup, NULL, NULL, &HeaderVariantInfo_t2058796272_0_0_0 } /* System.Net.HeaderVariantInfo */,
	{ NULL, AuthorizationState_t3879143100_marshal_pinvoke, AuthorizationState_t3879143100_marshal_pinvoke_back, AuthorizationState_t3879143100_marshal_pinvoke_cleanup, NULL, NULL, &AuthorizationState_t3879143100_0_0_0 } /* System.Net.HttpWebRequest/AuthorizationState */,
	{ NULL, IPv6AddressFormatter_t3116172695_marshal_pinvoke, IPv6AddressFormatter_t3116172695_marshal_pinvoke_back, IPv6AddressFormatter_t3116172695_marshal_pinvoke_cleanup, NULL, NULL, &IPv6AddressFormatter_t3116172695_0_0_0 } /* System.Net.IPv6AddressFormatter */,
	{ NULL, ifaddrs_t2532459533_marshal_pinvoke, ifaddrs_t2532459533_marshal_pinvoke_back, ifaddrs_t2532459533_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t2532459533_0_0_0 } /* System.Net.NetworkInformation.ifaddrs */,
	{ NULL, in6_addr_t4035827331_marshal_pinvoke, in6_addr_t4035827331_marshal_pinvoke_back, in6_addr_t4035827331_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t4035827331_0_0_0 } /* System.Net.NetworkInformation.in6_addr */,
	{ NULL, ifaddrs_t937751619_marshal_pinvoke, ifaddrs_t937751619_marshal_pinvoke_back, ifaddrs_t937751619_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t937751619_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.ifaddrs */,
	{ NULL, in6_addr_t1014645363_marshal_pinvoke, in6_addr_t1014645363_marshal_pinvoke_back, in6_addr_t1014645363_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t1014645363_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.in6_addr */,
	{ NULL, sockaddr_dl_t3955242742_marshal_pinvoke, sockaddr_dl_t3955242742_marshal_pinvoke_back, sockaddr_dl_t3955242742_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_dl_t3955242742_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_dl */,
	{ NULL, sockaddr_in6_t834146887_marshal_pinvoke, sockaddr_in6_t834146887_marshal_pinvoke_back, sockaddr_in6_t834146887_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t834146887_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_in6 */,
	{ NULL, sockaddr_in6_t2899168071_marshal_pinvoke, sockaddr_in6_t2899168071_marshal_pinvoke_back, sockaddr_in6_t2899168071_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t2899168071_0_0_0 } /* System.Net.NetworkInformation.sockaddr_in6 */,
	{ NULL, sockaddr_ll_t1681025498_marshal_pinvoke, sockaddr_ll_t1681025498_marshal_pinvoke_back, sockaddr_ll_t1681025498_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_ll_t1681025498_0_0_0 } /* System.Net.NetworkInformation.sockaddr_ll */,
	{ NULL, Win32_FIXED_INFO_t1371335919_marshal_pinvoke, Win32_FIXED_INFO_t1371335919_marshal_pinvoke_back, Win32_FIXED_INFO_t1371335919_marshal_pinvoke_cleanup, NULL, NULL, &Win32_FIXED_INFO_t1371335919_0_0_0 } /* System.Net.NetworkInformation.Win32_FIXED_INFO */,
	{ NULL, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_back, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES */,
	{ NULL, Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke, Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke_back, Win32_IP_ADDR_STRING_t2646152127_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADDR_STRING_t2646152127_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADDR_STRING */,
	{ NULL, Win32_MIB_IFROW_t4215928996_marshal_pinvoke, Win32_MIB_IFROW_t4215928996_marshal_pinvoke_back, Win32_MIB_IFROW_t4215928996_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_IFROW_t4215928996_0_0_0 } /* System.Net.NetworkInformation.Win32_MIB_IFROW */,
	{ NULL, Win32_SOCKADDR_t1606057231_marshal_pinvoke, Win32_SOCKADDR_t1606057231_marshal_pinvoke_back, Win32_SOCKADDR_t1606057231_marshal_pinvoke_cleanup, NULL, NULL, &Win32_SOCKADDR_t1606057231_0_0_0 } /* System.Net.NetworkInformation.Win32_SOCKADDR */,
	{ NULL, SocketAsyncResult_t3950677696_marshal_pinvoke, SocketAsyncResult_t3950677696_marshal_pinvoke_back, SocketAsyncResult_t3950677696_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t3950677696_0_0_0 } /* System.Net.Sockets.SocketAsyncResult */,
	{ NULL, X509ChainStatus_t4278378721_marshal_pinvoke, X509ChainStatus_t4278378721_marshal_pinvoke_back, X509ChainStatus_t4278378721_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t4278378721_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, LowerCaseMapping_t2153935826_marshal_pinvoke, LowerCaseMapping_t2153935826_marshal_pinvoke_back, LowerCaseMapping_t2153935826_marshal_pinvoke_cleanup, NULL, NULL, &LowerCaseMapping_t2153935826_0_0_0 } /* System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping */,
	{ NULL, XPathNode_t3118381855_marshal_pinvoke, XPathNode_t3118381855_marshal_pinvoke_back, XPathNode_t3118381855_marshal_pinvoke_cleanup, NULL, NULL, &XPathNode_t3118381855_0_0_0 } /* MS.Internal.Xml.Cache.XPathNode */,
	{ NULL, XPathNodeRef_t2092605142_marshal_pinvoke, XPathNodeRef_t2092605142_marshal_pinvoke_back, XPathNodeRef_t2092605142_marshal_pinvoke_cleanup, NULL, NULL, &XPathNodeRef_t2092605142_0_0_0 } /* MS.Internal.Xml.Cache.XPathNodeRef */,
	{ NULL, FacetsCompiler_t3565032206_marshal_pinvoke, FacetsCompiler_t3565032206_marshal_pinvoke_back, FacetsCompiler_t3565032206_marshal_pinvoke_cleanup, NULL, NULL, &FacetsCompiler_t3565032206_0_0_0 } /* System.Xml.Schema.FacetsChecker/FacetsCompiler */,
	{ NULL, Map_t2552390023_marshal_pinvoke, Map_t2552390023_marshal_pinvoke_back, Map_t2552390023_marshal_pinvoke_cleanup, NULL, NULL, &Map_t2552390023_0_0_0 } /* System.Xml.Schema.FacetsChecker/FacetsCompiler/Map */,
	{ NULL, Position_t1796812729_marshal_pinvoke, Position_t1796812729_marshal_pinvoke_back, Position_t1796812729_marshal_pinvoke_cleanup, NULL, NULL, &Position_t1796812729_0_0_0 } /* System.Xml.Schema.Position */,
	{ NULL, RangePositionInfo_t2780802922_marshal_pinvoke, RangePositionInfo_t2780802922_marshal_pinvoke_back, RangePositionInfo_t2780802922_marshal_pinvoke_cleanup, NULL, NULL, &RangePositionInfo_t2780802922_0_0_0 } /* System.Xml.Schema.RangePositionInfo */,
	{ NULL, SequenceConstructPosContext_t3853454650_marshal_pinvoke, SequenceConstructPosContext_t3853454650_marshal_pinvoke_back, SequenceConstructPosContext_t3853454650_marshal_pinvoke_cleanup, NULL, NULL, &SequenceConstructPosContext_t3853454650_0_0_0 } /* System.Xml.Schema.SequenceNode/SequenceConstructPosContext */,
	{ NULL, Union_t69719246_marshal_pinvoke, Union_t69719246_marshal_pinvoke_back, Union_t69719246_marshal_pinvoke_cleanup, NULL, NULL, &Union_t69719246_0_0_0 } /* System.Xml.Schema.XmlAtomicValue/Union */,
	{ NULL, XmlSchemaObjectEntry_t2510586510_marshal_pinvoke, XmlSchemaObjectEntry_t2510586510_marshal_pinvoke_back, XmlSchemaObjectEntry_t2510586510_marshal_pinvoke_cleanup, NULL, NULL, &XmlSchemaObjectEntry_t2510586510_0_0_0 } /* System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry */,
	{ NULL, XsdDateTime_t2434467484_marshal_pinvoke, XsdDateTime_t2434467484_marshal_pinvoke_back, XsdDateTime_t2434467484_marshal_pinvoke_cleanup, NULL, NULL, &XsdDateTime_t2434467484_0_0_0 } /* System.Xml.Schema.XsdDateTime */,
	{ NULL, Parser_t2170101039_marshal_pinvoke, Parser_t2170101039_marshal_pinvoke_back, Parser_t2170101039_marshal_pinvoke_cleanup, NULL, NULL, &Parser_t2170101039_0_0_0 } /* System.Xml.Schema.XsdDateTime/Parser */,
	{ DelegatePInvokeWrapper_HashCodeOfStringDelegate_t2463303350, NULL, NULL, NULL, NULL, NULL, &HashCodeOfStringDelegate_t2463303350_0_0_0 } /* System.Xml.SecureStringHasher/HashCodeOfStringDelegate */,
	{ NULL, XmlCharType_t1050521405_marshal_pinvoke, XmlCharType_t1050521405_marshal_pinvoke_back, XmlCharType_t1050521405_marshal_pinvoke_cleanup, NULL, NULL, &XmlCharType_t1050521405_0_0_0 } /* System.Xml.XmlCharType */,
	{ NULL, SmallXmlNodeList_t4252813691_marshal_pinvoke, SmallXmlNodeList_t4252813691_marshal_pinvoke_back, SmallXmlNodeList_t4252813691_marshal_pinvoke_cleanup, NULL, NULL, &SmallXmlNodeList_t4252813691_0_0_0 } /* System.Xml.XmlNamedNodeMap/SmallXmlNodeList */,
	{ NULL, NamespaceDeclaration_t3577631811_marshal_pinvoke, NamespaceDeclaration_t3577631811_marshal_pinvoke_back, NamespaceDeclaration_t3577631811_marshal_pinvoke_cleanup, NULL, NULL, &NamespaceDeclaration_t3577631811_0_0_0 } /* System.Xml.XmlNamespaceManager/NamespaceDeclaration */,
	{ NULL, VirtualAttribute_t2854289803_marshal_pinvoke, VirtualAttribute_t2854289803_marshal_pinvoke_back, VirtualAttribute_t2854289803_marshal_pinvoke_cleanup, NULL, NULL, &VirtualAttribute_t2854289803_0_0_0 } /* System.Xml.XmlNodeReaderNavigator/VirtualAttribute */,
	{ DelegatePInvokeWrapper_HashCodeOfStringDelegate_t3565421161, NULL, NULL, NULL, NULL, NULL, &HashCodeOfStringDelegate_t3565421161_0_0_0 } /* System.Xml.XmlQualifiedName/HashCodeOfStringDelegate */,
	{ NULL, ParsingState_t1278724163_marshal_pinvoke, ParsingState_t1278724163_marshal_pinvoke_back, ParsingState_t1278724163_marshal_pinvoke_cleanup, NULL, NULL, &ParsingState_t1278724163_0_0_0 } /* System.Xml.XmlTextReaderImpl/ParsingState */,
	{ NULL, Namespace_t1808381789_marshal_pinvoke, Namespace_t1808381789_marshal_pinvoke_back, Namespace_t1808381789_marshal_pinvoke_cleanup, NULL, NULL, &Namespace_t1808381789_0_0_0 } /* System.Xml.XmlTextWriter/Namespace */,
	{ NULL, TagInfo_t1244462138_marshal_pinvoke, TagInfo_t1244462138_marshal_pinvoke_back, TagInfo_t1244462138_marshal_pinvoke_cleanup, NULL, NULL, &TagInfo_t1244462138_0_0_0 } /* System.Xml.XmlTextWriter/TagInfo */,
	{ DelegatePInvokeWrapper_OnNavMeshPreUpdate_t2039022291, NULL, NULL, NULL, NULL, NULL, &OnNavMeshPreUpdate_t2039022291_0_0_0 } /* UnityEngine.AI.NavMesh/OnNavMeshPreUpdate */,
	{ NULL, CustomEventData_t1269126727_marshal_pinvoke, CustomEventData_t1269126727_marshal_pinvoke_back, CustomEventData_t1269126727_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t1269126727_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t3238795095_marshal_pinvoke, UnityAnalyticsHandler_t3238795095_marshal_pinvoke_back, UnityAnalyticsHandler_t3238795095_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t3238795095_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ NULL, AnimationCurve_t3306541151_marshal_pinvoke, AnimationCurve_t3306541151_marshal_pinvoke_back, AnimationCurve_t3306541151_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3306541151_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t2428323300_marshal_pinvoke, AnimationEvent_t2428323300_marshal_pinvoke_back, AnimationEvent_t2428323300_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2428323300_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2410896200_marshal_pinvoke, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2410896200_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t1867914413, NULL, NULL, NULL, NULL, NULL, &LogCallback_t1867914413_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t642977590, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t642977590_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t2674559435_marshal_pinvoke, AssetBundleRequest_t2674559435_marshal_pinvoke_back, AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2674559435_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t3814632279_marshal_pinvoke, AsyncOperation_t3814632279_marshal_pinvoke_back, AsyncOperation_t3814632279_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t3814632279_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t3007145346, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t3007145346_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t421863554_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t3743753033_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3522132132, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3522132132_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t2876846408_marshal_pinvoke, Collision_t2876846408_marshal_pinvoke_back, Collision_t2876846408_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t2876846408_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, Collision2D_t1539500754_marshal_pinvoke, Collision2D_t1539500754_marshal_pinvoke_back, Collision2D_t1539500754_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t1539500754_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t1672660996_marshal_pinvoke, ContactFilter2D_t1672660996_marshal_pinvoke_back, ContactFilter2D_t1672660996_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t1672660996_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, ControllerColliderHit_t4070855101_marshal_pinvoke, ControllerColliderHit_t4070855101_marshal_pinvoke_back, ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t4070855101_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t2299508840_marshal_pinvoke, Coroutine_t2299508840_marshal_pinvoke_back, Coroutine_t2299508840_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2299508840_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t1091689465_marshal_pinvoke, CullingGroup_t1091689465_marshal_pinvoke_back, CullingGroup_t1091689465_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t1091689465_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2480912210, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2480912210_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t3423469815_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t3028476042_marshal_pinvoke, Event_t3028476042_marshal_pinvoke_back, Event_t3028476042_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3028476042_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t4025899511, NULL, NULL, NULL, NULL, NULL, &UnityAction_t4025899511_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t1272078033_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3600583008_marshal_pinvoke, Gradient_t3600583008_marshal_pinvoke_back, Gradient_t3600583008_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3600583008_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3486805455, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3486805455_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t4210063000_marshal_pinvoke, GUIContent_t4210063000_marshal_pinvoke_back, GUIContent_t4210063000_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t4210063000_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3594822336_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t1799908754_marshal_pinvoke, GUIStyle_t1799908754_marshal_pinvoke_back, GUIStyle_t1799908754_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t1799908754_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t3801000545_marshal_pinvoke, GUIStyleState_t3801000545_marshal_pinvoke_back, GUIStyleState_t3801000545_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t3801000545_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t1529896151_marshal_pinvoke, HumanBone_t1529896151_marshal_pinvoke_back, HumanBone_t1529896151_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t1529896151_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, DownloadHandler_t1216180266_marshal_pinvoke, DownloadHandler_t1216180266_marshal_pinvoke_back, DownloadHandler_t1216180266_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t1216180266_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerAudioClip_t1520641178_marshal_pinvoke, DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_back, DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerAudioClip_t1520641178_0_0_0 } /* UnityEngine.Networking.DownloadHandlerAudioClip */,
	{ NULL, Object_t1021602117_marshal_pinvoke, Object_t1021602117_marshal_pinvoke_back, Object_t1021602117_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1021602117_0_0_0 } /* UnityEngine.Object */,
	{ NULL, RaycastHit_t87180320_marshal_pinvoke, RaycastHit_t87180320_marshal_pinvoke_back, RaycastHit_t87180320_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t87180320_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t4063908774_marshal_pinvoke, RaycastHit2D_t4063908774_marshal_pinvoke_back, RaycastHit2D_t4063908774_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t4063908774_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t3387826427_marshal_pinvoke, RectOffset_t3387826427_marshal_pinvoke_back, RectOffset_t3387826427_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t3387826427_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t3033456180_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t2560315377_marshal_pinvoke, ResourceRequest_t2560315377_marshal_pinvoke_back, ResourceRequest_t2560315377_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t2560315377_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1975622470_marshal_pinvoke, ScriptableObject_t1975622470_marshal_pinvoke_back, ScriptableObject_t1975622470_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1975622470_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t1761367055_marshal_pinvoke, HitInfo_t1761367055_marshal_pinvoke_back, HitInfo_t1761367055_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t1761367055_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t345082847_marshal_pinvoke, SkeletonBone_t345082847_marshal_pinvoke_back, SkeletonBone_t345082847_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t345082847_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, TrackedReference_t1045890189_marshal_pinvoke, TrackedReference_t1045890189_marshal_pinvoke_back, TrackedReference_t1045890189_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1045890189_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_GestureErrorDelegate_t2343902086, NULL, NULL, NULL, NULL, NULL, &GestureErrorDelegate_t2343902086_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/GestureErrorDelegate */,
	{ DelegatePInvokeWrapper_HoldCanceledEventDelegate_t1855824201, NULL, NULL, NULL, NULL, NULL, &HoldCanceledEventDelegate_t1855824201_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCanceledEventDelegate */,
	{ DelegatePInvokeWrapper_HoldCompletedEventDelegate_t3501137495, NULL, NULL, NULL, NULL, NULL, &HoldCompletedEventDelegate_t3501137495_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/HoldCompletedEventDelegate */,
	{ DelegatePInvokeWrapper_HoldStartedEventDelegate_t3415544547, NULL, NULL, NULL, NULL, NULL, &HoldStartedEventDelegate_t3415544547_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/HoldStartedEventDelegate */,
	{ DelegatePInvokeWrapper_ManipulationCanceledEventDelegate_t2553296249, NULL, NULL, NULL, NULL, NULL, &ManipulationCanceledEventDelegate_t2553296249_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCanceledEventDelegate */,
	{ DelegatePInvokeWrapper_ManipulationCompletedEventDelegate_t2759974023, NULL, NULL, NULL, NULL, NULL, &ManipulationCompletedEventDelegate_t2759974023_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationCompletedEventDelegate */,
	{ DelegatePInvokeWrapper_ManipulationStartedEventDelegate_t2487153075, NULL, NULL, NULL, NULL, NULL, &ManipulationStartedEventDelegate_t2487153075_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationStartedEventDelegate */,
	{ DelegatePInvokeWrapper_ManipulationUpdatedEventDelegate_t2394416487, NULL, NULL, NULL, NULL, NULL, &ManipulationUpdatedEventDelegate_t2394416487_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/ManipulationUpdatedEventDelegate */,
	{ DelegatePInvokeWrapper_NavigationCanceledEventDelegate_t1045620518, NULL, NULL, NULL, NULL, NULL, &NavigationCanceledEventDelegate_t1045620518_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCanceledEventDelegate */,
	{ DelegatePInvokeWrapper_NavigationCompletedEventDelegate_t1982950364, NULL, NULL, NULL, NULL, NULL, &NavigationCompletedEventDelegate_t1982950364_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationCompletedEventDelegate */,
	{ DelegatePInvokeWrapper_NavigationStartedEventDelegate_t3123342528, NULL, NULL, NULL, NULL, NULL, &NavigationStartedEventDelegate_t3123342528_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationStartedEventDelegate */,
	{ DelegatePInvokeWrapper_NavigationUpdatedEventDelegate_t3960847450, NULL, NULL, NULL, NULL, NULL, &NavigationUpdatedEventDelegate_t3960847450_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/NavigationUpdatedEventDelegate */,
	{ DelegatePInvokeWrapper_RecognitionEndedEventDelegate_t831649826, NULL, NULL, NULL, NULL, NULL, &RecognitionEndedEventDelegate_t831649826_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionEndedEventDelegate */,
	{ DelegatePInvokeWrapper_RecognitionStartedEventDelegate_t3116179703, NULL, NULL, NULL, NULL, NULL, &RecognitionStartedEventDelegate_t3116179703_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/RecognitionStartedEventDelegate */,
	{ DelegatePInvokeWrapper_TappedEventDelegate_t2437856093, NULL, NULL, NULL, NULL, NULL, &TappedEventDelegate_t2437856093_0_0_0 } /* UnityEngine.VR.WSA.Input.GestureRecognizer/TappedEventDelegate */,
	{ DelegatePInvokeWrapper_SerializationCompleteDelegate_t389490593, NULL, NULL, NULL, NULL, NULL, &SerializationCompleteDelegate_t389490593_0_0_0 } /* UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationCompleteDelegate */,
	{ DelegatePInvokeWrapper_SerializationDataAvailableDelegate_t2381700809, NULL, NULL, NULL, NULL, NULL, &SerializationDataAvailableDelegate_t2381700809_0_0_0 } /* UnityEngine.VR.WSA.Sharing.WorldAnchorTransferBatch/SerializationDataAvailableDelegate */,
	{ NULL, SurfaceData_t3147297845_marshal_pinvoke, SurfaceData_t3147297845_marshal_pinvoke_back, SurfaceData_t3147297845_marshal_pinvoke_cleanup, NULL, NULL, &SurfaceData_t3147297845_0_0_0 } /* UnityEngine.VR.WSA.SurfaceData */,
	{ DelegatePInvokeWrapper_OnCapturedToDiskCallback_t1784202510, NULL, NULL, NULL, NULL, NULL, &OnCapturedToDiskCallback_t1784202510_0_0_0 } /* UnityEngine.VR.WSA.WebCam.PhotoCapture/OnCapturedToDiskCallback */,
	{ DelegatePInvokeWrapper_OnPhotoModeStartedCallback_t522077320, NULL, NULL, NULL, NULL, NULL, &OnPhotoModeStartedCallback_t522077320_0_0_0 } /* UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStartedCallback */,
	{ DelegatePInvokeWrapper_OnPhotoModeStoppedCallback_t2742193416, NULL, NULL, NULL, NULL, NULL, &OnPhotoModeStoppedCallback_t2742193416_0_0_0 } /* UnityEngine.VR.WSA.WebCam.PhotoCapture/OnPhotoModeStoppedCallback */,
	{ DelegatePInvokeWrapper_OnStartedRecordingVideoCallback_t2612745932, NULL, NULL, NULL, NULL, NULL, &OnStartedRecordingVideoCallback_t2612745932_0_0_0 } /* UnityEngine.VR.WSA.WebCam.VideoCapture/OnStartedRecordingVideoCallback */,
	{ DelegatePInvokeWrapper_OnStoppedRecordingVideoCallback_t1976525720, NULL, NULL, NULL, NULL, NULL, &OnStoppedRecordingVideoCallback_t1976525720_0_0_0 } /* UnityEngine.VR.WSA.WebCam.VideoCapture/OnStoppedRecordingVideoCallback */,
	{ DelegatePInvokeWrapper_OnVideoModeStartedCallback_t1811850718, NULL, NULL, NULL, NULL, NULL, &OnVideoModeStartedCallback_t1811850718_0_0_0 } /* UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStartedCallback */,
	{ DelegatePInvokeWrapper_OnVideoModeStoppedCallback_t1862037614, NULL, NULL, NULL, NULL, NULL, &OnVideoModeStoppedCallback_t1862037614_0_0_0 } /* UnityEngine.VR.WSA.WebCam.VideoCapture/OnVideoModeStoppedCallback */,
	{ DelegatePInvokeWrapper_OnPositionalLocatorStateChangedDelegate_t3864636813, NULL, NULL, NULL, NULL, NULL, &OnPositionalLocatorStateChangedDelegate_t3864636813_0_0_0 } /* UnityEngine.VR.WSA.WorldManager/OnPositionalLocatorStateChangedDelegate */,
	{ NULL, WaitForSeconds_t3839502067_marshal_pinvoke, WaitForSeconds_t3839502067_marshal_pinvoke_back, WaitForSeconds_t3839502067_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t3839502067_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ DelegatePInvokeWrapper_DictationCompletedDelegate_t3326551541, NULL, NULL, NULL, NULL, NULL, &DictationCompletedDelegate_t3326551541_0_0_0 } /* UnityEngine.Windows.Speech.DictationRecognizer/DictationCompletedDelegate */,
	{ DelegatePInvokeWrapper_DictationErrorHandler_t3730830311, NULL, NULL, NULL, NULL, NULL, &DictationErrorHandler_t3730830311_0_0_0 } /* UnityEngine.Windows.Speech.DictationRecognizer/DictationErrorHandler */,
	{ DelegatePInvokeWrapper_DictationHypothesisDelegate_t1495849926, NULL, NULL, NULL, NULL, NULL, &DictationHypothesisDelegate_t1495849926_0_0_0 } /* UnityEngine.Windows.Speech.DictationRecognizer/DictationHypothesisDelegate */,
	{ DelegatePInvokeWrapper_DictationResultDelegate_t1941514337, NULL, NULL, NULL, NULL, NULL, &DictationResultDelegate_t1941514337_0_0_0 } /* UnityEngine.Windows.Speech.DictationRecognizer/DictationResultDelegate */,
	{ DelegatePInvokeWrapper_ErrorDelegate_t3690655641, NULL, NULL, NULL, NULL, NULL, &ErrorDelegate_t3690655641_0_0_0 } /* UnityEngine.Windows.Speech.PhraseRecognitionSystem/ErrorDelegate */,
	{ DelegatePInvokeWrapper_StatusDelegate_t2739717665, NULL, NULL, NULL, NULL, NULL, &StatusDelegate_t2739717665_0_0_0 } /* UnityEngine.Windows.Speech.PhraseRecognitionSystem/StatusDelegate */,
	{ NULL, PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke, PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_back, PhraseRecognizedEventArgs_t3185826360_marshal_pinvoke_cleanup, NULL, NULL, &PhraseRecognizedEventArgs_t3185826360_0_0_0 } /* UnityEngine.Windows.Speech.PhraseRecognizedEventArgs */,
	{ NULL, SemanticMeaning_t2306793223_marshal_pinvoke, SemanticMeaning_t2306793223_marshal_pinvoke_back, SemanticMeaning_t2306793223_marshal_pinvoke_cleanup, NULL, NULL, &SemanticMeaning_t2306793223_0_0_0 } /* UnityEngine.Windows.Speech.SemanticMeaning */,
	{ NULL, YieldInstruction_t3462875981_marshal_pinvoke, YieldInstruction_t3462875981_marshal_pinvoke_back, YieldInstruction_t3462875981_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3462875981_0_0_0 } /* UnityEngine.YieldInstruction */,
	NULL,
};
