﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1507412803;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct  ValidationEventHandling_t2798815538  : public Il2CppObject
{
public:
	// System.Xml.XmlValidatingReaderImpl System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::reader
	XmlValidatingReaderImpl_t1507412803 * ___reader_0;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_1;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ValidationEventHandling_t2798815538, ___reader_0)); }
	inline XmlValidatingReaderImpl_t1507412803 * get_reader_0() const { return ___reader_0; }
	inline XmlValidatingReaderImpl_t1507412803 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlValidatingReaderImpl_t1507412803 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}

	inline static int32_t get_offset_of_eventHandler_1() { return static_cast<int32_t>(offsetof(ValidationEventHandling_t2798815538, ___eventHandler_1)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_1() const { return ___eventHandler_1; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_1() { return &___eventHandler_1; }
	inline void set_eventHandler_1(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandler_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
