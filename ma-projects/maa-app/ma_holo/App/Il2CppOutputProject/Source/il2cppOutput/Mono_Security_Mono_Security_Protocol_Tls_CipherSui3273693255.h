﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuiteFactory
struct  CipherSuiteFactory_t3273693255  : public Il2CppObject
{
public:

public:
};

struct CipherSuiteFactory_t3273693255_StaticFields
{
public:
	// System.Type Mono.Security.Protocol.Tls.CipherSuiteFactory::spm
	Type_t * ___spm_0;

public:
	inline static int32_t get_offset_of_spm_0() { return static_cast<int32_t>(offsetof(CipherSuiteFactory_t3273693255_StaticFields, ___spm_0)); }
	inline Type_t * get_spm_0() const { return ___spm_0; }
	inline Type_t ** get_address_of_spm_0() { return &___spm_0; }
	inline void set_spm_0(Type_t * value)
	{
		___spm_0 = value;
		Il2CppCodeGenWriteBarrier(&___spm_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
