﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Cryptography.X509Certificates.INativeCertificateHelper
struct INativeCertificateHelper_t148226263;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper
struct  X509Helper_t1751628948  : public Il2CppObject
{
public:

public:
};

struct X509Helper_t1751628948_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.INativeCertificateHelper System.Security.Cryptography.X509Certificates.X509Helper::nativeHelper
	Il2CppObject * ___nativeHelper_0;

public:
	inline static int32_t get_offset_of_nativeHelper_0() { return static_cast<int32_t>(offsetof(X509Helper_t1751628948_StaticFields, ___nativeHelper_0)); }
	inline Il2CppObject * get_nativeHelper_0() const { return ___nativeHelper_0; }
	inline Il2CppObject ** get_address_of_nativeHelper_0() { return &___nativeHelper_0; }
	inline void set_nativeHelper_0(Il2CppObject * value)
	{
		___nativeHelper_0 = value;
		Il2CppCodeGenWriteBarrier(&___nativeHelper_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
