﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Decoder3792697818.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.UTF16Decoder
struct  UTF16Decoder_t2333661544  : public Decoder_t3792697818
{
public:
	// System.Boolean System.Xml.UTF16Decoder::bigEndian
	bool ___bigEndian_2;
	// System.Int32 System.Xml.UTF16Decoder::lastByte
	int32_t ___lastByte_3;

public:
	inline static int32_t get_offset_of_bigEndian_2() { return static_cast<int32_t>(offsetof(UTF16Decoder_t2333661544, ___bigEndian_2)); }
	inline bool get_bigEndian_2() const { return ___bigEndian_2; }
	inline bool* get_address_of_bigEndian_2() { return &___bigEndian_2; }
	inline void set_bigEndian_2(bool value)
	{
		___bigEndian_2 = value;
	}

	inline static int32_t get_offset_of_lastByte_3() { return static_cast<int32_t>(offsetof(UTF16Decoder_t2333661544, ___lastByte_3)); }
	inline int32_t get_lastByte_3() const { return ___lastByte_3; }
	inline int32_t* get_address_of_lastByte_3() { return &___lastByte_3; }
	inline void set_lastByte_3(int32_t value)
	{
		___lastByte_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
