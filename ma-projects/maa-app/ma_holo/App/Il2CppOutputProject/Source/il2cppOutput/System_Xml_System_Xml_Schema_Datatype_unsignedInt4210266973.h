﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_unsignedLong3462046402.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedInt
struct  Datatype_unsignedInt_t4210266973  : public Datatype_unsignedLong_t3462046402
{
public:

public:
};

struct Datatype_unsignedInt_t4210266973_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedInt::atomicValueType
	Type_t * ___atomicValueType_100;
	// System.Type System.Xml.Schema.Datatype_unsignedInt::listValueType
	Type_t * ___listValueType_101;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedInt::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_102;

public:
	inline static int32_t get_offset_of_atomicValueType_100() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___atomicValueType_100)); }
	inline Type_t * get_atomicValueType_100() const { return ___atomicValueType_100; }
	inline Type_t ** get_address_of_atomicValueType_100() { return &___atomicValueType_100; }
	inline void set_atomicValueType_100(Type_t * value)
	{
		___atomicValueType_100 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_100, value);
	}

	inline static int32_t get_offset_of_listValueType_101() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___listValueType_101)); }
	inline Type_t * get_listValueType_101() const { return ___listValueType_101; }
	inline Type_t ** get_address_of_listValueType_101() { return &___listValueType_101; }
	inline void set_listValueType_101(Type_t * value)
	{
		___listValueType_101 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_101, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_102() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___numeric10FacetsChecker_102)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_102() const { return ___numeric10FacetsChecker_102; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_102() { return &___numeric10FacetsChecker_102; }
	inline void set_numeric10FacetsChecker_102(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_102 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_102, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
