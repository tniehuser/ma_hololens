﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_anySimpleTyp4012795865.h"

// System.Type
struct Type_t;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t192177157;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_union
struct  Datatype_union_t2515141346  : public Datatype_anySimpleType_t4012795865
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.Datatype_union::types
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___types_95;

public:
	inline static int32_t get_offset_of_types_95() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346, ___types_95)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_types_95() const { return ___types_95; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_types_95() { return &___types_95; }
	inline void set_types_95(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___types_95 = value;
		Il2CppCodeGenWriteBarrier(&___types_95, value);
	}
};

struct Datatype_union_t2515141346_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_union::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_union::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_93, value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_94, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
