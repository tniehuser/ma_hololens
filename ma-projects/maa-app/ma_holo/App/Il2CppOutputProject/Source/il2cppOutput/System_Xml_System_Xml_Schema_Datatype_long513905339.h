﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_integer404053727.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_long
struct  Datatype_long_t513905339  : public Datatype_integer_t404053727
{
public:

public:
};

struct Datatype_long_t513905339_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_long::atomicValueType
	Type_t * ___atomicValueType_96;
	// System.Type System.Xml.Schema.Datatype_long::listValueType
	Type_t * ___listValueType_97;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_long::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_98;

public:
	inline static int32_t get_offset_of_atomicValueType_96() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___atomicValueType_96)); }
	inline Type_t * get_atomicValueType_96() const { return ___atomicValueType_96; }
	inline Type_t ** get_address_of_atomicValueType_96() { return &___atomicValueType_96; }
	inline void set_atomicValueType_96(Type_t * value)
	{
		___atomicValueType_96 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_96, value);
	}

	inline static int32_t get_offset_of_listValueType_97() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___listValueType_97)); }
	inline Type_t * get_listValueType_97() const { return ___listValueType_97; }
	inline Type_t ** get_address_of_listValueType_97() { return &___listValueType_97; }
	inline void set_listValueType_97(Type_t * value)
	{
		___listValueType_97 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_97, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_98() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___numeric10FacetsChecker_98)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_98() const { return ___numeric10FacetsChecker_98; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_98() { return &___numeric10FacetsChecker_98; }
	inline void set_numeric10FacetsChecker_98(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_98 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_98, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
