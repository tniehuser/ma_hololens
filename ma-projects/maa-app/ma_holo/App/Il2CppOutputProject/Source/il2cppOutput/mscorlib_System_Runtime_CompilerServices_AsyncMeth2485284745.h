﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t3152578875;
// System.Action
struct Action_t3226471752;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t296893742;
// System.Threading.WaitCallback
struct WaitCallback_t2798937288;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2485284745 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	Il2CppObject * ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t3226471752 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745, ___m_stateMachine_0)); }
	inline Il2CppObject * get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline Il2CppObject ** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(Il2CppObject * value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_stateMachine_0, value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745, ___m_defaultContextAction_1)); }
	inline Action_t3226471752 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t3226471752 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t3226471752 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_defaultContextAction_1, value);
	}
};

struct AsyncMethodBuilderCore_t2485284745_StaticFields
{
public:
	// System.Threading.SendOrPostCallback System.Runtime.CompilerServices.AsyncMethodBuilderCore::<>f__am$cache0
	SendOrPostCallback_t296893742 * ___U3CU3Ef__amU24cache0_2;
	// System.Threading.WaitCallback System.Runtime.CompilerServices.AsyncMethodBuilderCore::<>f__am$cache1
	WaitCallback_t2798937288 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline SendOrPostCallback_t296893742 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline SendOrPostCallback_t296893742 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(SendOrPostCallback_t296893742 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline WaitCallback_t2798937288 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline WaitCallback_t2798937288 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(WaitCallback_t2798937288 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2485284745_marshaled_pinvoke
{
	Il2CppObject * ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2485284745_marshaled_com
{
	Il2CppObject * ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
