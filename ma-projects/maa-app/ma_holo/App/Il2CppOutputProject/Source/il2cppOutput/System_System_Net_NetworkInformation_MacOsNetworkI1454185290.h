﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.MacOsNetworkInterface
struct  MacOsNetworkInterface_t1454185290  : public UnixNetworkInterface_t1000704527
{
public:
	// System.UInt32 System.Net.NetworkInformation.MacOsNetworkInterface::_ifa_flags
	uint32_t ____ifa_flags_5;

public:
	inline static int32_t get_offset_of__ifa_flags_5() { return static_cast<int32_t>(offsetof(MacOsNetworkInterface_t1454185290, ____ifa_flags_5)); }
	inline uint32_t get__ifa_flags_5() const { return ____ifa_flags_5; }
	inline uint32_t* get_address_of__ifa_flags_5() { return &____ifa_flags_5; }
	inline void set__ifa_flags_5(uint32_t value)
	{
		____ifa_flags_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
