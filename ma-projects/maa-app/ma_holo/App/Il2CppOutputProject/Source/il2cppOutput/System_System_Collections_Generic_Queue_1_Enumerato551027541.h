﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_221307626.h"

// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.IOSelectorJob>>
struct Queue_1_t40964461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.IOSelectorJob>>
struct  Enumerator_t551027541 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::_q
	Queue_1_t40964461 * ____q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.Queue`1/Enumerator::_currentElement
	KeyValuePair_2_t221307626  ____currentElement_3;

public:
	inline static int32_t get_offset_of__q_0() { return static_cast<int32_t>(offsetof(Enumerator_t551027541, ____q_0)); }
	inline Queue_1_t40964461 * get__q_0() const { return ____q_0; }
	inline Queue_1_t40964461 ** get_address_of__q_0() { return &____q_0; }
	inline void set__q_0(Queue_1_t40964461 * value)
	{
		____q_0 = value;
		Il2CppCodeGenWriteBarrier(&____q_0, value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_t551027541, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t551027541, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_t551027541, ____currentElement_3)); }
	inline KeyValuePair_2_t221307626  get__currentElement_3() const { return ____currentElement_3; }
	inline KeyValuePair_2_t221307626 * get_address_of__currentElement_3() { return &____currentElement_3; }
	inline void set__currentElement_3(KeyValuePair_2_t221307626  value)
	{
		____currentElement_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
