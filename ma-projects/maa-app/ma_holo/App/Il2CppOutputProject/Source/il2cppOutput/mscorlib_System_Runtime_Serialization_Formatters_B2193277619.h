﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectMapInfo
struct  ObjectMapInfo_t2193277619  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectMapInfo::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectMapInfo::numMembers
	int32_t ___numMembers_1;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.ObjectMapInfo::memberNames
	StringU5BU5D_t1642385972* ___memberNames_2;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.ObjectMapInfo::memberTypes
	TypeU5BU5D_t1664964607* ___memberTypes_3;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(ObjectMapInfo_t2193277619, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_numMembers_1() { return static_cast<int32_t>(offsetof(ObjectMapInfo_t2193277619, ___numMembers_1)); }
	inline int32_t get_numMembers_1() const { return ___numMembers_1; }
	inline int32_t* get_address_of_numMembers_1() { return &___numMembers_1; }
	inline void set_numMembers_1(int32_t value)
	{
		___numMembers_1 = value;
	}

	inline static int32_t get_offset_of_memberNames_2() { return static_cast<int32_t>(offsetof(ObjectMapInfo_t2193277619, ___memberNames_2)); }
	inline StringU5BU5D_t1642385972* get_memberNames_2() const { return ___memberNames_2; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_2() { return &___memberNames_2; }
	inline void set_memberNames_2(StringU5BU5D_t1642385972* value)
	{
		___memberNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_2, value);
	}

	inline static int32_t get_offset_of_memberTypes_3() { return static_cast<int32_t>(offsetof(ObjectMapInfo_t2193277619, ___memberTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_memberTypes_3() const { return ___memberTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_memberTypes_3() { return &___memberTypes_3; }
	inline void set_memberTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___memberTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
