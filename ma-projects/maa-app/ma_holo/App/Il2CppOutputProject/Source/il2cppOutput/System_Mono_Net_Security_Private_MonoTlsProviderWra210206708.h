﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t823784021;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoTlsProviderWrapper
struct  MonoTlsProviderWrapper_t210206708  : public Il2CppObject
{
public:
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.MonoTlsProviderWrapper::provider
	MonoTlsProvider_t823784021 * ___provider_0;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderWrapper_t210206708, ___provider_0)); }
	inline MonoTlsProvider_t823784021 * get_provider_0() const { return ___provider_0; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(MonoTlsProvider_t823784021 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier(&___provider_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
