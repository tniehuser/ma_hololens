﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Encoding663144255.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.UnicodeEncoding
struct  UnicodeEncoding_t4081757012  : public Encoding_t663144255
{
public:
	// System.Boolean System.Text.UnicodeEncoding::isThrowException
	bool ___isThrowException_15;
	// System.Boolean System.Text.UnicodeEncoding::bigEndian
	bool ___bigEndian_16;
	// System.Boolean System.Text.UnicodeEncoding::byteOrderMark
	bool ___byteOrderMark_17;

public:
	inline static int32_t get_offset_of_isThrowException_15() { return static_cast<int32_t>(offsetof(UnicodeEncoding_t4081757012, ___isThrowException_15)); }
	inline bool get_isThrowException_15() const { return ___isThrowException_15; }
	inline bool* get_address_of_isThrowException_15() { return &___isThrowException_15; }
	inline void set_isThrowException_15(bool value)
	{
		___isThrowException_15 = value;
	}

	inline static int32_t get_offset_of_bigEndian_16() { return static_cast<int32_t>(offsetof(UnicodeEncoding_t4081757012, ___bigEndian_16)); }
	inline bool get_bigEndian_16() const { return ___bigEndian_16; }
	inline bool* get_address_of_bigEndian_16() { return &___bigEndian_16; }
	inline void set_bigEndian_16(bool value)
	{
		___bigEndian_16 = value;
	}

	inline static int32_t get_offset_of_byteOrderMark_17() { return static_cast<int32_t>(offsetof(UnicodeEncoding_t4081757012, ___byteOrderMark_17)); }
	inline bool get_byteOrderMark_17() const { return ___byteOrderMark_17; }
	inline bool* get_address_of_byteOrderMark_17() { return &___byteOrderMark_17; }
	inline void set_byteOrderMark_17(bool value)
	{
		___byteOrderMark_17 = value;
	}
};

struct UnicodeEncoding_t4081757012_StaticFields
{
public:
	// System.UInt64 System.Text.UnicodeEncoding::highLowPatternMask
	uint64_t ___highLowPatternMask_18;

public:
	inline static int32_t get_offset_of_highLowPatternMask_18() { return static_cast<int32_t>(offsetof(UnicodeEncoding_t4081757012_StaticFields, ___highLowPatternMask_18)); }
	inline uint64_t get_highLowPatternMask_18() const { return ___highLowPatternMask_18; }
	inline uint64_t* get_address_of_highLowPatternMask_18() { return &___highLowPatternMask_18; }
	inline void set_highLowPatternMask_18(uint64_t value)
	{
		___highLowPatternMask_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
