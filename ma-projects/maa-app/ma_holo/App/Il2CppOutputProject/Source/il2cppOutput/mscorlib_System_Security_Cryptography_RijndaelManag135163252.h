﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_CipherMode162592484.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode3032142640.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana4113989319.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RijndaelManagedTransform
struct  RijndaelManagedTransform_t135163252  : public Il2CppObject
{
public:
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.RijndaelManagedTransform::m_cipherMode
	int32_t ___m_cipherMode_0;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.RijndaelManagedTransform::m_paddingValue
	int32_t ___m_paddingValue_1;
	// System.Security.Cryptography.RijndaelManagedTransformMode System.Security.Cryptography.RijndaelManagedTransform::m_transformMode
	int32_t ___m_transformMode_2;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_blockSizeBits
	int32_t ___m_blockSizeBits_3;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_blockSizeBytes
	int32_t ___m_blockSizeBytes_4;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_inputBlockSize
	int32_t ___m_inputBlockSize_5;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_outputBlockSize
	int32_t ___m_outputBlockSize_6;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_encryptKeyExpansion
	Int32U5BU5D_t3030399641* ___m_encryptKeyExpansion_7;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_decryptKeyExpansion
	Int32U5BU5D_t3030399641* ___m_decryptKeyExpansion_8;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_Nr
	int32_t ___m_Nr_9;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_Nb
	int32_t ___m_Nb_10;
	// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::m_Nk
	int32_t ___m_Nk_11;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_encryptindex
	Int32U5BU5D_t3030399641* ___m_encryptindex_12;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_decryptindex
	Int32U5BU5D_t3030399641* ___m_decryptindex_13;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_IV
	Int32U5BU5D_t3030399641* ___m_IV_14;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::m_lastBlockBuffer
	Int32U5BU5D_t3030399641* ___m_lastBlockBuffer_15;
	// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::m_depadBuffer
	ByteU5BU5D_t3397334013* ___m_depadBuffer_16;
	// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::m_shiftRegister
	ByteU5BU5D_t3397334013* ___m_shiftRegister_17;

public:
	inline static int32_t get_offset_of_m_cipherMode_0() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_cipherMode_0)); }
	inline int32_t get_m_cipherMode_0() const { return ___m_cipherMode_0; }
	inline int32_t* get_address_of_m_cipherMode_0() { return &___m_cipherMode_0; }
	inline void set_m_cipherMode_0(int32_t value)
	{
		___m_cipherMode_0 = value;
	}

	inline static int32_t get_offset_of_m_paddingValue_1() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_paddingValue_1)); }
	inline int32_t get_m_paddingValue_1() const { return ___m_paddingValue_1; }
	inline int32_t* get_address_of_m_paddingValue_1() { return &___m_paddingValue_1; }
	inline void set_m_paddingValue_1(int32_t value)
	{
		___m_paddingValue_1 = value;
	}

	inline static int32_t get_offset_of_m_transformMode_2() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_transformMode_2)); }
	inline int32_t get_m_transformMode_2() const { return ___m_transformMode_2; }
	inline int32_t* get_address_of_m_transformMode_2() { return &___m_transformMode_2; }
	inline void set_m_transformMode_2(int32_t value)
	{
		___m_transformMode_2 = value;
	}

	inline static int32_t get_offset_of_m_blockSizeBits_3() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_blockSizeBits_3)); }
	inline int32_t get_m_blockSizeBits_3() const { return ___m_blockSizeBits_3; }
	inline int32_t* get_address_of_m_blockSizeBits_3() { return &___m_blockSizeBits_3; }
	inline void set_m_blockSizeBits_3(int32_t value)
	{
		___m_blockSizeBits_3 = value;
	}

	inline static int32_t get_offset_of_m_blockSizeBytes_4() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_blockSizeBytes_4)); }
	inline int32_t get_m_blockSizeBytes_4() const { return ___m_blockSizeBytes_4; }
	inline int32_t* get_address_of_m_blockSizeBytes_4() { return &___m_blockSizeBytes_4; }
	inline void set_m_blockSizeBytes_4(int32_t value)
	{
		___m_blockSizeBytes_4 = value;
	}

	inline static int32_t get_offset_of_m_inputBlockSize_5() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_inputBlockSize_5)); }
	inline int32_t get_m_inputBlockSize_5() const { return ___m_inputBlockSize_5; }
	inline int32_t* get_address_of_m_inputBlockSize_5() { return &___m_inputBlockSize_5; }
	inline void set_m_inputBlockSize_5(int32_t value)
	{
		___m_inputBlockSize_5 = value;
	}

	inline static int32_t get_offset_of_m_outputBlockSize_6() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_outputBlockSize_6)); }
	inline int32_t get_m_outputBlockSize_6() const { return ___m_outputBlockSize_6; }
	inline int32_t* get_address_of_m_outputBlockSize_6() { return &___m_outputBlockSize_6; }
	inline void set_m_outputBlockSize_6(int32_t value)
	{
		___m_outputBlockSize_6 = value;
	}

	inline static int32_t get_offset_of_m_encryptKeyExpansion_7() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_encryptKeyExpansion_7)); }
	inline Int32U5BU5D_t3030399641* get_m_encryptKeyExpansion_7() const { return ___m_encryptKeyExpansion_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_encryptKeyExpansion_7() { return &___m_encryptKeyExpansion_7; }
	inline void set_m_encryptKeyExpansion_7(Int32U5BU5D_t3030399641* value)
	{
		___m_encryptKeyExpansion_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_encryptKeyExpansion_7, value);
	}

	inline static int32_t get_offset_of_m_decryptKeyExpansion_8() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_decryptKeyExpansion_8)); }
	inline Int32U5BU5D_t3030399641* get_m_decryptKeyExpansion_8() const { return ___m_decryptKeyExpansion_8; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_decryptKeyExpansion_8() { return &___m_decryptKeyExpansion_8; }
	inline void set_m_decryptKeyExpansion_8(Int32U5BU5D_t3030399641* value)
	{
		___m_decryptKeyExpansion_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_decryptKeyExpansion_8, value);
	}

	inline static int32_t get_offset_of_m_Nr_9() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_Nr_9)); }
	inline int32_t get_m_Nr_9() const { return ___m_Nr_9; }
	inline int32_t* get_address_of_m_Nr_9() { return &___m_Nr_9; }
	inline void set_m_Nr_9(int32_t value)
	{
		___m_Nr_9 = value;
	}

	inline static int32_t get_offset_of_m_Nb_10() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_Nb_10)); }
	inline int32_t get_m_Nb_10() const { return ___m_Nb_10; }
	inline int32_t* get_address_of_m_Nb_10() { return &___m_Nb_10; }
	inline void set_m_Nb_10(int32_t value)
	{
		___m_Nb_10 = value;
	}

	inline static int32_t get_offset_of_m_Nk_11() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_Nk_11)); }
	inline int32_t get_m_Nk_11() const { return ___m_Nk_11; }
	inline int32_t* get_address_of_m_Nk_11() { return &___m_Nk_11; }
	inline void set_m_Nk_11(int32_t value)
	{
		___m_Nk_11 = value;
	}

	inline static int32_t get_offset_of_m_encryptindex_12() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_encryptindex_12)); }
	inline Int32U5BU5D_t3030399641* get_m_encryptindex_12() const { return ___m_encryptindex_12; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_encryptindex_12() { return &___m_encryptindex_12; }
	inline void set_m_encryptindex_12(Int32U5BU5D_t3030399641* value)
	{
		___m_encryptindex_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_encryptindex_12, value);
	}

	inline static int32_t get_offset_of_m_decryptindex_13() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_decryptindex_13)); }
	inline Int32U5BU5D_t3030399641* get_m_decryptindex_13() const { return ___m_decryptindex_13; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_decryptindex_13() { return &___m_decryptindex_13; }
	inline void set_m_decryptindex_13(Int32U5BU5D_t3030399641* value)
	{
		___m_decryptindex_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_decryptindex_13, value);
	}

	inline static int32_t get_offset_of_m_IV_14() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_IV_14)); }
	inline Int32U5BU5D_t3030399641* get_m_IV_14() const { return ___m_IV_14; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_IV_14() { return &___m_IV_14; }
	inline void set_m_IV_14(Int32U5BU5D_t3030399641* value)
	{
		___m_IV_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_IV_14, value);
	}

	inline static int32_t get_offset_of_m_lastBlockBuffer_15() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_lastBlockBuffer_15)); }
	inline Int32U5BU5D_t3030399641* get_m_lastBlockBuffer_15() const { return ___m_lastBlockBuffer_15; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_lastBlockBuffer_15() { return &___m_lastBlockBuffer_15; }
	inline void set_m_lastBlockBuffer_15(Int32U5BU5D_t3030399641* value)
	{
		___m_lastBlockBuffer_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_lastBlockBuffer_15, value);
	}

	inline static int32_t get_offset_of_m_depadBuffer_16() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_depadBuffer_16)); }
	inline ByteU5BU5D_t3397334013* get_m_depadBuffer_16() const { return ___m_depadBuffer_16; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_depadBuffer_16() { return &___m_depadBuffer_16; }
	inline void set_m_depadBuffer_16(ByteU5BU5D_t3397334013* value)
	{
		___m_depadBuffer_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_depadBuffer_16, value);
	}

	inline static int32_t get_offset_of_m_shiftRegister_17() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252, ___m_shiftRegister_17)); }
	inline ByteU5BU5D_t3397334013* get_m_shiftRegister_17() const { return ___m_shiftRegister_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_m_shiftRegister_17() { return &___m_shiftRegister_17; }
	inline void set_m_shiftRegister_17(ByteU5BU5D_t3397334013* value)
	{
		___m_shiftRegister_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_shiftRegister_17, value);
	}
};

struct RijndaelManagedTransform_t135163252_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::s_Sbox
	ByteU5BU5D_t3397334013* ___s_Sbox_18;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::s_Rcon
	Int32U5BU5D_t3030399641* ___s_Rcon_19;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::s_T
	Int32U5BU5D_t3030399641* ___s_T_20;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::s_TF
	Int32U5BU5D_t3030399641* ___s_TF_21;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::s_iT
	Int32U5BU5D_t3030399641* ___s_iT_22;
	// System.Int32[] System.Security.Cryptography.RijndaelManagedTransform::s_iTF
	Int32U5BU5D_t3030399641* ___s_iTF_23;

public:
	inline static int32_t get_offset_of_s_Sbox_18() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_Sbox_18)); }
	inline ByteU5BU5D_t3397334013* get_s_Sbox_18() const { return ___s_Sbox_18; }
	inline ByteU5BU5D_t3397334013** get_address_of_s_Sbox_18() { return &___s_Sbox_18; }
	inline void set_s_Sbox_18(ByteU5BU5D_t3397334013* value)
	{
		___s_Sbox_18 = value;
		Il2CppCodeGenWriteBarrier(&___s_Sbox_18, value);
	}

	inline static int32_t get_offset_of_s_Rcon_19() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_Rcon_19)); }
	inline Int32U5BU5D_t3030399641* get_s_Rcon_19() const { return ___s_Rcon_19; }
	inline Int32U5BU5D_t3030399641** get_address_of_s_Rcon_19() { return &___s_Rcon_19; }
	inline void set_s_Rcon_19(Int32U5BU5D_t3030399641* value)
	{
		___s_Rcon_19 = value;
		Il2CppCodeGenWriteBarrier(&___s_Rcon_19, value);
	}

	inline static int32_t get_offset_of_s_T_20() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_T_20)); }
	inline Int32U5BU5D_t3030399641* get_s_T_20() const { return ___s_T_20; }
	inline Int32U5BU5D_t3030399641** get_address_of_s_T_20() { return &___s_T_20; }
	inline void set_s_T_20(Int32U5BU5D_t3030399641* value)
	{
		___s_T_20 = value;
		Il2CppCodeGenWriteBarrier(&___s_T_20, value);
	}

	inline static int32_t get_offset_of_s_TF_21() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_TF_21)); }
	inline Int32U5BU5D_t3030399641* get_s_TF_21() const { return ___s_TF_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_s_TF_21() { return &___s_TF_21; }
	inline void set_s_TF_21(Int32U5BU5D_t3030399641* value)
	{
		___s_TF_21 = value;
		Il2CppCodeGenWriteBarrier(&___s_TF_21, value);
	}

	inline static int32_t get_offset_of_s_iT_22() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_iT_22)); }
	inline Int32U5BU5D_t3030399641* get_s_iT_22() const { return ___s_iT_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_s_iT_22() { return &___s_iT_22; }
	inline void set_s_iT_22(Int32U5BU5D_t3030399641* value)
	{
		___s_iT_22 = value;
		Il2CppCodeGenWriteBarrier(&___s_iT_22, value);
	}

	inline static int32_t get_offset_of_s_iTF_23() { return static_cast<int32_t>(offsetof(RijndaelManagedTransform_t135163252_StaticFields, ___s_iTF_23)); }
	inline Int32U5BU5D_t3030399641* get_s_iTF_23() const { return ___s_iTF_23; }
	inline Int32U5BU5D_t3030399641** get_address_of_s_iTF_23() { return &___s_iTF_23; }
	inline void set_s_iTF_23(Int32U5BU5D_t3030399641* value)
	{
		___s_iTF_23 = value;
		Il2CppCodeGenWriteBarrier(&___s_iTF_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
