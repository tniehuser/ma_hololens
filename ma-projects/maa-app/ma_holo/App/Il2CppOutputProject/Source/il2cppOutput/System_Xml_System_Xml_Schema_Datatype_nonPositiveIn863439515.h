﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_integer404053727.h"

// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_nonPositiveInteger
struct  Datatype_nonPositiveInteger_t863439515  : public Datatype_integer_t404053727
{
public:

public:
};

struct Datatype_nonPositiveInteger_t863439515_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_nonPositiveInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_96;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_96() { return static_cast<int32_t>(offsetof(Datatype_nonPositiveInteger_t863439515_StaticFields, ___numeric10FacetsChecker_96)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_96() const { return ___numeric10FacetsChecker_96; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_96() { return &___numeric10FacetsChecker_96; }
	inline void set_numeric10FacetsChecker_96(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_96 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_96, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
