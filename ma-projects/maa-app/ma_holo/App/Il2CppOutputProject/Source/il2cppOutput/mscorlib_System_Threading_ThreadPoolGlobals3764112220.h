﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Threading.ThreadPoolWorkQueue
struct ThreadPoolWorkQueue_t673814512;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolGlobals
struct  ThreadPoolGlobals_t3764112220  : public Il2CppObject
{
public:

public:
};

struct ThreadPoolGlobals_t3764112220_StaticFields
{
public:
	// System.UInt32 System.Threading.ThreadPoolGlobals::tpQuantum
	uint32_t ___tpQuantum_0;
	// System.Int32 System.Threading.ThreadPoolGlobals::processorCount
	int32_t ___processorCount_1;
	// System.Boolean System.Threading.ThreadPoolGlobals::tpHosted
	bool ___tpHosted_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolGlobals::vmTpInitialized
	bool ___vmTpInitialized_3;
	// System.Boolean System.Threading.ThreadPoolGlobals::enableWorkerTracking
	bool ___enableWorkerTracking_4;
	// System.Threading.ThreadPoolWorkQueue System.Threading.ThreadPoolGlobals::workQueue
	ThreadPoolWorkQueue_t673814512 * ___workQueue_5;

public:
	inline static int32_t get_offset_of_tpQuantum_0() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___tpQuantum_0)); }
	inline uint32_t get_tpQuantum_0() const { return ___tpQuantum_0; }
	inline uint32_t* get_address_of_tpQuantum_0() { return &___tpQuantum_0; }
	inline void set_tpQuantum_0(uint32_t value)
	{
		___tpQuantum_0 = value;
	}

	inline static int32_t get_offset_of_processorCount_1() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___processorCount_1)); }
	inline int32_t get_processorCount_1() const { return ___processorCount_1; }
	inline int32_t* get_address_of_processorCount_1() { return &___processorCount_1; }
	inline void set_processorCount_1(int32_t value)
	{
		___processorCount_1 = value;
	}

	inline static int32_t get_offset_of_tpHosted_2() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___tpHosted_2)); }
	inline bool get_tpHosted_2() const { return ___tpHosted_2; }
	inline bool* get_address_of_tpHosted_2() { return &___tpHosted_2; }
	inline void set_tpHosted_2(bool value)
	{
		___tpHosted_2 = value;
	}

	inline static int32_t get_offset_of_vmTpInitialized_3() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___vmTpInitialized_3)); }
	inline bool get_vmTpInitialized_3() const { return ___vmTpInitialized_3; }
	inline bool* get_address_of_vmTpInitialized_3() { return &___vmTpInitialized_3; }
	inline void set_vmTpInitialized_3(bool value)
	{
		___vmTpInitialized_3 = value;
	}

	inline static int32_t get_offset_of_enableWorkerTracking_4() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___enableWorkerTracking_4)); }
	inline bool get_enableWorkerTracking_4() const { return ___enableWorkerTracking_4; }
	inline bool* get_address_of_enableWorkerTracking_4() { return &___enableWorkerTracking_4; }
	inline void set_enableWorkerTracking_4(bool value)
	{
		___enableWorkerTracking_4 = value;
	}

	inline static int32_t get_offset_of_workQueue_5() { return static_cast<int32_t>(offsetof(ThreadPoolGlobals_t3764112220_StaticFields, ___workQueue_5)); }
	inline ThreadPoolWorkQueue_t673814512 * get_workQueue_5() const { return ___workQueue_5; }
	inline ThreadPoolWorkQueue_t673814512 ** get_address_of_workQueue_5() { return &___workQueue_5; }
	inline void set_workQueue_5(ThreadPoolWorkQueue_t673814512 * value)
	{
		___workQueue_5 = value;
		Il2CppCodeGenWriteBarrier(&___workQueue_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
