﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.ThreadPoolWorkQueueThreadLocals
struct ThreadPoolWorkQueueThreadLocals_t3127465848;
// System.Threading.ThreadPoolWorkQueue
struct ThreadPoolWorkQueue_t673814512;
// System.Threading.ThreadPoolWorkQueue/WorkStealingQueue
struct WorkStealingQueue_t2897576067;
// System.Random
struct Random_t1044426839;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueueThreadLocals
struct  ThreadPoolWorkQueueThreadLocals_t3127465848  : public Il2CppObject
{
public:
	// System.Threading.ThreadPoolWorkQueue System.Threading.ThreadPoolWorkQueueThreadLocals::workQueue
	ThreadPoolWorkQueue_t673814512 * ___workQueue_1;
	// System.Threading.ThreadPoolWorkQueue/WorkStealingQueue System.Threading.ThreadPoolWorkQueueThreadLocals::workStealingQueue
	WorkStealingQueue_t2897576067 * ___workStealingQueue_2;
	// System.Random System.Threading.ThreadPoolWorkQueueThreadLocals::random
	Random_t1044426839 * ___random_3;

public:
	inline static int32_t get_offset_of_workQueue_1() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueueThreadLocals_t3127465848, ___workQueue_1)); }
	inline ThreadPoolWorkQueue_t673814512 * get_workQueue_1() const { return ___workQueue_1; }
	inline ThreadPoolWorkQueue_t673814512 ** get_address_of_workQueue_1() { return &___workQueue_1; }
	inline void set_workQueue_1(ThreadPoolWorkQueue_t673814512 * value)
	{
		___workQueue_1 = value;
		Il2CppCodeGenWriteBarrier(&___workQueue_1, value);
	}

	inline static int32_t get_offset_of_workStealingQueue_2() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueueThreadLocals_t3127465848, ___workStealingQueue_2)); }
	inline WorkStealingQueue_t2897576067 * get_workStealingQueue_2() const { return ___workStealingQueue_2; }
	inline WorkStealingQueue_t2897576067 ** get_address_of_workStealingQueue_2() { return &___workStealingQueue_2; }
	inline void set_workStealingQueue_2(WorkStealingQueue_t2897576067 * value)
	{
		___workStealingQueue_2 = value;
		Il2CppCodeGenWriteBarrier(&___workStealingQueue_2, value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueueThreadLocals_t3127465848, ___random_3)); }
	inline Random_t1044426839 * get_random_3() const { return ___random_3; }
	inline Random_t1044426839 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(Random_t1044426839 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier(&___random_3, value);
	}
};

struct ThreadPoolWorkQueueThreadLocals_t3127465848_ThreadStaticFields
{
public:
	// System.Threading.ThreadPoolWorkQueueThreadLocals System.Threading.ThreadPoolWorkQueueThreadLocals::threadLocals
	ThreadPoolWorkQueueThreadLocals_t3127465848 * ___threadLocals_0;

public:
	inline static int32_t get_offset_of_threadLocals_0() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueueThreadLocals_t3127465848_ThreadStaticFields, ___threadLocals_0)); }
	inline ThreadPoolWorkQueueThreadLocals_t3127465848 * get_threadLocals_0() const { return ___threadLocals_0; }
	inline ThreadPoolWorkQueueThreadLocals_t3127465848 ** get_address_of_threadLocals_0() { return &___threadLocals_0; }
	inline void set_threadLocals_0(ThreadPoolWorkQueueThreadLocals_t3127465848 * value)
	{
		___threadLocals_0 = value;
		Il2CppCodeGenWriteBarrier(&___threadLocals_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
