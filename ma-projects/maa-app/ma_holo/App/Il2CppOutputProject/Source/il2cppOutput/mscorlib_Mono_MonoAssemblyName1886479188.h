﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_Mono_MonoAssemblyName_U3Cpublic_key_token1384907566.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.MonoAssemblyName
struct  MonoAssemblyName_t1886479188 
{
public:
	// System.IntPtr Mono.MonoAssemblyName::name
	IntPtr_t ___name_0;
	// System.IntPtr Mono.MonoAssemblyName::culture
	IntPtr_t ___culture_1;
	// System.IntPtr Mono.MonoAssemblyName::hash_value
	IntPtr_t ___hash_value_2;
	// System.IntPtr Mono.MonoAssemblyName::public_key
	IntPtr_t ___public_key_3;
	// Mono.MonoAssemblyName/<public_key_token>__FixedBuffer5 Mono.MonoAssemblyName::public_key_token
	U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566  ___public_key_token_4;
	// System.UInt32 Mono.MonoAssemblyName::hash_alg
	uint32_t ___hash_alg_5;
	// System.UInt32 Mono.MonoAssemblyName::hash_len
	uint32_t ___hash_len_6;
	// System.UInt32 Mono.MonoAssemblyName::flags
	uint32_t ___flags_7;
	// System.UInt16 Mono.MonoAssemblyName::major
	uint16_t ___major_8;
	// System.UInt16 Mono.MonoAssemblyName::minor
	uint16_t ___minor_9;
	// System.UInt16 Mono.MonoAssemblyName::build
	uint16_t ___build_10;
	// System.UInt16 Mono.MonoAssemblyName::revision
	uint16_t ___revision_11;
	// System.UInt16 Mono.MonoAssemblyName::arch
	uint16_t ___arch_12;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___name_0)); }
	inline IntPtr_t get_name_0() const { return ___name_0; }
	inline IntPtr_t* get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(IntPtr_t value)
	{
		___name_0 = value;
	}

	inline static int32_t get_offset_of_culture_1() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___culture_1)); }
	inline IntPtr_t get_culture_1() const { return ___culture_1; }
	inline IntPtr_t* get_address_of_culture_1() { return &___culture_1; }
	inline void set_culture_1(IntPtr_t value)
	{
		___culture_1 = value;
	}

	inline static int32_t get_offset_of_hash_value_2() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___hash_value_2)); }
	inline IntPtr_t get_hash_value_2() const { return ___hash_value_2; }
	inline IntPtr_t* get_address_of_hash_value_2() { return &___hash_value_2; }
	inline void set_hash_value_2(IntPtr_t value)
	{
		___hash_value_2 = value;
	}

	inline static int32_t get_offset_of_public_key_3() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___public_key_3)); }
	inline IntPtr_t get_public_key_3() const { return ___public_key_3; }
	inline IntPtr_t* get_address_of_public_key_3() { return &___public_key_3; }
	inline void set_public_key_3(IntPtr_t value)
	{
		___public_key_3 = value;
	}

	inline static int32_t get_offset_of_public_key_token_4() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___public_key_token_4)); }
	inline U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566  get_public_key_token_4() const { return ___public_key_token_4; }
	inline U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566 * get_address_of_public_key_token_4() { return &___public_key_token_4; }
	inline void set_public_key_token_4(U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566  value)
	{
		___public_key_token_4 = value;
	}

	inline static int32_t get_offset_of_hash_alg_5() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___hash_alg_5)); }
	inline uint32_t get_hash_alg_5() const { return ___hash_alg_5; }
	inline uint32_t* get_address_of_hash_alg_5() { return &___hash_alg_5; }
	inline void set_hash_alg_5(uint32_t value)
	{
		___hash_alg_5 = value;
	}

	inline static int32_t get_offset_of_hash_len_6() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___hash_len_6)); }
	inline uint32_t get_hash_len_6() const { return ___hash_len_6; }
	inline uint32_t* get_address_of_hash_len_6() { return &___hash_len_6; }
	inline void set_hash_len_6(uint32_t value)
	{
		___hash_len_6 = value;
	}

	inline static int32_t get_offset_of_flags_7() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___flags_7)); }
	inline uint32_t get_flags_7() const { return ___flags_7; }
	inline uint32_t* get_address_of_flags_7() { return &___flags_7; }
	inline void set_flags_7(uint32_t value)
	{
		___flags_7 = value;
	}

	inline static int32_t get_offset_of_major_8() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___major_8)); }
	inline uint16_t get_major_8() const { return ___major_8; }
	inline uint16_t* get_address_of_major_8() { return &___major_8; }
	inline void set_major_8(uint16_t value)
	{
		___major_8 = value;
	}

	inline static int32_t get_offset_of_minor_9() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___minor_9)); }
	inline uint16_t get_minor_9() const { return ___minor_9; }
	inline uint16_t* get_address_of_minor_9() { return &___minor_9; }
	inline void set_minor_9(uint16_t value)
	{
		___minor_9 = value;
	}

	inline static int32_t get_offset_of_build_10() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___build_10)); }
	inline uint16_t get_build_10() const { return ___build_10; }
	inline uint16_t* get_address_of_build_10() { return &___build_10; }
	inline void set_build_10(uint16_t value)
	{
		___build_10 = value;
	}

	inline static int32_t get_offset_of_revision_11() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___revision_11)); }
	inline uint16_t get_revision_11() const { return ___revision_11; }
	inline uint16_t* get_address_of_revision_11() { return &___revision_11; }
	inline void set_revision_11(uint16_t value)
	{
		___revision_11 = value;
	}

	inline static int32_t get_offset_of_arch_12() { return static_cast<int32_t>(offsetof(MonoAssemblyName_t1886479188, ___arch_12)); }
	inline uint16_t get_arch_12() const { return ___arch_12; }
	inline uint16_t* get_address_of_arch_12() { return &___arch_12; }
	inline void set_arch_12(uint16_t value)
	{
		___arch_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
