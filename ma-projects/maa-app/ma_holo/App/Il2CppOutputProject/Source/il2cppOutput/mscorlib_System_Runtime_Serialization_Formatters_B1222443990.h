﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2877339122.h"

// System.Runtime.Serialization.Formatters.Binary.ObjectReader
struct ObjectReader_t1476095226;
// System.IO.Stream
struct Stream_t3255436806;
// System.Runtime.Serialization.Formatters.Binary.SizedArray
struct SizedArray_t2151058854;
// System.Runtime.Serialization.Formatters.Binary.SerStack
struct SerStack_t3886188184;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.Formatters.Binary.ParseRecord
struct ParseRecord_t2674254118;
// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo
struct BinaryAssemblyInfo_t316080507;
// System.IO.BinaryReader
struct BinaryReader_t2491843768;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Runtime.Serialization.Formatters.Binary.BinaryObject
struct BinaryObject_t763496928;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap
struct BinaryObjectWithMap_t2163984170;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped
struct BinaryObjectWithMapTyped_t3348677078;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectString
struct BinaryObjectString_t2307902425;
// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainString
struct BinaryCrossAppDomainString_t1989374585;
// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped
struct MemberPrimitiveTyped_t3733510915;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped
struct MemberPrimitiveUnTyped_t3584398440;
// System.Runtime.Serialization.Formatters.Binary.MemberReference
struct MemberReference_t1102219583;
// System.Runtime.Serialization.Formatters.Binary.ObjectNull
struct ObjectNull_t1089955196;
// System.Runtime.Serialization.Formatters.Binary.MessageEnd
struct MessageEnd_t3819299862;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.__BinaryParser
struct  __BinaryParser_t1222443990  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.ObjectReader System.Runtime.Serialization.Formatters.Binary.__BinaryParser::objectReader
	ObjectReader_t1476095226 * ___objectReader_0;
	// System.IO.Stream System.Runtime.Serialization.Formatters.Binary.__BinaryParser::input
	Stream_t3255436806 * ___input_1;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.__BinaryParser::topId
	int64_t ___topId_2;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.__BinaryParser::headerId
	int64_t ___headerId_3;
	// System.Runtime.Serialization.Formatters.Binary.SizedArray System.Runtime.Serialization.Formatters.Binary.__BinaryParser::objectMapIdTable
	SizedArray_t2151058854 * ___objectMapIdTable_4;
	// System.Runtime.Serialization.Formatters.Binary.SizedArray System.Runtime.Serialization.Formatters.Binary.__BinaryParser::assemIdToAssemblyTable
	SizedArray_t2151058854 * ___assemIdToAssemblyTable_5;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.__BinaryParser::stack
	SerStack_t3886188184 * ___stack_6;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum System.Runtime.Serialization.Formatters.Binary.__BinaryParser::expectedType
	int32_t ___expectedType_7;
	// System.Object System.Runtime.Serialization.Formatters.Binary.__BinaryParser::expectedTypeInformation
	Il2CppObject * ___expectedTypeInformation_8;
	// System.Runtime.Serialization.Formatters.Binary.ParseRecord System.Runtime.Serialization.Formatters.Binary.__BinaryParser::PRS
	ParseRecord_t2674254118 * ___PRS_9;
	// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo System.Runtime.Serialization.Formatters.Binary.__BinaryParser::systemAssemblyInfo
	BinaryAssemblyInfo_t316080507 * ___systemAssemblyInfo_10;
	// System.IO.BinaryReader System.Runtime.Serialization.Formatters.Binary.__BinaryParser::dataReader
	BinaryReader_t2491843768 * ___dataReader_11;
	// System.Runtime.Serialization.Formatters.Binary.SerStack System.Runtime.Serialization.Formatters.Binary.__BinaryParser::opPool
	SerStack_t3886188184 * ___opPool_13;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObject System.Runtime.Serialization.Formatters.Binary.__BinaryParser::binaryObject
	BinaryObject_t763496928 * ___binaryObject_14;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap System.Runtime.Serialization.Formatters.Binary.__BinaryParser::bowm
	BinaryObjectWithMap_t2163984170 * ___bowm_15;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped System.Runtime.Serialization.Formatters.Binary.__BinaryParser::bowmt
	BinaryObjectWithMapTyped_t3348677078 * ___bowmt_16;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectString System.Runtime.Serialization.Formatters.Binary.__BinaryParser::objectString
	BinaryObjectString_t2307902425 * ___objectString_17;
	// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainString System.Runtime.Serialization.Formatters.Binary.__BinaryParser::crossAppDomainString
	BinaryCrossAppDomainString_t1989374585 * ___crossAppDomainString_18;
	// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped System.Runtime.Serialization.Formatters.Binary.__BinaryParser::memberPrimitiveTyped
	MemberPrimitiveTyped_t3733510915 * ___memberPrimitiveTyped_19;
	// System.Byte[] System.Runtime.Serialization.Formatters.Binary.__BinaryParser::byteBuffer
	ByteU5BU5D_t3397334013* ___byteBuffer_20;
	// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped System.Runtime.Serialization.Formatters.Binary.__BinaryParser::memberPrimitiveUnTyped
	MemberPrimitiveUnTyped_t3584398440 * ___memberPrimitiveUnTyped_21;
	// System.Runtime.Serialization.Formatters.Binary.MemberReference System.Runtime.Serialization.Formatters.Binary.__BinaryParser::memberReference
	MemberReference_t1102219583 * ___memberReference_22;
	// System.Runtime.Serialization.Formatters.Binary.ObjectNull System.Runtime.Serialization.Formatters.Binary.__BinaryParser::objectNull
	ObjectNull_t1089955196 * ___objectNull_23;

public:
	inline static int32_t get_offset_of_objectReader_0() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___objectReader_0)); }
	inline ObjectReader_t1476095226 * get_objectReader_0() const { return ___objectReader_0; }
	inline ObjectReader_t1476095226 ** get_address_of_objectReader_0() { return &___objectReader_0; }
	inline void set_objectReader_0(ObjectReader_t1476095226 * value)
	{
		___objectReader_0 = value;
		Il2CppCodeGenWriteBarrier(&___objectReader_0, value);
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___input_1)); }
	inline Stream_t3255436806 * get_input_1() const { return ___input_1; }
	inline Stream_t3255436806 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(Stream_t3255436806 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier(&___input_1, value);
	}

	inline static int32_t get_offset_of_topId_2() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___topId_2)); }
	inline int64_t get_topId_2() const { return ___topId_2; }
	inline int64_t* get_address_of_topId_2() { return &___topId_2; }
	inline void set_topId_2(int64_t value)
	{
		___topId_2 = value;
	}

	inline static int32_t get_offset_of_headerId_3() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___headerId_3)); }
	inline int64_t get_headerId_3() const { return ___headerId_3; }
	inline int64_t* get_address_of_headerId_3() { return &___headerId_3; }
	inline void set_headerId_3(int64_t value)
	{
		___headerId_3 = value;
	}

	inline static int32_t get_offset_of_objectMapIdTable_4() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___objectMapIdTable_4)); }
	inline SizedArray_t2151058854 * get_objectMapIdTable_4() const { return ___objectMapIdTable_4; }
	inline SizedArray_t2151058854 ** get_address_of_objectMapIdTable_4() { return &___objectMapIdTable_4; }
	inline void set_objectMapIdTable_4(SizedArray_t2151058854 * value)
	{
		___objectMapIdTable_4 = value;
		Il2CppCodeGenWriteBarrier(&___objectMapIdTable_4, value);
	}

	inline static int32_t get_offset_of_assemIdToAssemblyTable_5() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___assemIdToAssemblyTable_5)); }
	inline SizedArray_t2151058854 * get_assemIdToAssemblyTable_5() const { return ___assemIdToAssemblyTable_5; }
	inline SizedArray_t2151058854 ** get_address_of_assemIdToAssemblyTable_5() { return &___assemIdToAssemblyTable_5; }
	inline void set_assemIdToAssemblyTable_5(SizedArray_t2151058854 * value)
	{
		___assemIdToAssemblyTable_5 = value;
		Il2CppCodeGenWriteBarrier(&___assemIdToAssemblyTable_5, value);
	}

	inline static int32_t get_offset_of_stack_6() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___stack_6)); }
	inline SerStack_t3886188184 * get_stack_6() const { return ___stack_6; }
	inline SerStack_t3886188184 ** get_address_of_stack_6() { return &___stack_6; }
	inline void set_stack_6(SerStack_t3886188184 * value)
	{
		___stack_6 = value;
		Il2CppCodeGenWriteBarrier(&___stack_6, value);
	}

	inline static int32_t get_offset_of_expectedType_7() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___expectedType_7)); }
	inline int32_t get_expectedType_7() const { return ___expectedType_7; }
	inline int32_t* get_address_of_expectedType_7() { return &___expectedType_7; }
	inline void set_expectedType_7(int32_t value)
	{
		___expectedType_7 = value;
	}

	inline static int32_t get_offset_of_expectedTypeInformation_8() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___expectedTypeInformation_8)); }
	inline Il2CppObject * get_expectedTypeInformation_8() const { return ___expectedTypeInformation_8; }
	inline Il2CppObject ** get_address_of_expectedTypeInformation_8() { return &___expectedTypeInformation_8; }
	inline void set_expectedTypeInformation_8(Il2CppObject * value)
	{
		___expectedTypeInformation_8 = value;
		Il2CppCodeGenWriteBarrier(&___expectedTypeInformation_8, value);
	}

	inline static int32_t get_offset_of_PRS_9() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___PRS_9)); }
	inline ParseRecord_t2674254118 * get_PRS_9() const { return ___PRS_9; }
	inline ParseRecord_t2674254118 ** get_address_of_PRS_9() { return &___PRS_9; }
	inline void set_PRS_9(ParseRecord_t2674254118 * value)
	{
		___PRS_9 = value;
		Il2CppCodeGenWriteBarrier(&___PRS_9, value);
	}

	inline static int32_t get_offset_of_systemAssemblyInfo_10() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___systemAssemblyInfo_10)); }
	inline BinaryAssemblyInfo_t316080507 * get_systemAssemblyInfo_10() const { return ___systemAssemblyInfo_10; }
	inline BinaryAssemblyInfo_t316080507 ** get_address_of_systemAssemblyInfo_10() { return &___systemAssemblyInfo_10; }
	inline void set_systemAssemblyInfo_10(BinaryAssemblyInfo_t316080507 * value)
	{
		___systemAssemblyInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___systemAssemblyInfo_10, value);
	}

	inline static int32_t get_offset_of_dataReader_11() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___dataReader_11)); }
	inline BinaryReader_t2491843768 * get_dataReader_11() const { return ___dataReader_11; }
	inline BinaryReader_t2491843768 ** get_address_of_dataReader_11() { return &___dataReader_11; }
	inline void set_dataReader_11(BinaryReader_t2491843768 * value)
	{
		___dataReader_11 = value;
		Il2CppCodeGenWriteBarrier(&___dataReader_11, value);
	}

	inline static int32_t get_offset_of_opPool_13() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___opPool_13)); }
	inline SerStack_t3886188184 * get_opPool_13() const { return ___opPool_13; }
	inline SerStack_t3886188184 ** get_address_of_opPool_13() { return &___opPool_13; }
	inline void set_opPool_13(SerStack_t3886188184 * value)
	{
		___opPool_13 = value;
		Il2CppCodeGenWriteBarrier(&___opPool_13, value);
	}

	inline static int32_t get_offset_of_binaryObject_14() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___binaryObject_14)); }
	inline BinaryObject_t763496928 * get_binaryObject_14() const { return ___binaryObject_14; }
	inline BinaryObject_t763496928 ** get_address_of_binaryObject_14() { return &___binaryObject_14; }
	inline void set_binaryObject_14(BinaryObject_t763496928 * value)
	{
		___binaryObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObject_14, value);
	}

	inline static int32_t get_offset_of_bowm_15() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___bowm_15)); }
	inline BinaryObjectWithMap_t2163984170 * get_bowm_15() const { return ___bowm_15; }
	inline BinaryObjectWithMap_t2163984170 ** get_address_of_bowm_15() { return &___bowm_15; }
	inline void set_bowm_15(BinaryObjectWithMap_t2163984170 * value)
	{
		___bowm_15 = value;
		Il2CppCodeGenWriteBarrier(&___bowm_15, value);
	}

	inline static int32_t get_offset_of_bowmt_16() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___bowmt_16)); }
	inline BinaryObjectWithMapTyped_t3348677078 * get_bowmt_16() const { return ___bowmt_16; }
	inline BinaryObjectWithMapTyped_t3348677078 ** get_address_of_bowmt_16() { return &___bowmt_16; }
	inline void set_bowmt_16(BinaryObjectWithMapTyped_t3348677078 * value)
	{
		___bowmt_16 = value;
		Il2CppCodeGenWriteBarrier(&___bowmt_16, value);
	}

	inline static int32_t get_offset_of_objectString_17() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___objectString_17)); }
	inline BinaryObjectString_t2307902425 * get_objectString_17() const { return ___objectString_17; }
	inline BinaryObjectString_t2307902425 ** get_address_of_objectString_17() { return &___objectString_17; }
	inline void set_objectString_17(BinaryObjectString_t2307902425 * value)
	{
		___objectString_17 = value;
		Il2CppCodeGenWriteBarrier(&___objectString_17, value);
	}

	inline static int32_t get_offset_of_crossAppDomainString_18() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___crossAppDomainString_18)); }
	inline BinaryCrossAppDomainString_t1989374585 * get_crossAppDomainString_18() const { return ___crossAppDomainString_18; }
	inline BinaryCrossAppDomainString_t1989374585 ** get_address_of_crossAppDomainString_18() { return &___crossAppDomainString_18; }
	inline void set_crossAppDomainString_18(BinaryCrossAppDomainString_t1989374585 * value)
	{
		___crossAppDomainString_18 = value;
		Il2CppCodeGenWriteBarrier(&___crossAppDomainString_18, value);
	}

	inline static int32_t get_offset_of_memberPrimitiveTyped_19() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___memberPrimitiveTyped_19)); }
	inline MemberPrimitiveTyped_t3733510915 * get_memberPrimitiveTyped_19() const { return ___memberPrimitiveTyped_19; }
	inline MemberPrimitiveTyped_t3733510915 ** get_address_of_memberPrimitiveTyped_19() { return &___memberPrimitiveTyped_19; }
	inline void set_memberPrimitiveTyped_19(MemberPrimitiveTyped_t3733510915 * value)
	{
		___memberPrimitiveTyped_19 = value;
		Il2CppCodeGenWriteBarrier(&___memberPrimitiveTyped_19, value);
	}

	inline static int32_t get_offset_of_byteBuffer_20() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___byteBuffer_20)); }
	inline ByteU5BU5D_t3397334013* get_byteBuffer_20() const { return ___byteBuffer_20; }
	inline ByteU5BU5D_t3397334013** get_address_of_byteBuffer_20() { return &___byteBuffer_20; }
	inline void set_byteBuffer_20(ByteU5BU5D_t3397334013* value)
	{
		___byteBuffer_20 = value;
		Il2CppCodeGenWriteBarrier(&___byteBuffer_20, value);
	}

	inline static int32_t get_offset_of_memberPrimitiveUnTyped_21() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___memberPrimitiveUnTyped_21)); }
	inline MemberPrimitiveUnTyped_t3584398440 * get_memberPrimitiveUnTyped_21() const { return ___memberPrimitiveUnTyped_21; }
	inline MemberPrimitiveUnTyped_t3584398440 ** get_address_of_memberPrimitiveUnTyped_21() { return &___memberPrimitiveUnTyped_21; }
	inline void set_memberPrimitiveUnTyped_21(MemberPrimitiveUnTyped_t3584398440 * value)
	{
		___memberPrimitiveUnTyped_21 = value;
		Il2CppCodeGenWriteBarrier(&___memberPrimitiveUnTyped_21, value);
	}

	inline static int32_t get_offset_of_memberReference_22() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___memberReference_22)); }
	inline MemberReference_t1102219583 * get_memberReference_22() const { return ___memberReference_22; }
	inline MemberReference_t1102219583 ** get_address_of_memberReference_22() { return &___memberReference_22; }
	inline void set_memberReference_22(MemberReference_t1102219583 * value)
	{
		___memberReference_22 = value;
		Il2CppCodeGenWriteBarrier(&___memberReference_22, value);
	}

	inline static int32_t get_offset_of_objectNull_23() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990, ___objectNull_23)); }
	inline ObjectNull_t1089955196 * get_objectNull_23() const { return ___objectNull_23; }
	inline ObjectNull_t1089955196 ** get_address_of_objectNull_23() { return &___objectNull_23; }
	inline void set_objectNull_23(ObjectNull_t1089955196 * value)
	{
		___objectNull_23 = value;
		Il2CppCodeGenWriteBarrier(&___objectNull_23, value);
	}
};

struct __BinaryParser_t1222443990_StaticFields
{
public:
	// System.Text.Encoding System.Runtime.Serialization.Formatters.Binary.__BinaryParser::encoding
	Encoding_t663144255 * ___encoding_12;
	// System.Runtime.Serialization.Formatters.Binary.MessageEnd modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.__BinaryParser::messageEnd
	MessageEnd_t3819299862 * ___messageEnd_24;

public:
	inline static int32_t get_offset_of_encoding_12() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990_StaticFields, ___encoding_12)); }
	inline Encoding_t663144255 * get_encoding_12() const { return ___encoding_12; }
	inline Encoding_t663144255 ** get_address_of_encoding_12() { return &___encoding_12; }
	inline void set_encoding_12(Encoding_t663144255 * value)
	{
		___encoding_12 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_12, value);
	}

	inline static int32_t get_offset_of_messageEnd_24() { return static_cast<int32_t>(offsetof(__BinaryParser_t1222443990_StaticFields, ___messageEnd_24)); }
	inline MessageEnd_t3819299862 * get_messageEnd_24() const { return ___messageEnd_24; }
	inline MessageEnd_t3819299862 ** get_address_of_messageEnd_24() { return &___messageEnd_24; }
	inline void set_messageEnd_24(MessageEnd_t3819299862 * value)
	{
		___messageEnd_24 = value;
		Il2CppCodeGenWriteBarrier(&___messageEnd_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
