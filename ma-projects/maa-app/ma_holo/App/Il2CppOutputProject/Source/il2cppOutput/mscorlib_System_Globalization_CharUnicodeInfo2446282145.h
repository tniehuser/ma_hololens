﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_CharUnicodeInfo_Digi2603367031.h"

// System.UInt16
struct UInt16_t986882611;
// System.Byte
struct Byte_t3683104436;
// System.Globalization.CharUnicodeInfo/DigitValues
struct DigitValues_t2603367031;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CharUnicodeInfo
struct  CharUnicodeInfo_t2446282145  : public Il2CppObject
{
public:

public:
};

struct CharUnicodeInfo_t2446282145_StaticFields
{
public:
	// System.Boolean System.Globalization.CharUnicodeInfo::s_initialized
	bool ___s_initialized_0;
	// System.UInt16* System.Globalization.CharUnicodeInfo::s_pCategoryLevel1Index
	uint16_t* ___s_pCategoryLevel1Index_1;
	// System.Byte* System.Globalization.CharUnicodeInfo::s_pCategoriesValue
	uint8_t* ___s_pCategoriesValue_2;
	// System.UInt16* System.Globalization.CharUnicodeInfo::s_pNumericLevel1Index
	uint16_t* ___s_pNumericLevel1Index_3;
	// System.Byte* System.Globalization.CharUnicodeInfo::s_pNumericValues
	uint8_t* ___s_pNumericValues_4;
	// System.Globalization.CharUnicodeInfo/DigitValues* System.Globalization.CharUnicodeInfo::s_pDigitValues
	DigitValues_t2603367031 * ___s_pDigitValues_5;

public:
	inline static int32_t get_offset_of_s_initialized_0() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_initialized_0)); }
	inline bool get_s_initialized_0() const { return ___s_initialized_0; }
	inline bool* get_address_of_s_initialized_0() { return &___s_initialized_0; }
	inline void set_s_initialized_0(bool value)
	{
		___s_initialized_0 = value;
	}

	inline static int32_t get_offset_of_s_pCategoryLevel1Index_1() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_pCategoryLevel1Index_1)); }
	inline uint16_t* get_s_pCategoryLevel1Index_1() const { return ___s_pCategoryLevel1Index_1; }
	inline uint16_t** get_address_of_s_pCategoryLevel1Index_1() { return &___s_pCategoryLevel1Index_1; }
	inline void set_s_pCategoryLevel1Index_1(uint16_t* value)
	{
		___s_pCategoryLevel1Index_1 = value;
	}

	inline static int32_t get_offset_of_s_pCategoriesValue_2() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_pCategoriesValue_2)); }
	inline uint8_t* get_s_pCategoriesValue_2() const { return ___s_pCategoriesValue_2; }
	inline uint8_t** get_address_of_s_pCategoriesValue_2() { return &___s_pCategoriesValue_2; }
	inline void set_s_pCategoriesValue_2(uint8_t* value)
	{
		___s_pCategoriesValue_2 = value;
	}

	inline static int32_t get_offset_of_s_pNumericLevel1Index_3() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_pNumericLevel1Index_3)); }
	inline uint16_t* get_s_pNumericLevel1Index_3() const { return ___s_pNumericLevel1Index_3; }
	inline uint16_t** get_address_of_s_pNumericLevel1Index_3() { return &___s_pNumericLevel1Index_3; }
	inline void set_s_pNumericLevel1Index_3(uint16_t* value)
	{
		___s_pNumericLevel1Index_3 = value;
	}

	inline static int32_t get_offset_of_s_pNumericValues_4() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_pNumericValues_4)); }
	inline uint8_t* get_s_pNumericValues_4() const { return ___s_pNumericValues_4; }
	inline uint8_t** get_address_of_s_pNumericValues_4() { return &___s_pNumericValues_4; }
	inline void set_s_pNumericValues_4(uint8_t* value)
	{
		___s_pNumericValues_4 = value;
	}

	inline static int32_t get_offset_of_s_pDigitValues_5() { return static_cast<int32_t>(offsetof(CharUnicodeInfo_t2446282145_StaticFields, ___s_pDigitValues_5)); }
	inline DigitValues_t2603367031 * get_s_pDigitValues_5() const { return ___s_pDigitValues_5; }
	inline DigitValues_t2603367031 ** get_address_of_s_pDigitValues_5() { return &___s_pDigitValues_5; }
	inline void set_s_pDigitValues_5(DigitValues_t2603367031 * value)
	{
		___s_pDigitValues_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
