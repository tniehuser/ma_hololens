﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ActiveAxis439376929.h"

// System.Xml.Schema.KeySequence
struct KeySequence_t746093258;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LocatedActiveAxis
struct  LocatedActiveAxis_t90453917  : public ActiveAxis_t439376929
{
public:
	// System.Int32 System.Xml.Schema.LocatedActiveAxis::column
	int32_t ___column_4;
	// System.Boolean System.Xml.Schema.LocatedActiveAxis::isMatched
	bool ___isMatched_5;
	// System.Xml.Schema.KeySequence System.Xml.Schema.LocatedActiveAxis::Ks
	KeySequence_t746093258 * ___Ks_6;

public:
	inline static int32_t get_offset_of_column_4() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___column_4)); }
	inline int32_t get_column_4() const { return ___column_4; }
	inline int32_t* get_address_of_column_4() { return &___column_4; }
	inline void set_column_4(int32_t value)
	{
		___column_4 = value;
	}

	inline static int32_t get_offset_of_isMatched_5() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___isMatched_5)); }
	inline bool get_isMatched_5() const { return ___isMatched_5; }
	inline bool* get_address_of_isMatched_5() { return &___isMatched_5; }
	inline void set_isMatched_5(bool value)
	{
		___isMatched_5 = value;
	}

	inline static int32_t get_offset_of_Ks_6() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___Ks_6)); }
	inline KeySequence_t746093258 * get_Ks_6() const { return ___Ks_6; }
	inline KeySequence_t746093258 ** get_address_of_Ks_6() { return &___Ks_6; }
	inline void set_Ks_6(KeySequence_t746093258 * value)
	{
		___Ks_6 = value;
		Il2CppCodeGenWriteBarrier(&___Ks_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
