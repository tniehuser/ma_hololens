﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_EncoderFallbackBuffer3883615514.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderReplacementFallbackBuffer
struct  EncoderReplacementFallbackBuffer_t1313574570  : public EncoderFallbackBuffer_t3883615514
{
public:
	// System.String System.Text.EncoderReplacementFallbackBuffer::strDefault
	String_t* ___strDefault_7;
	// System.Int32 System.Text.EncoderReplacementFallbackBuffer::fallbackCount
	int32_t ___fallbackCount_8;
	// System.Int32 System.Text.EncoderReplacementFallbackBuffer::fallbackIndex
	int32_t ___fallbackIndex_9;

public:
	inline static int32_t get_offset_of_strDefault_7() { return static_cast<int32_t>(offsetof(EncoderReplacementFallbackBuffer_t1313574570, ___strDefault_7)); }
	inline String_t* get_strDefault_7() const { return ___strDefault_7; }
	inline String_t** get_address_of_strDefault_7() { return &___strDefault_7; }
	inline void set_strDefault_7(String_t* value)
	{
		___strDefault_7 = value;
		Il2CppCodeGenWriteBarrier(&___strDefault_7, value);
	}

	inline static int32_t get_offset_of_fallbackCount_8() { return static_cast<int32_t>(offsetof(EncoderReplacementFallbackBuffer_t1313574570, ___fallbackCount_8)); }
	inline int32_t get_fallbackCount_8() const { return ___fallbackCount_8; }
	inline int32_t* get_address_of_fallbackCount_8() { return &___fallbackCount_8; }
	inline void set_fallbackCount_8(int32_t value)
	{
		___fallbackCount_8 = value;
	}

	inline static int32_t get_offset_of_fallbackIndex_9() { return static_cast<int32_t>(offsetof(EncoderReplacementFallbackBuffer_t1313574570, ___fallbackIndex_9)); }
	inline int32_t get_fallbackIndex_9() const { return ___fallbackIndex_9; }
	inline int32_t* get_address_of_fallbackIndex_9() { return &___fallbackIndex_9; }
	inline void set_fallbackIndex_9(int32_t value)
	{
		___fallbackIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
