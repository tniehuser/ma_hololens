﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_Linq_Enumerable_Iterator_1_gen2759310362.h"

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers[]
struct MessageTypeSubscribersU5BU5D_t2572700727;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t2919696197;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  WhereArrayIterator_1_t2600159350  : public Iterator_1_t2759310362
{
public:
	// TSource[] System.Linq.Enumerable/WhereArrayIterator`1::source
	MessageTypeSubscribersU5BU5D_t2572700727* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereArrayIterator`1::predicate
	Func_2_t2919696197 * ___predicate_4;
	// System.Int32 System.Linq.Enumerable/WhereArrayIterator`1::index
	int32_t ___index_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t2600159350, ___source_3)); }
	inline MessageTypeSubscribersU5BU5D_t2572700727* get_source_3() const { return ___source_3; }
	inline MessageTypeSubscribersU5BU5D_t2572700727** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(MessageTypeSubscribersU5BU5D_t2572700727* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t2600159350, ___predicate_4)); }
	inline Func_2_t2919696197 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2919696197 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2919696197 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_4, value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t2600159350, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
