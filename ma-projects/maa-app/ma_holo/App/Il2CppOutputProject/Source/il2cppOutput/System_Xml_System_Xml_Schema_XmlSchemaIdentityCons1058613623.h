﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t604820427;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t964629540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaIdentityConstraint
struct  XmlSchemaIdentityConstraint_t1058613623  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaIdentityConstraint::name
	String_t* ___name_9;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XmlSchemaIdentityConstraint::selector
	XmlSchemaXPath_t604820427 * ___selector_10;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaIdentityConstraint::fields
	XmlSchemaObjectCollection_t395083109 * ___fields_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaIdentityConstraint::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_12;
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.XmlSchemaIdentityConstraint::compiledConstraint
	CompiledIdentityConstraint_t964629540 * ___compiledConstraint_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_selector_10() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___selector_10)); }
	inline XmlSchemaXPath_t604820427 * get_selector_10() const { return ___selector_10; }
	inline XmlSchemaXPath_t604820427 ** get_address_of_selector_10() { return &___selector_10; }
	inline void set_selector_10(XmlSchemaXPath_t604820427 * value)
	{
		___selector_10 = value;
		Il2CppCodeGenWriteBarrier(&___selector_10, value);
	}

	inline static int32_t get_offset_of_fields_11() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___fields_11)); }
	inline XmlSchemaObjectCollection_t395083109 * get_fields_11() const { return ___fields_11; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_fields_11() { return &___fields_11; }
	inline void set_fields_11(XmlSchemaObjectCollection_t395083109 * value)
	{
		___fields_11 = value;
		Il2CppCodeGenWriteBarrier(&___fields_11, value);
	}

	inline static int32_t get_offset_of_qualifiedName_12() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___qualifiedName_12)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_12() const { return ___qualifiedName_12; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_12() { return &___qualifiedName_12; }
	inline void set_qualifiedName_12(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_12 = value;
		Il2CppCodeGenWriteBarrier(&___qualifiedName_12, value);
	}

	inline static int32_t get_offset_of_compiledConstraint_13() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___compiledConstraint_13)); }
	inline CompiledIdentityConstraint_t964629540 * get_compiledConstraint_13() const { return ___compiledConstraint_13; }
	inline CompiledIdentityConstraint_t964629540 ** get_address_of_compiledConstraint_13() { return &___compiledConstraint_13; }
	inline void set_compiledConstraint_13(CompiledIdentityConstraint_t964629540 * value)
	{
		___compiledConstraint_13 = value;
		Il2CppCodeGenWriteBarrier(&___compiledConstraint_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
