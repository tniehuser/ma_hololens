﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_short3426858539.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_byte
struct  Datatype_byte_t702122475  : public Datatype_short_t3426858539
{
public:

public:
};

struct Datatype_byte_t702122475_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_byte::atomicValueType
	Type_t * ___atomicValueType_105;
	// System.Type System.Xml.Schema.Datatype_byte::listValueType
	Type_t * ___listValueType_106;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_byte::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_107;

public:
	inline static int32_t get_offset_of_atomicValueType_105() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___atomicValueType_105)); }
	inline Type_t * get_atomicValueType_105() const { return ___atomicValueType_105; }
	inline Type_t ** get_address_of_atomicValueType_105() { return &___atomicValueType_105; }
	inline void set_atomicValueType_105(Type_t * value)
	{
		___atomicValueType_105 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_105, value);
	}

	inline static int32_t get_offset_of_listValueType_106() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___listValueType_106)); }
	inline Type_t * get_listValueType_106() const { return ___listValueType_106; }
	inline Type_t ** get_address_of_listValueType_106() { return &___listValueType_106; }
	inline void set_listValueType_106(Type_t * value)
	{
		___listValueType_106 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_106, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_107() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___numeric10FacetsChecker_107)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_107() const { return ___numeric10FacetsChecker_107; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_107() { return &___numeric10FacetsChecker_107; }
	inline void set_numeric10FacetsChecker_107(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_107 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_107, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
