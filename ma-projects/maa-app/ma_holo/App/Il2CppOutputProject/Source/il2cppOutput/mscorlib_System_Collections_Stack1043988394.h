﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Stack
struct  Stack_t1043988394  : public Il2CppObject
{
public:
	// System.Object[] System.Collections.Stack::_array
	ObjectU5BU5D_t3614634134* ____array_0;
	// System.Int32 System.Collections.Stack::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Stack::_version
	int32_t ____version_2;
	// System.Object System.Collections.Stack::_syncRoot
	Il2CppObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ____array_0)); }
	inline ObjectU5BU5D_t3614634134* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3614634134* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier(&____array_0, value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ____syncRoot_3)); }
	inline Il2CppObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline Il2CppObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(Il2CppObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
