﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaContent3733871217.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct  XmlSchemaSimpleContentRestriction_t2728776481  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseType
	XmlSchemaSimpleType_t248156492 * ___baseType_10;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::facets
	XmlSchemaObjectCollection_t395083109 * ___facets_11;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_12;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentRestriction::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_13;

public:
	inline static int32_t get_offset_of_baseTypeName_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___baseTypeName_9)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_9() const { return ___baseTypeName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_9() { return &___baseTypeName_9; }
	inline void set_baseTypeName_9(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_9 = value;
		Il2CppCodeGenWriteBarrier(&___baseTypeName_9, value);
	}

	inline static int32_t get_offset_of_baseType_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___baseType_10)); }
	inline XmlSchemaSimpleType_t248156492 * get_baseType_10() const { return ___baseType_10; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_baseType_10() { return &___baseType_10; }
	inline void set_baseType_10(XmlSchemaSimpleType_t248156492 * value)
	{
		___baseType_10 = value;
		Il2CppCodeGenWriteBarrier(&___baseType_10, value);
	}

	inline static int32_t get_offset_of_facets_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___facets_11)); }
	inline XmlSchemaObjectCollection_t395083109 * get_facets_11() const { return ___facets_11; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_facets_11() { return &___facets_11; }
	inline void set_facets_11(XmlSchemaObjectCollection_t395083109 * value)
	{
		___facets_11 = value;
		Il2CppCodeGenWriteBarrier(&___facets_11, value);
	}

	inline static int32_t get_offset_of_attributes_12() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___attributes_12)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_12() const { return ___attributes_12; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_12() { return &___attributes_12; }
	inline void set_attributes_12(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_12 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_12, value);
	}

	inline static int32_t get_offset_of_anyAttribute_13() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___anyAttribute_13)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_13() const { return ___anyAttribute_13; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_13() { return &___anyAttribute_13; }
	inline void set_anyAttribute_13(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_13 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
