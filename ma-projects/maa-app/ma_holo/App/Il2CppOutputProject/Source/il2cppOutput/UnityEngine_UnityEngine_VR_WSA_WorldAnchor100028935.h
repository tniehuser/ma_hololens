﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Component3819376471.h"

// UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate
struct OnTrackingChangedDelegate_t417897799;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.WorldAnchor
struct  WorldAnchor_t100028935  : public Component_t3819376471
{
public:
	// UnityEngine.VR.WSA.WorldAnchor/OnTrackingChangedDelegate UnityEngine.VR.WSA.WorldAnchor::OnTrackingChanged
	OnTrackingChangedDelegate_t417897799 * ___OnTrackingChanged_2;

public:
	inline static int32_t get_offset_of_OnTrackingChanged_2() { return static_cast<int32_t>(offsetof(WorldAnchor_t100028935, ___OnTrackingChanged_2)); }
	inline OnTrackingChangedDelegate_t417897799 * get_OnTrackingChanged_2() const { return ___OnTrackingChanged_2; }
	inline OnTrackingChangedDelegate_t417897799 ** get_address_of_OnTrackingChanged_2() { return &___OnTrackingChanged_2; }
	inline void set_OnTrackingChanged_2(OnTrackingChangedDelegate_t417897799 * value)
	{
		___OnTrackingChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnTrackingChanged_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
