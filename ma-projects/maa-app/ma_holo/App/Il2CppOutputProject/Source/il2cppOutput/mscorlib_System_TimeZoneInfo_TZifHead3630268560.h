﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_TimeZoneInfo_TZVersion2863501951.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/TZifHead
struct  TZifHead_t3630268560 
{
public:
	// System.UInt32 System.TimeZoneInfo/TZifHead::Magic
	uint32_t ___Magic_0;
	// System.TimeZoneInfo/TZVersion System.TimeZoneInfo/TZifHead::Version
	uint8_t ___Version_1;
	// System.UInt32 System.TimeZoneInfo/TZifHead::IsGmtCount
	uint32_t ___IsGmtCount_2;
	// System.UInt32 System.TimeZoneInfo/TZifHead::IsStdCount
	uint32_t ___IsStdCount_3;
	// System.UInt32 System.TimeZoneInfo/TZifHead::LeapCount
	uint32_t ___LeapCount_4;
	// System.UInt32 System.TimeZoneInfo/TZifHead::TimeCount
	uint32_t ___TimeCount_5;
	// System.UInt32 System.TimeZoneInfo/TZifHead::TypeCount
	uint32_t ___TypeCount_6;
	// System.UInt32 System.TimeZoneInfo/TZifHead::CharCount
	uint32_t ___CharCount_7;

public:
	inline static int32_t get_offset_of_Magic_0() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___Magic_0)); }
	inline uint32_t get_Magic_0() const { return ___Magic_0; }
	inline uint32_t* get_address_of_Magic_0() { return &___Magic_0; }
	inline void set_Magic_0(uint32_t value)
	{
		___Magic_0 = value;
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___Version_1)); }
	inline uint8_t get_Version_1() const { return ___Version_1; }
	inline uint8_t* get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(uint8_t value)
	{
		___Version_1 = value;
	}

	inline static int32_t get_offset_of_IsGmtCount_2() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___IsGmtCount_2)); }
	inline uint32_t get_IsGmtCount_2() const { return ___IsGmtCount_2; }
	inline uint32_t* get_address_of_IsGmtCount_2() { return &___IsGmtCount_2; }
	inline void set_IsGmtCount_2(uint32_t value)
	{
		___IsGmtCount_2 = value;
	}

	inline static int32_t get_offset_of_IsStdCount_3() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___IsStdCount_3)); }
	inline uint32_t get_IsStdCount_3() const { return ___IsStdCount_3; }
	inline uint32_t* get_address_of_IsStdCount_3() { return &___IsStdCount_3; }
	inline void set_IsStdCount_3(uint32_t value)
	{
		___IsStdCount_3 = value;
	}

	inline static int32_t get_offset_of_LeapCount_4() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___LeapCount_4)); }
	inline uint32_t get_LeapCount_4() const { return ___LeapCount_4; }
	inline uint32_t* get_address_of_LeapCount_4() { return &___LeapCount_4; }
	inline void set_LeapCount_4(uint32_t value)
	{
		___LeapCount_4 = value;
	}

	inline static int32_t get_offset_of_TimeCount_5() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___TimeCount_5)); }
	inline uint32_t get_TimeCount_5() const { return ___TimeCount_5; }
	inline uint32_t* get_address_of_TimeCount_5() { return &___TimeCount_5; }
	inline void set_TimeCount_5(uint32_t value)
	{
		___TimeCount_5 = value;
	}

	inline static int32_t get_offset_of_TypeCount_6() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___TypeCount_6)); }
	inline uint32_t get_TypeCount_6() const { return ___TypeCount_6; }
	inline uint32_t* get_address_of_TypeCount_6() { return &___TypeCount_6; }
	inline void set_TypeCount_6(uint32_t value)
	{
		___TypeCount_6 = value;
	}

	inline static int32_t get_offset_of_CharCount_7() { return static_cast<int32_t>(offsetof(TZifHead_t3630268560, ___CharCount_7)); }
	inline uint32_t get_CharCount_7() const { return ___CharCount_7; }
	inline uint32_t* get_address_of_CharCount_7() { return &___CharCount_7; }
	inline void set_CharCount_7(uint32_t value)
	{
		___CharCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
