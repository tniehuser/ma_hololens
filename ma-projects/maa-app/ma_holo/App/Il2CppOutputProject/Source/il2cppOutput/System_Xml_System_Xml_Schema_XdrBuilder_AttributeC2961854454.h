﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t1510907267;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/AttributeContent
struct  AttributeContent_t2961854454  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XdrBuilder/AttributeContent::_AttDef
	SchemaAttDef_t1510907267 * ____AttDef_0;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/AttributeContent::_Name
	XmlQualifiedName_t1944712516 * ____Name_1;
	// System.String System.Xml.Schema.XdrBuilder/AttributeContent::_Prefix
	String_t* ____Prefix_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_Required
	bool ____Required_3;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MinVal
	uint32_t ____MinVal_4;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MaxVal
	uint32_t ____MaxVal_5;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MaxLength
	uint32_t ____MaxLength_6;
	// System.UInt32 System.Xml.Schema.XdrBuilder/AttributeContent::_MinLength
	uint32_t ____MinLength_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_EnumerationRequired
	bool ____EnumerationRequired_8;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_HasDataType
	bool ____HasDataType_9;
	// System.Boolean System.Xml.Schema.XdrBuilder/AttributeContent::_Global
	bool ____Global_10;
	// System.Object System.Xml.Schema.XdrBuilder/AttributeContent::_Default
	Il2CppObject * ____Default_11;

public:
	inline static int32_t get_offset_of__AttDef_0() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____AttDef_0)); }
	inline SchemaAttDef_t1510907267 * get__AttDef_0() const { return ____AttDef_0; }
	inline SchemaAttDef_t1510907267 ** get_address_of__AttDef_0() { return &____AttDef_0; }
	inline void set__AttDef_0(SchemaAttDef_t1510907267 * value)
	{
		____AttDef_0 = value;
		Il2CppCodeGenWriteBarrier(&____AttDef_0, value);
	}

	inline static int32_t get_offset_of__Name_1() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____Name_1)); }
	inline XmlQualifiedName_t1944712516 * get__Name_1() const { return ____Name_1; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__Name_1() { return &____Name_1; }
	inline void set__Name_1(XmlQualifiedName_t1944712516 * value)
	{
		____Name_1 = value;
		Il2CppCodeGenWriteBarrier(&____Name_1, value);
	}

	inline static int32_t get_offset_of__Prefix_2() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____Prefix_2)); }
	inline String_t* get__Prefix_2() const { return ____Prefix_2; }
	inline String_t** get_address_of__Prefix_2() { return &____Prefix_2; }
	inline void set__Prefix_2(String_t* value)
	{
		____Prefix_2 = value;
		Il2CppCodeGenWriteBarrier(&____Prefix_2, value);
	}

	inline static int32_t get_offset_of__Required_3() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____Required_3)); }
	inline bool get__Required_3() const { return ____Required_3; }
	inline bool* get_address_of__Required_3() { return &____Required_3; }
	inline void set__Required_3(bool value)
	{
		____Required_3 = value;
	}

	inline static int32_t get_offset_of__MinVal_4() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____MinVal_4)); }
	inline uint32_t get__MinVal_4() const { return ____MinVal_4; }
	inline uint32_t* get_address_of__MinVal_4() { return &____MinVal_4; }
	inline void set__MinVal_4(uint32_t value)
	{
		____MinVal_4 = value;
	}

	inline static int32_t get_offset_of__MaxVal_5() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____MaxVal_5)); }
	inline uint32_t get__MaxVal_5() const { return ____MaxVal_5; }
	inline uint32_t* get_address_of__MaxVal_5() { return &____MaxVal_5; }
	inline void set__MaxVal_5(uint32_t value)
	{
		____MaxVal_5 = value;
	}

	inline static int32_t get_offset_of__MaxLength_6() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____MaxLength_6)); }
	inline uint32_t get__MaxLength_6() const { return ____MaxLength_6; }
	inline uint32_t* get_address_of__MaxLength_6() { return &____MaxLength_6; }
	inline void set__MaxLength_6(uint32_t value)
	{
		____MaxLength_6 = value;
	}

	inline static int32_t get_offset_of__MinLength_7() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____MinLength_7)); }
	inline uint32_t get__MinLength_7() const { return ____MinLength_7; }
	inline uint32_t* get_address_of__MinLength_7() { return &____MinLength_7; }
	inline void set__MinLength_7(uint32_t value)
	{
		____MinLength_7 = value;
	}

	inline static int32_t get_offset_of__EnumerationRequired_8() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____EnumerationRequired_8)); }
	inline bool get__EnumerationRequired_8() const { return ____EnumerationRequired_8; }
	inline bool* get_address_of__EnumerationRequired_8() { return &____EnumerationRequired_8; }
	inline void set__EnumerationRequired_8(bool value)
	{
		____EnumerationRequired_8 = value;
	}

	inline static int32_t get_offset_of__HasDataType_9() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____HasDataType_9)); }
	inline bool get__HasDataType_9() const { return ____HasDataType_9; }
	inline bool* get_address_of__HasDataType_9() { return &____HasDataType_9; }
	inline void set__HasDataType_9(bool value)
	{
		____HasDataType_9 = value;
	}

	inline static int32_t get_offset_of__Global_10() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____Global_10)); }
	inline bool get__Global_10() const { return ____Global_10; }
	inline bool* get_address_of__Global_10() { return &____Global_10; }
	inline void set__Global_10(bool value)
	{
		____Global_10 = value;
	}

	inline static int32_t get_offset_of__Default_11() { return static_cast<int32_t>(offsetof(AttributeContent_t2961854454, ____Default_11)); }
	inline Il2CppObject * get__Default_11() const { return ____Default_11; }
	inline Il2CppObject ** get_address_of__Default_11() { return &____Default_11; }
	inline void set__Default_11(Il2CppObject * value)
	{
		____Default_11 = value;
		Il2CppCodeGenWriteBarrier(&____Default_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
