﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_CookieToken1256074727.h"

// System.String
struct String_t;
// System.Net.CookieTokenizer/RecognizedAttribute[]
struct RecognizedAttributeU5BU5D_t3981250559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer
struct  CookieTokenizer_t3643404755  : public Il2CppObject
{
public:
	// System.Boolean System.Net.CookieTokenizer::m_eofCookie
	bool ___m_eofCookie_0;
	// System.Int32 System.Net.CookieTokenizer::m_index
	int32_t ___m_index_1;
	// System.Int32 System.Net.CookieTokenizer::m_length
	int32_t ___m_length_2;
	// System.String System.Net.CookieTokenizer::m_name
	String_t* ___m_name_3;
	// System.Boolean System.Net.CookieTokenizer::m_quoted
	bool ___m_quoted_4;
	// System.Int32 System.Net.CookieTokenizer::m_start
	int32_t ___m_start_5;
	// System.Net.CookieToken System.Net.CookieTokenizer::m_token
	int32_t ___m_token_6;
	// System.Int32 System.Net.CookieTokenizer::m_tokenLength
	int32_t ___m_tokenLength_7;
	// System.String System.Net.CookieTokenizer::m_tokenStream
	String_t* ___m_tokenStream_8;
	// System.String System.Net.CookieTokenizer::m_value
	String_t* ___m_value_9;

public:
	inline static int32_t get_offset_of_m_eofCookie_0() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_eofCookie_0)); }
	inline bool get_m_eofCookie_0() const { return ___m_eofCookie_0; }
	inline bool* get_address_of_m_eofCookie_0() { return &___m_eofCookie_0; }
	inline void set_m_eofCookie_0(bool value)
	{
		___m_eofCookie_0 = value;
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}

	inline static int32_t get_offset_of_m_length_2() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_length_2)); }
	inline int32_t get_m_length_2() const { return ___m_length_2; }
	inline int32_t* get_address_of_m_length_2() { return &___m_length_2; }
	inline void set_m_length_2(int32_t value)
	{
		___m_length_2 = value;
	}

	inline static int32_t get_offset_of_m_name_3() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_name_3)); }
	inline String_t* get_m_name_3() const { return ___m_name_3; }
	inline String_t** get_address_of_m_name_3() { return &___m_name_3; }
	inline void set_m_name_3(String_t* value)
	{
		___m_name_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_3, value);
	}

	inline static int32_t get_offset_of_m_quoted_4() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_quoted_4)); }
	inline bool get_m_quoted_4() const { return ___m_quoted_4; }
	inline bool* get_address_of_m_quoted_4() { return &___m_quoted_4; }
	inline void set_m_quoted_4(bool value)
	{
		___m_quoted_4 = value;
	}

	inline static int32_t get_offset_of_m_start_5() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_start_5)); }
	inline int32_t get_m_start_5() const { return ___m_start_5; }
	inline int32_t* get_address_of_m_start_5() { return &___m_start_5; }
	inline void set_m_start_5(int32_t value)
	{
		___m_start_5 = value;
	}

	inline static int32_t get_offset_of_m_token_6() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_token_6)); }
	inline int32_t get_m_token_6() const { return ___m_token_6; }
	inline int32_t* get_address_of_m_token_6() { return &___m_token_6; }
	inline void set_m_token_6(int32_t value)
	{
		___m_token_6 = value;
	}

	inline static int32_t get_offset_of_m_tokenLength_7() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_tokenLength_7)); }
	inline int32_t get_m_tokenLength_7() const { return ___m_tokenLength_7; }
	inline int32_t* get_address_of_m_tokenLength_7() { return &___m_tokenLength_7; }
	inline void set_m_tokenLength_7(int32_t value)
	{
		___m_tokenLength_7 = value;
	}

	inline static int32_t get_offset_of_m_tokenStream_8() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_tokenStream_8)); }
	inline String_t* get_m_tokenStream_8() const { return ___m_tokenStream_8; }
	inline String_t** get_address_of_m_tokenStream_8() { return &___m_tokenStream_8; }
	inline void set_m_tokenStream_8(String_t* value)
	{
		___m_tokenStream_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_tokenStream_8, value);
	}

	inline static int32_t get_offset_of_m_value_9() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755, ___m_value_9)); }
	inline String_t* get_m_value_9() const { return ___m_value_9; }
	inline String_t** get_address_of_m_value_9() { return &___m_value_9; }
	inline void set_m_value_9(String_t* value)
	{
		___m_value_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_value_9, value);
	}
};

struct CookieTokenizer_t3643404755_StaticFields
{
public:
	// System.Net.CookieTokenizer/RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedAttributes
	RecognizedAttributeU5BU5D_t3981250559* ___RecognizedAttributes_10;
	// System.Net.CookieTokenizer/RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedServerAttributes
	RecognizedAttributeU5BU5D_t3981250559* ___RecognizedServerAttributes_11;

public:
	inline static int32_t get_offset_of_RecognizedAttributes_10() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755_StaticFields, ___RecognizedAttributes_10)); }
	inline RecognizedAttributeU5BU5D_t3981250559* get_RecognizedAttributes_10() const { return ___RecognizedAttributes_10; }
	inline RecognizedAttributeU5BU5D_t3981250559** get_address_of_RecognizedAttributes_10() { return &___RecognizedAttributes_10; }
	inline void set_RecognizedAttributes_10(RecognizedAttributeU5BU5D_t3981250559* value)
	{
		___RecognizedAttributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___RecognizedAttributes_10, value);
	}

	inline static int32_t get_offset_of_RecognizedServerAttributes_11() { return static_cast<int32_t>(offsetof(CookieTokenizer_t3643404755_StaticFields, ___RecognizedServerAttributes_11)); }
	inline RecognizedAttributeU5BU5D_t3981250559* get_RecognizedServerAttributes_11() const { return ___RecognizedServerAttributes_11; }
	inline RecognizedAttributeU5BU5D_t3981250559** get_address_of_RecognizedServerAttributes_11() { return &___RecognizedServerAttributes_11; }
	inline void set_RecognizedServerAttributes_11(RecognizedAttributeU5BU5D_t3981250559* value)
	{
		___RecognizedServerAttributes_11 = value;
		Il2CppCodeGenWriteBarrier(&___RecognizedServerAttributes_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
