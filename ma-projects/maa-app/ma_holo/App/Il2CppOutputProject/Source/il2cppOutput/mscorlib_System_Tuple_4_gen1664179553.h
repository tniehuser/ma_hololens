﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Tuple`4<System.Object,System.Object,System.Int32,System.Int32>
struct  Tuple_4_t1664179553  : public Il2CppObject
{
public:
	// T1 System.Tuple`4::m_Item1
	Il2CppObject * ___m_Item1_0;
	// T2 System.Tuple`4::m_Item2
	Il2CppObject * ___m_Item2_1;
	// T3 System.Tuple`4::m_Item3
	int32_t ___m_Item3_2;
	// T4 System.Tuple`4::m_Item4
	int32_t ___m_Item4_3;

public:
	inline static int32_t get_offset_of_m_Item1_0() { return static_cast<int32_t>(offsetof(Tuple_4_t1664179553, ___m_Item1_0)); }
	inline Il2CppObject * get_m_Item1_0() const { return ___m_Item1_0; }
	inline Il2CppObject ** get_address_of_m_Item1_0() { return &___m_Item1_0; }
	inline void set_m_Item1_0(Il2CppObject * value)
	{
		___m_Item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item1_0, value);
	}

	inline static int32_t get_offset_of_m_Item2_1() { return static_cast<int32_t>(offsetof(Tuple_4_t1664179553, ___m_Item2_1)); }
	inline Il2CppObject * get_m_Item2_1() const { return ___m_Item2_1; }
	inline Il2CppObject ** get_address_of_m_Item2_1() { return &___m_Item2_1; }
	inline void set_m_Item2_1(Il2CppObject * value)
	{
		___m_Item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Item2_1, value);
	}

	inline static int32_t get_offset_of_m_Item3_2() { return static_cast<int32_t>(offsetof(Tuple_4_t1664179553, ___m_Item3_2)); }
	inline int32_t get_m_Item3_2() const { return ___m_Item3_2; }
	inline int32_t* get_address_of_m_Item3_2() { return &___m_Item3_2; }
	inline void set_m_Item3_2(int32_t value)
	{
		___m_Item3_2 = value;
	}

	inline static int32_t get_offset_of_m_Item4_3() { return static_cast<int32_t>(offsetof(Tuple_4_t1664179553, ___m_Item4_3)); }
	inline int32_t get_m_Item4_3() const { return ___m_Item4_3; }
	inline int32_t* get_address_of_m_Item4_3() { return &___m_Item4_3; }
	inline void set_m_Item4_3(int32_t value)
	{
		___m_Item4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
