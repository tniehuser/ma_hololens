﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.MemberHolder
struct  MemberHolder_t3720434074  : public Il2CppObject
{
public:
	// System.Type System.Runtime.Serialization.MemberHolder::memberType
	Type_t * ___memberType_0;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.MemberHolder::context
	StreamingContext_t1417235061  ___context_1;

public:
	inline static int32_t get_offset_of_memberType_0() { return static_cast<int32_t>(offsetof(MemberHolder_t3720434074, ___memberType_0)); }
	inline Type_t * get_memberType_0() const { return ___memberType_0; }
	inline Type_t ** get_address_of_memberType_0() { return &___memberType_0; }
	inline void set_memberType_0(Type_t * value)
	{
		___memberType_0 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_0, value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(MemberHolder_t3720434074, ___context_1)); }
	inline StreamingContext_t1417235061  get_context_1() const { return ___context_1; }
	inline StreamingContext_t1417235061 * get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(StreamingContext_t1417235061  value)
	{
		___context_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
