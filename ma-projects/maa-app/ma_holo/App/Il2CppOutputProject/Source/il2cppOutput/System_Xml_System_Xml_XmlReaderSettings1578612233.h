﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_System_Xml_DtdProcessing2734370679.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t313318308;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_t1578612233  : public Il2CppObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::useAsync
	bool ___useAsync_0;
	// System.Xml.XmlNameTable System.Xml.XmlReaderSettings::nameTable
	XmlNameTable_t1345805268 * ___nameTable_1;
	// System.Xml.XmlResolver System.Xml.XmlReaderSettings::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_2;
	// System.Int32 System.Xml.XmlReaderSettings::lineNumberOffset
	int32_t ___lineNumberOffset_3;
	// System.Int32 System.Xml.XmlReaderSettings::linePositionOffset
	int32_t ___linePositionOffset_4;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformanceLevel
	int32_t ___conformanceLevel_5;
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_6;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_7;
	// System.Int64 System.Xml.XmlReaderSettings::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_8;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreWhitespace
	bool ___ignoreWhitespace_9;
	// System.Boolean System.Xml.XmlReaderSettings::ignorePIs
	bool ___ignorePIs_10;
	// System.Boolean System.Xml.XmlReaderSettings::ignoreComments
	bool ___ignoreComments_11;
	// System.Xml.DtdProcessing System.Xml.XmlReaderSettings::dtdProcessing
	int32_t ___dtdProcessing_12;
	// System.Xml.ValidationType System.Xml.XmlReaderSettings::validationType
	int32_t ___validationType_13;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.XmlReaderSettings::validationFlags
	int32_t ___validationFlags_14;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::schemas
	XmlSchemaSet_t313318308 * ___schemas_15;
	// System.Boolean System.Xml.XmlReaderSettings::closeInput
	bool ___closeInput_16;
	// System.Boolean System.Xml.XmlReaderSettings::isReadOnly
	bool ___isReadOnly_17;
	// System.Boolean System.Xml.XmlReaderSettings::<IsXmlResolverSet>k__BackingField
	bool ___U3CIsXmlResolverSetU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___nameTable_1)); }
	inline XmlNameTable_t1345805268 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t1345805268 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_1, value);
	}

	inline static int32_t get_offset_of_xmlResolver_2() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___xmlResolver_2)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_2() const { return ___xmlResolver_2; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_2() { return &___xmlResolver_2; }
	inline void set_xmlResolver_2(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_2 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_2, value);
	}

	inline static int32_t get_offset_of_lineNumberOffset_3() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___lineNumberOffset_3)); }
	inline int32_t get_lineNumberOffset_3() const { return ___lineNumberOffset_3; }
	inline int32_t* get_address_of_lineNumberOffset_3() { return &___lineNumberOffset_3; }
	inline void set_lineNumberOffset_3(int32_t value)
	{
		___lineNumberOffset_3 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_4() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___linePositionOffset_4)); }
	inline int32_t get_linePositionOffset_4() const { return ___linePositionOffset_4; }
	inline int32_t* get_address_of_linePositionOffset_4() { return &___linePositionOffset_4; }
	inline void set_linePositionOffset_4(int32_t value)
	{
		___linePositionOffset_4 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_5() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___conformanceLevel_5)); }
	inline int32_t get_conformanceLevel_5() const { return ___conformanceLevel_5; }
	inline int32_t* get_address_of_conformanceLevel_5() { return &___conformanceLevel_5; }
	inline void set_conformanceLevel_5(int32_t value)
	{
		___conformanceLevel_5 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_6() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___checkCharacters_6)); }
	inline bool get_checkCharacters_6() const { return ___checkCharacters_6; }
	inline bool* get_address_of_checkCharacters_6() { return &___checkCharacters_6; }
	inline void set_checkCharacters_6(bool value)
	{
		___checkCharacters_6 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_7() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___maxCharactersInDocument_7)); }
	inline int64_t get_maxCharactersInDocument_7() const { return ___maxCharactersInDocument_7; }
	inline int64_t* get_address_of_maxCharactersInDocument_7() { return &___maxCharactersInDocument_7; }
	inline void set_maxCharactersInDocument_7(int64_t value)
	{
		___maxCharactersInDocument_7 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_8() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___maxCharactersFromEntities_8)); }
	inline int64_t get_maxCharactersFromEntities_8() const { return ___maxCharactersFromEntities_8; }
	inline int64_t* get_address_of_maxCharactersFromEntities_8() { return &___maxCharactersFromEntities_8; }
	inline void set_maxCharactersFromEntities_8(int64_t value)
	{
		___maxCharactersFromEntities_8 = value;
	}

	inline static int32_t get_offset_of_ignoreWhitespace_9() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___ignoreWhitespace_9)); }
	inline bool get_ignoreWhitespace_9() const { return ___ignoreWhitespace_9; }
	inline bool* get_address_of_ignoreWhitespace_9() { return &___ignoreWhitespace_9; }
	inline void set_ignoreWhitespace_9(bool value)
	{
		___ignoreWhitespace_9 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_10() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___ignorePIs_10)); }
	inline bool get_ignorePIs_10() const { return ___ignorePIs_10; }
	inline bool* get_address_of_ignorePIs_10() { return &___ignorePIs_10; }
	inline void set_ignorePIs_10(bool value)
	{
		___ignorePIs_10 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_11() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___ignoreComments_11)); }
	inline bool get_ignoreComments_11() const { return ___ignoreComments_11; }
	inline bool* get_address_of_ignoreComments_11() { return &___ignoreComments_11; }
	inline void set_ignoreComments_11(bool value)
	{
		___ignoreComments_11 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_12() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___dtdProcessing_12)); }
	inline int32_t get_dtdProcessing_12() const { return ___dtdProcessing_12; }
	inline int32_t* get_address_of_dtdProcessing_12() { return &___dtdProcessing_12; }
	inline void set_dtdProcessing_12(int32_t value)
	{
		___dtdProcessing_12 = value;
	}

	inline static int32_t get_offset_of_validationType_13() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___validationType_13)); }
	inline int32_t get_validationType_13() const { return ___validationType_13; }
	inline int32_t* get_address_of_validationType_13() { return &___validationType_13; }
	inline void set_validationType_13(int32_t value)
	{
		___validationType_13 = value;
	}

	inline static int32_t get_offset_of_validationFlags_14() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___validationFlags_14)); }
	inline int32_t get_validationFlags_14() const { return ___validationFlags_14; }
	inline int32_t* get_address_of_validationFlags_14() { return &___validationFlags_14; }
	inline void set_validationFlags_14(int32_t value)
	{
		___validationFlags_14 = value;
	}

	inline static int32_t get_offset_of_schemas_15() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___schemas_15)); }
	inline XmlSchemaSet_t313318308 * get_schemas_15() const { return ___schemas_15; }
	inline XmlSchemaSet_t313318308 ** get_address_of_schemas_15() { return &___schemas_15; }
	inline void set_schemas_15(XmlSchemaSet_t313318308 * value)
	{
		___schemas_15 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_15, value);
	}

	inline static int32_t get_offset_of_closeInput_16() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___closeInput_16)); }
	inline bool get_closeInput_16() const { return ___closeInput_16; }
	inline bool* get_address_of_closeInput_16() { return &___closeInput_16; }
	inline void set_closeInput_16(bool value)
	{
		___closeInput_16 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_17() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___isReadOnly_17)); }
	inline bool get_isReadOnly_17() const { return ___isReadOnly_17; }
	inline bool* get_address_of_isReadOnly_17() { return &___isReadOnly_17; }
	inline void set_isReadOnly_17(bool value)
	{
		___isReadOnly_17 = value;
	}

	inline static int32_t get_offset_of_U3CIsXmlResolverSetU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233, ___U3CIsXmlResolverSetU3Ek__BackingField_18)); }
	inline bool get_U3CIsXmlResolverSetU3Ek__BackingField_18() const { return ___U3CIsXmlResolverSetU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsXmlResolverSetU3Ek__BackingField_18() { return &___U3CIsXmlResolverSetU3Ek__BackingField_18; }
	inline void set_U3CIsXmlResolverSetU3Ek__BackingField_18(bool value)
	{
		___U3CIsXmlResolverSetU3Ek__BackingField_18 = value;
	}
};

struct XmlReaderSettings_t1578612233_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> System.Xml.XmlReaderSettings::s_enableLegacyXmlSettings
	Nullable_1_t2088641033  ___s_enableLegacyXmlSettings_19;

public:
	inline static int32_t get_offset_of_s_enableLegacyXmlSettings_19() { return static_cast<int32_t>(offsetof(XmlReaderSettings_t1578612233_StaticFields, ___s_enableLegacyXmlSettings_19)); }
	inline Nullable_1_t2088641033  get_s_enableLegacyXmlSettings_19() const { return ___s_enableLegacyXmlSettings_19; }
	inline Nullable_1_t2088641033 * get_address_of_s_enableLegacyXmlSettings_19() { return &___s_enableLegacyXmlSettings_19; }
	inline void set_s_enableLegacyXmlSettings_19(Nullable_1_t2088641033  value)
	{
		___s_enableLegacyXmlSettings_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
