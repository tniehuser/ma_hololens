﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare666240413.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeType2836228502.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_ByteEqualityCo1095452717.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3735343258.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeRef2092605142.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_ExceptionResource2812258639.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2399209989.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality1173345538.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2256739707.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality1030875256.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3561808236.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality2335943785.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4231591473.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3005727022.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3593303644.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality2367439193.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2651650952.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality1425786501.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1107236864.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality4176339709.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2614881185.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality1389016734.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare645512719.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3714615564.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3777680604.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality2551816153.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1263084566.h"
#include "mscorlib_System_Collections_Generic_ObjectEqualityCo37220115.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare730026155.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3799129000.h"
#include "mscorlib_System_Resources_ResourceLocator2156390884.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3323020116.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality2097155665.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare650145203.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3719248048.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare991894998.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality4060997843.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2003894220.h"
#include "mscorlib_System_Collections_Generic_ObjectEqualityC778029769.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3855485178.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality2629620727.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare723317292.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality3792420137.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1482832185.h"
#include "mscorlib_System_Collections_Generic_ObjectEqualityC256967734.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1354438193.h"
#include "mscorlib_System_Collections_Generic_ObjectEqualityC128573742.h"
#include "System_Xml_System_Xml_Schema_RangePositionInfo2780802922.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1084221781.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality4153324626.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2510586510.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2479386620.h"
#include "mscorlib_System_Collections_Generic_ObjectEquality1253522169.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Collections_Generic_GenericCompare1563550222.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2715583837.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_GenericCompare1421079940.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555.h"
#include "mscorlib_System_Collections_Generic_GenericCompare2726148469.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084.h"
#include "mscorlib_System_Collections_Generic_GenericCompare3395931706.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen252998025.h"
#include "mscorlib_System_Collections_Generic_GenericCompare2757643877.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3909677492.h"
#include "mscorlib_System_Collections_Generic_GenericCompare1815991185.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2968024800.h"
#include "mscorlib_System_Collections_Generic_GenericCompare1779221418.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2931255033.h"
#include "mscorlib_System_Collections_Generic_GenericCompare4104820248.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "mscorlib_System_Collections_Generic_GenericCompare2942020837.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4094054452.h"
#include "mscorlib_System_Collections_Generic_GenericComparer427424799.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Collections_Generic_GenericCompare2487360349.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3639393964.h"
#include "mscorlib_System_Collections_Generic_GenericCompare4109452732.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen966519051.h"
#include "mscorlib_System_Collections_Generic_GenericCompare1168234453.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068.h"
#include "mscorlib_System_Collections_Generic_GenericCompare3019825411.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4171859026.h"
#include "mscorlib_System_Collections_Generic_GenericCompare4182624821.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1039691140.h"
#include "mscorlib_System_Collections_Generic_GenericComparer647172418.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1799206033.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit2809456854.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit2666986572.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit3972055101.h"
#include "mscorlib_System_Collections_Generic_GenericEquality346871042.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit4003550509.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit3061897817.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1517483729.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit3025128050.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1055759584.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit4187927469.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1673331431.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit3733266981.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1060392068.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit2414141085.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit4265732043.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1133564157.h"
#include "mscorlib_System_Collections_Generic_GenericEqualit1893079050.h"

// System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>
struct EqualityComparer_1_t666240413;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.ByteEqualityComparer
struct ByteEqualityComparer_t1095452717;
// System.RuntimeType
struct RuntimeType_t2836228502;
// MS.Internal.Xml.Cache.XPathNodeRef[]
struct XPathNodeRefU5BU5D_t1361399059;
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t2399209989;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t2256739707;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t3561808236;
// System.DateTime[]
struct DateTimeU5BU5D_t3111277864;
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct EqualityComparer_1_t4231591473;
// System.DateTimeOffset[]
struct DateTimeOffsetU5BU5D_t435078063;
// System.Collections.Generic.EqualityComparer`1<System.Decimal>
struct EqualityComparer_1_t3593303644;
// System.Decimal[]
struct DecimalU5BU5D_t624008824;
// System.Collections.Generic.EqualityComparer`1<System.Double>
struct EqualityComparer_1_t2651650952;
// System.Double[]
struct DoubleU5BU5D_t1889952540;
// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct EqualityComparer_1_t1107236864;
// System.Guid[]
struct GuidU5BU5D_t3556289988;
// System.Collections.Generic.EqualityComparer`1<System.Int16>
struct EqualityComparer_1_t2614881185;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t645512719;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.EqualityComparer`1<System.Int64>
struct EqualityComparer_1_t3777680604;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t1263084566;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>
struct EqualityComparer_1_t730026155;
// System.Resources.ResourceLocator[]
struct ResourceLocatorU5BU5D_t1324310029;
// System.Collections.Generic.EqualityComparer`1<System.SByte>
struct EqualityComparer_1_t3323020116;
// System.SByte[]
struct SByteU5BU5D_t3472287392;
// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t650145203;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>
struct EqualityComparer_1_t991894998;
// System.Text.RegularExpressions.RegexOptions[]
struct RegexOptionsU5BU5D_t1359681494;
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t2003894220;
// System.TimeSpan[]
struct TimeSpanU5BU5D_t1313935688;
// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct EqualityComparer_1_t3855485178;
// System.UInt16[]
struct UInt16U5BU5D_t2527266722;
// System.Collections.Generic.EqualityComparer`1<System.UInt32>
struct EqualityComparer_1_t723317292;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct EqualityComparer_1_t1482832185;
// System.UInt64[]
struct UInt64U5BU5D_t1668688775;
// System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>
struct EqualityComparer_1_t1354438193;
// System.Xml.Schema.RangePositionInfo[]
struct RangePositionInfoU5BU5D_t4253033391;
// System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct EqualityComparer_1_t1084221781;
// System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry[]
struct XmlSchemaObjectEntryU5BU5D_t621120379;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>
struct EqualityComparer_1_t2479386620;
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t2969332312;
// System.Collections.Generic.GenericComparer`1<System.Boolean>
struct GenericComparer_1_t1563550222;
// System.Collections.Generic.GenericComparer`1<System.Byte>
struct GenericComparer_1_t1421079940;
// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2726148469;
// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t3395931706;
// System.Collections.Generic.GenericComparer`1<System.Decimal>
struct GenericComparer_1_t2757643877;
// System.Collections.Generic.GenericComparer`1<System.Double>
struct GenericComparer_1_t1815991185;
// System.Collections.Generic.GenericComparer`1<System.Int16>
struct GenericComparer_1_t1779221418;
// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t4104820248;
// System.Collections.Generic.GenericComparer`1<System.Int64>
struct GenericComparer_1_t2942020837;
// System.Collections.Generic.GenericComparer`1<System.Object>
struct GenericComparer_1_t427424799;
// System.Collections.Generic.GenericComparer`1<System.SByte>
struct GenericComparer_1_t2487360349;
// System.Collections.Generic.GenericComparer`1<System.Single>
struct GenericComparer_1_t4109452732;
// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t1168234453;
// System.Collections.Generic.GenericComparer`1<System.UInt16>
struct GenericComparer_1_t3019825411;
// System.Collections.Generic.GenericComparer`1<System.UInt32>
struct GenericComparer_1_t4182624821;
// System.Collections.Generic.GenericComparer`1<System.UInt64>
struct GenericComparer_1_t647172418;
// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t2809456854;
// System.Collections.Generic.GenericEqualityComparer`1<System.Byte>
struct GenericEqualityComparer_1_t2666986572;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t3972055101;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t346871042;
// System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>
struct GenericEqualityComparer_1_t4003550509;
// System.Collections.Generic.GenericEqualityComparer`1<System.Double>
struct GenericEqualityComparer_1_t3061897817;
// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t1517483729;
// System.Collections.Generic.GenericEqualityComparer`1<System.Int16>
struct GenericEqualityComparer_1_t3025128050;
// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t1055759584;
// System.Collections.Generic.GenericEqualityComparer`1<System.Int64>
struct GenericEqualityComparer_1_t4187927469;
// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct GenericEqualityComparer_1_t1673331431;
// System.Collections.Generic.GenericEqualityComparer`1<System.SByte>
struct GenericEqualityComparer_1_t3733266981;
// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t1060392068;
// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t2414141085;
// System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>
struct GenericEqualityComparer_1_t4265732043;
// System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>
struct GenericEqualityComparer_1_t1133564157;
// System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>
struct GenericEqualityComparer_1_t1893079050;
extern const Il2CppType* Byte_t3683104436_0_0_0_var;
extern const Il2CppType* GenericEqualityComparer_1_t2202941003_0_0_0_var;
extern const Il2CppType* Nullable_1_t1398937014_0_0_0_var;
extern const Il2CppType* IEquatable_1_t2913394932_0_0_0_var;
extern const Il2CppType* NullableEqualityComparer_1_t1847642941_0_0_0_var;
extern const Il2CppType* ShortEnumEqualityComparer_1_t2311249405_0_0_0_var;
extern const Il2CppType* SByteEnumEqualityComparer_1_t1634080424_0_0_0_var;
extern const Il2CppType* EnumEqualityComparer_1_t343233057_0_0_0_var;
extern const Il2CppType* LongEnumEqualityComparer_1_t2298570595_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeType_t2836228502_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1_CreateComparer_m3014891628_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m3311856537_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m3811803183_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1952975778_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m214398509_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m3502120454_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m191238502_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m2390620562_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m3577966597_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m633606899_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1567147598_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1302575724_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m175496384_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1489375786_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1544170251_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m542703678_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m2134235622_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m587440072_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m2390196090_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m798414069_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m1656190225_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m4116535204_MetadataUsageId;
extern const uint32_t EqualityComparer_1_CreateComparer_m4000786850_MetadataUsageId;

// System.Type[]
struct TypeU5BU5D_t1664964607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MS.Internal.Xml.Cache.XPathNodeRef[]
struct XPathNodeRefU5BU5D_t1361399059  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) XPathNodeRef_t2092605142  m_Items[1];

public:
	inline XPathNodeRef_t2092605142  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline XPathNodeRef_t2092605142 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, XPathNodeRef_t2092605142  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline XPathNodeRef_t2092605142  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline XPathNodeRef_t2092605142 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, XPathNodeRef_t2092605142  value)
	{
		m_Items[index] = value;
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t3568034315  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.DateTime[]
struct DateTimeU5BU5D_t3111277864  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DateTime_t693205669  m_Items[1];

public:
	inline DateTime_t693205669  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DateTime_t693205669 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DateTime_t693205669  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DateTime_t693205669  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DateTime_t693205669 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DateTime_t693205669  value)
	{
		m_Items[index] = value;
	}
};
// System.DateTimeOffset[]
struct DateTimeOffsetU5BU5D_t435078063  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DateTimeOffset_t1362988906  m_Items[1];

public:
	inline DateTimeOffset_t1362988906  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DateTimeOffset_t1362988906 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DateTimeOffset_t1362988906  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline DateTimeOffset_t1362988906  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DateTimeOffset_t1362988906 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DateTimeOffset_t1362988906  value)
	{
		m_Items[index] = value;
	}
};
// System.Decimal[]
struct DecimalU5BU5D_t624008824  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Decimal_t724701077  m_Items[1];

public:
	inline Decimal_t724701077  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Decimal_t724701077 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Decimal_t724701077  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Decimal_t724701077  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Decimal_t724701077 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Decimal_t724701077  value)
	{
		m_Items[index] = value;
	}
};
// System.Double[]
struct DoubleU5BU5D_t1889952540  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) double m_Items[1];

public:
	inline double GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline double* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline double GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline double* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.Guid[]
struct GuidU5BU5D_t3556289988  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Guid_t  m_Items[1];

public:
	inline Guid_t  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Guid_t * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Guid_t  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Guid_t  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Guid_t * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Guid_t  value)
	{
		m_Items[index] = value;
	}
};
// System.Int16[]
struct Int16U5BU5D_t3104283263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_t717125112  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Resources.ResourceLocator[]
struct ResourceLocatorU5BU5D_t1324310029  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResourceLocator_t2156390884  m_Items[1];

public:
	inline ResourceLocator_t2156390884  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ResourceLocator_t2156390884 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ResourceLocator_t2156390884  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ResourceLocator_t2156390884  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ResourceLocator_t2156390884 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ResourceLocator_t2156390884  value)
	{
		m_Items[index] = value;
	}
};
// System.SByte[]
struct SByteU5BU5D_t3472287392  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int8_t m_Items[1];

public:
	inline int8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t577127397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Text.RegularExpressions.RegexOptions[]
struct RegexOptionsU5BU5D_t1359681494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.TimeSpan[]
struct TimeSpanU5BU5D_t1313935688  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TimeSpan_t3430258949  m_Items[1];

public:
	inline TimeSpan_t3430258949  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TimeSpan_t3430258949 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TimeSpan_t3430258949  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TimeSpan_t3430258949  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TimeSpan_t3430258949 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TimeSpan_t3430258949  value)
	{
		m_Items[index] = value;
	}
};
// System.UInt16[]
struct UInt16U5BU5D_t2527266722  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t59386216  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt64[]
struct UInt64U5BU5D_t1668688775  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint64_t m_Items[1];

public:
	inline uint64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Xml.Schema.RangePositionInfo[]
struct RangePositionInfoU5BU5D_t4253033391  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RangePositionInfo_t2780802922  m_Items[1];

public:
	inline RangePositionInfo_t2780802922  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RangePositionInfo_t2780802922 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RangePositionInfo_t2780802922  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RangePositionInfo_t2780802922  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RangePositionInfo_t2780802922 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RangePositionInfo_t2780802922  value)
	{
		m_Items[index] = value;
	}
};
// System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry[]
struct XmlSchemaObjectEntryU5BU5D_t621120379  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) XmlSchemaObjectEntry_t2510586510  m_Items[1];

public:
	inline XmlSchemaObjectEntry_t2510586510  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline XmlSchemaObjectEntry_t2510586510 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, XmlSchemaObjectEntry_t2510586510  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline XmlSchemaObjectEntry_t2510586510  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline XmlSchemaObjectEntry_t2510586510 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, XmlSchemaObjectEntry_t2510586510  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t2969332312  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t3905751349  m_Items[1];

public:
	inline AnimatorClipInfo_t3905751349  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3905751349 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t3905751349  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline AnimatorClipInfo_t3905751349  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3905751349 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimatorClipInfo_t3905751349  value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
extern "C"  bool Type_op_Equality_m3620493675 (Il2CppObject * __this /* static, unused */, Type_t * ___left0, Type_t * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.ByteEqualityComparer::.ctor()
extern "C"  void ByteEqualityComparer__ctor_m1220141194 (ByteEqualityComparer_t1095452717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.RuntimeType::CreateInstanceForAnotherGenericParameter(System.Type,System.RuntimeType)
extern "C"  Il2CppObject * RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795 (Il2CppObject * __this /* static, unused */, Type_t * ___genericType0, RuntimeType_t2836228502 * ___genericArgument1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Enum::GetUnderlyingType(System.Type)
extern "C"  Type_t * Enum_GetUnderlyingType_m3513899012 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.Type::GetTypeCode(System.Type)
extern "C"  int32_t Type_GetTypeCode_m1044483454 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ThrowHelper::ThrowArgumentException(System.ExceptionResource)
extern "C"  void ThrowHelper_ThrowArgumentException_m1802962943 (Il2CppObject * __this /* static, unused */, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::CompareTo(System.Boolean)
extern "C"  int32_t Boolean_CompareTo_m1086129598 (bool* __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Byte::CompareTo(System.Byte)
extern "C"  int32_t Byte_CompareTo_m1850579028 (uint8_t* __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::CompareTo(System.DateTime)
extern "C"  int32_t DateTime_CompareTo_m1511117942 (DateTime_t693205669 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::CompareTo(System.DateTimeOffset)
extern "C"  int32_t DateTimeOffset_CompareTo_m441053436 (DateTimeOffset_t1362988906 * __this, DateTimeOffset_t1362988906  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::CompareTo(System.Decimal)
extern "C"  int32_t Decimal_CompareTo_m573986782 (Decimal_t724701077 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Double::CompareTo(System.Double)
extern "C"  int32_t Double_CompareTo_m3968040230 (double* __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int16::CompareTo(System.Int16)
extern "C"  int32_t Int16_CompareTo_m2802154270 (int16_t* __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::CompareTo(System.Int32)
extern "C"  int32_t Int32_CompareTo_m3808534558 (int32_t* __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int64::CompareTo(System.Int64)
extern "C"  int32_t Int64_CompareTo_m1385259998 (int64_t* __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.SByte::CompareTo(System.SByte)
extern "C"  int32_t SByte_CompareTo_m3157383966 (int8_t* __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::CompareTo(System.Single)
extern "C"  int32_t Single_CompareTo_m1534635028 (float* __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern "C"  int32_t TimeSpan_CompareTo_m4183101766 (TimeSpan_t3430258949 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.UInt16)
extern "C"  int32_t UInt16_CompareTo_m1703646358 (uint16_t* __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt32::CompareTo(System.UInt32)
extern "C"  int32_t UInt32_CompareTo_m2313773902 (uint32_t* __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt64::CompareTo(System.UInt64)
extern "C"  int32_t UInt64_CompareTo_m2422461804 (uint64_t* __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Boolean)
extern "C"  bool Boolean_Equals_m294106711 (bool* __this, bool ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1894638460 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Byte::Equals(System.Byte)
extern "C"  bool Byte_Equals_m368616327 (uint8_t* __this, uint8_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Byte::GetHashCode()
extern "C"  int32_t Byte_GetHashCode_m2885528842 (uint8_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.DateTime)
extern "C"  bool DateTime_Equals_m1104060551 (DateTime_t693205669 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m974799321 (DateTime_t693205669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.DateTimeOffset)
extern "C"  bool DateTimeOffset_Equals_m3728302791 (DateTimeOffset_t1362988906 * __this, DateTimeOffset_t1362988906  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::GetHashCode()
extern "C"  int32_t DateTimeOffset_GetHashCode_m3312197462 (DateTimeOffset_t1362988906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::Equals(System.Decimal)
extern "C"  bool Decimal_Equals_m1115043331 (Decimal_t724701077 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::GetHashCode()
extern "C"  int32_t Decimal_GetHashCode_m703641627 (Decimal_t724701077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Double::Equals(System.Double)
extern "C"  bool Double_Equals_m920556135 (double* __this, double ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Double::GetHashCode()
extern "C"  int32_t Double_GetHashCode_m3403732029 (double* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::Equals(System.Guid)
extern "C"  bool Guid_Equals_m2389236871 (Guid_t * __this, Guid_t  ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::GetHashCode()
extern "C"  int32_t Guid_GetHashCode_m1401300871 (Guid_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int16::Equals(System.Int16)
extern "C"  bool Int16_Equals_m920337175 (int16_t* __this, int16_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int16::GetHashCode()
extern "C"  int32_t Int16_GetHashCode_m304503362 (int16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Int32)
extern "C"  bool Int32_Equals_m321398519 (int32_t* __this, int32_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m1381647448 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int64::Equals(System.Int64)
extern "C"  bool Int64_Equals_m2821075083 (int64_t* __this, int64_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int64::GetHashCode()
extern "C"  int32_t Int64_GetHashCode_m4047499913 (int64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.SByte::Equals(System.SByte)
extern "C"  bool SByte_Equals_m2330990811 (int8_t* __this, int8_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.SByte::GetHashCode()
extern "C"  int32_t SByte_GetHashCode_m1692727069 (int8_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m3359827399 (float* __this, float ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3102305584 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern "C"  bool TimeSpan_Equals_m2029123271 (TimeSpan_t3430258949 * __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m550404245 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.UInt16)
extern "C"  bool UInt16_Equals_m4201490279 (uint16_t* __this, uint16_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::GetHashCode()
extern "C"  int32_t UInt16_GetHashCode_m1468226569 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt32::Equals(System.UInt32)
extern "C"  bool UInt32_Equals_m787945383 (uint32_t* __this, uint32_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt32::GetHashCode()
extern "C"  int32_t UInt32_GetHashCode_m2903162199 (uint32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt64::Equals(System.UInt64)
extern "C"  bool UInt64_Equals_m1977009287 (uint64_t* __this, uint64_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt64::GetHashCode()
extern "C"  int32_t UInt64_GetHashCode_m3478338766 (uint64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3929439349_gshared (EqualityComparer_1_t666240413 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::get_Default()
extern "C"  EqualityComparer_1_t666240413 * EqualityComparer_1_get_Default_m368307481_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t666240413 * V_0 = NULL;
	{
		EqualityComparer_1_t666240413 * L_0 = ((EqualityComparer_1_t666240413_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t666240413 *)L_0;
		EqualityComparer_1_t666240413 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t666240413 * L_2 = ((  EqualityComparer_1_t666240413 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t666240413 *)L_2;
		EqualityComparer_1_t666240413 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t666240413_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t666240413 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::CreateComparer()
extern "C"  EqualityComparer_1_t666240413 * EqualityComparer_1_CreateComparer_m3014891628_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m3014891628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t666240413 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3735343258 * L_48 = (ObjectEqualityComparer_1_t3735343258 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3735343258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1412358068_gshared (EqualityComparer_1_t666240413 * __this, XPathNodeRefU5BU5D_t1361399059* ___array0, XPathNodeRef_t2092605142  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		XPathNodeRefU5BU5D_t1361399059* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		XPathNodeRef_t2092605142  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		XPathNodeRef_t2092605142  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t666240413 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, XPathNodeRef_t2092605142 , XPathNodeRef_t2092605142  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::Equals(T,T) */, (EqualityComparer_1_t666240413 *)__this, (XPathNodeRef_t2092605142 )L_6, (XPathNodeRef_t2092605142 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1949659206_gshared (EqualityComparer_1_t666240413 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t666240413 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, XPathNodeRef_t2092605142  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::GetHashCode(T) */, (EqualityComparer_1_t666240413 *)__this, (XPathNodeRef_t2092605142 )((*(XPathNodeRef_t2092605142 *)((XPathNodeRef_t2092605142 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m54466572_gshared (EqualityComparer_1_t666240413 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t666240413 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, XPathNodeRef_t2092605142 , XPathNodeRef_t2092605142  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<MS.Internal.Xml.Cache.XPathNodeRef>::Equals(T,T) */, (EqualityComparer_1_t666240413 *)__this, (XPathNodeRef_t2092605142 )((*(XPathNodeRef_t2092605142 *)((XPathNodeRef_t2092605142 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (XPathNodeRef_t2092605142 )((*(XPathNodeRef_t2092605142 *)((XPathNodeRef_t2092605142 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1952047100_gshared (EqualityComparer_1_t2399209989 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::get_Default()
extern "C"  EqualityComparer_1_t2399209989 * EqualityComparer_1_get_Default_m3911577264_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2399209989 * V_0 = NULL;
	{
		EqualityComparer_1_t2399209989 * L_0 = ((EqualityComparer_1_t2399209989_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2399209989 *)L_0;
		EqualityComparer_1_t2399209989 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2399209989 * L_2 = ((  EqualityComparer_1_t2399209989 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2399209989 *)L_2;
		EqualityComparer_1_t2399209989 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2399209989_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2399209989 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::CreateComparer()
extern "C"  EqualityComparer_1_t2399209989 * EqualityComparer_1_CreateComparer_m3311856537_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m3311856537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2399209989 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t1173345538 * L_48 = (ObjectEqualityComparer_1_t1173345538 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t1173345538 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m3861564469_gshared (EqualityComparer_1_t2399209989 * __this, BooleanU5BU5D_t3568034315* ___array0, bool ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		BooleanU5BU5D_t3568034315* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		bool L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		bool L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2399209989 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, bool, bool >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(T,T) */, (EqualityComparer_1_t2399209989 *)__this, (bool)L_6, (bool)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3901093757_gshared (EqualityComparer_1_t2399209989 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2399209989 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, bool >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(T) */, (EqualityComparer_1_t2399209989 *)__this, (bool)((*(bool*)((bool*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3134072983_gshared (EqualityComparer_1_t2399209989 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2399209989 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, bool, bool >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(T,T) */, (EqualityComparer_1_t2399209989 *)__this, (bool)((*(bool*)((bool*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (bool)((*(bool*)((bool*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1913708920_gshared (EqualityComparer_1_t2256739707 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::get_Default()
extern "C"  EqualityComparer_1_t2256739707 * EqualityComparer_1_get_Default_m1180152772_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2256739707 * V_0 = NULL;
	{
		EqualityComparer_1_t2256739707 * L_0 = ((EqualityComparer_1_t2256739707_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2256739707 *)L_0;
		EqualityComparer_1_t2256739707 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2256739707 * L_2 = ((  EqualityComparer_1_t2256739707 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2256739707 *)L_2;
		EqualityComparer_1_t2256739707 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2256739707_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2256739707 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::CreateComparer()
extern "C"  EqualityComparer_1_t2256739707 * EqualityComparer_1_CreateComparer_m3811803183_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m3811803183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2256739707 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t1030875256 * L_48 = (ObjectEqualityComparer_1_t1030875256 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t1030875256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1857871447_gshared (EqualityComparer_1_t2256739707 * __this, ByteU5BU5D_t3397334013* ___array0, uint8_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		ByteU5BU5D_t3397334013* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		uint8_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2256739707 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::Equals(T,T) */, (EqualityComparer_1_t2256739707 *)__this, (uint8_t)L_6, (uint8_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m589434723_gshared (EqualityComparer_1_t2256739707 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2256739707 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, uint8_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::GetHashCode(T) */, (EqualityComparer_1_t2256739707 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3048721017_gshared (EqualityComparer_1_t2256739707 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2256739707 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::Equals(T,T) */, (EqualityComparer_1_t2256739707 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1389939323_gshared (EqualityComparer_1_t3561808236 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::get_Default()
extern "C"  EqualityComparer_1_t3561808236 * EqualityComparer_1_get_Default_m2183586459_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t3561808236 * V_0 = NULL;
	{
		EqualityComparer_1_t3561808236 * L_0 = ((EqualityComparer_1_t3561808236_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t3561808236 *)L_0;
		EqualityComparer_1_t3561808236 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t3561808236 * L_2 = ((  EqualityComparer_1_t3561808236 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t3561808236 *)L_2;
		EqualityComparer_1_t3561808236 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t3561808236_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t3561808236 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::CreateComparer()
extern "C"  EqualityComparer_1_t3561808236 * EqualityComparer_1_CreateComparer_m1952975778_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1952975778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3561808236 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t2335943785 * L_48 = (ObjectEqualityComparer_1_t2335943785 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t2335943785 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1378334414_gshared (EqualityComparer_1_t3561808236 * __this, DateTimeU5BU5D_t3111277864* ___array0, DateTime_t693205669  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		DateTimeU5BU5D_t3111277864* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		DateTime_t693205669  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		DateTime_t693205669  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t3561808236 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, DateTime_t693205669 , DateTime_t693205669  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(T,T) */, (EqualityComparer_1_t3561808236 *)__this, (DateTime_t693205669 )L_6, (DateTime_t693205669 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m438492364_gshared (EqualityComparer_1_t3561808236 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t3561808236 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, DateTime_t693205669  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::GetHashCode(T) */, (EqualityComparer_1_t3561808236 *)__this, (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1565968086_gshared (EqualityComparer_1_t3561808236 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t3561808236 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, DateTime_t693205669 , DateTime_t693205669  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(T,T) */, (EqualityComparer_1_t3561808236 *)__this, (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3067713332_gshared (EqualityComparer_1_t4231591473 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::get_Default()
extern "C"  EqualityComparer_1_t4231591473 * EqualityComparer_1_get_Default_m1225763480_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t4231591473 * V_0 = NULL;
	{
		EqualityComparer_1_t4231591473 * L_0 = ((EqualityComparer_1_t4231591473_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t4231591473 *)L_0;
		EqualityComparer_1_t4231591473 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t4231591473 * L_2 = ((  EqualityComparer_1_t4231591473 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t4231591473 *)L_2;
		EqualityComparer_1_t4231591473 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t4231591473_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t4231591473 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::CreateComparer()
extern "C"  EqualityComparer_1_t4231591473 * EqualityComparer_1_CreateComparer_m214398509_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m214398509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t4231591473 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3005727022 * L_48 = (ObjectEqualityComparer_1_t3005727022 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3005727022 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m478938625_gshared (EqualityComparer_1_t4231591473 * __this, DateTimeOffsetU5BU5D_t435078063* ___array0, DateTimeOffset_t1362988906  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		DateTimeOffsetU5BU5D_t435078063* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		DateTimeOffset_t1362988906  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		DateTimeOffset_t1362988906  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t4231591473 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, DateTimeOffset_t1362988906 , DateTimeOffset_t1362988906  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::Equals(T,T) */, (EqualityComparer_1_t4231591473 *)__this, (DateTimeOffset_t1362988906 )L_6, (DateTimeOffset_t1362988906 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1203798961_gshared (EqualityComparer_1_t4231591473 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t4231591473 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, DateTimeOffset_t1362988906  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::GetHashCode(T) */, (EqualityComparer_1_t4231591473 *)__this, (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2542582691_gshared (EqualityComparer_1_t4231591473 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t4231591473 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, DateTimeOffset_t1362988906 , DateTimeOffset_t1362988906  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::Equals(T,T) */, (EqualityComparer_1_t4231591473 *)__this, (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Decimal>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3330910501_gshared (EqualityComparer_1_t3593303644 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Decimal>::get_Default()
extern "C"  EqualityComparer_1_t3593303644 * EqualityComparer_1_get_Default_m1437354633_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t3593303644 * V_0 = NULL;
	{
		EqualityComparer_1_t3593303644 * L_0 = ((EqualityComparer_1_t3593303644_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t3593303644 *)L_0;
		EqualityComparer_1_t3593303644 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t3593303644 * L_2 = ((  EqualityComparer_1_t3593303644 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t3593303644 *)L_2;
		EqualityComparer_1_t3593303644 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t3593303644_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t3593303644 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Decimal>::CreateComparer()
extern "C"  EqualityComparer_1_t3593303644 * EqualityComparer_1_CreateComparer_m3502120454_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m3502120454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3593303644 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t2367439193 * L_48 = (ObjectEqualityComparer_1_t2367439193 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t2367439193 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Decimal>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m3589700382_gshared (EqualityComparer_1_t3593303644 * __this, DecimalU5BU5D_t624008824* ___array0, Decimal_t724701077  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		DecimalU5BU5D_t624008824* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Decimal_t724701077  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Decimal_t724701077  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t3593303644 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Decimal_t724701077 , Decimal_t724701077  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Decimal>::Equals(T,T) */, (EqualityComparer_1_t3593303644 *)__this, (Decimal_t724701077 )L_6, (Decimal_t724701077 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Decimal>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2871903996_gshared (EqualityComparer_1_t3593303644 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t3593303644 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, Decimal_t724701077  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Decimal>::GetHashCode(T) */, (EqualityComparer_1_t3593303644 *)__this, (Decimal_t724701077 )((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Decimal>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3650553910_gshared (EqualityComparer_1_t3593303644 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t3593303644 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Decimal_t724701077 , Decimal_t724701077  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Decimal>::Equals(T,T) */, (EqualityComparer_1_t3593303644 *)__this, (Decimal_t724701077 )((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (Decimal_t724701077 )((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Double>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3098430215_gshared (EqualityComparer_1_t2651650952 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Double>::get_Default()
extern "C"  EqualityComparer_1_t2651650952 * EqualityComparer_1_get_Default_m418567807_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2651650952 * V_0 = NULL;
	{
		EqualityComparer_1_t2651650952 * L_0 = ((EqualityComparer_1_t2651650952_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2651650952 *)L_0;
		EqualityComparer_1_t2651650952 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2651650952 * L_2 = ((  EqualityComparer_1_t2651650952 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2651650952 *)L_2;
		EqualityComparer_1_t2651650952 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2651650952_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2651650952 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Double>::CreateComparer()
extern "C"  EqualityComparer_1_t2651650952 * EqualityComparer_1_CreateComparer_m191238502_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m191238502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2651650952 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t1425786501 * L_48 = (ObjectEqualityComparer_1_t1425786501 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t1425786501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Double>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m71996138_gshared (EqualityComparer_1_t2651650952 * __this, DoubleU5BU5D_t1889952540* ___array0, double ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		DoubleU5BU5D_t1889952540* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		double L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		double L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2651650952 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, double, double >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Double>::Equals(T,T) */, (EqualityComparer_1_t2651650952 *)__this, (double)L_6, (double)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Double>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2714973000_gshared (EqualityComparer_1_t2651650952 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2651650952 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, double >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Double>::GetHashCode(T) */, (EqualityComparer_1_t2651650952 *)__this, (double)((*(double*)((double*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Double>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3640442426_gshared (EqualityComparer_1_t2651650952 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2651650952 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, double, double >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Double>::Equals(T,T) */, (EqualityComparer_1_t2651650952 *)__this, (double)((*(double*)((double*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (double)((*(double*)((double*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2583021089_gshared (EqualityComparer_1_t1107236864 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::get_Default()
extern "C"  EqualityComparer_1_t1107236864 * EqualityComparer_1_get_Default_m875724809_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t1107236864 * V_0 = NULL;
	{
		EqualityComparer_1_t1107236864 * L_0 = ((EqualityComparer_1_t1107236864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t1107236864 *)L_0;
		EqualityComparer_1_t1107236864 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t1107236864 * L_2 = ((  EqualityComparer_1_t1107236864 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t1107236864 *)L_2;
		EqualityComparer_1_t1107236864 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t1107236864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t1107236864 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::CreateComparer()
extern "C"  EqualityComparer_1_t1107236864 * EqualityComparer_1_CreateComparer_m2390620562_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m2390620562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1107236864 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t4176339709 * L_48 = (ObjectEqualityComparer_1_t4176339709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t4176339709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1215716946_gshared (EqualityComparer_1_t1107236864 * __this, GuidU5BU5D_t3556289988* ___array0, Guid_t  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		GuidU5BU5D_t3556289988* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Guid_t  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Guid_t  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t1107236864 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Guid_t , Guid_t  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T) */, (EqualityComparer_1_t1107236864 *)__this, (Guid_t )L_6, (Guid_t )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1465362976_gshared (EqualityComparer_1_t1107236864 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t1107236864 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, Guid_t  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::GetHashCode(T) */, (EqualityComparer_1_t1107236864 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m300683774_gshared (EqualityComparer_1_t1107236864 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t1107236864 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Guid_t , Guid_t  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T) */, (EqualityComparer_1_t1107236864 *)__this, (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (Guid_t )((*(Guid_t *)((Guid_t *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int16>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1396533254_gshared (EqualityComparer_1_t2614881185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int16>::get_Default()
extern "C"  EqualityComparer_1_t2614881185 * EqualityComparer_1_get_Default_m4047747474_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2614881185 * V_0 = NULL;
	{
		EqualityComparer_1_t2614881185 * L_0 = ((EqualityComparer_1_t2614881185_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2614881185 *)L_0;
		EqualityComparer_1_t2614881185 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2614881185 * L_2 = ((  EqualityComparer_1_t2614881185 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2614881185 *)L_2;
		EqualityComparer_1_t2614881185 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2614881185_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2614881185 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int16>::CreateComparer()
extern "C"  EqualityComparer_1_t2614881185 * EqualityComparer_1_CreateComparer_m3577966597_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m3577966597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2614881185 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t1389016734 * L_48 = (ObjectEqualityComparer_1_t1389016734 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t1389016734 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int16>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2863573185_gshared (EqualityComparer_1_t2614881185 * __this, Int16U5BU5D_t3104283263* ___array0, int16_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		Int16U5BU5D_t3104283263* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int16_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2614881185 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int16_t, int16_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int16>::Equals(T,T) */, (EqualityComparer_1_t2614881185 *)__this, (int16_t)L_6, (int16_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int16>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m55248833_gshared (EqualityComparer_1_t2614881185 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2614881185 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int16_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int16>::GetHashCode(T) */, (EqualityComparer_1_t2614881185 *)__this, (int16_t)((*(int16_t*)((int16_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int16>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m590872539_gshared (EqualityComparer_1_t2614881185 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2614881185 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int16_t, int16_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int16>::Equals(T,T) */, (EqualityComparer_1_t2614881185 *)__this, (int16_t)((*(int16_t*)((int16_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (int16_t)((*(int16_t*)((int16_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m376370188_gshared (EqualityComparer_1_t645512719 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::get_Default()
extern "C"  EqualityComparer_1_t645512719 * EqualityComparer_1_get_Default_m3396023804_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t645512719 * V_0 = NULL;
	{
		EqualityComparer_1_t645512719 * L_0 = ((EqualityComparer_1_t645512719_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t645512719 *)L_0;
		EqualityComparer_1_t645512719 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t645512719 * L_2 = ((  EqualityComparer_1_t645512719 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t645512719 *)L_2;
		EqualityComparer_1_t645512719 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t645512719_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t645512719 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::CreateComparer()
extern "C"  EqualityComparer_1_t645512719 * EqualityComparer_1_CreateComparer_m633606899_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m633606899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t645512719 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3714615564 * L_48 = (ObjectEqualityComparer_1_t3714615564 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3714615564 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1217619243_gshared (EqualityComparer_1_t645512719 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		Int32U5BU5D_t3030399641* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t645512719 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T) */, (EqualityComparer_1_t645512719 *)__this, (int32_t)L_6, (int32_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3860410351_gshared (EqualityComparer_1_t645512719 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t645512719 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(T) */, (EqualityComparer_1_t645512719 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3376587337_gshared (EqualityComparer_1_t645512719 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t645512719 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T) */, (EqualityComparer_1_t645512719 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int64>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1847532519_gshared (EqualityComparer_1_t3777680604 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int64>::get_Default()
extern "C"  EqualityComparer_1_t3777680604 * EqualityComparer_1_get_Default_m218671063_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t3777680604 * V_0 = NULL;
	{
		EqualityComparer_1_t3777680604 * L_0 = ((EqualityComparer_1_t3777680604_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t3777680604 *)L_0;
		EqualityComparer_1_t3777680604 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t3777680604 * L_2 = ((  EqualityComparer_1_t3777680604 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t3777680604 *)L_2;
		EqualityComparer_1_t3777680604 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t3777680604_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t3777680604 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int64>::CreateComparer()
extern "C"  EqualityComparer_1_t3777680604 * EqualityComparer_1_CreateComparer_m1567147598_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1567147598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3777680604 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t2551816153 * L_48 = (ObjectEqualityComparer_1_t2551816153 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t2551816153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m1282664754_gshared (EqualityComparer_1_t3777680604 * __this, Int64U5BU5D_t717125112* ___array0, int64_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		Int64U5BU5D_t717125112* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int64_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int64_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t3777680604 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int64_t, int64_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::Equals(T,T) */, (EqualityComparer_1_t3777680604 *)__this, (int64_t)L_6, (int64_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3638358504_gshared (EqualityComparer_1_t3777680604 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t3777680604 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int64_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::GetHashCode(T) */, (EqualityComparer_1_t3777680604 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2904098702_gshared (EqualityComparer_1_t3777680604 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t3777680604 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int64_t, int64_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::Equals(T,T) */, (EqualityComparer_1_t3777680604 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (int64_t)((*(int64_t*)((int64_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1185444131_gshared (EqualityComparer_1_t1263084566 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
extern "C"  EqualityComparer_1_t1263084566 * EqualityComparer_1_get_Default_m1577971315_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t1263084566 * V_0 = NULL;
	{
		EqualityComparer_1_t1263084566 * L_0 = ((EqualityComparer_1_t1263084566_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t1263084566 *)L_0;
		EqualityComparer_1_t1263084566 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t1263084566 *)L_2;
		EqualityComparer_1_t1263084566 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t1263084566_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t1263084566 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::CreateComparer()
extern "C"  EqualityComparer_1_t1263084566 * EqualityComparer_1_CreateComparer_m1302575724_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1302575724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1263084566 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t37220115 * L_48 = (ObjectEqualityComparer_1_t37220115 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t37220115 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m3591552184_gshared (EqualityComparer_1_t1263084566 * __this, ObjectU5BU5D_t3614634134* ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Il2CppObject * L_7 = ___value1;
		NullCheck((EqualityComparer_1_t1263084566 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t1263084566 *)__this, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4285727610_gshared (EqualityComparer_1_t1263084566 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t1263084566 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(T) */, (EqualityComparer_1_t1263084566 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2170611288_gshared (EqualityComparer_1_t1263084566 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t1263084566 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t1263084566 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2027214119_gshared (EqualityComparer_1_t730026155 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::get_Default()
extern "C"  EqualityComparer_1_t730026155 * EqualityComparer_1_get_Default_m3034098135_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t730026155 * V_0 = NULL;
	{
		EqualityComparer_1_t730026155 * L_0 = ((EqualityComparer_1_t730026155_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t730026155 *)L_0;
		EqualityComparer_1_t730026155 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t730026155 * L_2 = ((  EqualityComparer_1_t730026155 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t730026155 *)L_2;
		EqualityComparer_1_t730026155 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t730026155_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t730026155 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::CreateComparer()
extern "C"  EqualityComparer_1_t730026155 * EqualityComparer_1_CreateComparer_m175496384_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m175496384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t730026155 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3799129000 * L_48 = (ObjectEqualityComparer_1_t3799129000 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3799129000 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m4191103196_gshared (EqualityComparer_1_t730026155 * __this, ResourceLocatorU5BU5D_t1324310029* ___array0, ResourceLocator_t2156390884  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		ResourceLocatorU5BU5D_t1324310029* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ResourceLocator_t2156390884  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		ResourceLocator_t2156390884  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t730026155 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, ResourceLocator_t2156390884 , ResourceLocator_t2156390884  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::Equals(T,T) */, (EqualityComparer_1_t730026155 *)__this, (ResourceLocator_t2156390884 )L_6, (ResourceLocator_t2156390884 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m348345158_gshared (EqualityComparer_1_t730026155 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t730026155 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, ResourceLocator_t2156390884  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::GetHashCode(T) */, (EqualityComparer_1_t730026155 *)__this, (ResourceLocator_t2156390884 )((*(ResourceLocator_t2156390884 *)((ResourceLocator_t2156390884 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m266819904_gshared (EqualityComparer_1_t730026155 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t730026155 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, ResourceLocator_t2156390884 , ResourceLocator_t2156390884  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Resources.ResourceLocator>::Equals(T,T) */, (EqualityComparer_1_t730026155 *)__this, (ResourceLocator_t2156390884 )((*(ResourceLocator_t2156390884 *)((ResourceLocator_t2156390884 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (ResourceLocator_t2156390884 )((*(ResourceLocator_t2156390884 *)((ResourceLocator_t2156390884 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.SByte>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1389967279_gshared (EqualityComparer_1_t3323020116 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.SByte>::get_Default()
extern "C"  EqualityComparer_1_t3323020116 * EqualityComparer_1_get_Default_m67782343_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t3323020116 * V_0 = NULL;
	{
		EqualityComparer_1_t3323020116 * L_0 = ((EqualityComparer_1_t3323020116_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t3323020116 *)L_0;
		EqualityComparer_1_t3323020116 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t3323020116 * L_2 = ((  EqualityComparer_1_t3323020116 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t3323020116 *)L_2;
		EqualityComparer_1_t3323020116 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t3323020116_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t3323020116 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.SByte>::CreateComparer()
extern "C"  EqualityComparer_1_t3323020116 * EqualityComparer_1_CreateComparer_m1489375786_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1489375786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3323020116 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t2097155665 * L_48 = (ObjectEqualityComparer_1_t2097155665 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t2097155665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.SByte>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2725431654_gshared (EqualityComparer_1_t3323020116 * __this, SByteU5BU5D_t3472287392* ___array0, int8_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		SByteU5BU5D_t3472287392* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int8_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t3323020116 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int8_t, int8_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.SByte>::Equals(T,T) */, (EqualityComparer_1_t3323020116 *)__this, (int8_t)L_6, (int8_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.SByte>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m966207764_gshared (EqualityComparer_1_t3323020116 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t3323020116 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int8_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.SByte>::GetHashCode(T) */, (EqualityComparer_1_t3323020116 *)__this, (int8_t)((*(int8_t*)((int8_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.SByte>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2729643854_gshared (EqualityComparer_1_t3323020116 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t3323020116 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int8_t, int8_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.SByte>::Equals(T,T) */, (EqualityComparer_1_t3323020116 *)__this, (int8_t)((*(int8_t*)((int8_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (int8_t)((*(int8_t*)((int8_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3788663378_gshared (EqualityComparer_1_t650145203 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C"  EqualityComparer_1_t650145203 * EqualityComparer_1_get_Default_m4065943638_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t650145203 * V_0 = NULL;
	{
		EqualityComparer_1_t650145203 * L_0 = ((EqualityComparer_1_t650145203_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t650145203 *)L_0;
		EqualityComparer_1_t650145203 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t650145203 * L_2 = ((  EqualityComparer_1_t650145203 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t650145203 *)L_2;
		EqualityComparer_1_t650145203 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t650145203_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t650145203 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::CreateComparer()
extern "C"  EqualityComparer_1_t650145203 * EqualityComparer_1_CreateComparer_m1544170251_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1544170251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t650145203 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3719248048 * L_48 = (ObjectEqualityComparer_1_t3719248048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3719248048 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m3888039363_gshared (EqualityComparer_1_t650145203 * __this, SingleU5BU5D_t577127397* ___array0, float ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		SingleU5BU5D_t577127397* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		float L_7 = ___value1;
		NullCheck((EqualityComparer_1_t650145203 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, float, float >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t650145203 *)__this, (float)L_6, (float)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1300051223_gshared (EqualityComparer_1_t650145203 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t650145203 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, float >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::GetHashCode(T) */, (EqualityComparer_1_t650145203 *)__this, (float)((*(float*)((float*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3589836321_gshared (EqualityComparer_1_t650145203 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t650145203 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, float, float >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t650145203 *)__this, (float)((*(float*)((float*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (float)((*(float*)((float*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2522767949_gshared (EqualityComparer_1_t991894998 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::get_Default()
extern "C"  EqualityComparer_1_t991894998 * EqualityComparer_1_get_Default_m2707073525_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t991894998 * V_0 = NULL;
	{
		EqualityComparer_1_t991894998 * L_0 = ((EqualityComparer_1_t991894998_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t991894998 *)L_0;
		EqualityComparer_1_t991894998 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t991894998 * L_2 = ((  EqualityComparer_1_t991894998 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t991894998 *)L_2;
		EqualityComparer_1_t991894998 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t991894998_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t991894998 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::CreateComparer()
extern "C"  EqualityComparer_1_t991894998 * EqualityComparer_1_CreateComparer_m542703678_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m542703678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t991894998 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t4060997843 * L_48 = (ObjectEqualityComparer_1_t4060997843 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t4060997843 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2108026950_gshared (EqualityComparer_1_t991894998 * __this, RegexOptionsU5BU5D_t1359681494* ___array0, int32_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		RegexOptionsU5BU5D_t1359681494* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t991894998 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::Equals(T,T) */, (EqualityComparer_1_t991894998 *)__this, (int32_t)L_6, (int32_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3469717780_gshared (EqualityComparer_1_t991894998 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t991894998 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::GetHashCode(T) */, (EqualityComparer_1_t991894998 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m229847050_gshared (EqualityComparer_1_t991894998 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t991894998 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Text.RegularExpressions.RegexOptions>::Equals(T,T) */, (EqualityComparer_1_t991894998 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m442580331_gshared (EqualityComparer_1_t2003894220 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
extern "C"  EqualityComparer_1_t2003894220 * EqualityComparer_1_get_Default_m1852501307_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2003894220 * V_0 = NULL;
	{
		EqualityComparer_1_t2003894220 * L_0 = ((EqualityComparer_1_t2003894220_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2003894220 *)L_0;
		EqualityComparer_1_t2003894220 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2003894220 * L_2 = ((  EqualityComparer_1_t2003894220 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2003894220 *)L_2;
		EqualityComparer_1_t2003894220 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2003894220_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2003894220 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::CreateComparer()
extern "C"  EqualityComparer_1_t2003894220 * EqualityComparer_1_CreateComparer_m2134235622_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m2134235622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2003894220 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t778029769 * L_48 = (ObjectEqualityComparer_1_t778029769 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t778029769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2293539210_gshared (EqualityComparer_1_t2003894220 * __this, TimeSpanU5BU5D_t1313935688* ___array0, TimeSpan_t3430258949  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		TimeSpanU5BU5D_t1313935688* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		TimeSpan_t3430258949  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		TimeSpan_t3430258949  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2003894220 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, TimeSpan_t3430258949 , TimeSpan_t3430258949  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T) */, (EqualityComparer_1_t2003894220 *)__this, (TimeSpan_t3430258949 )L_6, (TimeSpan_t3430258949 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2008684464_gshared (EqualityComparer_1_t2003894220 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2003894220 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, TimeSpan_t3430258949  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T) */, (EqualityComparer_1_t2003894220 *)__this, (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m144708998_gshared (EqualityComparer_1_t2003894220 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2003894220 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, TimeSpan_t3430258949 , TimeSpan_t3430258949  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T) */, (EqualityComparer_1_t2003894220 *)__this, (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt16>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3593860743_gshared (EqualityComparer_1_t3855485178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::get_Default()
extern "C"  EqualityComparer_1_t3855485178 * EqualityComparer_1_get_Default_m857195919_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t3855485178 * V_0 = NULL;
	{
		EqualityComparer_1_t3855485178 * L_0 = ((EqualityComparer_1_t3855485178_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t3855485178 *)L_0;
		EqualityComparer_1_t3855485178 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t3855485178 * L_2 = ((  EqualityComparer_1_t3855485178 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t3855485178 *)L_2;
		EqualityComparer_1_t3855485178 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t3855485178_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t3855485178 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::CreateComparer()
extern "C"  EqualityComparer_1_t3855485178 * EqualityComparer_1_CreateComparer_m587440072_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m587440072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t3855485178 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t2629620727 * L_48 = (ObjectEqualityComparer_1_t2629620727 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t2629620727 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2402494748_gshared (EqualityComparer_1_t3855485178 * __this, UInt16U5BU5D_t2527266722* ___array0, uint16_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		UInt16U5BU5D_t2527266722* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		uint16_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t3855485178 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::Equals(T,T) */, (EqualityComparer_1_t3855485178 *)__this, (uint16_t)L_6, (uint16_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4090360222_gshared (EqualityComparer_1_t3855485178 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t3855485178 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, uint16_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::GetHashCode(T) */, (EqualityComparer_1_t3855485178 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m373706356_gshared (EqualityComparer_1_t3855485178 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t3855485178 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint16_t, uint16_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::Equals(T,T) */, (EqualityComparer_1_t3855485178 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (uint16_t)((*(uint16_t*)((uint16_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m261013813_gshared (EqualityComparer_1_t723317292 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt32>::get_Default()
extern "C"  EqualityComparer_1_t723317292 * EqualityComparer_1_get_Default_m1855238229_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t723317292 * V_0 = NULL;
	{
		EqualityComparer_1_t723317292 * L_0 = ((EqualityComparer_1_t723317292_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t723317292 *)L_0;
		EqualityComparer_1_t723317292 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t723317292 * L_2 = ((  EqualityComparer_1_t723317292 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t723317292 *)L_2;
		EqualityComparer_1_t723317292 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t723317292_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t723317292 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt32>::CreateComparer()
extern "C"  EqualityComparer_1_t723317292 * EqualityComparer_1_CreateComparer_m2390196090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m2390196090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t723317292 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t3792420137 * L_48 = (ObjectEqualityComparer_1_t3792420137 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t3792420137 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt32>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2523210730_gshared (EqualityComparer_1_t723317292 * __this, UInt32U5BU5D_t59386216* ___array0, uint32_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		UInt32U5BU5D_t59386216* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		uint32_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t723317292 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt32>::Equals(T,T) */, (EqualityComparer_1_t723317292 *)__this, (uint32_t)L_6, (uint32_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2683083800_gshared (EqualityComparer_1_t723317292 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t723317292 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, uint32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt32>::GetHashCode(T) */, (EqualityComparer_1_t723317292 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m700104250_gshared (EqualityComparer_1_t723317292 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t723317292 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint32_t, uint32_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt32>::Equals(T,T) */, (EqualityComparer_1_t723317292 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt64>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1483783260_gshared (EqualityComparer_1_t1482832185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt64>::get_Default()
extern "C"  EqualityComparer_1_t1482832185 * EqualityComparer_1_get_Default_m3819458256_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t1482832185 * V_0 = NULL;
	{
		EqualityComparer_1_t1482832185 * L_0 = ((EqualityComparer_1_t1482832185_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t1482832185 *)L_0;
		EqualityComparer_1_t1482832185 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t1482832185 * L_2 = ((  EqualityComparer_1_t1482832185 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t1482832185 *)L_2;
		EqualityComparer_1_t1482832185 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t1482832185_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t1482832185 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt64>::CreateComparer()
extern "C"  EqualityComparer_1_t1482832185 * EqualityComparer_1_CreateComparer_m798414069_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m798414069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1482832185 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t256967734 * L_48 = (ObjectEqualityComparer_1_t256967734 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t256967734 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt64>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m2972214137_gshared (EqualityComparer_1_t1482832185 * __this, UInt64U5BU5D_t1668688775* ___array0, uint64_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		UInt64U5BU5D_t1668688775* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint64_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		uint64_t L_7 = ___value1;
		NullCheck((EqualityComparer_1_t1482832185 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint64_t, uint64_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt64>::Equals(T,T) */, (EqualityComparer_1_t1482832185 *)__this, (uint64_t)L_6, (uint64_t)L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt64>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3115016633_gshared (EqualityComparer_1_t1482832185 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t1482832185 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, uint64_t >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt64>::GetHashCode(T) */, (EqualityComparer_1_t1482832185 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt64>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1279536107_gshared (EqualityComparer_1_t1482832185 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t1482832185 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, uint64_t, uint64_t >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt64>::Equals(T,T) */, (EqualityComparer_1_t1482832185 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m839928680_gshared (EqualityComparer_1_t1354438193 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::get_Default()
extern "C"  EqualityComparer_1_t1354438193 * EqualityComparer_1_get_Default_m2995317452_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t1354438193 * V_0 = NULL;
	{
		EqualityComparer_1_t1354438193 * L_0 = ((EqualityComparer_1_t1354438193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t1354438193 *)L_0;
		EqualityComparer_1_t1354438193 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t1354438193 * L_2 = ((  EqualityComparer_1_t1354438193 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t1354438193 *)L_2;
		EqualityComparer_1_t1354438193 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t1354438193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t1354438193 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::CreateComparer()
extern "C"  EqualityComparer_1_t1354438193 * EqualityComparer_1_CreateComparer_m1656190225_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m1656190225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1354438193 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t128573742 * L_48 = (ObjectEqualityComparer_1_t128573742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t128573742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m258585981_gshared (EqualityComparer_1_t1354438193 * __this, RangePositionInfoU5BU5D_t4253033391* ___array0, RangePositionInfo_t2780802922  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		RangePositionInfoU5BU5D_t4253033391* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		RangePositionInfo_t2780802922  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		RangePositionInfo_t2780802922  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t1354438193 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, RangePositionInfo_t2780802922 , RangePositionInfo_t2780802922  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::Equals(T,T) */, (EqualityComparer_1_t1354438193 *)__this, (RangePositionInfo_t2780802922 )L_6, (RangePositionInfo_t2780802922 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3835598685_gshared (EqualityComparer_1_t1354438193 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t1354438193 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, RangePositionInfo_t2780802922  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::GetHashCode(T) */, (EqualityComparer_1_t1354438193 *)__this, (RangePositionInfo_t2780802922 )((*(RangePositionInfo_t2780802922 *)((RangePositionInfo_t2780802922 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1269907751_gshared (EqualityComparer_1_t1354438193 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t1354438193 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, RangePositionInfo_t2780802922 , RangePositionInfo_t2780802922  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.RangePositionInfo>::Equals(T,T) */, (EqualityComparer_1_t1354438193 *)__this, (RangePositionInfo_t2780802922 )((*(RangePositionInfo_t2780802922 *)((RangePositionInfo_t2780802922 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (RangePositionInfo_t2780802922 )((*(RangePositionInfo_t2780802922 *)((RangePositionInfo_t2780802922 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1938217563_gshared (EqualityComparer_1_t1084221781 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::get_Default()
extern "C"  EqualityComparer_1_t1084221781 * EqualityComparer_1_get_Default_m3644520635_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t1084221781 * V_0 = NULL;
	{
		EqualityComparer_1_t1084221781 * L_0 = ((EqualityComparer_1_t1084221781_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t1084221781 *)L_0;
		EqualityComparer_1_t1084221781 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t1084221781 * L_2 = ((  EqualityComparer_1_t1084221781 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t1084221781 *)L_2;
		EqualityComparer_1_t1084221781 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t1084221781_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t1084221781 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::CreateComparer()
extern "C"  EqualityComparer_1_t1084221781 * EqualityComparer_1_CreateComparer_m4116535204_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m4116535204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t1084221781 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t4153324626 * L_48 = (ObjectEqualityComparer_1_t4153324626 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t4153324626 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m3030007712_gshared (EqualityComparer_1_t1084221781 * __this, XmlSchemaObjectEntryU5BU5D_t621120379* ___array0, XmlSchemaObjectEntry_t2510586510  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		XmlSchemaObjectEntryU5BU5D_t621120379* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		XmlSchemaObjectEntry_t2510586510  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		XmlSchemaObjectEntry_t2510586510  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t1084221781 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, XmlSchemaObjectEntry_t2510586510 , XmlSchemaObjectEntry_t2510586510  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::Equals(T,T) */, (EqualityComparer_1_t1084221781 *)__this, (XmlSchemaObjectEntry_t2510586510 )L_6, (XmlSchemaObjectEntry_t2510586510 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m929053650_gshared (EqualityComparer_1_t1084221781 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t1084221781 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, XmlSchemaObjectEntry_t2510586510  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::GetHashCode(T) */, (EqualityComparer_1_t1084221781 *)__this, (XmlSchemaObjectEntry_t2510586510 )((*(XmlSchemaObjectEntry_t2510586510 *)((XmlSchemaObjectEntry_t2510586510 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m649285104_gshared (EqualityComparer_1_t1084221781 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t1084221781 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, XmlSchemaObjectEntry_t2510586510 , XmlSchemaObjectEntry_t2510586510  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::Equals(T,T) */, (EqualityComparer_1_t1084221781 *)__this, (XmlSchemaObjectEntry_t2510586510 )((*(XmlSchemaObjectEntry_t2510586510 *)((XmlSchemaObjectEntry_t2510586510 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (XmlSchemaObjectEntry_t2510586510 )((*(XmlSchemaObjectEntry_t2510586510 *)((XmlSchemaObjectEntry_t2510586510 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m718740765_gshared (EqualityComparer_1_t2479386620 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::get_Default()
extern "C"  EqualityComparer_1_t2479386620 * EqualityComparer_1_get_Default_m3611111465_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	EqualityComparer_1_t2479386620 * V_0 = NULL;
	{
		EqualityComparer_1_t2479386620 * L_0 = ((EqualityComparer_1_t2479386620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_defaultComparer_0();
		il2cpp_codegen_memory_barrier();
		V_0 = (EqualityComparer_1_t2479386620 *)L_0;
		EqualityComparer_1_t2479386620 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		EqualityComparer_1_t2479386620 * L_2 = ((  EqualityComparer_1_t2479386620 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (EqualityComparer_1_t2479386620 *)L_2;
		EqualityComparer_1_t2479386620 * L_3 = V_0;
		il2cpp_codegen_memory_barrier();
		((EqualityComparer_1_t2479386620_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultComparer_0(L_3);
	}

IL_001c:
	{
		EqualityComparer_1_t2479386620 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::CreateComparer()
extern "C"  EqualityComparer_1_t2479386620 * EqualityComparer_1_CreateComparer_m4000786850_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1_CreateComparer_m4000786850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeType_t2836228502 * V_0 = NULL;
	RuntimeType_t2836228502 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_0, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		RuntimeType_t2836228502 * L_1 = V_0;
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Byte_t3683104436_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteEqualityComparer_t1095452717 * L_4 = (ByteEqualityComparer_t1095452717 *)il2cpp_codegen_object_new(ByteEqualityComparer_t1095452717_il2cpp_TypeInfo_var);
		ByteEqualityComparer__ctor_m1220141194(L_4, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_6 = V_0;
		NullCheck((Type_t *)L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_5, (Type_t *)L_6);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericEqualityComparer_1_t2202941003_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_10 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_8, (RuntimeType_t2836228502 *)L_9, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_005b:
	{
		RuntimeType_t2836228502 * L_11 = V_0;
		NullCheck((Type_t *)L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, (Type_t *)L_11);
		if (!L_12)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_13 = V_0;
		NullCheck((Type_t *)L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(101 /* System.Type System.Type::GetGenericTypeDefinition() */, (Type_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		bool L_16 = Type_op_Equality_m3620493675(NULL /*static, unused*/, (Type_t *)L_14, (Type_t *)L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RuntimeType_t2836228502 * L_17 = V_0;
		NullCheck((Type_t *)L_17);
		TypeU5BU5D_t1664964607* L_18 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, (Type_t *)L_17);
		NullCheck(L_18);
		int32_t L_19 = 0;
		Type_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = (RuntimeType_t2836228502 *)((RuntimeType_t2836228502 *)Castclass(L_20, RuntimeType_t2836228502_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IEquatable_1_t2913394932_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_22 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeType_t2836228502 * L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_23);
		NullCheck((Type_t *)L_21);
		Type_t * L_24 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_21, (TypeU5BU5D_t1664964607*)L_22);
		RuntimeType_t2836228502 * L_25 = V_1;
		NullCheck((Type_t *)L_24);
		bool L_26 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_24, (Type_t *)L_25);
		if (!L_26)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(NullableEqualityComparer_1_t1847642941_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_27, (RuntimeType_t2836228502 *)L_28, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_00c8:
	{
		RuntimeType_t2836228502 * L_30 = V_0;
		NullCheck((Type_t *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(72 /* System.Boolean System.Type::get_IsEnum() */, (Type_t *)L_30);
		if (!L_31)
		{
			goto IL_0164;
		}
	}
	{
		RuntimeType_t2836228502 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_33 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, (Type_t *)L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, (Type_t *)L_33, /*hidden argument*/NULL);
		V_2 = (int32_t)L_34;
		int32_t L_35 = V_2;
		switch (((int32_t)((int32_t)L_35-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0122;
			}
			case 1:
			{
				goto IL_0138;
			}
			case 2:
			{
				goto IL_010c;
			}
			case 3:
			{
				goto IL_0138;
			}
			case 4:
			{
				goto IL_0138;
			}
			case 5:
			{
				goto IL_0138;
			}
			case 6:
			{
				goto IL_014e;
			}
			case 7:
			{
				goto IL_014e;
			}
		}
	}
	{
		goto IL_0164;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(ShortEnumEqualityComparer_1_t2311249405_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_36, (RuntimeType_t2836228502 *)L_37, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_38, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(SByteEnumEqualityComparer_1_t1634080424_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_41 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_39, (RuntimeType_t2836228502 *)L_40, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_41, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(EnumEqualityComparer_1_t343233057_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_42, (RuntimeType_t2836228502 *)L_43, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_44, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_014e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(LongEnumEqualityComparer_1_t2298570595_0_0_0_var), /*hidden argument*/NULL);
		RuntimeType_t2836228502 * L_46 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeType_t2836228502_il2cpp_TypeInfo_var);
		Il2CppObject * L_47 = RuntimeType_CreateInstanceForAnotherGenericParameter_m2645871795(NULL /*static, unused*/, (Type_t *)L_45, (RuntimeType_t2836228502 *)L_46, /*hidden argument*/NULL);
		return ((EqualityComparer_1_t2479386620 *)Castclass(L_47, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_0164:
	{
		ObjectEqualityComparer_1_t1253522169 * L_48 = (ObjectEqualityComparer_1_t1253522169 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ObjectEqualityComparer_1_t1253522169 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_48;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t EqualityComparer_1_IndexOf_m695403818_gshared (EqualityComparer_1_t2479386620 * __this, AnimatorClipInfoU5BU5D_t2969332312* ___array0, AnimatorClipInfo_t3905751349  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex2;
		V_1 = (int32_t)L_2;
		goto IL_0025;
	}

IL_000c:
	{
		AnimatorClipInfoU5BU5D_t2969332312* L_3 = ___array0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimatorClipInfo_t3905751349  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		AnimatorClipInfo_t3905751349  L_7 = ___value1;
		NullCheck((EqualityComparer_1_t2479386620 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, AnimatorClipInfo_t3905751349 , AnimatorClipInfo_t3905751349  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::Equals(T,T) */, (EqualityComparer_1_t2479386620 *)__this, (AnimatorClipInfo_t3905751349 )L_6, (AnimatorClipInfo_t3905751349 )L_7);
		if (!L_8)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000c;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1162246232_gshared (EqualityComparer_1_t2479386620 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck((EqualityComparer_1_t2479386620 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, AnimatorClipInfo_t3905751349  >::Invoke(9 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::GetHashCode(T) */, (EqualityComparer_1_t2479386620 *)__this, (AnimatorClipInfo_t3905751349 )((*(AnimatorClipInfo_t3905751349 *)((AnimatorClipInfo_t3905751349 *)UnBox(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_3;
	}

IL_0020:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3724603646_gshared (EqualityComparer_1_t2479386620 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		Il2CppObject * L_1 = ___y1;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_2 = ___x0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		Il2CppObject * L_4 = ___x0;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_5 = ___y1;
		if (!((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_6 = ___x0;
		Il2CppObject * L_7 = ___y1;
		NullCheck((EqualityComparer_1_t2479386620 *)__this);
		bool L_8 = VirtFuncInvoker2< bool, AnimatorClipInfo_t3905751349 , AnimatorClipInfo_t3905751349  >::Invoke(8 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.AnimatorClipInfo>::Equals(T,T) */, (EqualityComparer_1_t2479386620 *)__this, (AnimatorClipInfo_t3905751349 )((*(AnimatorClipInfo_t3905751349 *)((AnimatorClipInfo_t3905751349 *)UnBox(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), (AnimatorClipInfo_t3905751349 )((*(AnimatorClipInfo_t3905751349 *)((AnimatorClipInfo_t3905751349 *)UnBox(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_8;
	}

IL_0040:
	{
		ThrowHelper_ThrowArgumentException_m1802962943(NULL /*static, unused*/, (int32_t)2, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Boolean>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3042634703_gshared (GenericComparer_1_t1563550222 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2715583837 *)__this);
		((  void (*) (Comparer_1_t2715583837 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2715583837 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Boolean>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m658762326_gshared (GenericComparer_1_t1563550222 * __this, bool ___x0, bool ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		bool L_2 = ___y1;
		int32_t L_3 = Boolean_CompareTo_m1086129598((bool*)(&___x0), (bool)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Boolean>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m4038554642_gshared (GenericComparer_1_t1563550222 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t1563550222 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t1563550222 *)((GenericComparer_1_t1563550222 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t1563550222 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t1563550222 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Boolean>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m4098678138_gshared (GenericComparer_1_t1563550222 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Byte>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1318071569_gshared (GenericComparer_1_t1421079940 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2573113555 *)__this);
		((  void (*) (Comparer_1_t2573113555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2573113555 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Byte>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m4213399892_gshared (GenericComparer_1_t1421079940 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint8_t L_2 = ___y1;
		int32_t L_3 = Byte_CompareTo_m1850579028((uint8_t*)(&___x0), (uint8_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Byte>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m4143663636_gshared (GenericComparer_1_t1421079940 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t1421079940 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t1421079940 *)((GenericComparer_1_t1421079940 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t1421079940 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t1421079940 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Byte>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m760270384_gshared (GenericComparer_1_t1421079940 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericComparer_1__ctor_m474482338_gshared (GenericComparer_1_t2726148469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3878182084 *)__this);
		((  void (*) (Comparer_1_t3878182084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3878182084 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1840768387_gshared (GenericComparer_1_t2726148469 * __this, DateTime_t693205669  ___x0, DateTime_t693205669  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		DateTime_t693205669  L_2 = ___y1;
		int32_t L_3 = DateTime_CompareTo_m1511117942((DateTime_t693205669 *)(&___x0), (DateTime_t693205669 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.DateTime>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m3461851455_gshared (GenericComparer_1_t2726148469 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t2726148469 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t2726148469 *)((GenericComparer_1_t2726148469 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t2726148469 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t2726148469 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m1576564125_gshared (GenericComparer_1_t2726148469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericComparer_1__ctor_m4106585959_gshared (GenericComparer_1_t3395931706 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t252998025 *)__this);
		((  void (*) (Comparer_1_t252998025 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t252998025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m2516380588_gshared (GenericComparer_1_t3395931706 * __this, DateTimeOffset_t1362988906  ___x0, DateTimeOffset_t1362988906  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		DateTimeOffset_t1362988906  L_2 = ___y1;
		int32_t L_3 = DateTimeOffset_CompareTo_m441053436((DateTimeOffset_t1362988906 *)(&___x0), (DateTimeOffset_t1362988906 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m4183020680_gshared (GenericComparer_1_t3395931706 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t3395931706 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t3395931706 *)((GenericComparer_1_t3395931706 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t3395931706 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t3395931706 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m1172528284_gshared (GenericComparer_1_t3395931706 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Decimal>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3889356880_gshared (GenericComparer_1_t2757643877 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3909677492 *)__this);
		((  void (*) (Comparer_1_t3909677492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3909677492 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Decimal>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m117824657_gshared (GenericComparer_1_t2757643877 * __this, Decimal_t724701077  ___x0, Decimal_t724701077  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		Decimal_t724701077  L_2 = ___y1;
		int32_t L_3 = Decimal_CompareTo_m573986782((Decimal_t724701077 *)(&___x0), (Decimal_t724701077 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Decimal>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m2076439781_gshared (GenericComparer_1_t2757643877 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t2757643877 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t2757643877 *)((GenericComparer_1_t2757643877 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t2757643877 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t2757643877 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Decimal>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m2518635947_gshared (GenericComparer_1_t2757643877 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Double>::.ctor()
extern "C"  void GenericComparer_1__ctor_m4243670686_gshared (GenericComparer_1_t1815991185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2968024800 *)__this);
		((  void (*) (Comparer_1_t2968024800 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2968024800 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Double>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3783643887_gshared (GenericComparer_1_t1815991185 * __this, double ___x0, double ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		double L_2 = ___y1;
		int32_t L_3 = Double_CompareTo_m3968040230((double*)(&___x0), (double)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Double>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m497558091_gshared (GenericComparer_1_t1815991185 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t1815991185 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t1815991185 *)((GenericComparer_1_t1815991185 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t1815991185 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t1815991185 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Double>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m1969245929_gshared (GenericComparer_1_t1815991185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Int16>::.ctor()
extern "C"  void GenericComparer_1__ctor_m158989555_gshared (GenericComparer_1_t1779221418 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2931255033 *)__this);
		((  void (*) (Comparer_1_t2931255033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2931255033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int16>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m2317189868_gshared (GenericComparer_1_t1779221418 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int16_t L_2 = ___y1;
		int32_t L_3 = Int16_CompareTo_m2802154270((int16_t*)(&___x0), (int16_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Int16>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m4208169328_gshared (GenericComparer_1_t1779221418 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t1779221418 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t1779221418 *)((GenericComparer_1_t1779221418 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t1779221418 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t1779221418 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int16>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m813263236_gshared (GenericComparer_1_t1779221418 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m973776669_gshared (GenericComparer_1_t4104820248 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t961886567 *)__this);
		((  void (*) (Comparer_1_t961886567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t961886567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m4255737786_gshared (GenericComparer_1_t4104820248 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int32_t L_2 = ___y1;
		int32_t L_3 = Int32_CompareTo_m3808534558((int32_t*)(&___x0), (int32_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Int32>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m4007564350_gshared (GenericComparer_1_t4104820248 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t4104820248 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t4104820248 *)((GenericComparer_1_t4104820248 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t4104820248 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t4104820248 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m431547602_gshared (GenericComparer_1_t4104820248 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Int64>::.ctor()
extern "C"  void GenericComparer_1__ctor_m735017824_gshared (GenericComparer_1_t2942020837 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4094054452 *)__this);
		((  void (*) (Comparer_1_t4094054452 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4094054452 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int64>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1098748219_gshared (GenericComparer_1_t2942020837 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int64_t L_2 = ___y1;
		int32_t L_3 = Int64_CompareTo_m1385259998((int64_t*)(&___x0), (int64_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Int64>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m1172885607_gshared (GenericComparer_1_t2942020837 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t2942020837 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t2942020837 *)((GenericComparer_1_t2942020837 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t2942020837 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t2942020837 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int64>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m246949665_gshared (GenericComparer_1_t2942020837 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Object>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1146681644_gshared (GenericComparer_1_t427424799 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1579458414 *)__this);
		((  void (*) (Comparer_1_t1579458414 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1579458414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Object>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m78150427_gshared (GenericComparer_1_t427424799 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_2 = ___y1;
		NullCheck((Il2CppObject*)(*(&___x0)));
		int32_t L_3 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)(*(&___x0)), (Il2CppObject *)L_2);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
		Il2CppObject * L_4 = ___y1;
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Object>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m1186256055_gshared (GenericComparer_1_t427424799 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t427424799 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t427424799 *)((GenericComparer_1_t427424799 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t427424799 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t427424799 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Object>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m2303798337_gshared (GenericComparer_1_t427424799 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.SByte>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1753697720_gshared (GenericComparer_1_t2487360349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3639393964 *)__this);
		((  void (*) (Comparer_1_t3639393964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3639393964 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.SByte>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3799851379_gshared (GenericComparer_1_t2487360349 * __this, int8_t ___x0, int8_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int8_t L_2 = ___y1;
		int32_t L_3 = SByte_CompareTo_m3157383966((int8_t*)(&___x0), (int8_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.SByte>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m2161111287_gshared (GenericComparer_1_t2487360349 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t2487360349 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t2487360349 *)((GenericComparer_1_t2487360349 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t2487360349 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t2487360349 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.SByte>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m2522458453_gshared (GenericComparer_1_t2487360349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Single>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1501074793_gshared (GenericComparer_1_t4109452732 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t966519051 *)__this);
		((  void (*) (Comparer_1_t966519051 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t966519051 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Single>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3311684598_gshared (GenericComparer_1_t4109452732 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		float L_2 = ___y1;
		int32_t L_3 = Single_CompareTo_m1534635028((float*)(&___x0), (float)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.Single>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m516450502_gshared (GenericComparer_1_t4109452732 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t4109452732 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t4109452732 *)((GenericComparer_1_t4109452732 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t4109452732 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t4109452732 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Single>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m880133978_gshared (GenericComparer_1_t4109452732 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericComparer_1__ctor_m221205314_gshared (GenericComparer_1_t1168234453 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2320268068 *)__this);
		((  void (*) (Comparer_1_t2320268068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2320268068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1517459603_gshared (GenericComparer_1_t1168234453 * __this, TimeSpan_t3430258949  ___x0, TimeSpan_t3430258949  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		TimeSpan_t3430258949  L_2 = ___y1;
		int32_t L_3 = TimeSpan_CompareTo_m4183101766((TimeSpan_t3430258949 *)(&___x0), (TimeSpan_t3430258949 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m1081066127_gshared (GenericComparer_1_t1168234453 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t1168234453 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t1168234453 *)((GenericComparer_1_t1168234453 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t1168234453 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t1168234453 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m1525406817_gshared (GenericComparer_1_t1168234453 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.UInt16>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3558611152_gshared (GenericComparer_1_t3019825411 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4171859026 *)__this);
		((  void (*) (Comparer_1_t4171859026 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4171859026 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt16>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1547021951_gshared (GenericComparer_1_t3019825411 * __this, uint16_t ___x0, uint16_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint16_t L_2 = ___y1;
		int32_t L_3 = UInt16_CompareTo_m1703646358((uint16_t*)(&___x0), (uint16_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.UInt16>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m3412686827_gshared (GenericComparer_1_t3019825411 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t3019825411 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t3019825411 *)((GenericComparer_1_t3019825411 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t3019825411 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t3019825411 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt16>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m2094092549_gshared (GenericComparer_1_t3019825411 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1431381186_gshared (GenericComparer_1_t4182624821 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1039691140 *)__this);
		((  void (*) (Comparer_1_t1039691140 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1039691140 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1920782057_gshared (GenericComparer_1_t4182624821 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint32_t L_2 = ___y1;
		int32_t L_3 = UInt32_CompareTo_m2313773902((uint32_t*)(&___x0), (uint32_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.UInt32>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m1017649537_gshared (GenericComparer_1_t4182624821 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t4182624821 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t4182624821 *)((GenericComparer_1_t4182624821 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t4182624821 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t4182624821 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt32>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m3061917935_gshared (GenericComparer_1_t4182624821 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.UInt64>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2050606319_gshared (GenericComparer_1_t647172418 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1799206033 *)__this);
		((  void (*) (Comparer_1_t1799206033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1799206033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt64>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1950990020_gshared (GenericComparer_1_t647172418 * __this, uint64_t ___x0, uint64_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint64_t L_2 = ___y1;
		int32_t L_3 = UInt64_CompareTo_m2422461804((uint64_t*)(&___x0), (uint64_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return 1;
	}

IL_0027:
	{
	}
	{
		return (-1);
	}

IL_0034:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.GenericComparer`1<System.UInt64>::Equals(System.Object)
extern "C"  bool GenericComparer_1_Equals_m293801760_gshared (GenericComparer_1_t647172418 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericComparer_1_t647172418 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericComparer_1_t647172418 *)((GenericComparer_1_t647172418 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericComparer_1_t647172418 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericComparer_1_t647172418 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt64>::GetHashCode()
extern "C"  int32_t GenericComparer_1_GetHashCode_m3357216692_gshared (GenericComparer_1_t647172418 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1096417895_gshared (GenericEqualityComparer_1_t2809456854 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2399209989 *)__this);
		((  void (*) (EqualityComparer_1_t2399209989 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2399209989 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2469044952_gshared (GenericEqualityComparer_1_t2809456854 * __this, bool ___x0, bool ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		bool L_2 = ___y1;
		bool L_3 = Boolean_Equals_m294106711((bool*)(&___x0), (bool)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3450627064_gshared (GenericEqualityComparer_1_t2809456854 * __this, bool ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Boolean_GetHashCode_m1894638460((bool*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m3946080546_gshared (GenericEqualityComparer_1_t2809456854 * __this, BooleanU5BU5D_t3568034315* ___array0, bool ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		BooleanU5BU5D_t3568034315* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		bool L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		BooleanU5BU5D_t3568034315* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		bool L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		BooleanU5BU5D_t3568034315* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		bool L_19 = ___value1;
		bool L_20 = Boolean_Equals_m294106711((bool*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (bool)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m3943994458_gshared (GenericEqualityComparer_1_t2809456854 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t2809456854 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t2809456854 *)((GenericEqualityComparer_1_t2809456854 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t2809456854 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t2809456854 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1288758066_gshared (GenericEqualityComparer_1_t2809456854 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1641103993_gshared (GenericEqualityComparer_1_t2666986572 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2256739707 *)__this);
		((  void (*) (EqualityComparer_1_t2256739707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2256739707 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1249112582_gshared (GenericEqualityComparer_1_t2666986572 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint8_t L_2 = ___y1;
		bool L_3 = Byte_Equals_m368616327((uint8_t*)(&___x0), (uint8_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3195742934_gshared (GenericEqualityComparer_1_t2666986572 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Byte_GetHashCode_m2885528842((uint8_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m2798671620_gshared (GenericEqualityComparer_1_t2666986572 * __this, ByteU5BU5D_t3397334013* ___array0, uint8_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		ByteU5BU5D_t3397334013* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		ByteU5BU5D_t3397334013* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		ByteU5BU5D_t3397334013* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		uint8_t L_19 = ___value1;
		bool L_20 = Byte_Equals_m368616327((uint8_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (uint8_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m584501452_gshared (GenericEqualityComparer_1_t2666986572 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t2666986572 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t2666986572 *)((GenericEqualityComparer_1_t2666986572 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t2666986572 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t2666986572 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m220602856_gshared (GenericEqualityComparer_1_t2666986572 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m603915962_gshared (GenericEqualityComparer_1_t3972055101 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3561808236 *)__this);
		((  void (*) (EqualityComparer_1_t3561808236 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3561808236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2324680497_gshared (GenericEqualityComparer_1_t3972055101 * __this, DateTime_t693205669  ___x0, DateTime_t693205669  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		DateTime_t693205669  L_2 = ___y1;
		bool L_3 = DateTime_Equals_m1104060551((DateTime_t693205669 *)(&___x0), (DateTime_t693205669 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2969953181_gshared (GenericEqualityComparer_1_t3972055101 * __this, DateTime_t693205669  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTime_GetHashCode_m974799321((DateTime_t693205669 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m3711285661_gshared (GenericEqualityComparer_1_t3972055101 * __this, DateTimeU5BU5D_t3111277864* ___array0, DateTime_t693205669  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		DateTimeU5BU5D_t3111277864* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		DateTime_t693205669  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		DateTimeU5BU5D_t3111277864* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		DateTime_t693205669  L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		DateTimeU5BU5D_t3111277864* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		DateTime_t693205669  L_19 = ___value1;
		bool L_20 = DateTime_Equals_m1104060551((DateTime_t693205669 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (DateTime_t693205669 )L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m2650781703_gshared (GenericEqualityComparer_1_t3972055101 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t3972055101 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t3972055101 *)((GenericEqualityComparer_1_t3972055101 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t3972055101 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t3972055101 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m556330853_gshared (GenericEqualityComparer_1_t3972055101 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2311357775_gshared (GenericEqualityComparer_1_t346871042 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t4231591473 *)__this);
		((  void (*) (EqualityComparer_1_t4231591473 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t4231591473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m418285146_gshared (GenericEqualityComparer_1_t346871042 * __this, DateTimeOffset_t1362988906  ___x0, DateTimeOffset_t1362988906  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		DateTimeOffset_t1362988906  L_2 = ___y1;
		bool L_3 = DateTimeOffset_Equals_m3728302791((DateTimeOffset_t1362988906 *)(&___x0), (DateTimeOffset_t1362988906 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2782420646_gshared (GenericEqualityComparer_1_t346871042 * __this, DateTimeOffset_t1362988906  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTimeOffset_GetHashCode_m3312197462((DateTimeOffset_t1362988906 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m3415302452_gshared (GenericEqualityComparer_1_t346871042 * __this, DateTimeOffsetU5BU5D_t435078063* ___array0, DateTimeOffset_t1362988906  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		DateTimeOffsetU5BU5D_t435078063* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		DateTimeOffset_t1362988906  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		DateTimeOffsetU5BU5D_t435078063* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		DateTimeOffset_t1362988906  L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		DateTimeOffsetU5BU5D_t435078063* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		DateTimeOffset_t1362988906  L_19 = ___value1;
		bool L_20 = DateTimeOffset_Equals_m3728302791((DateTimeOffset_t1362988906 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (DateTimeOffset_t1362988906 )L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m2567351232_gshared (GenericEqualityComparer_1_t346871042 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t346871042 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t346871042 *)((GenericEqualityComparer_1_t346871042 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t346871042 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t346871042 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2189336276_gshared (GenericEqualityComparer_1_t346871042 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3770532840_gshared (GenericEqualityComparer_1_t4003550509 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3593303644 *)__this);
		((  void (*) (EqualityComparer_1_t3593303644 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3593303644 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m880188891_gshared (GenericEqualityComparer_1_t4003550509 * __this, Decimal_t724701077  ___x0, Decimal_t724701077  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		Decimal_t724701077  L_2 = ___y1;
		bool L_3 = Decimal_Equals_m1115043331((Decimal_t724701077 *)(&___x0), (Decimal_t724701077 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3019823587_gshared (GenericEqualityComparer_1_t4003550509 * __this, Decimal_t724701077  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Decimal_GetHashCode_m703641627((Decimal_t724701077 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m299974935_gshared (GenericEqualityComparer_1_t4003550509 * __this, DecimalU5BU5D_t624008824* ___array0, Decimal_t724701077  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		DecimalU5BU5D_t624008824* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Decimal_t724701077  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		DecimalU5BU5D_t624008824* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Decimal_t724701077  L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		DecimalU5BU5D_t624008824* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		Decimal_t724701077  L_19 = ___value1;
		bool L_20 = Decimal_Equals_m1115043331((Decimal_t724701077 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (Decimal_t724701077 )L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m381129261_gshared (GenericEqualityComparer_1_t4003550509 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t4003550509 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t4003550509 *)((GenericEqualityComparer_1_t4003550509 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t4003550509 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t4003550509 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Decimal>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2277727843_gshared (GenericEqualityComparer_1_t4003550509 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Double>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2172451126_gshared (GenericEqualityComparer_1_t3061897817 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2651650952 *)__this);
		((  void (*) (EqualityComparer_1_t2651650952 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2651650952 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Double>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1169714461_gshared (GenericEqualityComparer_1_t3061897817 * __this, double ___x0, double ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		double L_2 = ___y1;
		bool L_3 = Double_Equals_m920556135((double*)(&___x0), (double)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Double>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2118311785_gshared (GenericEqualityComparer_1_t3061897817 * __this, double ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Double_GetHashCode_m3403732029((double*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Double>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m455946705_gshared (GenericEqualityComparer_1_t3061897817 * __this, DoubleU5BU5D_t1889952540* ___array0, double ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		DoubleU5BU5D_t1889952540* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		double L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		DoubleU5BU5D_t1889952540* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		double L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		DoubleU5BU5D_t1889952540* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		double L_19 = ___value1;
		bool L_20 = Double_Equals_m920556135((double*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (double)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Double>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m2650253971_gshared (GenericEqualityComparer_1_t3061897817 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t3061897817 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t3061897817 *)((GenericEqualityComparer_1_t3061897817 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t3061897817 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t3061897817 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Double>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3750160945_gshared (GenericEqualityComparer_1_t3061897817 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2595781006_gshared (GenericEqualityComparer_1_t1517483729 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1107236864 *)__this);
		((  void (*) (EqualityComparer_1_t1107236864 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1107236864 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1549453511_gshared (GenericEqualityComparer_1_t1517483729 * __this, Guid_t  ___x0, Guid_t  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		Guid_t  L_2 = ___y1;
		bool L_3 = Guid_Equals_m2389236871((Guid_t *)(&___x0), (Guid_t )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3320722759_gshared (GenericEqualityComparer_1_t1517483729 * __this, Guid_t  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Guid_GetHashCode_m1401300871((Guid_t *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m3776698291_gshared (GenericEqualityComparer_1_t1517483729 * __this, GuidU5BU5D_t3556289988* ___array0, Guid_t  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		GuidU5BU5D_t3556289988* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Guid_t  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		GuidU5BU5D_t3556289988* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Guid_t  L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		GuidU5BU5D_t3556289988* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		Guid_t  L_19 = ___value1;
		bool L_20 = Guid_Equals_m2389236871((Guid_t *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (Guid_t )L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m4166982861_gshared (GenericEqualityComparer_1_t1517483729 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1517483729 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1517483729 *)((GenericEqualityComparer_1_t1517483729 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1517483729 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1517483729 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m4095457751_gshared (GenericEqualityComparer_1_t1517483729 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m39386843_gshared (GenericEqualityComparer_1_t3025128050 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2614881185 *)__this);
		((  void (*) (EqualityComparer_1_t2614881185 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2614881185 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m847891558_gshared (GenericEqualityComparer_1_t3025128050 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int16_t L_2 = ___y1;
		bool L_3 = Int16_Equals_m920337175((int16_t*)(&___x0), (int16_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m709743494_gshared (GenericEqualityComparer_1_t3025128050 * __this, int16_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int16_GetHashCode_m304503362((int16_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m3694867156_gshared (GenericEqualityComparer_1_t3025128050 * __this, Int16U5BU5D_t3104283263* ___array0, int16_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		Int16U5BU5D_t3104283263* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		Int16U5BU5D_t3104283263* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int16_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		Int16U5BU5D_t3104283263* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int16_t L_19 = ___value1;
		bool L_20 = Int16_Equals_m920337175((int16_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (int16_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m286535336_gshared (GenericEqualityComparer_1_t3025128050 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t3025128050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t3025128050 *)((GenericEqualityComparer_1_t3025128050 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t3025128050 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t3025128050 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2141137740_gshared (GenericEqualityComparer_1_t3025128050 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m854452741_gshared (GenericEqualityComparer_1_t1055759584 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t645512719 *)__this);
		((  void (*) (EqualityComparer_1_t645512719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t645512719 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m4153713908_gshared (GenericEqualityComparer_1_t1055759584 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int32_t L_2 = ___y1;
		bool L_3 = Int32_Equals_m321398519((int32_t*)(&___x0), (int32_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3520912652_gshared (GenericEqualityComparer_1_t1055759584 * __this, int32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int32_GetHashCode_m1381647448((int32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m1808415758_gshared (GenericEqualityComparer_1_t1055759584 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		Int32U5BU5D_t3030399641* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		Int32U5BU5D_t3030399641* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int32_t L_19 = ___value1;
		bool L_20 = Int32_Equals_m321398519((int32_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (int32_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m3538844534_gshared (GenericEqualityComparer_1_t1055759584 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1055759584 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1055759584 *)((GenericEqualityComparer_1_t1055759584 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1055759584 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1055759584 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3345378202_gshared (GenericEqualityComparer_1_t1055759584 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m615702344_gshared (GenericEqualityComparer_1_t4187927469 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3777680604 *)__this);
		((  void (*) (EqualityComparer_1_t3777680604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3777680604 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1233858369_gshared (GenericEqualityComparer_1_t4187927469 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int64_t L_2 = ___y1;
		bool L_3 = Int64_Equals_m2821075083((int64_t*)(&___x0), (int64_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1722124025_gshared (GenericEqualityComparer_1_t4187927469 * __this, int64_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int64_GetHashCode_m4047499913((int64_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m1606971969_gshared (GenericEqualityComparer_1_t4187927469 * __this, Int64U5BU5D_t717125112* ___array0, int64_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		Int64U5BU5D_t717125112* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int64_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		Int64U5BU5D_t717125112* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int64_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		Int64U5BU5D_t717125112* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int64_t L_19 = ___value1;
		bool L_20 = Int64_Equals_m2821075083((int64_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (int64_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m829611167_gshared (GenericEqualityComparer_1_t4187927469 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t4187927469 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t4187927469 *)((GenericEqualityComparer_1_t4187927469 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t4187927469 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t4187927469 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1546995945_gshared (GenericEqualityComparer_1_t4187927469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Object>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2748998164_gshared (GenericEqualityComparer_1_t1673331431 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1263084566 *)__this);
		((  void (*) (EqualityComparer_1_t1263084566 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1263084566 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m482771493_gshared (GenericEqualityComparer_1_t1673331431 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_2 = ___y1;
		NullCheck((Il2CppObject*)(*(&___x0)));
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.IEquatable`1<System.Object>::Equals(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)(*(&___x0)), (Il2CppObject *)L_2);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
		Il2CppObject * L_4 = ___y1;
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3511004089_gshared (GenericEqualityComparer_1_t1673331431 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Il2CppObject *)(*(&___obj0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&___obj0)));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m2389639681_gshared (GenericEqualityComparer_1_t1673331431 * __this, ObjectU5BU5D_t3614634134* ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		Il2CppObject * L_2 = ___value1;
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if (L_7)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		ObjectU5BU5D_t3614634134* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if (!L_16)
		{
			goto IL_006e;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		Il2CppObject * L_19 = ___value1;
		NullCheck((Il2CppObject*)(*((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))));
		bool L_20 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.IEquatable`1<System.Object>::Equals(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)(*((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), (Il2CppObject *)L_19);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m2044643311_gshared (GenericEqualityComparer_1_t1673331431 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1673331431 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1673331431 *)((GenericEqualityComparer_1_t1673331431 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1673331431 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1673331431 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1947619961_gshared (GenericEqualityComparer_1_t1673331431 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2844076832_gshared (GenericEqualityComparer_1_t3733266981 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3323020116 *)__this);
		((  void (*) (EqualityComparer_1_t3323020116 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3323020116 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m4042977661_gshared (GenericEqualityComparer_1_t3733266981 * __this, int8_t ___x0, int8_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		int8_t L_2 = ___y1;
		bool L_3 = SByte_Equals_m2330990811((int8_t*)(&___x0), (int8_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1505478421_gshared (GenericEqualityComparer_1_t3733266981 * __this, int8_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = SByte_GetHashCode_m1692727069((int8_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m567985189_gshared (GenericEqualityComparer_1_t3733266981 * __this, SByteU5BU5D_t3472287392* ___array0, int8_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		SByteU5BU5D_t3472287392* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		SByteU5BU5D_t3472287392* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int8_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		SByteU5BU5D_t3472287392* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		int8_t L_19 = ___value1;
		bool L_20 = SByte_Equals_m2330990811((int8_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (int8_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m4278422703_gshared (GenericEqualityComparer_1_t3733266981 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t3733266981 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t3733266981 *)((GenericEqualityComparer_1_t3733266981 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t3733266981 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t3733266981 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.SByte>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m665122461_gshared (GenericEqualityComparer_1_t3733266981 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3487039313_gshared (GenericEqualityComparer_1_t1060392068 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t650145203 *)__this);
		((  void (*) (EqualityComparer_1_t650145203 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t650145203 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2779085860_gshared (GenericEqualityComparer_1_t1060392068 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		float L_2 = ___y1;
		bool L_3 = Single_Equals_m3359827399((float*)(&___x0), (float)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1950634276_gshared (GenericEqualityComparer_1_t1060392068 * __this, float ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Single_GetHashCode_m3102305584((float*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m2802653046_gshared (GenericEqualityComparer_1_t1060392068 * __this, SingleU5BU5D_t577127397* ___array0, float ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		SingleU5BU5D_t577127397* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		float L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		SingleU5BU5D_t577127397* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		float L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		SingleU5BU5D_t577127397* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		float L_19 = ___value1;
		bool L_20 = Single_Equals_m3359827399((float*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (float)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m340958206_gshared (GenericEqualityComparer_1_t1060392068 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1060392068 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1060392068 *)((GenericEqualityComparer_1_t1060392068 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1060392068 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1060392068 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1395110546_gshared (GenericEqualityComparer_1_t1060392068 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1269284954_gshared (GenericEqualityComparer_1_t2414141085 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2003894220 *)__this);
		((  void (*) (EqualityComparer_1_t2003894220 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2003894220 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1663005117_gshared (GenericEqualityComparer_1_t2414141085 * __this, TimeSpan_t3430258949  ___x0, TimeSpan_t3430258949  ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		TimeSpan_t3430258949  L_2 = ___y1;
		bool L_3 = TimeSpan_Equals_m2029123271((TimeSpan_t3430258949 *)(&___x0), (TimeSpan_t3430258949 )L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2293071025_gshared (GenericEqualityComparer_1_t2414141085 * __this, TimeSpan_t3430258949  ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m1957549705_gshared (GenericEqualityComparer_1_t2414141085 * __this, TimeSpanU5BU5D_t1313935688* ___array0, TimeSpan_t3430258949  ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		TimeSpanU5BU5D_t1313935688* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		TimeSpan_t3430258949  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		TimeSpanU5BU5D_t1313935688* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		TimeSpan_t3430258949  L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		TimeSpanU5BU5D_t1313935688* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		TimeSpan_t3430258949  L_19 = ___value1;
		bool L_20 = TimeSpan_Equals_m2029123271((TimeSpan_t3430258949 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (TimeSpan_t3430258949 )L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m3489789783_gshared (GenericEqualityComparer_1_t2414141085 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t2414141085 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t2414141085 *)((GenericEqualityComparer_1_t2414141085 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t2414141085 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t2414141085 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m271396393_gshared (GenericEqualityComparer_1_t2414141085 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m661057768_gshared (GenericEqualityComparer_1_t4265732043 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t3855485178 *)__this);
		((  void (*) (EqualityComparer_1_t3855485178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3855485178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m818657033_gshared (GenericEqualityComparer_1_t4265732043 * __this, uint16_t ___x0, uint16_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint16_t L_2 = ___y1;
		bool L_3 = UInt16_Equals_m4201490279((uint16_t*)(&___x0), (uint16_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m949672813_gshared (GenericEqualityComparer_1_t4265732043 * __this, uint16_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = UInt16_GetHashCode_m1468226569((uint16_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m2289440845_gshared (GenericEqualityComparer_1_t4265732043 * __this, UInt16U5BU5D_t2527266722* ___array0, uint16_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		UInt16U5BU5D_t2527266722* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		UInt16U5BU5D_t2527266722* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint16_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		UInt16U5BU5D_t2527266722* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		uint16_t L_19 = ___value1;
		bool L_20 = UInt16_Equals_m4201490279((uint16_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (uint16_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m1036981683_gshared (GenericEqualityComparer_1_t4265732043 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t4265732043 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t4265732043 *)((GenericEqualityComparer_1_t4265732043 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t4265732043 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t4265732043 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2381800909_gshared (GenericEqualityComparer_1_t4265732043 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2828795098_gshared (GenericEqualityComparer_1_t1133564157 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t723317292 *)__this);
		((  void (*) (EqualityComparer_1_t723317292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t723317292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m719101535_gshared (GenericEqualityComparer_1_t1133564157 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint32_t L_2 = ___y1;
		bool L_3 = UInt32_Equals_m787945383((uint32_t*)(&___x0), (uint32_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m734592575_gshared (GenericEqualityComparer_1_t1133564157 * __this, uint32_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = UInt32_GetHashCode_m2903162199((uint32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m1644873403_gshared (GenericEqualityComparer_1_t1133564157 * __this, UInt32U5BU5D_t59386216* ___array0, uint32_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		UInt32U5BU5D_t59386216* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		UInt32U5BU5D_t59386216* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		UInt32U5BU5D_t59386216* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		uint32_t L_19 = ___value1;
		bool L_20 = UInt32_Equals_m787945383((uint32_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (uint32_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m2936911689_gshared (GenericEqualityComparer_1_t1133564157 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1133564157 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1133564157 *)((GenericEqualityComparer_1_t1133564157 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1133564157 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1133564157 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3349626295_gshared (GenericEqualityComparer_1_t1133564157 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3448299015_gshared (GenericEqualityComparer_1_t1893079050 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1482832185 *)__this);
		((  void (*) (EqualityComparer_1_t1482832185 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1482832185 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1201629554_gshared (GenericEqualityComparer_1_t1893079050 * __this, uint64_t ___x0, uint64_t ___y1, const MethodInfo* method)
{
	{
	}
	{
	}
	{
		uint64_t L_2 = ___y1;
		bool L_3 = UInt64_Equals_m1977009287((uint64_t*)(&___x0), (uint64_t)L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1941890174_gshared (GenericEqualityComparer_1_t1893079050 * __this, uint64_t ___obj0, const MethodInfo* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = UInt64_GetHashCode_m3478338766((uint64_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::IndexOf(T[],T,System.Int32,System.Int32)
extern "C"  int32_t GenericEqualityComparer_1_IndexOf_m1560355292_gshared (GenericEqualityComparer_1_t1893079050 * __this, UInt64U5BU5D_t1668688775* ___array0, uint64_t ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___startIndex2;
		int32_t L_1 = ___count3;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_003a;
	}
	{
		int32_t L_3 = ___startIndex2;
		V_1 = (int32_t)L_3;
		goto IL_002e;
	}

IL_0017:
	{
		UInt64U5BU5D_t1668688775* L_4 = ___array0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint64_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		goto IL_002a;
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0079;
	}

IL_003a:
	{
		int32_t L_12 = ___startIndex2;
		V_2 = (int32_t)L_12;
		goto IL_0072;
	}

IL_0041:
	{
		UInt64U5BU5D_t1668688775* L_13 = ___array0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint64_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
	}
	{
		UInt64U5BU5D_t1668688775* L_17 = ___array0;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		uint64_t L_19 = ___value1;
		bool L_20 = UInt64_Equals_m1977009287((uint64_t*)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), (uint64_t)L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_21 = V_2;
		return L_21;
	}

IL_006e:
	{
		int32_t L_22 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0041;
		}
	}

IL_0079:
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::Equals(System.Object)
extern "C"  bool GenericEqualityComparer_1_Equals_m1972235496_gshared (GenericEqualityComparer_1_t1893079050 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	GenericEqualityComparer_1_t1893079050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (GenericEqualityComparer_1_t1893079050 *)((GenericEqualityComparer_1_t1893079050 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
		GenericEqualityComparer_1_t1893079050 * L_1 = V_0;
		return (bool)((((int32_t)((((Il2CppObject*)(GenericEqualityComparer_1_t1893079050 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::GetHashCode()
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2058968956_gshared (GenericEqualityComparer_1_t1893079050 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_0 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_0);
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
