﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/GetProxyData
struct  GetProxyData_t1386489386  : public Il2CppObject
{
public:
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::script
	IntPtr_t ___script_0;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::targetUri
	IntPtr_t ___targetUri_1;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::error
	IntPtr_t ___error_2;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::result
	IntPtr_t ___result_3;
	// System.Threading.ManualResetEvent Mono.Net.CFNetwork/GetProxyData::evt
	ManualResetEvent_t926074657 * ___evt_4;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___script_0)); }
	inline IntPtr_t get_script_0() const { return ___script_0; }
	inline IntPtr_t* get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(IntPtr_t value)
	{
		___script_0 = value;
	}

	inline static int32_t get_offset_of_targetUri_1() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___targetUri_1)); }
	inline IntPtr_t get_targetUri_1() const { return ___targetUri_1; }
	inline IntPtr_t* get_address_of_targetUri_1() { return &___targetUri_1; }
	inline void set_targetUri_1(IntPtr_t value)
	{
		___targetUri_1 = value;
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___error_2)); }
	inline IntPtr_t get_error_2() const { return ___error_2; }
	inline IntPtr_t* get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(IntPtr_t value)
	{
		___error_2 = value;
	}

	inline static int32_t get_offset_of_result_3() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___result_3)); }
	inline IntPtr_t get_result_3() const { return ___result_3; }
	inline IntPtr_t* get_address_of_result_3() { return &___result_3; }
	inline void set_result_3(IntPtr_t value)
	{
		___result_3 = value;
	}

	inline static int32_t get_offset_of_evt_4() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___evt_4)); }
	inline ManualResetEvent_t926074657 * get_evt_4() const { return ___evt_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_evt_4() { return &___evt_4; }
	inline void set_evt_4(ManualResetEvent_t926074657 * value)
	{
		___evt_4 = value;
		Il2CppCodeGenWriteBarrier(&___evt_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
