﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"
#include "System_Xml_MS_Internal_Xml_XPath_Function_Function2662603618.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Function
struct  Function_t2441061476  : public AstNode_t2002670936
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.Function::functionType
	int32_t ___functionType_0;
	// System.Collections.ArrayList MS.Internal.Xml.XPath.Function::argumentList
	ArrayList_t4252133567 * ___argumentList_1;
	// System.String MS.Internal.Xml.XPath.Function::name
	String_t* ___name_2;
	// System.String MS.Internal.Xml.XPath.Function::prefix
	String_t* ___prefix_3;

public:
	inline static int32_t get_offset_of_functionType_0() { return static_cast<int32_t>(offsetof(Function_t2441061476, ___functionType_0)); }
	inline int32_t get_functionType_0() const { return ___functionType_0; }
	inline int32_t* get_address_of_functionType_0() { return &___functionType_0; }
	inline void set_functionType_0(int32_t value)
	{
		___functionType_0 = value;
	}

	inline static int32_t get_offset_of_argumentList_1() { return static_cast<int32_t>(offsetof(Function_t2441061476, ___argumentList_1)); }
	inline ArrayList_t4252133567 * get_argumentList_1() const { return ___argumentList_1; }
	inline ArrayList_t4252133567 ** get_address_of_argumentList_1() { return &___argumentList_1; }
	inline void set_argumentList_1(ArrayList_t4252133567 * value)
	{
		___argumentList_1 = value;
		Il2CppCodeGenWriteBarrier(&___argumentList_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Function_t2441061476, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(Function_t2441061476, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_3, value);
	}
};

struct Function_t2441061476_StaticFields
{
public:
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.Function::ReturnTypes
	XPathResultTypeU5BU5D_t2966113519* ___ReturnTypes_4;

public:
	inline static int32_t get_offset_of_ReturnTypes_4() { return static_cast<int32_t>(offsetof(Function_t2441061476_StaticFields, ___ReturnTypes_4)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_ReturnTypes_4() const { return ___ReturnTypes_4; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_ReturnTypes_4() { return &___ReturnTypes_4; }
	inline void set_ReturnTypes_4(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___ReturnTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___ReturnTypes_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
