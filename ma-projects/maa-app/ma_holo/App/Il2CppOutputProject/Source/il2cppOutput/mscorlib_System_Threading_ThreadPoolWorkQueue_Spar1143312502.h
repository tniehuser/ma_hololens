﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.ThreadPoolWorkQueue/WorkStealingQueue[]
struct WorkStealingQueueU5BU5D_t1854264850;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Threading.ThreadPoolWorkQueue/WorkStealingQueue>
struct  SparseArray_1_t1143312502  : public Il2CppObject
{
public:
	// T[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue/SparseArray`1::m_array
	WorkStealingQueueU5BU5D_t1854264850* ___m_array_0;

public:
	inline static int32_t get_offset_of_m_array_0() { return static_cast<int32_t>(offsetof(SparseArray_1_t1143312502, ___m_array_0)); }
	inline WorkStealingQueueU5BU5D_t1854264850* get_m_array_0() const { return ___m_array_0; }
	inline WorkStealingQueueU5BU5D_t1854264850** get_address_of_m_array_0() { return &___m_array_0; }
	inline void set_m_array_0(WorkStealingQueueU5BU5D_t1854264850* value)
	{
		___m_array_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_array_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
