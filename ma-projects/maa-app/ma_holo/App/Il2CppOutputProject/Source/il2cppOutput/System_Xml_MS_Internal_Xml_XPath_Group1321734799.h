﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"

// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2002670936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Group
struct  Group_t1321734799  : public AstNode_t2002670936
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Group::groupNode
	AstNode_t2002670936 * ___groupNode_0;

public:
	inline static int32_t get_offset_of_groupNode_0() { return static_cast<int32_t>(offsetof(Group_t1321734799, ___groupNode_0)); }
	inline AstNode_t2002670936 * get_groupNode_0() const { return ___groupNode_0; }
	inline AstNode_t2002670936 ** get_address_of_groupNode_0() { return &___groupNode_0; }
	inline void set_groupNode_0(AstNode_t2002670936 * value)
	{
		___groupNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___groupNode_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
