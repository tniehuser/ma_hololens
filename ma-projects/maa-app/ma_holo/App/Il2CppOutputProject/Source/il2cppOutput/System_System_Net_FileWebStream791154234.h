﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_FileStream1695958676.h"

// System.Net.FileWebRequest
struct FileWebRequest_t1571840111;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebStream
struct  FileWebStream_t791154234  : public FileStream_t1695958676
{
public:
	// System.Net.FileWebRequest System.Net.FileWebStream::m_request
	FileWebRequest_t1571840111 * ___m_request_25;

public:
	inline static int32_t get_offset_of_m_request_25() { return static_cast<int32_t>(offsetof(FileWebStream_t791154234, ___m_request_25)); }
	inline FileWebRequest_t1571840111 * get_m_request_25() const { return ___m_request_25; }
	inline FileWebRequest_t1571840111 ** get_address_of_m_request_25() { return &___m_request_25; }
	inline void set_m_request_25(FileWebRequest_t1571840111 * value)
	{
		___m_request_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_request_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
