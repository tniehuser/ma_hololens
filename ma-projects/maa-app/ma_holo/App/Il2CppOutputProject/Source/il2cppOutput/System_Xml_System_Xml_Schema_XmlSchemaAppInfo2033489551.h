﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"

// System.String
struct String_t;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t2118142256;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAppInfo
struct  XmlSchemaAppInfo_t2033489551  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaAppInfo::source
	String_t* ___source_6;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaAppInfo::markup
	XmlNodeU5BU5D_t2118142256* ___markup_7;

public:
	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t2033489551, ___source_6)); }
	inline String_t* get_source_6() const { return ___source_6; }
	inline String_t** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(String_t* value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier(&___source_6, value);
	}

	inline static int32_t get_offset_of_markup_7() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t2033489551, ___markup_7)); }
	inline XmlNodeU5BU5D_t2118142256* get_markup_7() const { return ___markup_7; }
	inline XmlNodeU5BU5D_t2118142256** get_address_of_markup_7() { return &___markup_7; }
	inline void set_markup_7(XmlNodeU5BU5D_t2118142256* value)
	{
		___markup_7 = value;
		Il2CppCodeGenWriteBarrier(&___markup_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
