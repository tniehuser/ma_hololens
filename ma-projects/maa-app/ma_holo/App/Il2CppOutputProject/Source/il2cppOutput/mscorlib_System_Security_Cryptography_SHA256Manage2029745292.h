﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_SHA256582564463.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SHA256Managed
struct  SHA256Managed_t2029745292  : public SHA256_t582564463
{
public:
	// System.Byte[] System.Security.Cryptography.SHA256Managed::_buffer
	ByteU5BU5D_t3397334013* ____buffer_4;
	// System.Int64 System.Security.Cryptography.SHA256Managed::_count
	int64_t ____count_5;
	// System.UInt32[] System.Security.Cryptography.SHA256Managed::_stateSHA256
	UInt32U5BU5D_t59386216* ____stateSHA256_6;
	// System.UInt32[] System.Security.Cryptography.SHA256Managed::_W
	UInt32U5BU5D_t59386216* ____W_7;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(SHA256Managed_t2029745292, ____buffer_4)); }
	inline ByteU5BU5D_t3397334013* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_t3397334013* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_4, value);
	}

	inline static int32_t get_offset_of__count_5() { return static_cast<int32_t>(offsetof(SHA256Managed_t2029745292, ____count_5)); }
	inline int64_t get__count_5() const { return ____count_5; }
	inline int64_t* get_address_of__count_5() { return &____count_5; }
	inline void set__count_5(int64_t value)
	{
		____count_5 = value;
	}

	inline static int32_t get_offset_of__stateSHA256_6() { return static_cast<int32_t>(offsetof(SHA256Managed_t2029745292, ____stateSHA256_6)); }
	inline UInt32U5BU5D_t59386216* get__stateSHA256_6() const { return ____stateSHA256_6; }
	inline UInt32U5BU5D_t59386216** get_address_of__stateSHA256_6() { return &____stateSHA256_6; }
	inline void set__stateSHA256_6(UInt32U5BU5D_t59386216* value)
	{
		____stateSHA256_6 = value;
		Il2CppCodeGenWriteBarrier(&____stateSHA256_6, value);
	}

	inline static int32_t get_offset_of__W_7() { return static_cast<int32_t>(offsetof(SHA256Managed_t2029745292, ____W_7)); }
	inline UInt32U5BU5D_t59386216* get__W_7() const { return ____W_7; }
	inline UInt32U5BU5D_t59386216** get_address_of__W_7() { return &____W_7; }
	inline void set__W_7(UInt32U5BU5D_t59386216* value)
	{
		____W_7 = value;
		Il2CppCodeGenWriteBarrier(&____W_7, value);
	}
};

struct SHA256Managed_t2029745292_StaticFields
{
public:
	// System.UInt32[] System.Security.Cryptography.SHA256Managed::_K
	UInt32U5BU5D_t59386216* ____K_8;

public:
	inline static int32_t get_offset_of__K_8() { return static_cast<int32_t>(offsetof(SHA256Managed_t2029745292_StaticFields, ____K_8)); }
	inline UInt32U5BU5D_t59386216* get__K_8() const { return ____K_8; }
	inline UInt32U5BU5D_t59386216** get_address_of__K_8() { return &____K_8; }
	inline void set__K_8(UInt32U5BU5D_t59386216* value)
	{
		____K_8 = value;
		Il2CppCodeGenWriteBarrier(&____K_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
