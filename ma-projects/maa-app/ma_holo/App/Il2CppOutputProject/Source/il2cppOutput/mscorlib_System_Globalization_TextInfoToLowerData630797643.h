﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TextInfoToLowerData
struct  TextInfoToLowerData_t630797643  : public Il2CppObject
{
public:

public:
};

struct TextInfoToLowerData_t630797643_StaticFields
{
public:
	// System.Char[] System.Globalization.TextInfoToLowerData::range_00c0_0556
	CharU5BU5D_t1328083999* ___range_00c0_0556_0;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_10a0_10c5
	CharU5BU5D_t1328083999* ___range_10a0_10c5_1;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_1e00_1ffc
	CharU5BU5D_t1328083999* ___range_1e00_1ffc_2;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_2160_216f
	CharU5BU5D_t1328083999* ___range_2160_216f_3;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_24b6_24cf
	CharU5BU5D_t1328083999* ___range_24b6_24cf_4;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_2c00_2c2e
	CharU5BU5D_t1328083999* ___range_2c00_2c2e_5;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_2c60_2ce2
	CharU5BU5D_t1328083999* ___range_2c60_2ce2_6;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_a640_a696
	CharU5BU5D_t1328083999* ___range_a640_a696_7;
	// System.Char[] System.Globalization.TextInfoToLowerData::range_a722_a78b
	CharU5BU5D_t1328083999* ___range_a722_a78b_8;

public:
	inline static int32_t get_offset_of_range_00c0_0556_0() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_00c0_0556_0)); }
	inline CharU5BU5D_t1328083999* get_range_00c0_0556_0() const { return ___range_00c0_0556_0; }
	inline CharU5BU5D_t1328083999** get_address_of_range_00c0_0556_0() { return &___range_00c0_0556_0; }
	inline void set_range_00c0_0556_0(CharU5BU5D_t1328083999* value)
	{
		___range_00c0_0556_0 = value;
		Il2CppCodeGenWriteBarrier(&___range_00c0_0556_0, value);
	}

	inline static int32_t get_offset_of_range_10a0_10c5_1() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_10a0_10c5_1)); }
	inline CharU5BU5D_t1328083999* get_range_10a0_10c5_1() const { return ___range_10a0_10c5_1; }
	inline CharU5BU5D_t1328083999** get_address_of_range_10a0_10c5_1() { return &___range_10a0_10c5_1; }
	inline void set_range_10a0_10c5_1(CharU5BU5D_t1328083999* value)
	{
		___range_10a0_10c5_1 = value;
		Il2CppCodeGenWriteBarrier(&___range_10a0_10c5_1, value);
	}

	inline static int32_t get_offset_of_range_1e00_1ffc_2() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_1e00_1ffc_2)); }
	inline CharU5BU5D_t1328083999* get_range_1e00_1ffc_2() const { return ___range_1e00_1ffc_2; }
	inline CharU5BU5D_t1328083999** get_address_of_range_1e00_1ffc_2() { return &___range_1e00_1ffc_2; }
	inline void set_range_1e00_1ffc_2(CharU5BU5D_t1328083999* value)
	{
		___range_1e00_1ffc_2 = value;
		Il2CppCodeGenWriteBarrier(&___range_1e00_1ffc_2, value);
	}

	inline static int32_t get_offset_of_range_2160_216f_3() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_2160_216f_3)); }
	inline CharU5BU5D_t1328083999* get_range_2160_216f_3() const { return ___range_2160_216f_3; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2160_216f_3() { return &___range_2160_216f_3; }
	inline void set_range_2160_216f_3(CharU5BU5D_t1328083999* value)
	{
		___range_2160_216f_3 = value;
		Il2CppCodeGenWriteBarrier(&___range_2160_216f_3, value);
	}

	inline static int32_t get_offset_of_range_24b6_24cf_4() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_24b6_24cf_4)); }
	inline CharU5BU5D_t1328083999* get_range_24b6_24cf_4() const { return ___range_24b6_24cf_4; }
	inline CharU5BU5D_t1328083999** get_address_of_range_24b6_24cf_4() { return &___range_24b6_24cf_4; }
	inline void set_range_24b6_24cf_4(CharU5BU5D_t1328083999* value)
	{
		___range_24b6_24cf_4 = value;
		Il2CppCodeGenWriteBarrier(&___range_24b6_24cf_4, value);
	}

	inline static int32_t get_offset_of_range_2c00_2c2e_5() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_2c00_2c2e_5)); }
	inline CharU5BU5D_t1328083999* get_range_2c00_2c2e_5() const { return ___range_2c00_2c2e_5; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2c00_2c2e_5() { return &___range_2c00_2c2e_5; }
	inline void set_range_2c00_2c2e_5(CharU5BU5D_t1328083999* value)
	{
		___range_2c00_2c2e_5 = value;
		Il2CppCodeGenWriteBarrier(&___range_2c00_2c2e_5, value);
	}

	inline static int32_t get_offset_of_range_2c60_2ce2_6() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_2c60_2ce2_6)); }
	inline CharU5BU5D_t1328083999* get_range_2c60_2ce2_6() const { return ___range_2c60_2ce2_6; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2c60_2ce2_6() { return &___range_2c60_2ce2_6; }
	inline void set_range_2c60_2ce2_6(CharU5BU5D_t1328083999* value)
	{
		___range_2c60_2ce2_6 = value;
		Il2CppCodeGenWriteBarrier(&___range_2c60_2ce2_6, value);
	}

	inline static int32_t get_offset_of_range_a640_a696_7() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_a640_a696_7)); }
	inline CharU5BU5D_t1328083999* get_range_a640_a696_7() const { return ___range_a640_a696_7; }
	inline CharU5BU5D_t1328083999** get_address_of_range_a640_a696_7() { return &___range_a640_a696_7; }
	inline void set_range_a640_a696_7(CharU5BU5D_t1328083999* value)
	{
		___range_a640_a696_7 = value;
		Il2CppCodeGenWriteBarrier(&___range_a640_a696_7, value);
	}

	inline static int32_t get_offset_of_range_a722_a78b_8() { return static_cast<int32_t>(offsetof(TextInfoToLowerData_t630797643_StaticFields, ___range_a722_a78b_8)); }
	inline CharU5BU5D_t1328083999* get_range_a722_a78b_8() const { return ___range_a722_a78b_8; }
	inline CharU5BU5D_t1328083999** get_address_of_range_a722_a78b_8() { return &___range_a722_a78b_8; }
	inline void set_range_a722_a78b_8(CharU5BU5D_t1328083999* value)
	{
		___range_a722_a78b_8 = value;
		Il2CppCodeGenWriteBarrier(&___range_a722_a78b_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
