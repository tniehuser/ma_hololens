﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Globalization.Unicode.Contraction
struct  Contraction_t1673853792  : public Il2CppObject
{
public:
	// System.Int32 Mono.Globalization.Unicode.Contraction::Index
	int32_t ___Index_0;
	// System.Char[] Mono.Globalization.Unicode.Contraction::Source
	CharU5BU5D_t1328083999* ___Source_1;
	// System.String Mono.Globalization.Unicode.Contraction::Replacement
	String_t* ___Replacement_2;
	// System.Byte[] Mono.Globalization.Unicode.Contraction::SortKey
	ByteU5BU5D_t3397334013* ___SortKey_3;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(Contraction_t1673853792, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Source_1() { return static_cast<int32_t>(offsetof(Contraction_t1673853792, ___Source_1)); }
	inline CharU5BU5D_t1328083999* get_Source_1() const { return ___Source_1; }
	inline CharU5BU5D_t1328083999** get_address_of_Source_1() { return &___Source_1; }
	inline void set_Source_1(CharU5BU5D_t1328083999* value)
	{
		___Source_1 = value;
		Il2CppCodeGenWriteBarrier(&___Source_1, value);
	}

	inline static int32_t get_offset_of_Replacement_2() { return static_cast<int32_t>(offsetof(Contraction_t1673853792, ___Replacement_2)); }
	inline String_t* get_Replacement_2() const { return ___Replacement_2; }
	inline String_t** get_address_of_Replacement_2() { return &___Replacement_2; }
	inline void set_Replacement_2(String_t* value)
	{
		___Replacement_2 = value;
		Il2CppCodeGenWriteBarrier(&___Replacement_2, value);
	}

	inline static int32_t get_offset_of_SortKey_3() { return static_cast<int32_t>(offsetof(Contraction_t1673853792, ___SortKey_3)); }
	inline ByteU5BU5D_t3397334013* get_SortKey_3() const { return ___SortKey_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_SortKey_3() { return &___SortKey_3; }
	inline void set_SortKey_3(ByteU5BU5D_t3397334013* value)
	{
		___SortKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___SortKey_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
