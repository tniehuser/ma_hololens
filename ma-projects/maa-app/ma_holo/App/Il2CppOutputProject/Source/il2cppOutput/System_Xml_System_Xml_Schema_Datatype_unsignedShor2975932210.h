﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_unsignedInt4210266973.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedShort
struct  Datatype_unsignedShort_t2975932210  : public Datatype_unsignedInt_t4210266973
{
public:

public:
};

struct Datatype_unsignedShort_t2975932210_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedShort::atomicValueType
	Type_t * ___atomicValueType_103;
	// System.Type System.Xml.Schema.Datatype_unsignedShort::listValueType
	Type_t * ___listValueType_104;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedShort::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_105;

public:
	inline static int32_t get_offset_of_atomicValueType_103() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___atomicValueType_103)); }
	inline Type_t * get_atomicValueType_103() const { return ___atomicValueType_103; }
	inline Type_t ** get_address_of_atomicValueType_103() { return &___atomicValueType_103; }
	inline void set_atomicValueType_103(Type_t * value)
	{
		___atomicValueType_103 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_103, value);
	}

	inline static int32_t get_offset_of_listValueType_104() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___listValueType_104)); }
	inline Type_t * get_listValueType_104() const { return ___listValueType_104; }
	inline Type_t ** get_address_of_listValueType_104() { return &___listValueType_104; }
	inline void set_listValueType_104(Type_t * value)
	{
		___listValueType_104 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_104, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_105() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___numeric10FacetsChecker_105)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_105() const { return ___numeric10FacetsChecker_105; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_105() { return &___numeric10FacetsChecker_105; }
	inline void set_numeric10FacetsChecker_105(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_105 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_105, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
