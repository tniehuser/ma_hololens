﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t431196913;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollection
struct  XmlSchemaCollection_t3518500204  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaCollection::collection
	Hashtable_t909839986 * ___collection_0;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaCollection::nameTable
	XmlNameTable_t1345805268 * ___nameTable_1;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XmlSchemaCollection::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_2;
	// System.Threading.ReaderWriterLock System.Xml.Schema.XmlSchemaCollection::wLock
	ReaderWriterLock_t431196913 * ___wLock_3;
	// System.Int32 System.Xml.Schema.XmlSchemaCollection::timeout
	int32_t ___timeout_4;
	// System.Boolean System.Xml.Schema.XmlSchemaCollection::isThreadSafe
	bool ___isThreadSafe_5;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaCollection::validationEventHandler
	ValidationEventHandler_t1580700381 * ___validationEventHandler_6;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaCollection::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_7;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___collection_0)); }
	inline Hashtable_t909839986 * get_collection_0() const { return ___collection_0; }
	inline Hashtable_t909839986 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(Hashtable_t909839986 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier(&___collection_0, value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___nameTable_1)); }
	inline XmlNameTable_t1345805268 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t1345805268 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_1, value);
	}

	inline static int32_t get_offset_of_schemaNames_2() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___schemaNames_2)); }
	inline SchemaNames_t1619962557 * get_schemaNames_2() const { return ___schemaNames_2; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_2() { return &___schemaNames_2; }
	inline void set_schemaNames_2(SchemaNames_t1619962557 * value)
	{
		___schemaNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___schemaNames_2, value);
	}

	inline static int32_t get_offset_of_wLock_3() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___wLock_3)); }
	inline ReaderWriterLock_t431196913 * get_wLock_3() const { return ___wLock_3; }
	inline ReaderWriterLock_t431196913 ** get_address_of_wLock_3() { return &___wLock_3; }
	inline void set_wLock_3(ReaderWriterLock_t431196913 * value)
	{
		___wLock_3 = value;
		Il2CppCodeGenWriteBarrier(&___wLock_3, value);
	}

	inline static int32_t get_offset_of_timeout_4() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___timeout_4)); }
	inline int32_t get_timeout_4() const { return ___timeout_4; }
	inline int32_t* get_address_of_timeout_4() { return &___timeout_4; }
	inline void set_timeout_4(int32_t value)
	{
		___timeout_4 = value;
	}

	inline static int32_t get_offset_of_isThreadSafe_5() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___isThreadSafe_5)); }
	inline bool get_isThreadSafe_5() const { return ___isThreadSafe_5; }
	inline bool* get_address_of_isThreadSafe_5() { return &___isThreadSafe_5; }
	inline void set_isThreadSafe_5(bool value)
	{
		___isThreadSafe_5 = value;
	}

	inline static int32_t get_offset_of_validationEventHandler_6() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___validationEventHandler_6)); }
	inline ValidationEventHandler_t1580700381 * get_validationEventHandler_6() const { return ___validationEventHandler_6; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_validationEventHandler_6() { return &___validationEventHandler_6; }
	inline void set_validationEventHandler_6(ValidationEventHandler_t1580700381 * value)
	{
		___validationEventHandler_6 = value;
		Il2CppCodeGenWriteBarrier(&___validationEventHandler_6, value);
	}

	inline static int32_t get_offset_of_xmlResolver_7() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___xmlResolver_7)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_7() const { return ___xmlResolver_7; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_7() { return &___xmlResolver_7; }
	inline void set_xmlResolver_7(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_7 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
