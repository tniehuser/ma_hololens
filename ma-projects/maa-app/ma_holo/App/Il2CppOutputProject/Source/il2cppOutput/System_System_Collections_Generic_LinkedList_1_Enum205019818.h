﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Collections.Generic.LinkedList`1<System.WeakReference>
struct LinkedList_1_t1382113796;
// System.Collections.Generic.LinkedListNode`1<System.WeakReference>
struct LinkedListNode_1_t4268478776;
// System.WeakReference
struct WeakReference_t1077405567;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1/Enumerator<System.WeakReference>
struct  Enumerator_t205019818 
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::list
	LinkedList_1_t1382113796 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::node
	LinkedListNode_1_t4268478776 * ___node_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.LinkedList`1/Enumerator::current
	WeakReference_t1077405567 * ___current_3;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::index
	int32_t ___index_4;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1/Enumerator::siInfo
	SerializationInfo_t228987430 * ___siInfo_5;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___list_0)); }
	inline LinkedList_1_t1382113796 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t1382113796 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t1382113796 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_node_1() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___node_1)); }
	inline LinkedListNode_1_t4268478776 * get_node_1() const { return ___node_1; }
	inline LinkedListNode_1_t4268478776 ** get_address_of_node_1() { return &___node_1; }
	inline void set_node_1(LinkedListNode_1_t4268478776 * value)
	{
		___node_1 = value;
		Il2CppCodeGenWriteBarrier(&___node_1, value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___current_3)); }
	inline WeakReference_t1077405567 * get_current_3() const { return ___current_3; }
	inline WeakReference_t1077405567 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(WeakReference_t1077405567 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier(&___current_3, value);
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_siInfo_5() { return static_cast<int32_t>(offsetof(Enumerator_t205019818, ___siInfo_5)); }
	inline SerializationInfo_t228987430 * get_siInfo_5() const { return ___siInfo_5; }
	inline SerializationInfo_t228987430 ** get_address_of_siInfo_5() { return &___siInfo_5; }
	inline void set_siInfo_5(SerializationInfo_t228987430 * value)
	{
		___siInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___siInfo_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
