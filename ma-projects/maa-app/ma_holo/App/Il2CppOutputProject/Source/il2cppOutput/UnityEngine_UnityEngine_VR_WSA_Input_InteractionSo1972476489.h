﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_VR_WSA_Input_InteractionSo2135528789.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VR.WSA.Input.InteractionSource
struct  InteractionSource_t1972476489 
{
public:
	// System.UInt32 UnityEngine.VR.WSA.Input.InteractionSource::m_id
	uint32_t ___m_id_0;
	// UnityEngine.VR.WSA.Input.InteractionSourceKind UnityEngine.VR.WSA.Input.InteractionSource::m_kind
	int32_t ___m_kind_1;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(InteractionSource_t1972476489, ___m_id_0)); }
	inline uint32_t get_m_id_0() const { return ___m_id_0; }
	inline uint32_t* get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(uint32_t value)
	{
		___m_id_0 = value;
	}

	inline static int32_t get_offset_of_m_kind_1() { return static_cast<int32_t>(offsetof(InteractionSource_t1972476489, ___m_kind_1)); }
	inline int32_t get_m_kind_1() const { return ___m_kind_1; }
	inline int32_t* get_address_of_m_kind_1() { return &___m_kind_1; }
	inline void set_m_kind_1(int32_t value)
	{
		___m_kind_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
