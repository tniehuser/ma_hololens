﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ParameterizedStrings/LowLevelStack
struct LowLevelStack_t375164147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParameterizedStrings
struct  ParameterizedStrings_t2474305833  : public Il2CppObject
{
public:

public:
};

struct ParameterizedStrings_t2474305833_ThreadStaticFields
{
public:
	// System.ParameterizedStrings/LowLevelStack System.ParameterizedStrings::_cachedStack
	LowLevelStack_t375164147 * ____cachedStack_0;

public:
	inline static int32_t get_offset_of__cachedStack_0() { return static_cast<int32_t>(offsetof(ParameterizedStrings_t2474305833_ThreadStaticFields, ____cachedStack_0)); }
	inline LowLevelStack_t375164147 * get__cachedStack_0() const { return ____cachedStack_0; }
	inline LowLevelStack_t375164147 ** get_address_of__cachedStack_0() { return &____cachedStack_0; }
	inline void set__cachedStack_0(LowLevelStack_t375164147 * value)
	{
		____cachedStack_0 = value;
		Il2CppCodeGenWriteBarrier(&____cachedStack_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
