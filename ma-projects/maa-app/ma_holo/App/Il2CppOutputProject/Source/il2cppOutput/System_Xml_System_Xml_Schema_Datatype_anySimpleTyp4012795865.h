﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_DatatypeImplementatio1152094268.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anySimpleType
struct  Datatype_anySimpleType_t4012795865  : public DatatypeImplementation_t1152094268
{
public:

public:
};

struct Datatype_anySimpleType_t4012795865_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anySimpleType::atomicValueType
	Type_t * ___atomicValueType_91;
	// System.Type System.Xml.Schema.Datatype_anySimpleType::listValueType
	Type_t * ___listValueType_92;

public:
	inline static int32_t get_offset_of_atomicValueType_91() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_t4012795865_StaticFields, ___atomicValueType_91)); }
	inline Type_t * get_atomicValueType_91() const { return ___atomicValueType_91; }
	inline Type_t ** get_address_of_atomicValueType_91() { return &___atomicValueType_91; }
	inline void set_atomicValueType_91(Type_t * value)
	{
		___atomicValueType_91 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_91, value);
	}

	inline static int32_t get_offset_of_listValueType_92() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_t4012795865_StaticFields, ___listValueType_92)); }
	inline Type_t * get_listValueType_92() const { return ___listValueType_92; }
	inline Type_t ** get_address_of_listValueType_92() { return &___listValueType_92; }
	inline void set_listValueType_92(Type_t * value)
	{
		___listValueType_92 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_92, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
