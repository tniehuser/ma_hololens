﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Bits
struct  Bits_t2854626690  : public Il2CppObject
{
public:

public:
};

struct Bits_t2854626690_StaticFields
{
public:
	// System.UInt32 System.Xml.Bits::MASK_0101010101010101
	uint32_t ___MASK_0101010101010101_0;
	// System.UInt32 System.Xml.Bits::MASK_0011001100110011
	uint32_t ___MASK_0011001100110011_1;
	// System.UInt32 System.Xml.Bits::MASK_0000111100001111
	uint32_t ___MASK_0000111100001111_2;
	// System.UInt32 System.Xml.Bits::MASK_0000000011111111
	uint32_t ___MASK_0000000011111111_3;
	// System.UInt32 System.Xml.Bits::MASK_1111111111111111
	uint32_t ___MASK_1111111111111111_4;

public:
	inline static int32_t get_offset_of_MASK_0101010101010101_0() { return static_cast<int32_t>(offsetof(Bits_t2854626690_StaticFields, ___MASK_0101010101010101_0)); }
	inline uint32_t get_MASK_0101010101010101_0() const { return ___MASK_0101010101010101_0; }
	inline uint32_t* get_address_of_MASK_0101010101010101_0() { return &___MASK_0101010101010101_0; }
	inline void set_MASK_0101010101010101_0(uint32_t value)
	{
		___MASK_0101010101010101_0 = value;
	}

	inline static int32_t get_offset_of_MASK_0011001100110011_1() { return static_cast<int32_t>(offsetof(Bits_t2854626690_StaticFields, ___MASK_0011001100110011_1)); }
	inline uint32_t get_MASK_0011001100110011_1() const { return ___MASK_0011001100110011_1; }
	inline uint32_t* get_address_of_MASK_0011001100110011_1() { return &___MASK_0011001100110011_1; }
	inline void set_MASK_0011001100110011_1(uint32_t value)
	{
		___MASK_0011001100110011_1 = value;
	}

	inline static int32_t get_offset_of_MASK_0000111100001111_2() { return static_cast<int32_t>(offsetof(Bits_t2854626690_StaticFields, ___MASK_0000111100001111_2)); }
	inline uint32_t get_MASK_0000111100001111_2() const { return ___MASK_0000111100001111_2; }
	inline uint32_t* get_address_of_MASK_0000111100001111_2() { return &___MASK_0000111100001111_2; }
	inline void set_MASK_0000111100001111_2(uint32_t value)
	{
		___MASK_0000111100001111_2 = value;
	}

	inline static int32_t get_offset_of_MASK_0000000011111111_3() { return static_cast<int32_t>(offsetof(Bits_t2854626690_StaticFields, ___MASK_0000000011111111_3)); }
	inline uint32_t get_MASK_0000000011111111_3() const { return ___MASK_0000000011111111_3; }
	inline uint32_t* get_address_of_MASK_0000000011111111_3() { return &___MASK_0000000011111111_3; }
	inline void set_MASK_0000000011111111_3(uint32_t value)
	{
		___MASK_0000000011111111_3 = value;
	}

	inline static int32_t get_offset_of_MASK_1111111111111111_4() { return static_cast<int32_t>(offsetof(Bits_t2854626690_StaticFields, ___MASK_1111111111111111_4)); }
	inline uint32_t get_MASK_1111111111111111_4() const { return ___MASK_1111111111111111_4; }
	inline uint32_t* get_address_of_MASK_1111111111111111_4() { return &___MASK_1111111111111111_4; }
	inline void set_MASK_1111111111111111_4(uint32_t value)
	{
		___MASK_1111111111111111_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
