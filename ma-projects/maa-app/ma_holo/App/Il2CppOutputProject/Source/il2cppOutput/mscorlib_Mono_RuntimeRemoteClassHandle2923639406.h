﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeStructs_RemoteClass3863182804.h"

// Mono.RuntimeStructs/RemoteClass
struct RemoteClass_t3863182804;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeRemoteClassHandle
struct  RuntimeRemoteClassHandle_t2923639406 
{
public:
	// Mono.RuntimeStructs/RemoteClass* Mono.RuntimeRemoteClassHandle::value
	RemoteClass_t3863182804 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeRemoteClassHandle_t2923639406, ___value_0)); }
	inline RemoteClass_t3863182804 * get_value_0() const { return ___value_0; }
	inline RemoteClass_t3863182804 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RemoteClass_t3863182804 * value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeRemoteClassHandle
struct RuntimeRemoteClassHandle_t2923639406_marshaled_pinvoke
{
	RemoteClass_t3863182804 * ___value_0;
};
// Native definition for COM marshalling of Mono.RuntimeRemoteClassHandle
struct RuntimeRemoteClassHandle_t2923639406_marshaled_com
{
	RemoteClass_t3863182804 * ___value_0;
};
