﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IO_FileAttributes3843045335.h"
#include "mscorlib_System_IO_MonoIOError733012845.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_IO_SearchOption3245573.h"

// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Directory/<EnumerateKind>c__Iterator0
struct  U3CEnumerateKindU3Ec__Iterator0_t3216484634  : public Il2CppObject
{
public:
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::searchPattern
	String_t* ___searchPattern_0;
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::path
	String_t* ___path_1;
	// System.Boolean System.IO.Directory/<EnumerateKind>c__Iterator0::<stop>__0
	bool ___U3CstopU3E__0_2;
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::<path_with_pattern>__0
	String_t* ___U3Cpath_with_patternU3E__0_3;
	// System.IO.FileAttributes System.IO.Directory/<EnumerateKind>c__Iterator0::<rattr>__0
	int32_t ___U3CrattrU3E__0_4;
	// System.IO.MonoIOError System.IO.Directory/<EnumerateKind>c__Iterator0::<error>__0
	int32_t ___U3CerrorU3E__0_5;
	// System.IntPtr System.IO.Directory/<EnumerateKind>c__Iterator0::<handle>__0
	IntPtr_t ___U3ChandleU3E__0_6;
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::<s>__0
	String_t* ___U3CsU3E__0_7;
	// System.IO.FileAttributes System.IO.Directory/<EnumerateKind>c__Iterator0::kind
	int32_t ___kind_8;
	// System.IO.SearchOption System.IO.Directory/<EnumerateKind>c__Iterator0::searchOption
	int32_t ___searchOption_9;
	// System.Collections.Generic.IEnumerator`1<System.String> System.IO.Directory/<EnumerateKind>c__Iterator0::$locvar0
	Il2CppObject* ___U24locvar0_10;
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::<child>__1
	String_t* ___U3CchildU3E__1_11;
	// System.String System.IO.Directory/<EnumerateKind>c__Iterator0::$current
	String_t* ___U24current_12;
	// System.Boolean System.IO.Directory/<EnumerateKind>c__Iterator0::$disposing
	bool ___U24disposing_13;
	// System.Int32 System.IO.Directory/<EnumerateKind>c__Iterator0::$PC
	int32_t ___U24PC_14;

public:
	inline static int32_t get_offset_of_searchPattern_0() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___searchPattern_0)); }
	inline String_t* get_searchPattern_0() const { return ___searchPattern_0; }
	inline String_t** get_address_of_searchPattern_0() { return &___searchPattern_0; }
	inline void set_searchPattern_0(String_t* value)
	{
		___searchPattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___searchPattern_0, value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier(&___path_1, value);
	}

	inline static int32_t get_offset_of_U3CstopU3E__0_2() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3CstopU3E__0_2)); }
	inline bool get_U3CstopU3E__0_2() const { return ___U3CstopU3E__0_2; }
	inline bool* get_address_of_U3CstopU3E__0_2() { return &___U3CstopU3E__0_2; }
	inline void set_U3CstopU3E__0_2(bool value)
	{
		___U3CstopU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cpath_with_patternU3E__0_3() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3Cpath_with_patternU3E__0_3)); }
	inline String_t* get_U3Cpath_with_patternU3E__0_3() const { return ___U3Cpath_with_patternU3E__0_3; }
	inline String_t** get_address_of_U3Cpath_with_patternU3E__0_3() { return &___U3Cpath_with_patternU3E__0_3; }
	inline void set_U3Cpath_with_patternU3E__0_3(String_t* value)
	{
		___U3Cpath_with_patternU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cpath_with_patternU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CrattrU3E__0_4() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3CrattrU3E__0_4)); }
	inline int32_t get_U3CrattrU3E__0_4() const { return ___U3CrattrU3E__0_4; }
	inline int32_t* get_address_of_U3CrattrU3E__0_4() { return &___U3CrattrU3E__0_4; }
	inline void set_U3CrattrU3E__0_4(int32_t value)
	{
		___U3CrattrU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CerrorU3E__0_5() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3CerrorU3E__0_5)); }
	inline int32_t get_U3CerrorU3E__0_5() const { return ___U3CerrorU3E__0_5; }
	inline int32_t* get_address_of_U3CerrorU3E__0_5() { return &___U3CerrorU3E__0_5; }
	inline void set_U3CerrorU3E__0_5(int32_t value)
	{
		___U3CerrorU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3ChandleU3E__0_6() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3ChandleU3E__0_6)); }
	inline IntPtr_t get_U3ChandleU3E__0_6() const { return ___U3ChandleU3E__0_6; }
	inline IntPtr_t* get_address_of_U3ChandleU3E__0_6() { return &___U3ChandleU3E__0_6; }
	inline void set_U3ChandleU3E__0_6(IntPtr_t value)
	{
		___U3ChandleU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CsU3E__0_7() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3CsU3E__0_7)); }
	inline String_t* get_U3CsU3E__0_7() const { return ___U3CsU3E__0_7; }
	inline String_t** get_address_of_U3CsU3E__0_7() { return &___U3CsU3E__0_7; }
	inline void set_U3CsU3E__0_7(String_t* value)
	{
		___U3CsU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E__0_7, value);
	}

	inline static int32_t get_offset_of_kind_8() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___kind_8)); }
	inline int32_t get_kind_8() const { return ___kind_8; }
	inline int32_t* get_address_of_kind_8() { return &___kind_8; }
	inline void set_kind_8(int32_t value)
	{
		___kind_8 = value;
	}

	inline static int32_t get_offset_of_searchOption_9() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___searchOption_9)); }
	inline int32_t get_searchOption_9() const { return ___searchOption_9; }
	inline int32_t* get_address_of_searchOption_9() { return &___searchOption_9; }
	inline void set_searchOption_9(int32_t value)
	{
		___searchOption_9 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_10() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U24locvar0_10)); }
	inline Il2CppObject* get_U24locvar0_10() const { return ___U24locvar0_10; }
	inline Il2CppObject** get_address_of_U24locvar0_10() { return &___U24locvar0_10; }
	inline void set_U24locvar0_10(Il2CppObject* value)
	{
		___U24locvar0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_10, value);
	}

	inline static int32_t get_offset_of_U3CchildU3E__1_11() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U3CchildU3E__1_11)); }
	inline String_t* get_U3CchildU3E__1_11() const { return ___U3CchildU3E__1_11; }
	inline String_t** get_address_of_U3CchildU3E__1_11() { return &___U3CchildU3E__1_11; }
	inline void set_U3CchildU3E__1_11(String_t* value)
	{
		___U3CchildU3E__1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__1_11, value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U24current_12)); }
	inline String_t* get_U24current_12() const { return ___U24current_12; }
	inline String_t** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(String_t* value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CEnumerateKindU3Ec__Iterator0_t3216484634, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
