﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_RuntimeMethodHandle894824333.h"
#include "mscorlib_System_Reflection_MethodAttributes790385034.h"
#include "mscorlib_System_Reflection_MethodImplAttributes1541361196.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet2778376310.h"
#include "mscorlib_System_Runtime_InteropServices_CallingCon3354538265.h"
#include "mscorlib_System_Reflection_CallingConventions1097349142.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t99948092;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t2122994367;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t358971386;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2318378278;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t3643576428;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MethodBuilder
struct  MethodBuilder_t644187984  : public MethodInfo_t
{
public:
	// System.RuntimeMethodHandle System.Reflection.Emit.MethodBuilder::mhandle
	RuntimeMethodHandle_t894824333  ___mhandle_0;
	// System.Type System.Reflection.Emit.MethodBuilder::rtype
	Type_t * ___rtype_1;
	// System.Type[] System.Reflection.Emit.MethodBuilder::parameters
	TypeU5BU5D_t1664964607* ___parameters_2;
	// System.Reflection.MethodAttributes System.Reflection.Emit.MethodBuilder::attrs
	int32_t ___attrs_3;
	// System.Reflection.MethodImplAttributes System.Reflection.Emit.MethodBuilder::iattrs
	int32_t ___iattrs_4;
	// System.String System.Reflection.Emit.MethodBuilder::name
	String_t* ___name_5;
	// System.Int32 System.Reflection.Emit.MethodBuilder::table_idx
	int32_t ___table_idx_6;
	// System.Byte[] System.Reflection.Emit.MethodBuilder::code
	ByteU5BU5D_t3397334013* ___code_7;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.MethodBuilder::ilgen
	ILGenerator_t99948092 * ___ilgen_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.MethodBuilder::type
	TypeBuilder_t3308873219 * ___type_9;
	// System.Reflection.Emit.ParameterBuilder[] System.Reflection.Emit.MethodBuilder::pinfo
	ParameterBuilderU5BU5D_t2122994367* ___pinfo_10;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.MethodBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_11;
	// System.Reflection.MethodInfo[] System.Reflection.Emit.MethodBuilder::override_methods
	MethodInfoU5BU5D_t152480188* ___override_methods_12;
	// System.String System.Reflection.Emit.MethodBuilder::pi_dll
	String_t* ___pi_dll_13;
	// System.String System.Reflection.Emit.MethodBuilder::pi_entry
	String_t* ___pi_entry_14;
	// System.Runtime.InteropServices.CharSet System.Reflection.Emit.MethodBuilder::charset
	int32_t ___charset_15;
	// System.UInt32 System.Reflection.Emit.MethodBuilder::extra_flags
	uint32_t ___extra_flags_16;
	// System.Runtime.InteropServices.CallingConvention System.Reflection.Emit.MethodBuilder::native_cc
	int32_t ___native_cc_17;
	// System.Reflection.CallingConventions System.Reflection.Emit.MethodBuilder::call_conv
	int32_t ___call_conv_18;
	// System.Boolean System.Reflection.Emit.MethodBuilder::init_locals
	bool ___init_locals_19;
	// System.IntPtr System.Reflection.Emit.MethodBuilder::generic_container
	IntPtr_t ___generic_container_20;
	// System.Reflection.Emit.GenericTypeParameterBuilder[] System.Reflection.Emit.MethodBuilder::generic_params
	GenericTypeParameterBuilderU5BU5D_t358971386* ___generic_params_21;
	// System.Type[] System.Reflection.Emit.MethodBuilder::returnModReq
	TypeU5BU5D_t1664964607* ___returnModReq_22;
	// System.Type[] System.Reflection.Emit.MethodBuilder::returnModOpt
	TypeU5BU5D_t1664964607* ___returnModOpt_23;
	// System.Type[][] System.Reflection.Emit.MethodBuilder::paramModReq
	TypeU5BU5DU5BU5D_t2318378278* ___paramModReq_24;
	// System.Type[][] System.Reflection.Emit.MethodBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t2318378278* ___paramModOpt_25;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.MethodBuilder::permissions
	RefEmitPermissionSetU5BU5D_t3643576428* ___permissions_26;

public:
	inline static int32_t get_offset_of_mhandle_0() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___mhandle_0)); }
	inline RuntimeMethodHandle_t894824333  get_mhandle_0() const { return ___mhandle_0; }
	inline RuntimeMethodHandle_t894824333 * get_address_of_mhandle_0() { return &___mhandle_0; }
	inline void set_mhandle_0(RuntimeMethodHandle_t894824333  value)
	{
		___mhandle_0 = value;
	}

	inline static int32_t get_offset_of_rtype_1() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___rtype_1)); }
	inline Type_t * get_rtype_1() const { return ___rtype_1; }
	inline Type_t ** get_address_of_rtype_1() { return &___rtype_1; }
	inline void set_rtype_1(Type_t * value)
	{
		___rtype_1 = value;
		Il2CppCodeGenWriteBarrier(&___rtype_1, value);
	}

	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___parameters_2)); }
	inline TypeU5BU5D_t1664964607* get_parameters_2() const { return ___parameters_2; }
	inline TypeU5BU5D_t1664964607** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(TypeU5BU5D_t1664964607* value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_2, value);
	}

	inline static int32_t get_offset_of_attrs_3() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___attrs_3)); }
	inline int32_t get_attrs_3() const { return ___attrs_3; }
	inline int32_t* get_address_of_attrs_3() { return &___attrs_3; }
	inline void set_attrs_3(int32_t value)
	{
		___attrs_3 = value;
	}

	inline static int32_t get_offset_of_iattrs_4() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___iattrs_4)); }
	inline int32_t get_iattrs_4() const { return ___iattrs_4; }
	inline int32_t* get_address_of_iattrs_4() { return &___iattrs_4; }
	inline void set_iattrs_4(int32_t value)
	{
		___iattrs_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_table_idx_6() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___table_idx_6)); }
	inline int32_t get_table_idx_6() const { return ___table_idx_6; }
	inline int32_t* get_address_of_table_idx_6() { return &___table_idx_6; }
	inline void set_table_idx_6(int32_t value)
	{
		___table_idx_6 = value;
	}

	inline static int32_t get_offset_of_code_7() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___code_7)); }
	inline ByteU5BU5D_t3397334013* get_code_7() const { return ___code_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_code_7() { return &___code_7; }
	inline void set_code_7(ByteU5BU5D_t3397334013* value)
	{
		___code_7 = value;
		Il2CppCodeGenWriteBarrier(&___code_7, value);
	}

	inline static int32_t get_offset_of_ilgen_8() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___ilgen_8)); }
	inline ILGenerator_t99948092 * get_ilgen_8() const { return ___ilgen_8; }
	inline ILGenerator_t99948092 ** get_address_of_ilgen_8() { return &___ilgen_8; }
	inline void set_ilgen_8(ILGenerator_t99948092 * value)
	{
		___ilgen_8 = value;
		Il2CppCodeGenWriteBarrier(&___ilgen_8, value);
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___type_9)); }
	inline TypeBuilder_t3308873219 * get_type_9() const { return ___type_9; }
	inline TypeBuilder_t3308873219 ** get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(TypeBuilder_t3308873219 * value)
	{
		___type_9 = value;
		Il2CppCodeGenWriteBarrier(&___type_9, value);
	}

	inline static int32_t get_offset_of_pinfo_10() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___pinfo_10)); }
	inline ParameterBuilderU5BU5D_t2122994367* get_pinfo_10() const { return ___pinfo_10; }
	inline ParameterBuilderU5BU5D_t2122994367** get_address_of_pinfo_10() { return &___pinfo_10; }
	inline void set_pinfo_10(ParameterBuilderU5BU5D_t2122994367* value)
	{
		___pinfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___pinfo_10, value);
	}

	inline static int32_t get_offset_of_cattrs_11() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___cattrs_11)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_11() const { return ___cattrs_11; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_11() { return &___cattrs_11; }
	inline void set_cattrs_11(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_11 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_11, value);
	}

	inline static int32_t get_offset_of_override_methods_12() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___override_methods_12)); }
	inline MethodInfoU5BU5D_t152480188* get_override_methods_12() const { return ___override_methods_12; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_override_methods_12() { return &___override_methods_12; }
	inline void set_override_methods_12(MethodInfoU5BU5D_t152480188* value)
	{
		___override_methods_12 = value;
		Il2CppCodeGenWriteBarrier(&___override_methods_12, value);
	}

	inline static int32_t get_offset_of_pi_dll_13() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___pi_dll_13)); }
	inline String_t* get_pi_dll_13() const { return ___pi_dll_13; }
	inline String_t** get_address_of_pi_dll_13() { return &___pi_dll_13; }
	inline void set_pi_dll_13(String_t* value)
	{
		___pi_dll_13 = value;
		Il2CppCodeGenWriteBarrier(&___pi_dll_13, value);
	}

	inline static int32_t get_offset_of_pi_entry_14() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___pi_entry_14)); }
	inline String_t* get_pi_entry_14() const { return ___pi_entry_14; }
	inline String_t** get_address_of_pi_entry_14() { return &___pi_entry_14; }
	inline void set_pi_entry_14(String_t* value)
	{
		___pi_entry_14 = value;
		Il2CppCodeGenWriteBarrier(&___pi_entry_14, value);
	}

	inline static int32_t get_offset_of_charset_15() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___charset_15)); }
	inline int32_t get_charset_15() const { return ___charset_15; }
	inline int32_t* get_address_of_charset_15() { return &___charset_15; }
	inline void set_charset_15(int32_t value)
	{
		___charset_15 = value;
	}

	inline static int32_t get_offset_of_extra_flags_16() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___extra_flags_16)); }
	inline uint32_t get_extra_flags_16() const { return ___extra_flags_16; }
	inline uint32_t* get_address_of_extra_flags_16() { return &___extra_flags_16; }
	inline void set_extra_flags_16(uint32_t value)
	{
		___extra_flags_16 = value;
	}

	inline static int32_t get_offset_of_native_cc_17() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___native_cc_17)); }
	inline int32_t get_native_cc_17() const { return ___native_cc_17; }
	inline int32_t* get_address_of_native_cc_17() { return &___native_cc_17; }
	inline void set_native_cc_17(int32_t value)
	{
		___native_cc_17 = value;
	}

	inline static int32_t get_offset_of_call_conv_18() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___call_conv_18)); }
	inline int32_t get_call_conv_18() const { return ___call_conv_18; }
	inline int32_t* get_address_of_call_conv_18() { return &___call_conv_18; }
	inline void set_call_conv_18(int32_t value)
	{
		___call_conv_18 = value;
	}

	inline static int32_t get_offset_of_init_locals_19() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___init_locals_19)); }
	inline bool get_init_locals_19() const { return ___init_locals_19; }
	inline bool* get_address_of_init_locals_19() { return &___init_locals_19; }
	inline void set_init_locals_19(bool value)
	{
		___init_locals_19 = value;
	}

	inline static int32_t get_offset_of_generic_container_20() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___generic_container_20)); }
	inline IntPtr_t get_generic_container_20() const { return ___generic_container_20; }
	inline IntPtr_t* get_address_of_generic_container_20() { return &___generic_container_20; }
	inline void set_generic_container_20(IntPtr_t value)
	{
		___generic_container_20 = value;
	}

	inline static int32_t get_offset_of_generic_params_21() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___generic_params_21)); }
	inline GenericTypeParameterBuilderU5BU5D_t358971386* get_generic_params_21() const { return ___generic_params_21; }
	inline GenericTypeParameterBuilderU5BU5D_t358971386** get_address_of_generic_params_21() { return &___generic_params_21; }
	inline void set_generic_params_21(GenericTypeParameterBuilderU5BU5D_t358971386* value)
	{
		___generic_params_21 = value;
		Il2CppCodeGenWriteBarrier(&___generic_params_21, value);
	}

	inline static int32_t get_offset_of_returnModReq_22() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___returnModReq_22)); }
	inline TypeU5BU5D_t1664964607* get_returnModReq_22() const { return ___returnModReq_22; }
	inline TypeU5BU5D_t1664964607** get_address_of_returnModReq_22() { return &___returnModReq_22; }
	inline void set_returnModReq_22(TypeU5BU5D_t1664964607* value)
	{
		___returnModReq_22 = value;
		Il2CppCodeGenWriteBarrier(&___returnModReq_22, value);
	}

	inline static int32_t get_offset_of_returnModOpt_23() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___returnModOpt_23)); }
	inline TypeU5BU5D_t1664964607* get_returnModOpt_23() const { return ___returnModOpt_23; }
	inline TypeU5BU5D_t1664964607** get_address_of_returnModOpt_23() { return &___returnModOpt_23; }
	inline void set_returnModOpt_23(TypeU5BU5D_t1664964607* value)
	{
		___returnModOpt_23 = value;
		Il2CppCodeGenWriteBarrier(&___returnModOpt_23, value);
	}

	inline static int32_t get_offset_of_paramModReq_24() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___paramModReq_24)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModReq_24() const { return ___paramModReq_24; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModReq_24() { return &___paramModReq_24; }
	inline void set_paramModReq_24(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModReq_24 = value;
		Il2CppCodeGenWriteBarrier(&___paramModReq_24, value);
	}

	inline static int32_t get_offset_of_paramModOpt_25() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___paramModOpt_25)); }
	inline TypeU5BU5DU5BU5D_t2318378278* get_paramModOpt_25() const { return ___paramModOpt_25; }
	inline TypeU5BU5DU5BU5D_t2318378278** get_address_of_paramModOpt_25() { return &___paramModOpt_25; }
	inline void set_paramModOpt_25(TypeU5BU5DU5BU5D_t2318378278* value)
	{
		___paramModOpt_25 = value;
		Il2CppCodeGenWriteBarrier(&___paramModOpt_25, value);
	}

	inline static int32_t get_offset_of_permissions_26() { return static_cast<int32_t>(offsetof(MethodBuilder_t644187984, ___permissions_26)); }
	inline RefEmitPermissionSetU5BU5D_t3643576428* get_permissions_26() const { return ___permissions_26; }
	inline RefEmitPermissionSetU5BU5D_t3643576428** get_address_of_permissions_26() { return &___permissions_26; }
	inline void set_permissions_26(RefEmitPermissionSetU5BU5D_t3643576428* value)
	{
		___permissions_26 = value;
		Il2CppCodeGenWriteBarrier(&___permissions_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
