﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_ContentValidator2510151843.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Xml.Schema.BitSet
struct BitSet_t1062448123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AllElementsContentValidator
struct  AllElementsContentValidator_t2095068475  : public ContentValidator_t2510151843
{
public:
	// System.Collections.Hashtable System.Xml.Schema.AllElementsContentValidator::elements
	Hashtable_t909839986 * ___elements_7;
	// System.Object[] System.Xml.Schema.AllElementsContentValidator::particles
	ObjectU5BU5D_t3614634134* ___particles_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.AllElementsContentValidator::isRequired
	BitSet_t1062448123 * ___isRequired_9;
	// System.Int32 System.Xml.Schema.AllElementsContentValidator::countRequired
	int32_t ___countRequired_10;

public:
	inline static int32_t get_offset_of_elements_7() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___elements_7)); }
	inline Hashtable_t909839986 * get_elements_7() const { return ___elements_7; }
	inline Hashtable_t909839986 ** get_address_of_elements_7() { return &___elements_7; }
	inline void set_elements_7(Hashtable_t909839986 * value)
	{
		___elements_7 = value;
		Il2CppCodeGenWriteBarrier(&___elements_7, value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___particles_8)); }
	inline ObjectU5BU5D_t3614634134* get_particles_8() const { return ___particles_8; }
	inline ObjectU5BU5D_t3614634134** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ObjectU5BU5D_t3614634134* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier(&___particles_8, value);
	}

	inline static int32_t get_offset_of_isRequired_9() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___isRequired_9)); }
	inline BitSet_t1062448123 * get_isRequired_9() const { return ___isRequired_9; }
	inline BitSet_t1062448123 ** get_address_of_isRequired_9() { return &___isRequired_9; }
	inline void set_isRequired_9(BitSet_t1062448123 * value)
	{
		___isRequired_9 = value;
		Il2CppCodeGenWriteBarrier(&___isRequired_9, value);
	}

	inline static int32_t get_offset_of_countRequired_10() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___countRequired_10)); }
	inline int32_t get_countRequired_10() const { return ___countRequired_10; }
	inline int32_t* get_address_of_countRequired_10() { return &___countRequired_10; }
	inline void set_countRequired_10(int32_t value)
	{
		___countRequired_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
