﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_Formatting1126649075.h"
#include "System_Xml_System_Xml_XmlTextWriter_State563545493.h"
#include "System_Xml_System_Xml_XmlTextWriter_Token2485601873.h"
#include "System_Xml_System_Xml_XmlTextWriter_SpecialAttr1516144018.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"

// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t2806209204;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Xml.XmlTextWriter/TagInfo[]
struct TagInfoU5BU5D_t2495379871;
// System.Xml.XmlTextWriter/State[]
struct StateU5BU5D_t222040184;
// System.Xml.XmlTextWriterBase64Encoder
struct XmlTextWriterBase64Encoder_t365185068;
// System.String
struct String_t;
// System.Xml.XmlTextWriter/Namespace[]
struct NamespaceU5BU5D_t409646736;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_t2527250655  : public XmlWriter_t1048088568
{
public:
	// System.IO.TextWriter System.Xml.XmlTextWriter::textWriter
	TextWriter_t4027217640 * ___textWriter_1;
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriter::xmlEncoder
	XmlTextEncoder_t2806209204 * ___xmlEncoder_2;
	// System.Text.Encoding System.Xml.XmlTextWriter::encoding
	Encoding_t663144255 * ___encoding_3;
	// System.Xml.Formatting System.Xml.XmlTextWriter::formatting
	int32_t ___formatting_4;
	// System.Boolean System.Xml.XmlTextWriter::indented
	bool ___indented_5;
	// System.Int32 System.Xml.XmlTextWriter::indentation
	int32_t ___indentation_6;
	// System.Char System.Xml.XmlTextWriter::indentChar
	Il2CppChar ___indentChar_7;
	// System.Xml.XmlTextWriter/TagInfo[] System.Xml.XmlTextWriter::stack
	TagInfoU5BU5D_t2495379871* ___stack_8;
	// System.Int32 System.Xml.XmlTextWriter::top
	int32_t ___top_9;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTable
	StateU5BU5D_t222040184* ___stateTable_10;
	// System.Xml.XmlTextWriter/State System.Xml.XmlTextWriter::currentState
	int32_t ___currentState_11;
	// System.Xml.XmlTextWriter/Token System.Xml.XmlTextWriter::lastToken
	int32_t ___lastToken_12;
	// System.Xml.XmlTextWriterBase64Encoder System.Xml.XmlTextWriter::base64Encoder
	XmlTextWriterBase64Encoder_t365185068 * ___base64Encoder_13;
	// System.Char System.Xml.XmlTextWriter::quoteChar
	Il2CppChar ___quoteChar_14;
	// System.Char System.Xml.XmlTextWriter::curQuoteChar
	Il2CppChar ___curQuoteChar_15;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_16;
	// System.Xml.XmlTextWriter/SpecialAttr System.Xml.XmlTextWriter::specialAttr
	int32_t ___specialAttr_17;
	// System.String System.Xml.XmlTextWriter::prefixForXmlNs
	String_t* ___prefixForXmlNs_18;
	// System.Boolean System.Xml.XmlTextWriter::flush
	bool ___flush_19;
	// System.Xml.XmlTextWriter/Namespace[] System.Xml.XmlTextWriter::nsStack
	NamespaceU5BU5D_t409646736* ___nsStack_20;
	// System.Int32 System.Xml.XmlTextWriter::nsTop
	int32_t ___nsTop_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::nsHashtable
	Dictionary_2_t3986656710 * ___nsHashtable_22;
	// System.Boolean System.Xml.XmlTextWriter::useNsHashtable
	bool ___useNsHashtable_23;
	// System.Xml.XmlCharType System.Xml.XmlTextWriter::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_24;

public:
	inline static int32_t get_offset_of_textWriter_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___textWriter_1)); }
	inline TextWriter_t4027217640 * get_textWriter_1() const { return ___textWriter_1; }
	inline TextWriter_t4027217640 ** get_address_of_textWriter_1() { return &___textWriter_1; }
	inline void set_textWriter_1(TextWriter_t4027217640 * value)
	{
		___textWriter_1 = value;
		Il2CppCodeGenWriteBarrier(&___textWriter_1, value);
	}

	inline static int32_t get_offset_of_xmlEncoder_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___xmlEncoder_2)); }
	inline XmlTextEncoder_t2806209204 * get_xmlEncoder_2() const { return ___xmlEncoder_2; }
	inline XmlTextEncoder_t2806209204 ** get_address_of_xmlEncoder_2() { return &___xmlEncoder_2; }
	inline void set_xmlEncoder_2(XmlTextEncoder_t2806209204 * value)
	{
		___xmlEncoder_2 = value;
		Il2CppCodeGenWriteBarrier(&___xmlEncoder_2, value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___encoding_3)); }
	inline Encoding_t663144255 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t663144255 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t663144255 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_3, value);
	}

	inline static int32_t get_offset_of_formatting_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___formatting_4)); }
	inline int32_t get_formatting_4() const { return ___formatting_4; }
	inline int32_t* get_address_of_formatting_4() { return &___formatting_4; }
	inline void set_formatting_4(int32_t value)
	{
		___formatting_4 = value;
	}

	inline static int32_t get_offset_of_indented_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___indented_5)); }
	inline bool get_indented_5() const { return ___indented_5; }
	inline bool* get_address_of_indented_5() { return &___indented_5; }
	inline void set_indented_5(bool value)
	{
		___indented_5 = value;
	}

	inline static int32_t get_offset_of_indentation_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___indentation_6)); }
	inline int32_t get_indentation_6() const { return ___indentation_6; }
	inline int32_t* get_address_of_indentation_6() { return &___indentation_6; }
	inline void set_indentation_6(int32_t value)
	{
		___indentation_6 = value;
	}

	inline static int32_t get_offset_of_indentChar_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___indentChar_7)); }
	inline Il2CppChar get_indentChar_7() const { return ___indentChar_7; }
	inline Il2CppChar* get_address_of_indentChar_7() { return &___indentChar_7; }
	inline void set_indentChar_7(Il2CppChar value)
	{
		___indentChar_7 = value;
	}

	inline static int32_t get_offset_of_stack_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___stack_8)); }
	inline TagInfoU5BU5D_t2495379871* get_stack_8() const { return ___stack_8; }
	inline TagInfoU5BU5D_t2495379871** get_address_of_stack_8() { return &___stack_8; }
	inline void set_stack_8(TagInfoU5BU5D_t2495379871* value)
	{
		___stack_8 = value;
		Il2CppCodeGenWriteBarrier(&___stack_8, value);
	}

	inline static int32_t get_offset_of_top_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___top_9)); }
	inline int32_t get_top_9() const { return ___top_9; }
	inline int32_t* get_address_of_top_9() { return &___top_9; }
	inline void set_top_9(int32_t value)
	{
		___top_9 = value;
	}

	inline static int32_t get_offset_of_stateTable_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___stateTable_10)); }
	inline StateU5BU5D_t222040184* get_stateTable_10() const { return ___stateTable_10; }
	inline StateU5BU5D_t222040184** get_address_of_stateTable_10() { return &___stateTable_10; }
	inline void set_stateTable_10(StateU5BU5D_t222040184* value)
	{
		___stateTable_10 = value;
		Il2CppCodeGenWriteBarrier(&___stateTable_10, value);
	}

	inline static int32_t get_offset_of_currentState_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___currentState_11)); }
	inline int32_t get_currentState_11() const { return ___currentState_11; }
	inline int32_t* get_address_of_currentState_11() { return &___currentState_11; }
	inline void set_currentState_11(int32_t value)
	{
		___currentState_11 = value;
	}

	inline static int32_t get_offset_of_lastToken_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___lastToken_12)); }
	inline int32_t get_lastToken_12() const { return ___lastToken_12; }
	inline int32_t* get_address_of_lastToken_12() { return &___lastToken_12; }
	inline void set_lastToken_12(int32_t value)
	{
		___lastToken_12 = value;
	}

	inline static int32_t get_offset_of_base64Encoder_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___base64Encoder_13)); }
	inline XmlTextWriterBase64Encoder_t365185068 * get_base64Encoder_13() const { return ___base64Encoder_13; }
	inline XmlTextWriterBase64Encoder_t365185068 ** get_address_of_base64Encoder_13() { return &___base64Encoder_13; }
	inline void set_base64Encoder_13(XmlTextWriterBase64Encoder_t365185068 * value)
	{
		___base64Encoder_13 = value;
		Il2CppCodeGenWriteBarrier(&___base64Encoder_13, value);
	}

	inline static int32_t get_offset_of_quoteChar_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___quoteChar_14)); }
	inline Il2CppChar get_quoteChar_14() const { return ___quoteChar_14; }
	inline Il2CppChar* get_address_of_quoteChar_14() { return &___quoteChar_14; }
	inline void set_quoteChar_14(Il2CppChar value)
	{
		___quoteChar_14 = value;
	}

	inline static int32_t get_offset_of_curQuoteChar_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___curQuoteChar_15)); }
	inline Il2CppChar get_curQuoteChar_15() const { return ___curQuoteChar_15; }
	inline Il2CppChar* get_address_of_curQuoteChar_15() { return &___curQuoteChar_15; }
	inline void set_curQuoteChar_15(Il2CppChar value)
	{
		___curQuoteChar_15 = value;
	}

	inline static int32_t get_offset_of_namespaces_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___namespaces_16)); }
	inline bool get_namespaces_16() const { return ___namespaces_16; }
	inline bool* get_address_of_namespaces_16() { return &___namespaces_16; }
	inline void set_namespaces_16(bool value)
	{
		___namespaces_16 = value;
	}

	inline static int32_t get_offset_of_specialAttr_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___specialAttr_17)); }
	inline int32_t get_specialAttr_17() const { return ___specialAttr_17; }
	inline int32_t* get_address_of_specialAttr_17() { return &___specialAttr_17; }
	inline void set_specialAttr_17(int32_t value)
	{
		___specialAttr_17 = value;
	}

	inline static int32_t get_offset_of_prefixForXmlNs_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___prefixForXmlNs_18)); }
	inline String_t* get_prefixForXmlNs_18() const { return ___prefixForXmlNs_18; }
	inline String_t** get_address_of_prefixForXmlNs_18() { return &___prefixForXmlNs_18; }
	inline void set_prefixForXmlNs_18(String_t* value)
	{
		___prefixForXmlNs_18 = value;
		Il2CppCodeGenWriteBarrier(&___prefixForXmlNs_18, value);
	}

	inline static int32_t get_offset_of_flush_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___flush_19)); }
	inline bool get_flush_19() const { return ___flush_19; }
	inline bool* get_address_of_flush_19() { return &___flush_19; }
	inline void set_flush_19(bool value)
	{
		___flush_19 = value;
	}

	inline static int32_t get_offset_of_nsStack_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___nsStack_20)); }
	inline NamespaceU5BU5D_t409646736* get_nsStack_20() const { return ___nsStack_20; }
	inline NamespaceU5BU5D_t409646736** get_address_of_nsStack_20() { return &___nsStack_20; }
	inline void set_nsStack_20(NamespaceU5BU5D_t409646736* value)
	{
		___nsStack_20 = value;
		Il2CppCodeGenWriteBarrier(&___nsStack_20, value);
	}

	inline static int32_t get_offset_of_nsTop_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___nsTop_21)); }
	inline int32_t get_nsTop_21() const { return ___nsTop_21; }
	inline int32_t* get_address_of_nsTop_21() { return &___nsTop_21; }
	inline void set_nsTop_21(int32_t value)
	{
		___nsTop_21 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___nsHashtable_22)); }
	inline Dictionary_2_t3986656710 * get_nsHashtable_22() const { return ___nsHashtable_22; }
	inline Dictionary_2_t3986656710 ** get_address_of_nsHashtable_22() { return &___nsHashtable_22; }
	inline void set_nsHashtable_22(Dictionary_2_t3986656710 * value)
	{
		___nsHashtable_22 = value;
		Il2CppCodeGenWriteBarrier(&___nsHashtable_22, value);
	}

	inline static int32_t get_offset_of_useNsHashtable_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___useNsHashtable_23)); }
	inline bool get_useNsHashtable_23() const { return ___useNsHashtable_23; }
	inline bool* get_address_of_useNsHashtable_23() { return &___useNsHashtable_23; }
	inline void set_useNsHashtable_23(bool value)
	{
		___useNsHashtable_23 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655, ___xmlCharType_24)); }
	inline XmlCharType_t1050521405  get_xmlCharType_24() const { return ___xmlCharType_24; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_24() { return &___xmlCharType_24; }
	inline void set_xmlCharType_24(XmlCharType_t1050521405  value)
	{
		___xmlCharType_24 = value;
	}
};

struct XmlTextWriter_t2527250655_StaticFields
{
public:
	// System.String[] System.Xml.XmlTextWriter::stateName
	StringU5BU5D_t1642385972* ___stateName_25;
	// System.String[] System.Xml.XmlTextWriter::tokenName
	StringU5BU5D_t1642385972* ___tokenName_26;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDefault
	StateU5BU5D_t222040184* ___stateTableDefault_27;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDocument
	StateU5BU5D_t222040184* ___stateTableDocument_28;

public:
	inline static int32_t get_offset_of_stateName_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655_StaticFields, ___stateName_25)); }
	inline StringU5BU5D_t1642385972* get_stateName_25() const { return ___stateName_25; }
	inline StringU5BU5D_t1642385972** get_address_of_stateName_25() { return &___stateName_25; }
	inline void set_stateName_25(StringU5BU5D_t1642385972* value)
	{
		___stateName_25 = value;
		Il2CppCodeGenWriteBarrier(&___stateName_25, value);
	}

	inline static int32_t get_offset_of_tokenName_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655_StaticFields, ___tokenName_26)); }
	inline StringU5BU5D_t1642385972* get_tokenName_26() const { return ___tokenName_26; }
	inline StringU5BU5D_t1642385972** get_address_of_tokenName_26() { return &___tokenName_26; }
	inline void set_tokenName_26(StringU5BU5D_t1642385972* value)
	{
		___tokenName_26 = value;
		Il2CppCodeGenWriteBarrier(&___tokenName_26, value);
	}

	inline static int32_t get_offset_of_stateTableDefault_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655_StaticFields, ___stateTableDefault_27)); }
	inline StateU5BU5D_t222040184* get_stateTableDefault_27() const { return ___stateTableDefault_27; }
	inline StateU5BU5D_t222040184** get_address_of_stateTableDefault_27() { return &___stateTableDefault_27; }
	inline void set_stateTableDefault_27(StateU5BU5D_t222040184* value)
	{
		___stateTableDefault_27 = value;
		Il2CppCodeGenWriteBarrier(&___stateTableDefault_27, value);
	}

	inline static int32_t get_offset_of_stateTableDocument_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_t2527250655_StaticFields, ___stateTableDocument_28)); }
	inline StateU5BU5D_t222040184* get_stateTableDocument_28() const { return ___stateTableDocument_28; }
	inline StateU5BU5D_t222040184** get_address_of_stateTableDocument_28() { return &___stateTableDocument_28; }
	inline void set_stateTableDocument_28(StateU5BU5D_t222040184* value)
	{
		___stateTableDocument_28 = value;
		Il2CppCodeGenWriteBarrier(&___stateTableDocument_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
