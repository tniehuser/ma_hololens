﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_t4229094479;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.StringComparer
struct StringComparer_t1574862926;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2034248631  : public Il2CppObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t4252133567 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	Il2CppObject * ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t909839986 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_t4229094479 * ____nullKeyEntry_4;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::_keys
	KeysCollection_t633582367 * ____keys_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t228987430 * ____serializationInfo_6;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_7;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	Il2CppObject * ____syncRoot_8;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesArray_1)); }
	inline ArrayList_t4252133567 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t4252133567 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t4252133567 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier(&____entriesArray_1, value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keyComparer_2)); }
	inline Il2CppObject * get__keyComparer_2() const { return ____keyComparer_2; }
	inline Il2CppObject ** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(Il2CppObject * value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier(&____keyComparer_2, value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesTable_3)); }
	inline Hashtable_t909839986 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t909839986 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t909839986 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier(&____entriesTable_3, value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____nullKeyEntry_4)); }
	inline NameObjectEntry_t4229094479 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_t4229094479 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_t4229094479 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier(&____nullKeyEntry_4, value);
	}

	inline static int32_t get_offset_of__keys_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keys_5)); }
	inline KeysCollection_t633582367 * get__keys_5() const { return ____keys_5; }
	inline KeysCollection_t633582367 ** get_address_of__keys_5() { return &____keys_5; }
	inline void set__keys_5(KeysCollection_t633582367 * value)
	{
		____keys_5 = value;
		Il2CppCodeGenWriteBarrier(&____keys_5, value);
	}

	inline static int32_t get_offset_of__serializationInfo_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____serializationInfo_6)); }
	inline SerializationInfo_t228987430 * get__serializationInfo_6() const { return ____serializationInfo_6; }
	inline SerializationInfo_t228987430 ** get_address_of__serializationInfo_6() { return &____serializationInfo_6; }
	inline void set__serializationInfo_6(SerializationInfo_t228987430 * value)
	{
		____serializationInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&____serializationInfo_6, value);
	}

	inline static int32_t get_offset_of__version_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____version_7)); }
	inline int32_t get__version_7() const { return ____version_7; }
	inline int32_t* get_address_of__version_7() { return &____version_7; }
	inline void set__version_7(int32_t value)
	{
		____version_7 = value;
	}

	inline static int32_t get_offset_of__syncRoot_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____syncRoot_8)); }
	inline Il2CppObject * get__syncRoot_8() const { return ____syncRoot_8; }
	inline Il2CppObject ** get_address_of__syncRoot_8() { return &____syncRoot_8; }
	inline void set__syncRoot_8(Il2CppObject * value)
	{
		____syncRoot_8 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_8, value);
	}
};

struct NameObjectCollectionBase_t2034248631_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t1574862926 * ___defaultComparer_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Collections.Specialized.NameObjectCollectionBase::<>f__switch$map6
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map6_10;

public:
	inline static int32_t get_offset_of_defaultComparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631_StaticFields, ___defaultComparer_9)); }
	inline StringComparer_t1574862926 * get_defaultComparer_9() const { return ___defaultComparer_9; }
	inline StringComparer_t1574862926 ** get_address_of_defaultComparer_9() { return &___defaultComparer_9; }
	inline void set_defaultComparer_9(StringComparer_t1574862926 * value)
	{
		___defaultComparer_9 = value;
		Il2CppCodeGenWriteBarrier(&___defaultComparer_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_10() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631_StaticFields, ___U3CU3Ef__switchU24map6_10)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map6_10() const { return ___U3CU3Ef__switchU24map6_10; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map6_10() { return &___U3CU3Ef__switchU24map6_10; }
	inline void set_U3CU3Ef__switchU24map6_10(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
