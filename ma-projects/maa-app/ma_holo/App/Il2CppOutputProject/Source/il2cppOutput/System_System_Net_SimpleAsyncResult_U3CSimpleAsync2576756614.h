﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.SimpleAsyncResult
struct SimpleAsyncResult_t2937691397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult/<SimpleAsyncResult>c__AnonStorey0
struct  U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614  : public Il2CppObject
{
public:
	// System.AsyncCallback System.Net.SimpleAsyncResult/<SimpleAsyncResult>c__AnonStorey0::cb
	AsyncCallback_t163412349 * ___cb_0;
	// System.Net.SimpleAsyncResult System.Net.SimpleAsyncResult/<SimpleAsyncResult>c__AnonStorey0::$this
	SimpleAsyncResult_t2937691397 * ___U24this_1;

public:
	inline static int32_t get_offset_of_cb_0() { return static_cast<int32_t>(offsetof(U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614, ___cb_0)); }
	inline AsyncCallback_t163412349 * get_cb_0() const { return ___cb_0; }
	inline AsyncCallback_t163412349 ** get_address_of_cb_0() { return &___cb_0; }
	inline void set_cb_0(AsyncCallback_t163412349 * value)
	{
		___cb_0 = value;
		Il2CppCodeGenWriteBarrier(&___cb_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSimpleAsyncResultU3Ec__AnonStorey0_t2576756614, ___U24this_1)); }
	inline SimpleAsyncResult_t2937691397 * get_U24this_1() const { return ___U24this_1; }
	inline SimpleAsyncResult_t2937691397 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SimpleAsyncResult_t2937691397 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
