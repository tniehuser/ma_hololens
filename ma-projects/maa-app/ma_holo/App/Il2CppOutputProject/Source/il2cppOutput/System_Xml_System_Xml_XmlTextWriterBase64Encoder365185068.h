﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Base64Encoder1608613663.h"

// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t2806209204;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t365185068  : public Base64Encoder_t1608613663
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_t2806209204 * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t365185068, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_t2806209204 * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_t2806209204 ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_t2806209204 * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier(&___xmlTextEncoder_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
