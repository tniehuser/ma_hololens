﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_t3811767860;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t4189650927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupRef
struct  XmlSchemaGroupRef_t3082205844  : public XmlSchemaParticle_t3365045970
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroupRef::refName
	XmlQualifiedName_t1944712516 * ___refName_13;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroupRef::particle
	XmlSchemaGroupBase_t3811767860 * ___particle_14;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroupRef::refined
	XmlSchemaGroup_t4189650927 * ___refined_15;

public:
	inline static int32_t get_offset_of_refName_13() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___refName_13)); }
	inline XmlQualifiedName_t1944712516 * get_refName_13() const { return ___refName_13; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_13() { return &___refName_13; }
	inline void set_refName_13(XmlQualifiedName_t1944712516 * value)
	{
		___refName_13 = value;
		Il2CppCodeGenWriteBarrier(&___refName_13, value);
	}

	inline static int32_t get_offset_of_particle_14() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___particle_14)); }
	inline XmlSchemaGroupBase_t3811767860 * get_particle_14() const { return ___particle_14; }
	inline XmlSchemaGroupBase_t3811767860 ** get_address_of_particle_14() { return &___particle_14; }
	inline void set_particle_14(XmlSchemaGroupBase_t3811767860 * value)
	{
		___particle_14 = value;
		Il2CppCodeGenWriteBarrier(&___particle_14, value);
	}

	inline static int32_t get_offset_of_refined_15() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___refined_15)); }
	inline XmlSchemaGroup_t4189650927 * get_refined_15() const { return ___refined_15; }
	inline XmlSchemaGroup_t4189650927 ** get_address_of_refined_15() { return &___refined_15; }
	inline void set_refined_15(XmlSchemaGroup_t4189650927 * value)
	{
		___refined_15 = value;
		Il2CppCodeGenWriteBarrier(&___refined_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
