﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Resources.ResourceManager
struct ResourceManager_t264715885;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Resources.ResourceManager/ResourceManagerMediator
struct  ResourceManagerMediator_t1155374454  : public Il2CppObject
{
public:
	// System.Resources.ResourceManager System.Resources.ResourceManager/ResourceManagerMediator::_rm
	ResourceManager_t264715885 * ____rm_0;

public:
	inline static int32_t get_offset_of__rm_0() { return static_cast<int32_t>(offsetof(ResourceManagerMediator_t1155374454, ____rm_0)); }
	inline ResourceManager_t264715885 * get__rm_0() const { return ____rm_0; }
	inline ResourceManager_t264715885 ** get_address_of__rm_0() { return &____rm_0; }
	inline void set__rm_0(ResourceManager_t264715885 * value)
	{
		____rm_0 = value;
		Il2CppCodeGenWriteBarrier(&____rm_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
