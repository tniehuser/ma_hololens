﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Globalization.CultureInfo>>
struct Action_1_t1793350945;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct  AsyncLocal_1_t842524053  : public Il2CppObject
{
public:
	// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<T>> System.Threading.AsyncLocal`1::m_valueChangedHandler
	Action_1_t1793350945 * ___m_valueChangedHandler_0;

public:
	inline static int32_t get_offset_of_m_valueChangedHandler_0() { return static_cast<int32_t>(offsetof(AsyncLocal_1_t842524053, ___m_valueChangedHandler_0)); }
	inline Action_1_t1793350945 * get_m_valueChangedHandler_0() const { return ___m_valueChangedHandler_0; }
	inline Action_1_t1793350945 ** get_address_of_m_valueChangedHandler_0() { return &___m_valueChangedHandler_0; }
	inline void set_m_valueChangedHandler_0(Action_1_t1793350945 * value)
	{
		___m_valueChangedHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_valueChangedHandler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
