﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Net.WebConnectionGroup/ConnectionState>
struct LinkedList_1_t2913323272;
// System.Collections.Queue
struct Queue_t1288490777;
// System.EventHandler
struct EventHandler_t277755526;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup
struct  WebConnectionGroup_t3242458773  : public Il2CppObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnectionGroup::sPoint
	ServicePoint_t2765344313 * ___sPoint_0;
	// System.String System.Net.WebConnectionGroup::name
	String_t* ___name_1;
	// System.Collections.Generic.LinkedList`1<System.Net.WebConnectionGroup/ConnectionState> System.Net.WebConnectionGroup::connections
	LinkedList_1_t2913323272 * ___connections_2;
	// System.Collections.Queue System.Net.WebConnectionGroup::queue
	Queue_t1288490777 * ___queue_3;
	// System.Boolean System.Net.WebConnectionGroup::closing
	bool ___closing_4;
	// System.EventHandler System.Net.WebConnectionGroup::ConnectionClosed
	EventHandler_t277755526 * ___ConnectionClosed_5;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___sPoint_0)); }
	inline ServicePoint_t2765344313 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_t2765344313 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_t2765344313 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier(&___sPoint_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___connections_2)); }
	inline LinkedList_1_t2913323272 * get_connections_2() const { return ___connections_2; }
	inline LinkedList_1_t2913323272 ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(LinkedList_1_t2913323272 * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier(&___connections_2, value);
	}

	inline static int32_t get_offset_of_queue_3() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___queue_3)); }
	inline Queue_t1288490777 * get_queue_3() const { return ___queue_3; }
	inline Queue_t1288490777 ** get_address_of_queue_3() { return &___queue_3; }
	inline void set_queue_3(Queue_t1288490777 * value)
	{
		___queue_3 = value;
		Il2CppCodeGenWriteBarrier(&___queue_3, value);
	}

	inline static int32_t get_offset_of_closing_4() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___closing_4)); }
	inline bool get_closing_4() const { return ___closing_4; }
	inline bool* get_address_of_closing_4() { return &___closing_4; }
	inline void set_closing_4(bool value)
	{
		___closing_4 = value;
	}

	inline static int32_t get_offset_of_ConnectionClosed_5() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t3242458773, ___ConnectionClosed_5)); }
	inline EventHandler_t277755526 * get_ConnectionClosed_5() const { return ___ConnectionClosed_5; }
	inline EventHandler_t277755526 ** get_address_of_ConnectionClosed_5() { return &___ConnectionClosed_5; }
	inline void set_ConnectionClosed_5(EventHandler_t277755526 * value)
	{
		___ConnectionClosed_5 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionClosed_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
