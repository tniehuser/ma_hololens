﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_int2281243990.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_short
struct  Datatype_short_t3426858539  : public Datatype_int_t2281243990
{
public:

public:
};

struct Datatype_short_t3426858539_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_short::atomicValueType
	Type_t * ___atomicValueType_102;
	// System.Type System.Xml.Schema.Datatype_short::listValueType
	Type_t * ___listValueType_103;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_short::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_104;

public:
	inline static int32_t get_offset_of_atomicValueType_102() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___atomicValueType_102)); }
	inline Type_t * get_atomicValueType_102() const { return ___atomicValueType_102; }
	inline Type_t ** get_address_of_atomicValueType_102() { return &___atomicValueType_102; }
	inline void set_atomicValueType_102(Type_t * value)
	{
		___atomicValueType_102 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_102, value);
	}

	inline static int32_t get_offset_of_listValueType_103() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___listValueType_103)); }
	inline Type_t * get_listValueType_103() const { return ___listValueType_103; }
	inline Type_t ** get_address_of_listValueType_103() { return &___listValueType_103; }
	inline void set_listValueType_103(Type_t * value)
	{
		___listValueType_103 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_103, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_104() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___numeric10FacetsChecker_104)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_104() const { return ___numeric10FacetsChecker_104; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_104() { return &___numeric10FacetsChecker_104; }
	inline void set_numeric10FacetsChecker_104(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_104 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_104, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
