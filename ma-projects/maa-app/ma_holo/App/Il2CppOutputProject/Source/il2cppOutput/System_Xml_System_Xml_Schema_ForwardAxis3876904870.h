﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.DoubleLinkAxis
struct DoubleLinkAxis_t3164907012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ForwardAxis
struct  ForwardAxis_t3876904870  : public Il2CppObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::topNode
	DoubleLinkAxis_t3164907012 * ___topNode_0;
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::rootNode
	DoubleLinkAxis_t3164907012 * ___rootNode_1;
	// System.Boolean System.Xml.Schema.ForwardAxis::isAttribute
	bool ___isAttribute_2;
	// System.Boolean System.Xml.Schema.ForwardAxis::isDss
	bool ___isDss_3;
	// System.Boolean System.Xml.Schema.ForwardAxis::isSelfAxis
	bool ___isSelfAxis_4;

public:
	inline static int32_t get_offset_of_topNode_0() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___topNode_0)); }
	inline DoubleLinkAxis_t3164907012 * get_topNode_0() const { return ___topNode_0; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_topNode_0() { return &___topNode_0; }
	inline void set_topNode_0(DoubleLinkAxis_t3164907012 * value)
	{
		___topNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___topNode_0, value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___rootNode_1)); }
	inline DoubleLinkAxis_t3164907012 * get_rootNode_1() const { return ___rootNode_1; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(DoubleLinkAxis_t3164907012 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___rootNode_1, value);
	}

	inline static int32_t get_offset_of_isAttribute_2() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isAttribute_2)); }
	inline bool get_isAttribute_2() const { return ___isAttribute_2; }
	inline bool* get_address_of_isAttribute_2() { return &___isAttribute_2; }
	inline void set_isAttribute_2(bool value)
	{
		___isAttribute_2 = value;
	}

	inline static int32_t get_offset_of_isDss_3() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isDss_3)); }
	inline bool get_isDss_3() const { return ___isDss_3; }
	inline bool* get_address_of_isDss_3() { return &___isDss_3; }
	inline void set_isDss_3(bool value)
	{
		___isDss_3 = value;
	}

	inline static int32_t get_offset_of_isSelfAxis_4() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isSelfAxis_4)); }
	inline bool get_isSelfAxis_4() const { return ___isSelfAxis_4; }
	inline bool* get_address_of_isSelfAxis_4() { return &___isSelfAxis_4; }
	inline void set_isSelfAxis_4(bool value)
	{
		___isSelfAxis_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
