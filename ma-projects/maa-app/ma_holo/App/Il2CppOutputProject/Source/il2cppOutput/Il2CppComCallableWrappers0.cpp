﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "vm/CachedCCWBase.h"
#include "mscorlib_System_Runtime_InteropServices_ManagedErro914761495.h"
#include "mscorlib_System_Runtime_InteropServices_IErrorInfo3966376335.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_UInt322149682021.h"

// System.Runtime.InteropServices.ManagedErrorInfo
struct ManagedErrorInfo_t914761495;
// System.String
struct String_t;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t IErrorInfo_GetGUID_m3816441410_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetGUID_m3684105608_MetadataUsageId;
extern const uint32_t IErrorInfo_GetSource_m2851584934_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetSource_m2771582700_MetadataUsageId;
extern const uint32_t IErrorInfo_GetDescription_m689279495_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetDescription_m3399255087_MetadataUsageId;
extern const uint32_t IErrorInfo_GetHelpFile_m2074525126_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetHelpFile_m1071094872_MetadataUsageId;
extern const uint32_t IErrorInfo_GetHelpContext_m3383284997_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetHelpContext_m3080255405_MetadataUsageId;




// System.Int32 System.Runtime.InteropServices.ManagedErrorInfo::GetGUID(System.Guid&)
extern "C"  int32_t ManagedErrorInfo_GetGUID_m3684105608 (ManagedErrorInfo_t914761495 * __this, Guid_t * ___guid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ManagedErrorInfo::GetSource(System.String&)
extern "C"  int32_t ManagedErrorInfo_GetSource_m2771582700 (ManagedErrorInfo_t914761495 * __this, String_t** ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ManagedErrorInfo::GetDescription(System.String&)
extern "C"  int32_t ManagedErrorInfo_GetDescription_m3399255087 (ManagedErrorInfo_t914761495 * __this, String_t** ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ManagedErrorInfo::GetHelpFile(System.String&)
extern "C"  int32_t ManagedErrorInfo_GetHelpFile_m1071094872 (ManagedErrorInfo_t914761495 * __this, String_t** ___helpFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ManagedErrorInfo::GetHelpContext(System.UInt32&)
extern "C"  int32_t ManagedErrorInfo_GetHelpContext_m3080255405 (ManagedErrorInfo_t914761495 * __this, uint32_t* ___helpContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// COM Callable Wrapper for System.Runtime.InteropServices.ManagedErrorInfo
struct ManagedErrorInfo_t914761495_ComCallableWrapper IL2CPP_FINAL : il2cpp::vm::CachedCCWBase<ManagedErrorInfo_t914761495_ComCallableWrapper>, IErrorInfo_t3966376335
{
	inline ManagedErrorInfo_t914761495_ComCallableWrapper(Il2CppObject* obj) : il2cpp::vm::CachedCCWBase<ManagedErrorInfo_t914761495_ComCallableWrapper>(obj) {}

	virtual il2cpp_hresult_t STDCALL QueryInterface(const Il2CppGuid& iid, void** object) IL2CPP_OVERRIDE
	{
		if (::memcmp(&iid, &Il2CppIUnknown::IID, sizeof(Il2CppGuid)) == 0
		 || ::memcmp(&iid, &Il2CppIInspectable::IID, sizeof(Il2CppGuid)) == 0
		 || ::memcmp(&iid, &Il2CppIAgileObject::IID, sizeof(Il2CppGuid)) == 0)
		{
			*object = GetIdentity();
			AddRefImpl();
			return IL2CPP_S_OK;
		}

		if (::memcmp(&iid, &Il2CppIManagedObjectHolder::IID, sizeof(Il2CppGuid)) == 0)
		{
			*object = static_cast<Il2CppIManagedObjectHolder*>(this);
			AddRefImpl();
			return IL2CPP_S_OK;
		}

		if (::memcmp(&iid, &IErrorInfo_t3966376335::IID, sizeof(Il2CppGuid)) == 0)
		{
			*object = static_cast<IErrorInfo_t3966376335*>(this);
			AddRefImpl();
			return IL2CPP_S_OK;
		}

		if (::memcmp(&iid, &Il2CppIMarshal::IID, sizeof(Il2CppGuid)) == 0)
		{
			*object = static_cast<Il2CppIMarshal*>(this);
			AddRefImpl();
			return IL2CPP_S_OK;
		}

		*object = NULL;
		return IL2CPP_E_NOINTERFACE;
	}

	virtual uint32_t STDCALL AddRef() IL2CPP_OVERRIDE
	{
		return AddRefImpl();
	}

	virtual uint32_t STDCALL Release() IL2CPP_OVERRIDE
	{
		return ReleaseImpl();
	}

	virtual il2cpp_hresult_t STDCALL GetIids(uint32_t* iidCount, Il2CppGuid** iids) IL2CPP_OVERRIDE
	{
		*iidCount = 0;
		*iids = NULL;
		return IL2CPP_S_OK;
	}

	virtual il2cpp_hresult_t STDCALL IErrorInfo_GetGUID_m3816441410(Guid_t * ___guid0, int32_t* comReturnValue) IL2CPP_OVERRIDE
	{
		static bool s_Il2CppMethodInitialized;
		if (!s_Il2CppMethodInitialized)
		{
			il2cpp_codegen_initialize_method (IErrorInfo_GetGUID_m3816441410_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetGUID_m3684105608_MetadataUsageId);
			s_Il2CppMethodInitialized = true;
		}
		il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

		// Marshaling of parameter '___guid0' to managed representation
		Guid_t  ____guid0_empty;
		memset(&____guid0_empty, 0, sizeof(____guid0_empty));

		// Managed method invocation
		int32_t returnValue;
		try
		{
			ManagedErrorInfo_t914761495 * __thisValue = (ManagedErrorInfo_t914761495 *)GetManagedObjectInline();
			returnValue = ManagedErrorInfo_GetGUID_m3684105608(__thisValue, &____guid0_empty, NULL);
		}
		catch (const Il2CppExceptionWrapper& ex)
		{
			*comReturnValue = 0;
			String_t* exceptionStr = NULL;
			try
			{
				exceptionStr = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ex.ex);
			}
			catch (const Il2CppExceptionWrapper&)
			{
				exceptionStr = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
			}
			il2cpp_codegen_store_exception_info(ex.ex, exceptionStr);
			return ex.ex->hresult;
		}

		// Marshaling of parameter '___guid0' back from managed representation
		*___guid0 = ____guid0_empty;

		*comReturnValue = returnValue;
		return IL2CPP_S_OK;
	}

	virtual il2cpp_hresult_t STDCALL IErrorInfo_GetSource_m2851584934(Il2CppChar** ___source0, int32_t* comReturnValue) IL2CPP_OVERRIDE
	{
		static bool s_Il2CppMethodInitialized;
		if (!s_Il2CppMethodInitialized)
		{
			il2cpp_codegen_initialize_method (IErrorInfo_GetSource_m2851584934_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetSource_m2771582700_MetadataUsageId);
			s_Il2CppMethodInitialized = true;
		}
		il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

		// Marshaling of parameter '___source0' to managed representation
		String_t* ____source0_empty = NULL;

		// Managed method invocation
		int32_t returnValue;
		try
		{
			ManagedErrorInfo_t914761495 * __thisValue = (ManagedErrorInfo_t914761495 *)GetManagedObjectInline();
			returnValue = ManagedErrorInfo_GetSource_m2771582700(__thisValue, &____source0_empty, NULL);
		}
		catch (const Il2CppExceptionWrapper& ex)
		{
			*comReturnValue = 0;
			String_t* exceptionStr = NULL;
			try
			{
				exceptionStr = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ex.ex);
			}
			catch (const Il2CppExceptionWrapper&)
			{
				exceptionStr = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
			}
			il2cpp_codegen_store_exception_info(ex.ex, exceptionStr);
			return ex.ex->hresult;
		}

		// Marshaling of parameter '___source0' back from managed representation
		*___source0 = il2cpp_codegen_marshal_bstring(____source0_empty);

		*comReturnValue = returnValue;
		return IL2CPP_S_OK;
	}

	virtual il2cpp_hresult_t STDCALL IErrorInfo_GetDescription_m689279495(Il2CppChar** ___description0, int32_t* comReturnValue) IL2CPP_OVERRIDE
	{
		static bool s_Il2CppMethodInitialized;
		if (!s_Il2CppMethodInitialized)
		{
			il2cpp_codegen_initialize_method (IErrorInfo_GetDescription_m689279495_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetDescription_m3399255087_MetadataUsageId);
			s_Il2CppMethodInitialized = true;
		}
		il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

		// Marshaling of parameter '___description0' to managed representation
		String_t* ____description0_empty = NULL;

		// Managed method invocation
		int32_t returnValue;
		try
		{
			ManagedErrorInfo_t914761495 * __thisValue = (ManagedErrorInfo_t914761495 *)GetManagedObjectInline();
			returnValue = ManagedErrorInfo_GetDescription_m3399255087(__thisValue, &____description0_empty, NULL);
		}
		catch (const Il2CppExceptionWrapper& ex)
		{
			*comReturnValue = 0;
			String_t* exceptionStr = NULL;
			try
			{
				exceptionStr = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ex.ex);
			}
			catch (const Il2CppExceptionWrapper&)
			{
				exceptionStr = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
			}
			il2cpp_codegen_store_exception_info(ex.ex, exceptionStr);
			return ex.ex->hresult;
		}

		// Marshaling of parameter '___description0' back from managed representation
		*___description0 = il2cpp_codegen_marshal_bstring(____description0_empty);

		*comReturnValue = returnValue;
		return IL2CPP_S_OK;
	}

	virtual il2cpp_hresult_t STDCALL IErrorInfo_GetHelpFile_m2074525126(Il2CppChar** ___helpFile0, int32_t* comReturnValue) IL2CPP_OVERRIDE
	{
		static bool s_Il2CppMethodInitialized;
		if (!s_Il2CppMethodInitialized)
		{
			il2cpp_codegen_initialize_method (IErrorInfo_GetHelpFile_m2074525126_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetHelpFile_m1071094872_MetadataUsageId);
			s_Il2CppMethodInitialized = true;
		}
		il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

		// Marshaling of parameter '___helpFile0' to managed representation
		String_t* ____helpFile0_empty = NULL;

		// Managed method invocation
		int32_t returnValue;
		try
		{
			ManagedErrorInfo_t914761495 * __thisValue = (ManagedErrorInfo_t914761495 *)GetManagedObjectInline();
			returnValue = ManagedErrorInfo_GetHelpFile_m1071094872(__thisValue, &____helpFile0_empty, NULL);
		}
		catch (const Il2CppExceptionWrapper& ex)
		{
			*comReturnValue = 0;
			String_t* exceptionStr = NULL;
			try
			{
				exceptionStr = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ex.ex);
			}
			catch (const Il2CppExceptionWrapper&)
			{
				exceptionStr = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
			}
			il2cpp_codegen_store_exception_info(ex.ex, exceptionStr);
			return ex.ex->hresult;
		}

		// Marshaling of parameter '___helpFile0' back from managed representation
		*___helpFile0 = il2cpp_codegen_marshal_bstring(____helpFile0_empty);

		*comReturnValue = returnValue;
		return IL2CPP_S_OK;
	}

	virtual il2cpp_hresult_t STDCALL IErrorInfo_GetHelpContext_m3383284997(uint32_t* ___helpContext0, int32_t* comReturnValue) IL2CPP_OVERRIDE
	{
		static bool s_Il2CppMethodInitialized;
		if (!s_Il2CppMethodInitialized)
		{
			il2cpp_codegen_initialize_method (IErrorInfo_GetHelpContext_m3383284997_CCW_ManagedErrorInfo_t914761495_ComCallableWrapper_ManagedErrorInfo_GetHelpContext_m3080255405_MetadataUsageId);
			s_Il2CppMethodInitialized = true;
		}
		il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

		// Marshaling of parameter '___helpContext0' to managed representation
		uint32_t ____helpContext0_empty = 0;

		// Managed method invocation
		int32_t returnValue;
		try
		{
			ManagedErrorInfo_t914761495 * __thisValue = (ManagedErrorInfo_t914761495 *)GetManagedObjectInline();
			returnValue = ManagedErrorInfo_GetHelpContext_m3080255405(__thisValue, &____helpContext0_empty, NULL);
		}
		catch (const Il2CppExceptionWrapper& ex)
		{
			*comReturnValue = 0;
			String_t* exceptionStr = NULL;
			try
			{
				exceptionStr = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ex.ex);
			}
			catch (const Il2CppExceptionWrapper&)
			{
				exceptionStr = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_5();
			}
			il2cpp_codegen_store_exception_info(ex.ex, exceptionStr);
			return ex.ex->hresult;
		}

		// Marshaling of parameter '___helpContext0' back from managed representation
		*___helpContext0 = ____helpContext0_empty;

		*comReturnValue = returnValue;
		return IL2CPP_S_OK;
	}
};

extern "C" Il2CppIUnknown* CreateComCallableWrapperFor_ManagedErrorInfo_t914761495(Il2CppObject* obj)
{
	void* memory = il2cpp::utils::Memory::Malloc(sizeof(ManagedErrorInfo_t914761495_ComCallableWrapper));
	if (memory == NULL)
	{
		il2cpp_codegen_raise_out_of_memory_exception();
	}

	return static_cast<Il2CppIManagedObjectHolder*>(new(memory) ManagedErrorInfo_t914761495_ComCallableWrapper(obj));
}
