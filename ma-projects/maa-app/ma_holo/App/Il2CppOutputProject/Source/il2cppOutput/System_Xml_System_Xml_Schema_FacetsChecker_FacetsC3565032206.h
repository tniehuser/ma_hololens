﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "System_Xml_System_Xml_Schema_RestrictionFlags2588355947.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode58293802.h"

// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_t1152094268;
// System.Xml.Schema.RestrictionFacets
struct RestrictionFacets_t4012658256;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t2024909611;
// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[]
struct MapU5BU5D_t3908492414;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler
struct  FacetsCompiler_t3565032206 
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.FacetsChecker/FacetsCompiler::datatype
	DatatypeImplementation_t1152094268 * ___datatype_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.FacetsChecker/FacetsCompiler::derivedRestriction
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFlags
	int32_t ___baseFlags_2;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFixedFlags
	int32_t ___baseFixedFlags_3;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::validRestrictionFlags
	int32_t ___validRestrictionFlags_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::nonNegativeInt
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInType
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	// System.Xml.Schema.XmlTypeCode System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInEnum
	int32_t ___builtInEnum_7;
	// System.Boolean System.Xml.Schema.FacetsChecker/FacetsCompiler::firstPattern
	bool ___firstPattern_8;
	// System.Text.StringBuilder System.Xml.Schema.FacetsChecker/FacetsCompiler::regStr
	StringBuilder_t1221177846 * ___regStr_9;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Schema.FacetsChecker/FacetsCompiler::pattern_facet
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;

public:
	inline static int32_t get_offset_of_datatype_0() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___datatype_0)); }
	inline DatatypeImplementation_t1152094268 * get_datatype_0() const { return ___datatype_0; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_datatype_0() { return &___datatype_0; }
	inline void set_datatype_0(DatatypeImplementation_t1152094268 * value)
	{
		___datatype_0 = value;
		Il2CppCodeGenWriteBarrier(&___datatype_0, value);
	}

	inline static int32_t get_offset_of_derivedRestriction_1() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___derivedRestriction_1)); }
	inline RestrictionFacets_t4012658256 * get_derivedRestriction_1() const { return ___derivedRestriction_1; }
	inline RestrictionFacets_t4012658256 ** get_address_of_derivedRestriction_1() { return &___derivedRestriction_1; }
	inline void set_derivedRestriction_1(RestrictionFacets_t4012658256 * value)
	{
		___derivedRestriction_1 = value;
		Il2CppCodeGenWriteBarrier(&___derivedRestriction_1, value);
	}

	inline static int32_t get_offset_of_baseFlags_2() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___baseFlags_2)); }
	inline int32_t get_baseFlags_2() const { return ___baseFlags_2; }
	inline int32_t* get_address_of_baseFlags_2() { return &___baseFlags_2; }
	inline void set_baseFlags_2(int32_t value)
	{
		___baseFlags_2 = value;
	}

	inline static int32_t get_offset_of_baseFixedFlags_3() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___baseFixedFlags_3)); }
	inline int32_t get_baseFixedFlags_3() const { return ___baseFixedFlags_3; }
	inline int32_t* get_address_of_baseFixedFlags_3() { return &___baseFixedFlags_3; }
	inline void set_baseFixedFlags_3(int32_t value)
	{
		___baseFixedFlags_3 = value;
	}

	inline static int32_t get_offset_of_validRestrictionFlags_4() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___validRestrictionFlags_4)); }
	inline int32_t get_validRestrictionFlags_4() const { return ___validRestrictionFlags_4; }
	inline int32_t* get_address_of_validRestrictionFlags_4() { return &___validRestrictionFlags_4; }
	inline void set_validRestrictionFlags_4(int32_t value)
	{
		___validRestrictionFlags_4 = value;
	}

	inline static int32_t get_offset_of_nonNegativeInt_5() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___nonNegativeInt_5)); }
	inline XmlSchemaDatatype_t1195946242 * get_nonNegativeInt_5() const { return ___nonNegativeInt_5; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_nonNegativeInt_5() { return &___nonNegativeInt_5; }
	inline void set_nonNegativeInt_5(XmlSchemaDatatype_t1195946242 * value)
	{
		___nonNegativeInt_5 = value;
		Il2CppCodeGenWriteBarrier(&___nonNegativeInt_5, value);
	}

	inline static int32_t get_offset_of_builtInType_6() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___builtInType_6)); }
	inline XmlSchemaDatatype_t1195946242 * get_builtInType_6() const { return ___builtInType_6; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_builtInType_6() { return &___builtInType_6; }
	inline void set_builtInType_6(XmlSchemaDatatype_t1195946242 * value)
	{
		___builtInType_6 = value;
		Il2CppCodeGenWriteBarrier(&___builtInType_6, value);
	}

	inline static int32_t get_offset_of_builtInEnum_7() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___builtInEnum_7)); }
	inline int32_t get_builtInEnum_7() const { return ___builtInEnum_7; }
	inline int32_t* get_address_of_builtInEnum_7() { return &___builtInEnum_7; }
	inline void set_builtInEnum_7(int32_t value)
	{
		___builtInEnum_7 = value;
	}

	inline static int32_t get_offset_of_firstPattern_8() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___firstPattern_8)); }
	inline bool get_firstPattern_8() const { return ___firstPattern_8; }
	inline bool* get_address_of_firstPattern_8() { return &___firstPattern_8; }
	inline void set_firstPattern_8(bool value)
	{
		___firstPattern_8 = value;
	}

	inline static int32_t get_offset_of_regStr_9() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___regStr_9)); }
	inline StringBuilder_t1221177846 * get_regStr_9() const { return ___regStr_9; }
	inline StringBuilder_t1221177846 ** get_address_of_regStr_9() { return &___regStr_9; }
	inline void set_regStr_9(StringBuilder_t1221177846 * value)
	{
		___regStr_9 = value;
		Il2CppCodeGenWriteBarrier(&___regStr_9, value);
	}

	inline static int32_t get_offset_of_pattern_facet_10() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___pattern_facet_10)); }
	inline XmlSchemaPatternFacet_t2024909611 * get_pattern_facet_10() const { return ___pattern_facet_10; }
	inline XmlSchemaPatternFacet_t2024909611 ** get_address_of_pattern_facet_10() { return &___pattern_facet_10; }
	inline void set_pattern_facet_10(XmlSchemaPatternFacet_t2024909611 * value)
	{
		___pattern_facet_10 = value;
		Il2CppCodeGenWriteBarrier(&___pattern_facet_10, value);
	}
};

struct FacetsCompiler_t3565032206_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[] System.Xml.Schema.FacetsChecker/FacetsCompiler::c_map
	MapU5BU5D_t3908492414* ___c_map_11;

public:
	inline static int32_t get_offset_of_c_map_11() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206_StaticFields, ___c_map_11)); }
	inline MapU5BU5D_t3908492414* get_c_map_11() const { return ___c_map_11; }
	inline MapU5BU5D_t3908492414** get_address_of_c_map_11() { return &___c_map_11; }
	inline void set_c_map_11(MapU5BU5D_t3908492414* value)
	{
		___c_map_11 = value;
		Il2CppCodeGenWriteBarrier(&___c_map_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t3565032206_marshaled_pinvoke
{
	DatatypeImplementation_t1152094268 * ___datatype_0;
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	char* ___regStr_9;
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t3565032206_marshaled_com
{
	DatatypeImplementation_t1152094268 * ___datatype_0;
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	Il2CppChar* ___regStr_9;
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;
};
