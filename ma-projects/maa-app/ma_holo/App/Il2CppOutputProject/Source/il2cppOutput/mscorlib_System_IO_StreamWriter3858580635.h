﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter4027217640.h"

// System.IO.StreamWriter
struct StreamWriter_t3858580635;
// System.IO.Stream
struct Stream_t3255436806;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Text.Encoder
struct Encoder_t751367874;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamWriter
struct  StreamWriter_t3858580635  : public TextWriter_t4027217640
{
public:
	// System.IO.Stream System.IO.StreamWriter::stream
	Stream_t3255436806 * ___stream_12;
	// System.Text.Encoding System.IO.StreamWriter::encoding
	Encoding_t663144255 * ___encoding_13;
	// System.Text.Encoder System.IO.StreamWriter::encoder
	Encoder_t751367874 * ___encoder_14;
	// System.Byte[] System.IO.StreamWriter::byteBuffer
	ByteU5BU5D_t3397334013* ___byteBuffer_15;
	// System.Char[] System.IO.StreamWriter::charBuffer
	CharU5BU5D_t1328083999* ___charBuffer_16;
	// System.Int32 System.IO.StreamWriter::charPos
	int32_t ___charPos_17;
	// System.Int32 System.IO.StreamWriter::charLen
	int32_t ___charLen_18;
	// System.Boolean System.IO.StreamWriter::autoFlush
	bool ___autoFlush_19;
	// System.Boolean System.IO.StreamWriter::haveWrittenPreamble
	bool ___haveWrittenPreamble_20;
	// System.Boolean System.IO.StreamWriter::closable
	bool ___closable_21;
	// System.Threading.Tasks.Task modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamWriter::_asyncWriteTask
	Task_t1843236107 * ____asyncWriteTask_22;

public:
	inline static int32_t get_offset_of_stream_12() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___stream_12)); }
	inline Stream_t3255436806 * get_stream_12() const { return ___stream_12; }
	inline Stream_t3255436806 ** get_address_of_stream_12() { return &___stream_12; }
	inline void set_stream_12(Stream_t3255436806 * value)
	{
		___stream_12 = value;
		Il2CppCodeGenWriteBarrier(&___stream_12, value);
	}

	inline static int32_t get_offset_of_encoding_13() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___encoding_13)); }
	inline Encoding_t663144255 * get_encoding_13() const { return ___encoding_13; }
	inline Encoding_t663144255 ** get_address_of_encoding_13() { return &___encoding_13; }
	inline void set_encoding_13(Encoding_t663144255 * value)
	{
		___encoding_13 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_13, value);
	}

	inline static int32_t get_offset_of_encoder_14() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___encoder_14)); }
	inline Encoder_t751367874 * get_encoder_14() const { return ___encoder_14; }
	inline Encoder_t751367874 ** get_address_of_encoder_14() { return &___encoder_14; }
	inline void set_encoder_14(Encoder_t751367874 * value)
	{
		___encoder_14 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_14, value);
	}

	inline static int32_t get_offset_of_byteBuffer_15() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___byteBuffer_15)); }
	inline ByteU5BU5D_t3397334013* get_byteBuffer_15() const { return ___byteBuffer_15; }
	inline ByteU5BU5D_t3397334013** get_address_of_byteBuffer_15() { return &___byteBuffer_15; }
	inline void set_byteBuffer_15(ByteU5BU5D_t3397334013* value)
	{
		___byteBuffer_15 = value;
		Il2CppCodeGenWriteBarrier(&___byteBuffer_15, value);
	}

	inline static int32_t get_offset_of_charBuffer_16() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___charBuffer_16)); }
	inline CharU5BU5D_t1328083999* get_charBuffer_16() const { return ___charBuffer_16; }
	inline CharU5BU5D_t1328083999** get_address_of_charBuffer_16() { return &___charBuffer_16; }
	inline void set_charBuffer_16(CharU5BU5D_t1328083999* value)
	{
		___charBuffer_16 = value;
		Il2CppCodeGenWriteBarrier(&___charBuffer_16, value);
	}

	inline static int32_t get_offset_of_charPos_17() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___charPos_17)); }
	inline int32_t get_charPos_17() const { return ___charPos_17; }
	inline int32_t* get_address_of_charPos_17() { return &___charPos_17; }
	inline void set_charPos_17(int32_t value)
	{
		___charPos_17 = value;
	}

	inline static int32_t get_offset_of_charLen_18() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___charLen_18)); }
	inline int32_t get_charLen_18() const { return ___charLen_18; }
	inline int32_t* get_address_of_charLen_18() { return &___charLen_18; }
	inline void set_charLen_18(int32_t value)
	{
		___charLen_18 = value;
	}

	inline static int32_t get_offset_of_autoFlush_19() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___autoFlush_19)); }
	inline bool get_autoFlush_19() const { return ___autoFlush_19; }
	inline bool* get_address_of_autoFlush_19() { return &___autoFlush_19; }
	inline void set_autoFlush_19(bool value)
	{
		___autoFlush_19 = value;
	}

	inline static int32_t get_offset_of_haveWrittenPreamble_20() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___haveWrittenPreamble_20)); }
	inline bool get_haveWrittenPreamble_20() const { return ___haveWrittenPreamble_20; }
	inline bool* get_address_of_haveWrittenPreamble_20() { return &___haveWrittenPreamble_20; }
	inline void set_haveWrittenPreamble_20(bool value)
	{
		___haveWrittenPreamble_20 = value;
	}

	inline static int32_t get_offset_of_closable_21() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___closable_21)); }
	inline bool get_closable_21() const { return ___closable_21; }
	inline bool* get_address_of_closable_21() { return &___closable_21; }
	inline void set_closable_21(bool value)
	{
		___closable_21 = value;
	}

	inline static int32_t get_offset_of__asyncWriteTask_22() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ____asyncWriteTask_22)); }
	inline Task_t1843236107 * get__asyncWriteTask_22() const { return ____asyncWriteTask_22; }
	inline Task_t1843236107 ** get_address_of__asyncWriteTask_22() { return &____asyncWriteTask_22; }
	inline void set__asyncWriteTask_22(Task_t1843236107 * value)
	{
		____asyncWriteTask_22 = value;
		Il2CppCodeGenWriteBarrier(&____asyncWriteTask_22, value);
	}
};

struct StreamWriter_t3858580635_StaticFields
{
public:
	// System.IO.StreamWriter System.IO.StreamWriter::Null
	StreamWriter_t3858580635 * ___Null_11;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamWriter::_UTF8NoBOM
	Encoding_t663144255 * ____UTF8NoBOM_23;

public:
	inline static int32_t get_offset_of_Null_11() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635_StaticFields, ___Null_11)); }
	inline StreamWriter_t3858580635 * get_Null_11() const { return ___Null_11; }
	inline StreamWriter_t3858580635 ** get_address_of_Null_11() { return &___Null_11; }
	inline void set_Null_11(StreamWriter_t3858580635 * value)
	{
		___Null_11 = value;
		Il2CppCodeGenWriteBarrier(&___Null_11, value);
	}

	inline static int32_t get_offset_of__UTF8NoBOM_23() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635_StaticFields, ____UTF8NoBOM_23)); }
	inline Encoding_t663144255 * get__UTF8NoBOM_23() const { return ____UTF8NoBOM_23; }
	inline Encoding_t663144255 ** get_address_of__UTF8NoBOM_23() { return &____UTF8NoBOM_23; }
	inline void set__UTF8NoBOM_23(Encoding_t663144255 * value)
	{
		____UTF8NoBOM_23 = value;
		Il2CppCodeGenWriteBarrier(&____UTF8NoBOM_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
