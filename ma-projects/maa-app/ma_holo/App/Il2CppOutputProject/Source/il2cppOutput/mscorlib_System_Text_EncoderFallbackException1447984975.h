﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ArgumentException3259014390.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallbackException
struct  EncoderFallbackException_t1447984975  : public ArgumentException_t3259014390
{
public:
	// System.Char System.Text.EncoderFallbackException::charUnknown
	Il2CppChar ___charUnknown_17;
	// System.Char System.Text.EncoderFallbackException::charUnknownHigh
	Il2CppChar ___charUnknownHigh_18;
	// System.Char System.Text.EncoderFallbackException::charUnknownLow
	Il2CppChar ___charUnknownLow_19;
	// System.Int32 System.Text.EncoderFallbackException::index
	int32_t ___index_20;

public:
	inline static int32_t get_offset_of_charUnknown_17() { return static_cast<int32_t>(offsetof(EncoderFallbackException_t1447984975, ___charUnknown_17)); }
	inline Il2CppChar get_charUnknown_17() const { return ___charUnknown_17; }
	inline Il2CppChar* get_address_of_charUnknown_17() { return &___charUnknown_17; }
	inline void set_charUnknown_17(Il2CppChar value)
	{
		___charUnknown_17 = value;
	}

	inline static int32_t get_offset_of_charUnknownHigh_18() { return static_cast<int32_t>(offsetof(EncoderFallbackException_t1447984975, ___charUnknownHigh_18)); }
	inline Il2CppChar get_charUnknownHigh_18() const { return ___charUnknownHigh_18; }
	inline Il2CppChar* get_address_of_charUnknownHigh_18() { return &___charUnknownHigh_18; }
	inline void set_charUnknownHigh_18(Il2CppChar value)
	{
		___charUnknownHigh_18 = value;
	}

	inline static int32_t get_offset_of_charUnknownLow_19() { return static_cast<int32_t>(offsetof(EncoderFallbackException_t1447984975, ___charUnknownLow_19)); }
	inline Il2CppChar get_charUnknownLow_19() const { return ___charUnknownLow_19; }
	inline Il2CppChar* get_address_of_charUnknownLow_19() { return &___charUnknownLow_19; }
	inline void set_charUnknownLow_19(Il2CppChar value)
	{
		___charUnknownLow_19 = value;
	}

	inline static int32_t get_offset_of_index_20() { return static_cast<int32_t>(offsetof(EncoderFallbackException_t1447984975, ___index_20)); }
	inline int32_t get_index_20() const { return ___index_20; }
	inline int32_t* get_address_of_index_20() { return &___index_20; }
	inline void set_index_20(int32_t value)
	{
		___index_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
