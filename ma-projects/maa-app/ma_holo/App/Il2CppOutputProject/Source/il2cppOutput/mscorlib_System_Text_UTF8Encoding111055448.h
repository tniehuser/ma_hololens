﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Encoding663144255.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.UTF8Encoding
struct  UTF8Encoding_t111055448  : public Encoding_t663144255
{
public:
	// System.Boolean System.Text.UTF8Encoding::emitUTF8Identifier
	bool ___emitUTF8Identifier_15;
	// System.Boolean System.Text.UTF8Encoding::isThrowException
	bool ___isThrowException_16;

public:
	inline static int32_t get_offset_of_emitUTF8Identifier_15() { return static_cast<int32_t>(offsetof(UTF8Encoding_t111055448, ___emitUTF8Identifier_15)); }
	inline bool get_emitUTF8Identifier_15() const { return ___emitUTF8Identifier_15; }
	inline bool* get_address_of_emitUTF8Identifier_15() { return &___emitUTF8Identifier_15; }
	inline void set_emitUTF8Identifier_15(bool value)
	{
		___emitUTF8Identifier_15 = value;
	}

	inline static int32_t get_offset_of_isThrowException_16() { return static_cast<int32_t>(offsetof(UTF8Encoding_t111055448, ___isThrowException_16)); }
	inline bool get_isThrowException_16() const { return ___isThrowException_16; }
	inline bool* get_address_of_isThrowException_16() { return &___isThrowException_16; }
	inline void set_isThrowException_16(bool value)
	{
		___isThrowException_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
