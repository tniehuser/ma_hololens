﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t2469392150;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_t3902733837;
// System.Text.RegularExpressions.ExclusiveReference
struct ExclusiveReference_t708182869;
// System.Text.RegularExpressions.SharedReference
struct SharedReference_t2137668360;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CachedCodeEntry
struct  CachedCodeEntry_t3553821051  : public Il2CppObject
{
public:
	// System.String System.Text.RegularExpressions.CachedCodeEntry::_key
	String_t* ____key_0;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.CachedCodeEntry::_code
	RegexCode_t2469392150 * ____code_1;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_caps
	Hashtable_t909839986 * ____caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_capnames
	Hashtable_t909839986 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.CachedCodeEntry::_capslist
	StringU5BU5D_t1642385972* ____capslist_4;
	// System.Int32 System.Text.RegularExpressions.CachedCodeEntry::_capsize
	int32_t ____capsize_5;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.CachedCodeEntry::_factory
	RegexRunnerFactory_t3902733837 * ____factory_6;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.CachedCodeEntry::_runnerref
	ExclusiveReference_t708182869 * ____runnerref_7;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.CachedCodeEntry::_replref
	SharedReference_t2137668360 * ____replref_8;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier(&____key_0, value);
	}

	inline static int32_t get_offset_of__code_1() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____code_1)); }
	inline RegexCode_t2469392150 * get__code_1() const { return ____code_1; }
	inline RegexCode_t2469392150 ** get_address_of__code_1() { return &____code_1; }
	inline void set__code_1(RegexCode_t2469392150 * value)
	{
		____code_1 = value;
		Il2CppCodeGenWriteBarrier(&____code_1, value);
	}

	inline static int32_t get_offset_of__caps_2() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____caps_2)); }
	inline Hashtable_t909839986 * get__caps_2() const { return ____caps_2; }
	inline Hashtable_t909839986 ** get_address_of__caps_2() { return &____caps_2; }
	inline void set__caps_2(Hashtable_t909839986 * value)
	{
		____caps_2 = value;
		Il2CppCodeGenWriteBarrier(&____caps_2, value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capnames_3)); }
	inline Hashtable_t909839986 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t909839986 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t909839986 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier(&____capnames_3, value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capslist_4)); }
	inline StringU5BU5D_t1642385972* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1642385972** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1642385972* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier(&____capslist_4, value);
	}

	inline static int32_t get_offset_of__capsize_5() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capsize_5)); }
	inline int32_t get__capsize_5() const { return ____capsize_5; }
	inline int32_t* get_address_of__capsize_5() { return &____capsize_5; }
	inline void set__capsize_5(int32_t value)
	{
		____capsize_5 = value;
	}

	inline static int32_t get_offset_of__factory_6() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____factory_6)); }
	inline RegexRunnerFactory_t3902733837 * get__factory_6() const { return ____factory_6; }
	inline RegexRunnerFactory_t3902733837 ** get_address_of__factory_6() { return &____factory_6; }
	inline void set__factory_6(RegexRunnerFactory_t3902733837 * value)
	{
		____factory_6 = value;
		Il2CppCodeGenWriteBarrier(&____factory_6, value);
	}

	inline static int32_t get_offset_of__runnerref_7() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____runnerref_7)); }
	inline ExclusiveReference_t708182869 * get__runnerref_7() const { return ____runnerref_7; }
	inline ExclusiveReference_t708182869 ** get_address_of__runnerref_7() { return &____runnerref_7; }
	inline void set__runnerref_7(ExclusiveReference_t708182869 * value)
	{
		____runnerref_7 = value;
		Il2CppCodeGenWriteBarrier(&____runnerref_7, value);
	}

	inline static int32_t get_offset_of__replref_8() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____replref_8)); }
	inline SharedReference_t2137668360 * get__replref_8() const { return ____replref_8; }
	inline SharedReference_t2137668360 ** get_address_of__replref_8() { return &____replref_8; }
	inline void set__replref_8(SharedReference_t2137668360 * value)
	{
		____replref_8 = value;
		Il2CppCodeGenWriteBarrier(&____replref_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
