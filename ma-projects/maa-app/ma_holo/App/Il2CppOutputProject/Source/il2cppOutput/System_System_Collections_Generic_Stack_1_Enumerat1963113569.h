﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Collections.Generic.Stack`1<System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame>
struct Stack_1_t1313115209;
// System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame
struct ParseElementOnlyContent_LocalFrame_t225387055;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame>
struct  Enumerator_t1963113569 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator::_stack
	Stack_1_t1313115209 * ____stack_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.Stack`1/Enumerator::currentElement
	ParseElementOnlyContent_LocalFrame_t225387055 * ___currentElement_3;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(Enumerator_t1963113569, ____stack_0)); }
	inline Stack_1_t1313115209 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t1313115209 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t1313115209 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier(&____stack_0, value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1963113569, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1963113569, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of_currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_t1963113569, ___currentElement_3)); }
	inline ParseElementOnlyContent_LocalFrame_t225387055 * get_currentElement_3() const { return ___currentElement_3; }
	inline ParseElementOnlyContent_LocalFrame_t225387055 ** get_address_of_currentElement_3() { return &___currentElement_3; }
	inline void set_currentElement_3(ParseElementOnlyContent_LocalFrame_t225387055 * value)
	{
		___currentElement_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentElement_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
