﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterf295178211.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterf910653559.h"
#include "mscorlib_System_Runtime_InteropServices_ComVisible2245573759.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImp2390314680.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnum3193309542.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedT2550630890.h"
#include "mscorlib_System_Runtime_InteropServices_ComImportAt468083054.h"
#include "mscorlib_System_Runtime_InteropServices_GuidAttribu222072359.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSi1564965109.h"
#include "mscorlib_System_Runtime_InteropServices_InAttribut1394050551.h"
#include "mscorlib_System_Runtime_InteropServices_OutAttribu1539424546.h"
#include "mscorlib_System_Runtime_InteropServices_OptionalAtt827982902.h"
#include "mscorlib_System_Runtime_InteropServices_DllImportA3000813225.h"
#include "mscorlib_System_Runtime_InteropServices_FieldOffse1553145711.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompati1884740313.h"
#include "mscorlib_System_Runtime_InteropServices_CallingCon3354538265.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet2778376310.h"
#include "mscorlib_System_Runtime_InteropServices_COMExcepti1790481504.h"
#include "mscorlib_System_Runtime_InteropServices_ErrorWrapp2775489663.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1252662682.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDir1326890414.h"
#include "mscorlib_System_Runtime_InteropServices_RuntimeEnv4110511353.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle2733794115.h"
#include "mscorlib_System_Runtime_InteropServices_WindowsRun3312105128.h"
#include "mscorlib_System_Runtime_InteropServices_WindowsRun1031881217.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Illogica206449943.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalC725724420.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Logical1568751674.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallCon4091024823.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallCon2648008188.h"
#include "mscorlib_System_Runtime_Serialization_Deserializat1801856893.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2339848500.h"
#include "mscorlib_System_Runtime_Serialization_FormatterConv764140214.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3092009747.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1159771836.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi316080507.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2423901178.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3375684671.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3309678508.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi763496928.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1773568836.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3366135632.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2307902425.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1989374585.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1486079134.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3733510915.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2163984170.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3348677078.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2848964452.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3584398440.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1102219583.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1089955196.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3819299862.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3191050891.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2581360082.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1331452382.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4279057407.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2877339122.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi414850623.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1212206240.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi682686529.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3272317689.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4215080384.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3719433431.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi642857832.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2706794673.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1992316134.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1383558988.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2912249390.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2193277619.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3709702807.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3642132829.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2489189536.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1645661573.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4094458531.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4068137215.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3804990478.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1476095226.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2965287612.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3366789405.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4293742132.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1222443990.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2674254118.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3886188184.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2151058854.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2017939501.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3722972407.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1348745115.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3355145566.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2407316929.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3608773834.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo943306207.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo999493661.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_T1182459634.h"
#include "mscorlib_System_Runtime_Serialization_FormatterSer3161112612.h"
#include "mscorlib_System_Runtime_Serialization_SurrogateFor2955664662.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize500 = { sizeof (ClassInterfaceType_t295178211)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable500[4] = 
{
	ClassInterfaceType_t295178211::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize501 = { sizeof (ClassInterfaceAttribute_t910653559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable501[1] = 
{
	ClassInterfaceAttribute_t910653559::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize502 = { sizeof (ComVisibleAttribute_t2245573759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable502[1] = 
{
	ComVisibleAttribute_t2245573759::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize503 = { sizeof (TypeLibImportClassAttribute_t2390314680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable503[1] = 
{
	TypeLibImportClassAttribute_t2390314680::get_offset_of__importClassName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize504 = { sizeof (VarEnum_t3193309542)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable504[45] = 
{
	VarEnum_t3193309542::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize505 = { sizeof (UnmanagedType_t2550630890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable505[38] = 
{
	UnmanagedType_t2550630890::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize506 = { sizeof (ComImportAttribute_t468083054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize507 = { sizeof (GuidAttribute_t222072359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable507[1] = 
{
	GuidAttribute_t222072359::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize508 = { sizeof (PreserveSigAttribute_t1564965109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize509 = { sizeof (InAttribute_t1394050551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize510 = { sizeof (OutAttribute_t1539424546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize511 = { sizeof (OptionalAttribute_t827982902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize512 = { sizeof (DllImportAttribute_t3000813225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable512[9] = 
{
	DllImportAttribute_t3000813225::get_offset_of__val_0(),
	DllImportAttribute_t3000813225::get_offset_of_EntryPoint_1(),
	DllImportAttribute_t3000813225::get_offset_of_CharSet_2(),
	DllImportAttribute_t3000813225::get_offset_of_SetLastError_3(),
	DllImportAttribute_t3000813225::get_offset_of_ExactSpelling_4(),
	DllImportAttribute_t3000813225::get_offset_of_PreserveSig_5(),
	DllImportAttribute_t3000813225::get_offset_of_CallingConvention_6(),
	DllImportAttribute_t3000813225::get_offset_of_BestFitMapping_7(),
	DllImportAttribute_t3000813225::get_offset_of_ThrowOnUnmappableChar_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize513 = { sizeof (FieldOffsetAttribute_t1553145711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable513[1] = 
{
	FieldOffsetAttribute_t1553145711::get_offset_of__val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize514 = { sizeof (ComCompatibleVersionAttribute_t1884740313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable514[4] = 
{
	ComCompatibleVersionAttribute_t1884740313::get_offset_of__major_0(),
	ComCompatibleVersionAttribute_t1884740313::get_offset_of__minor_1(),
	ComCompatibleVersionAttribute_t1884740313::get_offset_of__build_2(),
	ComCompatibleVersionAttribute_t1884740313::get_offset_of__revision_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize515 = { sizeof (CallingConvention_t3354538265)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable515[6] = 
{
	CallingConvention_t3354538265::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize516 = { sizeof (CharSet_t2778376310)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable516[5] = 
{
	CharSet_t2778376310::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize517 = { sizeof (COMException_t1790481504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize518 = { sizeof (ErrorWrapper_t2775489663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable518[1] = 
{
	ErrorWrapper_t2775489663::get_offset_of_m_ErrorCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize519 = { sizeof (ExternalException_t1252662682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize520 = { sizeof (MarshalDirectiveException_t1326890414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize521 = { sizeof (RuntimeEnvironment_t4110511353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize522 = { sizeof (SafeHandle_t2733794115), sizeof(void*), 0, 0 };
extern const int32_t g_FieldOffsetTable522[4] = 
{
	SafeHandle_t2733794115::get_offset_of_handle_0(),
	SafeHandle_t2733794115::get_offset_of__state_1(),
	SafeHandle_t2733794115::get_offset_of__ownsHandle_2(),
	SafeHandle_t2733794115::get_offset_of__fullyInitialized_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize523 = { 0, sizeof(IRestrictedErrorInfo_t3312105128*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize524 = { sizeof (WindowsRuntimeMarshal_t1031881217), -1, sizeof(WindowsRuntimeMarshal_t1031881217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable524[2] = 
{
	WindowsRuntimeMarshal_t1031881217_StaticFields::get_offset_of_s_haveBlueErrorApis_0(),
	WindowsRuntimeMarshal_t1031881217_StaticFields::get_offset_of_s_iidIErrorInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize525 = { sizeof (IllogicalCallContext_t206449943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable525[2] = 
{
	IllogicalCallContext_t206449943::get_offset_of_m_Datastore_0(),
	IllogicalCallContext_t206449943::get_offset_of_m_HostContext_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize526 = { sizeof (LogicalCallContext_t725724420), -1, sizeof(LogicalCallContext_t725724420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable526[6] = 
{
	LogicalCallContext_t725724420_StaticFields::get_offset_of_s_callContextType_0(),
	LogicalCallContext_t725724420::get_offset_of_m_Datastore_1(),
	LogicalCallContext_t725724420::get_offset_of_m_RemotingData_2(),
	LogicalCallContext_t725724420::get_offset_of_m_SecurityData_3(),
	LogicalCallContext_t725724420::get_offset_of_m_HostContext_4(),
	LogicalCallContext_t725724420::get_offset_of_m_IsCorrelationMgr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize527 = { sizeof (Reader_t1568751674)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable527[1] = 
{
	Reader_t1568751674::get_offset_of_m_ctx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize528 = { sizeof (CallContextSecurityData_t4091024823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable528[1] = 
{
	CallContextSecurityData_t4091024823::get_offset_of__principal_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize529 = { sizeof (CallContextRemotingData_t2648008188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable529[1] = 
{
	CallContextRemotingData_t2648008188::get_offset_of__logicalCallID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize530 = { sizeof (DeserializationEventHandler_t1801856893), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize531 = { sizeof (SerializationEventHandler_t2339848500), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize532 = { sizeof (FormatterConverter_t764140214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize533 = { sizeof (BinaryConverter_t3092009747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize534 = { sizeof (IOUtil_t1159771836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize536 = { sizeof (BinaryAssemblyInfo_t316080507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable536[2] = 
{
	BinaryAssemblyInfo_t316080507::get_offset_of_assemblyString_0(),
	BinaryAssemblyInfo_t316080507::get_offset_of_assembly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize537 = { sizeof (SerializationHeaderRecord_t2423901178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable537[7] = 
{
	SerializationHeaderRecord_t2423901178::get_offset_of_binaryFormatterMajorVersion_0(),
	SerializationHeaderRecord_t2423901178::get_offset_of_binaryFormatterMinorVersion_1(),
	SerializationHeaderRecord_t2423901178::get_offset_of_binaryHeaderEnum_2(),
	SerializationHeaderRecord_t2423901178::get_offset_of_topId_3(),
	SerializationHeaderRecord_t2423901178::get_offset_of_headerId_4(),
	SerializationHeaderRecord_t2423901178::get_offset_of_majorVersion_5(),
	SerializationHeaderRecord_t2423901178::get_offset_of_minorVersion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize538 = { sizeof (BinaryAssembly_t3375684671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable538[2] = 
{
	BinaryAssembly_t3375684671::get_offset_of_assemId_0(),
	BinaryAssembly_t3375684671::get_offset_of_assemblyString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize539 = { sizeof (BinaryCrossAppDomainAssembly_t3309678508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable539[2] = 
{
	BinaryCrossAppDomainAssembly_t3309678508::get_offset_of_assemId_0(),
	BinaryCrossAppDomainAssembly_t3309678508::get_offset_of_assemblyIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize540 = { sizeof (BinaryObject_t763496928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable540[2] = 
{
	BinaryObject_t763496928::get_offset_of_objectId_0(),
	BinaryObject_t763496928::get_offset_of_mapId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize541 = { sizeof (BinaryMethodCall_t1773568836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable541[13] = 
{
	BinaryMethodCall_t1773568836::get_offset_of_uri_0(),
	BinaryMethodCall_t1773568836::get_offset_of_methodName_1(),
	BinaryMethodCall_t1773568836::get_offset_of_typeName_2(),
	BinaryMethodCall_t1773568836::get_offset_of_instArgs_3(),
	BinaryMethodCall_t1773568836::get_offset_of_args_4(),
	BinaryMethodCall_t1773568836::get_offset_of_methodSignature_5(),
	BinaryMethodCall_t1773568836::get_offset_of_callContext_6(),
	BinaryMethodCall_t1773568836::get_offset_of_scallContext_7(),
	BinaryMethodCall_t1773568836::get_offset_of_properties_8(),
	BinaryMethodCall_t1773568836::get_offset_of_argTypes_9(),
	BinaryMethodCall_t1773568836::get_offset_of_bArgsPrimitive_10(),
	BinaryMethodCall_t1773568836::get_offset_of_messageEnum_11(),
	BinaryMethodCall_t1773568836::get_offset_of_callA_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize542 = { sizeof (BinaryMethodReturn_t3366135632), -1, sizeof(BinaryMethodReturn_t3366135632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable542[12] = 
{
	BinaryMethodReturn_t3366135632::get_offset_of_returnValue_0(),
	BinaryMethodReturn_t3366135632::get_offset_of_args_1(),
	BinaryMethodReturn_t3366135632::get_offset_of_exception_2(),
	BinaryMethodReturn_t3366135632::get_offset_of_callContext_3(),
	BinaryMethodReturn_t3366135632::get_offset_of_scallContext_4(),
	BinaryMethodReturn_t3366135632::get_offset_of_properties_5(),
	BinaryMethodReturn_t3366135632::get_offset_of_argTypes_6(),
	BinaryMethodReturn_t3366135632::get_offset_of_bArgsPrimitive_7(),
	BinaryMethodReturn_t3366135632::get_offset_of_messageEnum_8(),
	BinaryMethodReturn_t3366135632::get_offset_of_callA_9(),
	BinaryMethodReturn_t3366135632::get_offset_of_returnType_10(),
	BinaryMethodReturn_t3366135632_StaticFields::get_offset_of_instanceOfVoid_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize543 = { sizeof (BinaryObjectString_t2307902425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable543[2] = 
{
	BinaryObjectString_t2307902425::get_offset_of_objectId_0(),
	BinaryObjectString_t2307902425::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize544 = { sizeof (BinaryCrossAppDomainString_t1989374585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable544[2] = 
{
	BinaryCrossAppDomainString_t1989374585::get_offset_of_objectId_0(),
	BinaryCrossAppDomainString_t1989374585::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize545 = { sizeof (BinaryCrossAppDomainMap_t1486079134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable545[1] = 
{
	BinaryCrossAppDomainMap_t1486079134::get_offset_of_crossAppDomainArrayIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize546 = { sizeof (MemberPrimitiveTyped_t3733510915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable546[2] = 
{
	MemberPrimitiveTyped_t3733510915::get_offset_of_primitiveTypeEnum_0(),
	MemberPrimitiveTyped_t3733510915::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize547 = { sizeof (BinaryObjectWithMap_t2163984170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable547[6] = 
{
	BinaryObjectWithMap_t2163984170::get_offset_of_binaryHeaderEnum_0(),
	BinaryObjectWithMap_t2163984170::get_offset_of_objectId_1(),
	BinaryObjectWithMap_t2163984170::get_offset_of_name_2(),
	BinaryObjectWithMap_t2163984170::get_offset_of_numMembers_3(),
	BinaryObjectWithMap_t2163984170::get_offset_of_memberNames_4(),
	BinaryObjectWithMap_t2163984170::get_offset_of_assemId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize548 = { sizeof (BinaryObjectWithMapTyped_t3348677078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable548[9] = 
{
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_binaryHeaderEnum_0(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_objectId_1(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_name_2(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_numMembers_3(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_memberNames_4(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_binaryTypeEnumA_5(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_typeInformationA_6(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_memberAssemIds_7(),
	BinaryObjectWithMapTyped_t3348677078::get_offset_of_assemId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize549 = { sizeof (BinaryArray_t2848964452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable549[9] = 
{
	BinaryArray_t2848964452::get_offset_of_objectId_0(),
	BinaryArray_t2848964452::get_offset_of_rank_1(),
	BinaryArray_t2848964452::get_offset_of_lengthA_2(),
	BinaryArray_t2848964452::get_offset_of_lowerBoundA_3(),
	BinaryArray_t2848964452::get_offset_of_binaryTypeEnum_4(),
	BinaryArray_t2848964452::get_offset_of_typeInformation_5(),
	BinaryArray_t2848964452::get_offset_of_assemId_6(),
	BinaryArray_t2848964452::get_offset_of_binaryHeaderEnum_7(),
	BinaryArray_t2848964452::get_offset_of_binaryArrayTypeEnum_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize550 = { sizeof (MemberPrimitiveUnTyped_t3584398440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable550[2] = 
{
	MemberPrimitiveUnTyped_t3584398440::get_offset_of_typeInformation_0(),
	MemberPrimitiveUnTyped_t3584398440::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize551 = { sizeof (MemberReference_t1102219583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable551[1] = 
{
	MemberReference_t1102219583::get_offset_of_idRef_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize552 = { sizeof (ObjectNull_t1089955196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable552[1] = 
{
	ObjectNull_t1089955196::get_offset_of_nullCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize553 = { sizeof (MessageEnd_t3819299862), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize554 = { sizeof (ObjectMap_t3191050891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable554[11] = 
{
	ObjectMap_t3191050891::get_offset_of_objectName_0(),
	ObjectMap_t3191050891::get_offset_of_objectType_1(),
	ObjectMap_t3191050891::get_offset_of_binaryTypeEnumA_2(),
	ObjectMap_t3191050891::get_offset_of_typeInformationA_3(),
	ObjectMap_t3191050891::get_offset_of_memberTypes_4(),
	ObjectMap_t3191050891::get_offset_of_memberNames_5(),
	ObjectMap_t3191050891::get_offset_of_objectInfo_6(),
	ObjectMap_t3191050891::get_offset_of_isInitObjectInfo_7(),
	ObjectMap_t3191050891::get_offset_of_objectReader_8(),
	ObjectMap_t3191050891::get_offset_of_objectId_9(),
	ObjectMap_t3191050891::get_offset_of_assemblyInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize555 = { sizeof (ObjectProgress_t2581360082), -1, sizeof(ObjectProgress_t2581360082_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable555[20] = 
{
	ObjectProgress_t2581360082_StaticFields::get_offset_of_opRecordIdCount_0(),
	ObjectProgress_t2581360082::get_offset_of_isInitial_1(),
	ObjectProgress_t2581360082::get_offset_of_count_2(),
	ObjectProgress_t2581360082::get_offset_of_expectedType_3(),
	ObjectProgress_t2581360082::get_offset_of_expectedTypeInformation_4(),
	ObjectProgress_t2581360082::get_offset_of_name_5(),
	ObjectProgress_t2581360082::get_offset_of_objectTypeEnum_6(),
	ObjectProgress_t2581360082::get_offset_of_memberTypeEnum_7(),
	ObjectProgress_t2581360082::get_offset_of_memberValueEnum_8(),
	ObjectProgress_t2581360082::get_offset_of_dtType_9(),
	ObjectProgress_t2581360082::get_offset_of_numItems_10(),
	ObjectProgress_t2581360082::get_offset_of_binaryTypeEnum_11(),
	ObjectProgress_t2581360082::get_offset_of_typeInformation_12(),
	ObjectProgress_t2581360082::get_offset_of_nullCount_13(),
	ObjectProgress_t2581360082::get_offset_of_memberLength_14(),
	ObjectProgress_t2581360082::get_offset_of_binaryTypeEnumA_15(),
	ObjectProgress_t2581360082::get_offset_of_typeInformationA_16(),
	ObjectProgress_t2581360082::get_offset_of_memberNames_17(),
	ObjectProgress_t2581360082::get_offset_of_memberTypes_18(),
	ObjectProgress_t2581360082::get_offset_of_pr_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize556 = { sizeof (Converter_t1331452382), -1, sizeof(Converter_t1331452382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable556[47] = 
{
	Converter_t1331452382_StaticFields::get_offset_of_primitiveTypeEnumLength_0(),
	Converter_t1331452382_StaticFields::get_offset_of_typeA_1(),
	Converter_t1331452382_StaticFields::get_offset_of_arrayTypeA_2(),
	Converter_t1331452382_StaticFields::get_offset_of_valueA_3(),
	Converter_t1331452382_StaticFields::get_offset_of_typeCodeA_4(),
	Converter_t1331452382_StaticFields::get_offset_of_codeA_5(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofISerializable_6(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofString_7(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofConverter_8(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofBoolean_9(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofByte_10(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofChar_11(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDecimal_12(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDouble_13(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt16_14(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt32_15(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt64_16(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofSByte_17(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofSingle_18(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofTimeSpan_19(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDateTime_20(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt16_21(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt32_22(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt64_23(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofObject_24(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofSystemVoid_25(),
	Converter_t1331452382_StaticFields::get_offset_of_urtAssembly_26(),
	Converter_t1331452382_StaticFields::get_offset_of_urtAssemblyString_27(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofTypeArray_28(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofObjectArray_29(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofStringArray_30(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofBooleanArray_31(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofByteArray_32(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofCharArray_33(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDecimalArray_34(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDoubleArray_35(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt16Array_36(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt32Array_37(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofInt64Array_38(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofSByteArray_39(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofSingleArray_40(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofTimeSpanArray_41(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofDateTimeArray_42(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt16Array_43(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt32Array_44(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofUInt64Array_45(),
	Converter_t1331452382_StaticFields::get_offset_of_typeofMarshalByRefObject_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize557 = { sizeof (BinaryHeaderEnum_t4279057407)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable557[24] = 
{
	BinaryHeaderEnum_t4279057407::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize558 = { sizeof (BinaryTypeEnum_t2877339122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable558[9] = 
{
	BinaryTypeEnum_t2877339122::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize559 = { sizeof (BinaryArrayTypeEnum_t414850623)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable559[7] = 
{
	BinaryArrayTypeEnum_t414850623::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize560 = { sizeof (InternalSerializerTypeE_t1212206240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable560[3] = 
{
	InternalSerializerTypeE_t1212206240::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize561 = { sizeof (InternalParseTypeE_t682686529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable561[14] = 
{
	InternalParseTypeE_t682686529::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize562 = { sizeof (InternalObjectTypeE_t3272317689)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable562[4] = 
{
	InternalObjectTypeE_t3272317689::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize563 = { sizeof (InternalObjectPositionE_t4215080384)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable563[5] = 
{
	InternalObjectPositionE_t4215080384::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize564 = { sizeof (InternalArrayTypeE_t3719433431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable564[6] = 
{
	InternalArrayTypeE_t3719433431::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize565 = { sizeof (InternalMemberTypeE_t642857832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable565[5] = 
{
	InternalMemberTypeE_t642857832::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize566 = { sizeof (InternalMemberValueE_t2706794673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable566[6] = 
{
	InternalMemberValueE_t2706794673::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize567 = { sizeof (InternalPrimitiveTypeE_t3020811655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable567[20] = 
{
	InternalPrimitiveTypeE_t3020811655::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize568 = { sizeof (MessageEnum_t1992316134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable568[16] = 
{
	MessageEnum_t1992316134::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize569 = { sizeof (ValueFixupEnum_t1383558988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable569[5] = 
{
	ValueFixupEnum_t1383558988::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize570 = { sizeof (BinaryFormatter_t1866979105), -1, sizeof(BinaryFormatter_t1866979105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable570[8] = 
{
	BinaryFormatter_t1866979105::get_offset_of_m_surrogates_0(),
	BinaryFormatter_t1866979105::get_offset_of_m_context_1(),
	BinaryFormatter_t1866979105::get_offset_of_m_binder_2(),
	BinaryFormatter_t1866979105::get_offset_of_m_typeFormat_3(),
	BinaryFormatter_t1866979105::get_offset_of_m_assemblyFormat_4(),
	BinaryFormatter_t1866979105::get_offset_of_m_securityLevel_5(),
	BinaryFormatter_t1866979105::get_offset_of_m_crossAppDomainArray_6(),
	BinaryFormatter_t1866979105_StaticFields::get_offset_of_typeNameCache_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize571 = { sizeof (__BinaryWriter_t2912249390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable571[21] = 
{
	__BinaryWriter_t2912249390::get_offset_of_sout_0(),
	__BinaryWriter_t2912249390::get_offset_of_formatterTypeStyle_1(),
	__BinaryWriter_t2912249390::get_offset_of_objectMapTable_2(),
	__BinaryWriter_t2912249390::get_offset_of_objectWriter_3(),
	__BinaryWriter_t2912249390::get_offset_of_dataWriter_4(),
	__BinaryWriter_t2912249390::get_offset_of_m_nestedObjectCount_5(),
	__BinaryWriter_t2912249390::get_offset_of_nullCount_6(),
	__BinaryWriter_t2912249390::get_offset_of_binaryMethodCall_7(),
	__BinaryWriter_t2912249390::get_offset_of_binaryMethodReturn_8(),
	__BinaryWriter_t2912249390::get_offset_of_binaryObject_9(),
	__BinaryWriter_t2912249390::get_offset_of_binaryObjectWithMap_10(),
	__BinaryWriter_t2912249390::get_offset_of_binaryObjectWithMapTyped_11(),
	__BinaryWriter_t2912249390::get_offset_of_binaryObjectString_12(),
	__BinaryWriter_t2912249390::get_offset_of_binaryArray_13(),
	__BinaryWriter_t2912249390::get_offset_of_byteBuffer_14(),
	__BinaryWriter_t2912249390::get_offset_of_chunkSize_15(),
	__BinaryWriter_t2912249390::get_offset_of_memberPrimitiveUnTyped_16(),
	__BinaryWriter_t2912249390::get_offset_of_memberPrimitiveTyped_17(),
	__BinaryWriter_t2912249390::get_offset_of_objectNull_18(),
	__BinaryWriter_t2912249390::get_offset_of_memberReference_19(),
	__BinaryWriter_t2912249390::get_offset_of_binaryAssembly_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize572 = { sizeof (ObjectMapInfo_t2193277619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable572[4] = 
{
	ObjectMapInfo_t2193277619::get_offset_of_objectId_0(),
	ObjectMapInfo_t2193277619::get_offset_of_numMembers_1(),
	ObjectMapInfo_t2193277619::get_offset_of_memberNames_2(),
	ObjectMapInfo_t2193277619::get_offset_of_memberTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize573 = { sizeof (BinaryMethodCallMessage_t3709702807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable573[8] = 
{
	BinaryMethodCallMessage_t3709702807::get_offset_of__inargs_0(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__methodName_1(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__typeName_2(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__methodSignature_3(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__instArgs_4(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__args_5(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__logicalCallContext_6(),
	BinaryMethodCallMessage_t3709702807::get_offset_of__properties_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize574 = { sizeof (BinaryMethodReturnMessage_t3642132829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable574[6] = 
{
	BinaryMethodReturnMessage_t3642132829::get_offset_of__outargs_0(),
	BinaryMethodReturnMessage_t3642132829::get_offset_of__exception_1(),
	BinaryMethodReturnMessage_t3642132829::get_offset_of__returnValue_2(),
	BinaryMethodReturnMessage_t3642132829::get_offset_of__args_3(),
	BinaryMethodReturnMessage_t3642132829::get_offset_of__logicalCallContext_4(),
	BinaryMethodReturnMessage_t3642132829::get_offset_of__properties_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize575 = { sizeof (WriteObjectInfo_t2489189536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable575[17] = 
{
	WriteObjectInfo_t2489189536::get_offset_of_objectInfoId_0(),
	WriteObjectInfo_t2489189536::get_offset_of_obj_1(),
	WriteObjectInfo_t2489189536::get_offset_of_objectType_2(),
	WriteObjectInfo_t2489189536::get_offset_of_isSi_3(),
	WriteObjectInfo_t2489189536::get_offset_of_isNamed_4(),
	WriteObjectInfo_t2489189536::get_offset_of_isTyped_5(),
	WriteObjectInfo_t2489189536::get_offset_of_isArray_6(),
	WriteObjectInfo_t2489189536::get_offset_of_si_7(),
	WriteObjectInfo_t2489189536::get_offset_of_cache_8(),
	WriteObjectInfo_t2489189536::get_offset_of_memberData_9(),
	WriteObjectInfo_t2489189536::get_offset_of_serializationSurrogate_10(),
	WriteObjectInfo_t2489189536::get_offset_of_context_11(),
	WriteObjectInfo_t2489189536::get_offset_of_serObjectInfoInit_12(),
	WriteObjectInfo_t2489189536::get_offset_of_objectId_13(),
	WriteObjectInfo_t2489189536::get_offset_of_assemId_14(),
	WriteObjectInfo_t2489189536::get_offset_of_binderTypeName_15(),
	WriteObjectInfo_t2489189536::get_offset_of_binderAssemblyString_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize576 = { sizeof (ReadObjectInfo_t1645661573), -1, sizeof(ReadObjectInfo_t1645661573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable576[18] = 
{
	ReadObjectInfo_t1645661573::get_offset_of_objectInfoId_0(),
	ReadObjectInfo_t1645661573_StaticFields::get_offset_of_readObjectInfoCounter_1(),
	ReadObjectInfo_t1645661573::get_offset_of_objectType_2(),
	ReadObjectInfo_t1645661573::get_offset_of_objectManager_3(),
	ReadObjectInfo_t1645661573::get_offset_of_count_4(),
	ReadObjectInfo_t1645661573::get_offset_of_isSi_5(),
	ReadObjectInfo_t1645661573::get_offset_of_isNamed_6(),
	ReadObjectInfo_t1645661573::get_offset_of_isTyped_7(),
	ReadObjectInfo_t1645661573::get_offset_of_bSimpleAssembly_8(),
	ReadObjectInfo_t1645661573::get_offset_of_cache_9(),
	ReadObjectInfo_t1645661573::get_offset_of_wireMemberNames_10(),
	ReadObjectInfo_t1645661573::get_offset_of_wireMemberTypes_11(),
	ReadObjectInfo_t1645661573::get_offset_of_lastPosition_12(),
	ReadObjectInfo_t1645661573::get_offset_of_serializationSurrogate_13(),
	ReadObjectInfo_t1645661573::get_offset_of_context_14(),
	ReadObjectInfo_t1645661573::get_offset_of_memberTypesList_15(),
	ReadObjectInfo_t1645661573::get_offset_of_serObjectInfoInit_16(),
	ReadObjectInfo_t1645661573::get_offset_of_formatterConverter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize577 = { sizeof (SerObjectInfoInit_t4094458531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable577[3] = 
{
	SerObjectInfoInit_t4094458531::get_offset_of_seenBeforeTable_0(),
	SerObjectInfoInit_t4094458531::get_offset_of_objectInfoIdCount_1(),
	SerObjectInfoInit_t4094458531::get_offset_of_oiPool_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize578 = { sizeof (SerObjectInfoCache_t4068137215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable578[6] = 
{
	SerObjectInfoCache_t4068137215::get_offset_of_fullTypeName_0(),
	SerObjectInfoCache_t4068137215::get_offset_of_assemblyString_1(),
	SerObjectInfoCache_t4068137215::get_offset_of_hasTypeForwardedFrom_2(),
	SerObjectInfoCache_t4068137215::get_offset_of_memberInfos_3(),
	SerObjectInfoCache_t4068137215::get_offset_of_memberNames_4(),
	SerObjectInfoCache_t4068137215::get_offset_of_memberTypes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize579 = { sizeof (TypeInformation_t3804990478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable579[3] = 
{
	TypeInformation_t3804990478::get_offset_of_fullTypeName_0(),
	TypeInformation_t3804990478::get_offset_of_assemblyString_1(),
	TypeInformation_t3804990478::get_offset_of_hasTypeForwardedFrom_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize580 = { sizeof (ObjectReader_t1476095226), -1, sizeof(ObjectReader_t1476095226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable580[30] = 
{
	ObjectReader_t1476095226::get_offset_of_m_stream_0(),
	ObjectReader_t1476095226::get_offset_of_m_surrogates_1(),
	ObjectReader_t1476095226::get_offset_of_m_context_2(),
	ObjectReader_t1476095226::get_offset_of_m_objectManager_3(),
	ObjectReader_t1476095226::get_offset_of_formatterEnums_4(),
	ObjectReader_t1476095226::get_offset_of_m_binder_5(),
	ObjectReader_t1476095226::get_offset_of_topId_6(),
	ObjectReader_t1476095226::get_offset_of_bSimpleAssembly_7(),
	ObjectReader_t1476095226::get_offset_of_handlerObject_8(),
	ObjectReader_t1476095226::get_offset_of_m_topObject_9(),
	ObjectReader_t1476095226::get_offset_of_headers_10(),
	ObjectReader_t1476095226::get_offset_of_handler_11(),
	ObjectReader_t1476095226::get_offset_of_serObjectInfoInit_12(),
	ObjectReader_t1476095226::get_offset_of_m_formatterConverter_13(),
	ObjectReader_t1476095226::get_offset_of_stack_14(),
	ObjectReader_t1476095226::get_offset_of_valueFixupStack_15(),
	ObjectReader_t1476095226::get_offset_of_crossAppDomainArray_16(),
	ObjectReader_t1476095226::get_offset_of_bFullDeserialization_17(),
	ObjectReader_t1476095226::get_offset_of_bMethodCall_18(),
	ObjectReader_t1476095226::get_offset_of_bMethodReturn_19(),
	ObjectReader_t1476095226::get_offset_of_binaryMethodCall_20(),
	ObjectReader_t1476095226::get_offset_of_binaryMethodReturn_21(),
	ObjectReader_t1476095226::get_offset_of_bIsCrossAppDomain_22(),
	ObjectReader_t1476095226::get_offset_of_bOldFormatDetected_23(),
	ObjectReader_t1476095226::get_offset_of_valTypeObjectIdTable_24(),
	ObjectReader_t1476095226::get_offset_of_typeCache_25(),
	ObjectReader_t1476095226::get_offset_of_previousAssemblyString_26(),
	ObjectReader_t1476095226::get_offset_of_previousName_27(),
	ObjectReader_t1476095226::get_offset_of_previousType_28(),
	ObjectReader_t1476095226_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize581 = { sizeof (TypeNAssembly_t2965287612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable581[2] = 
{
	TypeNAssembly_t2965287612::get_offset_of_type_0(),
	TypeNAssembly_t2965287612::get_offset_of_assemblyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize582 = { sizeof (TopLevelAssemblyTypeResolver_t3366789405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable582[1] = 
{
	TopLevelAssemblyTypeResolver_t3366789405::get_offset_of_m_topLevelAssembly_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize583 = { sizeof (ObjectWriter_t4293742132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable583[21] = 
{
	ObjectWriter_t4293742132::get_offset_of_m_objectQueue_0(),
	ObjectWriter_t4293742132::get_offset_of_m_idGenerator_1(),
	ObjectWriter_t4293742132::get_offset_of_m_currentId_2(),
	ObjectWriter_t4293742132::get_offset_of_m_surrogates_3(),
	ObjectWriter_t4293742132::get_offset_of_m_context_4(),
	ObjectWriter_t4293742132::get_offset_of_serWriter_5(),
	ObjectWriter_t4293742132::get_offset_of_m_objectManager_6(),
	ObjectWriter_t4293742132::get_offset_of_topId_7(),
	ObjectWriter_t4293742132::get_offset_of_topName_8(),
	ObjectWriter_t4293742132::get_offset_of_headers_9(),
	ObjectWriter_t4293742132::get_offset_of_formatterEnums_10(),
	ObjectWriter_t4293742132::get_offset_of_m_binder_11(),
	ObjectWriter_t4293742132::get_offset_of_serObjectInfoInit_12(),
	ObjectWriter_t4293742132::get_offset_of_m_formatterConverter_13(),
	ObjectWriter_t4293742132::get_offset_of_crossAppDomainArray_14(),
	ObjectWriter_t4293742132::get_offset_of_previousObj_15(),
	ObjectWriter_t4293742132::get_offset_of_previousId_16(),
	ObjectWriter_t4293742132::get_offset_of_previousType_17(),
	ObjectWriter_t4293742132::get_offset_of_previousCode_18(),
	ObjectWriter_t4293742132::get_offset_of_assemblyToIdTable_19(),
	ObjectWriter_t4293742132::get_offset_of_niPool_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize584 = { sizeof (__BinaryParser_t1222443990), -1, sizeof(__BinaryParser_t1222443990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable584[25] = 
{
	__BinaryParser_t1222443990::get_offset_of_objectReader_0(),
	__BinaryParser_t1222443990::get_offset_of_input_1(),
	__BinaryParser_t1222443990::get_offset_of_topId_2(),
	__BinaryParser_t1222443990::get_offset_of_headerId_3(),
	__BinaryParser_t1222443990::get_offset_of_objectMapIdTable_4(),
	__BinaryParser_t1222443990::get_offset_of_assemIdToAssemblyTable_5(),
	__BinaryParser_t1222443990::get_offset_of_stack_6(),
	__BinaryParser_t1222443990::get_offset_of_expectedType_7(),
	__BinaryParser_t1222443990::get_offset_of_expectedTypeInformation_8(),
	__BinaryParser_t1222443990::get_offset_of_PRS_9(),
	__BinaryParser_t1222443990::get_offset_of_systemAssemblyInfo_10(),
	__BinaryParser_t1222443990::get_offset_of_dataReader_11(),
	__BinaryParser_t1222443990_StaticFields::get_offset_of_encoding_12(),
	__BinaryParser_t1222443990::get_offset_of_opPool_13(),
	__BinaryParser_t1222443990::get_offset_of_binaryObject_14(),
	__BinaryParser_t1222443990::get_offset_of_bowm_15(),
	__BinaryParser_t1222443990::get_offset_of_bowmt_16(),
	__BinaryParser_t1222443990::get_offset_of_objectString_17(),
	__BinaryParser_t1222443990::get_offset_of_crossAppDomainString_18(),
	__BinaryParser_t1222443990::get_offset_of_memberPrimitiveTyped_19(),
	__BinaryParser_t1222443990::get_offset_of_byteBuffer_20(),
	__BinaryParser_t1222443990::get_offset_of_memberPrimitiveUnTyped_21(),
	__BinaryParser_t1222443990::get_offset_of_memberReference_22(),
	__BinaryParser_t1222443990::get_offset_of_objectNull_23(),
	__BinaryParser_t1222443990_StaticFields::get_offset_of_messageEnd_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize585 = { sizeof (ParseRecord_t2674254118), -1, sizeof(ParseRecord_t2674254118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable585[41] = 
{
	ParseRecord_t2674254118_StaticFields::get_offset_of_parseRecordIdCount_0(),
	ParseRecord_t2674254118::get_offset_of_PRparseTypeEnum_1(),
	ParseRecord_t2674254118::get_offset_of_PRobjectTypeEnum_2(),
	ParseRecord_t2674254118::get_offset_of_PRarrayTypeEnum_3(),
	ParseRecord_t2674254118::get_offset_of_PRmemberTypeEnum_4(),
	ParseRecord_t2674254118::get_offset_of_PRmemberValueEnum_5(),
	ParseRecord_t2674254118::get_offset_of_PRobjectPositionEnum_6(),
	ParseRecord_t2674254118::get_offset_of_PRname_7(),
	ParseRecord_t2674254118::get_offset_of_PRvalue_8(),
	ParseRecord_t2674254118::get_offset_of_PRvarValue_9(),
	ParseRecord_t2674254118::get_offset_of_PRkeyDt_10(),
	ParseRecord_t2674254118::get_offset_of_PRdtType_11(),
	ParseRecord_t2674254118::get_offset_of_PRdtTypeCode_12(),
	ParseRecord_t2674254118::get_offset_of_PRisEnum_13(),
	ParseRecord_t2674254118::get_offset_of_PRobjectId_14(),
	ParseRecord_t2674254118::get_offset_of_PRidRef_15(),
	ParseRecord_t2674254118::get_offset_of_PRarrayElementTypeString_16(),
	ParseRecord_t2674254118::get_offset_of_PRarrayElementType_17(),
	ParseRecord_t2674254118::get_offset_of_PRisArrayVariant_18(),
	ParseRecord_t2674254118::get_offset_of_PRarrayElementTypeCode_19(),
	ParseRecord_t2674254118::get_offset_of_PRrank_20(),
	ParseRecord_t2674254118::get_offset_of_PRlengthA_21(),
	ParseRecord_t2674254118::get_offset_of_PRpositionA_22(),
	ParseRecord_t2674254118::get_offset_of_PRlowerBoundA_23(),
	ParseRecord_t2674254118::get_offset_of_PRupperBoundA_24(),
	ParseRecord_t2674254118::get_offset_of_PRindexMap_25(),
	ParseRecord_t2674254118::get_offset_of_PRmemberIndex_26(),
	ParseRecord_t2674254118::get_offset_of_PRlinearlength_27(),
	ParseRecord_t2674254118::get_offset_of_PRrectangularMap_28(),
	ParseRecord_t2674254118::get_offset_of_PRisLowerBound_29(),
	ParseRecord_t2674254118::get_offset_of_PRtopId_30(),
	ParseRecord_t2674254118::get_offset_of_PRheaderId_31(),
	ParseRecord_t2674254118::get_offset_of_PRobjectInfo_32(),
	ParseRecord_t2674254118::get_offset_of_PRisValueTypeFixup_33(),
	ParseRecord_t2674254118::get_offset_of_PRnewObj_34(),
	ParseRecord_t2674254118::get_offset_of_PRobjectA_35(),
	ParseRecord_t2674254118::get_offset_of_PRprimitiveArray_36(),
	ParseRecord_t2674254118::get_offset_of_PRisRegistered_37(),
	ParseRecord_t2674254118::get_offset_of_PRmemberData_38(),
	ParseRecord_t2674254118::get_offset_of_PRsi_39(),
	ParseRecord_t2674254118::get_offset_of_PRnullCount_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize586 = { sizeof (SerStack_t3886188184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable586[3] = 
{
	SerStack_t3886188184::get_offset_of_objects_0(),
	SerStack_t3886188184::get_offset_of_stackId_1(),
	SerStack_t3886188184::get_offset_of_top_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize587 = { sizeof (SizedArray_t2151058854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable587[2] = 
{
	SizedArray_t2151058854::get_offset_of_objects_0(),
	SizedArray_t2151058854::get_offset_of_negObjects_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize588 = { sizeof (IntSizedArray_t2017939501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable588[2] = 
{
	IntSizedArray_t2017939501::get_offset_of_objects_0(),
	IntSizedArray_t2017939501::get_offset_of_negObjects_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize589 = { sizeof (NameCache_t3722972407), -1, sizeof(NameCache_t3722972407_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable589[2] = 
{
	NameCache_t3722972407_StaticFields::get_offset_of_ht_0(),
	NameCache_t3722972407::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize590 = { sizeof (ValueFixup_t1348745115), -1, sizeof(ValueFixup_t1348745115_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable590[8] = 
{
	ValueFixup_t1348745115::get_offset_of_valueFixupEnum_0(),
	ValueFixup_t1348745115::get_offset_of_arrayObj_1(),
	ValueFixup_t1348745115::get_offset_of_indexMap_2(),
	ValueFixup_t1348745115::get_offset_of_header_3(),
	ValueFixup_t1348745115::get_offset_of_memberObject_4(),
	ValueFixup_t1348745115_StaticFields::get_offset_of_valueInfo_5(),
	ValueFixup_t1348745115::get_offset_of_objectInfo_6(),
	ValueFixup_t1348745115::get_offset_of_memberName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize591 = { sizeof (InternalFE_t3355145566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable591[4] = 
{
	InternalFE_t3355145566::get_offset_of_FEtypeFormat_0(),
	InternalFE_t3355145566::get_offset_of_FEassemblyFormat_1(),
	InternalFE_t3355145566::get_offset_of_FEsecurityLevel_2(),
	InternalFE_t3355145566::get_offset_of_FEserializerTypeEnum_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize592 = { sizeof (NameInfo_t2407316929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable592[13] = 
{
	NameInfo_t2407316929::get_offset_of_NIFullName_0(),
	NameInfo_t2407316929::get_offset_of_NIobjectId_1(),
	NameInfo_t2407316929::get_offset_of_NIassemId_2(),
	NameInfo_t2407316929::get_offset_of_NIprimitiveTypeEnum_3(),
	NameInfo_t2407316929::get_offset_of_NItype_4(),
	NameInfo_t2407316929::get_offset_of_NIisSealed_5(),
	NameInfo_t2407316929::get_offset_of_NIisArray_6(),
	NameInfo_t2407316929::get_offset_of_NIisArrayItem_7(),
	NameInfo_t2407316929::get_offset_of_NItransmitTypeOnObject_8(),
	NameInfo_t2407316929::get_offset_of_NItransmitTypeOnMember_9(),
	NameInfo_t2407316929::get_offset_of_NIisParentTypeOnObject_10(),
	NameInfo_t2407316929::get_offset_of_NIarrayEnum_11(),
	NameInfo_t2407316929::get_offset_of_NIsealedStatusChecked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize593 = { sizeof (PrimitiveArray_t3608773834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable593[12] = 
{
	PrimitiveArray_t3608773834::get_offset_of_code_0(),
	PrimitiveArray_t3608773834::get_offset_of_booleanA_1(),
	PrimitiveArray_t3608773834::get_offset_of_charA_2(),
	PrimitiveArray_t3608773834::get_offset_of_doubleA_3(),
	PrimitiveArray_t3608773834::get_offset_of_int16A_4(),
	PrimitiveArray_t3608773834::get_offset_of_int32A_5(),
	PrimitiveArray_t3608773834::get_offset_of_int64A_6(),
	PrimitiveArray_t3608773834::get_offset_of_sbyteA_7(),
	PrimitiveArray_t3608773834::get_offset_of_singleA_8(),
	PrimitiveArray_t3608773834::get_offset_of_uint16A_9(),
	PrimitiveArray_t3608773834::get_offset_of_uint32A_10(),
	PrimitiveArray_t3608773834::get_offset_of_uint64A_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize594 = { sizeof (FormatterTypeStyle_t943306207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable594[4] = 
{
	FormatterTypeStyle_t943306207::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize595 = { sizeof (FormatterAssemblyStyle_t999493661)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable595[3] = 
{
	FormatterAssemblyStyle_t999493661::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize596 = { sizeof (TypeFilterLevel_t1182459634)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable596[3] = 
{
	TypeFilterLevel_t1182459634::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize597 = { sizeof (FormatterServices_t3161112612), -1, sizeof(FormatterServices_t3161112612_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable597[6] = 
{
	FormatterServices_t3161112612_StaticFields::get_offset_of_m_MemberInfoTable_0(),
	FormatterServices_t3161112612_StaticFields::get_offset_of_unsafeTypeForwardersIsEnabled_1(),
	FormatterServices_t3161112612_StaticFields::get_offset_of_unsafeTypeForwardersIsEnabledInitialized_2(),
	FormatterServices_t3161112612_StaticFields::get_offset_of_s_FormatterServicesSyncObject_3(),
	FormatterServices_t3161112612_StaticFields::get_offset_of_advancedTypes_4(),
	FormatterServices_t3161112612_StaticFields::get_offset_of_s_binder_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize598 = { sizeof (SurrogateForCyclicalReference_t2955664662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable598[1] = 
{
	SurrogateForCyclicalReference_t2955664662::get_offset_of_innerSurrogate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize599 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
