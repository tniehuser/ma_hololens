﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t1795361321;
// System.Action`1<System.Object>
struct Action_1_t2491248677;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationToken
struct  CancellationToken_t1851405782 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t1795361321 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1851405782, ___m_source_0)); }
	inline CancellationTokenSource_t1795361321 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t1795361321 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t1795361321 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_source_0, value);
	}
};

struct CancellationToken_t1851405782_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_t2491248677 * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1851405782_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_t2491248677 * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_t2491248677 ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_t2491248677 * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_ActionToActionObjShunt_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t1851405782_marshaled_pinvoke
{
	CancellationTokenSource_t1795361321 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t1851405782_marshaled_com
{
	CancellationTokenSource_t1795361321 * ___m_source_0;
};
