﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Globalization_Calendar585061108.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Globalization.EraInfo[]
struct EraInfoU5BU5D_t1865950449;
// System.Globalization.Calendar
struct Calendar_t585061108;
// System.Globalization.GregorianCalendarHelper
struct GregorianCalendarHelper_t3151146692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TaiwanCalendar
struct  TaiwanCalendar_t2121259800  : public Calendar_t585061108
{
public:
	// System.Globalization.GregorianCalendarHelper System.Globalization.TaiwanCalendar::helper
	GregorianCalendarHelper_t3151146692 * ___helper_5;

public:
	inline static int32_t get_offset_of_helper_5() { return static_cast<int32_t>(offsetof(TaiwanCalendar_t2121259800, ___helper_5)); }
	inline GregorianCalendarHelper_t3151146692 * get_helper_5() const { return ___helper_5; }
	inline GregorianCalendarHelper_t3151146692 ** get_address_of_helper_5() { return &___helper_5; }
	inline void set_helper_5(GregorianCalendarHelper_t3151146692 * value)
	{
		___helper_5 = value;
		Il2CppCodeGenWriteBarrier(&___helper_5, value);
	}
};

struct TaiwanCalendar_t2121259800_StaticFields
{
public:
	// System.Globalization.EraInfo[] System.Globalization.TaiwanCalendar::taiwanEraInfo
	EraInfoU5BU5D_t1865950449* ___taiwanEraInfo_3;
	// System.Globalization.Calendar modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.TaiwanCalendar::s_defaultInstance
	Calendar_t585061108 * ___s_defaultInstance_4;
	// System.DateTime System.Globalization.TaiwanCalendar::calendarMinValue
	DateTime_t693205669  ___calendarMinValue_6;

public:
	inline static int32_t get_offset_of_taiwanEraInfo_3() { return static_cast<int32_t>(offsetof(TaiwanCalendar_t2121259800_StaticFields, ___taiwanEraInfo_3)); }
	inline EraInfoU5BU5D_t1865950449* get_taiwanEraInfo_3() const { return ___taiwanEraInfo_3; }
	inline EraInfoU5BU5D_t1865950449** get_address_of_taiwanEraInfo_3() { return &___taiwanEraInfo_3; }
	inline void set_taiwanEraInfo_3(EraInfoU5BU5D_t1865950449* value)
	{
		___taiwanEraInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___taiwanEraInfo_3, value);
	}

	inline static int32_t get_offset_of_s_defaultInstance_4() { return static_cast<int32_t>(offsetof(TaiwanCalendar_t2121259800_StaticFields, ___s_defaultInstance_4)); }
	inline Calendar_t585061108 * get_s_defaultInstance_4() const { return ___s_defaultInstance_4; }
	inline Calendar_t585061108 ** get_address_of_s_defaultInstance_4() { return &___s_defaultInstance_4; }
	inline void set_s_defaultInstance_4(Calendar_t585061108 * value)
	{
		___s_defaultInstance_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_defaultInstance_4, value);
	}

	inline static int32_t get_offset_of_calendarMinValue_6() { return static_cast<int32_t>(offsetof(TaiwanCalendar_t2121259800_StaticFields, ___calendarMinValue_6)); }
	inline DateTime_t693205669  get_calendarMinValue_6() const { return ___calendarMinValue_6; }
	inline DateTime_t693205669 * get_address_of_calendarMinValue_6() { return &___calendarMinValue_6; }
	inline void set_calendarMinValue_6(DateTime_t693205669  value)
	{
		___calendarMinValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
