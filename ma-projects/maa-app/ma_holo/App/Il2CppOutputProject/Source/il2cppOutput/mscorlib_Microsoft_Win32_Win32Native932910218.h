﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Version
struct Version_t1755874712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.Win32Native
struct  Win32Native_t932910218  : public Il2CppObject
{
public:

public:
};

struct Win32Native_t932910218_StaticFields
{
public:
	// System.IntPtr Microsoft.Win32.Win32Native::INVALID_HANDLE_VALUE
	IntPtr_t ___INVALID_HANDLE_VALUE_0;
	// System.Version Microsoft.Win32.Win32Native::ThreadErrorModeMinOsVersion
	Version_t1755874712 * ___ThreadErrorModeMinOsVersion_1;

public:
	inline static int32_t get_offset_of_INVALID_HANDLE_VALUE_0() { return static_cast<int32_t>(offsetof(Win32Native_t932910218_StaticFields, ___INVALID_HANDLE_VALUE_0)); }
	inline IntPtr_t get_INVALID_HANDLE_VALUE_0() const { return ___INVALID_HANDLE_VALUE_0; }
	inline IntPtr_t* get_address_of_INVALID_HANDLE_VALUE_0() { return &___INVALID_HANDLE_VALUE_0; }
	inline void set_INVALID_HANDLE_VALUE_0(IntPtr_t value)
	{
		___INVALID_HANDLE_VALUE_0 = value;
	}

	inline static int32_t get_offset_of_ThreadErrorModeMinOsVersion_1() { return static_cast<int32_t>(offsetof(Win32Native_t932910218_StaticFields, ___ThreadErrorModeMinOsVersion_1)); }
	inline Version_t1755874712 * get_ThreadErrorModeMinOsVersion_1() const { return ___ThreadErrorModeMinOsVersion_1; }
	inline Version_t1755874712 ** get_address_of_ThreadErrorModeMinOsVersion_1() { return &___ThreadErrorModeMinOsVersion_1; }
	inline void set_ThreadErrorModeMinOsVersion_1(Version_t1755874712 * value)
	{
		___ThreadErrorModeMinOsVersion_1 = value;
		Il2CppCodeGenWriteBarrier(&___ThreadErrorModeMinOsVersion_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
