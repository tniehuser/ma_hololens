﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Collections.Generic.Dictionary`2<System.Runtime.Serialization.MemberHolder,System.Reflection.MemberInfo[]>
struct Dictionary_2_t2672672302;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.Binder
struct Binder_t3404612058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FormatterServices
struct  FormatterServices_t3161112612  : public Il2CppObject
{
public:

public:
};

struct FormatterServices_t3161112612_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Runtime.Serialization.MemberHolder,System.Reflection.MemberInfo[]> System.Runtime.Serialization.FormatterServices::m_MemberInfoTable
	Dictionary_2_t2672672302 * ___m_MemberInfoTable_0;
	// System.Boolean System.Runtime.Serialization.FormatterServices::unsafeTypeForwardersIsEnabled
	bool ___unsafeTypeForwardersIsEnabled_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.FormatterServices::unsafeTypeForwardersIsEnabledInitialized
	bool ___unsafeTypeForwardersIsEnabledInitialized_2;
	// System.Object System.Runtime.Serialization.FormatterServices::s_FormatterServicesSyncObject
	Il2CppObject * ___s_FormatterServicesSyncObject_3;
	// System.Type[] System.Runtime.Serialization.FormatterServices::advancedTypes
	TypeU5BU5D_t1664964607* ___advancedTypes_4;
	// System.Reflection.Binder System.Runtime.Serialization.FormatterServices::s_binder
	Binder_t3404612058 * ___s_binder_5;

public:
	inline static int32_t get_offset_of_m_MemberInfoTable_0() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___m_MemberInfoTable_0)); }
	inline Dictionary_2_t2672672302 * get_m_MemberInfoTable_0() const { return ___m_MemberInfoTable_0; }
	inline Dictionary_2_t2672672302 ** get_address_of_m_MemberInfoTable_0() { return &___m_MemberInfoTable_0; }
	inline void set_m_MemberInfoTable_0(Dictionary_2_t2672672302 * value)
	{
		___m_MemberInfoTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_MemberInfoTable_0, value);
	}

	inline static int32_t get_offset_of_unsafeTypeForwardersIsEnabled_1() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___unsafeTypeForwardersIsEnabled_1)); }
	inline bool get_unsafeTypeForwardersIsEnabled_1() const { return ___unsafeTypeForwardersIsEnabled_1; }
	inline bool* get_address_of_unsafeTypeForwardersIsEnabled_1() { return &___unsafeTypeForwardersIsEnabled_1; }
	inline void set_unsafeTypeForwardersIsEnabled_1(bool value)
	{
		___unsafeTypeForwardersIsEnabled_1 = value;
	}

	inline static int32_t get_offset_of_unsafeTypeForwardersIsEnabledInitialized_2() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___unsafeTypeForwardersIsEnabledInitialized_2)); }
	inline bool get_unsafeTypeForwardersIsEnabledInitialized_2() const { return ___unsafeTypeForwardersIsEnabledInitialized_2; }
	inline bool* get_address_of_unsafeTypeForwardersIsEnabledInitialized_2() { return &___unsafeTypeForwardersIsEnabledInitialized_2; }
	inline void set_unsafeTypeForwardersIsEnabledInitialized_2(bool value)
	{
		___unsafeTypeForwardersIsEnabledInitialized_2 = value;
	}

	inline static int32_t get_offset_of_s_FormatterServicesSyncObject_3() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___s_FormatterServicesSyncObject_3)); }
	inline Il2CppObject * get_s_FormatterServicesSyncObject_3() const { return ___s_FormatterServicesSyncObject_3; }
	inline Il2CppObject ** get_address_of_s_FormatterServicesSyncObject_3() { return &___s_FormatterServicesSyncObject_3; }
	inline void set_s_FormatterServicesSyncObject_3(Il2CppObject * value)
	{
		___s_FormatterServicesSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_FormatterServicesSyncObject_3, value);
	}

	inline static int32_t get_offset_of_advancedTypes_4() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___advancedTypes_4)); }
	inline TypeU5BU5D_t1664964607* get_advancedTypes_4() const { return ___advancedTypes_4; }
	inline TypeU5BU5D_t1664964607** get_address_of_advancedTypes_4() { return &___advancedTypes_4; }
	inline void set_advancedTypes_4(TypeU5BU5D_t1664964607* value)
	{
		___advancedTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___advancedTypes_4, value);
	}

	inline static int32_t get_offset_of_s_binder_5() { return static_cast<int32_t>(offsetof(FormatterServices_t3161112612_StaticFields, ___s_binder_5)); }
	inline Binder_t3404612058 * get_s_binder_5() const { return ___s_binder_5; }
	inline Binder_t3404612058 ** get_address_of_s_binder_5() { return &___s_binder_5; }
	inline void set_s_binder_5(Binder_t3404612058 * value)
	{
		___s_binder_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_binder_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
