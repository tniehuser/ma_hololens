﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_DecoderFallback1715117820.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.UTF7Encoding/DecoderUTF7Fallback
struct  DecoderUTF7Fallback_t3319501624  : public DecoderFallback_t1715117820
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
