﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpace3746245107.h"
#include "System_Xml_System_Xml_Schema_RestrictionFlags2588355947.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFacets
struct  RestrictionFacets_t4012658256  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFacets::Length
	int32_t ___Length_0;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MinLength
	int32_t ___MinLength_1;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MaxLength
	int32_t ___MaxLength_2;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Patterns
	ArrayList_t4252133567 * ___Patterns_3;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Enumeration
	ArrayList_t4252133567 * ___Enumeration_4;
	// System.Xml.Schema.XmlSchemaWhiteSpace System.Xml.Schema.RestrictionFacets::WhiteSpace
	int32_t ___WhiteSpace_5;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxInclusive
	Il2CppObject * ___MaxInclusive_6;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxExclusive
	Il2CppObject * ___MaxExclusive_7;
	// System.Object System.Xml.Schema.RestrictionFacets::MinInclusive
	Il2CppObject * ___MinInclusive_8;
	// System.Object System.Xml.Schema.RestrictionFacets::MinExclusive
	Il2CppObject * ___MinExclusive_9;
	// System.Int32 System.Xml.Schema.RestrictionFacets::TotalDigits
	int32_t ___TotalDigits_10;
	// System.Int32 System.Xml.Schema.RestrictionFacets::FractionDigits
	int32_t ___FractionDigits_11;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::Flags
	int32_t ___Flags_12;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::FixedFlags
	int32_t ___FixedFlags_13;

public:
	inline static int32_t get_offset_of_Length_0() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Length_0)); }
	inline int32_t get_Length_0() const { return ___Length_0; }
	inline int32_t* get_address_of_Length_0() { return &___Length_0; }
	inline void set_Length_0(int32_t value)
	{
		___Length_0 = value;
	}

	inline static int32_t get_offset_of_MinLength_1() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinLength_1)); }
	inline int32_t get_MinLength_1() const { return ___MinLength_1; }
	inline int32_t* get_address_of_MinLength_1() { return &___MinLength_1; }
	inline void set_MinLength_1(int32_t value)
	{
		___MinLength_1 = value;
	}

	inline static int32_t get_offset_of_MaxLength_2() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxLength_2)); }
	inline int32_t get_MaxLength_2() const { return ___MaxLength_2; }
	inline int32_t* get_address_of_MaxLength_2() { return &___MaxLength_2; }
	inline void set_MaxLength_2(int32_t value)
	{
		___MaxLength_2 = value;
	}

	inline static int32_t get_offset_of_Patterns_3() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Patterns_3)); }
	inline ArrayList_t4252133567 * get_Patterns_3() const { return ___Patterns_3; }
	inline ArrayList_t4252133567 ** get_address_of_Patterns_3() { return &___Patterns_3; }
	inline void set_Patterns_3(ArrayList_t4252133567 * value)
	{
		___Patterns_3 = value;
		Il2CppCodeGenWriteBarrier(&___Patterns_3, value);
	}

	inline static int32_t get_offset_of_Enumeration_4() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Enumeration_4)); }
	inline ArrayList_t4252133567 * get_Enumeration_4() const { return ___Enumeration_4; }
	inline ArrayList_t4252133567 ** get_address_of_Enumeration_4() { return &___Enumeration_4; }
	inline void set_Enumeration_4(ArrayList_t4252133567 * value)
	{
		___Enumeration_4 = value;
		Il2CppCodeGenWriteBarrier(&___Enumeration_4, value);
	}

	inline static int32_t get_offset_of_WhiteSpace_5() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___WhiteSpace_5)); }
	inline int32_t get_WhiteSpace_5() const { return ___WhiteSpace_5; }
	inline int32_t* get_address_of_WhiteSpace_5() { return &___WhiteSpace_5; }
	inline void set_WhiteSpace_5(int32_t value)
	{
		___WhiteSpace_5 = value;
	}

	inline static int32_t get_offset_of_MaxInclusive_6() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxInclusive_6)); }
	inline Il2CppObject * get_MaxInclusive_6() const { return ___MaxInclusive_6; }
	inline Il2CppObject ** get_address_of_MaxInclusive_6() { return &___MaxInclusive_6; }
	inline void set_MaxInclusive_6(Il2CppObject * value)
	{
		___MaxInclusive_6 = value;
		Il2CppCodeGenWriteBarrier(&___MaxInclusive_6, value);
	}

	inline static int32_t get_offset_of_MaxExclusive_7() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxExclusive_7)); }
	inline Il2CppObject * get_MaxExclusive_7() const { return ___MaxExclusive_7; }
	inline Il2CppObject ** get_address_of_MaxExclusive_7() { return &___MaxExclusive_7; }
	inline void set_MaxExclusive_7(Il2CppObject * value)
	{
		___MaxExclusive_7 = value;
		Il2CppCodeGenWriteBarrier(&___MaxExclusive_7, value);
	}

	inline static int32_t get_offset_of_MinInclusive_8() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinInclusive_8)); }
	inline Il2CppObject * get_MinInclusive_8() const { return ___MinInclusive_8; }
	inline Il2CppObject ** get_address_of_MinInclusive_8() { return &___MinInclusive_8; }
	inline void set_MinInclusive_8(Il2CppObject * value)
	{
		___MinInclusive_8 = value;
		Il2CppCodeGenWriteBarrier(&___MinInclusive_8, value);
	}

	inline static int32_t get_offset_of_MinExclusive_9() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinExclusive_9)); }
	inline Il2CppObject * get_MinExclusive_9() const { return ___MinExclusive_9; }
	inline Il2CppObject ** get_address_of_MinExclusive_9() { return &___MinExclusive_9; }
	inline void set_MinExclusive_9(Il2CppObject * value)
	{
		___MinExclusive_9 = value;
		Il2CppCodeGenWriteBarrier(&___MinExclusive_9, value);
	}

	inline static int32_t get_offset_of_TotalDigits_10() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___TotalDigits_10)); }
	inline int32_t get_TotalDigits_10() const { return ___TotalDigits_10; }
	inline int32_t* get_address_of_TotalDigits_10() { return &___TotalDigits_10; }
	inline void set_TotalDigits_10(int32_t value)
	{
		___TotalDigits_10 = value;
	}

	inline static int32_t get_offset_of_FractionDigits_11() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___FractionDigits_11)); }
	inline int32_t get_FractionDigits_11() const { return ___FractionDigits_11; }
	inline int32_t* get_address_of_FractionDigits_11() { return &___FractionDigits_11; }
	inline void set_FractionDigits_11(int32_t value)
	{
		___FractionDigits_11 = value;
	}

	inline static int32_t get_offset_of_Flags_12() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Flags_12)); }
	inline int32_t get_Flags_12() const { return ___Flags_12; }
	inline int32_t* get_address_of_Flags_12() { return &___Flags_12; }
	inline void set_Flags_12(int32_t value)
	{
		___Flags_12 = value;
	}

	inline static int32_t get_offset_of_FixedFlags_13() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___FixedFlags_13)); }
	inline int32_t get_FixedFlags_13() const { return ___FixedFlags_13; }
	inline int32_t* get_address_of_FixedFlags_13() { return &___FixedFlags_13; }
	inline void set_FixedFlags_13(int32_t value)
	{
		___FixedFlags_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
