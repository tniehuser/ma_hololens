﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlBaseConverter217670330.h"

// System.Xml.Schema.XmlValueConverter[]
struct XmlValueConverterU5BU5D_t583536453;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlUnionConverter
struct  XmlUnionConverter_t2065897640  : public XmlBaseConverter_t217670330
{
public:
	// System.Xml.Schema.XmlValueConverter[] System.Xml.Schema.XmlUnionConverter::converters
	XmlValueConverterU5BU5D_t583536453* ___converters_32;
	// System.Boolean System.Xml.Schema.XmlUnionConverter::hasAtomicMember
	bool ___hasAtomicMember_33;
	// System.Boolean System.Xml.Schema.XmlUnionConverter::hasListMember
	bool ___hasListMember_34;

public:
	inline static int32_t get_offset_of_converters_32() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t2065897640, ___converters_32)); }
	inline XmlValueConverterU5BU5D_t583536453* get_converters_32() const { return ___converters_32; }
	inline XmlValueConverterU5BU5D_t583536453** get_address_of_converters_32() { return &___converters_32; }
	inline void set_converters_32(XmlValueConverterU5BU5D_t583536453* value)
	{
		___converters_32 = value;
		Il2CppCodeGenWriteBarrier(&___converters_32, value);
	}

	inline static int32_t get_offset_of_hasAtomicMember_33() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t2065897640, ___hasAtomicMember_33)); }
	inline bool get_hasAtomicMember_33() const { return ___hasAtomicMember_33; }
	inline bool* get_address_of_hasAtomicMember_33() { return &___hasAtomicMember_33; }
	inline void set_hasAtomicMember_33(bool value)
	{
		___hasAtomicMember_33 = value;
	}

	inline static int32_t get_offset_of_hasListMember_34() { return static_cast<int32_t>(offsetof(XmlUnionConverter_t2065897640, ___hasListMember_34)); }
	inline bool get_hasListMember_34() const { return ___hasListMember_34; }
	inline bool* get_address_of_hasListMember_34() { return &___hasListMember_34; }
	inline void set_hasListMember_34(bool value)
	{
		___hasListMember_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
