﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.OpenedHost
struct  OpenedHost_t3322155821  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.OpenedHost::nonCachedConnectionsCount
	int32_t ___nonCachedConnectionsCount_0;

public:
	inline static int32_t get_offset_of_nonCachedConnectionsCount_0() { return static_cast<int32_t>(offsetof(OpenedHost_t3322155821, ___nonCachedConnectionsCount_0)); }
	inline int32_t get_nonCachedConnectionsCount_0() const { return ___nonCachedConnectionsCount_0; }
	inline int32_t* get_address_of_nonCachedConnectionsCount_0() { return &___nonCachedConnectionsCount_0; }
	inline void set_nonCachedConnectionsCount_0(int32_t value)
	{
		___nonCachedConnectionsCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
