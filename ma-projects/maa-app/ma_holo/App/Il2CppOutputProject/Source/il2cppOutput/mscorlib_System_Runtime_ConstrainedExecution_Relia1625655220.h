﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi1390725888.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer2101567438.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct  ReliabilityContractAttribute_t1625655220  : public Attribute_t542643598
{
public:
	// System.Runtime.ConstrainedExecution.Consistency System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::_consistency
	int32_t ____consistency_0;
	// System.Runtime.ConstrainedExecution.Cer System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::_cer
	int32_t ____cer_1;

public:
	inline static int32_t get_offset_of__consistency_0() { return static_cast<int32_t>(offsetof(ReliabilityContractAttribute_t1625655220, ____consistency_0)); }
	inline int32_t get__consistency_0() const { return ____consistency_0; }
	inline int32_t* get_address_of__consistency_0() { return &____consistency_0; }
	inline void set__consistency_0(int32_t value)
	{
		____consistency_0 = value;
	}

	inline static int32_t get_offset_of__cer_1() { return static_cast<int32_t>(offsetof(ReliabilityContractAttribute_t1625655220, ____cer_1)); }
	inline int32_t get__cer_1() const { return ____cer_1; }
	inline int32_t* get_address_of__cer_1() { return &____cer_1; }
	inline void set__cer_1(int32_t value)
	{
		____cer_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
