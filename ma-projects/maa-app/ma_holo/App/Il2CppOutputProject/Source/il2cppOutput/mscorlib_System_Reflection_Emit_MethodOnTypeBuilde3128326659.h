﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MethodOnTypeBuilderInst
struct  MethodOnTypeBuilderInst_t3128326659  : public MethodInfo_t
{
public:
	// System.Type System.Reflection.Emit.MethodOnTypeBuilderInst::instantiation
	Type_t * ___instantiation_0;
	// System.Reflection.MethodInfo System.Reflection.Emit.MethodOnTypeBuilderInst::base_method
	MethodInfo_t * ___base_method_1;
	// System.Type[] System.Reflection.Emit.MethodOnTypeBuilderInst::method_arguments
	TypeU5BU5D_t1664964607* ___method_arguments_2;
	// System.Reflection.MethodInfo System.Reflection.Emit.MethodOnTypeBuilderInst::generic_method_definition
	MethodInfo_t * ___generic_method_definition_3;

public:
	inline static int32_t get_offset_of_instantiation_0() { return static_cast<int32_t>(offsetof(MethodOnTypeBuilderInst_t3128326659, ___instantiation_0)); }
	inline Type_t * get_instantiation_0() const { return ___instantiation_0; }
	inline Type_t ** get_address_of_instantiation_0() { return &___instantiation_0; }
	inline void set_instantiation_0(Type_t * value)
	{
		___instantiation_0 = value;
		Il2CppCodeGenWriteBarrier(&___instantiation_0, value);
	}

	inline static int32_t get_offset_of_base_method_1() { return static_cast<int32_t>(offsetof(MethodOnTypeBuilderInst_t3128326659, ___base_method_1)); }
	inline MethodInfo_t * get_base_method_1() const { return ___base_method_1; }
	inline MethodInfo_t ** get_address_of_base_method_1() { return &___base_method_1; }
	inline void set_base_method_1(MethodInfo_t * value)
	{
		___base_method_1 = value;
		Il2CppCodeGenWriteBarrier(&___base_method_1, value);
	}

	inline static int32_t get_offset_of_method_arguments_2() { return static_cast<int32_t>(offsetof(MethodOnTypeBuilderInst_t3128326659, ___method_arguments_2)); }
	inline TypeU5BU5D_t1664964607* get_method_arguments_2() const { return ___method_arguments_2; }
	inline TypeU5BU5D_t1664964607** get_address_of_method_arguments_2() { return &___method_arguments_2; }
	inline void set_method_arguments_2(TypeU5BU5D_t1664964607* value)
	{
		___method_arguments_2 = value;
		Il2CppCodeGenWriteBarrier(&___method_arguments_2, value);
	}

	inline static int32_t get_offset_of_generic_method_definition_3() { return static_cast<int32_t>(offsetof(MethodOnTypeBuilderInst_t3128326659, ___generic_method_definition_3)); }
	inline MethodInfo_t * get_generic_method_definition_3() const { return ___generic_method_definition_3; }
	inline MethodInfo_t ** get_address_of_generic_method_definition_3() { return &___generic_method_definition_3; }
	inline void set_generic_method_definition_3(MethodInfo_t * value)
	{
		___generic_method_definition_3 = value;
		Il2CppCodeGenWriteBarrier(&___generic_method_definition_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
