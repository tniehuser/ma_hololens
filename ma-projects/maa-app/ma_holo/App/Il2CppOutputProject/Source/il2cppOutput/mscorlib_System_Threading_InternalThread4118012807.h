﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Runtime_ConstrainedExecution_Criti1920899984.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Threading_ThreadState1158972609.h"
#include "mscorlib_System_UIntPtr1549717846.h"
#include "mscorlib_System_Int322071877448.h"

// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.InternalThread
struct  InternalThread_t4118012807  : public CriticalFinalizerObject_t1920899984
{
public:
	// System.Int32 System.Threading.InternalThread::lock_thread_id
	int32_t ___lock_thread_id_0;
	// System.IntPtr System.Threading.InternalThread::system_thread_handle
	IntPtr_t ___system_thread_handle_1;
	// System.Object System.Threading.InternalThread::cached_culture_info
	Il2CppObject * ___cached_culture_info_2;
	// System.IntPtr System.Threading.InternalThread::name
	IntPtr_t ___name_3;
	// System.Int32 System.Threading.InternalThread::name_len
	int32_t ___name_len_4;
	// System.Threading.ThreadState System.Threading.InternalThread::state
	int32_t ___state_5;
	// System.Object System.Threading.InternalThread::abort_exc
	Il2CppObject * ___abort_exc_6;
	// System.Int32 System.Threading.InternalThread::abort_state_handle
	int32_t ___abort_state_handle_7;
	// System.Int64 System.Threading.InternalThread::thread_id
	int64_t ___thread_id_8;
	// System.IntPtr System.Threading.InternalThread::stack_ptr
	IntPtr_t ___stack_ptr_9;
	// System.UIntPtr System.Threading.InternalThread::static_data
	UIntPtr_t  ___static_data_10;
	// System.IntPtr System.Threading.InternalThread::runtime_thread_info
	IntPtr_t ___runtime_thread_info_11;
	// System.Object System.Threading.InternalThread::current_appcontext
	Il2CppObject * ___current_appcontext_12;
	// System.Object System.Threading.InternalThread::root_domain_thread
	Il2CppObject * ___root_domain_thread_13;
	// System.Byte[] System.Threading.InternalThread::_serialized_principal
	ByteU5BU5D_t3397334013* ____serialized_principal_14;
	// System.Int32 System.Threading.InternalThread::_serialized_principal_version
	int32_t ____serialized_principal_version_15;
	// System.IntPtr System.Threading.InternalThread::appdomain_refs
	IntPtr_t ___appdomain_refs_16;
	// System.Int32 System.Threading.InternalThread::interruption_requested
	int32_t ___interruption_requested_17;
	// System.IntPtr System.Threading.InternalThread::synch_cs
	IntPtr_t ___synch_cs_18;
	// System.Boolean System.Threading.InternalThread::threadpool_thread
	bool ___threadpool_thread_19;
	// System.Boolean System.Threading.InternalThread::thread_interrupt_requested
	bool ___thread_interrupt_requested_20;
	// System.Int32 System.Threading.InternalThread::stack_size
	int32_t ___stack_size_21;
	// System.Byte System.Threading.InternalThread::apartment_state
	uint8_t ___apartment_state_22;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.InternalThread::critical_region_level
	int32_t ___critical_region_level_23;
	// System.Int32 System.Threading.InternalThread::managed_id
	int32_t ___managed_id_24;
	// System.Int32 System.Threading.InternalThread::small_id
	int32_t ___small_id_25;
	// System.IntPtr System.Threading.InternalThread::manage_callback
	IntPtr_t ___manage_callback_26;
	// System.IntPtr System.Threading.InternalThread::interrupt_on_stop
	IntPtr_t ___interrupt_on_stop_27;
	// System.IntPtr System.Threading.InternalThread::flags
	IntPtr_t ___flags_28;
	// System.IntPtr System.Threading.InternalThread::thread_pinning_ref
	IntPtr_t ___thread_pinning_ref_29;
	// System.IntPtr System.Threading.InternalThread::abort_protected_block_count
	IntPtr_t ___abort_protected_block_count_30;
	// System.IntPtr System.Threading.InternalThread::unused1
	IntPtr_t ___unused1_31;
	// System.IntPtr System.Threading.InternalThread::unused2
	IntPtr_t ___unused2_32;
	// System.IntPtr System.Threading.InternalThread::last
	IntPtr_t ___last_33;

public:
	inline static int32_t get_offset_of_lock_thread_id_0() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___lock_thread_id_0)); }
	inline int32_t get_lock_thread_id_0() const { return ___lock_thread_id_0; }
	inline int32_t* get_address_of_lock_thread_id_0() { return &___lock_thread_id_0; }
	inline void set_lock_thread_id_0(int32_t value)
	{
		___lock_thread_id_0 = value;
	}

	inline static int32_t get_offset_of_system_thread_handle_1() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___system_thread_handle_1)); }
	inline IntPtr_t get_system_thread_handle_1() const { return ___system_thread_handle_1; }
	inline IntPtr_t* get_address_of_system_thread_handle_1() { return &___system_thread_handle_1; }
	inline void set_system_thread_handle_1(IntPtr_t value)
	{
		___system_thread_handle_1 = value;
	}

	inline static int32_t get_offset_of_cached_culture_info_2() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___cached_culture_info_2)); }
	inline Il2CppObject * get_cached_culture_info_2() const { return ___cached_culture_info_2; }
	inline Il2CppObject ** get_address_of_cached_culture_info_2() { return &___cached_culture_info_2; }
	inline void set_cached_culture_info_2(Il2CppObject * value)
	{
		___cached_culture_info_2 = value;
		Il2CppCodeGenWriteBarrier(&___cached_culture_info_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___name_3)); }
	inline IntPtr_t get_name_3() const { return ___name_3; }
	inline IntPtr_t* get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(IntPtr_t value)
	{
		___name_3 = value;
	}

	inline static int32_t get_offset_of_name_len_4() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___name_len_4)); }
	inline int32_t get_name_len_4() const { return ___name_len_4; }
	inline int32_t* get_address_of_name_len_4() { return &___name_len_4; }
	inline void set_name_len_4(int32_t value)
	{
		___name_len_4 = value;
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___state_5)); }
	inline int32_t get_state_5() const { return ___state_5; }
	inline int32_t* get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(int32_t value)
	{
		___state_5 = value;
	}

	inline static int32_t get_offset_of_abort_exc_6() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___abort_exc_6)); }
	inline Il2CppObject * get_abort_exc_6() const { return ___abort_exc_6; }
	inline Il2CppObject ** get_address_of_abort_exc_6() { return &___abort_exc_6; }
	inline void set_abort_exc_6(Il2CppObject * value)
	{
		___abort_exc_6 = value;
		Il2CppCodeGenWriteBarrier(&___abort_exc_6, value);
	}

	inline static int32_t get_offset_of_abort_state_handle_7() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___abort_state_handle_7)); }
	inline int32_t get_abort_state_handle_7() const { return ___abort_state_handle_7; }
	inline int32_t* get_address_of_abort_state_handle_7() { return &___abort_state_handle_7; }
	inline void set_abort_state_handle_7(int32_t value)
	{
		___abort_state_handle_7 = value;
	}

	inline static int32_t get_offset_of_thread_id_8() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___thread_id_8)); }
	inline int64_t get_thread_id_8() const { return ___thread_id_8; }
	inline int64_t* get_address_of_thread_id_8() { return &___thread_id_8; }
	inline void set_thread_id_8(int64_t value)
	{
		___thread_id_8 = value;
	}

	inline static int32_t get_offset_of_stack_ptr_9() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___stack_ptr_9)); }
	inline IntPtr_t get_stack_ptr_9() const { return ___stack_ptr_9; }
	inline IntPtr_t* get_address_of_stack_ptr_9() { return &___stack_ptr_9; }
	inline void set_stack_ptr_9(IntPtr_t value)
	{
		___stack_ptr_9 = value;
	}

	inline static int32_t get_offset_of_static_data_10() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___static_data_10)); }
	inline UIntPtr_t  get_static_data_10() const { return ___static_data_10; }
	inline UIntPtr_t * get_address_of_static_data_10() { return &___static_data_10; }
	inline void set_static_data_10(UIntPtr_t  value)
	{
		___static_data_10 = value;
	}

	inline static int32_t get_offset_of_runtime_thread_info_11() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___runtime_thread_info_11)); }
	inline IntPtr_t get_runtime_thread_info_11() const { return ___runtime_thread_info_11; }
	inline IntPtr_t* get_address_of_runtime_thread_info_11() { return &___runtime_thread_info_11; }
	inline void set_runtime_thread_info_11(IntPtr_t value)
	{
		___runtime_thread_info_11 = value;
	}

	inline static int32_t get_offset_of_current_appcontext_12() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___current_appcontext_12)); }
	inline Il2CppObject * get_current_appcontext_12() const { return ___current_appcontext_12; }
	inline Il2CppObject ** get_address_of_current_appcontext_12() { return &___current_appcontext_12; }
	inline void set_current_appcontext_12(Il2CppObject * value)
	{
		___current_appcontext_12 = value;
		Il2CppCodeGenWriteBarrier(&___current_appcontext_12, value);
	}

	inline static int32_t get_offset_of_root_domain_thread_13() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___root_domain_thread_13)); }
	inline Il2CppObject * get_root_domain_thread_13() const { return ___root_domain_thread_13; }
	inline Il2CppObject ** get_address_of_root_domain_thread_13() { return &___root_domain_thread_13; }
	inline void set_root_domain_thread_13(Il2CppObject * value)
	{
		___root_domain_thread_13 = value;
		Il2CppCodeGenWriteBarrier(&___root_domain_thread_13, value);
	}

	inline static int32_t get_offset_of__serialized_principal_14() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ____serialized_principal_14)); }
	inline ByteU5BU5D_t3397334013* get__serialized_principal_14() const { return ____serialized_principal_14; }
	inline ByteU5BU5D_t3397334013** get_address_of__serialized_principal_14() { return &____serialized_principal_14; }
	inline void set__serialized_principal_14(ByteU5BU5D_t3397334013* value)
	{
		____serialized_principal_14 = value;
		Il2CppCodeGenWriteBarrier(&____serialized_principal_14, value);
	}

	inline static int32_t get_offset_of__serialized_principal_version_15() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ____serialized_principal_version_15)); }
	inline int32_t get__serialized_principal_version_15() const { return ____serialized_principal_version_15; }
	inline int32_t* get_address_of__serialized_principal_version_15() { return &____serialized_principal_version_15; }
	inline void set__serialized_principal_version_15(int32_t value)
	{
		____serialized_principal_version_15 = value;
	}

	inline static int32_t get_offset_of_appdomain_refs_16() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___appdomain_refs_16)); }
	inline IntPtr_t get_appdomain_refs_16() const { return ___appdomain_refs_16; }
	inline IntPtr_t* get_address_of_appdomain_refs_16() { return &___appdomain_refs_16; }
	inline void set_appdomain_refs_16(IntPtr_t value)
	{
		___appdomain_refs_16 = value;
	}

	inline static int32_t get_offset_of_interruption_requested_17() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___interruption_requested_17)); }
	inline int32_t get_interruption_requested_17() const { return ___interruption_requested_17; }
	inline int32_t* get_address_of_interruption_requested_17() { return &___interruption_requested_17; }
	inline void set_interruption_requested_17(int32_t value)
	{
		___interruption_requested_17 = value;
	}

	inline static int32_t get_offset_of_synch_cs_18() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___synch_cs_18)); }
	inline IntPtr_t get_synch_cs_18() const { return ___synch_cs_18; }
	inline IntPtr_t* get_address_of_synch_cs_18() { return &___synch_cs_18; }
	inline void set_synch_cs_18(IntPtr_t value)
	{
		___synch_cs_18 = value;
	}

	inline static int32_t get_offset_of_threadpool_thread_19() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___threadpool_thread_19)); }
	inline bool get_threadpool_thread_19() const { return ___threadpool_thread_19; }
	inline bool* get_address_of_threadpool_thread_19() { return &___threadpool_thread_19; }
	inline void set_threadpool_thread_19(bool value)
	{
		___threadpool_thread_19 = value;
	}

	inline static int32_t get_offset_of_thread_interrupt_requested_20() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___thread_interrupt_requested_20)); }
	inline bool get_thread_interrupt_requested_20() const { return ___thread_interrupt_requested_20; }
	inline bool* get_address_of_thread_interrupt_requested_20() { return &___thread_interrupt_requested_20; }
	inline void set_thread_interrupt_requested_20(bool value)
	{
		___thread_interrupt_requested_20 = value;
	}

	inline static int32_t get_offset_of_stack_size_21() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___stack_size_21)); }
	inline int32_t get_stack_size_21() const { return ___stack_size_21; }
	inline int32_t* get_address_of_stack_size_21() { return &___stack_size_21; }
	inline void set_stack_size_21(int32_t value)
	{
		___stack_size_21 = value;
	}

	inline static int32_t get_offset_of_apartment_state_22() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___apartment_state_22)); }
	inline uint8_t get_apartment_state_22() const { return ___apartment_state_22; }
	inline uint8_t* get_address_of_apartment_state_22() { return &___apartment_state_22; }
	inline void set_apartment_state_22(uint8_t value)
	{
		___apartment_state_22 = value;
	}

	inline static int32_t get_offset_of_critical_region_level_23() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___critical_region_level_23)); }
	inline int32_t get_critical_region_level_23() const { return ___critical_region_level_23; }
	inline int32_t* get_address_of_critical_region_level_23() { return &___critical_region_level_23; }
	inline void set_critical_region_level_23(int32_t value)
	{
		___critical_region_level_23 = value;
	}

	inline static int32_t get_offset_of_managed_id_24() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___managed_id_24)); }
	inline int32_t get_managed_id_24() const { return ___managed_id_24; }
	inline int32_t* get_address_of_managed_id_24() { return &___managed_id_24; }
	inline void set_managed_id_24(int32_t value)
	{
		___managed_id_24 = value;
	}

	inline static int32_t get_offset_of_small_id_25() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___small_id_25)); }
	inline int32_t get_small_id_25() const { return ___small_id_25; }
	inline int32_t* get_address_of_small_id_25() { return &___small_id_25; }
	inline void set_small_id_25(int32_t value)
	{
		___small_id_25 = value;
	}

	inline static int32_t get_offset_of_manage_callback_26() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___manage_callback_26)); }
	inline IntPtr_t get_manage_callback_26() const { return ___manage_callback_26; }
	inline IntPtr_t* get_address_of_manage_callback_26() { return &___manage_callback_26; }
	inline void set_manage_callback_26(IntPtr_t value)
	{
		___manage_callback_26 = value;
	}

	inline static int32_t get_offset_of_interrupt_on_stop_27() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___interrupt_on_stop_27)); }
	inline IntPtr_t get_interrupt_on_stop_27() const { return ___interrupt_on_stop_27; }
	inline IntPtr_t* get_address_of_interrupt_on_stop_27() { return &___interrupt_on_stop_27; }
	inline void set_interrupt_on_stop_27(IntPtr_t value)
	{
		___interrupt_on_stop_27 = value;
	}

	inline static int32_t get_offset_of_flags_28() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___flags_28)); }
	inline IntPtr_t get_flags_28() const { return ___flags_28; }
	inline IntPtr_t* get_address_of_flags_28() { return &___flags_28; }
	inline void set_flags_28(IntPtr_t value)
	{
		___flags_28 = value;
	}

	inline static int32_t get_offset_of_thread_pinning_ref_29() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___thread_pinning_ref_29)); }
	inline IntPtr_t get_thread_pinning_ref_29() const { return ___thread_pinning_ref_29; }
	inline IntPtr_t* get_address_of_thread_pinning_ref_29() { return &___thread_pinning_ref_29; }
	inline void set_thread_pinning_ref_29(IntPtr_t value)
	{
		___thread_pinning_ref_29 = value;
	}

	inline static int32_t get_offset_of_abort_protected_block_count_30() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___abort_protected_block_count_30)); }
	inline IntPtr_t get_abort_protected_block_count_30() const { return ___abort_protected_block_count_30; }
	inline IntPtr_t* get_address_of_abort_protected_block_count_30() { return &___abort_protected_block_count_30; }
	inline void set_abort_protected_block_count_30(IntPtr_t value)
	{
		___abort_protected_block_count_30 = value;
	}

	inline static int32_t get_offset_of_unused1_31() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___unused1_31)); }
	inline IntPtr_t get_unused1_31() const { return ___unused1_31; }
	inline IntPtr_t* get_address_of_unused1_31() { return &___unused1_31; }
	inline void set_unused1_31(IntPtr_t value)
	{
		___unused1_31 = value;
	}

	inline static int32_t get_offset_of_unused2_32() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___unused2_32)); }
	inline IntPtr_t get_unused2_32() const { return ___unused2_32; }
	inline IntPtr_t* get_address_of_unused2_32() { return &___unused2_32; }
	inline void set_unused2_32(IntPtr_t value)
	{
		___unused2_32 = value;
	}

	inline static int32_t get_offset_of_last_33() { return static_cast<int32_t>(offsetof(InternalThread_t4118012807, ___last_33)); }
	inline IntPtr_t get_last_33() const { return ___last_33; }
	inline IntPtr_t* get_address_of_last_33() { return &___last_33; }
	inline void set_last_33(IntPtr_t value)
	{
		___last_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
