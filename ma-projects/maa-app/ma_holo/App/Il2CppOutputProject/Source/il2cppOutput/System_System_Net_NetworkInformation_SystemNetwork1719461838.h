﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.NetworkInformation.NetworkInterfaceFactory
struct NetworkInterfaceFactory_t2383622021;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.SystemNetworkInterface
struct  SystemNetworkInterface_t1719461838  : public Il2CppObject
{
public:

public:
};

struct SystemNetworkInterface_t1719461838_StaticFields
{
public:
	// System.Net.NetworkInformation.NetworkInterfaceFactory System.Net.NetworkInformation.SystemNetworkInterface::nif
	NetworkInterfaceFactory_t2383622021 * ___nif_0;

public:
	inline static int32_t get_offset_of_nif_0() { return static_cast<int32_t>(offsetof(SystemNetworkInterface_t1719461838_StaticFields, ___nif_0)); }
	inline NetworkInterfaceFactory_t2383622021 * get_nif_0() const { return ___nif_0; }
	inline NetworkInterfaceFactory_t2383622021 ** get_address_of_nif_0() { return &___nif_0; }
	inline void set_nif_0(NetworkInterfaceFactory_t2383622021 * value)
	{
		___nif_0 = value;
		Il2CppCodeGenWriteBarrier(&___nif_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
