﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseProcessor2373158431.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Compiler
struct  Compiler_t2139734799  : public BaseProcessor_t2373158431
{
public:
	// System.String System.Xml.Schema.Compiler::restrictionErrorMsg
	String_t* ___restrictionErrorMsg_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::attributes
	XmlSchemaObjectTable_t3364835593 * ___attributes_7;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::attributeGroups
	XmlSchemaObjectTable_t3364835593 * ___attributeGroups_8;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::elements
	XmlSchemaObjectTable_t3364835593 * ___elements_9;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::schemaTypes
	XmlSchemaObjectTable_t3364835593 * ___schemaTypes_10;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::groups
	XmlSchemaObjectTable_t3364835593 * ___groups_11;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::notations
	XmlSchemaObjectTable_t3364835593 * ___notations_12;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::examplars
	XmlSchemaObjectTable_t3364835593 * ___examplars_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::identityConstraints
	XmlSchemaObjectTable_t3364835593 * ___identityConstraints_14;
	// System.Collections.Stack System.Xml.Schema.Compiler::complexTypeStack
	Stack_t1043988394 * ___complexTypeStack_15;
	// System.Collections.Hashtable System.Xml.Schema.Compiler::schemasToCompile
	Hashtable_t909839986 * ___schemasToCompile_16;
	// System.Collections.Hashtable System.Xml.Schema.Compiler::importedSchemas
	Hashtable_t909839986 * ___importedSchemas_17;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Compiler::schemaForSchema
	XmlSchema_t880472818 * ___schemaForSchema_18;

public:
	inline static int32_t get_offset_of_restrictionErrorMsg_6() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___restrictionErrorMsg_6)); }
	inline String_t* get_restrictionErrorMsg_6() const { return ___restrictionErrorMsg_6; }
	inline String_t** get_address_of_restrictionErrorMsg_6() { return &___restrictionErrorMsg_6; }
	inline void set_restrictionErrorMsg_6(String_t* value)
	{
		___restrictionErrorMsg_6 = value;
		Il2CppCodeGenWriteBarrier(&___restrictionErrorMsg_6, value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___attributes_7)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributes_7() const { return ___attributes_7; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_7, value);
	}

	inline static int32_t get_offset_of_attributeGroups_8() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___attributeGroups_8)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeGroups_8() const { return ___attributeGroups_8; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeGroups_8() { return &___attributeGroups_8; }
	inline void set_attributeGroups_8(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeGroups_8 = value;
		Il2CppCodeGenWriteBarrier(&___attributeGroups_8, value);
	}

	inline static int32_t get_offset_of_elements_9() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___elements_9)); }
	inline XmlSchemaObjectTable_t3364835593 * get_elements_9() const { return ___elements_9; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_elements_9() { return &___elements_9; }
	inline void set_elements_9(XmlSchemaObjectTable_t3364835593 * value)
	{
		___elements_9 = value;
		Il2CppCodeGenWriteBarrier(&___elements_9, value);
	}

	inline static int32_t get_offset_of_schemaTypes_10() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___schemaTypes_10)); }
	inline XmlSchemaObjectTable_t3364835593 * get_schemaTypes_10() const { return ___schemaTypes_10; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_schemaTypes_10() { return &___schemaTypes_10; }
	inline void set_schemaTypes_10(XmlSchemaObjectTable_t3364835593 * value)
	{
		___schemaTypes_10 = value;
		Il2CppCodeGenWriteBarrier(&___schemaTypes_10, value);
	}

	inline static int32_t get_offset_of_groups_11() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___groups_11)); }
	inline XmlSchemaObjectTable_t3364835593 * get_groups_11() const { return ___groups_11; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_groups_11() { return &___groups_11; }
	inline void set_groups_11(XmlSchemaObjectTable_t3364835593 * value)
	{
		___groups_11 = value;
		Il2CppCodeGenWriteBarrier(&___groups_11, value);
	}

	inline static int32_t get_offset_of_notations_12() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___notations_12)); }
	inline XmlSchemaObjectTable_t3364835593 * get_notations_12() const { return ___notations_12; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_notations_12() { return &___notations_12; }
	inline void set_notations_12(XmlSchemaObjectTable_t3364835593 * value)
	{
		___notations_12 = value;
		Il2CppCodeGenWriteBarrier(&___notations_12, value);
	}

	inline static int32_t get_offset_of_examplars_13() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___examplars_13)); }
	inline XmlSchemaObjectTable_t3364835593 * get_examplars_13() const { return ___examplars_13; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_examplars_13() { return &___examplars_13; }
	inline void set_examplars_13(XmlSchemaObjectTable_t3364835593 * value)
	{
		___examplars_13 = value;
		Il2CppCodeGenWriteBarrier(&___examplars_13, value);
	}

	inline static int32_t get_offset_of_identityConstraints_14() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___identityConstraints_14)); }
	inline XmlSchemaObjectTable_t3364835593 * get_identityConstraints_14() const { return ___identityConstraints_14; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_identityConstraints_14() { return &___identityConstraints_14; }
	inline void set_identityConstraints_14(XmlSchemaObjectTable_t3364835593 * value)
	{
		___identityConstraints_14 = value;
		Il2CppCodeGenWriteBarrier(&___identityConstraints_14, value);
	}

	inline static int32_t get_offset_of_complexTypeStack_15() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___complexTypeStack_15)); }
	inline Stack_t1043988394 * get_complexTypeStack_15() const { return ___complexTypeStack_15; }
	inline Stack_t1043988394 ** get_address_of_complexTypeStack_15() { return &___complexTypeStack_15; }
	inline void set_complexTypeStack_15(Stack_t1043988394 * value)
	{
		___complexTypeStack_15 = value;
		Il2CppCodeGenWriteBarrier(&___complexTypeStack_15, value);
	}

	inline static int32_t get_offset_of_schemasToCompile_16() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___schemasToCompile_16)); }
	inline Hashtable_t909839986 * get_schemasToCompile_16() const { return ___schemasToCompile_16; }
	inline Hashtable_t909839986 ** get_address_of_schemasToCompile_16() { return &___schemasToCompile_16; }
	inline void set_schemasToCompile_16(Hashtable_t909839986 * value)
	{
		___schemasToCompile_16 = value;
		Il2CppCodeGenWriteBarrier(&___schemasToCompile_16, value);
	}

	inline static int32_t get_offset_of_importedSchemas_17() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___importedSchemas_17)); }
	inline Hashtable_t909839986 * get_importedSchemas_17() const { return ___importedSchemas_17; }
	inline Hashtable_t909839986 ** get_address_of_importedSchemas_17() { return &___importedSchemas_17; }
	inline void set_importedSchemas_17(Hashtable_t909839986 * value)
	{
		___importedSchemas_17 = value;
		Il2CppCodeGenWriteBarrier(&___importedSchemas_17, value);
	}

	inline static int32_t get_offset_of_schemaForSchema_18() { return static_cast<int32_t>(offsetof(Compiler_t2139734799, ___schemaForSchema_18)); }
	inline XmlSchema_t880472818 * get_schemaForSchema_18() const { return ___schemaForSchema_18; }
	inline XmlSchema_t880472818 ** get_address_of_schemaForSchema_18() { return &___schemaForSchema_18; }
	inline void set_schemaForSchema_18(XmlSchema_t880472818 * value)
	{
		___schemaForSchema_18 = value;
		Il2CppCodeGenWriteBarrier(&___schemaForSchema_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
