﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime_DateTimeTy563167880.h"
#include "System_Xml_System_Xml_Schema_XsdDateTime_XsdDateTi2077819616.h"

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTime/Parser
struct  Parser_t2170101039 
{
public:
	// System.Xml.Schema.XsdDateTime/DateTimeTypeCode System.Xml.Schema.XsdDateTime/Parser::typeCode
	int32_t ___typeCode_0;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::year
	int32_t ___year_1;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::month
	int32_t ___month_2;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::day
	int32_t ___day_3;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::hour
	int32_t ___hour_4;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::minute
	int32_t ___minute_5;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::second
	int32_t ___second_6;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::fraction
	int32_t ___fraction_7;
	// System.Xml.Schema.XsdDateTime/XsdDateTimeKind System.Xml.Schema.XsdDateTime/Parser::kind
	int32_t ___kind_8;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneHour
	int32_t ___zoneHour_9;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::zoneMinute
	int32_t ___zoneMinute_10;
	// System.String System.Xml.Schema.XsdDateTime/Parser::text
	String_t* ___text_11;
	// System.Int32 System.Xml.Schema.XsdDateTime/Parser::length
	int32_t ___length_12;

public:
	inline static int32_t get_offset_of_typeCode_0() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___typeCode_0)); }
	inline int32_t get_typeCode_0() const { return ___typeCode_0; }
	inline int32_t* get_address_of_typeCode_0() { return &___typeCode_0; }
	inline void set_typeCode_0(int32_t value)
	{
		___typeCode_0 = value;
	}

	inline static int32_t get_offset_of_year_1() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___year_1)); }
	inline int32_t get_year_1() const { return ___year_1; }
	inline int32_t* get_address_of_year_1() { return &___year_1; }
	inline void set_year_1(int32_t value)
	{
		___year_1 = value;
	}

	inline static int32_t get_offset_of_month_2() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___month_2)); }
	inline int32_t get_month_2() const { return ___month_2; }
	inline int32_t* get_address_of_month_2() { return &___month_2; }
	inline void set_month_2(int32_t value)
	{
		___month_2 = value;
	}

	inline static int32_t get_offset_of_day_3() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___day_3)); }
	inline int32_t get_day_3() const { return ___day_3; }
	inline int32_t* get_address_of_day_3() { return &___day_3; }
	inline void set_day_3(int32_t value)
	{
		___day_3 = value;
	}

	inline static int32_t get_offset_of_hour_4() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___hour_4)); }
	inline int32_t get_hour_4() const { return ___hour_4; }
	inline int32_t* get_address_of_hour_4() { return &___hour_4; }
	inline void set_hour_4(int32_t value)
	{
		___hour_4 = value;
	}

	inline static int32_t get_offset_of_minute_5() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___minute_5)); }
	inline int32_t get_minute_5() const { return ___minute_5; }
	inline int32_t* get_address_of_minute_5() { return &___minute_5; }
	inline void set_minute_5(int32_t value)
	{
		___minute_5 = value;
	}

	inline static int32_t get_offset_of_second_6() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___second_6)); }
	inline int32_t get_second_6() const { return ___second_6; }
	inline int32_t* get_address_of_second_6() { return &___second_6; }
	inline void set_second_6(int32_t value)
	{
		___second_6 = value;
	}

	inline static int32_t get_offset_of_fraction_7() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___fraction_7)); }
	inline int32_t get_fraction_7() const { return ___fraction_7; }
	inline int32_t* get_address_of_fraction_7() { return &___fraction_7; }
	inline void set_fraction_7(int32_t value)
	{
		___fraction_7 = value;
	}

	inline static int32_t get_offset_of_kind_8() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___kind_8)); }
	inline int32_t get_kind_8() const { return ___kind_8; }
	inline int32_t* get_address_of_kind_8() { return &___kind_8; }
	inline void set_kind_8(int32_t value)
	{
		___kind_8 = value;
	}

	inline static int32_t get_offset_of_zoneHour_9() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___zoneHour_9)); }
	inline int32_t get_zoneHour_9() const { return ___zoneHour_9; }
	inline int32_t* get_address_of_zoneHour_9() { return &___zoneHour_9; }
	inline void set_zoneHour_9(int32_t value)
	{
		___zoneHour_9 = value;
	}

	inline static int32_t get_offset_of_zoneMinute_10() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___zoneMinute_10)); }
	inline int32_t get_zoneMinute_10() const { return ___zoneMinute_10; }
	inline int32_t* get_address_of_zoneMinute_10() { return &___zoneMinute_10; }
	inline void set_zoneMinute_10(int32_t value)
	{
		___zoneMinute_10 = value;
	}

	inline static int32_t get_offset_of_text_11() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___text_11)); }
	inline String_t* get_text_11() const { return ___text_11; }
	inline String_t** get_address_of_text_11() { return &___text_11; }
	inline void set_text_11(String_t* value)
	{
		___text_11 = value;
		Il2CppCodeGenWriteBarrier(&___text_11, value);
	}

	inline static int32_t get_offset_of_length_12() { return static_cast<int32_t>(offsetof(Parser_t2170101039, ___length_12)); }
	inline int32_t get_length_12() const { return ___length_12; }
	inline int32_t* get_address_of_length_12() { return &___length_12; }
	inline void set_length_12(int32_t value)
	{
		___length_12 = value;
	}
};

struct Parser_t2170101039_StaticFields
{
public:
	// System.Int32[] System.Xml.Schema.XsdDateTime/Parser::Power10
	Int32U5BU5D_t3030399641* ___Power10_13;

public:
	inline static int32_t get_offset_of_Power10_13() { return static_cast<int32_t>(offsetof(Parser_t2170101039_StaticFields, ___Power10_13)); }
	inline Int32U5BU5D_t3030399641* get_Power10_13() const { return ___Power10_13; }
	inline Int32U5BU5D_t3030399641** get_address_of_Power10_13() { return &___Power10_13; }
	inline void set_Power10_13(Int32U5BU5D_t3030399641* value)
	{
		___Power10_13 = value;
		Il2CppCodeGenWriteBarrier(&___Power10_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t2170101039_marshaled_pinvoke
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	char* ___text_11;
	int32_t ___length_12;
};
// Native definition for COM marshalling of System.Xml.Schema.XsdDateTime/Parser
struct Parser_t2170101039_marshaled_com
{
	int32_t ___typeCode_0;
	int32_t ___year_1;
	int32_t ___month_2;
	int32_t ___day_3;
	int32_t ___hour_4;
	int32_t ___minute_5;
	int32_t ___second_6;
	int32_t ___fraction_7;
	int32_t ___kind_8;
	int32_t ___zoneHour_9;
	int32_t ___zoneMinute_10;
	Il2CppChar* ___text_11;
	int32_t ___length_12;
};
