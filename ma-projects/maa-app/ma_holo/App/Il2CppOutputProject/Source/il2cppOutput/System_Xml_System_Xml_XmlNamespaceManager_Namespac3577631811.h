﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct  NamespaceDeclaration_t3577631811 
{
public:
	// System.String System.Xml.XmlNamespaceManager/NamespaceDeclaration::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NamespaceDeclaration::uri
	String_t* ___uri_1;
	// System.Int32 System.Xml.XmlNamespaceManager/NamespaceDeclaration::scopeId
	int32_t ___scopeId_2;
	// System.Int32 System.Xml.XmlNamespaceManager/NamespaceDeclaration::previousNsIndex
	int32_t ___previousNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_0, value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier(&___uri_1, value);
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___scopeId_2)); }
	inline int32_t get_scopeId_2() const { return ___scopeId_2; }
	inline int32_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int32_t value)
	{
		___scopeId_2 = value;
	}

	inline static int32_t get_offset_of_previousNsIndex_3() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___previousNsIndex_3)); }
	inline int32_t get_previousNsIndex_3() const { return ___previousNsIndex_3; }
	inline int32_t* get_address_of_previousNsIndex_3() { return &___previousNsIndex_3; }
	inline void set_previousNsIndex_3(int32_t value)
	{
		___previousNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct NamespaceDeclaration_t3577631811_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___uri_1;
	int32_t ___scopeId_2;
	int32_t ___previousNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct NamespaceDeclaration_t3577631811_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___uri_1;
	int32_t ___scopeId_2;
	int32_t ___previousNsIndex_3;
};
