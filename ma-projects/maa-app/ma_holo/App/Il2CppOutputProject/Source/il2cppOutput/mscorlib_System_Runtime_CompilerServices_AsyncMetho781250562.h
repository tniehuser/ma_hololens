﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t3152578875;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner
struct  MoveNextRunner_t781250562  : public Il2CppObject
{
public:
	// System.Threading.ExecutionContext System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner::m_context
	ExecutionContext_t1392266323 * ___m_context_0;
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner::m_stateMachine
	Il2CppObject * ___m_stateMachine_1;

public:
	inline static int32_t get_offset_of_m_context_0() { return static_cast<int32_t>(offsetof(MoveNextRunner_t781250562, ___m_context_0)); }
	inline ExecutionContext_t1392266323 * get_m_context_0() const { return ___m_context_0; }
	inline ExecutionContext_t1392266323 ** get_address_of_m_context_0() { return &___m_context_0; }
	inline void set_m_context_0(ExecutionContext_t1392266323 * value)
	{
		___m_context_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_context_0, value);
	}

	inline static int32_t get_offset_of_m_stateMachine_1() { return static_cast<int32_t>(offsetof(MoveNextRunner_t781250562, ___m_stateMachine_1)); }
	inline Il2CppObject * get_m_stateMachine_1() const { return ___m_stateMachine_1; }
	inline Il2CppObject ** get_address_of_m_stateMachine_1() { return &___m_stateMachine_1; }
	inline void set_m_stateMachine_1(Il2CppObject * value)
	{
		___m_stateMachine_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_stateMachine_1, value);
	}
};

struct MoveNextRunner_t781250562_StaticFields
{
public:
	// System.Threading.ContextCallback System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner::s_invokeMoveNext
	ContextCallback_t2287130692 * ___s_invokeMoveNext_2;
	// System.Threading.ContextCallback System.Runtime.CompilerServices.AsyncMethodBuilderCore/MoveNextRunner::<>f__mg$cache0
	ContextCallback_t2287130692 * ___U3CU3Ef__mgU24cache0_3;

public:
	inline static int32_t get_offset_of_s_invokeMoveNext_2() { return static_cast<int32_t>(offsetof(MoveNextRunner_t781250562_StaticFields, ___s_invokeMoveNext_2)); }
	inline ContextCallback_t2287130692 * get_s_invokeMoveNext_2() const { return ___s_invokeMoveNext_2; }
	inline ContextCallback_t2287130692 ** get_address_of_s_invokeMoveNext_2() { return &___s_invokeMoveNext_2; }
	inline void set_s_invokeMoveNext_2(ContextCallback_t2287130692 * value)
	{
		___s_invokeMoveNext_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_invokeMoveNext_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(MoveNextRunner_t781250562_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ContextCallback_t2287130692 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ContextCallback_t2287130692 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ContextCallback_t2287130692 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
