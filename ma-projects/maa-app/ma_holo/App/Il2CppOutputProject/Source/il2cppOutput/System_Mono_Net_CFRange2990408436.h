﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFRange
struct  CFRange_t2990408436 
{
public:
	// System.IntPtr Mono.Net.CFRange::Location
	IntPtr_t ___Location_0;
	// System.IntPtr Mono.Net.CFRange::Length
	IntPtr_t ___Length_1;

public:
	inline static int32_t get_offset_of_Location_0() { return static_cast<int32_t>(offsetof(CFRange_t2990408436, ___Location_0)); }
	inline IntPtr_t get_Location_0() const { return ___Location_0; }
	inline IntPtr_t* get_address_of_Location_0() { return &___Location_0; }
	inline void set_Location_0(IntPtr_t value)
	{
		___Location_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(CFRange_t2990408436, ___Length_1)); }
	inline IntPtr_t get_Length_1() const { return ___Length_1; }
	inline IntPtr_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(IntPtr_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
