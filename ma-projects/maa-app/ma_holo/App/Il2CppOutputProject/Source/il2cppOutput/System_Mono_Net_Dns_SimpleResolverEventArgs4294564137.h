﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"
#include "System_Mono_Net_Dns_ResolverError2494446112.h"

// System.EventHandler`1<Mono.Net.Dns.SimpleResolverEventArgs>
struct EventHandler_1_t2885871309;
// System.String
struct String_t;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.Threading.Timer
struct Timer_t791717973;
// System.Net.IPAddress
struct IPAddress_t1399971723;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.SimpleResolverEventArgs
struct  SimpleResolverEventArgs_t4294564137  : public EventArgs_t3289624707
{
public:
	// System.EventHandler`1<Mono.Net.Dns.SimpleResolverEventArgs> Mono.Net.Dns.SimpleResolverEventArgs::Completed
	EventHandler_1_t2885871309 * ___Completed_1;
	// Mono.Net.Dns.ResolverError Mono.Net.Dns.SimpleResolverEventArgs::<ResolverError>k__BackingField
	int32_t ___U3CResolverErrorU3Ek__BackingField_2;
	// System.String Mono.Net.Dns.SimpleResolverEventArgs::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_3;
	// System.String Mono.Net.Dns.SimpleResolverEventArgs::<HostName>k__BackingField
	String_t* ___U3CHostNameU3Ek__BackingField_4;
	// System.Net.IPHostEntry Mono.Net.Dns.SimpleResolverEventArgs::<HostEntry>k__BackingField
	IPHostEntry_t994738509 * ___U3CHostEntryU3Ek__BackingField_5;
	// System.UInt16 Mono.Net.Dns.SimpleResolverEventArgs::QueryID
	uint16_t ___QueryID_6;
	// System.UInt16 Mono.Net.Dns.SimpleResolverEventArgs::Retries
	uint16_t ___Retries_7;
	// System.Threading.Timer Mono.Net.Dns.SimpleResolverEventArgs::Timer
	Timer_t791717973 * ___Timer_8;
	// System.Net.IPAddress Mono.Net.Dns.SimpleResolverEventArgs::PTRAddress
	IPAddress_t1399971723 * ___PTRAddress_9;

public:
	inline static int32_t get_offset_of_Completed_1() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Completed_1)); }
	inline EventHandler_1_t2885871309 * get_Completed_1() const { return ___Completed_1; }
	inline EventHandler_1_t2885871309 ** get_address_of_Completed_1() { return &___Completed_1; }
	inline void set_Completed_1(EventHandler_1_t2885871309 * value)
	{
		___Completed_1 = value;
		Il2CppCodeGenWriteBarrier(&___Completed_1, value);
	}

	inline static int32_t get_offset_of_U3CResolverErrorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CResolverErrorU3Ek__BackingField_2)); }
	inline int32_t get_U3CResolverErrorU3Ek__BackingField_2() const { return ___U3CResolverErrorU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CResolverErrorU3Ek__BackingField_2() { return &___U3CResolverErrorU3Ek__BackingField_2; }
	inline void set_U3CResolverErrorU3Ek__BackingField_2(int32_t value)
	{
		___U3CResolverErrorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CErrorMessageU3Ek__BackingField_3)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_3() const { return ___U3CErrorMessageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_3() { return &___U3CErrorMessageU3Ek__BackingField_3; }
	inline void set_U3CErrorMessageU3Ek__BackingField_3(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CHostNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CHostNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CHostNameU3Ek__BackingField_4() const { return ___U3CHostNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CHostNameU3Ek__BackingField_4() { return &___U3CHostNameU3Ek__BackingField_4; }
	inline void set_U3CHostNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CHostNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHostNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CHostEntryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CHostEntryU3Ek__BackingField_5)); }
	inline IPHostEntry_t994738509 * get_U3CHostEntryU3Ek__BackingField_5() const { return ___U3CHostEntryU3Ek__BackingField_5; }
	inline IPHostEntry_t994738509 ** get_address_of_U3CHostEntryU3Ek__BackingField_5() { return &___U3CHostEntryU3Ek__BackingField_5; }
	inline void set_U3CHostEntryU3Ek__BackingField_5(IPHostEntry_t994738509 * value)
	{
		___U3CHostEntryU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHostEntryU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_QueryID_6() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___QueryID_6)); }
	inline uint16_t get_QueryID_6() const { return ___QueryID_6; }
	inline uint16_t* get_address_of_QueryID_6() { return &___QueryID_6; }
	inline void set_QueryID_6(uint16_t value)
	{
		___QueryID_6 = value;
	}

	inline static int32_t get_offset_of_Retries_7() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Retries_7)); }
	inline uint16_t get_Retries_7() const { return ___Retries_7; }
	inline uint16_t* get_address_of_Retries_7() { return &___Retries_7; }
	inline void set_Retries_7(uint16_t value)
	{
		___Retries_7 = value;
	}

	inline static int32_t get_offset_of_Timer_8() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Timer_8)); }
	inline Timer_t791717973 * get_Timer_8() const { return ___Timer_8; }
	inline Timer_t791717973 ** get_address_of_Timer_8() { return &___Timer_8; }
	inline void set_Timer_8(Timer_t791717973 * value)
	{
		___Timer_8 = value;
		Il2CppCodeGenWriteBarrier(&___Timer_8, value);
	}

	inline static int32_t get_offset_of_PTRAddress_9() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___PTRAddress_9)); }
	inline IPAddress_t1399971723 * get_PTRAddress_9() const { return ___PTRAddress_9; }
	inline IPAddress_t1399971723 ** get_address_of_PTRAddress_9() { return &___PTRAddress_9; }
	inline void set_PTRAddress_9(IPAddress_t1399971723 * value)
	{
		___PTRAddress_9 = value;
		Il2CppCodeGenWriteBarrier(&___PTRAddress_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
