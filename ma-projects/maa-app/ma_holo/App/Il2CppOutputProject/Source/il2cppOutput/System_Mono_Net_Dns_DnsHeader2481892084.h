﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsHeader
struct  DnsHeader_t2481892084  : public Il2CppObject
{
public:
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsHeader::bytes
	ArraySegment_1_t2594217482  ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DnsHeader_t2481892084, ___bytes_0)); }
	inline ArraySegment_1_t2594217482  get_bytes_0() const { return ___bytes_0; }
	inline ArraySegment_1_t2594217482 * get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ArraySegment_1_t2594217482  value)
	{
		___bytes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
