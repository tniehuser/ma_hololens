﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_CancellationTokenRegistr1708859357.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Shared`1<System.Threading.CancellationTokenRegistration>
struct  Shared_1_t2858597776  : public Il2CppObject
{
public:
	// T System.Threading.Tasks.Shared`1::Value
	CancellationTokenRegistration_t1708859357  ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Shared_1_t2858597776, ___Value_0)); }
	inline CancellationTokenRegistration_t1708859357  get_Value_0() const { return ___Value_0; }
	inline CancellationTokenRegistration_t1708859357 * get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(CancellationTokenRegistration_t1708859357  value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
