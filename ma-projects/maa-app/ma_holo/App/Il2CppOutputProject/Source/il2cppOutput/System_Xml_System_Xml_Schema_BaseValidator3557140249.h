﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3518500204;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t482152337;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Uri
struct Uri_t19570940;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1507412803;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.ValidationState
struct ValidationState_t3143048826;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_t3557140249  : public Il2CppObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_t3518500204 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	Il2CppObject * ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t19570940 * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t1507412803 * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_t1944712516 * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t3143048826 * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t1221177846 * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaCollection_0)); }
	inline XmlSchemaCollection_t3518500204 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_t3518500204 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_t3518500204 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier(&___schemaCollection_0, value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___eventHandling_1)); }
	inline Il2CppObject * get_eventHandling_1() const { return ___eventHandling_1; }
	inline Il2CppObject ** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(Il2CppObject * value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandling_1, value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_2, value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaNames_3)); }
	inline SchemaNames_t1619962557 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t1619962557 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier(&___schemaNames_3, value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___positionInfo_4)); }
	inline PositionInfo_t3273236083 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_t3273236083 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___positionInfo_4, value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___xmlResolver_5)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_5, value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___baseUri_6)); }
	inline Uri_t19570940 * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t19570940 ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t19570940 * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_6, value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaInfo_7)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___schemaInfo_7, value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___reader_8)); }
	inline XmlValidatingReaderImpl_t1507412803 * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t1507412803 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t1507412803 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier(&___reader_8, value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___elementName_9)); }
	inline XmlQualifiedName_t1944712516 * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_t1944712516 * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier(&___elementName_9, value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___context_10)); }
	inline ValidationState_t3143048826 * get_context_10() const { return ___context_10; }
	inline ValidationState_t3143048826 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t3143048826 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier(&___context_10, value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textValue_11)); }
	inline StringBuilder_t1221177846 * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t1221177846 ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t1221177846 * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___textValue_11, value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier(&___textString_12, value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
