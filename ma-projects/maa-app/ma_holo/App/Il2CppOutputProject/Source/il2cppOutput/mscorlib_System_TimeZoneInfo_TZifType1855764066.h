﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_TimeSpan3430258949.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/TZifType
struct  TZifType_t1855764066 
{
public:
	// System.TimeSpan System.TimeZoneInfo/TZifType::UtcOffset
	TimeSpan_t3430258949  ___UtcOffset_0;
	// System.Boolean System.TimeZoneInfo/TZifType::IsDst
	bool ___IsDst_1;
	// System.Byte System.TimeZoneInfo/TZifType::AbbreviationIndex
	uint8_t ___AbbreviationIndex_2;

public:
	inline static int32_t get_offset_of_UtcOffset_0() { return static_cast<int32_t>(offsetof(TZifType_t1855764066, ___UtcOffset_0)); }
	inline TimeSpan_t3430258949  get_UtcOffset_0() const { return ___UtcOffset_0; }
	inline TimeSpan_t3430258949 * get_address_of_UtcOffset_0() { return &___UtcOffset_0; }
	inline void set_UtcOffset_0(TimeSpan_t3430258949  value)
	{
		___UtcOffset_0 = value;
	}

	inline static int32_t get_offset_of_IsDst_1() { return static_cast<int32_t>(offsetof(TZifType_t1855764066, ___IsDst_1)); }
	inline bool get_IsDst_1() const { return ___IsDst_1; }
	inline bool* get_address_of_IsDst_1() { return &___IsDst_1; }
	inline void set_IsDst_1(bool value)
	{
		___IsDst_1 = value;
	}

	inline static int32_t get_offset_of_AbbreviationIndex_2() { return static_cast<int32_t>(offsetof(TZifType_t1855764066, ___AbbreviationIndex_2)); }
	inline uint8_t get_AbbreviationIndex_2() const { return ___AbbreviationIndex_2; }
	inline uint8_t* get_address_of_AbbreviationIndex_2() { return &___AbbreviationIndex_2; }
	inline void set_AbbreviationIndex_2(uint8_t value)
	{
		___AbbreviationIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.TimeZoneInfo/TZifType
struct TZifType_t1855764066_marshaled_pinvoke
{
	TimeSpan_t3430258949  ___UtcOffset_0;
	int32_t ___IsDst_1;
	uint8_t ___AbbreviationIndex_2;
};
// Native definition for COM marshalling of System.TimeZoneInfo/TZifType
struct TZifType_t1855764066_marshaled_com
{
	TimeSpan_t3430258949  ___UtcOffset_0;
	int32_t ___IsDst_1;
	uint8_t ___AbbreviationIndex_2;
};
