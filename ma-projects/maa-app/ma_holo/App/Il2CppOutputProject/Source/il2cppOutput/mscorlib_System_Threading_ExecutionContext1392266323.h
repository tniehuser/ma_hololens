﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_ExecutionContext_Flags4022531286.h"

// System.Threading.SynchronizationContext
struct SynchronizationContext_t3857790437;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t725724420;
// System.Runtime.Remoting.Messaging.IllogicalCallContext
struct IllogicalCallContext_t206449943;
// System.Collections.Generic.Dictionary`2<System.Threading.IAsyncLocal,System.Object>
struct Dictionary_2_t3003703034;
// System.Collections.Generic.List`1<System.Threading.IAsyncLocal>
struct List_1_t3012907916;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ExecutionContext
struct  ExecutionContext_t1392266323  : public Il2CppObject
{
public:
	// System.Threading.SynchronizationContext System.Threading.ExecutionContext::_syncContext
	SynchronizationContext_t3857790437 * ____syncContext_0;
	// System.Threading.SynchronizationContext System.Threading.ExecutionContext::_syncContextNoFlow
	SynchronizationContext_t3857790437 * ____syncContextNoFlow_1;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Threading.ExecutionContext::_logicalCallContext
	LogicalCallContext_t725724420 * ____logicalCallContext_2;
	// System.Runtime.Remoting.Messaging.IllogicalCallContext System.Threading.ExecutionContext::_illogicalCallContext
	IllogicalCallContext_t206449943 * ____illogicalCallContext_3;
	// System.Threading.ExecutionContext/Flags System.Threading.ExecutionContext::_flags
	int32_t ____flags_4;
	// System.Collections.Generic.Dictionary`2<System.Threading.IAsyncLocal,System.Object> System.Threading.ExecutionContext::_localValues
	Dictionary_2_t3003703034 * ____localValues_5;
	// System.Collections.Generic.List`1<System.Threading.IAsyncLocal> System.Threading.ExecutionContext::_localChangeNotifications
	List_1_t3012907916 * ____localChangeNotifications_6;

public:
	inline static int32_t get_offset_of__syncContext_0() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____syncContext_0)); }
	inline SynchronizationContext_t3857790437 * get__syncContext_0() const { return ____syncContext_0; }
	inline SynchronizationContext_t3857790437 ** get_address_of__syncContext_0() { return &____syncContext_0; }
	inline void set__syncContext_0(SynchronizationContext_t3857790437 * value)
	{
		____syncContext_0 = value;
		Il2CppCodeGenWriteBarrier(&____syncContext_0, value);
	}

	inline static int32_t get_offset_of__syncContextNoFlow_1() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____syncContextNoFlow_1)); }
	inline SynchronizationContext_t3857790437 * get__syncContextNoFlow_1() const { return ____syncContextNoFlow_1; }
	inline SynchronizationContext_t3857790437 ** get_address_of__syncContextNoFlow_1() { return &____syncContextNoFlow_1; }
	inline void set__syncContextNoFlow_1(SynchronizationContext_t3857790437 * value)
	{
		____syncContextNoFlow_1 = value;
		Il2CppCodeGenWriteBarrier(&____syncContextNoFlow_1, value);
	}

	inline static int32_t get_offset_of__logicalCallContext_2() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____logicalCallContext_2)); }
	inline LogicalCallContext_t725724420 * get__logicalCallContext_2() const { return ____logicalCallContext_2; }
	inline LogicalCallContext_t725724420 ** get_address_of__logicalCallContext_2() { return &____logicalCallContext_2; }
	inline void set__logicalCallContext_2(LogicalCallContext_t725724420 * value)
	{
		____logicalCallContext_2 = value;
		Il2CppCodeGenWriteBarrier(&____logicalCallContext_2, value);
	}

	inline static int32_t get_offset_of__illogicalCallContext_3() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____illogicalCallContext_3)); }
	inline IllogicalCallContext_t206449943 * get__illogicalCallContext_3() const { return ____illogicalCallContext_3; }
	inline IllogicalCallContext_t206449943 ** get_address_of__illogicalCallContext_3() { return &____illogicalCallContext_3; }
	inline void set__illogicalCallContext_3(IllogicalCallContext_t206449943 * value)
	{
		____illogicalCallContext_3 = value;
		Il2CppCodeGenWriteBarrier(&____illogicalCallContext_3, value);
	}

	inline static int32_t get_offset_of__flags_4() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____flags_4)); }
	inline int32_t get__flags_4() const { return ____flags_4; }
	inline int32_t* get_address_of__flags_4() { return &____flags_4; }
	inline void set__flags_4(int32_t value)
	{
		____flags_4 = value;
	}

	inline static int32_t get_offset_of__localValues_5() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____localValues_5)); }
	inline Dictionary_2_t3003703034 * get__localValues_5() const { return ____localValues_5; }
	inline Dictionary_2_t3003703034 ** get_address_of__localValues_5() { return &____localValues_5; }
	inline void set__localValues_5(Dictionary_2_t3003703034 * value)
	{
		____localValues_5 = value;
		Il2CppCodeGenWriteBarrier(&____localValues_5, value);
	}

	inline static int32_t get_offset_of__localChangeNotifications_6() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323, ____localChangeNotifications_6)); }
	inline List_1_t3012907916 * get__localChangeNotifications_6() const { return ____localChangeNotifications_6; }
	inline List_1_t3012907916 ** get_address_of__localChangeNotifications_6() { return &____localChangeNotifications_6; }
	inline void set__localChangeNotifications_6(List_1_t3012907916 * value)
	{
		____localChangeNotifications_6 = value;
		Il2CppCodeGenWriteBarrier(&____localChangeNotifications_6, value);
	}
};

struct ExecutionContext_t1392266323_StaticFields
{
public:
	// System.Threading.ExecutionContext System.Threading.ExecutionContext::s_dummyDefaultEC
	ExecutionContext_t1392266323 * ___s_dummyDefaultEC_7;

public:
	inline static int32_t get_offset_of_s_dummyDefaultEC_7() { return static_cast<int32_t>(offsetof(ExecutionContext_t1392266323_StaticFields, ___s_dummyDefaultEC_7)); }
	inline ExecutionContext_t1392266323 * get_s_dummyDefaultEC_7() const { return ___s_dummyDefaultEC_7; }
	inline ExecutionContext_t1392266323 ** get_address_of_s_dummyDefaultEC_7() { return &___s_dummyDefaultEC_7; }
	inline void set_s_dummyDefaultEC_7(ExecutionContext_t1392266323 * value)
	{
		___s_dummyDefaultEC_7 = value;
		Il2CppCodeGenWriteBarrier(&___s_dummyDefaultEC_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
