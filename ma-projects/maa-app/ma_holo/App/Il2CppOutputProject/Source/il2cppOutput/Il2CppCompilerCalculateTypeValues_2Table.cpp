﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Base64FormattingOptions1648395238.h"
#include "mscorlib_System_Convert2607082565.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_DayOfWeek721777893.h"
#include "mscorlib_System_DBNull972229383.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_DefaultBinder424142959.h"
#include "mscorlib_System_DefaultBinder_BinderState2820609036.h"
#include "mscorlib_System_Diagnostics_Contracts_Contract1212488476.h"
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttr518825354.h"
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute638884887.h"
#include "mscorlib_System_Diagnostics_DebuggableAttribute994551506.h"
#include "mscorlib_System_Diagnostics_DebuggableAttribute_De2073970606.h"
#include "mscorlib_System_Diagnostics_DebuggerBrowsableState944457511.h"
#include "mscorlib_System_Diagnostics_DebuggerBrowsableAttri1386379234.h"
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttrib970972087.h"
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribu1528914581.h"
#include "mscorlib_System_DivideByZeroException1660837001.h"
#include "mscorlib_System_DllNotFoundException3636280042.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Empty3498280455.h"
#include "mscorlib_System_EntryPointNotFoundException3956266210.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_Enum_ParseFailureKind3876725692.h"
#include "mscorlib_System_Enum_EnumResult2872047947.h"
#include "mscorlib_System_Enum_ValuesAndNames2142337900.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Exception_ExceptionMessageKind1743375253.h"
#include "mscorlib_System_ExecutionEngineException1360775125.h"
#include "mscorlib_System_FieldAccessException1797813379.h"
#include "mscorlib_System_FlagsAttribute859561169.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "mscorlib_System_GC2902933594.h"
#include "mscorlib_System_Globalization_Calendar585061108.h"
#include "mscorlib_System_Globalization_CalendarData275297348.h"
#include "mscorlib_System_Globalization_CharUnicodeInfo2446282145.h"
#include "mscorlib_System_Globalization_CharUnicodeInfo_Unic2784996206.h"
#include "mscorlib_System_Globalization_CharUnicodeInfo_Digi2603367031.h"
#include "mscorlib_System_Globalization_CompareOptions2829943955.h"
#include "mscorlib_System_Globalization_CompareInfo2310920157.h"
#include "mscorlib_System_Globalization_CultureNotFoundExcep1206712152.h"
#include "mscorlib_System_Globalization_CultureTypes441055003.h"
#include "mscorlib_System_DateTimeFormat2556779728.h"
#include "mscorlib_System_Globalization_MonthNameStyles552260427.h"
#include "mscorlib_System_Globalization_DateTimeFormatFlags3140910561.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2187473504.h"
#include "mscorlib_System_Globalization_TokenHashValue393941324.h"
#include "mscorlib_System_Globalization_FORMATFLAGS2783048252.h"
#include "mscorlib_System_Globalization_CalendarId693969307.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfoSc2782759166.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfoSc2584404353.h"
#include "mscorlib_System_DateTimeParse2729286702.h"
#include "mscorlib_System_DateTimeParse_MatchNumberDelegate2323488373.h"
#include "mscorlib_System_DateTimeParse_DTT3106618880.h"
#include "mscorlib_System_DateTimeParse_TM56736635.h"
#include "mscorlib_System_DateTimeParse_DS769197341.h"
#include "mscorlib_System___DTString420639831.h"
#include "mscorlib_System_DTSubStringType564418869.h"
#include "mscorlib_System_DTSubString4037085109.h"
#include "mscorlib_System_DateTimeToken4280265824.h"
#include "mscorlib_System_DateTimeRawInfo1887088787.h"
#include "mscorlib_System_ParseFailureKind297234793.h"
#include "mscorlib_System_ParseFlags944640856.h"
#include "mscorlib_System_DateTimeResult824298922.h"
#include "mscorlib_System_ParsingInfo442145320.h"
#include "mscorlib_System_TokenType854809643.h"
#include "mscorlib_System_Globalization_DateTimeStyles370343085.h"
#include "mscorlib_System_Globalization_DaylightTime3800227331.h"
#include "mscorlib_System_Globalization_GlobalizationAssembl2733185295.h"
#include "mscorlib_System_Globalization_GregorianCalendar3361245568.h"
#include "mscorlib_System_Globalization_EraInfo1792567760.h"
#include "mscorlib_System_Globalization_GregorianCalendarHel3151146692.h"
#include "mscorlib_System_Globalization_GregorianCalendarTyp3080789929.h"
#include "mscorlib_System_Globalization_HebrewNumberParsingC2737450771.h"
#include "mscorlib_System_Globalization_HebrewNumberParsingSt678415071.h"
#include "mscorlib_System_Globalization_HebrewNumber1466597772.h"
#include "mscorlib_System_Globalization_HebrewNumber_HebrewTo765430061.h"
#include "mscorlib_System_Globalization_HebrewNumber_HebrewV2452471125.h"
#include "mscorlib_System_Globalization_HebrewNumber_HS1739780518.h"
#include "mscorlib_System_Globalization_JapaneseCalendar3073160889.h"
#include "mscorlib_System_Globalization_NumberFormatInfo104580544.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "mscorlib_System_Globalization_TaiwanCalendar2121259800.h"
#include "mscorlib_System_Globalization_TextInfo3620182823.h"
#include "mscorlib_System_Globalization_TimeSpanFormat1848991148.h"
#include "mscorlib_System_Globalization_TimeSpanFormat_Patter522564603.h"
#include "mscorlib_System_Globalization_TimeSpanFormat_Forma2087080800.h"
#include "mscorlib_System_Globalization_TimeSpanStyles2925399185.h"
#include "mscorlib_System_Globalization_TimeSpanParse3827434274.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp2652982273.h"
#include "mscorlib_System_Globalization_TimeSpanParse_ParseF2272087344.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp2692921727.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TTT150500429.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp2031659367.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp1845673523.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize200 = { sizeof (Base64FormattingOptions_t1648395238)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable200[3] = 
{
	Base64FormattingOptions_t1648395238::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize201 = { sizeof (Convert_t2607082565), -1, sizeof(Convert_t2607082565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable201[4] = 
{
	Convert_t2607082565_StaticFields::get_offset_of_ConvertTypes_0(),
	Convert_t2607082565_StaticFields::get_offset_of_EnumType_1(),
	Convert_t2607082565_StaticFields::get_offset_of_base64Table_2(),
	Convert_t2607082565_StaticFields::get_offset_of_DBNull_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize202 = { sizeof (DateTime_t693205669)+ sizeof (Il2CppObject), -1, sizeof(DateTime_t693205669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable202[5] = 
{
	DateTime_t693205669_StaticFields::get_offset_of_DaysToMonth365_0(),
	DateTime_t693205669_StaticFields::get_offset_of_DaysToMonth366_1(),
	DateTime_t693205669_StaticFields::get_offset_of_MinValue_2(),
	DateTime_t693205669_StaticFields::get_offset_of_MaxValue_3(),
	DateTime_t693205669::get_offset_of_dateData_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize203 = { sizeof (DateTimeKind_t2186819611)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable203[4] = 
{
	DateTimeKind_t2186819611::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize204 = { sizeof (DateTimeOffset_t1362988906)+ sizeof (Il2CppObject), -1, sizeof(DateTimeOffset_t1362988906_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable204[4] = 
{
	DateTimeOffset_t1362988906_StaticFields::get_offset_of_MinValue_0(),
	DateTimeOffset_t1362988906_StaticFields::get_offset_of_MaxValue_1(),
	DateTimeOffset_t1362988906::get_offset_of_m_dateTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeOffset_t1362988906::get_offset_of_m_offsetMinutes_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize205 = { sizeof (DayOfWeek_t721777893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable205[8] = 
{
	DayOfWeek_t721777893::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize206 = { sizeof (DBNull_t972229383), -1, sizeof(DBNull_t972229383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable206[1] = 
{
	DBNull_t972229383_StaticFields::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize207 = { sizeof (Decimal_t724701077)+ sizeof (Il2CppObject), sizeof(Decimal_t724701077 ), sizeof(Decimal_t724701077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable207[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	Decimal_t724701077_StaticFields::get_offset_of_Powers10_6(),
	Decimal_t724701077_StaticFields::get_offset_of_Zero_7(),
	Decimal_t724701077_StaticFields::get_offset_of_One_8(),
	Decimal_t724701077_StaticFields::get_offset_of_MinusOne_9(),
	Decimal_t724701077_StaticFields::get_offset_of_MaxValue_10(),
	Decimal_t724701077_StaticFields::get_offset_of_MinValue_11(),
	Decimal_t724701077_StaticFields::get_offset_of_NearNegativeZero_12(),
	Decimal_t724701077_StaticFields::get_offset_of_NearPositiveZero_13(),
	Decimal_t724701077::get_offset_of_flags_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Decimal_t724701077::get_offset_of_hi_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Decimal_t724701077::get_offset_of_lo_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Decimal_t724701077::get_offset_of_mid_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize208 = { sizeof (DefaultBinder_t424142959), -1, sizeof(DefaultBinder_t424142959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable208[1] = 
{
	DefaultBinder_t424142959_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize209 = { sizeof (BinderState_t2820609036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable209[3] = 
{
	BinderState_t2820609036::get_offset_of_m_argsMap_0(),
	BinderState_t2820609036::get_offset_of_m_originalSize_1(),
	BinderState_t2820609036::get_offset_of_m_isParamArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize210 = { sizeof (Contract_t1212488476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize211 = { sizeof (DebuggerStepThroughAttribute_t518825354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize212 = { sizeof (DebuggerHiddenAttribute_t638884887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize213 = { sizeof (DebuggableAttribute_t994551506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable213[1] = 
{
	DebuggableAttribute_t994551506::get_offset_of_m_debuggingModes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize214 = { sizeof (DebuggingModes_t2073970606)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable214[6] = 
{
	DebuggingModes_t2073970606::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize215 = { sizeof (DebuggerBrowsableState_t944457511)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable215[4] = 
{
	DebuggerBrowsableState_t944457511::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize216 = { sizeof (DebuggerBrowsableAttribute_t1386379234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable216[1] = 
{
	DebuggerBrowsableAttribute_t1386379234::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize217 = { sizeof (DebuggerTypeProxyAttribute_t970972087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable217[1] = 
{
	DebuggerTypeProxyAttribute_t970972087::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize218 = { sizeof (DebuggerDisplayAttribute_t1528914581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable218[3] = 
{
	DebuggerDisplayAttribute_t1528914581::get_offset_of_name_0(),
	DebuggerDisplayAttribute_t1528914581::get_offset_of_value_1(),
	DebuggerDisplayAttribute_t1528914581::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize219 = { sizeof (DivideByZeroException_t1660837001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize220 = { sizeof (DllNotFoundException_t3636280042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize221 = { sizeof (Double_t4078015681)+ sizeof (Il2CppObject), sizeof(double), sizeof(Double_t4078015681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable221[2] = 
{
	Double_t4078015681::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Double_t4078015681_StaticFields::get_offset_of_NegativeZero_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize222 = { sizeof (Empty_t3498280455), -1, sizeof(Empty_t3498280455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable222[1] = 
{
	Empty_t3498280455_StaticFields::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize223 = { sizeof (EntryPointNotFoundException_t3956266210), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize224 = { sizeof (Enum_t2459695545), sizeof(Enum_t2459695545_marshaled_pinvoke), sizeof(Enum_t2459695545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable224[2] = 
{
	Enum_t2459695545_StaticFields::get_offset_of_enumSeperatorCharArray_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize225 = { sizeof (ParseFailureKind_t3876725692)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable225[6] = 
{
	ParseFailureKind_t3876725692::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize226 = { sizeof (EnumResult_t2872047947)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable226[7] = 
{
	EnumResult_t2872047947::get_offset_of_parsedEnum_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_canThrow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_m_failure_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_m_failureMessageID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_m_failureParameter_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_m_failureMessageFormatArgument_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EnumResult_t2872047947::get_offset_of_m_innerException_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize227 = { sizeof (ValuesAndNames_t2142337900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable227[2] = 
{
	ValuesAndNames_t2142337900::get_offset_of_Values_0(),
	ValuesAndNames_t2142337900::get_offset_of_Names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize228 = { sizeof (EventArgs_t3289624707), -1, sizeof(EventArgs_t3289624707_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable228[1] = 
{
	EventArgs_t3289624707_StaticFields::get_offset_of_Empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize229 = { sizeof (EventHandler_t277755526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize230 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize231 = { sizeof (Exception_t1927440687), -1, sizeof(Exception_t1927440687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable231[16] = 
{
	Exception_t1927440687_StaticFields::get_offset_of_s_EDILock_0(),
	Exception_t1927440687::get_offset_of__className_1(),
	Exception_t1927440687::get_offset_of__message_2(),
	Exception_t1927440687::get_offset_of__data_3(),
	Exception_t1927440687::get_offset_of__innerException_4(),
	Exception_t1927440687::get_offset_of__helpURL_5(),
	Exception_t1927440687::get_offset_of__stackTrace_6(),
	Exception_t1927440687::get_offset_of__stackTraceString_7(),
	Exception_t1927440687::get_offset_of__remoteStackTraceString_8(),
	Exception_t1927440687::get_offset_of__remoteStackIndex_9(),
	Exception_t1927440687::get_offset_of__dynamicMethods_10(),
	Exception_t1927440687::get_offset_of__HResult_11(),
	Exception_t1927440687::get_offset_of__source_12(),
	Exception_t1927440687::get_offset_of__safeSerializationManager_13(),
	Exception_t1927440687::get_offset_of_captured_traces_14(),
	Exception_t1927440687::get_offset_of_native_trace_ips_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize232 = { sizeof (ExceptionMessageKind_t1743375253)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable232[4] = 
{
	ExceptionMessageKind_t1743375253::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize233 = { sizeof (ExecutionEngineException_t1360775125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize234 = { sizeof (FieldAccessException_t1797813379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize235 = { sizeof (FlagsAttribute_t859561169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize236 = { sizeof (FormatException_t2948921286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize237 = { sizeof (GC_t2902933594), -1, sizeof(GC_t2902933594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable237[1] = 
{
	GC_t2902933594_StaticFields::get_offset_of_EPHEMERON_TOMBSTONE_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize238 = { sizeof (Calendar_t585061108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable238[3] = 
{
	Calendar_t585061108::get_offset_of_m_currentEraValue_0(),
	Calendar_t585061108::get_offset_of_m_isReadOnly_1(),
	Calendar_t585061108::get_offset_of_twoDigitYearMax_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize239 = { sizeof (CalendarData_t275297348), -1, sizeof(CalendarData_t275297348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable239[21] = 
{
	0,
	CalendarData_t275297348::get_offset_of_sNativeName_1(),
	CalendarData_t275297348::get_offset_of_saShortDates_2(),
	CalendarData_t275297348::get_offset_of_saYearMonths_3(),
	CalendarData_t275297348::get_offset_of_saLongDates_4(),
	CalendarData_t275297348::get_offset_of_sMonthDay_5(),
	CalendarData_t275297348::get_offset_of_saEraNames_6(),
	CalendarData_t275297348::get_offset_of_saAbbrevEraNames_7(),
	CalendarData_t275297348::get_offset_of_saAbbrevEnglishEraNames_8(),
	CalendarData_t275297348::get_offset_of_saDayNames_9(),
	CalendarData_t275297348::get_offset_of_saAbbrevDayNames_10(),
	CalendarData_t275297348::get_offset_of_saSuperShortDayNames_11(),
	CalendarData_t275297348::get_offset_of_saMonthNames_12(),
	CalendarData_t275297348::get_offset_of_saAbbrevMonthNames_13(),
	CalendarData_t275297348::get_offset_of_saMonthGenitiveNames_14(),
	CalendarData_t275297348::get_offset_of_saAbbrevMonthGenitiveNames_15(),
	CalendarData_t275297348::get_offset_of_saLeapYearMonthNames_16(),
	CalendarData_t275297348::get_offset_of_iTwoDigitYearMax_17(),
	CalendarData_t275297348::get_offset_of_iCurrentEra_18(),
	CalendarData_t275297348::get_offset_of_bUseUserOverrides_19(),
	CalendarData_t275297348_StaticFields::get_offset_of_Invariant_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize240 = { sizeof (CharUnicodeInfo_t2446282145), -1, sizeof(CharUnicodeInfo_t2446282145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable240[6] = 
{
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_initialized_0(),
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_pCategoryLevel1Index_1(),
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_pCategoriesValue_2(),
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_pNumericLevel1Index_3(),
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_pNumericValues_4(),
	CharUnicodeInfo_t2446282145_StaticFields::get_offset_of_s_pDigitValues_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize241 = { sizeof (UnicodeDataHeader_t2784996206)+ sizeof (Il2CppObject), sizeof(UnicodeDataHeader_t2784996206_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable241[7] = 
{
	UnicodeDataHeader_t2784996206::get_offset_of_TableName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_OffsetToCategoriesIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_OffsetToCategoriesValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_OffsetToNumbericIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_OffsetToDigitValue_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnicodeDataHeader_t2784996206::get_offset_of_OffsetToNumbericValue_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize242 = { sizeof (DigitValues_t2603367031)+ sizeof (Il2CppObject), sizeof(DigitValues_t2603367031 ), 0, 0 };
extern const int32_t g_FieldOffsetTable242[2] = 
{
	DigitValues_t2603367031::get_offset_of_decimalDigit_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DigitValues_t2603367031::get_offset_of_digit_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize243 = { sizeof (CompareOptions_t2829943955)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable243[10] = 
{
	CompareOptions_t2829943955::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize244 = { sizeof (CompareInfo_t2310920157), -1, sizeof(CompareInfo_t2310920157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable244[7] = 
{
	CompareInfo_t2310920157::get_offset_of_m_name_0(),
	CompareInfo_t2310920157::get_offset_of_m_sortName_1(),
	CompareInfo_t2310920157::get_offset_of_culture_2(),
	CompareInfo_t2310920157::get_offset_of_collator_3(),
	CompareInfo_t2310920157_StaticFields::get_offset_of_collators_4(),
	CompareInfo_t2310920157_StaticFields::get_offset_of_managedCollation_5(),
	CompareInfo_t2310920157_StaticFields::get_offset_of_managedCollationChecked_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize245 = { sizeof (CultureNotFoundException_t1206712152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable245[2] = 
{
	CultureNotFoundException_t1206712152::get_offset_of_m_invalidCultureName_17(),
	CultureNotFoundException_t1206712152::get_offset_of_m_invalidCultureId_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize246 = { sizeof (CultureTypes_t441055003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable246[9] = 
{
	CultureTypes_t441055003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize247 = { sizeof (DateTimeFormat_t2556779728), -1, sizeof(DateTimeFormat_t2556779728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable247[3] = 
{
	DateTimeFormat_t2556779728_StaticFields::get_offset_of_NullOffset_0(),
	DateTimeFormat_t2556779728_StaticFields::get_offset_of_allStandardFormats_1(),
	DateTimeFormat_t2556779728_StaticFields::get_offset_of_fixedNumberFormats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize248 = { sizeof (MonthNameStyles_t552260427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable248[4] = 
{
	MonthNameStyles_t552260427::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize249 = { sizeof (DateTimeFormatFlags_t3140910561)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable249[9] = 
{
	DateTimeFormatFlags_t3140910561::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize250 = { sizeof (DateTimeFormatInfo_t2187473504), -1, sizeof(DateTimeFormatInfo_t2187473504_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable250[85] = 
{
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_invariantInfo_0(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_cultureData_1(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_name_2(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_langName_3(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_compareInfo_4(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_cultureInfo_5(),
	DateTimeFormatInfo_t2187473504::get_offset_of_amDesignator_6(),
	DateTimeFormatInfo_t2187473504::get_offset_of_pmDesignator_7(),
	DateTimeFormatInfo_t2187473504::get_offset_of_dateSeparator_8(),
	DateTimeFormatInfo_t2187473504::get_offset_of_generalShortTimePattern_9(),
	DateTimeFormatInfo_t2187473504::get_offset_of_generalLongTimePattern_10(),
	DateTimeFormatInfo_t2187473504::get_offset_of_timeSeparator_11(),
	DateTimeFormatInfo_t2187473504::get_offset_of_monthDayPattern_12(),
	DateTimeFormatInfo_t2187473504::get_offset_of_dateTimeOffsetPattern_13(),
	0,
	0,
	0,
	DateTimeFormatInfo_t2187473504::get_offset_of_calendar_17(),
	DateTimeFormatInfo_t2187473504::get_offset_of_firstDayOfWeek_18(),
	DateTimeFormatInfo_t2187473504::get_offset_of_calendarWeekRule_19(),
	DateTimeFormatInfo_t2187473504::get_offset_of_fullDateTimePattern_20(),
	DateTimeFormatInfo_t2187473504::get_offset_of_abbreviatedDayNames_21(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_superShortDayNames_22(),
	DateTimeFormatInfo_t2187473504::get_offset_of_dayNames_23(),
	DateTimeFormatInfo_t2187473504::get_offset_of_abbreviatedMonthNames_24(),
	DateTimeFormatInfo_t2187473504::get_offset_of_monthNames_25(),
	DateTimeFormatInfo_t2187473504::get_offset_of_genitiveMonthNames_26(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_genitiveAbbreviatedMonthNames_27(),
	DateTimeFormatInfo_t2187473504::get_offset_of_leapYearMonthNames_28(),
	DateTimeFormatInfo_t2187473504::get_offset_of_longDatePattern_29(),
	DateTimeFormatInfo_t2187473504::get_offset_of_shortDatePattern_30(),
	DateTimeFormatInfo_t2187473504::get_offset_of_yearMonthPattern_31(),
	DateTimeFormatInfo_t2187473504::get_offset_of_longTimePattern_32(),
	DateTimeFormatInfo_t2187473504::get_offset_of_shortTimePattern_33(),
	DateTimeFormatInfo_t2187473504::get_offset_of_allYearMonthPatterns_34(),
	DateTimeFormatInfo_t2187473504::get_offset_of_allShortDatePatterns_35(),
	DateTimeFormatInfo_t2187473504::get_offset_of_allLongDatePatterns_36(),
	DateTimeFormatInfo_t2187473504::get_offset_of_allShortTimePatterns_37(),
	DateTimeFormatInfo_t2187473504::get_offset_of_allLongTimePatterns_38(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_eraNames_39(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_abbrevEraNames_40(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_abbrevEnglishEraNames_41(),
	DateTimeFormatInfo_t2187473504::get_offset_of_optionalCalendars_42(),
	0,
	DateTimeFormatInfo_t2187473504::get_offset_of_m_isReadOnly_44(),
	DateTimeFormatInfo_t2187473504::get_offset_of_formatFlags_45(),
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_preferExistingTokens_46(),
	DateTimeFormatInfo_t2187473504::get_offset_of_CultureID_47(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_useUserOverride_48(),
	DateTimeFormatInfo_t2187473504::get_offset_of_bUseCalendarInfo_49(),
	DateTimeFormatInfo_t2187473504::get_offset_of_nDataItem_50(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_isDefaultCalendar_51(),
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_s_calendarNativeNames_52(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_dateWords_53(),
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_MonthSpaces_54(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_fullTimeSpanPositivePattern_55(),
	DateTimeFormatInfo_t2187473504::get_offset_of_m_fullTimeSpanNegativePattern_56(),
	0,
	DateTimeFormatInfo_t2187473504::get_offset_of_m_dtfiTokenHash_58(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_s_jajpDTFI_83(),
	DateTimeFormatInfo_t2187473504_StaticFields::get_offset_of_s_zhtwDTFI_84(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize251 = { sizeof (TokenHashValue_t393941324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable251[3] = 
{
	TokenHashValue_t393941324::get_offset_of_tokenString_0(),
	TokenHashValue_t393941324::get_offset_of_tokenType_1(),
	TokenHashValue_t393941324::get_offset_of_tokenValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize252 = { sizeof (FORMATFLAGS_t2783048252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable252[8] = 
{
	FORMATFLAGS_t2783048252::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize253 = { sizeof (CalendarId_t693969307)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable253[25] = 
{
	CalendarId_t693969307::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize254 = { sizeof (DateTimeFormatInfoScanner_t2782759166), -1, sizeof(DateTimeFormatInfoScanner_t2782759166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable254[3] = 
{
	DateTimeFormatInfoScanner_t2782759166::get_offset_of_m_dateWords_0(),
	DateTimeFormatInfoScanner_t2782759166_StaticFields::get_offset_of_s_knownWords_1(),
	DateTimeFormatInfoScanner_t2782759166::get_offset_of_m_ymdFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize255 = { sizeof (FoundDatePattern_t2584404353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable255[6] = 
{
	FoundDatePattern_t2584404353::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize256 = { sizeof (DateTimeParse_t2729286702), -1, sizeof(DateTimeParse_t2729286702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable256[2] = 
{
	DateTimeParse_t2729286702_StaticFields::get_offset_of_m_hebrewNumberParser_0(),
	DateTimeParse_t2729286702_StaticFields::get_offset_of_dateParsingStates_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize257 = { sizeof (MatchNumberDelegate_t2323488373), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize258 = { sizeof (DTT_t3106618880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable258[22] = 
{
	DTT_t3106618880::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize259 = { sizeof (TM_t56736635)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable259[4] = 
{
	TM_t56736635::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize260 = { sizeof (DS_t769197341)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable260[40] = 
{
	DS_t769197341::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize261 = { sizeof (__DTString_t420639831)+ sizeof (Il2CppObject), -1, sizeof(__DTString_t420639831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable261[7] = 
{
	__DTString_t420639831::get_offset_of_Value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831::get_offset_of_Index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831::get_offset_of_len_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831::get_offset_of_m_current_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831::get_offset_of_m_info_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831::get_offset_of_m_checkDigitToken_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	__DTString_t420639831_StaticFields::get_offset_of_WhiteSpaceChecks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize262 = { sizeof (DTSubStringType_t564418869)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable262[6] = 
{
	DTSubStringType_t564418869::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize263 = { sizeof (DTSubString_t4037085109)+ sizeof (Il2CppObject), sizeof(DTSubString_t4037085109_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable263[5] = 
{
	DTSubString_t4037085109::get_offset_of_s_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTSubString_t4037085109::get_offset_of_index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTSubString_t4037085109::get_offset_of_length_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTSubString_t4037085109::get_offset_of_type_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DTSubString_t4037085109::get_offset_of_value_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize264 = { sizeof (DateTimeToken_t4280265824)+ sizeof (Il2CppObject), sizeof(DateTimeToken_t4280265824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable264[3] = 
{
	DateTimeToken_t4280265824::get_offset_of_dtt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeToken_t4280265824::get_offset_of_suffix_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeToken_t4280265824::get_offset_of_num_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize265 = { sizeof (DateTimeRawInfo_t1887088787)+ sizeof (Il2CppObject), sizeof(DateTimeRawInfo_t1887088787_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable265[10] = 
{
	DateTimeRawInfo_t1887088787::get_offset_of_num_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_numCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_month_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_year_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_dayOfWeek_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_era_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_timeMark_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_fraction_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_hasSameDateAndTimeSeparators_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeRawInfo_t1887088787::get_offset_of_timeZone_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize266 = { sizeof (ParseFailureKind_t297234793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable266[6] = 
{
	ParseFailureKind_t297234793::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize267 = { sizeof (ParseFlags_t944640856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable267[16] = 
{
	ParseFlags_t944640856::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize268 = { sizeof (DateTimeResult_t824298922)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable268[16] = 
{
	DateTimeResult_t824298922::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_fraction_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_era_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_flags_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_timeZoneOffset_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_calendar_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_parsedDate_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_failure_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_failureMessageID_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_failureMessageFormatArgument_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeResult_t824298922::get_offset_of_failureArgumentName_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize269 = { sizeof (ParsingInfo_t442145320)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable269[9] = 
{
	ParsingInfo_t442145320::get_offset_of_calendar_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_dayOfWeek_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_timeMark_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_fUseHour12_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_fUseTwoDigitYear_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_fAllowInnerWhite_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_fAllowTrailingWhite_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_fCustomNumberParser_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParsingInfo_t442145320::get_offset_of_parseNumberDelegate_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize270 = { sizeof (TokenType_t854809643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable270[33] = 
{
	TokenType_t854809643::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize271 = { sizeof (DateTimeStyles_t370343085)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable271[11] = 
{
	DateTimeStyles_t370343085::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize272 = { sizeof (DaylightTime_t3800227331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable272[3] = 
{
	DaylightTime_t3800227331::get_offset_of_m_start_0(),
	DaylightTime_t3800227331::get_offset_of_m_end_1(),
	DaylightTime_t3800227331::get_offset_of_m_delta_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize273 = { sizeof (GlobalizationAssembly_t2733185295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize274 = { sizeof (GregorianCalendar_t3361245568), -1, sizeof(GregorianCalendar_t3361245568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable274[4] = 
{
	GregorianCalendar_t3361245568::get_offset_of_m_type_3(),
	GregorianCalendar_t3361245568_StaticFields::get_offset_of_DaysToMonth365_4(),
	GregorianCalendar_t3361245568_StaticFields::get_offset_of_DaysToMonth366_5(),
	GregorianCalendar_t3361245568_StaticFields::get_offset_of_s_defaultInstance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize275 = { sizeof (EraInfo_t1792567760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable275[8] = 
{
	EraInfo_t1792567760::get_offset_of_era_0(),
	EraInfo_t1792567760::get_offset_of_ticks_1(),
	EraInfo_t1792567760::get_offset_of_yearOffset_2(),
	EraInfo_t1792567760::get_offset_of_minEraYear_3(),
	EraInfo_t1792567760::get_offset_of_maxEraYear_4(),
	EraInfo_t1792567760::get_offset_of_eraName_5(),
	EraInfo_t1792567760::get_offset_of_abbrevEraName_6(),
	EraInfo_t1792567760::get_offset_of_englishEraName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize276 = { sizeof (GregorianCalendarHelper_t3151146692), -1, sizeof(GregorianCalendarHelper_t3151146692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable276[8] = 
{
	GregorianCalendarHelper_t3151146692_StaticFields::get_offset_of_DaysToMonth365_0(),
	GregorianCalendarHelper_t3151146692_StaticFields::get_offset_of_DaysToMonth366_1(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_maxYear_2(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_minYear_3(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_Cal_4(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_EraInfo_5(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_eras_6(),
	GregorianCalendarHelper_t3151146692::get_offset_of_m_minDate_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize277 = { sizeof (GregorianCalendarTypes_t3080789929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable277[7] = 
{
	GregorianCalendarTypes_t3080789929::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize278 = { sizeof (HebrewNumberParsingContext_t2737450771)+ sizeof (Il2CppObject), sizeof(HebrewNumberParsingContext_t2737450771 ), 0, 0 };
extern const int32_t g_FieldOffsetTable278[2] = 
{
	HebrewNumberParsingContext_t2737450771::get_offset_of_state_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HebrewNumberParsingContext_t2737450771::get_offset_of_result_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize279 = { sizeof (HebrewNumberParsingState_t678415071)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable279[5] = 
{
	HebrewNumberParsingState_t678415071::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize280 = { sizeof (HebrewNumber_t1466597772), -1, sizeof(HebrewNumber_t1466597772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable280[3] = 
{
	HebrewNumber_t1466597772_StaticFields::get_offset_of_HebrewValues_0(),
	HebrewNumber_t1466597772_StaticFields::get_offset_of_maxHebrewNumberCh_1(),
	HebrewNumber_t1466597772_StaticFields::get_offset_of_NumberPasingState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize281 = { sizeof (HebrewToken_t765430061)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable281[12] = 
{
	HebrewToken_t765430061::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize282 = { sizeof (HebrewValue_t2452471125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable282[2] = 
{
	HebrewValue_t2452471125::get_offset_of_token_0(),
	HebrewValue_t2452471125::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize283 = { sizeof (HS_t1739780518)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable283[20] = 
{
	HS_t1739780518::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize284 = { sizeof (JapaneseCalendar_t3073160889), -1, sizeof(JapaneseCalendar_t3073160889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable284[4] = 
{
	JapaneseCalendar_t3073160889_StaticFields::get_offset_of_calendarMinValue_3(),
	JapaneseCalendar_t3073160889_StaticFields::get_offset_of_japaneseEraInfo_4(),
	JapaneseCalendar_t3073160889_StaticFields::get_offset_of_s_defaultInstance_5(),
	JapaneseCalendar_t3073160889::get_offset_of_helper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize285 = { sizeof (NumberFormatInfo_t104580544), -1, sizeof(NumberFormatInfo_t104580544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable285[36] = 
{
	NumberFormatInfo_t104580544_StaticFields::get_offset_of_invariantInfo_0(),
	NumberFormatInfo_t104580544::get_offset_of_numberGroupSizes_1(),
	NumberFormatInfo_t104580544::get_offset_of_currencyGroupSizes_2(),
	NumberFormatInfo_t104580544::get_offset_of_percentGroupSizes_3(),
	NumberFormatInfo_t104580544::get_offset_of_positiveSign_4(),
	NumberFormatInfo_t104580544::get_offset_of_negativeSign_5(),
	NumberFormatInfo_t104580544::get_offset_of_numberDecimalSeparator_6(),
	NumberFormatInfo_t104580544::get_offset_of_numberGroupSeparator_7(),
	NumberFormatInfo_t104580544::get_offset_of_currencyGroupSeparator_8(),
	NumberFormatInfo_t104580544::get_offset_of_currencyDecimalSeparator_9(),
	NumberFormatInfo_t104580544::get_offset_of_currencySymbol_10(),
	NumberFormatInfo_t104580544::get_offset_of_ansiCurrencySymbol_11(),
	NumberFormatInfo_t104580544::get_offset_of_nanSymbol_12(),
	NumberFormatInfo_t104580544::get_offset_of_positiveInfinitySymbol_13(),
	NumberFormatInfo_t104580544::get_offset_of_negativeInfinitySymbol_14(),
	NumberFormatInfo_t104580544::get_offset_of_percentDecimalSeparator_15(),
	NumberFormatInfo_t104580544::get_offset_of_percentGroupSeparator_16(),
	NumberFormatInfo_t104580544::get_offset_of_percentSymbol_17(),
	NumberFormatInfo_t104580544::get_offset_of_perMilleSymbol_18(),
	NumberFormatInfo_t104580544::get_offset_of_nativeDigits_19(),
	NumberFormatInfo_t104580544::get_offset_of_m_dataItem_20(),
	NumberFormatInfo_t104580544::get_offset_of_numberDecimalDigits_21(),
	NumberFormatInfo_t104580544::get_offset_of_currencyDecimalDigits_22(),
	NumberFormatInfo_t104580544::get_offset_of_currencyPositivePattern_23(),
	NumberFormatInfo_t104580544::get_offset_of_currencyNegativePattern_24(),
	NumberFormatInfo_t104580544::get_offset_of_numberNegativePattern_25(),
	NumberFormatInfo_t104580544::get_offset_of_percentPositivePattern_26(),
	NumberFormatInfo_t104580544::get_offset_of_percentNegativePattern_27(),
	NumberFormatInfo_t104580544::get_offset_of_percentDecimalDigits_28(),
	NumberFormatInfo_t104580544::get_offset_of_digitSubstitution_29(),
	NumberFormatInfo_t104580544::get_offset_of_isReadOnly_30(),
	NumberFormatInfo_t104580544::get_offset_of_m_useUserOverride_31(),
	NumberFormatInfo_t104580544::get_offset_of_m_isInvariant_32(),
	NumberFormatInfo_t104580544::get_offset_of_validForParseAsNumber_33(),
	NumberFormatInfo_t104580544::get_offset_of_validForParseAsCurrency_34(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize286 = { sizeof (NumberStyles_t3408984435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable286[18] = 
{
	NumberStyles_t3408984435::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize287 = { sizeof (TaiwanCalendar_t2121259800), -1, sizeof(TaiwanCalendar_t2121259800_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable287[4] = 
{
	TaiwanCalendar_t2121259800_StaticFields::get_offset_of_taiwanEraInfo_3(),
	TaiwanCalendar_t2121259800_StaticFields::get_offset_of_s_defaultInstance_4(),
	TaiwanCalendar_t2121259800::get_offset_of_helper_5(),
	TaiwanCalendar_t2121259800_StaticFields::get_offset_of_calendarMinValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize288 = { sizeof (TextInfo_t3620182823), -1, sizeof(TextInfo_t3620182823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable288[8] = 
{
	TextInfo_t3620182823::get_offset_of_m_isReadOnly_0(),
	TextInfo_t3620182823::get_offset_of_m_cultureName_1(),
	TextInfo_t3620182823::get_offset_of_m_cultureData_2(),
	TextInfo_t3620182823::get_offset_of_m_textInfoName_3(),
	TextInfo_t3620182823::get_offset_of_m_IsAsciiCasingSameAsInvariant_4(),
	TextInfo_t3620182823_StaticFields::get_offset_of_s_Invariant_5(),
	TextInfo_t3620182823::get_offset_of_customCultureName_6(),
	TextInfo_t3620182823::get_offset_of_m_win32LangID_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize289 = { sizeof (TimeSpanFormat_t1848991148), -1, sizeof(TimeSpanFormat_t1848991148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable289[2] = 
{
	TimeSpanFormat_t1848991148_StaticFields::get_offset_of_PositiveInvariantFormatLiterals_0(),
	TimeSpanFormat_t1848991148_StaticFields::get_offset_of_NegativeInvariantFormatLiterals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize290 = { sizeof (Pattern_t522564603)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable290[4] = 
{
	Pattern_t522564603::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize291 = { sizeof (FormatLiterals_t2087080800)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable291[7] = 
{
	FormatLiterals_t2087080800::get_offset_of_AppCompatLiteral_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_dd_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_hh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_mm_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_ss_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_ff_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatLiterals_t2087080800::get_offset_of_literals_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize292 = { sizeof (TimeSpanStyles_t2925399185)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable292[3] = 
{
	TimeSpanStyles_t2925399185::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize293 = { sizeof (TimeSpanParse_t3827434274), -1, sizeof(TimeSpanParse_t3827434274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable293[1] = 
{
	TimeSpanParse_t3827434274_StaticFields::get_offset_of_zero_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize294 = { sizeof (TimeSpanThrowStyle_t2652982273)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable294[3] = 
{
	TimeSpanThrowStyle_t2652982273::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize295 = { sizeof (ParseFailureKind_t2272087344)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable295[6] = 
{
	ParseFailureKind_t2272087344::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize296 = { sizeof (TimeSpanStandardStyles_t2692921727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable296[6] = 
{
	TimeSpanStandardStyles_t2692921727::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize297 = { sizeof (TTT_t150500429)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable297[6] = 
{
	TTT_t150500429::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize298 = { sizeof (TimeSpanToken_t2031659367)+ sizeof (Il2CppObject), sizeof(TimeSpanToken_t2031659367_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable298[4] = 
{
	TimeSpanToken_t2031659367::get_offset_of_ttt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanToken_t2031659367::get_offset_of_num_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanToken_t2031659367::get_offset_of_zeroes_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanToken_t2031659367::get_offset_of_sep_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize299 = { sizeof (TimeSpanTokenizer_t1845673523)+ sizeof (Il2CppObject), sizeof(TimeSpanTokenizer_t1845673523_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable299[2] = 
{
	TimeSpanTokenizer_t1845673523::get_offset_of_m_pos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanTokenizer_t1845673523::get_offset_of_m_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
