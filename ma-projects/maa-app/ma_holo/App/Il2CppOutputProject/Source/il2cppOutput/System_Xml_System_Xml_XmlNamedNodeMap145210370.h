﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap_SmallXmlNode4252813691.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t145210370  : public Il2CppObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t616554813 * ___parent_0;
	// System.Xml.XmlNamedNodeMap/SmallXmlNodeList System.Xml.XmlNamedNodeMap::nodes
	SmallXmlNodeList_t4252813691  ___nodes_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370, ___parent_0)); }
	inline XmlNode_t616554813 * get_parent_0() const { return ___parent_0; }
	inline XmlNode_t616554813 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XmlNode_t616554813 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370, ___nodes_1)); }
	inline SmallXmlNodeList_t4252813691  get_nodes_1() const { return ___nodes_1; }
	inline SmallXmlNodeList_t4252813691 * get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(SmallXmlNodeList_t4252813691  value)
	{
		___nodes_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
