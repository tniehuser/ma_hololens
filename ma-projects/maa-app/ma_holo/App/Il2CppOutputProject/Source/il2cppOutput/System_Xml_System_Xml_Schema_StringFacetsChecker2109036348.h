﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_FacetsChecker1235574227.h"

// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringFacetsChecker
struct  StringFacetsChecker_t2109036348  : public FacetsChecker_t1235574227
{
public:

public:
};

struct StringFacetsChecker_t2109036348_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex System.Xml.Schema.StringFacetsChecker::languagePattern
	Regex_t1803876613 * ___languagePattern_0;

public:
	inline static int32_t get_offset_of_languagePattern_0() { return static_cast<int32_t>(offsetof(StringFacetsChecker_t2109036348_StaticFields, ___languagePattern_0)); }
	inline Regex_t1803876613 * get_languagePattern_0() const { return ___languagePattern_0; }
	inline Regex_t1803876613 ** get_address_of_languagePattern_0() { return &___languagePattern_0; }
	inline void set_languagePattern_0(Regex_t1803876613 * value)
	{
		___languagePattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___languagePattern_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
