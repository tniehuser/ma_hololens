﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseProcessor2373158431.h"

// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Preprocessor
struct  Preprocessor_t4137165425  : public BaseProcessor_t2373158431
{
public:

public:
};

struct Preprocessor_t4137165425_StaticFields
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::builtInSchemaForXmlNS
	XmlSchema_t880472818 * ___builtInSchemaForXmlNS_6;

public:
	inline static int32_t get_offset_of_builtInSchemaForXmlNS_6() { return static_cast<int32_t>(offsetof(Preprocessor_t4137165425_StaticFields, ___builtInSchemaForXmlNS_6)); }
	inline XmlSchema_t880472818 * get_builtInSchemaForXmlNS_6() const { return ___builtInSchemaForXmlNS_6; }
	inline XmlSchema_t880472818 ** get_address_of_builtInSchemaForXmlNS_6() { return &___builtInSchemaForXmlNS_6; }
	inline void set_builtInSchemaForXmlNS_6(XmlSchema_t880472818 * value)
	{
		___builtInSchemaForXmlNS_6 = value;
		Il2CppCodeGenWriteBarrier(&___builtInSchemaForXmlNS_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
