﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaXPath
struct  XmlSchemaXPath_t604820427  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaXPath::xpath
	String_t* ___xpath_9;

public:
	inline static int32_t get_offset_of_xpath_9() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___xpath_9)); }
	inline String_t* get_xpath_9() const { return ___xpath_9; }
	inline String_t** get_address_of_xpath_9() { return &___xpath_9; }
	inline void set_xpath_9(String_t* value)
	{
		___xpath_9 = value;
		Il2CppCodeGenWriteBarrier(&___xpath_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
