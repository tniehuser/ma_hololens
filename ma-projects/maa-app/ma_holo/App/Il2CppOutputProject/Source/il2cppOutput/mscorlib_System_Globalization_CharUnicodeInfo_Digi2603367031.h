﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CharUnicodeInfo/DigitValues
#pragma pack(push, tp, 2)
struct  DigitValues_t2603367031 
{
public:
	// System.SByte System.Globalization.CharUnicodeInfo/DigitValues::decimalDigit
	int8_t ___decimalDigit_0;
	// System.SByte System.Globalization.CharUnicodeInfo/DigitValues::digit
	int8_t ___digit_1;

public:
	inline static int32_t get_offset_of_decimalDigit_0() { return static_cast<int32_t>(offsetof(DigitValues_t2603367031, ___decimalDigit_0)); }
	inline int8_t get_decimalDigit_0() const { return ___decimalDigit_0; }
	inline int8_t* get_address_of_decimalDigit_0() { return &___decimalDigit_0; }
	inline void set_decimalDigit_0(int8_t value)
	{
		___decimalDigit_0 = value;
	}

	inline static int32_t get_offset_of_digit_1() { return static_cast<int32_t>(offsetof(DigitValues_t2603367031, ___digit_1)); }
	inline int8_t get_digit_1() const { return ___digit_1; }
	inline int8_t* get_address_of_digit_1() { return &___digit_1; }
	inline void set_digit_1(int8_t value)
	{
		___digit_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
