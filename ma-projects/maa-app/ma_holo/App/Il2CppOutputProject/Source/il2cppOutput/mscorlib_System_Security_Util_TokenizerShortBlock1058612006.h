﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Security.Util.TokenizerShortBlock
struct TokenizerShortBlock_t1058612006;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.TokenizerShortBlock
struct  TokenizerShortBlock_t1058612006  : public Il2CppObject
{
public:
	// System.Int16[] System.Security.Util.TokenizerShortBlock::m_block
	Int16U5BU5D_t3104283263* ___m_block_0;
	// System.Security.Util.TokenizerShortBlock System.Security.Util.TokenizerShortBlock::m_next
	TokenizerShortBlock_t1058612006 * ___m_next_1;

public:
	inline static int32_t get_offset_of_m_block_0() { return static_cast<int32_t>(offsetof(TokenizerShortBlock_t1058612006, ___m_block_0)); }
	inline Int16U5BU5D_t3104283263* get_m_block_0() const { return ___m_block_0; }
	inline Int16U5BU5D_t3104283263** get_address_of_m_block_0() { return &___m_block_0; }
	inline void set_m_block_0(Int16U5BU5D_t3104283263* value)
	{
		___m_block_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_block_0, value);
	}

	inline static int32_t get_offset_of_m_next_1() { return static_cast<int32_t>(offsetof(TokenizerShortBlock_t1058612006, ___m_next_1)); }
	inline TokenizerShortBlock_t1058612006 * get_m_next_1() const { return ___m_next_1; }
	inline TokenizerShortBlock_t1058612006 ** get_address_of_m_next_1() { return &___m_next_1; }
	inline void set_m_next_1(TokenizerShortBlock_t1058612006 * value)
	{
		___m_next_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_next_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
