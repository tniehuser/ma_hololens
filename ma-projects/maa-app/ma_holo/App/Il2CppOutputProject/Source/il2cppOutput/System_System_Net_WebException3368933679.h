﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebExceptionInternalStatus1357294310.h"

// System.Net.WebResponse
struct WebResponse_t1895226051;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_t3368933679  : public InvalidOperationException_t721527559
{
public:
	// System.Net.WebExceptionStatus System.Net.WebException::m_Status
	int32_t ___m_Status_16;
	// System.Net.WebResponse System.Net.WebException::m_Response
	WebResponse_t1895226051 * ___m_Response_17;
	// System.Net.WebExceptionInternalStatus System.Net.WebException::m_InternalStatus
	int32_t ___m_InternalStatus_18;

public:
	inline static int32_t get_offset_of_m_Status_16() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_Status_16)); }
	inline int32_t get_m_Status_16() const { return ___m_Status_16; }
	inline int32_t* get_address_of_m_Status_16() { return &___m_Status_16; }
	inline void set_m_Status_16(int32_t value)
	{
		___m_Status_16 = value;
	}

	inline static int32_t get_offset_of_m_Response_17() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_Response_17)); }
	inline WebResponse_t1895226051 * get_m_Response_17() const { return ___m_Response_17; }
	inline WebResponse_t1895226051 ** get_address_of_m_Response_17() { return &___m_Response_17; }
	inline void set_m_Response_17(WebResponse_t1895226051 * value)
	{
		___m_Response_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_Response_17, value);
	}

	inline static int32_t get_offset_of_m_InternalStatus_18() { return static_cast<int32_t>(offsetof(WebException_t3368933679, ___m_InternalStatus_18)); }
	inline int32_t get_m_InternalStatus_18() const { return ___m_InternalStatus_18; }
	inline int32_t* get_address_of_m_InternalStatus_18() { return &___m_InternalStatus_18; }
	inline void set_m_InternalStatus_18(int32_t value)
	{
		___m_InternalStatus_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
