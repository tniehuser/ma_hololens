﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Concurrent.ConcurrentDictionary`2/Node<System.String,System.Object>[]
struct NodeU5BU5D_t1077299993;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentDictionary`2/Tables<System.String,System.Object>
struct  Tables_t3011375419  : public Il2CppObject
{
public:
	// System.Collections.Concurrent.ConcurrentDictionary`2/Node<TKey,TValue>[] System.Collections.Concurrent.ConcurrentDictionary`2/Tables::m_buckets
	NodeU5BU5D_t1077299993* ___m_buckets_0;
	// System.Object[] System.Collections.Concurrent.ConcurrentDictionary`2/Tables::m_locks
	ObjectU5BU5D_t3614634134* ___m_locks_1;
	// System.Int32[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Concurrent.ConcurrentDictionary`2/Tables::m_countPerLock
	Int32U5BU5D_t3030399641* ___m_countPerLock_2;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Concurrent.ConcurrentDictionary`2/Tables::m_comparer
	Il2CppObject* ___m_comparer_3;

public:
	inline static int32_t get_offset_of_m_buckets_0() { return static_cast<int32_t>(offsetof(Tables_t3011375419, ___m_buckets_0)); }
	inline NodeU5BU5D_t1077299993* get_m_buckets_0() const { return ___m_buckets_0; }
	inline NodeU5BU5D_t1077299993** get_address_of_m_buckets_0() { return &___m_buckets_0; }
	inline void set_m_buckets_0(NodeU5BU5D_t1077299993* value)
	{
		___m_buckets_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_buckets_0, value);
	}

	inline static int32_t get_offset_of_m_locks_1() { return static_cast<int32_t>(offsetof(Tables_t3011375419, ___m_locks_1)); }
	inline ObjectU5BU5D_t3614634134* get_m_locks_1() const { return ___m_locks_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_locks_1() { return &___m_locks_1; }
	inline void set_m_locks_1(ObjectU5BU5D_t3614634134* value)
	{
		___m_locks_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_locks_1, value);
	}

	inline static int32_t get_offset_of_m_countPerLock_2() { return static_cast<int32_t>(offsetof(Tables_t3011375419, ___m_countPerLock_2)); }
	inline Int32U5BU5D_t3030399641* get_m_countPerLock_2() const { return ___m_countPerLock_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_countPerLock_2() { return &___m_countPerLock_2; }
	inline void set_m_countPerLock_2(Int32U5BU5D_t3030399641* value)
	{
		___m_countPerLock_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_countPerLock_2, value);
	}

	inline static int32_t get_offset_of_m_comparer_3() { return static_cast<int32_t>(offsetof(Tables_t3011375419, ___m_comparer_3)); }
	inline Il2CppObject* get_m_comparer_3() const { return ___m_comparer_3; }
	inline Il2CppObject** get_address_of_m_comparer_3() { return &___m_comparer_3; }
	inline void set_m_comparer_3(Il2CppObject* value)
	{
		___m_comparer_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_comparer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
