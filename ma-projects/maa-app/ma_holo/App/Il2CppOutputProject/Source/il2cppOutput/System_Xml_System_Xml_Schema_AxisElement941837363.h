﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.DoubleLinkAxis
struct DoubleLinkAxis_t3164907012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisElement
struct  AxisElement_t941837363  : public Il2CppObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.AxisElement::curNode
	DoubleLinkAxis_t3164907012 * ___curNode_0;
	// System.Int32 System.Xml.Schema.AxisElement::rootDepth
	int32_t ___rootDepth_1;
	// System.Int32 System.Xml.Schema.AxisElement::curDepth
	int32_t ___curDepth_2;
	// System.Boolean System.Xml.Schema.AxisElement::isMatch
	bool ___isMatch_3;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___curNode_0)); }
	inline DoubleLinkAxis_t3164907012 * get_curNode_0() const { return ___curNode_0; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(DoubleLinkAxis_t3164907012 * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___curNode_0, value);
	}

	inline static int32_t get_offset_of_rootDepth_1() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___rootDepth_1)); }
	inline int32_t get_rootDepth_1() const { return ___rootDepth_1; }
	inline int32_t* get_address_of_rootDepth_1() { return &___rootDepth_1; }
	inline void set_rootDepth_1(int32_t value)
	{
		___rootDepth_1 = value;
	}

	inline static int32_t get_offset_of_curDepth_2() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___curDepth_2)); }
	inline int32_t get_curDepth_2() const { return ___curDepth_2; }
	inline int32_t* get_address_of_curDepth_2() { return &___curDepth_2; }
	inline void set_curDepth_2(int32_t value)
	{
		___curDepth_2 = value;
	}

	inline static int32_t get_offset_of_isMatch_3() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___isMatch_3)); }
	inline bool get_isMatch_3() const { return ___isMatch_3; }
	inline bool* get_address_of_isMatch_3() { return &___isMatch_3; }
	inline void set_isMatch_3(bool value)
	{
		___isMatch_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
