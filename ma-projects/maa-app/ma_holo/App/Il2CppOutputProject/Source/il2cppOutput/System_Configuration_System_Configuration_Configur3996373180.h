﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"

// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement/SaveContext
struct  SaveContext_t3996373180  : public Il2CppObject
{
public:
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElement/SaveContext::Element
	ConfigurationElement_t1776195828 * ___Element_0;
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElement/SaveContext::Parent
	ConfigurationElement_t1776195828 * ___Parent_1;
	// System.Configuration.ConfigurationSaveMode System.Configuration.ConfigurationElement/SaveContext::Mode
	int32_t ___Mode_2;

public:
	inline static int32_t get_offset_of_Element_0() { return static_cast<int32_t>(offsetof(SaveContext_t3996373180, ___Element_0)); }
	inline ConfigurationElement_t1776195828 * get_Element_0() const { return ___Element_0; }
	inline ConfigurationElement_t1776195828 ** get_address_of_Element_0() { return &___Element_0; }
	inline void set_Element_0(ConfigurationElement_t1776195828 * value)
	{
		___Element_0 = value;
		Il2CppCodeGenWriteBarrier(&___Element_0, value);
	}

	inline static int32_t get_offset_of_Parent_1() { return static_cast<int32_t>(offsetof(SaveContext_t3996373180, ___Parent_1)); }
	inline ConfigurationElement_t1776195828 * get_Parent_1() const { return ___Parent_1; }
	inline ConfigurationElement_t1776195828 ** get_address_of_Parent_1() { return &___Parent_1; }
	inline void set_Parent_1(ConfigurationElement_t1776195828 * value)
	{
		___Parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_1, value);
	}

	inline static int32_t get_offset_of_Mode_2() { return static_cast<int32_t>(offsetof(SaveContext_t3996373180, ___Mode_2)); }
	inline int32_t get_Mode_2() const { return ___Mode_2; }
	inline int32_t* get_address_of_Mode_2() { return &___Mode_2; }
	inline void set_Mode_2(int32_t value)
	{
		___Mode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
