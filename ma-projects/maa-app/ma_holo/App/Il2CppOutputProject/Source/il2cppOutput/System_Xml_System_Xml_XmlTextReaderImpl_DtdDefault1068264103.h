﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer
struct  DtdDefaultAttributeInfoToNodeDataComparer_t1068264103  : public Il2CppObject
{
public:

public:
};

struct DtdDefaultAttributeInfoToNodeDataComparer_t1068264103_StaticFields
{
public:
	// System.Collections.Generic.IComparer`1<System.Object> System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer::s_instance
	Il2CppObject* ___s_instance_0;

public:
	inline static int32_t get_offset_of_s_instance_0() { return static_cast<int32_t>(offsetof(DtdDefaultAttributeInfoToNodeDataComparer_t1068264103_StaticFields, ___s_instance_0)); }
	inline Il2CppObject* get_s_instance_0() const { return ___s_instance_0; }
	inline Il2CppObject** get_address_of_s_instance_0() { return &___s_instance_0; }
	inline void set_s_instance_0(Il2CppObject* value)
	{
		___s_instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
