﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_InvalidOperationException721527559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t2695136451  : public InvalidOperationException_t721527559
{
public:
	// System.String System.ObjectDisposedException::objectName
	String_t* ___objectName_16;

public:
	inline static int32_t get_offset_of_objectName_16() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2695136451, ___objectName_16)); }
	inline String_t* get_objectName_16() const { return ___objectName_16; }
	inline String_t** get_address_of_objectName_16() { return &___objectName_16; }
	inline void set_objectName_16(String_t* value)
	{
		___objectName_16 = value;
		Il2CppCodeGenWriteBarrier(&___objectName_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
