﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Security_Mono_Xml_MiniParser185565106.h"

// System.Security.SecurityElement
struct SecurityElement_t2325568386;
// System.String
struct String_t;
// System.Collections.Stack
struct Stack_t1043988394;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.SecurityParser
struct  SecurityParser_t30730986  : public MiniParser_t185565106
{
public:
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t2325568386 * ___root_7;
	// System.String Mono.Xml.SecurityParser::xmldoc
	String_t* ___xmldoc_8;
	// System.Int32 Mono.Xml.SecurityParser::pos
	int32_t ___pos_9;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t2325568386 * ___current_10;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t1043988394 * ___stack_11;

public:
	inline static int32_t get_offset_of_root_7() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___root_7)); }
	inline SecurityElement_t2325568386 * get_root_7() const { return ___root_7; }
	inline SecurityElement_t2325568386 ** get_address_of_root_7() { return &___root_7; }
	inline void set_root_7(SecurityElement_t2325568386 * value)
	{
		___root_7 = value;
		Il2CppCodeGenWriteBarrier(&___root_7, value);
	}

	inline static int32_t get_offset_of_xmldoc_8() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___xmldoc_8)); }
	inline String_t* get_xmldoc_8() const { return ___xmldoc_8; }
	inline String_t** get_address_of_xmldoc_8() { return &___xmldoc_8; }
	inline void set_xmldoc_8(String_t* value)
	{
		___xmldoc_8 = value;
		Il2CppCodeGenWriteBarrier(&___xmldoc_8, value);
	}

	inline static int32_t get_offset_of_pos_9() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___pos_9)); }
	inline int32_t get_pos_9() const { return ___pos_9; }
	inline int32_t* get_address_of_pos_9() { return &___pos_9; }
	inline void set_pos_9(int32_t value)
	{
		___pos_9 = value;
	}

	inline static int32_t get_offset_of_current_10() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___current_10)); }
	inline SecurityElement_t2325568386 * get_current_10() const { return ___current_10; }
	inline SecurityElement_t2325568386 ** get_address_of_current_10() { return &___current_10; }
	inline void set_current_10(SecurityElement_t2325568386 * value)
	{
		___current_10 = value;
		Il2CppCodeGenWriteBarrier(&___current_10, value);
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(SecurityParser_t30730986, ___stack_11)); }
	inline Stack_t1043988394 * get_stack_11() const { return ___stack_11; }
	inline Stack_t1043988394 ** get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(Stack_t1043988394 * value)
	{
		___stack_11 = value;
		Il2CppCodeGenWriteBarrier(&___stack_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
