﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.EventHandlerList/ListEntry
struct ListEntry_t385037026;
// System.ComponentModel.Component
struct Component_t2826673791;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventHandlerList
struct  EventHandlerList_t1298116880  : public Il2CppObject
{
public:
	// System.ComponentModel.EventHandlerList/ListEntry System.ComponentModel.EventHandlerList::head
	ListEntry_t385037026 * ___head_0;
	// System.ComponentModel.Component System.ComponentModel.EventHandlerList::parent
	Component_t2826673791 * ___parent_1;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(EventHandlerList_t1298116880, ___head_0)); }
	inline ListEntry_t385037026 * get_head_0() const { return ___head_0; }
	inline ListEntry_t385037026 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(ListEntry_t385037026 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier(&___head_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(EventHandlerList_t1298116880, ___parent_1)); }
	inline Component_t2826673791 * get_parent_1() const { return ___parent_1; }
	inline Component_t2826673791 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Component_t2826673791 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
