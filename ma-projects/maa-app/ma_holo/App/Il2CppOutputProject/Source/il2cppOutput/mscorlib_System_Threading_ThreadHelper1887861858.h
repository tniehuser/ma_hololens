﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Delegate
struct Delegate_t3022476291;
// System.Object
struct Il2CppObject;
// System.Threading.ExecutionContext
struct ExecutionContext_t1392266323;
// System.Threading.ContextCallback
struct ContextCallback_t2287130692;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadHelper
struct  ThreadHelper_t1887861858  : public Il2CppObject
{
public:
	// System.Delegate System.Threading.ThreadHelper::_start
	Delegate_t3022476291 * ____start_0;
	// System.Object System.Threading.ThreadHelper::_startArg
	Il2CppObject * ____startArg_1;
	// System.Threading.ExecutionContext System.Threading.ThreadHelper::_executionContext
	ExecutionContext_t1392266323 * ____executionContext_2;

public:
	inline static int32_t get_offset_of__start_0() { return static_cast<int32_t>(offsetof(ThreadHelper_t1887861858, ____start_0)); }
	inline Delegate_t3022476291 * get__start_0() const { return ____start_0; }
	inline Delegate_t3022476291 ** get_address_of__start_0() { return &____start_0; }
	inline void set__start_0(Delegate_t3022476291 * value)
	{
		____start_0 = value;
		Il2CppCodeGenWriteBarrier(&____start_0, value);
	}

	inline static int32_t get_offset_of__startArg_1() { return static_cast<int32_t>(offsetof(ThreadHelper_t1887861858, ____startArg_1)); }
	inline Il2CppObject * get__startArg_1() const { return ____startArg_1; }
	inline Il2CppObject ** get_address_of__startArg_1() { return &____startArg_1; }
	inline void set__startArg_1(Il2CppObject * value)
	{
		____startArg_1 = value;
		Il2CppCodeGenWriteBarrier(&____startArg_1, value);
	}

	inline static int32_t get_offset_of__executionContext_2() { return static_cast<int32_t>(offsetof(ThreadHelper_t1887861858, ____executionContext_2)); }
	inline ExecutionContext_t1392266323 * get__executionContext_2() const { return ____executionContext_2; }
	inline ExecutionContext_t1392266323 ** get_address_of__executionContext_2() { return &____executionContext_2; }
	inline void set__executionContext_2(ExecutionContext_t1392266323 * value)
	{
		____executionContext_2 = value;
		Il2CppCodeGenWriteBarrier(&____executionContext_2, value);
	}
};

struct ThreadHelper_t1887861858_StaticFields
{
public:
	// System.Threading.ContextCallback System.Threading.ThreadHelper::_ccb
	ContextCallback_t2287130692 * ____ccb_3;

public:
	inline static int32_t get_offset_of__ccb_3() { return static_cast<int32_t>(offsetof(ThreadHelper_t1887861858_StaticFields, ____ccb_3)); }
	inline ContextCallback_t2287130692 * get__ccb_3() const { return ____ccb_3; }
	inline ContextCallback_t2287130692 ** get_address_of__ccb_3() { return &____ccb_3; }
	inline void set__ccb_3(ContextCallback_t2287130692 * value)
	{
		____ccb_3 = value;
		Il2CppCodeGenWriteBarrier(&____ccb_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
