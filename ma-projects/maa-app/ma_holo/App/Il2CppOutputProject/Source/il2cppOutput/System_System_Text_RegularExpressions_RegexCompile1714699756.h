﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"

// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t99948092;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t2116499186;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t2469392150;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t2204811018;
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t196522893;
// System.Text.RegularExpressions.RegexCompiler/BacktrackNote[]
struct BacktrackNoteU5BU5D_t2230749527;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCompiler
struct  RegexCompiler_t1714699756  : public Il2CppObject
{
public:
	// System.Reflection.Emit.ILGenerator System.Text.RegularExpressions.RegexCompiler::_ilg
	ILGenerator_t99948092 * ____ilg_26;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textstartV
	LocalBuilder_t2116499186 * ____textstartV_27;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textbegV
	LocalBuilder_t2116499186 * ____textbegV_28;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textendV
	LocalBuilder_t2116499186 * ____textendV_29;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textposV
	LocalBuilder_t2116499186 * ____textposV_30;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_textV
	LocalBuilder_t2116499186 * ____textV_31;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_trackposV
	LocalBuilder_t2116499186 * ____trackposV_32;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_trackV
	LocalBuilder_t2116499186 * ____trackV_33;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_stackposV
	LocalBuilder_t2116499186 * ____stackposV_34;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_stackV
	LocalBuilder_t2116499186 * ____stackV_35;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_tempV
	LocalBuilder_t2116499186 * ____tempV_36;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_temp2V
	LocalBuilder_t2116499186 * ____temp2V_37;
	// System.Reflection.Emit.LocalBuilder System.Text.RegularExpressions.RegexCompiler::_temp3V
	LocalBuilder_t2116499186 * ____temp3V_38;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexCompiler::_code
	RegexCode_t2469392150 * ____code_39;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_codes
	Int32U5BU5D_t3030399641* ____codes_40;
	// System.String[] System.Text.RegularExpressions.RegexCompiler::_strings
	StringU5BU5D_t1642385972* ____strings_41;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCompiler::_fcPrefix
	RegexPrefix_t1013837165 * ____fcPrefix_42;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCompiler::_bmPrefix
	RegexBoyerMoore_t2204811018 * ____bmPrefix_43;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_anchors
	int32_t ____anchors_44;
	// System.Reflection.Emit.Label[] System.Text.RegularExpressions.RegexCompiler::_labels
	LabelU5BU5D_t196522893* ____labels_45;
	// System.Text.RegularExpressions.RegexCompiler/BacktrackNote[] System.Text.RegularExpressions.RegexCompiler::_notes
	BacktrackNoteU5BU5D_t2230749527* ____notes_46;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_notecount
	int32_t ____notecount_47;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_trackcount
	int32_t ____trackcount_48;
	// System.Reflection.Emit.Label System.Text.RegularExpressions.RegexCompiler::_backtrack
	Label_t4243202660  ____backtrack_49;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_regexopcode
	int32_t ____regexopcode_50;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_codepos
	int32_t ____codepos_51;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler::_backpos
	int32_t ____backpos_52;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexCompiler::_options
	int32_t ____options_53;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_uniquenote
	Int32U5BU5D_t3030399641* ____uniquenote_54;
	// System.Int32[] System.Text.RegularExpressions.RegexCompiler::_goto
	Int32U5BU5D_t3030399641* ____goto_55;

public:
	inline static int32_t get_offset_of__ilg_26() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____ilg_26)); }
	inline ILGenerator_t99948092 * get__ilg_26() const { return ____ilg_26; }
	inline ILGenerator_t99948092 ** get_address_of__ilg_26() { return &____ilg_26; }
	inline void set__ilg_26(ILGenerator_t99948092 * value)
	{
		____ilg_26 = value;
		Il2CppCodeGenWriteBarrier(&____ilg_26, value);
	}

	inline static int32_t get_offset_of__textstartV_27() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textstartV_27)); }
	inline LocalBuilder_t2116499186 * get__textstartV_27() const { return ____textstartV_27; }
	inline LocalBuilder_t2116499186 ** get_address_of__textstartV_27() { return &____textstartV_27; }
	inline void set__textstartV_27(LocalBuilder_t2116499186 * value)
	{
		____textstartV_27 = value;
		Il2CppCodeGenWriteBarrier(&____textstartV_27, value);
	}

	inline static int32_t get_offset_of__textbegV_28() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textbegV_28)); }
	inline LocalBuilder_t2116499186 * get__textbegV_28() const { return ____textbegV_28; }
	inline LocalBuilder_t2116499186 ** get_address_of__textbegV_28() { return &____textbegV_28; }
	inline void set__textbegV_28(LocalBuilder_t2116499186 * value)
	{
		____textbegV_28 = value;
		Il2CppCodeGenWriteBarrier(&____textbegV_28, value);
	}

	inline static int32_t get_offset_of__textendV_29() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textendV_29)); }
	inline LocalBuilder_t2116499186 * get__textendV_29() const { return ____textendV_29; }
	inline LocalBuilder_t2116499186 ** get_address_of__textendV_29() { return &____textendV_29; }
	inline void set__textendV_29(LocalBuilder_t2116499186 * value)
	{
		____textendV_29 = value;
		Il2CppCodeGenWriteBarrier(&____textendV_29, value);
	}

	inline static int32_t get_offset_of__textposV_30() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textposV_30)); }
	inline LocalBuilder_t2116499186 * get__textposV_30() const { return ____textposV_30; }
	inline LocalBuilder_t2116499186 ** get_address_of__textposV_30() { return &____textposV_30; }
	inline void set__textposV_30(LocalBuilder_t2116499186 * value)
	{
		____textposV_30 = value;
		Il2CppCodeGenWriteBarrier(&____textposV_30, value);
	}

	inline static int32_t get_offset_of__textV_31() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____textV_31)); }
	inline LocalBuilder_t2116499186 * get__textV_31() const { return ____textV_31; }
	inline LocalBuilder_t2116499186 ** get_address_of__textV_31() { return &____textV_31; }
	inline void set__textV_31(LocalBuilder_t2116499186 * value)
	{
		____textV_31 = value;
		Il2CppCodeGenWriteBarrier(&____textV_31, value);
	}

	inline static int32_t get_offset_of__trackposV_32() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackposV_32)); }
	inline LocalBuilder_t2116499186 * get__trackposV_32() const { return ____trackposV_32; }
	inline LocalBuilder_t2116499186 ** get_address_of__trackposV_32() { return &____trackposV_32; }
	inline void set__trackposV_32(LocalBuilder_t2116499186 * value)
	{
		____trackposV_32 = value;
		Il2CppCodeGenWriteBarrier(&____trackposV_32, value);
	}

	inline static int32_t get_offset_of__trackV_33() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackV_33)); }
	inline LocalBuilder_t2116499186 * get__trackV_33() const { return ____trackV_33; }
	inline LocalBuilder_t2116499186 ** get_address_of__trackV_33() { return &____trackV_33; }
	inline void set__trackV_33(LocalBuilder_t2116499186 * value)
	{
		____trackV_33 = value;
		Il2CppCodeGenWriteBarrier(&____trackV_33, value);
	}

	inline static int32_t get_offset_of__stackposV_34() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____stackposV_34)); }
	inline LocalBuilder_t2116499186 * get__stackposV_34() const { return ____stackposV_34; }
	inline LocalBuilder_t2116499186 ** get_address_of__stackposV_34() { return &____stackposV_34; }
	inline void set__stackposV_34(LocalBuilder_t2116499186 * value)
	{
		____stackposV_34 = value;
		Il2CppCodeGenWriteBarrier(&____stackposV_34, value);
	}

	inline static int32_t get_offset_of__stackV_35() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____stackV_35)); }
	inline LocalBuilder_t2116499186 * get__stackV_35() const { return ____stackV_35; }
	inline LocalBuilder_t2116499186 ** get_address_of__stackV_35() { return &____stackV_35; }
	inline void set__stackV_35(LocalBuilder_t2116499186 * value)
	{
		____stackV_35 = value;
		Il2CppCodeGenWriteBarrier(&____stackV_35, value);
	}

	inline static int32_t get_offset_of__tempV_36() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____tempV_36)); }
	inline LocalBuilder_t2116499186 * get__tempV_36() const { return ____tempV_36; }
	inline LocalBuilder_t2116499186 ** get_address_of__tempV_36() { return &____tempV_36; }
	inline void set__tempV_36(LocalBuilder_t2116499186 * value)
	{
		____tempV_36 = value;
		Il2CppCodeGenWriteBarrier(&____tempV_36, value);
	}

	inline static int32_t get_offset_of__temp2V_37() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____temp2V_37)); }
	inline LocalBuilder_t2116499186 * get__temp2V_37() const { return ____temp2V_37; }
	inline LocalBuilder_t2116499186 ** get_address_of__temp2V_37() { return &____temp2V_37; }
	inline void set__temp2V_37(LocalBuilder_t2116499186 * value)
	{
		____temp2V_37 = value;
		Il2CppCodeGenWriteBarrier(&____temp2V_37, value);
	}

	inline static int32_t get_offset_of__temp3V_38() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____temp3V_38)); }
	inline LocalBuilder_t2116499186 * get__temp3V_38() const { return ____temp3V_38; }
	inline LocalBuilder_t2116499186 ** get_address_of__temp3V_38() { return &____temp3V_38; }
	inline void set__temp3V_38(LocalBuilder_t2116499186 * value)
	{
		____temp3V_38 = value;
		Il2CppCodeGenWriteBarrier(&____temp3V_38, value);
	}

	inline static int32_t get_offset_of__code_39() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____code_39)); }
	inline RegexCode_t2469392150 * get__code_39() const { return ____code_39; }
	inline RegexCode_t2469392150 ** get_address_of__code_39() { return &____code_39; }
	inline void set__code_39(RegexCode_t2469392150 * value)
	{
		____code_39 = value;
		Il2CppCodeGenWriteBarrier(&____code_39, value);
	}

	inline static int32_t get_offset_of__codes_40() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____codes_40)); }
	inline Int32U5BU5D_t3030399641* get__codes_40() const { return ____codes_40; }
	inline Int32U5BU5D_t3030399641** get_address_of__codes_40() { return &____codes_40; }
	inline void set__codes_40(Int32U5BU5D_t3030399641* value)
	{
		____codes_40 = value;
		Il2CppCodeGenWriteBarrier(&____codes_40, value);
	}

	inline static int32_t get_offset_of__strings_41() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____strings_41)); }
	inline StringU5BU5D_t1642385972* get__strings_41() const { return ____strings_41; }
	inline StringU5BU5D_t1642385972** get_address_of__strings_41() { return &____strings_41; }
	inline void set__strings_41(StringU5BU5D_t1642385972* value)
	{
		____strings_41 = value;
		Il2CppCodeGenWriteBarrier(&____strings_41, value);
	}

	inline static int32_t get_offset_of__fcPrefix_42() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____fcPrefix_42)); }
	inline RegexPrefix_t1013837165 * get__fcPrefix_42() const { return ____fcPrefix_42; }
	inline RegexPrefix_t1013837165 ** get_address_of__fcPrefix_42() { return &____fcPrefix_42; }
	inline void set__fcPrefix_42(RegexPrefix_t1013837165 * value)
	{
		____fcPrefix_42 = value;
		Il2CppCodeGenWriteBarrier(&____fcPrefix_42, value);
	}

	inline static int32_t get_offset_of__bmPrefix_43() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____bmPrefix_43)); }
	inline RegexBoyerMoore_t2204811018 * get__bmPrefix_43() const { return ____bmPrefix_43; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of__bmPrefix_43() { return &____bmPrefix_43; }
	inline void set__bmPrefix_43(RegexBoyerMoore_t2204811018 * value)
	{
		____bmPrefix_43 = value;
		Il2CppCodeGenWriteBarrier(&____bmPrefix_43, value);
	}

	inline static int32_t get_offset_of__anchors_44() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____anchors_44)); }
	inline int32_t get__anchors_44() const { return ____anchors_44; }
	inline int32_t* get_address_of__anchors_44() { return &____anchors_44; }
	inline void set__anchors_44(int32_t value)
	{
		____anchors_44 = value;
	}

	inline static int32_t get_offset_of__labels_45() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____labels_45)); }
	inline LabelU5BU5D_t196522893* get__labels_45() const { return ____labels_45; }
	inline LabelU5BU5D_t196522893** get_address_of__labels_45() { return &____labels_45; }
	inline void set__labels_45(LabelU5BU5D_t196522893* value)
	{
		____labels_45 = value;
		Il2CppCodeGenWriteBarrier(&____labels_45, value);
	}

	inline static int32_t get_offset_of__notes_46() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____notes_46)); }
	inline BacktrackNoteU5BU5D_t2230749527* get__notes_46() const { return ____notes_46; }
	inline BacktrackNoteU5BU5D_t2230749527** get_address_of__notes_46() { return &____notes_46; }
	inline void set__notes_46(BacktrackNoteU5BU5D_t2230749527* value)
	{
		____notes_46 = value;
		Il2CppCodeGenWriteBarrier(&____notes_46, value);
	}

	inline static int32_t get_offset_of__notecount_47() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____notecount_47)); }
	inline int32_t get__notecount_47() const { return ____notecount_47; }
	inline int32_t* get_address_of__notecount_47() { return &____notecount_47; }
	inline void set__notecount_47(int32_t value)
	{
		____notecount_47 = value;
	}

	inline static int32_t get_offset_of__trackcount_48() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____trackcount_48)); }
	inline int32_t get__trackcount_48() const { return ____trackcount_48; }
	inline int32_t* get_address_of__trackcount_48() { return &____trackcount_48; }
	inline void set__trackcount_48(int32_t value)
	{
		____trackcount_48 = value;
	}

	inline static int32_t get_offset_of__backtrack_49() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____backtrack_49)); }
	inline Label_t4243202660  get__backtrack_49() const { return ____backtrack_49; }
	inline Label_t4243202660 * get_address_of__backtrack_49() { return &____backtrack_49; }
	inline void set__backtrack_49(Label_t4243202660  value)
	{
		____backtrack_49 = value;
	}

	inline static int32_t get_offset_of__regexopcode_50() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____regexopcode_50)); }
	inline int32_t get__regexopcode_50() const { return ____regexopcode_50; }
	inline int32_t* get_address_of__regexopcode_50() { return &____regexopcode_50; }
	inline void set__regexopcode_50(int32_t value)
	{
		____regexopcode_50 = value;
	}

	inline static int32_t get_offset_of__codepos_51() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____codepos_51)); }
	inline int32_t get__codepos_51() const { return ____codepos_51; }
	inline int32_t* get_address_of__codepos_51() { return &____codepos_51; }
	inline void set__codepos_51(int32_t value)
	{
		____codepos_51 = value;
	}

	inline static int32_t get_offset_of__backpos_52() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____backpos_52)); }
	inline int32_t get__backpos_52() const { return ____backpos_52; }
	inline int32_t* get_address_of__backpos_52() { return &____backpos_52; }
	inline void set__backpos_52(int32_t value)
	{
		____backpos_52 = value;
	}

	inline static int32_t get_offset_of__options_53() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____options_53)); }
	inline int32_t get__options_53() const { return ____options_53; }
	inline int32_t* get_address_of__options_53() { return &____options_53; }
	inline void set__options_53(int32_t value)
	{
		____options_53 = value;
	}

	inline static int32_t get_offset_of__uniquenote_54() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____uniquenote_54)); }
	inline Int32U5BU5D_t3030399641* get__uniquenote_54() const { return ____uniquenote_54; }
	inline Int32U5BU5D_t3030399641** get_address_of__uniquenote_54() { return &____uniquenote_54; }
	inline void set__uniquenote_54(Int32U5BU5D_t3030399641* value)
	{
		____uniquenote_54 = value;
		Il2CppCodeGenWriteBarrier(&____uniquenote_54, value);
	}

	inline static int32_t get_offset_of__goto_55() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756, ____goto_55)); }
	inline Int32U5BU5D_t3030399641* get__goto_55() const { return ____goto_55; }
	inline Int32U5BU5D_t3030399641** get_address_of__goto_55() { return &____goto_55; }
	inline void set__goto_55(Int32U5BU5D_t3030399641* value)
	{
		____goto_55 = value;
		Il2CppCodeGenWriteBarrier(&____goto_55, value);
	}
};

struct RegexCompiler_t1714699756_StaticFields
{
public:
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textbegF
	FieldInfo_t * ____textbegF_0;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textendF
	FieldInfo_t * ____textendF_1;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textstartF
	FieldInfo_t * ____textstartF_2;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textposF
	FieldInfo_t * ____textposF_3;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_textF
	FieldInfo_t * ____textF_4;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackposF
	FieldInfo_t * ____trackposF_5;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackF
	FieldInfo_t * ____trackF_6;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_stackposF
	FieldInfo_t * ____stackposF_7;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_stackF
	FieldInfo_t * ____stackF_8;
	// System.Reflection.FieldInfo System.Text.RegularExpressions.RegexCompiler::_trackcountF
	FieldInfo_t * ____trackcountF_9;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_ensurestorageM
	MethodInfo_t * ____ensurestorageM_10;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_captureM
	MethodInfo_t * ____captureM_11;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_transferM
	MethodInfo_t * ____transferM_12;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_uncaptureM
	MethodInfo_t * ____uncaptureM_13;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_ismatchedM
	MethodInfo_t * ____ismatchedM_14;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_matchlengthM
	MethodInfo_t * ____matchlengthM_15;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_matchindexM
	MethodInfo_t * ____matchindexM_16;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_isboundaryM
	MethodInfo_t * ____isboundaryM_17;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_isECMABoundaryM
	MethodInfo_t * ____isECMABoundaryM_18;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_chartolowerM
	MethodInfo_t * ____chartolowerM_19;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getcharM
	MethodInfo_t * ____getcharM_20;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_crawlposM
	MethodInfo_t * ____crawlposM_21;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_charInSetM
	MethodInfo_t * ____charInSetM_22;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getCurrentCulture
	MethodInfo_t * ____getCurrentCulture_23;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_getInvariantCulture
	MethodInfo_t * ____getInvariantCulture_24;
	// System.Reflection.MethodInfo System.Text.RegularExpressions.RegexCompiler::_checkTimeoutM
	MethodInfo_t * ____checkTimeoutM_25;

public:
	inline static int32_t get_offset_of__textbegF_0() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textbegF_0)); }
	inline FieldInfo_t * get__textbegF_0() const { return ____textbegF_0; }
	inline FieldInfo_t ** get_address_of__textbegF_0() { return &____textbegF_0; }
	inline void set__textbegF_0(FieldInfo_t * value)
	{
		____textbegF_0 = value;
		Il2CppCodeGenWriteBarrier(&____textbegF_0, value);
	}

	inline static int32_t get_offset_of__textendF_1() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textendF_1)); }
	inline FieldInfo_t * get__textendF_1() const { return ____textendF_1; }
	inline FieldInfo_t ** get_address_of__textendF_1() { return &____textendF_1; }
	inline void set__textendF_1(FieldInfo_t * value)
	{
		____textendF_1 = value;
		Il2CppCodeGenWriteBarrier(&____textendF_1, value);
	}

	inline static int32_t get_offset_of__textstartF_2() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textstartF_2)); }
	inline FieldInfo_t * get__textstartF_2() const { return ____textstartF_2; }
	inline FieldInfo_t ** get_address_of__textstartF_2() { return &____textstartF_2; }
	inline void set__textstartF_2(FieldInfo_t * value)
	{
		____textstartF_2 = value;
		Il2CppCodeGenWriteBarrier(&____textstartF_2, value);
	}

	inline static int32_t get_offset_of__textposF_3() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textposF_3)); }
	inline FieldInfo_t * get__textposF_3() const { return ____textposF_3; }
	inline FieldInfo_t ** get_address_of__textposF_3() { return &____textposF_3; }
	inline void set__textposF_3(FieldInfo_t * value)
	{
		____textposF_3 = value;
		Il2CppCodeGenWriteBarrier(&____textposF_3, value);
	}

	inline static int32_t get_offset_of__textF_4() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____textF_4)); }
	inline FieldInfo_t * get__textF_4() const { return ____textF_4; }
	inline FieldInfo_t ** get_address_of__textF_4() { return &____textF_4; }
	inline void set__textF_4(FieldInfo_t * value)
	{
		____textF_4 = value;
		Il2CppCodeGenWriteBarrier(&____textF_4, value);
	}

	inline static int32_t get_offset_of__trackposF_5() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackposF_5)); }
	inline FieldInfo_t * get__trackposF_5() const { return ____trackposF_5; }
	inline FieldInfo_t ** get_address_of__trackposF_5() { return &____trackposF_5; }
	inline void set__trackposF_5(FieldInfo_t * value)
	{
		____trackposF_5 = value;
		Il2CppCodeGenWriteBarrier(&____trackposF_5, value);
	}

	inline static int32_t get_offset_of__trackF_6() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackF_6)); }
	inline FieldInfo_t * get__trackF_6() const { return ____trackF_6; }
	inline FieldInfo_t ** get_address_of__trackF_6() { return &____trackF_6; }
	inline void set__trackF_6(FieldInfo_t * value)
	{
		____trackF_6 = value;
		Il2CppCodeGenWriteBarrier(&____trackF_6, value);
	}

	inline static int32_t get_offset_of__stackposF_7() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____stackposF_7)); }
	inline FieldInfo_t * get__stackposF_7() const { return ____stackposF_7; }
	inline FieldInfo_t ** get_address_of__stackposF_7() { return &____stackposF_7; }
	inline void set__stackposF_7(FieldInfo_t * value)
	{
		____stackposF_7 = value;
		Il2CppCodeGenWriteBarrier(&____stackposF_7, value);
	}

	inline static int32_t get_offset_of__stackF_8() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____stackF_8)); }
	inline FieldInfo_t * get__stackF_8() const { return ____stackF_8; }
	inline FieldInfo_t ** get_address_of__stackF_8() { return &____stackF_8; }
	inline void set__stackF_8(FieldInfo_t * value)
	{
		____stackF_8 = value;
		Il2CppCodeGenWriteBarrier(&____stackF_8, value);
	}

	inline static int32_t get_offset_of__trackcountF_9() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____trackcountF_9)); }
	inline FieldInfo_t * get__trackcountF_9() const { return ____trackcountF_9; }
	inline FieldInfo_t ** get_address_of__trackcountF_9() { return &____trackcountF_9; }
	inline void set__trackcountF_9(FieldInfo_t * value)
	{
		____trackcountF_9 = value;
		Il2CppCodeGenWriteBarrier(&____trackcountF_9, value);
	}

	inline static int32_t get_offset_of__ensurestorageM_10() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____ensurestorageM_10)); }
	inline MethodInfo_t * get__ensurestorageM_10() const { return ____ensurestorageM_10; }
	inline MethodInfo_t ** get_address_of__ensurestorageM_10() { return &____ensurestorageM_10; }
	inline void set__ensurestorageM_10(MethodInfo_t * value)
	{
		____ensurestorageM_10 = value;
		Il2CppCodeGenWriteBarrier(&____ensurestorageM_10, value);
	}

	inline static int32_t get_offset_of__captureM_11() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____captureM_11)); }
	inline MethodInfo_t * get__captureM_11() const { return ____captureM_11; }
	inline MethodInfo_t ** get_address_of__captureM_11() { return &____captureM_11; }
	inline void set__captureM_11(MethodInfo_t * value)
	{
		____captureM_11 = value;
		Il2CppCodeGenWriteBarrier(&____captureM_11, value);
	}

	inline static int32_t get_offset_of__transferM_12() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____transferM_12)); }
	inline MethodInfo_t * get__transferM_12() const { return ____transferM_12; }
	inline MethodInfo_t ** get_address_of__transferM_12() { return &____transferM_12; }
	inline void set__transferM_12(MethodInfo_t * value)
	{
		____transferM_12 = value;
		Il2CppCodeGenWriteBarrier(&____transferM_12, value);
	}

	inline static int32_t get_offset_of__uncaptureM_13() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____uncaptureM_13)); }
	inline MethodInfo_t * get__uncaptureM_13() const { return ____uncaptureM_13; }
	inline MethodInfo_t ** get_address_of__uncaptureM_13() { return &____uncaptureM_13; }
	inline void set__uncaptureM_13(MethodInfo_t * value)
	{
		____uncaptureM_13 = value;
		Il2CppCodeGenWriteBarrier(&____uncaptureM_13, value);
	}

	inline static int32_t get_offset_of__ismatchedM_14() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____ismatchedM_14)); }
	inline MethodInfo_t * get__ismatchedM_14() const { return ____ismatchedM_14; }
	inline MethodInfo_t ** get_address_of__ismatchedM_14() { return &____ismatchedM_14; }
	inline void set__ismatchedM_14(MethodInfo_t * value)
	{
		____ismatchedM_14 = value;
		Il2CppCodeGenWriteBarrier(&____ismatchedM_14, value);
	}

	inline static int32_t get_offset_of__matchlengthM_15() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____matchlengthM_15)); }
	inline MethodInfo_t * get__matchlengthM_15() const { return ____matchlengthM_15; }
	inline MethodInfo_t ** get_address_of__matchlengthM_15() { return &____matchlengthM_15; }
	inline void set__matchlengthM_15(MethodInfo_t * value)
	{
		____matchlengthM_15 = value;
		Il2CppCodeGenWriteBarrier(&____matchlengthM_15, value);
	}

	inline static int32_t get_offset_of__matchindexM_16() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____matchindexM_16)); }
	inline MethodInfo_t * get__matchindexM_16() const { return ____matchindexM_16; }
	inline MethodInfo_t ** get_address_of__matchindexM_16() { return &____matchindexM_16; }
	inline void set__matchindexM_16(MethodInfo_t * value)
	{
		____matchindexM_16 = value;
		Il2CppCodeGenWriteBarrier(&____matchindexM_16, value);
	}

	inline static int32_t get_offset_of__isboundaryM_17() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____isboundaryM_17)); }
	inline MethodInfo_t * get__isboundaryM_17() const { return ____isboundaryM_17; }
	inline MethodInfo_t ** get_address_of__isboundaryM_17() { return &____isboundaryM_17; }
	inline void set__isboundaryM_17(MethodInfo_t * value)
	{
		____isboundaryM_17 = value;
		Il2CppCodeGenWriteBarrier(&____isboundaryM_17, value);
	}

	inline static int32_t get_offset_of__isECMABoundaryM_18() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____isECMABoundaryM_18)); }
	inline MethodInfo_t * get__isECMABoundaryM_18() const { return ____isECMABoundaryM_18; }
	inline MethodInfo_t ** get_address_of__isECMABoundaryM_18() { return &____isECMABoundaryM_18; }
	inline void set__isECMABoundaryM_18(MethodInfo_t * value)
	{
		____isECMABoundaryM_18 = value;
		Il2CppCodeGenWriteBarrier(&____isECMABoundaryM_18, value);
	}

	inline static int32_t get_offset_of__chartolowerM_19() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____chartolowerM_19)); }
	inline MethodInfo_t * get__chartolowerM_19() const { return ____chartolowerM_19; }
	inline MethodInfo_t ** get_address_of__chartolowerM_19() { return &____chartolowerM_19; }
	inline void set__chartolowerM_19(MethodInfo_t * value)
	{
		____chartolowerM_19 = value;
		Il2CppCodeGenWriteBarrier(&____chartolowerM_19, value);
	}

	inline static int32_t get_offset_of__getcharM_20() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getcharM_20)); }
	inline MethodInfo_t * get__getcharM_20() const { return ____getcharM_20; }
	inline MethodInfo_t ** get_address_of__getcharM_20() { return &____getcharM_20; }
	inline void set__getcharM_20(MethodInfo_t * value)
	{
		____getcharM_20 = value;
		Il2CppCodeGenWriteBarrier(&____getcharM_20, value);
	}

	inline static int32_t get_offset_of__crawlposM_21() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____crawlposM_21)); }
	inline MethodInfo_t * get__crawlposM_21() const { return ____crawlposM_21; }
	inline MethodInfo_t ** get_address_of__crawlposM_21() { return &____crawlposM_21; }
	inline void set__crawlposM_21(MethodInfo_t * value)
	{
		____crawlposM_21 = value;
		Il2CppCodeGenWriteBarrier(&____crawlposM_21, value);
	}

	inline static int32_t get_offset_of__charInSetM_22() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____charInSetM_22)); }
	inline MethodInfo_t * get__charInSetM_22() const { return ____charInSetM_22; }
	inline MethodInfo_t ** get_address_of__charInSetM_22() { return &____charInSetM_22; }
	inline void set__charInSetM_22(MethodInfo_t * value)
	{
		____charInSetM_22 = value;
		Il2CppCodeGenWriteBarrier(&____charInSetM_22, value);
	}

	inline static int32_t get_offset_of__getCurrentCulture_23() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getCurrentCulture_23)); }
	inline MethodInfo_t * get__getCurrentCulture_23() const { return ____getCurrentCulture_23; }
	inline MethodInfo_t ** get_address_of__getCurrentCulture_23() { return &____getCurrentCulture_23; }
	inline void set__getCurrentCulture_23(MethodInfo_t * value)
	{
		____getCurrentCulture_23 = value;
		Il2CppCodeGenWriteBarrier(&____getCurrentCulture_23, value);
	}

	inline static int32_t get_offset_of__getInvariantCulture_24() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____getInvariantCulture_24)); }
	inline MethodInfo_t * get__getInvariantCulture_24() const { return ____getInvariantCulture_24; }
	inline MethodInfo_t ** get_address_of__getInvariantCulture_24() { return &____getInvariantCulture_24; }
	inline void set__getInvariantCulture_24(MethodInfo_t * value)
	{
		____getInvariantCulture_24 = value;
		Il2CppCodeGenWriteBarrier(&____getInvariantCulture_24, value);
	}

	inline static int32_t get_offset_of__checkTimeoutM_25() { return static_cast<int32_t>(offsetof(RegexCompiler_t1714699756_StaticFields, ____checkTimeoutM_25)); }
	inline MethodInfo_t * get__checkTimeoutM_25() const { return ____checkTimeoutM_25; }
	inline MethodInfo_t ** get_address_of__checkTimeoutM_25() { return &____checkTimeoutM_25; }
	inline void set__checkTimeoutM_25(MethodInfo_t * value)
	{
		____checkTimeoutM_25 = value;
		Il2CppCodeGenWriteBarrier(&____checkTimeoutM_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
