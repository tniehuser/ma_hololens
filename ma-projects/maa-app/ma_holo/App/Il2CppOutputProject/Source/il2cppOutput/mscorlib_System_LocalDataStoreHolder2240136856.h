﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.LocalDataStore
struct LocalDataStore_t228818476;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalDataStoreHolder
struct  LocalDataStoreHolder_t2240136856  : public Il2CppObject
{
public:
	// System.LocalDataStore System.LocalDataStoreHolder::m_Store
	LocalDataStore_t228818476 * ___m_Store_0;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(LocalDataStoreHolder_t2240136856, ___m_Store_0)); }
	inline LocalDataStore_t228818476 * get_m_Store_0() const { return ___m_Store_0; }
	inline LocalDataStore_t228818476 ** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(LocalDataStore_t228818476 * value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Store_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
