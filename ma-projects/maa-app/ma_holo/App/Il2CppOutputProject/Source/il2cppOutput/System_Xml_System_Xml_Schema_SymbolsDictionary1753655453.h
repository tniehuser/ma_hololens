﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SymbolsDictionary
struct  SymbolsDictionary_t1753655453  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.Schema.SymbolsDictionary::last
	int32_t ___last_0;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::names
	Hashtable_t909839986 * ___names_1;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::wildcards
	Hashtable_t909839986 * ___wildcards_2;
	// System.Collections.ArrayList System.Xml.Schema.SymbolsDictionary::particles
	ArrayList_t4252133567 * ___particles_3;
	// System.Object System.Xml.Schema.SymbolsDictionary::particleLast
	Il2CppObject * ___particleLast_4;
	// System.Boolean System.Xml.Schema.SymbolsDictionary::isUpaEnforced
	bool ___isUpaEnforced_5;

public:
	inline static int32_t get_offset_of_last_0() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___last_0)); }
	inline int32_t get_last_0() const { return ___last_0; }
	inline int32_t* get_address_of_last_0() { return &___last_0; }
	inline void set_last_0(int32_t value)
	{
		___last_0 = value;
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___names_1)); }
	inline Hashtable_t909839986 * get_names_1() const { return ___names_1; }
	inline Hashtable_t909839986 ** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(Hashtable_t909839986 * value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier(&___names_1, value);
	}

	inline static int32_t get_offset_of_wildcards_2() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___wildcards_2)); }
	inline Hashtable_t909839986 * get_wildcards_2() const { return ___wildcards_2; }
	inline Hashtable_t909839986 ** get_address_of_wildcards_2() { return &___wildcards_2; }
	inline void set_wildcards_2(Hashtable_t909839986 * value)
	{
		___wildcards_2 = value;
		Il2CppCodeGenWriteBarrier(&___wildcards_2, value);
	}

	inline static int32_t get_offset_of_particles_3() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___particles_3)); }
	inline ArrayList_t4252133567 * get_particles_3() const { return ___particles_3; }
	inline ArrayList_t4252133567 ** get_address_of_particles_3() { return &___particles_3; }
	inline void set_particles_3(ArrayList_t4252133567 * value)
	{
		___particles_3 = value;
		Il2CppCodeGenWriteBarrier(&___particles_3, value);
	}

	inline static int32_t get_offset_of_particleLast_4() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___particleLast_4)); }
	inline Il2CppObject * get_particleLast_4() const { return ___particleLast_4; }
	inline Il2CppObject ** get_address_of_particleLast_4() { return &___particleLast_4; }
	inline void set_particleLast_4(Il2CppObject * value)
	{
		___particleLast_4 = value;
		Il2CppCodeGenWriteBarrier(&___particleLast_4, value);
	}

	inline static int32_t get_offset_of_isUpaEnforced_5() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___isUpaEnforced_5)); }
	inline bool get_isUpaEnforced_5() const { return ___isUpaEnforced_5; }
	inline bool* get_address_of_isUpaEnforced_5() { return &___isUpaEnforced_5; }
	inline void set_isUpaEnforced_5(bool value)
	{
		___isUpaEnforced_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
