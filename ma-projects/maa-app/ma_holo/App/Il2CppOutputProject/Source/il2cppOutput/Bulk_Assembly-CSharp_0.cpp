﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_TESTING3255940056.h"
#include "mscorlib_System_Void1841601450.h"
#include "MetaArcade_Core_MetaArcade_Core_Signals_Signal1504483080.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// TESTING
struct TESTING_t3255940056;
// MetaArcade.Core.Signals.Signal
struct Signal_t1504483080;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
extern Il2CppClass* Signal_t1504483080_il2cpp_TypeInfo_var;
extern const uint32_t TESTING_Start_m3623630857_MetadataUsageId;




// System.Void MetaArcade.Core.Signals.Signal::.ctor()
extern "C"  void Signal__ctor_m1584521506 (Signal_t1504483080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TESTING::Start()
extern "C"  void TESTING_Start_m3623630857 (TESTING_t3255940056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TESTING_Start_m3623630857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Signal signal = new Signal();
		Signal_t1504483080 * L_0 = (Signal_t1504483080 *)il2cpp_codegen_object_new(Signal_t1504483080_il2cpp_TypeInfo_var);
		Signal__ctor_m1584521506(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TESTING::Update()
extern "C"  void TESTING_Update_m3398228710 (TESTING_t3255940056 * __this, const MethodInfo* method)
{
	{
		// }
		return;
	}
}
// System.Void TESTING::.ctor()
extern "C"  void TESTING__ctor_m2583951577 (TESTING_t3255940056 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
