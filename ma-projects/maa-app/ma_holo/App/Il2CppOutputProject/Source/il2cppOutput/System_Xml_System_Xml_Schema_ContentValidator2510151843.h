﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2874429441.h"

// System.Xml.Schema.ContentValidator
struct ContentValidator_t2510151843;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ContentValidator
struct  ContentValidator_t2510151843  : public Il2CppObject
{
public:
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.ContentValidator::contentType
	int32_t ___contentType_0;
	// System.Boolean System.Xml.Schema.ContentValidator::isOpen
	bool ___isOpen_1;
	// System.Boolean System.Xml.Schema.ContentValidator::isEmptiable
	bool ___isEmptiable_2;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___contentType_0)); }
	inline int32_t get_contentType_0() const { return ___contentType_0; }
	inline int32_t* get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(int32_t value)
	{
		___contentType_0 = value;
	}

	inline static int32_t get_offset_of_isOpen_1() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isOpen_1)); }
	inline bool get_isOpen_1() const { return ___isOpen_1; }
	inline bool* get_address_of_isOpen_1() { return &___isOpen_1; }
	inline void set_isOpen_1(bool value)
	{
		___isOpen_1 = value;
	}

	inline static int32_t get_offset_of_isEmptiable_2() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isEmptiable_2)); }
	inline bool get_isEmptiable_2() const { return ___isEmptiable_2; }
	inline bool* get_address_of_isEmptiable_2() { return &___isEmptiable_2; }
	inline void set_isEmptiable_2(bool value)
	{
		___isEmptiable_2 = value;
	}
};

struct ContentValidator_t2510151843_StaticFields
{
public:
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Empty
	ContentValidator_t2510151843 * ___Empty_3;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::TextOnly
	ContentValidator_t2510151843 * ___TextOnly_4;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Mixed
	ContentValidator_t2510151843 * ___Mixed_5;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Any
	ContentValidator_t2510151843 * ___Any_6;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Empty_3)); }
	inline ContentValidator_t2510151843 * get_Empty_3() const { return ___Empty_3; }
	inline ContentValidator_t2510151843 ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ContentValidator_t2510151843 * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_3, value);
	}

	inline static int32_t get_offset_of_TextOnly_4() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___TextOnly_4)); }
	inline ContentValidator_t2510151843 * get_TextOnly_4() const { return ___TextOnly_4; }
	inline ContentValidator_t2510151843 ** get_address_of_TextOnly_4() { return &___TextOnly_4; }
	inline void set_TextOnly_4(ContentValidator_t2510151843 * value)
	{
		___TextOnly_4 = value;
		Il2CppCodeGenWriteBarrier(&___TextOnly_4, value);
	}

	inline static int32_t get_offset_of_Mixed_5() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Mixed_5)); }
	inline ContentValidator_t2510151843 * get_Mixed_5() const { return ___Mixed_5; }
	inline ContentValidator_t2510151843 ** get_address_of_Mixed_5() { return &___Mixed_5; }
	inline void set_Mixed_5(ContentValidator_t2510151843 * value)
	{
		___Mixed_5 = value;
		Il2CppCodeGenWriteBarrier(&___Mixed_5, value);
	}

	inline static int32_t get_offset_of_Any_6() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Any_6)); }
	inline ContentValidator_t2510151843 * get_Any_6() const { return ___Any_6; }
	inline ContentValidator_t2510151843 ** get_address_of_Any_6() { return &___Any_6; }
	inline void set_Any_6(ContentValidator_t2510151843 * value)
	{
		___Any_6 = value;
		Il2CppCodeGenWriteBarrier(&___Any_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
