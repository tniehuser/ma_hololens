﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildEnumerator
struct  XmlChildEnumerator_t1349548835  : public Il2CppObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::container
	XmlNode_t616554813 * ___container_0;
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::child
	XmlNode_t616554813 * ___child_1;
	// System.Boolean System.Xml.XmlChildEnumerator::isFirst
	bool ___isFirst_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t1349548835, ___container_0)); }
	inline XmlNode_t616554813 * get_container_0() const { return ___container_0; }
	inline XmlNode_t616554813 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t616554813 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier(&___container_0, value);
	}

	inline static int32_t get_offset_of_child_1() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t1349548835, ___child_1)); }
	inline XmlNode_t616554813 * get_child_1() const { return ___child_1; }
	inline XmlNode_t616554813 ** get_address_of_child_1() { return &___child_1; }
	inline void set_child_1(XmlNode_t616554813 * value)
	{
		___child_1 = value;
		Il2CppCodeGenWriteBarrier(&___child_1, value);
	}

	inline static int32_t get_offset_of_isFirst_2() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_t1349548835, ___isFirst_2)); }
	inline bool get_isFirst_2() const { return ___isFirst_2; }
	inline bool* get_address_of_isFirst_2() { return &___isFirst_2; }
	inline void set_isFirst_2(bool value)
	{
		___isFirst_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
