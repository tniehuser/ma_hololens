﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1992316134.h"

// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall
struct  BinaryMethodCall_t1773568836  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::uri
	String_t* ___uri_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::methodName
	String_t* ___methodName_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::typeName
	String_t* ___typeName_2;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::instArgs
	TypeU5BU5D_t1664964607* ___instArgs_3;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::args
	ObjectU5BU5D_t3614634134* ___args_4;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::methodSignature
	Il2CppObject * ___methodSignature_5;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::callContext
	Il2CppObject * ___callContext_6;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::scallContext
	String_t* ___scallContext_7;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::properties
	Il2CppObject * ___properties_8;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::argTypes
	TypeU5BU5D_t1664964607* ___argTypes_9;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::bArgsPrimitive
	bool ___bArgsPrimitive_10;
	// System.Runtime.Serialization.Formatters.Binary.MessageEnum System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::messageEnum
	int32_t ___messageEnum_11;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::callA
	ObjectU5BU5D_t3614634134* ___callA_12;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier(&___uri_0, value);
	}

	inline static int32_t get_offset_of_methodName_1() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___methodName_1)); }
	inline String_t* get_methodName_1() const { return ___methodName_1; }
	inline String_t** get_address_of_methodName_1() { return &___methodName_1; }
	inline void set_methodName_1(String_t* value)
	{
		___methodName_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_1, value);
	}

	inline static int32_t get_offset_of_typeName_2() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___typeName_2)); }
	inline String_t* get_typeName_2() const { return ___typeName_2; }
	inline String_t** get_address_of_typeName_2() { return &___typeName_2; }
	inline void set_typeName_2(String_t* value)
	{
		___typeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_2, value);
	}

	inline static int32_t get_offset_of_instArgs_3() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___instArgs_3)); }
	inline TypeU5BU5D_t1664964607* get_instArgs_3() const { return ___instArgs_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_instArgs_3() { return &___instArgs_3; }
	inline void set_instArgs_3(TypeU5BU5D_t1664964607* value)
	{
		___instArgs_3 = value;
		Il2CppCodeGenWriteBarrier(&___instArgs_3, value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___args_4)); }
	inline ObjectU5BU5D_t3614634134* get_args_4() const { return ___args_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(ObjectU5BU5D_t3614634134* value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier(&___args_4, value);
	}

	inline static int32_t get_offset_of_methodSignature_5() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___methodSignature_5)); }
	inline Il2CppObject * get_methodSignature_5() const { return ___methodSignature_5; }
	inline Il2CppObject ** get_address_of_methodSignature_5() { return &___methodSignature_5; }
	inline void set_methodSignature_5(Il2CppObject * value)
	{
		___methodSignature_5 = value;
		Il2CppCodeGenWriteBarrier(&___methodSignature_5, value);
	}

	inline static int32_t get_offset_of_callContext_6() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___callContext_6)); }
	inline Il2CppObject * get_callContext_6() const { return ___callContext_6; }
	inline Il2CppObject ** get_address_of_callContext_6() { return &___callContext_6; }
	inline void set_callContext_6(Il2CppObject * value)
	{
		___callContext_6 = value;
		Il2CppCodeGenWriteBarrier(&___callContext_6, value);
	}

	inline static int32_t get_offset_of_scallContext_7() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___scallContext_7)); }
	inline String_t* get_scallContext_7() const { return ___scallContext_7; }
	inline String_t** get_address_of_scallContext_7() { return &___scallContext_7; }
	inline void set_scallContext_7(String_t* value)
	{
		___scallContext_7 = value;
		Il2CppCodeGenWriteBarrier(&___scallContext_7, value);
	}

	inline static int32_t get_offset_of_properties_8() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___properties_8)); }
	inline Il2CppObject * get_properties_8() const { return ___properties_8; }
	inline Il2CppObject ** get_address_of_properties_8() { return &___properties_8; }
	inline void set_properties_8(Il2CppObject * value)
	{
		___properties_8 = value;
		Il2CppCodeGenWriteBarrier(&___properties_8, value);
	}

	inline static int32_t get_offset_of_argTypes_9() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___argTypes_9)); }
	inline TypeU5BU5D_t1664964607* get_argTypes_9() const { return ___argTypes_9; }
	inline TypeU5BU5D_t1664964607** get_address_of_argTypes_9() { return &___argTypes_9; }
	inline void set_argTypes_9(TypeU5BU5D_t1664964607* value)
	{
		___argTypes_9 = value;
		Il2CppCodeGenWriteBarrier(&___argTypes_9, value);
	}

	inline static int32_t get_offset_of_bArgsPrimitive_10() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___bArgsPrimitive_10)); }
	inline bool get_bArgsPrimitive_10() const { return ___bArgsPrimitive_10; }
	inline bool* get_address_of_bArgsPrimitive_10() { return &___bArgsPrimitive_10; }
	inline void set_bArgsPrimitive_10(bool value)
	{
		___bArgsPrimitive_10 = value;
	}

	inline static int32_t get_offset_of_messageEnum_11() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___messageEnum_11)); }
	inline int32_t get_messageEnum_11() const { return ___messageEnum_11; }
	inline int32_t* get_address_of_messageEnum_11() { return &___messageEnum_11; }
	inline void set_messageEnum_11(int32_t value)
	{
		___messageEnum_11 = value;
	}

	inline static int32_t get_offset_of_callA_12() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t1773568836, ___callA_12)); }
	inline ObjectU5BU5D_t3614634134* get_callA_12() const { return ___callA_12; }
	inline ObjectU5BU5D_t3614634134** get_address_of_callA_12() { return &___callA_12; }
	inline void set_callA_12(ObjectU5BU5D_t3614634134* value)
	{
		___callA_12 = value;
		Il2CppCodeGenWriteBarrier(&___callA_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
