﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.ICustomTypeDescriptor
struct ICustomTypeDescriptor_t594940201;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor/MergedTypeDescriptor
struct  MergedTypeDescriptor_t3445332928  : public Il2CppObject
{
public:
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor/MergedTypeDescriptor::_primary
	Il2CppObject * ____primary_0;
	// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor/MergedTypeDescriptor::_secondary
	Il2CppObject * ____secondary_1;

public:
	inline static int32_t get_offset_of__primary_0() { return static_cast<int32_t>(offsetof(MergedTypeDescriptor_t3445332928, ____primary_0)); }
	inline Il2CppObject * get__primary_0() const { return ____primary_0; }
	inline Il2CppObject ** get_address_of__primary_0() { return &____primary_0; }
	inline void set__primary_0(Il2CppObject * value)
	{
		____primary_0 = value;
		Il2CppCodeGenWriteBarrier(&____primary_0, value);
	}

	inline static int32_t get_offset_of__secondary_1() { return static_cast<int32_t>(offsetof(MergedTypeDescriptor_t3445332928, ____secondary_1)); }
	inline Il2CppObject * get__secondary_1() const { return ____secondary_1; }
	inline Il2CppObject ** get_address_of__secondary_1() { return &____secondary_1; }
	inline void set__secondary_1(Il2CppObject * value)
	{
		____secondary_1 = value;
		Il2CppCodeGenWriteBarrier(&____secondary_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
