﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Queue
struct Queue_t1288490777;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Queue/QueueEnumerator
struct  QueueEnumerator_t644509261  : public Il2CppObject
{
public:
	// System.Collections.Queue System.Collections.Queue/QueueEnumerator::_q
	Queue_t1288490777 * ____q_0;
	// System.Int32 System.Collections.Queue/QueueEnumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Queue/QueueEnumerator::_version
	int32_t ____version_2;
	// System.Object System.Collections.Queue/QueueEnumerator::currentElement
	Il2CppObject * ___currentElement_3;

public:
	inline static int32_t get_offset_of__q_0() { return static_cast<int32_t>(offsetof(QueueEnumerator_t644509261, ____q_0)); }
	inline Queue_t1288490777 * get__q_0() const { return ____q_0; }
	inline Queue_t1288490777 ** get_address_of__q_0() { return &____q_0; }
	inline void set__q_0(Queue_t1288490777 * value)
	{
		____q_0 = value;
		Il2CppCodeGenWriteBarrier(&____q_0, value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(QueueEnumerator_t644509261, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(QueueEnumerator_t644509261, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of_currentElement_3() { return static_cast<int32_t>(offsetof(QueueEnumerator_t644509261, ___currentElement_3)); }
	inline Il2CppObject * get_currentElement_3() const { return ___currentElement_3; }
	inline Il2CppObject ** get_address_of_currentElement_3() { return &___currentElement_3; }
	inline void set_currentElement_3(Il2CppObject * value)
	{
		___currentElement_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentElement_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
