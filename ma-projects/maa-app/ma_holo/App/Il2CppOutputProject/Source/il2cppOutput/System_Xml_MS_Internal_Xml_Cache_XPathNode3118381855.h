﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// MS.Internal.Xml.Cache.XPathNodeInfoAtom
struct XPathNodeInfoAtom_t2810782300;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNode
struct  XPathNode_t3118381855 
{
public:
	// MS.Internal.Xml.Cache.XPathNodeInfoAtom MS.Internal.Xml.Cache.XPathNode::info
	XPathNodeInfoAtom_t2810782300 * ___info_0;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSibling
	uint16_t ___idxSibling_1;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxParent
	uint16_t ___idxParent_2;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::idxSimilar
	uint16_t ___idxSimilar_3;
	// System.UInt16 MS.Internal.Xml.Cache.XPathNode::posOffset
	uint16_t ___posOffset_4;
	// System.UInt32 MS.Internal.Xml.Cache.XPathNode::props
	uint32_t ___props_5;
	// System.String MS.Internal.Xml.Cache.XPathNode::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___info_0)); }
	inline XPathNodeInfoAtom_t2810782300 * get_info_0() const { return ___info_0; }
	inline XPathNodeInfoAtom_t2810782300 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(XPathNodeInfoAtom_t2810782300 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier(&___info_0, value);
	}

	inline static int32_t get_offset_of_idxSibling_1() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___idxSibling_1)); }
	inline uint16_t get_idxSibling_1() const { return ___idxSibling_1; }
	inline uint16_t* get_address_of_idxSibling_1() { return &___idxSibling_1; }
	inline void set_idxSibling_1(uint16_t value)
	{
		___idxSibling_1 = value;
	}

	inline static int32_t get_offset_of_idxParent_2() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___idxParent_2)); }
	inline uint16_t get_idxParent_2() const { return ___idxParent_2; }
	inline uint16_t* get_address_of_idxParent_2() { return &___idxParent_2; }
	inline void set_idxParent_2(uint16_t value)
	{
		___idxParent_2 = value;
	}

	inline static int32_t get_offset_of_idxSimilar_3() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___idxSimilar_3)); }
	inline uint16_t get_idxSimilar_3() const { return ___idxSimilar_3; }
	inline uint16_t* get_address_of_idxSimilar_3() { return &___idxSimilar_3; }
	inline void set_idxSimilar_3(uint16_t value)
	{
		___idxSimilar_3 = value;
	}

	inline static int32_t get_offset_of_posOffset_4() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___posOffset_4)); }
	inline uint16_t get_posOffset_4() const { return ___posOffset_4; }
	inline uint16_t* get_address_of_posOffset_4() { return &___posOffset_4; }
	inline void set_posOffset_4(uint16_t value)
	{
		___posOffset_4 = value;
	}

	inline static int32_t get_offset_of_props_5() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___props_5)); }
	inline uint32_t get_props_5() const { return ___props_5; }
	inline uint32_t* get_address_of_props_5() { return &___props_5; }
	inline void set_props_5(uint32_t value)
	{
		___props_5 = value;
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(XPathNode_t3118381855, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier(&___value_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t3118381855_marshaled_pinvoke
{
	XPathNodeInfoAtom_t2810782300 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	char* ___value_6;
};
// Native definition for COM marshalling of MS.Internal.Xml.Cache.XPathNode
struct XPathNode_t3118381855_marshaled_com
{
	XPathNodeInfoAtom_t2810782300 * ___info_0;
	uint16_t ___idxSibling_1;
	uint16_t ___idxParent_2;
	uint16_t ___idxSimilar_3;
	uint16_t ___posOffset_4;
	uint32_t ___props_5;
	Il2CppChar* ___value_6;
};
