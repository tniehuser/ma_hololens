﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Enum_ParseFailureKind3876725692.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum/EnumResult
struct  EnumResult_t2872047947 
{
public:
	// System.Object System.Enum/EnumResult::parsedEnum
	Il2CppObject * ___parsedEnum_0;
	// System.Boolean System.Enum/EnumResult::canThrow
	bool ___canThrow_1;
	// System.Enum/ParseFailureKind System.Enum/EnumResult::m_failure
	int32_t ___m_failure_2;
	// System.String System.Enum/EnumResult::m_failureMessageID
	String_t* ___m_failureMessageID_3;
	// System.String System.Enum/EnumResult::m_failureParameter
	String_t* ___m_failureParameter_4;
	// System.Object System.Enum/EnumResult::m_failureMessageFormatArgument
	Il2CppObject * ___m_failureMessageFormatArgument_5;
	// System.Exception System.Enum/EnumResult::m_innerException
	Exception_t1927440687 * ___m_innerException_6;

public:
	inline static int32_t get_offset_of_parsedEnum_0() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___parsedEnum_0)); }
	inline Il2CppObject * get_parsedEnum_0() const { return ___parsedEnum_0; }
	inline Il2CppObject ** get_address_of_parsedEnum_0() { return &___parsedEnum_0; }
	inline void set_parsedEnum_0(Il2CppObject * value)
	{
		___parsedEnum_0 = value;
		Il2CppCodeGenWriteBarrier(&___parsedEnum_0, value);
	}

	inline static int32_t get_offset_of_canThrow_1() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___canThrow_1)); }
	inline bool get_canThrow_1() const { return ___canThrow_1; }
	inline bool* get_address_of_canThrow_1() { return &___canThrow_1; }
	inline void set_canThrow_1(bool value)
	{
		___canThrow_1 = value;
	}

	inline static int32_t get_offset_of_m_failure_2() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___m_failure_2)); }
	inline int32_t get_m_failure_2() const { return ___m_failure_2; }
	inline int32_t* get_address_of_m_failure_2() { return &___m_failure_2; }
	inline void set_m_failure_2(int32_t value)
	{
		___m_failure_2 = value;
	}

	inline static int32_t get_offset_of_m_failureMessageID_3() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___m_failureMessageID_3)); }
	inline String_t* get_m_failureMessageID_3() const { return ___m_failureMessageID_3; }
	inline String_t** get_address_of_m_failureMessageID_3() { return &___m_failureMessageID_3; }
	inline void set_m_failureMessageID_3(String_t* value)
	{
		___m_failureMessageID_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureMessageID_3, value);
	}

	inline static int32_t get_offset_of_m_failureParameter_4() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___m_failureParameter_4)); }
	inline String_t* get_m_failureParameter_4() const { return ___m_failureParameter_4; }
	inline String_t** get_address_of_m_failureParameter_4() { return &___m_failureParameter_4; }
	inline void set_m_failureParameter_4(String_t* value)
	{
		___m_failureParameter_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureParameter_4, value);
	}

	inline static int32_t get_offset_of_m_failureMessageFormatArgument_5() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___m_failureMessageFormatArgument_5)); }
	inline Il2CppObject * get_m_failureMessageFormatArgument_5() const { return ___m_failureMessageFormatArgument_5; }
	inline Il2CppObject ** get_address_of_m_failureMessageFormatArgument_5() { return &___m_failureMessageFormatArgument_5; }
	inline void set_m_failureMessageFormatArgument_5(Il2CppObject * value)
	{
		___m_failureMessageFormatArgument_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_failureMessageFormatArgument_5, value);
	}

	inline static int32_t get_offset_of_m_innerException_6() { return static_cast<int32_t>(offsetof(EnumResult_t2872047947, ___m_innerException_6)); }
	inline Exception_t1927440687 * get_m_innerException_6() const { return ___m_innerException_6; }
	inline Exception_t1927440687 ** get_address_of_m_innerException_6() { return &___m_innerException_6; }
	inline void set_m_innerException_6(Exception_t1927440687 * value)
	{
		___m_innerException_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_innerException_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum/EnumResult
struct EnumResult_t2872047947_marshaled_pinvoke
{
	Il2CppIUnknown* ___parsedEnum_0;
	int32_t ___canThrow_1;
	int32_t ___m_failure_2;
	char* ___m_failureMessageID_3;
	char* ___m_failureParameter_4;
	Il2CppIUnknown* ___m_failureMessageFormatArgument_5;
	Exception_t1927440687_marshaled_pinvoke* ___m_innerException_6;
};
// Native definition for COM marshalling of System.Enum/EnumResult
struct EnumResult_t2872047947_marshaled_com
{
	Il2CppIUnknown* ___parsedEnum_0;
	int32_t ___canThrow_1;
	int32_t ___m_failure_2;
	Il2CppChar* ___m_failureMessageID_3;
	Il2CppChar* ___m_failureParameter_4;
	Il2CppIUnknown* ___m_failureMessageFormatArgument_5;
	Exception_t1927440687_marshaled_com* ___m_innerException_6;
};
