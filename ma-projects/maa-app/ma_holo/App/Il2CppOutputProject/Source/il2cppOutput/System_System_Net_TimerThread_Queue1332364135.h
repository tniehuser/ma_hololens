﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/Queue
struct  Queue_t1332364135  : public Il2CppObject
{
public:
	// System.Int32 System.Net.TimerThread/Queue::m_DurationMilliseconds
	int32_t ___m_DurationMilliseconds_0;

public:
	inline static int32_t get_offset_of_m_DurationMilliseconds_0() { return static_cast<int32_t>(offsetof(Queue_t1332364135, ___m_DurationMilliseconds_0)); }
	inline int32_t get_m_DurationMilliseconds_0() const { return ___m_DurationMilliseconds_0; }
	inline int32_t* get_address_of_m_DurationMilliseconds_0() { return &___m_DurationMilliseconds_0; }
	inline void set_m_DurationMilliseconds_0(int32_t value)
	{
		___m_DurationMilliseconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
