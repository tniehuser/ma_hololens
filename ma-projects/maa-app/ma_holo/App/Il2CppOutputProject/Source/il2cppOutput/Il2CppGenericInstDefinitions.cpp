﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Entry_t3655379380_0_0_0;
static const Il2CppType* GenInst_Entry_t3655379380_0_0_0_Types[] = { &Entry_t3655379380_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3655379380_0_0_0 = { 1, GenInst_Entry_t3655379380_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType SerializationInfo_t228987430_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_SerializationInfo_t228987430_0_0_0_Types[] = { &Il2CppObject_0_0_0, &SerializationInfo_t228987430_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_SerializationInfo_t228987430_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_SerializationInfo_t228987430_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Ephemeron_t1875076633_0_0_0;
static const Il2CppType* GenInst_Ephemeron_t1875076633_0_0_0_Types[] = { &Ephemeron_t1875076633_0_0_0 };
extern const Il2CppGenericInst GenInst_Ephemeron_t1875076633_0_0_0 = { 1, GenInst_Ephemeron_t1875076633_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Exception_t1927440687_0_0_0;
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_Types[] = { &Exception_t1927440687_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType ExceptionDispatchInfo_t3341954301_0_0_0;
static const Il2CppType* GenInst_ExceptionDispatchInfo_t3341954301_0_0_0_Types[] = { &ExceptionDispatchInfo_t3341954301_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionDispatchInfo_t3341954301_0_0_0 = { 1, GenInst_ExceptionDispatchInfo_t3341954301_0_0_0_Types };
extern const Il2CppType AggregateException_t420812976_0_0_0;
static const Il2CppType* GenInst_AggregateException_t420812976_0_0_0_Types[] = { &AggregateException_t420812976_0_0_0 };
extern const Il2CppGenericInst GenInst_AggregateException_t420812976_0_0_0 = { 1, GenInst_AggregateException_t420812976_0_0_0_Types };
extern const Il2CppType LocalDataStoreElement_t3980707294_0_0_0;
static const Il2CppType* GenInst_LocalDataStoreElement_t3980707294_0_0_0_Types[] = { &LocalDataStoreElement_t3980707294_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalDataStoreElement_t3980707294_0_0_0 = { 1, GenInst_LocalDataStoreElement_t3980707294_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType LocalDataStore_t228818476_0_0_0;
static const Il2CppType* GenInst_LocalDataStore_t228818476_0_0_0_Types[] = { &LocalDataStore_t228818476_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalDataStore_t228818476_0_0_0 = { 1, GenInst_LocalDataStore_t228818476_0_0_0_Types };
extern const Il2CppType LocalDataStoreSlot_t486331200_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_LocalDataStoreSlot_t486331200_0_0_0_Types[] = { &String_t_0_0_0, &LocalDataStoreSlot_t486331200_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LocalDataStoreSlot_t486331200_0_0_0 = { 2, GenInst_String_t_0_0_0_LocalDataStoreSlot_t486331200_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType Entry_t4272951227_0_0_0;
static const Il2CppType* GenInst_Entry_t4272951227_0_0_0_Types[] = { &Entry_t4272951227_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t4272951227_0_0_0 = { 1, GenInst_Entry_t4272951227_0_0_0_Types };
static const Il2CppType* GenInst_LocalDataStoreSlot_t486331200_0_0_0_Types[] = { &LocalDataStoreSlot_t486331200_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalDataStoreSlot_t486331200_0_0_0 = { 1, GenInst_LocalDataStoreSlot_t486331200_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t158455684_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t158455684_0_0_0_Types[] = { &KeyValuePair_2_t158455684_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t158455684_0_0_0 = { 1, GenInst_KeyValuePair_2_t158455684_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType Node_t1921535786_0_0_0;
static const Il2CppType* GenInst_Node_t1921535786_0_0_0_Types[] = { &Node_t1921535786_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t1921535786_0_0_0 = { 1, GenInst_Node_t1921535786_0_0_0_Types };
extern const Il2CppType bucket_t976591655_0_0_0;
static const Il2CppType* GenInst_bucket_t976591655_0_0_0_Types[] = { &bucket_t976591655_0_0_0 };
extern const Il2CppGenericInst GenInst_bucket_t976591655_0_0_0 = { 1, GenInst_bucket_t976591655_0_0_0_Types };
extern const Il2CppType RuntimeType_t2836228502_0_0_0;
static const Il2CppType* GenInst_RuntimeType_t2836228502_0_0_0_Types[] = { &RuntimeType_t2836228502_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeType_t2836228502_0_0_0 = { 1, GenInst_RuntimeType_t2836228502_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType TypeInfo_t3822613806_0_0_0;
static const Il2CppType* GenInst_TypeInfo_t3822613806_0_0_0_Types[] = { &TypeInfo_t3822613806_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeInfo_t3822613806_0_0_0 = { 1, GenInst_TypeInfo_t3822613806_0_0_0_Types };
extern const Il2CppType IReflectableType_t2431043262_0_0_0;
static const Il2CppType* GenInst_IReflectableType_t2431043262_0_0_0_Types[] = { &IReflectableType_t2431043262_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflectableType_t2431043262_0_0_0 = { 1, GenInst_IReflectableType_t2431043262_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3030399641_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3030399641_0_0_0 = { 1, GenInst_Int32U5BU5D_t3030399641_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType IStructuralComparable_t881765570_0_0_0;
static const Il2CppType* GenInst_IStructuralComparable_t881765570_0_0_0_Types[] = { &IStructuralComparable_t881765570_0_0_0 };
extern const Il2CppGenericInst GenInst_IStructuralComparable_t881765570_0_0_0 = { 1, GenInst_IStructuralComparable_t881765570_0_0_0_Types };
extern const Il2CppType IStructuralEquatable_t3751153838_0_0_0;
static const Il2CppType* GenInst_IStructuralEquatable_t3751153838_0_0_0_Types[] = { &IStructuralEquatable_t3751153838_0_0_0 };
extern const Il2CppGenericInst GenInst_IStructuralEquatable_t3751153838_0_0_0 = { 1, GenInst_IStructuralEquatable_t3751153838_0_0_0_Types };
extern const Il2CppType IList_1_t2612818049_0_0_0;
static const Il2CppType* GenInst_IList_1_t2612818049_0_0_0_Types[] = { &IList_1_t2612818049_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2612818049_0_0_0 = { 1, GenInst_IList_1_t2612818049_0_0_0_Types };
extern const Il2CppType ICollection_1_t3023952753_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3023952753_0_0_0_Types[] = { &ICollection_1_t3023952753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3023952753_0_0_0 = { 1, GenInst_ICollection_1_t3023952753_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2364004493_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2364004493_0_0_0_Types[] = { &IEnumerable_1_t2364004493_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2364004493_0_0_0 = { 1, GenInst_IEnumerable_1_t2364004493_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t3770255835_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t3770255835_0_0_0_Types[] = { &IReadOnlyList_1_t3770255835_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t3770255835_0_0_0 = { 1, GenInst_IReadOnlyList_1_t3770255835_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t1659542175_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t1659542175_0_0_0_Types[] = { &IReadOnlyCollection_1_t1659542175_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t1659542175_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t1659542175_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType IObjectReference_t2936130011_0_0_0;
static const Il2CppType* GenInst_IObjectReference_t2936130011_0_0_0_Types[] = { &IObjectReference_t2936130011_0_0_0 };
extern const Il2CppGenericInst GenInst_IObjectReference_t2936130011_0_0_0 = { 1, GenInst_IObjectReference_t2936130011_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType StackTrace_t2500644597_0_0_0;
static const Il2CppType* GenInst_StackTrace_t2500644597_0_0_0_Types[] = { &StackTrace_t2500644597_0_0_0 };
extern const Il2CppGenericInst GenInst_StackTrace_t2500644597_0_0_0 = { 1, GenInst_StackTrace_t2500644597_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType SimpleCollator_t4081201584_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_SimpleCollator_t4081201584_0_0_0_Types[] = { &String_t_0_0_0, &SimpleCollator_t4081201584_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SimpleCollator_t4081201584_0_0_0 = { 2, GenInst_String_t_0_0_0_SimpleCollator_t4081201584_0_0_0_Types };
static const Il2CppType* GenInst_SimpleCollator_t4081201584_0_0_0_Types[] = { &SimpleCollator_t4081201584_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleCollator_t4081201584_0_0_0 = { 1, GenInst_SimpleCollator_t4081201584_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3753326068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3753326068_0_0_0_Types[] = { &KeyValuePair_2_t3753326068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3753326068_0_0_0 = { 1, GenInst_KeyValuePair_2_t3753326068_0_0_0_Types };
extern const Il2CppType TokenHashValue_t393941324_0_0_0;
static const Il2CppType* GenInst_TokenHashValue_t393941324_0_0_0_Types[] = { &TokenHashValue_t393941324_0_0_0 };
extern const Il2CppGenericInst GenInst_TokenHashValue_t393941324_0_0_0 = { 1, GenInst_TokenHashValue_t393941324_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType DSU5BU5D_t3090528720_0_0_0;
static const Il2CppType* GenInst_DSU5BU5D_t3090528720_0_0_0_Types[] = { &DSU5BU5D_t3090528720_0_0_0 };
extern const Il2CppGenericInst GenInst_DSU5BU5D_t3090528720_0_0_0 = { 1, GenInst_DSU5BU5D_t3090528720_0_0_0_Types };
extern const Il2CppType IList_1_t1310137942_0_0_0;
static const Il2CppType* GenInst_IList_1_t1310137942_0_0_0_Types[] = { &IList_1_t1310137942_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1310137942_0_0_0 = { 1, GenInst_IList_1_t1310137942_0_0_0_Types };
extern const Il2CppType DS_t769197341_0_0_0;
static const Il2CppType* GenInst_DS_t769197341_0_0_0_Types[] = { &DS_t769197341_0_0_0 };
extern const Il2CppGenericInst GenInst_DS_t769197341_0_0_0 = { 1, GenInst_DS_t769197341_0_0_0_Types };
extern const Il2CppType ICollection_1_t1721272646_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1721272646_0_0_0_Types[] = { &ICollection_1_t1721272646_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1721272646_0_0_0 = { 1, GenInst_ICollection_1_t1721272646_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1061324386_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1061324386_0_0_0_Types[] = { &IEnumerable_1_t1061324386_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1061324386_0_0_0 = { 1, GenInst_IEnumerable_1_t1061324386_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t2467575728_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t2467575728_0_0_0_Types[] = { &IReadOnlyList_1_t2467575728_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t2467575728_0_0_0 = { 1, GenInst_IReadOnlyList_1_t2467575728_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t356862068_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t356862068_0_0_0_Types[] = { &IReadOnlyCollection_1_t356862068_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t356862068_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t356862068_0_0_0_Types };
extern const Il2CppType EraInfo_t1792567760_0_0_0;
static const Il2CppType* GenInst_EraInfo_t1792567760_0_0_0_Types[] = { &EraInfo_t1792567760_0_0_0 };
extern const Il2CppGenericInst GenInst_EraInfo_t1792567760_0_0_0 = { 1, GenInst_EraInfo_t1792567760_0_0_0_Types };
extern const Il2CppType HebrewValue_t2452471125_0_0_0;
static const Il2CppType* GenInst_HebrewValue_t2452471125_0_0_0_Types[] = { &HebrewValue_t2452471125_0_0_0 };
extern const Il2CppGenericInst GenInst_HebrewValue_t2452471125_0_0_0 = { 1, GenInst_HebrewValue_t2452471125_0_0_0_Types };
extern const Il2CppType HSU5BU5D_t387695875_0_0_0;
static const Il2CppType* GenInst_HSU5BU5D_t387695875_0_0_0_Types[] = { &HSU5BU5D_t387695875_0_0_0 };
extern const Il2CppGenericInst GenInst_HSU5BU5D_t387695875_0_0_0 = { 1, GenInst_HSU5BU5D_t387695875_0_0_0_Types };
extern const Il2CppType IList_1_t2280721119_0_0_0;
static const Il2CppType* GenInst_IList_1_t2280721119_0_0_0_Types[] = { &IList_1_t2280721119_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2280721119_0_0_0 = { 1, GenInst_IList_1_t2280721119_0_0_0_Types };
extern const Il2CppType HS_t1739780518_0_0_0;
static const Il2CppType* GenInst_HS_t1739780518_0_0_0_Types[] = { &HS_t1739780518_0_0_0 };
extern const Il2CppGenericInst GenInst_HS_t1739780518_0_0_0 = { 1, GenInst_HS_t1739780518_0_0_0_Types };
extern const Il2CppType ICollection_1_t2691855823_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2691855823_0_0_0_Types[] = { &ICollection_1_t2691855823_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2691855823_0_0_0 = { 1, GenInst_ICollection_1_t2691855823_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2031907563_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2031907563_0_0_0_Types[] = { &IEnumerable_1_t2031907563_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2031907563_0_0_0 = { 1, GenInst_IEnumerable_1_t2031907563_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t3438158905_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t3438158905_0_0_0_Types[] = { &IReadOnlyList_1_t3438158905_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t3438158905_0_0_0 = { 1, GenInst_IReadOnlyList_1_t3438158905_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t1327445245_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t1327445245_0_0_0_Types[] = { &IReadOnlyCollection_1_t1327445245_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t1327445245_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t1327445245_0_0_0_Types };
extern const Il2CppType TimeSpanToken_t2031659367_0_0_0;
static const Il2CppType* GenInst_TimeSpanToken_t2031659367_0_0_0_Types[] = { &TimeSpanToken_t2031659367_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpanToken_t2031659367_0_0_0 = { 1, GenInst_TimeSpanToken_t2031659367_0_0_0_Types };
extern const Il2CppType Guid_t_0_0_0;
static const Il2CppType* GenInst_Guid_t_0_0_0_Types[] = { &Guid_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
extern const Il2CppType IAsyncResult_t1999651008_0_0_0;
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0 = { 1, GenInst_IAsyncResult_t1999651008_0_0_0_Types };
extern const Il2CppType AsyncCallback_t163412349_0_0_0;
static const Il2CppType* GenInst_AsyncCallback_t163412349_0_0_0_Il2CppObject_0_0_0_IAsyncResult_t1999651008_0_0_0_Types[] = { &AsyncCallback_t163412349_0_0_0, &Il2CppObject_0_0_0, &IAsyncResult_t1999651008_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncCallback_t163412349_0_0_0_Il2CppObject_0_0_0_IAsyncResult_t1999651008_0_0_0 = { 3, GenInst_AsyncCallback_t163412349_0_0_0_Il2CppObject_0_0_0_IAsyncResult_t1999651008_0_0_0_Types };
extern const Il2CppType Task_1_t963265114_0_0_0;
extern const Il2CppType Task_1_t1191906455_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t1191906455_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t1191906455_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t1191906455_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t1191906455_0_0_0_Types };
extern const Il2CppType Task_t1843236107_0_0_0;
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Types[] = { &Task_t1843236107_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0 = { 1, GenInst_Task_t1843236107_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t963265114_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t963265114_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t963265114_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t963265114_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Task_t1843236107_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Task_t1843236107_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Task_t1843236107_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Task_t1843236107_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_Il2CppObject_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Task_1_t1809478302_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t1809478302_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t1809478302_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t1809478302_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t1809478302_0_0_0_Types };
extern const Il2CppType SemaphoreSlim_t461808439_0_0_0;
static const Il2CppType* GenInst_SemaphoreSlim_t461808439_0_0_0_Types[] = { &SemaphoreSlim_t461808439_0_0_0 };
extern const Il2CppGenericInst GenInst_SemaphoreSlim_t461808439_0_0_0 = { 1, GenInst_SemaphoreSlim_t461808439_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Stream_t3255436806_0_0_0;
extern const Il2CppType ReadWriteTask_t2745753060_0_0_0;
static const Il2CppType* GenInst_Stream_t3255436806_0_0_0_ReadWriteTask_t2745753060_0_0_0_Types[] = { &Stream_t3255436806_0_0_0, &ReadWriteTask_t2745753060_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0_ReadWriteTask_t2745753060_0_0_0 = { 2, GenInst_Stream_t3255436806_0_0_0_ReadWriteTask_t2745753060_0_0_0_Types };
extern const Il2CppType ManualResetEvent_t926074657_0_0_0;
static const Il2CppType* GenInst_ManualResetEvent_t926074657_0_0_0_Types[] = { &ManualResetEvent_t926074657_0_0_0 };
extern const Il2CppGenericInst GenInst_ManualResetEvent_t926074657_0_0_0 = { 1, GenInst_ManualResetEvent_t926074657_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType TextReader_t1561828458_0_0_0;
extern const Il2CppType CharU5BU5D_t1328083999_0_0_0;
static const Il2CppType* GenInst_TextReader_t1561828458_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &TextReader_t1561828458_0_0_0, &CharU5BU5D_t1328083999_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_TextReader_t1561828458_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 4, GenInst_TextReader_t1561828458_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType TextWriter_t4027217640_0_0_0;
static const Il2CppType* GenInst_TextWriter_t4027217640_0_0_0_Char_t3454481338_0_0_0_Types[] = { &TextWriter_t4027217640_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_TextWriter_t4027217640_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_TextWriter_t4027217640_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_TextWriter_t4027217640_0_0_0_String_t_0_0_0_Types[] = { &TextWriter_t4027217640_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextWriter_t4027217640_0_0_0_String_t_0_0_0 = { 2, GenInst_TextWriter_t4027217640_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_TextWriter_t4027217640_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &TextWriter_t4027217640_0_0_0, &CharU5BU5D_t1328083999_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_TextWriter_t4027217640_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 4, GenInst_TextWriter_t4027217640_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
static const Il2CppType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { &_EventInfo_t2430923913_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
extern const Il2CppType ResourceLocator_t2156390884_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ResourceLocator_t2156390884_0_0_0_Types[] = { &String_t_0_0_0, &ResourceLocator_t2156390884_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ResourceLocator_t2156390884_0_0_0 = { 2, GenInst_String_t_0_0_0_ResourceLocator_t2156390884_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ResourceLocator_t2156390884_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ResourceLocator_t2156390884_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ResourceLocator_t2156390884_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ResourceLocator_t2156390884_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3800763530_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3800763530_0_0_0_Types[] = { &KeyValuePair_2_t3800763530_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3800763530_0_0_0 = { 1, GenInst_KeyValuePair_2_t3800763530_0_0_0_Types };
static const Il2CppType* GenInst_ResourceLocator_t2156390884_0_0_0_Types[] = { &ResourceLocator_t2156390884_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceLocator_t2156390884_0_0_0 = { 1, GenInst_ResourceLocator_t2156390884_0_0_0_Types };
extern const Il2CppType Entry_t3739892816_0_0_0;
static const Il2CppType* GenInst_Entry_t3739892816_0_0_0_Types[] = { &Entry_t3739892816_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3739892816_0_0_0 = { 1, GenInst_Entry_t3739892816_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1828515368_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1828515368_0_0_0_Types[] = { &KeyValuePair_2_t1828515368_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1828515368_0_0_0 = { 1, GenInst_KeyValuePair_2_t1828515368_0_0_0_Types };
static const Il2CppType* GenInst_Guid_t_0_0_0_Type_t_0_0_0_Types[] = { &Guid_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Guid_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Guid_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &Guid_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Guid_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4275477795_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4275477795_0_0_0_Types[] = { &KeyValuePair_2_t4275477795_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4275477795_0_0_0 = { 1, GenInst_KeyValuePair_2_t4275477795_0_0_0_Types };
extern const Il2CppType Entry_t4214607081_0_0_0;
static const Il2CppType* GenInst_Entry_t4214607081_0_0_0_Types[] = { &Entry_t4214607081_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t4214607081_0_0_0 = { 1, GenInst_Entry_t4214607081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2889831726_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2889831726_0_0_0_Types[] = { &KeyValuePair_2_t2889831726_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2889831726_0_0_0 = { 1, GenInst_KeyValuePair_2_t2889831726_0_0_0_Types };
extern const Il2CppType RuntimeMethodInfo_t3887273209_0_0_0;
static const Il2CppType* GenInst_RuntimeMethodInfo_t3887273209_0_0_0_Types[] = { &RuntimeMethodInfo_t3887273209_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeMethodInfo_t3887273209_0_0_0 = { 1, GenInst_RuntimeMethodInfo_t3887273209_0_0_0_Types };
extern const Il2CppType RuntimeConstructorInfo_t311714390_0_0_0;
static const Il2CppType* GenInst_RuntimeConstructorInfo_t311714390_0_0_0_Types[] = { &RuntimeConstructorInfo_t311714390_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeConstructorInfo_t311714390_0_0_0 = { 1, GenInst_RuntimeConstructorInfo_t311714390_0_0_0_Types };
extern const Il2CppType RuntimePropertyInfo_t3497259377_0_0_0;
static const Il2CppType* GenInst_RuntimePropertyInfo_t3497259377_0_0_0_Types[] = { &RuntimePropertyInfo_t3497259377_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePropertyInfo_t3497259377_0_0_0 = { 1, GenInst_RuntimePropertyInfo_t3497259377_0_0_0_Types };
extern const Il2CppType RuntimeEventInfo_t3861156374_0_0_0;
static const Il2CppType* GenInst_RuntimeEventInfo_t3861156374_0_0_0_Types[] = { &RuntimeEventInfo_t3861156374_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeEventInfo_t3861156374_0_0_0 = { 1, GenInst_RuntimeEventInfo_t3861156374_0_0_0_Types };
extern const Il2CppType RuntimeFieldInfo_t1687134186_0_0_0;
static const Il2CppType* GenInst_RuntimeFieldInfo_t1687134186_0_0_0_Types[] = { &RuntimeFieldInfo_t1687134186_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeFieldInfo_t1687134186_0_0_0 = { 1, GenInst_RuntimeFieldInfo_t1687134186_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Task_1_t2945603725_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t2945603725_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t2945603725_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t2945603725_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t2945603725_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t1191906455_0_0_0_Types[] = { &Task_1_t1191906455_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1191906455_0_0_0 = { 1, GenInst_Task_1_t1191906455_0_0_0_Types };
extern const Il2CppType IThreadPoolWorkItem_t2893622999_0_0_0;
static const Il2CppType* GenInst_IThreadPoolWorkItem_t2893622999_0_0_0_Types[] = { &IThreadPoolWorkItem_t2893622999_0_0_0 };
extern const Il2CppGenericInst GenInst_IThreadPoolWorkItem_t2893622999_0_0_0 = { 1, GenInst_IThreadPoolWorkItem_t2893622999_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType BinaryTypeEnum_t2877339122_0_0_0;
static const Il2CppType* GenInst_BinaryTypeEnum_t2877339122_0_0_0_Types[] = { &BinaryTypeEnum_t2877339122_0_0_0 };
extern const Il2CppGenericInst GenInst_BinaryTypeEnum_t2877339122_0_0_0 = { 1, GenInst_BinaryTypeEnum_t2877339122_0_0_0_Types };
extern const Il2CppType TypeCode_t2536926201_0_0_0;
static const Il2CppType* GenInst_TypeCode_t2536926201_0_0_0_Types[] = { &TypeCode_t2536926201_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeCode_t2536926201_0_0_0 = { 1, GenInst_TypeCode_t2536926201_0_0_0_Types };
extern const Il2CppType InternalPrimitiveTypeE_t3020811655_0_0_0;
static const Il2CppType* GenInst_InternalPrimitiveTypeE_t3020811655_0_0_0_Types[] = { &InternalPrimitiveTypeE_t3020811655_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalPrimitiveTypeE_t3020811655_0_0_0 = { 1, GenInst_InternalPrimitiveTypeE_t3020811655_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType TypeInformation_t3804990478_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_TypeInformation_t3804990478_0_0_0_Types[] = { &Type_t_0_0_0, &TypeInformation_t3804990478_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_TypeInformation_t3804990478_0_0_0 = { 2, GenInst_Type_t_0_0_0_TypeInformation_t3804990478_0_0_0_Types };
static const Il2CppType* GenInst_TypeInformation_t3804990478_0_0_0_Types[] = { &TypeInformation_t3804990478_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeInformation_t3804990478_0_0_0 = { 1, GenInst_TypeInformation_t3804990478_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3499693597_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3499693597_0_0_0_Types[] = { &KeyValuePair_2_t3499693597_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3499693597_0_0_0 = { 1, GenInst_KeyValuePair_2_t3499693597_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType WriteObjectInfo_t2489189536_0_0_0;
static const Il2CppType* GenInst_WriteObjectInfo_t2489189536_0_0_0_Types[] = { &WriteObjectInfo_t2489189536_0_0_0 };
extern const Il2CppGenericInst GenInst_WriteObjectInfo_t2489189536_0_0_0 = { 1, GenInst_WriteObjectInfo_t2489189536_0_0_0_Types };
extern const Il2CppType AssemblyName_t894705941_0_0_0;
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_AssemblyName_t894705941_0_0_0_Assembly_t4268412390_0_0_0_Types[] = { &AssemblyName_t894705941_0_0_0, &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_AssemblyName_t894705941_0_0_0_Assembly_t4268412390_0_0_0 = { 2, GenInst_AssemblyName_t894705941_0_0_0_Assembly_t4268412390_0_0_0_Types };
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Type_t_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0, &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Type_t_0_0_0 = { 4, GenInst_Assembly_t4268412390_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType MemberHolder_t3720434074_0_0_0;
extern const Il2CppType MemberInfoU5BU5D_t4238939941_0_0_0;
static const Il2CppType* GenInst_MemberHolder_t3720434074_0_0_0_MemberInfoU5BU5D_t4238939941_0_0_0_Types[] = { &MemberHolder_t3720434074_0_0_0, &MemberInfoU5BU5D_t4238939941_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberHolder_t3720434074_0_0_0_MemberInfoU5BU5D_t4238939941_0_0_0 = { 2, GenInst_MemberHolder_t3720434074_0_0_0_MemberInfoU5BU5D_t4238939941_0_0_0_Types };
static const Il2CppType* GenInst_MemberHolder_t3720434074_0_0_0_Types[] = { &MemberHolder_t3720434074_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberHolder_t3720434074_0_0_0 = { 1, GenInst_MemberHolder_t3720434074_0_0_0_Types };
static const Il2CppType* GenInst_MemberInfoU5BU5D_t4238939941_0_0_0_Types[] = { &MemberInfoU5BU5D_t4238939941_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfoU5BU5D_t4238939941_0_0_0 = { 1, GenInst_MemberInfoU5BU5D_t4238939941_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t430017524_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t430017524_0_0_0_Types[] = { &KeyValuePair_2_t430017524_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t430017524_0_0_0 = { 1, GenInst_KeyValuePair_2_t430017524_0_0_0_Types };
extern const Il2CppType SerializationFieldInfo_t2472586292_0_0_0;
static const Il2CppType* GenInst_SerializationFieldInfo_t2472586292_0_0_0_Types[] = { &SerializationFieldInfo_t2472586292_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationFieldInfo_t2472586292_0_0_0 = { 1, GenInst_SerializationFieldInfo_t2472586292_0_0_0_Types };
extern const Il2CppType ObjectHolder_t2992553423_0_0_0;
static const Il2CppType* GenInst_ObjectHolder_t2992553423_0_0_0_Types[] = { &ObjectHolder_t2992553423_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectHolder_t2992553423_0_0_0 = { 1, GenInst_ObjectHolder_t2992553423_0_0_0_Types };
extern const Il2CppType FixupHolder_t2028025012_0_0_0;
static const Il2CppType* GenInst_FixupHolder_t2028025012_0_0_0_Types[] = { &FixupHolder_t2028025012_0_0_0 };
extern const Il2CppGenericInst GenInst_FixupHolder_t2028025012_0_0_0 = { 1, GenInst_FixupHolder_t2028025012_0_0_0_Types };
extern const Il2CppType SafeSerializationEventArgs_t1088103422_0_0_0;
static const Il2CppType* GenInst_SafeSerializationEventArgs_t1088103422_0_0_0_Types[] = { &SafeSerializationEventArgs_t1088103422_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeSerializationEventArgs_t1088103422_0_0_0 = { 1, GenInst_SafeSerializationEventArgs_t1088103422_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType HashAlgorithmName_t1682985260_0_0_0;
static const Il2CppType* GenInst_HashAlgorithmName_t1682985260_0_0_0_Types[] = { &HashAlgorithmName_t1682985260_0_0_0 };
extern const Il2CppGenericInst GenInst_HashAlgorithmName_t1682985260_0_0_0 = { 1, GenInst_HashAlgorithmName_t1682985260_0_0_0_Types };
extern const Il2CppType RSAEncryptionPadding_t1083150860_0_0_0;
static const Il2CppType* GenInst_RSAEncryptionPadding_t1083150860_0_0_0_Types[] = { &RSAEncryptionPadding_t1083150860_0_0_0 };
extern const Il2CppGenericInst GenInst_RSAEncryptionPadding_t1083150860_0_0_0 = { 1, GenInst_RSAEncryptionPadding_t1083150860_0_0_0_Types };
extern const Il2CppType HashAlgorithm_t2624936259_0_0_0;
static const Il2CppType* GenInst_HashAlgorithm_t2624936259_0_0_0_Types[] = { &HashAlgorithm_t2624936259_0_0_0 };
extern const Il2CppGenericInst GenInst_HashAlgorithm_t2624936259_0_0_0 = { 1, GenInst_HashAlgorithm_t2624936259_0_0_0_Types };
extern const Il2CppType EncodingProvider_t2755886406_0_0_0;
static const Il2CppType* GenInst_EncodingProvider_t2755886406_0_0_0_Types[] = { &EncodingProvider_t2755886406_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodingProvider_t2755886406_0_0_0 = { 1, GenInst_EncodingProvider_t2755886406_0_0_0_Types };
extern const Il2CppType CancellationTokenRegistration_t1708859357_0_0_0;
static const Il2CppType* GenInst_CancellationTokenRegistration_t1708859357_0_0_0_Types[] = { &CancellationTokenRegistration_t1708859357_0_0_0 };
extern const Il2CppGenericInst GenInst_CancellationTokenRegistration_t1708859357_0_0_0 = { 1, GenInst_CancellationTokenRegistration_t1708859357_0_0_0_Types };
extern const Il2CppType CancellationCallbackInfo_t1473383178_0_0_0;
static const Il2CppType* GenInst_CancellationCallbackInfo_t1473383178_0_0_0_Types[] = { &CancellationCallbackInfo_t1473383178_0_0_0 };
extern const Il2CppGenericInst GenInst_CancellationCallbackInfo_t1473383178_0_0_0 = { 1, GenInst_CancellationCallbackInfo_t1473383178_0_0_0_Types };
extern const Il2CppType SparselyPopulatedArray_1_t4170979568_0_0_0;
static const Il2CppType* GenInst_SparselyPopulatedArray_1_t4170979568_0_0_0_Types[] = { &SparselyPopulatedArray_1_t4170979568_0_0_0 };
extern const Il2CppGenericInst GenInst_SparselyPopulatedArray_1_t4170979568_0_0_0 = { 1, GenInst_SparselyPopulatedArray_1_t4170979568_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Task_t1843236107_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Task_t1843236107_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Task_t1843236107_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Task_t1843236107_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
extern const Il2CppType Entry_t3688716734_0_0_0;
static const Il2CppType* GenInst_Entry_t3688716734_0_0_0_Types[] = { &Entry_t3688716734_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3688716734_0_0_0 = { 1, GenInst_Entry_t3688716734_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2903374260_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2903374260_0_0_0_Types[] = { &KeyValuePair_2_t2903374260_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2903374260_0_0_0 = { 1, GenInst_KeyValuePair_2_t2903374260_0_0_0_Types };
extern const Il2CppType ContingentProperties_t606988207_0_0_0;
static const Il2CppType* GenInst_ContingentProperties_t606988207_0_0_0_Types[] = { &ContingentProperties_t606988207_0_0_0 };
extern const Il2CppGenericInst GenInst_ContingentProperties_t606988207_0_0_0 = { 1, GenInst_ContingentProperties_t606988207_0_0_0_Types };
extern const Il2CppType TaskContinuation_t1422769290_0_0_0;
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Task_t1843236107_0_0_0_TaskContinuation_t1422769290_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Task_t1843236107_0_0_0, &TaskContinuation_t1422769290_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Task_t1843236107_0_0_0_TaskContinuation_t1422769290_0_0_0 = { 3, GenInst_Task_t1843236107_0_0_0_Task_t1843236107_0_0_0_TaskContinuation_t1422769290_0_0_0_Types };
extern const Il2CppType VoidTaskResult_t3325310798_0_0_0;
static const Il2CppType* GenInst_VoidTaskResult_t3325310798_0_0_0_Types[] = { &VoidTaskResult_t3325310798_0_0_0 };
extern const Il2CppGenericInst GenInst_VoidTaskResult_t3325310798_0_0_0 = { 1, GenInst_VoidTaskResult_t3325310798_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_VoidTaskResult_t3325310798_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &VoidTaskResult_t3325310798_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_VoidTaskResult_t3325310798_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_VoidTaskResult_t3325310798_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_VoidTaskResult_t3325310798_0_0_0_Types[] = { &Il2CppObject_0_0_0, &VoidTaskResult_t3325310798_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_VoidTaskResult_t3325310798_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_VoidTaskResult_t3325310798_0_0_0_Types };
extern const Il2CppType Task_1_t2445339805_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t2445339805_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t2445339805_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t2445339805_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t2445339805_0_0_0_Types };
extern const Il2CppType TaskScheduler_t3932792796_0_0_0;
static const Il2CppType* GenInst_TaskScheduler_t3932792796_0_0_0_Il2CppObject_0_0_0_Types[] = { &TaskScheduler_t3932792796_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskScheduler_t3932792796_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TaskScheduler_t3932792796_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType UnobservedTaskExceptionEventArgs_t1087040814_0_0_0;
static const Il2CppType* GenInst_UnobservedTaskExceptionEventArgs_t1087040814_0_0_0_Types[] = { &UnobservedTaskExceptionEventArgs_t1087040814_0_0_0 };
extern const Il2CppGenericInst GenInst_UnobservedTaskExceptionEventArgs_t1087040814_0_0_0 = { 1, GenInst_UnobservedTaskExceptionEventArgs_t1087040814_0_0_0_Types };
extern const Il2CppType AsyncLocalValueChangedArgs_1_t1180157334_0_0_0;
static const Il2CppType* GenInst_AsyncLocalValueChangedArgs_1_t1180157334_0_0_0_Types[] = { &AsyncLocalValueChangedArgs_1_t1180157334_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncLocalValueChangedArgs_1_t1180157334_0_0_0 = { 1, GenInst_AsyncLocalValueChangedArgs_1_t1180157334_0_0_0_Types };
extern const Il2CppType IAsyncLocal_t3643786784_0_0_0;
static const Il2CppType* GenInst_IAsyncLocal_t3643786784_0_0_0_Il2CppObject_0_0_0_Types[] = { &IAsyncLocal_t3643786784_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncLocal_t3643786784_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IAsyncLocal_t3643786784_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncLocal_t3643786784_0_0_0_Types[] = { &IAsyncLocal_t3643786784_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncLocal_t3643786784_0_0_0 = { 1, GenInst_IAsyncLocal_t3643786784_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t761048256_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t761048256_0_0_0_Types[] = { &KeyValuePair_2_t761048256_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t761048256_0_0_0 = { 1, GenInst_KeyValuePair_2_t761048256_0_0_0_Types };
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t3500843524_0_0_0_Types[] = { &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t3500843524_0_0_0 = { 1, GenInst_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType AsyncLocalValueChangedArgs_1_t1991551563_0_0_0;
static const Il2CppType* GenInst_AsyncLocalValueChangedArgs_1_t1991551563_0_0_0_Types[] = { &AsyncLocalValueChangedArgs_1_t1991551563_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncLocalValueChangedArgs_1_t1991551563_0_0_0 = { 1, GenInst_AsyncLocalValueChangedArgs_1_t1991551563_0_0_0_Types };
extern const Il2CppType WorkStealingQueue_t2897576067_0_0_0;
static const Il2CppType* GenInst_WorkStealingQueue_t2897576067_0_0_0_Types[] = { &WorkStealingQueue_t2897576067_0_0_0 };
extern const Il2CppGenericInst GenInst_WorkStealingQueue_t2897576067_0_0_0 = { 1, GenInst_WorkStealingQueue_t2897576067_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType DirectoryInfo_t1934446453_0_0_0;
static const Il2CppType* GenInst_DirectoryInfo_t1934446453_0_0_0_Types[] = { &DirectoryInfo_t1934446453_0_0_0 };
extern const Il2CppGenericInst GenInst_DirectoryInfo_t1934446453_0_0_0 = { 1, GenInst_DirectoryInfo_t1934446453_0_0_0_Types };
extern const Il2CppType FileSystemInfo_t2360991899_0_0_0;
static const Il2CppType* GenInst_FileSystemInfo_t2360991899_0_0_0_Types[] = { &FileSystemInfo_t2360991899_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSystemInfo_t2360991899_0_0_0 = { 1, GenInst_FileSystemInfo_t2360991899_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType UriScheme_t683497865_0_0_0;
static const Il2CppType* GenInst_UriScheme_t683497865_0_0_0_Types[] = { &UriScheme_t683497865_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t683497865_0_0_0 = { 1, GenInst_UriScheme_t683497865_0_0_0_Types };
extern const Il2CppType FirstChanceExceptionEventArgs_t3799922078_0_0_0;
static const Il2CppType* GenInst_FirstChanceExceptionEventArgs_t3799922078_0_0_0_Types[] = { &FirstChanceExceptionEventArgs_t3799922078_0_0_0 };
extern const Il2CppGenericInst GenInst_FirstChanceExceptionEventArgs_t3799922078_0_0_0 = { 1, GenInst_FirstChanceExceptionEventArgs_t3799922078_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType CalendarData_t275297348_0_0_0;
static const Il2CppType* GenInst_CalendarData_t275297348_0_0_0_Types[] = { &CalendarData_t275297348_0_0_0 };
extern const Il2CppGenericInst GenInst_CalendarData_t275297348_0_0_0 = { 1, GenInst_CalendarData_t275297348_0_0_0_Types };
extern const Il2CppType InternalCodePageDataItem_t1742109366_0_0_0;
static const Il2CppType* GenInst_InternalCodePageDataItem_t1742109366_0_0_0_Types[] = { &InternalCodePageDataItem_t1742109366_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalCodePageDataItem_t1742109366_0_0_0 = { 1, GenInst_InternalCodePageDataItem_t1742109366_0_0_0_Types };
extern const Il2CppType InternalEncodingDataItem_t82919681_0_0_0;
static const Il2CppType* GenInst_InternalEncodingDataItem_t82919681_0_0_0_Types[] = { &InternalEncodingDataItem_t82919681_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEncodingDataItem_t82919681_0_0_0 = { 1, GenInst_InternalEncodingDataItem_t82919681_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Func_2_t768253681_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Func_2_t768253681_0_0_0_Types[] = { &String_t_0_0_0, &Func_2_t768253681_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Func_2_t768253681_0_0_0 = { 2, GenInst_String_t_0_0_0_Func_2_t768253681_0_0_0_Types };
static const Il2CppType* GenInst_StackTrace_t2500644597_0_0_0_String_t_0_0_0_Types[] = { &StackTrace_t2500644597_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_StackTrace_t2500644597_0_0_0_String_t_0_0_0 = { 2, GenInst_StackTrace_t2500644597_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Func_2_t768253681_0_0_0_Types[] = { &Func_2_t768253681_0_0_0 };
extern const Il2CppGenericInst GenInst_Func_2_t768253681_0_0_0 = { 1, GenInst_Func_2_t768253681_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t440378165_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t440378165_0_0_0_Types[] = { &KeyValuePair_2_t440378165_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t440378165_0_0_0 = { 1, GenInst_KeyValuePair_2_t440378165_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CultureInfo_t3500843524_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CultureInfo_t3500843524_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t266014381_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t266014381_0_0_0_Types[] = { &KeyValuePair_2_t266014381_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t266014381_0_0_0 = { 1, GenInst_KeyValuePair_2_t266014381_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CultureInfo_t3500843524_0_0_0_Types[] = { &String_t_0_0_0, &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CultureInfo_t3500843524_0_0_0 = { 2, GenInst_String_t_0_0_0_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3172968008_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3172968008_0_0_0_Types[] = { &KeyValuePair_2_t3172968008_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3172968008_0_0_0 = { 1, GenInst_KeyValuePair_2_t3172968008_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2849799027_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2849799027_0_0_0_Types[] = { &IFormatProvider_t2849799027_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2849799027_0_0_0 = { 1, GenInst_IFormatProvider_t2849799027_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType CustomAttributeBuilder_t2970303184_0_0_0;
static const Il2CppType* GenInst_CustomAttributeBuilder_t2970303184_0_0_0_Types[] = { &CustomAttributeBuilder_t2970303184_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t2970303184_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t2970303184_0_0_0_Types };
extern const Il2CppType _CustomAttributeBuilder_t3917123293_0_0_0;
static const Il2CppType* GenInst__CustomAttributeBuilder_t3917123293_0_0_0_Types[] = { &_CustomAttributeBuilder_t3917123293_0_0_0 };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t3917123293_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t3917123293_0_0_0_Types };
extern const Il2CppType MonoResource_t3127387157_0_0_0;
static const Il2CppType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { &MonoResource_t3127387157_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
extern const Il2CppType MonoWin32Resource_t2467306218_0_0_0;
static const Il2CppType* GenInst_MonoWin32Resource_t2467306218_0_0_0_Types[] = { &MonoWin32Resource_t2467306218_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoWin32Resource_t2467306218_0_0_0 = { 1, GenInst_MonoWin32Resource_t2467306218_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t2708608433_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types[] = { &RefEmitPermissionSet_t2708608433_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t2708608433_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType IList_1_t1844743827_0_0_0;
static const Il2CppType* GenInst_IList_1_t1844743827_0_0_0_Types[] = { &IList_1_t1844743827_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1844743827_0_0_0 = { 1, GenInst_IList_1_t1844743827_0_0_0_Types };
extern const Il2CppType ICollection_1_t2255878531_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2255878531_0_0_0_Types[] = { &ICollection_1_t2255878531_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255878531_0_0_0 = { 1, GenInst_ICollection_1_t2255878531_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1595930271_0_0_0_Types[] = { &IEnumerable_1_t1595930271_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1595930271_0_0_0 = { 1, GenInst_IEnumerable_1_t1595930271_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t3002181613_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t3002181613_0_0_0_Types[] = { &IReadOnlyList_1_t3002181613_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t3002181613_0_0_0 = { 1, GenInst_IReadOnlyList_1_t3002181613_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t891467953_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t891467953_0_0_0_Types[] = { &IReadOnlyCollection_1_t891467953_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t891467953_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t891467953_0_0_0_Types };
extern const Il2CppType IList_1_t643717440_0_0_0;
static const Il2CppType* GenInst_IList_1_t643717440_0_0_0_Types[] = { &IList_1_t643717440_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t643717440_0_0_0 = { 1, GenInst_IList_1_t643717440_0_0_0_Types };
extern const Il2CppType ICollection_1_t1054852144_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1054852144_0_0_0_Types[] = { &ICollection_1_t1054852144_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1054852144_0_0_0 = { 1, GenInst_ICollection_1_t1054852144_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t394903884_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t394903884_0_0_0_Types[] = { &IEnumerable_1_t394903884_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t394903884_0_0_0 = { 1, GenInst_IEnumerable_1_t394903884_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t1801155226_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t1801155226_0_0_0_Types[] = { &IReadOnlyList_1_t1801155226_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t1801155226_0_0_0 = { 1, GenInst_IReadOnlyList_1_t1801155226_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t3985408862_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t3985408862_0_0_0_Types[] = { &IReadOnlyCollection_1_t3985408862_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t3985408862_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t3985408862_0_0_0_Types };
extern const Il2CppType IList_1_t3952977575_0_0_0;
static const Il2CppType* GenInst_IList_1_t3952977575_0_0_0_Types[] = { &IList_1_t3952977575_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3952977575_0_0_0 = { 1, GenInst_IList_1_t3952977575_0_0_0_Types };
extern const Il2CppType ICollection_1_t69144983_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t69144983_0_0_0_Types[] = { &ICollection_1_t69144983_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t69144983_0_0_0 = { 1, GenInst_ICollection_1_t69144983_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3704164019_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3704164019_0_0_0_Types[] = { &IEnumerable_1_t3704164019_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3704164019_0_0_0 = { 1, GenInst_IEnumerable_1_t3704164019_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t815448065_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t815448065_0_0_0_Types[] = { &IReadOnlyList_1_t815448065_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t815448065_0_0_0 = { 1, GenInst_IReadOnlyList_1_t815448065_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t2999701701_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t2999701701_0_0_0_Types[] = { &IReadOnlyCollection_1_t2999701701_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t2999701701_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t2999701701_0_0_0_Types };
extern const Il2CppType IList_1_t289070565_0_0_0;
static const Il2CppType* GenInst_IList_1_t289070565_0_0_0_Types[] = { &IList_1_t289070565_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t289070565_0_0_0 = { 1, GenInst_IList_1_t289070565_0_0_0_Types };
extern const Il2CppType ICollection_1_t700205269_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t700205269_0_0_0_Types[] = { &ICollection_1_t700205269_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t700205269_0_0_0 = { 1, GenInst_ICollection_1_t700205269_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t40257009_0_0_0_Types[] = { &IEnumerable_1_t40257009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t40257009_0_0_0 = { 1, GenInst_IEnumerable_1_t40257009_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t1446508351_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t1446508351_0_0_0_Types[] = { &IReadOnlyList_1_t1446508351_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t1446508351_0_0_0 = { 1, GenInst_IReadOnlyList_1_t1446508351_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t3630761987_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t3630761987_0_0_0_Types[] = { &IReadOnlyCollection_1_t3630761987_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t3630761987_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t3630761987_0_0_0_Types };
extern const Il2CppType IList_1_t1043143288_0_0_0;
static const Il2CppType* GenInst_IList_1_t1043143288_0_0_0_Types[] = { &IList_1_t1043143288_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1043143288_0_0_0 = { 1, GenInst_IList_1_t1043143288_0_0_0_Types };
extern const Il2CppType ICollection_1_t1454277992_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1454277992_0_0_0_Types[] = { &ICollection_1_t1454277992_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1454277992_0_0_0 = { 1, GenInst_ICollection_1_t1454277992_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t794329732_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t794329732_0_0_0_Types[] = { &IEnumerable_1_t794329732_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t794329732_0_0_0 = { 1, GenInst_IEnumerable_1_t794329732_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t2200581074_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t2200581074_0_0_0_Types[] = { &IReadOnlyList_1_t2200581074_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t2200581074_0_0_0 = { 1, GenInst_IReadOnlyList_1_t2200581074_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t89867414_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t89867414_0_0_0_Types[] = { &IReadOnlyCollection_1_t89867414_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t89867414_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t89867414_0_0_0_Types };
extern const Il2CppType IList_1_t873662762_0_0_0;
static const Il2CppType* GenInst_IList_1_t873662762_0_0_0_Types[] = { &IList_1_t873662762_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t873662762_0_0_0 = { 1, GenInst_IList_1_t873662762_0_0_0_Types };
extern const Il2CppType ICollection_1_t1284797466_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1284797466_0_0_0_Types[] = { &ICollection_1_t1284797466_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1284797466_0_0_0 = { 1, GenInst_ICollection_1_t1284797466_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t624849206_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t624849206_0_0_0_Types[] = { &IEnumerable_1_t624849206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t624849206_0_0_0 = { 1, GenInst_IEnumerable_1_t624849206_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t2031100548_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t2031100548_0_0_0_Types[] = { &IReadOnlyList_1_t2031100548_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t2031100548_0_0_0 = { 1, GenInst_IReadOnlyList_1_t2031100548_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t4215354184_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t4215354184_0_0_0_Types[] = { &IReadOnlyCollection_1_t4215354184_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t4215354184_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t4215354184_0_0_0_Types };
extern const Il2CppType IList_1_t3230389896_0_0_0;
static const Il2CppType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { &IList_1_t3230389896_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
extern const Il2CppType ICollection_1_t3641524600_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3641524600_0_0_0_Types[] = { &ICollection_1_t3641524600_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3641524600_0_0_0 = { 1, GenInst_ICollection_1_t3641524600_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t92860386_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t92860386_0_0_0_Types[] = { &IReadOnlyList_1_t92860386_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t92860386_0_0_0 = { 1, GenInst_IReadOnlyList_1_t92860386_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t2277114022_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t2277114022_0_0_0_Types[] = { &IReadOnlyCollection_1_t2277114022_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t2277114022_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t2277114022_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ILExceptionBlock_t2042475189_0_0_0;
static const Il2CppType* GenInst_ILExceptionBlock_t2042475189_0_0_0_Types[] = { &ILExceptionBlock_t2042475189_0_0_0 };
extern const Il2CppGenericInst GenInst_ILExceptionBlock_t2042475189_0_0_0 = { 1, GenInst_ILExceptionBlock_t2042475189_0_0_0_Types };
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t2116499186_0_0_0_Types[] = { &LocalBuilder_t2116499186_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2116499186_0_0_0 = { 1, GenInst_LocalBuilder_t2116499186_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t61912499_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t61912499_0_0_0_Types[] = { &_LocalBuilder_t61912499_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t61912499_0_0_0 = { 1, GenInst__LocalBuilder_t61912499_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1749284021_0_0_0_Types[] = { &LocalVariableInfo_t1749284021_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1749284021_0_0_0 = { 1, GenInst_LocalVariableInfo_t1749284021_0_0_0_Types };
extern const Il2CppType ILExceptionInfo_t1490154598_0_0_0;
static const Il2CppType* GenInst_ILExceptionInfo_t1490154598_0_0_0_Types[] = { &ILExceptionInfo_t1490154598_0_0_0 };
extern const Il2CppGenericInst GenInst_ILExceptionInfo_t1490154598_0_0_0 = { 1, GenInst_ILExceptionInfo_t1490154598_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType Label_t4243202660_0_0_0;
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Types[] = { &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0 = { 1, GenInst_Label_t4243202660_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType TypeName_t2073290883_0_0_0;
static const Il2CppType* GenInst_TypeName_t2073290883_0_0_0_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeName_t2073290883_0_0_0, &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeName_t2073290883_0_0_0_TypeBuilder_t3308873219_0_0_0 = { 2, GenInst_TypeName_t2073290883_0_0_0_TypeBuilder_t3308873219_0_0_0_Types };
static const Il2CppType* GenInst_TypeName_t2073290883_0_0_0_Types[] = { &TypeName_t2073290883_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeName_t2073290883_0_0_0 = { 1, GenInst_TypeName_t2073290883_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t73557541_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t73557541_0_0_0_Types[] = { &KeyValuePair_2_t73557541_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t73557541_0_0_0 = { 1, GenInst_KeyValuePair_2_t73557541_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType EventBuilder_t2927243889_0_0_0;
static const Il2CppType* GenInst_EventBuilder_t2927243889_0_0_0_Types[] = { &EventBuilder_t2927243889_0_0_0 };
extern const Il2CppGenericInst GenInst_EventBuilder_t2927243889_0_0_0 = { 1, GenInst_EventBuilder_t2927243889_0_0_0_Types };
extern const Il2CppType _EventBuilder_t801715496_0_0_0;
static const Il2CppType* GenInst__EventBuilder_t801715496_0_0_0_Types[] = { &_EventBuilder_t801715496_0_0_0 };
extern const Il2CppGenericInst GenInst__EventBuilder_t801715496_0_0_0 = { 1, GenInst__EventBuilder_t801715496_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType DecimalConstantAttribute_t569828555_0_0_0;
static const Il2CppType* GenInst_DecimalConstantAttribute_t569828555_0_0_0_Types[] = { &DecimalConstantAttribute_t569828555_0_0_0 };
extern const Il2CppGenericInst GenInst_DecimalConstantAttribute_t569828555_0_0_0 = { 1, GenInst_DecimalConstantAttribute_t569828555_0_0_0_Types };
extern const Il2CppType DateTimeConstantAttribute_t4275890373_0_0_0;
static const Il2CppType* GenInst_DateTimeConstantAttribute_t4275890373_0_0_0_Types[] = { &DateTimeConstantAttribute_t4275890373_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeConstantAttribute_t4275890373_0_0_0 = { 1, GenInst_DateTimeConstantAttribute_t4275890373_0_0_0_Types };
extern const Il2CppType CustomConstantAttribute_t2797584351_0_0_0;
static const Il2CppType* GenInst_CustomConstantAttribute_t2797584351_0_0_0_Types[] = { &CustomConstantAttribute_t2797584351_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomConstantAttribute_t2797584351_0_0_0 = { 1, GenInst_CustomConstantAttribute_t2797584351_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t975927710_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t975927710_0_0_0_Types[] = { &KeyValuePair_2_t975927710_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t975927710_0_0_0 = { 1, GenInst_KeyValuePair_2_t975927710_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t41069825_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t41069825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType IIdentityPermissionFactory_t2988326850_0_0_0;
static const Il2CppType* GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types[] = { &IIdentityPermissionFactory_t2988326850_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2988326850_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types };
extern const Il2CppType IBuiltInEvidence_t1114073477_0_0_0;
static const Il2CppType* GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types[] = { &IBuiltInEvidence_t1114073477_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1114073477_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types };
extern const Il2CppType EvidenceBase_t1783132120_0_0_0;
static const Il2CppType* GenInst_EvidenceBase_t1783132120_0_0_0_Types[] = { &EvidenceBase_t1783132120_0_0_0 };
extern const Il2CppGenericInst GenInst_EvidenceBase_t1783132120_0_0_0 = { 1, GenInst_EvidenceBase_t1783132120_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t3638993531_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t3638993531_0_0_0_Types[] = { &CodeConnectAccess_t3638993531_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t3638993531_0_0_0 = { 1, GenInst_CodeConnectAccess_t3638993531_0_0_0_Types };
extern const Il2CppType Timer_t791717973_0_0_0;
static const Il2CppType* GenInst_Timer_t791717973_0_0_0_Types[] = { &Timer_t791717973_0_0_0 };
extern const Il2CppGenericInst GenInst_Timer_t791717973_0_0_0 = { 1, GenInst_Timer_t791717973_0_0_0_Types };
extern const Il2CppType AttributeUsageAttribute_t1057435127_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_AttributeUsageAttribute_t1057435127_0_0_0_Types[] = { &Type_t_0_0_0, &AttributeUsageAttribute_t1057435127_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_AttributeUsageAttribute_t1057435127_0_0_0 = { 2, GenInst_Type_t_0_0_0_AttributeUsageAttribute_t1057435127_0_0_0_Types };
static const Il2CppType* GenInst_AttributeUsageAttribute_t1057435127_0_0_0_Types[] = { &AttributeUsageAttribute_t1057435127_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeUsageAttribute_t1057435127_0_0_0 = { 1, GenInst_AttributeUsageAttribute_t1057435127_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t752138246_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t752138246_0_0_0_Types[] = { &KeyValuePair_2_t752138246_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t752138246_0_0_0 = { 1, GenInst_KeyValuePair_2_t752138246_0_0_0_Types };
extern const Il2CppType AttributeInfo_t2366110328_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_AttributeInfo_t2366110328_0_0_0_Types[] = { &Type_t_0_0_0, &AttributeInfo_t2366110328_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_AttributeInfo_t2366110328_0_0_0 = { 2, GenInst_Type_t_0_0_0_AttributeInfo_t2366110328_0_0_0_Types };
static const Il2CppType* GenInst_AttributeInfo_t2366110328_0_0_0_Types[] = { &AttributeInfo_t2366110328_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeInfo_t2366110328_0_0_0 = { 1, GenInst_AttributeInfo_t2366110328_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2060813447_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2060813447_0_0_0_Types[] = { &KeyValuePair_2_t2060813447_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2060813447_0_0_0 = { 1, GenInst_KeyValuePair_2_t2060813447_0_0_0_Types };
extern const Il2CppType FormatParam_t2947516451_0_0_0;
static const Il2CppType* GenInst_FormatParam_t2947516451_0_0_0_Types[] = { &FormatParam_t2947516451_0_0_0 };
extern const Il2CppGenericInst GenInst_FormatParam_t2947516451_0_0_0 = { 1, GenInst_FormatParam_t2947516451_0_0_0_Types };
extern const Il2CppType TermInfoStrings_t1425267120_0_0_0;
static const Il2CppType* GenInst_TermInfoStrings_t1425267120_0_0_0_Types[] = { &TermInfoStrings_t1425267120_0_0_0 };
extern const Il2CppGenericInst GenInst_TermInfoStrings_t1425267120_0_0_0 = { 1, GenInst_TermInfoStrings_t1425267120_0_0_0_Types };
extern const Il2CppType TimeZoneInfo_t436210607_0_0_0;
static const Il2CppType* GenInst_TimeZoneInfo_t436210607_0_0_0_Types[] = { &TimeZoneInfo_t436210607_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeZoneInfo_t436210607_0_0_0 = { 1, GenInst_TimeZoneInfo_t436210607_0_0_0_Types };
extern const Il2CppType AdjustmentRule_t2179708818_0_0_0;
static const Il2CppType* GenInst_AdjustmentRule_t2179708818_0_0_0_Types[] = { &AdjustmentRule_t2179708818_0_0_0 };
extern const Il2CppGenericInst GenInst_AdjustmentRule_t2179708818_0_0_0 = { 1, GenInst_AdjustmentRule_t2179708818_0_0_0_Types };
extern const Il2CppType IEquatable_1_t88723691_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t88723691_0_0_0_Types[] = { &IEquatable_1_t88723691_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t88723691_0_0_0 = { 1, GenInst_IEquatable_1_t88723691_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType TZifType_t1855764066_0_0_0;
static const Il2CppType* GenInst_TZifType_t1855764066_0_0_0_Types[] = { &TZifType_t1855764066_0_0_0 };
extern const Il2CppGenericInst GenInst_TZifType_t1855764066_0_0_0 = { 1, GenInst_TZifType_t1855764066_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TimeZoneInfo_t436210607_0_0_0_Types[] = { &String_t_0_0_0, &TimeZoneInfo_t436210607_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeZoneInfo_t436210607_0_0_0 = { 2, GenInst_String_t_0_0_0_TimeZoneInfo_t436210607_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t108335091_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t108335091_0_0_0_Types[] = { &KeyValuePair_2_t108335091_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t108335091_0_0_0 = { 1, GenInst_KeyValuePair_2_t108335091_0_0_0_Types };
extern const Il2CppType TransitionTime_t3441274853_0_0_0;
static const Il2CppType* GenInst_TransitionTime_t3441274853_0_0_0_Types[] = { &TransitionTime_t3441274853_0_0_0 };
extern const Il2CppGenericInst GenInst_TransitionTime_t3441274853_0_0_0 = { 1, GenInst_TransitionTime_t3441274853_0_0_0_Types };
extern const Il2CppType ModifierSpec_t570925440_0_0_0;
static const Il2CppType* GenInst_ModifierSpec_t570925440_0_0_0_Types[] = { &ModifierSpec_t570925440_0_0_0 };
extern const Il2CppGenericInst GenInst_ModifierSpec_t570925440_0_0_0 = { 1, GenInst_ModifierSpec_t570925440_0_0_0_Types };
extern const Il2CppType TypeIdentifier_t2572806063_0_0_0;
static const Il2CppType* GenInst_TypeIdentifier_t2572806063_0_0_0_Types[] = { &TypeIdentifier_t2572806063_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeIdentifier_t2572806063_0_0_0 = { 1, GenInst_TypeIdentifier_t2572806063_0_0_0_Types };
extern const Il2CppType TypeSpec_t2066641911_0_0_0;
static const Il2CppType* GenInst_TypeSpec_t2066641911_0_0_0_Types[] = { &TypeSpec_t2066641911_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeSpec_t2066641911_0_0_0 = { 1, GenInst_TypeSpec_t2066641911_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType IList_1_t4224045037_0_0_0;
static const Il2CppType* GenInst_IList_1_t4224045037_0_0_0_Types[] = { &IList_1_t4224045037_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4224045037_0_0_0 = { 1, GenInst_IList_1_t4224045037_0_0_0_Types };
extern const Il2CppType ICollection_1_t340212445_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t340212445_0_0_0_Types[] = { &ICollection_1_t340212445_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t340212445_0_0_0 = { 1, GenInst_ICollection_1_t340212445_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3975231481_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3975231481_0_0_0_Types[] = { &IEnumerable_1_t3975231481_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3975231481_0_0_0 = { 1, GenInst_IEnumerable_1_t3975231481_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t1086515527_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t1086515527_0_0_0_Types[] = { &IReadOnlyList_1_t1086515527_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t1086515527_0_0_0 = { 1, GenInst_IReadOnlyList_1_t1086515527_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t3270769163_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t3270769163_0_0_0_Types[] = { &IReadOnlyCollection_1_t3270769163_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t3270769163_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t3270769163_0_0_0_Types };
extern const Il2CppType CipherSuite_t491456551_0_0_0;
static const Il2CppType* GenInst_CipherSuite_t491456551_0_0_0_Types[] = { &CipherSuite_t491456551_0_0_0 };
extern const Il2CppGenericInst GenInst_CipherSuite_t491456551_0_0_0 = { 1, GenInst_CipherSuite_t491456551_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType MonoSslPolicyErrors_t621534536_0_0_0;
static const Il2CppType* GenInst_MonoSslPolicyErrors_t621534536_0_0_0_Types[] = { &MonoSslPolicyErrors_t621534536_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoSslPolicyErrors_t621534536_0_0_0 = { 1, GenInst_MonoSslPolicyErrors_t621534536_0_0_0_Types };
extern const Il2CppType TlsProtocols_t1951446164_0_0_0;
static const Il2CppType* GenInst_TlsProtocols_t1951446164_0_0_0_Types[] = { &TlsProtocols_t1951446164_0_0_0 };
extern const Il2CppGenericInst GenInst_TlsProtocols_t1951446164_0_0_0 = { 1, GenInst_TlsProtocols_t1951446164_0_0_0_Types };
extern const Il2CppType CipherSuiteCode_t4229451518_0_0_0;
static const Il2CppType* GenInst_CipherSuiteCode_t4229451518_0_0_0_Types[] = { &CipherSuiteCode_t4229451518_0_0_0 };
extern const Il2CppGenericInst GenInst_CipherSuiteCode_t4229451518_0_0_0 = { 1, GenInst_CipherSuiteCode_t4229451518_0_0_0_Types };
extern const Il2CppType Chunk_t3860501603_0_0_0;
static const Il2CppType* GenInst_Chunk_t3860501603_0_0_0_Types[] = { &Chunk_t3860501603_0_0_0 };
extern const Il2CppGenericInst GenInst_Chunk_t3860501603_0_0_0 = { 1, GenInst_Chunk_t3860501603_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
static const Il2CppType* GenInst_Stream_t3255436806_0_0_0_Types[] = { &Stream_t3255436806_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0 = { 1, GenInst_Stream_t3255436806_0_0_0_Types };
extern const Il2CppType SimpleAsyncResult_t2937691397_0_0_0;
static const Il2CppType* GenInst_SimpleAsyncResult_t2937691397_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &SimpleAsyncResult_t2937691397_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleAsyncResult_t2937691397_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_SimpleAsyncResult_t2937691397_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GetProxyData_t1386489386_0_0_0;
static const Il2CppType* GenInst_GetProxyData_t1386489386_0_0_0_Types[] = { &GetProxyData_t1386489386_0_0_0 };
extern const Il2CppGenericInst GenInst_GetProxyData_t1386489386_0_0_0 = { 1, GenInst_GetProxyData_t1386489386_0_0_0_Types };
extern const Il2CppType CFProxy_t537977545_0_0_0;
static const Il2CppType* GenInst_CFProxy_t537977545_0_0_0_Types[] = { &CFProxy_t537977545_0_0_0 };
extern const Il2CppGenericInst GenInst_CFProxy_t537977545_0_0_0 = { 1, GenInst_CFProxy_t537977545_0_0_0_Types };
extern const Il2CppType NetworkInterface_t63927633_0_0_0;
static const Il2CppType* GenInst_NetworkInterface_t63927633_0_0_0_Types[] = { &NetworkInterface_t63927633_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkInterface_t63927633_0_0_0 = { 1, GenInst_NetworkInterface_t63927633_0_0_0_Types };
extern const Il2CppType MacOsNetworkInterface_t1454185290_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { &String_t_0_0_0, &MacOsNetworkInterface_t1454185290_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0 = { 2, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types };
static const Il2CppType* GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { &MacOsNetworkInterface_t1454185290_0_0_0 };
extern const Il2CppGenericInst GenInst_MacOsNetworkInterface_t1454185290_0_0_0 = { 1, GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1126309774_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1126309774_0_0_0_Types[] = { &KeyValuePair_2_t1126309774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1126309774_0_0_0 = { 1, GenInst_KeyValuePair_2_t1126309774_0_0_0_Types };
extern const Il2CppType LinuxNetworkInterface_t3864470295_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { &String_t_0_0_0, &LinuxNetworkInterface_t3864470295_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0 = { 2, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types };
static const Il2CppType* GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { &LinuxNetworkInterface_t3864470295_0_0_0 };
extern const Il2CppGenericInst GenInst_LinuxNetworkInterface_t3864470295_0_0_0 = { 1, GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3536594779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3536594779_0_0_0_Types[] = { &KeyValuePair_2_t3536594779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3536594779_0_0_0 = { 1, GenInst_KeyValuePair_2_t3536594779_0_0_0_Types };
extern const Il2CppType Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0;
static const Il2CppType* GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types[] = { &Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 = { 1, GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types };
extern const Il2CppType WebConnectionGroup_t3242458773_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_WebConnectionGroup_t3242458773_0_0_0_Types[] = { &String_t_0_0_0, &WebConnectionGroup_t3242458773_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebConnectionGroup_t3242458773_0_0_0 = { 2, GenInst_String_t_0_0_0_WebConnectionGroup_t3242458773_0_0_0_Types };
static const Il2CppType* GenInst_WebConnectionGroup_t3242458773_0_0_0_Types[] = { &WebConnectionGroup_t3242458773_0_0_0 };
extern const Il2CppGenericInst GenInst_WebConnectionGroup_t3242458773_0_0_0 = { 1, GenInst_WebConnectionGroup_t3242458773_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2914583257_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2914583257_0_0_0_Types[] = { &KeyValuePair_2_t2914583257_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2914583257_0_0_0 = { 1, GenInst_KeyValuePair_2_t2914583257_0_0_0_Types };
extern const Il2CppType Thread_t241561612_0_0_0;
static const Il2CppType* GenInst_Thread_t241561612_0_0_0_Types[] = { &Thread_t241561612_0_0_0 };
extern const Il2CppGenericInst GenInst_Thread_t241561612_0_0_0 = { 1, GenInst_Thread_t241561612_0_0_0_Types };
static const Il2CppType* GenInst_Thread_t241561612_0_0_0_StackTrace_t2500644597_0_0_0_Types[] = { &Thread_t241561612_0_0_0, &StackTrace_t2500644597_0_0_0 };
extern const Il2CppGenericInst GenInst_Thread_t241561612_0_0_0_StackTrace_t2500644597_0_0_0 = { 2, GenInst_Thread_t241561612_0_0_0_StackTrace_t2500644597_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2926637898_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2926637898_0_0_0_Types[] = { &KeyValuePair_2_t2926637898_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2926637898_0_0_0 = { 1, GenInst_KeyValuePair_2_t2926637898_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t221307626_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t221307626_0_0_0_Types[] = { &KeyValuePair_2_t221307626_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t221307626_0_0_0 = { 1, GenInst_KeyValuePair_2_t221307626_0_0_0_Types };
extern const Il2CppType IOSelectorJob_t2021937086_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_IOSelectorJob_t2021937086_0_0_0_Types[] = { &IntPtr_t_0_0_0, &IOSelectorJob_t2021937086_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IOSelectorJob_t2021937086_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IOSelectorJob_t2021937086_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
extern const Il2CppType WSABUF_t2199312694_0_0_0;
static const Il2CppType* GenInst_WSABUF_t2199312694_0_0_0_Types[] = { &WSABUF_t2199312694_0_0_0 };
extern const Il2CppGenericInst GenInst_WSABUF_t2199312694_0_0_0 = { 1, GenInst_WSABUF_t2199312694_0_0_0_Types };
extern const Il2CppType GCHandle_t3409268066_0_0_0;
static const Il2CppType* GenInst_GCHandle_t3409268066_0_0_0_Types[] = { &GCHandle_t3409268066_0_0_0 };
extern const Il2CppGenericInst GenInst_GCHandle_t3409268066_0_0_0 = { 1, GenInst_GCHandle_t3409268066_0_0_0_Types };
extern const Il2CppType SocketAsyncEventArgs_t2815111766_0_0_0;
static const Il2CppType* GenInst_SocketAsyncEventArgs_t2815111766_0_0_0_Types[] = { &SocketAsyncEventArgs_t2815111766_0_0_0 };
extern const Il2CppGenericInst GenInst_SocketAsyncEventArgs_t2815111766_0_0_0 = { 1, GenInst_SocketAsyncEventArgs_t2815111766_0_0_0_Types };
extern const Il2CppType ConnectionState_t2608615043_0_0_0;
static const Il2CppType* GenInst_ConnectionState_t2608615043_0_0_0_Types[] = { &ConnectionState_t2608615043_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionState_t2608615043_0_0_0 = { 1, GenInst_ConnectionState_t2608615043_0_0_0_Types };
extern const Il2CppType WebConnection_t324679648_0_0_0;
static const Il2CppType* GenInst_WebConnection_t324679648_0_0_0_Types[] = { &WebConnection_t324679648_0_0_0 };
extern const Il2CppGenericInst GenInst_WebConnection_t324679648_0_0_0 = { 1, GenInst_WebConnection_t324679648_0_0_0_Types };
extern const Il2CppType X509CertificateImpl_t3842064707_0_0_0;
static const Il2CppType* GenInst_X509CertificateImpl_t3842064707_0_0_0_Types[] = { &X509CertificateImpl_t3842064707_0_0_0 };
extern const Il2CppGenericInst GenInst_X509CertificateImpl_t3842064707_0_0_0 = { 1, GenInst_X509CertificateImpl_t3842064707_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType DnsResourceRecord_t2943454412_0_0_0;
static const Il2CppType* GenInst_DnsResourceRecord_t2943454412_0_0_0_Types[] = { &DnsResourceRecord_t2943454412_0_0_0 };
extern const Il2CppGenericInst GenInst_DnsResourceRecord_t2943454412_0_0_0 = { 1, GenInst_DnsResourceRecord_t2943454412_0_0_0_Types };
extern const Il2CppType DnsQuestion_t3090842959_0_0_0;
static const Il2CppType* GenInst_DnsQuestion_t3090842959_0_0_0_Types[] = { &DnsQuestion_t3090842959_0_0_0 };
extern const Il2CppGenericInst GenInst_DnsQuestion_t3090842959_0_0_0 = { 1, GenInst_DnsQuestion_t3090842959_0_0_0_Types };
extern const Il2CppType IPEndPoint_t2615413766_0_0_0;
static const Il2CppType* GenInst_IPEndPoint_t2615413766_0_0_0_Types[] = { &IPEndPoint_t2615413766_0_0_0 };
extern const Il2CppGenericInst GenInst_IPEndPoint_t2615413766_0_0_0 = { 1, GenInst_IPEndPoint_t2615413766_0_0_0_Types };
extern const Il2CppType EndPoint_t4156119363_0_0_0;
static const Il2CppType* GenInst_EndPoint_t4156119363_0_0_0_Types[] = { &EndPoint_t4156119363_0_0_0 };
extern const Il2CppGenericInst GenInst_EndPoint_t4156119363_0_0_0 = { 1, GenInst_EndPoint_t4156119363_0_0_0_Types };
extern const Il2CppType SimpleResolverEventArgs_t4294564137_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SimpleResolverEventArgs_t4294564137_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SimpleResolverEventArgs_t4294564137_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SimpleResolverEventArgs_t4294564137_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_SimpleResolverEventArgs_t4294564137_0_0_0_Types };
static const Il2CppType* GenInst_SimpleResolverEventArgs_t4294564137_0_0_0_Types[] = { &SimpleResolverEventArgs_t4294564137_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleResolverEventArgs_t4294564137_0_0_0 = { 1, GenInst_SimpleResolverEventArgs_t4294564137_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1059734994_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1059734994_0_0_0_Types[] = { &KeyValuePair_2_t1059734994_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1059734994_0_0_0 = { 1, GenInst_KeyValuePair_2_t1059734994_0_0_0_Types };
extern const Il2CppType CachedCodeEntry_t3553821051_0_0_0;
static const Il2CppType* GenInst_CachedCodeEntry_t3553821051_0_0_0_Types[] = { &CachedCodeEntry_t3553821051_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedCodeEntry_t3553821051_0_0_0 = { 1, GenInst_CachedCodeEntry_t3553821051_0_0_0_Types };
extern const Il2CppType SingleRange_t3794243288_0_0_0;
static const Il2CppType* GenInst_SingleRange_t3794243288_0_0_0_Types[] = { &SingleRange_t3794243288_0_0_0 };
extern const Il2CppGenericInst GenInst_SingleRange_t3794243288_0_0_0 = { 1, GenInst_SingleRange_t3794243288_0_0_0_Types };
extern const Il2CppType LowerCaseMapping_t2153935826_0_0_0;
static const Il2CppType* GenInst_LowerCaseMapping_t2153935826_0_0_0_Types[] = { &LowerCaseMapping_t2153935826_0_0_0 };
extern const Il2CppGenericInst GenInst_LowerCaseMapping_t2153935826_0_0_0 = { 1, GenInst_LowerCaseMapping_t2153935826_0_0_0_Types };
extern const Il2CppType BacktrackNote_t1775865058_0_0_0;
static const Il2CppType* GenInst_BacktrackNote_t1775865058_0_0_0_Types[] = { &BacktrackNote_t1775865058_0_0_0 };
extern const Il2CppGenericInst GenInst_BacktrackNote_t1775865058_0_0_0 = { 1, GenInst_BacktrackNote_t1775865058_0_0_0_Types };
extern const Il2CppType RegexFC_t2009854258_0_0_0;
static const Il2CppType* GenInst_RegexFC_t2009854258_0_0_0_Types[] = { &RegexFC_t2009854258_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexFC_t2009854258_0_0_0 = { 1, GenInst_RegexFC_t2009854258_0_0_0_Types };
extern const Il2CppType RegexNode_t2469392321_0_0_0;
static const Il2CppType* GenInst_RegexNode_t2469392321_0_0_0_Types[] = { &RegexNode_t2469392321_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexNode_t2469392321_0_0_0 = { 1, GenInst_RegexNode_t2469392321_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType RegexOptions_t2418259727_0_0_0;
static const Il2CppType* GenInst_RegexOptions_t2418259727_0_0_0_Types[] = { &RegexOptions_t2418259727_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexOptions_t2418259727_0_0_0 = { 1, GenInst_RegexOptions_t2418259727_0_0_0_Types };
extern const Il2CppType AttributeEntry_t168441916_0_0_0;
static const Il2CppType* GenInst_AttributeEntry_t168441916_0_0_0_Types[] = { &AttributeEntry_t168441916_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeEntry_t168441916_0_0_0 = { 1, GenInst_AttributeEntry_t168441916_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType EventDescriptor_t962731901_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t962731901_0_0_0_Types[] = { &EventDescriptor_t962731901_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t962731901_0_0_0 = { 1, GenInst_EventDescriptor_t962731901_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType WeakReference_t1077405567_0_0_0;
static const Il2CppType* GenInst_WeakReference_t1077405567_0_0_0_Types[] = { &WeakReference_t1077405567_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakReference_t1077405567_0_0_0 = { 1, GenInst_WeakReference_t1077405567_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType UriParser_t1012511323_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_UriParser_t1012511323_0_0_0_Types[] = { &String_t_0_0_0, &UriParser_t1012511323_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UriParser_t1012511323_0_0_0 = { 2, GenInst_String_t_0_0_0_UriParser_t1012511323_0_0_0_Types };
static const Il2CppType* GenInst_UriParser_t1012511323_0_0_0_Types[] = { &UriParser_t1012511323_0_0_0 };
extern const Il2CppGenericInst GenInst_UriParser_t1012511323_0_0_0 = { 1, GenInst_UriParser_t1012511323_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t684635807_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t684635807_0_0_0_Types[] = { &KeyValuePair_2_t684635807_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t684635807_0_0_0 = { 1, GenInst_KeyValuePair_2_t684635807_0_0_0_Types };
extern const Il2CppType HeaderInfo_t3044570949_0_0_0;
static const Il2CppType* GenInst_HeaderInfo_t3044570949_0_0_0_Types[] = { &HeaderInfo_t3044570949_0_0_0 };
extern const Il2CppGenericInst GenInst_HeaderInfo_t3044570949_0_0_0 = { 1, GenInst_HeaderInfo_t3044570949_0_0_0_Types };
extern const Il2CppType ICredentials_t3855617113_0_0_0;
static const Il2CppType* GenInst_ICredentials_t3855617113_0_0_0_Types[] = { &ICredentials_t3855617113_0_0_0 };
extern const Il2CppGenericInst GenInst_ICredentials_t3855617113_0_0_0 = { 1, GenInst_ICredentials_t3855617113_0_0_0_Types };
extern const Il2CppType RecognizedAttribute_t3930529114_0_0_0;
static const Il2CppType* GenInst_RecognizedAttribute_t3930529114_0_0_0_Types[] = { &RecognizedAttribute_t3930529114_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedAttribute_t3930529114_0_0_0 = { 1, GenInst_RecognizedAttribute_t3930529114_0_0_0_Types };
extern const Il2CppType HeaderVariantInfo_t2058796272_0_0_0;
static const Il2CppType* GenInst_HeaderVariantInfo_t2058796272_0_0_0_Types[] = { &HeaderVariantInfo_t2058796272_0_0_0 };
extern const Il2CppGenericInst GenInst_HeaderVariantInfo_t2058796272_0_0_0 = { 1, GenInst_HeaderVariantInfo_t2058796272_0_0_0_Types };
extern const Il2CppType RfcChar_t1416622761_0_0_0;
static const Il2CppType* GenInst_RfcChar_t1416622761_0_0_0_Types[] = { &RfcChar_t1416622761_0_0_0 };
extern const Il2CppGenericInst GenInst_RfcChar_t1416622761_0_0_0 = { 1, GenInst_RfcChar_t1416622761_0_0_0_Types };
extern const Il2CppType Regex_t1803876613_0_0_0;
static const Il2CppType* GenInst_Regex_t1803876613_0_0_0_Types[] = { &Regex_t1803876613_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
extern const Il2CppType ConfigurationProperty_t2048066811_0_0_0;
static const Il2CppType* GenInst_ConfigurationProperty_t2048066811_0_0_0_Types[] = { &ConfigurationProperty_t2048066811_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationProperty_t2048066811_0_0_0 = { 1, GenInst_ConfigurationProperty_t2048066811_0_0_0_Types };
extern const Il2CppType XPathNode_t3118381855_0_0_0;
static const Il2CppType* GenInst_XPathNode_t3118381855_0_0_0_Types[] = { &XPathNode_t3118381855_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNode_t3118381855_0_0_0 = { 1, GenInst_XPathNode_t3118381855_0_0_0_Types };
extern const Il2CppType IDtdDefaultAttributeInfo_t2326795264_0_0_0;
static const Il2CppType* GenInst_IDtdDefaultAttributeInfo_t2326795264_0_0_0_Types[] = { &IDtdDefaultAttributeInfo_t2326795264_0_0_0 };
extern const Il2CppGenericInst GenInst_IDtdDefaultAttributeInfo_t2326795264_0_0_0 = { 1, GenInst_IDtdDefaultAttributeInfo_t2326795264_0_0_0_Types };
extern const Il2CppType IDtdAttributeInfo_t728124959_0_0_0;
static const Il2CppType* GenInst_IDtdAttributeInfo_t728124959_0_0_0_Types[] = { &IDtdAttributeInfo_t728124959_0_0_0 };
extern const Il2CppGenericInst GenInst_IDtdAttributeInfo_t728124959_0_0_0 = { 1, GenInst_IDtdAttributeInfo_t728124959_0_0_0_Types };
extern const Il2CppType ParsingState_t1278724163_0_0_0;
static const Il2CppType* GenInst_ParsingState_t1278724163_0_0_0_Types[] = { &ParsingState_t1278724163_0_0_0 };
extern const Il2CppGenericInst GenInst_ParsingState_t1278724163_0_0_0 = { 1, GenInst_ParsingState_t1278724163_0_0_0_Types };
extern const Il2CppType Tuple_4_t3284226035_0_0_0;
static const Il2CppType* GenInst_Tuple_4_t3284226035_0_0_0_Types[] = { &Tuple_4_t3284226035_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_4_t3284226035_0_0_0 = { 1, GenInst_Tuple_4_t3284226035_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Task_1_t2404255042_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t2404255042_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t2404255042_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t2404255042_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t2404255042_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Tuple_4_t3284226035_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Tuple_4_t3284226035_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Tuple_4_t3284226035_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Tuple_4_t3284226035_0_0_0_Types };
extern const Il2CppType NodeData_t2613273532_0_0_0;
static const Il2CppType* GenInst_NodeData_t2613273532_0_0_0_Types[] = { &NodeData_t2613273532_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeData_t2613273532_0_0_0 = { 1, GenInst_NodeData_t2613273532_0_0_0_Types };
extern const Il2CppType IDtdEntityInfo_t4127388056_0_0_0;
static const Il2CppType* GenInst_IDtdEntityInfo_t4127388056_0_0_0_IDtdEntityInfo_t4127388056_0_0_0_Types[] = { &IDtdEntityInfo_t4127388056_0_0_0, &IDtdEntityInfo_t4127388056_0_0_0 };
extern const Il2CppGenericInst GenInst_IDtdEntityInfo_t4127388056_0_0_0_IDtdEntityInfo_t4127388056_0_0_0 = { 2, GenInst_IDtdEntityInfo_t4127388056_0_0_0_IDtdEntityInfo_t4127388056_0_0_0_Types };
static const Il2CppType* GenInst_IDtdEntityInfo_t4127388056_0_0_0_Types[] = { &IDtdEntityInfo_t4127388056_0_0_0 };
extern const Il2CppGenericInst GenInst_IDtdEntityInfo_t4127388056_0_0_0 = { 1, GenInst_IDtdEntityInfo_t4127388056_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1617559729_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1617559729_0_0_0_Types[] = { &KeyValuePair_2_t1617559729_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1617559729_0_0_0 = { 1, GenInst_KeyValuePair_2_t1617559729_0_0_0_Types };
extern const Il2CppType TagInfo_t1244462138_0_0_0;
static const Il2CppType* GenInst_TagInfo_t1244462138_0_0_0_Types[] = { &TagInfo_t1244462138_0_0_0 };
extern const Il2CppGenericInst GenInst_TagInfo_t1244462138_0_0_0 = { 1, GenInst_TagInfo_t1244462138_0_0_0_Types };
extern const Il2CppType State_t563545493_0_0_0;
static const Il2CppType* GenInst_State_t563545493_0_0_0_Types[] = { &State_t563545493_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t563545493_0_0_0 = { 1, GenInst_State_t563545493_0_0_0_Types };
extern const Il2CppType Namespace_t1808381789_0_0_0;
static const Il2CppType* GenInst_Namespace_t1808381789_0_0_0_Types[] = { &Namespace_t1808381789_0_0_0 };
extern const Il2CppGenericInst GenInst_Namespace_t1808381789_0_0_0 = { 1, GenInst_Namespace_t1808381789_0_0_0_Types };
extern const Il2CppType XmlName_t3016058992_0_0_0;
static const Il2CppType* GenInst_XmlName_t3016058992_0_0_0_Types[] = { &XmlName_t3016058992_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlName_t3016058992_0_0_0 = { 1, GenInst_XmlName_t3016058992_0_0_0_Types };
extern const Il2CppType IXmlSchemaInfo_t2533799901_0_0_0;
static const Il2CppType* GenInst_IXmlSchemaInfo_t2533799901_0_0_0_Types[] = { &IXmlSchemaInfo_t2533799901_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlSchemaInfo_t2533799901_0_0_0 = { 1, GenInst_IXmlSchemaInfo_t2533799901_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t1944712516_0_0_0;
extern const Il2CppType SchemaAttDef_t1510907267_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaAttDef_t1510907267_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0, &SchemaAttDef_t1510907267_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaAttDef_t1510907267_0_0_0 = { 2, GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaAttDef_t1510907267_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0 = { 1, GenInst_XmlQualifiedName_t1944712516_0_0_0_Types };
static const Il2CppType* GenInst_SchemaAttDef_t1510907267_0_0_0_Types[] = { &SchemaAttDef_t1510907267_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaAttDef_t1510907267_0_0_0 = { 1, GenInst_SchemaAttDef_t1510907267_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t257992896_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t257992896_0_0_0_Types[] = { &KeyValuePair_2_t257992896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t257992896_0_0_0 = { 1, GenInst_KeyValuePair_2_t257992896_0_0_0_Types };
extern const Il2CppType SchemaElementDecl_t1940851905_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaElementDecl_t1940851905_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0, &SchemaElementDecl_t1940851905_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaElementDecl_t1940851905_0_0_0 = { 2, GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaElementDecl_t1940851905_0_0_0_Types };
static const Il2CppType* GenInst_SchemaElementDecl_t1940851905_0_0_0_Types[] = { &SchemaElementDecl_t1940851905_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaElementDecl_t1940851905_0_0_0 = { 1, GenInst_SchemaElementDecl_t1940851905_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t687937534_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t687937534_0_0_0_Types[] = { &KeyValuePair_2_t687937534_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t687937534_0_0_0 = { 1, GenInst_KeyValuePair_2_t687937534_0_0_0_Types };
extern const Il2CppType SchemaNotation_t2083484095_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_SchemaNotation_t2083484095_0_0_0_Types[] = { &String_t_0_0_0, &SchemaNotation_t2083484095_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_SchemaNotation_t2083484095_0_0_0 = { 2, GenInst_String_t_0_0_0_SchemaNotation_t2083484095_0_0_0_Types };
extern const Il2CppType SchemaEntity_t980649128_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaEntity_t980649128_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0, &SchemaEntity_t980649128_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaEntity_t980649128_0_0_0 = { 2, GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaEntity_t980649128_0_0_0_Types };
static const Il2CppType* GenInst_SchemaNotation_t2083484095_0_0_0_Types[] = { &SchemaNotation_t2083484095_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaNotation_t2083484095_0_0_0 = { 1, GenInst_SchemaNotation_t2083484095_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1755608579_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1755608579_0_0_0_Types[] = { &KeyValuePair_2_t1755608579_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1755608579_0_0_0 = { 1, GenInst_KeyValuePair_2_t1755608579_0_0_0_Types };
static const Il2CppType* GenInst_SchemaEntity_t980649128_0_0_0_Types[] = { &SchemaEntity_t980649128_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaEntity_t980649128_0_0_0 = { 1, GenInst_SchemaEntity_t980649128_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4022702053_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4022702053_0_0_0_Types[] = { &KeyValuePair_2_t4022702053_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022702053_0_0_0 = { 1, GenInst_KeyValuePair_2_t4022702053_0_0_0_Types };
extern const Il2CppType VirtualAttribute_t2854289803_0_0_0;
static const Il2CppType* GenInst_VirtualAttribute_t2854289803_0_0_0_Types[] = { &VirtualAttribute_t2854289803_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualAttribute_t2854289803_0_0_0 = { 1, GenInst_VirtualAttribute_t2854289803_0_0_0_Types };
extern const Il2CppType Entry_t2583369454_0_0_0;
static const Il2CppType* GenInst_Entry_t2583369454_0_0_0_Types[] = { &Entry_t2583369454_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
extern const Il2CppType Asttree_t3451058494_0_0_0;
static const Il2CppType* GenInst_Asttree_t3451058494_0_0_0_Types[] = { &Asttree_t3451058494_0_0_0 };
extern const Il2CppGenericInst GenInst_Asttree_t3451058494_0_0_0 = { 1, GenInst_Asttree_t3451058494_0_0_0_Types };
extern const Il2CppType LocatedActiveAxis_t90453917_0_0_0;
static const Il2CppType* GenInst_LocatedActiveAxis_t90453917_0_0_0_Types[] = { &LocatedActiveAxis_t90453917_0_0_0 };
extern const Il2CppGenericInst GenInst_LocatedActiveAxis_t90453917_0_0_0 = { 1, GenInst_LocatedActiveAxis_t90453917_0_0_0_Types };
extern const Il2CppType ActiveAxis_t439376929_0_0_0;
static const Il2CppType* GenInst_ActiveAxis_t439376929_0_0_0_Types[] = { &ActiveAxis_t439376929_0_0_0 };
extern const Il2CppGenericInst GenInst_ActiveAxis_t439376929_0_0_0 = { 1, GenInst_ActiveAxis_t439376929_0_0_0_Types };
extern const Il2CppType XmlAtomicValue_t752869371_0_0_0;
static const Il2CppType* GenInst_XmlAtomicValue_t752869371_0_0_0_Types[] = { &XmlAtomicValue_t752869371_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAtomicValue_t752869371_0_0_0 = { 1, GenInst_XmlAtomicValue_t752869371_0_0_0_Types };
extern const Il2CppType XPathItem_t3130801258_0_0_0;
static const Il2CppType* GenInst_XPathItem_t3130801258_0_0_0_Types[] = { &XPathItem_t3130801258_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathItem_t3130801258_0_0_0 = { 1, GenInst_XPathItem_t3130801258_0_0_0_Types };
extern const Il2CppType TypedObject_t1797374135_0_0_0;
static const Il2CppType* GenInst_TypedObject_t1797374135_0_0_0_Types[] = { &TypedObject_t1797374135_0_0_0 };
extern const Il2CppGenericInst GenInst_TypedObject_t1797374135_0_0_0 = { 1, GenInst_TypedObject_t1797374135_0_0_0_Types };
extern const Il2CppType BitSet_t1062448123_0_0_0;
static const Il2CppType* GenInst_BitSet_t1062448123_0_0_0_Types[] = { &BitSet_t1062448123_0_0_0 };
extern const Il2CppGenericInst GenInst_BitSet_t1062448123_0_0_0 = { 1, GenInst_BitSet_t1062448123_0_0_0_Types };
extern const Il2CppType InteriorNode_t2716368958_0_0_0;
static const Il2CppType* GenInst_InteriorNode_t2716368958_0_0_0_Types[] = { &InteriorNode_t2716368958_0_0_0 };
extern const Il2CppGenericInst GenInst_InteriorNode_t2716368958_0_0_0 = { 1, GenInst_InteriorNode_t2716368958_0_0_0_Types };
extern const Il2CppType SequenceConstructPosContext_t3853454650_0_0_0;
static const Il2CppType* GenInst_SequenceConstructPosContext_t3853454650_0_0_0_Types[] = { &SequenceConstructPosContext_t3853454650_0_0_0 };
extern const Il2CppGenericInst GenInst_SequenceConstructPosContext_t3853454650_0_0_0 = { 1, GenInst_SequenceConstructPosContext_t3853454650_0_0_0_Types };
extern const Il2CppType RangePositionInfo_t2780802922_0_0_0;
static const Il2CppType* GenInst_RangePositionInfo_t2780802922_0_0_0_Types[] = { &RangePositionInfo_t2780802922_0_0_0 };
extern const Il2CppGenericInst GenInst_RangePositionInfo_t2780802922_0_0_0 = { 1, GenInst_RangePositionInfo_t2780802922_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t248156492_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types[] = { &XmlSchemaSimpleType_t248156492_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t248156492_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t1795078578_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t1795078578_0_0_0_Types[] = { &XmlSchemaType_t1795078578_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t1795078578_0_0_0 = { 1, GenInst_XmlSchemaType_t1795078578_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t2082486936_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types[] = { &XmlSchemaAnnotated_t2082486936_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2082486936_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t2050913741_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t2050913741_0_0_0_Types[] = { &XmlSchemaObject_t2050913741_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t2050913741_0_0_0 = { 1, GenInst_XmlSchemaObject_t2050913741_0_0_0_Types };
extern const Il2CppType DatatypeImplementation_t1152094268_0_0_0;
static const Il2CppType* GenInst_DatatypeImplementation_t1152094268_0_0_0_Types[] = { &DatatypeImplementation_t1152094268_0_0_0 };
extern const Il2CppGenericInst GenInst_DatatypeImplementation_t1152094268_0_0_0 = { 1, GenInst_DatatypeImplementation_t1152094268_0_0_0_Types };
extern const Il2CppType XmlSchemaDatatype_t1195946242_0_0_0;
static const Il2CppType* GenInst_XmlSchemaDatatype_t1195946242_0_0_0_Types[] = { &XmlSchemaDatatype_t1195946242_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaDatatype_t1195946242_0_0_0 = { 1, GenInst_XmlSchemaDatatype_t1195946242_0_0_0_Types };
extern const Il2CppType SchemaDatatypeMap_t2661667341_0_0_0;
static const Il2CppType* GenInst_SchemaDatatypeMap_t2661667341_0_0_0_Types[] = { &SchemaDatatypeMap_t2661667341_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaDatatypeMap_t2661667341_0_0_0 = { 1, GenInst_SchemaDatatypeMap_t2661667341_0_0_0_Types };
extern const Il2CppType Uri_t19570940_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Types[] = { &Uri_t19570940_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0 = { 1, GenInst_Uri_t19570940_0_0_0_Types };
extern const Il2CppType UndeclaredNotation_t2066394897_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_UndeclaredNotation_t2066394897_0_0_0_Types[] = { &String_t_0_0_0, &UndeclaredNotation_t2066394897_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UndeclaredNotation_t2066394897_0_0_0 = { 2, GenInst_String_t_0_0_0_UndeclaredNotation_t2066394897_0_0_0_Types };
static const Il2CppType* GenInst_UndeclaredNotation_t2066394897_0_0_0_Types[] = { &UndeclaredNotation_t2066394897_0_0_0 };
extern const Il2CppGenericInst GenInst_UndeclaredNotation_t2066394897_0_0_0 = { 1, GenInst_UndeclaredNotation_t2066394897_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1738519381_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1738519381_0_0_0_Types[] = { &KeyValuePair_2_t1738519381_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1738519381_0_0_0 = { 1, GenInst_KeyValuePair_2_t1738519381_0_0_0_Types };
extern const Il2CppType ParseElementOnlyContent_LocalFrame_t225387055_0_0_0;
static const Il2CppType* GenInst_ParseElementOnlyContent_LocalFrame_t225387055_0_0_0_Types[] = { &ParseElementOnlyContent_LocalFrame_t225387055_0_0_0 };
extern const Il2CppGenericInst GenInst_ParseElementOnlyContent_LocalFrame_t225387055_0_0_0 = { 1, GenInst_ParseElementOnlyContent_LocalFrame_t225387055_0_0_0_Types };
extern const Il2CppType Map_t2552390023_0_0_0;
static const Il2CppType* GenInst_Map_t2552390023_0_0_0_Types[] = { &Map_t2552390023_0_0_0 };
extern const Il2CppGenericInst GenInst_Map_t2552390023_0_0_0 = { 1, GenInst_Map_t2552390023_0_0_0_Types };
extern const Il2CppType XmlNode_t616554813_0_0_0;
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { &IXPathNavigable_t845515791_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
extern const Il2CppType Entry_t1114109354_0_0_0;
static const Il2CppType* GenInst_Entry_t1114109354_0_0_0_Types[] = { &Entry_t1114109354_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1114109354_0_0_0 = { 1, GenInst_Entry_t1114109354_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlQualifiedName_t1944712516_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0, &XmlQualifiedName_t1944712516_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlQualifiedName_t1944712516_0_0_0 = { 2, GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlQualifiedName_t1944712516_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t691798145_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t691798145_0_0_0_Types[] = { &KeyValuePair_2_t691798145_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t691798145_0_0_0 = { 1, GenInst_KeyValuePair_2_t691798145_0_0_0_Types };
extern const Il2CppType CompiledIdentityConstraint_t964629540_0_0_0;
static const Il2CppType* GenInst_CompiledIdentityConstraint_t964629540_0_0_0_Types[] = { &CompiledIdentityConstraint_t964629540_0_0_0 };
extern const Il2CppGenericInst GenInst_CompiledIdentityConstraint_t964629540_0_0_0 = { 1, GenInst_CompiledIdentityConstraint_t964629540_0_0_0_Types };
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
extern const Il2CppType ConstraintStruct_t2462842120_0_0_0;
static const Il2CppType* GenInst_ConstraintStruct_t2462842120_0_0_0_Types[] = { &ConstraintStruct_t2462842120_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstraintStruct_t2462842120_0_0_0 = { 1, GenInst_ConstraintStruct_t2462842120_0_0_0_Types };
extern const Il2CppType XdrAttributeEntry_t2834798683_0_0_0;
static const Il2CppType* GenInst_XdrAttributeEntry_t2834798683_0_0_0_Types[] = { &XdrAttributeEntry_t2834798683_0_0_0 };
extern const Il2CppGenericInst GenInst_XdrAttributeEntry_t2834798683_0_0_0 = { 1, GenInst_XdrAttributeEntry_t2834798683_0_0_0_Types };
extern const Il2CppType XdrEntry_t2813485863_0_0_0;
static const Il2CppType* GenInst_XdrEntry_t2813485863_0_0_0_Types[] = { &XdrEntry_t2813485863_0_0_0 };
extern const Il2CppGenericInst GenInst_XdrEntry_t2813485863_0_0_0 = { 1, GenInst_XdrEntry_t2813485863_0_0_0_Types };
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlSchemaObject_t2050913741_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0, &XmlSchemaObject_t2050913741_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlSchemaObject_t2050913741_0_0_0 = { 2, GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlSchemaObject_t2050913741_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t797999370_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t797999370_0_0_0_Types[] = { &KeyValuePair_2_t797999370_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t797999370_0_0_0 = { 1, GenInst_KeyValuePair_2_t797999370_0_0_0_Types };
extern const Il2CppType XmlSchemaObjectEntry_t2510586510_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObjectEntry_t2510586510_0_0_0_Types[] = { &XmlSchemaObjectEntry_t2510586510_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObjectEntry_t2510586510_0_0_0 = { 1, GenInst_XmlSchemaObjectEntry_t2510586510_0_0_0_Types };
extern const Il2CppType XmlSchemaParticle_t3365045970_0_0_0;
static const Il2CppType* GenInst_XmlSchemaParticle_t3365045970_0_0_0_Types[] = { &XmlSchemaParticle_t3365045970_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaParticle_t3365045970_0_0_0 = { 1, GenInst_XmlSchemaParticle_t3365045970_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t4015859774_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types[] = { &XmlSchemaAttribute_t4015859774_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t4015859774_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types };
extern const Il2CppType XPathNavigator_t3981235968_0_0_0;
static const Il2CppType* GenInst_XPathNavigator_t3981235968_0_0_0_Types[] = { &XPathNavigator_t3981235968_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNavigator_t3981235968_0_0_0 = { 1, GenInst_XPathNavigator_t3981235968_0_0_0_Types };
extern const Il2CppType XmlValueConverter_t68179724_0_0_0;
static const Il2CppType* GenInst_XmlValueConverter_t68179724_0_0_0_Types[] = { &XmlValueConverter_t68179724_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlValueConverter_t68179724_0_0_0 = { 1, GenInst_XmlValueConverter_t68179724_0_0_0_Types };
extern const Il2CppType State_t2667800191_0_0_0;
static const Il2CppType* GenInst_State_t2667800191_0_0_0_Types[] = { &State_t2667800191_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t2667800191_0_0_0 = { 1, GenInst_State_t2667800191_0_0_0_Types };
extern const Il2CppType XsdAttributeEntry_t2279094079_0_0_0;
static const Il2CppType* GenInst_XsdAttributeEntry_t2279094079_0_0_0_Types[] = { &XsdAttributeEntry_t2279094079_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdAttributeEntry_t2279094079_0_0_0 = { 1, GenInst_XsdAttributeEntry_t2279094079_0_0_0_Types };
extern const Il2CppType XsdEntry_t2653064619_0_0_0;
static const Il2CppType* GenInst_XsdEntry_t2653064619_0_0_0_Types[] = { &XsdEntry_t2653064619_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdEntry_t2653064619_0_0_0 = { 1, GenInst_XsdEntry_t2653064619_0_0_0_Types };
extern const Il2CppType XmlTypeCode_t58293802_0_0_0;
static const Il2CppType* GenInst_XmlTypeCode_t58293802_0_0_0_Types[] = { &XmlTypeCode_t58293802_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeCode_t58293802_0_0_0 = { 1, GenInst_XmlTypeCode_t58293802_0_0_0_Types };
extern const Il2CppType Task_1_t2375465813_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t2375465813_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t2375465813_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t2375465813_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t2375465813_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Stream_t3255436806_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Stream_t3255436806_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Stream_t3255436806_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Stream_t3255436806_0_0_0_Types };
extern const Il2CppType WebResponse_t1895226051_0_0_0;
static const Il2CppType* GenInst_WebResponse_t1895226051_0_0_0_Types[] = { &WebResponse_t1895226051_0_0_0 };
extern const Il2CppGenericInst GenInst_WebResponse_t1895226051_0_0_0 = { 1, GenInst_WebResponse_t1895226051_0_0_0_Types };
extern const Il2CppType Task_1_t1015255058_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t1015255058_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t1015255058_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t1015255058_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t1015255058_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_WebResponse_t1895226051_0_0_0_Types[] = { &Il2CppObject_0_0_0, &WebResponse_t1895226051_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_WebResponse_t1895226051_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_WebResponse_t1895226051_0_0_0_Types };
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_WebResponse_t1895226051_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &WebResponse_t1895226051_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_WebResponse_t1895226051_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_WebResponse_t1895226051_0_0_0_Types };
extern const Il2CppType NamespaceDeclaration_t3577631811_0_0_0;
static const Il2CppType* GenInst_NamespaceDeclaration_t3577631811_0_0_0_Types[] = { &NamespaceDeclaration_t3577631811_0_0_0 };
extern const Il2CppGenericInst GenInst_NamespaceDeclaration_t3577631811_0_0_0 = { 1, GenInst_NamespaceDeclaration_t3577631811_0_0_0_Types };
extern const Il2CppType XPathResultType_t1521569578_0_0_0;
static const Il2CppType* GenInst_XPathResultType_t1521569578_0_0_0_Types[] = { &XPathResultType_t1521569578_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathResultType_t1521569578_0_0_0 = { 1, GenInst_XPathResultType_t1521569578_0_0_0_Types };
extern const Il2CppType Op_t1831748991_0_0_0;
static const Il2CppType* GenInst_Op_t1831748991_0_0_0_Types[] = { &Op_t1831748991_0_0_0 };
extern const Il2CppGenericInst GenInst_Op_t1831748991_0_0_0 = { 1, GenInst_Op_t1831748991_0_0_0_Types };
extern const Il2CppType XPathNodeRef_t2092605142_0_0_0;
static const Il2CppType* GenInst_XPathNodeRef_t2092605142_0_0_0_XPathNodeRef_t2092605142_0_0_0_Types[] = { &XPathNodeRef_t2092605142_0_0_0, &XPathNodeRef_t2092605142_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNodeRef_t2092605142_0_0_0_XPathNodeRef_t2092605142_0_0_0 = { 2, GenInst_XPathNodeRef_t2092605142_0_0_0_XPathNodeRef_t2092605142_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1483742713_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1483742713_0_0_0_Types[] = { &KeyValuePair_2_t1483742713_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1483742713_0_0_0 = { 1, GenInst_KeyValuePair_2_t1483742713_0_0_0_Types };
static const Il2CppType* GenInst_XPathNodeRef_t2092605142_0_0_0_Types[] = { &XPathNodeRef_t2092605142_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNodeRef_t2092605142_0_0_0 = { 1, GenInst_XPathNodeRef_t2092605142_0_0_0_Types };
extern const Il2CppType Entry_t1422871999_0_0_0;
static const Il2CppType* GenInst_Entry_t1422871999_0_0_0_Types[] = { &Entry_t1422871999_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1422871999_0_0_0 = { 1, GenInst_Entry_t1422871999_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType IWeakActionHandler_t2772394101_0_0_0;
static const Il2CppType* GenInst_IWeakActionHandler_t2772394101_0_0_0_Types[] = { &IWeakActionHandler_t2772394101_0_0_0 };
extern const Il2CppGenericInst GenInst_IWeakActionHandler_t2772394101_0_0_0 = { 1, GenInst_IWeakActionHandler_t2772394101_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType SemanticMeaning_t2306793223_0_0_0;
static const Il2CppType* GenInst_SemanticMeaning_t2306793223_0_0_0_Types[] = { &SemanticMeaning_t2306793223_0_0_0 };
extern const Il2CppGenericInst GenInst_SemanticMeaning_t2306793223_0_0_0 = { 1, GenInst_SemanticMeaning_t2306793223_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType AnimatorClipInfo_t3905751349_0_0_0;
static const Il2CppType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { &AnimatorClipInfo_t3905751349_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types };
extern const Il2CppType AnimatorControllerParameter_t1381019216_0_0_0;
static const Il2CppType* GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types[] = { &AnimatorControllerParameter_t1381019216_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1381019216_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType MessageTypeSubscribers_t2291506050_0_0_0;
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType MessageEventArgs_t301283622_0_0_0;
static const Il2CppType* GenInst_MessageEventArgs_t301283622_0_0_0_Types[] = { &MessageEventArgs_t301283622_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t301283622_0_0_0 = { 1, GenInst_MessageEventArgs_t301283622_0_0_0_Types };
extern const Il2CppType FieldWithTarget_t2256174789_0_0_0;
static const Il2CppType* GenInst_FieldWithTarget_t2256174789_0_0_0_Types[] = { &FieldWithTarget_t2256174789_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldWithTarget_t2256174789_0_0_0 = { 1, GenInst_FieldWithTarget_t2256174789_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegmentEnumerator_t4144823243_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegmentEnumerator_t4144823243_gp_0_0_0_0_Types[] = { &ArraySegmentEnumerator_t4144823243_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegmentEnumerator_t4144823243_gp_0_0_0_0 = { 1, GenInst_ArraySegmentEnumerator_t4144823243_gp_0_0_0_0_Types };
extern const Il2CppType ConcurrentDictionary_2_t165941581_gp_0_0_0_0;
static const Il2CppType* GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_Types[] = { &ConcurrentDictionary_2_t165941581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0 = { 1, GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_Types };
extern const Il2CppType ConcurrentDictionary_2_t165941581_gp_1_0_0_0;
static const Il2CppType* GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_ConcurrentDictionary_2_t165941581_gp_1_0_0_0_Types[] = { &ConcurrentDictionary_2_t165941581_gp_0_0_0_0, &ConcurrentDictionary_2_t165941581_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_ConcurrentDictionary_2_t165941581_gp_1_0_0_0 = { 2, GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_ConcurrentDictionary_2_t165941581_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2550395762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2550395762_0_0_0_Types[] = { &KeyValuePair_2_t2550395762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2550395762_0_0_0 = { 1, GenInst_KeyValuePair_2_t2550395762_0_0_0_Types };
static const Il2CppType* GenInst_ConcurrentDictionary_2_t165941581_gp_1_0_0_0_Types[] = { &ConcurrentDictionary_2_t165941581_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ConcurrentDictionary_2_t165941581_gp_1_0_0_0 = { 1, GenInst_ConcurrentDictionary_2_t165941581_gp_1_0_0_0_Types };
extern const Il2CppType Tables_t14801510_gp_0_0_0_0;
extern const Il2CppType Tables_t14801510_gp_1_0_0_0;
static const Il2CppType* GenInst_Tables_t14801510_gp_0_0_0_0_Tables_t14801510_gp_1_0_0_0_Types[] = { &Tables_t14801510_gp_0_0_0_0, &Tables_t14801510_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Tables_t14801510_gp_0_0_0_0_Tables_t14801510_gp_1_0_0_0 = { 2, GenInst_Tables_t14801510_gp_0_0_0_0_Tables_t14801510_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Tables_t14801510_gp_0_0_0_0_Types[] = { &Tables_t14801510_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tables_t14801510_gp_0_0_0_0 = { 1, GenInst_Tables_t14801510_gp_0_0_0_0_Types };
extern const Il2CppType Node_t639350773_gp_0_0_0_0;
extern const Il2CppType Node_t639350773_gp_1_0_0_0;
static const Il2CppType* GenInst_Node_t639350773_gp_0_0_0_0_Node_t639350773_gp_1_0_0_0_Types[] = { &Node_t639350773_gp_0_0_0_0, &Node_t639350773_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t639350773_gp_0_0_0_0_Node_t639350773_gp_1_0_0_0 = { 2, GenInst_Node_t639350773_gp_0_0_0_0_Node_t639350773_gp_1_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_t1325381877_gp_0_0_0_0;
extern const Il2CppType DictionaryEnumerator_t1325381877_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_t1325381877_gp_0_0_0_0_DictionaryEnumerator_t1325381877_gp_1_0_0_0_Types[] = { &DictionaryEnumerator_t1325381877_gp_0_0_0_0, &DictionaryEnumerator_t1325381877_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_t1325381877_gp_0_0_0_0_DictionaryEnumerator_t1325381877_gp_1_0_0_0 = { 2, GenInst_DictionaryEnumerator_t1325381877_gp_0_0_0_0_DictionaryEnumerator_t1325381877_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2515556306_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2515556306_0_0_0_Types[] = { &KeyValuePair_2_t2515556306_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2515556306_0_0_0 = { 1, GenInst_KeyValuePair_2_t2515556306_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1111953118_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1111953118_0_0_0_Types[] = { &KeyValuePair_2_t1111953118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1111953118_0_0_0 = { 1, GenInst_KeyValuePair_2_t1111953118_0_0_0_Types };
extern const Il2CppType Node_t2994634259_0_0_0;
static const Il2CppType* GenInst_Node_t2994634259_0_0_0_Types[] = { &Node_t2994634259_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2994634259_0_0_0 = { 1, GenInst_Node_t2994634259_0_0_0_Types };
extern const Il2CppType Node_t138109607_0_0_0;
static const Il2CppType* GenInst_Node_t138109607_0_0_0_Types[] = { &Node_t138109607_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t138109607_0_0_0 = { 1, GenInst_Node_t138109607_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t3627505585_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t3627505585_0_0_0_Types[] = { &Nullable_1_t3627505585_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t3627505585_0_0_0 = { 1, GenInst_Nullable_1_t3627505585_0_0_0_Types };
extern const Il2CppType NullableComparer_1_t421839525_gp_0_0_0_0;
static const Il2CppType* GenInst_NullableComparer_1_t421839525_gp_0_0_0_0_Types[] = { &NullableComparer_1_t421839525_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NullableComparer_1_t421839525_gp_0_0_0_0 = { 1, GenInst_NullableComparer_1_t421839525_gp_0_0_0_0_Types };
extern const Il2CppType ObjectComparer_1_t19367471_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectComparer_1_t19367471_gp_0_0_0_0_Types[] = { &ObjectComparer_1_t19367471_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectComparer_1_t19367471_gp_0_0_0_0 = { 1, GenInst_ObjectComparer_1_t19367471_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t758341705_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t758341705_0_0_0_Types[] = { &Nullable_1_t758341705_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t758341705_0_0_0 = { 1, GenInst_Nullable_1_t758341705_0_0_0_Types };
extern const Il2CppType NullableEqualityComparer_1_t1847642941_gp_0_0_0_0;
static const Il2CppType* GenInst_NullableEqualityComparer_1_t1847642941_gp_0_0_0_0_Types[] = { &NullableEqualityComparer_1_t1847642941_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NullableEqualityComparer_1_t1847642941_gp_0_0_0_0 = { 1, GenInst_NullableEqualityComparer_1_t1847642941_gp_0_0_0_0_Types };
extern const Il2CppType ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0_Types[] = { &ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0 = { 1, GenInst_ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0_Types };
extern const Il2CppType EnumEqualityComparer_1_t343233057_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumEqualityComparer_1_t343233057_gp_0_0_0_0_Types[] = { &EnumEqualityComparer_1_t343233057_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumEqualityComparer_1_t343233057_gp_0_0_0_0 = { 1, GenInst_EnumEqualityComparer_1_t343233057_gp_0_0_0_0_Types };
extern const Il2CppType SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0;
static const Il2CppType* GenInst_SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0_Types[] = { &SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0 = { 1, GenInst_SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0_Types };
extern const Il2CppType ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0;
static const Il2CppType* GenInst_ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0_Types[] = { &ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0 = { 1, GenInst_ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0_Types };
extern const Il2CppType LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0;
static const Il2CppType* GenInst_LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0_Types[] = { &LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0 = { 1, GenInst_LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType IReadOnlyCollection_1_t4269750562_gp_0_0_0_0;
static const Il2CppType* GenInst_IReadOnlyCollection_1_t4269750562_gp_0_0_0_0_Types[] = { &IReadOnlyCollection_1_t4269750562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyCollection_1_t4269750562_gp_0_0_0_0 = { 1, GenInst_IReadOnlyCollection_1_t4269750562_gp_0_0_0_0_Types };
extern const Il2CppType IReadOnlyDictionary_2_t985373069_gp_0_0_0_0;
static const Il2CppType* GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_Types[] = { &IReadOnlyDictionary_2_t985373069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0 = { 1, GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_Types };
extern const Il2CppType IReadOnlyDictionary_2_t985373069_gp_1_0_0_0;
static const Il2CppType* GenInst_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0_Types[] = { &IReadOnlyDictionary_2_t985373069_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0 = { 1, GenInst_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t66423410_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t66423410_0_0_0_Types[] = { &KeyValuePair_2_t66423410_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t66423410_0_0_0 = { 1, GenInst_KeyValuePair_2_t66423410_0_0_0_Types };
static const Il2CppType* GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0_Types[] = { &IReadOnlyDictionary_2_t985373069_gp_0_0_0_0, &IReadOnlyDictionary_2_t985373069_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0 = { 2, GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0_Types };
extern const Il2CppType IReadOnlyList_1_t898315966_gp_0_0_0_0;
static const Il2CppType* GenInst_IReadOnlyList_1_t898315966_gp_0_0_0_0_Types[] = { &IReadOnlyList_1_t898315966_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IReadOnlyList_1_t898315966_gp_0_0_0_0 = { 1, GenInst_IReadOnlyList_1_t898315966_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType Contract_ForAll_m1974577506_gp_0_0_0_0;
static const Il2CppType* GenInst_Contract_ForAll_m1974577506_gp_0_0_0_0_Types[] = { &Contract_ForAll_m1974577506_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Contract_ForAll_m1974577506_gp_0_0_0_0 = { 1, GenInst_Contract_ForAll_m1974577506_gp_0_0_0_0_Types };
extern const Il2CppType ListBuilder_1_t3185767060_gp_0_0_0_0;
static const Il2CppType* GenInst_ListBuilder_1_t3185767060_gp_0_0_0_0_Types[] = { &ListBuilder_1_t3185767060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListBuilder_1_t3185767060_gp_0_0_0_0 = { 1, GenInst_ListBuilder_1_t3185767060_gp_0_0_0_0_Types };
extern const Il2CppType AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0;
static const Il2CppType* GenInst_AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0_Types[] = { &AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0 = { 1, GenInst_AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t3672445870_0_0_0;
static const Il2CppType* GenInst_Task_1_t3672445870_0_0_0_Types[] = { &Task_1_t3672445870_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3672445870_0_0_0 = { 1, GenInst_Task_1_t3672445870_0_0_0_Types };
extern const Il2CppType AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0;
static const Il2CppType* GenInst_AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0_Types[] = { &AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0 = { 1, GenInst_AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0_Types };
extern const Il2CppType TaskAwaiter_1_t3746736679_gp_0_0_0_0;
static const Il2CppType* GenInst_TaskAwaiter_1_t3746736679_gp_0_0_0_0_Types[] = { &TaskAwaiter_1_t3746736679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskAwaiter_1_t3746736679_gp_0_0_0_0 = { 1, GenInst_TaskAwaiter_1_t3746736679_gp_0_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0_Types[] = { &ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0 = { 1, GenInst_ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0 = { 1, GenInst_ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0_Types };
extern const Il2CppType SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0;
static const Il2CppType* GenInst_SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0_Types[] = { &SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0 = { 1, GenInst_SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0_Types };
extern const Il2CppType SparselyPopulatedArrayFragment_1_t719568888_0_0_0;
static const Il2CppType* GenInst_SparselyPopulatedArrayFragment_1_t719568888_0_0_0_Types[] = { &SparselyPopulatedArrayFragment_1_t719568888_0_0_0 };
extern const Il2CppGenericInst GenInst_SparselyPopulatedArrayFragment_1_t719568888_0_0_0 = { 1, GenInst_SparselyPopulatedArrayFragment_1_t719568888_0_0_0_Types };
extern const Il2CppType SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0;
static const Il2CppType* GenInst_SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0_Types[] = { &SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0 = { 1, GenInst_SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0_Types };
extern const Il2CppType SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0;
static const Il2CppType* GenInst_SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0_Types[] = { &SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0 = { 1, GenInst_SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0_Types };
extern const Il2CppType LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0;
static const Il2CppType* GenInst_LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0_Types[] = { &LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0 = { 1, GenInst_LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0_Types };
extern const Il2CppType LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0;
static const Il2CppType* GenInst_LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0_Types[] = { &LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0 = { 1, GenInst_LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t3208082394_gp_0_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Task_1_t3208082394_gp_0_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Task_1_t3208082394_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Task_1_t3208082394_gp_0_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Task_1_t3208082394_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t3208082394_gp_0_0_0_0_Types[] = { &Task_1_t3208082394_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3208082394_gp_0_0_0_0 = { 1, GenInst_Task_1_t3208082394_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t2975743850_0_0_0;
static const Il2CppType* GenInst_Task_1_t963265114_0_0_0_Task_1_t2975743850_0_0_0_Types[] = { &Task_1_t963265114_0_0_0, &Task_1_t2975743850_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t963265114_0_0_0_Task_1_t2975743850_0_0_0 = { 2, GenInst_Task_1_t963265114_0_0_0_Task_1_t2975743850_0_0_0_Types };
extern const Il2CppType TaskFactory_1_t4074777222_gp_0_0_0_0;
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_TaskFactory_1_t4074777222_gp_0_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &TaskFactory_1_t4074777222_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_TaskFactory_1_t4074777222_gp_0_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_TaskFactory_1_t4074777222_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_TaskFactory_1_t4074777222_gp_0_0_0_0_Types[] = { &TaskFactory_1_t4074777222_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskFactory_1_t4074777222_gp_0_0_0_0 = { 1, GenInst_TaskFactory_1_t4074777222_gp_0_0_0_0_Types };
extern const Il2CppType U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0;
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0, &U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0 = { 2, GenInst_IAsyncResult_t1999651008_0_0_0_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0_Types[] = { &U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0 = { 1, GenInst_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0_Types };
extern const Il2CppType U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0_Types[] = { &U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0 = { 1, GenInst_U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0_Types };
extern const Il2CppType Task_FromResult_m725378983_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_FromResult_m725378983_gp_0_0_0_0_Types[] = { &Task_FromResult_m725378983_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_FromResult_m725378983_gp_0_0_0_0 = { 1, GenInst_Task_FromResult_m725378983_gp_0_0_0_0_Types };
extern const Il2CppType Task_FromCancellation_m977792743_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_FromCancellation_m977792743_gp_0_0_0_0_Types[] = { &Task_FromCancellation_m977792743_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_FromCancellation_m977792743_gp_0_0_0_0 = { 1, GenInst_Task_FromCancellation_m977792743_gp_0_0_0_0_Types };
extern const Il2CppType Task_Run_m3210616763_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_Run_m3210616763_gp_0_0_0_0_Types[] = { &Task_Run_m3210616763_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_Run_m3210616763_gp_0_0_0_0 = { 1, GenInst_Task_Run_m3210616763_gp_0_0_0_0_Types };
extern const Il2CppType AsyncLocalValueChangedArgs_1_t3831819380_0_0_0;
static const Il2CppType* GenInst_AsyncLocalValueChangedArgs_1_t3831819380_0_0_0_Types[] = { &AsyncLocalValueChangedArgs_1_t3831819380_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncLocalValueChangedArgs_1_t3831819380_0_0_0 = { 1, GenInst_AsyncLocalValueChangedArgs_1_t3831819380_0_0_0_Types };
extern const Il2CppType AsyncLocal_1_t398511596_gp_0_0_0_0;
static const Il2CppType* GenInst_AsyncLocal_1_t398511596_gp_0_0_0_0_Types[] = { &AsyncLocal_1_t398511596_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncLocal_1_t398511596_gp_0_0_0_0 = { 1, GenInst_AsyncLocal_1_t398511596_gp_0_0_0_0_Types };
extern const Il2CppType AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0;
static const Il2CppType* GenInst_AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0_Types[] = { &AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0 = { 1, GenInst_AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0_Types };
extern const Il2CppType SparseArray_1_t2081600286_gp_0_0_0_0;
static const Il2CppType* GenInst_SparseArray_1_t2081600286_gp_0_0_0_0_Types[] = { &SparseArray_1_t2081600286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SparseArray_1_t2081600286_gp_0_0_0_0 = { 1, GenInst_SparseArray_1_t2081600286_gp_0_0_0_0_Types };
extern const Il2CppType Tuple_Create_m3881096823_gp_0_0_0_0;
extern const Il2CppType Tuple_Create_m3881096823_gp_1_0_0_0;
static const Il2CppType* GenInst_Tuple_Create_m3881096823_gp_0_0_0_0_Tuple_Create_m3881096823_gp_1_0_0_0_Types[] = { &Tuple_Create_m3881096823_gp_0_0_0_0, &Tuple_Create_m3881096823_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_Create_m3881096823_gp_0_0_0_0_Tuple_Create_m3881096823_gp_1_0_0_0 = { 2, GenInst_Tuple_Create_m3881096823_gp_0_0_0_0_Tuple_Create_m3881096823_gp_1_0_0_0_Types };
extern const Il2CppType Tuple_2_t1951933832_gp_0_0_0_0;
extern const Il2CppType Tuple_2_t1951933832_gp_1_0_0_0;
static const Il2CppType* GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types[] = { &Tuple_2_t1951933832_gp_0_0_0_0, &Tuple_2_t1951933832_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0 = { 2, GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types };
extern const Il2CppType Tuple_3_t3518017773_gp_0_0_0_0;
extern const Il2CppType Tuple_3_t3518017773_gp_1_0_0_0;
extern const Il2CppType Tuple_3_t3518017773_gp_2_0_0_0;
static const Il2CppType* GenInst_Tuple_3_t3518017773_gp_0_0_0_0_Tuple_3_t3518017773_gp_1_0_0_0_Tuple_3_t3518017773_gp_2_0_0_0_Types[] = { &Tuple_3_t3518017773_gp_0_0_0_0, &Tuple_3_t3518017773_gp_1_0_0_0, &Tuple_3_t3518017773_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_3_t3518017773_gp_0_0_0_0_Tuple_3_t3518017773_gp_1_0_0_0_Tuple_3_t3518017773_gp_2_0_0_0 = { 3, GenInst_Tuple_3_t3518017773_gp_0_0_0_0_Tuple_3_t3518017773_gp_1_0_0_0_Tuple_3_t3518017773_gp_2_0_0_0_Types };
extern const Il2CppType Tuple_4_t1145364778_gp_0_0_0_0;
extern const Il2CppType Tuple_4_t1145364778_gp_1_0_0_0;
extern const Il2CppType Tuple_4_t1145364778_gp_2_0_0_0;
extern const Il2CppType Tuple_4_t1145364778_gp_3_0_0_0;
static const Il2CppType* GenInst_Tuple_4_t1145364778_gp_0_0_0_0_Tuple_4_t1145364778_gp_1_0_0_0_Tuple_4_t1145364778_gp_2_0_0_0_Tuple_4_t1145364778_gp_3_0_0_0_Types[] = { &Tuple_4_t1145364778_gp_0_0_0_0, &Tuple_4_t1145364778_gp_1_0_0_0, &Tuple_4_t1145364778_gp_2_0_0_0, &Tuple_4_t1145364778_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_4_t1145364778_gp_0_0_0_0_Tuple_4_t1145364778_gp_1_0_0_0_Tuple_4_t1145364778_gp_2_0_0_0_Tuple_4_t1145364778_gp_3_0_0_0 = { 4, GenInst_Tuple_4_t1145364778_gp_0_0_0_0_Tuple_4_t1145364778_gp_1_0_0_0_Tuple_4_t1145364778_gp_2_0_0_0_Tuple_4_t1145364778_gp_3_0_0_0_Types };
extern const Il2CppType JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0_Types };
extern const Il2CppType JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0;
static const Il2CppType* GenInst_JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0;
static const Il2CppType* GenInst_JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0_Int64_t909078037_0_0_0_Types[] = { &JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0_Int64_t909078037_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType ConditionalWeakTable_2_t2129616300_gp_0_0_0_0;
extern const Il2CppType ConditionalWeakTable_2_t2129616300_gp_1_0_0_0;
static const Il2CppType* GenInst_ConditionalWeakTable_2_t2129616300_gp_0_0_0_0_ConditionalWeakTable_2_t2129616300_gp_1_0_0_0_Types[] = { &ConditionalWeakTable_2_t2129616300_gp_0_0_0_0, &ConditionalWeakTable_2_t2129616300_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ConditionalWeakTable_2_t2129616300_gp_0_0_0_0_ConditionalWeakTable_2_t2129616300_gp_1_0_0_0 = { 2, GenInst_ConditionalWeakTable_2_t2129616300_gp_0_0_0_0_ConditionalWeakTable_2_t2129616300_gp_1_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_SortImpl_m1706208635_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Types[] = { &Array_SortImpl_m1706208635_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0 = { 1, GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Types };
extern const Il2CppType Array_SortImpl_m1706208635_gp_1_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &DateTime_t693205669_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Decimal_t724701077_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Decimal_t724701077_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Double_t4078015681_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Double_t4078015681_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Int16_t4041245914_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Int16_t4041245914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &SByte_t454417549_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_SByte_t454417549_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &UInt16_t986882611_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_UInt16_t986882611_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_UInt32_t2149682021_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types[] = { &Array_SortImpl_m1706208635_gp_0_0_0_0, &Array_SortImpl_m1706208635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0 = { 2, GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0_Types };
extern const Il2CppType Array_SortImpl_m3985765524_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_SortImpl_m3985765524_gp_0_0_0_0_Types[] = { &Array_SortImpl_m3985765524_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_SortImpl_m3985765524_gp_0_0_0_0 = { 1, GenInst_Array_SortImpl_m3985765524_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_SortImpl_m2766726474_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_SortImpl_m2766726474_gp_0_0_0_0_Types[] = { &Array_SortImpl_m2766726474_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_SortImpl_m2766726474_gp_0_0_0_0 = { 1, GenInst_Array_SortImpl_m2766726474_gp_0_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m3119918310_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Types[] = { &Array_QSortArrange_m3119918310_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0 = { 1, GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m3119918310_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Array_QSortArrange_m3119918310_gp_1_0_0_0_Types[] = { &Array_QSortArrange_m3119918310_gp_0_0_0_0, &Array_QSortArrange_m3119918310_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Array_QSortArrange_m3119918310_gp_1_0_0_0 = { 2, GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Array_QSortArrange_m3119918310_gp_1_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m3147515399_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m3147515399_gp_0_0_0_0_Types[] = { &Array_QSortArrange_m3147515399_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m3147515399_gp_0_0_0_0 = { 1, GenInst_Array_QSortArrange_m3147515399_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m931699978_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m931699978_gp_0_0_0_0_Types[] = { &Array_qsort_m931699978_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m931699978_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m931699978_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m931699978_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m931699978_gp_0_0_0_0_Array_qsort_m931699978_gp_1_0_0_0_Types[] = { &Array_qsort_m931699978_gp_0_0_0_0, &Array_qsort_m931699978_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m931699978_gp_0_0_0_0_Array_qsort_m931699978_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m931699978_gp_0_0_0_0_Array_qsort_m931699978_gp_1_0_0_0_Types };
extern const Il2CppType Array_qsort_m3010626211_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3010626211_gp_0_0_0_0_Types[] = { &Array_qsort_m3010626211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3010626211_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3010626211_gp_0_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m3669011159_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Types[] = { &Array_QSortArrange_m3669011159_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0 = { 1, GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m3669011159_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Array_QSortArrange_m3669011159_gp_1_0_0_0_Types[] = { &Array_QSortArrange_m3669011159_gp_0_0_0_0, &Array_QSortArrange_m3669011159_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Array_QSortArrange_m3669011159_gp_1_0_0_0 = { 2, GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Array_QSortArrange_m3669011159_gp_1_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m2419137335_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m2419137335_gp_0_0_0_0_Types[] = { &Array_QSortArrange_m2419137335_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m2419137335_gp_0_0_0_0 = { 1, GenInst_Array_QSortArrange_m2419137335_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_qsort_m3180236531_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3180236531_gp_0_0_0_0_Types[] = { &Array_qsort_m3180236531_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3180236531_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3180236531_gp_0_0_0_0_Types };
extern const Il2CppType Array_QSortArrange_m1211890906_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_QSortArrange_m1211890906_gp_0_0_0_0_Types[] = { &Array_QSortArrange_m1211890906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_QSortArrange_m1211890906_gp_0_0_0_0 = { 1, GenInst_Array_QSortArrange_m1211890906_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_CheckComparerAvailable_m866539301_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_CheckComparerAvailable_m866539301_gp_0_0_0_0_Types[] = { &Array_CheckComparerAvailable_m866539301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_CheckComparerAvailable_m866539301_gp_0_0_0_0 = { 1, GenInst_Array_CheckComparerAvailable_m866539301_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_GetLastIndex_m3612547965_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_GetLastIndex_m3612547965_gp_0_0_0_0_Types[] = { &Array_GetLastIndex_m3612547965_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_GetLastIndex_m3612547965_gp_0_0_0_0 = { 1, GenInst_Array_GetLastIndex_m3612547965_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_GetIndex_m101930135_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_GetIndex_m101930135_gp_0_0_0_0_Types[] = { &Array_GetIndex_m101930135_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_GetIndex_m101930135_gp_0_0_0_0 = { 1, GenInst_Array_GetIndex_m101930135_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Empty_m4207413324_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Empty_m4207413324_gp_0_0_0_0_Types[] = { &Array_Empty_m4207413324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Empty_m4207413324_gp_0_0_0_0 = { 1, GenInst_Array_Empty_m4207413324_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType FunctorComparer_1_t657880694_gp_0_0_0_0;
static const Il2CppType* GenInst_FunctorComparer_1_t657880694_gp_0_0_0_0_Types[] = { &FunctorComparer_1_t657880694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_FunctorComparer_1_t657880694_gp_0_0_0_0 = { 1, GenInst_FunctorComparer_1_t657880694_gp_0_0_0_0_Types };
extern const Il2CppType EmptyArray_1_t1448450545_gp_0_0_0_0;
static const Il2CppType* GenInst_EmptyArray_1_t1448450545_gp_0_0_0_0_Types[] = { &EmptyArray_1_t1448450545_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EmptyArray_1_t1448450545_gp_0_0_0_0 = { 1, GenInst_EmptyArray_1_t1448450545_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType XmlListConverter_ToArray_m3797099009_gp_0_0_0_0;
static const Il2CppType* GenInst_XmlListConverter_ToArray_m3797099009_gp_0_0_0_0_Types[] = { &XmlListConverter_ToArray_m3797099009_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlListConverter_ToArray_m3797099009_gp_0_0_0_0 = { 1, GenInst_XmlListConverter_ToArray_m3797099009_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Types[] = { &Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0 = { 1, GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Iterator_1_t2284292427_gp_0_0_0_0;
static const Il2CppType* GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Types[] = { &Iterator_1_t2284292427_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Iterator_1_t2284292427_gp_0_0_0_0 = { 1, GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Iterator_1_t2284292427_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType WhereEnumerableIterator_1_t451024438_gp_0_0_0_0;
static const Il2CppType* GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Types[] = { &WhereEnumerableIterator_1_t451024438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0 = { 1, GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &WhereEnumerableIterator_1_t451024438_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType WhereArrayIterator_1_t2256832775_gp_0_0_0_0;
static const Il2CppType* GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Types[] = { &WhereArrayIterator_1_t2256832775_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0 = { 1, GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &WhereArrayIterator_1_t2256832775_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType WhereListIterator_1_t2012596740_gp_0_0_0_0;
static const Il2CppType* GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Types[] = { &WhereListIterator_1_t2012596740_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0 = { 1, GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &WhereListIterator_1_t2012596740_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Il2CppObject_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0_Types[] = { &DateTime_t693205669_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types[] = { &Decimal_t724701077_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Il2CppObject_0_0_0_Types[] = { &Double_t4078015681_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Double_t4078015681_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int16_t4041245914_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int16_t4041245914_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Il2CppObject_0_0_0_Types[] = { &SByte_t454417549_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SByte_t454417549_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt16_t986882611_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt32_t2149682021_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_String_t_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_String_t_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0;
static const Il2CppType* GenInst_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types[] = { &U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 = { 1, GenInst_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t2759266955_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t2759266955_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t2759266955_0_0_0, &U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t2759266955_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t2759266955_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t446638270_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t446638270_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t446638270_0_0_0, &U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t446638270_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t446638270_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types };
extern const Il2CppType RefreshEventHandler_t456069287_0_0_0;
static const Il2CppType* GenInst_RefreshEventHandler_t456069287_0_0_0_Types[] = { &RefreshEventHandler_t456069287_0_0_0 };
extern const Il2CppGenericInst GenInst_RefreshEventHandler_t456069287_0_0_0 = { 1, GenInst_RefreshEventHandler_t456069287_0_0_0_Types };
extern const Il2CppType Win32_FIXED_INFO_t1371335919_0_0_0;
static const Il2CppType* GenInst_Win32_FIXED_INFO_t1371335919_0_0_0_Types[] = { &Win32_FIXED_INFO_t1371335919_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32_FIXED_INFO_t1371335919_0_0_0 = { 1, GenInst_Win32_FIXED_INFO_t1371335919_0_0_0_Types };
extern const Il2CppType U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0;
static const Il2CppType* GenInst_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types[] = { &U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 = { 1, GenInst_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t2811256899_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t2811256899_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t2811256899_0_0_0, &U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t2811256899_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t2811256899_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types };
extern const Il2CppType U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0;
static const Il2CppType* GenInst_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types[] = { &U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 = { 1, GenInst_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t4171467654_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t4171467654_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t4171467654_0_0_0, &U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t4171467654_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t4171467654_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType PlayerConnection_t3517219175_0_0_0;
static const Il2CppType* GenInst_PlayerConnection_t3517219175_0_0_0_Types[] = { &PlayerConnection_t3517219175_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3517219175_0_0_0 = { 1, GenInst_PlayerConnection_t3517219175_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
static const Il2CppType* GenInst_RegexOptions_t2418259727_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &RegexOptions_t2418259727_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexOptions_t2418259727_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_RegexOptions_t2418259727_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ConfiguredTaskAwaiter_t3605480143_0_0_0;
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t3605480143_0_0_0, &U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0_Types };
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t3605480143_0_0_0, &U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0_Types };
static const Il2CppType* GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types[] = { &ConfiguredTaskAwaiter_t3605480143_0_0_0, &U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0 = { 2, GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[810] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Entry_t3655379380_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_Il2CppObject_0_0_0_SerializationInfo_t228987430_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Ephemeron_t1875076633_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_ExceptionDispatchInfo_t3341954301_0_0_0,
	&GenInst_AggregateException_t420812976_0_0_0,
	&GenInst_LocalDataStoreElement_t3980707294_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_LocalDataStore_t228818476_0_0_0,
	&GenInst_String_t_0_0_0_LocalDataStoreSlot_t486331200_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Entry_t4272951227_0_0_0,
	&GenInst_LocalDataStoreSlot_t486331200_0_0_0,
	&GenInst_KeyValuePair_2_t158455684_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_Node_t1921535786_0_0_0,
	&GenInst_bucket_t976591655_0_0_0,
	&GenInst_RuntimeType_t2836228502_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_TypeInfo_t3822613806_0_0_0,
	&GenInst_IReflectableType_t2431043262_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_IStructuralComparable_t881765570_0_0_0,
	&GenInst_IStructuralEquatable_t3751153838_0_0_0,
	&GenInst_IList_1_t2612818049_0_0_0,
	&GenInst_ICollection_1_t3023952753_0_0_0,
	&GenInst_IEnumerable_1_t2364004493_0_0_0,
	&GenInst_IReadOnlyList_1_t3770255835_0_0_0,
	&GenInst_IReadOnlyCollection_1_t1659542175_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_IObjectReference_t2936130011_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_StackTrace_t2500644597_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_String_t_0_0_0_SimpleCollator_t4081201584_0_0_0,
	&GenInst_SimpleCollator_t4081201584_0_0_0,
	&GenInst_KeyValuePair_2_t3753326068_0_0_0,
	&GenInst_TokenHashValue_t393941324_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_DSU5BU5D_t3090528720_0_0_0,
	&GenInst_IList_1_t1310137942_0_0_0,
	&GenInst_DS_t769197341_0_0_0,
	&GenInst_ICollection_1_t1721272646_0_0_0,
	&GenInst_IEnumerable_1_t1061324386_0_0_0,
	&GenInst_IReadOnlyList_1_t2467575728_0_0_0,
	&GenInst_IReadOnlyCollection_1_t356862068_0_0_0,
	&GenInst_EraInfo_t1792567760_0_0_0,
	&GenInst_HebrewValue_t2452471125_0_0_0,
	&GenInst_HSU5BU5D_t387695875_0_0_0,
	&GenInst_IList_1_t2280721119_0_0_0,
	&GenInst_HS_t1739780518_0_0_0,
	&GenInst_ICollection_1_t2691855823_0_0_0,
	&GenInst_IEnumerable_1_t2031907563_0_0_0,
	&GenInst_IReadOnlyList_1_t3438158905_0_0_0,
	&GenInst_IReadOnlyCollection_1_t1327445245_0_0_0,
	&GenInst_TimeSpanToken_t2031659367_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0,
	&GenInst_AsyncCallback_t163412349_0_0_0_Il2CppObject_0_0_0_IAsyncResult_t1999651008_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t1191906455_0_0_0,
	&GenInst_Task_t1843236107_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t963265114_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Task_t1843236107_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t1809478302_0_0_0,
	&GenInst_SemaphoreSlim_t461808439_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0_ReadWriteTask_t2745753060_0_0_0,
	&GenInst_ManualResetEvent_t926074657_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_TextReader_t1561828458_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_TextWriter_t4027217640_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_TextWriter_t4027217640_0_0_0_String_t_0_0_0,
	&GenInst_TextWriter_t4027217640_0_0_0_CharU5BU5D_t1328083999_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_String_t_0_0_0_ResourceLocator_t2156390884_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ResourceLocator_t2156390884_0_0_0,
	&GenInst_KeyValuePair_2_t3800763530_0_0_0,
	&GenInst_ResourceLocator_t2156390884_0_0_0,
	&GenInst_Entry_t3739892816_0_0_0,
	&GenInst_KeyValuePair_2_t1828515368_0_0_0,
	&GenInst_Guid_t_0_0_0_Type_t_0_0_0,
	&GenInst_Guid_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4275477795_0_0_0,
	&GenInst_Entry_t4214607081_0_0_0,
	&GenInst_KeyValuePair_2_t2889831726_0_0_0,
	&GenInst_RuntimeMethodInfo_t3887273209_0_0_0,
	&GenInst_RuntimeConstructorInfo_t311714390_0_0_0,
	&GenInst_RuntimePropertyInfo_t3497259377_0_0_0,
	&GenInst_RuntimeEventInfo_t3861156374_0_0_0,
	&GenInst_RuntimeFieldInfo_t1687134186_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t2945603725_0_0_0,
	&GenInst_Task_1_t1191906455_0_0_0,
	&GenInst_IThreadPoolWorkItem_t2893622999_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_BinaryTypeEnum_t2877339122_0_0_0,
	&GenInst_TypeCode_t2536926201_0_0_0,
	&GenInst_InternalPrimitiveTypeE_t3020811655_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_Type_t_0_0_0_TypeInformation_t3804990478_0_0_0,
	&GenInst_TypeInformation_t3804990478_0_0_0,
	&GenInst_KeyValuePair_2_t3499693597_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_WriteObjectInfo_t2489189536_0_0_0,
	&GenInst_AssemblyName_t894705941_0_0_0_Assembly_t4268412390_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Type_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_MemberHolder_t3720434074_0_0_0_MemberInfoU5BU5D_t4238939941_0_0_0,
	&GenInst_MemberHolder_t3720434074_0_0_0,
	&GenInst_MemberInfoU5BU5D_t4238939941_0_0_0,
	&GenInst_KeyValuePair_2_t430017524_0_0_0,
	&GenInst_SerializationFieldInfo_t2472586292_0_0_0,
	&GenInst_ObjectHolder_t2992553423_0_0_0,
	&GenInst_FixupHolder_t2028025012_0_0_0,
	&GenInst_SafeSerializationEventArgs_t1088103422_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_HashAlgorithmName_t1682985260_0_0_0,
	&GenInst_RSAEncryptionPadding_t1083150860_0_0_0,
	&GenInst_HashAlgorithm_t2624936259_0_0_0,
	&GenInst_EncodingProvider_t2755886406_0_0_0,
	&GenInst_CancellationTokenRegistration_t1708859357_0_0_0,
	&GenInst_CancellationCallbackInfo_t1473383178_0_0_0,
	&GenInst_SparselyPopulatedArray_1_t4170979568_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Task_t1843236107_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Entry_t3688716734_0_0_0,
	&GenInst_KeyValuePair_2_t2903374260_0_0_0,
	&GenInst_ContingentProperties_t606988207_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Task_t1843236107_0_0_0_TaskContinuation_t1422769290_0_0_0,
	&GenInst_VoidTaskResult_t3325310798_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_VoidTaskResult_t3325310798_0_0_0,
	&GenInst_Il2CppObject_0_0_0_VoidTaskResult_t3325310798_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t2445339805_0_0_0,
	&GenInst_TaskScheduler_t3932792796_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UnobservedTaskExceptionEventArgs_t1087040814_0_0_0,
	&GenInst_AsyncLocalValueChangedArgs_1_t1180157334_0_0_0,
	&GenInst_IAsyncLocal_t3643786784_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IAsyncLocal_t3643786784_0_0_0,
	&GenInst_KeyValuePair_2_t761048256_0_0_0,
	&GenInst_CultureInfo_t3500843524_0_0_0,
	&GenInst_AsyncLocalValueChangedArgs_1_t1991551563_0_0_0,
	&GenInst_WorkStealingQueue_t2897576067_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_DirectoryInfo_t1934446453_0_0_0,
	&GenInst_FileSystemInfo_t2360991899_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_UriScheme_t683497865_0_0_0,
	&GenInst_FirstChanceExceptionEventArgs_t3799922078_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_CalendarData_t275297348_0_0_0,
	&GenInst_InternalCodePageDataItem_t1742109366_0_0_0,
	&GenInst_InternalEncodingDataItem_t82919681_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_String_t_0_0_0_Func_2_t768253681_0_0_0,
	&GenInst_StackTrace_t2500644597_0_0_0_String_t_0_0_0,
	&GenInst_Func_2_t768253681_0_0_0,
	&GenInst_KeyValuePair_2_t440378165_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CultureInfo_t3500843524_0_0_0,
	&GenInst_KeyValuePair_2_t266014381_0_0_0,
	&GenInst_String_t_0_0_0_CultureInfo_t3500843524_0_0_0,
	&GenInst_KeyValuePair_2_t3172968008_0_0_0,
	&GenInst_IFormatProvider_t2849799027_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_CustomAttributeBuilder_t2970303184_0_0_0,
	&GenInst__CustomAttributeBuilder_t3917123293_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_MonoWin32Resource_t2467306218_0_0_0,
	&GenInst_RefEmitPermissionSet_t2708608433_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_IList_1_t1844743827_0_0_0,
	&GenInst_ICollection_1_t2255878531_0_0_0,
	&GenInst_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_IReadOnlyList_1_t3002181613_0_0_0,
	&GenInst_IReadOnlyCollection_1_t891467953_0_0_0,
	&GenInst_IList_1_t643717440_0_0_0,
	&GenInst_ICollection_1_t1054852144_0_0_0,
	&GenInst_IEnumerable_1_t394903884_0_0_0,
	&GenInst_IReadOnlyList_1_t1801155226_0_0_0,
	&GenInst_IReadOnlyCollection_1_t3985408862_0_0_0,
	&GenInst_IList_1_t3952977575_0_0_0,
	&GenInst_ICollection_1_t69144983_0_0_0,
	&GenInst_IEnumerable_1_t3704164019_0_0_0,
	&GenInst_IReadOnlyList_1_t815448065_0_0_0,
	&GenInst_IReadOnlyCollection_1_t2999701701_0_0_0,
	&GenInst_IList_1_t289070565_0_0_0,
	&GenInst_ICollection_1_t700205269_0_0_0,
	&GenInst_IEnumerable_1_t40257009_0_0_0,
	&GenInst_IReadOnlyList_1_t1446508351_0_0_0,
	&GenInst_IReadOnlyCollection_1_t3630761987_0_0_0,
	&GenInst_IList_1_t1043143288_0_0_0,
	&GenInst_ICollection_1_t1454277992_0_0_0,
	&GenInst_IEnumerable_1_t794329732_0_0_0,
	&GenInst_IReadOnlyList_1_t2200581074_0_0_0,
	&GenInst_IReadOnlyCollection_1_t89867414_0_0_0,
	&GenInst_IList_1_t873662762_0_0_0,
	&GenInst_ICollection_1_t1284797466_0_0_0,
	&GenInst_IEnumerable_1_t624849206_0_0_0,
	&GenInst_IReadOnlyList_1_t2031100548_0_0_0,
	&GenInst_IReadOnlyCollection_1_t4215354184_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_ICollection_1_t3641524600_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_IReadOnlyList_1_t92860386_0_0_0,
	&GenInst_IReadOnlyCollection_1_t2277114022_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ILExceptionBlock_t2042475189_0_0_0,
	&GenInst_LocalBuilder_t2116499186_0_0_0,
	&GenInst__LocalBuilder_t61912499_0_0_0,
	&GenInst_LocalVariableInfo_t1749284021_0_0_0,
	&GenInst_ILExceptionInfo_t1490154598_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_Label_t4243202660_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_TypeName_t2073290883_0_0_0_TypeBuilder_t3308873219_0_0_0,
	&GenInst_TypeName_t2073290883_0_0_0,
	&GenInst_KeyValuePair_2_t73557541_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_EventBuilder_t2927243889_0_0_0,
	&GenInst__EventBuilder_t801715496_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_DecimalConstantAttribute_t569828555_0_0_0,
	&GenInst_DateTimeConstantAttribute_t4275890373_0_0_0,
	&GenInst_CustomConstantAttribute_t2797584351_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_KeyValuePair_2_t975927710_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2988326850_0_0_0,
	&GenInst_IBuiltInEvidence_t1114073477_0_0_0,
	&GenInst_EvidenceBase_t1783132120_0_0_0,
	&GenInst_CodeConnectAccess_t3638993531_0_0_0,
	&GenInst_Timer_t791717973_0_0_0,
	&GenInst_Type_t_0_0_0_AttributeUsageAttribute_t1057435127_0_0_0,
	&GenInst_AttributeUsageAttribute_t1057435127_0_0_0,
	&GenInst_KeyValuePair_2_t752138246_0_0_0,
	&GenInst_Type_t_0_0_0_AttributeInfo_t2366110328_0_0_0,
	&GenInst_AttributeInfo_t2366110328_0_0_0,
	&GenInst_KeyValuePair_2_t2060813447_0_0_0,
	&GenInst_FormatParam_t2947516451_0_0_0,
	&GenInst_TermInfoStrings_t1425267120_0_0_0,
	&GenInst_TimeZoneInfo_t436210607_0_0_0,
	&GenInst_AdjustmentRule_t2179708818_0_0_0,
	&GenInst_IEquatable_1_t88723691_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_TZifType_t1855764066_0_0_0,
	&GenInst_String_t_0_0_0_TimeZoneInfo_t436210607_0_0_0,
	&GenInst_KeyValuePair_2_t108335091_0_0_0,
	&GenInst_TransitionTime_t3441274853_0_0_0,
	&GenInst_ModifierSpec_t570925440_0_0_0,
	&GenInst_TypeIdentifier_t2572806063_0_0_0,
	&GenInst_TypeSpec_t2066641911_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_IList_1_t4224045037_0_0_0,
	&GenInst_ICollection_1_t340212445_0_0_0,
	&GenInst_IEnumerable_1_t3975231481_0_0_0,
	&GenInst_IReadOnlyList_1_t1086515527_0_0_0,
	&GenInst_IReadOnlyCollection_1_t3270769163_0_0_0,
	&GenInst_CipherSuite_t491456551_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_MonoSslPolicyErrors_t621534536_0_0_0,
	&GenInst_TlsProtocols_t1951446164_0_0_0,
	&GenInst_CipherSuiteCode_t4229451518_0_0_0,
	&GenInst_Chunk_t3860501603_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0,
	&GenInst_SimpleAsyncResult_t2937691397_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GetProxyData_t1386489386_0_0_0,
	&GenInst_CFProxy_t537977545_0_0_0,
	&GenInst_NetworkInterface_t63927633_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_KeyValuePair_2_t1126309774_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_KeyValuePair_2_t3536594779_0_0_0,
	&GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0,
	&GenInst_String_t_0_0_0_WebConnectionGroup_t3242458773_0_0_0,
	&GenInst_WebConnectionGroup_t3242458773_0_0_0,
	&GenInst_KeyValuePair_2_t2914583257_0_0_0,
	&GenInst_Thread_t241561612_0_0_0,
	&GenInst_Thread_t241561612_0_0_0_StackTrace_t2500644597_0_0_0,
	&GenInst_KeyValuePair_2_t2926637898_0_0_0,
	&GenInst_KeyValuePair_2_t221307626_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IOSelectorJob_t2021937086_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_WSABUF_t2199312694_0_0_0,
	&GenInst_GCHandle_t3409268066_0_0_0,
	&GenInst_SocketAsyncEventArgs_t2815111766_0_0_0,
	&GenInst_ConnectionState_t2608615043_0_0_0,
	&GenInst_WebConnection_t324679648_0_0_0,
	&GenInst_X509CertificateImpl_t3842064707_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_DnsResourceRecord_t2943454412_0_0_0,
	&GenInst_DnsQuestion_t3090842959_0_0_0,
	&GenInst_IPEndPoint_t2615413766_0_0_0,
	&GenInst_EndPoint_t4156119363_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SimpleResolverEventArgs_t4294564137_0_0_0,
	&GenInst_SimpleResolverEventArgs_t4294564137_0_0_0,
	&GenInst_KeyValuePair_2_t1059734994_0_0_0,
	&GenInst_CachedCodeEntry_t3553821051_0_0_0,
	&GenInst_SingleRange_t3794243288_0_0_0,
	&GenInst_LowerCaseMapping_t2153935826_0_0_0,
	&GenInst_BacktrackNote_t1775865058_0_0_0,
	&GenInst_RegexFC_t2009854258_0_0_0,
	&GenInst_RegexNode_t2469392321_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_RegexOptions_t2418259727_0_0_0,
	&GenInst_AttributeEntry_t168441916_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_EventDescriptor_t962731901_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_WeakReference_t1077405567_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_String_t_0_0_0_UriParser_t1012511323_0_0_0,
	&GenInst_UriParser_t1012511323_0_0_0,
	&GenInst_KeyValuePair_2_t684635807_0_0_0,
	&GenInst_HeaderInfo_t3044570949_0_0_0,
	&GenInst_ICredentials_t3855617113_0_0_0,
	&GenInst_RecognizedAttribute_t3930529114_0_0_0,
	&GenInst_HeaderVariantInfo_t2058796272_0_0_0,
	&GenInst_RfcChar_t1416622761_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_ConfigurationProperty_t2048066811_0_0_0,
	&GenInst_XPathNode_t3118381855_0_0_0,
	&GenInst_IDtdDefaultAttributeInfo_t2326795264_0_0_0,
	&GenInst_IDtdAttributeInfo_t728124959_0_0_0,
	&GenInst_ParsingState_t1278724163_0_0_0,
	&GenInst_Tuple_4_t3284226035_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t2404255042_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Tuple_4_t3284226035_0_0_0,
	&GenInst_NodeData_t2613273532_0_0_0,
	&GenInst_IDtdEntityInfo_t4127388056_0_0_0_IDtdEntityInfo_t4127388056_0_0_0,
	&GenInst_IDtdEntityInfo_t4127388056_0_0_0,
	&GenInst_KeyValuePair_2_t1617559729_0_0_0,
	&GenInst_TagInfo_t1244462138_0_0_0,
	&GenInst_State_t563545493_0_0_0,
	&GenInst_Namespace_t1808381789_0_0_0,
	&GenInst_XmlName_t3016058992_0_0_0,
	&GenInst_IXmlSchemaInfo_t2533799901_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaAttDef_t1510907267_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_SchemaAttDef_t1510907267_0_0_0,
	&GenInst_KeyValuePair_2_t257992896_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaElementDecl_t1940851905_0_0_0,
	&GenInst_SchemaElementDecl_t1940851905_0_0_0,
	&GenInst_KeyValuePair_2_t687937534_0_0_0,
	&GenInst_String_t_0_0_0_SchemaNotation_t2083484095_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0_SchemaEntity_t980649128_0_0_0,
	&GenInst_SchemaNotation_t2083484095_0_0_0,
	&GenInst_KeyValuePair_2_t1755608579_0_0_0,
	&GenInst_SchemaEntity_t980649128_0_0_0,
	&GenInst_KeyValuePair_2_t4022702053_0_0_0,
	&GenInst_VirtualAttribute_t2854289803_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_Asttree_t3451058494_0_0_0,
	&GenInst_LocatedActiveAxis_t90453917_0_0_0,
	&GenInst_ActiveAxis_t439376929_0_0_0,
	&GenInst_XmlAtomicValue_t752869371_0_0_0,
	&GenInst_XPathItem_t3130801258_0_0_0,
	&GenInst_TypedObject_t1797374135_0_0_0,
	&GenInst_BitSet_t1062448123_0_0_0,
	&GenInst_InteriorNode_t2716368958_0_0_0,
	&GenInst_SequenceConstructPosContext_t3853454650_0_0_0,
	&GenInst_RangePositionInfo_t2780802922_0_0_0,
	&GenInst_XmlSchemaSimpleType_t248156492_0_0_0,
	&GenInst_XmlSchemaType_t1795078578_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2082486936_0_0_0,
	&GenInst_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_DatatypeImplementation_t1152094268_0_0_0,
	&GenInst_XmlSchemaDatatype_t1195946242_0_0_0,
	&GenInst_SchemaDatatypeMap_t2661667341_0_0_0,
	&GenInst_Uri_t19570940_0_0_0,
	&GenInst_String_t_0_0_0_UndeclaredNotation_t2066394897_0_0_0,
	&GenInst_UndeclaredNotation_t2066394897_0_0_0,
	&GenInst_KeyValuePair_2_t1738519381_0_0_0,
	&GenInst_ParseElementOnlyContent_LocalFrame_t225387055_0_0_0,
	&GenInst_Map_t2552390023_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Entry_t1114109354_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_KeyValuePair_2_t691798145_0_0_0,
	&GenInst_CompiledIdentityConstraint_t964629540_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_ConstraintStruct_t2462842120_0_0_0,
	&GenInst_XdrAttributeEntry_t2834798683_0_0_0,
	&GenInst_XdrEntry_t2813485863_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_KeyValuePair_2_t797999370_0_0_0,
	&GenInst_XmlSchemaObjectEntry_t2510586510_0_0_0,
	&GenInst_XmlSchemaParticle_t3365045970_0_0_0,
	&GenInst_XmlSchemaAttribute_t4015859774_0_0_0,
	&GenInst_XPathNavigator_t3981235968_0_0_0,
	&GenInst_XmlValueConverter_t68179724_0_0_0,
	&GenInst_State_t2667800191_0_0_0,
	&GenInst_XsdAttributeEntry_t2279094079_0_0_0,
	&GenInst_XsdEntry_t2653064619_0_0_0,
	&GenInst_XmlTypeCode_t58293802_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t2375465813_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Stream_t3255436806_0_0_0,
	&GenInst_WebResponse_t1895226051_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t1015255058_0_0_0,
	&GenInst_Il2CppObject_0_0_0_WebResponse_t1895226051_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_WebResponse_t1895226051_0_0_0,
	&GenInst_NamespaceDeclaration_t3577631811_0_0_0,
	&GenInst_XPathResultType_t1521569578_0_0_0,
	&GenInst_Op_t1831748991_0_0_0,
	&GenInst_XPathNodeRef_t2092605142_0_0_0_XPathNodeRef_t2092605142_0_0_0,
	&GenInst_KeyValuePair_2_t1483742713_0_0_0,
	&GenInst_XPathNodeRef_t2092605142_0_0_0,
	&GenInst_Entry_t1422871999_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_IWeakActionHandler_t2772394101_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_SemanticMeaning_t2306793223_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_AnimatorControllerParameter_t1381019216_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MessageEventArgs_t301283622_0_0_0,
	&GenInst_FieldWithTarget_t2256174789_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_ArraySegmentEnumerator_t4144823243_gp_0_0_0_0,
	&GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0,
	&GenInst_ConcurrentDictionary_2_t165941581_gp_0_0_0_0_ConcurrentDictionary_2_t165941581_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2550395762_0_0_0,
	&GenInst_ConcurrentDictionary_2_t165941581_gp_1_0_0_0,
	&GenInst_Tables_t14801510_gp_0_0_0_0_Tables_t14801510_gp_1_0_0_0,
	&GenInst_Tables_t14801510_gp_0_0_0_0,
	&GenInst_Node_t639350773_gp_0_0_0_0_Node_t639350773_gp_1_0_0_0,
	&GenInst_DictionaryEnumerator_t1325381877_gp_0_0_0_0_DictionaryEnumerator_t1325381877_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2515556306_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t2423412810_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1111953118_0_0_0,
	&GenInst_Node_t2994634259_0_0_0,
	&GenInst_Node_t138109607_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Nullable_1_t3627505585_0_0_0,
	&GenInst_NullableComparer_1_t421839525_gp_0_0_0_0,
	&GenInst_ObjectComparer_1_t19367471_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_Nullable_1_t758341705_0_0_0,
	&GenInst_NullableEqualityComparer_1_t1847642941_gp_0_0_0_0,
	&GenInst_ObjectEqualityComparer_1_t3384044119_gp_0_0_0_0,
	&GenInst_EnumEqualityComparer_1_t343233057_gp_0_0_0_0,
	&GenInst_SByteEnumEqualityComparer_1_t1634080424_gp_0_0_0_0,
	&GenInst_ShortEnumEqualityComparer_1_t2311249405_gp_0_0_0_0,
	&GenInst_LongEnumEqualityComparer_1_t2298570595_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_IReadOnlyCollection_1_t4269750562_gp_0_0_0_0,
	&GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0,
	&GenInst_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t66423410_0_0_0,
	&GenInst_IReadOnlyDictionary_2_t985373069_gp_0_0_0_0_IReadOnlyDictionary_2_t985373069_gp_1_0_0_0,
	&GenInst_IReadOnlyList_1_t898315966_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_Contract_ForAll_m1974577506_gp_0_0_0_0,
	&GenInst_ListBuilder_1_t3185767060_gp_0_0_0_0,
	&GenInst_AsyncTaskMethodBuilder_1_t3904784414_gp_0_0_0_0,
	&GenInst_Task_1_t3672445870_0_0_0,
	&GenInst_AsyncTaskCache_CreateCacheableTask_m765140694_gp_0_0_0_0,
	&GenInst_TaskAwaiter_1_t3746736679_gp_0_0_0_0,
	&GenInst_ConfiguredTaskAwaitable_1_t666767128_gp_0_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t2142494835_gp_0_0_0_0,
	&GenInst_SparselyPopulatedArray_1_t3185419593_gp_0_0_0_0,
	&GenInst_SparselyPopulatedArrayFragment_1_t719568888_0_0_0,
	&GenInst_SparselyPopulatedArrayAddInfo_1_t1006175284_gp_0_0_0_0,
	&GenInst_SparselyPopulatedArrayFragment_1_t2207155985_gp_0_0_0_0,
	&GenInst_LazyInitializer_EnsureInitialized_m1972180488_gp_0_0_0_0,
	&GenInst_LazyInitializer_EnsureInitializedCore_m2809920105_gp_0_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Task_1_t3208082394_gp_0_0_0_0,
	&GenInst_Task_1_t3208082394_gp_0_0_0_0,
	&GenInst_Task_1_t963265114_0_0_0_Task_1_t2975743850_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_TaskFactory_1_t4074777222_gp_0_0_0_0,
	&GenInst_TaskFactory_1_t4074777222_gp_0_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0,
	&GenInst_U3CFromAsyncImplU3Ec__AnonStorey2_t3028490193_gp_0_0_0_0,
	&GenInst_U3CFromAsyncImplU3Ec__AnonStorey1_t1462406252_gp_0_0_0_0,
	&GenInst_Task_FromResult_m725378983_gp_0_0_0_0,
	&GenInst_Task_FromCancellation_m977792743_gp_0_0_0_0,
	&GenInst_Task_Run_m3210616763_gp_0_0_0_0,
	&GenInst_AsyncLocalValueChangedArgs_1_t3831819380_0_0_0,
	&GenInst_AsyncLocal_1_t398511596_gp_0_0_0_0,
	&GenInst_AsyncLocalValueChangedArgs_1_t3798497170_gp_0_0_0_0,
	&GenInst_SparseArray_1_t2081600286_gp_0_0_0_0,
	&GenInst_Tuple_Create_m3881096823_gp_0_0_0_0_Tuple_Create_m3881096823_gp_1_0_0_0,
	&GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0,
	&GenInst_Tuple_3_t3518017773_gp_0_0_0_0_Tuple_3_t3518017773_gp_1_0_0_0_Tuple_3_t3518017773_gp_2_0_0_0,
	&GenInst_Tuple_4_t1145364778_gp_0_0_0_0_Tuple_4_t1145364778_gp_1_0_0_0_Tuple_4_t1145364778_gp_2_0_0_0_Tuple_4_t1145364778_gp_3_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JitHelpers_UnsafeCast_m4204636222_gp_0_0_0_0,
	&GenInst_JitHelpers_UnsafeEnumCast_m956476160_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_JitHelpers_UnsafeEnumCastLong_m3682415355_gp_0_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_ConditionalWeakTable_2_t2129616300_gp_0_0_0_0_ConditionalWeakTable_2_t2129616300_gp_1_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Double_t4078015681_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_SByte_t454417549_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Array_SortImpl_m1706208635_gp_0_0_0_0_Array_SortImpl_m1706208635_gp_1_0_0_0,
	&GenInst_Array_SortImpl_m3985765524_gp_0_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_SortImpl_m2766726474_gp_0_0_0_0,
	&GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0,
	&GenInst_Array_QSortArrange_m3119918310_gp_0_0_0_0_Array_QSortArrange_m3119918310_gp_1_0_0_0,
	&GenInst_Array_QSortArrange_m3147515399_gp_0_0_0_0,
	&GenInst_Array_qsort_m931699978_gp_0_0_0_0,
	&GenInst_Array_qsort_m931699978_gp_0_0_0_0_Array_qsort_m931699978_gp_1_0_0_0,
	&GenInst_Array_qsort_m3010626211_gp_0_0_0_0,
	&GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0,
	&GenInst_Array_QSortArrange_m3669011159_gp_0_0_0_0_Array_QSortArrange_m3669011159_gp_1_0_0_0,
	&GenInst_Array_QSortArrange_m2419137335_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_qsort_m3180236531_gp_0_0_0_0,
	&GenInst_Array_QSortArrange_m1211890906_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_CheckComparerAvailable_m866539301_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_GetLastIndex_m3612547965_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_GetIndex_m101930135_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Empty_m4207413324_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_FunctorComparer_1_t657880694_gp_0_0_0_0,
	&GenInst_EmptyArray_1_t1448450545_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_XmlListConverter_ToArray_m3797099009_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CombinePredicates_m2893912461_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Iterator_1_t2284292427_gp_0_0_0_0,
	&GenInst_Iterator_1_t2284292427_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0,
	&GenInst_WhereEnumerableIterator_1_t451024438_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0,
	&GenInst_WhereArrayIterator_1_t2256832775_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0,
	&GenInst_WhereListIterator_1_t2012596740_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCombinePredicatesU3Ec__AnonStorey1A_1_t3223571399_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Double_t4078015681_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SByte_t454417549_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_String_t_0_0_0,
	&GenInst_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t2759266955_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t446638270_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0,
	&GenInst_RefreshEventHandler_t456069287_0_0_0,
	&GenInst_Win32_FIXED_INFO_t1371335919_0_0_0,
	&GenInst_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t2811256899_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0,
	&GenInst_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t4171467654_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_PlayerConnection_t3517219175_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_RegexOptions_t2418259727_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetNonFileStreamAsyncU3Ec__async0_t1529316717_0_0_0,
	&GenInst_ConfiguredTaskAwaiter_t3605480143_0_0_0_U3CGetEntityAsyncU3Ec__async0_t381354349_0_0_0,
};
