﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SchemaDeclBase797759480.h"
#include "System_Xml_System_Xml_Schema_SchemaAttDef_Reserve2330987269.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4015859774;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t1510907267;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaAttDef
struct  SchemaAttDef_t1510907267  : public SchemaDeclBase_t797759480
{
public:
	// System.String System.Xml.Schema.SchemaAttDef::defExpanded
	String_t* ___defExpanded_11;
	// System.Int32 System.Xml.Schema.SchemaAttDef::lineNum
	int32_t ___lineNum_12;
	// System.Int32 System.Xml.Schema.SchemaAttDef::linePos
	int32_t ___linePos_13;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLineNum
	int32_t ___valueLineNum_14;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLinePos
	int32_t ___valueLinePos_15;
	// System.Xml.Schema.SchemaAttDef/Reserve System.Xml.Schema.SchemaAttDef::reserved
	int32_t ___reserved_16;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.SchemaAttDef::schemaAttribute
	XmlSchemaAttribute_t4015859774 * ___schemaAttribute_17;

public:
	inline static int32_t get_offset_of_defExpanded_11() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___defExpanded_11)); }
	inline String_t* get_defExpanded_11() const { return ___defExpanded_11; }
	inline String_t** get_address_of_defExpanded_11() { return &___defExpanded_11; }
	inline void set_defExpanded_11(String_t* value)
	{
		___defExpanded_11 = value;
		Il2CppCodeGenWriteBarrier(&___defExpanded_11, value);
	}

	inline static int32_t get_offset_of_lineNum_12() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___lineNum_12)); }
	inline int32_t get_lineNum_12() const { return ___lineNum_12; }
	inline int32_t* get_address_of_lineNum_12() { return &___lineNum_12; }
	inline void set_lineNum_12(int32_t value)
	{
		___lineNum_12 = value;
	}

	inline static int32_t get_offset_of_linePos_13() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___linePos_13)); }
	inline int32_t get_linePos_13() const { return ___linePos_13; }
	inline int32_t* get_address_of_linePos_13() { return &___linePos_13; }
	inline void set_linePos_13(int32_t value)
	{
		___linePos_13 = value;
	}

	inline static int32_t get_offset_of_valueLineNum_14() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___valueLineNum_14)); }
	inline int32_t get_valueLineNum_14() const { return ___valueLineNum_14; }
	inline int32_t* get_address_of_valueLineNum_14() { return &___valueLineNum_14; }
	inline void set_valueLineNum_14(int32_t value)
	{
		___valueLineNum_14 = value;
	}

	inline static int32_t get_offset_of_valueLinePos_15() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___valueLinePos_15)); }
	inline int32_t get_valueLinePos_15() const { return ___valueLinePos_15; }
	inline int32_t* get_address_of_valueLinePos_15() { return &___valueLinePos_15; }
	inline void set_valueLinePos_15(int32_t value)
	{
		___valueLinePos_15 = value;
	}

	inline static int32_t get_offset_of_reserved_16() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___reserved_16)); }
	inline int32_t get_reserved_16() const { return ___reserved_16; }
	inline int32_t* get_address_of_reserved_16() { return &___reserved_16; }
	inline void set_reserved_16(int32_t value)
	{
		___reserved_16 = value;
	}

	inline static int32_t get_offset_of_schemaAttribute_17() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___schemaAttribute_17)); }
	inline XmlSchemaAttribute_t4015859774 * get_schemaAttribute_17() const { return ___schemaAttribute_17; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_schemaAttribute_17() { return &___schemaAttribute_17; }
	inline void set_schemaAttribute_17(XmlSchemaAttribute_t4015859774 * value)
	{
		___schemaAttribute_17 = value;
		Il2CppCodeGenWriteBarrier(&___schemaAttribute_17, value);
	}
};

struct SchemaAttDef_t1510907267_StaticFields
{
public:
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.SchemaAttDef::Empty
	SchemaAttDef_t1510907267 * ___Empty_18;

public:
	inline static int32_t get_offset_of_Empty_18() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267_StaticFields, ___Empty_18)); }
	inline SchemaAttDef_t1510907267 * get_Empty_18() const { return ___Empty_18; }
	inline SchemaAttDef_t1510907267 ** get_address_of_Empty_18() { return &___Empty_18; }
	inline void set_Empty_18(SchemaAttDef_t1510907267 * value)
	{
		___Empty_18 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
