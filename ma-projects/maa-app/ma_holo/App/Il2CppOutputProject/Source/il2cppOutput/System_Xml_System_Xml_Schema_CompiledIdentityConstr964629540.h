﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_CompiledIdentityConst3811561833.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.Asttree
struct Asttree_t3451058494;
// System.Xml.Schema.Asttree[]
struct AsttreeU5BU5D_t1751151627;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t964629540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint
struct  CompiledIdentityConstraint_t964629540  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::name
	XmlQualifiedName_t1944712516 * ___name_0;
	// System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole System.Xml.Schema.CompiledIdentityConstraint::role
	int32_t ___role_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.CompiledIdentityConstraint::selector
	Asttree_t3451058494 * ___selector_2;
	// System.Xml.Schema.Asttree[] System.Xml.Schema.CompiledIdentityConstraint::fields
	AsttreeU5BU5D_t1751151627* ___fields_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::refer
	XmlQualifiedName_t1944712516 * ___refer_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___name_0)); }
	inline XmlQualifiedName_t1944712516 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t1944712516 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_role_1() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___role_1)); }
	inline int32_t get_role_1() const { return ___role_1; }
	inline int32_t* get_address_of_role_1() { return &___role_1; }
	inline void set_role_1(int32_t value)
	{
		___role_1 = value;
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___selector_2)); }
	inline Asttree_t3451058494 * get_selector_2() const { return ___selector_2; }
	inline Asttree_t3451058494 ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Asttree_t3451058494 * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier(&___selector_2, value);
	}

	inline static int32_t get_offset_of_fields_3() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___fields_3)); }
	inline AsttreeU5BU5D_t1751151627* get_fields_3() const { return ___fields_3; }
	inline AsttreeU5BU5D_t1751151627** get_address_of_fields_3() { return &___fields_3; }
	inline void set_fields_3(AsttreeU5BU5D_t1751151627* value)
	{
		___fields_3 = value;
		Il2CppCodeGenWriteBarrier(&___fields_3, value);
	}

	inline static int32_t get_offset_of_refer_4() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___refer_4)); }
	inline XmlQualifiedName_t1944712516 * get_refer_4() const { return ___refer_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refer_4() { return &___refer_4; }
	inline void set_refer_4(XmlQualifiedName_t1944712516 * value)
	{
		___refer_4 = value;
		Il2CppCodeGenWriteBarrier(&___refer_4, value);
	}
};

struct CompiledIdentityConstraint_t964629540_StaticFields
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.CompiledIdentityConstraint::Empty
	CompiledIdentityConstraint_t964629540 * ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540_StaticFields, ___Empty_5)); }
	inline CompiledIdentityConstraint_t964629540 * get_Empty_5() const { return ___Empty_5; }
	inline CompiledIdentityConstraint_t964629540 ** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(CompiledIdentityConstraint_t964629540 * value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
