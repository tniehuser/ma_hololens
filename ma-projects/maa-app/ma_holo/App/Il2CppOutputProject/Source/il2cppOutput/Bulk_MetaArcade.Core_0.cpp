﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "MetaArcade_Core_U3CModuleU3E3783534214.h"
#include "MetaArcade_Core_MetaArcade_Core_Signals_Signal1504483080.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884.h"
#include "MetaArcade_Core_MetaArcade_Core_Signals_WeakActionL119657226.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1676244907.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2141515233.h"

// MetaArcade.Core.Signals.Signal
struct Signal_t1504483080;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2595592884;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// MetaArcade.Core.Signals.WeakActionList
struct WeakActionList_t119657226;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<MetaArcade.Core.Signals.IWeakActionHandler>
struct IEnumerator_1_t247917928;
// System.Collections.Generic.List`1<MetaArcade.Core.Signals.IWeakActionHandler>
struct List_1_t2141515233;
// System.Collections.Generic.IEnumerable`1<MetaArcade.Core.Signals.IWeakActionHandler>
struct IEnumerable_1_t3064521146;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
extern Il2CppClass* List_1_t2595592884_il2cpp_TypeInfo_var;
extern Il2CppClass* WeakActionList_t119657226_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1684310414_MethodInfo_var;
extern const uint32_t Signal__ctor_m1584521506_MetadataUsageId;
extern Il2CppClass* Enumerator_t1676244907_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisIWeakActionHandler_t2772394101_m3192408209_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2286967750_MethodInfo_var;
extern const uint32_t WeakActionList_GetEnumerator_m2161730766_MetadataUsageId;
extern Il2CppClass* List_1_t2141515233_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2210655267_MethodInfo_var;
extern const uint32_t WeakActionList__ctor_m1269328028_MetadataUsageId;



// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m2600900561_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m3294992758_gshared (List_1_t2058570427 * __this, const MethodInfo* method);

// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
#define List_1__ctor_m1684310414(__this, method) ((  void (*) (List_1_t2595592884 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void MetaArcade.Core.Signals.WeakActionList::.ctor()
extern "C"  void WeakActionList__ctor_m1269328028 (WeakActionList_t119657226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<MetaArcade.Core.Signals.IWeakActionHandler>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisIWeakActionHandler_t2772394101_m3192408209(__this /* static, unused */, p0, method) ((  List_1_t2141515233 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m2600900561_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<MetaArcade.Core.Signals.IWeakActionHandler>::GetEnumerator()
#define List_1_GetEnumerator_m2286967750(__this, method) ((  Enumerator_t1676244907  (*) (List_1_t2141515233 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<MetaArcade.Core.Signals.IWeakActionHandler> MetaArcade.Core.Signals.WeakActionList::GetEnumerator()
extern "C"  Il2CppObject* WeakActionList_GetEnumerator_m2161730766 (WeakActionList_t119657226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<MetaArcade.Core.Signals.IWeakActionHandler>::.ctor()
#define List_1__ctor_m2210655267(__this, method) ((  void (*) (List_1_t2141515233 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MetaArcade.Core.Signals.Signal::.ctor()
extern "C"  void Signal__ctor_m1584521506 (Signal_t1504483080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Signal__ctor_m1584521506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2595592884 * L_0 = (List_1_t2595592884 *)il2cpp_codegen_object_new(List_1_t2595592884_il2cpp_TypeInfo_var);
		List_1__ctor_m1684310414(L_0, /*hidden argument*/List_1__ctor_m1684310414_MethodInfo_var);
		__this->set_m_oneTimeListeners_0(L_0);
		List_1_t2595592884 * L_1 = (List_1_t2595592884 *)il2cpp_codegen_object_new(List_1_t2595592884_il2cpp_TypeInfo_var);
		List_1__ctor_m1684310414(L_1, /*hidden argument*/List_1__ctor_m1684310414_MethodInfo_var);
		__this->set_m_listeners_1(L_1);
		WeakActionList_t119657226 * L_2 = (WeakActionList_t119657226 *)il2cpp_codegen_object_new(WeakActionList_t119657226_il2cpp_TypeInfo_var);
		WeakActionList__ctor_m1269328028(L_2, /*hidden argument*/NULL);
		__this->set_m_weakListeners_2(L_2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<MetaArcade.Core.Signals.IWeakActionHandler> MetaArcade.Core.Signals.WeakActionList::GetEnumerator()
extern "C"  Il2CppObject* WeakActionList_GetEnumerator_m2161730766 (WeakActionList_t119657226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeakActionList_GetEnumerator_m2161730766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2141515233 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_m_sync_1();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		List_1_t2141515233 * L_2 = __this->get_m_internalList_0();
		List_1_t2141515233 * L_3 = Enumerable_ToList_TisIWeakActionHandler_t2772394101_m3192408209(NULL /*static, unused*/, L_2, /*hidden argument*/Enumerable_ToList_TisIWeakActionHandler_t2772394101_m3192408209_MethodInfo_var);
		V_0 = L_3;
		IL2CPP_LEAVE(0x27, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0027:
	{
		List_1_t2141515233 * L_5 = V_0;
		NullCheck(L_5);
		Enumerator_t1676244907  L_6 = List_1_GetEnumerator_m2286967750(L_5, /*hidden argument*/List_1_GetEnumerator_m2286967750_MethodInfo_var);
		Enumerator_t1676244907  L_7 = L_6;
		Il2CppObject * L_8 = Box(Enumerator_t1676244907_il2cpp_TypeInfo_var, &L_7);
		V_2 = (Il2CppObject*)L_8;
		goto IL_0035;
	}

IL_0035:
	{
		Il2CppObject* L_9 = V_2;
		return L_9;
	}
}
// System.Collections.IEnumerator MetaArcade.Core.Signals.WeakActionList::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * WeakActionList_System_Collections_IEnumerable_GetEnumerator_m174763023 (WeakActionList_t119657226 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = WeakActionList_GetEnumerator_m2161730766(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MetaArcade.Core.Signals.WeakActionList::.ctor()
extern "C"  void WeakActionList__ctor_m1269328028 (WeakActionList_t119657226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeakActionList__ctor_m1269328028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2141515233 * L_0 = (List_1_t2141515233 *)il2cpp_codegen_object_new(List_1_t2141515233_il2cpp_TypeInfo_var);
		List_1__ctor_m2210655267(L_0, /*hidden argument*/List_1__ctor_m2210655267_MethodInfo_var);
		__this->set_m_internalList_0(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set_m_sync_1(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
