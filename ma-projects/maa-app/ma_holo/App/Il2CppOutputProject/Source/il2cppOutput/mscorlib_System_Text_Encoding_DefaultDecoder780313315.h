﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Text_Decoder3792697818.h"

// System.Text.Encoding
struct Encoding_t663144255;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding/DefaultDecoder
struct  DefaultDecoder_t780313315  : public Decoder_t3792697818
{
public:
	// System.Text.Encoding System.Text.Encoding/DefaultDecoder::m_encoding
	Encoding_t663144255 * ___m_encoding_2;
	// System.Boolean System.Text.Encoding/DefaultDecoder::m_hasInitializedEncoding
	bool ___m_hasInitializedEncoding_3;

public:
	inline static int32_t get_offset_of_m_encoding_2() { return static_cast<int32_t>(offsetof(DefaultDecoder_t780313315, ___m_encoding_2)); }
	inline Encoding_t663144255 * get_m_encoding_2() const { return ___m_encoding_2; }
	inline Encoding_t663144255 ** get_address_of_m_encoding_2() { return &___m_encoding_2; }
	inline void set_m_encoding_2(Encoding_t663144255 * value)
	{
		___m_encoding_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_encoding_2, value);
	}

	inline static int32_t get_offset_of_m_hasInitializedEncoding_3() { return static_cast<int32_t>(offsetof(DefaultDecoder_t780313315, ___m_hasInitializedEncoding_3)); }
	inline bool get_m_hasInitializedEncoding_3() const { return ___m_hasInitializedEncoding_3; }
	inline bool* get_address_of_m_hasInitializedEncoding_3() { return &___m_hasInitializedEncoding_3; }
	inline void set_m_hasInitializedEncoding_3(bool value)
	{
		___m_hasInitializedEncoding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
