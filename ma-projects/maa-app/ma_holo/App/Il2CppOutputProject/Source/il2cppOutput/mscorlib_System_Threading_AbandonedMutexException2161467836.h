﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.Threading.Mutex
struct Mutex_t297030111;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.AbandonedMutexException
struct  AbandonedMutexException_t2161467836  : public SystemException_t3877406272
{
public:
	// System.Int32 System.Threading.AbandonedMutexException::m_MutexIndex
	int32_t ___m_MutexIndex_16;
	// System.Threading.Mutex System.Threading.AbandonedMutexException::m_Mutex
	Mutex_t297030111 * ___m_Mutex_17;

public:
	inline static int32_t get_offset_of_m_MutexIndex_16() { return static_cast<int32_t>(offsetof(AbandonedMutexException_t2161467836, ___m_MutexIndex_16)); }
	inline int32_t get_m_MutexIndex_16() const { return ___m_MutexIndex_16; }
	inline int32_t* get_address_of_m_MutexIndex_16() { return &___m_MutexIndex_16; }
	inline void set_m_MutexIndex_16(int32_t value)
	{
		___m_MutexIndex_16 = value;
	}

	inline static int32_t get_offset_of_m_Mutex_17() { return static_cast<int32_t>(offsetof(AbandonedMutexException_t2161467836, ___m_Mutex_17)); }
	inline Mutex_t297030111 * get_m_Mutex_17() const { return ___m_Mutex_17; }
	inline Mutex_t297030111 ** get_address_of_m_Mutex_17() { return &___m_Mutex_17; }
	inline void set_m_Mutex_17(Mutex_t297030111 * value)
	{
		___m_Mutex_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_Mutex_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
