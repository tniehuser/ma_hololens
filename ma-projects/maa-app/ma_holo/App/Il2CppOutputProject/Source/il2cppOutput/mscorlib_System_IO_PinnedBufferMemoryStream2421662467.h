﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_UnmanagedMemoryStream822875729.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.PinnedBufferMemoryStream
struct  PinnedBufferMemoryStream_t2421662467  : public UnmanagedMemoryStream_t822875729
{
public:
	// System.Byte[] System.IO.PinnedBufferMemoryStream::_array
	ByteU5BU5D_t3397334013* ____array_16;
	// System.Runtime.InteropServices.GCHandle System.IO.PinnedBufferMemoryStream::_pinningHandle
	GCHandle_t3409268066  ____pinningHandle_17;

public:
	inline static int32_t get_offset_of__array_16() { return static_cast<int32_t>(offsetof(PinnedBufferMemoryStream_t2421662467, ____array_16)); }
	inline ByteU5BU5D_t3397334013* get__array_16() const { return ____array_16; }
	inline ByteU5BU5D_t3397334013** get_address_of__array_16() { return &____array_16; }
	inline void set__array_16(ByteU5BU5D_t3397334013* value)
	{
		____array_16 = value;
		Il2CppCodeGenWriteBarrier(&____array_16, value);
	}

	inline static int32_t get_offset_of__pinningHandle_17() { return static_cast<int32_t>(offsetof(PinnedBufferMemoryStream_t2421662467, ____pinningHandle_17)); }
	inline GCHandle_t3409268066  get__pinningHandle_17() const { return ____pinningHandle_17; }
	inline GCHandle_t3409268066 * get_address_of__pinningHandle_17() { return &____pinningHandle_17; }
	inline void set__pinningHandle_17(GCHandle_t3409268066  value)
	{
		____pinningHandle_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
