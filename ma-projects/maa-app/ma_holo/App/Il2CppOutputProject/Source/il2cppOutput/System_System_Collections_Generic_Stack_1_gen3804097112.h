﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.InteriorNode[]
struct InteriorNodeU5BU5D_t2811818251;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Xml.Schema.InteriorNode>
struct  Stack_1_t3804097112  : public Il2CppObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	InteriorNodeU5BU5D_t2811818251* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	Il2CppObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t3804097112, ____array_0)); }
	inline InteriorNodeU5BU5D_t2811818251* get__array_0() const { return ____array_0; }
	inline InteriorNodeU5BU5D_t2811818251** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(InteriorNodeU5BU5D_t2811818251* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier(&____array_0, value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t3804097112, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t3804097112, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t3804097112, ____syncRoot_3)); }
	inline Il2CppObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline Il2CppObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(Il2CppObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_3, value);
	}
};

struct Stack_1_t3804097112_StaticFields
{
public:
	// T[] System.Collections.Generic.Stack`1::_emptyArray
	InteriorNodeU5BU5D_t2811818251* ____emptyArray_4;

public:
	inline static int32_t get_offset_of__emptyArray_4() { return static_cast<int32_t>(offsetof(Stack_1_t3804097112_StaticFields, ____emptyArray_4)); }
	inline InteriorNodeU5BU5D_t2811818251* get__emptyArray_4() const { return ____emptyArray_4; }
	inline InteriorNodeU5BU5D_t2811818251** get_address_of__emptyArray_4() { return &____emptyArray_4; }
	inline void set__emptyArray_4(InteriorNodeU5BU5D_t2811818251* value)
	{
		____emptyArray_4 = value;
		Il2CppCodeGenWriteBarrier(&____emptyArray_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
