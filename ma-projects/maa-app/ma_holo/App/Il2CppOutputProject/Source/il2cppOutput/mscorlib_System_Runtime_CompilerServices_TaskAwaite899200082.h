﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t2945603725;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<System.Boolean>
struct  TaskAwaiter_1_t899200082 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t2945603725 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t899200082, ___m_task_0)); }
	inline Task_1_t2945603725 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t2945603725 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t2945603725 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_task_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
