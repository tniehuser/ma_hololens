﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_DateTime693205669.h"

// System.String
struct String_t;
// System.TimeZoneInfo/AdjustmentRule[]
struct AdjustmentRuleU5BU5D_t2338614759;
// System.TimeZoneInfo/CachedData
struct CachedData_t3197336479;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t1675079469;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo
struct  TimeZoneInfo_t436210607  : public Il2CppObject
{
public:
	// System.String System.TimeZoneInfo::m_id
	String_t* ___m_id_0;
	// System.String System.TimeZoneInfo::m_displayName
	String_t* ___m_displayName_1;
	// System.String System.TimeZoneInfo::m_standardDisplayName
	String_t* ___m_standardDisplayName_2;
	// System.String System.TimeZoneInfo::m_daylightDisplayName
	String_t* ___m_daylightDisplayName_3;
	// System.TimeSpan System.TimeZoneInfo::m_baseUtcOffset
	TimeSpan_t3430258949  ___m_baseUtcOffset_4;
	// System.Boolean System.TimeZoneInfo::m_supportsDaylightSavingTime
	bool ___m_supportsDaylightSavingTime_5;
	// System.TimeZoneInfo/AdjustmentRule[] System.TimeZoneInfo::m_adjustmentRules
	AdjustmentRuleU5BU5D_t2338614759* ___m_adjustmentRules_6;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_id_0)); }
	inline String_t* get_m_id_0() const { return ___m_id_0; }
	inline String_t** get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(String_t* value)
	{
		___m_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_id_0, value);
	}

	inline static int32_t get_offset_of_m_displayName_1() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_displayName_1)); }
	inline String_t* get_m_displayName_1() const { return ___m_displayName_1; }
	inline String_t** get_address_of_m_displayName_1() { return &___m_displayName_1; }
	inline void set_m_displayName_1(String_t* value)
	{
		___m_displayName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_displayName_1, value);
	}

	inline static int32_t get_offset_of_m_standardDisplayName_2() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_standardDisplayName_2)); }
	inline String_t* get_m_standardDisplayName_2() const { return ___m_standardDisplayName_2; }
	inline String_t** get_address_of_m_standardDisplayName_2() { return &___m_standardDisplayName_2; }
	inline void set_m_standardDisplayName_2(String_t* value)
	{
		___m_standardDisplayName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_standardDisplayName_2, value);
	}

	inline static int32_t get_offset_of_m_daylightDisplayName_3() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_daylightDisplayName_3)); }
	inline String_t* get_m_daylightDisplayName_3() const { return ___m_daylightDisplayName_3; }
	inline String_t** get_address_of_m_daylightDisplayName_3() { return &___m_daylightDisplayName_3; }
	inline void set_m_daylightDisplayName_3(String_t* value)
	{
		___m_daylightDisplayName_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_daylightDisplayName_3, value);
	}

	inline static int32_t get_offset_of_m_baseUtcOffset_4() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_baseUtcOffset_4)); }
	inline TimeSpan_t3430258949  get_m_baseUtcOffset_4() const { return ___m_baseUtcOffset_4; }
	inline TimeSpan_t3430258949 * get_address_of_m_baseUtcOffset_4() { return &___m_baseUtcOffset_4; }
	inline void set_m_baseUtcOffset_4(TimeSpan_t3430258949  value)
	{
		___m_baseUtcOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_supportsDaylightSavingTime_5() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_supportsDaylightSavingTime_5)); }
	inline bool get_m_supportsDaylightSavingTime_5() const { return ___m_supportsDaylightSavingTime_5; }
	inline bool* get_address_of_m_supportsDaylightSavingTime_5() { return &___m_supportsDaylightSavingTime_5; }
	inline void set_m_supportsDaylightSavingTime_5(bool value)
	{
		___m_supportsDaylightSavingTime_5 = value;
	}

	inline static int32_t get_offset_of_m_adjustmentRules_6() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607, ___m_adjustmentRules_6)); }
	inline AdjustmentRuleU5BU5D_t2338614759* get_m_adjustmentRules_6() const { return ___m_adjustmentRules_6; }
	inline AdjustmentRuleU5BU5D_t2338614759** get_address_of_m_adjustmentRules_6() { return &___m_adjustmentRules_6; }
	inline void set_m_adjustmentRules_6(AdjustmentRuleU5BU5D_t2338614759* value)
	{
		___m_adjustmentRules_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_adjustmentRules_6, value);
	}
};

struct TimeZoneInfo_t436210607_StaticFields
{
public:
	// System.TimeZoneInfo/CachedData System.TimeZoneInfo::s_cachedData
	CachedData_t3197336479 * ___s_cachedData_7;
	// System.DateTime System.TimeZoneInfo::s_maxDateOnly
	DateTime_t693205669  ___s_maxDateOnly_8;
	// System.DateTime System.TimeZoneInfo::s_minDateOnly
	DateTime_t693205669  ___s_minDateOnly_9;
	// System.Func`2<System.Char,System.Boolean> System.TimeZoneInfo::<>f__am$cache0
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache0_10;
	// System.Func`2<System.Char,System.Boolean> System.TimeZoneInfo::<>f__am$cache1
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache1_11;
	// System.Func`2<System.Char,System.Boolean> System.TimeZoneInfo::<>f__am$cache2
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache2_12;
	// System.Func`2<System.Char,System.Boolean> System.TimeZoneInfo::<>f__am$cache3
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache3_13;

public:
	inline static int32_t get_offset_of_s_cachedData_7() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___s_cachedData_7)); }
	inline CachedData_t3197336479 * get_s_cachedData_7() const { return ___s_cachedData_7; }
	inline CachedData_t3197336479 ** get_address_of_s_cachedData_7() { return &___s_cachedData_7; }
	inline void set_s_cachedData_7(CachedData_t3197336479 * value)
	{
		___s_cachedData_7 = value;
		Il2CppCodeGenWriteBarrier(&___s_cachedData_7, value);
	}

	inline static int32_t get_offset_of_s_maxDateOnly_8() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___s_maxDateOnly_8)); }
	inline DateTime_t693205669  get_s_maxDateOnly_8() const { return ___s_maxDateOnly_8; }
	inline DateTime_t693205669 * get_address_of_s_maxDateOnly_8() { return &___s_maxDateOnly_8; }
	inline void set_s_maxDateOnly_8(DateTime_t693205669  value)
	{
		___s_maxDateOnly_8 = value;
	}

	inline static int32_t get_offset_of_s_minDateOnly_9() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___s_minDateOnly_9)); }
	inline DateTime_t693205669  get_s_minDateOnly_9() const { return ___s_minDateOnly_9; }
	inline DateTime_t693205669 * get_address_of_s_minDateOnly_9() { return &___s_minDateOnly_9; }
	inline void set_s_minDateOnly_9(DateTime_t693205669  value)
	{
		___s_minDateOnly_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_12() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___U3CU3Ef__amU24cache2_12)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache2_12() const { return ___U3CU3Ef__amU24cache2_12; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache2_12() { return &___U3CU3Ef__amU24cache2_12; }
	inline void set_U3CU3Ef__amU24cache2_12(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache2_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_13() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t436210607_StaticFields, ___U3CU3Ef__amU24cache3_13)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache3_13() const { return ___U3CU3Ef__amU24cache3_13; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache3_13() { return &___U3CU3Ef__amU24cache3_13; }
	inline void set_U3CU3Ef__amU24cache3_13(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache3_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
