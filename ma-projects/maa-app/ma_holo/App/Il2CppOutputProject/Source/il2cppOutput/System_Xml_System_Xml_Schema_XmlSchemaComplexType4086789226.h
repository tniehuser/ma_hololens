﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"

// System.Xml.Schema.XmlSchemaContentModel
struct XmlSchemaContentModel_t907989596;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t4086789226;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexType
struct  XmlSchemaComplexType_t4086789226  : public XmlSchemaType_t1795078578
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::block
	int32_t ___block_19;
	// System.Xml.Schema.XmlSchemaContentModel System.Xml.Schema.XmlSchemaComplexType::contentModel
	XmlSchemaContentModel_t907989596 * ___contentModel_20;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::particle
	XmlSchemaParticle_t3365045970 * ___particle_21;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexType::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_22;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_23;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::contentTypeParticle
	XmlSchemaParticle_t3365045970 * ___contentTypeParticle_24;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::blockResolved
	int32_t ___blockResolved_25;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::localElements
	XmlSchemaObjectTable_t3364835593 * ___localElements_26;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::attributeUses
	XmlSchemaObjectTable_t3364835593 * ___attributeUses_27;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::attributeWildcard
	XmlSchemaAnyAttribute_t530453212 * ___attributeWildcard_28;
	// System.Byte System.Xml.Schema.XmlSchemaComplexType::pvFlags
	uint8_t ___pvFlags_32;

public:
	inline static int32_t get_offset_of_block_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___block_19)); }
	inline int32_t get_block_19() const { return ___block_19; }
	inline int32_t* get_address_of_block_19() { return &___block_19; }
	inline void set_block_19(int32_t value)
	{
		___block_19 = value;
	}

	inline static int32_t get_offset_of_contentModel_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___contentModel_20)); }
	inline XmlSchemaContentModel_t907989596 * get_contentModel_20() const { return ___contentModel_20; }
	inline XmlSchemaContentModel_t907989596 ** get_address_of_contentModel_20() { return &___contentModel_20; }
	inline void set_contentModel_20(XmlSchemaContentModel_t907989596 * value)
	{
		___contentModel_20 = value;
		Il2CppCodeGenWriteBarrier(&___contentModel_20, value);
	}

	inline static int32_t get_offset_of_particle_21() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___particle_21)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_21() const { return ___particle_21; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_21() { return &___particle_21; }
	inline void set_particle_21(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_21 = value;
		Il2CppCodeGenWriteBarrier(&___particle_21, value);
	}

	inline static int32_t get_offset_of_attributes_22() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributes_22)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_22() const { return ___attributes_22; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_22() { return &___attributes_22; }
	inline void set_attributes_22(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_22 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_22, value);
	}

	inline static int32_t get_offset_of_anyAttribute_23() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___anyAttribute_23)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_23() const { return ___anyAttribute_23; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_23() { return &___anyAttribute_23; }
	inline void set_anyAttribute_23(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_23 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_23, value);
	}

	inline static int32_t get_offset_of_contentTypeParticle_24() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___contentTypeParticle_24)); }
	inline XmlSchemaParticle_t3365045970 * get_contentTypeParticle_24() const { return ___contentTypeParticle_24; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_contentTypeParticle_24() { return &___contentTypeParticle_24; }
	inline void set_contentTypeParticle_24(XmlSchemaParticle_t3365045970 * value)
	{
		___contentTypeParticle_24 = value;
		Il2CppCodeGenWriteBarrier(&___contentTypeParticle_24, value);
	}

	inline static int32_t get_offset_of_blockResolved_25() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___blockResolved_25)); }
	inline int32_t get_blockResolved_25() const { return ___blockResolved_25; }
	inline int32_t* get_address_of_blockResolved_25() { return &___blockResolved_25; }
	inline void set_blockResolved_25(int32_t value)
	{
		___blockResolved_25 = value;
	}

	inline static int32_t get_offset_of_localElements_26() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___localElements_26)); }
	inline XmlSchemaObjectTable_t3364835593 * get_localElements_26() const { return ___localElements_26; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_localElements_26() { return &___localElements_26; }
	inline void set_localElements_26(XmlSchemaObjectTable_t3364835593 * value)
	{
		___localElements_26 = value;
		Il2CppCodeGenWriteBarrier(&___localElements_26, value);
	}

	inline static int32_t get_offset_of_attributeUses_27() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributeUses_27)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeUses_27() const { return ___attributeUses_27; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeUses_27() { return &___attributeUses_27; }
	inline void set_attributeUses_27(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeUses_27 = value;
		Il2CppCodeGenWriteBarrier(&___attributeUses_27, value);
	}

	inline static int32_t get_offset_of_attributeWildcard_28() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributeWildcard_28)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_attributeWildcard_28() const { return ___attributeWildcard_28; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_attributeWildcard_28() { return &___attributeWildcard_28; }
	inline void set_attributeWildcard_28(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___attributeWildcard_28 = value;
		Il2CppCodeGenWriteBarrier(&___attributeWildcard_28, value);
	}

	inline static int32_t get_offset_of_pvFlags_32() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___pvFlags_32)); }
	inline uint8_t get_pvFlags_32() const { return ___pvFlags_32; }
	inline uint8_t* get_address_of_pvFlags_32() { return &___pvFlags_32; }
	inline void set_pvFlags_32(uint8_t value)
	{
		___pvFlags_32 = value;
	}
};

struct XmlSchemaComplexType_t4086789226_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyTypeLax
	XmlSchemaComplexType_t4086789226 * ___anyTypeLax_29;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyTypeSkip
	XmlSchemaComplexType_t4086789226 * ___anyTypeSkip_30;
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::untypedAnyType
	XmlSchemaComplexType_t4086789226 * ___untypedAnyType_31;

public:
	inline static int32_t get_offset_of_anyTypeLax_29() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226_StaticFields, ___anyTypeLax_29)); }
	inline XmlSchemaComplexType_t4086789226 * get_anyTypeLax_29() const { return ___anyTypeLax_29; }
	inline XmlSchemaComplexType_t4086789226 ** get_address_of_anyTypeLax_29() { return &___anyTypeLax_29; }
	inline void set_anyTypeLax_29(XmlSchemaComplexType_t4086789226 * value)
	{
		___anyTypeLax_29 = value;
		Il2CppCodeGenWriteBarrier(&___anyTypeLax_29, value);
	}

	inline static int32_t get_offset_of_anyTypeSkip_30() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226_StaticFields, ___anyTypeSkip_30)); }
	inline XmlSchemaComplexType_t4086789226 * get_anyTypeSkip_30() const { return ___anyTypeSkip_30; }
	inline XmlSchemaComplexType_t4086789226 ** get_address_of_anyTypeSkip_30() { return &___anyTypeSkip_30; }
	inline void set_anyTypeSkip_30(XmlSchemaComplexType_t4086789226 * value)
	{
		___anyTypeSkip_30 = value;
		Il2CppCodeGenWriteBarrier(&___anyTypeSkip_30, value);
	}

	inline static int32_t get_offset_of_untypedAnyType_31() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226_StaticFields, ___untypedAnyType_31)); }
	inline XmlSchemaComplexType_t4086789226 * get_untypedAnyType_31() const { return ___untypedAnyType_31; }
	inline XmlSchemaComplexType_t4086789226 ** get_address_of_untypedAnyType_31() { return &___untypedAnyType_31; }
	inline void set_untypedAnyType_31(XmlSchemaComplexType_t4086789226 * value)
	{
		___untypedAnyType_31 = value;
		Il2CppCodeGenWriteBarrier(&___untypedAnyType_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
