﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_TaskScheduler3932792796.h"

// System.Threading.ParameterizedThreadStart
struct ParameterizedThreadStart_t2412552885;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.ThreadPoolTaskScheduler
struct  ThreadPoolTaskScheduler_t943177722  : public TaskScheduler_t3932792796
{
public:

public:
};

struct ThreadPoolTaskScheduler_t943177722_StaticFields
{
public:
	// System.Threading.ParameterizedThreadStart System.Threading.Tasks.ThreadPoolTaskScheduler::s_longRunningThreadWork
	ParameterizedThreadStart_t2412552885 * ___s_longRunningThreadWork_4;

public:
	inline static int32_t get_offset_of_s_longRunningThreadWork_4() { return static_cast<int32_t>(offsetof(ThreadPoolTaskScheduler_t943177722_StaticFields, ___s_longRunningThreadWork_4)); }
	inline ParameterizedThreadStart_t2412552885 * get_s_longRunningThreadWork_4() const { return ___s_longRunningThreadWork_4; }
	inline ParameterizedThreadStart_t2412552885 ** get_address_of_s_longRunningThreadWork_4() { return &___s_longRunningThreadWork_4; }
	inline void set_s_longRunningThreadWork_4(ParameterizedThreadStart_t2412552885 * value)
	{
		___s_longRunningThreadWork_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_longRunningThreadWork_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
