﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_TimerThread777216161.h"
#include "System_System_Net_TimerThread_Queue1332364135.h"
#include "System_System_Net_TimerThread_Timer2936528677.h"
#include "System_System_Net_TimerThread_Callback697818895.h"
#include "System_System_Net_TimerThread_TimerQueue385709908.h"
#include "System_System_Net_TimerThread_InfiniteTimerQueue2156216176.h"
#include "System_System_Net_TimerThread_TimerNode3034660285.h"
#include "System_System_Net_TimerThread_TimerNode_TimerState3530038064.h"
#include "System_System_Net_Authorization1602399.h"
#include "System_System_Net_CredentialCache1992799279.h"
#include "System_System_Net_CredentialCache_CredentialEnumer3600195683.h"
#include "System_System_Net_SystemNetworkCredential677084018.h"
#include "System_System_Net_CredentialKey4100643814.h"
#include "System_System_Net_CookieVariant839969627.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Net_CookieToken1256074727.h"
#include "System_System_Net_CookieTokenizer3643404755.h"
#include "System_System_Net_CookieTokenizer_RecognizedAttrib3930529114.h"
#include "System_System_Net_CookieParser1405985527.h"
#include "System_System_Net_Comparer3706593733.h"
#include "System_System_Net_CookieCollection521422364.h"
#include "System_System_Net_CookieCollection_Stamp1524603388.h"
#include "System_System_Net_CookieCollection_CookieCollectio2406598047.h"
#include "System_System_Net_HeaderVariantInfo2058796272.h"
#include "System_System_Net_CookieContainer2808809223.h"
#include "System_System_Net_PathList718968007.h"
#include "System_System_Net_PathList_PathListComparer2346409738.h"
#include "System_System_Net_CookieException1505724635.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_FtpStatusCode1448112771.h"
#include "System_System_Net_FileWebRequest1571840111.h"
#include "System_System_Net_FileWebRequestCreator1109072211.h"
#include "System_System_Net_FileWebStream791154234.h"
#include "System_System_Net_FileWebResponse1934981865.h"
#include "System_System_Net_GlobalProxySelection2251180943.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "System_System_Net_HttpVersion1276659706.h"
#include "System_System_Net_InternalException1919254860.h"
#include "System_System_Net_NclUtilities1950342895.h"
#include "System_System_Net_ValidationHelper2126475125.h"
#include "System_System_Net_ExceptionHelper2752294609.h"
#include "System_System_Net_WebRequestPrefixElement283218055.h"
#include "System_System_Net_HttpContinueDelegate2713047268.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_NetworkCredential1714133953.h"
#include "System_System_Net_ProtocolViolationException4263317570.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"
#include "System_System_Net_ServerCertValidationCallback2774612835.h"
#include "System_System_Net_ServerCertValidationCallback_Cal3155372874.h"
#include "System_System_Net_SocketAddress838303055.h"
#include "System_System_Net_WebException3368933679.h"
#include "System_System_Net_WebExceptionInternalStatus1357294310.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebExceptionMapping159714763.h"
#include "System_System_Net_WebHeaderCollectionType1212469221.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Net_WebHeaderCollection_RfcChar1416622761.h"
#include "System_System_Net_CaseInsensitiveAscii4138584646.h"
#include "System_System_Net_WebProxyData1226725784.h"
#include "System_System_Net_WebProxy1169192840.h"
#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_WebRequest_DesignerWebRequestCre3086960480.h"
#include "System_System_Net_WebRequest_WebProxyWrapperOpaque1012624580.h"
#include "System_System_Net_WebRequest_WebProxyWrapper3016229293.h"
#include "System_System_Net_WebResponse1895226051.h"
#include "System_System_Net_Cache_RequestCache1417804387.h"
#include "System_System_Net_Cache_RequestCacheLevel2979444753.h"
#include "System_System_Net_Cache_RequestCachePolicy2663429579.h"
#include "System_System_Net_Cache_RequestCacheValidator1766318073.h"
#include "System_System_Net_Cache_RequestCacheBinding114276176.h"
#include "System_System_Net_Configuration_DefaultProxySectio2870546683.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"
#include "System_System_Net_NetworkInformation_IPAddressColl2986660307.h"
#include "System_System_Net_NetworkInformation_IPGlobalProper430107897.h"
#include "System_System_Net_NetworkInformation_IPInterfacePr3986609851.h"
#include "System_System_Net_NetworkInformation_IPv4Interface3946458365.h"
#include "System_System_Net_NetworkInformation_NetworkInform1863186723.h"
#include "System_System_Net_NetworkInformation_NetworkInterfac63927633.h"
#include "System_System_Net_NetworkInformation_OperationalSt2833345236.h"
#include "System_System_Net_NetworkInformation_NetworkInterf4226883065.h"
#include "System_System_Net_NetworkInformation_NetBiosNodeTy2005148930.h"
#include "System_System_Security_Cryptography_OidGroup4038341371.h"
#include "System_System_Security_Cryptography_Oid3221867120.h"
#include "System_System_Security_Cryptography_OidCollection3790243618.h"
#include "System_System_Security_Cryptography_OidEnumerator3674631724.h"
#include "System_System_Security_Cryptography_X509Certificate810863315.h"
#include "System_System_InvariantComparer3322871449.h"
#include "System_System_SecurityUtils1714011141.h"
#include "System_System_ComponentModel_WeakHashtable1679685894.h"
#include "System_System_ComponentModel_WeakHashtable_WeakKey3958653828.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1459944466.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (TimerThread_t777216161), -1, sizeof(TimerThread_t777216161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[7] = 
{
	TimerThread_t777216161_StaticFields::get_offset_of_s_Queues_0(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_NewQueues_1(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_ThreadState_2(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_ThreadReadyEvent_3(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_ThreadShutdownEvent_4(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_ThreadEvents_5(),
	TimerThread_t777216161_StaticFields::get_offset_of_s_QueuesCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (Queue_t1332364135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	Queue_t1332364135::get_offset_of_m_DurationMilliseconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (Timer_t2936528677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[2] = 
{
	Timer_t2936528677::get_offset_of_m_StartTimeMilliseconds_0(),
	Timer_t2936528677::get_offset_of_m_DurationMilliseconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (Callback_t697818895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (TimerQueue_t385709908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[1] = 
{
	TimerQueue_t385709908::get_offset_of_m_Timers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (InfiniteTimerQueue_t2156216176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (TimerNode_t3034660285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[6] = 
{
	TimerNode_t3034660285::get_offset_of_m_TimerState_2(),
	TimerNode_t3034660285::get_offset_of_m_Callback_3(),
	TimerNode_t3034660285::get_offset_of_m_Context_4(),
	TimerNode_t3034660285::get_offset_of_m_QueueLock_5(),
	TimerNode_t3034660285::get_offset_of_next_6(),
	TimerNode_t3034660285::get_offset_of_prev_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (TimerState_t3530038064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2207[5] = 
{
	TimerState_t3530038064::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Authorization_t1602399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[3] = 
{
	Authorization_t1602399::get_offset_of_m_Message_0(),
	Authorization_t1602399::get_offset_of_m_Complete_1(),
	Authorization_t1602399::get_offset_of_ModuleAuthenticationType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (CredentialCache_t1992799279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[3] = 
{
	CredentialCache_t1992799279::get_offset_of_cache_0(),
	CredentialCache_t1992799279::get_offset_of_cacheForHosts_1(),
	CredentialCache_t1992799279::get_offset_of_m_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (CredentialEnumerator_t3600195683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[4] = 
{
	CredentialEnumerator_t3600195683::get_offset_of_m_cache_0(),
	CredentialEnumerator_t3600195683::get_offset_of_m_array_1(),
	CredentialEnumerator_t3600195683::get_offset_of_m_index_2(),
	CredentialEnumerator_t3600195683::get_offset_of_m_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (SystemNetworkCredential_t677084018), -1, sizeof(SystemNetworkCredential_t677084018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2211[1] = 
{
	SystemNetworkCredential_t677084018_StaticFields::get_offset_of_defaultCredential_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (CredentialKey_t4100643814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	CredentialKey_t4100643814::get_offset_of_UriPrefix_0(),
	CredentialKey_t4100643814::get_offset_of_UriPrefixLength_1(),
	CredentialKey_t4100643814::get_offset_of_AuthenticationType_2(),
	CredentialKey_t4100643814::get_offset_of_m_HashCode_3(),
	CredentialKey_t4100643814::get_offset_of_m_ComputedHashCode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (CookieVariant_t839969627)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[6] = 
{
	CookieVariant_t839969627::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Cookie_t3154017544), -1, sizeof(Cookie_t3154017544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[25] = 
{
	Cookie_t3154017544_StaticFields::get_offset_of_PortSplitDelimiters_0(),
	Cookie_t3154017544_StaticFields::get_offset_of_Reserved2Name_1(),
	Cookie_t3154017544_StaticFields::get_offset_of_Reserved2Value_2(),
	Cookie_t3154017544_StaticFields::get_offset_of_staticComparer_3(),
	Cookie_t3154017544::get_offset_of_m_comment_4(),
	Cookie_t3154017544::get_offset_of_m_commentUri_5(),
	Cookie_t3154017544::get_offset_of_m_cookieVariant_6(),
	Cookie_t3154017544::get_offset_of_m_discard_7(),
	Cookie_t3154017544::get_offset_of_m_domain_8(),
	Cookie_t3154017544::get_offset_of_m_domain_implicit_9(),
	Cookie_t3154017544::get_offset_of_m_expires_10(),
	Cookie_t3154017544::get_offset_of_m_name_11(),
	Cookie_t3154017544::get_offset_of_m_path_12(),
	Cookie_t3154017544::get_offset_of_m_path_implicit_13(),
	Cookie_t3154017544::get_offset_of_m_port_14(),
	Cookie_t3154017544::get_offset_of_m_port_implicit_15(),
	Cookie_t3154017544::get_offset_of_m_port_list_16(),
	Cookie_t3154017544::get_offset_of_m_secure_17(),
	Cookie_t3154017544::get_offset_of_m_httpOnly_18(),
	Cookie_t3154017544::get_offset_of_m_timeStamp_19(),
	Cookie_t3154017544::get_offset_of_m_value_20(),
	Cookie_t3154017544::get_offset_of_m_version_21(),
	Cookie_t3154017544::get_offset_of_m_domainKey_22(),
	Cookie_t3154017544::get_offset_of_IsQuotedVersion_23(),
	Cookie_t3154017544::get_offset_of_IsQuotedDomain_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (CookieToken_t1256074727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2215[21] = 
{
	CookieToken_t1256074727::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (CookieTokenizer_t3643404755), -1, sizeof(CookieTokenizer_t3643404755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2216[12] = 
{
	CookieTokenizer_t3643404755::get_offset_of_m_eofCookie_0(),
	CookieTokenizer_t3643404755::get_offset_of_m_index_1(),
	CookieTokenizer_t3643404755::get_offset_of_m_length_2(),
	CookieTokenizer_t3643404755::get_offset_of_m_name_3(),
	CookieTokenizer_t3643404755::get_offset_of_m_quoted_4(),
	CookieTokenizer_t3643404755::get_offset_of_m_start_5(),
	CookieTokenizer_t3643404755::get_offset_of_m_token_6(),
	CookieTokenizer_t3643404755::get_offset_of_m_tokenLength_7(),
	CookieTokenizer_t3643404755::get_offset_of_m_tokenStream_8(),
	CookieTokenizer_t3643404755::get_offset_of_m_value_9(),
	CookieTokenizer_t3643404755_StaticFields::get_offset_of_RecognizedAttributes_10(),
	CookieTokenizer_t3643404755_StaticFields::get_offset_of_RecognizedServerAttributes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (RecognizedAttribute_t3930529114)+ sizeof (Il2CppObject), sizeof(RecognizedAttribute_t3930529114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2217[2] = 
{
	RecognizedAttribute_t3930529114::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RecognizedAttribute_t3930529114::get_offset_of_m_token_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (CookieParser_t1405985527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[1] = 
{
	CookieParser_t1405985527::get_offset_of_m_tokenizer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (Comparer_t3706593733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CookieCollection_t521422364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[5] = 
{
	CookieCollection_t521422364::get_offset_of_m_version_0(),
	CookieCollection_t521422364::get_offset_of_m_list_1(),
	CookieCollection_t521422364::get_offset_of_m_TimeStamp_2(),
	CookieCollection_t521422364::get_offset_of_m_has_other_versions_3(),
	CookieCollection_t521422364::get_offset_of_m_IsReadOnly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (Stamp_t1524603388)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[5] = 
{
	Stamp_t1524603388::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (CookieCollectionEnumerator_t2406598047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[4] = 
{
	CookieCollectionEnumerator_t2406598047::get_offset_of_m_cookies_0(),
	CookieCollectionEnumerator_t2406598047::get_offset_of_m_count_1(),
	CookieCollectionEnumerator_t2406598047::get_offset_of_m_index_2(),
	CookieCollectionEnumerator_t2406598047::get_offset_of_m_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (HeaderVariantInfo_t2058796272)+ sizeof (Il2CppObject), sizeof(HeaderVariantInfo_t2058796272_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	HeaderVariantInfo_t2058796272::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HeaderVariantInfo_t2058796272::get_offset_of_m_variant_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (CookieContainer_t2808809223), -1, sizeof(CookieContainer_t2808809223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2224[7] = 
{
	CookieContainer_t2808809223_StaticFields::get_offset_of_HeaderInfo_0(),
	CookieContainer_t2808809223::get_offset_of_m_domainTable_1(),
	CookieContainer_t2808809223::get_offset_of_m_maxCookieSize_2(),
	CookieContainer_t2808809223::get_offset_of_m_maxCookies_3(),
	CookieContainer_t2808809223::get_offset_of_m_maxCookiesPerDomain_4(),
	CookieContainer_t2808809223::get_offset_of_m_count_5(),
	CookieContainer_t2808809223::get_offset_of_m_fqdnMyDomain_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (PathList_t718968007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[1] = 
{
	PathList_t718968007::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (PathListComparer_t2346409738), -1, sizeof(PathListComparer_t2346409738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	PathListComparer_t2346409738_StaticFields::get_offset_of_StaticInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (CookieException_t1505724635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (FtpStatusCode_t1448112771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[38] = 
{
	FtpStatusCode_t1448112771::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (FileWebRequest_t1571840111), -1, sizeof(FileWebRequest_t1571840111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2230[23] = 
{
	FileWebRequest_t1571840111_StaticFields::get_offset_of_s_GetRequestStreamCallback_12(),
	FileWebRequest_t1571840111_StaticFields::get_offset_of_s_GetResponseCallback_13(),
	FileWebRequest_t1571840111_StaticFields::get_offset_of_s_WrappedGetRequestStreamCallback_14(),
	FileWebRequest_t1571840111_StaticFields::get_offset_of_s_WrappedResponseCallback_15(),
	FileWebRequest_t1571840111::get_offset_of_m_connectionGroupName_16(),
	FileWebRequest_t1571840111::get_offset_of_m_contentLength_17(),
	FileWebRequest_t1571840111::get_offset_of_m_credentials_18(),
	FileWebRequest_t1571840111::get_offset_of_m_fileAccess_19(),
	FileWebRequest_t1571840111::get_offset_of_m_headers_20(),
	FileWebRequest_t1571840111::get_offset_of_m_method_21(),
	FileWebRequest_t1571840111::get_offset_of_m_proxy_22(),
	FileWebRequest_t1571840111::get_offset_of_m_readerEvent_23(),
	FileWebRequest_t1571840111::get_offset_of_m_readPending_24(),
	FileWebRequest_t1571840111::get_offset_of_m_response_25(),
	FileWebRequest_t1571840111::get_offset_of_m_stream_26(),
	FileWebRequest_t1571840111::get_offset_of_m_syncHint_27(),
	FileWebRequest_t1571840111::get_offset_of_m_timeout_28(),
	FileWebRequest_t1571840111::get_offset_of_m_uri_29(),
	FileWebRequest_t1571840111::get_offset_of_m_writePending_30(),
	FileWebRequest_t1571840111::get_offset_of_m_writing_31(),
	FileWebRequest_t1571840111::get_offset_of_m_WriteAResult_32(),
	FileWebRequest_t1571840111::get_offset_of_m_ReadAResult_33(),
	FileWebRequest_t1571840111::get_offset_of_m_Aborted_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (FileWebRequestCreator_t1109072211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (FileWebStream_t791154234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[1] = 
{
	FileWebStream_t791154234::get_offset_of_m_request_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (FileWebResponse_t1934981865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[6] = 
{
	FileWebResponse_t1934981865::get_offset_of_m_closed_1(),
	FileWebResponse_t1934981865::get_offset_of_m_contentLength_2(),
	FileWebResponse_t1934981865::get_offset_of_m_fileAccess_3(),
	FileWebResponse_t1934981865::get_offset_of_m_headers_4(),
	FileWebResponse_t1934981865::get_offset_of_m_stream_5(),
	FileWebResponse_t1934981865::get_offset_of_m_uri_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (HttpStatusCode_t1898409641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[48] = 
{
	HttpStatusCode_t1898409641::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (InternalException_t1919254860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (NclUtilities_t1950342895), -1, sizeof(NclUtilities_t1950342895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2242[3] = 
{
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalAddresses_0(),
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalAddressesLock_1(),
	NclUtilities_t1950342895_StaticFields::get_offset_of__LocalDomainName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ValidationHelper_t2126475125), -1, sizeof(ValidationHelper_t2126475125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	ValidationHelper_t2126475125_StaticFields::get_offset_of_EmptyArray_0(),
	ValidationHelper_t2126475125_StaticFields::get_offset_of_InvalidMethodChars_1(),
	ValidationHelper_t2126475125_StaticFields::get_offset_of_InvalidParamChars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ExceptionHelper_t2752294609), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (WebRequestPrefixElement_t283218055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[3] = 
{
	WebRequestPrefixElement_t283218055::get_offset_of_Prefix_0(),
	WebRequestPrefixElement_t283218055::get_offset_of_creator_1(),
	WebRequestPrefixElement_t283218055::get_offset_of_creatorType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2247[13] = 
{
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_0(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_1(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_2(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_3(),
	IPAddress_t1399971723::get_offset_of_m_Address_4(),
	IPAddress_t1399971723::get_offset_of_m_ToString_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_8(),
	IPAddress_t1399971723::get_offset_of_m_Family_9(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_10(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_11(),
	IPAddress_t1399971723::get_offset_of_m_HashCode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (IPEndPoint_t2615413766), -1, sizeof(IPEndPoint_t2615413766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[4] = 
{
	IPEndPoint_t2615413766::get_offset_of_m_Address_0(),
	IPEndPoint_t2615413766::get_offset_of_m_Port_1(),
	IPEndPoint_t2615413766_StaticFields::get_offset_of_Any_2(),
	IPEndPoint_t2615413766_StaticFields::get_offset_of_IPv6Any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[4] = 
{
	IPHostEntry_t994738509::get_offset_of_hostName_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_addressList_2(),
	IPHostEntry_t994738509::get_offset_of_isTrustedHost_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (NetworkCredential_t1714133953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[3] = 
{
	NetworkCredential_t1714133953::get_offset_of_m_domain_0(),
	NetworkCredential_t1714133953::get_offset_of_m_userName_1(),
	NetworkCredential_t1714133953::get_offset_of_m_password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (ProtocolViolationException_t4263317570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (SecurityProtocolType_t3099771628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2254[5] = 
{
	SecurityProtocolType_t3099771628::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (ServerCertValidationCallback_t2774612835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[2] = 
{
	ServerCertValidationCallback_t2774612835::get_offset_of_m_ValidationCallback_0(),
	ServerCertValidationCallback_t2774612835::get_offset_of_m_Context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (CallbackContext_t3155372874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[5] = 
{
	CallbackContext_t3155372874::get_offset_of_request_0(),
	CallbackContext_t3155372874::get_offset_of_certificate_1(),
	CallbackContext_t3155372874::get_offset_of_chain_2(),
	CallbackContext_t3155372874::get_offset_of_sslPolicyErrors_3(),
	CallbackContext_t3155372874::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[4] = 
{
	SocketAddress_t838303055::get_offset_of_m_Size_0(),
	SocketAddress_t838303055::get_offset_of_m_Buffer_1(),
	SocketAddress_t838303055::get_offset_of_m_changed_2(),
	SocketAddress_t838303055::get_offset_of_m_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (WebException_t3368933679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	WebException_t3368933679::get_offset_of_m_Status_16(),
	WebException_t3368933679::get_offset_of_m_Response_17(),
	WebException_t3368933679::get_offset_of_m_InternalStatus_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (WebExceptionInternalStatus_t1357294310)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2259[5] = 
{
	WebExceptionInternalStatus_t1357294310::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (WebExceptionStatus_t1169373531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[22] = 
{
	WebExceptionStatus_t1169373531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (WebExceptionMapping_t159714763), -1, sizeof(WebExceptionMapping_t159714763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	WebExceptionMapping_t159714763_StaticFields::get_offset_of_s_Mapping_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (WebHeaderCollectionType_t1212469221)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[12] = 
{
	WebHeaderCollectionType_t1212469221::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (WebHeaderCollection_t3028142837), -1, sizeof(WebHeaderCollection_t3028142837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[9] = 
{
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_HInfo_13(),
	WebHeaderCollection_t3028142837::get_offset_of_m_CommonHeaders_14(),
	WebHeaderCollection_t3028142837::get_offset_of_m_NumCommonHeaders_15(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_s_CommonHeaderNames_16(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_s_CommonHeaderHints_17(),
	WebHeaderCollection_t3028142837::get_offset_of_m_InnerCollection_18(),
	WebHeaderCollection_t3028142837::get_offset_of_m_Type_19(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_HttpTrimCharacters_20(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_RfcCharMap_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (RfcChar_t1416622761)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[9] = 
{
	RfcChar_t1416622761::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (CaseInsensitiveAscii_t4138584646), -1, sizeof(CaseInsensitiveAscii_t4138584646_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2265[2] = 
{
	CaseInsensitiveAscii_t4138584646_StaticFields::get_offset_of_StaticInstance_0(),
	CaseInsensitiveAscii_t4138584646_StaticFields::get_offset_of_AsciiToLower_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (WebProxyData_t1226725784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[6] = 
{
	WebProxyData_t1226725784::get_offset_of_bypassOnLocal_0(),
	WebProxyData_t1226725784::get_offset_of_automaticallyDetectSettings_1(),
	WebProxyData_t1226725784::get_offset_of_proxyAddress_2(),
	WebProxyData_t1226725784::get_offset_of_proxyHostAddresses_3(),
	WebProxyData_t1226725784::get_offset_of_scriptLocation_4(),
	WebProxyData_t1226725784::get_offset_of_bypassList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (WebProxy_t1169192840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[9] = 
{
	WebProxy_t1169192840::get_offset_of__UseRegistry_0(),
	WebProxy_t1169192840::get_offset_of__BypassOnLocal_1(),
	WebProxy_t1169192840::get_offset_of_m_EnableAutoproxy_2(),
	WebProxy_t1169192840::get_offset_of__ProxyAddress_3(),
	WebProxy_t1169192840::get_offset_of__BypassList_4(),
	WebProxy_t1169192840::get_offset_of__Credentials_5(),
	WebProxy_t1169192840::get_offset_of__RegExBypassList_6(),
	WebProxy_t1169192840::get_offset_of__ProxyHostAddresses_7(),
	WebProxy_t1169192840::get_offset_of_m_ScriptEngine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (WebRequest_t1365124353), -1, sizeof(WebRequest_t1365124353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2268[11] = 
{
	WebRequest_t1365124353_StaticFields::get_offset_of_s_PrefixList_1(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_InternalSyncObject_2(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultTimerQueue_3(),
	WebRequest_t1365124353::get_offset_of_m_AuthenticationLevel_4(),
	WebRequest_t1365124353::get_offset_of_m_ImpersonationLevel_5(),
	WebRequest_t1365124353::get_offset_of_m_CachePolicy_6(),
	WebRequest_t1365124353::get_offset_of_m_CacheProtocol_7(),
	WebRequest_t1365124353::get_offset_of_m_CacheBinding_8(),
	WebRequest_t1365124353_StaticFields::get_offset_of_webRequestCreate_9(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultWebProxy_10(),
	WebRequest_t1365124353_StaticFields::get_offset_of_s_DefaultWebProxyInitialized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (DesignerWebRequestCreate_t3086960480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (WebProxyWrapperOpaque_t1012624580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[1] = 
{
	WebProxyWrapperOpaque_t1012624580::get_offset_of_webProxy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (WebProxyWrapper_t3016229293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (WebResponse_t1895226051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (RequestCache_t1417804387), -1, sizeof(RequestCache_t1417804387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	RequestCache_t1417804387_StaticFields::get_offset_of_LineSplits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (RequestCacheLevel_t2979444753)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[8] = 
{
	RequestCacheLevel_t2979444753::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (RequestCachePolicy_t2663429579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[1] = 
{
	RequestCachePolicy_t2663429579::get_offset_of_m_Level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (RequestCacheValidator_t1766318073), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (RequestCacheBinding_t114276176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	RequestCacheBinding_t114276176::get_offset_of_m_RequestCache_0(),
	RequestCacheBinding_t114276176::get_offset_of_m_CacheValidator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (DefaultProxySectionInternal_t2870546683), -1, sizeof(DefaultProxySectionInternal_t2870546683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	DefaultProxySectionInternal_t2870546683::get_offset_of_webProxy_0(),
	DefaultProxySectionInternal_t2870546683_StaticFields::get_offset_of_classSyncObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (NetworkStream_t581172200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[8] = 
{
	NetworkStream_t581172200::get_offset_of_m_StreamSocket_8(),
	NetworkStream_t581172200::get_offset_of_m_Readable_9(),
	NetworkStream_t581172200::get_offset_of_m_Writeable_10(),
	NetworkStream_t581172200::get_offset_of_m_OwnsSocket_11(),
	NetworkStream_t581172200::get_offset_of_m_CloseTimeout_12(),
	NetworkStream_t581172200::get_offset_of_m_CleanedUp_13(),
	NetworkStream_t581172200::get_offset_of_m_CurrentReadTimeout_14(),
	NetworkStream_t581172200::get_offset_of_m_CurrentWriteTimeout_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (IPAddressCollection_t2986660307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[1] = 
{
	IPAddressCollection_t2986660307::get_offset_of_addresses_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (IPGlobalProperties_t430107897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (IPInterfaceProperties_t3986609851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (IPv4InterfaceStatistics_t3946458365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (NetworkInformationException_t1863186723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (NetworkInterface_t63927633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (OperationalStatus_t2833345236)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2286[8] = 
{
	OperationalStatus_t2833345236::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (NetworkInterfaceType_t4226883065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[29] = 
{
	NetworkInterfaceType_t4226883065::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (NetBiosNodeType_t2005148930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2288[6] = 
{
	NetBiosNodeType_t2005148930::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (OidGroup_t4038341371)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[12] = 
{
	OidGroup_t4038341371::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (Oid_t3221867120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[3] = 
{
	Oid_t3221867120::get_offset_of_m_value_0(),
	Oid_t3221867120::get_offset_of_m_friendlyName_1(),
	Oid_t3221867120::get_offset_of_m_group_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (OidCollection_t3790243618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[1] = 
{
	OidCollection_t3790243618::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (OidEnumerator_t3674631724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[2] = 
{
	OidEnumerator_t3674631724::get_offset_of_m_oids_0(),
	OidEnumerator_t3674631724::get_offset_of_m_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (X509Utils_t810863315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (InvariantComparer_t3322871449), -1, sizeof(InvariantComparer_t3322871449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2294[2] = 
{
	InvariantComparer_t3322871449::get_offset_of_m_compareInfo_0(),
	InvariantComparer_t3322871449_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (SecurityUtils_t1714011141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (WeakHashtable_t1679685894), -1, sizeof(WeakHashtable_t1679685894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2296[1] = 
{
	WeakHashtable_t1679685894_StaticFields::get_offset_of__comparer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (WeakKeyComparer_t3958653828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[16] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D98A44A6F8606AE6F23FE230286C1D6FBCC407226_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DBBBED0563515BBA6A562E6B8827C1074404A3689_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D7C2A9CB288780591A4DD199ED7FDF1C472107480_3(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DE8AA9C2F35723FDE82408FCF60820DFBB7EDCBE7_4(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DCCEEADA43268372341F81AE0C9208C6856441C04_5(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DC02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_6(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DE5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_7(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D83A5209E2DB06F1E59CB110E5C0456EA8D8D3C36_8(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DE7C90F0337516A86C6EB434D323DEFEE8204948C_9(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_10(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_11(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DEC5842B3154E1AF94500B57220EB9F684BCCC42A_12(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC_13(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2D6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E_14(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24fieldU2DEEAFE8C6E1AB017237567305EE925C976CDB6458_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (U24ArrayTypeU3D8_t1459944467)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t1459944467 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
