﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t4188277960  : public SystemException_t3877406272
{
public:
	// System.String System.Xml.XmlException::res
	String_t* ___res_16;
	// System.String[] System.Xml.XmlException::args
	StringU5BU5D_t1642385972* ___args_17;
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_18;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_19;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_20;
	// System.String System.Xml.XmlException::message
	String_t* ___message_21;

public:
	inline static int32_t get_offset_of_res_16() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___res_16)); }
	inline String_t* get_res_16() const { return ___res_16; }
	inline String_t** get_address_of_res_16() { return &___res_16; }
	inline void set_res_16(String_t* value)
	{
		___res_16 = value;
		Il2CppCodeGenWriteBarrier(&___res_16, value);
	}

	inline static int32_t get_offset_of_args_17() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___args_17)); }
	inline StringU5BU5D_t1642385972* get_args_17() const { return ___args_17; }
	inline StringU5BU5D_t1642385972** get_address_of_args_17() { return &___args_17; }
	inline void set_args_17(StringU5BU5D_t1642385972* value)
	{
		___args_17 = value;
		Il2CppCodeGenWriteBarrier(&___args_17, value);
	}

	inline static int32_t get_offset_of_lineNumber_18() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___lineNumber_18)); }
	inline int32_t get_lineNumber_18() const { return ___lineNumber_18; }
	inline int32_t* get_address_of_lineNumber_18() { return &___lineNumber_18; }
	inline void set_lineNumber_18(int32_t value)
	{
		___lineNumber_18 = value;
	}

	inline static int32_t get_offset_of_linePosition_19() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___linePosition_19)); }
	inline int32_t get_linePosition_19() const { return ___linePosition_19; }
	inline int32_t* get_address_of_linePosition_19() { return &___linePosition_19; }
	inline void set_linePosition_19(int32_t value)
	{
		___linePosition_19 = value;
	}

	inline static int32_t get_offset_of_sourceUri_20() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___sourceUri_20)); }
	inline String_t* get_sourceUri_20() const { return ___sourceUri_20; }
	inline String_t** get_address_of_sourceUri_20() { return &___sourceUri_20; }
	inline void set_sourceUri_20(String_t* value)
	{
		___sourceUri_20 = value;
		Il2CppCodeGenWriteBarrier(&___sourceUri_20, value);
	}

	inline static int32_t get_offset_of_message_21() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___message_21)); }
	inline String_t* get_message_21() const { return ___message_21; }
	inline String_t** get_address_of_message_21() { return &___message_21; }
	inline void set_message_21(String_t* value)
	{
		___message_21 = value;
		Il2CppCodeGenWriteBarrier(&___message_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
