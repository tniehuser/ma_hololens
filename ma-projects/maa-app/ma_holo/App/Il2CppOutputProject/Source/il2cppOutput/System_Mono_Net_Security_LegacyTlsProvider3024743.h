﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Security_Mono_Security_Interface_MonoTlsProvid823784021.h"
#include "mscorlib_System_Guid2533601593.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.LegacyTlsProvider
struct  LegacyTlsProvider_t3024743  : public MonoTlsProvider_t823784021
{
public:

public:
};

struct LegacyTlsProvider_t3024743_StaticFields
{
public:
	// System.Guid Mono.Net.Security.LegacyTlsProvider::id
	Guid_t  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LegacyTlsProvider_t3024743_StaticFields, ___id_0)); }
	inline Guid_t  get_id_0() const { return ___id_0; }
	inline Guid_t * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(Guid_t  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
