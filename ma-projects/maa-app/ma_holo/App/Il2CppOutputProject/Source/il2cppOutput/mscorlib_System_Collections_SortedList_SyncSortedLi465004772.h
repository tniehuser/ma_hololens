﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_SortedList3004938869.h"

// System.Collections.SortedList
struct SortedList_t3004938869;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.SortedList/SyncSortedList
struct  SyncSortedList_t465004772  : public SortedList_t3004938869
{
public:
	// System.Collections.SortedList System.Collections.SortedList/SyncSortedList::_list
	SortedList_t3004938869 * ____list_9;
	// System.Object System.Collections.SortedList/SyncSortedList::_root
	Il2CppObject * ____root_10;

public:
	inline static int32_t get_offset_of__list_9() { return static_cast<int32_t>(offsetof(SyncSortedList_t465004772, ____list_9)); }
	inline SortedList_t3004938869 * get__list_9() const { return ____list_9; }
	inline SortedList_t3004938869 ** get_address_of__list_9() { return &____list_9; }
	inline void set__list_9(SortedList_t3004938869 * value)
	{
		____list_9 = value;
		Il2CppCodeGenWriteBarrier(&____list_9, value);
	}

	inline static int32_t get_offset_of__root_10() { return static_cast<int32_t>(offsetof(SyncSortedList_t465004772, ____root_10)); }
	inline Il2CppObject * get__root_10() const { return ____root_10; }
	inline Il2CppObject ** get_address_of__root_10() { return &____root_10; }
	inline void set__root_10(Il2CppObject * value)
	{
		____root_10 = value;
		Il2CppCodeGenWriteBarrier(&____root_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
