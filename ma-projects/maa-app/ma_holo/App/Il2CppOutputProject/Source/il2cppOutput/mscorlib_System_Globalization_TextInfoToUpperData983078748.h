﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.TextInfoToUpperData
struct  TextInfoToUpperData_t983078748  : public Il2CppObject
{
public:

public:
};

struct TextInfoToUpperData_t983078748_StaticFields
{
public:
	// System.Char[] System.Globalization.TextInfoToUpperData::range_00e0_0586
	CharU5BU5D_t1328083999* ___range_00e0_0586_0;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_1e01_1ff3
	CharU5BU5D_t1328083999* ___range_1e01_1ff3_1;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_2170_2184
	CharU5BU5D_t1328083999* ___range_2170_2184_2;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_24d0_24e9
	CharU5BU5D_t1328083999* ___range_24d0_24e9_3;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_2c30_2ce3
	CharU5BU5D_t1328083999* ___range_2c30_2ce3_4;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_2d00_2d25
	CharU5BU5D_t1328083999* ___range_2d00_2d25_5;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_a641_a697
	CharU5BU5D_t1328083999* ___range_a641_a697_6;
	// System.Char[] System.Globalization.TextInfoToUpperData::range_a723_a78c
	CharU5BU5D_t1328083999* ___range_a723_a78c_7;

public:
	inline static int32_t get_offset_of_range_00e0_0586_0() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_00e0_0586_0)); }
	inline CharU5BU5D_t1328083999* get_range_00e0_0586_0() const { return ___range_00e0_0586_0; }
	inline CharU5BU5D_t1328083999** get_address_of_range_00e0_0586_0() { return &___range_00e0_0586_0; }
	inline void set_range_00e0_0586_0(CharU5BU5D_t1328083999* value)
	{
		___range_00e0_0586_0 = value;
		Il2CppCodeGenWriteBarrier(&___range_00e0_0586_0, value);
	}

	inline static int32_t get_offset_of_range_1e01_1ff3_1() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_1e01_1ff3_1)); }
	inline CharU5BU5D_t1328083999* get_range_1e01_1ff3_1() const { return ___range_1e01_1ff3_1; }
	inline CharU5BU5D_t1328083999** get_address_of_range_1e01_1ff3_1() { return &___range_1e01_1ff3_1; }
	inline void set_range_1e01_1ff3_1(CharU5BU5D_t1328083999* value)
	{
		___range_1e01_1ff3_1 = value;
		Il2CppCodeGenWriteBarrier(&___range_1e01_1ff3_1, value);
	}

	inline static int32_t get_offset_of_range_2170_2184_2() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_2170_2184_2)); }
	inline CharU5BU5D_t1328083999* get_range_2170_2184_2() const { return ___range_2170_2184_2; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2170_2184_2() { return &___range_2170_2184_2; }
	inline void set_range_2170_2184_2(CharU5BU5D_t1328083999* value)
	{
		___range_2170_2184_2 = value;
		Il2CppCodeGenWriteBarrier(&___range_2170_2184_2, value);
	}

	inline static int32_t get_offset_of_range_24d0_24e9_3() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_24d0_24e9_3)); }
	inline CharU5BU5D_t1328083999* get_range_24d0_24e9_3() const { return ___range_24d0_24e9_3; }
	inline CharU5BU5D_t1328083999** get_address_of_range_24d0_24e9_3() { return &___range_24d0_24e9_3; }
	inline void set_range_24d0_24e9_3(CharU5BU5D_t1328083999* value)
	{
		___range_24d0_24e9_3 = value;
		Il2CppCodeGenWriteBarrier(&___range_24d0_24e9_3, value);
	}

	inline static int32_t get_offset_of_range_2c30_2ce3_4() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_2c30_2ce3_4)); }
	inline CharU5BU5D_t1328083999* get_range_2c30_2ce3_4() const { return ___range_2c30_2ce3_4; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2c30_2ce3_4() { return &___range_2c30_2ce3_4; }
	inline void set_range_2c30_2ce3_4(CharU5BU5D_t1328083999* value)
	{
		___range_2c30_2ce3_4 = value;
		Il2CppCodeGenWriteBarrier(&___range_2c30_2ce3_4, value);
	}

	inline static int32_t get_offset_of_range_2d00_2d25_5() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_2d00_2d25_5)); }
	inline CharU5BU5D_t1328083999* get_range_2d00_2d25_5() const { return ___range_2d00_2d25_5; }
	inline CharU5BU5D_t1328083999** get_address_of_range_2d00_2d25_5() { return &___range_2d00_2d25_5; }
	inline void set_range_2d00_2d25_5(CharU5BU5D_t1328083999* value)
	{
		___range_2d00_2d25_5 = value;
		Il2CppCodeGenWriteBarrier(&___range_2d00_2d25_5, value);
	}

	inline static int32_t get_offset_of_range_a641_a697_6() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_a641_a697_6)); }
	inline CharU5BU5D_t1328083999* get_range_a641_a697_6() const { return ___range_a641_a697_6; }
	inline CharU5BU5D_t1328083999** get_address_of_range_a641_a697_6() { return &___range_a641_a697_6; }
	inline void set_range_a641_a697_6(CharU5BU5D_t1328083999* value)
	{
		___range_a641_a697_6 = value;
		Il2CppCodeGenWriteBarrier(&___range_a641_a697_6, value);
	}

	inline static int32_t get_offset_of_range_a723_a78c_7() { return static_cast<int32_t>(offsetof(TextInfoToUpperData_t983078748_StaticFields, ___range_a723_a78c_7)); }
	inline CharU5BU5D_t1328083999* get_range_a723_a78c_7() const { return ___range_a723_a78c_7; }
	inline CharU5BU5D_t1328083999** get_address_of_range_a723_a78c_7() { return &___range_a723_a78c_7; }
	inline void set_range_a723_a78c_7(CharU5BU5D_t1328083999* value)
	{
		___range_a723_a78c_7 = value;
		Il2CppCodeGenWriteBarrier(&___range_a723_a78c_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
