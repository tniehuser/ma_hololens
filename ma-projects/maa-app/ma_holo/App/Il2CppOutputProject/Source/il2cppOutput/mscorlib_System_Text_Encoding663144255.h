﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2022420531;
// System.Text.EncoderFallback
struct EncoderFallback_t1756452756;
// System.Text.DecoderFallback
struct DecoderFallback_t1715117820;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t663144255  : public Il2CppObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2022420531 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_11;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1756452756 * ___encoderFallback_12;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t1715117820 * ___decoderFallback_13;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___dataItem_10)); }
	inline CodePageDataItem_t2022420531 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2022420531 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2022420531 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier(&___dataItem_10, value);
	}

	inline static int32_t get_offset_of_m_isReadOnly_11() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_isReadOnly_11)); }
	inline bool get_m_isReadOnly_11() const { return ___m_isReadOnly_11; }
	inline bool* get_address_of_m_isReadOnly_11() { return &___m_isReadOnly_11; }
	inline void set_m_isReadOnly_11(bool value)
	{
		___m_isReadOnly_11 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_12() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoderFallback_12)); }
	inline EncoderFallback_t1756452756 * get_encoderFallback_12() const { return ___encoderFallback_12; }
	inline EncoderFallback_t1756452756 ** get_address_of_encoderFallback_12() { return &___encoderFallback_12; }
	inline void set_encoderFallback_12(EncoderFallback_t1756452756 * value)
	{
		___encoderFallback_12 = value;
		Il2CppCodeGenWriteBarrier(&___encoderFallback_12, value);
	}

	inline static int32_t get_offset_of_decoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___decoderFallback_13)); }
	inline DecoderFallback_t1715117820 * get_decoderFallback_13() const { return ___decoderFallback_13; }
	inline DecoderFallback_t1715117820 ** get_address_of_decoderFallback_13() { return &___decoderFallback_13; }
	inline void set_decoderFallback_13(DecoderFallback_t1715117820 * value)
	{
		___decoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier(&___decoderFallback_13, value);
	}
};

struct Encoding_t663144255_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t663144255 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t663144255 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t663144255 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t663144255 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t663144255 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t663144255 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t663144255 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t663144255 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t909839986 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	Il2CppObject * ___s_InternalSyncObject_14;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t663144255 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t663144255 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t663144255 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultEncoding_0, value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t663144255 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t663144255 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t663144255 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier(&___unicodeEncoding_1, value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t663144255 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t663144255 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t663144255 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier(&___bigEndianUnicode_2, value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t663144255 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t663144255 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t663144255 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier(&___utf7Encoding_3, value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t663144255 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t663144255 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t663144255 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier(&___utf8Encoding_4, value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t663144255 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t663144255 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t663144255 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier(&___utf32Encoding_5, value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t663144255 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t663144255 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t663144255 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier(&___asciiEncoding_6, value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t663144255 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t663144255 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t663144255 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier(&___latin1Encoding_7, value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___encodings_8)); }
	inline Hashtable_t909839986 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t909839986 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t909839986 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier(&___encodings_8, value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_14() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___s_InternalSyncObject_14)); }
	inline Il2CppObject * get_s_InternalSyncObject_14() const { return ___s_InternalSyncObject_14; }
	inline Il2CppObject ** get_address_of_s_InternalSyncObject_14() { return &___s_InternalSyncObject_14; }
	inline void set_s_InternalSyncObject_14(Il2CppObject * value)
	{
		___s_InternalSyncObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___s_InternalSyncObject_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
