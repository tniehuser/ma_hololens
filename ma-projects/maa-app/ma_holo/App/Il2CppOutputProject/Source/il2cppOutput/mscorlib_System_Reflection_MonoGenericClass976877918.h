﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_TypeInfo3822613806.h"

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoGenericClass
struct  MonoGenericClass_t976877918  : public TypeInfo_t3822613806
{
public:
	// System.Type System.Reflection.MonoGenericClass::generic_type
	Type_t * ___generic_type_8;
	// System.Type[] System.Reflection.MonoGenericClass::type_arguments
	TypeU5BU5D_t1664964607* ___type_arguments_9;
	// System.Boolean System.Reflection.MonoGenericClass::initialized
	bool ___initialized_10;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::fields
	Hashtable_t909839986 * ___fields_11;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::ctors
	Hashtable_t909839986 * ___ctors_12;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::methods
	Hashtable_t909839986 * ___methods_13;

public:
	inline static int32_t get_offset_of_generic_type_8() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___generic_type_8)); }
	inline Type_t * get_generic_type_8() const { return ___generic_type_8; }
	inline Type_t ** get_address_of_generic_type_8() { return &___generic_type_8; }
	inline void set_generic_type_8(Type_t * value)
	{
		___generic_type_8 = value;
		Il2CppCodeGenWriteBarrier(&___generic_type_8, value);
	}

	inline static int32_t get_offset_of_type_arguments_9() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___type_arguments_9)); }
	inline TypeU5BU5D_t1664964607* get_type_arguments_9() const { return ___type_arguments_9; }
	inline TypeU5BU5D_t1664964607** get_address_of_type_arguments_9() { return &___type_arguments_9; }
	inline void set_type_arguments_9(TypeU5BU5D_t1664964607* value)
	{
		___type_arguments_9 = value;
		Il2CppCodeGenWriteBarrier(&___type_arguments_9, value);
	}

	inline static int32_t get_offset_of_initialized_10() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___initialized_10)); }
	inline bool get_initialized_10() const { return ___initialized_10; }
	inline bool* get_address_of_initialized_10() { return &___initialized_10; }
	inline void set_initialized_10(bool value)
	{
		___initialized_10 = value;
	}

	inline static int32_t get_offset_of_fields_11() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___fields_11)); }
	inline Hashtable_t909839986 * get_fields_11() const { return ___fields_11; }
	inline Hashtable_t909839986 ** get_address_of_fields_11() { return &___fields_11; }
	inline void set_fields_11(Hashtable_t909839986 * value)
	{
		___fields_11 = value;
		Il2CppCodeGenWriteBarrier(&___fields_11, value);
	}

	inline static int32_t get_offset_of_ctors_12() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___ctors_12)); }
	inline Hashtable_t909839986 * get_ctors_12() const { return ___ctors_12; }
	inline Hashtable_t909839986 ** get_address_of_ctors_12() { return &___ctors_12; }
	inline void set_ctors_12(Hashtable_t909839986 * value)
	{
		___ctors_12 = value;
		Il2CppCodeGenWriteBarrier(&___ctors_12, value);
	}

	inline static int32_t get_offset_of_methods_13() { return static_cast<int32_t>(offsetof(MonoGenericClass_t976877918, ___methods_13)); }
	inline Hashtable_t909839986 * get_methods_13() const { return ___methods_13; }
	inline Hashtable_t909839986 ** get_address_of_methods_13() { return &___methods_13; }
	inline void set_methods_13(Hashtable_t909839986 * value)
	{
		___methods_13 = value;
		Il2CppCodeGenWriteBarrier(&___methods_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
