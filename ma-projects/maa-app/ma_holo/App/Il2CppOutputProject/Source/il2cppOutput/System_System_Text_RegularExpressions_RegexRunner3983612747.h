﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunner
struct  RegexRunner_t3983612747  : public Il2CppObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextbeg
	int32_t ___runtextbeg_0;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextend
	int32_t ___runtextend_1;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextstart
	int32_t ___runtextstart_2;
	// System.String System.Text.RegularExpressions.RegexRunner::runtext
	String_t* ___runtext_3;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextpos
	int32_t ___runtextpos_4;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runtrack
	Int32U5BU5D_t3030399641* ___runtrack_5;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackpos
	int32_t ___runtrackpos_6;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runstack
	Int32U5BU5D_t3030399641* ___runstack_7;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runstackpos
	int32_t ___runstackpos_8;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runcrawl
	Int32U5BU5D_t3030399641* ___runcrawl_9;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runcrawlpos
	int32_t ___runcrawlpos_10;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackcount
	int32_t ___runtrackcount_11;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::runmatch
	Match_t3164245899 * ___runmatch_12;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.RegexRunner::runregex
	Regex_t1803876613 * ___runregex_13;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeout
	int32_t ___timeout_14;
	// System.Boolean System.Text.RegularExpressions.RegexRunner::ignoreTimeout
	bool ___ignoreTimeout_15;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutOccursAt
	int32_t ___timeoutOccursAt_16;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutChecksToSkip
	int32_t ___timeoutChecksToSkip_17;

public:
	inline static int32_t get_offset_of_runtextbeg_0() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextbeg_0)); }
	inline int32_t get_runtextbeg_0() const { return ___runtextbeg_0; }
	inline int32_t* get_address_of_runtextbeg_0() { return &___runtextbeg_0; }
	inline void set_runtextbeg_0(int32_t value)
	{
		___runtextbeg_0 = value;
	}

	inline static int32_t get_offset_of_runtextend_1() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextend_1)); }
	inline int32_t get_runtextend_1() const { return ___runtextend_1; }
	inline int32_t* get_address_of_runtextend_1() { return &___runtextend_1; }
	inline void set_runtextend_1(int32_t value)
	{
		___runtextend_1 = value;
	}

	inline static int32_t get_offset_of_runtextstart_2() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextstart_2)); }
	inline int32_t get_runtextstart_2() const { return ___runtextstart_2; }
	inline int32_t* get_address_of_runtextstart_2() { return &___runtextstart_2; }
	inline void set_runtextstart_2(int32_t value)
	{
		___runtextstart_2 = value;
	}

	inline static int32_t get_offset_of_runtext_3() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtext_3)); }
	inline String_t* get_runtext_3() const { return ___runtext_3; }
	inline String_t** get_address_of_runtext_3() { return &___runtext_3; }
	inline void set_runtext_3(String_t* value)
	{
		___runtext_3 = value;
		Il2CppCodeGenWriteBarrier(&___runtext_3, value);
	}

	inline static int32_t get_offset_of_runtextpos_4() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtextpos_4)); }
	inline int32_t get_runtextpos_4() const { return ___runtextpos_4; }
	inline int32_t* get_address_of_runtextpos_4() { return &___runtextpos_4; }
	inline void set_runtextpos_4(int32_t value)
	{
		___runtextpos_4 = value;
	}

	inline static int32_t get_offset_of_runtrack_5() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrack_5)); }
	inline Int32U5BU5D_t3030399641* get_runtrack_5() const { return ___runtrack_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_runtrack_5() { return &___runtrack_5; }
	inline void set_runtrack_5(Int32U5BU5D_t3030399641* value)
	{
		___runtrack_5 = value;
		Il2CppCodeGenWriteBarrier(&___runtrack_5, value);
	}

	inline static int32_t get_offset_of_runtrackpos_6() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrackpos_6)); }
	inline int32_t get_runtrackpos_6() const { return ___runtrackpos_6; }
	inline int32_t* get_address_of_runtrackpos_6() { return &___runtrackpos_6; }
	inline void set_runtrackpos_6(int32_t value)
	{
		___runtrackpos_6 = value;
	}

	inline static int32_t get_offset_of_runstack_7() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runstack_7)); }
	inline Int32U5BU5D_t3030399641* get_runstack_7() const { return ___runstack_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_runstack_7() { return &___runstack_7; }
	inline void set_runstack_7(Int32U5BU5D_t3030399641* value)
	{
		___runstack_7 = value;
		Il2CppCodeGenWriteBarrier(&___runstack_7, value);
	}

	inline static int32_t get_offset_of_runstackpos_8() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runstackpos_8)); }
	inline int32_t get_runstackpos_8() const { return ___runstackpos_8; }
	inline int32_t* get_address_of_runstackpos_8() { return &___runstackpos_8; }
	inline void set_runstackpos_8(int32_t value)
	{
		___runstackpos_8 = value;
	}

	inline static int32_t get_offset_of_runcrawl_9() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runcrawl_9)); }
	inline Int32U5BU5D_t3030399641* get_runcrawl_9() const { return ___runcrawl_9; }
	inline Int32U5BU5D_t3030399641** get_address_of_runcrawl_9() { return &___runcrawl_9; }
	inline void set_runcrawl_9(Int32U5BU5D_t3030399641* value)
	{
		___runcrawl_9 = value;
		Il2CppCodeGenWriteBarrier(&___runcrawl_9, value);
	}

	inline static int32_t get_offset_of_runcrawlpos_10() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runcrawlpos_10)); }
	inline int32_t get_runcrawlpos_10() const { return ___runcrawlpos_10; }
	inline int32_t* get_address_of_runcrawlpos_10() { return &___runcrawlpos_10; }
	inline void set_runcrawlpos_10(int32_t value)
	{
		___runcrawlpos_10 = value;
	}

	inline static int32_t get_offset_of_runtrackcount_11() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runtrackcount_11)); }
	inline int32_t get_runtrackcount_11() const { return ___runtrackcount_11; }
	inline int32_t* get_address_of_runtrackcount_11() { return &___runtrackcount_11; }
	inline void set_runtrackcount_11(int32_t value)
	{
		___runtrackcount_11 = value;
	}

	inline static int32_t get_offset_of_runmatch_12() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runmatch_12)); }
	inline Match_t3164245899 * get_runmatch_12() const { return ___runmatch_12; }
	inline Match_t3164245899 ** get_address_of_runmatch_12() { return &___runmatch_12; }
	inline void set_runmatch_12(Match_t3164245899 * value)
	{
		___runmatch_12 = value;
		Il2CppCodeGenWriteBarrier(&___runmatch_12, value);
	}

	inline static int32_t get_offset_of_runregex_13() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___runregex_13)); }
	inline Regex_t1803876613 * get_runregex_13() const { return ___runregex_13; }
	inline Regex_t1803876613 ** get_address_of_runregex_13() { return &___runregex_13; }
	inline void set_runregex_13(Regex_t1803876613 * value)
	{
		___runregex_13 = value;
		Il2CppCodeGenWriteBarrier(&___runregex_13, value);
	}

	inline static int32_t get_offset_of_timeout_14() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeout_14)); }
	inline int32_t get_timeout_14() const { return ___timeout_14; }
	inline int32_t* get_address_of_timeout_14() { return &___timeout_14; }
	inline void set_timeout_14(int32_t value)
	{
		___timeout_14 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeout_15() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___ignoreTimeout_15)); }
	inline bool get_ignoreTimeout_15() const { return ___ignoreTimeout_15; }
	inline bool* get_address_of_ignoreTimeout_15() { return &___ignoreTimeout_15; }
	inline void set_ignoreTimeout_15(bool value)
	{
		___ignoreTimeout_15 = value;
	}

	inline static int32_t get_offset_of_timeoutOccursAt_16() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeoutOccursAt_16)); }
	inline int32_t get_timeoutOccursAt_16() const { return ___timeoutOccursAt_16; }
	inline int32_t* get_address_of_timeoutOccursAt_16() { return &___timeoutOccursAt_16; }
	inline void set_timeoutOccursAt_16(int32_t value)
	{
		___timeoutOccursAt_16 = value;
	}

	inline static int32_t get_offset_of_timeoutChecksToSkip_17() { return static_cast<int32_t>(offsetof(RegexRunner_t3983612747, ___timeoutChecksToSkip_17)); }
	inline int32_t get_timeoutChecksToSkip_17() const { return ___timeoutChecksToSkip_17; }
	inline int32_t* get_address_of_timeoutChecksToSkip_17() { return &___timeoutChecksToSkip_17; }
	inline void set_timeoutChecksToSkip_17(int32_t value)
	{
		___timeoutChecksToSkip_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
