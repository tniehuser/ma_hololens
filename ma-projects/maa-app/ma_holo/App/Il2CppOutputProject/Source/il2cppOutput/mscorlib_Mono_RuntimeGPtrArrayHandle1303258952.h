﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_Mono_RuntimeStructs_GPtrArray3128553612.h"

// Mono.RuntimeStructs/GPtrArray
struct GPtrArray_t3128553612;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeGPtrArrayHandle
struct  RuntimeGPtrArrayHandle_t1303258952 
{
public:
	// Mono.RuntimeStructs/GPtrArray* Mono.RuntimeGPtrArrayHandle::value
	GPtrArray_t3128553612 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeGPtrArrayHandle_t1303258952, ___value_0)); }
	inline GPtrArray_t3128553612 * get_value_0() const { return ___value_0; }
	inline GPtrArray_t3128553612 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(GPtrArray_t3128553612 * value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.RuntimeGPtrArrayHandle
struct RuntimeGPtrArrayHandle_t1303258952_marshaled_pinvoke
{
	GPtrArray_t3128553612 * ___value_0;
};
// Native definition for COM marshalling of Mono.RuntimeGPtrArrayHandle
struct RuntimeGPtrArrayHandle_t1303258952_marshaled_com
{
	GPtrArray_t3128553612 * ___value_0;
};
