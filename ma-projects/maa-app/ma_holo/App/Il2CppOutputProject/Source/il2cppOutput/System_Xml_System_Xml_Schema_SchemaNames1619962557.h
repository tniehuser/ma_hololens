﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.String
struct String_t;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t717347117;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNames
struct  SchemaNames_t1619962557  : public Il2CppObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.SchemaNames::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.String System.Xml.Schema.SchemaNames::NsDataType
	String_t* ___NsDataType_1;
	// System.String System.Xml.Schema.SchemaNames::NsDataTypeAlias
	String_t* ___NsDataTypeAlias_2;
	// System.String System.Xml.Schema.SchemaNames::NsDataTypeOld
	String_t* ___NsDataTypeOld_3;
	// System.String System.Xml.Schema.SchemaNames::NsXml
	String_t* ___NsXml_4;
	// System.String System.Xml.Schema.SchemaNames::NsXmlNs
	String_t* ___NsXmlNs_5;
	// System.String System.Xml.Schema.SchemaNames::NsXdr
	String_t* ___NsXdr_6;
	// System.String System.Xml.Schema.SchemaNames::NsXdrAlias
	String_t* ___NsXdrAlias_7;
	// System.String System.Xml.Schema.SchemaNames::NsXs
	String_t* ___NsXs_8;
	// System.String System.Xml.Schema.SchemaNames::NsXsi
	String_t* ___NsXsi_9;
	// System.String System.Xml.Schema.SchemaNames::XsiType
	String_t* ___XsiType_10;
	// System.String System.Xml.Schema.SchemaNames::XsiNil
	String_t* ___XsiNil_11;
	// System.String System.Xml.Schema.SchemaNames::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_12;
	// System.String System.Xml.Schema.SchemaNames::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_13;
	// System.String System.Xml.Schema.SchemaNames::XsdSchema
	String_t* ___XsdSchema_14;
	// System.String System.Xml.Schema.SchemaNames::XdrSchema
	String_t* ___XdrSchema_15;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnPCData
	XmlQualifiedName_t1944712516 * ___QnPCData_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXml
	XmlQualifiedName_t1944712516 * ___QnXml_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXmlNs
	XmlQualifiedName_t1944712516 * ___QnXmlNs_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtDt
	XmlQualifiedName_t1944712516 * ___QnDtDt_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXmlLang
	XmlQualifiedName_t1944712516 * ___QnXmlLang_20;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnName
	XmlQualifiedName_t1944712516 * ___QnName_21;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnType
	XmlQualifiedName_t1944712516 * ___QnType_22;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMaxOccurs
	XmlQualifiedName_t1944712516 * ___QnMaxOccurs_23;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMinOccurs
	XmlQualifiedName_t1944712516 * ___QnMinOccurs_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnInfinite
	XmlQualifiedName_t1944712516 * ___QnInfinite_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnModel
	XmlQualifiedName_t1944712516 * ___QnModel_26;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOpen
	XmlQualifiedName_t1944712516 * ___QnOpen_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnClosed
	XmlQualifiedName_t1944712516 * ___QnClosed_28;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnContent
	XmlQualifiedName_t1944712516 * ___QnContent_29;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMixed
	XmlQualifiedName_t1944712516 * ___QnMixed_30;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEmpty
	XmlQualifiedName_t1944712516 * ___QnEmpty_31;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEltOnly
	XmlQualifiedName_t1944712516 * ___QnEltOnly_32;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnTextOnly
	XmlQualifiedName_t1944712516 * ___QnTextOnly_33;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOrder
	XmlQualifiedName_t1944712516 * ___QnOrder_34;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSeq
	XmlQualifiedName_t1944712516 * ___QnSeq_35;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOne
	XmlQualifiedName_t1944712516 * ___QnOne_36;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMany
	XmlQualifiedName_t1944712516 * ___QnMany_37;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRequired
	XmlQualifiedName_t1944712516 * ___QnRequired_38;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnYes
	XmlQualifiedName_t1944712516 * ___QnYes_39;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNo
	XmlQualifiedName_t1944712516 * ___QnNo_40;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnString
	XmlQualifiedName_t1944712516 * ___QnString_41;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnID
	XmlQualifiedName_t1944712516 * ___QnID_42;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnIDRef
	XmlQualifiedName_t1944712516 * ___QnIDRef_43;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnIDRefs
	XmlQualifiedName_t1944712516 * ___QnIDRefs_44;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEntity
	XmlQualifiedName_t1944712516 * ___QnEntity_45;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEntities
	XmlQualifiedName_t1944712516 * ___QnEntities_46;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNmToken
	XmlQualifiedName_t1944712516 * ___QnNmToken_47;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNmTokens
	XmlQualifiedName_t1944712516 * ___QnNmTokens_48;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEnumeration
	XmlQualifiedName_t1944712516 * ___QnEnumeration_49;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDefault
	XmlQualifiedName_t1944712516 * ___QnDefault_50;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrSchema
	XmlQualifiedName_t1944712516 * ___QnXdrSchema_51;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrElementType
	XmlQualifiedName_t1944712516 * ___QnXdrElementType_52;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrElement
	XmlQualifiedName_t1944712516 * ___QnXdrElement_53;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrGroup
	XmlQualifiedName_t1944712516 * ___QnXdrGroup_54;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAttributeType
	XmlQualifiedName_t1944712516 * ___QnXdrAttributeType_55;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAttribute
	XmlQualifiedName_t1944712516 * ___QnXdrAttribute_56;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrDataType
	XmlQualifiedName_t1944712516 * ___QnXdrDataType_57;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrDescription
	XmlQualifiedName_t1944712516 * ___QnXdrDescription_58;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrExtends
	XmlQualifiedName_t1944712516 * ___QnXdrExtends_59;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAliasSchema
	XmlQualifiedName_t1944712516 * ___QnXdrAliasSchema_60;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtType
	XmlQualifiedName_t1944712516 * ___QnDtType_61;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtValues
	XmlQualifiedName_t1944712516 * ___QnDtValues_62;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMaxLength
	XmlQualifiedName_t1944712516 * ___QnDtMaxLength_63;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMinLength
	XmlQualifiedName_t1944712516 * ___QnDtMinLength_64;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMax
	XmlQualifiedName_t1944712516 * ___QnDtMax_65;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMin
	XmlQualifiedName_t1944712516 * ___QnDtMin_66;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMinExclusive
	XmlQualifiedName_t1944712516 * ___QnDtMinExclusive_67;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMaxExclusive
	XmlQualifiedName_t1944712516 * ___QnDtMaxExclusive_68;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnTargetNamespace
	XmlQualifiedName_t1944712516 * ___QnTargetNamespace_69;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnVersion
	XmlQualifiedName_t1944712516 * ___QnVersion_70;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFinalDefault
	XmlQualifiedName_t1944712516 * ___QnFinalDefault_71;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBlockDefault
	XmlQualifiedName_t1944712516 * ___QnBlockDefault_72;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFixed
	XmlQualifiedName_t1944712516 * ___QnFixed_73;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnAbstract
	XmlQualifiedName_t1944712516 * ___QnAbstract_74;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBlock
	XmlQualifiedName_t1944712516 * ___QnBlock_75;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSubstitutionGroup
	XmlQualifiedName_t1944712516 * ___QnSubstitutionGroup_76;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFinal
	XmlQualifiedName_t1944712516 * ___QnFinal_77;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNillable
	XmlQualifiedName_t1944712516 * ___QnNillable_78;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRef
	XmlQualifiedName_t1944712516 * ___QnRef_79;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBase
	XmlQualifiedName_t1944712516 * ___QnBase_80;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDerivedBy
	XmlQualifiedName_t1944712516 * ___QnDerivedBy_81;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNamespace
	XmlQualifiedName_t1944712516 * ___QnNamespace_82;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnProcessContents
	XmlQualifiedName_t1944712516 * ___QnProcessContents_83;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRefer
	XmlQualifiedName_t1944712516 * ___QnRefer_84;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnPublic
	XmlQualifiedName_t1944712516 * ___QnPublic_85;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSystem
	XmlQualifiedName_t1944712516 * ___QnSystem_86;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSchemaLocation
	XmlQualifiedName_t1944712516 * ___QnSchemaLocation_87;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnValue
	XmlQualifiedName_t1944712516 * ___QnValue_88;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnUse
	XmlQualifiedName_t1944712516 * ___QnUse_89;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnForm
	XmlQualifiedName_t1944712516 * ___QnForm_90;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnElementFormDefault
	XmlQualifiedName_t1944712516 * ___QnElementFormDefault_91;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnAttributeFormDefault
	XmlQualifiedName_t1944712516 * ___QnAttributeFormDefault_92;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnItemType
	XmlQualifiedName_t1944712516 * ___QnItemType_93;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMemberTypes
	XmlQualifiedName_t1944712516 * ___QnMemberTypes_94;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXPath
	XmlQualifiedName_t1944712516 * ___QnXPath_95;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSchema
	XmlQualifiedName_t1944712516 * ___QnXsdSchema_96;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnnotation
	XmlQualifiedName_t1944712516 * ___QnXsdAnnotation_97;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdInclude
	XmlQualifiedName_t1944712516 * ___QnXsdInclude_98;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdImport
	XmlQualifiedName_t1944712516 * ___QnXsdImport_99;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdElement
	XmlQualifiedName_t1944712516 * ___QnXsdElement_100;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAttribute
	XmlQualifiedName_t1944712516 * ___QnXsdAttribute_101;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAttributeGroup
	XmlQualifiedName_t1944712516 * ___QnXsdAttributeGroup_102;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnyAttribute
	XmlQualifiedName_t1944712516 * ___QnXsdAnyAttribute_103;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdGroup
	XmlQualifiedName_t1944712516 * ___QnXsdGroup_104;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAll
	XmlQualifiedName_t1944712516 * ___QnXsdAll_105;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdChoice
	XmlQualifiedName_t1944712516 * ___QnXsdChoice_106;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSequence
	XmlQualifiedName_t1944712516 * ___QnXsdSequence_107;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAny
	XmlQualifiedName_t1944712516 * ___QnXsdAny_108;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdNotation
	XmlQualifiedName_t1944712516 * ___QnXsdNotation_109;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSimpleType
	XmlQualifiedName_t1944712516 * ___QnXsdSimpleType_110;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdComplexType
	XmlQualifiedName_t1944712516 * ___QnXsdComplexType_111;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdUnique
	XmlQualifiedName_t1944712516 * ___QnXsdUnique_112;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdKey
	XmlQualifiedName_t1944712516 * ___QnXsdKey_113;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdKeyRef
	XmlQualifiedName_t1944712516 * ___QnXsdKeyRef_114;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSelector
	XmlQualifiedName_t1944712516 * ___QnXsdSelector_115;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdField
	XmlQualifiedName_t1944712516 * ___QnXsdField_116;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinExclusive
	XmlQualifiedName_t1944712516 * ___QnXsdMinExclusive_117;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinInclusive
	XmlQualifiedName_t1944712516 * ___QnXsdMinInclusive_118;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxInclusive
	XmlQualifiedName_t1944712516 * ___QnXsdMaxInclusive_119;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxExclusive
	XmlQualifiedName_t1944712516 * ___QnXsdMaxExclusive_120;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdTotalDigits
	XmlQualifiedName_t1944712516 * ___QnXsdTotalDigits_121;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdFractionDigits
	XmlQualifiedName_t1944712516 * ___QnXsdFractionDigits_122;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdLength
	XmlQualifiedName_t1944712516 * ___QnXsdLength_123;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinLength
	XmlQualifiedName_t1944712516 * ___QnXsdMinLength_124;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxLength
	XmlQualifiedName_t1944712516 * ___QnXsdMaxLength_125;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdEnumeration
	XmlQualifiedName_t1944712516 * ___QnXsdEnumeration_126;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdPattern
	XmlQualifiedName_t1944712516 * ___QnXsdPattern_127;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdDocumentation
	XmlQualifiedName_t1944712516 * ___QnXsdDocumentation_128;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAppinfo
	XmlQualifiedName_t1944712516 * ___QnXsdAppinfo_129;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSource
	XmlQualifiedName_t1944712516 * ___QnSource_130;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdComplexContent
	XmlQualifiedName_t1944712516 * ___QnXsdComplexContent_131;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSimpleContent
	XmlQualifiedName_t1944712516 * ___QnXsdSimpleContent_132;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdRestriction
	XmlQualifiedName_t1944712516 * ___QnXsdRestriction_133;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdExtension
	XmlQualifiedName_t1944712516 * ___QnXsdExtension_134;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdUnion
	XmlQualifiedName_t1944712516 * ___QnXsdUnion_135;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdList
	XmlQualifiedName_t1944712516 * ___QnXsdList_136;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdWhiteSpace
	XmlQualifiedName_t1944712516 * ___QnXsdWhiteSpace_137;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdRedefine
	XmlQualifiedName_t1944712516 * ___QnXsdRedefine_138;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnyType
	XmlQualifiedName_t1944712516 * ___QnXsdAnyType_139;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.SchemaNames::TokenToQName
	XmlQualifiedNameU5BU5D_t717347117* ___TokenToQName_140;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_0, value);
	}

	inline static int32_t get_offset_of_NsDataType_1() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsDataType_1)); }
	inline String_t* get_NsDataType_1() const { return ___NsDataType_1; }
	inline String_t** get_address_of_NsDataType_1() { return &___NsDataType_1; }
	inline void set_NsDataType_1(String_t* value)
	{
		___NsDataType_1 = value;
		Il2CppCodeGenWriteBarrier(&___NsDataType_1, value);
	}

	inline static int32_t get_offset_of_NsDataTypeAlias_2() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsDataTypeAlias_2)); }
	inline String_t* get_NsDataTypeAlias_2() const { return ___NsDataTypeAlias_2; }
	inline String_t** get_address_of_NsDataTypeAlias_2() { return &___NsDataTypeAlias_2; }
	inline void set_NsDataTypeAlias_2(String_t* value)
	{
		___NsDataTypeAlias_2 = value;
		Il2CppCodeGenWriteBarrier(&___NsDataTypeAlias_2, value);
	}

	inline static int32_t get_offset_of_NsDataTypeOld_3() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsDataTypeOld_3)); }
	inline String_t* get_NsDataTypeOld_3() const { return ___NsDataTypeOld_3; }
	inline String_t** get_address_of_NsDataTypeOld_3() { return &___NsDataTypeOld_3; }
	inline void set_NsDataTypeOld_3(String_t* value)
	{
		___NsDataTypeOld_3 = value;
		Il2CppCodeGenWriteBarrier(&___NsDataTypeOld_3, value);
	}

	inline static int32_t get_offset_of_NsXml_4() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXml_4)); }
	inline String_t* get_NsXml_4() const { return ___NsXml_4; }
	inline String_t** get_address_of_NsXml_4() { return &___NsXml_4; }
	inline void set_NsXml_4(String_t* value)
	{
		___NsXml_4 = value;
		Il2CppCodeGenWriteBarrier(&___NsXml_4, value);
	}

	inline static int32_t get_offset_of_NsXmlNs_5() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXmlNs_5)); }
	inline String_t* get_NsXmlNs_5() const { return ___NsXmlNs_5; }
	inline String_t** get_address_of_NsXmlNs_5() { return &___NsXmlNs_5; }
	inline void set_NsXmlNs_5(String_t* value)
	{
		___NsXmlNs_5 = value;
		Il2CppCodeGenWriteBarrier(&___NsXmlNs_5, value);
	}

	inline static int32_t get_offset_of_NsXdr_6() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXdr_6)); }
	inline String_t* get_NsXdr_6() const { return ___NsXdr_6; }
	inline String_t** get_address_of_NsXdr_6() { return &___NsXdr_6; }
	inline void set_NsXdr_6(String_t* value)
	{
		___NsXdr_6 = value;
		Il2CppCodeGenWriteBarrier(&___NsXdr_6, value);
	}

	inline static int32_t get_offset_of_NsXdrAlias_7() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXdrAlias_7)); }
	inline String_t* get_NsXdrAlias_7() const { return ___NsXdrAlias_7; }
	inline String_t** get_address_of_NsXdrAlias_7() { return &___NsXdrAlias_7; }
	inline void set_NsXdrAlias_7(String_t* value)
	{
		___NsXdrAlias_7 = value;
		Il2CppCodeGenWriteBarrier(&___NsXdrAlias_7, value);
	}

	inline static int32_t get_offset_of_NsXs_8() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXs_8)); }
	inline String_t* get_NsXs_8() const { return ___NsXs_8; }
	inline String_t** get_address_of_NsXs_8() { return &___NsXs_8; }
	inline void set_NsXs_8(String_t* value)
	{
		___NsXs_8 = value;
		Il2CppCodeGenWriteBarrier(&___NsXs_8, value);
	}

	inline static int32_t get_offset_of_NsXsi_9() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___NsXsi_9)); }
	inline String_t* get_NsXsi_9() const { return ___NsXsi_9; }
	inline String_t** get_address_of_NsXsi_9() { return &___NsXsi_9; }
	inline void set_NsXsi_9(String_t* value)
	{
		___NsXsi_9 = value;
		Il2CppCodeGenWriteBarrier(&___NsXsi_9, value);
	}

	inline static int32_t get_offset_of_XsiType_10() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XsiType_10)); }
	inline String_t* get_XsiType_10() const { return ___XsiType_10; }
	inline String_t** get_address_of_XsiType_10() { return &___XsiType_10; }
	inline void set_XsiType_10(String_t* value)
	{
		___XsiType_10 = value;
		Il2CppCodeGenWriteBarrier(&___XsiType_10, value);
	}

	inline static int32_t get_offset_of_XsiNil_11() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XsiNil_11)); }
	inline String_t* get_XsiNil_11() const { return ___XsiNil_11; }
	inline String_t** get_address_of_XsiNil_11() { return &___XsiNil_11; }
	inline void set_XsiNil_11(String_t* value)
	{
		___XsiNil_11 = value;
		Il2CppCodeGenWriteBarrier(&___XsiNil_11, value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_12() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XsiSchemaLocation_12)); }
	inline String_t* get_XsiSchemaLocation_12() const { return ___XsiSchemaLocation_12; }
	inline String_t** get_address_of_XsiSchemaLocation_12() { return &___XsiSchemaLocation_12; }
	inline void set_XsiSchemaLocation_12(String_t* value)
	{
		___XsiSchemaLocation_12 = value;
		Il2CppCodeGenWriteBarrier(&___XsiSchemaLocation_12, value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_13() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XsiNoNamespaceSchemaLocation_13)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_13() const { return ___XsiNoNamespaceSchemaLocation_13; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_13() { return &___XsiNoNamespaceSchemaLocation_13; }
	inline void set_XsiNoNamespaceSchemaLocation_13(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_13 = value;
		Il2CppCodeGenWriteBarrier(&___XsiNoNamespaceSchemaLocation_13, value);
	}

	inline static int32_t get_offset_of_XsdSchema_14() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XsdSchema_14)); }
	inline String_t* get_XsdSchema_14() const { return ___XsdSchema_14; }
	inline String_t** get_address_of_XsdSchema_14() { return &___XsdSchema_14; }
	inline void set_XsdSchema_14(String_t* value)
	{
		___XsdSchema_14 = value;
		Il2CppCodeGenWriteBarrier(&___XsdSchema_14, value);
	}

	inline static int32_t get_offset_of_XdrSchema_15() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___XdrSchema_15)); }
	inline String_t* get_XdrSchema_15() const { return ___XdrSchema_15; }
	inline String_t** get_address_of_XdrSchema_15() { return &___XdrSchema_15; }
	inline void set_XdrSchema_15(String_t* value)
	{
		___XdrSchema_15 = value;
		Il2CppCodeGenWriteBarrier(&___XdrSchema_15, value);
	}

	inline static int32_t get_offset_of_QnPCData_16() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnPCData_16)); }
	inline XmlQualifiedName_t1944712516 * get_QnPCData_16() const { return ___QnPCData_16; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnPCData_16() { return &___QnPCData_16; }
	inline void set_QnPCData_16(XmlQualifiedName_t1944712516 * value)
	{
		___QnPCData_16 = value;
		Il2CppCodeGenWriteBarrier(&___QnPCData_16, value);
	}

	inline static int32_t get_offset_of_QnXml_17() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXml_17)); }
	inline XmlQualifiedName_t1944712516 * get_QnXml_17() const { return ___QnXml_17; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXml_17() { return &___QnXml_17; }
	inline void set_QnXml_17(XmlQualifiedName_t1944712516 * value)
	{
		___QnXml_17 = value;
		Il2CppCodeGenWriteBarrier(&___QnXml_17, value);
	}

	inline static int32_t get_offset_of_QnXmlNs_18() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXmlNs_18)); }
	inline XmlQualifiedName_t1944712516 * get_QnXmlNs_18() const { return ___QnXmlNs_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXmlNs_18() { return &___QnXmlNs_18; }
	inline void set_QnXmlNs_18(XmlQualifiedName_t1944712516 * value)
	{
		___QnXmlNs_18 = value;
		Il2CppCodeGenWriteBarrier(&___QnXmlNs_18, value);
	}

	inline static int32_t get_offset_of_QnDtDt_19() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtDt_19)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtDt_19() const { return ___QnDtDt_19; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtDt_19() { return &___QnDtDt_19; }
	inline void set_QnDtDt_19(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtDt_19 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtDt_19, value);
	}

	inline static int32_t get_offset_of_QnXmlLang_20() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXmlLang_20)); }
	inline XmlQualifiedName_t1944712516 * get_QnXmlLang_20() const { return ___QnXmlLang_20; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXmlLang_20() { return &___QnXmlLang_20; }
	inline void set_QnXmlLang_20(XmlQualifiedName_t1944712516 * value)
	{
		___QnXmlLang_20 = value;
		Il2CppCodeGenWriteBarrier(&___QnXmlLang_20, value);
	}

	inline static int32_t get_offset_of_QnName_21() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnName_21)); }
	inline XmlQualifiedName_t1944712516 * get_QnName_21() const { return ___QnName_21; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnName_21() { return &___QnName_21; }
	inline void set_QnName_21(XmlQualifiedName_t1944712516 * value)
	{
		___QnName_21 = value;
		Il2CppCodeGenWriteBarrier(&___QnName_21, value);
	}

	inline static int32_t get_offset_of_QnType_22() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnType_22)); }
	inline XmlQualifiedName_t1944712516 * get_QnType_22() const { return ___QnType_22; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnType_22() { return &___QnType_22; }
	inline void set_QnType_22(XmlQualifiedName_t1944712516 * value)
	{
		___QnType_22 = value;
		Il2CppCodeGenWriteBarrier(&___QnType_22, value);
	}

	inline static int32_t get_offset_of_QnMaxOccurs_23() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnMaxOccurs_23)); }
	inline XmlQualifiedName_t1944712516 * get_QnMaxOccurs_23() const { return ___QnMaxOccurs_23; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnMaxOccurs_23() { return &___QnMaxOccurs_23; }
	inline void set_QnMaxOccurs_23(XmlQualifiedName_t1944712516 * value)
	{
		___QnMaxOccurs_23 = value;
		Il2CppCodeGenWriteBarrier(&___QnMaxOccurs_23, value);
	}

	inline static int32_t get_offset_of_QnMinOccurs_24() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnMinOccurs_24)); }
	inline XmlQualifiedName_t1944712516 * get_QnMinOccurs_24() const { return ___QnMinOccurs_24; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnMinOccurs_24() { return &___QnMinOccurs_24; }
	inline void set_QnMinOccurs_24(XmlQualifiedName_t1944712516 * value)
	{
		___QnMinOccurs_24 = value;
		Il2CppCodeGenWriteBarrier(&___QnMinOccurs_24, value);
	}

	inline static int32_t get_offset_of_QnInfinite_25() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnInfinite_25)); }
	inline XmlQualifiedName_t1944712516 * get_QnInfinite_25() const { return ___QnInfinite_25; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnInfinite_25() { return &___QnInfinite_25; }
	inline void set_QnInfinite_25(XmlQualifiedName_t1944712516 * value)
	{
		___QnInfinite_25 = value;
		Il2CppCodeGenWriteBarrier(&___QnInfinite_25, value);
	}

	inline static int32_t get_offset_of_QnModel_26() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnModel_26)); }
	inline XmlQualifiedName_t1944712516 * get_QnModel_26() const { return ___QnModel_26; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnModel_26() { return &___QnModel_26; }
	inline void set_QnModel_26(XmlQualifiedName_t1944712516 * value)
	{
		___QnModel_26 = value;
		Il2CppCodeGenWriteBarrier(&___QnModel_26, value);
	}

	inline static int32_t get_offset_of_QnOpen_27() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnOpen_27)); }
	inline XmlQualifiedName_t1944712516 * get_QnOpen_27() const { return ___QnOpen_27; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnOpen_27() { return &___QnOpen_27; }
	inline void set_QnOpen_27(XmlQualifiedName_t1944712516 * value)
	{
		___QnOpen_27 = value;
		Il2CppCodeGenWriteBarrier(&___QnOpen_27, value);
	}

	inline static int32_t get_offset_of_QnClosed_28() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnClosed_28)); }
	inline XmlQualifiedName_t1944712516 * get_QnClosed_28() const { return ___QnClosed_28; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnClosed_28() { return &___QnClosed_28; }
	inline void set_QnClosed_28(XmlQualifiedName_t1944712516 * value)
	{
		___QnClosed_28 = value;
		Il2CppCodeGenWriteBarrier(&___QnClosed_28, value);
	}

	inline static int32_t get_offset_of_QnContent_29() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnContent_29)); }
	inline XmlQualifiedName_t1944712516 * get_QnContent_29() const { return ___QnContent_29; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnContent_29() { return &___QnContent_29; }
	inline void set_QnContent_29(XmlQualifiedName_t1944712516 * value)
	{
		___QnContent_29 = value;
		Il2CppCodeGenWriteBarrier(&___QnContent_29, value);
	}

	inline static int32_t get_offset_of_QnMixed_30() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnMixed_30)); }
	inline XmlQualifiedName_t1944712516 * get_QnMixed_30() const { return ___QnMixed_30; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnMixed_30() { return &___QnMixed_30; }
	inline void set_QnMixed_30(XmlQualifiedName_t1944712516 * value)
	{
		___QnMixed_30 = value;
		Il2CppCodeGenWriteBarrier(&___QnMixed_30, value);
	}

	inline static int32_t get_offset_of_QnEmpty_31() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnEmpty_31)); }
	inline XmlQualifiedName_t1944712516 * get_QnEmpty_31() const { return ___QnEmpty_31; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnEmpty_31() { return &___QnEmpty_31; }
	inline void set_QnEmpty_31(XmlQualifiedName_t1944712516 * value)
	{
		___QnEmpty_31 = value;
		Il2CppCodeGenWriteBarrier(&___QnEmpty_31, value);
	}

	inline static int32_t get_offset_of_QnEltOnly_32() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnEltOnly_32)); }
	inline XmlQualifiedName_t1944712516 * get_QnEltOnly_32() const { return ___QnEltOnly_32; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnEltOnly_32() { return &___QnEltOnly_32; }
	inline void set_QnEltOnly_32(XmlQualifiedName_t1944712516 * value)
	{
		___QnEltOnly_32 = value;
		Il2CppCodeGenWriteBarrier(&___QnEltOnly_32, value);
	}

	inline static int32_t get_offset_of_QnTextOnly_33() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnTextOnly_33)); }
	inline XmlQualifiedName_t1944712516 * get_QnTextOnly_33() const { return ___QnTextOnly_33; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnTextOnly_33() { return &___QnTextOnly_33; }
	inline void set_QnTextOnly_33(XmlQualifiedName_t1944712516 * value)
	{
		___QnTextOnly_33 = value;
		Il2CppCodeGenWriteBarrier(&___QnTextOnly_33, value);
	}

	inline static int32_t get_offset_of_QnOrder_34() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnOrder_34)); }
	inline XmlQualifiedName_t1944712516 * get_QnOrder_34() const { return ___QnOrder_34; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnOrder_34() { return &___QnOrder_34; }
	inline void set_QnOrder_34(XmlQualifiedName_t1944712516 * value)
	{
		___QnOrder_34 = value;
		Il2CppCodeGenWriteBarrier(&___QnOrder_34, value);
	}

	inline static int32_t get_offset_of_QnSeq_35() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnSeq_35)); }
	inline XmlQualifiedName_t1944712516 * get_QnSeq_35() const { return ___QnSeq_35; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnSeq_35() { return &___QnSeq_35; }
	inline void set_QnSeq_35(XmlQualifiedName_t1944712516 * value)
	{
		___QnSeq_35 = value;
		Il2CppCodeGenWriteBarrier(&___QnSeq_35, value);
	}

	inline static int32_t get_offset_of_QnOne_36() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnOne_36)); }
	inline XmlQualifiedName_t1944712516 * get_QnOne_36() const { return ___QnOne_36; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnOne_36() { return &___QnOne_36; }
	inline void set_QnOne_36(XmlQualifiedName_t1944712516 * value)
	{
		___QnOne_36 = value;
		Il2CppCodeGenWriteBarrier(&___QnOne_36, value);
	}

	inline static int32_t get_offset_of_QnMany_37() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnMany_37)); }
	inline XmlQualifiedName_t1944712516 * get_QnMany_37() const { return ___QnMany_37; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnMany_37() { return &___QnMany_37; }
	inline void set_QnMany_37(XmlQualifiedName_t1944712516 * value)
	{
		___QnMany_37 = value;
		Il2CppCodeGenWriteBarrier(&___QnMany_37, value);
	}

	inline static int32_t get_offset_of_QnRequired_38() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnRequired_38)); }
	inline XmlQualifiedName_t1944712516 * get_QnRequired_38() const { return ___QnRequired_38; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnRequired_38() { return &___QnRequired_38; }
	inline void set_QnRequired_38(XmlQualifiedName_t1944712516 * value)
	{
		___QnRequired_38 = value;
		Il2CppCodeGenWriteBarrier(&___QnRequired_38, value);
	}

	inline static int32_t get_offset_of_QnYes_39() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnYes_39)); }
	inline XmlQualifiedName_t1944712516 * get_QnYes_39() const { return ___QnYes_39; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnYes_39() { return &___QnYes_39; }
	inline void set_QnYes_39(XmlQualifiedName_t1944712516 * value)
	{
		___QnYes_39 = value;
		Il2CppCodeGenWriteBarrier(&___QnYes_39, value);
	}

	inline static int32_t get_offset_of_QnNo_40() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnNo_40)); }
	inline XmlQualifiedName_t1944712516 * get_QnNo_40() const { return ___QnNo_40; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnNo_40() { return &___QnNo_40; }
	inline void set_QnNo_40(XmlQualifiedName_t1944712516 * value)
	{
		___QnNo_40 = value;
		Il2CppCodeGenWriteBarrier(&___QnNo_40, value);
	}

	inline static int32_t get_offset_of_QnString_41() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnString_41)); }
	inline XmlQualifiedName_t1944712516 * get_QnString_41() const { return ___QnString_41; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnString_41() { return &___QnString_41; }
	inline void set_QnString_41(XmlQualifiedName_t1944712516 * value)
	{
		___QnString_41 = value;
		Il2CppCodeGenWriteBarrier(&___QnString_41, value);
	}

	inline static int32_t get_offset_of_QnID_42() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnID_42)); }
	inline XmlQualifiedName_t1944712516 * get_QnID_42() const { return ___QnID_42; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnID_42() { return &___QnID_42; }
	inline void set_QnID_42(XmlQualifiedName_t1944712516 * value)
	{
		___QnID_42 = value;
		Il2CppCodeGenWriteBarrier(&___QnID_42, value);
	}

	inline static int32_t get_offset_of_QnIDRef_43() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnIDRef_43)); }
	inline XmlQualifiedName_t1944712516 * get_QnIDRef_43() const { return ___QnIDRef_43; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnIDRef_43() { return &___QnIDRef_43; }
	inline void set_QnIDRef_43(XmlQualifiedName_t1944712516 * value)
	{
		___QnIDRef_43 = value;
		Il2CppCodeGenWriteBarrier(&___QnIDRef_43, value);
	}

	inline static int32_t get_offset_of_QnIDRefs_44() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnIDRefs_44)); }
	inline XmlQualifiedName_t1944712516 * get_QnIDRefs_44() const { return ___QnIDRefs_44; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnIDRefs_44() { return &___QnIDRefs_44; }
	inline void set_QnIDRefs_44(XmlQualifiedName_t1944712516 * value)
	{
		___QnIDRefs_44 = value;
		Il2CppCodeGenWriteBarrier(&___QnIDRefs_44, value);
	}

	inline static int32_t get_offset_of_QnEntity_45() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnEntity_45)); }
	inline XmlQualifiedName_t1944712516 * get_QnEntity_45() const { return ___QnEntity_45; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnEntity_45() { return &___QnEntity_45; }
	inline void set_QnEntity_45(XmlQualifiedName_t1944712516 * value)
	{
		___QnEntity_45 = value;
		Il2CppCodeGenWriteBarrier(&___QnEntity_45, value);
	}

	inline static int32_t get_offset_of_QnEntities_46() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnEntities_46)); }
	inline XmlQualifiedName_t1944712516 * get_QnEntities_46() const { return ___QnEntities_46; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnEntities_46() { return &___QnEntities_46; }
	inline void set_QnEntities_46(XmlQualifiedName_t1944712516 * value)
	{
		___QnEntities_46 = value;
		Il2CppCodeGenWriteBarrier(&___QnEntities_46, value);
	}

	inline static int32_t get_offset_of_QnNmToken_47() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnNmToken_47)); }
	inline XmlQualifiedName_t1944712516 * get_QnNmToken_47() const { return ___QnNmToken_47; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnNmToken_47() { return &___QnNmToken_47; }
	inline void set_QnNmToken_47(XmlQualifiedName_t1944712516 * value)
	{
		___QnNmToken_47 = value;
		Il2CppCodeGenWriteBarrier(&___QnNmToken_47, value);
	}

	inline static int32_t get_offset_of_QnNmTokens_48() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnNmTokens_48)); }
	inline XmlQualifiedName_t1944712516 * get_QnNmTokens_48() const { return ___QnNmTokens_48; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnNmTokens_48() { return &___QnNmTokens_48; }
	inline void set_QnNmTokens_48(XmlQualifiedName_t1944712516 * value)
	{
		___QnNmTokens_48 = value;
		Il2CppCodeGenWriteBarrier(&___QnNmTokens_48, value);
	}

	inline static int32_t get_offset_of_QnEnumeration_49() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnEnumeration_49)); }
	inline XmlQualifiedName_t1944712516 * get_QnEnumeration_49() const { return ___QnEnumeration_49; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnEnumeration_49() { return &___QnEnumeration_49; }
	inline void set_QnEnumeration_49(XmlQualifiedName_t1944712516 * value)
	{
		___QnEnumeration_49 = value;
		Il2CppCodeGenWriteBarrier(&___QnEnumeration_49, value);
	}

	inline static int32_t get_offset_of_QnDefault_50() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDefault_50)); }
	inline XmlQualifiedName_t1944712516 * get_QnDefault_50() const { return ___QnDefault_50; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDefault_50() { return &___QnDefault_50; }
	inline void set_QnDefault_50(XmlQualifiedName_t1944712516 * value)
	{
		___QnDefault_50 = value;
		Il2CppCodeGenWriteBarrier(&___QnDefault_50, value);
	}

	inline static int32_t get_offset_of_QnXdrSchema_51() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrSchema_51)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrSchema_51() const { return ___QnXdrSchema_51; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrSchema_51() { return &___QnXdrSchema_51; }
	inline void set_QnXdrSchema_51(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrSchema_51 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrSchema_51, value);
	}

	inline static int32_t get_offset_of_QnXdrElementType_52() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrElementType_52)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrElementType_52() const { return ___QnXdrElementType_52; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrElementType_52() { return &___QnXdrElementType_52; }
	inline void set_QnXdrElementType_52(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrElementType_52 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrElementType_52, value);
	}

	inline static int32_t get_offset_of_QnXdrElement_53() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrElement_53)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrElement_53() const { return ___QnXdrElement_53; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrElement_53() { return &___QnXdrElement_53; }
	inline void set_QnXdrElement_53(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrElement_53 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrElement_53, value);
	}

	inline static int32_t get_offset_of_QnXdrGroup_54() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrGroup_54)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrGroup_54() const { return ___QnXdrGroup_54; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrGroup_54() { return &___QnXdrGroup_54; }
	inline void set_QnXdrGroup_54(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrGroup_54 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrGroup_54, value);
	}

	inline static int32_t get_offset_of_QnXdrAttributeType_55() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrAttributeType_55)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrAttributeType_55() const { return ___QnXdrAttributeType_55; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrAttributeType_55() { return &___QnXdrAttributeType_55; }
	inline void set_QnXdrAttributeType_55(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrAttributeType_55 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrAttributeType_55, value);
	}

	inline static int32_t get_offset_of_QnXdrAttribute_56() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrAttribute_56)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrAttribute_56() const { return ___QnXdrAttribute_56; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrAttribute_56() { return &___QnXdrAttribute_56; }
	inline void set_QnXdrAttribute_56(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrAttribute_56 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrAttribute_56, value);
	}

	inline static int32_t get_offset_of_QnXdrDataType_57() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrDataType_57)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrDataType_57() const { return ___QnXdrDataType_57; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrDataType_57() { return &___QnXdrDataType_57; }
	inline void set_QnXdrDataType_57(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrDataType_57 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrDataType_57, value);
	}

	inline static int32_t get_offset_of_QnXdrDescription_58() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrDescription_58)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrDescription_58() const { return ___QnXdrDescription_58; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrDescription_58() { return &___QnXdrDescription_58; }
	inline void set_QnXdrDescription_58(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrDescription_58 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrDescription_58, value);
	}

	inline static int32_t get_offset_of_QnXdrExtends_59() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrExtends_59)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrExtends_59() const { return ___QnXdrExtends_59; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrExtends_59() { return &___QnXdrExtends_59; }
	inline void set_QnXdrExtends_59(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrExtends_59 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrExtends_59, value);
	}

	inline static int32_t get_offset_of_QnXdrAliasSchema_60() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXdrAliasSchema_60)); }
	inline XmlQualifiedName_t1944712516 * get_QnXdrAliasSchema_60() const { return ___QnXdrAliasSchema_60; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXdrAliasSchema_60() { return &___QnXdrAliasSchema_60; }
	inline void set_QnXdrAliasSchema_60(XmlQualifiedName_t1944712516 * value)
	{
		___QnXdrAliasSchema_60 = value;
		Il2CppCodeGenWriteBarrier(&___QnXdrAliasSchema_60, value);
	}

	inline static int32_t get_offset_of_QnDtType_61() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtType_61)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtType_61() const { return ___QnDtType_61; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtType_61() { return &___QnDtType_61; }
	inline void set_QnDtType_61(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtType_61 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtType_61, value);
	}

	inline static int32_t get_offset_of_QnDtValues_62() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtValues_62)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtValues_62() const { return ___QnDtValues_62; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtValues_62() { return &___QnDtValues_62; }
	inline void set_QnDtValues_62(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtValues_62 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtValues_62, value);
	}

	inline static int32_t get_offset_of_QnDtMaxLength_63() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMaxLength_63)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMaxLength_63() const { return ___QnDtMaxLength_63; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMaxLength_63() { return &___QnDtMaxLength_63; }
	inline void set_QnDtMaxLength_63(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMaxLength_63 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMaxLength_63, value);
	}

	inline static int32_t get_offset_of_QnDtMinLength_64() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMinLength_64)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMinLength_64() const { return ___QnDtMinLength_64; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMinLength_64() { return &___QnDtMinLength_64; }
	inline void set_QnDtMinLength_64(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMinLength_64 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMinLength_64, value);
	}

	inline static int32_t get_offset_of_QnDtMax_65() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMax_65)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMax_65() const { return ___QnDtMax_65; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMax_65() { return &___QnDtMax_65; }
	inline void set_QnDtMax_65(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMax_65 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMax_65, value);
	}

	inline static int32_t get_offset_of_QnDtMin_66() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMin_66)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMin_66() const { return ___QnDtMin_66; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMin_66() { return &___QnDtMin_66; }
	inline void set_QnDtMin_66(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMin_66 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMin_66, value);
	}

	inline static int32_t get_offset_of_QnDtMinExclusive_67() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMinExclusive_67)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMinExclusive_67() const { return ___QnDtMinExclusive_67; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMinExclusive_67() { return &___QnDtMinExclusive_67; }
	inline void set_QnDtMinExclusive_67(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMinExclusive_67 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMinExclusive_67, value);
	}

	inline static int32_t get_offset_of_QnDtMaxExclusive_68() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDtMaxExclusive_68)); }
	inline XmlQualifiedName_t1944712516 * get_QnDtMaxExclusive_68() const { return ___QnDtMaxExclusive_68; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDtMaxExclusive_68() { return &___QnDtMaxExclusive_68; }
	inline void set_QnDtMaxExclusive_68(XmlQualifiedName_t1944712516 * value)
	{
		___QnDtMaxExclusive_68 = value;
		Il2CppCodeGenWriteBarrier(&___QnDtMaxExclusive_68, value);
	}

	inline static int32_t get_offset_of_QnTargetNamespace_69() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnTargetNamespace_69)); }
	inline XmlQualifiedName_t1944712516 * get_QnTargetNamespace_69() const { return ___QnTargetNamespace_69; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnTargetNamespace_69() { return &___QnTargetNamespace_69; }
	inline void set_QnTargetNamespace_69(XmlQualifiedName_t1944712516 * value)
	{
		___QnTargetNamespace_69 = value;
		Il2CppCodeGenWriteBarrier(&___QnTargetNamespace_69, value);
	}

	inline static int32_t get_offset_of_QnVersion_70() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnVersion_70)); }
	inline XmlQualifiedName_t1944712516 * get_QnVersion_70() const { return ___QnVersion_70; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnVersion_70() { return &___QnVersion_70; }
	inline void set_QnVersion_70(XmlQualifiedName_t1944712516 * value)
	{
		___QnVersion_70 = value;
		Il2CppCodeGenWriteBarrier(&___QnVersion_70, value);
	}

	inline static int32_t get_offset_of_QnFinalDefault_71() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnFinalDefault_71)); }
	inline XmlQualifiedName_t1944712516 * get_QnFinalDefault_71() const { return ___QnFinalDefault_71; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnFinalDefault_71() { return &___QnFinalDefault_71; }
	inline void set_QnFinalDefault_71(XmlQualifiedName_t1944712516 * value)
	{
		___QnFinalDefault_71 = value;
		Il2CppCodeGenWriteBarrier(&___QnFinalDefault_71, value);
	}

	inline static int32_t get_offset_of_QnBlockDefault_72() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnBlockDefault_72)); }
	inline XmlQualifiedName_t1944712516 * get_QnBlockDefault_72() const { return ___QnBlockDefault_72; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnBlockDefault_72() { return &___QnBlockDefault_72; }
	inline void set_QnBlockDefault_72(XmlQualifiedName_t1944712516 * value)
	{
		___QnBlockDefault_72 = value;
		Il2CppCodeGenWriteBarrier(&___QnBlockDefault_72, value);
	}

	inline static int32_t get_offset_of_QnFixed_73() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnFixed_73)); }
	inline XmlQualifiedName_t1944712516 * get_QnFixed_73() const { return ___QnFixed_73; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnFixed_73() { return &___QnFixed_73; }
	inline void set_QnFixed_73(XmlQualifiedName_t1944712516 * value)
	{
		___QnFixed_73 = value;
		Il2CppCodeGenWriteBarrier(&___QnFixed_73, value);
	}

	inline static int32_t get_offset_of_QnAbstract_74() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnAbstract_74)); }
	inline XmlQualifiedName_t1944712516 * get_QnAbstract_74() const { return ___QnAbstract_74; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAbstract_74() { return &___QnAbstract_74; }
	inline void set_QnAbstract_74(XmlQualifiedName_t1944712516 * value)
	{
		___QnAbstract_74 = value;
		Il2CppCodeGenWriteBarrier(&___QnAbstract_74, value);
	}

	inline static int32_t get_offset_of_QnBlock_75() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnBlock_75)); }
	inline XmlQualifiedName_t1944712516 * get_QnBlock_75() const { return ___QnBlock_75; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnBlock_75() { return &___QnBlock_75; }
	inline void set_QnBlock_75(XmlQualifiedName_t1944712516 * value)
	{
		___QnBlock_75 = value;
		Il2CppCodeGenWriteBarrier(&___QnBlock_75, value);
	}

	inline static int32_t get_offset_of_QnSubstitutionGroup_76() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnSubstitutionGroup_76)); }
	inline XmlQualifiedName_t1944712516 * get_QnSubstitutionGroup_76() const { return ___QnSubstitutionGroup_76; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnSubstitutionGroup_76() { return &___QnSubstitutionGroup_76; }
	inline void set_QnSubstitutionGroup_76(XmlQualifiedName_t1944712516 * value)
	{
		___QnSubstitutionGroup_76 = value;
		Il2CppCodeGenWriteBarrier(&___QnSubstitutionGroup_76, value);
	}

	inline static int32_t get_offset_of_QnFinal_77() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnFinal_77)); }
	inline XmlQualifiedName_t1944712516 * get_QnFinal_77() const { return ___QnFinal_77; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnFinal_77() { return &___QnFinal_77; }
	inline void set_QnFinal_77(XmlQualifiedName_t1944712516 * value)
	{
		___QnFinal_77 = value;
		Il2CppCodeGenWriteBarrier(&___QnFinal_77, value);
	}

	inline static int32_t get_offset_of_QnNillable_78() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnNillable_78)); }
	inline XmlQualifiedName_t1944712516 * get_QnNillable_78() const { return ___QnNillable_78; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnNillable_78() { return &___QnNillable_78; }
	inline void set_QnNillable_78(XmlQualifiedName_t1944712516 * value)
	{
		___QnNillable_78 = value;
		Il2CppCodeGenWriteBarrier(&___QnNillable_78, value);
	}

	inline static int32_t get_offset_of_QnRef_79() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnRef_79)); }
	inline XmlQualifiedName_t1944712516 * get_QnRef_79() const { return ___QnRef_79; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnRef_79() { return &___QnRef_79; }
	inline void set_QnRef_79(XmlQualifiedName_t1944712516 * value)
	{
		___QnRef_79 = value;
		Il2CppCodeGenWriteBarrier(&___QnRef_79, value);
	}

	inline static int32_t get_offset_of_QnBase_80() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnBase_80)); }
	inline XmlQualifiedName_t1944712516 * get_QnBase_80() const { return ___QnBase_80; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnBase_80() { return &___QnBase_80; }
	inline void set_QnBase_80(XmlQualifiedName_t1944712516 * value)
	{
		___QnBase_80 = value;
		Il2CppCodeGenWriteBarrier(&___QnBase_80, value);
	}

	inline static int32_t get_offset_of_QnDerivedBy_81() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnDerivedBy_81)); }
	inline XmlQualifiedName_t1944712516 * get_QnDerivedBy_81() const { return ___QnDerivedBy_81; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnDerivedBy_81() { return &___QnDerivedBy_81; }
	inline void set_QnDerivedBy_81(XmlQualifiedName_t1944712516 * value)
	{
		___QnDerivedBy_81 = value;
		Il2CppCodeGenWriteBarrier(&___QnDerivedBy_81, value);
	}

	inline static int32_t get_offset_of_QnNamespace_82() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnNamespace_82)); }
	inline XmlQualifiedName_t1944712516 * get_QnNamespace_82() const { return ___QnNamespace_82; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnNamespace_82() { return &___QnNamespace_82; }
	inline void set_QnNamespace_82(XmlQualifiedName_t1944712516 * value)
	{
		___QnNamespace_82 = value;
		Il2CppCodeGenWriteBarrier(&___QnNamespace_82, value);
	}

	inline static int32_t get_offset_of_QnProcessContents_83() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnProcessContents_83)); }
	inline XmlQualifiedName_t1944712516 * get_QnProcessContents_83() const { return ___QnProcessContents_83; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnProcessContents_83() { return &___QnProcessContents_83; }
	inline void set_QnProcessContents_83(XmlQualifiedName_t1944712516 * value)
	{
		___QnProcessContents_83 = value;
		Il2CppCodeGenWriteBarrier(&___QnProcessContents_83, value);
	}

	inline static int32_t get_offset_of_QnRefer_84() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnRefer_84)); }
	inline XmlQualifiedName_t1944712516 * get_QnRefer_84() const { return ___QnRefer_84; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnRefer_84() { return &___QnRefer_84; }
	inline void set_QnRefer_84(XmlQualifiedName_t1944712516 * value)
	{
		___QnRefer_84 = value;
		Il2CppCodeGenWriteBarrier(&___QnRefer_84, value);
	}

	inline static int32_t get_offset_of_QnPublic_85() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnPublic_85)); }
	inline XmlQualifiedName_t1944712516 * get_QnPublic_85() const { return ___QnPublic_85; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnPublic_85() { return &___QnPublic_85; }
	inline void set_QnPublic_85(XmlQualifiedName_t1944712516 * value)
	{
		___QnPublic_85 = value;
		Il2CppCodeGenWriteBarrier(&___QnPublic_85, value);
	}

	inline static int32_t get_offset_of_QnSystem_86() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnSystem_86)); }
	inline XmlQualifiedName_t1944712516 * get_QnSystem_86() const { return ___QnSystem_86; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnSystem_86() { return &___QnSystem_86; }
	inline void set_QnSystem_86(XmlQualifiedName_t1944712516 * value)
	{
		___QnSystem_86 = value;
		Il2CppCodeGenWriteBarrier(&___QnSystem_86, value);
	}

	inline static int32_t get_offset_of_QnSchemaLocation_87() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnSchemaLocation_87)); }
	inline XmlQualifiedName_t1944712516 * get_QnSchemaLocation_87() const { return ___QnSchemaLocation_87; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnSchemaLocation_87() { return &___QnSchemaLocation_87; }
	inline void set_QnSchemaLocation_87(XmlQualifiedName_t1944712516 * value)
	{
		___QnSchemaLocation_87 = value;
		Il2CppCodeGenWriteBarrier(&___QnSchemaLocation_87, value);
	}

	inline static int32_t get_offset_of_QnValue_88() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnValue_88)); }
	inline XmlQualifiedName_t1944712516 * get_QnValue_88() const { return ___QnValue_88; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnValue_88() { return &___QnValue_88; }
	inline void set_QnValue_88(XmlQualifiedName_t1944712516 * value)
	{
		___QnValue_88 = value;
		Il2CppCodeGenWriteBarrier(&___QnValue_88, value);
	}

	inline static int32_t get_offset_of_QnUse_89() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnUse_89)); }
	inline XmlQualifiedName_t1944712516 * get_QnUse_89() const { return ___QnUse_89; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnUse_89() { return &___QnUse_89; }
	inline void set_QnUse_89(XmlQualifiedName_t1944712516 * value)
	{
		___QnUse_89 = value;
		Il2CppCodeGenWriteBarrier(&___QnUse_89, value);
	}

	inline static int32_t get_offset_of_QnForm_90() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnForm_90)); }
	inline XmlQualifiedName_t1944712516 * get_QnForm_90() const { return ___QnForm_90; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnForm_90() { return &___QnForm_90; }
	inline void set_QnForm_90(XmlQualifiedName_t1944712516 * value)
	{
		___QnForm_90 = value;
		Il2CppCodeGenWriteBarrier(&___QnForm_90, value);
	}

	inline static int32_t get_offset_of_QnElementFormDefault_91() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnElementFormDefault_91)); }
	inline XmlQualifiedName_t1944712516 * get_QnElementFormDefault_91() const { return ___QnElementFormDefault_91; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnElementFormDefault_91() { return &___QnElementFormDefault_91; }
	inline void set_QnElementFormDefault_91(XmlQualifiedName_t1944712516 * value)
	{
		___QnElementFormDefault_91 = value;
		Il2CppCodeGenWriteBarrier(&___QnElementFormDefault_91, value);
	}

	inline static int32_t get_offset_of_QnAttributeFormDefault_92() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnAttributeFormDefault_92)); }
	inline XmlQualifiedName_t1944712516 * get_QnAttributeFormDefault_92() const { return ___QnAttributeFormDefault_92; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAttributeFormDefault_92() { return &___QnAttributeFormDefault_92; }
	inline void set_QnAttributeFormDefault_92(XmlQualifiedName_t1944712516 * value)
	{
		___QnAttributeFormDefault_92 = value;
		Il2CppCodeGenWriteBarrier(&___QnAttributeFormDefault_92, value);
	}

	inline static int32_t get_offset_of_QnItemType_93() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnItemType_93)); }
	inline XmlQualifiedName_t1944712516 * get_QnItemType_93() const { return ___QnItemType_93; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnItemType_93() { return &___QnItemType_93; }
	inline void set_QnItemType_93(XmlQualifiedName_t1944712516 * value)
	{
		___QnItemType_93 = value;
		Il2CppCodeGenWriteBarrier(&___QnItemType_93, value);
	}

	inline static int32_t get_offset_of_QnMemberTypes_94() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnMemberTypes_94)); }
	inline XmlQualifiedName_t1944712516 * get_QnMemberTypes_94() const { return ___QnMemberTypes_94; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnMemberTypes_94() { return &___QnMemberTypes_94; }
	inline void set_QnMemberTypes_94(XmlQualifiedName_t1944712516 * value)
	{
		___QnMemberTypes_94 = value;
		Il2CppCodeGenWriteBarrier(&___QnMemberTypes_94, value);
	}

	inline static int32_t get_offset_of_QnXPath_95() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXPath_95)); }
	inline XmlQualifiedName_t1944712516 * get_QnXPath_95() const { return ___QnXPath_95; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXPath_95() { return &___QnXPath_95; }
	inline void set_QnXPath_95(XmlQualifiedName_t1944712516 * value)
	{
		___QnXPath_95 = value;
		Il2CppCodeGenWriteBarrier(&___QnXPath_95, value);
	}

	inline static int32_t get_offset_of_QnXsdSchema_96() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdSchema_96)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdSchema_96() const { return ___QnXsdSchema_96; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdSchema_96() { return &___QnXsdSchema_96; }
	inline void set_QnXsdSchema_96(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdSchema_96 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdSchema_96, value);
	}

	inline static int32_t get_offset_of_QnXsdAnnotation_97() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAnnotation_97)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAnnotation_97() const { return ___QnXsdAnnotation_97; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAnnotation_97() { return &___QnXsdAnnotation_97; }
	inline void set_QnXsdAnnotation_97(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAnnotation_97 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAnnotation_97, value);
	}

	inline static int32_t get_offset_of_QnXsdInclude_98() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdInclude_98)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdInclude_98() const { return ___QnXsdInclude_98; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdInclude_98() { return &___QnXsdInclude_98; }
	inline void set_QnXsdInclude_98(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdInclude_98 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdInclude_98, value);
	}

	inline static int32_t get_offset_of_QnXsdImport_99() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdImport_99)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdImport_99() const { return ___QnXsdImport_99; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdImport_99() { return &___QnXsdImport_99; }
	inline void set_QnXsdImport_99(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdImport_99 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdImport_99, value);
	}

	inline static int32_t get_offset_of_QnXsdElement_100() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdElement_100)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdElement_100() const { return ___QnXsdElement_100; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdElement_100() { return &___QnXsdElement_100; }
	inline void set_QnXsdElement_100(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdElement_100 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdElement_100, value);
	}

	inline static int32_t get_offset_of_QnXsdAttribute_101() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAttribute_101)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAttribute_101() const { return ___QnXsdAttribute_101; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAttribute_101() { return &___QnXsdAttribute_101; }
	inline void set_QnXsdAttribute_101(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAttribute_101 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAttribute_101, value);
	}

	inline static int32_t get_offset_of_QnXsdAttributeGroup_102() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAttributeGroup_102)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAttributeGroup_102() const { return ___QnXsdAttributeGroup_102; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAttributeGroup_102() { return &___QnXsdAttributeGroup_102; }
	inline void set_QnXsdAttributeGroup_102(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAttributeGroup_102 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAttributeGroup_102, value);
	}

	inline static int32_t get_offset_of_QnXsdAnyAttribute_103() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAnyAttribute_103)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAnyAttribute_103() const { return ___QnXsdAnyAttribute_103; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAnyAttribute_103() { return &___QnXsdAnyAttribute_103; }
	inline void set_QnXsdAnyAttribute_103(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAnyAttribute_103 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAnyAttribute_103, value);
	}

	inline static int32_t get_offset_of_QnXsdGroup_104() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdGroup_104)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdGroup_104() const { return ___QnXsdGroup_104; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdGroup_104() { return &___QnXsdGroup_104; }
	inline void set_QnXsdGroup_104(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdGroup_104 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdGroup_104, value);
	}

	inline static int32_t get_offset_of_QnXsdAll_105() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAll_105)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAll_105() const { return ___QnXsdAll_105; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAll_105() { return &___QnXsdAll_105; }
	inline void set_QnXsdAll_105(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAll_105 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAll_105, value);
	}

	inline static int32_t get_offset_of_QnXsdChoice_106() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdChoice_106)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdChoice_106() const { return ___QnXsdChoice_106; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdChoice_106() { return &___QnXsdChoice_106; }
	inline void set_QnXsdChoice_106(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdChoice_106 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdChoice_106, value);
	}

	inline static int32_t get_offset_of_QnXsdSequence_107() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdSequence_107)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdSequence_107() const { return ___QnXsdSequence_107; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdSequence_107() { return &___QnXsdSequence_107; }
	inline void set_QnXsdSequence_107(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdSequence_107 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdSequence_107, value);
	}

	inline static int32_t get_offset_of_QnXsdAny_108() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAny_108)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAny_108() const { return ___QnXsdAny_108; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAny_108() { return &___QnXsdAny_108; }
	inline void set_QnXsdAny_108(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAny_108 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAny_108, value);
	}

	inline static int32_t get_offset_of_QnXsdNotation_109() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdNotation_109)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdNotation_109() const { return ___QnXsdNotation_109; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdNotation_109() { return &___QnXsdNotation_109; }
	inline void set_QnXsdNotation_109(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdNotation_109 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdNotation_109, value);
	}

	inline static int32_t get_offset_of_QnXsdSimpleType_110() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdSimpleType_110)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdSimpleType_110() const { return ___QnXsdSimpleType_110; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdSimpleType_110() { return &___QnXsdSimpleType_110; }
	inline void set_QnXsdSimpleType_110(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdSimpleType_110 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdSimpleType_110, value);
	}

	inline static int32_t get_offset_of_QnXsdComplexType_111() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdComplexType_111)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdComplexType_111() const { return ___QnXsdComplexType_111; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdComplexType_111() { return &___QnXsdComplexType_111; }
	inline void set_QnXsdComplexType_111(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdComplexType_111 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdComplexType_111, value);
	}

	inline static int32_t get_offset_of_QnXsdUnique_112() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdUnique_112)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdUnique_112() const { return ___QnXsdUnique_112; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdUnique_112() { return &___QnXsdUnique_112; }
	inline void set_QnXsdUnique_112(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdUnique_112 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdUnique_112, value);
	}

	inline static int32_t get_offset_of_QnXsdKey_113() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdKey_113)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdKey_113() const { return ___QnXsdKey_113; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdKey_113() { return &___QnXsdKey_113; }
	inline void set_QnXsdKey_113(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdKey_113 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdKey_113, value);
	}

	inline static int32_t get_offset_of_QnXsdKeyRef_114() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdKeyRef_114)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdKeyRef_114() const { return ___QnXsdKeyRef_114; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdKeyRef_114() { return &___QnXsdKeyRef_114; }
	inline void set_QnXsdKeyRef_114(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdKeyRef_114 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdKeyRef_114, value);
	}

	inline static int32_t get_offset_of_QnXsdSelector_115() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdSelector_115)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdSelector_115() const { return ___QnXsdSelector_115; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdSelector_115() { return &___QnXsdSelector_115; }
	inline void set_QnXsdSelector_115(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdSelector_115 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdSelector_115, value);
	}

	inline static int32_t get_offset_of_QnXsdField_116() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdField_116)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdField_116() const { return ___QnXsdField_116; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdField_116() { return &___QnXsdField_116; }
	inline void set_QnXsdField_116(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdField_116 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdField_116, value);
	}

	inline static int32_t get_offset_of_QnXsdMinExclusive_117() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMinExclusive_117)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMinExclusive_117() const { return ___QnXsdMinExclusive_117; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMinExclusive_117() { return &___QnXsdMinExclusive_117; }
	inline void set_QnXsdMinExclusive_117(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMinExclusive_117 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMinExclusive_117, value);
	}

	inline static int32_t get_offset_of_QnXsdMinInclusive_118() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMinInclusive_118)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMinInclusive_118() const { return ___QnXsdMinInclusive_118; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMinInclusive_118() { return &___QnXsdMinInclusive_118; }
	inline void set_QnXsdMinInclusive_118(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMinInclusive_118 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMinInclusive_118, value);
	}

	inline static int32_t get_offset_of_QnXsdMaxInclusive_119() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMaxInclusive_119)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMaxInclusive_119() const { return ___QnXsdMaxInclusive_119; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMaxInclusive_119() { return &___QnXsdMaxInclusive_119; }
	inline void set_QnXsdMaxInclusive_119(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMaxInclusive_119 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMaxInclusive_119, value);
	}

	inline static int32_t get_offset_of_QnXsdMaxExclusive_120() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMaxExclusive_120)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMaxExclusive_120() const { return ___QnXsdMaxExclusive_120; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMaxExclusive_120() { return &___QnXsdMaxExclusive_120; }
	inline void set_QnXsdMaxExclusive_120(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMaxExclusive_120 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMaxExclusive_120, value);
	}

	inline static int32_t get_offset_of_QnXsdTotalDigits_121() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdTotalDigits_121)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdTotalDigits_121() const { return ___QnXsdTotalDigits_121; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdTotalDigits_121() { return &___QnXsdTotalDigits_121; }
	inline void set_QnXsdTotalDigits_121(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdTotalDigits_121 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdTotalDigits_121, value);
	}

	inline static int32_t get_offset_of_QnXsdFractionDigits_122() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdFractionDigits_122)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdFractionDigits_122() const { return ___QnXsdFractionDigits_122; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdFractionDigits_122() { return &___QnXsdFractionDigits_122; }
	inline void set_QnXsdFractionDigits_122(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdFractionDigits_122 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdFractionDigits_122, value);
	}

	inline static int32_t get_offset_of_QnXsdLength_123() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdLength_123)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdLength_123() const { return ___QnXsdLength_123; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdLength_123() { return &___QnXsdLength_123; }
	inline void set_QnXsdLength_123(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdLength_123 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdLength_123, value);
	}

	inline static int32_t get_offset_of_QnXsdMinLength_124() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMinLength_124)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMinLength_124() const { return ___QnXsdMinLength_124; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMinLength_124() { return &___QnXsdMinLength_124; }
	inline void set_QnXsdMinLength_124(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMinLength_124 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMinLength_124, value);
	}

	inline static int32_t get_offset_of_QnXsdMaxLength_125() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdMaxLength_125)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdMaxLength_125() const { return ___QnXsdMaxLength_125; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdMaxLength_125() { return &___QnXsdMaxLength_125; }
	inline void set_QnXsdMaxLength_125(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdMaxLength_125 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdMaxLength_125, value);
	}

	inline static int32_t get_offset_of_QnXsdEnumeration_126() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdEnumeration_126)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdEnumeration_126() const { return ___QnXsdEnumeration_126; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdEnumeration_126() { return &___QnXsdEnumeration_126; }
	inline void set_QnXsdEnumeration_126(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdEnumeration_126 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdEnumeration_126, value);
	}

	inline static int32_t get_offset_of_QnXsdPattern_127() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdPattern_127)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdPattern_127() const { return ___QnXsdPattern_127; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdPattern_127() { return &___QnXsdPattern_127; }
	inline void set_QnXsdPattern_127(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdPattern_127 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdPattern_127, value);
	}

	inline static int32_t get_offset_of_QnXsdDocumentation_128() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdDocumentation_128)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdDocumentation_128() const { return ___QnXsdDocumentation_128; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdDocumentation_128() { return &___QnXsdDocumentation_128; }
	inline void set_QnXsdDocumentation_128(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdDocumentation_128 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdDocumentation_128, value);
	}

	inline static int32_t get_offset_of_QnXsdAppinfo_129() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAppinfo_129)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAppinfo_129() const { return ___QnXsdAppinfo_129; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAppinfo_129() { return &___QnXsdAppinfo_129; }
	inline void set_QnXsdAppinfo_129(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAppinfo_129 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAppinfo_129, value);
	}

	inline static int32_t get_offset_of_QnSource_130() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnSource_130)); }
	inline XmlQualifiedName_t1944712516 * get_QnSource_130() const { return ___QnSource_130; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnSource_130() { return &___QnSource_130; }
	inline void set_QnSource_130(XmlQualifiedName_t1944712516 * value)
	{
		___QnSource_130 = value;
		Il2CppCodeGenWriteBarrier(&___QnSource_130, value);
	}

	inline static int32_t get_offset_of_QnXsdComplexContent_131() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdComplexContent_131)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdComplexContent_131() const { return ___QnXsdComplexContent_131; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdComplexContent_131() { return &___QnXsdComplexContent_131; }
	inline void set_QnXsdComplexContent_131(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdComplexContent_131 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdComplexContent_131, value);
	}

	inline static int32_t get_offset_of_QnXsdSimpleContent_132() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdSimpleContent_132)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdSimpleContent_132() const { return ___QnXsdSimpleContent_132; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdSimpleContent_132() { return &___QnXsdSimpleContent_132; }
	inline void set_QnXsdSimpleContent_132(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdSimpleContent_132 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdSimpleContent_132, value);
	}

	inline static int32_t get_offset_of_QnXsdRestriction_133() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdRestriction_133)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdRestriction_133() const { return ___QnXsdRestriction_133; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdRestriction_133() { return &___QnXsdRestriction_133; }
	inline void set_QnXsdRestriction_133(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdRestriction_133 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdRestriction_133, value);
	}

	inline static int32_t get_offset_of_QnXsdExtension_134() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdExtension_134)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdExtension_134() const { return ___QnXsdExtension_134; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdExtension_134() { return &___QnXsdExtension_134; }
	inline void set_QnXsdExtension_134(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdExtension_134 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdExtension_134, value);
	}

	inline static int32_t get_offset_of_QnXsdUnion_135() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdUnion_135)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdUnion_135() const { return ___QnXsdUnion_135; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdUnion_135() { return &___QnXsdUnion_135; }
	inline void set_QnXsdUnion_135(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdUnion_135 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdUnion_135, value);
	}

	inline static int32_t get_offset_of_QnXsdList_136() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdList_136)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdList_136() const { return ___QnXsdList_136; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdList_136() { return &___QnXsdList_136; }
	inline void set_QnXsdList_136(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdList_136 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdList_136, value);
	}

	inline static int32_t get_offset_of_QnXsdWhiteSpace_137() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdWhiteSpace_137)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdWhiteSpace_137() const { return ___QnXsdWhiteSpace_137; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdWhiteSpace_137() { return &___QnXsdWhiteSpace_137; }
	inline void set_QnXsdWhiteSpace_137(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdWhiteSpace_137 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdWhiteSpace_137, value);
	}

	inline static int32_t get_offset_of_QnXsdRedefine_138() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdRedefine_138)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdRedefine_138() const { return ___QnXsdRedefine_138; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdRedefine_138() { return &___QnXsdRedefine_138; }
	inline void set_QnXsdRedefine_138(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdRedefine_138 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdRedefine_138, value);
	}

	inline static int32_t get_offset_of_QnXsdAnyType_139() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___QnXsdAnyType_139)); }
	inline XmlQualifiedName_t1944712516 * get_QnXsdAnyType_139() const { return ___QnXsdAnyType_139; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnXsdAnyType_139() { return &___QnXsdAnyType_139; }
	inline void set_QnXsdAnyType_139(XmlQualifiedName_t1944712516 * value)
	{
		___QnXsdAnyType_139 = value;
		Il2CppCodeGenWriteBarrier(&___QnXsdAnyType_139, value);
	}

	inline static int32_t get_offset_of_TokenToQName_140() { return static_cast<int32_t>(offsetof(SchemaNames_t1619962557, ___TokenToQName_140)); }
	inline XmlQualifiedNameU5BU5D_t717347117* get_TokenToQName_140() const { return ___TokenToQName_140; }
	inline XmlQualifiedNameU5BU5D_t717347117** get_address_of_TokenToQName_140() { return &___TokenToQName_140; }
	inline void set_TokenToQName_140(XmlQualifiedNameU5BU5D_t717347117* value)
	{
		___TokenToQName_140 = value;
		Il2CppCodeGenWriteBarrier(&___TokenToQName_140, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
