﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_NetworkInformation_NetworkInterfac63927633.h"
#include "System_System_Net_NetworkInformation_NetworkInterf4226883065.h"

// System.Net.NetworkInformation.IPInterfaceProperties
struct IPInterfaceProperties_t3986609851;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Net.IPAddress>
struct List_1_t769092855;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.UnixNetworkInterface
struct  UnixNetworkInterface_t1000704527  : public NetworkInterface_t63927633
{
public:
	// System.Net.NetworkInformation.IPInterfaceProperties System.Net.NetworkInformation.UnixNetworkInterface::ipproperties
	IPInterfaceProperties_t3986609851 * ___ipproperties_0;
	// System.String System.Net.NetworkInformation.UnixNetworkInterface::name
	String_t* ___name_1;
	// System.Collections.Generic.List`1<System.Net.IPAddress> System.Net.NetworkInformation.UnixNetworkInterface::addresses
	List_1_t769092855 * ___addresses_2;
	// System.Byte[] System.Net.NetworkInformation.UnixNetworkInterface::macAddress
	ByteU5BU5D_t3397334013* ___macAddress_3;
	// System.Net.NetworkInformation.NetworkInterfaceType System.Net.NetworkInformation.UnixNetworkInterface::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_ipproperties_0() { return static_cast<int32_t>(offsetof(UnixNetworkInterface_t1000704527, ___ipproperties_0)); }
	inline IPInterfaceProperties_t3986609851 * get_ipproperties_0() const { return ___ipproperties_0; }
	inline IPInterfaceProperties_t3986609851 ** get_address_of_ipproperties_0() { return &___ipproperties_0; }
	inline void set_ipproperties_0(IPInterfaceProperties_t3986609851 * value)
	{
		___ipproperties_0 = value;
		Il2CppCodeGenWriteBarrier(&___ipproperties_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(UnixNetworkInterface_t1000704527, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_addresses_2() { return static_cast<int32_t>(offsetof(UnixNetworkInterface_t1000704527, ___addresses_2)); }
	inline List_1_t769092855 * get_addresses_2() const { return ___addresses_2; }
	inline List_1_t769092855 ** get_address_of_addresses_2() { return &___addresses_2; }
	inline void set_addresses_2(List_1_t769092855 * value)
	{
		___addresses_2 = value;
		Il2CppCodeGenWriteBarrier(&___addresses_2, value);
	}

	inline static int32_t get_offset_of_macAddress_3() { return static_cast<int32_t>(offsetof(UnixNetworkInterface_t1000704527, ___macAddress_3)); }
	inline ByteU5BU5D_t3397334013* get_macAddress_3() const { return ___macAddress_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_macAddress_3() { return &___macAddress_3; }
	inline void set_macAddress_3(ByteU5BU5D_t3397334013* value)
	{
		___macAddress_3 = value;
		Il2CppCodeGenWriteBarrier(&___macAddress_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(UnixNetworkInterface_t1000704527, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
