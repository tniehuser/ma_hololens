﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Policy.Evidence
struct Evidence_t1407710183;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.SecurityException
struct  SecurityException_t887327375  : public SystemException_t3877406272
{
public:
	// System.String System.Security.SecurityException::permissionState
	String_t* ___permissionState_16;
	// System.Type System.Security.SecurityException::permissionType
	Type_t * ___permissionType_17;
	// System.String System.Security.SecurityException::_granted
	String_t* ____granted_18;
	// System.String System.Security.SecurityException::_refused
	String_t* ____refused_19;
	// System.Object System.Security.SecurityException::_demanded
	Il2CppObject * ____demanded_20;
	// System.Security.IPermission System.Security.SecurityException::_firstperm
	Il2CppObject * ____firstperm_21;
	// System.Reflection.MethodInfo System.Security.SecurityException::_method
	MethodInfo_t * ____method_22;
	// System.Security.Policy.Evidence System.Security.SecurityException::_evidence
	Evidence_t1407710183 * ____evidence_23;

public:
	inline static int32_t get_offset_of_permissionState_16() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ___permissionState_16)); }
	inline String_t* get_permissionState_16() const { return ___permissionState_16; }
	inline String_t** get_address_of_permissionState_16() { return &___permissionState_16; }
	inline void set_permissionState_16(String_t* value)
	{
		___permissionState_16 = value;
		Il2CppCodeGenWriteBarrier(&___permissionState_16, value);
	}

	inline static int32_t get_offset_of_permissionType_17() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ___permissionType_17)); }
	inline Type_t * get_permissionType_17() const { return ___permissionType_17; }
	inline Type_t ** get_address_of_permissionType_17() { return &___permissionType_17; }
	inline void set_permissionType_17(Type_t * value)
	{
		___permissionType_17 = value;
		Il2CppCodeGenWriteBarrier(&___permissionType_17, value);
	}

	inline static int32_t get_offset_of__granted_18() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____granted_18)); }
	inline String_t* get__granted_18() const { return ____granted_18; }
	inline String_t** get_address_of__granted_18() { return &____granted_18; }
	inline void set__granted_18(String_t* value)
	{
		____granted_18 = value;
		Il2CppCodeGenWriteBarrier(&____granted_18, value);
	}

	inline static int32_t get_offset_of__refused_19() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____refused_19)); }
	inline String_t* get__refused_19() const { return ____refused_19; }
	inline String_t** get_address_of__refused_19() { return &____refused_19; }
	inline void set__refused_19(String_t* value)
	{
		____refused_19 = value;
		Il2CppCodeGenWriteBarrier(&____refused_19, value);
	}

	inline static int32_t get_offset_of__demanded_20() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____demanded_20)); }
	inline Il2CppObject * get__demanded_20() const { return ____demanded_20; }
	inline Il2CppObject ** get_address_of__demanded_20() { return &____demanded_20; }
	inline void set__demanded_20(Il2CppObject * value)
	{
		____demanded_20 = value;
		Il2CppCodeGenWriteBarrier(&____demanded_20, value);
	}

	inline static int32_t get_offset_of__firstperm_21() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____firstperm_21)); }
	inline Il2CppObject * get__firstperm_21() const { return ____firstperm_21; }
	inline Il2CppObject ** get_address_of__firstperm_21() { return &____firstperm_21; }
	inline void set__firstperm_21(Il2CppObject * value)
	{
		____firstperm_21 = value;
		Il2CppCodeGenWriteBarrier(&____firstperm_21, value);
	}

	inline static int32_t get_offset_of__method_22() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____method_22)); }
	inline MethodInfo_t * get__method_22() const { return ____method_22; }
	inline MethodInfo_t ** get_address_of__method_22() { return &____method_22; }
	inline void set__method_22(MethodInfo_t * value)
	{
		____method_22 = value;
		Il2CppCodeGenWriteBarrier(&____method_22, value);
	}

	inline static int32_t get_offset_of__evidence_23() { return static_cast<int32_t>(offsetof(SecurityException_t887327375, ____evidence_23)); }
	inline Evidence_t1407710183 * get__evidence_23() const { return ____evidence_23; }
	inline Evidence_t1407710183 ** get_address_of__evidence_23() { return &____evidence_23; }
	inline void set__evidence_23(Evidence_t1407710183 * value)
	{
		____evidence_23 = value;
		Il2CppCodeGenWriteBarrier(&____evidence_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
