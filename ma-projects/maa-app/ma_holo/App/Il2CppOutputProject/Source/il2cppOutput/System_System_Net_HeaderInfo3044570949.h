﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.HeaderParser
struct HeaderParser_t3706119548;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderInfo
struct  HeaderInfo_t3044570949  : public Il2CppObject
{
public:
	// System.Boolean System.Net.HeaderInfo::IsRequestRestricted
	bool ___IsRequestRestricted_0;
	// System.Boolean System.Net.HeaderInfo::IsResponseRestricted
	bool ___IsResponseRestricted_1;
	// System.Net.HeaderParser System.Net.HeaderInfo::Parser
	HeaderParser_t3706119548 * ___Parser_2;
	// System.String System.Net.HeaderInfo::HeaderName
	String_t* ___HeaderName_3;
	// System.Boolean System.Net.HeaderInfo::AllowMultiValues
	bool ___AllowMultiValues_4;

public:
	inline static int32_t get_offset_of_IsRequestRestricted_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___IsRequestRestricted_0)); }
	inline bool get_IsRequestRestricted_0() const { return ___IsRequestRestricted_0; }
	inline bool* get_address_of_IsRequestRestricted_0() { return &___IsRequestRestricted_0; }
	inline void set_IsRequestRestricted_0(bool value)
	{
		___IsRequestRestricted_0 = value;
	}

	inline static int32_t get_offset_of_IsResponseRestricted_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___IsResponseRestricted_1)); }
	inline bool get_IsResponseRestricted_1() const { return ___IsResponseRestricted_1; }
	inline bool* get_address_of_IsResponseRestricted_1() { return &___IsResponseRestricted_1; }
	inline void set_IsResponseRestricted_1(bool value)
	{
		___IsResponseRestricted_1 = value;
	}

	inline static int32_t get_offset_of_Parser_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___Parser_2)); }
	inline HeaderParser_t3706119548 * get_Parser_2() const { return ___Parser_2; }
	inline HeaderParser_t3706119548 ** get_address_of_Parser_2() { return &___Parser_2; }
	inline void set_Parser_2(HeaderParser_t3706119548 * value)
	{
		___Parser_2 = value;
		Il2CppCodeGenWriteBarrier(&___Parser_2, value);
	}

	inline static int32_t get_offset_of_HeaderName_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___HeaderName_3)); }
	inline String_t* get_HeaderName_3() const { return ___HeaderName_3; }
	inline String_t** get_address_of_HeaderName_3() { return &___HeaderName_3; }
	inline void set_HeaderName_3(String_t* value)
	{
		___HeaderName_3 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderName_3, value);
	}

	inline static int32_t get_offset_of_AllowMultiValues_4() { return static_cast<int32_t>(offsetof(HeaderInfo_t3044570949, ___AllowMultiValues_4)); }
	inline bool get_AllowMultiValues_4() const { return ___AllowMultiValues_4; }
	inline bool* get_address_of_AllowMultiValues_4() { return &___AllowMultiValues_4; }
	inline void set_AllowMultiValues_4(bool value)
	{
		___AllowMultiValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
