﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_NullableConverter1941973167.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_PropertyDescriptorCol1540050832.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_RecommendedAsConfigura420947846.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_ReferenceConverter_Ref324026869.h"
#include "System_System_ComponentModel_ReflectTypeDescriptio1753777466.h"
#include "System_System_ComponentModel_ReflectTypeDescriptio2908979506.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224.h"
#include "System_System_ComponentModel_RefreshEventHandler456069287.h"
#include "System_System_ComponentModel_SByteConverter4003686519.h"
#include "System_System_ComponentModel_SingleConverter3693313828.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_TimeSpanConverter2149358279.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverter_Standard191679357.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3729801536.h"
#include "System_System_ComponentModel_TypeDescriptionProvide226398963.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_TypeDescriptor_FilterCa42357068.h"
#include "System_System_ComponentModel_TypeDescriptor_Member3553360933.h"
#include "System_System_ComponentModel_TypeDescriptor_Merged3445332928.h"
#include "System_System_ComponentModel_TypeDescriptor_TypeDe2784648617.h"
#include "System_System_ComponentModel_TypeDescriptor_TypeDe3612110989.h"
#include "System_System_ComponentModel_TypeDescriptor_TypeDe3334440910.h"
#include "System_System_ComponentModel_TypeDescriptor_TypeDe2250835238.h"
#include "System_System_ComponentModel_TypeDescriptor_TypeDe2613274749.h"
#include "System_System_ComponentModel_UInt16Converter1747783369.h"
#include "System_System_ComponentModel_UInt32Converter1748821239.h"
#include "System_System_ComponentModel_UInt64Converter977523578.h"
#include "System_System_Diagnostics_AssertSection2202838995.h"
#include "System_System_Diagnostics_BooleanSwitch1490001656.h"
#include "System_System_Diagnostics_DelimitedListTraceListen1075289718.h"
#include "System_System_Diagnostics_InitState1768812501.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration1565268762.h"
#include "System_System_Diagnostics_FilterElement959960876.h"
#include "System_System_Diagnostics_ListenerElementsCollecti1237474813.h"
#include "System_System_Diagnostics_SharedListenerElementsCo1354813622.h"
#include "System_System_Diagnostics_ListenerElement2632490878.h"
#include "System_System_Diagnostics_PerfCounterSection3986468662.h"
#include "System_System_Diagnostics_SourceElementsCollection1050626936.h"
#include "System_System_Diagnostics_SourceElement2880823865.h"
#include "System_System_Diagnostics_SourceLevels1530190938.h"
#include "System_System_Diagnostics_Switch2611057356.h"
#include "System_System_Diagnostics_SwitchElementsCollection835246741.h"
#include "System_System_Diagnostics_SwitchElement4125397718.h"
#include "System_System_Diagnostics_SwitchLevelAttribute1251804674.h"
#include "System_System_Diagnostics_SystemDiagnosticsSection2222033606.h"
#include "System_System_Diagnostics_TextWriterTraceListener3365973051.h"
#include "System_System_Diagnostics_TraceFilter87508953.h"
#include "System_System_Diagnostics_TraceInternal2818294232.h"
#include "System_System_Diagnostics_TraceListener3414949279.h"
#include "System_System_Diagnostics_TraceListenerCollection2289511703.h"
#include "System_System_Diagnostics_TraceOptions4183547961.h"
#include "System_System_Diagnostics_TraceSection1057220406.h"
#include "System_System_Diagnostics_TraceUtils1563802540.h"
#include "System_System_Diagnostics_TypedElement4034655484.h"
#include "System_System_Diagnostics_XmlWriterTraceListener1947709591.h"
#include "System_System_DomainNameHelper2237853587.h"
#include "System_System_IPv4AddressHelper3364954615.h"
#include "System_System_IPv6AddressHelper76851317.h"
#include "System_System_UncNameHelper1663961013.h"
#include "System_System_UriSyntaxFlags1242716474.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_UriParser_UriQuirksVersion4233729352.h"
#include "System_System_UriParser_BuiltInUriParser2634503859.h"
#include "System_System_IriHelper306005226.h"
#include "System_System_Uri19570940.h"
#include "System_System_Uri_Flags455382755.h"
#include "System_System_Uri_UriInfo4047916940.h"
#include "System_System_Uri_Offset266882373.h"
#include "System_System_Uri_MoreInfo2595315311.h"
#include "System_System_Uri_Check363272550.h"
#include "System_System_UriBuilder2016461725.h"
#include "System_System_UriKind1128731744.h"
#include "System_System_UriComponents3302767704.h"
#include "System_System_UriFormat2764505239.h"
#include "System_System_UriIdnScope761062207.h"
#include "System_System_ParsingError953959262.h"
#include "System_System_UnescapeMode584481035.h"
#include "System_System_UriFormatException3682083048.h"
#include "System_System_UriHelper2566857206.h"
#include "System_System_UriHostNameType2148127109.h"
#include "System_System_Net_EmptyWebProxy450353429.h"
#include "System_System_Net_HeaderParser3706119548.h"
#include "System_System_Net_HeaderInfo3044570949.h"
#include "System_System_Net_HeaderInfoTable2462863175.h"
#include "System_System_Net_CloseExState4185113936.h"
#include "System_System_Net_NetRes3820382307.h"
#include "System_System_Net_LazyAsyncResult698912221.h"
#include "System_System_Net_LazyAsyncResult_ThreadContext1626776243.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (MemberDescriptor_t3749827553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[11] = 
{
	MemberDescriptor_t3749827553::get_offset_of_name_0(),
	MemberDescriptor_t3749827553::get_offset_of_nameHash_1(),
	MemberDescriptor_t3749827553::get_offset_of_attributeCollection_2(),
	MemberDescriptor_t3749827553::get_offset_of_attributes_3(),
	MemberDescriptor_t3749827553::get_offset_of_originalAttributes_4(),
	MemberDescriptor_t3749827553::get_offset_of_attributesFiltered_5(),
	MemberDescriptor_t3749827553::get_offset_of_attributesFilled_6(),
	MemberDescriptor_t3749827553::get_offset_of_metadataVersion_7(),
	MemberDescriptor_t3749827553::get_offset_of_category_8(),
	MemberDescriptor_t3749827553::get_offset_of_description_9(),
	MemberDescriptor_t3749827553::get_offset_of_lockCookie_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (NullableConverter_t1941973167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[3] = 
{
	NullableConverter_t1941973167::get_offset_of_nullableType_4(),
	NullableConverter_t1941973167::get_offset_of_simpleType_5(),
	NullableConverter_t1941973167::get_offset_of_simpleTypeConverter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PropertyDescriptor_t4250402154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	PropertyDescriptor_t4250402154::get_offset_of_converter_11(),
	PropertyDescriptor_t4250402154::get_offset_of_editors_12(),
	PropertyDescriptor_t4250402154::get_offset_of_editorTypes_13(),
	PropertyDescriptor_t4250402154::get_offset_of_editorCount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (PropertyDescriptorCollection_t3166009492), -1, sizeof(PropertyDescriptorCollection_t3166009492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2104[10] = 
{
	PropertyDescriptorCollection_t3166009492_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_cachedFoundProperties_1(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_cachedIgnoreCase_2(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_properties_3(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_propCount_4(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_namedSort_5(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_comparer_6(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_propsOwned_7(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_needSort_8(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_readOnly_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (PropertyDescriptorEnumerator_t1540050832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[2] = 
{
	PropertyDescriptorEnumerator_t1540050832::get_offset_of_owner_0(),
	PropertyDescriptorEnumerator_t1540050832::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (ReadOnlyAttribute_t4102148880), -1, sizeof(ReadOnlyAttribute_t4102148880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	ReadOnlyAttribute_t4102148880::get_offset_of_isReadOnly_0(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Yes_1(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_No_2(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (RecommendedAsConfigurableAttribute_t420947846), -1, sizeof(RecommendedAsConfigurableAttribute_t420947846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[4] = 
{
	RecommendedAsConfigurableAttribute_t420947846::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_No_1(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Yes_2(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (ReferenceConverter_t3131270729), -1, sizeof(ReferenceConverter_t3131270729_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2108[2] = 
{
	ReferenceConverter_t3131270729_StaticFields::get_offset_of_none_4(),
	ReferenceConverter_t3131270729::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ReferenceComparer_t324026869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[1] = 
{
	ReferenceComparer_t324026869::get_offset_of_converter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (ReflectTypeDescriptionProvider_t1753777466), -1, sizeof(ReflectTypeDescriptionProvider_t1753777466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[12] = 
{
	ReflectTypeDescriptionProvider_t1753777466::get_offset_of__typeData_2(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__typeConstructor_3(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__intrinsicTypeConverters_4(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__intrinsicReferenceKey_5(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__intrinsicNullableKey_6(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__dictionaryKey_7(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__attributeCache_8(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__extenderProviderKey_9(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__extenderPropertiesKey_10(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__extenderProviderPropertiesKey_11(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__skipInterfaceAttributeList_12(),
	ReflectTypeDescriptionProvider_t1753777466_StaticFields::get_offset_of__internalSyncObject_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (ReflectedTypeData_t2908979506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[8] = 
{
	ReflectedTypeData_t2908979506::get_offset_of__type_0(),
	ReflectedTypeData_t2908979506::get_offset_of__attributes_1(),
	ReflectedTypeData_t2908979506::get_offset_of__events_2(),
	ReflectedTypeData_t2908979506::get_offset_of__properties_3(),
	ReflectedTypeData_t2908979506::get_offset_of__converter_4(),
	ReflectedTypeData_t2908979506::get_offset_of__editors_5(),
	ReflectedTypeData_t2908979506::get_offset_of__editorTypes_6(),
	ReflectedTypeData_t2908979506::get_offset_of__editorCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (RefreshEventArgs_t2077477224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[1] = 
{
	RefreshEventArgs_t2077477224::get_offset_of_typeChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (RefreshEventHandler_t456069287), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (SByteConverter_t4003686519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (SingleConverter_t3693313828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (TimeSpanConverter_t2149358279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TypeConverter_t745995970), -1, sizeof(TypeConverter_t745995970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[4] = 
{
	0,
	TypeConverter_t745995970_StaticFields::get_offset_of_useCompatibleTypeConversion_1(),
	TypeConverter_t745995970_StaticFields::get_offset_of_firstLoadAppSetting_2(),
	TypeConverter_t745995970_StaticFields::get_offset_of_loadAppSettingLock_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (StandardValuesCollection_t191679357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[2] = 
{
	StandardValuesCollection_t191679357::get_offset_of_values_0(),
	StandardValuesCollection_t191679357::get_offset_of_valueArray_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (TypeConverterAttribute_t252469870), -1, sizeof(TypeConverterAttribute_t252469870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[2] = 
{
	TypeConverterAttribute_t252469870::get_offset_of_typeName_0(),
	TypeConverterAttribute_t252469870_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (TypeDescriptionProvider_t2438624375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[2] = 
{
	TypeDescriptionProvider_t2438624375::get_offset_of__parent_0(),
	TypeDescriptionProvider_t2438624375::get_offset_of__emptyDescriptor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (EmptyCustomTypeDescriptor_t3729801536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (TypeDescriptionProviderAttribute_t226398963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	TypeDescriptionProviderAttribute_t226398963::get_offset_of__typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (TypeDescriptor_t3595688691), -1, sizeof(TypeDescriptor_t3595688691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2124[12] = 
{
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__providerTable_0(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__providerTypeTable_1(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__defaultProviders_2(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__metadataVersion_3(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__collisionIndex_4(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_TraceDescriptor_5(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__pipelineInitializeKeys_6(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__pipelineMergeKeys_7(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__pipelineFilterKeys_8(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__pipelineAttributeFilterKeys_9(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of__internalSyncObject_10(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_Refreshed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (FilterCacheItem_t42357068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	FilterCacheItem_t42357068::get_offset_of__filterService_0(),
	FilterCacheItem_t42357068::get_offset_of_FilteredMembers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (MemberDescriptorComparer_t3553360933), -1, sizeof(MemberDescriptorComparer_t3553360933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	MemberDescriptorComparer_t3553360933_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (MergedTypeDescriptor_t3445332928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	MergedTypeDescriptor_t3445332928::get_offset_of__primary_0(),
	MergedTypeDescriptor_t3445332928::get_offset_of__secondary_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (TypeDescriptionNode_t2784648617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	TypeDescriptionNode_t2784648617::get_offset_of_Next_2(),
	TypeDescriptionNode_t2784648617::get_offset_of_Provider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (DefaultExtendedTypeDescriptor_t3612110989)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[2] = 
{
	DefaultExtendedTypeDescriptor_t3612110989::get_offset_of__node_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DefaultExtendedTypeDescriptor_t3612110989::get_offset_of__instance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (DefaultTypeDescriptor_t3334440910)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[3] = 
{
	DefaultTypeDescriptor_t3334440910::get_offset_of__node_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DefaultTypeDescriptor_t3334440910::get_offset_of__objectType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DefaultTypeDescriptor_t3334440910::get_offset_of__instance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (TypeDescriptorComObject_t2250835238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (TypeDescriptorInterface_t2613274749), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (UInt16Converter_t1747783369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (UInt32Converter_t1748821239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (UInt64Converter_t977523578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (AssertSection_t2202838995), -1, sizeof(AssertSection_t2202838995_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[3] = 
{
	AssertSection_t2202838995_StaticFields::get_offset_of__properties_15(),
	AssertSection_t2202838995_StaticFields::get_offset_of__propAssertUIEnabled_16(),
	AssertSection_t2202838995_StaticFields::get_offset_of__propLogFile_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (BooleanSwitch_t1490001656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (DelimitedListTraceListener_t1075289718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (InitState_t1768812501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2140[4] = 
{
	InitState_t1768812501::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (DiagnosticsConfiguration_t1565268762), -1, sizeof(DiagnosticsConfiguration_t1565268762_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2141[2] = 
{
	DiagnosticsConfiguration_t1565268762_StaticFields::get_offset_of_configSection_0(),
	DiagnosticsConfiguration_t1565268762_StaticFields::get_offset_of_initState_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (FilterElement_t959960876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (ListenerElementsCollection_t1237474813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (SharedListenerElementsCollection_t1354813622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (ListenerElement_t2632490878), -1, sizeof(ListenerElement_t2632490878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[7] = 
{
	ListenerElement_t2632490878_StaticFields::get_offset_of__propFilter_20(),
	ListenerElement_t2632490878_StaticFields::get_offset_of__propName_21(),
	ListenerElement_t2632490878_StaticFields::get_offset_of__propOutputOpts_22(),
	ListenerElement_t2632490878::get_offset_of__propListenerTypeName_23(),
	ListenerElement_t2632490878::get_offset_of__allowReferences_24(),
	ListenerElement_t2632490878::get_offset_of__attributes_25(),
	ListenerElement_t2632490878::get_offset_of__isAddedByDefault_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (PerfCounterSection_t3986468662), -1, sizeof(PerfCounterSection_t3986468662_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2146[2] = 
{
	PerfCounterSection_t3986468662_StaticFields::get_offset_of__properties_15(),
	PerfCounterSection_t3986468662_StaticFields::get_offset_of__propFileMappingSize_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (SourceElementsCollection_t1050626936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (SourceElement_t2880823865), -1, sizeof(SourceElement_t2880823865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[7] = 
{
	SourceElement_t2880823865_StaticFields::get_offset_of__properties_15(),
	SourceElement_t2880823865_StaticFields::get_offset_of__propName_16(),
	SourceElement_t2880823865_StaticFields::get_offset_of__propSwitchName_17(),
	SourceElement_t2880823865_StaticFields::get_offset_of__propSwitchValue_18(),
	SourceElement_t2880823865_StaticFields::get_offset_of__propSwitchType_19(),
	SourceElement_t2880823865_StaticFields::get_offset_of__propListeners_20(),
	SourceElement_t2880823865::get_offset_of__attributes_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (SourceLevels_t1530190938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[9] = 
{
	SourceLevels_t1530190938::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (Switch_t2611057356), -1, sizeof(Switch_t2611057356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2150[6] = 
{
	Switch_t2611057356::get_offset_of_description_0(),
	Switch_t2611057356::get_offset_of_displayName_1(),
	Switch_t2611057356::get_offset_of_switchValueString_2(),
	Switch_t2611057356::get_offset_of_defaultValue_3(),
	Switch_t2611057356_StaticFields::get_offset_of_switches_4(),
	Switch_t2611057356_StaticFields::get_offset_of_s_LastCollectionCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (SwitchElementsCollection_t835246741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (SwitchElement_t4125397718), -1, sizeof(SwitchElement_t4125397718_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2152[4] = 
{
	SwitchElement_t4125397718_StaticFields::get_offset_of__properties_15(),
	SwitchElement_t4125397718_StaticFields::get_offset_of__propName_16(),
	SwitchElement_t4125397718_StaticFields::get_offset_of__propValue_17(),
	SwitchElement_t4125397718::get_offset_of__attributes_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (SwitchLevelAttribute_t1251804674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	SwitchLevelAttribute_t1251804674::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (SystemDiagnosticsSection_t2222033606), -1, sizeof(SystemDiagnosticsSection_t2222033606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[7] = 
{
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__properties_19(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propAssert_20(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propPerfCounters_21(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propSources_22(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propSharedListeners_23(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propSwitches_24(),
	SystemDiagnosticsSection_t2222033606_StaticFields::get_offset_of__propTrace_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (TextWriterTraceListener_t3365973051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[1] = 
{
	TextWriterTraceListener_t3365973051::get_offset_of_writer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (TraceFilter_t87508953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	TraceFilter_t87508953::get_offset_of_initializeData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (TraceInternal_t2818294232), -1, sizeof(TraceInternal_t2818294232_StaticFields), sizeof(TraceInternal_t2818294232_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2157[9] = 
{
	TraceInternal_t2818294232_StaticFields::get_offset_of_appName_0(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_listeners_1(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_autoFlush_2(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_useGlobalLock_3(),
	THREAD_STATIC_FIELD_OFFSET,
	TraceInternal_t2818294232_StaticFields::get_offset_of_indentSize_5(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_settingsInitialized_6(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_defaultInitialized_7(),
	TraceInternal_t2818294232_StaticFields::get_offset_of_critSec_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (TraceListener_t3414949279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[8] = 
{
	TraceListener_t3414949279::get_offset_of_indentLevel_1(),
	TraceListener_t3414949279::get_offset_of_indentSize_2(),
	TraceListener_t3414949279::get_offset_of_traceOptions_3(),
	TraceListener_t3414949279::get_offset_of_needIndent_4(),
	TraceListener_t3414949279::get_offset_of_listenerName_5(),
	TraceListener_t3414949279::get_offset_of_filter_6(),
	TraceListener_t3414949279::get_offset_of_attributes_7(),
	TraceListener_t3414949279::get_offset_of_initializeData_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (TraceListenerCollection_t2289511703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[1] = 
{
	TraceListenerCollection_t2289511703::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (TraceOptions_t4183547961)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[8] = 
{
	TraceOptions_t4183547961::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (TraceSection_t1057220406), -1, sizeof(TraceSection_t1057220406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[5] = 
{
	TraceSection_t1057220406_StaticFields::get_offset_of__properties_15(),
	TraceSection_t1057220406_StaticFields::get_offset_of__propListeners_16(),
	TraceSection_t1057220406_StaticFields::get_offset_of__propAutoFlush_17(),
	TraceSection_t1057220406_StaticFields::get_offset_of__propIndentSize_18(),
	TraceSection_t1057220406_StaticFields::get_offset_of__propUseGlobalLock_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (TraceUtils_t1563802540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (TypedElement_t4034655484), -1, sizeof(TypedElement_t4034655484_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2163[5] = 
{
	TypedElement_t4034655484_StaticFields::get_offset_of__propTypeName_15(),
	TypedElement_t4034655484_StaticFields::get_offset_of__propInitData_16(),
	TypedElement_t4034655484::get_offset_of__properties_17(),
	TypedElement_t4034655484::get_offset_of__runtimeObject_18(),
	TypedElement_t4034655484::get_offset_of__baseType_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (XmlWriterTraceListener_t1947709591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[3] = 
{
	XmlWriterTraceListener_t1947709591::get_offset_of_strBldr_10(),
	XmlWriterTraceListener_t1947709591::get_offset_of_xmlBlobWriter_11(),
	XmlWriterTraceListener_t1947709591::get_offset_of_shouldRespectFilterOnTraceTransfer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (DomainNameHelper_t2237853587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (IPv4AddressHelper_t3364954615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (IPv6AddressHelper_t76851317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (UncNameHelper_t1663961013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (UriSyntaxFlags_t1242716474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2169[30] = 
{
	UriSyntaxFlags_t1242716474::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (UriParser_t1012511323), -1, sizeof(UriParser_t1012511323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2170[26] = 
{
	UriParser_t1012511323_StaticFields::get_offset_of_m_Table_0(),
	UriParser_t1012511323_StaticFields::get_offset_of_m_TempTable_1(),
	UriParser_t1012511323::get_offset_of_m_Flags_2(),
	UriParser_t1012511323::get_offset_of_m_UpdatableFlags_3(),
	UriParser_t1012511323::get_offset_of_m_UpdatableFlagsUsed_4(),
	UriParser_t1012511323::get_offset_of_m_Port_5(),
	UriParser_t1012511323::get_offset_of_m_Scheme_6(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpUri_7(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpsUri_8(),
	UriParser_t1012511323_StaticFields::get_offset_of_WsUri_9(),
	UriParser_t1012511323_StaticFields::get_offset_of_WssUri_10(),
	UriParser_t1012511323_StaticFields::get_offset_of_FtpUri_11(),
	UriParser_t1012511323_StaticFields::get_offset_of_FileUri_12(),
	UriParser_t1012511323_StaticFields::get_offset_of_GopherUri_13(),
	UriParser_t1012511323_StaticFields::get_offset_of_NntpUri_14(),
	UriParser_t1012511323_StaticFields::get_offset_of_NewsUri_15(),
	UriParser_t1012511323_StaticFields::get_offset_of_MailToUri_16(),
	UriParser_t1012511323_StaticFields::get_offset_of_UuidUri_17(),
	UriParser_t1012511323_StaticFields::get_offset_of_TelnetUri_18(),
	UriParser_t1012511323_StaticFields::get_offset_of_LdapUri_19(),
	UriParser_t1012511323_StaticFields::get_offset_of_NetTcpUri_20(),
	UriParser_t1012511323_StaticFields::get_offset_of_NetPipeUri_21(),
	UriParser_t1012511323_StaticFields::get_offset_of_VsMacrosUri_22(),
	UriParser_t1012511323_StaticFields::get_offset_of_s_QuirksVersion_23(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpSyntaxFlags_24(),
	UriParser_t1012511323_StaticFields::get_offset_of_FileSyntaxFlags_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (UriQuirksVersion_t4233729352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2171[3] = 
{
	UriQuirksVersion_t4233729352::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (BuiltInUriParser_t2634503859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (IriHelper_t306005226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (Uri_t19570940), -1, sizeof(Uri_t19570940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2174[29] = 
{
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFile_0(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFtp_1(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeGopher_2(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttp_3(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttps_4(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeWs_5(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeWss_6(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeMailto_7(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNews_8(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNntp_9(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetTcp_10(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetPipe_11(),
	Uri_t19570940_StaticFields::get_offset_of_SchemeDelimiter_12(),
	Uri_t19570940::get_offset_of_m_String_13(),
	Uri_t19570940::get_offset_of_m_originalUnicodeString_14(),
	Uri_t19570940::get_offset_of_m_Syntax_15(),
	Uri_t19570940::get_offset_of_m_DnsSafeHost_16(),
	Uri_t19570940::get_offset_of_m_Flags_17(),
	Uri_t19570940::get_offset_of_m_Info_18(),
	Uri_t19570940::get_offset_of_m_iriParsing_19(),
	Uri_t19570940_StaticFields::get_offset_of_s_ConfigInitialized_20(),
	Uri_t19570940_StaticFields::get_offset_of_s_ConfigInitializing_21(),
	Uri_t19570940_StaticFields::get_offset_of_s_IdnScope_22(),
	Uri_t19570940_StaticFields::get_offset_of_s_IriParsing_23(),
	Uri_t19570940_StaticFields::get_offset_of_useDotNetRelativeOrAbsolute_24(),
	Uri_t19570940_StaticFields::get_offset_of_IsWindowsFileSystem_25(),
	Uri_t19570940_StaticFields::get_offset_of_s_initLock_26(),
	Uri_t19570940_StaticFields::get_offset_of_HexLowerChars_27(),
	Uri_t19570940_StaticFields::get_offset_of__WSchars_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (Flags_t455382755)+ sizeof (Il2CppObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[56] = 
{
	Flags_t455382755::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (UriInfo_t4047916940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[6] = 
{
	UriInfo_t4047916940::get_offset_of_Host_0(),
	UriInfo_t4047916940::get_offset_of_ScopeId_1(),
	UriInfo_t4047916940::get_offset_of_String_2(),
	UriInfo_t4047916940::get_offset_of_Offset_3(),
	UriInfo_t4047916940::get_offset_of_DnsSafeHost_4(),
	UriInfo_t4047916940::get_offset_of_MoreInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (Offset_t266882373)+ sizeof (Il2CppObject), sizeof(Offset_t266882373 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2177[8] = 
{
	Offset_t266882373::get_offset_of_Scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_User_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_Host_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_PortValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_Path_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_Query_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_Fragment_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Offset_t266882373::get_offset_of_End_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (MoreInfo_t2595315311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[6] = 
{
	MoreInfo_t2595315311::get_offset_of_Path_0(),
	MoreInfo_t2595315311::get_offset_of_Query_1(),
	MoreInfo_t2595315311::get_offset_of_Fragment_2(),
	MoreInfo_t2595315311::get_offset_of_AbsoluteUri_3(),
	MoreInfo_t2595315311::get_offset_of_Hash_4(),
	MoreInfo_t2595315311::get_offset_of_RemoteUrl_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (Check_t363272550)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[10] = 
{
	Check_t363272550::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (UriBuilder_t2016461725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[11] = 
{
	UriBuilder_t2016461725::get_offset_of_m_changed_0(),
	UriBuilder_t2016461725::get_offset_of_m_fragment_1(),
	UriBuilder_t2016461725::get_offset_of_m_host_2(),
	UriBuilder_t2016461725::get_offset_of_m_password_3(),
	UriBuilder_t2016461725::get_offset_of_m_path_4(),
	UriBuilder_t2016461725::get_offset_of_m_port_5(),
	UriBuilder_t2016461725::get_offset_of_m_query_6(),
	UriBuilder_t2016461725::get_offset_of_m_scheme_7(),
	UriBuilder_t2016461725::get_offset_of_m_schemeDelimiter_8(),
	UriBuilder_t2016461725::get_offset_of_m_uri_9(),
	UriBuilder_t2016461725::get_offset_of_m_username_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (UriKind_t1128731744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2181[4] = 
{
	UriKind_t1128731744::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (UriComponents_t3302767704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2182[18] = 
{
	UriComponents_t3302767704::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (UriFormat_t2764505239)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2183[4] = 
{
	UriFormat_t2764505239::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (UriIdnScope_t761062207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2184[4] = 
{
	UriIdnScope_t761062207::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (ParsingError_t953959262)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2185[15] = 
{
	ParsingError_t953959262::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (UnescapeMode_t584481035)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2186[8] = 
{
	UnescapeMode_t584481035::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (UriFormatException_t3682083048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (UriHelper_t2566857206), -1, sizeof(UriHelper_t2566857206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	UriHelper_t2566857206_StaticFields::get_offset_of_HexUpperChars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (UriHostNameType_t2148127109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2189[6] = 
{
	UriHostNameType_t2148127109::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (EmptyWebProxy_t450353429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[1] = 
{
	EmptyWebProxy_t450353429::get_offset_of_m_credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (HeaderParser_t3706119548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (HeaderInfo_t3044570949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[5] = 
{
	HeaderInfo_t3044570949::get_offset_of_IsRequestRestricted_0(),
	HeaderInfo_t3044570949::get_offset_of_IsResponseRestricted_1(),
	HeaderInfo_t3044570949::get_offset_of_Parser_2(),
	HeaderInfo_t3044570949::get_offset_of_HeaderName_3(),
	HeaderInfo_t3044570949::get_offset_of_AllowMultiValues_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (HeaderInfoTable_t2462863175), -1, sizeof(HeaderInfoTable_t2462863175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2193[4] = 
{
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_HeaderHashTable_0(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_UnknownHeaderInfo_1(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_SingleParser_2(),
	HeaderInfoTable_t2462863175_StaticFields::get_offset_of_MultiParser_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (CloseExState_t4185113936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2194[4] = 
{
	CloseExState_t4185113936::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (NetRes_t3820382307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (LazyAsyncResult_t698912221), -1, 0, sizeof(LazyAsyncResult_t698912221_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2197[8] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	LazyAsyncResult_t698912221::get_offset_of_m_AsyncObject_1(),
	LazyAsyncResult_t698912221::get_offset_of_m_AsyncState_2(),
	LazyAsyncResult_t698912221::get_offset_of_m_AsyncCallback_3(),
	LazyAsyncResult_t698912221::get_offset_of_m_Result_4(),
	LazyAsyncResult_t698912221::get_offset_of_m_IntCompleted_5(),
	LazyAsyncResult_t698912221::get_offset_of_m_UserEvent_6(),
	LazyAsyncResult_t698912221::get_offset_of_m_Event_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ThreadContext_t1626776243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[1] = 
{
	ThreadContext_t1626776243::get_offset_of_m_NestedIOCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
