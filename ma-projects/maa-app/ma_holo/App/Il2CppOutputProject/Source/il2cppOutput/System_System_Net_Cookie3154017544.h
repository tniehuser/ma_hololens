﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_CookieVariant839969627.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Net.Comparer
struct Comparer_t3706593733;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cookie
struct  Cookie_t3154017544  : public Il2CppObject
{
public:
	// System.String System.Net.Cookie::m_comment
	String_t* ___m_comment_4;
	// System.Uri System.Net.Cookie::m_commentUri
	Uri_t19570940 * ___m_commentUri_5;
	// System.Net.CookieVariant System.Net.Cookie::m_cookieVariant
	int32_t ___m_cookieVariant_6;
	// System.Boolean System.Net.Cookie::m_discard
	bool ___m_discard_7;
	// System.String System.Net.Cookie::m_domain
	String_t* ___m_domain_8;
	// System.Boolean System.Net.Cookie::m_domain_implicit
	bool ___m_domain_implicit_9;
	// System.DateTime System.Net.Cookie::m_expires
	DateTime_t693205669  ___m_expires_10;
	// System.String System.Net.Cookie::m_name
	String_t* ___m_name_11;
	// System.String System.Net.Cookie::m_path
	String_t* ___m_path_12;
	// System.Boolean System.Net.Cookie::m_path_implicit
	bool ___m_path_implicit_13;
	// System.String System.Net.Cookie::m_port
	String_t* ___m_port_14;
	// System.Boolean System.Net.Cookie::m_port_implicit
	bool ___m_port_implicit_15;
	// System.Int32[] System.Net.Cookie::m_port_list
	Int32U5BU5D_t3030399641* ___m_port_list_16;
	// System.Boolean System.Net.Cookie::m_secure
	bool ___m_secure_17;
	// System.Boolean System.Net.Cookie::m_httpOnly
	bool ___m_httpOnly_18;
	// System.DateTime System.Net.Cookie::m_timeStamp
	DateTime_t693205669  ___m_timeStamp_19;
	// System.String System.Net.Cookie::m_value
	String_t* ___m_value_20;
	// System.Int32 System.Net.Cookie::m_version
	int32_t ___m_version_21;
	// System.String System.Net.Cookie::m_domainKey
	String_t* ___m_domainKey_22;
	// System.Boolean System.Net.Cookie::IsQuotedVersion
	bool ___IsQuotedVersion_23;
	// System.Boolean System.Net.Cookie::IsQuotedDomain
	bool ___IsQuotedDomain_24;

public:
	inline static int32_t get_offset_of_m_comment_4() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_comment_4)); }
	inline String_t* get_m_comment_4() const { return ___m_comment_4; }
	inline String_t** get_address_of_m_comment_4() { return &___m_comment_4; }
	inline void set_m_comment_4(String_t* value)
	{
		___m_comment_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_comment_4, value);
	}

	inline static int32_t get_offset_of_m_commentUri_5() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_commentUri_5)); }
	inline Uri_t19570940 * get_m_commentUri_5() const { return ___m_commentUri_5; }
	inline Uri_t19570940 ** get_address_of_m_commentUri_5() { return &___m_commentUri_5; }
	inline void set_m_commentUri_5(Uri_t19570940 * value)
	{
		___m_commentUri_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_commentUri_5, value);
	}

	inline static int32_t get_offset_of_m_cookieVariant_6() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_cookieVariant_6)); }
	inline int32_t get_m_cookieVariant_6() const { return ___m_cookieVariant_6; }
	inline int32_t* get_address_of_m_cookieVariant_6() { return &___m_cookieVariant_6; }
	inline void set_m_cookieVariant_6(int32_t value)
	{
		___m_cookieVariant_6 = value;
	}

	inline static int32_t get_offset_of_m_discard_7() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_discard_7)); }
	inline bool get_m_discard_7() const { return ___m_discard_7; }
	inline bool* get_address_of_m_discard_7() { return &___m_discard_7; }
	inline void set_m_discard_7(bool value)
	{
		___m_discard_7 = value;
	}

	inline static int32_t get_offset_of_m_domain_8() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_domain_8)); }
	inline String_t* get_m_domain_8() const { return ___m_domain_8; }
	inline String_t** get_address_of_m_domain_8() { return &___m_domain_8; }
	inline void set_m_domain_8(String_t* value)
	{
		___m_domain_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_domain_8, value);
	}

	inline static int32_t get_offset_of_m_domain_implicit_9() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_domain_implicit_9)); }
	inline bool get_m_domain_implicit_9() const { return ___m_domain_implicit_9; }
	inline bool* get_address_of_m_domain_implicit_9() { return &___m_domain_implicit_9; }
	inline void set_m_domain_implicit_9(bool value)
	{
		___m_domain_implicit_9 = value;
	}

	inline static int32_t get_offset_of_m_expires_10() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_expires_10)); }
	inline DateTime_t693205669  get_m_expires_10() const { return ___m_expires_10; }
	inline DateTime_t693205669 * get_address_of_m_expires_10() { return &___m_expires_10; }
	inline void set_m_expires_10(DateTime_t693205669  value)
	{
		___m_expires_10 = value;
	}

	inline static int32_t get_offset_of_m_name_11() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_name_11)); }
	inline String_t* get_m_name_11() const { return ___m_name_11; }
	inline String_t** get_address_of_m_name_11() { return &___m_name_11; }
	inline void set_m_name_11(String_t* value)
	{
		___m_name_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_11, value);
	}

	inline static int32_t get_offset_of_m_path_12() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_path_12)); }
	inline String_t* get_m_path_12() const { return ___m_path_12; }
	inline String_t** get_address_of_m_path_12() { return &___m_path_12; }
	inline void set_m_path_12(String_t* value)
	{
		___m_path_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_path_12, value);
	}

	inline static int32_t get_offset_of_m_path_implicit_13() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_path_implicit_13)); }
	inline bool get_m_path_implicit_13() const { return ___m_path_implicit_13; }
	inline bool* get_address_of_m_path_implicit_13() { return &___m_path_implicit_13; }
	inline void set_m_path_implicit_13(bool value)
	{
		___m_path_implicit_13 = value;
	}

	inline static int32_t get_offset_of_m_port_14() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_port_14)); }
	inline String_t* get_m_port_14() const { return ___m_port_14; }
	inline String_t** get_address_of_m_port_14() { return &___m_port_14; }
	inline void set_m_port_14(String_t* value)
	{
		___m_port_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_port_14, value);
	}

	inline static int32_t get_offset_of_m_port_implicit_15() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_port_implicit_15)); }
	inline bool get_m_port_implicit_15() const { return ___m_port_implicit_15; }
	inline bool* get_address_of_m_port_implicit_15() { return &___m_port_implicit_15; }
	inline void set_m_port_implicit_15(bool value)
	{
		___m_port_implicit_15 = value;
	}

	inline static int32_t get_offset_of_m_port_list_16() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_port_list_16)); }
	inline Int32U5BU5D_t3030399641* get_m_port_list_16() const { return ___m_port_list_16; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_port_list_16() { return &___m_port_list_16; }
	inline void set_m_port_list_16(Int32U5BU5D_t3030399641* value)
	{
		___m_port_list_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_port_list_16, value);
	}

	inline static int32_t get_offset_of_m_secure_17() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_secure_17)); }
	inline bool get_m_secure_17() const { return ___m_secure_17; }
	inline bool* get_address_of_m_secure_17() { return &___m_secure_17; }
	inline void set_m_secure_17(bool value)
	{
		___m_secure_17 = value;
	}

	inline static int32_t get_offset_of_m_httpOnly_18() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_httpOnly_18)); }
	inline bool get_m_httpOnly_18() const { return ___m_httpOnly_18; }
	inline bool* get_address_of_m_httpOnly_18() { return &___m_httpOnly_18; }
	inline void set_m_httpOnly_18(bool value)
	{
		___m_httpOnly_18 = value;
	}

	inline static int32_t get_offset_of_m_timeStamp_19() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_timeStamp_19)); }
	inline DateTime_t693205669  get_m_timeStamp_19() const { return ___m_timeStamp_19; }
	inline DateTime_t693205669 * get_address_of_m_timeStamp_19() { return &___m_timeStamp_19; }
	inline void set_m_timeStamp_19(DateTime_t693205669  value)
	{
		___m_timeStamp_19 = value;
	}

	inline static int32_t get_offset_of_m_value_20() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_value_20)); }
	inline String_t* get_m_value_20() const { return ___m_value_20; }
	inline String_t** get_address_of_m_value_20() { return &___m_value_20; }
	inline void set_m_value_20(String_t* value)
	{
		___m_value_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_value_20, value);
	}

	inline static int32_t get_offset_of_m_version_21() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_version_21)); }
	inline int32_t get_m_version_21() const { return ___m_version_21; }
	inline int32_t* get_address_of_m_version_21() { return &___m_version_21; }
	inline void set_m_version_21(int32_t value)
	{
		___m_version_21 = value;
	}

	inline static int32_t get_offset_of_m_domainKey_22() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___m_domainKey_22)); }
	inline String_t* get_m_domainKey_22() const { return ___m_domainKey_22; }
	inline String_t** get_address_of_m_domainKey_22() { return &___m_domainKey_22; }
	inline void set_m_domainKey_22(String_t* value)
	{
		___m_domainKey_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_domainKey_22, value);
	}

	inline static int32_t get_offset_of_IsQuotedVersion_23() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___IsQuotedVersion_23)); }
	inline bool get_IsQuotedVersion_23() const { return ___IsQuotedVersion_23; }
	inline bool* get_address_of_IsQuotedVersion_23() { return &___IsQuotedVersion_23; }
	inline void set_IsQuotedVersion_23(bool value)
	{
		___IsQuotedVersion_23 = value;
	}

	inline static int32_t get_offset_of_IsQuotedDomain_24() { return static_cast<int32_t>(offsetof(Cookie_t3154017544, ___IsQuotedDomain_24)); }
	inline bool get_IsQuotedDomain_24() const { return ___IsQuotedDomain_24; }
	inline bool* get_address_of_IsQuotedDomain_24() { return &___IsQuotedDomain_24; }
	inline void set_IsQuotedDomain_24(bool value)
	{
		___IsQuotedDomain_24 = value;
	}
};

struct Cookie_t3154017544_StaticFields
{
public:
	// System.Char[] System.Net.Cookie::PortSplitDelimiters
	CharU5BU5D_t1328083999* ___PortSplitDelimiters_0;
	// System.Char[] System.Net.Cookie::Reserved2Name
	CharU5BU5D_t1328083999* ___Reserved2Name_1;
	// System.Char[] System.Net.Cookie::Reserved2Value
	CharU5BU5D_t1328083999* ___Reserved2Value_2;
	// System.Net.Comparer System.Net.Cookie::staticComparer
	Comparer_t3706593733 * ___staticComparer_3;

public:
	inline static int32_t get_offset_of_PortSplitDelimiters_0() { return static_cast<int32_t>(offsetof(Cookie_t3154017544_StaticFields, ___PortSplitDelimiters_0)); }
	inline CharU5BU5D_t1328083999* get_PortSplitDelimiters_0() const { return ___PortSplitDelimiters_0; }
	inline CharU5BU5D_t1328083999** get_address_of_PortSplitDelimiters_0() { return &___PortSplitDelimiters_0; }
	inline void set_PortSplitDelimiters_0(CharU5BU5D_t1328083999* value)
	{
		___PortSplitDelimiters_0 = value;
		Il2CppCodeGenWriteBarrier(&___PortSplitDelimiters_0, value);
	}

	inline static int32_t get_offset_of_Reserved2Name_1() { return static_cast<int32_t>(offsetof(Cookie_t3154017544_StaticFields, ___Reserved2Name_1)); }
	inline CharU5BU5D_t1328083999* get_Reserved2Name_1() const { return ___Reserved2Name_1; }
	inline CharU5BU5D_t1328083999** get_address_of_Reserved2Name_1() { return &___Reserved2Name_1; }
	inline void set_Reserved2Name_1(CharU5BU5D_t1328083999* value)
	{
		___Reserved2Name_1 = value;
		Il2CppCodeGenWriteBarrier(&___Reserved2Name_1, value);
	}

	inline static int32_t get_offset_of_Reserved2Value_2() { return static_cast<int32_t>(offsetof(Cookie_t3154017544_StaticFields, ___Reserved2Value_2)); }
	inline CharU5BU5D_t1328083999* get_Reserved2Value_2() const { return ___Reserved2Value_2; }
	inline CharU5BU5D_t1328083999** get_address_of_Reserved2Value_2() { return &___Reserved2Value_2; }
	inline void set_Reserved2Value_2(CharU5BU5D_t1328083999* value)
	{
		___Reserved2Value_2 = value;
		Il2CppCodeGenWriteBarrier(&___Reserved2Value_2, value);
	}

	inline static int32_t get_offset_of_staticComparer_3() { return static_cast<int32_t>(offsetof(Cookie_t3154017544_StaticFields, ___staticComparer_3)); }
	inline Comparer_t3706593733 * get_staticComparer_3() const { return ___staticComparer_3; }
	inline Comparer_t3706593733 ** get_address_of_staticComparer_3() { return &___staticComparer_3; }
	inline void set_staticComparer_3(Comparer_t3706593733 * value)
	{
		___staticComparer_3 = value;
		Il2CppCodeGenWriteBarrier(&___staticComparer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
