﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_RegexRunnerF3902733837.h"

// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t3307743052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CompiledRegexRunnerFactory
struct  CompiledRegexRunnerFactory_t3379954222  : public RegexRunnerFactory_t3902733837
{
public:
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::goMethod
	DynamicMethod_t3307743052 * ___goMethod_0;
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::findFirstCharMethod
	DynamicMethod_t3307743052 * ___findFirstCharMethod_1;
	// System.Reflection.Emit.DynamicMethod System.Text.RegularExpressions.CompiledRegexRunnerFactory::initTrackCountMethod
	DynamicMethod_t3307743052 * ___initTrackCountMethod_2;

public:
	inline static int32_t get_offset_of_goMethod_0() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___goMethod_0)); }
	inline DynamicMethod_t3307743052 * get_goMethod_0() const { return ___goMethod_0; }
	inline DynamicMethod_t3307743052 ** get_address_of_goMethod_0() { return &___goMethod_0; }
	inline void set_goMethod_0(DynamicMethod_t3307743052 * value)
	{
		___goMethod_0 = value;
		Il2CppCodeGenWriteBarrier(&___goMethod_0, value);
	}

	inline static int32_t get_offset_of_findFirstCharMethod_1() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___findFirstCharMethod_1)); }
	inline DynamicMethod_t3307743052 * get_findFirstCharMethod_1() const { return ___findFirstCharMethod_1; }
	inline DynamicMethod_t3307743052 ** get_address_of_findFirstCharMethod_1() { return &___findFirstCharMethod_1; }
	inline void set_findFirstCharMethod_1(DynamicMethod_t3307743052 * value)
	{
		___findFirstCharMethod_1 = value;
		Il2CppCodeGenWriteBarrier(&___findFirstCharMethod_1, value);
	}

	inline static int32_t get_offset_of_initTrackCountMethod_2() { return static_cast<int32_t>(offsetof(CompiledRegexRunnerFactory_t3379954222, ___initTrackCountMethod_2)); }
	inline DynamicMethod_t3307743052 * get_initTrackCountMethod_2() const { return ___initTrackCountMethod_2; }
	inline DynamicMethod_t3307743052 ** get_address_of_initTrackCountMethod_2() { return &___initTrackCountMethod_2; }
	inline void set_initTrackCountMethod_2(DynamicMethod_t3307743052 * value)
	{
		___initTrackCountMethod_2 = value;
		Il2CppCodeGenWriteBarrier(&___initTrackCountMethod_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
