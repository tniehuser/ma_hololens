﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialKey
struct  CredentialKey_t4100643814  : public Il2CppObject
{
public:
	// System.Uri System.Net.CredentialKey::UriPrefix
	Uri_t19570940 * ___UriPrefix_0;
	// System.Int32 System.Net.CredentialKey::UriPrefixLength
	int32_t ___UriPrefixLength_1;
	// System.String System.Net.CredentialKey::AuthenticationType
	String_t* ___AuthenticationType_2;
	// System.Int32 System.Net.CredentialKey::m_HashCode
	int32_t ___m_HashCode_3;
	// System.Boolean System.Net.CredentialKey::m_ComputedHashCode
	bool ___m_ComputedHashCode_4;

public:
	inline static int32_t get_offset_of_UriPrefix_0() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___UriPrefix_0)); }
	inline Uri_t19570940 * get_UriPrefix_0() const { return ___UriPrefix_0; }
	inline Uri_t19570940 ** get_address_of_UriPrefix_0() { return &___UriPrefix_0; }
	inline void set_UriPrefix_0(Uri_t19570940 * value)
	{
		___UriPrefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___UriPrefix_0, value);
	}

	inline static int32_t get_offset_of_UriPrefixLength_1() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___UriPrefixLength_1)); }
	inline int32_t get_UriPrefixLength_1() const { return ___UriPrefixLength_1; }
	inline int32_t* get_address_of_UriPrefixLength_1() { return &___UriPrefixLength_1; }
	inline void set_UriPrefixLength_1(int32_t value)
	{
		___UriPrefixLength_1 = value;
	}

	inline static int32_t get_offset_of_AuthenticationType_2() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___AuthenticationType_2)); }
	inline String_t* get_AuthenticationType_2() const { return ___AuthenticationType_2; }
	inline String_t** get_address_of_AuthenticationType_2() { return &___AuthenticationType_2; }
	inline void set_AuthenticationType_2(String_t* value)
	{
		___AuthenticationType_2 = value;
		Il2CppCodeGenWriteBarrier(&___AuthenticationType_2, value);
	}

	inline static int32_t get_offset_of_m_HashCode_3() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___m_HashCode_3)); }
	inline int32_t get_m_HashCode_3() const { return ___m_HashCode_3; }
	inline int32_t* get_address_of_m_HashCode_3() { return &___m_HashCode_3; }
	inline void set_m_HashCode_3(int32_t value)
	{
		___m_HashCode_3 = value;
	}

	inline static int32_t get_offset_of_m_ComputedHashCode_4() { return static_cast<int32_t>(offsetof(CredentialKey_t4100643814, ___m_ComputedHashCode_4)); }
	inline bool get_m_ComputedHashCode_4() const { return ___m_ComputedHashCode_4; }
	inline bool* get_address_of_m_ComputedHashCode_4() { return &___m_ComputedHashCode_4; }
	inline void set_m_ComputedHashCode_4(bool value)
	{
		___m_ComputedHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
