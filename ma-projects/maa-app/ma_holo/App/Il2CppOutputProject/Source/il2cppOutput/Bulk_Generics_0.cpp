﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Action_1_gen981956716.h"
#include "mscorlib_System_Threading_AsyncLocalValueChangedAr1180157334.h"
#include "mscorlib_System_Action_2_gen2572051853.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen610337993.h"
#include "mscorlib_System_Comparison_1_gen792346273.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen467867711.h"
#include "mscorlib_System_Comparison_1_gen649875991.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen1772936240.h"
#include "mscorlib_System_Comparison_1_gen1954944520.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen2442719477.h"
#include "mscorlib_System_Comparison_1_gen2624727757.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen1804431648.h"
#include "mscorlib_System_Comparison_1_gen1986439928.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen862778956.h"
#include "mscorlib_System_Comparison_1_gen1044787236.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen826009189.h"
#include "mscorlib_System_Comparison_1_gen1008017469.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3151608019.h"
#include "mscorlib_System_Comparison_1_gen3333616299.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen1988808608.h"
#include "mscorlib_System_Comparison_1_gen2170816888.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3769179866.h"
#include "mscorlib_System_Comparison_1_gen3951188146.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen1534148120.h"
#include "mscorlib_System_Comparison_1_gen1716156400.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3156240503.h"
#include "mscorlib_System_Comparison_1_gen3338248783.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3497990298.h"
#include "mscorlib_System_Comparison_1_gen3679998578.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen215022224.h"
#include "mscorlib_System_Comparison_1_gen397030504.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen2066613182.h"
#include "mscorlib_System_Comparison_1_gen2248621462.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3229412592.h"
#include "mscorlib_System_Comparison_1_gen3411420872.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3988927485.h"
#include "mscorlib_System_Comparison_1_gen4170935765.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3860533493.h"
#include "mscorlib_System_Comparison_1_gen4042541773.h"
#include "System_Xml_System_Xml_Schema_RangePositionInfo2780802922.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen3590317081.h"
#include "mscorlib_System_Comparison_1_gen3772325361.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2510586510.h"
#include "mscorlib_System_Array_FunctorComparer_1_gen690514624.h"
#include "mscorlib_System_Comparison_1_gen872522904.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen793236484.h"
#include "Mono_Security_Mono_Security_Interface_CipherSuiteC4229451518.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1542250127.h"
#include "mscorlib_Mono_Security_Uri_UriScheme683497865.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3977134117.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNode3118381855.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2951357404.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeRef2092605142.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2690501253.h"
#include "System_Xml_MS_Internal_Xml_XPath_Operator_Op1831748991.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2281624261.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1422871999.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen778392047.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4214607081.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen252501700.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3688716734.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1972861616.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1114109354.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen219164346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3655379380.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen836736193.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4272951227.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen303677782.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3739892816.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2342494975.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21483742713.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen839262761.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24275477795.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1747572097.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen364548496.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23800763530.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1835343917.h"
#include "mscorlib_System_Collections_Hashtable_bucket976591655.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1027194178.h"
#include "System_System_ComponentModel_AttributeCollection_At168441916.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1551957931.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2221741168.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1583453339.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen641800647.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2600861628.h"
#include "mscorlib_System_Globalization_InternalCodePageData1742109366.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen941671943.h"
#include "mscorlib_System_Globalization_InternalEncodingDataIt82919681.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2890411629.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp2031659367.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3392353855.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen605030880.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2930629710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1767830299.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3362812871.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen494314080.h"
#include "System_System_Net_CookieTokenizer_RecognizedAttrib3930529114.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2917548534.h"
#include "System_System_Net_HeaderVariantInfo2058796272.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3058064956.h"
#include "System_System_Net_Sockets_Socket_WSABUF2199312694.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2275375023.h"
#include "System_System_Net_WebHeaderCollection_RfcChar1416622761.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3806268713.h"
#include "mscorlib_System_ParameterizedStrings_FormatParam2947516451.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952909805.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2356950176.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2901227451.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionBlock2042475189.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2348906860.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionInfo1490154598.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen275897710.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3712112744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen654694480.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelF4090909514.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008311600.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo149559338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen806987626.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3986139419.h"
#include "mscorlib_System_Reflection_Emit_MonoResource3127387157.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3326058480.h"
#include "mscorlib_System_Reflection_Emit_MonoWin32Resource2467306218.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3567360695.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS2708608433.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679387182.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3015143146.h"
#include "mscorlib_System_Resources_ResourceLocator2156390884.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2733828895.h"
#include "mscorlib_System_Runtime_CompilerServices_Ephemeron1875076633.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4268020328.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3736091384.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2877339122.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3879563917.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1313169811.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen842163687.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2284019382.h"
#include "mscorlib_System_TermInfoStrings1425267120.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3012688088.h"
#include "System_System_Text_RegularExpressions_RegexCharCla2153935826.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3277011989.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2567611619.h"
#include "mscorlib_System_Threading_CancellationTokenRegistr1708859357.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2714516328.h"
#include "mscorlib_System_TimeZoneInfo_TZifType1855764066.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3395678463.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176.h"

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3022476291;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>
struct Action_1_t981956716;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Array/FunctorComparer`1<System.Boolean>
struct FunctorComparer_1_t610337993;
// System.Comparison`1<System.Boolean>
struct Comparison_1_t792346273;
// System.Array/FunctorComparer`1<System.Byte>
struct FunctorComparer_1_t467867711;
// System.Comparison`1<System.Byte>
struct Comparison_1_t649875991;
// System.Array/FunctorComparer`1<System.DateTime>
struct FunctorComparer_1_t1772936240;
// System.Comparison`1<System.DateTime>
struct Comparison_1_t1954944520;
// System.Array/FunctorComparer`1<System.DateTimeOffset>
struct FunctorComparer_1_t2442719477;
// System.Comparison`1<System.DateTimeOffset>
struct Comparison_1_t2624727757;
// System.Array/FunctorComparer`1<System.Decimal>
struct FunctorComparer_1_t1804431648;
// System.Comparison`1<System.Decimal>
struct Comparison_1_t1986439928;
// System.Array/FunctorComparer`1<System.Double>
struct FunctorComparer_1_t862778956;
// System.Comparison`1<System.Double>
struct Comparison_1_t1044787236;
// System.Array/FunctorComparer`1<System.Int16>
struct FunctorComparer_1_t826009189;
// System.Comparison`1<System.Int16>
struct Comparison_1_t1008017469;
// System.Array/FunctorComparer`1<System.Int32>
struct FunctorComparer_1_t3151608019;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3333616299;
// System.Array/FunctorComparer`1<System.Int64>
struct FunctorComparer_1_t1988808608;
// System.Comparison`1<System.Int64>
struct Comparison_1_t2170816888;
// System.Array/FunctorComparer`1<System.Object>
struct FunctorComparer_1_t3769179866;
// System.Comparison`1<System.Object>
struct Comparison_1_t3951188146;
// System.Array/FunctorComparer`1<System.SByte>
struct FunctorComparer_1_t1534148120;
// System.Comparison`1<System.SByte>
struct Comparison_1_t1716156400;
// System.Array/FunctorComparer`1<System.Single>
struct FunctorComparer_1_t3156240503;
// System.Comparison`1<System.Single>
struct Comparison_1_t3338248783;
// System.Array/FunctorComparer`1<System.Text.RegularExpressions.RegexOptions>
struct FunctorComparer_1_t3497990298;
// System.Comparison`1<System.Text.RegularExpressions.RegexOptions>
struct Comparison_1_t3679998578;
// System.Array/FunctorComparer`1<System.TimeSpan>
struct FunctorComparer_1_t215022224;
// System.Comparison`1<System.TimeSpan>
struct Comparison_1_t397030504;
// System.Array/FunctorComparer`1<System.UInt16>
struct FunctorComparer_1_t2066613182;
// System.Comparison`1<System.UInt16>
struct Comparison_1_t2248621462;
// System.Array/FunctorComparer`1<System.UInt32>
struct FunctorComparer_1_t3229412592;
// System.Comparison`1<System.UInt32>
struct Comparison_1_t3411420872;
// System.Array/FunctorComparer`1<System.UInt64>
struct FunctorComparer_1_t3988927485;
// System.Comparison`1<System.UInt64>
struct Comparison_1_t4170935765;
// System.Array/FunctorComparer`1<System.Xml.Schema.RangePositionInfo>
struct FunctorComparer_1_t3860533493;
// System.Comparison`1<System.Xml.Schema.RangePositionInfo>
struct Comparison_1_t4042541773;
// System.Array/FunctorComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct FunctorComparer_1_t3590317081;
// System.Comparison`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct Comparison_1_t3772325361;
// System.Array/FunctorComparer`1<UnityEngine.AnimatorClipInfo>
struct FunctorComparer_1_t690514624;
// System.Comparison`1<UnityEngine.AnimatorClipInfo>
struct Comparison_1_t872522904;
// System.Array
struct Il2CppArray;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.String
struct String_t;
extern Il2CppClass* AsyncLocalValueChangedArgs_1_t1180157334_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3341085586_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3424295274_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3577122241_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3611165223_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3003696592_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m706977411_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3542984528_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1304018714_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3145257525_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2653371815_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m840252737_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1718349432_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2987216314_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1154723631_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2948346621_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1526874475_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m358792963_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1021008655_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m419194570_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1776564649_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m245025210_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3667052076_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1145634683_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1471150671_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3939996428_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m10285187_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3788757124_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2429470294_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1669430788_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m419131001_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m421370399_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m451997186_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4010463147_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m322002637_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2685429706_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4167074463_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1274592126_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1643772188_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1864471731_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1012591846_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m948509279_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1174598684_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m314017974_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m727737343_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3362979103_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m466738280_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2578701872_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3338380422_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2840641472_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3431860728_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId;
extern const uint32_t InternalEnumerator_1_get_Current_m35328337_MetadataUsageId;

// System.Delegate[]
struct DelegateU5BU5D_t1606206610  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3022476291 * m_Items[1];

public:
	inline Delegate_t3022476291 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t3022476291 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t3022476291 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t3022476291 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853419099_gshared (InternalEnumerator_1_t793236484 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1975796854_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3711981423_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3424295274_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m815597740_gshared (InternalEnumerator_1_t1542250127 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m766702873_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2738761868_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::get_Current()
extern "C"  UriScheme_t683497865  InternalEnumerator_1_get_Current_m3577122241_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3692927446_gshared (InternalEnumerator_1_t3977134117 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m178761275_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2742061742_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::get_Current()
extern "C"  XPathNode_t3118381855  InternalEnumerator_1_get_Current_m3611165223_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1918766407_gshared (InternalEnumerator_1_t2951357404 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1171545318_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m21981123_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::get_Current()
extern "C"  XPathNodeRef_t2092605142  InternalEnumerator_1_get_Current_m3003696592_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1169622682_gshared (InternalEnumerator_1_t2690501253 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2160474007_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3508153682_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m706977411_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1866922360_gshared (InternalEnumerator_1_t3452969744 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m592267945_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m421278255_gshared (InternalEnumerator_1_t2281624261 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3790093886_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1016442011_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
extern "C"  Entry_t1422871999  InternalEnumerator_1_get_Current_m3542984528_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2192168017_gshared (InternalEnumerator_1_t778392047 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1447718640_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2474607397_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::get_Current()
extern "C"  Entry_t4214607081  InternalEnumerator_1_get_Current_m1304018714_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4190080554_gshared (InternalEnumerator_1_t252501700 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2858632093_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3459902714_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::get_Current()
extern "C"  Entry_t3688716734  InternalEnumerator_1_get_Current_m3145257525_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2775616774_gshared (InternalEnumerator_1_t1972861616 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m336048027_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3523763518_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::get_Current()
extern "C"  Entry_t1114109354  InternalEnumerator_1_get_Current_m2653371815_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1851236734_gshared (InternalEnumerator_1_t219164346 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3390693617_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3547199662_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::get_Current()
extern "C"  Entry_t3655379380  InternalEnumerator_1_get_Current_m840252737_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m981450495_gshared (InternalEnumerator_1_t836736193 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3328202350_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1076783627_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::get_Current()
extern "C"  Entry_t4272951227  InternalEnumerator_1_get_Current_m1718349432_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m453027211_gshared (InternalEnumerator_1_t303677782 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4125815062_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3692091135_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::get_Current()
extern "C"  Entry_t3739892816  InternalEnumerator_1_get_Current_m2987216314_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2417538736_gshared (InternalEnumerator_1_t2342494975 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m195176279_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m322636248_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
extern "C"  KeyValuePair_2_t1483742713  InternalEnumerator_1_get_Current_m1154723631_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1754127910_gshared (InternalEnumerator_1_t839262761 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1037069_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4073508358_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4275477795  InternalEnumerator_1_get_Current_m2948346621_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m920047666_gshared (InternalEnumerator_1_t1747572097 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1628196379_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m930150026_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t888819835  InternalEnumerator_1_get_Current_m1526874475_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m967618647_gshared (InternalEnumerator_1_t2033732330 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318835130_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3362782841_gshared (InternalEnumerator_1_t280035060 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3587374424_gshared (InternalEnumerator_1_t897606907 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3950184714_gshared (InternalEnumerator_1_t364548496 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2915635719_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3284769442_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::get_Current()
extern "C"  KeyValuePair_2_t3800763530  InternalEnumerator_1_get_Current_m358792963_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m645344728_gshared (InternalEnumerator_1_t1835343917 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3737488171_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2095753328_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::get_Current()
extern "C"  bucket_t976591655  InternalEnumerator_1_get_Current_m1021008655_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4085858451_gshared (InternalEnumerator_1_t1027194178 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2936110260_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3616673367_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::get_Current()
extern "C"  AttributeEntry_t168441916  InternalEnumerator_1_get_Current_m419194570_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m245588437_gshared (InternalEnumerator_1_t1551957931 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m966844380_gshared (InternalEnumerator_1_t2221741168 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2889182185_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.DateTimeOffset>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m276280428_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.DateTimeOffset>::get_Current()
extern "C"  DateTimeOffset_t1362988906  InternalEnumerator_1_get_Current_m1776564649_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150855019_gshared (InternalEnumerator_1_t1583453339 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3589241961_gshared (InternalEnumerator_1_t641800647 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m565351307_gshared (InternalEnumerator_1_t2600861628 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3886016744_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3943379735_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::get_Current()
extern "C"  InternalCodePageDataItem_t1742109366  InternalEnumerator_1_get_Current_m3667052076_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m298297972_gshared (InternalEnumerator_1_t941671943 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3568777087_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3256434108_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::get_Current()
extern "C"  InternalEncodingDataItem_t82919681  InternalEnumerator_1_get_Current_m1145634683_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3204909454_gshared (InternalEnumerator_1_t2890411629 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3020383423_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2897730678_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::get_Current()
extern "C"  TimeSpanToken_t2031659367  InternalEnumerator_1_get_Current_m1471150671_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Guid>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m702435723_gshared (InternalEnumerator_1_t3392353855 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Guid>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1976167928_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Guid>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1380038135_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Guid>::get_Current()
extern "C"  Guid_t  InternalEnumerator_1_get_Current_m3939996428_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m557239862_gshared (InternalEnumerator_1_t605030880 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m504913220_gshared (InternalEnumerator_1_t2930629710 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2597133905_gshared (InternalEnumerator_1_t1767830299 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m307741520_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1648185761_gshared (InternalEnumerator_1_t3362812871 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m926514125_gshared (InternalEnumerator_1_t494314080 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m676949342_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1352446537_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::get_Current()
extern "C"  RecognizedAttribute_t3930529114  InternalEnumerator_1_get_Current_m3788757124_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1019972839_gshared (InternalEnumerator_1_t2917548534 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1171085184_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2708971515_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::get_Current()
extern "C"  HeaderVariantInfo_t2058796272  InternalEnumerator_1_get_Current_m2429470294_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2945276195_gshared (InternalEnumerator_1_t3058064956 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1866063914_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2957122063_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::get_Current()
extern "C"  WSABUF_t2199312694  InternalEnumerator_1_get_Current_m1669430788_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1608269208_gshared (InternalEnumerator_1_t2275375023 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1743466569_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m706855480_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m419131001_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853313801_gshared (InternalEnumerator_1_t3548201557 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1626855592_gshared (InternalEnumerator_1_t3806268713 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1387288299_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2783536832_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::get_Current()
extern "C"  FormatParam_t2947516451  InternalEnumerator_1_get_Current_m421370399_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m492779768_gshared (InternalEnumerator_1_t952909805 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m238246335_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m821424641_gshared (InternalEnumerator_1_t2356950176 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3294994445_gshared (InternalEnumerator_1_t2901227451 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1287369282_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1560801521_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::get_Current()
extern "C"  ILExceptionBlock_t2042475189  InternalEnumerator_1_get_Current_m451997186_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1398783858_gshared (InternalEnumerator_1_t2348906860 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2657406875_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2180350026_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::get_Current()
extern "C"  ILExceptionInfo_t1490154598  InternalEnumerator_1_get_Current_m4010463147_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3323962057_gshared (InternalEnumerator_1_t275897710 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m549215360_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3228997263_gshared (InternalEnumerator_1_t654694480 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3387972470_gshared (InternalEnumerator_1_t1008311600 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m750604974_gshared (InternalEnumerator_1_t806987626 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m830280837_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2665983630_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::get_Current()
extern "C"  Label_t4243202660  InternalEnumerator_1_get_Current_m322002637_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2582404099_gshared (InternalEnumerator_1_t3986139419 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m219216750_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3074289735_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::get_Current()
extern "C"  MonoResource_t3127387157  InternalEnumerator_1_get_Current_m2685429706_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3708844878_gshared (InternalEnumerator_1_t3326058480 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1448063359_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4230111510_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::get_Current()
extern "C"  MonoWin32Resource_t2467306218  InternalEnumerator_1_get_Current_m4167074463_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3726334735_gshared (InternalEnumerator_1_t3567360695 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2901155554_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3875155235_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::get_Current()
extern "C"  RefEmitPermissionSet_t2708608433  InternalEnumerator_1_get_Current_m1274592126_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2890018883_gshared (InternalEnumerator_1_t2679387182 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2999622677_gshared (InternalEnumerator_1_t3015143146 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1652602110_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1876372465_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::get_Current()
extern "C"  ResourceLocator_t2156390884  InternalEnumerator_1_get_Current_m1643772188_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1198276628_gshared (InternalEnumerator_1_t2733828895 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3248673743_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1104149308_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::get_Current()
extern "C"  Ephemeron_t1875076633  InternalEnumerator_1_get_Current_m1864471731_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m313732903_gshared (InternalEnumerator_1_t4268020328 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m599783274_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4248732811_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::get_Current()
extern "C"  GCHandle_t3409268066  InternalEnumerator_1_get_Current_m1012591846_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3149706958_gshared (InternalEnumerator_1_t3736091384 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2801598691_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1137828214_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m948509279_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1277864681_gshared (InternalEnumerator_1_t3879563917 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1587363548_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m488449285_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1174598684_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108401677_gshared (InternalEnumerator_1_t1313169811 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m655778553_gshared (InternalEnumerator_1_t842163687 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2314640734_gshared (InternalEnumerator_1_t2935262194 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C"  float InternalEnumerator_1_get_Current_m727737343_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2853126416_gshared (InternalEnumerator_1_t2284019382 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m30795463_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.TermInfoStrings>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m138185816_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.TermInfoStrings>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3362979103_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2565240337_gshared (InternalEnumerator_1_t3012688088 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3391663714_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2599112621_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::get_Current()
extern "C"  LowerCaseMapping_t2153935826  InternalEnumerator_1_get_Current_m466738280_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m974809591_gshared (InternalEnumerator_1_t3277011989 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m779531596_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2770905747_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578701872_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2874479637_gshared (InternalEnumerator_1_t2567611619 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2776097548_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3251308169_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::get_Current()
extern "C"  CancellationTokenRegistration_t1708859357  InternalEnumerator_1_get_Current_m3338380422_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2189699457_gshared (InternalEnumerator_1_t4289011211 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m200749079_gshared (InternalEnumerator_1_t2714516328 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2957730742_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4246357971_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::get_Current()
extern "C"  TZifType_t1855764066  InternalEnumerator_1_get_Current_m2840641472_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2264046229_gshared (InternalEnumerator_1_t3395678463 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1071940184_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3081678193_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3431860728_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2981879621_gshared (InternalEnumerator_1_t1845634873 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691972083_gshared (InternalEnumerator_1_t3008434283 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3084132532_gshared (InternalEnumerator_1_t3767949176 * __this, Il2CppArray * ___array0, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method);
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method);
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method);
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method);
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2265739932(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2870158877 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2265739932_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
#define InternalEnumerator_1_Dispose_m1050822571(__this, method) ((  void (*) (InternalEnumerator_1_t2870158877 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1050822571_gshared)(__this, method)
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m1498215565 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1979432532(__this, method) ((  bool (*) (InternalEnumerator_1_t2870158877 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1979432532_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
#define InternalEnumerator_1_get_Current_m2151132603(__this, method) ((  TableRange_t2011406615  (*) (InternalEnumerator_1_t2870158877 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2151132603_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(__this, method) ((  void (*) (InternalEnumerator_1_t2870158877 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2870158877 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m853419099(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t793236484 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853419099_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::Dispose()
#define InternalEnumerator_1_Dispose_m1975796854(__this, method) ((  void (*) (InternalEnumerator_1_t793236484 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1975796854_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3711981423(__this, method) ((  bool (*) (InternalEnumerator_1_t793236484 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3711981423_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::get_Current()
#define InternalEnumerator_1_get_Current_m3424295274(__this, method) ((  uint16_t (*) (InternalEnumerator_1_t793236484 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3424295274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067(__this, method) ((  void (*) (InternalEnumerator_1_t793236484 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t793236484 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2111763266(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t565169432 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2111763266_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
#define InternalEnumerator_1_Dispose_m2038682075(__this, method) ((  void (*) (InternalEnumerator_1_t565169432 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2038682075_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1182905290(__this, method) ((  bool (*) (InternalEnumerator_1_t565169432 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1182905290_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
#define InternalEnumerator_1_get_Current_m3847951219(__this, method) ((  int32_t (*) (InternalEnumerator_1_t565169432 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3847951219_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(__this, method) ((  void (*) (InternalEnumerator_1_t565169432 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t565169432 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m815597740(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1542250127 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m815597740_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::Dispose()
#define InternalEnumerator_1_Dispose_m766702873(__this, method) ((  void (*) (InternalEnumerator_1_t1542250127 *, const MethodInfo*))InternalEnumerator_1_Dispose_m766702873_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2738761868(__this, method) ((  bool (*) (InternalEnumerator_1_t1542250127 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2738761868_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::get_Current()
#define InternalEnumerator_1_get_Current_m3577122241(__this, method) ((  UriScheme_t683497865  (*) (InternalEnumerator_1_t1542250127 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3577122241_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312(__this, method) ((  void (*) (InternalEnumerator_1_t1542250127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1542250127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3692927446(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3977134117 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3692927446_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::Dispose()
#define InternalEnumerator_1_Dispose_m178761275(__this, method) ((  void (*) (InternalEnumerator_1_t3977134117 *, const MethodInfo*))InternalEnumerator_1_Dispose_m178761275_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2742061742(__this, method) ((  bool (*) (InternalEnumerator_1_t3977134117 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2742061742_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::get_Current()
#define InternalEnumerator_1_get_Current_m3611165223(__this, method) ((  XPathNode_t3118381855  (*) (InternalEnumerator_1_t3977134117 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3611165223_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230(__this, method) ((  void (*) (InternalEnumerator_1_t3977134117 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3977134117 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1918766407(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2951357404 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1918766407_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::Dispose()
#define InternalEnumerator_1_Dispose_m1171545318(__this, method) ((  void (*) (InternalEnumerator_1_t2951357404 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1171545318_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21981123(__this, method) ((  bool (*) (InternalEnumerator_1_t2951357404 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21981123_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::get_Current()
#define InternalEnumerator_1_get_Current_m3003696592(__this, method) ((  XPathNodeRef_t2092605142  (*) (InternalEnumerator_1_t2951357404 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3003696592_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943(__this, method) ((  void (*) (InternalEnumerator_1_t2951357404 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2951357404 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1169622682(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2690501253 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1169622682_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::Dispose()
#define InternalEnumerator_1_Dispose_m2160474007(__this, method) ((  void (*) (InternalEnumerator_1_t2690501253 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2160474007_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3508153682(__this, method) ((  bool (*) (InternalEnumerator_1_t2690501253 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3508153682_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::get_Current()
#define InternalEnumerator_1_get_Current_m706977411(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2690501253 *, const MethodInfo*))InternalEnumerator_1_get_Current_m706977411_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506(__this, method) ((  void (*) (InternalEnumerator_1_t2690501253 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2690501253 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1866922360(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3452969744 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1866922360_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
#define InternalEnumerator_1_Dispose_m592267945(__this, method) ((  void (*) (InternalEnumerator_1_t3452969744 *, const MethodInfo*))InternalEnumerator_1_Dispose_m592267945_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1460734872(__this, method) ((  bool (*) (InternalEnumerator_1_t3452969744 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1460734872_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
#define InternalEnumerator_1_get_Current_m1894741129(__this, method) ((  ArraySegment_1_t2594217482  (*) (InternalEnumerator_1_t3452969744 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1894741129_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164(__this, method) ((  void (*) (InternalEnumerator_1_t3452969744 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3452969744 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4119890600(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t389359684 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4119890600_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
#define InternalEnumerator_1_Dispose_m1640363425(__this, method) ((  void (*) (InternalEnumerator_1_t389359684 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1640363425_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1595676968(__this, method) ((  bool (*) (InternalEnumerator_1_t389359684 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1595676968_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
#define InternalEnumerator_1_get_Current_m1943362081(__this, method) ((  bool (*) (InternalEnumerator_1_t389359684 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1943362081_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(__this, method) ((  void (*) (InternalEnumerator_1_t389359684 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t389359684 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3043733612(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t246889402 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3043733612_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
#define InternalEnumerator_1_Dispose_m1148506519(__this, method) ((  void (*) (InternalEnumerator_1_t246889402 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1148506519_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2651026500(__this, method) ((  bool (*) (InternalEnumerator_1_t246889402 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2651026500_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
#define InternalEnumerator_1_get_Current_m4154615771(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t246889402 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4154615771_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(__this, method) ((  void (*) (InternalEnumerator_1_t246889402 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t246889402 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m960275522(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t18266304 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m960275522_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
#define InternalEnumerator_1_Dispose_m811081805(__this, method) ((  void (*) (InternalEnumerator_1_t18266304 *, const MethodInfo*))InternalEnumerator_1_Dispose_m811081805_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
#define InternalEnumerator_1_MoveNext_m412569442(__this, method) ((  bool (*) (InternalEnumerator_1_t18266304 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m412569442_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
#define InternalEnumerator_1_get_Current_m2960188445(__this, method) ((  Il2CppChar (*) (InternalEnumerator_1_t18266304 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2960188445_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(__this, method) ((  void (*) (InternalEnumerator_1_t18266304 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t18266304 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m675130983(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3907627660 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m675130983_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
#define InternalEnumerator_1_Dispose_m3597982928(__this, method) ((  void (*) (InternalEnumerator_1_t3907627660 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3597982928_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1636015243(__this, method) ((  bool (*) (InternalEnumerator_1_t3907627660 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1636015243_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
#define InternalEnumerator_1_get_Current_m2351441486(__this, method) ((  DictionaryEntry_t3048875398  (*) (InternalEnumerator_1_t3907627660 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2351441486_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(__this, method) ((  void (*) (InternalEnumerator_1_t3907627660 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3907627660 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m421278255(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2281624261 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m421278255_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
#define InternalEnumerator_1_Dispose_m3790093886(__this, method) ((  void (*) (InternalEnumerator_1_t2281624261 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3790093886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1016442011(__this, method) ((  bool (*) (InternalEnumerator_1_t2281624261 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1016442011_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
#define InternalEnumerator_1_get_Current_m3542984528(__this, method) ((  Entry_t1422871999  (*) (InternalEnumerator_1_t2281624261 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3542984528_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735(__this, method) ((  void (*) (InternalEnumerator_1_t2281624261 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2281624261 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2192168017(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t778392047 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2192168017_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m1447718640(__this, method) ((  void (*) (InternalEnumerator_1_t778392047 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1447718640_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2474607397(__this, method) ((  bool (*) (InternalEnumerator_1_t778392047 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2474607397_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m1304018714(__this, method) ((  Entry_t4214607081  (*) (InternalEnumerator_1_t778392047 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1304018714_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429(__this, method) ((  void (*) (InternalEnumerator_1_t778392047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t778392047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4190080554(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t252501700 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4190080554_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m2858632093(__this, method) ((  void (*) (InternalEnumerator_1_t252501700 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2858632093_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3459902714(__this, method) ((  bool (*) (InternalEnumerator_1_t252501700 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3459902714_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m3145257525(__this, method) ((  Entry_t3688716734  (*) (InternalEnumerator_1_t252501700 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3145257525_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094(__this, method) ((  void (*) (InternalEnumerator_1_t252501700 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t252501700 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2775616774(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1972861616 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2775616774_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::Dispose()
#define InternalEnumerator_1_Dispose_m336048027(__this, method) ((  void (*) (InternalEnumerator_1_t1972861616 *, const MethodInfo*))InternalEnumerator_1_Dispose_m336048027_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3523763518(__this, method) ((  bool (*) (InternalEnumerator_1_t1972861616 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3523763518_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::get_Current()
#define InternalEnumerator_1_get_Current_m2653371815(__this, method) ((  Entry_t1114109354  (*) (InternalEnumerator_1_t1972861616 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2653371815_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158(__this, method) ((  void (*) (InternalEnumerator_1_t1972861616 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1972861616 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1851236734(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t219164346 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1851236734_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::Dispose()
#define InternalEnumerator_1_Dispose_m3390693617(__this, method) ((  void (*) (InternalEnumerator_1_t219164346 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3390693617_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3547199662(__this, method) ((  bool (*) (InternalEnumerator_1_t219164346 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3547199662_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::get_Current()
#define InternalEnumerator_1_get_Current_m840252737(__this, method) ((  Entry_t3655379380  (*) (InternalEnumerator_1_t219164346 *, const MethodInfo*))InternalEnumerator_1_get_Current_m840252737_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506(__this, method) ((  void (*) (InternalEnumerator_1_t219164346 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t219164346 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m981450495(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t836736193 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m981450495_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m3328202350(__this, method) ((  void (*) (InternalEnumerator_1_t836736193 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3328202350_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1076783627(__this, method) ((  bool (*) (InternalEnumerator_1_t836736193 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1076783627_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m1718349432(__this, method) ((  Entry_t4272951227  (*) (InternalEnumerator_1_t836736193 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1718349432_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303(__this, method) ((  void (*) (InternalEnumerator_1_t836736193 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t836736193 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m453027211(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t303677782 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m453027211_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::Dispose()
#define InternalEnumerator_1_Dispose_m4125815062(__this, method) ((  void (*) (InternalEnumerator_1_t303677782 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4125815062_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3692091135(__this, method) ((  bool (*) (InternalEnumerator_1_t303677782 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3692091135_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::get_Current()
#define InternalEnumerator_1_get_Current_m2987216314(__this, method) ((  Entry_t3739892816  (*) (InternalEnumerator_1_t303677782 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2987216314_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715(__this, method) ((  void (*) (InternalEnumerator_1_t303677782 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t303677782 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2417538736(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2342494975 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2417538736_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
#define InternalEnumerator_1_Dispose_m195176279(__this, method) ((  void (*) (InternalEnumerator_1_t2342494975 *, const MethodInfo*))InternalEnumerator_1_Dispose_m195176279_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m322636248(__this, method) ((  bool (*) (InternalEnumerator_1_t2342494975 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m322636248_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
#define InternalEnumerator_1_get_Current_m1154723631(__this, method) ((  KeyValuePair_2_t1483742713  (*) (InternalEnumerator_1_t2342494975 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1154723631_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536(__this, method) ((  void (*) (InternalEnumerator_1_t2342494975 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2342494975 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1754127910(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t839262761 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1754127910_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m1037069(__this, method) ((  void (*) (InternalEnumerator_1_t839262761 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1037069_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4073508358(__this, method) ((  bool (*) (InternalEnumerator_1_t839262761 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4073508358_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m2948346621(__this, method) ((  KeyValuePair_2_t4275477795  (*) (InternalEnumerator_1_t839262761 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2948346621_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986(__this, method) ((  void (*) (InternalEnumerator_1_t839262761 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t839262761 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3441346029(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t313372414 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3441346029_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m718416578(__this, method) ((  void (*) (InternalEnumerator_1_t313372414 *, const MethodInfo*))InternalEnumerator_1_Dispose_m718416578_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1791963761(__this, method) ((  bool (*) (InternalEnumerator_1_t313372414 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1791963761_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m3582710858(__this, method) ((  KeyValuePair_2_t3749587448  (*) (InternalEnumerator_1_t313372414 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3582710858_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(__this, method) ((  void (*) (InternalEnumerator_1_t313372414 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t313372414 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m920047666(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m920047666_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m1628196379(__this, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1628196379_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m930150026(__this, method) ((  bool (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m930150026_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m1526874475(__this, method) ((  KeyValuePair_2_t888819835  (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1526874475_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466(__this, method) ((  void (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1747572097 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m967618647(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2033732330 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m967618647_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
#define InternalEnumerator_1_Dispose_m318835130(__this, method) ((  void (*) (InternalEnumerator_1_t2033732330 *, const MethodInfo*))InternalEnumerator_1_Dispose_m318835130_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4294226955(__this, method) ((  bool (*) (InternalEnumerator_1_t2033732330 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4294226955_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
#define InternalEnumerator_1_get_Current_m3900993294(__this, method) ((  KeyValuePair_2_t1174980068  (*) (InternalEnumerator_1_t2033732330 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3900993294_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031(__this, method) ((  void (*) (InternalEnumerator_1_t2033732330 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2033732330 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3362782841(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t280035060 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3362782841_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
#define InternalEnumerator_1_Dispose_m1748410190(__this, method) ((  void (*) (InternalEnumerator_1_t280035060 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1748410190_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3486952605(__this, method) ((  bool (*) (InternalEnumerator_1_t280035060 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3486952605_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
#define InternalEnumerator_1_get_Current_m2882946014(__this, method) ((  KeyValuePair_2_t3716250094  (*) (InternalEnumerator_1_t280035060 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2882946014_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269(__this, method) ((  void (*) (InternalEnumerator_1_t280035060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t280035060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3587374424(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t897606907 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3587374424_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
#define InternalEnumerator_1_Dispose_m2413981551(__this, method) ((  void (*) (InternalEnumerator_1_t897606907 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2413981551_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1667794624(__this, method) ((  bool (*) (InternalEnumerator_1_t897606907 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1667794624_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
#define InternalEnumerator_1_get_Current_m2345377791(__this, method) ((  KeyValuePair_2_t38854645  (*) (InternalEnumerator_1_t897606907 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2345377791_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392(__this, method) ((  void (*) (InternalEnumerator_1_t897606907 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t897606907 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3950184714(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t364548496 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3950184714_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::Dispose()
#define InternalEnumerator_1_Dispose_m2915635719(__this, method) ((  void (*) (InternalEnumerator_1_t364548496 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2915635719_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3284769442(__this, method) ((  bool (*) (InternalEnumerator_1_t364548496 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3284769442_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::get_Current()
#define InternalEnumerator_1_get_Current_m358792963(__this, method) ((  KeyValuePair_2_t3800763530  (*) (InternalEnumerator_1_t364548496 *, const MethodInfo*))InternalEnumerator_1_get_Current_m358792963_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514(__this, method) ((  void (*) (InternalEnumerator_1_t364548496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t364548496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m645344728(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1835343917 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m645344728_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::Dispose()
#define InternalEnumerator_1_Dispose_m3737488171(__this, method) ((  void (*) (InternalEnumerator_1_t1835343917 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3737488171_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2095753328(__this, method) ((  bool (*) (InternalEnumerator_1_t1835343917 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2095753328_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::get_Current()
#define InternalEnumerator_1_get_Current_m1021008655(__this, method) ((  bucket_t976591655  (*) (InternalEnumerator_1_t1835343917 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1021008655_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896(__this, method) ((  void (*) (InternalEnumerator_1_t1835343917 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1835343917 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4085858451(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1027194178 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4085858451_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::Dispose()
#define InternalEnumerator_1_Dispose_m2936110260(__this, method) ((  void (*) (InternalEnumerator_1_t1027194178 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2936110260_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3616673367(__this, method) ((  bool (*) (InternalEnumerator_1_t1027194178 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3616673367_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::get_Current()
#define InternalEnumerator_1_get_Current_m419194570(__this, method) ((  AttributeEntry_t168441916  (*) (InternalEnumerator_1_t1027194178 *, const MethodInfo*))InternalEnumerator_1_get_Current_m419194570_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019(__this, method) ((  void (*) (InternalEnumerator_1_t1027194178 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1027194178 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m245588437(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1551957931 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m245588437_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
#define InternalEnumerator_1_Dispose_m3383574608(__this, method) ((  void (*) (InternalEnumerator_1_t1551957931 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3383574608_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3300932033(__this, method) ((  bool (*) (InternalEnumerator_1_t1551957931 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3300932033_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
#define InternalEnumerator_1_get_Current_m4279678504(__this, method) ((  DateTime_t693205669  (*) (InternalEnumerator_1_t1551957931 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4279678504_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777(__this, method) ((  void (*) (InternalEnumerator_1_t1551957931 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1551957931 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m966844380(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2221741168 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m966844380_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::Dispose()
#define InternalEnumerator_1_Dispose_m2889182185(__this, method) ((  void (*) (InternalEnumerator_1_t2221741168 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2889182185_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTimeOffset>::MoveNext()
#define InternalEnumerator_1_MoveNext_m276280428(__this, method) ((  bool (*) (InternalEnumerator_1_t2221741168 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m276280428_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTimeOffset>::get_Current()
#define InternalEnumerator_1_get_Current_m1776564649(__this, method) ((  DateTimeOffset_t1362988906  (*) (InternalEnumerator_1_t2221741168 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1776564649_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992(__this, method) ((  void (*) (InternalEnumerator_1_t2221741168 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2221741168 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4150855019(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1583453339 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4150855019_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
#define InternalEnumerator_1_Dispose_m3407567388(__this, method) ((  void (*) (InternalEnumerator_1_t1583453339 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3407567388_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4134231455(__this, method) ((  bool (*) (InternalEnumerator_1_t1583453339 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4134231455_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
#define InternalEnumerator_1_get_Current_m245025210(__this, method) ((  Decimal_t724701077  (*) (InternalEnumerator_1_t1583453339 *, const MethodInfo*))InternalEnumerator_1_get_Current_m245025210_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955(__this, method) ((  void (*) (InternalEnumerator_1_t1583453339 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1583453339 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3589241961(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t641800647 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3589241961_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
#define InternalEnumerator_1_Dispose_m3578333724(__this, method) ((  void (*) (InternalEnumerator_1_t641800647 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3578333724_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
#define InternalEnumerator_1_MoveNext_m83303365(__this, method) ((  bool (*) (InternalEnumerator_1_t641800647 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m83303365_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
#define InternalEnumerator_1_get_Current_m1389169756(__this, method) ((  double (*) (InternalEnumerator_1_t641800647 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1389169756_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029(__this, method) ((  void (*) (InternalEnumerator_1_t641800647 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t641800647 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m565351307(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2600861628 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m565351307_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::Dispose()
#define InternalEnumerator_1_Dispose_m3886016744(__this, method) ((  void (*) (InternalEnumerator_1_t2600861628 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3886016744_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3943379735(__this, method) ((  bool (*) (InternalEnumerator_1_t2600861628 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3943379735_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::get_Current()
#define InternalEnumerator_1_get_Current_m3667052076(__this, method) ((  InternalCodePageDataItem_t1742109366  (*) (InternalEnumerator_1_t2600861628 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3667052076_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907(__this, method) ((  void (*) (InternalEnumerator_1_t2600861628 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2600861628 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m298297972(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t941671943 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m298297972_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::Dispose()
#define InternalEnumerator_1_Dispose_m3568777087(__this, method) ((  void (*) (InternalEnumerator_1_t941671943 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3568777087_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3256434108(__this, method) ((  bool (*) (InternalEnumerator_1_t941671943 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3256434108_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::get_Current()
#define InternalEnumerator_1_get_Current_m1145634683(__this, method) ((  InternalEncodingDataItem_t82919681  (*) (InternalEnumerator_1_t941671943 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1145634683_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140(__this, method) ((  void (*) (InternalEnumerator_1_t941671943 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t941671943 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3204909454(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2890411629 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3204909454_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::Dispose()
#define InternalEnumerator_1_Dispose_m3020383423(__this, method) ((  void (*) (InternalEnumerator_1_t2890411629 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3020383423_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2897730678(__this, method) ((  bool (*) (InternalEnumerator_1_t2890411629 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2897730678_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::get_Current()
#define InternalEnumerator_1_get_Current_m1471150671(__this, method) ((  TimeSpanToken_t2031659367  (*) (InternalEnumerator_1_t2890411629 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1471150671_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830(__this, method) ((  void (*) (InternalEnumerator_1_t2890411629 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2890411629 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Guid>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m702435723(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3392353855 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m702435723_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Guid>::Dispose()
#define InternalEnumerator_1_Dispose_m1976167928(__this, method) ((  void (*) (InternalEnumerator_1_t3392353855 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1976167928_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Guid>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1380038135(__this, method) ((  bool (*) (InternalEnumerator_1_t3392353855 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1380038135_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Guid>::get_Current()
#define InternalEnumerator_1_get_Current_m3939996428(__this, method) ((  Guid_t  (*) (InternalEnumerator_1_t3392353855 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3939996428_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299(__this, method) ((  void (*) (InternalEnumerator_1_t3392353855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3392353855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m557239862(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t605030880 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m557239862_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
#define InternalEnumerator_1_Dispose_m2743309309(__this, method) ((  void (*) (InternalEnumerator_1_t605030880 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2743309309_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4274987126(__this, method) ((  bool (*) (InternalEnumerator_1_t605030880 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4274987126_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
#define InternalEnumerator_1_get_Current_m3259181373(__this, method) ((  int16_t (*) (InternalEnumerator_1_t605030880 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3259181373_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594(__this, method) ((  void (*) (InternalEnumerator_1_t605030880 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t605030880 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m504913220(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2930629710 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m504913220_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
#define InternalEnumerator_1_Dispose_m3393096515(__this, method) ((  void (*) (InternalEnumerator_1_t2930629710 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3393096515_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3679487948(__this, method) ((  bool (*) (InternalEnumerator_1_t2930629710 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3679487948_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
#define InternalEnumerator_1_get_Current_m10285187(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2930629710 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10285187_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860(__this, method) ((  void (*) (InternalEnumerator_1_t2930629710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2930629710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2597133905(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1767830299 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2597133905_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
#define InternalEnumerator_1_Dispose_m307741520(__this, method) ((  void (*) (InternalEnumerator_1_t1767830299 *, const MethodInfo*))InternalEnumerator_1_Dispose_m307741520_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1683120485(__this, method) ((  bool (*) (InternalEnumerator_1_t1767830299 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1683120485_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
#define InternalEnumerator_1_get_Current_m2415979394(__this, method) ((  int64_t (*) (InternalEnumerator_1_t1767830299 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2415979394_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197(__this, method) ((  void (*) (InternalEnumerator_1_t1767830299 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1767830299 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1648185761(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3362812871 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1648185761_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
#define InternalEnumerator_1_Dispose_m3933737284(__this, method) ((  void (*) (InternalEnumerator_1_t3362812871 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3933737284_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2720582493(__this, method) ((  bool (*) (InternalEnumerator_1_t3362812871 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2720582493_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
#define InternalEnumerator_1_get_Current_m1706492988(__this, method) ((  IntPtr_t (*) (InternalEnumerator_1_t3362812871 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1706492988_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733(__this, method) ((  void (*) (InternalEnumerator_1_t3362812871 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3362812871 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m926514125(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t494314080 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m926514125_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m676949342(__this, method) ((  void (*) (InternalEnumerator_1_t494314080 *, const MethodInfo*))InternalEnumerator_1_Dispose_m676949342_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1352446537(__this, method) ((  bool (*) (InternalEnumerator_1_t494314080 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1352446537_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m3788757124(__this, method) ((  RecognizedAttribute_t3930529114  (*) (InternalEnumerator_1_t494314080 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3788757124_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617(__this, method) ((  void (*) (InternalEnumerator_1_t494314080 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t494314080 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1019972839(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2917548534 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1019972839_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m1171085184(__this, method) ((  void (*) (InternalEnumerator_1_t2917548534 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1171085184_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2708971515(__this, method) ((  bool (*) (InternalEnumerator_1_t2917548534 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2708971515_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m2429470294(__this, method) ((  HeaderVariantInfo_t2058796272  (*) (InternalEnumerator_1_t2917548534 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2429470294_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479(__this, method) ((  void (*) (InternalEnumerator_1_t2917548534 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2917548534 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2945276195(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3058064956 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2945276195_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::Dispose()
#define InternalEnumerator_1_Dispose_m1866063914(__this, method) ((  void (*) (InternalEnumerator_1_t3058064956 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1866063914_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2957122063(__this, method) ((  bool (*) (InternalEnumerator_1_t3058064956 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2957122063_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::get_Current()
#define InternalEnumerator_1_get_Current_m1669430788(__this, method) ((  WSABUF_t2199312694  (*) (InternalEnumerator_1_t3058064956 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1669430788_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155(__this, method) ((  void (*) (InternalEnumerator_1_t3058064956 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3058064956 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1608269208(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2275375023 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1608269208_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::Dispose()
#define InternalEnumerator_1_Dispose_m1743466569(__this, method) ((  void (*) (InternalEnumerator_1_t2275375023 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1743466569_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::MoveNext()
#define InternalEnumerator_1_MoveNext_m706855480(__this, method) ((  bool (*) (InternalEnumerator_1_t2275375023 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m706855480_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::get_Current()
#define InternalEnumerator_1_get_Current_m419131001(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t2275375023 *, const MethodInfo*))InternalEnumerator_1_get_Current_m419131001_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284(__this, method) ((  void (*) (InternalEnumerator_1_t2275375023 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2275375023 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m853313801(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3548201557 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
#define InternalEnumerator_1_Dispose_m1636767846(__this, method) ((  void (*) (InternalEnumerator_1_t3548201557 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1047150157(__this, method) ((  bool (*) (InternalEnumerator_1_t3548201557 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
#define InternalEnumerator_1_get_Current_m3206960238(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3548201557 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213(__this, method) ((  void (*) (InternalEnumerator_1_t3548201557 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3548201557 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1626855592(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3806268713 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1626855592_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::Dispose()
#define InternalEnumerator_1_Dispose_m1387288299(__this, method) ((  void (*) (InternalEnumerator_1_t3806268713 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1387288299_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2783536832(__this, method) ((  bool (*) (InternalEnumerator_1_t3806268713 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2783536832_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::get_Current()
#define InternalEnumerator_1_get_Current_m421370399(__this, method) ((  FormatParam_t2947516451  (*) (InternalEnumerator_1_t3806268713 *, const MethodInfo*))InternalEnumerator_1_get_Current_m421370399_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296(__this, method) ((  void (*) (InternalEnumerator_1_t3806268713 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3806268713 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m492779768(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t952909805 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m492779768_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
#define InternalEnumerator_1_Dispose_m238246335(__this, method) ((  void (*) (InternalEnumerator_1_t952909805 *, const MethodInfo*))InternalEnumerator_1_Dispose_m238246335_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1548080384(__this, method) ((  bool (*) (InternalEnumerator_1_t952909805 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1548080384_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
#define InternalEnumerator_1_get_Current_m1089848479(__this, method) ((  CustomAttributeNamedArgument_t94157543  (*) (InternalEnumerator_1_t952909805 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1089848479_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096(__this, method) ((  void (*) (InternalEnumerator_1_t952909805 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t952909805 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m821424641(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2356950176 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m821424641_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
#define InternalEnumerator_1_Dispose_m4038440306(__this, method) ((  void (*) (InternalEnumerator_1_t2356950176 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4038440306_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2904932349(__this, method) ((  bool (*) (InternalEnumerator_1_t2356950176 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2904932349_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
#define InternalEnumerator_1_get_Current_m1047712960(__this, method) ((  CustomAttributeTypedArgument_t1498197914  (*) (InternalEnumerator_1_t2356950176 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1047712960_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805(__this, method) ((  void (*) (InternalEnumerator_1_t2356950176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2356950176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3294994445(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2901227451 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3294994445_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::Dispose()
#define InternalEnumerator_1_Dispose_m1287369282(__this, method) ((  void (*) (InternalEnumerator_1_t2901227451 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1287369282_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1560801521(__this, method) ((  bool (*) (InternalEnumerator_1_t2901227451 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1560801521_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::get_Current()
#define InternalEnumerator_1_get_Current_m451997186(__this, method) ((  ILExceptionBlock_t2042475189  (*) (InternalEnumerator_1_t2901227451 *, const MethodInfo*))InternalEnumerator_1_get_Current_m451997186_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881(__this, method) ((  void (*) (InternalEnumerator_1_t2901227451 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2901227451 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1398783858(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2348906860 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1398783858_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m2657406875(__this, method) ((  void (*) (InternalEnumerator_1_t2348906860 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2657406875_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2180350026(__this, method) ((  bool (*) (InternalEnumerator_1_t2348906860 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2180350026_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m4010463147(__this, method) ((  ILExceptionInfo_t1490154598  (*) (InternalEnumerator_1_t2348906860 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4010463147_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610(__this, method) ((  void (*) (InternalEnumerator_1_t2348906860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2348906860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3323962057(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t275897710 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3323962057_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
#define InternalEnumerator_1_Dispose_m549215360(__this, method) ((  void (*) (InternalEnumerator_1_t275897710 *, const MethodInfo*))InternalEnumerator_1_Dispose_m549215360_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3389738333(__this, method) ((  bool (*) (InternalEnumerator_1_t275897710 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3389738333_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
#define InternalEnumerator_1_get_Current_m3922357178(__this, method) ((  LabelData_t3712112744  (*) (InternalEnumerator_1_t275897710 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3922357178_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037(__this, method) ((  void (*) (InternalEnumerator_1_t275897710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t275897710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3228997263(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t654694480 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3228997263_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
#define InternalEnumerator_1_Dispose_m3927915442(__this, method) ((  void (*) (InternalEnumerator_1_t654694480 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3927915442_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4292005299(__this, method) ((  bool (*) (InternalEnumerator_1_t654694480 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4292005299_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
#define InternalEnumerator_1_get_Current_m2468740214(__this, method) ((  LabelFixup_t4090909514  (*) (InternalEnumerator_1_t654694480 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2468740214_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511(__this, method) ((  void (*) (InternalEnumerator_1_t654694480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t654694480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3387972470(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1008311600 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3387972470_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m2056889175(__this, method) ((  void (*) (InternalEnumerator_1_t1008311600 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2056889175_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1590907854(__this, method) ((  bool (*) (InternalEnumerator_1_t1008311600 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1590907854_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m3296972783(__this, method) ((  ILTokenInfo_t149559338  (*) (InternalEnumerator_1_t1008311600 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3296972783_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750(__this, method) ((  void (*) (InternalEnumerator_1_t1008311600 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1008311600 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m750604974(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t806987626 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m750604974_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::Dispose()
#define InternalEnumerator_1_Dispose_m830280837(__this, method) ((  void (*) (InternalEnumerator_1_t806987626 *, const MethodInfo*))InternalEnumerator_1_Dispose_m830280837_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2665983630(__this, method) ((  bool (*) (InternalEnumerator_1_t806987626 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2665983630_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::get_Current()
#define InternalEnumerator_1_get_Current_m322002637(__this, method) ((  Label_t4243202660  (*) (InternalEnumerator_1_t806987626 *, const MethodInfo*))InternalEnumerator_1_get_Current_m322002637_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898(__this, method) ((  void (*) (InternalEnumerator_1_t806987626 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t806987626 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2582404099(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3986139419 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2582404099_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::Dispose()
#define InternalEnumerator_1_Dispose_m219216750(__this, method) ((  void (*) (InternalEnumerator_1_t3986139419 *, const MethodInfo*))InternalEnumerator_1_Dispose_m219216750_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3074289735(__this, method) ((  bool (*) (InternalEnumerator_1_t3986139419 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3074289735_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::get_Current()
#define InternalEnumerator_1_get_Current_m2685429706(__this, method) ((  MonoResource_t3127387157  (*) (InternalEnumerator_1_t3986139419 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2685429706_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803(__this, method) ((  void (*) (InternalEnumerator_1_t3986139419 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3986139419 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3708844878(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3326058480 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3708844878_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::Dispose()
#define InternalEnumerator_1_Dispose_m1448063359(__this, method) ((  void (*) (InternalEnumerator_1_t3326058480 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1448063359_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4230111510(__this, method) ((  bool (*) (InternalEnumerator_1_t3326058480 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4230111510_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::get_Current()
#define InternalEnumerator_1_get_Current_m4167074463(__this, method) ((  MonoWin32Resource_t2467306218  (*) (InternalEnumerator_1_t3326058480 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4167074463_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086(__this, method) ((  void (*) (InternalEnumerator_1_t3326058480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3326058480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3726334735(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3567360695 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3726334735_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::Dispose()
#define InternalEnumerator_1_Dispose_m2901155554(__this, method) ((  void (*) (InternalEnumerator_1_t3567360695 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2901155554_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3875155235(__this, method) ((  bool (*) (InternalEnumerator_1_t3567360695 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3875155235_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::get_Current()
#define InternalEnumerator_1_get_Current_m1274592126(__this, method) ((  RefEmitPermissionSet_t2708608433  (*) (InternalEnumerator_1_t3567360695 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1274592126_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087(__this, method) ((  void (*) (InternalEnumerator_1_t3567360695 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3567360695 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2890018883(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2679387182 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2890018883_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
#define InternalEnumerator_1_Dispose_m3952699776(__this, method) ((  void (*) (InternalEnumerator_1_t2679387182 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3952699776_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1594563423(__this, method) ((  bool (*) (InternalEnumerator_1_t2679387182 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1594563423_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
#define InternalEnumerator_1_get_Current_m4083613828(__this, method) ((  ParameterModifier_t1820634920  (*) (InternalEnumerator_1_t2679387182 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4083613828_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235(__this, method) ((  void (*) (InternalEnumerator_1_t2679387182 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2679387182 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2999622677(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3015143146 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2999622677_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::Dispose()
#define InternalEnumerator_1_Dispose_m1652602110(__this, method) ((  void (*) (InternalEnumerator_1_t3015143146 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1652602110_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1876372465(__this, method) ((  bool (*) (InternalEnumerator_1_t3015143146 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1876372465_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::get_Current()
#define InternalEnumerator_1_get_Current_m1643772188(__this, method) ((  ResourceLocator_t2156390884  (*) (InternalEnumerator_1_t3015143146 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1643772188_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169(__this, method) ((  void (*) (InternalEnumerator_1_t3015143146 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3015143146 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1198276628(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2733828895 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1198276628_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::Dispose()
#define InternalEnumerator_1_Dispose_m3248673743(__this, method) ((  void (*) (InternalEnumerator_1_t2733828895 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3248673743_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1104149308(__this, method) ((  bool (*) (InternalEnumerator_1_t2733828895 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1104149308_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::get_Current()
#define InternalEnumerator_1_get_Current_m1864471731(__this, method) ((  Ephemeron_t1875076633  (*) (InternalEnumerator_1_t2733828895 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1864471731_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980(__this, method) ((  void (*) (InternalEnumerator_1_t2733828895 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2733828895 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m313732903(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m313732903_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::Dispose()
#define InternalEnumerator_1_Dispose_m599783274(__this, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_Dispose_m599783274_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4248732811(__this, method) ((  bool (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4248732811_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::get_Current()
#define InternalEnumerator_1_get_Current_m1012591846(__this, method) ((  GCHandle_t3409268066  (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1012591846_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351(__this, method) ((  void (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4268020328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3149706958(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3736091384 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3149706958_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::Dispose()
#define InternalEnumerator_1_Dispose_m2801598691(__this, method) ((  void (*) (InternalEnumerator_1_t3736091384 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2801598691_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1137828214(__this, method) ((  bool (*) (InternalEnumerator_1_t3736091384 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1137828214_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::get_Current()
#define InternalEnumerator_1_get_Current_m948509279(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3736091384 *, const MethodInfo*))InternalEnumerator_1_get_Current_m948509279_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566(__this, method) ((  void (*) (InternalEnumerator_1_t3736091384 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3736091384 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1277864681(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3879563917 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1277864681_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::Dispose()
#define InternalEnumerator_1_Dispose_m1587363548(__this, method) ((  void (*) (InternalEnumerator_1_t3879563917 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1587363548_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::MoveNext()
#define InternalEnumerator_1_MoveNext_m488449285(__this, method) ((  bool (*) (InternalEnumerator_1_t3879563917 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m488449285_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::get_Current()
#define InternalEnumerator_1_get_Current_m1174598684(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3879563917 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1174598684_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653(__this, method) ((  void (*) (InternalEnumerator_1_t3879563917 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3879563917 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2108401677(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1313169811 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2108401677_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
#define InternalEnumerator_1_Dispose_m1676985532(__this, method) ((  void (*) (InternalEnumerator_1_t1313169811 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1676985532_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3984801393(__this, method) ((  bool (*) (InternalEnumerator_1_t1313169811 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3984801393_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
#define InternalEnumerator_1_get_Current_m314017974(__this, method) ((  int8_t (*) (InternalEnumerator_1_t1313169811 *, const MethodInfo*))InternalEnumerator_1_get_Current_m314017974_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193(__this, method) ((  void (*) (InternalEnumerator_1_t1313169811 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1313169811 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m655778553(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t842163687 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m655778553_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
#define InternalEnumerator_1_Dispose_m3671580532(__this, method) ((  void (*) (InternalEnumerator_1_t842163687 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3671580532_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1869236997(__this, method) ((  bool (*) (InternalEnumerator_1_t842163687 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1869236997_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
#define InternalEnumerator_1_get_Current_m1550231132(__this, method) ((  X509ChainStatus_t4278378721  (*) (InternalEnumerator_1_t842163687 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1550231132_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685(__this, method) ((  void (*) (InternalEnumerator_1_t842163687 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t842163687 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2314640734(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2935262194 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2314640734_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
#define InternalEnumerator_1_Dispose_m2195973811(__this, method) ((  void (*) (InternalEnumerator_1_t2935262194 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2195973811_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
#define InternalEnumerator_1_MoveNext_m580128774(__this, method) ((  bool (*) (InternalEnumerator_1_t2935262194 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m580128774_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
#define InternalEnumerator_1_get_Current_m727737343(__this, method) ((  float (*) (InternalEnumerator_1_t2935262194 *, const MethodInfo*))InternalEnumerator_1_get_Current_m727737343_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662(__this, method) ((  void (*) (InternalEnumerator_1_t2935262194 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2935262194 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2853126416(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2284019382 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2853126416_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::Dispose()
#define InternalEnumerator_1_Dispose_m30795463(__this, method) ((  void (*) (InternalEnumerator_1_t2284019382 *, const MethodInfo*))InternalEnumerator_1_Dispose_m30795463_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TermInfoStrings>::MoveNext()
#define InternalEnumerator_1_MoveNext_m138185816(__this, method) ((  bool (*) (InternalEnumerator_1_t2284019382 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m138185816_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TermInfoStrings>::get_Current()
#define InternalEnumerator_1_get_Current_m3362979103(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2284019382 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3362979103_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920(__this, method) ((  void (*) (InternalEnumerator_1_t2284019382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2284019382 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2565240337(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3012688088 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2565240337_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::Dispose()
#define InternalEnumerator_1_Dispose_m3391663714(__this, method) ((  void (*) (InternalEnumerator_1_t3012688088 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3391663714_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2599112621(__this, method) ((  bool (*) (InternalEnumerator_1_t3012688088 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2599112621_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::get_Current()
#define InternalEnumerator_1_get_Current_m466738280(__this, method) ((  LowerCaseMapping_t2153935826  (*) (InternalEnumerator_1_t3012688088 *, const MethodInfo*))InternalEnumerator_1_get_Current_m466738280_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901(__this, method) ((  void (*) (InternalEnumerator_1_t3012688088 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3012688088 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m974809591(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3277011989 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m974809591_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::Dispose()
#define InternalEnumerator_1_Dispose_m779531596(__this, method) ((  void (*) (InternalEnumerator_1_t3277011989 *, const MethodInfo*))InternalEnumerator_1_Dispose_m779531596_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2770905747(__this, method) ((  bool (*) (InternalEnumerator_1_t3277011989 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2770905747_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::get_Current()
#define InternalEnumerator_1_get_Current_m2578701872(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3277011989 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2578701872_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183(__this, method) ((  void (*) (InternalEnumerator_1_t3277011989 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3277011989 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2874479637(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2567611619 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2874479637_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::Dispose()
#define InternalEnumerator_1_Dispose_m2776097548(__this, method) ((  void (*) (InternalEnumerator_1_t2567611619 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2776097548_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3251308169(__this, method) ((  bool (*) (InternalEnumerator_1_t2567611619 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3251308169_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::get_Current()
#define InternalEnumerator_1_get_Current_m3338380422(__this, method) ((  CancellationTokenRegistration_t1708859357  (*) (InternalEnumerator_1_t2567611619 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3338380422_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777(__this, method) ((  void (*) (InternalEnumerator_1_t2567611619 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2567611619 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2189699457(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4289011211 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2189699457_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
#define InternalEnumerator_1_Dispose_m3838127340(__this, method) ((  void (*) (InternalEnumerator_1_t4289011211 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3838127340_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1674480765(__this, method) ((  bool (*) (InternalEnumerator_1_t4289011211 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1674480765_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
#define InternalEnumerator_1_get_Current_m3411759116(__this, method) ((  TimeSpan_t3430258949  (*) (InternalEnumerator_1_t4289011211 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3411759116_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421(__this, method) ((  void (*) (InternalEnumerator_1_t4289011211 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4289011211 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m200749079(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2714516328 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m200749079_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::Dispose()
#define InternalEnumerator_1_Dispose_m2957730742(__this, method) ((  void (*) (InternalEnumerator_1_t2714516328 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2957730742_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m4246357971(__this, method) ((  bool (*) (InternalEnumerator_1_t2714516328 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4246357971_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::get_Current()
#define InternalEnumerator_1_get_Current_m2840641472(__this, method) ((  TZifType_t1855764066  (*) (InternalEnumerator_1_t2714516328 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2840641472_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695(__this, method) ((  void (*) (InternalEnumerator_1_t2714516328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2714516328 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2264046229(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3395678463 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2264046229_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
#define InternalEnumerator_1_Dispose_m1071940184(__this, method) ((  void (*) (InternalEnumerator_1_t3395678463 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1071940184_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3081678193(__this, method) ((  bool (*) (InternalEnumerator_1_t3395678463 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3081678193_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
#define InternalEnumerator_1_get_Current_m3431860728(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3395678463 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3431860728_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385(__this, method) ((  void (*) (InternalEnumerator_1_t3395678463 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3395678463 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2981879621(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1845634873 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2981879621_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
#define InternalEnumerator_1_Dispose_m1824402698(__this, method) ((  void (*) (InternalEnumerator_1_t1845634873 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1824402698_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2809569305(__this, method) ((  bool (*) (InternalEnumerator_1_t1845634873 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2809569305_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
#define InternalEnumerator_1_get_Current_m3179981210(__this, method) ((  uint16_t (*) (InternalEnumerator_1_t1845634873 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3179981210_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313(__this, method) ((  void (*) (InternalEnumerator_1_t1845634873 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1845634873 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m691972083(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3008434283 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m691972083_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
#define InternalEnumerator_1_Dispose_m2620838688(__this, method) ((  void (*) (InternalEnumerator_1_t3008434283 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2620838688_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
#define InternalEnumerator_1_MoveNext_m470170271(__this, method) ((  bool (*) (InternalEnumerator_1_t3008434283 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m470170271_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
#define InternalEnumerator_1_get_Current_m2198364332(__this, method) ((  uint32_t (*) (InternalEnumerator_1_t3008434283 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2198364332_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851(__this, method) ((  void (*) (InternalEnumerator_1_t3008434283 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3008434283 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3084132532(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3767949176 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3084132532_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
#define InternalEnumerator_1_Dispose_m3642485841(__this, method) ((  void (*) (InternalEnumerator_1_t3767949176 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3642485841_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2954283444(__this, method) ((  bool (*) (InternalEnumerator_1_t3767949176 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2954283444_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
#define InternalEnumerator_1_get_Current_m35328337(__this, method) ((  uint64_t (*) (InternalEnumerator_1_t3767949176 *, const MethodInfo*))InternalEnumerator_1_get_Current_m35328337_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888(__this, method) ((  void (*) (InternalEnumerator_1_t3767949176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3767949176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1255728717_gshared (Action_1_t981956716 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m215641981_gshared (Action_1_t981956716 * __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___obj0,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3341085586_gshared (Action_1_t981956716 * __this, AsyncLocalValueChangedArgs_1_t1180157334  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3341085586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AsyncLocalValueChangedArgs_1_t1180157334_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Threading.AsyncLocalValueChangedArgs`1<System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2987897115_gshared (Action_1_t981956716 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2424659357_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1202825274_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	DelegateU5BU5D_t1606206610* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		uint32_t length = delegatesToInvoke->max_length;
		for (uint32_t i = 0; i < length; i++)
		{
			Delegate_t3022476291* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			if (currentDelegate->get_m_target_2() != NULL && ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(NULL,currentDelegate->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else if (currentDelegate->get_m_target_2() != NULL || ___methodIsStatic)
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(currentDelegate->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
			else
			{
				typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
				((FunctionPointerType)currentDelegate->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(currentDelegate->get_method_3().get_m_value_0()));
			}
		}
	}
	else
	{
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
		if (__this->get_m_target_2() != NULL && ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
		else
		{
			typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
			((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
		}
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1129022497_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1767361515_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/FunctorComparer`1<System.Boolean>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2441335022_gshared (FunctorComparer_1_t610337993 * __this, Comparison_1_t792346273 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t792346273 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Boolean>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m1143243666_gshared (FunctorComparer_1_t610337993 * __this, bool ___x0, bool ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t792346273 * L_0 = (Comparison_1_t792346273 *)__this->get_comparison_0();
		bool L_1 = ___x0;
		bool L_2 = ___y1;
		NullCheck((Comparison_1_t792346273 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t792346273 *, bool, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t792346273 *)L_0, (bool)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Byte>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1122381900_gshared (FunctorComparer_1_t467867711 * __this, Comparison_1_t649875991 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t649875991 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Byte>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3004152508_gshared (FunctorComparer_1_t467867711 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t649875991 * L_0 = (Comparison_1_t649875991 *)__this->get_comparison_0();
		uint8_t L_1 = ___x0;
		uint8_t L_2 = ___y1;
		NullCheck((Comparison_1_t649875991 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t649875991 *, uint8_t, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t649875991 *)L_0, (uint8_t)L_1, (uint8_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.DateTime>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2936093271_gshared (FunctorComparer_1_t1772936240 * __this, Comparison_1_t1954944520 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t1954944520 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.DateTime>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3251800711_gshared (FunctorComparer_1_t1772936240 * __this, DateTime_t693205669  ___x0, DateTime_t693205669  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t1954944520 * L_0 = (Comparison_1_t1954944520 *)__this->get_comparison_0();
		DateTime_t693205669  L_1 = ___x0;
		DateTime_t693205669  L_2 = ___y1;
		NullCheck((Comparison_1_t1954944520 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t1954944520 *, DateTime_t693205669 , DateTime_t693205669 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t1954944520 *)L_0, (DateTime_t693205669 )L_1, (DateTime_t693205669 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.DateTimeOffset>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2272496908_gshared (FunctorComparer_1_t2442719477 * __this, Comparison_1_t2624727757 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t2624727757 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2363979184_gshared (FunctorComparer_1_t2442719477 * __this, DateTimeOffset_t1362988906  ___x0, DateTimeOffset_t1362988906  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t2624727757 * L_0 = (Comparison_1_t2624727757 *)__this->get_comparison_0();
		DateTimeOffset_t1362988906  L_1 = ___x0;
		DateTimeOffset_t1362988906  L_2 = ___y1;
		NullCheck((Comparison_1_t2624727757 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t2624727757 *, DateTimeOffset_t1362988906 , DateTimeOffset_t1362988906 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t2624727757 *)L_0, (DateTimeOffset_t1362988906 )L_1, (DateTimeOffset_t1362988906 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Decimal>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m597670025_gshared (FunctorComparer_1_t1804431648 * __this, Comparison_1_t1986439928 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t1986439928 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Decimal>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m1308658321_gshared (FunctorComparer_1_t1804431648 * __this, Decimal_t724701077  ___x0, Decimal_t724701077  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t1986439928 * L_0 = (Comparison_1_t1986439928 *)__this->get_comparison_0();
		Decimal_t724701077  L_1 = ___x0;
		Decimal_t724701077  L_2 = ___y1;
		NullCheck((Comparison_1_t1986439928 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t1986439928 *, Decimal_t724701077 , Decimal_t724701077 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t1986439928 *)L_0, (Decimal_t724701077 )L_1, (Decimal_t724701077 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Double>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m3824923411_gshared (FunctorComparer_1_t862778956 * __this, Comparison_1_t1044787236 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t1044787236 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Double>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m1304417779_gshared (FunctorComparer_1_t862778956 * __this, double ___x0, double ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t1044787236 * L_0 = (Comparison_1_t1044787236 *)__this->get_comparison_0();
		double L_1 = ___x0;
		double L_2 = ___y1;
		NullCheck((Comparison_1_t1044787236 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t1044787236 *, double, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t1044787236 *)L_0, (double)L_1, (double)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Int16>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2510515124_gshared (FunctorComparer_1_t826009189 * __this, Comparison_1_t1008017469 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t1008017469 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Int16>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2949298240_gshared (FunctorComparer_1_t826009189 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t1008017469 * L_0 = (Comparison_1_t1008017469 *)__this->get_comparison_0();
		int16_t L_1 = ___x0;
		int16_t L_2 = ___y1;
		NullCheck((Comparison_1_t1008017469 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t1008017469 *, int16_t, int16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t1008017469 *)L_0, (int16_t)L_1, (int16_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Int32>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1140204486_gshared (FunctorComparer_1_t3151608019 * __this, Comparison_1_t3333616299 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3333616299 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Int32>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3568069242_gshared (FunctorComparer_1_t3151608019 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3333616299 * L_0 = (Comparison_1_t3333616299 *)__this->get_comparison_0();
		int32_t L_1 = ___x0;
		int32_t L_2 = ___y1;
		NullCheck((Comparison_1_t3333616299 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3333616299 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3333616299 *)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Int64>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m4271040183_gshared (FunctorComparer_1_t1988808608 * __this, Comparison_1_t2170816888 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t2170816888 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Int64>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m4070996903_gshared (FunctorComparer_1_t1988808608 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t2170816888 * L_0 = (Comparison_1_t2170816888 *)__this->get_comparison_0();
		int64_t L_1 = ___x0;
		int64_t L_2 = ___y1;
		NullCheck((Comparison_1_t2170816888 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t2170816888 *, int64_t, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t2170816888 *)L_0, (int64_t)L_1, (int64_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Object>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m942241655_gshared (FunctorComparer_1_t3769179866 * __this, Comparison_1_t3951188146 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3951188146 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Object>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2335341999_gshared (FunctorComparer_1_t3769179866 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3951188146 * L_0 = (Comparison_1_t3951188146 *)__this->get_comparison_0();
		Il2CppObject * L_1 = ___x0;
		Il2CppObject * L_2 = ___y1;
		NullCheck((Comparison_1_t3951188146 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3951188146 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3951188146 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.SByte>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m3705825103_gshared (FunctorComparer_1_t1534148120 * __this, Comparison_1_t1716156400 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t1716156400 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.SByte>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3294924871_gshared (FunctorComparer_1_t1534148120 * __this, int8_t ___x0, int8_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t1716156400 * L_0 = (Comparison_1_t1716156400 *)__this->get_comparison_0();
		int8_t L_1 = ___x0;
		int8_t L_2 = ___y1;
		NullCheck((Comparison_1_t1716156400 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t1716156400 *, int8_t, int8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t1716156400 *)L_0, (int8_t)L_1, (int8_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Single>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2412110670_gshared (FunctorComparer_1_t3156240503 * __this, Comparison_1_t3338248783 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3338248783 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Single>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2143725070_gshared (FunctorComparer_1_t3156240503 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3338248783 * L_0 = (Comparison_1_t3338248783 *)__this->get_comparison_0();
		float L_1 = ___x0;
		float L_2 = ___y1;
		NullCheck((Comparison_1_t3338248783 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3338248783 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3338248783 *)L_0, (float)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Text.RegularExpressions.RegexOptions>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m353568125_gshared (FunctorComparer_1_t3497990298 * __this, Comparison_1_t3679998578 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3679998578 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Text.RegularExpressions.RegexOptions>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2073395953_gshared (FunctorComparer_1_t3497990298 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3679998578 * L_0 = (Comparison_1_t3679998578 *)__this->get_comparison_0();
		int32_t L_1 = ___x0;
		int32_t L_2 = ___y1;
		NullCheck((Comparison_1_t3679998578 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3679998578 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3679998578 *)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.TimeSpan>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m2688347527_gshared (FunctorComparer_1_t215022224 * __this, Comparison_1_t397030504 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t397030504 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.TimeSpan>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m959236135_gshared (FunctorComparer_1_t215022224 * __this, TimeSpan_t3430258949  ___x0, TimeSpan_t3430258949  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t397030504 * L_0 = (Comparison_1_t397030504 *)__this->get_comparison_0();
		TimeSpan_t3430258949  L_1 = ___x0;
		TimeSpan_t3430258949  L_2 = ___y1;
		NullCheck((Comparison_1_t397030504 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t397030504 *, TimeSpan_t3430258949 , TimeSpan_t3430258949 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t397030504 *)L_0, (TimeSpan_t3430258949 )L_1, (TimeSpan_t3430258949 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.UInt16>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m3576002619_gshared (FunctorComparer_1_t2066613182 * __this, Comparison_1_t2248621462 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t2248621462 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.UInt16>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3086561043_gshared (FunctorComparer_1_t2066613182 * __this, uint16_t ___x0, uint16_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t2248621462 * L_0 = (Comparison_1_t2248621462 *)__this->get_comparison_0();
		uint16_t L_1 = ___x0;
		uint16_t L_2 = ___y1;
		NullCheck((Comparison_1_t2248621462 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t2248621462 *, uint16_t, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t2248621462 *)L_0, (uint16_t)L_1, (uint16_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.UInt32>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1483189997_gshared (FunctorComparer_1_t3229412592 * __this, Comparison_1_t3411420872 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3411420872 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.UInt32>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2712800937_gshared (FunctorComparer_1_t3229412592 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3411420872 * L_0 = (Comparison_1_t3411420872 *)__this->get_comparison_0();
		uint32_t L_1 = ___x0;
		uint32_t L_2 = ___y1;
		NullCheck((Comparison_1_t3411420872 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3411420872 *, uint32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3411420872 *)L_0, (uint32_t)L_1, (uint32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.UInt64>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1632888580_gshared (FunctorComparer_1_t3988927485 * __this, Comparison_1_t4170935765 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t4170935765 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.UInt64>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2944180552_gshared (FunctorComparer_1_t3988927485 * __this, uint64_t ___x0, uint64_t ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t4170935765 * L_0 = (Comparison_1_t4170935765 *)__this->get_comparison_0();
		uint64_t L_1 = ___x0;
		uint64_t L_2 = ___y1;
		NullCheck((Comparison_1_t4170935765 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t4170935765 *, uint64_t, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t4170935765 *)L_0, (uint64_t)L_1, (uint64_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Xml.Schema.RangePositionInfo>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1082168640_gshared (FunctorComparer_1_t3860533493 * __this, Comparison_1_t4042541773 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t4042541773 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Xml.Schema.RangePositionInfo>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m1594649852_gshared (FunctorComparer_1_t3860533493 * __this, RangePositionInfo_t2780802922  ___x0, RangePositionInfo_t2780802922  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t4042541773 * L_0 = (Comparison_1_t4042541773 *)__this->get_comparison_0();
		RangePositionInfo_t2780802922  L_1 = ___x0;
		RangePositionInfo_t2780802922  L_2 = ___y1;
		NullCheck((Comparison_1_t4042541773 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t4042541773 *, RangePositionInfo_t2780802922 , RangePositionInfo_t2780802922 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t4042541773 *)L_0, (RangePositionInfo_t2780802922 )L_1, (RangePositionInfo_t2780802922 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m3420588991_gshared (FunctorComparer_1_t3590317081 * __this, Comparison_1_t3772325361 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t3772325361 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m3606618103_gshared (FunctorComparer_1_t3590317081 * __this, XmlSchemaObjectEntry_t2510586510  ___x0, XmlSchemaObjectEntry_t2510586510  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t3772325361 * L_0 = (Comparison_1_t3772325361 *)__this->get_comparison_0();
		XmlSchemaObjectEntry_t2510586510  L_1 = ___x0;
		XmlSchemaObjectEntry_t2510586510  L_2 = ___y1;
		NullCheck((Comparison_1_t3772325361 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t3772325361 *, XmlSchemaObjectEntry_t2510586510 , XmlSchemaObjectEntry_t2510586510 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t3772325361 *)L_0, (XmlSchemaObjectEntry_t2510586510 )L_1, (XmlSchemaObjectEntry_t2510586510 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/FunctorComparer`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Comparison`1<T>)
extern "C"  void FunctorComparer_1__ctor_m1020464289_gshared (FunctorComparer_1_t690514624 * __this, Comparison_1_t872522904 * ___comparison0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Comparison_1_t872522904 * L_0 = ___comparison0;
		__this->set_comparison_0(L_0);
		return;
	}
}
// System.Int32 System.Array/FunctorComparer`1<UnityEngine.AnimatorClipInfo>::Compare(T,T)
extern "C"  int32_t FunctorComparer_1_Compare_m2947816697_gshared (FunctorComparer_1_t690514624 * __this, AnimatorClipInfo_t3905751349  ___x0, AnimatorClipInfo_t3905751349  ___y1, const MethodInfo* method)
{
	{
		Comparison_1_t872522904 * L_0 = (Comparison_1_t872522904 *)__this->get_comparison_0();
		AnimatorClipInfo_t3905751349  L_1 = ___x0;
		AnimatorClipInfo_t3905751349  L_2 = ___y1;
		NullCheck((Comparison_1_t872522904 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Comparison_1_t872522904 *, AnimatorClipInfo_t3905751349 , AnimatorClipInfo_t3905751349 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparison_1_t872522904 *)L_0, (AnimatorClipInfo_t3905751349 )L_1, (AnimatorClipInfo_t3905751349 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_3;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853419099_gshared (InternalEnumerator_1_t793236484 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m853419099_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	InternalEnumerator_1__ctor_m853419099(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1975796854_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1975796854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1975796854(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3711981423_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3711981423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3711981423(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3424295274_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3424295274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3424295274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3424295274(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3098871067(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Interface.CipherSuiteCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619_gshared (InternalEnumerator_1_t793236484 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m3424295274((InternalEnumerator_1_t793236484 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t793236484 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t793236484 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2727387619(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m815597740_gshared (InternalEnumerator_1_t1542250127 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m815597740_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1__ctor_m815597740(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m766702873_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m766702873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1_Dispose_m766702873(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2738761868_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2738761868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2738761868(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::get_Current()
extern "C"  UriScheme_t683497865  InternalEnumerator_1_get_Current_m3577122241_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3577122241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t683497865  L_8 = ((  UriScheme_t683497865  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  UriScheme_t683497865  InternalEnumerator_1_get_Current_m3577122241_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3577122241(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		UriScheme_t683497865  L_0 = InternalEnumerator_1_get_Current_m3577122241((InternalEnumerator_1_t1542250127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UriScheme_t683497865  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3692927446_gshared (InternalEnumerator_1_t3977134117 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3692927446_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	InternalEnumerator_1__ctor_m3692927446(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m178761275_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m178761275_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	InternalEnumerator_1_Dispose_m178761275(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2742061742_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2742061742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2742061742(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::get_Current()
extern "C"  XPathNode_t3118381855  InternalEnumerator_1_get_Current_m3611165223_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3611165223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		XPathNode_t3118381855  L_8 = ((  XPathNode_t3118381855  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  XPathNode_t3118381855  InternalEnumerator_1_get_Current_m3611165223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3611165223(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1871230230(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456_gshared (InternalEnumerator_1_t3977134117 * __this, const MethodInfo* method)
{
	{
		XPathNode_t3118381855  L_0 = InternalEnumerator_1_get_Current_m3611165223((InternalEnumerator_1_t3977134117 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		XPathNode_t3118381855  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3977134117 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3977134117 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4202015456(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1918766407_gshared (InternalEnumerator_1_t2951357404 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1918766407_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	InternalEnumerator_1__ctor_m1918766407(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1171545318_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1171545318_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1171545318(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m21981123_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m21981123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m21981123(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::get_Current()
extern "C"  XPathNodeRef_t2092605142  InternalEnumerator_1_get_Current_m3003696592_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3003696592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		XPathNodeRef_t2092605142  L_8 = ((  XPathNodeRef_t2092605142  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  XPathNodeRef_t2092605142  InternalEnumerator_1_get_Current_m3003696592_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3003696592(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1540625943(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.Cache.XPathNodeRef>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075_gshared (InternalEnumerator_1_t2951357404 * __this, const MethodInfo* method)
{
	{
		XPathNodeRef_t2092605142  L_0 = InternalEnumerator_1_get_Current_m3003696592((InternalEnumerator_1_t2951357404 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		XPathNodeRef_t2092605142  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2951357404 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2951357404 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4041972075(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1169622682_gshared (InternalEnumerator_1_t2690501253 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1169622682_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	InternalEnumerator_1__ctor_m1169622682(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2160474007_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2160474007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2160474007(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3508153682_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3508153682_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3508153682(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m706977411_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m706977411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m706977411_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m706977411(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2042075506(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<MS.Internal.Xml.XPath.Operator/Op>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060_gshared (InternalEnumerator_1_t2690501253 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m706977411((InternalEnumerator_1_t2690501253 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2690501253 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2690501253 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1712462060(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1866922360_gshared (InternalEnumerator_1_t3452969744 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1866922360_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1__ctor_m1866922360(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m592267945_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m592267945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_Dispose_m592267945(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1460734872(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2594217482  L_8 = ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1894741129(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2594217482  L_0 = InternalEnumerator_1_get_Current_m1894741129((InternalEnumerator_1_t3452969744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ArraySegment_1_t2594217482  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m421278255_gshared (InternalEnumerator_1_t2281624261 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m421278255_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	InternalEnumerator_1__ctor_m421278255(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3790093886_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3790093886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3790093886(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1016442011_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1016442011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1016442011(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
extern "C"  Entry_t1422871999  InternalEnumerator_1_get_Current_m3542984528_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3542984528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t1422871999  L_8 = ((  Entry_t1422871999  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t1422871999  InternalEnumerator_1_get_Current_m3542984528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3542984528(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m624576735(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123_gshared (InternalEnumerator_1_t2281624261 * __this, const MethodInfo* method)
{
	{
		Entry_t1422871999  L_0 = InternalEnumerator_1_get_Current_m3542984528((InternalEnumerator_1_t2281624261 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t1422871999  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2281624261 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2281624261 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m6478123(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2192168017_gshared (InternalEnumerator_1_t778392047 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2192168017_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	InternalEnumerator_1__ctor_m2192168017(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1447718640_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1447718640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1447718640(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2474607397_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2474607397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2474607397(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::get_Current()
extern "C"  Entry_t4214607081  InternalEnumerator_1_get_Current_m1304018714_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1304018714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t4214607081  L_8 = ((  Entry_t4214607081  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t4214607081  InternalEnumerator_1_get_Current_m1304018714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1304018714(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3094284429(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205_gshared (InternalEnumerator_1_t778392047 * __this, const MethodInfo* method)
{
	{
		Entry_t4214607081  L_0 = InternalEnumerator_1_get_Current_m1304018714((InternalEnumerator_1_t778392047 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t4214607081  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t778392047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t778392047 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1994409205(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4190080554_gshared (InternalEnumerator_1_t252501700 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4190080554_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	InternalEnumerator_1__ctor_m4190080554(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2858632093_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2858632093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2858632093(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3459902714_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3459902714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3459902714(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::get_Current()
extern "C"  Entry_t3688716734  InternalEnumerator_1_get_Current_m3145257525_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3145257525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t3688716734  L_8 = ((  Entry_t3688716734  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t3688716734  InternalEnumerator_1_get_Current_m3145257525_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3145257525(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3063878094(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408_gshared (InternalEnumerator_1_t252501700 * __this, const MethodInfo* method)
{
	{
		Entry_t3688716734  L_0 = InternalEnumerator_1_get_Current_m3145257525((InternalEnumerator_1_t252501700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t3688716734  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t252501700 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t252501700 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m361459408(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2775616774_gshared (InternalEnumerator_1_t1972861616 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2775616774_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	InternalEnumerator_1__ctor_m2775616774(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m336048027_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m336048027_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	InternalEnumerator_1_Dispose_m336048027(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3523763518_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3523763518_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3523763518(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::get_Current()
extern "C"  Entry_t1114109354  InternalEnumerator_1_get_Current_m2653371815_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2653371815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t1114109354  L_8 = ((  Entry_t1114109354  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t1114109354  InternalEnumerator_1_get_Current_m2653371815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2653371815(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2704167158(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408_gshared (InternalEnumerator_1_t1972861616 * __this, const MethodInfo* method)
{
	{
		Entry_t1114109354  L_0 = InternalEnumerator_1_get_Current_m2653371815((InternalEnumerator_1_t1972861616 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t1114109354  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1972861616 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1972861616 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022229408(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1851236734_gshared (InternalEnumerator_1_t219164346 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1851236734_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	InternalEnumerator_1__ctor_m1851236734(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3390693617_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3390693617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3390693617(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3547199662_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3547199662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3547199662(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::get_Current()
extern "C"  Entry_t3655379380  InternalEnumerator_1_get_Current_m840252737_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m840252737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t3655379380  L_8 = ((  Entry_t3655379380  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t3655379380  InternalEnumerator_1_get_Current_m840252737_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m840252737(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m737024506(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740_gshared (InternalEnumerator_1_t219164346 * __this, const MethodInfo* method)
{
	{
		Entry_t3655379380  L_0 = InternalEnumerator_1_get_Current_m840252737((InternalEnumerator_1_t219164346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t3655379380  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t219164346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t219164346 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m671356740(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m981450495_gshared (InternalEnumerator_1_t836736193 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m981450495_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	InternalEnumerator_1__ctor_m981450495(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3328202350_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3328202350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3328202350(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1076783627_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1076783627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1076783627(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::get_Current()
extern "C"  Entry_t4272951227  InternalEnumerator_1_get_Current_m1718349432_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1718349432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t4272951227  L_8 = ((  Entry_t4272951227  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t4272951227  InternalEnumerator_1_get_Current_m1718349432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1718349432(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1665876303(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195_gshared (InternalEnumerator_1_t836736193 * __this, const MethodInfo* method)
{
	{
		Entry_t4272951227  L_0 = InternalEnumerator_1_get_Current_m1718349432((InternalEnumerator_1_t836736193 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t4272951227  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t836736193 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t836736193 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3763842195(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m453027211_gshared (InternalEnumerator_1_t303677782 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m453027211_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	InternalEnumerator_1__ctor_m453027211(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4125815062_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4125815062_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4125815062(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3692091135_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3692091135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3692091135(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::get_Current()
extern "C"  Entry_t3739892816  InternalEnumerator_1_get_Current_m2987216314_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2987216314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Entry_t3739892816  L_8 = ((  Entry_t3739892816  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Entry_t3739892816  InternalEnumerator_1_get_Current_m2987216314_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2987216314(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3043316715(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811_gshared (InternalEnumerator_1_t303677782 * __this, const MethodInfo* method)
{
	{
		Entry_t3739892816  L_0 = InternalEnumerator_1_get_Current_m2987216314((InternalEnumerator_1_t303677782 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Entry_t3739892816  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t303677782 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t303677782 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4072849811(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2417538736_gshared (InternalEnumerator_1_t2342494975 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2417538736_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	InternalEnumerator_1__ctor_m2417538736(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m195176279_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m195176279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	InternalEnumerator_1_Dispose_m195176279(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m322636248_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m322636248_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m322636248(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::get_Current()
extern "C"  KeyValuePair_2_t1483742713  InternalEnumerator_1_get_Current_m1154723631_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1154723631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1483742713  L_8 = ((  KeyValuePair_2_t1483742713  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1483742713  InternalEnumerator_1_get_Current_m1154723631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1154723631(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4234976536(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172_gshared (InternalEnumerator_1_t2342494975 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1483742713  L_0 = InternalEnumerator_1_get_Current_m1154723631((InternalEnumerator_1_t2342494975 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t1483742713  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2342494975 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2342494975 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3481741172(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1754127910_gshared (InternalEnumerator_1_t839262761 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1754127910_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	InternalEnumerator_1__ctor_m1754127910(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1037069_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1037069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1037069(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4073508358_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4073508358_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4073508358(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4275477795  InternalEnumerator_1_get_Current_m2948346621_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2948346621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4275477795  L_8 = ((  KeyValuePair_2_t4275477795  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4275477795  InternalEnumerator_1_get_Current_m2948346621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2948346621(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1938139986(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Guid,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090_gshared (InternalEnumerator_1_t839262761 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4275477795  L_0 = InternalEnumerator_1_get_Current_m2948346621((InternalEnumerator_1_t839262761 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t4275477795  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t839262761 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t839262761 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m573985090(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3441346029_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1__ctor_m3441346029(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m718416578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m718416578(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1791963761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3749587448  L_8 = ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582710858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = InternalEnumerator_1_get_Current_m3582710858((InternalEnumerator_1_t313372414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m920047666_gshared (InternalEnumerator_1_t1747572097 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m920047666_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	InternalEnumerator_1__ctor_m920047666(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1628196379_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1628196379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1628196379(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m930150026_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m930150026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m930150026(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t888819835  InternalEnumerator_1_get_Current_m1526874475_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1526874475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t888819835  L_8 = ((  KeyValuePair_2_t888819835  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t888819835  InternalEnumerator_1_get_Current_m1526874475_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1526874475(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138745466(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_gshared (InternalEnumerator_1_t1747572097 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t888819835  L_0 = InternalEnumerator_1_get_Current_m1526874475((InternalEnumerator_1_t1747572097 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t888819835  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1747572097 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1747572097 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1313308902(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m967618647_gshared (InternalEnumerator_1_t2033732330 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m967618647_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1__ctor_m967618647(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318835130_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m318835130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_Dispose_m318835130(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4294226955(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1174980068  L_8 = ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3900993294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = InternalEnumerator_1_get_Current_m3900993294((InternalEnumerator_1_t2033732330 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3362782841_gshared (InternalEnumerator_1_t280035060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3362782841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1__ctor_m3362782841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1748410190(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3486952605(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3716250094  L_8 = ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2882946014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = InternalEnumerator_1_get_Current_m2882946014((InternalEnumerator_1_t280035060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3587374424_gshared (InternalEnumerator_1_t897606907 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3587374424_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1__ctor_m3587374424(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2413981551(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1667794624(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t38854645  L_8 = ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2345377791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = InternalEnumerator_1_get_Current_m2345377791((InternalEnumerator_1_t897606907 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3950184714_gshared (InternalEnumerator_1_t364548496 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3950184714_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	InternalEnumerator_1__ctor_m3950184714(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2915635719_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2915635719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2915635719(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3284769442_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3284769442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3284769442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::get_Current()
extern "C"  KeyValuePair_2_t3800763530  InternalEnumerator_1_get_Current_m358792963_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m358792963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3800763530  L_8 = ((  KeyValuePair_2_t3800763530  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3800763530  InternalEnumerator_1_get_Current_m358792963_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m358792963(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1854006514(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Resources.ResourceLocator>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332_gshared (InternalEnumerator_1_t364548496 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3800763530  L_0 = InternalEnumerator_1_get_Current_m358792963((InternalEnumerator_1_t364548496 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t3800763530  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t364548496 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t364548496 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3397148332(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m645344728_gshared (InternalEnumerator_1_t1835343917 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m645344728_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	InternalEnumerator_1__ctor_m645344728(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3737488171_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3737488171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3737488171(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2095753328_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2095753328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2095753328(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::get_Current()
extern "C"  bucket_t976591655  InternalEnumerator_1_get_Current_m1021008655_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1021008655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bucket_t976591655  L_8 = ((  bucket_t976591655  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  bucket_t976591655  InternalEnumerator_1_get_Current_m1021008655_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1021008655(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1744876896(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/bucket>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334_gshared (InternalEnumerator_1_t1835343917 * __this, const MethodInfo* method)
{
	{
		bucket_t976591655  L_0 = InternalEnumerator_1_get_Current_m1021008655((InternalEnumerator_1_t1835343917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bucket_t976591655  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1835343917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1835343917 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1612293334(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4085858451_gshared (InternalEnumerator_1_t1027194178 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4085858451_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	InternalEnumerator_1__ctor_m4085858451(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2936110260_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2936110260_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2936110260(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3616673367_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3616673367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3616673367(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::get_Current()
extern "C"  AttributeEntry_t168441916  InternalEnumerator_1_get_Current_m419194570_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m419194570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		AttributeEntry_t168441916  L_8 = ((  AttributeEntry_t168441916  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  AttributeEntry_t168441916  InternalEnumerator_1_get_Current_m419194570_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m419194570(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m654074019(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.AttributeCollection/AttributeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223_gshared (InternalEnumerator_1_t1027194178 * __this, const MethodInfo* method)
{
	{
		AttributeEntry_t168441916  L_0 = InternalEnumerator_1_get_Current_m419194570((InternalEnumerator_1_t1027194178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		AttributeEntry_t168441916  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1027194178 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1027194178 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2741030223(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m245588437_gshared (InternalEnumerator_1_t1551957931 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m245588437_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1__ctor_m245588437(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3383574608(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3300932033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t693205669  L_8 = ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4279678504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = InternalEnumerator_1_get_Current_m4279678504((InternalEnumerator_1_t1551957931 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		DateTime_t693205669  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m966844380_gshared (InternalEnumerator_1_t2221741168 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m966844380_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	InternalEnumerator_1__ctor_m966844380(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2889182185_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2889182185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2889182185(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTimeOffset>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m276280428_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m276280428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m276280428(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTimeOffset>::get_Current()
extern "C"  DateTimeOffset_t1362988906  InternalEnumerator_1_get_Current_m1776564649_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1776564649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTimeOffset_t1362988906  L_8 = ((  DateTimeOffset_t1362988906  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  DateTimeOffset_t1362988906  InternalEnumerator_1_get_Current_m1776564649_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1776564649(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2909778992(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTimeOffset>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750_gshared (InternalEnumerator_1_t2221741168 * __this, const MethodInfo* method)
{
	{
		DateTimeOffset_t1362988906  L_0 = InternalEnumerator_1_get_Current_m1776564649((InternalEnumerator_1_t2221741168 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		DateTimeOffset_t1362988906  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2221741168 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2221741168 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m888475750(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150855019_gshared (InternalEnumerator_1_t1583453339 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4150855019_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1__ctor_m4150855019(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3407567388(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4134231455(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m245025210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t724701077  L_8 = ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m245025210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		Decimal_t724701077  L_0 = InternalEnumerator_1_get_Current_m245025210((InternalEnumerator_1_t1583453339 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Decimal_t724701077  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3589241961_gshared (InternalEnumerator_1_t641800647 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3589241961_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1__ctor_m3589241961(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3578333724(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m83303365(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1389169756(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m1389169756((InternalEnumerator_1_t641800647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m565351307_gshared (InternalEnumerator_1_t2600861628 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m565351307_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	InternalEnumerator_1__ctor_m565351307(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3886016744_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3886016744_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3886016744(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3943379735_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3943379735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3943379735(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::get_Current()
extern "C"  InternalCodePageDataItem_t1742109366  InternalEnumerator_1_get_Current_m3667052076_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3667052076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		InternalCodePageDataItem_t1742109366  L_8 = ((  InternalCodePageDataItem_t1742109366  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  InternalCodePageDataItem_t1742109366  InternalEnumerator_1_get_Current_m3667052076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3667052076(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m344753907(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalCodePageDataItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003_gshared (InternalEnumerator_1_t2600861628 * __this, const MethodInfo* method)
{
	{
		InternalCodePageDataItem_t1742109366  L_0 = InternalEnumerator_1_get_Current_m3667052076((InternalEnumerator_1_t2600861628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		InternalCodePageDataItem_t1742109366  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2600861628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2600861628 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1169364003(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m298297972_gshared (InternalEnumerator_1_t941671943 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m298297972_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	InternalEnumerator_1__ctor_m298297972(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3568777087_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3568777087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3568777087(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3256434108_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3256434108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3256434108(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::get_Current()
extern "C"  InternalEncodingDataItem_t82919681  InternalEnumerator_1_get_Current_m1145634683_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1145634683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		InternalEncodingDataItem_t82919681  L_8 = ((  InternalEncodingDataItem_t82919681  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  InternalEncodingDataItem_t82919681  InternalEnumerator_1_get_Current_m1145634683_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1145634683(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2203101140(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.InternalEncodingDataItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242_gshared (InternalEnumerator_1_t941671943 * __this, const MethodInfo* method)
{
	{
		InternalEncodingDataItem_t82919681  L_0 = InternalEnumerator_1_get_Current_m1145634683((InternalEnumerator_1_t941671943 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		InternalEncodingDataItem_t82919681  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t941671943 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t941671943 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3329198242(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3204909454_gshared (InternalEnumerator_1_t2890411629 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3204909454_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	InternalEnumerator_1__ctor_m3204909454(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3020383423_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3020383423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3020383423(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2897730678_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2897730678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2897730678(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::get_Current()
extern "C"  TimeSpanToken_t2031659367  InternalEnumerator_1_get_Current_m1471150671_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1471150671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpanToken_t2031659367  L_8 = ((  TimeSpanToken_t2031659367  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  TimeSpanToken_t2031659367  InternalEnumerator_1_get_Current_m1471150671_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1471150671(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3604772830(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.TimeSpanParse/TimeSpanToken>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114_gshared (InternalEnumerator_1_t2890411629 * __this, const MethodInfo* method)
{
	{
		TimeSpanToken_t2031659367  L_0 = InternalEnumerator_1_get_Current_m1471150671((InternalEnumerator_1_t2890411629 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TimeSpanToken_t2031659367  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2890411629 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2890411629 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3186567114(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Guid>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m702435723_gshared (InternalEnumerator_1_t3392353855 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m702435723_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	InternalEnumerator_1__ctor_m702435723(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Guid>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1976167928_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1976167928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1976167928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Guid>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1380038135_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1380038135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1380038135(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Guid>::get_Current()
extern "C"  Guid_t  InternalEnumerator_1_get_Current_m3939996428_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3939996428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Guid_t  L_8 = ((  Guid_t  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Guid_t  InternalEnumerator_1_get_Current_m3939996428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3939996428(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m633653299(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Guid>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547_gshared (InternalEnumerator_1_t3392353855 * __this, const MethodInfo* method)
{
	{
		Guid_t  L_0 = InternalEnumerator_1_get_Current_m3939996428((InternalEnumerator_1_t3392353855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Guid_t  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3392353855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3392353855 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493164547(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m557239862_gshared (InternalEnumerator_1_t605030880 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m557239862_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1__ctor_m557239862(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2743309309(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4274987126(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3259181373(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m3259181373((InternalEnumerator_1_t605030880 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m504913220_gshared (InternalEnumerator_1_t2930629710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m504913220_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1__ctor_m504913220(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3393096515(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3679487948(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m10285187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m10285187(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m10285187((InternalEnumerator_1_t2930629710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2597133905_gshared (InternalEnumerator_1_t1767830299 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2597133905_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1__ctor_m2597133905(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m307741520_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m307741520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_Dispose_m307741520(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1683120485(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2415979394(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m2415979394((InternalEnumerator_1_t1767830299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1648185761_gshared (InternalEnumerator_1_t3362812871 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1648185761_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1__ctor_m1648185761(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3933737284(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2720582493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1706492988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m1706492988((InternalEnumerator_1_t3362812871 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m926514125_gshared (InternalEnumerator_1_t494314080 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m926514125_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	InternalEnumerator_1__ctor_m926514125(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m676949342_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m676949342_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	InternalEnumerator_1_Dispose_m676949342(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1352446537_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1352446537_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1352446537(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::get_Current()
extern "C"  RecognizedAttribute_t3930529114  InternalEnumerator_1_get_Current_m3788757124_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3788757124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RecognizedAttribute_t3930529114  L_8 = ((  RecognizedAttribute_t3930529114  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  RecognizedAttribute_t3930529114  InternalEnumerator_1_get_Current_m3788757124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3788757124(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1795932617(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Net.CookieTokenizer/RecognizedAttribute>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841_gshared (InternalEnumerator_1_t494314080 * __this, const MethodInfo* method)
{
	{
		RecognizedAttribute_t3930529114  L_0 = InternalEnumerator_1_get_Current_m3788757124((InternalEnumerator_1_t494314080 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		RecognizedAttribute_t3930529114  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t494314080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t494314080 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2858532841(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1019972839_gshared (InternalEnumerator_1_t2917548534 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1019972839_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	InternalEnumerator_1__ctor_m1019972839(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1171085184_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1171085184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1171085184(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2708971515_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2708971515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2708971515(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::get_Current()
extern "C"  HeaderVariantInfo_t2058796272  InternalEnumerator_1_get_Current_m2429470294_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2429470294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HeaderVariantInfo_t2058796272  L_8 = ((  HeaderVariantInfo_t2058796272  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  HeaderVariantInfo_t2058796272  InternalEnumerator_1_get_Current_m2429470294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2429470294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m282906479(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Net.HeaderVariantInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883_gshared (InternalEnumerator_1_t2917548534 * __this, const MethodInfo* method)
{
	{
		HeaderVariantInfo_t2058796272  L_0 = InternalEnumerator_1_get_Current_m2429470294((InternalEnumerator_1_t2917548534 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		HeaderVariantInfo_t2058796272  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2917548534 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2917548534 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m523037883(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2945276195_gshared (InternalEnumerator_1_t3058064956 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2945276195_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	InternalEnumerator_1__ctor_m2945276195(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1866063914_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1866063914_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1866063914(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2957122063_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2957122063_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2957122063(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::get_Current()
extern "C"  WSABUF_t2199312694  InternalEnumerator_1_get_Current_m1669430788_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1669430788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WSABUF_t2199312694  L_8 = ((  WSABUF_t2199312694  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  WSABUF_t2199312694  InternalEnumerator_1_get_Current_m1669430788_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1669430788(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3708174155(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Sockets.Socket/WSABUF>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919_gshared (InternalEnumerator_1_t3058064956 * __this, const MethodInfo* method)
{
	{
		WSABUF_t2199312694  L_0 = InternalEnumerator_1_get_Current_m1669430788((InternalEnumerator_1_t3058064956 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		WSABUF_t2199312694  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3058064956 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3058064956 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297089919(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1608269208_gshared (InternalEnumerator_1_t2275375023 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1608269208_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	InternalEnumerator_1__ctor_m1608269208(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1743466569_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1743466569_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1743466569(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m706855480_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m706855480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m706855480(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m419131001_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m419131001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m419131001_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m419131001(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1703363284(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Net.WebHeaderCollection/RfcChar>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596_gshared (InternalEnumerator_1_t2275375023 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m419131001((InternalEnumerator_1_t2275375023 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2275375023 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2275375023 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497460596(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853313801_gshared (InternalEnumerator_1_t3548201557 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m853313801_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1__ctor_m853313801(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1636767846(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1047150157(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3206960238(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m3206960238((InternalEnumerator_1_t3548201557 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1626855592_gshared (InternalEnumerator_1_t3806268713 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1626855592_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	InternalEnumerator_1__ctor_m1626855592(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1387288299_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1387288299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1387288299(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2783536832_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2783536832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2783536832(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::get_Current()
extern "C"  FormatParam_t2947516451  InternalEnumerator_1_get_Current_m421370399_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m421370399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		FormatParam_t2947516451  L_8 = ((  FormatParam_t2947516451  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  FormatParam_t2947516451  InternalEnumerator_1_get_Current_m421370399_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m421370399(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m260621296(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ParameterizedStrings/FormatParam>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454_gshared (InternalEnumerator_1_t3806268713 * __this, const MethodInfo* method)
{
	{
		FormatParam_t2947516451  L_0 = InternalEnumerator_1_get_Current_m421370399((InternalEnumerator_1_t3806268713 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		FormatParam_t2947516451  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3806268713 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3806268713 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1960136454(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m492779768_gshared (InternalEnumerator_1_t952909805 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m492779768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1__ctor_m492779768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m238246335_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m238246335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_Dispose_m238246335(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1548080384(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t94157543  L_8 = ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1089848479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = InternalEnumerator_1_get_Current_m1089848479((InternalEnumerator_1_t952909805 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m821424641_gshared (InternalEnumerator_1_t2356950176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m821424641_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1__ctor_m821424641(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4038440306(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2904932349(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t1498197914  L_8 = ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1047712960(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = InternalEnumerator_1_get_Current_m1047712960((InternalEnumerator_1_t2356950176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3294994445_gshared (InternalEnumerator_1_t2901227451 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3294994445_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	InternalEnumerator_1__ctor_m3294994445(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1287369282_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1287369282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1287369282(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1560801521_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1560801521_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1560801521(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::get_Current()
extern "C"  ILExceptionBlock_t2042475189  InternalEnumerator_1_get_Current_m451997186_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m451997186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILExceptionBlock_t2042475189  L_8 = ((  ILExceptionBlock_t2042475189  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ILExceptionBlock_t2042475189  InternalEnumerator_1_get_Current_m451997186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m451997186(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m222340881(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397_gshared (InternalEnumerator_1_t2901227451 * __this, const MethodInfo* method)
{
	{
		ILExceptionBlock_t2042475189  L_0 = InternalEnumerator_1_get_Current_m451997186((InternalEnumerator_1_t2901227451 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ILExceptionBlock_t2042475189  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2901227451 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2901227451 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3573595397(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1398783858_gshared (InternalEnumerator_1_t2348906860 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1398783858_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	InternalEnumerator_1__ctor_m1398783858(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2657406875_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2657406875_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2657406875(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2180350026_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2180350026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2180350026(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::get_Current()
extern "C"  ILExceptionInfo_t1490154598  InternalEnumerator_1_get_Current_m4010463147_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4010463147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILExceptionInfo_t1490154598  L_8 = ((  ILExceptionInfo_t1490154598  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ILExceptionInfo_t1490154598  InternalEnumerator_1_get_Current_m4010463147_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4010463147(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m413447610(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382_gshared (InternalEnumerator_1_t2348906860 * __this, const MethodInfo* method)
{
	{
		ILExceptionInfo_t1490154598  L_0 = InternalEnumerator_1_get_Current_m4010463147((InternalEnumerator_1_t2348906860 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ILExceptionInfo_t1490154598  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2348906860 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2348906860 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1085513382(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3323962057_gshared (InternalEnumerator_1_t275897710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3323962057_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1__ctor_m3323962057(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m549215360_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m549215360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m549215360(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3389738333(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t3712112744  L_8 = ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3922357178(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		LabelData_t3712112744  L_0 = InternalEnumerator_1_get_Current_m3922357178((InternalEnumerator_1_t275897710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		LabelData_t3712112744  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3228997263_gshared (InternalEnumerator_1_t654694480 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3228997263_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1__ctor_m3228997263(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3927915442(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4292005299(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t4090909514  L_8 = ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2468740214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t4090909514  L_0 = InternalEnumerator_1_get_Current_m2468740214((InternalEnumerator_1_t654694480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		LabelFixup_t4090909514  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3387972470_gshared (InternalEnumerator_1_t1008311600 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3387972470_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1__ctor_m3387972470(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2056889175(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1590907854(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t149559338  L_8 = ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3296972783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t149559338  L_0 = InternalEnumerator_1_get_Current_m3296972783((InternalEnumerator_1_t1008311600 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ILTokenInfo_t149559338  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m750604974_gshared (InternalEnumerator_1_t806987626 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m750604974_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	InternalEnumerator_1__ctor_m750604974(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m830280837_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m830280837_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	InternalEnumerator_1_Dispose_m830280837(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2665983630_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2665983630_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2665983630(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::get_Current()
extern "C"  Label_t4243202660  InternalEnumerator_1_get_Current_m322002637_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m322002637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Label_t4243202660  L_8 = ((  Label_t4243202660  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Label_t4243202660  InternalEnumerator_1_get_Current_m322002637_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m322002637(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165191898(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466_gshared (InternalEnumerator_1_t806987626 * __this, const MethodInfo* method)
{
	{
		Label_t4243202660  L_0 = InternalEnumerator_1_get_Current_m322002637((InternalEnumerator_1_t806987626 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Label_t4243202660  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t806987626 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t806987626 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3687640466(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2582404099_gshared (InternalEnumerator_1_t3986139419 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2582404099_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	InternalEnumerator_1__ctor_m2582404099(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m219216750_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m219216750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	InternalEnumerator_1_Dispose_m219216750(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3074289735_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3074289735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3074289735(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::get_Current()
extern "C"  MonoResource_t3127387157  InternalEnumerator_1_get_Current_m2685429706_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2685429706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		MonoResource_t3127387157  L_8 = ((  MonoResource_t3127387157  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  MonoResource_t3127387157  InternalEnumerator_1_get_Current_m2685429706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2685429706(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m526016803(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515_gshared (InternalEnumerator_1_t3986139419 * __this, const MethodInfo* method)
{
	{
		MonoResource_t3127387157  L_0 = InternalEnumerator_1_get_Current_m2685429706((InternalEnumerator_1_t3986139419 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		MonoResource_t3127387157  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3986139419 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3986139419 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679802515(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3708844878_gshared (InternalEnumerator_1_t3326058480 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3708844878_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	InternalEnumerator_1__ctor_m3708844878(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1448063359_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1448063359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1448063359(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4230111510_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4230111510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4230111510(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::get_Current()
extern "C"  MonoWin32Resource_t2467306218  InternalEnumerator_1_get_Current_m4167074463_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4167074463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		MonoWin32Resource_t2467306218  L_8 = ((  MonoWin32Resource_t2467306218  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  MonoWin32Resource_t2467306218  InternalEnumerator_1_get_Current_m4167074463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4167074463(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2293471086(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506_gshared (InternalEnumerator_1_t3326058480 * __this, const MethodInfo* method)
{
	{
		MonoWin32Resource_t2467306218  L_0 = InternalEnumerator_1_get_Current_m4167074463((InternalEnumerator_1_t3326058480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		MonoWin32Resource_t2467306218  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3326058480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3326058480 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m759524506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3726334735_gshared (InternalEnumerator_1_t3567360695 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3726334735_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	InternalEnumerator_1__ctor_m3726334735(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2901155554_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2901155554_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2901155554(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3875155235_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3875155235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3875155235(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::get_Current()
extern "C"  RefEmitPermissionSet_t2708608433  InternalEnumerator_1_get_Current_m1274592126_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1274592126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RefEmitPermissionSet_t2708608433  L_8 = ((  RefEmitPermissionSet_t2708608433  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  RefEmitPermissionSet_t2708608433  InternalEnumerator_1_get_Current_m1274592126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1274592126(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1133072087(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751_gshared (InternalEnumerator_1_t3567360695 * __this, const MethodInfo* method)
{
	{
		RefEmitPermissionSet_t2708608433  L_0 = InternalEnumerator_1_get_Current_m1274592126((InternalEnumerator_1_t3567360695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		RefEmitPermissionSet_t2708608433  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3567360695 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3567360695 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1038263751(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2890018883_gshared (InternalEnumerator_1_t2679387182 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2890018883_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1__ctor_m2890018883(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3952699776(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1594563423(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t1820634920  L_8 = ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4083613828(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t1820634920  L_0 = InternalEnumerator_1_get_Current_m4083613828((InternalEnumerator_1_t2679387182 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ParameterModifier_t1820634920  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2999622677_gshared (InternalEnumerator_1_t3015143146 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2999622677_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	InternalEnumerator_1__ctor_m2999622677(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1652602110_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1652602110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1652602110(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1876372465_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1876372465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1876372465(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::get_Current()
extern "C"  ResourceLocator_t2156390884  InternalEnumerator_1_get_Current_m1643772188_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1643772188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceLocator_t2156390884  L_8 = ((  ResourceLocator_t2156390884  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  ResourceLocator_t2156390884  InternalEnumerator_1_get_Current_m1643772188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1643772188(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2220673169(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceLocator>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433_gshared (InternalEnumerator_1_t3015143146 * __this, const MethodInfo* method)
{
	{
		ResourceLocator_t2156390884  L_0 = InternalEnumerator_1_get_Current_m1643772188((InternalEnumerator_1_t3015143146 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ResourceLocator_t2156390884  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3015143146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3015143146 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1877866433(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1198276628_gshared (InternalEnumerator_1_t2733828895 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1198276628_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	InternalEnumerator_1__ctor_m1198276628(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3248673743_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3248673743_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3248673743(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1104149308_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1104149308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1104149308(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::get_Current()
extern "C"  Ephemeron_t1875076633  InternalEnumerator_1_get_Current_m1864471731_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1864471731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Ephemeron_t1875076633  L_8 = ((  Ephemeron_t1875076633  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  Ephemeron_t1875076633  InternalEnumerator_1_get_Current_m1864471731_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1864471731(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599270980(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.Ephemeron>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178_gshared (InternalEnumerator_1_t2733828895 * __this, const MethodInfo* method)
{
	{
		Ephemeron_t1875076633  L_0 = InternalEnumerator_1_get_Current_m1864471731((InternalEnumerator_1_t2733828895 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Ephemeron_t1875076633  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2733828895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2733828895 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4166161178(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m313732903_gshared (InternalEnumerator_1_t4268020328 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m313732903_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	InternalEnumerator_1__ctor_m313732903(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m599783274_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m599783274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	InternalEnumerator_1_Dispose_m599783274(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4248732811_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4248732811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4248732811(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::get_Current()
extern "C"  GCHandle_t3409268066  InternalEnumerator_1_get_Current_m1012591846_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1012591846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GCHandle_t3409268066  L_8 = ((  GCHandle_t3409268066  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  GCHandle_t3409268066  InternalEnumerator_1_get_Current_m1012591846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1012591846(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1529120351(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_gshared (InternalEnumerator_1_t4268020328 * __this, const MethodInfo* method)
{
	{
		GCHandle_t3409268066  L_0 = InternalEnumerator_1_get_Current_m1012591846((InternalEnumerator_1_t4268020328 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		GCHandle_t3409268066  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4268020328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4268020328 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1382282783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3149706958_gshared (InternalEnumerator_1_t3736091384 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3149706958_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	InternalEnumerator_1__ctor_m3149706958(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2801598691_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2801598691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2801598691(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1137828214_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1137828214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1137828214(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m948509279_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m948509279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m948509279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m948509279(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1228887566(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864_gshared (InternalEnumerator_1_t3736091384 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m948509279((InternalEnumerator_1_t3736091384 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3736091384 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3736091384 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3430821864(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1277864681_gshared (InternalEnumerator_1_t3879563917 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1277864681_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	InternalEnumerator_1__ctor_m1277864681(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1587363548_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1587363548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1587363548(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m488449285_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m488449285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m488449285(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1174598684_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1174598684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1174598684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1174598684(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3913732653(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473_gshared (InternalEnumerator_1_t3879563917 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1174598684((InternalEnumerator_1_t3879563917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3879563917 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3879563917 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607502473(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108401677_gshared (InternalEnumerator_1_t1313169811 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2108401677_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1__ctor_m2108401677(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1676985532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3984801393(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m314017974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m314017974(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m314017974((InternalEnumerator_1_t1313169811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m655778553_gshared (InternalEnumerator_1_t842163687 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m655778553_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1__ctor_m655778553(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3671580532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1869236997(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t4278378721  L_8 = ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1550231132(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t4278378721  L_0 = InternalEnumerator_1_get_Current_m1550231132((InternalEnumerator_1_t842163687 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		X509ChainStatus_t4278378721  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2314640734_gshared (InternalEnumerator_1_t2935262194 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2314640734_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1__ctor_m2314640734(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2195973811(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m580128774(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C"  float InternalEnumerator_1_get_Current_m727737343_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m727737343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m727737343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m727737343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m727737343((InternalEnumerator_1_t2935262194 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2853126416_gshared (InternalEnumerator_1_t2284019382 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2853126416_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	InternalEnumerator_1__ctor_m2853126416(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m30795463_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m30795463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	InternalEnumerator_1_Dispose_m30795463(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TermInfoStrings>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m138185816_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m138185816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m138185816(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TermInfoStrings>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3362979103_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3362979103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3362979103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3362979103(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589790920(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132_gshared (InternalEnumerator_1_t2284019382 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3362979103((InternalEnumerator_1_t2284019382 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2284019382 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2284019382 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m187752132(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2565240337_gshared (InternalEnumerator_1_t3012688088 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2565240337_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	InternalEnumerator_1__ctor_m2565240337(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3391663714_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3391663714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3391663714(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2599112621_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2599112621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2599112621(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::get_Current()
extern "C"  LowerCaseMapping_t2153935826  InternalEnumerator_1_get_Current_m466738280_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m466738280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LowerCaseMapping_t2153935826  L_8 = ((  LowerCaseMapping_t2153935826  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  LowerCaseMapping_t2153935826  InternalEnumerator_1_get_Current_m466738280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m466738280(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2479820901(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429_gshared (InternalEnumerator_1_t3012688088 * __this, const MethodInfo* method)
{
	{
		LowerCaseMapping_t2153935826  L_0 = InternalEnumerator_1_get_Current_m466738280((InternalEnumerator_1_t3012688088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		LowerCaseMapping_t2153935826  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3012688088 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3012688088 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2524152429(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m974809591_gshared (InternalEnumerator_1_t3277011989 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m974809591_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	InternalEnumerator_1__ctor_m974809591(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m779531596_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m779531596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	InternalEnumerator_1_Dispose_m779531596(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2770905747_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2770905747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2770905747(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578701872_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2578701872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2578701872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2578701872(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m987888183(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.RegexOptions>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967_gshared (InternalEnumerator_1_t3277011989 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2578701872((InternalEnumerator_1_t3277011989 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3277011989 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3277011989 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3059288967(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2874479637_gshared (InternalEnumerator_1_t2567611619 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2874479637_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	InternalEnumerator_1__ctor_m2874479637(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2776097548_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2776097548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2776097548(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3251308169_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3251308169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3251308169(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::get_Current()
extern "C"  CancellationTokenRegistration_t1708859357  InternalEnumerator_1_get_Current_m3338380422_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3338380422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CancellationTokenRegistration_t1708859357  L_8 = ((  CancellationTokenRegistration_t1708859357  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  CancellationTokenRegistration_t1708859357  InternalEnumerator_1_get_Current_m3338380422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3338380422(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1821396777(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Threading.CancellationTokenRegistration>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841_gshared (InternalEnumerator_1_t2567611619 * __this, const MethodInfo* method)
{
	{
		CancellationTokenRegistration_t1708859357  L_0 = InternalEnumerator_1_get_Current_m3338380422((InternalEnumerator_1_t2567611619 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		CancellationTokenRegistration_t1708859357  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2567611619 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2567611619 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1537547841(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2189699457_gshared (InternalEnumerator_1_t4289011211 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2189699457_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1__ctor_m2189699457(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3838127340(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1674480765(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t3430258949  L_8 = ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3411759116(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t3430258949  L_0 = InternalEnumerator_1_get_Current_m3411759116((InternalEnumerator_1_t4289011211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TimeSpan_t3430258949  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m200749079_gshared (InternalEnumerator_1_t2714516328 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m200749079_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	InternalEnumerator_1__ctor_m200749079(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2957730742_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2957730742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2957730742(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4246357971_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4246357971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4246357971(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::get_Current()
extern "C"  TZifType_t1855764066  InternalEnumerator_1_get_Current_m2840641472_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2840641472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TZifType_t1855764066  L_8 = ((  TZifType_t1855764066  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  TZifType_t1855764066  InternalEnumerator_1_get_Current_m2840641472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2840641472(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4268798695(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeZoneInfo/TZifType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523_gshared (InternalEnumerator_1_t2714516328 * __this, const MethodInfo* method)
{
	{
		TZifType_t1855764066  L_0 = InternalEnumerator_1_get_Current_m2840641472((InternalEnumerator_1_t2714516328 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TZifType_t1855764066  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2714516328 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2714516328 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m878094523(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2264046229_gshared (InternalEnumerator_1_t3395678463 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2264046229_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	InternalEnumerator_1__ctor_m2264046229(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1071940184_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1071940184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1071940184(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3081678193_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3081678193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3081678193(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3431860728_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3431860728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3431860728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3431860728(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2159234385(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005_gshared (InternalEnumerator_1_t3395678463 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3431860728((InternalEnumerator_1_t3395678463 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3395678463 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3395678463 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3227205005(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2981879621_gshared (InternalEnumerator_1_t1845634873 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2981879621_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1__ctor_m2981879621(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1824402698(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2809569305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3179981210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m3179981210((InternalEnumerator_1_t1845634873 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691972083_gshared (InternalEnumerator_1_t3008434283 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m691972083_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1__ctor_m691972083(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2620838688(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m470170271(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2198364332(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m2198364332((InternalEnumerator_1_t3008434283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3084132532_gshared (InternalEnumerator_1_t3767949176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3084132532_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1__ctor_m3084132532(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3642485841(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2954283444(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m35328337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m35328337(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m35328337((InternalEnumerator_1_t3767949176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
