﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalDataStoreElement
struct  LocalDataStoreElement_t3980707294  : public Il2CppObject
{
public:
	// System.Object System.LocalDataStoreElement::m_value
	Il2CppObject * ___m_value_0;
	// System.Int64 System.LocalDataStoreElement::m_cookie
	int64_t ___m_cookie_1;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(LocalDataStoreElement_t3980707294, ___m_value_0)); }
	inline Il2CppObject * get_m_value_0() const { return ___m_value_0; }
	inline Il2CppObject ** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppObject * value)
	{
		___m_value_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_value_0, value);
	}

	inline static int32_t get_offset_of_m_cookie_1() { return static_cast<int32_t>(offsetof(LocalDataStoreElement_t3980707294, ___m_cookie_1)); }
	inline int64_t get_m_cookie_1() const { return ___m_cookie_1; }
	inline int64_t* get_address_of_m_cookie_1() { return &___m_cookie_1; }
	inline void set_m_cookie_1(int64_t value)
	{
		___m_cookie_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
