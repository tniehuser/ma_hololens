﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.LinkedListNode`1<System.Text.RegularExpressions.CachedCodeEntry>
struct LinkedListNode_1_t2449926964;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry>
struct  LinkedList_1_t3858529280  : public Il2CppObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t2449926964 * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	Il2CppObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::siInfo
	SerializationInfo_t228987430 * ___siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t3858529280, ___head_0)); }
	inline LinkedListNode_1_t2449926964 * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_t2449926964 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_t2449926964 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier(&___head_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t3858529280, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t3858529280, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t3858529280, ____syncRoot_3)); }
	inline Il2CppObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline Il2CppObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(Il2CppObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_3, value);
	}

	inline static int32_t get_offset_of_siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t3858529280, ___siInfo_4)); }
	inline SerializationInfo_t228987430 * get_siInfo_4() const { return ___siInfo_4; }
	inline SerializationInfo_t228987430 ** get_address_of_siInfo_4() { return &___siInfo_4; }
	inline void set_siInfo_4(SerializationInfo_t228987430 * value)
	{
		___siInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___siInfo_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
