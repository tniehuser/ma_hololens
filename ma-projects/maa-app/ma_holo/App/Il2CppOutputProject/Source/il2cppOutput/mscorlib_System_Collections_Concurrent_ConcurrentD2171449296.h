﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Concurrent.ConcurrentDictionary`2/Tables<System.String,System.Object>
struct Tables_t3011375419;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentDictionary`2<System.String,System.Object>
struct  ConcurrentDictionary_2_t2171449296  : public Il2CppObject
{
public:
	// System.Collections.Concurrent.ConcurrentDictionary`2/Tables<TKey,TValue> modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Concurrent.ConcurrentDictionary`2::m_tables
	Tables_t3011375419 * ___m_tables_0;
	// System.Boolean System.Collections.Concurrent.ConcurrentDictionary`2::m_growLockArray
	bool ___m_growLockArray_1;
	// System.Int32 System.Collections.Concurrent.ConcurrentDictionary`2::m_keyRehashCount
	int32_t ___m_keyRehashCount_2;
	// System.Int32 System.Collections.Concurrent.ConcurrentDictionary`2::m_budget
	int32_t ___m_budget_3;

public:
	inline static int32_t get_offset_of_m_tables_0() { return static_cast<int32_t>(offsetof(ConcurrentDictionary_2_t2171449296, ___m_tables_0)); }
	inline Tables_t3011375419 * get_m_tables_0() const { return ___m_tables_0; }
	inline Tables_t3011375419 ** get_address_of_m_tables_0() { return &___m_tables_0; }
	inline void set_m_tables_0(Tables_t3011375419 * value)
	{
		___m_tables_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tables_0, value);
	}

	inline static int32_t get_offset_of_m_growLockArray_1() { return static_cast<int32_t>(offsetof(ConcurrentDictionary_2_t2171449296, ___m_growLockArray_1)); }
	inline bool get_m_growLockArray_1() const { return ___m_growLockArray_1; }
	inline bool* get_address_of_m_growLockArray_1() { return &___m_growLockArray_1; }
	inline void set_m_growLockArray_1(bool value)
	{
		___m_growLockArray_1 = value;
	}

	inline static int32_t get_offset_of_m_keyRehashCount_2() { return static_cast<int32_t>(offsetof(ConcurrentDictionary_2_t2171449296, ___m_keyRehashCount_2)); }
	inline int32_t get_m_keyRehashCount_2() const { return ___m_keyRehashCount_2; }
	inline int32_t* get_address_of_m_keyRehashCount_2() { return &___m_keyRehashCount_2; }
	inline void set_m_keyRehashCount_2(int32_t value)
	{
		___m_keyRehashCount_2 = value;
	}

	inline static int32_t get_offset_of_m_budget_3() { return static_cast<int32_t>(offsetof(ConcurrentDictionary_2_t2171449296, ___m_budget_3)); }
	inline int32_t get_m_budget_3() const { return ___m_budget_3; }
	inline int32_t* get_address_of_m_budget_3() { return &___m_budget_3; }
	inline void set_m_budget_3(int32_t value)
	{
		___m_budget_3 = value;
	}
};

struct ConcurrentDictionary_2_t2171449296_StaticFields
{
public:
	// System.Boolean System.Collections.Concurrent.ConcurrentDictionary`2::s_isValueWriteAtomic
	bool ___s_isValueWriteAtomic_4;

public:
	inline static int32_t get_offset_of_s_isValueWriteAtomic_4() { return static_cast<int32_t>(offsetof(ConcurrentDictionary_2_t2171449296_StaticFields, ___s_isValueWriteAtomic_4)); }
	inline bool get_s_isValueWriteAtomic_4() const { return ___s_isValueWriteAtomic_4; }
	inline bool* get_address_of_s_isValueWriteAtomic_4() { return &___s_isValueWriteAtomic_4; }
	inline void set_s_isValueWriteAtomic_4(bool value)
	{
		___s_isValueWriteAtomic_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
