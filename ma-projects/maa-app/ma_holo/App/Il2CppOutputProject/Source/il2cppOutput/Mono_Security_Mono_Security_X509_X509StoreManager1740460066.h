﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// Mono.Security.X509.X509Stores
struct X509Stores_t3001420399;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509StoreManager
struct  X509StoreManager_t1740460067  : public Il2CppObject
{
public:

public:
};

struct X509StoreManager_t1740460067_StaticFields
{
public:
	// System.String Mono.Security.X509.X509StoreManager::_userPath
	String_t* ____userPath_0;
	// System.String Mono.Security.X509.X509StoreManager::_localMachinePath
	String_t* ____localMachinePath_1;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_userStore
	X509Stores_t3001420399 * ____userStore_2;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_machineStore
	X509Stores_t3001420399 * ____machineStore_3;

public:
	inline static int32_t get_offset_of__userPath_0() { return static_cast<int32_t>(offsetof(X509StoreManager_t1740460067_StaticFields, ____userPath_0)); }
	inline String_t* get__userPath_0() const { return ____userPath_0; }
	inline String_t** get_address_of__userPath_0() { return &____userPath_0; }
	inline void set__userPath_0(String_t* value)
	{
		____userPath_0 = value;
		Il2CppCodeGenWriteBarrier(&____userPath_0, value);
	}

	inline static int32_t get_offset_of__localMachinePath_1() { return static_cast<int32_t>(offsetof(X509StoreManager_t1740460067_StaticFields, ____localMachinePath_1)); }
	inline String_t* get__localMachinePath_1() const { return ____localMachinePath_1; }
	inline String_t** get_address_of__localMachinePath_1() { return &____localMachinePath_1; }
	inline void set__localMachinePath_1(String_t* value)
	{
		____localMachinePath_1 = value;
		Il2CppCodeGenWriteBarrier(&____localMachinePath_1, value);
	}

	inline static int32_t get_offset_of__userStore_2() { return static_cast<int32_t>(offsetof(X509StoreManager_t1740460067_StaticFields, ____userStore_2)); }
	inline X509Stores_t3001420399 * get__userStore_2() const { return ____userStore_2; }
	inline X509Stores_t3001420399 ** get_address_of__userStore_2() { return &____userStore_2; }
	inline void set__userStore_2(X509Stores_t3001420399 * value)
	{
		____userStore_2 = value;
		Il2CppCodeGenWriteBarrier(&____userStore_2, value);
	}

	inline static int32_t get_offset_of__machineStore_3() { return static_cast<int32_t>(offsetof(X509StoreManager_t1740460067_StaticFields, ____machineStore_3)); }
	inline X509Stores_t3001420399 * get__machineStore_3() const { return ____machineStore_3; }
	inline X509Stores_t3001420399 ** get_address_of__machineStore_3() { return &____machineStore_3; }
	inline void set__machineStore_3(X509Stores_t3001420399 * value)
	{
		____machineStore_3 = value;
		Il2CppCodeGenWriteBarrier(&____machineStore_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
