﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.TypedObject[]
struct TypedObjectU5BU5D_t948546190;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KeySequence
struct  KeySequence_t746093258  : public Il2CppObject
{
public:
	// System.Xml.Schema.TypedObject[] System.Xml.Schema.KeySequence::ks
	TypedObjectU5BU5D_t948546190* ___ks_0;
	// System.Int32 System.Xml.Schema.KeySequence::dim
	int32_t ___dim_1;
	// System.Int32 System.Xml.Schema.KeySequence::hashcode
	int32_t ___hashcode_2;
	// System.Int32 System.Xml.Schema.KeySequence::posline
	int32_t ___posline_3;
	// System.Int32 System.Xml.Schema.KeySequence::poscol
	int32_t ___poscol_4;

public:
	inline static int32_t get_offset_of_ks_0() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___ks_0)); }
	inline TypedObjectU5BU5D_t948546190* get_ks_0() const { return ___ks_0; }
	inline TypedObjectU5BU5D_t948546190** get_address_of_ks_0() { return &___ks_0; }
	inline void set_ks_0(TypedObjectU5BU5D_t948546190* value)
	{
		___ks_0 = value;
		Il2CppCodeGenWriteBarrier(&___ks_0, value);
	}

	inline static int32_t get_offset_of_dim_1() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___dim_1)); }
	inline int32_t get_dim_1() const { return ___dim_1; }
	inline int32_t* get_address_of_dim_1() { return &___dim_1; }
	inline void set_dim_1(int32_t value)
	{
		___dim_1 = value;
	}

	inline static int32_t get_offset_of_hashcode_2() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___hashcode_2)); }
	inline int32_t get_hashcode_2() const { return ___hashcode_2; }
	inline int32_t* get_address_of_hashcode_2() { return &___hashcode_2; }
	inline void set_hashcode_2(int32_t value)
	{
		___hashcode_2 = value;
	}

	inline static int32_t get_offset_of_posline_3() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___posline_3)); }
	inline int32_t get_posline_3() const { return ___posline_3; }
	inline int32_t* get_address_of_posline_3() { return &___posline_3; }
	inline void set_posline_3(int32_t value)
	{
		___posline_3 = value;
	}

	inline static int32_t get_offset_of_poscol_4() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___poscol_4)); }
	inline int32_t get_poscol_4() const { return ___poscol_4; }
	inline int32_t* get_address_of_poscol_4() { return &___poscol_4; }
	inline void set_poscol_4(int32_t value)
	{
		___poscol_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
