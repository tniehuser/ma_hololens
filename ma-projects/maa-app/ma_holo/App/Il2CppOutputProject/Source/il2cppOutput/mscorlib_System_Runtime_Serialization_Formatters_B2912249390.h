﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo943306207.h"

// System.IO.Stream
struct Stream_t3255436806;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Serialization.Formatters.Binary.ObjectWriter
struct ObjectWriter_t4293742132;
// System.IO.BinaryWriter
struct BinaryWriter_t3179371318;
// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall
struct BinaryMethodCall_t1773568836;
// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn
struct BinaryMethodReturn_t3366135632;
// System.Runtime.Serialization.Formatters.Binary.BinaryObject
struct BinaryObject_t763496928;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap
struct BinaryObjectWithMap_t2163984170;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped
struct BinaryObjectWithMapTyped_t3348677078;
// System.Runtime.Serialization.Formatters.Binary.BinaryObjectString
struct BinaryObjectString_t2307902425;
// System.Runtime.Serialization.Formatters.Binary.BinaryArray
struct BinaryArray_t2848964452;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped
struct MemberPrimitiveUnTyped_t3584398440;
// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped
struct MemberPrimitiveTyped_t3733510915;
// System.Runtime.Serialization.Formatters.Binary.ObjectNull
struct ObjectNull_t1089955196;
// System.Runtime.Serialization.Formatters.Binary.MemberReference
struct MemberReference_t1102219583;
// System.Runtime.Serialization.Formatters.Binary.BinaryAssembly
struct BinaryAssembly_t3375684671;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.__BinaryWriter
struct  __BinaryWriter_t2912249390  : public Il2CppObject
{
public:
	// System.IO.Stream System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::sout
	Stream_t3255436806 * ___sout_0;
	// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::formatterTypeStyle
	int32_t ___formatterTypeStyle_1;
	// System.Collections.Hashtable System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::objectMapTable
	Hashtable_t909839986 * ___objectMapTable_2;
	// System.Runtime.Serialization.Formatters.Binary.ObjectWriter System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::objectWriter
	ObjectWriter_t4293742132 * ___objectWriter_3;
	// System.IO.BinaryWriter System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::dataWriter
	BinaryWriter_t3179371318 * ___dataWriter_4;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::m_nestedObjectCount
	int32_t ___m_nestedObjectCount_5;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::nullCount
	int32_t ___nullCount_6;
	// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryMethodCall
	BinaryMethodCall_t1773568836 * ___binaryMethodCall_7;
	// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryMethodReturn
	BinaryMethodReturn_t3366135632 * ___binaryMethodReturn_8;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObject System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryObject
	BinaryObject_t763496928 * ___binaryObject_9;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryObjectWithMap
	BinaryObjectWithMap_t2163984170 * ___binaryObjectWithMap_10;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryObjectWithMapTyped
	BinaryObjectWithMapTyped_t3348677078 * ___binaryObjectWithMapTyped_11;
	// System.Runtime.Serialization.Formatters.Binary.BinaryObjectString System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryObjectString
	BinaryObjectString_t2307902425 * ___binaryObjectString_12;
	// System.Runtime.Serialization.Formatters.Binary.BinaryArray System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryArray
	BinaryArray_t2848964452 * ___binaryArray_13;
	// System.Byte[] System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::byteBuffer
	ByteU5BU5D_t3397334013* ___byteBuffer_14;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::chunkSize
	int32_t ___chunkSize_15;
	// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::memberPrimitiveUnTyped
	MemberPrimitiveUnTyped_t3584398440 * ___memberPrimitiveUnTyped_16;
	// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::memberPrimitiveTyped
	MemberPrimitiveTyped_t3733510915 * ___memberPrimitiveTyped_17;
	// System.Runtime.Serialization.Formatters.Binary.ObjectNull System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::objectNull
	ObjectNull_t1089955196 * ___objectNull_18;
	// System.Runtime.Serialization.Formatters.Binary.MemberReference System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::memberReference
	MemberReference_t1102219583 * ___memberReference_19;
	// System.Runtime.Serialization.Formatters.Binary.BinaryAssembly System.Runtime.Serialization.Formatters.Binary.__BinaryWriter::binaryAssembly
	BinaryAssembly_t3375684671 * ___binaryAssembly_20;

public:
	inline static int32_t get_offset_of_sout_0() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___sout_0)); }
	inline Stream_t3255436806 * get_sout_0() const { return ___sout_0; }
	inline Stream_t3255436806 ** get_address_of_sout_0() { return &___sout_0; }
	inline void set_sout_0(Stream_t3255436806 * value)
	{
		___sout_0 = value;
		Il2CppCodeGenWriteBarrier(&___sout_0, value);
	}

	inline static int32_t get_offset_of_formatterTypeStyle_1() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___formatterTypeStyle_1)); }
	inline int32_t get_formatterTypeStyle_1() const { return ___formatterTypeStyle_1; }
	inline int32_t* get_address_of_formatterTypeStyle_1() { return &___formatterTypeStyle_1; }
	inline void set_formatterTypeStyle_1(int32_t value)
	{
		___formatterTypeStyle_1 = value;
	}

	inline static int32_t get_offset_of_objectMapTable_2() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___objectMapTable_2)); }
	inline Hashtable_t909839986 * get_objectMapTable_2() const { return ___objectMapTable_2; }
	inline Hashtable_t909839986 ** get_address_of_objectMapTable_2() { return &___objectMapTable_2; }
	inline void set_objectMapTable_2(Hashtable_t909839986 * value)
	{
		___objectMapTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectMapTable_2, value);
	}

	inline static int32_t get_offset_of_objectWriter_3() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___objectWriter_3)); }
	inline ObjectWriter_t4293742132 * get_objectWriter_3() const { return ___objectWriter_3; }
	inline ObjectWriter_t4293742132 ** get_address_of_objectWriter_3() { return &___objectWriter_3; }
	inline void set_objectWriter_3(ObjectWriter_t4293742132 * value)
	{
		___objectWriter_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectWriter_3, value);
	}

	inline static int32_t get_offset_of_dataWriter_4() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___dataWriter_4)); }
	inline BinaryWriter_t3179371318 * get_dataWriter_4() const { return ___dataWriter_4; }
	inline BinaryWriter_t3179371318 ** get_address_of_dataWriter_4() { return &___dataWriter_4; }
	inline void set_dataWriter_4(BinaryWriter_t3179371318 * value)
	{
		___dataWriter_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataWriter_4, value);
	}

	inline static int32_t get_offset_of_m_nestedObjectCount_5() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___m_nestedObjectCount_5)); }
	inline int32_t get_m_nestedObjectCount_5() const { return ___m_nestedObjectCount_5; }
	inline int32_t* get_address_of_m_nestedObjectCount_5() { return &___m_nestedObjectCount_5; }
	inline void set_m_nestedObjectCount_5(int32_t value)
	{
		___m_nestedObjectCount_5 = value;
	}

	inline static int32_t get_offset_of_nullCount_6() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___nullCount_6)); }
	inline int32_t get_nullCount_6() const { return ___nullCount_6; }
	inline int32_t* get_address_of_nullCount_6() { return &___nullCount_6; }
	inline void set_nullCount_6(int32_t value)
	{
		___nullCount_6 = value;
	}

	inline static int32_t get_offset_of_binaryMethodCall_7() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryMethodCall_7)); }
	inline BinaryMethodCall_t1773568836 * get_binaryMethodCall_7() const { return ___binaryMethodCall_7; }
	inline BinaryMethodCall_t1773568836 ** get_address_of_binaryMethodCall_7() { return &___binaryMethodCall_7; }
	inline void set_binaryMethodCall_7(BinaryMethodCall_t1773568836 * value)
	{
		___binaryMethodCall_7 = value;
		Il2CppCodeGenWriteBarrier(&___binaryMethodCall_7, value);
	}

	inline static int32_t get_offset_of_binaryMethodReturn_8() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryMethodReturn_8)); }
	inline BinaryMethodReturn_t3366135632 * get_binaryMethodReturn_8() const { return ___binaryMethodReturn_8; }
	inline BinaryMethodReturn_t3366135632 ** get_address_of_binaryMethodReturn_8() { return &___binaryMethodReturn_8; }
	inline void set_binaryMethodReturn_8(BinaryMethodReturn_t3366135632 * value)
	{
		___binaryMethodReturn_8 = value;
		Il2CppCodeGenWriteBarrier(&___binaryMethodReturn_8, value);
	}

	inline static int32_t get_offset_of_binaryObject_9() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryObject_9)); }
	inline BinaryObject_t763496928 * get_binaryObject_9() const { return ___binaryObject_9; }
	inline BinaryObject_t763496928 ** get_address_of_binaryObject_9() { return &___binaryObject_9; }
	inline void set_binaryObject_9(BinaryObject_t763496928 * value)
	{
		___binaryObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObject_9, value);
	}

	inline static int32_t get_offset_of_binaryObjectWithMap_10() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryObjectWithMap_10)); }
	inline BinaryObjectWithMap_t2163984170 * get_binaryObjectWithMap_10() const { return ___binaryObjectWithMap_10; }
	inline BinaryObjectWithMap_t2163984170 ** get_address_of_binaryObjectWithMap_10() { return &___binaryObjectWithMap_10; }
	inline void set_binaryObjectWithMap_10(BinaryObjectWithMap_t2163984170 * value)
	{
		___binaryObjectWithMap_10 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObjectWithMap_10, value);
	}

	inline static int32_t get_offset_of_binaryObjectWithMapTyped_11() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryObjectWithMapTyped_11)); }
	inline BinaryObjectWithMapTyped_t3348677078 * get_binaryObjectWithMapTyped_11() const { return ___binaryObjectWithMapTyped_11; }
	inline BinaryObjectWithMapTyped_t3348677078 ** get_address_of_binaryObjectWithMapTyped_11() { return &___binaryObjectWithMapTyped_11; }
	inline void set_binaryObjectWithMapTyped_11(BinaryObjectWithMapTyped_t3348677078 * value)
	{
		___binaryObjectWithMapTyped_11 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObjectWithMapTyped_11, value);
	}

	inline static int32_t get_offset_of_binaryObjectString_12() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryObjectString_12)); }
	inline BinaryObjectString_t2307902425 * get_binaryObjectString_12() const { return ___binaryObjectString_12; }
	inline BinaryObjectString_t2307902425 ** get_address_of_binaryObjectString_12() { return &___binaryObjectString_12; }
	inline void set_binaryObjectString_12(BinaryObjectString_t2307902425 * value)
	{
		___binaryObjectString_12 = value;
		Il2CppCodeGenWriteBarrier(&___binaryObjectString_12, value);
	}

	inline static int32_t get_offset_of_binaryArray_13() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryArray_13)); }
	inline BinaryArray_t2848964452 * get_binaryArray_13() const { return ___binaryArray_13; }
	inline BinaryArray_t2848964452 ** get_address_of_binaryArray_13() { return &___binaryArray_13; }
	inline void set_binaryArray_13(BinaryArray_t2848964452 * value)
	{
		___binaryArray_13 = value;
		Il2CppCodeGenWriteBarrier(&___binaryArray_13, value);
	}

	inline static int32_t get_offset_of_byteBuffer_14() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___byteBuffer_14)); }
	inline ByteU5BU5D_t3397334013* get_byteBuffer_14() const { return ___byteBuffer_14; }
	inline ByteU5BU5D_t3397334013** get_address_of_byteBuffer_14() { return &___byteBuffer_14; }
	inline void set_byteBuffer_14(ByteU5BU5D_t3397334013* value)
	{
		___byteBuffer_14 = value;
		Il2CppCodeGenWriteBarrier(&___byteBuffer_14, value);
	}

	inline static int32_t get_offset_of_chunkSize_15() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___chunkSize_15)); }
	inline int32_t get_chunkSize_15() const { return ___chunkSize_15; }
	inline int32_t* get_address_of_chunkSize_15() { return &___chunkSize_15; }
	inline void set_chunkSize_15(int32_t value)
	{
		___chunkSize_15 = value;
	}

	inline static int32_t get_offset_of_memberPrimitiveUnTyped_16() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___memberPrimitiveUnTyped_16)); }
	inline MemberPrimitiveUnTyped_t3584398440 * get_memberPrimitiveUnTyped_16() const { return ___memberPrimitiveUnTyped_16; }
	inline MemberPrimitiveUnTyped_t3584398440 ** get_address_of_memberPrimitiveUnTyped_16() { return &___memberPrimitiveUnTyped_16; }
	inline void set_memberPrimitiveUnTyped_16(MemberPrimitiveUnTyped_t3584398440 * value)
	{
		___memberPrimitiveUnTyped_16 = value;
		Il2CppCodeGenWriteBarrier(&___memberPrimitiveUnTyped_16, value);
	}

	inline static int32_t get_offset_of_memberPrimitiveTyped_17() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___memberPrimitiveTyped_17)); }
	inline MemberPrimitiveTyped_t3733510915 * get_memberPrimitiveTyped_17() const { return ___memberPrimitiveTyped_17; }
	inline MemberPrimitiveTyped_t3733510915 ** get_address_of_memberPrimitiveTyped_17() { return &___memberPrimitiveTyped_17; }
	inline void set_memberPrimitiveTyped_17(MemberPrimitiveTyped_t3733510915 * value)
	{
		___memberPrimitiveTyped_17 = value;
		Il2CppCodeGenWriteBarrier(&___memberPrimitiveTyped_17, value);
	}

	inline static int32_t get_offset_of_objectNull_18() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___objectNull_18)); }
	inline ObjectNull_t1089955196 * get_objectNull_18() const { return ___objectNull_18; }
	inline ObjectNull_t1089955196 ** get_address_of_objectNull_18() { return &___objectNull_18; }
	inline void set_objectNull_18(ObjectNull_t1089955196 * value)
	{
		___objectNull_18 = value;
		Il2CppCodeGenWriteBarrier(&___objectNull_18, value);
	}

	inline static int32_t get_offset_of_memberReference_19() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___memberReference_19)); }
	inline MemberReference_t1102219583 * get_memberReference_19() const { return ___memberReference_19; }
	inline MemberReference_t1102219583 ** get_address_of_memberReference_19() { return &___memberReference_19; }
	inline void set_memberReference_19(MemberReference_t1102219583 * value)
	{
		___memberReference_19 = value;
		Il2CppCodeGenWriteBarrier(&___memberReference_19, value);
	}

	inline static int32_t get_offset_of_binaryAssembly_20() { return static_cast<int32_t>(offsetof(__BinaryWriter_t2912249390, ___binaryAssembly_20)); }
	inline BinaryAssembly_t3375684671 * get_binaryAssembly_20() const { return ___binaryAssembly_20; }
	inline BinaryAssembly_t3375684671 ** get_address_of_binaryAssembly_20() { return &___binaryAssembly_20; }
	inline void set_binaryAssembly_20(BinaryAssembly_t3375684671 * value)
	{
		___binaryAssembly_20 = value;
		Il2CppCodeGenWriteBarrier(&___binaryAssembly_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
