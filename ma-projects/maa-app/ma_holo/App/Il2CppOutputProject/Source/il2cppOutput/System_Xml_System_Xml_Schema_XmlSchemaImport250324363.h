﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaExternal3943748629.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t2400301303;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaImport
struct  XmlSchemaImport_t250324363  : public XmlSchemaExternal_t3943748629
{
public:
	// System.String System.Xml.Schema.XmlSchemaImport::ns
	String_t* ___ns_12;
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaImport::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_13;

public:
	inline static int32_t get_offset_of_ns_12() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t250324363, ___ns_12)); }
	inline String_t* get_ns_12() const { return ___ns_12; }
	inline String_t** get_address_of_ns_12() { return &___ns_12; }
	inline void set_ns_12(String_t* value)
	{
		___ns_12 = value;
		Il2CppCodeGenWriteBarrier(&___ns_12, value);
	}

	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t250324363, ___annotation_13)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier(&___annotation_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
