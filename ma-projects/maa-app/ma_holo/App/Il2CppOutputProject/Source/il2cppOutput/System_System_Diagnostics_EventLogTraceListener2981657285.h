﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Diagnostics_TraceListener3414949279.h"

// System.Diagnostics.EventLog
struct EventLog_t681067562;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.EventLogTraceListener
struct  EventLogTraceListener_t2981657285  : public TraceListener_t3414949279
{
public:
	// System.Diagnostics.EventLog System.Diagnostics.EventLogTraceListener::event_log
	EventLog_t681067562 * ___event_log_9;
	// System.String System.Diagnostics.EventLogTraceListener::name
	String_t* ___name_10;

public:
	inline static int32_t get_offset_of_event_log_9() { return static_cast<int32_t>(offsetof(EventLogTraceListener_t2981657285, ___event_log_9)); }
	inline EventLog_t681067562 * get_event_log_9() const { return ___event_log_9; }
	inline EventLog_t681067562 ** get_address_of_event_log_9() { return &___event_log_9; }
	inline void set_event_log_9(EventLog_t681067562 * value)
	{
		___event_log_9 = value;
		Il2CppCodeGenWriteBarrier(&___event_log_9, value);
	}

	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(EventLogTraceListener_t2981657285, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier(&___name_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
