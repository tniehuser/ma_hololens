﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CredentialCache
struct  CredentialCache_t1992799279  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Net.CredentialCache::cache
	Hashtable_t909839986 * ___cache_0;
	// System.Collections.Hashtable System.Net.CredentialCache::cacheForHosts
	Hashtable_t909839986 * ___cacheForHosts_1;
	// System.Int32 System.Net.CredentialCache::m_version
	int32_t ___m_version_2;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___cache_0)); }
	inline Hashtable_t909839986 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t909839986 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t909839986 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier(&___cache_0, value);
	}

	inline static int32_t get_offset_of_cacheForHosts_1() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___cacheForHosts_1)); }
	inline Hashtable_t909839986 * get_cacheForHosts_1() const { return ___cacheForHosts_1; }
	inline Hashtable_t909839986 ** get_address_of_cacheForHosts_1() { return &___cacheForHosts_1; }
	inline void set_cacheForHosts_1(Hashtable_t909839986 * value)
	{
		___cacheForHosts_1 = value;
		Il2CppCodeGenWriteBarrier(&___cacheForHosts_1, value);
	}

	inline static int32_t get_offset_of_m_version_2() { return static_cast<int32_t>(offsetof(CredentialCache_t1992799279, ___m_version_2)); }
	inline int32_t get_m_version_2() const { return ___m_version_2; }
	inline int32_t* get_address_of_m_version_2() { return &___m_version_2; }
	inline void set_m_version_2(int32_t value)
	{
		___m_version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
