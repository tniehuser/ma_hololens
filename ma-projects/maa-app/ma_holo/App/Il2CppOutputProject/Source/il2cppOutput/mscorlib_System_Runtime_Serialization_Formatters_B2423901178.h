﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4279057407.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord
struct  SerializationHeaderRecord_t2423901178  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryFormatterMajorVersion
	int32_t ___binaryFormatterMajorVersion_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryFormatterMinorVersion
	int32_t ___binaryFormatterMinorVersion_1;
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::topId
	int32_t ___topId_3;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::headerId
	int32_t ___headerId_4;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::majorVersion
	int32_t ___majorVersion_5;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::minorVersion
	int32_t ___minorVersion_6;

public:
	inline static int32_t get_offset_of_binaryFormatterMajorVersion_0() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___binaryFormatterMajorVersion_0)); }
	inline int32_t get_binaryFormatterMajorVersion_0() const { return ___binaryFormatterMajorVersion_0; }
	inline int32_t* get_address_of_binaryFormatterMajorVersion_0() { return &___binaryFormatterMajorVersion_0; }
	inline void set_binaryFormatterMajorVersion_0(int32_t value)
	{
		___binaryFormatterMajorVersion_0 = value;
	}

	inline static int32_t get_offset_of_binaryFormatterMinorVersion_1() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___binaryFormatterMinorVersion_1)); }
	inline int32_t get_binaryFormatterMinorVersion_1() const { return ___binaryFormatterMinorVersion_1; }
	inline int32_t* get_address_of_binaryFormatterMinorVersion_1() { return &___binaryFormatterMinorVersion_1; }
	inline void set_binaryFormatterMinorVersion_1(int32_t value)
	{
		___binaryFormatterMinorVersion_1 = value;
	}

	inline static int32_t get_offset_of_binaryHeaderEnum_2() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___binaryHeaderEnum_2)); }
	inline int32_t get_binaryHeaderEnum_2() const { return ___binaryHeaderEnum_2; }
	inline int32_t* get_address_of_binaryHeaderEnum_2() { return &___binaryHeaderEnum_2; }
	inline void set_binaryHeaderEnum_2(int32_t value)
	{
		___binaryHeaderEnum_2 = value;
	}

	inline static int32_t get_offset_of_topId_3() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___topId_3)); }
	inline int32_t get_topId_3() const { return ___topId_3; }
	inline int32_t* get_address_of_topId_3() { return &___topId_3; }
	inline void set_topId_3(int32_t value)
	{
		___topId_3 = value;
	}

	inline static int32_t get_offset_of_headerId_4() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___headerId_4)); }
	inline int32_t get_headerId_4() const { return ___headerId_4; }
	inline int32_t* get_address_of_headerId_4() { return &___headerId_4; }
	inline void set_headerId_4(int32_t value)
	{
		___headerId_4 = value;
	}

	inline static int32_t get_offset_of_majorVersion_5() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___majorVersion_5)); }
	inline int32_t get_majorVersion_5() const { return ___majorVersion_5; }
	inline int32_t* get_address_of_majorVersion_5() { return &___majorVersion_5; }
	inline void set_majorVersion_5(int32_t value)
	{
		___majorVersion_5 = value;
	}

	inline static int32_t get_offset_of_minorVersion_6() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t2423901178, ___minorVersion_6)); }
	inline int32_t get_minorVersion_6() const { return ___minorVersion_6; }
	inline int32_t* get_address_of_minorVersion_6() { return &___minorVersion_6; }
	inline void set_minorVersion_6(int32_t value)
	{
		___minorVersion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
