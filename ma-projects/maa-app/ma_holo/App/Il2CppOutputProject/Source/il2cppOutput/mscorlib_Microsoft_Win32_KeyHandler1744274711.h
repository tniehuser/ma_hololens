﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.Win32.KeyHandler
struct  KeyHandler_t1744274711  : public Il2CppObject
{
public:
	// System.String Microsoft.Win32.KeyHandler::Dir
	String_t* ___Dir_2;
	// System.String Microsoft.Win32.KeyHandler::ActualDir
	String_t* ___ActualDir_3;
	// System.Boolean Microsoft.Win32.KeyHandler::IsVolatile
	bool ___IsVolatile_4;
	// System.Collections.Hashtable Microsoft.Win32.KeyHandler::values
	Hashtable_t909839986 * ___values_5;
	// System.String Microsoft.Win32.KeyHandler::file
	String_t* ___file_6;
	// System.Boolean Microsoft.Win32.KeyHandler::dirty
	bool ___dirty_7;

public:
	inline static int32_t get_offset_of_Dir_2() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___Dir_2)); }
	inline String_t* get_Dir_2() const { return ___Dir_2; }
	inline String_t** get_address_of_Dir_2() { return &___Dir_2; }
	inline void set_Dir_2(String_t* value)
	{
		___Dir_2 = value;
		Il2CppCodeGenWriteBarrier(&___Dir_2, value);
	}

	inline static int32_t get_offset_of_ActualDir_3() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___ActualDir_3)); }
	inline String_t* get_ActualDir_3() const { return ___ActualDir_3; }
	inline String_t** get_address_of_ActualDir_3() { return &___ActualDir_3; }
	inline void set_ActualDir_3(String_t* value)
	{
		___ActualDir_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActualDir_3, value);
	}

	inline static int32_t get_offset_of_IsVolatile_4() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___IsVolatile_4)); }
	inline bool get_IsVolatile_4() const { return ___IsVolatile_4; }
	inline bool* get_address_of_IsVolatile_4() { return &___IsVolatile_4; }
	inline void set_IsVolatile_4(bool value)
	{
		___IsVolatile_4 = value;
	}

	inline static int32_t get_offset_of_values_5() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___values_5)); }
	inline Hashtable_t909839986 * get_values_5() const { return ___values_5; }
	inline Hashtable_t909839986 ** get_address_of_values_5() { return &___values_5; }
	inline void set_values_5(Hashtable_t909839986 * value)
	{
		___values_5 = value;
		Il2CppCodeGenWriteBarrier(&___values_5, value);
	}

	inline static int32_t get_offset_of_file_6() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___file_6)); }
	inline String_t* get_file_6() const { return ___file_6; }
	inline String_t** get_address_of_file_6() { return &___file_6; }
	inline void set_file_6(String_t* value)
	{
		___file_6 = value;
		Il2CppCodeGenWriteBarrier(&___file_6, value);
	}

	inline static int32_t get_offset_of_dirty_7() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711, ___dirty_7)); }
	inline bool get_dirty_7() const { return ___dirty_7; }
	inline bool* get_address_of_dirty_7() { return &___dirty_7; }
	inline void set_dirty_7(bool value)
	{
		___dirty_7 = value;
	}
};

struct KeyHandler_t1744274711_StaticFields
{
public:
	// System.Collections.Hashtable Microsoft.Win32.KeyHandler::key_to_handler
	Hashtable_t909839986 * ___key_to_handler_0;
	// System.Collections.Hashtable Microsoft.Win32.KeyHandler::dir_to_handler
	Hashtable_t909839986 * ___dir_to_handler_1;
	// System.String Microsoft.Win32.KeyHandler::user_store
	String_t* ___user_store_8;
	// System.String Microsoft.Win32.KeyHandler::machine_store
	String_t* ___machine_store_9;

public:
	inline static int32_t get_offset_of_key_to_handler_0() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711_StaticFields, ___key_to_handler_0)); }
	inline Hashtable_t909839986 * get_key_to_handler_0() const { return ___key_to_handler_0; }
	inline Hashtable_t909839986 ** get_address_of_key_to_handler_0() { return &___key_to_handler_0; }
	inline void set_key_to_handler_0(Hashtable_t909839986 * value)
	{
		___key_to_handler_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_to_handler_0, value);
	}

	inline static int32_t get_offset_of_dir_to_handler_1() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711_StaticFields, ___dir_to_handler_1)); }
	inline Hashtable_t909839986 * get_dir_to_handler_1() const { return ___dir_to_handler_1; }
	inline Hashtable_t909839986 ** get_address_of_dir_to_handler_1() { return &___dir_to_handler_1; }
	inline void set_dir_to_handler_1(Hashtable_t909839986 * value)
	{
		___dir_to_handler_1 = value;
		Il2CppCodeGenWriteBarrier(&___dir_to_handler_1, value);
	}

	inline static int32_t get_offset_of_user_store_8() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711_StaticFields, ___user_store_8)); }
	inline String_t* get_user_store_8() const { return ___user_store_8; }
	inline String_t** get_address_of_user_store_8() { return &___user_store_8; }
	inline void set_user_store_8(String_t* value)
	{
		___user_store_8 = value;
		Il2CppCodeGenWriteBarrier(&___user_store_8, value);
	}

	inline static int32_t get_offset_of_machine_store_9() { return static_cast<int32_t>(offsetof(KeyHandler_t1744274711_StaticFields, ___machine_store_9)); }
	inline String_t* get_machine_store_9() const { return ___machine_store_9; }
	inline String_t** get_address_of_machine_store_9() { return &___machine_store_9; }
	inline void set_machine_store_9(String_t* value)
	{
		___machine_store_9 = value;
		Il2CppCodeGenWriteBarrier(&___machine_store_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
