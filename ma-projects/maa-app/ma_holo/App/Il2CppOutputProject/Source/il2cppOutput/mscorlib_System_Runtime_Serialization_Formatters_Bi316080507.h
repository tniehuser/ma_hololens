﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4268412390;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo
struct  BinaryAssemblyInfo_t316080507  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo::assemblyString
	String_t* ___assemblyString_0;
	// System.Reflection.Assembly System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo::assembly
	Assembly_t4268412390 * ___assembly_1;

public:
	inline static int32_t get_offset_of_assemblyString_0() { return static_cast<int32_t>(offsetof(BinaryAssemblyInfo_t316080507, ___assemblyString_0)); }
	inline String_t* get_assemblyString_0() const { return ___assemblyString_0; }
	inline String_t** get_address_of_assemblyString_0() { return &___assemblyString_0; }
	inline void set_assemblyString_0(String_t* value)
	{
		___assemblyString_0 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyString_0, value);
	}

	inline static int32_t get_offset_of_assembly_1() { return static_cast<int32_t>(offsetof(BinaryAssemblyInfo_t316080507, ___assembly_1)); }
	inline Assembly_t4268412390 * get_assembly_1() const { return ___assembly_1; }
	inline Assembly_t4268412390 ** get_address_of_assembly_1() { return &___assembly_1; }
	inline void set_assembly_1(Assembly_t4268412390 * value)
	{
		___assembly_1 = value;
		Il2CppCodeGenWriteBarrier(&___assembly_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
