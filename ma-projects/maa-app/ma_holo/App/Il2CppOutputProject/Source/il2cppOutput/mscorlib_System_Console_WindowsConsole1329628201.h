﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Console/WindowsConsole/WindowsCancelHandler
struct WindowsCancelHandler_t1197082027;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Console/WindowsConsole
struct  WindowsConsole_t1329628201  : public Il2CppObject
{
public:

public:
};

struct WindowsConsole_t1329628201_StaticFields
{
public:
	// System.Boolean System.Console/WindowsConsole::ctrlHandlerAdded
	bool ___ctrlHandlerAdded_0;
	// System.Console/WindowsConsole/WindowsCancelHandler System.Console/WindowsConsole::cancelHandler
	WindowsCancelHandler_t1197082027 * ___cancelHandler_1;

public:
	inline static int32_t get_offset_of_ctrlHandlerAdded_0() { return static_cast<int32_t>(offsetof(WindowsConsole_t1329628201_StaticFields, ___ctrlHandlerAdded_0)); }
	inline bool get_ctrlHandlerAdded_0() const { return ___ctrlHandlerAdded_0; }
	inline bool* get_address_of_ctrlHandlerAdded_0() { return &___ctrlHandlerAdded_0; }
	inline void set_ctrlHandlerAdded_0(bool value)
	{
		___ctrlHandlerAdded_0 = value;
	}

	inline static int32_t get_offset_of_cancelHandler_1() { return static_cast<int32_t>(offsetof(WindowsConsole_t1329628201_StaticFields, ___cancelHandler_1)); }
	inline WindowsCancelHandler_t1197082027 * get_cancelHandler_1() const { return ___cancelHandler_1; }
	inline WindowsCancelHandler_t1197082027 ** get_address_of_cancelHandler_1() { return &___cancelHandler_1; }
	inline void set_cancelHandler_1(WindowsCancelHandler_t1197082027 * value)
	{
		___cancelHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___cancelHandler_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
