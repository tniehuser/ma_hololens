﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypedReference
struct  TypedReference_t1025199857 
{
public:
	// System.RuntimeTypeHandle System.TypedReference::type
	RuntimeTypeHandle_t2330101084  ___type_0;
	// System.IntPtr System.TypedReference::Value
	IntPtr_t ___Value_1;
	// System.IntPtr System.TypedReference::Type
	IntPtr_t ___Type_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypedReference_t1025199857, ___type_0)); }
	inline RuntimeTypeHandle_t2330101084  get_type_0() const { return ___type_0; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(RuntimeTypeHandle_t2330101084  value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypedReference_t1025199857, ___Value_1)); }
	inline IntPtr_t get_Value_1() const { return ___Value_1; }
	inline IntPtr_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(IntPtr_t value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(TypedReference_t1025199857, ___Type_2)); }
	inline IntPtr_t get_Type_2() const { return ___Type_2; }
	inline IntPtr_t* get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(IntPtr_t value)
	{
		___Type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
