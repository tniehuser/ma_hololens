﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Mono.Net.Security.IMonoTlsProvider
struct IMonoTlsProvider_t2506971578;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsProviderFactory
struct  MonoTlsProviderFactory_t3928617369  : public Il2CppObject
{
public:

public:
};

struct MonoTlsProviderFactory_t3928617369_StaticFields
{
public:
	// System.Object Mono.Net.Security.MonoTlsProviderFactory::locker
	Il2CppObject * ___locker_0;
	// System.Boolean Mono.Net.Security.MonoTlsProviderFactory::initialized
	bool ___initialized_1;
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsProviderFactory::defaultProvider
	Il2CppObject * ___defaultProvider_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mono.Net.Security.MonoTlsProviderFactory::providerRegistration
	Dictionary_2_t3943999495 * ___providerRegistration_3;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___locker_0)); }
	inline Il2CppObject * get_locker_0() const { return ___locker_0; }
	inline Il2CppObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(Il2CppObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier(&___locker_0, value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_defaultProvider_2() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___defaultProvider_2)); }
	inline Il2CppObject * get_defaultProvider_2() const { return ___defaultProvider_2; }
	inline Il2CppObject ** get_address_of_defaultProvider_2() { return &___defaultProvider_2; }
	inline void set_defaultProvider_2(Il2CppObject * value)
	{
		___defaultProvider_2 = value;
		Il2CppCodeGenWriteBarrier(&___defaultProvider_2, value);
	}

	inline static int32_t get_offset_of_providerRegistration_3() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___providerRegistration_3)); }
	inline Dictionary_2_t3943999495 * get_providerRegistration_3() const { return ___providerRegistration_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_providerRegistration_3() { return &___providerRegistration_3; }
	inline void set_providerRegistration_3(Dictionary_2_t3943999495 * value)
	{
		___providerRegistration_3 = value;
		Il2CppCodeGenWriteBarrier(&___providerRegistration_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
