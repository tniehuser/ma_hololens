﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2509106130;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<System.Object>
struct  Enumerator_t3019169210 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::_q
	Queue_1_t2509106130 * ____q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.Queue`1/Enumerator::_currentElement
	Il2CppObject * ____currentElement_3;

public:
	inline static int32_t get_offset_of__q_0() { return static_cast<int32_t>(offsetof(Enumerator_t3019169210, ____q_0)); }
	inline Queue_1_t2509106130 * get__q_0() const { return ____q_0; }
	inline Queue_1_t2509106130 ** get_address_of__q_0() { return &____q_0; }
	inline void set__q_0(Queue_1_t2509106130 * value)
	{
		____q_0 = value;
		Il2CppCodeGenWriteBarrier(&____q_0, value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3019169210, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3019169210, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_t3019169210, ____currentElement_3)); }
	inline Il2CppObject * get__currentElement_3() const { return ____currentElement_3; }
	inline Il2CppObject ** get_address_of__currentElement_3() { return &____currentElement_3; }
	inline void set__currentElement_3(Il2CppObject * value)
	{
		____currentElement_3 = value;
		Il2CppCodeGenWriteBarrier(&____currentElement_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
