﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.Remoting.Messaging.MessageDictionary
struct MessageDictionary_t4157710479;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.MessageDictionary/DictionaryEnumerator
struct  DictionaryEnumerator_t4235333696  : public Il2CppObject
{
public:
	// System.Runtime.Remoting.Messaging.MessageDictionary System.Runtime.Remoting.Messaging.MessageDictionary/DictionaryEnumerator::_methodDictionary
	MessageDictionary_t4157710479 * ____methodDictionary_0;
	// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Messaging.MessageDictionary/DictionaryEnumerator::_hashtableEnum
	Il2CppObject * ____hashtableEnum_1;
	// System.Int32 System.Runtime.Remoting.Messaging.MessageDictionary/DictionaryEnumerator::_posMethod
	int32_t ____posMethod_2;

public:
	inline static int32_t get_offset_of__methodDictionary_0() { return static_cast<int32_t>(offsetof(DictionaryEnumerator_t4235333696, ____methodDictionary_0)); }
	inline MessageDictionary_t4157710479 * get__methodDictionary_0() const { return ____methodDictionary_0; }
	inline MessageDictionary_t4157710479 ** get_address_of__methodDictionary_0() { return &____methodDictionary_0; }
	inline void set__methodDictionary_0(MessageDictionary_t4157710479 * value)
	{
		____methodDictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&____methodDictionary_0, value);
	}

	inline static int32_t get_offset_of__hashtableEnum_1() { return static_cast<int32_t>(offsetof(DictionaryEnumerator_t4235333696, ____hashtableEnum_1)); }
	inline Il2CppObject * get__hashtableEnum_1() const { return ____hashtableEnum_1; }
	inline Il2CppObject ** get_address_of__hashtableEnum_1() { return &____hashtableEnum_1; }
	inline void set__hashtableEnum_1(Il2CppObject * value)
	{
		____hashtableEnum_1 = value;
		Il2CppCodeGenWriteBarrier(&____hashtableEnum_1, value);
	}

	inline static int32_t get_offset_of__posMethod_2() { return static_cast<int32_t>(offsetof(DictionaryEnumerator_t4235333696, ____posMethod_2)); }
	inline int32_t get__posMethod_2() const { return ____posMethod_2; }
	inline int32_t* get_address_of__posMethod_2() { return &____posMethod_2; }
	inline void set__posMethod_2(int32_t value)
	{
		____posMethod_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
