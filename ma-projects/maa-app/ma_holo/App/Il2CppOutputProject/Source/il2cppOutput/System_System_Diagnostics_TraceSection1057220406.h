﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceSection
struct  TraceSection_t1057220406  : public ConfigurationElement_t1776195828
{
public:

public:
};

struct TraceSection_t1057220406_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.TraceSection::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.TraceSection::_propListeners
	ConfigurationProperty_t2048066811 * ____propListeners_16;
	// System.Configuration.ConfigurationProperty System.Diagnostics.TraceSection::_propAutoFlush
	ConfigurationProperty_t2048066811 * ____propAutoFlush_17;
	// System.Configuration.ConfigurationProperty System.Diagnostics.TraceSection::_propIndentSize
	ConfigurationProperty_t2048066811 * ____propIndentSize_18;
	// System.Configuration.ConfigurationProperty System.Diagnostics.TraceSection::_propUseGlobalLock
	ConfigurationProperty_t2048066811 * ____propUseGlobalLock_19;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(TraceSection_t1057220406_StaticFields, ____properties_15)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}

	inline static int32_t get_offset_of__propListeners_16() { return static_cast<int32_t>(offsetof(TraceSection_t1057220406_StaticFields, ____propListeners_16)); }
	inline ConfigurationProperty_t2048066811 * get__propListeners_16() const { return ____propListeners_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propListeners_16() { return &____propListeners_16; }
	inline void set__propListeners_16(ConfigurationProperty_t2048066811 * value)
	{
		____propListeners_16 = value;
		Il2CppCodeGenWriteBarrier(&____propListeners_16, value);
	}

	inline static int32_t get_offset_of__propAutoFlush_17() { return static_cast<int32_t>(offsetof(TraceSection_t1057220406_StaticFields, ____propAutoFlush_17)); }
	inline ConfigurationProperty_t2048066811 * get__propAutoFlush_17() const { return ____propAutoFlush_17; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propAutoFlush_17() { return &____propAutoFlush_17; }
	inline void set__propAutoFlush_17(ConfigurationProperty_t2048066811 * value)
	{
		____propAutoFlush_17 = value;
		Il2CppCodeGenWriteBarrier(&____propAutoFlush_17, value);
	}

	inline static int32_t get_offset_of__propIndentSize_18() { return static_cast<int32_t>(offsetof(TraceSection_t1057220406_StaticFields, ____propIndentSize_18)); }
	inline ConfigurationProperty_t2048066811 * get__propIndentSize_18() const { return ____propIndentSize_18; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propIndentSize_18() { return &____propIndentSize_18; }
	inline void set__propIndentSize_18(ConfigurationProperty_t2048066811 * value)
	{
		____propIndentSize_18 = value;
		Il2CppCodeGenWriteBarrier(&____propIndentSize_18, value);
	}

	inline static int32_t get_offset_of__propUseGlobalLock_19() { return static_cast<int32_t>(offsetof(TraceSection_t1057220406_StaticFields, ____propUseGlobalLock_19)); }
	inline ConfigurationProperty_t2048066811 * get__propUseGlobalLock_19() const { return ____propUseGlobalLock_19; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propUseGlobalLock_19() { return &____propUseGlobalLock_19; }
	inline void set__propUseGlobalLock_19(ConfigurationProperty_t2048066811 * value)
	{
		____propUseGlobalLock_19 = value;
		Il2CppCodeGenWriteBarrier(&____propUseGlobalLock_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
