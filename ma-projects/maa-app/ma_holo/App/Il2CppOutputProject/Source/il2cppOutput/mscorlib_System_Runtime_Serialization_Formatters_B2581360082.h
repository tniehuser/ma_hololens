﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2877339122.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3272317689.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi642857832.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2706794673.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[]
struct BinaryTypeEnumU5BU5D_t1351797255;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Runtime.Serialization.Formatters.Binary.ParseRecord
struct ParseRecord_t2674254118;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectProgress
struct  ObjectProgress_t2581360082  : public Il2CppObject
{
public:
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectProgress::isInitial
	bool ___isInitial_1;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectProgress::count
	int32_t ___count_2;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum System.Runtime.Serialization.Formatters.Binary.ObjectProgress::expectedType
	int32_t ___expectedType_3;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectProgress::expectedTypeInformation
	Il2CppObject * ___expectedTypeInformation_4;
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectProgress::name
	String_t* ___name_5;
	// System.Runtime.Serialization.Formatters.Binary.InternalObjectTypeE System.Runtime.Serialization.Formatters.Binary.ObjectProgress::objectTypeEnum
	int32_t ___objectTypeEnum_6;
	// System.Runtime.Serialization.Formatters.Binary.InternalMemberTypeE System.Runtime.Serialization.Formatters.Binary.ObjectProgress::memberTypeEnum
	int32_t ___memberTypeEnum_7;
	// System.Runtime.Serialization.Formatters.Binary.InternalMemberValueE System.Runtime.Serialization.Formatters.Binary.ObjectProgress::memberValueEnum
	int32_t ___memberValueEnum_8;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectProgress::dtType
	Type_t * ___dtType_9;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectProgress::numItems
	int32_t ___numItems_10;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum System.Runtime.Serialization.Formatters.Binary.ObjectProgress::binaryTypeEnum
	int32_t ___binaryTypeEnum_11;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectProgress::typeInformation
	Il2CppObject * ___typeInformation_12;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectProgress::nullCount
	int32_t ___nullCount_13;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectProgress::memberLength
	int32_t ___memberLength_14;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[] System.Runtime.Serialization.Formatters.Binary.ObjectProgress::binaryTypeEnumA
	BinaryTypeEnumU5BU5D_t1351797255* ___binaryTypeEnumA_15;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ObjectProgress::typeInformationA
	ObjectU5BU5D_t3614634134* ___typeInformationA_16;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.ObjectProgress::memberNames
	StringU5BU5D_t1642385972* ___memberNames_17;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.ObjectProgress::memberTypes
	TypeU5BU5D_t1664964607* ___memberTypes_18;
	// System.Runtime.Serialization.Formatters.Binary.ParseRecord System.Runtime.Serialization.Formatters.Binary.ObjectProgress::pr
	ParseRecord_t2674254118 * ___pr_19;

public:
	inline static int32_t get_offset_of_isInitial_1() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___isInitial_1)); }
	inline bool get_isInitial_1() const { return ___isInitial_1; }
	inline bool* get_address_of_isInitial_1() { return &___isInitial_1; }
	inline void set_isInitial_1(bool value)
	{
		___isInitial_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_expectedType_3() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___expectedType_3)); }
	inline int32_t get_expectedType_3() const { return ___expectedType_3; }
	inline int32_t* get_address_of_expectedType_3() { return &___expectedType_3; }
	inline void set_expectedType_3(int32_t value)
	{
		___expectedType_3 = value;
	}

	inline static int32_t get_offset_of_expectedTypeInformation_4() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___expectedTypeInformation_4)); }
	inline Il2CppObject * get_expectedTypeInformation_4() const { return ___expectedTypeInformation_4; }
	inline Il2CppObject ** get_address_of_expectedTypeInformation_4() { return &___expectedTypeInformation_4; }
	inline void set_expectedTypeInformation_4(Il2CppObject * value)
	{
		___expectedTypeInformation_4 = value;
		Il2CppCodeGenWriteBarrier(&___expectedTypeInformation_4, value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier(&___name_5, value);
	}

	inline static int32_t get_offset_of_objectTypeEnum_6() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___objectTypeEnum_6)); }
	inline int32_t get_objectTypeEnum_6() const { return ___objectTypeEnum_6; }
	inline int32_t* get_address_of_objectTypeEnum_6() { return &___objectTypeEnum_6; }
	inline void set_objectTypeEnum_6(int32_t value)
	{
		___objectTypeEnum_6 = value;
	}

	inline static int32_t get_offset_of_memberTypeEnum_7() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___memberTypeEnum_7)); }
	inline int32_t get_memberTypeEnum_7() const { return ___memberTypeEnum_7; }
	inline int32_t* get_address_of_memberTypeEnum_7() { return &___memberTypeEnum_7; }
	inline void set_memberTypeEnum_7(int32_t value)
	{
		___memberTypeEnum_7 = value;
	}

	inline static int32_t get_offset_of_memberValueEnum_8() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___memberValueEnum_8)); }
	inline int32_t get_memberValueEnum_8() const { return ___memberValueEnum_8; }
	inline int32_t* get_address_of_memberValueEnum_8() { return &___memberValueEnum_8; }
	inline void set_memberValueEnum_8(int32_t value)
	{
		___memberValueEnum_8 = value;
	}

	inline static int32_t get_offset_of_dtType_9() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___dtType_9)); }
	inline Type_t * get_dtType_9() const { return ___dtType_9; }
	inline Type_t ** get_address_of_dtType_9() { return &___dtType_9; }
	inline void set_dtType_9(Type_t * value)
	{
		___dtType_9 = value;
		Il2CppCodeGenWriteBarrier(&___dtType_9, value);
	}

	inline static int32_t get_offset_of_numItems_10() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___numItems_10)); }
	inline int32_t get_numItems_10() const { return ___numItems_10; }
	inline int32_t* get_address_of_numItems_10() { return &___numItems_10; }
	inline void set_numItems_10(int32_t value)
	{
		___numItems_10 = value;
	}

	inline static int32_t get_offset_of_binaryTypeEnum_11() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___binaryTypeEnum_11)); }
	inline int32_t get_binaryTypeEnum_11() const { return ___binaryTypeEnum_11; }
	inline int32_t* get_address_of_binaryTypeEnum_11() { return &___binaryTypeEnum_11; }
	inline void set_binaryTypeEnum_11(int32_t value)
	{
		___binaryTypeEnum_11 = value;
	}

	inline static int32_t get_offset_of_typeInformation_12() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___typeInformation_12)); }
	inline Il2CppObject * get_typeInformation_12() const { return ___typeInformation_12; }
	inline Il2CppObject ** get_address_of_typeInformation_12() { return &___typeInformation_12; }
	inline void set_typeInformation_12(Il2CppObject * value)
	{
		___typeInformation_12 = value;
		Il2CppCodeGenWriteBarrier(&___typeInformation_12, value);
	}

	inline static int32_t get_offset_of_nullCount_13() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___nullCount_13)); }
	inline int32_t get_nullCount_13() const { return ___nullCount_13; }
	inline int32_t* get_address_of_nullCount_13() { return &___nullCount_13; }
	inline void set_nullCount_13(int32_t value)
	{
		___nullCount_13 = value;
	}

	inline static int32_t get_offset_of_memberLength_14() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___memberLength_14)); }
	inline int32_t get_memberLength_14() const { return ___memberLength_14; }
	inline int32_t* get_address_of_memberLength_14() { return &___memberLength_14; }
	inline void set_memberLength_14(int32_t value)
	{
		___memberLength_14 = value;
	}

	inline static int32_t get_offset_of_binaryTypeEnumA_15() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___binaryTypeEnumA_15)); }
	inline BinaryTypeEnumU5BU5D_t1351797255* get_binaryTypeEnumA_15() const { return ___binaryTypeEnumA_15; }
	inline BinaryTypeEnumU5BU5D_t1351797255** get_address_of_binaryTypeEnumA_15() { return &___binaryTypeEnumA_15; }
	inline void set_binaryTypeEnumA_15(BinaryTypeEnumU5BU5D_t1351797255* value)
	{
		___binaryTypeEnumA_15 = value;
		Il2CppCodeGenWriteBarrier(&___binaryTypeEnumA_15, value);
	}

	inline static int32_t get_offset_of_typeInformationA_16() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___typeInformationA_16)); }
	inline ObjectU5BU5D_t3614634134* get_typeInformationA_16() const { return ___typeInformationA_16; }
	inline ObjectU5BU5D_t3614634134** get_address_of_typeInformationA_16() { return &___typeInformationA_16; }
	inline void set_typeInformationA_16(ObjectU5BU5D_t3614634134* value)
	{
		___typeInformationA_16 = value;
		Il2CppCodeGenWriteBarrier(&___typeInformationA_16, value);
	}

	inline static int32_t get_offset_of_memberNames_17() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___memberNames_17)); }
	inline StringU5BU5D_t1642385972* get_memberNames_17() const { return ___memberNames_17; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_17() { return &___memberNames_17; }
	inline void set_memberNames_17(StringU5BU5D_t1642385972* value)
	{
		___memberNames_17 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_17, value);
	}

	inline static int32_t get_offset_of_memberTypes_18() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___memberTypes_18)); }
	inline TypeU5BU5D_t1664964607* get_memberTypes_18() const { return ___memberTypes_18; }
	inline TypeU5BU5D_t1664964607** get_address_of_memberTypes_18() { return &___memberTypes_18; }
	inline void set_memberTypes_18(TypeU5BU5D_t1664964607* value)
	{
		___memberTypes_18 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_18, value);
	}

	inline static int32_t get_offset_of_pr_19() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082, ___pr_19)); }
	inline ParseRecord_t2674254118 * get_pr_19() const { return ___pr_19; }
	inline ParseRecord_t2674254118 ** get_address_of_pr_19() { return &___pr_19; }
	inline void set_pr_19(ParseRecord_t2674254118 * value)
	{
		___pr_19 = value;
		Il2CppCodeGenWriteBarrier(&___pr_19, value);
	}
};

struct ObjectProgress_t2581360082_StaticFields
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectProgress::opRecordIdCount
	int32_t ___opRecordIdCount_0;

public:
	inline static int32_t get_offset_of_opRecordIdCount_0() { return static_cast<int32_t>(offsetof(ObjectProgress_t2581360082_StaticFields, ___opRecordIdCount_0)); }
	inline int32_t get_opRecordIdCount_0() const { return ___opRecordIdCount_0; }
	inline int32_t* get_address_of_opRecordIdCount_0() { return &___opRecordIdCount_0; }
	inline void set_opRecordIdCount_0(int32_t value)
	{
		___opRecordIdCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
