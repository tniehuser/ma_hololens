﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Globalization.CalendarData[]
struct CalendarDataU5BU5D_t3322756653;
// System.Globalization.CultureData
struct CultureData_t3400086592;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureData
struct  CultureData_t3400086592  : public Il2CppObject
{
public:
	// System.String System.Globalization.CultureData::sAM1159
	String_t* ___sAM1159_0;
	// System.String System.Globalization.CultureData::sPM2359
	String_t* ___sPM2359_1;
	// System.String System.Globalization.CultureData::sTimeSeparator
	String_t* ___sTimeSeparator_2;
	// System.String[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureData::saLongTimes
	StringU5BU5D_t1642385972* ___saLongTimes_3;
	// System.String[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureData::saShortTimes
	StringU5BU5D_t1642385972* ___saShortTimes_4;
	// System.Int32 System.Globalization.CultureData::iFirstDayOfWeek
	int32_t ___iFirstDayOfWeek_5;
	// System.Int32 System.Globalization.CultureData::iFirstWeekOfYear
	int32_t ___iFirstWeekOfYear_6;
	// System.Int32[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureData::waCalendars
	Int32U5BU5D_t3030399641* ___waCalendars_7;
	// System.Globalization.CalendarData[] System.Globalization.CultureData::calendars
	CalendarDataU5BU5D_t3322756653* ___calendars_8;
	// System.String System.Globalization.CultureData::sISO639Language
	String_t* ___sISO639Language_9;
	// System.String System.Globalization.CultureData::sRealName
	String_t* ___sRealName_10;
	// System.Boolean System.Globalization.CultureData::bUseOverrides
	bool ___bUseOverrides_11;
	// System.Int32 System.Globalization.CultureData::calendarId
	int32_t ___calendarId_12;
	// System.Int32 System.Globalization.CultureData::numberIndex
	int32_t ___numberIndex_13;
	// System.Int32 System.Globalization.CultureData::iDefaultAnsiCodePage
	int32_t ___iDefaultAnsiCodePage_14;
	// System.Int32 System.Globalization.CultureData::iDefaultOemCodePage
	int32_t ___iDefaultOemCodePage_15;
	// System.Int32 System.Globalization.CultureData::iDefaultMacCodePage
	int32_t ___iDefaultMacCodePage_16;
	// System.Int32 System.Globalization.CultureData::iDefaultEbcdicCodePage
	int32_t ___iDefaultEbcdicCodePage_17;
	// System.Boolean System.Globalization.CultureData::isRightToLeft
	bool ___isRightToLeft_18;
	// System.String System.Globalization.CultureData::sListSeparator
	String_t* ___sListSeparator_19;

public:
	inline static int32_t get_offset_of_sAM1159_0() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sAM1159_0)); }
	inline String_t* get_sAM1159_0() const { return ___sAM1159_0; }
	inline String_t** get_address_of_sAM1159_0() { return &___sAM1159_0; }
	inline void set_sAM1159_0(String_t* value)
	{
		___sAM1159_0 = value;
		Il2CppCodeGenWriteBarrier(&___sAM1159_0, value);
	}

	inline static int32_t get_offset_of_sPM2359_1() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sPM2359_1)); }
	inline String_t* get_sPM2359_1() const { return ___sPM2359_1; }
	inline String_t** get_address_of_sPM2359_1() { return &___sPM2359_1; }
	inline void set_sPM2359_1(String_t* value)
	{
		___sPM2359_1 = value;
		Il2CppCodeGenWriteBarrier(&___sPM2359_1, value);
	}

	inline static int32_t get_offset_of_sTimeSeparator_2() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sTimeSeparator_2)); }
	inline String_t* get_sTimeSeparator_2() const { return ___sTimeSeparator_2; }
	inline String_t** get_address_of_sTimeSeparator_2() { return &___sTimeSeparator_2; }
	inline void set_sTimeSeparator_2(String_t* value)
	{
		___sTimeSeparator_2 = value;
		Il2CppCodeGenWriteBarrier(&___sTimeSeparator_2, value);
	}

	inline static int32_t get_offset_of_saLongTimes_3() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___saLongTimes_3)); }
	inline StringU5BU5D_t1642385972* get_saLongTimes_3() const { return ___saLongTimes_3; }
	inline StringU5BU5D_t1642385972** get_address_of_saLongTimes_3() { return &___saLongTimes_3; }
	inline void set_saLongTimes_3(StringU5BU5D_t1642385972* value)
	{
		___saLongTimes_3 = value;
		Il2CppCodeGenWriteBarrier(&___saLongTimes_3, value);
	}

	inline static int32_t get_offset_of_saShortTimes_4() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___saShortTimes_4)); }
	inline StringU5BU5D_t1642385972* get_saShortTimes_4() const { return ___saShortTimes_4; }
	inline StringU5BU5D_t1642385972** get_address_of_saShortTimes_4() { return &___saShortTimes_4; }
	inline void set_saShortTimes_4(StringU5BU5D_t1642385972* value)
	{
		___saShortTimes_4 = value;
		Il2CppCodeGenWriteBarrier(&___saShortTimes_4, value);
	}

	inline static int32_t get_offset_of_iFirstDayOfWeek_5() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iFirstDayOfWeek_5)); }
	inline int32_t get_iFirstDayOfWeek_5() const { return ___iFirstDayOfWeek_5; }
	inline int32_t* get_address_of_iFirstDayOfWeek_5() { return &___iFirstDayOfWeek_5; }
	inline void set_iFirstDayOfWeek_5(int32_t value)
	{
		___iFirstDayOfWeek_5 = value;
	}

	inline static int32_t get_offset_of_iFirstWeekOfYear_6() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iFirstWeekOfYear_6)); }
	inline int32_t get_iFirstWeekOfYear_6() const { return ___iFirstWeekOfYear_6; }
	inline int32_t* get_address_of_iFirstWeekOfYear_6() { return &___iFirstWeekOfYear_6; }
	inline void set_iFirstWeekOfYear_6(int32_t value)
	{
		___iFirstWeekOfYear_6 = value;
	}

	inline static int32_t get_offset_of_waCalendars_7() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___waCalendars_7)); }
	inline Int32U5BU5D_t3030399641* get_waCalendars_7() const { return ___waCalendars_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_waCalendars_7() { return &___waCalendars_7; }
	inline void set_waCalendars_7(Int32U5BU5D_t3030399641* value)
	{
		___waCalendars_7 = value;
		Il2CppCodeGenWriteBarrier(&___waCalendars_7, value);
	}

	inline static int32_t get_offset_of_calendars_8() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___calendars_8)); }
	inline CalendarDataU5BU5D_t3322756653* get_calendars_8() const { return ___calendars_8; }
	inline CalendarDataU5BU5D_t3322756653** get_address_of_calendars_8() { return &___calendars_8; }
	inline void set_calendars_8(CalendarDataU5BU5D_t3322756653* value)
	{
		___calendars_8 = value;
		Il2CppCodeGenWriteBarrier(&___calendars_8, value);
	}

	inline static int32_t get_offset_of_sISO639Language_9() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sISO639Language_9)); }
	inline String_t* get_sISO639Language_9() const { return ___sISO639Language_9; }
	inline String_t** get_address_of_sISO639Language_9() { return &___sISO639Language_9; }
	inline void set_sISO639Language_9(String_t* value)
	{
		___sISO639Language_9 = value;
		Il2CppCodeGenWriteBarrier(&___sISO639Language_9, value);
	}

	inline static int32_t get_offset_of_sRealName_10() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sRealName_10)); }
	inline String_t* get_sRealName_10() const { return ___sRealName_10; }
	inline String_t** get_address_of_sRealName_10() { return &___sRealName_10; }
	inline void set_sRealName_10(String_t* value)
	{
		___sRealName_10 = value;
		Il2CppCodeGenWriteBarrier(&___sRealName_10, value);
	}

	inline static int32_t get_offset_of_bUseOverrides_11() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___bUseOverrides_11)); }
	inline bool get_bUseOverrides_11() const { return ___bUseOverrides_11; }
	inline bool* get_address_of_bUseOverrides_11() { return &___bUseOverrides_11; }
	inline void set_bUseOverrides_11(bool value)
	{
		___bUseOverrides_11 = value;
	}

	inline static int32_t get_offset_of_calendarId_12() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___calendarId_12)); }
	inline int32_t get_calendarId_12() const { return ___calendarId_12; }
	inline int32_t* get_address_of_calendarId_12() { return &___calendarId_12; }
	inline void set_calendarId_12(int32_t value)
	{
		___calendarId_12 = value;
	}

	inline static int32_t get_offset_of_numberIndex_13() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___numberIndex_13)); }
	inline int32_t get_numberIndex_13() const { return ___numberIndex_13; }
	inline int32_t* get_address_of_numberIndex_13() { return &___numberIndex_13; }
	inline void set_numberIndex_13(int32_t value)
	{
		___numberIndex_13 = value;
	}

	inline static int32_t get_offset_of_iDefaultAnsiCodePage_14() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iDefaultAnsiCodePage_14)); }
	inline int32_t get_iDefaultAnsiCodePage_14() const { return ___iDefaultAnsiCodePage_14; }
	inline int32_t* get_address_of_iDefaultAnsiCodePage_14() { return &___iDefaultAnsiCodePage_14; }
	inline void set_iDefaultAnsiCodePage_14(int32_t value)
	{
		___iDefaultAnsiCodePage_14 = value;
	}

	inline static int32_t get_offset_of_iDefaultOemCodePage_15() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iDefaultOemCodePage_15)); }
	inline int32_t get_iDefaultOemCodePage_15() const { return ___iDefaultOemCodePage_15; }
	inline int32_t* get_address_of_iDefaultOemCodePage_15() { return &___iDefaultOemCodePage_15; }
	inline void set_iDefaultOemCodePage_15(int32_t value)
	{
		___iDefaultOemCodePage_15 = value;
	}

	inline static int32_t get_offset_of_iDefaultMacCodePage_16() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iDefaultMacCodePage_16)); }
	inline int32_t get_iDefaultMacCodePage_16() const { return ___iDefaultMacCodePage_16; }
	inline int32_t* get_address_of_iDefaultMacCodePage_16() { return &___iDefaultMacCodePage_16; }
	inline void set_iDefaultMacCodePage_16(int32_t value)
	{
		___iDefaultMacCodePage_16 = value;
	}

	inline static int32_t get_offset_of_iDefaultEbcdicCodePage_17() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___iDefaultEbcdicCodePage_17)); }
	inline int32_t get_iDefaultEbcdicCodePage_17() const { return ___iDefaultEbcdicCodePage_17; }
	inline int32_t* get_address_of_iDefaultEbcdicCodePage_17() { return &___iDefaultEbcdicCodePage_17; }
	inline void set_iDefaultEbcdicCodePage_17(int32_t value)
	{
		___iDefaultEbcdicCodePage_17 = value;
	}

	inline static int32_t get_offset_of_isRightToLeft_18() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___isRightToLeft_18)); }
	inline bool get_isRightToLeft_18() const { return ___isRightToLeft_18; }
	inline bool* get_address_of_isRightToLeft_18() { return &___isRightToLeft_18; }
	inline void set_isRightToLeft_18(bool value)
	{
		___isRightToLeft_18 = value;
	}

	inline static int32_t get_offset_of_sListSeparator_19() { return static_cast<int32_t>(offsetof(CultureData_t3400086592, ___sListSeparator_19)); }
	inline String_t* get_sListSeparator_19() const { return ___sListSeparator_19; }
	inline String_t** get_address_of_sListSeparator_19() { return &___sListSeparator_19; }
	inline void set_sListSeparator_19(String_t* value)
	{
		___sListSeparator_19 = value;
		Il2CppCodeGenWriteBarrier(&___sListSeparator_19, value);
	}
};

struct CultureData_t3400086592_StaticFields
{
public:
	// System.Globalization.CultureData System.Globalization.CultureData::s_Invariant
	CultureData_t3400086592 * ___s_Invariant_20;

public:
	inline static int32_t get_offset_of_s_Invariant_20() { return static_cast<int32_t>(offsetof(CultureData_t3400086592_StaticFields, ___s_Invariant_20)); }
	inline CultureData_t3400086592 * get_s_Invariant_20() const { return ___s_Invariant_20; }
	inline CultureData_t3400086592 ** get_address_of_s_Invariant_20() { return &___s_Invariant_20; }
	inline void set_s_Invariant_20(CultureData_t3400086592 * value)
	{
		___s_Invariant_20 = value;
		Il2CppCodeGenWriteBarrier(&___s_Invariant_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureData
struct CultureData_t3400086592_marshaled_pinvoke
{
	char* ___sAM1159_0;
	char* ___sPM2359_1;
	char* ___sTimeSeparator_2;
	StringU5BU5D_t1642385972* ___saLongTimes_3;
	StringU5BU5D_t1642385972* ___saShortTimes_4;
	int32_t ___iFirstDayOfWeek_5;
	int32_t ___iFirstWeekOfYear_6;
	Int32U5BU5D_t3030399641* ___waCalendars_7;
	CalendarDataU5BU5D_t3322756653* ___calendars_8;
	char* ___sISO639Language_9;
	char* ___sRealName_10;
	int32_t ___bUseOverrides_11;
	int32_t ___calendarId_12;
	int32_t ___numberIndex_13;
	int32_t ___iDefaultAnsiCodePage_14;
	int32_t ___iDefaultOemCodePage_15;
	int32_t ___iDefaultMacCodePage_16;
	int32_t ___iDefaultEbcdicCodePage_17;
	int32_t ___isRightToLeft_18;
	char* ___sListSeparator_19;
};
// Native definition for COM marshalling of System.Globalization.CultureData
struct CultureData_t3400086592_marshaled_com
{
	Il2CppChar* ___sAM1159_0;
	Il2CppChar* ___sPM2359_1;
	Il2CppChar* ___sTimeSeparator_2;
	StringU5BU5D_t1642385972* ___saLongTimes_3;
	StringU5BU5D_t1642385972* ___saShortTimes_4;
	int32_t ___iFirstDayOfWeek_5;
	int32_t ___iFirstWeekOfYear_6;
	Int32U5BU5D_t3030399641* ___waCalendars_7;
	CalendarDataU5BU5D_t3322756653* ___calendars_8;
	Il2CppChar* ___sISO639Language_9;
	Il2CppChar* ___sRealName_10;
	int32_t ___bUseOverrides_11;
	int32_t ___calendarId_12;
	int32_t ___numberIndex_13;
	int32_t ___iDefaultAnsiCodePage_14;
	int32_t ___iDefaultOemCodePage_15;
	int32_t ___iDefaultMacCodePage_16;
	int32_t ___iDefaultEbcdicCodePage_17;
	int32_t ___isRightToLeft_18;
	Il2CppChar* ___sListSeparator_19;
};
