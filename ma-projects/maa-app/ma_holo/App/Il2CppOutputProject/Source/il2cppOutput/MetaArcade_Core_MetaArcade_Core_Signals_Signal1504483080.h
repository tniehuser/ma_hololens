﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Action>
struct List_1_t2595592884;
// MetaArcade.Core.Signals.WeakActionList
struct WeakActionList_t119657226;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MetaArcade.Core.Signals.Signal
struct  Signal_t1504483080  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Action> MetaArcade.Core.Signals.Signal::m_oneTimeListeners
	List_1_t2595592884 * ___m_oneTimeListeners_0;
	// System.Collections.Generic.List`1<System.Action> MetaArcade.Core.Signals.Signal::m_listeners
	List_1_t2595592884 * ___m_listeners_1;
	// MetaArcade.Core.Signals.WeakActionList MetaArcade.Core.Signals.Signal::m_weakListeners
	WeakActionList_t119657226 * ___m_weakListeners_2;

public:
	inline static int32_t get_offset_of_m_oneTimeListeners_0() { return static_cast<int32_t>(offsetof(Signal_t1504483080, ___m_oneTimeListeners_0)); }
	inline List_1_t2595592884 * get_m_oneTimeListeners_0() const { return ___m_oneTimeListeners_0; }
	inline List_1_t2595592884 ** get_address_of_m_oneTimeListeners_0() { return &___m_oneTimeListeners_0; }
	inline void set_m_oneTimeListeners_0(List_1_t2595592884 * value)
	{
		___m_oneTimeListeners_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_oneTimeListeners_0, value);
	}

	inline static int32_t get_offset_of_m_listeners_1() { return static_cast<int32_t>(offsetof(Signal_t1504483080, ___m_listeners_1)); }
	inline List_1_t2595592884 * get_m_listeners_1() const { return ___m_listeners_1; }
	inline List_1_t2595592884 ** get_address_of_m_listeners_1() { return &___m_listeners_1; }
	inline void set_m_listeners_1(List_1_t2595592884 * value)
	{
		___m_listeners_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_listeners_1, value);
	}

	inline static int32_t get_offset_of_m_weakListeners_2() { return static_cast<int32_t>(offsetof(Signal_t1504483080, ___m_weakListeners_2)); }
	inline WeakActionList_t119657226 * get_m_weakListeners_2() const { return ___m_weakListeners_2; }
	inline WeakActionList_t119657226 ** get_address_of_m_weakListeners_2() { return &___m_weakListeners_2; }
	inline void set_m_weakListeners_2(WeakActionList_t119657226 * value)
	{
		___m_weakListeners_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_weakListeners_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
