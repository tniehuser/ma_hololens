﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_MD5CryptoSer4009738925.h"
#include "mscorlib_System_Security_Cryptography_RC2Transform93681661.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer2688843926.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Signa145198701.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Signa925064184.h"
#include "mscorlib_System_Security_Cryptography_SHA1Internal3873507626.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3913997830.h"
#include "mscorlib_System_Security_Cryptography_TripleDESTra2384411462.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPer935902940.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPe3182667772.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer3088110531.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer4165817673.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss3702443043.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss1079931101.h"
#include "mscorlib_System_Security_Permissions_GacIdentityPer944690662.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorag174977552.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3424948193.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3796409103.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3643383499.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPer41069825.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPe592240881.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3437457615.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3003512226.h"
#include "mscorlib_System_Security_Permissions_PermissionSta3557289502.h"
#include "mscorlib_System_Security_Permissions_PublisherIden4085785193.h"
#include "mscorlib_System_Security_Permissions_ReflectionPer2253232706.h"
#include "mscorlib_System_Security_Permissions_ReflectionPerm604469518.h"
#include "mscorlib_System_Security_Permissions_RegistryPermi3065894328.h"
#include "mscorlib_System_Security_Permissions_RegistryPermi1881045040.h"
#include "mscorlib_System_Security_Permissions_SecurityAction446643378.h"
#include "mscorlib_System_Security_Permissions_SecurityAttri2016950496.h"
#include "mscorlib_System_Security_Permissions_SecurityPermis502442079.h"
#include "mscorlib_System_Security_Permissions_SecurityPermi1642245049.h"
#include "mscorlib_System_Security_Permissions_SiteIdentityP2362922692.h"
#include "mscorlib_System_Security_Permissions_StrongNameIde2574761831.h"
#include "mscorlib_System_Security_Permissions_StrongNameIde4214504621.h"
#include "mscorlib_System_Security_Permissions_StrongNamePub2860422703.h"
#include "mscorlib_System_Security_Permissions_UIPermission2463628927.h"
#include "mscorlib_System_Security_Permissions_UIPermissionC1473065281.h"
#include "mscorlib_System_Security_Permissions_UIPermissionW3535816307.h"
#include "mscorlib_System_Security_Permissions_UrlIdentityPe3056144520.h"
#include "mscorlib_System_Security_Permissions_ZoneIdentityPe639379881.h"
#include "mscorlib_System_Security_Policy_AllMembershipCondit717795426.h"
#include "mscorlib_System_Security_Policy_ApplicationTrust3968282840.h"
#include "mscorlib_System_Security_Policy_CodeConnectAccess3638993531.h"
#include "mscorlib_System_Security_Policy_CodeGroup1856851900.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies883770295.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies_Ke1645655740.h"
#include "mscorlib_System_Security_Policy_Evidence1407710183.h"
#include "mscorlib_System_Security_Policy_Evidence_EvidenceE2963371598.h"
#include "mscorlib_System_Security_Policy_EvidenceBase1783132120.h"
#include "mscorlib_System_Security_Policy_FileCodeGroup227438634.h"
#include "mscorlib_System_Security_Policy_FirstMatchCodeGrou2461363693.h"
#include "mscorlib_System_Security_Policy_GacInstalled342770895.h"
#include "mscorlib_System_Security_Policy_Hash2681437964.h"
#include "mscorlib_System_Security_Policy_MembershipCondition724086969.h"
#include "mscorlib_System_Security_Policy_NetCodeGroup853742759.h"
#include "mscorlib_System_Security_Policy_PermissionRequestE3776717977.h"
#include "mscorlib_System_Security_Policy_PolicyException3854570267.h"
#include "mscorlib_System_Security_Policy_PolicyLevel43919632.h"
#include "mscorlib_System_Security_Policy_PolicyStatement1594053347.h"
#include "mscorlib_System_Security_Policy_PolicyStatementAtt2506836895.h"
#include "mscorlib_System_Security_Policy_Publisher988316888.h"
#include "mscorlib_System_Security_Policy_Site1485138885.h"
#include "mscorlib_System_Security_Policy_StrongName2988747270.h"
#include "mscorlib_System_Security_Policy_StrongNameMembershi552614029.h"
#include "mscorlib_System_Security_Policy_UnionCodeGroup2013835339.h"
#include "mscorlib_System_Security_Policy_Url627176909.h"
#include "mscorlib_System_Security_Policy_Zone2399370716.h"
#include "mscorlib_System_Security_Policy_ZoneMembershipCond3435127283.h"
#include "mscorlib_System_Security_Principal_PrincipalPolicy289802916.h"
#include "mscorlib_System_Security_Principal_TokenImpersonat2477301187.h"
#include "mscorlib_System_Security_CodeAccessPermission3468021764.h"
#include "mscorlib_System_Security_HostSecurityManager1605199171.h"
#include "mscorlib_System_Security_HostSecurityManagerOption2988201557.h"
#include "mscorlib_System_Security_NamedPermissionSet4149260796.h"
#include "mscorlib_System_Security_PermissionBuilder3954309332.h"
#include "mscorlib_System_Security_PermissionSet1941658161.h"
#include "mscorlib_System_Security_PolicyLevelType2082293816.h"
#include "mscorlib_System_Security_SecureString412202620.h"
#include "mscorlib_System_Security_SecurityElementType2871790626.h"
#include "mscorlib_System_Security_SecurityElement2325568386.h"
#include "mscorlib_System_Security_SecurityElement_SecurityA1594670782.h"
#include "mscorlib_System_Security_SecurityException887327375.h"
#include "mscorlib_System_Security_SecurityManager3191249573.h"
#include "mscorlib_System_Security_SecurityZone140334334.h"
#include "mscorlib_System_Security_XmlSyntaxException2895742617.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (MD5CryptoServiceProvider_t4009738925), -1, sizeof(MD5CryptoServiceProvider_t4009738925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1301[6] = 
{
	MD5CryptoServiceProvider_t4009738925::get_offset_of__H_4(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of_buff_5(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of_count_6(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of__ProcessingBuffer_7(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of__ProcessingBufferCount_8(),
	MD5CryptoServiceProvider_t4009738925_StaticFields::get_offset_of_K_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (RC2Transform_t93681661), -1, sizeof(RC2Transform_t93681661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1302[7] = 
{
	RC2Transform_t93681661::get_offset_of_R0_12(),
	RC2Transform_t93681661::get_offset_of_R1_13(),
	RC2Transform_t93681661::get_offset_of_R2_14(),
	RC2Transform_t93681661::get_offset_of_R3_15(),
	RC2Transform_t93681661::get_offset_of_K_16(),
	RC2Transform_t93681661::get_offset_of_j_17(),
	RC2Transform_t93681661_StaticFields::get_offset_of_pitable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (RNGCryptoServiceProvider_t2688843926), -1, sizeof(RNGCryptoServiceProvider_t2688843926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1303[2] = 
{
	RNGCryptoServiceProvider_t2688843926_StaticFields::get_offset_of__lock_0(),
	RNGCryptoServiceProvider_t2688843926::get_offset_of__handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (RSAPKCS1SignatureDeformatter_t145198701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[2] = 
{
	RSAPKCS1SignatureDeformatter_t145198701::get_offset_of_rsa_0(),
	RSAPKCS1SignatureDeformatter_t145198701::get_offset_of_hashName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (RSAPKCS1SignatureFormatter_t925064184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[2] = 
{
	RSAPKCS1SignatureFormatter_t925064184::get_offset_of_rsa_0(),
	RSAPKCS1SignatureFormatter_t925064184::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (SHA1Internal_t3873507626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1306[5] = 
{
	SHA1Internal_t3873507626::get_offset_of__H_0(),
	SHA1Internal_t3873507626::get_offset_of_count_1(),
	SHA1Internal_t3873507626::get_offset_of__ProcessingBuffer_2(),
	SHA1Internal_t3873507626::get_offset_of__ProcessingBufferCount_3(),
	SHA1Internal_t3873507626::get_offset_of_buff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (SHA1CryptoServiceProvider_t3913997830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[1] = 
{
	SHA1CryptoServiceProvider_t3913997830::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (TripleDESTransform_t2384411462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[6] = 
{
	TripleDESTransform_t2384411462::get_offset_of_E1_12(),
	TripleDESTransform_t2384411462::get_offset_of_D2_13(),
	TripleDESTransform_t2384411462::get_offset_of_E3_14(),
	TripleDESTransform_t2384411462::get_offset_of_D1_15(),
	TripleDESTransform_t2384411462::get_offset_of_E2_16(),
	TripleDESTransform_t2384411462::get_offset_of_D3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (EnvironmentPermission_t935902940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1309[3] = 
{
	EnvironmentPermission_t935902940::get_offset_of__state_0(),
	EnvironmentPermission_t935902940::get_offset_of_readList_1(),
	EnvironmentPermission_t935902940::get_offset_of_writeList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (EnvironmentPermissionAccess_t3182667772)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1310[5] = 
{
	EnvironmentPermissionAccess_t3182667772::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (FileDialogPermission_t3088110531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1311[1] = 
{
	FileDialogPermission_t3088110531::get_offset_of__access_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (FileDialogPermissionAccess_t4165817673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1312[5] = 
{
	FileDialogPermissionAccess_t4165817673::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (FileIOPermission_t3702443043), -1, sizeof(FileIOPermission_t3702443043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1313[9] = 
{
	FileIOPermission_t3702443043_StaticFields::get_offset_of_BadPathNameCharacters_0(),
	FileIOPermission_t3702443043_StaticFields::get_offset_of_BadFileNameCharacters_1(),
	FileIOPermission_t3702443043::get_offset_of_m_Unrestricted_2(),
	FileIOPermission_t3702443043::get_offset_of_m_AllFilesAccess_3(),
	FileIOPermission_t3702443043::get_offset_of_m_AllLocalFilesAccess_4(),
	FileIOPermission_t3702443043::get_offset_of_readList_5(),
	FileIOPermission_t3702443043::get_offset_of_writeList_6(),
	FileIOPermission_t3702443043::get_offset_of_appendList_7(),
	FileIOPermission_t3702443043::get_offset_of_pathList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (FileIOPermissionAccess_t1079931101)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[7] = 
{
	FileIOPermissionAccess_t1079931101::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (GacIdentityPermission_t944690662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (IsolatedStorageContainment_t174977552)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1318[13] = 
{
	IsolatedStorageContainment_t174977552::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (IsolatedStorageFilePermission_t3424948193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (IsolatedStoragePermission_t3796409103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1320[5] = 
{
	IsolatedStoragePermission_t3796409103::get_offset_of_m_userQuota_0(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_machineQuota_1(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_expirationDays_2(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_permanentData_3(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_allowed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (KeyContainerPermission_t3643383499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1321[2] = 
{
	KeyContainerPermission_t3643383499::get_offset_of__accessEntries_0(),
	KeyContainerPermission_t3643383499::get_offset_of__flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (KeyContainerPermissionAccessEntry_t41069825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1322[6] = 
{
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__flags_0(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__containerName_1(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__spec_2(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__store_3(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__providerName_4(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (KeyContainerPermissionAccessEntryCollection_t592240881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1323[1] = 
{
	KeyContainerPermissionAccessEntryCollection_t592240881::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (KeyContainerPermissionAccessEntryEnumerator_t3437457615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1324[1] = 
{
	KeyContainerPermissionAccessEntryEnumerator_t3437457615::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (KeyContainerPermissionFlags_t3003512226)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1325[12] = 
{
	KeyContainerPermissionFlags_t3003512226::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (PermissionState_t3557289502)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1326[3] = 
{
	PermissionState_t3557289502::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (PublisherIdentityPermission_t4085785193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[1] = 
{
	PublisherIdentityPermission_t4085785193::get_offset_of_x509_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (ReflectionPermission_t2253232706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1328[1] = 
{
	ReflectionPermission_t2253232706::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (ReflectionPermissionFlag_t604469518)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1329[7] = 
{
	ReflectionPermissionFlag_t604469518::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (RegistryPermission_t3065894328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[4] = 
{
	RegistryPermission_t3065894328::get_offset_of__state_0(),
	RegistryPermission_t3065894328::get_offset_of_createList_1(),
	RegistryPermission_t3065894328::get_offset_of_readList_2(),
	RegistryPermission_t3065894328::get_offset_of_writeList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (RegistryPermissionAccess_t1881045040)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1331[6] = 
{
	RegistryPermissionAccess_t1881045040::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (SecurityAction_t446643378)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1332[10] = 
{
	SecurityAction_t446643378::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (SecurityAttribute_t2016950496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (SecurityPermission_t502442079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[1] = 
{
	SecurityPermission_t502442079::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (SecurityPermissionFlag_t1642245049)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1335[17] = 
{
	SecurityPermissionFlag_t1642245049::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (SiteIdentityPermission_t2362922692), -1, sizeof(SiteIdentityPermission_t2362922692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1336[2] = 
{
	SiteIdentityPermission_t2362922692::get_offset_of__site_0(),
	SiteIdentityPermission_t2362922692_StaticFields::get_offset_of_valid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (StrongNameIdentityPermission_t2574761831), -1, sizeof(StrongNameIdentityPermission_t2574761831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1337[3] = 
{
	StrongNameIdentityPermission_t2574761831_StaticFields::get_offset_of_defaultVersion_0(),
	StrongNameIdentityPermission_t2574761831::get_offset_of__state_1(),
	StrongNameIdentityPermission_t2574761831::get_offset_of__list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (SNIP_t4214504621)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1338[3] = 
{
	SNIP_t4214504621::get_offset_of_PublicKey_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t4214504621::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t4214504621::get_offset_of_AssemblyVersion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (StrongNamePublicKeyBlob_t2860422703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1339[1] = 
{
	StrongNamePublicKeyBlob_t2860422703::get_offset_of_pubkey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (UIPermission_t2463628927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[2] = 
{
	UIPermission_t2463628927::get_offset_of__window_0(),
	UIPermission_t2463628927::get_offset_of__clipboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (UIPermissionClipboard_t1473065281)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1341[4] = 
{
	UIPermissionClipboard_t1473065281::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (UIPermissionWindow_t3535816307)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1342[5] = 
{
	UIPermissionWindow_t3535816307::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (UrlIdentityPermission_t3056144520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1343[1] = 
{
	UrlIdentityPermission_t3056144520::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (ZoneIdentityPermission_t639379881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[1] = 
{
	ZoneIdentityPermission_t639379881::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (AllMembershipCondition_t717795426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1345[1] = 
{
	AllMembershipCondition_t717795426::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (ApplicationTrust_t3968282840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[6] = 
{
	ApplicationTrust_t3968282840::get_offset_of__appid_0(),
	ApplicationTrust_t3968282840::get_offset_of__defaultPolicy_1(),
	ApplicationTrust_t3968282840::get_offset_of__xtranfo_2(),
	ApplicationTrust_t3968282840::get_offset_of__trustrun_3(),
	ApplicationTrust_t3968282840::get_offset_of__persist_4(),
	ApplicationTrust_t3968282840::get_offset_of_fullTrustAssemblies_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (CodeConnectAccess_t3638993531), -1, sizeof(CodeConnectAccess_t3638993531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1347[6] = 
{
	CodeConnectAccess_t3638993531_StaticFields::get_offset_of_AnyScheme_0(),
	CodeConnectAccess_t3638993531_StaticFields::get_offset_of_DefaultPort_1(),
	CodeConnectAccess_t3638993531_StaticFields::get_offset_of_OriginPort_2(),
	CodeConnectAccess_t3638993531_StaticFields::get_offset_of_OriginScheme_3(),
	CodeConnectAccess_t3638993531::get_offset_of__scheme_4(),
	CodeConnectAccess_t3638993531::get_offset_of__port_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (CodeGroup_t1856851900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1348[5] = 
{
	CodeGroup_t1856851900::get_offset_of_m_policy_0(),
	CodeGroup_t1856851900::get_offset_of_m_membershipCondition_1(),
	CodeGroup_t1856851900::get_offset_of_m_description_2(),
	CodeGroup_t1856851900::get_offset_of_m_name_3(),
	CodeGroup_t1856851900::get_offset_of_m_children_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (DefaultPolicies_t883770295), -1, sizeof(DefaultPolicies_t883770295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[12] = 
{
	DefaultPolicies_t883770295_StaticFields::get_offset_of__fxVersion_0(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__ecmaKey_1(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__ecma_2(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__msFinalKey_3(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__msFinal_4(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__fullTrust_5(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__localIntranet_6(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__internet_7(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__skipVerification_8(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__execution_9(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__nothing_10(),
	DefaultPolicies_t883770295_StaticFields::get_offset_of__everything_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (Key_t1645655740)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1350[3] = 
{
	Key_t1645655740::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (Evidence_t1407710183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1351[3] = 
{
	Evidence_t1407710183::get_offset_of__locked_0(),
	Evidence_t1407710183::get_offset_of_hostEvidenceList_1(),
	Evidence_t1407710183::get_offset_of_assemblyEvidenceList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (EvidenceEnumerator_t2963371598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[3] = 
{
	EvidenceEnumerator_t2963371598::get_offset_of_currentEnum_0(),
	EvidenceEnumerator_t2963371598::get_offset_of_hostEnum_1(),
	EvidenceEnumerator_t2963371598::get_offset_of_assemblyEnum_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (EvidenceBase_t1783132120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (FileCodeGroup_t227438634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1354[1] = 
{
	FileCodeGroup_t227438634::get_offset_of_m_access_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (FirstMatchCodeGroup_t2461363693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (GacInstalled_t342770895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (Hash_t2681437964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1357[2] = 
{
	Hash_t2681437964::get_offset_of_assembly_0(),
	Hash_t2681437964::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (MembershipConditionHelper_t724086969), -1, sizeof(MembershipConditionHelper_t724086969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1362[1] = 
{
	MembershipConditionHelper_t724086969_StaticFields::get_offset_of_XmlTag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (NetCodeGroup_t853742759), -1, sizeof(NetCodeGroup_t853742759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1363[4] = 
{
	NetCodeGroup_t853742759_StaticFields::get_offset_of_AbsentOriginScheme_5(),
	NetCodeGroup_t853742759_StaticFields::get_offset_of_AnyOtherOriginScheme_6(),
	NetCodeGroup_t853742759::get_offset_of__rules_7(),
	NetCodeGroup_t853742759::get_offset_of__hashcode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (PermissionRequestEvidence_t3776717977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1364[3] = 
{
	PermissionRequestEvidence_t3776717977::get_offset_of_requested_0(),
	PermissionRequestEvidence_t3776717977::get_offset_of_optional_1(),
	PermissionRequestEvidence_t3776717977::get_offset_of_denied_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (PolicyException_t3854570267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (PolicyLevel_t43919632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1366[8] = 
{
	PolicyLevel_t43919632::get_offset_of_label_0(),
	PolicyLevel_t43919632::get_offset_of_root_code_group_1(),
	PolicyLevel_t43919632::get_offset_of_full_trust_assemblies_2(),
	PolicyLevel_t43919632::get_offset_of_named_permission_sets_3(),
	PolicyLevel_t43919632::get_offset_of__location_4(),
	PolicyLevel_t43919632::get_offset_of__type_5(),
	PolicyLevel_t43919632::get_offset_of_fullNames_6(),
	PolicyLevel_t43919632::get_offset_of_xml_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (PolicyStatement_t1594053347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1367[2] = 
{
	PolicyStatement_t1594053347::get_offset_of_perms_0(),
	PolicyStatement_t1594053347::get_offset_of_attrs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (PolicyStatementAttribute_t2506836895)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1368[5] = 
{
	PolicyStatementAttribute_t2506836895::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (Publisher_t988316888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[1] = 
{
	Publisher_t988316888::get_offset_of_m_cert_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (Site_t1485138885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[1] = 
{
	Site_t1485138885::get_offset_of_origin_site_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (StrongName_t2988747270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1371[3] = 
{
	StrongName_t2988747270::get_offset_of_publickey_0(),
	StrongName_t2988747270::get_offset_of_name_1(),
	StrongName_t2988747270::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (StrongNameMembershipCondition_t552614029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1372[4] = 
{
	StrongNameMembershipCondition_t552614029::get_offset_of_version_0(),
	StrongNameMembershipCondition_t552614029::get_offset_of_blob_1(),
	StrongNameMembershipCondition_t552614029::get_offset_of_name_2(),
	StrongNameMembershipCondition_t552614029::get_offset_of_assemblyVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (UnionCodeGroup_t2013835339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (Url_t627176909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1374[1] = 
{
	Url_t627176909::get_offset_of_origin_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (Zone_t2399370716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1375[1] = 
{
	Zone_t2399370716::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (ZoneMembershipCondition_t3435127283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1376[2] = 
{
	ZoneMembershipCondition_t3435127283::get_offset_of_version_0(),
	ZoneMembershipCondition_t3435127283::get_offset_of_zone_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (PrincipalPolicy_t289802916)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1378[4] = 
{
	PrincipalPolicy_t289802916::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (TokenImpersonationLevel_t2477301187)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[6] = 
{
	TokenImpersonationLevel_t2477301187::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (CodeAccessPermission_t3468021764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (HostSecurityManager_t1605199171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (HostSecurityManagerOptions_t2988201557)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1382[8] = 
{
	HostSecurityManagerOptions_t2988201557::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (NamedPermissionSet_t4149260796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1388[2] = 
{
	NamedPermissionSet_t4149260796::get_offset_of_name_7(),
	NamedPermissionSet_t4149260796::get_offset_of_description_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (PermissionBuilder_t3954309332), -1, sizeof(PermissionBuilder_t3954309332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	PermissionBuilder_t3954309332_StaticFields::get_offset_of_psNone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (PermissionSet_t1941658161), -1, sizeof(PermissionSet_t1941658161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1390[7] = 
{
	PermissionSet_t1941658161_StaticFields::get_offset_of_psUnrestricted_0(),
	PermissionSet_t1941658161::get_offset_of_state_1(),
	PermissionSet_t1941658161::get_offset_of_list_2(),
	PermissionSet_t1941658161::get_offset_of__policyLevel_3(),
	PermissionSet_t1941658161::get_offset_of__readOnly_4(),
	PermissionSet_t1941658161::get_offset_of__ignored_5(),
	PermissionSet_t1941658161_StaticFields::get_offset_of_action_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (PolicyLevelType_t2082293816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1391[5] = 
{
	PolicyLevelType_t2082293816::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (SecureString_t412202620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1392[3] = 
{
	SecureString_t412202620::get_offset_of_length_0(),
	SecureString_t412202620::get_offset_of_disposed_1(),
	SecureString_t412202620::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (SecurityElementType_t2871790626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1393[4] = 
{
	SecurityElementType_t2871790626::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (SecurityElement_t2325568386), -1, sizeof(SecurityElement_t2325568386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1394[9] = 
{
	SecurityElement_t2325568386::get_offset_of_text_0(),
	SecurityElement_t2325568386::get_offset_of_tag_1(),
	SecurityElement_t2325568386::get_offset_of_attributes_2(),
	SecurityElement_t2325568386::get_offset_of_children_3(),
	SecurityElement_t2325568386_StaticFields::get_offset_of_invalid_tag_chars_4(),
	SecurityElement_t2325568386_StaticFields::get_offset_of_invalid_text_chars_5(),
	SecurityElement_t2325568386_StaticFields::get_offset_of_invalid_attr_name_chars_6(),
	SecurityElement_t2325568386_StaticFields::get_offset_of_invalid_attr_value_chars_7(),
	SecurityElement_t2325568386_StaticFields::get_offset_of_invalid_chars_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (SecurityAttribute_t1594670782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1395[2] = 
{
	SecurityAttribute_t1594670782::get_offset_of__name_0(),
	SecurityAttribute_t1594670782::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (SecurityException_t887327375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1396[8] = 
{
	SecurityException_t887327375::get_offset_of_permissionState_16(),
	SecurityException_t887327375::get_offset_of_permissionType_17(),
	SecurityException_t887327375::get_offset_of__granted_18(),
	SecurityException_t887327375::get_offset_of__refused_19(),
	SecurityException_t887327375::get_offset_of__demanded_20(),
	SecurityException_t887327375::get_offset_of__firstperm_21(),
	SecurityException_t887327375::get_offset_of__method_22(),
	SecurityException_t887327375::get_offset_of__evidence_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (SecurityManager_t3191249573), -1, sizeof(SecurityManager_t3191249573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1397[4] = 
{
	SecurityManager_t3191249573_StaticFields::get_offset_of__lockObject_0(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__hierarchy_1(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__level_2(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__execution_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (SecurityZone_t140334334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1398[7] = 
{
	SecurityZone_t140334334::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (XmlSyntaxException_t2895742617), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
