﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter745995970.h"

// System.Type
struct Type_t;
// System.ComponentModel.TypeConverter
struct TypeConverter_t745995970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NullableConverter
struct  NullableConverter_t1941973167  : public TypeConverter_t745995970
{
public:
	// System.Type System.ComponentModel.NullableConverter::nullableType
	Type_t * ___nullableType_4;
	// System.Type System.ComponentModel.NullableConverter::simpleType
	Type_t * ___simpleType_5;
	// System.ComponentModel.TypeConverter System.ComponentModel.NullableConverter::simpleTypeConverter
	TypeConverter_t745995970 * ___simpleTypeConverter_6;

public:
	inline static int32_t get_offset_of_nullableType_4() { return static_cast<int32_t>(offsetof(NullableConverter_t1941973167, ___nullableType_4)); }
	inline Type_t * get_nullableType_4() const { return ___nullableType_4; }
	inline Type_t ** get_address_of_nullableType_4() { return &___nullableType_4; }
	inline void set_nullableType_4(Type_t * value)
	{
		___nullableType_4 = value;
		Il2CppCodeGenWriteBarrier(&___nullableType_4, value);
	}

	inline static int32_t get_offset_of_simpleType_5() { return static_cast<int32_t>(offsetof(NullableConverter_t1941973167, ___simpleType_5)); }
	inline Type_t * get_simpleType_5() const { return ___simpleType_5; }
	inline Type_t ** get_address_of_simpleType_5() { return &___simpleType_5; }
	inline void set_simpleType_5(Type_t * value)
	{
		___simpleType_5 = value;
		Il2CppCodeGenWriteBarrier(&___simpleType_5, value);
	}

	inline static int32_t get_offset_of_simpleTypeConverter_6() { return static_cast<int32_t>(offsetof(NullableConverter_t1941973167, ___simpleTypeConverter_6)); }
	inline TypeConverter_t745995970 * get_simpleTypeConverter_6() const { return ___simpleTypeConverter_6; }
	inline TypeConverter_t745995970 ** get_address_of_simpleTypeConverter_6() { return &___simpleTypeConverter_6; }
	inline void set_simpleTypeConverter_6(TypeConverter_t745995970 * value)
	{
		___simpleTypeConverter_6 = value;
		Il2CppCodeGenWriteBarrier(&___simpleTypeConverter_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
