﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22361573779.h"

// System.Collections.Concurrent.ConcurrentDictionary`2/Node<System.String,System.Object>[]
struct NodeU5BU5D_t1077299993;
// System.Collections.Concurrent.ConcurrentDictionary`2/Node<System.String,System.Object>
struct Node_t4244254920;
// System.Collections.Concurrent.ConcurrentDictionary`2<System.String,System.Object>
struct ConcurrentDictionary_2_t2171449296;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0<System.String,System.Object>
struct  U3CGetEnumeratorU3Ec__Iterator0_t2362880871  : public Il2CppObject
{
public:
	// System.Collections.Concurrent.ConcurrentDictionary`2/Node<TKey,TValue>[] System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::<buckets>__0
	NodeU5BU5D_t1077299993* ___U3CbucketsU3E__0_0;
	// System.Int32 System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// System.Collections.Concurrent.ConcurrentDictionary`2/Node<TKey,TValue> System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::<current>__2
	Node_t4244254920 * ___U3CcurrentU3E__2_2;
	// System.Collections.Concurrent.ConcurrentDictionary`2<TKey,TValue> System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::$this
	ConcurrentDictionary_2_t2171449296 * ___U24this_3;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::$current
	KeyValuePair_2_t2361573779  ___U24current_4;
	// System.Boolean System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 System.Collections.Concurrent.ConcurrentDictionary`2/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CbucketsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U3CbucketsU3E__0_0)); }
	inline NodeU5BU5D_t1077299993* get_U3CbucketsU3E__0_0() const { return ___U3CbucketsU3E__0_0; }
	inline NodeU5BU5D_t1077299993** get_address_of_U3CbucketsU3E__0_0() { return &___U3CbucketsU3E__0_0; }
	inline void set_U3CbucketsU3E__0_0(NodeU5BU5D_t1077299993* value)
	{
		___U3CbucketsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbucketsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U3CcurrentU3E__2_2)); }
	inline Node_t4244254920 * get_U3CcurrentU3E__2_2() const { return ___U3CcurrentU3E__2_2; }
	inline Node_t4244254920 ** get_address_of_U3CcurrentU3E__2_2() { return &___U3CcurrentU3E__2_2; }
	inline void set_U3CcurrentU3E__2_2(Node_t4244254920 * value)
	{
		___U3CcurrentU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U24this_3)); }
	inline ConcurrentDictionary_2_t2171449296 * get_U24this_3() const { return ___U24this_3; }
	inline ConcurrentDictionary_2_t2171449296 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ConcurrentDictionary_2_t2171449296 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U24current_4)); }
	inline KeyValuePair_2_t2361573779  get_U24current_4() const { return ___U24current_4; }
	inline KeyValuePair_2_t2361573779 * get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(KeyValuePair_2_t2361573779  value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2362880871, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
