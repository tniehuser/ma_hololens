﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"

// System.Threading.ThreadPoolWorkQueue/QueueSegment
struct QueueSegment_t905751008;
// System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Threading.ThreadPoolWorkQueue/WorkStealingQueue>
struct SparseArray_1_t1143312502;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPoolWorkQueue
struct  ThreadPoolWorkQueue_t673814512  : public Il2CppObject
{
public:
	// System.Threading.ThreadPoolWorkQueue/QueueSegment modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue::queueHead
	QueueSegment_t905751008 * ___queueHead_0;
	// System.Threading.ThreadPoolWorkQueue/QueueSegment modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue::queueTail
	QueueSegment_t905751008 * ___queueTail_1;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.ThreadPoolWorkQueue::numOutstandingThreadRequests
	int32_t ___numOutstandingThreadRequests_3;

public:
	inline static int32_t get_offset_of_queueHead_0() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueue_t673814512, ___queueHead_0)); }
	inline QueueSegment_t905751008 * get_queueHead_0() const { return ___queueHead_0; }
	inline QueueSegment_t905751008 ** get_address_of_queueHead_0() { return &___queueHead_0; }
	inline void set_queueHead_0(QueueSegment_t905751008 * value)
	{
		___queueHead_0 = value;
		Il2CppCodeGenWriteBarrier(&___queueHead_0, value);
	}

	inline static int32_t get_offset_of_queueTail_1() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueue_t673814512, ___queueTail_1)); }
	inline QueueSegment_t905751008 * get_queueTail_1() const { return ___queueTail_1; }
	inline QueueSegment_t905751008 ** get_address_of_queueTail_1() { return &___queueTail_1; }
	inline void set_queueTail_1(QueueSegment_t905751008 * value)
	{
		___queueTail_1 = value;
		Il2CppCodeGenWriteBarrier(&___queueTail_1, value);
	}

	inline static int32_t get_offset_of_numOutstandingThreadRequests_3() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueue_t673814512, ___numOutstandingThreadRequests_3)); }
	inline int32_t get_numOutstandingThreadRequests_3() const { return ___numOutstandingThreadRequests_3; }
	inline int32_t* get_address_of_numOutstandingThreadRequests_3() { return &___numOutstandingThreadRequests_3; }
	inline void set_numOutstandingThreadRequests_3(int32_t value)
	{
		___numOutstandingThreadRequests_3 = value;
	}
};

struct ThreadPoolWorkQueue_t673814512_StaticFields
{
public:
	// System.Threading.ThreadPoolWorkQueue/SparseArray`1<System.Threading.ThreadPoolWorkQueue/WorkStealingQueue> System.Threading.ThreadPoolWorkQueue::allThreadQueues
	SparseArray_1_t1143312502 * ___allThreadQueues_2;

public:
	inline static int32_t get_offset_of_allThreadQueues_2() { return static_cast<int32_t>(offsetof(ThreadPoolWorkQueue_t673814512_StaticFields, ___allThreadQueues_2)); }
	inline SparseArray_1_t1143312502 * get_allThreadQueues_2() const { return ___allThreadQueues_2; }
	inline SparseArray_1_t1143312502 ** get_address_of_allThreadQueues_2() { return &___allThreadQueues_2; }
	inline void set_allThreadQueues_2(SparseArray_1_t1143312502 * value)
	{
		___allThreadQueues_2 = value;
		Il2CppCodeGenWriteBarrier(&___allThreadQueues_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
