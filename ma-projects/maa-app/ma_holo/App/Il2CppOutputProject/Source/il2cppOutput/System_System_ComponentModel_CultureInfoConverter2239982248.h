﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter745995970.h"

// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t191679357;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CultureInfoConverter
struct  CultureInfoConverter_t2239982248  : public TypeConverter_t745995970
{
public:
	// System.ComponentModel.TypeConverter/StandardValuesCollection System.ComponentModel.CultureInfoConverter::values
	StandardValuesCollection_t191679357 * ___values_4;

public:
	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(CultureInfoConverter_t2239982248, ___values_4)); }
	inline StandardValuesCollection_t191679357 * get_values_4() const { return ___values_4; }
	inline StandardValuesCollection_t191679357 ** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StandardValuesCollection_t191679357 * value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier(&___values_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
