﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinaryCompatibility
struct  BinaryCompatibility_t3641193253  : public Il2CppObject
{
public:

public:
};

struct BinaryCompatibility_t3641193253_StaticFields
{
public:
	// System.Boolean System.Xml.BinaryCompatibility::_targetsAtLeast_Desktop_V4_5_2
	bool ____targetsAtLeast_Desktop_V4_5_2_0;

public:
	inline static int32_t get_offset_of__targetsAtLeast_Desktop_V4_5_2_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t3641193253_StaticFields, ____targetsAtLeast_Desktop_V4_5_2_0)); }
	inline bool get__targetsAtLeast_Desktop_V4_5_2_0() const { return ____targetsAtLeast_Desktop_V4_5_2_0; }
	inline bool* get_address_of__targetsAtLeast_Desktop_V4_5_2_0() { return &____targetsAtLeast_Desktop_V4_5_2_0; }
	inline void set__targetsAtLeast_Desktop_V4_5_2_0(bool value)
	{
		____targetsAtLeast_Desktop_V4_5_2_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
