﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap
struct  BinaryCompatibilityMap_t3841228664  : public Il2CppObject
{
public:
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Phone_V7_1
	bool ___TargetsAtLeast_Phone_V7_1_0;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Phone_V8_0
	bool ___TargetsAtLeast_Phone_V8_0_1;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V4_5
	bool ___TargetsAtLeast_Desktop_V4_5_2;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V4_5_1
	bool ___TargetsAtLeast_Desktop_V4_5_1_3;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V4_5_2
	bool ___TargetsAtLeast_Desktop_V4_5_2_4;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V4_5_3
	bool ___TargetsAtLeast_Desktop_V4_5_3_5;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V4_5_4
	bool ___TargetsAtLeast_Desktop_V4_5_4_6;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Desktop_V5_0
	bool ___TargetsAtLeast_Desktop_V5_0_7;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Silverlight_V4
	bool ___TargetsAtLeast_Silverlight_V4_8;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Silverlight_V5
	bool ___TargetsAtLeast_Silverlight_V5_9;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility/BinaryCompatibilityMap::TargetsAtLeast_Silverlight_V6
	bool ___TargetsAtLeast_Silverlight_V6_10;

public:
	inline static int32_t get_offset_of_TargetsAtLeast_Phone_V7_1_0() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Phone_V7_1_0)); }
	inline bool get_TargetsAtLeast_Phone_V7_1_0() const { return ___TargetsAtLeast_Phone_V7_1_0; }
	inline bool* get_address_of_TargetsAtLeast_Phone_V7_1_0() { return &___TargetsAtLeast_Phone_V7_1_0; }
	inline void set_TargetsAtLeast_Phone_V7_1_0(bool value)
	{
		___TargetsAtLeast_Phone_V7_1_0 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Phone_V8_0_1() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Phone_V8_0_1)); }
	inline bool get_TargetsAtLeast_Phone_V8_0_1() const { return ___TargetsAtLeast_Phone_V8_0_1; }
	inline bool* get_address_of_TargetsAtLeast_Phone_V8_0_1() { return &___TargetsAtLeast_Phone_V8_0_1; }
	inline void set_TargetsAtLeast_Phone_V8_0_1(bool value)
	{
		___TargetsAtLeast_Phone_V8_0_1 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_2() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V4_5_2)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_2() const { return ___TargetsAtLeast_Desktop_V4_5_2; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_2() { return &___TargetsAtLeast_Desktop_V4_5_2; }
	inline void set_TargetsAtLeast_Desktop_V4_5_2(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_2 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_1_3() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V4_5_1_3)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_1_3() const { return ___TargetsAtLeast_Desktop_V4_5_1_3; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_1_3() { return &___TargetsAtLeast_Desktop_V4_5_1_3; }
	inline void set_TargetsAtLeast_Desktop_V4_5_1_3(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_1_3 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_2_4() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V4_5_2_4)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_2_4() const { return ___TargetsAtLeast_Desktop_V4_5_2_4; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_2_4() { return &___TargetsAtLeast_Desktop_V4_5_2_4; }
	inline void set_TargetsAtLeast_Desktop_V4_5_2_4(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_2_4 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_3_5() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V4_5_3_5)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_3_5() const { return ___TargetsAtLeast_Desktop_V4_5_3_5; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_3_5() { return &___TargetsAtLeast_Desktop_V4_5_3_5; }
	inline void set_TargetsAtLeast_Desktop_V4_5_3_5(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_3_5 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_4_6() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V4_5_4_6)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_4_6() const { return ___TargetsAtLeast_Desktop_V4_5_4_6; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_4_6() { return &___TargetsAtLeast_Desktop_V4_5_4_6; }
	inline void set_TargetsAtLeast_Desktop_V4_5_4_6(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_4_6 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V5_0_7() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Desktop_V5_0_7)); }
	inline bool get_TargetsAtLeast_Desktop_V5_0_7() const { return ___TargetsAtLeast_Desktop_V5_0_7; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V5_0_7() { return &___TargetsAtLeast_Desktop_V5_0_7; }
	inline void set_TargetsAtLeast_Desktop_V5_0_7(bool value)
	{
		___TargetsAtLeast_Desktop_V5_0_7 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Silverlight_V4_8() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Silverlight_V4_8)); }
	inline bool get_TargetsAtLeast_Silverlight_V4_8() const { return ___TargetsAtLeast_Silverlight_V4_8; }
	inline bool* get_address_of_TargetsAtLeast_Silverlight_V4_8() { return &___TargetsAtLeast_Silverlight_V4_8; }
	inline void set_TargetsAtLeast_Silverlight_V4_8(bool value)
	{
		___TargetsAtLeast_Silverlight_V4_8 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Silverlight_V5_9() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Silverlight_V5_9)); }
	inline bool get_TargetsAtLeast_Silverlight_V5_9() const { return ___TargetsAtLeast_Silverlight_V5_9; }
	inline bool* get_address_of_TargetsAtLeast_Silverlight_V5_9() { return &___TargetsAtLeast_Silverlight_V5_9; }
	inline void set_TargetsAtLeast_Silverlight_V5_9(bool value)
	{
		___TargetsAtLeast_Silverlight_V5_9 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Silverlight_V6_10() { return static_cast<int32_t>(offsetof(BinaryCompatibilityMap_t3841228664, ___TargetsAtLeast_Silverlight_V6_10)); }
	inline bool get_TargetsAtLeast_Silverlight_V6_10() const { return ___TargetsAtLeast_Silverlight_V6_10; }
	inline bool* get_address_of_TargetsAtLeast_Silverlight_V6_10() { return &___TargetsAtLeast_Silverlight_V6_10; }
	inline void set_TargetsAtLeast_Silverlight_V6_10(bool value)
	{
		___TargetsAtLeast_Silverlight_V6_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
