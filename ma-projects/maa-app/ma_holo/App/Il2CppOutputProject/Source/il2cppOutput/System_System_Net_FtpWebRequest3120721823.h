﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_FtpWebRequest_RequestState4256633122.h"

// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.IO.Stream
struct Stream_t3255436806;
// System.IO.StreamReader
struct StreamReader_t2360341767;
// System.Net.NetworkCredential
struct NetworkCredential_t1714133953;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Object
struct Il2CppObject;
// System.Net.FtpAsyncResult
struct FtpAsyncResult_t770082413;
// System.Net.FtpWebResponse
struct FtpWebResponse_t2609078769;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FtpWebRequest
struct  FtpWebRequest_t3120721823  : public WebRequest_t1365124353
{
public:
	// System.Uri System.Net.FtpWebRequest::requestUri
	Uri_t19570940 * ___requestUri_12;
	// System.String System.Net.FtpWebRequest::file_name
	String_t* ___file_name_13;
	// System.Net.ServicePoint System.Net.FtpWebRequest::servicePoint
	ServicePoint_t2765344313 * ___servicePoint_14;
	// System.IO.Stream System.Net.FtpWebRequest::origDataStream
	Stream_t3255436806 * ___origDataStream_15;
	// System.IO.Stream System.Net.FtpWebRequest::dataStream
	Stream_t3255436806 * ___dataStream_16;
	// System.IO.Stream System.Net.FtpWebRequest::controlStream
	Stream_t3255436806 * ___controlStream_17;
	// System.IO.StreamReader System.Net.FtpWebRequest::controlReader
	StreamReader_t2360341767 * ___controlReader_18;
	// System.Net.NetworkCredential System.Net.FtpWebRequest::credentials
	NetworkCredential_t1714133953 * ___credentials_19;
	// System.Net.IPHostEntry System.Net.FtpWebRequest::hostEntry
	IPHostEntry_t994738509 * ___hostEntry_20;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::localEndPoint
	IPEndPoint_t2615413766 * ___localEndPoint_21;
	// System.Net.IPEndPoint System.Net.FtpWebRequest::remoteEndPoint
	IPEndPoint_t2615413766 * ___remoteEndPoint_22;
	// System.Net.IWebProxy System.Net.FtpWebRequest::proxy
	Il2CppObject * ___proxy_23;
	// System.Int32 System.Net.FtpWebRequest::timeout
	int32_t ___timeout_24;
	// System.Int32 System.Net.FtpWebRequest::rwTimeout
	int32_t ___rwTimeout_25;
	// System.Int64 System.Net.FtpWebRequest::offset
	int64_t ___offset_26;
	// System.Boolean System.Net.FtpWebRequest::binary
	bool ___binary_27;
	// System.Boolean System.Net.FtpWebRequest::enableSsl
	bool ___enableSsl_28;
	// System.Boolean System.Net.FtpWebRequest::usePassive
	bool ___usePassive_29;
	// System.Boolean System.Net.FtpWebRequest::keepAlive
	bool ___keepAlive_30;
	// System.String System.Net.FtpWebRequest::method
	String_t* ___method_31;
	// System.String System.Net.FtpWebRequest::renameTo
	String_t* ___renameTo_32;
	// System.Object System.Net.FtpWebRequest::locker
	Il2CppObject * ___locker_33;
	// System.Net.FtpWebRequest/RequestState System.Net.FtpWebRequest::requestState
	int32_t ___requestState_34;
	// System.Net.FtpAsyncResult System.Net.FtpWebRequest::asyncResult
	FtpAsyncResult_t770082413 * ___asyncResult_35;
	// System.Net.FtpWebResponse System.Net.FtpWebRequest::ftpResponse
	FtpWebResponse_t2609078769 * ___ftpResponse_36;
	// System.IO.Stream System.Net.FtpWebRequest::requestStream
	Stream_t3255436806 * ___requestStream_37;
	// System.String System.Net.FtpWebRequest::initial_path
	String_t* ___initial_path_38;
	// System.Text.Encoding System.Net.FtpWebRequest::dataEncoding
	Encoding_t663144255 * ___dataEncoding_40;

public:
	inline static int32_t get_offset_of_requestUri_12() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___requestUri_12)); }
	inline Uri_t19570940 * get_requestUri_12() const { return ___requestUri_12; }
	inline Uri_t19570940 ** get_address_of_requestUri_12() { return &___requestUri_12; }
	inline void set_requestUri_12(Uri_t19570940 * value)
	{
		___requestUri_12 = value;
		Il2CppCodeGenWriteBarrier(&___requestUri_12, value);
	}

	inline static int32_t get_offset_of_file_name_13() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___file_name_13)); }
	inline String_t* get_file_name_13() const { return ___file_name_13; }
	inline String_t** get_address_of_file_name_13() { return &___file_name_13; }
	inline void set_file_name_13(String_t* value)
	{
		___file_name_13 = value;
		Il2CppCodeGenWriteBarrier(&___file_name_13, value);
	}

	inline static int32_t get_offset_of_servicePoint_14() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___servicePoint_14)); }
	inline ServicePoint_t2765344313 * get_servicePoint_14() const { return ___servicePoint_14; }
	inline ServicePoint_t2765344313 ** get_address_of_servicePoint_14() { return &___servicePoint_14; }
	inline void set_servicePoint_14(ServicePoint_t2765344313 * value)
	{
		___servicePoint_14 = value;
		Il2CppCodeGenWriteBarrier(&___servicePoint_14, value);
	}

	inline static int32_t get_offset_of_origDataStream_15() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___origDataStream_15)); }
	inline Stream_t3255436806 * get_origDataStream_15() const { return ___origDataStream_15; }
	inline Stream_t3255436806 ** get_address_of_origDataStream_15() { return &___origDataStream_15; }
	inline void set_origDataStream_15(Stream_t3255436806 * value)
	{
		___origDataStream_15 = value;
		Il2CppCodeGenWriteBarrier(&___origDataStream_15, value);
	}

	inline static int32_t get_offset_of_dataStream_16() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___dataStream_16)); }
	inline Stream_t3255436806 * get_dataStream_16() const { return ___dataStream_16; }
	inline Stream_t3255436806 ** get_address_of_dataStream_16() { return &___dataStream_16; }
	inline void set_dataStream_16(Stream_t3255436806 * value)
	{
		___dataStream_16 = value;
		Il2CppCodeGenWriteBarrier(&___dataStream_16, value);
	}

	inline static int32_t get_offset_of_controlStream_17() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___controlStream_17)); }
	inline Stream_t3255436806 * get_controlStream_17() const { return ___controlStream_17; }
	inline Stream_t3255436806 ** get_address_of_controlStream_17() { return &___controlStream_17; }
	inline void set_controlStream_17(Stream_t3255436806 * value)
	{
		___controlStream_17 = value;
		Il2CppCodeGenWriteBarrier(&___controlStream_17, value);
	}

	inline static int32_t get_offset_of_controlReader_18() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___controlReader_18)); }
	inline StreamReader_t2360341767 * get_controlReader_18() const { return ___controlReader_18; }
	inline StreamReader_t2360341767 ** get_address_of_controlReader_18() { return &___controlReader_18; }
	inline void set_controlReader_18(StreamReader_t2360341767 * value)
	{
		___controlReader_18 = value;
		Il2CppCodeGenWriteBarrier(&___controlReader_18, value);
	}

	inline static int32_t get_offset_of_credentials_19() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___credentials_19)); }
	inline NetworkCredential_t1714133953 * get_credentials_19() const { return ___credentials_19; }
	inline NetworkCredential_t1714133953 ** get_address_of_credentials_19() { return &___credentials_19; }
	inline void set_credentials_19(NetworkCredential_t1714133953 * value)
	{
		___credentials_19 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_19, value);
	}

	inline static int32_t get_offset_of_hostEntry_20() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___hostEntry_20)); }
	inline IPHostEntry_t994738509 * get_hostEntry_20() const { return ___hostEntry_20; }
	inline IPHostEntry_t994738509 ** get_address_of_hostEntry_20() { return &___hostEntry_20; }
	inline void set_hostEntry_20(IPHostEntry_t994738509 * value)
	{
		___hostEntry_20 = value;
		Il2CppCodeGenWriteBarrier(&___hostEntry_20, value);
	}

	inline static int32_t get_offset_of_localEndPoint_21() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___localEndPoint_21)); }
	inline IPEndPoint_t2615413766 * get_localEndPoint_21() const { return ___localEndPoint_21; }
	inline IPEndPoint_t2615413766 ** get_address_of_localEndPoint_21() { return &___localEndPoint_21; }
	inline void set_localEndPoint_21(IPEndPoint_t2615413766 * value)
	{
		___localEndPoint_21 = value;
		Il2CppCodeGenWriteBarrier(&___localEndPoint_21, value);
	}

	inline static int32_t get_offset_of_remoteEndPoint_22() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___remoteEndPoint_22)); }
	inline IPEndPoint_t2615413766 * get_remoteEndPoint_22() const { return ___remoteEndPoint_22; }
	inline IPEndPoint_t2615413766 ** get_address_of_remoteEndPoint_22() { return &___remoteEndPoint_22; }
	inline void set_remoteEndPoint_22(IPEndPoint_t2615413766 * value)
	{
		___remoteEndPoint_22 = value;
		Il2CppCodeGenWriteBarrier(&___remoteEndPoint_22, value);
	}

	inline static int32_t get_offset_of_proxy_23() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___proxy_23)); }
	inline Il2CppObject * get_proxy_23() const { return ___proxy_23; }
	inline Il2CppObject ** get_address_of_proxy_23() { return &___proxy_23; }
	inline void set_proxy_23(Il2CppObject * value)
	{
		___proxy_23 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_23, value);
	}

	inline static int32_t get_offset_of_timeout_24() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___timeout_24)); }
	inline int32_t get_timeout_24() const { return ___timeout_24; }
	inline int32_t* get_address_of_timeout_24() { return &___timeout_24; }
	inline void set_timeout_24(int32_t value)
	{
		___timeout_24 = value;
	}

	inline static int32_t get_offset_of_rwTimeout_25() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___rwTimeout_25)); }
	inline int32_t get_rwTimeout_25() const { return ___rwTimeout_25; }
	inline int32_t* get_address_of_rwTimeout_25() { return &___rwTimeout_25; }
	inline void set_rwTimeout_25(int32_t value)
	{
		___rwTimeout_25 = value;
	}

	inline static int32_t get_offset_of_offset_26() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___offset_26)); }
	inline int64_t get_offset_26() const { return ___offset_26; }
	inline int64_t* get_address_of_offset_26() { return &___offset_26; }
	inline void set_offset_26(int64_t value)
	{
		___offset_26 = value;
	}

	inline static int32_t get_offset_of_binary_27() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___binary_27)); }
	inline bool get_binary_27() const { return ___binary_27; }
	inline bool* get_address_of_binary_27() { return &___binary_27; }
	inline void set_binary_27(bool value)
	{
		___binary_27 = value;
	}

	inline static int32_t get_offset_of_enableSsl_28() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___enableSsl_28)); }
	inline bool get_enableSsl_28() const { return ___enableSsl_28; }
	inline bool* get_address_of_enableSsl_28() { return &___enableSsl_28; }
	inline void set_enableSsl_28(bool value)
	{
		___enableSsl_28 = value;
	}

	inline static int32_t get_offset_of_usePassive_29() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___usePassive_29)); }
	inline bool get_usePassive_29() const { return ___usePassive_29; }
	inline bool* get_address_of_usePassive_29() { return &___usePassive_29; }
	inline void set_usePassive_29(bool value)
	{
		___usePassive_29 = value;
	}

	inline static int32_t get_offset_of_keepAlive_30() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___keepAlive_30)); }
	inline bool get_keepAlive_30() const { return ___keepAlive_30; }
	inline bool* get_address_of_keepAlive_30() { return &___keepAlive_30; }
	inline void set_keepAlive_30(bool value)
	{
		___keepAlive_30 = value;
	}

	inline static int32_t get_offset_of_method_31() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___method_31)); }
	inline String_t* get_method_31() const { return ___method_31; }
	inline String_t** get_address_of_method_31() { return &___method_31; }
	inline void set_method_31(String_t* value)
	{
		___method_31 = value;
		Il2CppCodeGenWriteBarrier(&___method_31, value);
	}

	inline static int32_t get_offset_of_renameTo_32() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___renameTo_32)); }
	inline String_t* get_renameTo_32() const { return ___renameTo_32; }
	inline String_t** get_address_of_renameTo_32() { return &___renameTo_32; }
	inline void set_renameTo_32(String_t* value)
	{
		___renameTo_32 = value;
		Il2CppCodeGenWriteBarrier(&___renameTo_32, value);
	}

	inline static int32_t get_offset_of_locker_33() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___locker_33)); }
	inline Il2CppObject * get_locker_33() const { return ___locker_33; }
	inline Il2CppObject ** get_address_of_locker_33() { return &___locker_33; }
	inline void set_locker_33(Il2CppObject * value)
	{
		___locker_33 = value;
		Il2CppCodeGenWriteBarrier(&___locker_33, value);
	}

	inline static int32_t get_offset_of_requestState_34() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___requestState_34)); }
	inline int32_t get_requestState_34() const { return ___requestState_34; }
	inline int32_t* get_address_of_requestState_34() { return &___requestState_34; }
	inline void set_requestState_34(int32_t value)
	{
		___requestState_34 = value;
	}

	inline static int32_t get_offset_of_asyncResult_35() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___asyncResult_35)); }
	inline FtpAsyncResult_t770082413 * get_asyncResult_35() const { return ___asyncResult_35; }
	inline FtpAsyncResult_t770082413 ** get_address_of_asyncResult_35() { return &___asyncResult_35; }
	inline void set_asyncResult_35(FtpAsyncResult_t770082413 * value)
	{
		___asyncResult_35 = value;
		Il2CppCodeGenWriteBarrier(&___asyncResult_35, value);
	}

	inline static int32_t get_offset_of_ftpResponse_36() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___ftpResponse_36)); }
	inline FtpWebResponse_t2609078769 * get_ftpResponse_36() const { return ___ftpResponse_36; }
	inline FtpWebResponse_t2609078769 ** get_address_of_ftpResponse_36() { return &___ftpResponse_36; }
	inline void set_ftpResponse_36(FtpWebResponse_t2609078769 * value)
	{
		___ftpResponse_36 = value;
		Il2CppCodeGenWriteBarrier(&___ftpResponse_36, value);
	}

	inline static int32_t get_offset_of_requestStream_37() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___requestStream_37)); }
	inline Stream_t3255436806 * get_requestStream_37() const { return ___requestStream_37; }
	inline Stream_t3255436806 ** get_address_of_requestStream_37() { return &___requestStream_37; }
	inline void set_requestStream_37(Stream_t3255436806 * value)
	{
		___requestStream_37 = value;
		Il2CppCodeGenWriteBarrier(&___requestStream_37, value);
	}

	inline static int32_t get_offset_of_initial_path_38() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___initial_path_38)); }
	inline String_t* get_initial_path_38() const { return ___initial_path_38; }
	inline String_t** get_address_of_initial_path_38() { return &___initial_path_38; }
	inline void set_initial_path_38(String_t* value)
	{
		___initial_path_38 = value;
		Il2CppCodeGenWriteBarrier(&___initial_path_38, value);
	}

	inline static int32_t get_offset_of_dataEncoding_40() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823, ___dataEncoding_40)); }
	inline Encoding_t663144255 * get_dataEncoding_40() const { return ___dataEncoding_40; }
	inline Encoding_t663144255 ** get_address_of_dataEncoding_40() { return &___dataEncoding_40; }
	inline void set_dataEncoding_40(Encoding_t663144255 * value)
	{
		___dataEncoding_40 = value;
		Il2CppCodeGenWriteBarrier(&___dataEncoding_40, value);
	}
};

struct FtpWebRequest_t3120721823_StaticFields
{
public:
	// System.String[] System.Net.FtpWebRequest::supportedCommands
	StringU5BU5D_t1642385972* ___supportedCommands_39;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Net.FtpWebRequest::<>f__switch$map2
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2_41;

public:
	inline static int32_t get_offset_of_supportedCommands_39() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823_StaticFields, ___supportedCommands_39)); }
	inline StringU5BU5D_t1642385972* get_supportedCommands_39() const { return ___supportedCommands_39; }
	inline StringU5BU5D_t1642385972** get_address_of_supportedCommands_39() { return &___supportedCommands_39; }
	inline void set_supportedCommands_39(StringU5BU5D_t1642385972* value)
	{
		___supportedCommands_39 = value;
		Il2CppCodeGenWriteBarrier(&___supportedCommands_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_41() { return static_cast<int32_t>(offsetof(FtpWebRequest_t3120721823_StaticFields, ___U3CU3Ef__switchU24map2_41)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2_41() const { return ___U3CU3Ef__switchU24map2_41; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2_41() { return &___U3CU3Ef__switchU24map2_41; }
	inline void set_U3CU3Ef__switchU24map2_41(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_41, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
