﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSp1900124058.h"
#include "mscorlib_System_Globalization_TimeSpanParse_TimeSpa782244817.h"
#include "mscorlib_System_Globalization_TimeSpanParse_String2680633667.h"
#include "mscorlib_System_Globalization_UnicodeCategory682236799.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Guid_GuidStyles604550366.h"
#include "mscorlib_System_Guid_GuidParseThrowStyle3041578670.h"
#include "mscorlib_System_Guid_ParseFailureKind3905197472.h"
#include "mscorlib_System_Guid_GuidResult2567604379.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_InvalidTimeZoneException1221755569.h"
#include "mscorlib_System_IO___Error2214032394.h"
#include "mscorlib_System_IO_BinaryReader2491843768.h"
#include "mscorlib_System_IO_BinaryWriter3179371318.h"
#include "mscorlib_System_IO_DirectoryNotFoundException373523477.h"
#include "mscorlib_System_IO_DriveNotFoundException4230319522.h"
#include "mscorlib_System_IO_EndOfStreamException1711658693.h"
#include "mscorlib_System_IO_FileLoadException3198361301.h"
#include "mscorlib_System_IO_FileNotFoundException4200667904.h"
#include "mscorlib_System_IO_FileSystemInfo2360991899.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_IO_PathTooLongException2469314706.h"
#include "mscorlib_System_IO_PinnedBufferMemoryStream2421662467.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_Stream_ReadWriteTask2745753060.h"
#include "mscorlib_System_IO_Stream_NullStream2069088193.h"
#include "mscorlib_System_IO_Stream_SynchronousAsyncResult2050900148.h"
#include "mscorlib_System_IO_StreamReader2360341767.h"
#include "mscorlib_System_IO_StreamReader_NullStreamReader1178646293.h"
#include "mscorlib_System_IO_StreamWriter3858580635.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "mscorlib_System_IO_StringWriter4139609088.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_IO_TextReader_NullTextReader516142577.h"
#include "mscorlib_System_IO_TextReader_SyncTextReader993141785.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_IO_TextWriter_NullTextWriter1732518121.h"
#include "mscorlib_System_IO_TextWriter_SyncTextWriter3616208401.h"
#include "mscorlib_System_IO_UnmanagedMemoryStream822875729.h"
#include "mscorlib_System_Math2022911894.h"
#include "mscorlib_System_MemberAccessException2005094827.h"
#include "mscorlib_System_MethodAccessException4093255254.h"
#include "mscorlib_System_MissingFieldException3702079619.h"
#include "mscorlib_System_MissingMemberException1839900847.h"
#include "mscorlib_System_MissingMethodException3441205986.h"
#include "mscorlib_System_MulticastNotSupportedException1815247018.h"
#include "mscorlib_System_NonSerializedAttribute399263003.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_Number2840369917.h"
#include "mscorlib_System_Number_NumberBuffer3547128574.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_ObsoleteAttribute3878847927.h"
#include "mscorlib_System_OperationCanceledException2897400967.h"
#include "mscorlib_System_OutOfMemoryException1181064283.h"
#include "mscorlib_System_OverflowException1075868493.h"
#include "mscorlib_System_ParamArrayAttribute2144993728.h"
#include "mscorlib_System_ParamsArray2726825425.h"
#include "mscorlib_System_PlatformNotSupportedException3778770305.h"
#include "mscorlib_System_Random1044426839.h"
#include "mscorlib_System_RankException1539875949.h"
#include "mscorlib_System_Reflection_AmbiguousMatchException1406414556.h"
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribu177123295.h"
#include "mscorlib_System_Reflection_AssemblyTrademarkAttrib3740556705.h"
#include "mscorlib_System_Reflection_AssemblyProductAttribut1523443169.h"
#include "mscorlib_System_Reflection_AssemblyCompanyAttribut2851673381.h"
#include "mscorlib_System_Reflection_AssemblyDescriptionAttr1018387888.h"
#include "mscorlib_System_Reflection_AssemblyTitleAttribute92945912.h"
#include "mscorlib_System_Reflection_AssemblyConfigurationAt1678917172.h"
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAtt1774139159.h"
#include "mscorlib_System_Reflection_AssemblyInformationalVe3037389657.h"
#include "mscorlib_System_Reflection_AssemblyFileVersionAttr2897687916.h"
#include "mscorlib_System_Reflection_AssemblyVersionAttribut2009500602.h"
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute605245443.h"
#include "mscorlib_System_Reflection_AssemblyDelaySignAttrib2705758496.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags1794031440.h"
#include "mscorlib_System_Reflection_AssemblyContentType3870195675.h"
#include "mscorlib_System_Reflection_ProcessorArchitecture1620065459.h"
#include "mscorlib_System_Reflection_Binder3404612058.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize300 = { sizeof (TimeSpanRawInfo_t1900124058)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable300[12] = 
{
	TimeSpanRawInfo_t1900124058::get_offset_of_lastSeenTTT_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_tokenCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_SepCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_NumCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_literals_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_numbers_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_posLoc_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_negLoc_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_posLocInit_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_negLocInit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_fullPosPattern_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanRawInfo_t1900124058::get_offset_of_m_fullNegPattern_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize301 = { sizeof (TimeSpanResult_t782244817)+ sizeof (Il2CppObject), sizeof(TimeSpanResult_t782244817_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable301[6] = 
{
	TimeSpanResult_t782244817::get_offset_of_parsedTimeSpan_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanResult_t782244817::get_offset_of_throwStyle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanResult_t782244817::get_offset_of_m_failure_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanResult_t782244817::get_offset_of_m_failureMessageID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanResult_t782244817::get_offset_of_m_failureMessageFormatArgument_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpanResult_t782244817::get_offset_of_m_failureArgumentName_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize302 = { sizeof (StringParser_t2680633667)+ sizeof (Il2CppObject), sizeof(StringParser_t2680633667_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable302[4] = 
{
	StringParser_t2680633667::get_offset_of_str_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringParser_t2680633667::get_offset_of_ch_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringParser_t2680633667::get_offset_of_pos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringParser_t2680633667::get_offset_of_len_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize303 = { sizeof (UnicodeCategory_t682236799)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable303[31] = 
{
	UnicodeCategory_t682236799::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize304 = { sizeof (Guid_t)+ sizeof (Il2CppObject), sizeof(Guid_t ), sizeof(Guid_t_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable304[15] = 
{
	Guid_t_StaticFields::get_offset_of_Empty_0(),
	Guid_t::get_offset_of__a_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__c_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__d_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__e_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__f_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__g_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__h_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__i_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__j_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t::get_offset_of__k_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Guid_t_StaticFields::get_offset_of__rngAccess_12(),
	Guid_t_StaticFields::get_offset_of__rng_13(),
	Guid_t_StaticFields::get_offset_of__fastRng_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize305 = { sizeof (GuidStyles_t604550366)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable305[16] = 
{
	GuidStyles_t604550366::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize306 = { sizeof (GuidParseThrowStyle_t3041578670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable306[4] = 
{
	GuidParseThrowStyle_t3041578670::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize307 = { sizeof (ParseFailureKind_t3905197472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable307[7] = 
{
	ParseFailureKind_t3905197472::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize308 = { sizeof (GuidResult_t2567604379)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable308[7] = 
{
	GuidResult_t2567604379::get_offset_of_parsedGuid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_throwStyle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_m_failure_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_m_failureMessageID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_m_failureMessageFormatArgument_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_m_failureArgumentName_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GuidResult_t2567604379::get_offset_of_m_innerException_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize314 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize315 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize316 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize318 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize319 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize320 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize321 = { sizeof (IndexOutOfRangeException_t3527622107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize322 = { sizeof (Int16_t4041245914)+ sizeof (Il2CppObject), sizeof(int16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable322[1] = 
{
	Int16_t4041245914::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize323 = { sizeof (Int32_t2071877448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable323[3] = 
{
	Int32_t2071877448::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize324 = { sizeof (Int64_t909078037)+ sizeof (Il2CppObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable324[1] = 
{
	Int64_t909078037::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize325 = { sizeof (InvalidCastException_t3625212209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize326 = { sizeof (InvalidOperationException_t721527559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize327 = { sizeof (InvalidTimeZoneException_t1221755569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize328 = { sizeof (__Error_t2214032394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize329 = { sizeof (BinaryReader_t2491843768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable329[10] = 
{
	BinaryReader_t2491843768::get_offset_of_m_stream_0(),
	BinaryReader_t2491843768::get_offset_of_m_buffer_1(),
	BinaryReader_t2491843768::get_offset_of_m_decoder_2(),
	BinaryReader_t2491843768::get_offset_of_m_charBytes_3(),
	BinaryReader_t2491843768::get_offset_of_m_singleChar_4(),
	BinaryReader_t2491843768::get_offset_of_m_charBuffer_5(),
	BinaryReader_t2491843768::get_offset_of_m_maxCharsSize_6(),
	BinaryReader_t2491843768::get_offset_of_m_2BytesPerChar_7(),
	BinaryReader_t2491843768::get_offset_of_m_isMemoryStream_8(),
	BinaryReader_t2491843768::get_offset_of_m_leaveOpen_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize330 = { sizeof (BinaryWriter_t3179371318), -1, sizeof(BinaryWriter_t3179371318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable330[8] = 
{
	BinaryWriter_t3179371318_StaticFields::get_offset_of_Null_0(),
	BinaryWriter_t3179371318::get_offset_of_OutStream_1(),
	BinaryWriter_t3179371318::get_offset_of__buffer_2(),
	BinaryWriter_t3179371318::get_offset_of__encoding_3(),
	BinaryWriter_t3179371318::get_offset_of__encoder_4(),
	BinaryWriter_t3179371318::get_offset_of__leaveOpen_5(),
	BinaryWriter_t3179371318::get_offset_of__largeByteBuffer_6(),
	BinaryWriter_t3179371318::get_offset_of__maxChars_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize331 = { sizeof (DirectoryNotFoundException_t373523477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize332 = { sizeof (DriveNotFoundException_t4230319522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize333 = { sizeof (EndOfStreamException_t1711658693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize334 = { sizeof (FileLoadException_t3198361301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable334[2] = 
{
	FileLoadException_t3198361301::get_offset_of__fileName_17(),
	FileLoadException_t3198361301::get_offset_of__fusionLog_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize335 = { sizeof (FileNotFoundException_t4200667904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable335[2] = 
{
	FileNotFoundException_t4200667904::get_offset_of__fileName_17(),
	FileNotFoundException_t4200667904::get_offset_of__fusionLog_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize336 = { sizeof (FileSystemInfo_t2360991899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable336[5] = 
{
	FileSystemInfo_t2360991899::get_offset_of__data_1(),
	FileSystemInfo_t2360991899::get_offset_of__dataInitialised_2(),
	FileSystemInfo_t2360991899::get_offset_of_FullPath_3(),
	FileSystemInfo_t2360991899::get_offset_of_OriginalPath_4(),
	FileSystemInfo_t2360991899::get_offset_of__displayPath_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize337 = { sizeof (IOException_t2458421087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable337[1] = 
{
	IOException_t2458421087::get_offset_of__maybeFullPath_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize338 = { sizeof (MemoryStream_t743994179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable338[10] = 
{
	MemoryStream_t743994179::get_offset_of__buffer_8(),
	MemoryStream_t743994179::get_offset_of__origin_9(),
	MemoryStream_t743994179::get_offset_of__position_10(),
	MemoryStream_t743994179::get_offset_of__length_11(),
	MemoryStream_t743994179::get_offset_of__capacity_12(),
	MemoryStream_t743994179::get_offset_of__expandable_13(),
	MemoryStream_t743994179::get_offset_of__writable_14(),
	MemoryStream_t743994179::get_offset_of__exposable_15(),
	MemoryStream_t743994179::get_offset_of__isOpen_16(),
	MemoryStream_t743994179::get_offset_of__lastReadTask_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize339 = { sizeof (PathTooLongException_t2469314706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize340 = { sizeof (PinnedBufferMemoryStream_t2421662467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable340[2] = 
{
	PinnedBufferMemoryStream_t2421662467::get_offset_of__array_16(),
	PinnedBufferMemoryStream_t2421662467::get_offset_of__pinningHandle_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize341 = { sizeof (Stream_t3255436806), -1, sizeof(Stream_t3255436806_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable341[7] = 
{
	Stream_t3255436806_StaticFields::get_offset_of_Null_1(),
	Stream_t3255436806::get_offset_of__activeReadWriteTask_2(),
	Stream_t3255436806::get_offset_of__asyncActiveSemaphore_3(),
	Stream_t3255436806_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	Stream_t3255436806_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
	Stream_t3255436806_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_6(),
	Stream_t3255436806_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize342 = { sizeof (ReadWriteTask_t2745753060), -1, sizeof(ReadWriteTask_t2745753060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable342[9] = 
{
	ReadWriteTask_t2745753060::get_offset_of__isRead_27(),
	ReadWriteTask_t2745753060::get_offset_of__stream_28(),
	ReadWriteTask_t2745753060::get_offset_of__buffer_29(),
	ReadWriteTask_t2745753060::get_offset_of__offset_30(),
	ReadWriteTask_t2745753060::get_offset_of__count_31(),
	ReadWriteTask_t2745753060::get_offset_of__callback_32(),
	ReadWriteTask_t2745753060::get_offset_of__context_33(),
	ReadWriteTask_t2745753060_StaticFields::get_offset_of_s_invokeAsyncCallback_34(),
	ReadWriteTask_t2745753060_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize343 = { sizeof (NullStream_t2069088193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize344 = { sizeof (SynchronousAsyncResult_t2050900148), -1, sizeof(SynchronousAsyncResult_t2050900148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable344[7] = 
{
	SynchronousAsyncResult_t2050900148::get_offset_of__stateObject_0(),
	SynchronousAsyncResult_t2050900148::get_offset_of__isWrite_1(),
	SynchronousAsyncResult_t2050900148::get_offset_of__waitHandle_2(),
	SynchronousAsyncResult_t2050900148::get_offset_of__exceptionInfo_3(),
	SynchronousAsyncResult_t2050900148::get_offset_of__endXxxCalled_4(),
	SynchronousAsyncResult_t2050900148::get_offset_of__bytesRead_5(),
	SynchronousAsyncResult_t2050900148_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize345 = { sizeof (StreamReader_t2360341767), -1, sizeof(StreamReader_t2360341767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable345[17] = 
{
	StreamReader_t2360341767_StaticFields::get_offset_of_Null_4(),
	StreamReader_t2360341767::get_offset_of_stream_5(),
	StreamReader_t2360341767::get_offset_of_encoding_6(),
	StreamReader_t2360341767::get_offset_of_decoder_7(),
	StreamReader_t2360341767::get_offset_of_byteBuffer_8(),
	StreamReader_t2360341767::get_offset_of_charBuffer_9(),
	StreamReader_t2360341767::get_offset_of__preamble_10(),
	StreamReader_t2360341767::get_offset_of_charPos_11(),
	StreamReader_t2360341767::get_offset_of_charLen_12(),
	StreamReader_t2360341767::get_offset_of_byteLen_13(),
	StreamReader_t2360341767::get_offset_of_bytePos_14(),
	StreamReader_t2360341767::get_offset_of__maxCharsPerBuffer_15(),
	StreamReader_t2360341767::get_offset_of__detectEncoding_16(),
	StreamReader_t2360341767::get_offset_of__checkPreamble_17(),
	StreamReader_t2360341767::get_offset_of__isBlocked_18(),
	StreamReader_t2360341767::get_offset_of__closable_19(),
	StreamReader_t2360341767::get_offset_of__asyncReadTask_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize346 = { sizeof (NullStreamReader_t1178646293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize347 = { sizeof (StreamWriter_t3858580635), -1, sizeof(StreamWriter_t3858580635_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable347[13] = 
{
	StreamWriter_t3858580635_StaticFields::get_offset_of_Null_11(),
	StreamWriter_t3858580635::get_offset_of_stream_12(),
	StreamWriter_t3858580635::get_offset_of_encoding_13(),
	StreamWriter_t3858580635::get_offset_of_encoder_14(),
	StreamWriter_t3858580635::get_offset_of_byteBuffer_15(),
	StreamWriter_t3858580635::get_offset_of_charBuffer_16(),
	StreamWriter_t3858580635::get_offset_of_charPos_17(),
	StreamWriter_t3858580635::get_offset_of_charLen_18(),
	StreamWriter_t3858580635::get_offset_of_autoFlush_19(),
	StreamWriter_t3858580635::get_offset_of_haveWrittenPreamble_20(),
	StreamWriter_t3858580635::get_offset_of_closable_21(),
	StreamWriter_t3858580635::get_offset_of__asyncWriteTask_22(),
	StreamWriter_t3858580635_StaticFields::get_offset_of__UTF8NoBOM_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize348 = { sizeof (StringReader_t1480123486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable348[3] = 
{
	StringReader_t1480123486::get_offset_of__s_4(),
	StringReader_t1480123486::get_offset_of__pos_5(),
	StringReader_t1480123486::get_offset_of__length_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize349 = { sizeof (StringWriter_t4139609088), -1, sizeof(StringWriter_t4139609088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable349[3] = 
{
	StringWriter_t4139609088_StaticFields::get_offset_of_m_encoding_11(),
	StringWriter_t4139609088::get_offset_of__sb_12(),
	StringWriter_t4139609088::get_offset_of__isOpen_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize350 = { sizeof (TextReader_t1561828458), -1, sizeof(TextReader_t1561828458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable350[3] = 
{
	TextReader_t1561828458_StaticFields::get_offset_of__ReadLineDelegate_1(),
	TextReader_t1561828458_StaticFields::get_offset_of__ReadDelegate_2(),
	TextReader_t1561828458_StaticFields::get_offset_of_Null_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize351 = { sizeof (NullTextReader_t516142577), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize352 = { sizeof (SyncTextReader_t993141785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable352[1] = 
{
	SyncTextReader_t993141785::get_offset_of__in_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize353 = { sizeof (TextWriter_t4027217640), -1, sizeof(TextWriter_t4027217640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable353[10] = 
{
	TextWriter_t4027217640_StaticFields::get_offset_of_Null_1(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteCharDelegate_2(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteStringDelegate_3(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteCharArrayRangeDelegate_4(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteLineCharDelegate_5(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteLineStringDelegate_6(),
	TextWriter_t4027217640_StaticFields::get_offset_of__WriteLineCharArrayRangeDelegate_7(),
	TextWriter_t4027217640_StaticFields::get_offset_of__FlushDelegate_8(),
	TextWriter_t4027217640::get_offset_of_CoreNewLine_9(),
	TextWriter_t4027217640::get_offset_of_InternalFormatProvider_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize354 = { sizeof (NullTextWriter_t1732518121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize355 = { sizeof (SyncTextWriter_t3616208401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable355[1] = 
{
	SyncTextWriter_t3616208401::get_offset_of__out_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize356 = { sizeof (UnmanagedMemoryStream_t822875729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable356[8] = 
{
	UnmanagedMemoryStream_t822875729::get_offset_of__buffer_8(),
	UnmanagedMemoryStream_t822875729::get_offset_of__mem_9(),
	UnmanagedMemoryStream_t822875729::get_offset_of__length_10(),
	UnmanagedMemoryStream_t822875729::get_offset_of__capacity_11(),
	UnmanagedMemoryStream_t822875729::get_offset_of__position_12(),
	UnmanagedMemoryStream_t822875729::get_offset_of__offset_13(),
	UnmanagedMemoryStream_t822875729::get_offset_of__access_14(),
	UnmanagedMemoryStream_t822875729::get_offset_of__isOpen_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize357 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize358 = { sizeof (Math_t2022911894), -1, sizeof(Math_t2022911894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable358[2] = 
{
	Math_t2022911894_StaticFields::get_offset_of_doubleRoundLimit_0(),
	Math_t2022911894_StaticFields::get_offset_of_roundPower10Double_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize359 = { sizeof (MemberAccessException_t2005094827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize360 = { sizeof (MethodAccessException_t4093255254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize361 = { sizeof (MissingFieldException_t3702079619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize362 = { sizeof (MissingMemberException_t1839900847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable362[3] = 
{
	MissingMemberException_t1839900847::get_offset_of_ClassName_16(),
	MissingMemberException_t1839900847::get_offset_of_MemberName_17(),
	MissingMemberException_t1839900847::get_offset_of_Signature_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize363 = { sizeof (MissingMethodException_t3441205986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize364 = { sizeof (MulticastNotSupportedException_t1815247018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize365 = { sizeof (NonSerializedAttribute_t399263003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize366 = { sizeof (NotImplementedException_t2785117854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize367 = { sizeof (NotSupportedException_t1793819818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize368 = { sizeof (NullReferenceException_t3156209119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize369 = { sizeof (Number_t2840369917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize370 = { sizeof (NumberBuffer_t3547128574)+ sizeof (Il2CppObject), sizeof(NumberBuffer_t3547128574_marshaled_pinvoke), sizeof(NumberBuffer_t3547128574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable370[6] = 
{
	NumberBuffer_t3547128574_StaticFields::get_offset_of_NumberBufferBytes_0(),
	NumberBuffer_t3547128574::get_offset_of_baseAddress_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NumberBuffer_t3547128574::get_offset_of_digits_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NumberBuffer_t3547128574::get_offset_of_precision_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NumberBuffer_t3547128574::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NumberBuffer_t3547128574::get_offset_of_sign_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize371 = { sizeof (ObjectDisposedException_t2695136451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable371[1] = 
{
	ObjectDisposedException_t2695136451::get_offset_of_objectName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize372 = { sizeof (ObsoleteAttribute_t3878847927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable372[2] = 
{
	ObsoleteAttribute_t3878847927::get_offset_of__message_0(),
	ObsoleteAttribute_t3878847927::get_offset_of__error_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize373 = { sizeof (OperationCanceledException_t2897400967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable373[1] = 
{
	OperationCanceledException_t2897400967::get_offset_of__cancellationToken_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize374 = { sizeof (OutOfMemoryException_t1181064283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize375 = { sizeof (OverflowException_t1075868493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize376 = { sizeof (ParamArrayAttribute_t2144993728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize377 = { sizeof (ParamsArray_t2726825425)+ sizeof (Il2CppObject), -1, sizeof(ParamsArray_t2726825425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable377[7] = 
{
	ParamsArray_t2726825425_StaticFields::get_offset_of_oneArgArray_0(),
	ParamsArray_t2726825425_StaticFields::get_offset_of_twoArgArray_1(),
	ParamsArray_t2726825425_StaticFields::get_offset_of_threeArgArray_2(),
	ParamsArray_t2726825425::get_offset_of_arg0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamsArray_t2726825425::get_offset_of_arg1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamsArray_t2726825425::get_offset_of_arg2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamsArray_t2726825425::get_offset_of_args_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize378 = { sizeof (PlatformNotSupportedException_t3778770305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize379 = { sizeof (Random_t1044426839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable379[3] = 
{
	Random_t1044426839::get_offset_of_inext_0(),
	Random_t1044426839::get_offset_of_inextp_1(),
	Random_t1044426839::get_offset_of_SeedArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize380 = { sizeof (RankException_t1539875949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize381 = { sizeof (AmbiguousMatchException_t1406414556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize382 = { sizeof (AssemblyCopyrightAttribute_t177123295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable382[1] = 
{
	AssemblyCopyrightAttribute_t177123295::get_offset_of_m_copyright_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize383 = { sizeof (AssemblyTrademarkAttribute_t3740556705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable383[1] = 
{
	AssemblyTrademarkAttribute_t3740556705::get_offset_of_m_trademark_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize384 = { sizeof (AssemblyProductAttribute_t1523443169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable384[1] = 
{
	AssemblyProductAttribute_t1523443169::get_offset_of_m_product_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize385 = { sizeof (AssemblyCompanyAttribute_t2851673381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable385[1] = 
{
	AssemblyCompanyAttribute_t2851673381::get_offset_of_m_company_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize386 = { sizeof (AssemblyDescriptionAttribute_t1018387888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable386[1] = 
{
	AssemblyDescriptionAttribute_t1018387888::get_offset_of_m_description_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize387 = { sizeof (AssemblyTitleAttribute_t92945912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable387[1] = 
{
	AssemblyTitleAttribute_t92945912::get_offset_of_m_title_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize388 = { sizeof (AssemblyConfigurationAttribute_t1678917172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable388[1] = 
{
	AssemblyConfigurationAttribute_t1678917172::get_offset_of_m_configuration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize389 = { sizeof (AssemblyDefaultAliasAttribute_t1774139159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable389[1] = 
{
	AssemblyDefaultAliasAttribute_t1774139159::get_offset_of_m_defaultAlias_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize390 = { sizeof (AssemblyInformationalVersionAttribute_t3037389657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable390[1] = 
{
	AssemblyInformationalVersionAttribute_t3037389657::get_offset_of_m_informationalVersion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize391 = { sizeof (AssemblyFileVersionAttribute_t2897687916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable391[1] = 
{
	AssemblyFileVersionAttribute_t2897687916::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize392 = { sizeof (AssemblyVersionAttribute_t2009500602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable392[1] = 
{
	AssemblyVersionAttribute_t2009500602::get_offset_of_m_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize393 = { sizeof (AssemblyKeyFileAttribute_t605245443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable393[1] = 
{
	AssemblyKeyFileAttribute_t605245443::get_offset_of_m_keyFile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize394 = { sizeof (AssemblyDelaySignAttribute_t2705758496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable394[1] = 
{
	AssemblyDelaySignAttribute_t2705758496::get_offset_of_m_delaySign_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize395 = { sizeof (AssemblyNameFlags_t1794031440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable395[6] = 
{
	AssemblyNameFlags_t1794031440::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize396 = { sizeof (AssemblyContentType_t3870195675)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable396[3] = 
{
	AssemblyContentType_t3870195675::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize397 = { sizeof (ProcessorArchitecture_t1620065459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable397[7] = 
{
	ProcessorArchitecture_t1620065459::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize398 = { sizeof (Binder_t3404612058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize399 = { sizeof (BindingFlags_t1082350898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable399[21] = 
{
	BindingFlags_t1082350898::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
