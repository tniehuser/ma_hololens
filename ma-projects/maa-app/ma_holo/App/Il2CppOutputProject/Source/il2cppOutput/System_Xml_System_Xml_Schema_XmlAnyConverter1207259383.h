﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlBaseConverter217670330.h"

// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAnyConverter
struct  XmlAnyConverter_t1207259383  : public XmlBaseConverter_t217670330
{
public:

public:
};

struct XmlAnyConverter_t1207259383_StaticFields
{
public:
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyConverter::Item
	XmlValueConverter_t68179724 * ___Item_32;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.XmlAnyConverter::AnyAtomic
	XmlValueConverter_t68179724 * ___AnyAtomic_33;

public:
	inline static int32_t get_offset_of_Item_32() { return static_cast<int32_t>(offsetof(XmlAnyConverter_t1207259383_StaticFields, ___Item_32)); }
	inline XmlValueConverter_t68179724 * get_Item_32() const { return ___Item_32; }
	inline XmlValueConverter_t68179724 ** get_address_of_Item_32() { return &___Item_32; }
	inline void set_Item_32(XmlValueConverter_t68179724 * value)
	{
		___Item_32 = value;
		Il2CppCodeGenWriteBarrier(&___Item_32, value);
	}

	inline static int32_t get_offset_of_AnyAtomic_33() { return static_cast<int32_t>(offsetof(XmlAnyConverter_t1207259383_StaticFields, ___AnyAtomic_33)); }
	inline XmlValueConverter_t68179724 * get_AnyAtomic_33() const { return ___AnyAtomic_33; }
	inline XmlValueConverter_t68179724 ** get_address_of_AnyAtomic_33() { return &___AnyAtomic_33; }
	inline void set_AnyAtomic_33(XmlValueConverter_t68179724 * value)
	{
		___AnyAtomic_33 = value;
		Il2CppCodeGenWriteBarrier(&___AnyAtomic_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
