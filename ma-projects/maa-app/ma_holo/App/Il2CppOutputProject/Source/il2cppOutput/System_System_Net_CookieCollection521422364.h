﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection
struct  CookieCollection_t521422364  : public Il2CppObject
{
public:
	// System.Int32 System.Net.CookieCollection::m_version
	int32_t ___m_version_0;
	// System.Collections.ArrayList System.Net.CookieCollection::m_list
	ArrayList_t4252133567 * ___m_list_1;
	// System.DateTime System.Net.CookieCollection::m_TimeStamp
	DateTime_t693205669  ___m_TimeStamp_2;
	// System.Boolean System.Net.CookieCollection::m_has_other_versions
	bool ___m_has_other_versions_3;
	// System.Boolean System.Net.CookieCollection::m_IsReadOnly
	bool ___m_IsReadOnly_4;

public:
	inline static int32_t get_offset_of_m_version_0() { return static_cast<int32_t>(offsetof(CookieCollection_t521422364, ___m_version_0)); }
	inline int32_t get_m_version_0() const { return ___m_version_0; }
	inline int32_t* get_address_of_m_version_0() { return &___m_version_0; }
	inline void set_m_version_0(int32_t value)
	{
		___m_version_0 = value;
	}

	inline static int32_t get_offset_of_m_list_1() { return static_cast<int32_t>(offsetof(CookieCollection_t521422364, ___m_list_1)); }
	inline ArrayList_t4252133567 * get_m_list_1() const { return ___m_list_1; }
	inline ArrayList_t4252133567 ** get_address_of_m_list_1() { return &___m_list_1; }
	inline void set_m_list_1(ArrayList_t4252133567 * value)
	{
		___m_list_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_list_1, value);
	}

	inline static int32_t get_offset_of_m_TimeStamp_2() { return static_cast<int32_t>(offsetof(CookieCollection_t521422364, ___m_TimeStamp_2)); }
	inline DateTime_t693205669  get_m_TimeStamp_2() const { return ___m_TimeStamp_2; }
	inline DateTime_t693205669 * get_address_of_m_TimeStamp_2() { return &___m_TimeStamp_2; }
	inline void set_m_TimeStamp_2(DateTime_t693205669  value)
	{
		___m_TimeStamp_2 = value;
	}

	inline static int32_t get_offset_of_m_has_other_versions_3() { return static_cast<int32_t>(offsetof(CookieCollection_t521422364, ___m_has_other_versions_3)); }
	inline bool get_m_has_other_versions_3() const { return ___m_has_other_versions_3; }
	inline bool* get_address_of_m_has_other_versions_3() { return &___m_has_other_versions_3; }
	inline void set_m_has_other_versions_3(bool value)
	{
		___m_has_other_versions_3 = value;
	}

	inline static int32_t get_offset_of_m_IsReadOnly_4() { return static_cast<int32_t>(offsetof(CookieCollection_t521422364, ___m_IsReadOnly_4)); }
	inline bool get_m_IsReadOnly_4() const { return ___m_IsReadOnly_4; }
	inline bool* get_address_of_m_IsReadOnly_4() { return &___m_IsReadOnly_4; }
	inline void set_m_IsReadOnly_4(bool value)
	{
		___m_IsReadOnly_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
