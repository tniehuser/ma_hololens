﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Security_Mono_Security_Protocol_Tls_Context4285182719.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ServerContext
struct  ServerContext_t3823737132  : public Context_t4285182719
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::request_client_certificate
	bool ___request_client_certificate_31;
	// System.Boolean Mono.Security.Protocol.Tls.ServerContext::clientCertificateRequired
	bool ___clientCertificateRequired_32;

public:
	inline static int32_t get_offset_of_request_client_certificate_31() { return static_cast<int32_t>(offsetof(ServerContext_t3823737132, ___request_client_certificate_31)); }
	inline bool get_request_client_certificate_31() const { return ___request_client_certificate_31; }
	inline bool* get_address_of_request_client_certificate_31() { return &___request_client_certificate_31; }
	inline void set_request_client_certificate_31(bool value)
	{
		___request_client_certificate_31 = value;
	}

	inline static int32_t get_offset_of_clientCertificateRequired_32() { return static_cast<int32_t>(offsetof(ServerContext_t3823737132, ___clientCertificateRequired_32)); }
	inline bool get_clientCertificateRequired_32() const { return ___clientCertificateRequired_32; }
	inline bool* get_address_of_clientCertificateRequired_32() { return &___clientCertificateRequired_32; }
	inline void set_clientCertificateRequired_32(bool value)
	{
		___clientCertificateRequired_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
