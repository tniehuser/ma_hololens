﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.NativeOverlapped
struct  NativeOverlapped_t2890300033 
{
public:
	// System.IntPtr System.Threading.NativeOverlapped::InternalLow
	IntPtr_t ___InternalLow_0;
	// System.IntPtr System.Threading.NativeOverlapped::InternalHigh
	IntPtr_t ___InternalHigh_1;
	// System.Int32 System.Threading.NativeOverlapped::OffsetLow
	int32_t ___OffsetLow_2;
	// System.Int32 System.Threading.NativeOverlapped::OffsetHigh
	int32_t ___OffsetHigh_3;
	// System.IntPtr System.Threading.NativeOverlapped::EventHandle
	IntPtr_t ___EventHandle_4;

public:
	inline static int32_t get_offset_of_InternalLow_0() { return static_cast<int32_t>(offsetof(NativeOverlapped_t2890300033, ___InternalLow_0)); }
	inline IntPtr_t get_InternalLow_0() const { return ___InternalLow_0; }
	inline IntPtr_t* get_address_of_InternalLow_0() { return &___InternalLow_0; }
	inline void set_InternalLow_0(IntPtr_t value)
	{
		___InternalLow_0 = value;
	}

	inline static int32_t get_offset_of_InternalHigh_1() { return static_cast<int32_t>(offsetof(NativeOverlapped_t2890300033, ___InternalHigh_1)); }
	inline IntPtr_t get_InternalHigh_1() const { return ___InternalHigh_1; }
	inline IntPtr_t* get_address_of_InternalHigh_1() { return &___InternalHigh_1; }
	inline void set_InternalHigh_1(IntPtr_t value)
	{
		___InternalHigh_1 = value;
	}

	inline static int32_t get_offset_of_OffsetLow_2() { return static_cast<int32_t>(offsetof(NativeOverlapped_t2890300033, ___OffsetLow_2)); }
	inline int32_t get_OffsetLow_2() const { return ___OffsetLow_2; }
	inline int32_t* get_address_of_OffsetLow_2() { return &___OffsetLow_2; }
	inline void set_OffsetLow_2(int32_t value)
	{
		___OffsetLow_2 = value;
	}

	inline static int32_t get_offset_of_OffsetHigh_3() { return static_cast<int32_t>(offsetof(NativeOverlapped_t2890300033, ___OffsetHigh_3)); }
	inline int32_t get_OffsetHigh_3() const { return ___OffsetHigh_3; }
	inline int32_t* get_address_of_OffsetHigh_3() { return &___OffsetHigh_3; }
	inline void set_OffsetHigh_3(int32_t value)
	{
		___OffsetHigh_3 = value;
	}

	inline static int32_t get_offset_of_EventHandle_4() { return static_cast<int32_t>(offsetof(NativeOverlapped_t2890300033, ___EventHandle_4)); }
	inline IntPtr_t get_EventHandle_4() const { return ___EventHandle_4; }
	inline IntPtr_t* get_address_of_EventHandle_4() { return &___EventHandle_4; }
	inline void set_EventHandle_4(IntPtr_t value)
	{
		___EventHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
