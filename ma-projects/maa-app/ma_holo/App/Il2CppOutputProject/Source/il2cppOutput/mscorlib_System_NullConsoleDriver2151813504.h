﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_ConsoleKeyInfo3124575640.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NullConsoleDriver
struct  NullConsoleDriver_t2151813504  : public Il2CppObject
{
public:

public:
};

struct NullConsoleDriver_t2151813504_StaticFields
{
public:
	// System.ConsoleKeyInfo System.NullConsoleDriver::EmptyConsoleKeyInfo
	ConsoleKeyInfo_t3124575640  ___EmptyConsoleKeyInfo_0;

public:
	inline static int32_t get_offset_of_EmptyConsoleKeyInfo_0() { return static_cast<int32_t>(offsetof(NullConsoleDriver_t2151813504_StaticFields, ___EmptyConsoleKeyInfo_0)); }
	inline ConsoleKeyInfo_t3124575640  get_EmptyConsoleKeyInfo_0() const { return ___EmptyConsoleKeyInfo_0; }
	inline ConsoleKeyInfo_t3124575640 * get_address_of_EmptyConsoleKeyInfo_0() { return &___EmptyConsoleKeyInfo_0; }
	inline void set_EmptyConsoleKeyInfo_0(ConsoleKeyInfo_t3124575640  value)
	{
		___EmptyConsoleKeyInfo_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
