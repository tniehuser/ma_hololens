﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Exception1927440687.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser/XMLError
struct  XMLError_t1680455686  : public Exception_t1927440687
{
public:
	// System.String Mono.Xml.MiniParser/XMLError::descr
	String_t* ___descr_16;
	// System.Int32 Mono.Xml.MiniParser/XMLError::line
	int32_t ___line_17;
	// System.Int32 Mono.Xml.MiniParser/XMLError::column
	int32_t ___column_18;

public:
	inline static int32_t get_offset_of_descr_16() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___descr_16)); }
	inline String_t* get_descr_16() const { return ___descr_16; }
	inline String_t** get_address_of_descr_16() { return &___descr_16; }
	inline void set_descr_16(String_t* value)
	{
		___descr_16 = value;
		Il2CppCodeGenWriteBarrier(&___descr_16, value);
	}

	inline static int32_t get_offset_of_line_17() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___line_17)); }
	inline int32_t get_line_17() const { return ___line_17; }
	inline int32_t* get_address_of_line_17() { return &___line_17; }
	inline void set_line_17(int32_t value)
	{
		___line_17 = value;
	}

	inline static int32_t get_offset_of_column_18() { return static_cast<int32_t>(offsetof(XMLError_t1680455686, ___column_18)); }
	inline int32_t get_column_18() const { return ___column_18; }
	inline int32_t* get_address_of_column_18() { return &___column_18; }
	inline void set_column_18(int32_t value)
	{
		___column_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
