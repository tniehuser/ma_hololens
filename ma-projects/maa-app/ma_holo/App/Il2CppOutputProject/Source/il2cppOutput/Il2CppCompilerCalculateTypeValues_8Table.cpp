﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Threading_Tasks_AsyncCausalityStat2986644407.h"
#include "mscorlib_System_Threading_Tasks_CausalityRelation1204206495.h"
#include "mscorlib_System_Threading_Tasks_CausalitySynchrono1445715615.h"
#include "mscorlib_System_Threading_Tasks_AsyncCausalityTrac4124660798.h"
#include "mscorlib_System_Threading_Tasks_TaskStatus2768542553.h"
#include "mscorlib_System_Threading_Tasks_Task1843236107.h"
#include "mscorlib_System_Threading_Tasks_Task_ContingentProp606988207.h"
#include "mscorlib_System_Threading_Tasks_Task_SetOnInvokeMre468075565.h"
#include "mscorlib_System_Threading_Tasks_Task_DelayPromise1534456833.h"
#include "mscorlib_System_Threading_Tasks_Task_U3CExecuteSel4021759861.h"
#include "mscorlib_System_Threading_Tasks_CompletionActionIn1583323542.h"
#include "mscorlib_System_Threading_Tasks_SystemThreadingTask909725975.h"
#include "mscorlib_System_Threading_Tasks_TaskCreationOptions547302442.h"
#include "mscorlib_System_Threading_Tasks_InternalTaskOption4086034348.h"
#include "mscorlib_System_Threading_Tasks_TaskContinuationOp2826278702.h"
#include "mscorlib_System_Threading_Tasks_StackGuard1348285595.h"
#include "mscorlib_System_Threading_Tasks_VoidTaskResult3325310798.h"
#include "mscorlib_System_Threading_Tasks_TaskCanceledExcepti772081275.h"
#include "mscorlib_System_Threading_Tasks_ContinuationTaskFr2763513533.h"
#include "mscorlib_System_Threading_Tasks_TaskContinuation1422769290.h"
#include "mscorlib_System_Threading_Tasks_StandardTaskContin3524399553.h"
#include "mscorlib_System_Threading_Tasks_SynchronizationCon2253979809.h"
#include "mscorlib_System_Threading_Tasks_TaskSchedulerAwait1166004506.h"
#include "mscorlib_System_Threading_Tasks_AwaitTaskContinuat2160930432.h"
#include "mscorlib_System_Threading_Tasks_TaskExceptionHolde2208677448.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory4228908881.h"
#include "mscorlib_System_Threading_Tasks_TaskFactory_Comple1948414588.h"
#include "mscorlib_System_Threading_Tasks_TaskScheduler3932792796.h"
#include "mscorlib_System_Threading_Tasks_TaskScheduler_Syst1880936772.h"
#include "mscorlib_System_Threading_Tasks_UnobservedTaskExce1087040814.h"
#include "mscorlib_System_Threading_Tasks_TaskSchedulerExcep3829758285.h"
#include "mscorlib_System_Threading_Tasks_ThreadPoolTaskSched943177722.h"
#include "mscorlib_System_Threading_AbandonedMutexException2161467836.h"
#include "mscorlib_System_Threading_AutoResetEvent15112628.h"
#include "mscorlib_System_Threading_EventResetMode4116945436.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307.h"
#include "mscorlib_System_Threading_ContextCallback2287130692.h"
#include "mscorlib_System_Threading_ExecutionContextSwitcher3304742894.h"
#include "mscorlib_System_Threading_ExecutionContext1392266323.h"
#include "mscorlib_System_Threading_ExecutionContext_Flags4022531286.h"
#include "mscorlib_System_Threading_ExecutionContext_Reader611157692.h"
#include "mscorlib_System_Threading_ExecutionContext_Capture2720903475.h"
#include "mscorlib_System_Threading_LockRecursionException2514728202.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Threading_Monitor3228523394.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart2412552885.h"
#include "mscorlib_System_Threading_SemaphoreFullException1495620572.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "mscorlib_System_Threading_SynchronizationContext3857790437.h"
#include "mscorlib_System_Threading_SynchronizationLockExcept117698316.h"
#include "mscorlib_System_Threading_ThreadHelper1887861858.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_Threading_StackCrawlMark2550138982.h"
#include "mscorlib_System_Threading_ThreadAbortException1150575753.h"
#include "mscorlib_System_Threading_ThreadInterruptedException63303933.h"
#include "mscorlib_System_Threading_WaitCallback2798937288.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback2724438238.h"
#include "mscorlib_System_Threading_ThreadPoolGlobals3764112220.h"
#include "mscorlib_System_Threading_ThreadPoolWorkQueue673814512.h"
#include "mscorlib_System_Threading_ThreadPoolWorkQueue_Work2897576067.h"
#include "mscorlib_System_Threading_ThreadPoolWorkQueue_Queue905751008.h"
#include "mscorlib_System_Threading_ThreadPoolWorkQueueThrea3127465848.h"
#include "mscorlib_System_Threading__ThreadPoolWaitCallback1965863693.h"
#include "mscorlib_System_Threading_QueueUserWorkItemCallbac3786001013.h"
#include "mscorlib_System_Threading_ThreadPool3989917080.h"
#include "mscorlib_System_Threading_ThreadStart3437517264.h"
#include "mscorlib_System_Threading_ThreadState1158972609.h"
#include "mscorlib_System_Threading_ThreadStateException1404755912.h"
#include "mscorlib_System_Threading_Timeout35240459.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "mscorlib_System_Threading_WaitHandleCannotBeOpened2190772789.h"
#include "mscorlib_System_ThreadStaticAttribute1787731584.h"
#include "mscorlib_System_ThrowHelper3666395978.h"
#include "mscorlib_System_ExceptionArgument2966871834.h"
#include "mscorlib_System_ExceptionResource2812258639.h"
#include "mscorlib_System_TimeoutException3246754798.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Tuple3195304974.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_TypedReference1025199857.h"
#include "mscorlib_System_TypeInitializationException3654642183.h"
#include "mscorlib_System_TypeLoadException723359155.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt322149682021.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize800 = { sizeof (AsyncCausalityStatus_t2986644407)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable800[5] = 
{
	AsyncCausalityStatus_t2986644407::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize801 = { sizeof (CausalityRelation_t1204206495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable801[6] = 
{
	CausalityRelation_t1204206495::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize802 = { sizeof (CausalitySynchronousWork_t1445715615)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable802[4] = 
{
	CausalitySynchronousWork_t1445715615::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize803 = { sizeof (AsyncCausalityTracer_t4124660798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize804 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable804[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize805 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable806[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable807[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable808[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable809[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize810 = { sizeof (TaskStatus_t2768542553)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable810[9] = 
{
	TaskStatus_t2768542553::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize811 = { sizeof (Task_t1843236107), -1, sizeof(Task_t1843236107_StaticFields), sizeof(Task_t1843236107_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable811[24] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	Task_t1843236107_StaticFields::get_offset_of_s_taskIdCounter_2(),
	Task_t1843236107_StaticFields::get_offset_of_s_factory_3(),
	Task_t1843236107::get_offset_of_m_taskId_4(),
	Task_t1843236107::get_offset_of_m_action_5(),
	Task_t1843236107::get_offset_of_m_stateObject_6(),
	Task_t1843236107::get_offset_of_m_taskScheduler_7(),
	Task_t1843236107::get_offset_of_m_parent_8(),
	Task_t1843236107::get_offset_of_m_stateFlags_9(),
	Task_t1843236107::get_offset_of_m_continuationObject_10(),
	Task_t1843236107_StaticFields::get_offset_of_s_taskCompletionSentinel_11(),
	Task_t1843236107_StaticFields::get_offset_of_s_asyncDebuggingEnabled_12(),
	Task_t1843236107_StaticFields::get_offset_of_s_currentActiveTasks_13(),
	Task_t1843236107_StaticFields::get_offset_of_s_activeTasksLock_14(),
	Task_t1843236107::get_offset_of_m_contingentProperties_15(),
	Task_t1843236107_StaticFields::get_offset_of_s_taskCancelCallback_16(),
	Task_t1843236107_StaticFields::get_offset_of_s_createContingentProperties_17(),
	Task_t1843236107_StaticFields::get_offset_of_s_completedTask_18(),
	Task_t1843236107_StaticFields::get_offset_of_s_IsExceptionObservedByParentPredicate_19(),
	Task_t1843236107_StaticFields::get_offset_of_s_ecCallback_20(),
	Task_t1843236107_StaticFields::get_offset_of_s_IsTaskContinuationNullPredicate_21(),
	Task_t1843236107_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
	Task_t1843236107_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize812 = { sizeof (ContingentProperties_t606988207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable812[8] = 
{
	ContingentProperties_t606988207::get_offset_of_m_capturedContext_0(),
	ContingentProperties_t606988207::get_offset_of_m_completionEvent_1(),
	ContingentProperties_t606988207::get_offset_of_m_exceptionsHolder_2(),
	ContingentProperties_t606988207::get_offset_of_m_cancellationToken_3(),
	ContingentProperties_t606988207::get_offset_of_m_cancellationRegistration_4(),
	ContingentProperties_t606988207::get_offset_of_m_internalCancellationRequested_5(),
	ContingentProperties_t606988207::get_offset_of_m_completionCountdown_6(),
	ContingentProperties_t606988207::get_offset_of_m_exceptionalChildren_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize813 = { sizeof (SetOnInvokeMres_t468075565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize814 = { sizeof (DelayPromise_t1534456833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable814[3] = 
{
	DelayPromise_t1534456833::get_offset_of_Token_27(),
	DelayPromise_t1534456833::get_offset_of_Registration_28(),
	DelayPromise_t1534456833::get_offset_of_Timer_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize815 = { sizeof (U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable815[5] = 
{
	U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861::get_offset_of_root_0(),
	U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861::get_offset_of_replicasAreQuitting_1(),
	U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861::get_offset_of_taskReplicaDelegate_2(),
	U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861::get_offset_of_creationOptionsForReplicas_3(),
	U3CExecuteSelfReplicatingU3Ec__AnonStorey0_t4021759861::get_offset_of_internalOptionsForReplicas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize816 = { sizeof (CompletionActionInvoker_t1583323542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable816[2] = 
{
	CompletionActionInvoker_t1583323542::get_offset_of_m_action_0(),
	CompletionActionInvoker_t1583323542::get_offset_of_m_completingTask_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize817 = { sizeof (SystemThreadingTasks_TaskDebugView_t909725975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize818 = { sizeof (TaskCreationOptions_t547302442)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable818[8] = 
{
	TaskCreationOptions_t547302442::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize819 = { sizeof (InternalTaskOptions_t4086034348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable819[10] = 
{
	InternalTaskOptions_t4086034348::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize820 = { sizeof (TaskContinuationOptions_t2826278702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable820[16] = 
{
	TaskContinuationOptions_t2826278702::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize821 = { sizeof (StackGuard_t1348285595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable821[1] = 
{
	StackGuard_t1348285595::get_offset_of_m_inliningDepth_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize822 = { sizeof (VoidTaskResult_t3325310798)+ sizeof (Il2CppObject), sizeof(VoidTaskResult_t3325310798 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize824 = { sizeof (TaskCanceledException_t772081275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable824[1] = 
{
	TaskCanceledException_t772081275::get_offset_of_m_canceledTask_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize825 = { sizeof (ContinuationTaskFromTask_t2763513533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable825[1] = 
{
	ContinuationTaskFromTask_t2763513533::get_offset_of_m_antecedent_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize826 = { sizeof (TaskContinuation_t1422769290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize827 = { sizeof (StandardTaskContinuation_t3524399553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable827[3] = 
{
	StandardTaskContinuation_t3524399553::get_offset_of_m_task_0(),
	StandardTaskContinuation_t3524399553::get_offset_of_m_options_1(),
	StandardTaskContinuation_t3524399553::get_offset_of_m_taskScheduler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize828 = { sizeof (SynchronizationContextAwaitTaskContinuation_t2253979809), -1, sizeof(SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable828[4] = 
{
	SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields::get_offset_of_s_postCallback_5(),
	SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields::get_offset_of_s_postActionCallback_6(),
	SynchronizationContextAwaitTaskContinuation_t2253979809::get_offset_of_m_syncContext_7(),
	SynchronizationContextAwaitTaskContinuation_t2253979809_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize829 = { sizeof (TaskSchedulerAwaitTaskContinuation_t1166004506), -1, sizeof(TaskSchedulerAwaitTaskContinuation_t1166004506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable829[2] = 
{
	TaskSchedulerAwaitTaskContinuation_t1166004506::get_offset_of_m_scheduler_5(),
	TaskSchedulerAwaitTaskContinuation_t1166004506_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize830 = { sizeof (AwaitTaskContinuation_t2160930432), -1, sizeof(AwaitTaskContinuation_t2160930432_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable830[5] = 
{
	AwaitTaskContinuation_t2160930432::get_offset_of_m_capturedContext_0(),
	AwaitTaskContinuation_t2160930432::get_offset_of_m_action_1(),
	AwaitTaskContinuation_t2160930432_StaticFields::get_offset_of_s_invokeActionCallback_2(),
	AwaitTaskContinuation_t2160930432_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	AwaitTaskContinuation_t2160930432_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize831 = { sizeof (TaskExceptionHolder_t2208677448), -1, sizeof(TaskExceptionHolder_t2208677448_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable831[8] = 
{
	TaskExceptionHolder_t2208677448_StaticFields::get_offset_of_s_failFastOnUnobservedException_0(),
	TaskExceptionHolder_t2208677448_StaticFields::get_offset_of_s_domainUnloadStarted_1(),
	TaskExceptionHolder_t2208677448_StaticFields::get_offset_of_s_adUnloadEventHandler_2(),
	TaskExceptionHolder_t2208677448::get_offset_of_m_task_3(),
	TaskExceptionHolder_t2208677448::get_offset_of_m_faultExceptions_4(),
	TaskExceptionHolder_t2208677448::get_offset_of_m_cancellationException_5(),
	TaskExceptionHolder_t2208677448::get_offset_of_m_isHandled_6(),
	TaskExceptionHolder_t2208677448_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize832 = { sizeof (TaskFactory_t4228908881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable832[4] = 
{
	TaskFactory_t4228908881::get_offset_of_m_defaultCancellationToken_0(),
	TaskFactory_t4228908881::get_offset_of_m_defaultScheduler_1(),
	TaskFactory_t4228908881::get_offset_of_m_defaultCreationOptions_2(),
	TaskFactory_t4228908881::get_offset_of_m_defaultContinuationOptions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize833 = { sizeof (CompleteOnInvokePromise_t1948414588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable833[2] = 
{
	CompleteOnInvokePromise_t1948414588::get_offset_of__tasks_27(),
	CompleteOnInvokePromise_t1948414588::get_offset_of_m_firstTaskAlreadyCompleted_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize834 = { sizeof (TaskScheduler_t3932792796), -1, sizeof(TaskScheduler_t3932792796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable834[4] = 
{
	TaskScheduler_t3932792796_StaticFields::get_offset_of_s_activeTaskSchedulers_0(),
	TaskScheduler_t3932792796_StaticFields::get_offset_of_s_defaultTaskScheduler_1(),
	TaskScheduler_t3932792796_StaticFields::get_offset_of__unobservedTaskException_2(),
	TaskScheduler_t3932792796_StaticFields::get_offset_of__unobservedTaskExceptionLockObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize835 = { sizeof (SystemThreadingTasks_TaskSchedulerDebugView_t1880936772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize836 = { sizeof (UnobservedTaskExceptionEventArgs_t1087040814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable836[2] = 
{
	UnobservedTaskExceptionEventArgs_t1087040814::get_offset_of_m_exception_1(),
	UnobservedTaskExceptionEventArgs_t1087040814::get_offset_of_m_observed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize837 = { sizeof (TaskSchedulerException_t3829758285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize838 = { sizeof (ThreadPoolTaskScheduler_t943177722), -1, sizeof(ThreadPoolTaskScheduler_t943177722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable838[1] = 
{
	ThreadPoolTaskScheduler_t943177722_StaticFields::get_offset_of_s_longRunningThreadWork_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize839 = { sizeof (AbandonedMutexException_t2161467836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable839[2] = 
{
	AbandonedMutexException_t2161467836::get_offset_of_m_MutexIndex_16(),
	AbandonedMutexException_t2161467836::get_offset_of_m_Mutex_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable840[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize841 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize842 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable842[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize843 = { sizeof (AutoResetEvent_t15112628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize844 = { sizeof (EventResetMode_t4116945436)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable844[3] = 
{
	EventResetMode_t4116945436::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize845 = { sizeof (EventWaitHandle_t2091316307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize846 = { sizeof (ContextCallback_t2287130692), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize847 = { sizeof (ExecutionContextSwitcher_t3304742894)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable847[4] = 
{
	ExecutionContextSwitcher_t3304742894::get_offset_of_outerEC_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ExecutionContextSwitcher_t3304742894::get_offset_of_outerECBelongsToScope_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ExecutionContextSwitcher_t3304742894::get_offset_of_hecsw_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ExecutionContextSwitcher_t3304742894::get_offset_of_thread_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize848 = { sizeof (ExecutionContext_t1392266323), -1, sizeof(ExecutionContext_t1392266323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable848[8] = 
{
	ExecutionContext_t1392266323::get_offset_of__syncContext_0(),
	ExecutionContext_t1392266323::get_offset_of__syncContextNoFlow_1(),
	ExecutionContext_t1392266323::get_offset_of__logicalCallContext_2(),
	ExecutionContext_t1392266323::get_offset_of__illogicalCallContext_3(),
	ExecutionContext_t1392266323::get_offset_of__flags_4(),
	ExecutionContext_t1392266323::get_offset_of__localValues_5(),
	ExecutionContext_t1392266323::get_offset_of__localChangeNotifications_6(),
	ExecutionContext_t1392266323_StaticFields::get_offset_of_s_dummyDefaultEC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize849 = { sizeof (Flags_t4022531286)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable849[5] = 
{
	Flags_t4022531286::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize850 = { sizeof (Reader_t611157692)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable850[1] = 
{
	Reader_t611157692::get_offset_of_m_ec_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize851 = { sizeof (CaptureOptions_t2720903475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable851[4] = 
{
	CaptureOptions_t2720903475::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize852 = { sizeof (LockRecursionException_t2514728202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize853 = { sizeof (ManualResetEvent_t926074657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize854 = { sizeof (Monitor_t3228523394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize855 = { sizeof (ParameterizedThreadStart_t2412552885), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize856 = { sizeof (SemaphoreFullException_t1495620572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize857 = { sizeof (SendOrPostCallback_t296893742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize858 = { sizeof (SynchronizationContext_t3857790437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize859 = { sizeof (SynchronizationLockException_t117698316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize860 = { sizeof (ThreadHelper_t1887861858), -1, sizeof(ThreadHelper_t1887861858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable860[4] = 
{
	ThreadHelper_t1887861858::get_offset_of__start_0(),
	ThreadHelper_t1887861858::get_offset_of__startArg_1(),
	ThreadHelper_t1887861858::get_offset_of__executionContext_2(),
	ThreadHelper_t1887861858_StaticFields::get_offset_of__ccb_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize861 = { sizeof (Thread_t241561612), -1, sizeof(Thread_t241561612_StaticFields), sizeof(Thread_t241561612_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable861[18] = 
{
	Thread_t241561612_StaticFields::get_offset_of_s_LocalDataStoreMgr_0(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	Thread_t241561612_StaticFields::get_offset_of_s_asyncLocalCurrentCulture_4(),
	Thread_t241561612_StaticFields::get_offset_of_s_asyncLocalCurrentUICulture_5(),
	Thread_t241561612::get_offset_of_internal_thread_6(),
	Thread_t241561612::get_offset_of_m_ThreadStartArg_7(),
	Thread_t241561612::get_offset_of_pending_exception_8(),
	Thread_t241561612::get_offset_of_priority_9(),
	Thread_t241561612::get_offset_of_principal_10(),
	Thread_t241561612::get_offset_of_principal_version_11(),
	THREAD_STATIC_FIELD_OFFSET,
	Thread_t241561612::get_offset_of_m_Delegate_13(),
	Thread_t241561612::get_offset_of_m_ExecutionContext_14(),
	Thread_t241561612::get_offset_of_m_ExecutionContextBelongsToOuterScope_15(),
	Thread_t241561612_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_16(),
	Thread_t241561612_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize862 = { sizeof (StackCrawlMark_t2550138982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable862[5] = 
{
	StackCrawlMark_t2550138982::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize863 = { sizeof (ThreadAbortException_t1150575753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize864 = { sizeof (ThreadInterruptedException_t63303933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize866 = { sizeof (WaitCallback_t2798937288), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize867 = { sizeof (WaitOrTimerCallback_t2724438238), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize868 = { sizeof (ThreadPoolGlobals_t3764112220), -1, sizeof(ThreadPoolGlobals_t3764112220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable868[6] = 
{
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_tpQuantum_0(),
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_processorCount_1(),
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_tpHosted_2(),
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_vmTpInitialized_3(),
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_enableWorkerTracking_4(),
	ThreadPoolGlobals_t3764112220_StaticFields::get_offset_of_workQueue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize869 = { sizeof (ThreadPoolWorkQueue_t673814512), -1, sizeof(ThreadPoolWorkQueue_t673814512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable869[4] = 
{
	ThreadPoolWorkQueue_t673814512::get_offset_of_queueHead_0(),
	ThreadPoolWorkQueue_t673814512::get_offset_of_queueTail_1(),
	ThreadPoolWorkQueue_t673814512_StaticFields::get_offset_of_allThreadQueues_2(),
	ThreadPoolWorkQueue_t673814512::get_offset_of_numOutstandingThreadRequests_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize870 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable870[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize871 = { sizeof (WorkStealingQueue_t2897576067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable871[5] = 
{
	WorkStealingQueue_t2897576067::get_offset_of_m_array_0(),
	WorkStealingQueue_t2897576067::get_offset_of_m_mask_1(),
	WorkStealingQueue_t2897576067::get_offset_of_m_headIndex_2(),
	WorkStealingQueue_t2897576067::get_offset_of_m_tailIndex_3(),
	WorkStealingQueue_t2897576067::get_offset_of_m_foreignLock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize872 = { sizeof (QueueSegment_t905751008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable872[3] = 
{
	QueueSegment_t905751008::get_offset_of_nodes_0(),
	QueueSegment_t905751008::get_offset_of_indexes_1(),
	QueueSegment_t905751008::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize873 = { sizeof (ThreadPoolWorkQueueThreadLocals_t3127465848), -1, 0, sizeof(ThreadPoolWorkQueueThreadLocals_t3127465848_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable873[4] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	ThreadPoolWorkQueueThreadLocals_t3127465848::get_offset_of_workQueue_1(),
	ThreadPoolWorkQueueThreadLocals_t3127465848::get_offset_of_workStealingQueue_2(),
	ThreadPoolWorkQueueThreadLocals_t3127465848::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize874 = { sizeof (_ThreadPoolWaitCallback_t1965863693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize875 = { sizeof (QueueUserWorkItemCallback_t3786001013), -1, sizeof(QueueUserWorkItemCallback_t3786001013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable875[4] = 
{
	QueueUserWorkItemCallback_t3786001013::get_offset_of_callback_0(),
	QueueUserWorkItemCallback_t3786001013::get_offset_of_context_1(),
	QueueUserWorkItemCallback_t3786001013::get_offset_of_state_2(),
	QueueUserWorkItemCallback_t3786001013_StaticFields::get_offset_of_ccb_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize876 = { sizeof (ThreadPool_t3989917080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize877 = { sizeof (ThreadStart_t3437517264), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize878 = { sizeof (ThreadState_t1158972609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable878[11] = 
{
	ThreadState_t1158972609::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize879 = { sizeof (ThreadStateException_t1404755912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize880 = { sizeof (Timeout_t35240459), -1, sizeof(Timeout_t35240459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable880[1] = 
{
	Timeout_t35240459_StaticFields::get_offset_of_InfiniteTimeSpan_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize881 = { sizeof (WaitHandle_t677569169), -1, sizeof(WaitHandle_t677569169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable881[10] = 
{
	0,
	0,
	WaitHandle_t677569169::get_offset_of_waitHandle_3(),
	WaitHandle_t677569169::get_offset_of_safeWaitHandle_4(),
	WaitHandle_t677569169::get_offset_of_hasThreadAffinity_5(),
	0,
	0,
	0,
	0,
	WaitHandle_t677569169_StaticFields::get_offset_of_InvalidHandle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize882 = { sizeof (WaitHandleCannotBeOpenedException_t2190772789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize883 = { sizeof (ThreadStaticAttribute_t1787731584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize884 = { sizeof (ThrowHelper_t3666395978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize885 = { sizeof (ExceptionArgument_t2966871834)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable885[25] = 
{
	ExceptionArgument_t2966871834::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize886 = { sizeof (ExceptionResource_t2812258639)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable886[47] = 
{
	ExceptionResource_t2812258639::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize887 = { sizeof (TimeoutException_t3246754798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize888 = { sizeof (TimeSpan_t3430258949)+ sizeof (Il2CppObject), sizeof(TimeSpan_t3430258949 ), sizeof(TimeSpan_t3430258949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable888[6] = 
{
	TimeSpan_t3430258949_StaticFields::get_offset_of_Zero_0(),
	TimeSpan_t3430258949_StaticFields::get_offset_of_MaxValue_1(),
	TimeSpan_t3430258949_StaticFields::get_offset_of_MinValue_2(),
	TimeSpan_t3430258949::get_offset_of__ticks_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TimeSpan_t3430258949_StaticFields::get_offset_of__legacyConfigChecked_4(),
	TimeSpan_t3430258949_StaticFields::get_offset_of__legacyMode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize889 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize890 = { sizeof (Tuple_t3195304974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize891 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable891[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize892 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable892[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize893 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable893[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize894 = { sizeof (Type_t), -1, sizeof(Type_t_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable894[8] = 
{
	Type_t_StaticFields::get_offset_of_FilterAttribute_0(),
	Type_t_StaticFields::get_offset_of_FilterName_1(),
	Type_t_StaticFields::get_offset_of_FilterNameIgnoreCase_2(),
	Type_t_StaticFields::get_offset_of_Missing_3(),
	Type_t_StaticFields::get_offset_of_Delimiter_4(),
	Type_t_StaticFields::get_offset_of_EmptyTypes_5(),
	Type_t_StaticFields::get_offset_of_defaultBinder_6(),
	Type_t::get_offset_of__impl_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize895 = { sizeof (TypedReference_t1025199857)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable895[3] = 
{
	TypedReference_t1025199857::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypedReference_t1025199857::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypedReference_t1025199857::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize896 = { sizeof (TypeInitializationException_t3654642183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable896[1] = 
{
	TypeInitializationException_t3654642183::get_offset_of__typeName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize897 = { sizeof (TypeLoadException_t723359155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable897[4] = 
{
	TypeLoadException_t723359155::get_offset_of_ClassName_16(),
	TypeLoadException_t723359155::get_offset_of_AssemblyName_17(),
	TypeLoadException_t723359155::get_offset_of_MessageArg_18(),
	TypeLoadException_t723359155::get_offset_of_ResourceId_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize898 = { sizeof (UInt16_t986882611)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable898[1] = 
{
	UInt16_t986882611::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize899 = { sizeof (UInt32_t2149682021)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable899[1] = 
{
	UInt32_t2149682021::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
