﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.Assembly
struct Assembly_t4268412390;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TopLevelAssemblyTypeResolver
struct  TopLevelAssemblyTypeResolver_t3366789405  : public Il2CppObject
{
public:
	// System.Reflection.Assembly System.Runtime.Serialization.Formatters.Binary.ObjectReader/TopLevelAssemblyTypeResolver::m_topLevelAssembly
	Assembly_t4268412390 * ___m_topLevelAssembly_0;

public:
	inline static int32_t get_offset_of_m_topLevelAssembly_0() { return static_cast<int32_t>(offsetof(TopLevelAssemblyTypeResolver_t3366789405, ___m_topLevelAssembly_0)); }
	inline Assembly_t4268412390 * get_m_topLevelAssembly_0() const { return ___m_topLevelAssembly_0; }
	inline Assembly_t4268412390 ** get_address_of_m_topLevelAssembly_0() { return &___m_topLevelAssembly_0; }
	inline void set_m_topLevelAssembly_0(Assembly_t4268412390 * value)
	{
		___m_topLevelAssembly_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_topLevelAssembly_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
