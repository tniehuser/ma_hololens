﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexPrefix
struct  RegexPrefix_t1013837165  : public Il2CppObject
{
public:
	// System.String System.Text.RegularExpressions.RegexPrefix::_prefix
	String_t* ____prefix_0;
	// System.Boolean System.Text.RegularExpressions.RegexPrefix::_caseInsensitive
	bool ____caseInsensitive_1;

public:
	inline static int32_t get_offset_of__prefix_0() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165, ____prefix_0)); }
	inline String_t* get__prefix_0() const { return ____prefix_0; }
	inline String_t** get_address_of__prefix_0() { return &____prefix_0; }
	inline void set__prefix_0(String_t* value)
	{
		____prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&____prefix_0, value);
	}

	inline static int32_t get_offset_of__caseInsensitive_1() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165, ____caseInsensitive_1)); }
	inline bool get__caseInsensitive_1() const { return ____caseInsensitive_1; }
	inline bool* get_address_of__caseInsensitive_1() { return &____caseInsensitive_1; }
	inline void set__caseInsensitive_1(bool value)
	{
		____caseInsensitive_1 = value;
	}
};

struct RegexPrefix_t1013837165_StaticFields
{
public:
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexPrefix::_empty
	RegexPrefix_t1013837165 * ____empty_2;

public:
	inline static int32_t get_offset_of__empty_2() { return static_cast<int32_t>(offsetof(RegexPrefix_t1013837165_StaticFields, ____empty_2)); }
	inline RegexPrefix_t1013837165 * get__empty_2() const { return ____empty_2; }
	inline RegexPrefix_t1013837165 ** get_address_of__empty_2() { return &____empty_2; }
	inline void set__empty_2(RegexPrefix_t1013837165 * value)
	{
		____empty_2 = value;
		Il2CppCodeGenWriteBarrier(&____empty_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
