﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_TypeConverter745995970.h"

// System.String
struct String_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ReferenceConverter
struct  ReferenceConverter_t3131270729  : public TypeConverter_t745995970
{
public:
	// System.Type System.ComponentModel.ReferenceConverter::type
	Type_t * ___type_5;

public:
	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ReferenceConverter_t3131270729, ___type_5)); }
	inline Type_t * get_type_5() const { return ___type_5; }
	inline Type_t ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(Type_t * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier(&___type_5, value);
	}
};

struct ReferenceConverter_t3131270729_StaticFields
{
public:
	// System.String System.ComponentModel.ReferenceConverter::none
	String_t* ___none_4;

public:
	inline static int32_t get_offset_of_none_4() { return static_cast<int32_t>(offsetof(ReferenceConverter_t3131270729_StaticFields, ___none_4)); }
	inline String_t* get_none_4() const { return ___none_4; }
	inline String_t** get_address_of_none_4() { return &___none_4; }
	inline void set_none_4(String_t* value)
	{
		___none_4 = value;
		Il2CppCodeGenWriteBarrier(&___none_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
