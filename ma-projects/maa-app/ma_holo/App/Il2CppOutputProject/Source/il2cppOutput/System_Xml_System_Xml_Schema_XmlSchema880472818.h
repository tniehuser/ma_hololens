﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"

// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t287209776;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Uri
struct Uri_t19570940;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchema
struct  XmlSchema_t880472818  : public XmlSchemaObject_t2050913741
{
public:
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::attributeFormDefault
	int32_t ___attributeFormDefault_6;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::elementFormDefault
	int32_t ___elementFormDefault_7;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::blockDefault
	int32_t ___blockDefault_8;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::finalDefault
	int32_t ___finalDefault_9;
	// System.String System.Xml.Schema.XmlSchema::targetNs
	String_t* ___targetNs_10;
	// System.String System.Xml.Schema.XmlSchema::version
	String_t* ___version_11;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::includes
	XmlSchemaObjectCollection_t395083109 * ___includes_12;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::items
	XmlSchemaObjectCollection_t395083109 * ___items_13;
	// System.String System.Xml.Schema.XmlSchema::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchema::moreAttributes
	XmlAttributeU5BU5D_t287209776* ___moreAttributes_15;
	// System.Boolean System.Xml.Schema.XmlSchema::isCompiled
	bool ___isCompiled_16;
	// System.Boolean System.Xml.Schema.XmlSchema::isCompiledBySet
	bool ___isCompiledBySet_17;
	// System.Boolean System.Xml.Schema.XmlSchema::isPreprocessed
	bool ___isPreprocessed_18;
	// System.Int32 System.Xml.Schema.XmlSchema::errorCount
	int32_t ___errorCount_19;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributes
	XmlSchemaObjectTable_t3364835593 * ___attributes_20;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributeGroups
	XmlSchemaObjectTable_t3364835593 * ___attributeGroups_21;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::elements
	XmlSchemaObjectTable_t3364835593 * ___elements_22;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::types
	XmlSchemaObjectTable_t3364835593 * ___types_23;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::groups
	XmlSchemaObjectTable_t3364835593 * ___groups_24;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::notations
	XmlSchemaObjectTable_t3364835593 * ___notations_25;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::identityConstraints
	XmlSchemaObjectTable_t3364835593 * ___identityConstraints_26;
	// System.Int32 System.Xml.Schema.XmlSchema::schemaId
	int32_t ___schemaId_28;
	// System.Uri System.Xml.Schema.XmlSchema::baseUri
	Uri_t19570940 * ___baseUri_29;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchema::ids
	Hashtable_t909839986 * ___ids_30;
	// System.Xml.XmlDocument System.Xml.Schema.XmlSchema::document
	XmlDocument_t3649534162 * ___document_31;

public:
	inline static int32_t get_offset_of_attributeFormDefault_6() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributeFormDefault_6)); }
	inline int32_t get_attributeFormDefault_6() const { return ___attributeFormDefault_6; }
	inline int32_t* get_address_of_attributeFormDefault_6() { return &___attributeFormDefault_6; }
	inline void set_attributeFormDefault_6(int32_t value)
	{
		___attributeFormDefault_6 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_7() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___elementFormDefault_7)); }
	inline int32_t get_elementFormDefault_7() const { return ___elementFormDefault_7; }
	inline int32_t* get_address_of_elementFormDefault_7() { return &___elementFormDefault_7; }
	inline void set_elementFormDefault_7(int32_t value)
	{
		___elementFormDefault_7 = value;
	}

	inline static int32_t get_offset_of_blockDefault_8() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___blockDefault_8)); }
	inline int32_t get_blockDefault_8() const { return ___blockDefault_8; }
	inline int32_t* get_address_of_blockDefault_8() { return &___blockDefault_8; }
	inline void set_blockDefault_8(int32_t value)
	{
		___blockDefault_8 = value;
	}

	inline static int32_t get_offset_of_finalDefault_9() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___finalDefault_9)); }
	inline int32_t get_finalDefault_9() const { return ___finalDefault_9; }
	inline int32_t* get_address_of_finalDefault_9() { return &___finalDefault_9; }
	inline void set_finalDefault_9(int32_t value)
	{
		___finalDefault_9 = value;
	}

	inline static int32_t get_offset_of_targetNs_10() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___targetNs_10)); }
	inline String_t* get_targetNs_10() const { return ___targetNs_10; }
	inline String_t** get_address_of_targetNs_10() { return &___targetNs_10; }
	inline void set_targetNs_10(String_t* value)
	{
		___targetNs_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetNs_10, value);
	}

	inline static int32_t get_offset_of_version_11() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___version_11)); }
	inline String_t* get_version_11() const { return ___version_11; }
	inline String_t** get_address_of_version_11() { return &___version_11; }
	inline void set_version_11(String_t* value)
	{
		___version_11 = value;
		Il2CppCodeGenWriteBarrier(&___version_11, value);
	}

	inline static int32_t get_offset_of_includes_12() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___includes_12)); }
	inline XmlSchemaObjectCollection_t395083109 * get_includes_12() const { return ___includes_12; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_includes_12() { return &___includes_12; }
	inline void set_includes_12(XmlSchemaObjectCollection_t395083109 * value)
	{
		___includes_12 = value;
		Il2CppCodeGenWriteBarrier(&___includes_12, value);
	}

	inline static int32_t get_offset_of_items_13() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___items_13)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_13() const { return ___items_13; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_13() { return &___items_13; }
	inline void set_items_13(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_13 = value;
		Il2CppCodeGenWriteBarrier(&___items_13, value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier(&___id_14, value);
	}

	inline static int32_t get_offset_of_moreAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___moreAttributes_15)); }
	inline XmlAttributeU5BU5D_t287209776* get_moreAttributes_15() const { return ___moreAttributes_15; }
	inline XmlAttributeU5BU5D_t287209776** get_address_of_moreAttributes_15() { return &___moreAttributes_15; }
	inline void set_moreAttributes_15(XmlAttributeU5BU5D_t287209776* value)
	{
		___moreAttributes_15 = value;
		Il2CppCodeGenWriteBarrier(&___moreAttributes_15, value);
	}

	inline static int32_t get_offset_of_isCompiled_16() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___isCompiled_16)); }
	inline bool get_isCompiled_16() const { return ___isCompiled_16; }
	inline bool* get_address_of_isCompiled_16() { return &___isCompiled_16; }
	inline void set_isCompiled_16(bool value)
	{
		___isCompiled_16 = value;
	}

	inline static int32_t get_offset_of_isCompiledBySet_17() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___isCompiledBySet_17)); }
	inline bool get_isCompiledBySet_17() const { return ___isCompiledBySet_17; }
	inline bool* get_address_of_isCompiledBySet_17() { return &___isCompiledBySet_17; }
	inline void set_isCompiledBySet_17(bool value)
	{
		___isCompiledBySet_17 = value;
	}

	inline static int32_t get_offset_of_isPreprocessed_18() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___isPreprocessed_18)); }
	inline bool get_isPreprocessed_18() const { return ___isPreprocessed_18; }
	inline bool* get_address_of_isPreprocessed_18() { return &___isPreprocessed_18; }
	inline void set_isPreprocessed_18(bool value)
	{
		___isPreprocessed_18 = value;
	}

	inline static int32_t get_offset_of_errorCount_19() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___errorCount_19)); }
	inline int32_t get_errorCount_19() const { return ___errorCount_19; }
	inline int32_t* get_address_of_errorCount_19() { return &___errorCount_19; }
	inline void set_errorCount_19(int32_t value)
	{
		___errorCount_19 = value;
	}

	inline static int32_t get_offset_of_attributes_20() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributes_20)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributes_20() const { return ___attributes_20; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributes_20() { return &___attributes_20; }
	inline void set_attributes_20(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributes_20 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_20, value);
	}

	inline static int32_t get_offset_of_attributeGroups_21() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributeGroups_21)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeGroups_21() const { return ___attributeGroups_21; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeGroups_21() { return &___attributeGroups_21; }
	inline void set_attributeGroups_21(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeGroups_21 = value;
		Il2CppCodeGenWriteBarrier(&___attributeGroups_21, value);
	}

	inline static int32_t get_offset_of_elements_22() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___elements_22)); }
	inline XmlSchemaObjectTable_t3364835593 * get_elements_22() const { return ___elements_22; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_elements_22() { return &___elements_22; }
	inline void set_elements_22(XmlSchemaObjectTable_t3364835593 * value)
	{
		___elements_22 = value;
		Il2CppCodeGenWriteBarrier(&___elements_22, value);
	}

	inline static int32_t get_offset_of_types_23() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___types_23)); }
	inline XmlSchemaObjectTable_t3364835593 * get_types_23() const { return ___types_23; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_types_23() { return &___types_23; }
	inline void set_types_23(XmlSchemaObjectTable_t3364835593 * value)
	{
		___types_23 = value;
		Il2CppCodeGenWriteBarrier(&___types_23, value);
	}

	inline static int32_t get_offset_of_groups_24() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___groups_24)); }
	inline XmlSchemaObjectTable_t3364835593 * get_groups_24() const { return ___groups_24; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_groups_24() { return &___groups_24; }
	inline void set_groups_24(XmlSchemaObjectTable_t3364835593 * value)
	{
		___groups_24 = value;
		Il2CppCodeGenWriteBarrier(&___groups_24, value);
	}

	inline static int32_t get_offset_of_notations_25() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___notations_25)); }
	inline XmlSchemaObjectTable_t3364835593 * get_notations_25() const { return ___notations_25; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_notations_25() { return &___notations_25; }
	inline void set_notations_25(XmlSchemaObjectTable_t3364835593 * value)
	{
		___notations_25 = value;
		Il2CppCodeGenWriteBarrier(&___notations_25, value);
	}

	inline static int32_t get_offset_of_identityConstraints_26() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___identityConstraints_26)); }
	inline XmlSchemaObjectTable_t3364835593 * get_identityConstraints_26() const { return ___identityConstraints_26; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_identityConstraints_26() { return &___identityConstraints_26; }
	inline void set_identityConstraints_26(XmlSchemaObjectTable_t3364835593 * value)
	{
		___identityConstraints_26 = value;
		Il2CppCodeGenWriteBarrier(&___identityConstraints_26, value);
	}

	inline static int32_t get_offset_of_schemaId_28() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___schemaId_28)); }
	inline int32_t get_schemaId_28() const { return ___schemaId_28; }
	inline int32_t* get_address_of_schemaId_28() { return &___schemaId_28; }
	inline void set_schemaId_28(int32_t value)
	{
		___schemaId_28 = value;
	}

	inline static int32_t get_offset_of_baseUri_29() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___baseUri_29)); }
	inline Uri_t19570940 * get_baseUri_29() const { return ___baseUri_29; }
	inline Uri_t19570940 ** get_address_of_baseUri_29() { return &___baseUri_29; }
	inline void set_baseUri_29(Uri_t19570940 * value)
	{
		___baseUri_29 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_29, value);
	}

	inline static int32_t get_offset_of_ids_30() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___ids_30)); }
	inline Hashtable_t909839986 * get_ids_30() const { return ___ids_30; }
	inline Hashtable_t909839986 ** get_address_of_ids_30() { return &___ids_30; }
	inline void set_ids_30(Hashtable_t909839986 * value)
	{
		___ids_30 = value;
		Il2CppCodeGenWriteBarrier(&___ids_30, value);
	}

	inline static int32_t get_offset_of_document_31() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___document_31)); }
	inline XmlDocument_t3649534162 * get_document_31() const { return ___document_31; }
	inline XmlDocument_t3649534162 ** get_address_of_document_31() { return &___document_31; }
	inline void set_document_31(XmlDocument_t3649534162 * value)
	{
		___document_31 = value;
		Il2CppCodeGenWriteBarrier(&___document_31, value);
	}
};

struct XmlSchema_t880472818_StaticFields
{
public:
	// System.Int32 System.Xml.Schema.XmlSchema::globalIdCounter
	int32_t ___globalIdCounter_27;

public:
	inline static int32_t get_offset_of_globalIdCounter_27() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818_StaticFields, ___globalIdCounter_27)); }
	inline int32_t get_globalIdCounter_27() const { return ___globalIdCounter_27; }
	inline int32_t* get_address_of_globalIdCounter_27() { return &___globalIdCounter_27; }
	inline void set_globalIdCounter_27(int32_t value)
	{
		___globalIdCounter_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
