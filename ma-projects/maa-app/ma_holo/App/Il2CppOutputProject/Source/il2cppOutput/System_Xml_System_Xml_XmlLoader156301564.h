﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlReader
struct XmlReader_t3675626668;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLoader
struct  XmlLoader_t156301564  : public Il2CppObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlLoader::doc
	XmlDocument_t3649534162 * ___doc_0;
	// System.Xml.XmlReader System.Xml.XmlLoader::reader
	XmlReader_t3675626668 * ___reader_1;
	// System.Boolean System.Xml.XmlLoader::preserveWhitespace
	bool ___preserveWhitespace_2;

public:
	inline static int32_t get_offset_of_doc_0() { return static_cast<int32_t>(offsetof(XmlLoader_t156301564, ___doc_0)); }
	inline XmlDocument_t3649534162 * get_doc_0() const { return ___doc_0; }
	inline XmlDocument_t3649534162 ** get_address_of_doc_0() { return &___doc_0; }
	inline void set_doc_0(XmlDocument_t3649534162 * value)
	{
		___doc_0 = value;
		Il2CppCodeGenWriteBarrier(&___doc_0, value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlLoader_t156301564, ___reader_1)); }
	inline XmlReader_t3675626668 * get_reader_1() const { return ___reader_1; }
	inline XmlReader_t3675626668 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlReader_t3675626668 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier(&___reader_1, value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_2() { return static_cast<int32_t>(offsetof(XmlLoader_t156301564, ___preserveWhitespace_2)); }
	inline bool get_preserveWhitespace_2() const { return ___preserveWhitespace_2; }
	inline bool* get_address_of_preserveWhitespace_2() { return &___preserveWhitespace_2; }
	inline void set_preserveWhitespace_2(bool value)
	{
		___preserveWhitespace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
