﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_CharEnumerator1926099410.h"
#include "mscorlib_System_CLSCompliantAttribute809966061.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_ArrayList_IListWrapper2368627560.h"
#include "mscorlib_System_Collections_ArrayList_SyncArrayList249867678.h"
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArra1065935995.h"
#include "mscorlib_System_Collections_ArrayList_ArrayListEnu1425333127.h"
#include "mscorlib_System_Collections_ArrayList_ArrayListDeb2931531541.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparer157661140.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCod2307530285.h"
#include "mscorlib_System_Collections_CollectionBase1101587467.h"
#include "mscorlib_System_Collections_Comparer3673668605.h"
#include "mscorlib_System_Collections_CompatibleComparer2934985531.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_ByteEqualityCo1095452717.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc1722175009.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_Hashtable_bucket976591655.h"
#include "mscorlib_System_Collections_Hashtable_KeyCollection145059300.h"
#include "mscorlib_System_Collections_Hashtable_ValueCollect3757008326.h"
#include "mscorlib_System_Collections_Hashtable_SyncHashtabl1343674558.h"
#include "mscorlib_System_Collections_Hashtable_HashtableEnu2328195885.h"
#include "mscorlib_System_Collections_Hashtable_HashtableDeb3834546805.h"
#include "mscorlib_System_Collections_HashHelpers2236981683.h"
#include "mscorlib_System_Collections_Queue1288490777.h"
#include "mscorlib_System_Collections_Queue_QueueEnumerator644509261.h"
#include "mscorlib_System_Collections_Queue_QueueDebugView3545126293.h"
#include "mscorlib_System_Collections_ReadOnlyCollectionBase22281769.h"
#include "mscorlib_System_Collections_SortedList3004938869.h"
#include "mscorlib_System_Collections_SortedList_SyncSortedLi465004772.h"
#include "mscorlib_System_Collections_SortedList_SortedListEn410146509.h"
#include "mscorlib_System_Collections_SortedList_KeyList1496669825.h"
#include "mscorlib_System_Collections_SortedList_ValueList1155029929.h"
#include "mscorlib_System_Collections_SortedList_SortedListD2499465029.h"
#include "mscorlib_System_Collections_Stack1043988394.h"
#include "mscorlib_System_Collections_Stack_StackEnumerator1617648877.h"
#include "mscorlib_System_Collections_Stack_StackDebugView2528224053.h"
#include "mscorlib_System_ConsoleCancelEventHandler1138054497.h"
#include "mscorlib_System_ConsoleCancelEventArgs2161883584.h"
#include "mscorlib_System_ConsoleColor1950027312.h"
#include "mscorlib_System_ConsoleKey1768883954.h"
#include "mscorlib_System_ConsoleKeyInfo3124575640.h"
#include "mscorlib_System_ConsoleModifiers118142373.h"
#include "mscorlib_System_ConsoleSpecialKey2502349621.h"
#include "mscorlib_System_ContextBoundObject4264702438.h"
#include "mscorlib_System_ContextStaticAttribute1079602319.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize100 = { sizeof (CharEnumerator_t1926099410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable100[3] = 
{
	CharEnumerator_t1926099410::get_offset_of_str_0(),
	CharEnumerator_t1926099410::get_offset_of_index_1(),
	CharEnumerator_t1926099410::get_offset_of_currentElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize101 = { sizeof (CLSCompliantAttribute_t809966061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable101[1] = 
{
	CLSCompliantAttribute_t809966061::get_offset_of_m_compliant_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize102 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable102[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize103 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable103[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable104[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize105 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable105[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize106 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable106[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize107 = { sizeof (ArrayList_t4252133567), -1, sizeof(ArrayList_t4252133567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable107[5] = 
{
	ArrayList_t4252133567::get_offset_of__items_0(),
	ArrayList_t4252133567::get_offset_of__size_1(),
	ArrayList_t4252133567::get_offset_of__version_2(),
	ArrayList_t4252133567::get_offset_of__syncRoot_3(),
	ArrayList_t4252133567_StaticFields::get_offset_of_emptyArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize108 = { sizeof (IListWrapper_t2368627560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable108[1] = 
{
	IListWrapper_t2368627560::get_offset_of__list_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize109 = { sizeof (SyncArrayList_t249867678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable109[2] = 
{
	SyncArrayList_t249867678::get_offset_of__list_5(),
	SyncArrayList_t249867678::get_offset_of__root_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize110 = { sizeof (ReadOnlyArrayList_t1065935995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable110[1] = 
{
	ReadOnlyArrayList_t1065935995::get_offset_of__list_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize111 = { sizeof (ArrayListEnumeratorSimple_t1425333127), -1, sizeof(ArrayListEnumeratorSimple_t1425333127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable111[6] = 
{
	ArrayListEnumeratorSimple_t1425333127::get_offset_of_list_0(),
	ArrayListEnumeratorSimple_t1425333127::get_offset_of_index_1(),
	ArrayListEnumeratorSimple_t1425333127::get_offset_of_version_2(),
	ArrayListEnumeratorSimple_t1425333127::get_offset_of_currentElement_3(),
	ArrayListEnumeratorSimple_t1425333127::get_offset_of_isArrayList_4(),
	ArrayListEnumeratorSimple_t1425333127_StaticFields::get_offset_of_dummyObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize112 = { sizeof (ArrayListDebugView_t2931531541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize113 = { sizeof (CaseInsensitiveComparer_t157661140), -1, sizeof(CaseInsensitiveComparer_t157661140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable113[2] = 
{
	CaseInsensitiveComparer_t157661140::get_offset_of_m_compareInfo_0(),
	CaseInsensitiveComparer_t157661140_StaticFields::get_offset_of_m_InvariantCaseInsensitiveComparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize114 = { sizeof (CaseInsensitiveHashCodeProvider_t2307530285), -1, sizeof(CaseInsensitiveHashCodeProvider_t2307530285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable114[2] = 
{
	CaseInsensitiveHashCodeProvider_t2307530285::get_offset_of_m_text_0(),
	CaseInsensitiveHashCodeProvider_t2307530285_StaticFields::get_offset_of_m_InvariantCaseInsensitiveHashCodeProvider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize115 = { sizeof (CollectionBase_t1101587467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable115[1] = 
{
	CollectionBase_t1101587467::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize116 = { sizeof (Comparer_t3673668605), -1, sizeof(Comparer_t3673668605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable116[3] = 
{
	Comparer_t3673668605::get_offset_of_m_compareInfo_0(),
	Comparer_t3673668605_StaticFields::get_offset_of_Default_1(),
	Comparer_t3673668605_StaticFields::get_offset_of_DefaultInvariant_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize117 = { sizeof (CompatibleComparer_t2934985531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable117[2] = 
{
	CompatibleComparer_t2934985531::get_offset_of__comparer_0(),
	CompatibleComparer_t2934985531::get_offset_of__hcp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize118 = { sizeof (DictionaryEntry_t3048875398)+ sizeof (Il2CppObject), sizeof(DictionaryEntry_t3048875398_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable118[2] = 
{
	DictionaryEntry_t3048875398::get_offset_of__key_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DictionaryEntry_t3048875398::get_offset_of__value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize119 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable119[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize120 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize121 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize122 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize123 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize124 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize125 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize126 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable127[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize128 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable128[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize129 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable129[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize130 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable130[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize131 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable131[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize132 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable132[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable133[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize134 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable134[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize135 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize136 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize137 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize138 = { sizeof (ByteEqualityComparer_t1095452717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize139 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize140 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize141 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize142 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize143 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize144 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize145 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize146 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize147 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize148 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize149 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize150 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize151 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize152 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize153 = { sizeof (KeyNotFoundException_t1722175009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize154 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable154[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize155 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable155[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize156 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable156[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize157 = { sizeof (Hashtable_t909839986), -1, sizeof(Hashtable_t909839986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable157[12] = 
{
	Hashtable_t909839986::get_offset_of_buckets_0(),
	Hashtable_t909839986::get_offset_of_count_1(),
	Hashtable_t909839986::get_offset_of_occupancy_2(),
	Hashtable_t909839986::get_offset_of_loadsize_3(),
	Hashtable_t909839986::get_offset_of_loadFactor_4(),
	Hashtable_t909839986::get_offset_of_version_5(),
	Hashtable_t909839986::get_offset_of_isWriterInProgress_6(),
	Hashtable_t909839986::get_offset_of_keys_7(),
	Hashtable_t909839986::get_offset_of_values_8(),
	Hashtable_t909839986::get_offset_of__keycomparer_9(),
	Hashtable_t909839986::get_offset_of__syncRoot_10(),
	Hashtable_t909839986_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize158 = { sizeof (bucket_t976591655)+ sizeof (Il2CppObject), sizeof(bucket_t976591655_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable158[3] = 
{
	bucket_t976591655::get_offset_of_key_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	bucket_t976591655::get_offset_of_val_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	bucket_t976591655::get_offset_of_hash_coll_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize159 = { sizeof (KeyCollection_t145059300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable159[1] = 
{
	KeyCollection_t145059300::get_offset_of__hashtable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize160 = { sizeof (ValueCollection_t3757008326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable160[1] = 
{
	ValueCollection_t3757008326::get_offset_of__hashtable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize161 = { sizeof (SyncHashtable_t1343674558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable161[1] = 
{
	SyncHashtable_t1343674558::get_offset_of__table_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize162 = { sizeof (HashtableEnumerator_t2328195885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable162[7] = 
{
	HashtableEnumerator_t2328195885::get_offset_of_hashtable_0(),
	HashtableEnumerator_t2328195885::get_offset_of_bucket_1(),
	HashtableEnumerator_t2328195885::get_offset_of_version_2(),
	HashtableEnumerator_t2328195885::get_offset_of_current_3(),
	HashtableEnumerator_t2328195885::get_offset_of_getObjectRetType_4(),
	HashtableEnumerator_t2328195885::get_offset_of_currentKey_5(),
	HashtableEnumerator_t2328195885::get_offset_of_currentValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize163 = { sizeof (HashtableDebugView_t3834546805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize164 = { sizeof (HashHelpers_t2236981683), -1, sizeof(HashHelpers_t2236981683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable164[2] = 
{
	HashHelpers_t2236981683_StaticFields::get_offset_of_primes_0(),
	HashHelpers_t2236981683_StaticFields::get_offset_of_s_SerializationInfoTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize165 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize166 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize167 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize168 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize169 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize172 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize173 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize174 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize176 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable176[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize177 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable177[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize178 = { sizeof (Queue_t1288490777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable178[7] = 
{
	Queue_t1288490777::get_offset_of__array_0(),
	Queue_t1288490777::get_offset_of__head_1(),
	Queue_t1288490777::get_offset_of__tail_2(),
	Queue_t1288490777::get_offset_of__size_3(),
	Queue_t1288490777::get_offset_of__growFactor_4(),
	Queue_t1288490777::get_offset_of__version_5(),
	Queue_t1288490777::get_offset_of__syncRoot_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize179 = { sizeof (QueueEnumerator_t644509261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable179[4] = 
{
	QueueEnumerator_t644509261::get_offset_of__q_0(),
	QueueEnumerator_t644509261::get_offset_of__index_1(),
	QueueEnumerator_t644509261::get_offset_of__version_2(),
	QueueEnumerator_t644509261::get_offset_of_currentElement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize180 = { sizeof (QueueDebugView_t3545126293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize181 = { sizeof (ReadOnlyCollectionBase_t22281769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable181[1] = 
{
	ReadOnlyCollectionBase_t22281769::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize182 = { sizeof (SortedList_t3004938869), -1, sizeof(SortedList_t3004938869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable182[9] = 
{
	SortedList_t3004938869::get_offset_of_keys_0(),
	SortedList_t3004938869::get_offset_of_values_1(),
	SortedList_t3004938869::get_offset_of__size_2(),
	SortedList_t3004938869::get_offset_of_version_3(),
	SortedList_t3004938869::get_offset_of_comparer_4(),
	SortedList_t3004938869::get_offset_of_keyList_5(),
	SortedList_t3004938869::get_offset_of_valueList_6(),
	SortedList_t3004938869::get_offset_of__syncRoot_7(),
	SortedList_t3004938869_StaticFields::get_offset_of_emptyArray_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize183 = { sizeof (SyncSortedList_t465004772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable183[2] = 
{
	SyncSortedList_t465004772::get_offset_of__list_9(),
	SyncSortedList_t465004772::get_offset_of__root_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize184 = { sizeof (SortedListEnumerator_t410146509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable184[9] = 
{
	SortedListEnumerator_t410146509::get_offset_of_sortedList_0(),
	SortedListEnumerator_t410146509::get_offset_of_key_1(),
	SortedListEnumerator_t410146509::get_offset_of_value_2(),
	SortedListEnumerator_t410146509::get_offset_of_index_3(),
	SortedListEnumerator_t410146509::get_offset_of_startIndex_4(),
	SortedListEnumerator_t410146509::get_offset_of_endIndex_5(),
	SortedListEnumerator_t410146509::get_offset_of_version_6(),
	SortedListEnumerator_t410146509::get_offset_of_current_7(),
	SortedListEnumerator_t410146509::get_offset_of_getObjectRetType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize185 = { sizeof (KeyList_t1496669825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable185[1] = 
{
	KeyList_t1496669825::get_offset_of_sortedList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize186 = { sizeof (ValueList_t1155029929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable186[1] = 
{
	ValueList_t1155029929::get_offset_of_sortedList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize187 = { sizeof (SortedListDebugView_t2499465029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize188 = { sizeof (Stack_t1043988394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable188[4] = 
{
	Stack_t1043988394::get_offset_of__array_0(),
	Stack_t1043988394::get_offset_of__size_1(),
	Stack_t1043988394::get_offset_of__version_2(),
	Stack_t1043988394::get_offset_of__syncRoot_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize189 = { sizeof (StackEnumerator_t1617648877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable189[4] = 
{
	StackEnumerator_t1617648877::get_offset_of__stack_0(),
	StackEnumerator_t1617648877::get_offset_of__index_1(),
	StackEnumerator_t1617648877::get_offset_of__version_2(),
	StackEnumerator_t1617648877::get_offset_of_currentElement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize190 = { sizeof (StackDebugView_t2528224053), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize191 = { sizeof (ConsoleCancelEventHandler_t1138054497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize192 = { sizeof (ConsoleCancelEventArgs_t2161883584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable192[2] = 
{
	ConsoleCancelEventArgs_t2161883584::get_offset_of__type_1(),
	ConsoleCancelEventArgs_t2161883584::get_offset_of__cancel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize193 = { sizeof (ConsoleColor_t1950027312)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable193[17] = 
{
	ConsoleColor_t1950027312::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize194 = { sizeof (ConsoleKey_t1768883954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable194[145] = 
{
	ConsoleKey_t1768883954::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize195 = { sizeof (ConsoleKeyInfo_t3124575640)+ sizeof (Il2CppObject), sizeof(ConsoleKeyInfo_t3124575640_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable195[3] = 
{
	ConsoleKeyInfo_t3124575640::get_offset_of__keyChar_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleKeyInfo_t3124575640::get_offset_of__key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConsoleKeyInfo_t3124575640::get_offset_of__mods_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize196 = { sizeof (ConsoleModifiers_t118142373)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable196[4] = 
{
	ConsoleModifiers_t118142373::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize197 = { sizeof (ConsoleSpecialKey_t2502349621)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable197[3] = 
{
	ConsoleSpecialKey_t2502349621::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize198 = { sizeof (ContextBoundObject_t4264702438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize199 = { sizeof (ContextStaticAttribute_t1079602319), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
