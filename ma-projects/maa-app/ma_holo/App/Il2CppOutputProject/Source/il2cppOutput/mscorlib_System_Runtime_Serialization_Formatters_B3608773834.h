﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"

// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Double[]
struct DoubleU5BU5D_t1889952540;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.SByte[]
struct SByteU5BU5D_t3472287392;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.UInt16[]
struct UInt16U5BU5D_t2527266722;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.UInt64[]
struct UInt64U5BU5D_t1668688775;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.PrimitiveArray
struct  PrimitiveArray_t3608773834  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::code
	int32_t ___code_0;
	// System.Boolean[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::booleanA
	BooleanU5BU5D_t3568034315* ___booleanA_1;
	// System.Char[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::charA
	CharU5BU5D_t1328083999* ___charA_2;
	// System.Double[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::doubleA
	DoubleU5BU5D_t1889952540* ___doubleA_3;
	// System.Int16[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::int16A
	Int16U5BU5D_t3104283263* ___int16A_4;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::int32A
	Int32U5BU5D_t3030399641* ___int32A_5;
	// System.Int64[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::int64A
	Int64U5BU5D_t717125112* ___int64A_6;
	// System.SByte[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::sbyteA
	SByteU5BU5D_t3472287392* ___sbyteA_7;
	// System.Single[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::singleA
	SingleU5BU5D_t577127397* ___singleA_8;
	// System.UInt16[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::uint16A
	UInt16U5BU5D_t2527266722* ___uint16A_9;
	// System.UInt32[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::uint32A
	UInt32U5BU5D_t59386216* ___uint32A_10;
	// System.UInt64[] System.Runtime.Serialization.Formatters.Binary.PrimitiveArray::uint64A
	UInt64U5BU5D_t1668688775* ___uint64A_11;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_booleanA_1() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___booleanA_1)); }
	inline BooleanU5BU5D_t3568034315* get_booleanA_1() const { return ___booleanA_1; }
	inline BooleanU5BU5D_t3568034315** get_address_of_booleanA_1() { return &___booleanA_1; }
	inline void set_booleanA_1(BooleanU5BU5D_t3568034315* value)
	{
		___booleanA_1 = value;
		Il2CppCodeGenWriteBarrier(&___booleanA_1, value);
	}

	inline static int32_t get_offset_of_charA_2() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___charA_2)); }
	inline CharU5BU5D_t1328083999* get_charA_2() const { return ___charA_2; }
	inline CharU5BU5D_t1328083999** get_address_of_charA_2() { return &___charA_2; }
	inline void set_charA_2(CharU5BU5D_t1328083999* value)
	{
		___charA_2 = value;
		Il2CppCodeGenWriteBarrier(&___charA_2, value);
	}

	inline static int32_t get_offset_of_doubleA_3() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___doubleA_3)); }
	inline DoubleU5BU5D_t1889952540* get_doubleA_3() const { return ___doubleA_3; }
	inline DoubleU5BU5D_t1889952540** get_address_of_doubleA_3() { return &___doubleA_3; }
	inline void set_doubleA_3(DoubleU5BU5D_t1889952540* value)
	{
		___doubleA_3 = value;
		Il2CppCodeGenWriteBarrier(&___doubleA_3, value);
	}

	inline static int32_t get_offset_of_int16A_4() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___int16A_4)); }
	inline Int16U5BU5D_t3104283263* get_int16A_4() const { return ___int16A_4; }
	inline Int16U5BU5D_t3104283263** get_address_of_int16A_4() { return &___int16A_4; }
	inline void set_int16A_4(Int16U5BU5D_t3104283263* value)
	{
		___int16A_4 = value;
		Il2CppCodeGenWriteBarrier(&___int16A_4, value);
	}

	inline static int32_t get_offset_of_int32A_5() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___int32A_5)); }
	inline Int32U5BU5D_t3030399641* get_int32A_5() const { return ___int32A_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_int32A_5() { return &___int32A_5; }
	inline void set_int32A_5(Int32U5BU5D_t3030399641* value)
	{
		___int32A_5 = value;
		Il2CppCodeGenWriteBarrier(&___int32A_5, value);
	}

	inline static int32_t get_offset_of_int64A_6() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___int64A_6)); }
	inline Int64U5BU5D_t717125112* get_int64A_6() const { return ___int64A_6; }
	inline Int64U5BU5D_t717125112** get_address_of_int64A_6() { return &___int64A_6; }
	inline void set_int64A_6(Int64U5BU5D_t717125112* value)
	{
		___int64A_6 = value;
		Il2CppCodeGenWriteBarrier(&___int64A_6, value);
	}

	inline static int32_t get_offset_of_sbyteA_7() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___sbyteA_7)); }
	inline SByteU5BU5D_t3472287392* get_sbyteA_7() const { return ___sbyteA_7; }
	inline SByteU5BU5D_t3472287392** get_address_of_sbyteA_7() { return &___sbyteA_7; }
	inline void set_sbyteA_7(SByteU5BU5D_t3472287392* value)
	{
		___sbyteA_7 = value;
		Il2CppCodeGenWriteBarrier(&___sbyteA_7, value);
	}

	inline static int32_t get_offset_of_singleA_8() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___singleA_8)); }
	inline SingleU5BU5D_t577127397* get_singleA_8() const { return ___singleA_8; }
	inline SingleU5BU5D_t577127397** get_address_of_singleA_8() { return &___singleA_8; }
	inline void set_singleA_8(SingleU5BU5D_t577127397* value)
	{
		___singleA_8 = value;
		Il2CppCodeGenWriteBarrier(&___singleA_8, value);
	}

	inline static int32_t get_offset_of_uint16A_9() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___uint16A_9)); }
	inline UInt16U5BU5D_t2527266722* get_uint16A_9() const { return ___uint16A_9; }
	inline UInt16U5BU5D_t2527266722** get_address_of_uint16A_9() { return &___uint16A_9; }
	inline void set_uint16A_9(UInt16U5BU5D_t2527266722* value)
	{
		___uint16A_9 = value;
		Il2CppCodeGenWriteBarrier(&___uint16A_9, value);
	}

	inline static int32_t get_offset_of_uint32A_10() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___uint32A_10)); }
	inline UInt32U5BU5D_t59386216* get_uint32A_10() const { return ___uint32A_10; }
	inline UInt32U5BU5D_t59386216** get_address_of_uint32A_10() { return &___uint32A_10; }
	inline void set_uint32A_10(UInt32U5BU5D_t59386216* value)
	{
		___uint32A_10 = value;
		Il2CppCodeGenWriteBarrier(&___uint32A_10, value);
	}

	inline static int32_t get_offset_of_uint64A_11() { return static_cast<int32_t>(offsetof(PrimitiveArray_t3608773834, ___uint64A_11)); }
	inline UInt64U5BU5D_t1668688775* get_uint64A_11() const { return ___uint64A_11; }
	inline UInt64U5BU5D_t1668688775** get_address_of_uint64A_11() { return &___uint64A_11; }
	inline void set_uint64A_11(UInt64U5BU5D_t1668688775* value)
	{
		___uint64A_11 = value;
		Il2CppCodeGenWriteBarrier(&___uint64A_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
