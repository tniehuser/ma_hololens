﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Collections.Hashtable/bucket[]
struct bucketU5BU5D_t2149607518;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable
struct  Hashtable_t909839986  : public Il2CppObject
{
public:
	// System.Collections.Hashtable/bucket[] System.Collections.Hashtable::buckets
	bucketU5BU5D_t2149607518* ___buckets_0;
	// System.Int32 System.Collections.Hashtable::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Hashtable::occupancy
	int32_t ___occupancy_2;
	// System.Int32 System.Collections.Hashtable::loadsize
	int32_t ___loadsize_3;
	// System.Single System.Collections.Hashtable::loadFactor
	float ___loadFactor_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::version
	int32_t ___version_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::isWriterInProgress
	bool ___isWriterInProgress_6;
	// System.Collections.ICollection System.Collections.Hashtable::keys
	Il2CppObject * ___keys_7;
	// System.Collections.ICollection System.Collections.Hashtable::values
	Il2CppObject * ___values_8;
	// System.Collections.IEqualityComparer System.Collections.Hashtable::_keycomparer
	Il2CppObject * ____keycomparer_9;
	// System.Object System.Collections.Hashtable::_syncRoot
	Il2CppObject * ____syncRoot_10;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___buckets_0)); }
	inline bucketU5BU5D_t2149607518* get_buckets_0() const { return ___buckets_0; }
	inline bucketU5BU5D_t2149607518** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(bucketU5BU5D_t2149607518* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier(&___buckets_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_occupancy_2() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___occupancy_2)); }
	inline int32_t get_occupancy_2() const { return ___occupancy_2; }
	inline int32_t* get_address_of_occupancy_2() { return &___occupancy_2; }
	inline void set_occupancy_2(int32_t value)
	{
		___occupancy_2 = value;
	}

	inline static int32_t get_offset_of_loadsize_3() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___loadsize_3)); }
	inline int32_t get_loadsize_3() const { return ___loadsize_3; }
	inline int32_t* get_address_of_loadsize_3() { return &___loadsize_3; }
	inline void set_loadsize_3(int32_t value)
	{
		___loadsize_3 = value;
	}

	inline static int32_t get_offset_of_loadFactor_4() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___loadFactor_4)); }
	inline float get_loadFactor_4() const { return ___loadFactor_4; }
	inline float* get_address_of_loadFactor_4() { return &___loadFactor_4; }
	inline void set_loadFactor_4(float value)
	{
		___loadFactor_4 = value;
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___version_5)); }
	inline int32_t get_version_5() const { return ___version_5; }
	inline int32_t* get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(int32_t value)
	{
		___version_5 = value;
	}

	inline static int32_t get_offset_of_isWriterInProgress_6() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___isWriterInProgress_6)); }
	inline bool get_isWriterInProgress_6() const { return ___isWriterInProgress_6; }
	inline bool* get_address_of_isWriterInProgress_6() { return &___isWriterInProgress_6; }
	inline void set_isWriterInProgress_6(bool value)
	{
		___isWriterInProgress_6 = value;
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___keys_7)); }
	inline Il2CppObject * get_keys_7() const { return ___keys_7; }
	inline Il2CppObject ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(Il2CppObject * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier(&___keys_7, value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ___values_8)); }
	inline Il2CppObject * get_values_8() const { return ___values_8; }
	inline Il2CppObject ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(Il2CppObject * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier(&___values_8, value);
	}

	inline static int32_t get_offset_of__keycomparer_9() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ____keycomparer_9)); }
	inline Il2CppObject * get__keycomparer_9() const { return ____keycomparer_9; }
	inline Il2CppObject ** get_address_of__keycomparer_9() { return &____keycomparer_9; }
	inline void set__keycomparer_9(Il2CppObject * value)
	{
		____keycomparer_9 = value;
		Il2CppCodeGenWriteBarrier(&____keycomparer_9, value);
	}

	inline static int32_t get_offset_of__syncRoot_10() { return static_cast<int32_t>(offsetof(Hashtable_t909839986, ____syncRoot_10)); }
	inline Il2CppObject * get__syncRoot_10() const { return ____syncRoot_10; }
	inline Il2CppObject ** get_address_of__syncRoot_10() { return &____syncRoot_10; }
	inline void set__syncRoot_10(Il2CppObject * value)
	{
		____syncRoot_10 = value;
		Il2CppCodeGenWriteBarrier(&____syncRoot_10, value);
	}
};

struct Hashtable_t909839986_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Collections.Hashtable::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_11() { return static_cast<int32_t>(offsetof(Hashtable_t909839986_StaticFields, ___U3CU3Ef__switchU24map4_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_11() const { return ___U3CU3Ef__switchU24map4_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_11() { return &___U3CU3Ef__switchU24map4_11; }
	inline void set_U3CU3Ef__switchU24map4_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
