﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SpinWait
struct  SpinWait_t3991132955 
{
public:
	// System.Int32 System.Threading.SpinWait::m_count
	int32_t ___m_count_0;

public:
	inline static int32_t get_offset_of_m_count_0() { return static_cast<int32_t>(offsetof(SpinWait_t3991132955, ___m_count_0)); }
	inline int32_t get_m_count_0() const { return ___m_count_0; }
	inline int32_t* get_address_of_m_count_0() { return &___m_count_0; }
	inline void set_m_count_0(int32_t value)
	{
		___m_count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
