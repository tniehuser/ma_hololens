﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.Xml.XmlName
struct XmlName_t3016058992;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t175731005  : public XmlNode_t616554813
{
public:
	// System.Xml.XmlName System.Xml.XmlAttribute::name
	XmlName_t3016058992 * ___name_1;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastChild
	XmlLinkedNode_t1287616130 * ___lastChild_2;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___name_1)); }
	inline XmlName_t3016058992 * get_name_1() const { return ___name_1; }
	inline XmlName_t3016058992 ** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(XmlName_t3016058992 * value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_lastChild_2() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___lastChild_2)); }
	inline XmlLinkedNode_t1287616130 * get_lastChild_2() const { return ___lastChild_2; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastChild_2() { return &___lastChild_2; }
	inline void set_lastChild_2(XmlLinkedNode_t1287616130 * value)
	{
		___lastChild_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastChild_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
