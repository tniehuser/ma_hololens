﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_TimerThread_Queue1332364135.h"

// System.Net.TimerThread/TimerNode
struct TimerNode_t3034660285;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread/TimerQueue
struct  TimerQueue_t385709908  : public Queue_t1332364135
{
public:
	// System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerQueue::m_Timers
	TimerNode_t3034660285 * ___m_Timers_1;

public:
	inline static int32_t get_offset_of_m_Timers_1() { return static_cast<int32_t>(offsetof(TimerQueue_t385709908, ___m_Timers_1)); }
	inline TimerNode_t3034660285 * get_m_Timers_1() const { return ___m_Timers_1; }
	inline TimerNode_t3034660285 ** get_address_of_m_Timers_1() { return &___m_Timers_1; }
	inline void set_m_Timers_1(TimerNode_t3034660285 * value)
	{
		___m_Timers_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Timers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
