﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_Linq_Enumerable_Iterator_1_gen2759310362.h"

// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t2583633095;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t2919696197;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerator_1_t4061997173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  WhereEnumerableIterator_1_t2810300109  : public Iterator_1_t2759310362
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	Il2CppObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t2919696197 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	Il2CppObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2810300109, ___source_3)); }
	inline Il2CppObject* get_source_3() const { return ___source_3; }
	inline Il2CppObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(Il2CppObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier(&___source_3, value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2810300109, ___predicate_4)); }
	inline Func_2_t2919696197 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2919696197 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2919696197 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_4, value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t2810300109, ___enumerator_5)); }
	inline Il2CppObject* get_enumerator_5() const { return ___enumerator_5; }
	inline Il2CppObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(Il2CppObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier(&___enumerator_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
