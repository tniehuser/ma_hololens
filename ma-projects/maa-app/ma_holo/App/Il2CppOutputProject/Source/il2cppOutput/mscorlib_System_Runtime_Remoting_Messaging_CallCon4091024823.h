﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Principal.IPrincipal
struct IPrincipal_t783141777;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Remoting.Messaging.CallContextSecurityData
struct  CallContextSecurityData_t4091024823  : public Il2CppObject
{
public:
	// System.Security.Principal.IPrincipal System.Runtime.Remoting.Messaging.CallContextSecurityData::_principal
	Il2CppObject * ____principal_0;

public:
	inline static int32_t get_offset_of__principal_0() { return static_cast<int32_t>(offsetof(CallContextSecurityData_t4091024823, ____principal_0)); }
	inline Il2CppObject * get__principal_0() const { return ____principal_0; }
	inline Il2CppObject ** get_address_of__principal_0() { return &____principal_0; }
	inline void set__principal_0(Il2CppObject * value)
	{
		____principal_0 = value;
		Il2CppCodeGenWriteBarrier(&____principal_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
