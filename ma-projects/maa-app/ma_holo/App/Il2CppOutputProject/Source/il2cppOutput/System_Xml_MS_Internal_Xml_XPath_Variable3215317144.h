﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Variable
struct  Variable_t3215317144  : public AstNode_t2002670936
{
public:
	// System.String MS.Internal.Xml.XPath.Variable::localname
	String_t* ___localname_0;
	// System.String MS.Internal.Xml.XPath.Variable::prefix
	String_t* ___prefix_1;

public:
	inline static int32_t get_offset_of_localname_0() { return static_cast<int32_t>(offsetof(Variable_t3215317144, ___localname_0)); }
	inline String_t* get_localname_0() const { return ___localname_0; }
	inline String_t** get_address_of_localname_0() { return &___localname_0; }
	inline void set_localname_0(String_t* value)
	{
		___localname_0 = value;
		Il2CppCodeGenWriteBarrier(&___localname_0, value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(Variable_t3215317144, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
