﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_Runtime_CompilerServices_AsyncTask2408546353.h"
#include "mscorlib_System_Runtime_CompilerServices_Configure2759266955.h"
#include "mscorlib_System_Runtime_CompilerServices_Configured446638270.h"

// System.Threading.SemaphoreSlim/TaskNode
struct TaskNode_t453295914;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t1795361321;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>
struct Task_1_t963265114;
// System.Object
struct Il2CppObject;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t461808439;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0
struct  U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442 
{
public:
	// System.Threading.SemaphoreSlim/TaskNode System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::asyncWaiter
	TaskNode_t453295914 * ___asyncWaiter_0;
	// System.Threading.CancellationToken System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::cancellationToken
	CancellationToken_t1851405782  ___cancellationToken_1;
	// System.Threading.CancellationTokenSource System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::<cts>__1
	CancellationTokenSource_t1795361321 * ___U3CctsU3E__1_2;
	// System.Int32 System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::millisecondsTimeout
	int32_t ___millisecondsTimeout_3;
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.Task> System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::<waitCompleted>__2
	Task_1_t963265114 * ___U3CwaitCompletedU3E__2_4;
	// System.Object System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$locvar0
	Il2CppObject * ___U24locvar0_5;
	// System.Boolean System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$locvar1
	bool ___U24locvar1_6;
	// System.Threading.SemaphoreSlim System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$this
	SemaphoreSlim_t461808439 * ___U24this_7;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Boolean> System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$builder
	AsyncTaskMethodBuilder_1_t2408546353  ___U24builder_8;
	// System.Int32 System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$PC
	int32_t ___U24PC_9;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Threading.Tasks.Task> System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$awaiter0
	ConfiguredTaskAwaiter_t2759266955  ___U24awaiter0_10;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Boolean> System.Threading.SemaphoreSlim/<WaitUntilCountOrTimeoutAsync>c__async0::$awaiter1
	ConfiguredTaskAwaiter_t446638270  ___U24awaiter1_11;

public:
	inline static int32_t get_offset_of_asyncWaiter_0() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___asyncWaiter_0)); }
	inline TaskNode_t453295914 * get_asyncWaiter_0() const { return ___asyncWaiter_0; }
	inline TaskNode_t453295914 ** get_address_of_asyncWaiter_0() { return &___asyncWaiter_0; }
	inline void set_asyncWaiter_0(TaskNode_t453295914 * value)
	{
		___asyncWaiter_0 = value;
		Il2CppCodeGenWriteBarrier(&___asyncWaiter_0, value);
	}

	inline static int32_t get_offset_of_cancellationToken_1() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___cancellationToken_1)); }
	inline CancellationToken_t1851405782  get_cancellationToken_1() const { return ___cancellationToken_1; }
	inline CancellationToken_t1851405782 * get_address_of_cancellationToken_1() { return &___cancellationToken_1; }
	inline void set_cancellationToken_1(CancellationToken_t1851405782  value)
	{
		___cancellationToken_1 = value;
	}

	inline static int32_t get_offset_of_U3CctsU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U3CctsU3E__1_2)); }
	inline CancellationTokenSource_t1795361321 * get_U3CctsU3E__1_2() const { return ___U3CctsU3E__1_2; }
	inline CancellationTokenSource_t1795361321 ** get_address_of_U3CctsU3E__1_2() { return &___U3CctsU3E__1_2; }
	inline void set_U3CctsU3E__1_2(CancellationTokenSource_t1795361321 * value)
	{
		___U3CctsU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CctsU3E__1_2, value);
	}

	inline static int32_t get_offset_of_millisecondsTimeout_3() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___millisecondsTimeout_3)); }
	inline int32_t get_millisecondsTimeout_3() const { return ___millisecondsTimeout_3; }
	inline int32_t* get_address_of_millisecondsTimeout_3() { return &___millisecondsTimeout_3; }
	inline void set_millisecondsTimeout_3(int32_t value)
	{
		___millisecondsTimeout_3 = value;
	}

	inline static int32_t get_offset_of_U3CwaitCompletedU3E__2_4() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U3CwaitCompletedU3E__2_4)); }
	inline Task_1_t963265114 * get_U3CwaitCompletedU3E__2_4() const { return ___U3CwaitCompletedU3E__2_4; }
	inline Task_1_t963265114 ** get_address_of_U3CwaitCompletedU3E__2_4() { return &___U3CwaitCompletedU3E__2_4; }
	inline void set_U3CwaitCompletedU3E__2_4(Task_1_t963265114 * value)
	{
		___U3CwaitCompletedU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwaitCompletedU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24locvar0_5() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24locvar0_5)); }
	inline Il2CppObject * get_U24locvar0_5() const { return ___U24locvar0_5; }
	inline Il2CppObject ** get_address_of_U24locvar0_5() { return &___U24locvar0_5; }
	inline void set_U24locvar0_5(Il2CppObject * value)
	{
		___U24locvar0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_5, value);
	}

	inline static int32_t get_offset_of_U24locvar1_6() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24locvar1_6)); }
	inline bool get_U24locvar1_6() const { return ___U24locvar1_6; }
	inline bool* get_address_of_U24locvar1_6() { return &___U24locvar1_6; }
	inline void set_U24locvar1_6(bool value)
	{
		___U24locvar1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24this_7)); }
	inline SemaphoreSlim_t461808439 * get_U24this_7() const { return ___U24this_7; }
	inline SemaphoreSlim_t461808439 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(SemaphoreSlim_t461808439 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24builder_8() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24builder_8)); }
	inline AsyncTaskMethodBuilder_1_t2408546353  get_U24builder_8() const { return ___U24builder_8; }
	inline AsyncTaskMethodBuilder_1_t2408546353 * get_address_of_U24builder_8() { return &___U24builder_8; }
	inline void set_U24builder_8(AsyncTaskMethodBuilder_1_t2408546353  value)
	{
		___U24builder_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24awaiter0_10() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24awaiter0_10)); }
	inline ConfiguredTaskAwaiter_t2759266955  get_U24awaiter0_10() const { return ___U24awaiter0_10; }
	inline ConfiguredTaskAwaiter_t2759266955 * get_address_of_U24awaiter0_10() { return &___U24awaiter0_10; }
	inline void set_U24awaiter0_10(ConfiguredTaskAwaiter_t2759266955  value)
	{
		___U24awaiter0_10 = value;
	}

	inline static int32_t get_offset_of_U24awaiter1_11() { return static_cast<int32_t>(offsetof(U3CWaitUntilCountOrTimeoutAsyncU3Ec__async0_t2777167442, ___U24awaiter1_11)); }
	inline ConfiguredTaskAwaiter_t446638270  get_U24awaiter1_11() const { return ___U24awaiter1_11; }
	inline ConfiguredTaskAwaiter_t446638270 * get_address_of_U24awaiter1_11() { return &___U24awaiter1_11; }
	inline void set_U24awaiter1_11(ConfiguredTaskAwaiter_t446638270  value)
	{
		___U24awaiter1_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
