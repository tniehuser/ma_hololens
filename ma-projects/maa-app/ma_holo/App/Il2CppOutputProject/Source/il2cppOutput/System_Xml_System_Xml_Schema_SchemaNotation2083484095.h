﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNotation
struct  SchemaNotation_t2083484095  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNotation::name
	XmlQualifiedName_t1944712516 * ___name_0;
	// System.String System.Xml.Schema.SchemaNotation::systemLiteral
	String_t* ___systemLiteral_1;
	// System.String System.Xml.Schema.SchemaNotation::pubid
	String_t* ___pubid_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaNotation_t2083484095, ___name_0)); }
	inline XmlQualifiedName_t1944712516 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t1944712516 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_systemLiteral_1() { return static_cast<int32_t>(offsetof(SchemaNotation_t2083484095, ___systemLiteral_1)); }
	inline String_t* get_systemLiteral_1() const { return ___systemLiteral_1; }
	inline String_t** get_address_of_systemLiteral_1() { return &___systemLiteral_1; }
	inline void set_systemLiteral_1(String_t* value)
	{
		___systemLiteral_1 = value;
		Il2CppCodeGenWriteBarrier(&___systemLiteral_1, value);
	}

	inline static int32_t get_offset_of_pubid_2() { return static_cast<int32_t>(offsetof(SchemaNotation_t2083484095, ___pubid_2)); }
	inline String_t* get_pubid_2() const { return ___pubid_2; }
	inline String_t** get_address_of_pubid_2() { return &___pubid_2; }
	inline void set_pubid_2(String_t* value)
	{
		___pubid_2 = value;
		Il2CppCodeGenWriteBarrier(&___pubid_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
