﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"

// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t3055722314;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t3055722314 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t1328083999* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t1328083999* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t3030399641* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t3055722314 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t3055722314 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t3055722314 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_0, value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t1328083999* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t1328083999** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t1328083999* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier(&___NodeTypeLetter_1, value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t1328083999* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t1328083999** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t1328083999* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier(&___UniqueIdTbl_2, value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t3030399641* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t3030399641* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier(&___ContentKindMasks_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
