﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_FieldInfo255040150.h"

// System.Reflection.RuntimeFieldInfo
struct RuntimeFieldInfo_t1687134186;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationFieldInfo
struct  SerializationFieldInfo_t2472586292  : public FieldInfo_t
{
public:
	// System.Reflection.RuntimeFieldInfo System.Runtime.Serialization.SerializationFieldInfo::m_field
	RuntimeFieldInfo_t1687134186 * ___m_field_0;
	// System.String System.Runtime.Serialization.SerializationFieldInfo::m_serializationName
	String_t* ___m_serializationName_1;

public:
	inline static int32_t get_offset_of_m_field_0() { return static_cast<int32_t>(offsetof(SerializationFieldInfo_t2472586292, ___m_field_0)); }
	inline RuntimeFieldInfo_t1687134186 * get_m_field_0() const { return ___m_field_0; }
	inline RuntimeFieldInfo_t1687134186 ** get_address_of_m_field_0() { return &___m_field_0; }
	inline void set_m_field_0(RuntimeFieldInfo_t1687134186 * value)
	{
		___m_field_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_field_0, value);
	}

	inline static int32_t get_offset_of_m_serializationName_1() { return static_cast<int32_t>(offsetof(SerializationFieldInfo_t2472586292, ___m_serializationName_1)); }
	inline String_t* get_m_serializationName_1() const { return ___m_serializationName_1; }
	inline String_t** get_address_of_m_serializationName_1() { return &___m_serializationName_1; }
	inline void set_m_serializationName_1(String_t* value)
	{
		___m_serializationName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_serializationName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
