﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Empty
struct Empty_t3498280455;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Empty
struct  Empty_t3498280455  : public Il2CppObject
{
public:

public:
};

struct Empty_t3498280455_StaticFields
{
public:
	// System.Empty System.Empty::Value
	Empty_t3498280455 * ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Empty_t3498280455_StaticFields, ___Value_0)); }
	inline Empty_t3498280455 * get_Value_0() const { return ___Value_0; }
	inline Empty_t3498280455 ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Empty_t3498280455 * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
