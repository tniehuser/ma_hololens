﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t3122949129;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdParserProxy
struct  DtdParserProxy_t633814591  : public Il2CppObject
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReaderImpl/DtdParserProxy::reader
	XmlTextReaderImpl_t3122949129 * ___reader_0;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(DtdParserProxy_t633814591, ___reader_0)); }
	inline XmlTextReaderImpl_t3122949129 * get_reader_0() const { return ___reader_0; }
	inline XmlTextReaderImpl_t3122949129 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlTextReaderImpl_t3122949129 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
