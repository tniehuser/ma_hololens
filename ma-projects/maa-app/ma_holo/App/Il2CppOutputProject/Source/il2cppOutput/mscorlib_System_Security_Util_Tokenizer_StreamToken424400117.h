﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.IO.StreamReader
struct StreamReader_t2360341767;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.Tokenizer/StreamTokenReader
struct  StreamTokenReader_t424400117  : public Il2CppObject
{
public:
	// System.IO.StreamReader System.Security.Util.Tokenizer/StreamTokenReader::_in
	StreamReader_t2360341767 * ____in_0;
	// System.Int32 System.Security.Util.Tokenizer/StreamTokenReader::_numCharRead
	int32_t ____numCharRead_1;

public:
	inline static int32_t get_offset_of__in_0() { return static_cast<int32_t>(offsetof(StreamTokenReader_t424400117, ____in_0)); }
	inline StreamReader_t2360341767 * get__in_0() const { return ____in_0; }
	inline StreamReader_t2360341767 ** get_address_of__in_0() { return &____in_0; }
	inline void set__in_0(StreamReader_t2360341767 * value)
	{
		____in_0 = value;
		Il2CppCodeGenWriteBarrier(&____in_0, value);
	}

	inline static int32_t get_offset_of__numCharRead_1() { return static_cast<int32_t>(offsetof(StreamTokenReader_t424400117, ____numCharRead_1)); }
	inline int32_t get__numCharRead_1() const { return ____numCharRead_1; }
	inline int32_t* get_address_of__numCharRead_1() { return &____numCharRead_1; }
	inline void set__numCharRead_1(int32_t value)
	{
		____numCharRead_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
