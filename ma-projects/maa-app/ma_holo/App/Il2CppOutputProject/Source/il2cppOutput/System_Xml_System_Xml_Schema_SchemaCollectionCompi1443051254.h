﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseProcessor2373158431.h"

// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionCompiler
struct  SchemaCollectionCompiler_t1443051254  : public BaseProcessor_t2373158431
{
public:
	// System.Boolean System.Xml.Schema.SchemaCollectionCompiler::compileContentModel
	bool ___compileContentModel_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.SchemaCollectionCompiler::examplars
	XmlSchemaObjectTable_t3364835593 * ___examplars_7;
	// System.Collections.Stack System.Xml.Schema.SchemaCollectionCompiler::complexTypeStack
	Stack_t1043988394 * ___complexTypeStack_8;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionCompiler::schema
	XmlSchema_t880472818 * ___schema_9;

public:
	inline static int32_t get_offset_of_compileContentModel_6() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___compileContentModel_6)); }
	inline bool get_compileContentModel_6() const { return ___compileContentModel_6; }
	inline bool* get_address_of_compileContentModel_6() { return &___compileContentModel_6; }
	inline void set_compileContentModel_6(bool value)
	{
		___compileContentModel_6 = value;
	}

	inline static int32_t get_offset_of_examplars_7() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___examplars_7)); }
	inline XmlSchemaObjectTable_t3364835593 * get_examplars_7() const { return ___examplars_7; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_examplars_7() { return &___examplars_7; }
	inline void set_examplars_7(XmlSchemaObjectTable_t3364835593 * value)
	{
		___examplars_7 = value;
		Il2CppCodeGenWriteBarrier(&___examplars_7, value);
	}

	inline static int32_t get_offset_of_complexTypeStack_8() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___complexTypeStack_8)); }
	inline Stack_t1043988394 * get_complexTypeStack_8() const { return ___complexTypeStack_8; }
	inline Stack_t1043988394 ** get_address_of_complexTypeStack_8() { return &___complexTypeStack_8; }
	inline void set_complexTypeStack_8(Stack_t1043988394 * value)
	{
		___complexTypeStack_8 = value;
		Il2CppCodeGenWriteBarrier(&___complexTypeStack_8, value);
	}

	inline static int32_t get_offset_of_schema_9() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___schema_9)); }
	inline XmlSchema_t880472818 * get_schema_9() const { return ___schema_9; }
	inline XmlSchema_t880472818 ** get_address_of_schema_9() { return &___schema_9; }
	inline void set_schema_9(XmlSchema_t880472818 * value)
	{
		___schema_9 = value;
		Il2CppCodeGenWriteBarrier(&___schema_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
