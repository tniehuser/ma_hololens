﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlName3016058992.h"

// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEx
struct  XmlNameEx_t693073035  : public XmlName_t3016058992
{
public:
	// System.Byte System.Xml.XmlNameEx::flags
	uint8_t ___flags_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.XmlNameEx::memberType
	XmlSchemaSimpleType_t248156492 * ___memberType_8;
	// System.Xml.Schema.XmlSchemaType System.Xml.XmlNameEx::schemaType
	XmlSchemaType_t1795078578 * ___schemaType_9;
	// System.Object System.Xml.XmlNameEx::decl
	Il2CppObject * ___decl_10;

public:
	inline static int32_t get_offset_of_flags_7() { return static_cast<int32_t>(offsetof(XmlNameEx_t693073035, ___flags_7)); }
	inline uint8_t get_flags_7() const { return ___flags_7; }
	inline uint8_t* get_address_of_flags_7() { return &___flags_7; }
	inline void set_flags_7(uint8_t value)
	{
		___flags_7 = value;
	}

	inline static int32_t get_offset_of_memberType_8() { return static_cast<int32_t>(offsetof(XmlNameEx_t693073035, ___memberType_8)); }
	inline XmlSchemaSimpleType_t248156492 * get_memberType_8() const { return ___memberType_8; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_memberType_8() { return &___memberType_8; }
	inline void set_memberType_8(XmlSchemaSimpleType_t248156492 * value)
	{
		___memberType_8 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_8, value);
	}

	inline static int32_t get_offset_of_schemaType_9() { return static_cast<int32_t>(offsetof(XmlNameEx_t693073035, ___schemaType_9)); }
	inline XmlSchemaType_t1795078578 * get_schemaType_9() const { return ___schemaType_9; }
	inline XmlSchemaType_t1795078578 ** get_address_of_schemaType_9() { return &___schemaType_9; }
	inline void set_schemaType_9(XmlSchemaType_t1795078578 * value)
	{
		___schemaType_9 = value;
		Il2CppCodeGenWriteBarrier(&___schemaType_9, value);
	}

	inline static int32_t get_offset_of_decl_10() { return static_cast<int32_t>(offsetof(XmlNameEx_t693073035, ___decl_10)); }
	inline Il2CppObject * get_decl_10() const { return ___decl_10; }
	inline Il2CppObject ** get_address_of_decl_10() { return &___decl_10; }
	inline void set_decl_10(Il2CppObject * value)
	{
		___decl_10 = value;
		Il2CppCodeGenWriteBarrier(&___decl_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
