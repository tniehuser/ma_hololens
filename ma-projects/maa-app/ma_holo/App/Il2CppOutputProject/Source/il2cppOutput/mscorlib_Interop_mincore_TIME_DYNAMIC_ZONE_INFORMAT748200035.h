﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION/<TimeZoneKeyName>__FixedBuffer2
struct  U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035 
{
public:
	union
	{
		struct
		{
			// System.Char Interop/mincore/TIME_DYNAMIC_ZONE_INFORMATION/<TimeZoneKeyName>__FixedBuffer2::FixedElementField
			Il2CppChar ___FixedElementField_0;
		};
		uint8_t U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035__padding[256];
	};

public:
	inline static int32_t get_offset_of_FixedElementField_0() { return static_cast<int32_t>(offsetof(U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035, ___FixedElementField_0)); }
	inline Il2CppChar get_FixedElementField_0() const { return ___FixedElementField_0; }
	inline Il2CppChar* get_address_of_FixedElementField_0() { return &___FixedElementField_0; }
	inline void set_FixedElementField_0(Il2CppChar value)
	{
		___FixedElementField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
