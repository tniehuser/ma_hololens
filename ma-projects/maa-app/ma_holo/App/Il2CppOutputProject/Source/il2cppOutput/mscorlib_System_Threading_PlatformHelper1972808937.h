﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.PlatformHelper
struct  PlatformHelper_t1972808937  : public Il2CppObject
{
public:

public:
};

struct PlatformHelper_t1972808937_StaticFields
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.PlatformHelper::s_processorCount
	int32_t ___s_processorCount_0;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.PlatformHelper::s_lastProcessorCountRefreshTicks
	int32_t ___s_lastProcessorCountRefreshTicks_1;

public:
	inline static int32_t get_offset_of_s_processorCount_0() { return static_cast<int32_t>(offsetof(PlatformHelper_t1972808937_StaticFields, ___s_processorCount_0)); }
	inline int32_t get_s_processorCount_0() const { return ___s_processorCount_0; }
	inline int32_t* get_address_of_s_processorCount_0() { return &___s_processorCount_0; }
	inline void set_s_processorCount_0(int32_t value)
	{
		___s_processorCount_0 = value;
	}

	inline static int32_t get_offset_of_s_lastProcessorCountRefreshTicks_1() { return static_cast<int32_t>(offsetof(PlatformHelper_t1972808937_StaticFields, ___s_lastProcessorCountRefreshTicks_1)); }
	inline int32_t get_s_lastProcessorCountRefreshTicks_1() const { return ___s_lastProcessorCountRefreshTicks_1; }
	inline int32_t* get_address_of_s_lastProcessorCountRefreshTicks_1() { return &___s_lastProcessorCountRefreshTicks_1; }
	inline void set_s_lastProcessorCountRefreshTicks_1(int32_t value)
	{
		___s_lastProcessorCountRefreshTicks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
