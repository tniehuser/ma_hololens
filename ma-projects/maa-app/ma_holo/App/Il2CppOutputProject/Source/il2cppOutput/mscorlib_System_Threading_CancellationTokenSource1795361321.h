﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"

// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t1795361321;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Threading.SparselyPopulatedArray`1<System.Threading.CancellationCallbackInfo>[]
struct SparselyPopulatedArray_1U5BU5D_t263271761;
// System.Threading.CancellationTokenRegistration[]
struct CancellationTokenRegistrationU5BU5D_t376445968;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Threading.CancellationCallbackInfo
struct CancellationCallbackInfo_t1473383178;
// System.Threading.Timer
struct Timer_t791717973;
// System.Threading.TimerCallback
struct TimerCallback_t1684927372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationTokenSource
struct  CancellationTokenSource_t1795361321  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_kernelEvent
	ManualResetEvent_t926074657 * ___m_kernelEvent_3;
	// System.Threading.SparselyPopulatedArray`1<System.Threading.CancellationCallbackInfo>[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_registeredCallbacksLists
	SparselyPopulatedArray_1U5BU5D_t263271761* ___m_registeredCallbacksLists_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_state
	int32_t ___m_state_5;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_threadIDExecutingCallbacks
	int32_t ___m_threadIDExecutingCallbacks_6;
	// System.Boolean System.Threading.CancellationTokenSource::m_disposed
	bool ___m_disposed_7;
	// System.Threading.CancellationTokenRegistration[] System.Threading.CancellationTokenSource::m_linkingRegistrations
	CancellationTokenRegistrationU5BU5D_t376445968* ___m_linkingRegistrations_8;
	// System.Threading.CancellationCallbackInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_executingCallback
	CancellationCallbackInfo_t1473383178 * ___m_executingCallback_10;
	// System.Threading.Timer modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.CancellationTokenSource::m_timer
	Timer_t791717973 * ___m_timer_11;

public:
	inline static int32_t get_offset_of_m_kernelEvent_3() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_kernelEvent_3)); }
	inline ManualResetEvent_t926074657 * get_m_kernelEvent_3() const { return ___m_kernelEvent_3; }
	inline ManualResetEvent_t926074657 ** get_address_of_m_kernelEvent_3() { return &___m_kernelEvent_3; }
	inline void set_m_kernelEvent_3(ManualResetEvent_t926074657 * value)
	{
		___m_kernelEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_kernelEvent_3, value);
	}

	inline static int32_t get_offset_of_m_registeredCallbacksLists_4() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_registeredCallbacksLists_4)); }
	inline SparselyPopulatedArray_1U5BU5D_t263271761* get_m_registeredCallbacksLists_4() const { return ___m_registeredCallbacksLists_4; }
	inline SparselyPopulatedArray_1U5BU5D_t263271761** get_address_of_m_registeredCallbacksLists_4() { return &___m_registeredCallbacksLists_4; }
	inline void set_m_registeredCallbacksLists_4(SparselyPopulatedArray_1U5BU5D_t263271761* value)
	{
		___m_registeredCallbacksLists_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_registeredCallbacksLists_4, value);
	}

	inline static int32_t get_offset_of_m_state_5() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_state_5)); }
	inline int32_t get_m_state_5() const { return ___m_state_5; }
	inline int32_t* get_address_of_m_state_5() { return &___m_state_5; }
	inline void set_m_state_5(int32_t value)
	{
		___m_state_5 = value;
	}

	inline static int32_t get_offset_of_m_threadIDExecutingCallbacks_6() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_threadIDExecutingCallbacks_6)); }
	inline int32_t get_m_threadIDExecutingCallbacks_6() const { return ___m_threadIDExecutingCallbacks_6; }
	inline int32_t* get_address_of_m_threadIDExecutingCallbacks_6() { return &___m_threadIDExecutingCallbacks_6; }
	inline void set_m_threadIDExecutingCallbacks_6(int32_t value)
	{
		___m_threadIDExecutingCallbacks_6 = value;
	}

	inline static int32_t get_offset_of_m_disposed_7() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_disposed_7)); }
	inline bool get_m_disposed_7() const { return ___m_disposed_7; }
	inline bool* get_address_of_m_disposed_7() { return &___m_disposed_7; }
	inline void set_m_disposed_7(bool value)
	{
		___m_disposed_7 = value;
	}

	inline static int32_t get_offset_of_m_linkingRegistrations_8() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_linkingRegistrations_8)); }
	inline CancellationTokenRegistrationU5BU5D_t376445968* get_m_linkingRegistrations_8() const { return ___m_linkingRegistrations_8; }
	inline CancellationTokenRegistrationU5BU5D_t376445968** get_address_of_m_linkingRegistrations_8() { return &___m_linkingRegistrations_8; }
	inline void set_m_linkingRegistrations_8(CancellationTokenRegistrationU5BU5D_t376445968* value)
	{
		___m_linkingRegistrations_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_linkingRegistrations_8, value);
	}

	inline static int32_t get_offset_of_m_executingCallback_10() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_executingCallback_10)); }
	inline CancellationCallbackInfo_t1473383178 * get_m_executingCallback_10() const { return ___m_executingCallback_10; }
	inline CancellationCallbackInfo_t1473383178 ** get_address_of_m_executingCallback_10() { return &___m_executingCallback_10; }
	inline void set_m_executingCallback_10(CancellationCallbackInfo_t1473383178 * value)
	{
		___m_executingCallback_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_executingCallback_10, value);
	}

	inline static int32_t get_offset_of_m_timer_11() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321, ___m_timer_11)); }
	inline Timer_t791717973 * get_m_timer_11() const { return ___m_timer_11; }
	inline Timer_t791717973 ** get_address_of_m_timer_11() { return &___m_timer_11; }
	inline void set_m_timer_11(Timer_t791717973 * value)
	{
		___m_timer_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_timer_11, value);
	}
};

struct CancellationTokenSource_t1795361321_StaticFields
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationTokenSource::_staticSource_Set
	CancellationTokenSource_t1795361321 * ____staticSource_Set_0;
	// System.Threading.CancellationTokenSource System.Threading.CancellationTokenSource::_staticSource_NotCancelable
	CancellationTokenSource_t1795361321 * ____staticSource_NotCancelable_1;
	// System.Int32 System.Threading.CancellationTokenSource::s_nLists
	int32_t ___s_nLists_2;
	// System.Action`1<System.Object> System.Threading.CancellationTokenSource::s_LinkedTokenCancelDelegate
	Action_1_t2491248677 * ___s_LinkedTokenCancelDelegate_9;
	// System.Threading.TimerCallback System.Threading.CancellationTokenSource::s_timerCallback
	TimerCallback_t1684927372 * ___s_timerCallback_12;

public:
	inline static int32_t get_offset_of__staticSource_Set_0() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321_StaticFields, ____staticSource_Set_0)); }
	inline CancellationTokenSource_t1795361321 * get__staticSource_Set_0() const { return ____staticSource_Set_0; }
	inline CancellationTokenSource_t1795361321 ** get_address_of__staticSource_Set_0() { return &____staticSource_Set_0; }
	inline void set__staticSource_Set_0(CancellationTokenSource_t1795361321 * value)
	{
		____staticSource_Set_0 = value;
		Il2CppCodeGenWriteBarrier(&____staticSource_Set_0, value);
	}

	inline static int32_t get_offset_of__staticSource_NotCancelable_1() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321_StaticFields, ____staticSource_NotCancelable_1)); }
	inline CancellationTokenSource_t1795361321 * get__staticSource_NotCancelable_1() const { return ____staticSource_NotCancelable_1; }
	inline CancellationTokenSource_t1795361321 ** get_address_of__staticSource_NotCancelable_1() { return &____staticSource_NotCancelable_1; }
	inline void set__staticSource_NotCancelable_1(CancellationTokenSource_t1795361321 * value)
	{
		____staticSource_NotCancelable_1 = value;
		Il2CppCodeGenWriteBarrier(&____staticSource_NotCancelable_1, value);
	}

	inline static int32_t get_offset_of_s_nLists_2() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321_StaticFields, ___s_nLists_2)); }
	inline int32_t get_s_nLists_2() const { return ___s_nLists_2; }
	inline int32_t* get_address_of_s_nLists_2() { return &___s_nLists_2; }
	inline void set_s_nLists_2(int32_t value)
	{
		___s_nLists_2 = value;
	}

	inline static int32_t get_offset_of_s_LinkedTokenCancelDelegate_9() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321_StaticFields, ___s_LinkedTokenCancelDelegate_9)); }
	inline Action_1_t2491248677 * get_s_LinkedTokenCancelDelegate_9() const { return ___s_LinkedTokenCancelDelegate_9; }
	inline Action_1_t2491248677 ** get_address_of_s_LinkedTokenCancelDelegate_9() { return &___s_LinkedTokenCancelDelegate_9; }
	inline void set_s_LinkedTokenCancelDelegate_9(Action_1_t2491248677 * value)
	{
		___s_LinkedTokenCancelDelegate_9 = value;
		Il2CppCodeGenWriteBarrier(&___s_LinkedTokenCancelDelegate_9, value);
	}

	inline static int32_t get_offset_of_s_timerCallback_12() { return static_cast<int32_t>(offsetof(CancellationTokenSource_t1795361321_StaticFields, ___s_timerCallback_12)); }
	inline TimerCallback_t1684927372 * get_s_timerCallback_12() const { return ___s_timerCallback_12; }
	inline TimerCallback_t1684927372 ** get_address_of_s_timerCallback_12() { return &___s_timerCallback_12; }
	inline void set_s_timerCallback_12(TimerCallback_t1684927372 * value)
	{
		___s_timerCallback_12 = value;
		Il2CppCodeGenWriteBarrier(&___s_timerCallback_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
