﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.StringComparer
struct StringComparer_t1574862926;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparer
struct  StringComparer_t1574862926  : public Il2CppObject
{
public:

public:
};

struct StringComparer_t1574862926_StaticFields
{
public:
	// System.StringComparer System.StringComparer::_invariantCulture
	StringComparer_t1574862926 * ____invariantCulture_0;
	// System.StringComparer System.StringComparer::_invariantCultureIgnoreCase
	StringComparer_t1574862926 * ____invariantCultureIgnoreCase_1;
	// System.StringComparer System.StringComparer::_ordinal
	StringComparer_t1574862926 * ____ordinal_2;
	// System.StringComparer System.StringComparer::_ordinalIgnoreCase
	StringComparer_t1574862926 * ____ordinalIgnoreCase_3;

public:
	inline static int32_t get_offset_of__invariantCulture_0() { return static_cast<int32_t>(offsetof(StringComparer_t1574862926_StaticFields, ____invariantCulture_0)); }
	inline StringComparer_t1574862926 * get__invariantCulture_0() const { return ____invariantCulture_0; }
	inline StringComparer_t1574862926 ** get_address_of__invariantCulture_0() { return &____invariantCulture_0; }
	inline void set__invariantCulture_0(StringComparer_t1574862926 * value)
	{
		____invariantCulture_0 = value;
		Il2CppCodeGenWriteBarrier(&____invariantCulture_0, value);
	}

	inline static int32_t get_offset_of__invariantCultureIgnoreCase_1() { return static_cast<int32_t>(offsetof(StringComparer_t1574862926_StaticFields, ____invariantCultureIgnoreCase_1)); }
	inline StringComparer_t1574862926 * get__invariantCultureIgnoreCase_1() const { return ____invariantCultureIgnoreCase_1; }
	inline StringComparer_t1574862926 ** get_address_of__invariantCultureIgnoreCase_1() { return &____invariantCultureIgnoreCase_1; }
	inline void set__invariantCultureIgnoreCase_1(StringComparer_t1574862926 * value)
	{
		____invariantCultureIgnoreCase_1 = value;
		Il2CppCodeGenWriteBarrier(&____invariantCultureIgnoreCase_1, value);
	}

	inline static int32_t get_offset_of__ordinal_2() { return static_cast<int32_t>(offsetof(StringComparer_t1574862926_StaticFields, ____ordinal_2)); }
	inline StringComparer_t1574862926 * get__ordinal_2() const { return ____ordinal_2; }
	inline StringComparer_t1574862926 ** get_address_of__ordinal_2() { return &____ordinal_2; }
	inline void set__ordinal_2(StringComparer_t1574862926 * value)
	{
		____ordinal_2 = value;
		Il2CppCodeGenWriteBarrier(&____ordinal_2, value);
	}

	inline static int32_t get_offset_of__ordinalIgnoreCase_3() { return static_cast<int32_t>(offsetof(StringComparer_t1574862926_StaticFields, ____ordinalIgnoreCase_3)); }
	inline StringComparer_t1574862926 * get__ordinalIgnoreCase_3() const { return ____ordinalIgnoreCase_3; }
	inline StringComparer_t1574862926 ** get_address_of__ordinalIgnoreCase_3() { return &____ordinalIgnoreCase_3; }
	inline void set__ordinalIgnoreCase_3(StringComparer_t1574862926 * value)
	{
		____ordinalIgnoreCase_3 = value;
		Il2CppCodeGenWriteBarrier(&____ordinalIgnoreCase_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
