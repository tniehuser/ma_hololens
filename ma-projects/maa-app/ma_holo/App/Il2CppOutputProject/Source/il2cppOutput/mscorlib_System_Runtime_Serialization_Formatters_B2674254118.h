﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi682686529.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3272317689.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3719433431.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi642857832.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2706794673.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4215080384.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3020811655.h"

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo
struct ReadObjectInfo_t1645661573;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Runtime.Serialization.Formatters.Binary.PrimitiveArray
struct PrimitiveArray_t3608773834;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ParseRecord
struct  ParseRecord_t2674254118  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.InternalParseTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRparseTypeEnum
	int32_t ___PRparseTypeEnum_1;
	// System.Runtime.Serialization.Formatters.Binary.InternalObjectTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRobjectTypeEnum
	int32_t ___PRobjectTypeEnum_2;
	// System.Runtime.Serialization.Formatters.Binary.InternalArrayTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRarrayTypeEnum
	int32_t ___PRarrayTypeEnum_3;
	// System.Runtime.Serialization.Formatters.Binary.InternalMemberTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRmemberTypeEnum
	int32_t ___PRmemberTypeEnum_4;
	// System.Runtime.Serialization.Formatters.Binary.InternalMemberValueE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRmemberValueEnum
	int32_t ___PRmemberValueEnum_5;
	// System.Runtime.Serialization.Formatters.Binary.InternalObjectPositionE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRobjectPositionEnum
	int32_t ___PRobjectPositionEnum_6;
	// System.String System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRname
	String_t* ___PRname_7;
	// System.String System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRvalue
	String_t* ___PRvalue_8;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRvarValue
	Il2CppObject * ___PRvarValue_9;
	// System.String System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRkeyDt
	String_t* ___PRkeyDt_10;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRdtType
	Type_t * ___PRdtType_11;
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRdtTypeCode
	int32_t ___PRdtTypeCode_12;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRisEnum
	bool ___PRisEnum_13;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRobjectId
	int64_t ___PRobjectId_14;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRidRef
	int64_t ___PRidRef_15;
	// System.String System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRarrayElementTypeString
	String_t* ___PRarrayElementTypeString_16;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRarrayElementType
	Type_t * ___PRarrayElementType_17;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRisArrayVariant
	bool ___PRisArrayVariant_18;
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRarrayElementTypeCode
	int32_t ___PRarrayElementTypeCode_19;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRrank
	int32_t ___PRrank_20;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRlengthA
	Int32U5BU5D_t3030399641* ___PRlengthA_21;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRpositionA
	Int32U5BU5D_t3030399641* ___PRpositionA_22;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRlowerBoundA
	Int32U5BU5D_t3030399641* ___PRlowerBoundA_23;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRupperBoundA
	Int32U5BU5D_t3030399641* ___PRupperBoundA_24;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRindexMap
	Int32U5BU5D_t3030399641* ___PRindexMap_25;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRmemberIndex
	int32_t ___PRmemberIndex_26;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRlinearlength
	int32_t ___PRlinearlength_27;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRrectangularMap
	Int32U5BU5D_t3030399641* ___PRrectangularMap_28;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRisLowerBound
	bool ___PRisLowerBound_29;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRtopId
	int64_t ___PRtopId_30;
	// System.Int64 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRheaderId
	int64_t ___PRheaderId_31;
	// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRobjectInfo
	ReadObjectInfo_t1645661573 * ___PRobjectInfo_32;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRisValueTypeFixup
	bool ___PRisValueTypeFixup_33;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRnewObj
	Il2CppObject * ___PRnewObj_34;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRobjectA
	ObjectU5BU5D_t3614634134* ___PRobjectA_35;
	// System.Runtime.Serialization.Formatters.Binary.PrimitiveArray System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRprimitiveArray
	PrimitiveArray_t3608773834 * ___PRprimitiveArray_36;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRisRegistered
	bool ___PRisRegistered_37;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRmemberData
	ObjectU5BU5D_t3614634134* ___PRmemberData_38;
	// System.Runtime.Serialization.SerializationInfo System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRsi
	SerializationInfo_t228987430 * ___PRsi_39;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ParseRecord::PRnullCount
	int32_t ___PRnullCount_40;

public:
	inline static int32_t get_offset_of_PRparseTypeEnum_1() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRparseTypeEnum_1)); }
	inline int32_t get_PRparseTypeEnum_1() const { return ___PRparseTypeEnum_1; }
	inline int32_t* get_address_of_PRparseTypeEnum_1() { return &___PRparseTypeEnum_1; }
	inline void set_PRparseTypeEnum_1(int32_t value)
	{
		___PRparseTypeEnum_1 = value;
	}

	inline static int32_t get_offset_of_PRobjectTypeEnum_2() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRobjectTypeEnum_2)); }
	inline int32_t get_PRobjectTypeEnum_2() const { return ___PRobjectTypeEnum_2; }
	inline int32_t* get_address_of_PRobjectTypeEnum_2() { return &___PRobjectTypeEnum_2; }
	inline void set_PRobjectTypeEnum_2(int32_t value)
	{
		___PRobjectTypeEnum_2 = value;
	}

	inline static int32_t get_offset_of_PRarrayTypeEnum_3() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRarrayTypeEnum_3)); }
	inline int32_t get_PRarrayTypeEnum_3() const { return ___PRarrayTypeEnum_3; }
	inline int32_t* get_address_of_PRarrayTypeEnum_3() { return &___PRarrayTypeEnum_3; }
	inline void set_PRarrayTypeEnum_3(int32_t value)
	{
		___PRarrayTypeEnum_3 = value;
	}

	inline static int32_t get_offset_of_PRmemberTypeEnum_4() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRmemberTypeEnum_4)); }
	inline int32_t get_PRmemberTypeEnum_4() const { return ___PRmemberTypeEnum_4; }
	inline int32_t* get_address_of_PRmemberTypeEnum_4() { return &___PRmemberTypeEnum_4; }
	inline void set_PRmemberTypeEnum_4(int32_t value)
	{
		___PRmemberTypeEnum_4 = value;
	}

	inline static int32_t get_offset_of_PRmemberValueEnum_5() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRmemberValueEnum_5)); }
	inline int32_t get_PRmemberValueEnum_5() const { return ___PRmemberValueEnum_5; }
	inline int32_t* get_address_of_PRmemberValueEnum_5() { return &___PRmemberValueEnum_5; }
	inline void set_PRmemberValueEnum_5(int32_t value)
	{
		___PRmemberValueEnum_5 = value;
	}

	inline static int32_t get_offset_of_PRobjectPositionEnum_6() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRobjectPositionEnum_6)); }
	inline int32_t get_PRobjectPositionEnum_6() const { return ___PRobjectPositionEnum_6; }
	inline int32_t* get_address_of_PRobjectPositionEnum_6() { return &___PRobjectPositionEnum_6; }
	inline void set_PRobjectPositionEnum_6(int32_t value)
	{
		___PRobjectPositionEnum_6 = value;
	}

	inline static int32_t get_offset_of_PRname_7() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRname_7)); }
	inline String_t* get_PRname_7() const { return ___PRname_7; }
	inline String_t** get_address_of_PRname_7() { return &___PRname_7; }
	inline void set_PRname_7(String_t* value)
	{
		___PRname_7 = value;
		Il2CppCodeGenWriteBarrier(&___PRname_7, value);
	}

	inline static int32_t get_offset_of_PRvalue_8() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRvalue_8)); }
	inline String_t* get_PRvalue_8() const { return ___PRvalue_8; }
	inline String_t** get_address_of_PRvalue_8() { return &___PRvalue_8; }
	inline void set_PRvalue_8(String_t* value)
	{
		___PRvalue_8 = value;
		Il2CppCodeGenWriteBarrier(&___PRvalue_8, value);
	}

	inline static int32_t get_offset_of_PRvarValue_9() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRvarValue_9)); }
	inline Il2CppObject * get_PRvarValue_9() const { return ___PRvarValue_9; }
	inline Il2CppObject ** get_address_of_PRvarValue_9() { return &___PRvarValue_9; }
	inline void set_PRvarValue_9(Il2CppObject * value)
	{
		___PRvarValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___PRvarValue_9, value);
	}

	inline static int32_t get_offset_of_PRkeyDt_10() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRkeyDt_10)); }
	inline String_t* get_PRkeyDt_10() const { return ___PRkeyDt_10; }
	inline String_t** get_address_of_PRkeyDt_10() { return &___PRkeyDt_10; }
	inline void set_PRkeyDt_10(String_t* value)
	{
		___PRkeyDt_10 = value;
		Il2CppCodeGenWriteBarrier(&___PRkeyDt_10, value);
	}

	inline static int32_t get_offset_of_PRdtType_11() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRdtType_11)); }
	inline Type_t * get_PRdtType_11() const { return ___PRdtType_11; }
	inline Type_t ** get_address_of_PRdtType_11() { return &___PRdtType_11; }
	inline void set_PRdtType_11(Type_t * value)
	{
		___PRdtType_11 = value;
		Il2CppCodeGenWriteBarrier(&___PRdtType_11, value);
	}

	inline static int32_t get_offset_of_PRdtTypeCode_12() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRdtTypeCode_12)); }
	inline int32_t get_PRdtTypeCode_12() const { return ___PRdtTypeCode_12; }
	inline int32_t* get_address_of_PRdtTypeCode_12() { return &___PRdtTypeCode_12; }
	inline void set_PRdtTypeCode_12(int32_t value)
	{
		___PRdtTypeCode_12 = value;
	}

	inline static int32_t get_offset_of_PRisEnum_13() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRisEnum_13)); }
	inline bool get_PRisEnum_13() const { return ___PRisEnum_13; }
	inline bool* get_address_of_PRisEnum_13() { return &___PRisEnum_13; }
	inline void set_PRisEnum_13(bool value)
	{
		___PRisEnum_13 = value;
	}

	inline static int32_t get_offset_of_PRobjectId_14() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRobjectId_14)); }
	inline int64_t get_PRobjectId_14() const { return ___PRobjectId_14; }
	inline int64_t* get_address_of_PRobjectId_14() { return &___PRobjectId_14; }
	inline void set_PRobjectId_14(int64_t value)
	{
		___PRobjectId_14 = value;
	}

	inline static int32_t get_offset_of_PRidRef_15() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRidRef_15)); }
	inline int64_t get_PRidRef_15() const { return ___PRidRef_15; }
	inline int64_t* get_address_of_PRidRef_15() { return &___PRidRef_15; }
	inline void set_PRidRef_15(int64_t value)
	{
		___PRidRef_15 = value;
	}

	inline static int32_t get_offset_of_PRarrayElementTypeString_16() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRarrayElementTypeString_16)); }
	inline String_t* get_PRarrayElementTypeString_16() const { return ___PRarrayElementTypeString_16; }
	inline String_t** get_address_of_PRarrayElementTypeString_16() { return &___PRarrayElementTypeString_16; }
	inline void set_PRarrayElementTypeString_16(String_t* value)
	{
		___PRarrayElementTypeString_16 = value;
		Il2CppCodeGenWriteBarrier(&___PRarrayElementTypeString_16, value);
	}

	inline static int32_t get_offset_of_PRarrayElementType_17() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRarrayElementType_17)); }
	inline Type_t * get_PRarrayElementType_17() const { return ___PRarrayElementType_17; }
	inline Type_t ** get_address_of_PRarrayElementType_17() { return &___PRarrayElementType_17; }
	inline void set_PRarrayElementType_17(Type_t * value)
	{
		___PRarrayElementType_17 = value;
		Il2CppCodeGenWriteBarrier(&___PRarrayElementType_17, value);
	}

	inline static int32_t get_offset_of_PRisArrayVariant_18() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRisArrayVariant_18)); }
	inline bool get_PRisArrayVariant_18() const { return ___PRisArrayVariant_18; }
	inline bool* get_address_of_PRisArrayVariant_18() { return &___PRisArrayVariant_18; }
	inline void set_PRisArrayVariant_18(bool value)
	{
		___PRisArrayVariant_18 = value;
	}

	inline static int32_t get_offset_of_PRarrayElementTypeCode_19() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRarrayElementTypeCode_19)); }
	inline int32_t get_PRarrayElementTypeCode_19() const { return ___PRarrayElementTypeCode_19; }
	inline int32_t* get_address_of_PRarrayElementTypeCode_19() { return &___PRarrayElementTypeCode_19; }
	inline void set_PRarrayElementTypeCode_19(int32_t value)
	{
		___PRarrayElementTypeCode_19 = value;
	}

	inline static int32_t get_offset_of_PRrank_20() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRrank_20)); }
	inline int32_t get_PRrank_20() const { return ___PRrank_20; }
	inline int32_t* get_address_of_PRrank_20() { return &___PRrank_20; }
	inline void set_PRrank_20(int32_t value)
	{
		___PRrank_20 = value;
	}

	inline static int32_t get_offset_of_PRlengthA_21() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRlengthA_21)); }
	inline Int32U5BU5D_t3030399641* get_PRlengthA_21() const { return ___PRlengthA_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRlengthA_21() { return &___PRlengthA_21; }
	inline void set_PRlengthA_21(Int32U5BU5D_t3030399641* value)
	{
		___PRlengthA_21 = value;
		Il2CppCodeGenWriteBarrier(&___PRlengthA_21, value);
	}

	inline static int32_t get_offset_of_PRpositionA_22() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRpositionA_22)); }
	inline Int32U5BU5D_t3030399641* get_PRpositionA_22() const { return ___PRpositionA_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRpositionA_22() { return &___PRpositionA_22; }
	inline void set_PRpositionA_22(Int32U5BU5D_t3030399641* value)
	{
		___PRpositionA_22 = value;
		Il2CppCodeGenWriteBarrier(&___PRpositionA_22, value);
	}

	inline static int32_t get_offset_of_PRlowerBoundA_23() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRlowerBoundA_23)); }
	inline Int32U5BU5D_t3030399641* get_PRlowerBoundA_23() const { return ___PRlowerBoundA_23; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRlowerBoundA_23() { return &___PRlowerBoundA_23; }
	inline void set_PRlowerBoundA_23(Int32U5BU5D_t3030399641* value)
	{
		___PRlowerBoundA_23 = value;
		Il2CppCodeGenWriteBarrier(&___PRlowerBoundA_23, value);
	}

	inline static int32_t get_offset_of_PRupperBoundA_24() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRupperBoundA_24)); }
	inline Int32U5BU5D_t3030399641* get_PRupperBoundA_24() const { return ___PRupperBoundA_24; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRupperBoundA_24() { return &___PRupperBoundA_24; }
	inline void set_PRupperBoundA_24(Int32U5BU5D_t3030399641* value)
	{
		___PRupperBoundA_24 = value;
		Il2CppCodeGenWriteBarrier(&___PRupperBoundA_24, value);
	}

	inline static int32_t get_offset_of_PRindexMap_25() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRindexMap_25)); }
	inline Int32U5BU5D_t3030399641* get_PRindexMap_25() const { return ___PRindexMap_25; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRindexMap_25() { return &___PRindexMap_25; }
	inline void set_PRindexMap_25(Int32U5BU5D_t3030399641* value)
	{
		___PRindexMap_25 = value;
		Il2CppCodeGenWriteBarrier(&___PRindexMap_25, value);
	}

	inline static int32_t get_offset_of_PRmemberIndex_26() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRmemberIndex_26)); }
	inline int32_t get_PRmemberIndex_26() const { return ___PRmemberIndex_26; }
	inline int32_t* get_address_of_PRmemberIndex_26() { return &___PRmemberIndex_26; }
	inline void set_PRmemberIndex_26(int32_t value)
	{
		___PRmemberIndex_26 = value;
	}

	inline static int32_t get_offset_of_PRlinearlength_27() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRlinearlength_27)); }
	inline int32_t get_PRlinearlength_27() const { return ___PRlinearlength_27; }
	inline int32_t* get_address_of_PRlinearlength_27() { return &___PRlinearlength_27; }
	inline void set_PRlinearlength_27(int32_t value)
	{
		___PRlinearlength_27 = value;
	}

	inline static int32_t get_offset_of_PRrectangularMap_28() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRrectangularMap_28)); }
	inline Int32U5BU5D_t3030399641* get_PRrectangularMap_28() const { return ___PRrectangularMap_28; }
	inline Int32U5BU5D_t3030399641** get_address_of_PRrectangularMap_28() { return &___PRrectangularMap_28; }
	inline void set_PRrectangularMap_28(Int32U5BU5D_t3030399641* value)
	{
		___PRrectangularMap_28 = value;
		Il2CppCodeGenWriteBarrier(&___PRrectangularMap_28, value);
	}

	inline static int32_t get_offset_of_PRisLowerBound_29() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRisLowerBound_29)); }
	inline bool get_PRisLowerBound_29() const { return ___PRisLowerBound_29; }
	inline bool* get_address_of_PRisLowerBound_29() { return &___PRisLowerBound_29; }
	inline void set_PRisLowerBound_29(bool value)
	{
		___PRisLowerBound_29 = value;
	}

	inline static int32_t get_offset_of_PRtopId_30() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRtopId_30)); }
	inline int64_t get_PRtopId_30() const { return ___PRtopId_30; }
	inline int64_t* get_address_of_PRtopId_30() { return &___PRtopId_30; }
	inline void set_PRtopId_30(int64_t value)
	{
		___PRtopId_30 = value;
	}

	inline static int32_t get_offset_of_PRheaderId_31() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRheaderId_31)); }
	inline int64_t get_PRheaderId_31() const { return ___PRheaderId_31; }
	inline int64_t* get_address_of_PRheaderId_31() { return &___PRheaderId_31; }
	inline void set_PRheaderId_31(int64_t value)
	{
		___PRheaderId_31 = value;
	}

	inline static int32_t get_offset_of_PRobjectInfo_32() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRobjectInfo_32)); }
	inline ReadObjectInfo_t1645661573 * get_PRobjectInfo_32() const { return ___PRobjectInfo_32; }
	inline ReadObjectInfo_t1645661573 ** get_address_of_PRobjectInfo_32() { return &___PRobjectInfo_32; }
	inline void set_PRobjectInfo_32(ReadObjectInfo_t1645661573 * value)
	{
		___PRobjectInfo_32 = value;
		Il2CppCodeGenWriteBarrier(&___PRobjectInfo_32, value);
	}

	inline static int32_t get_offset_of_PRisValueTypeFixup_33() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRisValueTypeFixup_33)); }
	inline bool get_PRisValueTypeFixup_33() const { return ___PRisValueTypeFixup_33; }
	inline bool* get_address_of_PRisValueTypeFixup_33() { return &___PRisValueTypeFixup_33; }
	inline void set_PRisValueTypeFixup_33(bool value)
	{
		___PRisValueTypeFixup_33 = value;
	}

	inline static int32_t get_offset_of_PRnewObj_34() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRnewObj_34)); }
	inline Il2CppObject * get_PRnewObj_34() const { return ___PRnewObj_34; }
	inline Il2CppObject ** get_address_of_PRnewObj_34() { return &___PRnewObj_34; }
	inline void set_PRnewObj_34(Il2CppObject * value)
	{
		___PRnewObj_34 = value;
		Il2CppCodeGenWriteBarrier(&___PRnewObj_34, value);
	}

	inline static int32_t get_offset_of_PRobjectA_35() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRobjectA_35)); }
	inline ObjectU5BU5D_t3614634134* get_PRobjectA_35() const { return ___PRobjectA_35; }
	inline ObjectU5BU5D_t3614634134** get_address_of_PRobjectA_35() { return &___PRobjectA_35; }
	inline void set_PRobjectA_35(ObjectU5BU5D_t3614634134* value)
	{
		___PRobjectA_35 = value;
		Il2CppCodeGenWriteBarrier(&___PRobjectA_35, value);
	}

	inline static int32_t get_offset_of_PRprimitiveArray_36() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRprimitiveArray_36)); }
	inline PrimitiveArray_t3608773834 * get_PRprimitiveArray_36() const { return ___PRprimitiveArray_36; }
	inline PrimitiveArray_t3608773834 ** get_address_of_PRprimitiveArray_36() { return &___PRprimitiveArray_36; }
	inline void set_PRprimitiveArray_36(PrimitiveArray_t3608773834 * value)
	{
		___PRprimitiveArray_36 = value;
		Il2CppCodeGenWriteBarrier(&___PRprimitiveArray_36, value);
	}

	inline static int32_t get_offset_of_PRisRegistered_37() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRisRegistered_37)); }
	inline bool get_PRisRegistered_37() const { return ___PRisRegistered_37; }
	inline bool* get_address_of_PRisRegistered_37() { return &___PRisRegistered_37; }
	inline void set_PRisRegistered_37(bool value)
	{
		___PRisRegistered_37 = value;
	}

	inline static int32_t get_offset_of_PRmemberData_38() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRmemberData_38)); }
	inline ObjectU5BU5D_t3614634134* get_PRmemberData_38() const { return ___PRmemberData_38; }
	inline ObjectU5BU5D_t3614634134** get_address_of_PRmemberData_38() { return &___PRmemberData_38; }
	inline void set_PRmemberData_38(ObjectU5BU5D_t3614634134* value)
	{
		___PRmemberData_38 = value;
		Il2CppCodeGenWriteBarrier(&___PRmemberData_38, value);
	}

	inline static int32_t get_offset_of_PRsi_39() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRsi_39)); }
	inline SerializationInfo_t228987430 * get_PRsi_39() const { return ___PRsi_39; }
	inline SerializationInfo_t228987430 ** get_address_of_PRsi_39() { return &___PRsi_39; }
	inline void set_PRsi_39(SerializationInfo_t228987430 * value)
	{
		___PRsi_39 = value;
		Il2CppCodeGenWriteBarrier(&___PRsi_39, value);
	}

	inline static int32_t get_offset_of_PRnullCount_40() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118, ___PRnullCount_40)); }
	inline int32_t get_PRnullCount_40() const { return ___PRnullCount_40; }
	inline int32_t* get_address_of_PRnullCount_40() { return &___PRnullCount_40; }
	inline void set_PRnullCount_40(int32_t value)
	{
		___PRnullCount_40 = value;
	}
};

struct ParseRecord_t2674254118_StaticFields
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ParseRecord::parseRecordIdCount
	int32_t ___parseRecordIdCount_0;

public:
	inline static int32_t get_offset_of_parseRecordIdCount_0() { return static_cast<int32_t>(offsetof(ParseRecord_t2674254118_StaticFields, ___parseRecordIdCount_0)); }
	inline int32_t get_parseRecordIdCount_0() const { return ___parseRecordIdCount_0; }
	inline int32_t* get_address_of_parseRecordIdCount_0() { return &___parseRecordIdCount_0; }
	inline void set_parseRecordIdCount_0(int32_t value)
	{
		___parseRecordIdCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
