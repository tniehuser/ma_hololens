﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseProcessor2373158431.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"

// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionPreprocessor
struct  SchemaCollectionPreprocessor_t2028437652  : public BaseProcessor_t2373158431
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionPreprocessor::schema
	XmlSchema_t880472818 * ___schema_6;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::targetNamespace
	String_t* ___targetNamespace_7;
	// System.Boolean System.Xml.Schema.SchemaCollectionPreprocessor::buildinIncluded
	bool ___buildinIncluded_8;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::elementFormDefault
	int32_t ___elementFormDefault_9;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::attributeFormDefault
	int32_t ___attributeFormDefault_10;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::blockDefault
	int32_t ___blockDefault_11;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::finalDefault
	int32_t ___finalDefault_12;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::schemaLocations
	Hashtable_t909839986 * ___schemaLocations_13;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::referenceNamespaces
	Hashtable_t909839986 * ___referenceNamespaces_14;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::Xmlns
	String_t* ___Xmlns_15;
	// System.Xml.XmlResolver System.Xml.Schema.SchemaCollectionPreprocessor::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_16;

public:
	inline static int32_t get_offset_of_schema_6() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___schema_6)); }
	inline XmlSchema_t880472818 * get_schema_6() const { return ___schema_6; }
	inline XmlSchema_t880472818 ** get_address_of_schema_6() { return &___schema_6; }
	inline void set_schema_6(XmlSchema_t880472818 * value)
	{
		___schema_6 = value;
		Il2CppCodeGenWriteBarrier(&___schema_6, value);
	}

	inline static int32_t get_offset_of_targetNamespace_7() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___targetNamespace_7)); }
	inline String_t* get_targetNamespace_7() const { return ___targetNamespace_7; }
	inline String_t** get_address_of_targetNamespace_7() { return &___targetNamespace_7; }
	inline void set_targetNamespace_7(String_t* value)
	{
		___targetNamespace_7 = value;
		Il2CppCodeGenWriteBarrier(&___targetNamespace_7, value);
	}

	inline static int32_t get_offset_of_buildinIncluded_8() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___buildinIncluded_8)); }
	inline bool get_buildinIncluded_8() const { return ___buildinIncluded_8; }
	inline bool* get_address_of_buildinIncluded_8() { return &___buildinIncluded_8; }
	inline void set_buildinIncluded_8(bool value)
	{
		___buildinIncluded_8 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_9() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___elementFormDefault_9)); }
	inline int32_t get_elementFormDefault_9() const { return ___elementFormDefault_9; }
	inline int32_t* get_address_of_elementFormDefault_9() { return &___elementFormDefault_9; }
	inline void set_elementFormDefault_9(int32_t value)
	{
		___elementFormDefault_9 = value;
	}

	inline static int32_t get_offset_of_attributeFormDefault_10() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___attributeFormDefault_10)); }
	inline int32_t get_attributeFormDefault_10() const { return ___attributeFormDefault_10; }
	inline int32_t* get_address_of_attributeFormDefault_10() { return &___attributeFormDefault_10; }
	inline void set_attributeFormDefault_10(int32_t value)
	{
		___attributeFormDefault_10 = value;
	}

	inline static int32_t get_offset_of_blockDefault_11() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___blockDefault_11)); }
	inline int32_t get_blockDefault_11() const { return ___blockDefault_11; }
	inline int32_t* get_address_of_blockDefault_11() { return &___blockDefault_11; }
	inline void set_blockDefault_11(int32_t value)
	{
		___blockDefault_11 = value;
	}

	inline static int32_t get_offset_of_finalDefault_12() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___finalDefault_12)); }
	inline int32_t get_finalDefault_12() const { return ___finalDefault_12; }
	inline int32_t* get_address_of_finalDefault_12() { return &___finalDefault_12; }
	inline void set_finalDefault_12(int32_t value)
	{
		___finalDefault_12 = value;
	}

	inline static int32_t get_offset_of_schemaLocations_13() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___schemaLocations_13)); }
	inline Hashtable_t909839986 * get_schemaLocations_13() const { return ___schemaLocations_13; }
	inline Hashtable_t909839986 ** get_address_of_schemaLocations_13() { return &___schemaLocations_13; }
	inline void set_schemaLocations_13(Hashtable_t909839986 * value)
	{
		___schemaLocations_13 = value;
		Il2CppCodeGenWriteBarrier(&___schemaLocations_13, value);
	}

	inline static int32_t get_offset_of_referenceNamespaces_14() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___referenceNamespaces_14)); }
	inline Hashtable_t909839986 * get_referenceNamespaces_14() const { return ___referenceNamespaces_14; }
	inline Hashtable_t909839986 ** get_address_of_referenceNamespaces_14() { return &___referenceNamespaces_14; }
	inline void set_referenceNamespaces_14(Hashtable_t909839986 * value)
	{
		___referenceNamespaces_14 = value;
		Il2CppCodeGenWriteBarrier(&___referenceNamespaces_14, value);
	}

	inline static int32_t get_offset_of_Xmlns_15() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___Xmlns_15)); }
	inline String_t* get_Xmlns_15() const { return ___Xmlns_15; }
	inline String_t** get_address_of_Xmlns_15() { return &___Xmlns_15; }
	inline void set_Xmlns_15(String_t* value)
	{
		___Xmlns_15 = value;
		Il2CppCodeGenWriteBarrier(&___Xmlns_15, value);
	}

	inline static int32_t get_offset_of_xmlResolver_16() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___xmlResolver_16)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_16() const { return ___xmlResolver_16; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_16() { return &___xmlResolver_16; }
	inline void set_xmlResolver_16(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_16 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
