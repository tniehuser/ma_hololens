﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[]
struct BinaryTypeEnumU5BU5D_t1351797255;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo
struct ReadObjectInfo_t1645661573;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
struct ObjectReader_t1476095226;
// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo
struct BinaryAssemblyInfo_t316080507;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectMap
struct  ObjectMap_t3191050891  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.ObjectMap::objectName
	String_t* ___objectName_0;
	// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectMap::objectType
	Type_t * ___objectType_1;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[] System.Runtime.Serialization.Formatters.Binary.ObjectMap::binaryTypeEnumA
	BinaryTypeEnumU5BU5D_t1351797255* ___binaryTypeEnumA_2;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.ObjectMap::typeInformationA
	ObjectU5BU5D_t3614634134* ___typeInformationA_3;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.ObjectMap::memberTypes
	TypeU5BU5D_t1664964607* ___memberTypes_4;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.ObjectMap::memberNames
	StringU5BU5D_t1642385972* ___memberNames_5;
	// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo System.Runtime.Serialization.Formatters.Binary.ObjectMap::objectInfo
	ReadObjectInfo_t1645661573 * ___objectInfo_6;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectMap::isInitObjectInfo
	bool ___isInitObjectInfo_7;
	// System.Runtime.Serialization.Formatters.Binary.ObjectReader System.Runtime.Serialization.Formatters.Binary.ObjectMap::objectReader
	ObjectReader_t1476095226 * ___objectReader_8;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectMap::objectId
	int32_t ___objectId_9;
	// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo System.Runtime.Serialization.Formatters.Binary.ObjectMap::assemblyInfo
	BinaryAssemblyInfo_t316080507 * ___assemblyInfo_10;

public:
	inline static int32_t get_offset_of_objectName_0() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___objectName_0)); }
	inline String_t* get_objectName_0() const { return ___objectName_0; }
	inline String_t** get_address_of_objectName_0() { return &___objectName_0; }
	inline void set_objectName_0(String_t* value)
	{
		___objectName_0 = value;
		Il2CppCodeGenWriteBarrier(&___objectName_0, value);
	}

	inline static int32_t get_offset_of_objectType_1() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___objectType_1)); }
	inline Type_t * get_objectType_1() const { return ___objectType_1; }
	inline Type_t ** get_address_of_objectType_1() { return &___objectType_1; }
	inline void set_objectType_1(Type_t * value)
	{
		___objectType_1 = value;
		Il2CppCodeGenWriteBarrier(&___objectType_1, value);
	}

	inline static int32_t get_offset_of_binaryTypeEnumA_2() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___binaryTypeEnumA_2)); }
	inline BinaryTypeEnumU5BU5D_t1351797255* get_binaryTypeEnumA_2() const { return ___binaryTypeEnumA_2; }
	inline BinaryTypeEnumU5BU5D_t1351797255** get_address_of_binaryTypeEnumA_2() { return &___binaryTypeEnumA_2; }
	inline void set_binaryTypeEnumA_2(BinaryTypeEnumU5BU5D_t1351797255* value)
	{
		___binaryTypeEnumA_2 = value;
		Il2CppCodeGenWriteBarrier(&___binaryTypeEnumA_2, value);
	}

	inline static int32_t get_offset_of_typeInformationA_3() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___typeInformationA_3)); }
	inline ObjectU5BU5D_t3614634134* get_typeInformationA_3() const { return ___typeInformationA_3; }
	inline ObjectU5BU5D_t3614634134** get_address_of_typeInformationA_3() { return &___typeInformationA_3; }
	inline void set_typeInformationA_3(ObjectU5BU5D_t3614634134* value)
	{
		___typeInformationA_3 = value;
		Il2CppCodeGenWriteBarrier(&___typeInformationA_3, value);
	}

	inline static int32_t get_offset_of_memberTypes_4() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___memberTypes_4)); }
	inline TypeU5BU5D_t1664964607* get_memberTypes_4() const { return ___memberTypes_4; }
	inline TypeU5BU5D_t1664964607** get_address_of_memberTypes_4() { return &___memberTypes_4; }
	inline void set_memberTypes_4(TypeU5BU5D_t1664964607* value)
	{
		___memberTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_4, value);
	}

	inline static int32_t get_offset_of_memberNames_5() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___memberNames_5)); }
	inline StringU5BU5D_t1642385972* get_memberNames_5() const { return ___memberNames_5; }
	inline StringU5BU5D_t1642385972** get_address_of_memberNames_5() { return &___memberNames_5; }
	inline void set_memberNames_5(StringU5BU5D_t1642385972* value)
	{
		___memberNames_5 = value;
		Il2CppCodeGenWriteBarrier(&___memberNames_5, value);
	}

	inline static int32_t get_offset_of_objectInfo_6() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___objectInfo_6)); }
	inline ReadObjectInfo_t1645661573 * get_objectInfo_6() const { return ___objectInfo_6; }
	inline ReadObjectInfo_t1645661573 ** get_address_of_objectInfo_6() { return &___objectInfo_6; }
	inline void set_objectInfo_6(ReadObjectInfo_t1645661573 * value)
	{
		___objectInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___objectInfo_6, value);
	}

	inline static int32_t get_offset_of_isInitObjectInfo_7() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___isInitObjectInfo_7)); }
	inline bool get_isInitObjectInfo_7() const { return ___isInitObjectInfo_7; }
	inline bool* get_address_of_isInitObjectInfo_7() { return &___isInitObjectInfo_7; }
	inline void set_isInitObjectInfo_7(bool value)
	{
		___isInitObjectInfo_7 = value;
	}

	inline static int32_t get_offset_of_objectReader_8() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___objectReader_8)); }
	inline ObjectReader_t1476095226 * get_objectReader_8() const { return ___objectReader_8; }
	inline ObjectReader_t1476095226 ** get_address_of_objectReader_8() { return &___objectReader_8; }
	inline void set_objectReader_8(ObjectReader_t1476095226 * value)
	{
		___objectReader_8 = value;
		Il2CppCodeGenWriteBarrier(&___objectReader_8, value);
	}

	inline static int32_t get_offset_of_objectId_9() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___objectId_9)); }
	inline int32_t get_objectId_9() const { return ___objectId_9; }
	inline int32_t* get_address_of_objectId_9() { return &___objectId_9; }
	inline void set_objectId_9(int32_t value)
	{
		___objectId_9 = value;
	}

	inline static int32_t get_offset_of_assemblyInfo_10() { return static_cast<int32_t>(offsetof(ObjectMap_t3191050891, ___assemblyInfo_10)); }
	inline BinaryAssemblyInfo_t316080507 * get_assemblyInfo_10() const { return ___assemblyInfo_10; }
	inline BinaryAssemblyInfo_t316080507 ** get_address_of_assemblyInfo_10() { return &___assemblyInfo_10; }
	inline void set_assemblyInfo_10(BinaryAssemblyInfo_t316080507 * value)
	{
		___assemblyInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyInfo_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
