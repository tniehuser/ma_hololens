﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.HeaderVariantInfo[]
struct HeaderVariantInfoU5BU5D_t1615036753;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieContainer
struct  CookieContainer_t2808809223  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Net.CookieContainer::m_domainTable
	Hashtable_t909839986 * ___m_domainTable_1;
	// System.Int32 System.Net.CookieContainer::m_maxCookieSize
	int32_t ___m_maxCookieSize_2;
	// System.Int32 System.Net.CookieContainer::m_maxCookies
	int32_t ___m_maxCookies_3;
	// System.Int32 System.Net.CookieContainer::m_maxCookiesPerDomain
	int32_t ___m_maxCookiesPerDomain_4;
	// System.Int32 System.Net.CookieContainer::m_count
	int32_t ___m_count_5;
	// System.String System.Net.CookieContainer::m_fqdnMyDomain
	String_t* ___m_fqdnMyDomain_6;

public:
	inline static int32_t get_offset_of_m_domainTable_1() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_domainTable_1)); }
	inline Hashtable_t909839986 * get_m_domainTable_1() const { return ___m_domainTable_1; }
	inline Hashtable_t909839986 ** get_address_of_m_domainTable_1() { return &___m_domainTable_1; }
	inline void set_m_domainTable_1(Hashtable_t909839986 * value)
	{
		___m_domainTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_domainTable_1, value);
	}

	inline static int32_t get_offset_of_m_maxCookieSize_2() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_maxCookieSize_2)); }
	inline int32_t get_m_maxCookieSize_2() const { return ___m_maxCookieSize_2; }
	inline int32_t* get_address_of_m_maxCookieSize_2() { return &___m_maxCookieSize_2; }
	inline void set_m_maxCookieSize_2(int32_t value)
	{
		___m_maxCookieSize_2 = value;
	}

	inline static int32_t get_offset_of_m_maxCookies_3() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_maxCookies_3)); }
	inline int32_t get_m_maxCookies_3() const { return ___m_maxCookies_3; }
	inline int32_t* get_address_of_m_maxCookies_3() { return &___m_maxCookies_3; }
	inline void set_m_maxCookies_3(int32_t value)
	{
		___m_maxCookies_3 = value;
	}

	inline static int32_t get_offset_of_m_maxCookiesPerDomain_4() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_maxCookiesPerDomain_4)); }
	inline int32_t get_m_maxCookiesPerDomain_4() const { return ___m_maxCookiesPerDomain_4; }
	inline int32_t* get_address_of_m_maxCookiesPerDomain_4() { return &___m_maxCookiesPerDomain_4; }
	inline void set_m_maxCookiesPerDomain_4(int32_t value)
	{
		___m_maxCookiesPerDomain_4 = value;
	}

	inline static int32_t get_offset_of_m_count_5() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_count_5)); }
	inline int32_t get_m_count_5() const { return ___m_count_5; }
	inline int32_t* get_address_of_m_count_5() { return &___m_count_5; }
	inline void set_m_count_5(int32_t value)
	{
		___m_count_5 = value;
	}

	inline static int32_t get_offset_of_m_fqdnMyDomain_6() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223, ___m_fqdnMyDomain_6)); }
	inline String_t* get_m_fqdnMyDomain_6() const { return ___m_fqdnMyDomain_6; }
	inline String_t** get_address_of_m_fqdnMyDomain_6() { return &___m_fqdnMyDomain_6; }
	inline void set_m_fqdnMyDomain_6(String_t* value)
	{
		___m_fqdnMyDomain_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_fqdnMyDomain_6, value);
	}
};

struct CookieContainer_t2808809223_StaticFields
{
public:
	// System.Net.HeaderVariantInfo[] System.Net.CookieContainer::HeaderInfo
	HeaderVariantInfoU5BU5D_t1615036753* ___HeaderInfo_0;

public:
	inline static int32_t get_offset_of_HeaderInfo_0() { return static_cast<int32_t>(offsetof(CookieContainer_t2808809223_StaticFields, ___HeaderInfo_0)); }
	inline HeaderVariantInfoU5BU5D_t1615036753* get_HeaderInfo_0() const { return ___HeaderInfo_0; }
	inline HeaderVariantInfoU5BU5D_t1615036753** get_address_of_HeaderInfo_0() { return &___HeaderInfo_0; }
	inline void set_HeaderInfo_0(HeaderVariantInfoU5BU5D_t1615036753* value)
	{
		___HeaderInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderInfo_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
