﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1383558988.h"

// System.Array
struct Il2CppArray;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Object
struct Il2CppObject;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo
struct ReadObjectInfo_t1645661573;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ValueFixup
struct  ValueFixup_t1348745115  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.ValueFixupEnum System.Runtime.Serialization.Formatters.Binary.ValueFixup::valueFixupEnum
	int32_t ___valueFixupEnum_0;
	// System.Array System.Runtime.Serialization.Formatters.Binary.ValueFixup::arrayObj
	Il2CppArray * ___arrayObj_1;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.ValueFixup::indexMap
	Int32U5BU5D_t3030399641* ___indexMap_2;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ValueFixup::header
	Il2CppObject * ___header_3;
	// System.Object System.Runtime.Serialization.Formatters.Binary.ValueFixup::memberObject
	Il2CppObject * ___memberObject_4;
	// System.Runtime.Serialization.Formatters.Binary.ReadObjectInfo System.Runtime.Serialization.Formatters.Binary.ValueFixup::objectInfo
	ReadObjectInfo_t1645661573 * ___objectInfo_6;
	// System.String System.Runtime.Serialization.Formatters.Binary.ValueFixup::memberName
	String_t* ___memberName_7;

public:
	inline static int32_t get_offset_of_valueFixupEnum_0() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___valueFixupEnum_0)); }
	inline int32_t get_valueFixupEnum_0() const { return ___valueFixupEnum_0; }
	inline int32_t* get_address_of_valueFixupEnum_0() { return &___valueFixupEnum_0; }
	inline void set_valueFixupEnum_0(int32_t value)
	{
		___valueFixupEnum_0 = value;
	}

	inline static int32_t get_offset_of_arrayObj_1() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___arrayObj_1)); }
	inline Il2CppArray * get_arrayObj_1() const { return ___arrayObj_1; }
	inline Il2CppArray ** get_address_of_arrayObj_1() { return &___arrayObj_1; }
	inline void set_arrayObj_1(Il2CppArray * value)
	{
		___arrayObj_1 = value;
		Il2CppCodeGenWriteBarrier(&___arrayObj_1, value);
	}

	inline static int32_t get_offset_of_indexMap_2() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___indexMap_2)); }
	inline Int32U5BU5D_t3030399641* get_indexMap_2() const { return ___indexMap_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_indexMap_2() { return &___indexMap_2; }
	inline void set_indexMap_2(Int32U5BU5D_t3030399641* value)
	{
		___indexMap_2 = value;
		Il2CppCodeGenWriteBarrier(&___indexMap_2, value);
	}

	inline static int32_t get_offset_of_header_3() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___header_3)); }
	inline Il2CppObject * get_header_3() const { return ___header_3; }
	inline Il2CppObject ** get_address_of_header_3() { return &___header_3; }
	inline void set_header_3(Il2CppObject * value)
	{
		___header_3 = value;
		Il2CppCodeGenWriteBarrier(&___header_3, value);
	}

	inline static int32_t get_offset_of_memberObject_4() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___memberObject_4)); }
	inline Il2CppObject * get_memberObject_4() const { return ___memberObject_4; }
	inline Il2CppObject ** get_address_of_memberObject_4() { return &___memberObject_4; }
	inline void set_memberObject_4(Il2CppObject * value)
	{
		___memberObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___memberObject_4, value);
	}

	inline static int32_t get_offset_of_objectInfo_6() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___objectInfo_6)); }
	inline ReadObjectInfo_t1645661573 * get_objectInfo_6() const { return ___objectInfo_6; }
	inline ReadObjectInfo_t1645661573 ** get_address_of_objectInfo_6() { return &___objectInfo_6; }
	inline void set_objectInfo_6(ReadObjectInfo_t1645661573 * value)
	{
		___objectInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___objectInfo_6, value);
	}

	inline static int32_t get_offset_of_memberName_7() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115, ___memberName_7)); }
	inline String_t* get_memberName_7() const { return ___memberName_7; }
	inline String_t** get_address_of_memberName_7() { return &___memberName_7; }
	inline void set_memberName_7(String_t* value)
	{
		___memberName_7 = value;
		Il2CppCodeGenWriteBarrier(&___memberName_7, value);
	}
};

struct ValueFixup_t1348745115_StaticFields
{
public:
	// System.Reflection.MemberInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.ValueFixup::valueInfo
	MemberInfo_t * ___valueInfo_5;

public:
	inline static int32_t get_offset_of_valueInfo_5() { return static_cast<int32_t>(offsetof(ValueFixup_t1348745115_StaticFields, ___valueInfo_5)); }
	inline MemberInfo_t * get_valueInfo_5() const { return ___valueInfo_5; }
	inline MemberInfo_t ** get_address_of_valueInfo_5() { return &___valueInfo_5; }
	inline void set_valueInfo_5(MemberInfo_t * value)
	{
		___valueInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___valueInfo_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
