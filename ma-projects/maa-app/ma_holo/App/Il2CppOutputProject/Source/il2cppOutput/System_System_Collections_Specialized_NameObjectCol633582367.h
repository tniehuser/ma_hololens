﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t2034248631;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct  KeysCollection_t633582367  : public Il2CppObject
{
public:
	// System.Collections.Specialized.NameObjectCollectionBase System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::_coll
	NameObjectCollectionBase_t2034248631 * ____coll_0;

public:
	inline static int32_t get_offset_of__coll_0() { return static_cast<int32_t>(offsetof(KeysCollection_t633582367, ____coll_0)); }
	inline NameObjectCollectionBase_t2034248631 * get__coll_0() const { return ____coll_0; }
	inline NameObjectCollectionBase_t2034248631 ** get_address_of__coll_0() { return &____coll_0; }
	inline void set__coll_0(NameObjectCollectionBase_t2034248631 * value)
	{
		____coll_0 = value;
		Il2CppCodeGenWriteBarrier(&____coll_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
