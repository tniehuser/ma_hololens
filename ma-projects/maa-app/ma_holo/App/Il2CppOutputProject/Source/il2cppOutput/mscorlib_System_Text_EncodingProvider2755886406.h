﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Text.EncodingProvider[]
struct EncodingProviderU5BU5D_t962492131;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncodingProvider
struct  EncodingProvider_t2755886406  : public Il2CppObject
{
public:

public:
};

struct EncodingProvider_t2755886406_StaticFields
{
public:
	// System.Object System.Text.EncodingProvider::s_InternalSyncObject
	Il2CppObject * ___s_InternalSyncObject_0;
	// System.Text.EncodingProvider[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncodingProvider::s_providers
	EncodingProviderU5BU5D_t962492131* ___s_providers_1;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_0() { return static_cast<int32_t>(offsetof(EncodingProvider_t2755886406_StaticFields, ___s_InternalSyncObject_0)); }
	inline Il2CppObject * get_s_InternalSyncObject_0() const { return ___s_InternalSyncObject_0; }
	inline Il2CppObject ** get_address_of_s_InternalSyncObject_0() { return &___s_InternalSyncObject_0; }
	inline void set_s_InternalSyncObject_0(Il2CppObject * value)
	{
		___s_InternalSyncObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_InternalSyncObject_0, value);
	}

	inline static int32_t get_offset_of_s_providers_1() { return static_cast<int32_t>(offsetof(EncodingProvider_t2755886406_StaticFields, ___s_providers_1)); }
	inline EncodingProviderU5BU5D_t962492131* get_s_providers_1() const { return ___s_providers_1; }
	inline EncodingProviderU5BU5D_t962492131** get_address_of_s_providers_1() { return &___s_providers_1; }
	inline void set_s_providers_1(EncodingProviderU5BU5D_t962492131* value)
	{
		___s_providers_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_providers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
