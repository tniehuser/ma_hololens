﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t339325318;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.Cache.XPathNodePageInfo
struct  XPathNodePageInfo_t617838178  : public Il2CppObject
{
public:
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::pageNum
	int32_t ___pageNum_0;
	// System.Int32 MS.Internal.Xml.Cache.XPathNodePageInfo::nodeCount
	int32_t ___nodeCount_1;
	// MS.Internal.Xml.Cache.XPathNode[] MS.Internal.Xml.Cache.XPathNodePageInfo::pageNext
	XPathNodeU5BU5D_t339325318* ___pageNext_2;

public:
	inline static int32_t get_offset_of_pageNum_0() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t617838178, ___pageNum_0)); }
	inline int32_t get_pageNum_0() const { return ___pageNum_0; }
	inline int32_t* get_address_of_pageNum_0() { return &___pageNum_0; }
	inline void set_pageNum_0(int32_t value)
	{
		___pageNum_0 = value;
	}

	inline static int32_t get_offset_of_nodeCount_1() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t617838178, ___nodeCount_1)); }
	inline int32_t get_nodeCount_1() const { return ___nodeCount_1; }
	inline int32_t* get_address_of_nodeCount_1() { return &___nodeCount_1; }
	inline void set_nodeCount_1(int32_t value)
	{
		___nodeCount_1 = value;
	}

	inline static int32_t get_offset_of_pageNext_2() { return static_cast<int32_t>(offsetof(XPathNodePageInfo_t617838178, ___pageNext_2)); }
	inline XPathNodeU5BU5D_t339325318* get_pageNext_2() const { return ___pageNext_2; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageNext_2() { return &___pageNext_2; }
	inline void set_pageNext_2(XPathNodeU5BU5D_t339325318* value)
	{
		___pageNext_2 = value;
		Il2CppCodeGenWriteBarrier(&___pageNext_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
