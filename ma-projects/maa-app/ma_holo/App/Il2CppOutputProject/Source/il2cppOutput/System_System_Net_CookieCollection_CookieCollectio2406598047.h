﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.CookieCollection
struct CookieCollection_t521422364;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection/CookieCollectionEnumerator
struct  CookieCollectionEnumerator_t2406598047  : public Il2CppObject
{
public:
	// System.Net.CookieCollection System.Net.CookieCollection/CookieCollectionEnumerator::m_cookies
	CookieCollection_t521422364 * ___m_cookies_0;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_count
	int32_t ___m_count_1;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_index
	int32_t ___m_index_2;
	// System.Int32 System.Net.CookieCollection/CookieCollectionEnumerator::m_version
	int32_t ___m_version_3;

public:
	inline static int32_t get_offset_of_m_cookies_0() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_t2406598047, ___m_cookies_0)); }
	inline CookieCollection_t521422364 * get_m_cookies_0() const { return ___m_cookies_0; }
	inline CookieCollection_t521422364 ** get_address_of_m_cookies_0() { return &___m_cookies_0; }
	inline void set_m_cookies_0(CookieCollection_t521422364 * value)
	{
		___m_cookies_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_cookies_0, value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_t2406598047, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_index_2() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_t2406598047, ___m_index_2)); }
	inline int32_t get_m_index_2() const { return ___m_index_2; }
	inline int32_t* get_address_of_m_index_2() { return &___m_index_2; }
	inline void set_m_index_2(int32_t value)
	{
		___m_index_2 = value;
	}

	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_t2406598047, ___m_version_3)); }
	inline int32_t get_m_version_3() const { return ___m_version_3; }
	inline int32_t* get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(int32_t value)
	{
		___m_version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
