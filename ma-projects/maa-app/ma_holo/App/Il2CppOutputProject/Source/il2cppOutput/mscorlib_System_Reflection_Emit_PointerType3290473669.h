﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_Emit_SymbolType2424957512.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.PointerType
struct  PointerType_t3290473669  : public SymbolType_t2424957512
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
