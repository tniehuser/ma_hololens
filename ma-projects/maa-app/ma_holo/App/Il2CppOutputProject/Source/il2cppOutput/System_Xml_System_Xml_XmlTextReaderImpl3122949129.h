﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingSta1278724163.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingFunc788942260.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_DtdProcessing2734370679.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_Incremental232776713.h"
#include "System_Xml_System_Xml_LineInfo1429635508.h"
#include "System_Xml_System_Xml_XmlTextReaderImpl_ParsingMod3052286627.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"

// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>>
struct Task_1_t2404255042;
// System.Xml.XmlTextReaderImpl/LaterInitParam
struct LaterInitParam_t3713317471;
// System.Xml.XmlTextReaderImpl/NodeData[]
struct NodeDataU5BU5D_t3671804373;
// System.Xml.XmlTextReaderImpl/NodeData
struct NodeData_t2613273532;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.String
struct String_t;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.XmlTextReaderImpl/XmlContext
struct XmlContext_t938582012;
// System.Xml.XmlTextReaderImpl/ParsingState[]
struct ParsingStateU5BU5D_t1709225810;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Xml.IDtdInfo
struct IDtdInfo_t671093251;
// System.Xml.XmlParserContext
struct XmlParserContext_t2728039553;
// System.Xml.IncrementalReadDecoder
struct IncrementalReadDecoder_t403822042;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t482152337;
// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate
struct OnDefaultAttributeUseDelegate_t3539809886;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Xml.IDtdEntityInfo
struct IDtdEntityInfo_t4127388056;
// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo>
struct Dictionary_2_t3860214507;
// System.Xml.XmlReader
struct XmlReader_t3675626668;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl
struct  XmlTextReaderImpl_t3122949129  : public XmlReader_t3675626668
{
public:
	// System.Threading.Tasks.Task`1<System.Tuple`4<System.Int32,System.Int32,System.Int32,System.Boolean>> System.Xml.XmlTextReaderImpl::parseText_dummyTask
	Task_1_t2404255042 * ___parseText_dummyTask_3;
	// System.Xml.XmlTextReaderImpl/LaterInitParam System.Xml.XmlTextReaderImpl::laterInitParam
	LaterInitParam_t3713317471 * ___laterInitParam_4;
	// System.Xml.XmlCharType System.Xml.XmlTextReaderImpl::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_5;
	// System.Xml.XmlTextReaderImpl/ParsingState System.Xml.XmlTextReaderImpl::ps
	ParsingState_t1278724163  ___ps_6;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::parsingFunction
	int32_t ___parsingFunction_7;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextParsingFunction
	int32_t ___nextParsingFunction_8;
	// System.Xml.XmlTextReaderImpl/ParsingFunction System.Xml.XmlTextReaderImpl::nextNextParsingFunction
	int32_t ___nextNextParsingFunction_9;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::nodes
	NodeDataU5BU5D_t3671804373* ___nodes_10;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl::curNode
	NodeData_t2613273532 * ___curNode_11;
	// System.Int32 System.Xml.XmlTextReaderImpl::index
	int32_t ___index_12;
	// System.Int32 System.Xml.XmlTextReaderImpl::curAttrIndex
	int32_t ___curAttrIndex_13;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrCount
	int32_t ___attrCount_14;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrHashtable
	int32_t ___attrHashtable_15;
	// System.Int32 System.Xml.XmlTextReaderImpl::attrDuplWalkCount
	int32_t ___attrDuplWalkCount_16;
	// System.Boolean System.Xml.XmlTextReaderImpl::attrNeedNamespaceLookup
	bool ___attrNeedNamespaceLookup_17;
	// System.Boolean System.Xml.XmlTextReaderImpl::fullAttrCleanup
	bool ___fullAttrCleanup_18;
	// System.Xml.XmlTextReaderImpl/NodeData[] System.Xml.XmlTextReaderImpl::attrDuplSortingArray
	NodeDataU5BU5D_t3671804373* ___attrDuplSortingArray_19;
	// System.Xml.XmlNameTable System.Xml.XmlTextReaderImpl::nameTable
	XmlNameTable_t1345805268 * ___nameTable_20;
	// System.Boolean System.Xml.XmlTextReaderImpl::nameTableFromSettings
	bool ___nameTableFromSettings_21;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_22;
	// System.String System.Xml.XmlTextReaderImpl::url
	String_t* ___url_23;
	// System.Boolean System.Xml.XmlTextReaderImpl::normalize
	bool ___normalize_24;
	// System.Boolean System.Xml.XmlTextReaderImpl::supportNamespaces
	bool ___supportNamespaces_25;
	// System.Xml.WhitespaceHandling System.Xml.XmlTextReaderImpl::whitespaceHandling
	int32_t ___whitespaceHandling_26;
	// System.Xml.DtdProcessing System.Xml.XmlTextReaderImpl::dtdProcessing
	int32_t ___dtdProcessing_27;
	// System.Xml.EntityHandling System.Xml.XmlTextReaderImpl::entityHandling
	int32_t ___entityHandling_28;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignorePIs
	bool ___ignorePIs_29;
	// System.Boolean System.Xml.XmlTextReaderImpl::ignoreComments
	bool ___ignoreComments_30;
	// System.Boolean System.Xml.XmlTextReaderImpl::checkCharacters
	bool ___checkCharacters_31;
	// System.Int32 System.Xml.XmlTextReaderImpl::lineNumberOffset
	int32_t ___lineNumberOffset_32;
	// System.Int32 System.Xml.XmlTextReaderImpl::linePositionOffset
	int32_t ___linePositionOffset_33;
	// System.Boolean System.Xml.XmlTextReaderImpl::closeInput
	bool ___closeInput_34;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersInDocument
	int64_t ___maxCharactersInDocument_35;
	// System.Int64 System.Xml.XmlTextReaderImpl::maxCharactersFromEntities
	int64_t ___maxCharactersFromEntities_36;
	// System.Boolean System.Xml.XmlTextReaderImpl::v1Compat
	bool ___v1Compat_37;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextReaderImpl::namespaceManager
	XmlNamespaceManager_t486731501 * ___namespaceManager_38;
	// System.String System.Xml.XmlTextReaderImpl::lastPrefix
	String_t* ___lastPrefix_39;
	// System.Xml.XmlTextReaderImpl/XmlContext System.Xml.XmlTextReaderImpl::xmlContext
	XmlContext_t938582012 * ___xmlContext_40;
	// System.Xml.XmlTextReaderImpl/ParsingState[] System.Xml.XmlTextReaderImpl::parsingStatesStack
	ParsingStateU5BU5D_t1709225810* ___parsingStatesStack_41;
	// System.Int32 System.Xml.XmlTextReaderImpl::parsingStatesStackTop
	int32_t ___parsingStatesStackTop_42;
	// System.String System.Xml.XmlTextReaderImpl::reportedBaseUri
	String_t* ___reportedBaseUri_43;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl::reportedEncoding
	Encoding_t663144255 * ___reportedEncoding_44;
	// System.Xml.IDtdInfo System.Xml.XmlTextReaderImpl::dtdInfo
	Il2CppObject * ___dtdInfo_45;
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl::fragmentType
	int32_t ___fragmentType_46;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl::fragmentParserContext
	XmlParserContext_t2728039553 * ___fragmentParserContext_47;
	// System.Boolean System.Xml.XmlTextReaderImpl::fragment
	bool ___fragment_48;
	// System.Xml.IncrementalReadDecoder System.Xml.XmlTextReaderImpl::incReadDecoder
	IncrementalReadDecoder_t403822042 * ___incReadDecoder_49;
	// System.Xml.XmlTextReaderImpl/IncrementalReadState System.Xml.XmlTextReaderImpl::incReadState
	int32_t ___incReadState_50;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl::incReadLineInfo
	LineInfo_t1429635508  ___incReadLineInfo_51;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadDepth
	int32_t ___incReadDepth_52;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftStartPos
	int32_t ___incReadLeftStartPos_53;
	// System.Int32 System.Xml.XmlTextReaderImpl::incReadLeftEndPos
	int32_t ___incReadLeftEndPos_54;
	// System.Int32 System.Xml.XmlTextReaderImpl::attributeValueBaseEntityId
	int32_t ___attributeValueBaseEntityId_55;
	// System.Boolean System.Xml.XmlTextReaderImpl::emptyEntityInAttributeResolved
	bool ___emptyEntityInAttributeResolved_56;
	// System.Xml.IValidationEventHandling System.Xml.XmlTextReaderImpl::validationEventHandling
	Il2CppObject * ___validationEventHandling_57;
	// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate System.Xml.XmlTextReaderImpl::onDefaultAttributeUse
	OnDefaultAttributeUseDelegate_t3539809886 * ___onDefaultAttributeUse_58;
	// System.Boolean System.Xml.XmlTextReaderImpl::validatingReaderCompatFlag
	bool ___validatingReaderCompatFlag_59;
	// System.Boolean System.Xml.XmlTextReaderImpl::addDefaultAttributesAndNormalize
	bool ___addDefaultAttributesAndNormalize_60;
	// System.Text.StringBuilder System.Xml.XmlTextReaderImpl::stringBuilder
	StringBuilder_t1221177846 * ___stringBuilder_61;
	// System.Boolean System.Xml.XmlTextReaderImpl::rootElementParsed
	bool ___rootElementParsed_62;
	// System.Boolean System.Xml.XmlTextReaderImpl::standalone
	bool ___standalone_63;
	// System.Int32 System.Xml.XmlTextReaderImpl::nextEntityId
	int32_t ___nextEntityId_64;
	// System.Xml.XmlTextReaderImpl/ParsingMode System.Xml.XmlTextReaderImpl::parsingMode
	int32_t ___parsingMode_65;
	// System.Xml.ReadState System.Xml.XmlTextReaderImpl::readState
	int32_t ___readState_66;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl::lastEntity
	Il2CppObject * ___lastEntity_67;
	// System.Boolean System.Xml.XmlTextReaderImpl::afterResetState
	bool ___afterResetState_68;
	// System.Int32 System.Xml.XmlTextReaderImpl::documentStartBytePos
	int32_t ___documentStartBytePos_69;
	// System.Int32 System.Xml.XmlTextReaderImpl::readValueOffset
	int32_t ___readValueOffset_70;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersInDocument
	int64_t ___charactersInDocument_71;
	// System.Int64 System.Xml.XmlTextReaderImpl::charactersFromEntities
	int64_t ___charactersFromEntities_72;
	// System.Collections.Generic.Dictionary`2<System.Xml.IDtdEntityInfo,System.Xml.IDtdEntityInfo> System.Xml.XmlTextReaderImpl::currentEntities
	Dictionary_2_t3860214507 * ___currentEntities_73;
	// System.Boolean System.Xml.XmlTextReaderImpl::disableUndeclaredEntityCheck
	bool ___disableUndeclaredEntityCheck_74;
	// System.Xml.XmlReader System.Xml.XmlTextReaderImpl::outerReader
	XmlReader_t3675626668 * ___outerReader_75;
	// System.Boolean System.Xml.XmlTextReaderImpl::xmlResolverIsSet
	bool ___xmlResolverIsSet_76;
	// System.String System.Xml.XmlTextReaderImpl::Xml
	String_t* ___Xml_77;
	// System.String System.Xml.XmlTextReaderImpl::XmlNs
	String_t* ___XmlNs_78;

public:
	inline static int32_t get_offset_of_parseText_dummyTask_3() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___parseText_dummyTask_3)); }
	inline Task_1_t2404255042 * get_parseText_dummyTask_3() const { return ___parseText_dummyTask_3; }
	inline Task_1_t2404255042 ** get_address_of_parseText_dummyTask_3() { return &___parseText_dummyTask_3; }
	inline void set_parseText_dummyTask_3(Task_1_t2404255042 * value)
	{
		___parseText_dummyTask_3 = value;
		Il2CppCodeGenWriteBarrier(&___parseText_dummyTask_3, value);
	}

	inline static int32_t get_offset_of_laterInitParam_4() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___laterInitParam_4)); }
	inline LaterInitParam_t3713317471 * get_laterInitParam_4() const { return ___laterInitParam_4; }
	inline LaterInitParam_t3713317471 ** get_address_of_laterInitParam_4() { return &___laterInitParam_4; }
	inline void set_laterInitParam_4(LaterInitParam_t3713317471 * value)
	{
		___laterInitParam_4 = value;
		Il2CppCodeGenWriteBarrier(&___laterInitParam_4, value);
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___xmlCharType_5)); }
	inline XmlCharType_t1050521405  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t1050521405  value)
	{
		___xmlCharType_5 = value;
	}

	inline static int32_t get_offset_of_ps_6() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___ps_6)); }
	inline ParsingState_t1278724163  get_ps_6() const { return ___ps_6; }
	inline ParsingState_t1278724163 * get_address_of_ps_6() { return &___ps_6; }
	inline void set_ps_6(ParsingState_t1278724163  value)
	{
		___ps_6 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_7() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___parsingFunction_7)); }
	inline int32_t get_parsingFunction_7() const { return ___parsingFunction_7; }
	inline int32_t* get_address_of_parsingFunction_7() { return &___parsingFunction_7; }
	inline void set_parsingFunction_7(int32_t value)
	{
		___parsingFunction_7 = value;
	}

	inline static int32_t get_offset_of_nextParsingFunction_8() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nextParsingFunction_8)); }
	inline int32_t get_nextParsingFunction_8() const { return ___nextParsingFunction_8; }
	inline int32_t* get_address_of_nextParsingFunction_8() { return &___nextParsingFunction_8; }
	inline void set_nextParsingFunction_8(int32_t value)
	{
		___nextParsingFunction_8 = value;
	}

	inline static int32_t get_offset_of_nextNextParsingFunction_9() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nextNextParsingFunction_9)); }
	inline int32_t get_nextNextParsingFunction_9() const { return ___nextNextParsingFunction_9; }
	inline int32_t* get_address_of_nextNextParsingFunction_9() { return &___nextNextParsingFunction_9; }
	inline void set_nextNextParsingFunction_9(int32_t value)
	{
		___nextNextParsingFunction_9 = value;
	}

	inline static int32_t get_offset_of_nodes_10() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nodes_10)); }
	inline NodeDataU5BU5D_t3671804373* get_nodes_10() const { return ___nodes_10; }
	inline NodeDataU5BU5D_t3671804373** get_address_of_nodes_10() { return &___nodes_10; }
	inline void set_nodes_10(NodeDataU5BU5D_t3671804373* value)
	{
		___nodes_10 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_10, value);
	}

	inline static int32_t get_offset_of_curNode_11() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___curNode_11)); }
	inline NodeData_t2613273532 * get_curNode_11() const { return ___curNode_11; }
	inline NodeData_t2613273532 ** get_address_of_curNode_11() { return &___curNode_11; }
	inline void set_curNode_11(NodeData_t2613273532 * value)
	{
		___curNode_11 = value;
		Il2CppCodeGenWriteBarrier(&___curNode_11, value);
	}

	inline static int32_t get_offset_of_index_12() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___index_12)); }
	inline int32_t get_index_12() const { return ___index_12; }
	inline int32_t* get_address_of_index_12() { return &___index_12; }
	inline void set_index_12(int32_t value)
	{
		___index_12 = value;
	}

	inline static int32_t get_offset_of_curAttrIndex_13() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___curAttrIndex_13)); }
	inline int32_t get_curAttrIndex_13() const { return ___curAttrIndex_13; }
	inline int32_t* get_address_of_curAttrIndex_13() { return &___curAttrIndex_13; }
	inline void set_curAttrIndex_13(int32_t value)
	{
		___curAttrIndex_13 = value;
	}

	inline static int32_t get_offset_of_attrCount_14() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attrCount_14)); }
	inline int32_t get_attrCount_14() const { return ___attrCount_14; }
	inline int32_t* get_address_of_attrCount_14() { return &___attrCount_14; }
	inline void set_attrCount_14(int32_t value)
	{
		___attrCount_14 = value;
	}

	inline static int32_t get_offset_of_attrHashtable_15() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attrHashtable_15)); }
	inline int32_t get_attrHashtable_15() const { return ___attrHashtable_15; }
	inline int32_t* get_address_of_attrHashtable_15() { return &___attrHashtable_15; }
	inline void set_attrHashtable_15(int32_t value)
	{
		___attrHashtable_15 = value;
	}

	inline static int32_t get_offset_of_attrDuplWalkCount_16() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attrDuplWalkCount_16)); }
	inline int32_t get_attrDuplWalkCount_16() const { return ___attrDuplWalkCount_16; }
	inline int32_t* get_address_of_attrDuplWalkCount_16() { return &___attrDuplWalkCount_16; }
	inline void set_attrDuplWalkCount_16(int32_t value)
	{
		___attrDuplWalkCount_16 = value;
	}

	inline static int32_t get_offset_of_attrNeedNamespaceLookup_17() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attrNeedNamespaceLookup_17)); }
	inline bool get_attrNeedNamespaceLookup_17() const { return ___attrNeedNamespaceLookup_17; }
	inline bool* get_address_of_attrNeedNamespaceLookup_17() { return &___attrNeedNamespaceLookup_17; }
	inline void set_attrNeedNamespaceLookup_17(bool value)
	{
		___attrNeedNamespaceLookup_17 = value;
	}

	inline static int32_t get_offset_of_fullAttrCleanup_18() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___fullAttrCleanup_18)); }
	inline bool get_fullAttrCleanup_18() const { return ___fullAttrCleanup_18; }
	inline bool* get_address_of_fullAttrCleanup_18() { return &___fullAttrCleanup_18; }
	inline void set_fullAttrCleanup_18(bool value)
	{
		___fullAttrCleanup_18 = value;
	}

	inline static int32_t get_offset_of_attrDuplSortingArray_19() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attrDuplSortingArray_19)); }
	inline NodeDataU5BU5D_t3671804373* get_attrDuplSortingArray_19() const { return ___attrDuplSortingArray_19; }
	inline NodeDataU5BU5D_t3671804373** get_address_of_attrDuplSortingArray_19() { return &___attrDuplSortingArray_19; }
	inline void set_attrDuplSortingArray_19(NodeDataU5BU5D_t3671804373* value)
	{
		___attrDuplSortingArray_19 = value;
		Il2CppCodeGenWriteBarrier(&___attrDuplSortingArray_19, value);
	}

	inline static int32_t get_offset_of_nameTable_20() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nameTable_20)); }
	inline XmlNameTable_t1345805268 * get_nameTable_20() const { return ___nameTable_20; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_20() { return &___nameTable_20; }
	inline void set_nameTable_20(XmlNameTable_t1345805268 * value)
	{
		___nameTable_20 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_20, value);
	}

	inline static int32_t get_offset_of_nameTableFromSettings_21() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nameTableFromSettings_21)); }
	inline bool get_nameTableFromSettings_21() const { return ___nameTableFromSettings_21; }
	inline bool* get_address_of_nameTableFromSettings_21() { return &___nameTableFromSettings_21; }
	inline void set_nameTableFromSettings_21(bool value)
	{
		___nameTableFromSettings_21 = value;
	}

	inline static int32_t get_offset_of_xmlResolver_22() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___xmlResolver_22)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_22() const { return ___xmlResolver_22; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_22() { return &___xmlResolver_22; }
	inline void set_xmlResolver_22(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_22 = value;
		Il2CppCodeGenWriteBarrier(&___xmlResolver_22, value);
	}

	inline static int32_t get_offset_of_url_23() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___url_23)); }
	inline String_t* get_url_23() const { return ___url_23; }
	inline String_t** get_address_of_url_23() { return &___url_23; }
	inline void set_url_23(String_t* value)
	{
		___url_23 = value;
		Il2CppCodeGenWriteBarrier(&___url_23, value);
	}

	inline static int32_t get_offset_of_normalize_24() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___normalize_24)); }
	inline bool get_normalize_24() const { return ___normalize_24; }
	inline bool* get_address_of_normalize_24() { return &___normalize_24; }
	inline void set_normalize_24(bool value)
	{
		___normalize_24 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_25() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___supportNamespaces_25)); }
	inline bool get_supportNamespaces_25() const { return ___supportNamespaces_25; }
	inline bool* get_address_of_supportNamespaces_25() { return &___supportNamespaces_25; }
	inline void set_supportNamespaces_25(bool value)
	{
		___supportNamespaces_25 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_26() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___whitespaceHandling_26)); }
	inline int32_t get_whitespaceHandling_26() const { return ___whitespaceHandling_26; }
	inline int32_t* get_address_of_whitespaceHandling_26() { return &___whitespaceHandling_26; }
	inline void set_whitespaceHandling_26(int32_t value)
	{
		___whitespaceHandling_26 = value;
	}

	inline static int32_t get_offset_of_dtdProcessing_27() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___dtdProcessing_27)); }
	inline int32_t get_dtdProcessing_27() const { return ___dtdProcessing_27; }
	inline int32_t* get_address_of_dtdProcessing_27() { return &___dtdProcessing_27; }
	inline void set_dtdProcessing_27(int32_t value)
	{
		___dtdProcessing_27 = value;
	}

	inline static int32_t get_offset_of_entityHandling_28() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___entityHandling_28)); }
	inline int32_t get_entityHandling_28() const { return ___entityHandling_28; }
	inline int32_t* get_address_of_entityHandling_28() { return &___entityHandling_28; }
	inline void set_entityHandling_28(int32_t value)
	{
		___entityHandling_28 = value;
	}

	inline static int32_t get_offset_of_ignorePIs_29() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___ignorePIs_29)); }
	inline bool get_ignorePIs_29() const { return ___ignorePIs_29; }
	inline bool* get_address_of_ignorePIs_29() { return &___ignorePIs_29; }
	inline void set_ignorePIs_29(bool value)
	{
		___ignorePIs_29 = value;
	}

	inline static int32_t get_offset_of_ignoreComments_30() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___ignoreComments_30)); }
	inline bool get_ignoreComments_30() const { return ___ignoreComments_30; }
	inline bool* get_address_of_ignoreComments_30() { return &___ignoreComments_30; }
	inline void set_ignoreComments_30(bool value)
	{
		___ignoreComments_30 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_31() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___checkCharacters_31)); }
	inline bool get_checkCharacters_31() const { return ___checkCharacters_31; }
	inline bool* get_address_of_checkCharacters_31() { return &___checkCharacters_31; }
	inline void set_checkCharacters_31(bool value)
	{
		___checkCharacters_31 = value;
	}

	inline static int32_t get_offset_of_lineNumberOffset_32() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___lineNumberOffset_32)); }
	inline int32_t get_lineNumberOffset_32() const { return ___lineNumberOffset_32; }
	inline int32_t* get_address_of_lineNumberOffset_32() { return &___lineNumberOffset_32; }
	inline void set_lineNumberOffset_32(int32_t value)
	{
		___lineNumberOffset_32 = value;
	}

	inline static int32_t get_offset_of_linePositionOffset_33() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___linePositionOffset_33)); }
	inline int32_t get_linePositionOffset_33() const { return ___linePositionOffset_33; }
	inline int32_t* get_address_of_linePositionOffset_33() { return &___linePositionOffset_33; }
	inline void set_linePositionOffset_33(int32_t value)
	{
		___linePositionOffset_33 = value;
	}

	inline static int32_t get_offset_of_closeInput_34() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___closeInput_34)); }
	inline bool get_closeInput_34() const { return ___closeInput_34; }
	inline bool* get_address_of_closeInput_34() { return &___closeInput_34; }
	inline void set_closeInput_34(bool value)
	{
		___closeInput_34 = value;
	}

	inline static int32_t get_offset_of_maxCharactersInDocument_35() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___maxCharactersInDocument_35)); }
	inline int64_t get_maxCharactersInDocument_35() const { return ___maxCharactersInDocument_35; }
	inline int64_t* get_address_of_maxCharactersInDocument_35() { return &___maxCharactersInDocument_35; }
	inline void set_maxCharactersInDocument_35(int64_t value)
	{
		___maxCharactersInDocument_35 = value;
	}

	inline static int32_t get_offset_of_maxCharactersFromEntities_36() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___maxCharactersFromEntities_36)); }
	inline int64_t get_maxCharactersFromEntities_36() const { return ___maxCharactersFromEntities_36; }
	inline int64_t* get_address_of_maxCharactersFromEntities_36() { return &___maxCharactersFromEntities_36; }
	inline void set_maxCharactersFromEntities_36(int64_t value)
	{
		___maxCharactersFromEntities_36 = value;
	}

	inline static int32_t get_offset_of_v1Compat_37() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___v1Compat_37)); }
	inline bool get_v1Compat_37() const { return ___v1Compat_37; }
	inline bool* get_address_of_v1Compat_37() { return &___v1Compat_37; }
	inline void set_v1Compat_37(bool value)
	{
		___v1Compat_37 = value;
	}

	inline static int32_t get_offset_of_namespaceManager_38() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___namespaceManager_38)); }
	inline XmlNamespaceManager_t486731501 * get_namespaceManager_38() const { return ___namespaceManager_38; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_namespaceManager_38() { return &___namespaceManager_38; }
	inline void set_namespaceManager_38(XmlNamespaceManager_t486731501 * value)
	{
		___namespaceManager_38 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceManager_38, value);
	}

	inline static int32_t get_offset_of_lastPrefix_39() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___lastPrefix_39)); }
	inline String_t* get_lastPrefix_39() const { return ___lastPrefix_39; }
	inline String_t** get_address_of_lastPrefix_39() { return &___lastPrefix_39; }
	inline void set_lastPrefix_39(String_t* value)
	{
		___lastPrefix_39 = value;
		Il2CppCodeGenWriteBarrier(&___lastPrefix_39, value);
	}

	inline static int32_t get_offset_of_xmlContext_40() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___xmlContext_40)); }
	inline XmlContext_t938582012 * get_xmlContext_40() const { return ___xmlContext_40; }
	inline XmlContext_t938582012 ** get_address_of_xmlContext_40() { return &___xmlContext_40; }
	inline void set_xmlContext_40(XmlContext_t938582012 * value)
	{
		___xmlContext_40 = value;
		Il2CppCodeGenWriteBarrier(&___xmlContext_40, value);
	}

	inline static int32_t get_offset_of_parsingStatesStack_41() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___parsingStatesStack_41)); }
	inline ParsingStateU5BU5D_t1709225810* get_parsingStatesStack_41() const { return ___parsingStatesStack_41; }
	inline ParsingStateU5BU5D_t1709225810** get_address_of_parsingStatesStack_41() { return &___parsingStatesStack_41; }
	inline void set_parsingStatesStack_41(ParsingStateU5BU5D_t1709225810* value)
	{
		___parsingStatesStack_41 = value;
		Il2CppCodeGenWriteBarrier(&___parsingStatesStack_41, value);
	}

	inline static int32_t get_offset_of_parsingStatesStackTop_42() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___parsingStatesStackTop_42)); }
	inline int32_t get_parsingStatesStackTop_42() const { return ___parsingStatesStackTop_42; }
	inline int32_t* get_address_of_parsingStatesStackTop_42() { return &___parsingStatesStackTop_42; }
	inline void set_parsingStatesStackTop_42(int32_t value)
	{
		___parsingStatesStackTop_42 = value;
	}

	inline static int32_t get_offset_of_reportedBaseUri_43() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___reportedBaseUri_43)); }
	inline String_t* get_reportedBaseUri_43() const { return ___reportedBaseUri_43; }
	inline String_t** get_address_of_reportedBaseUri_43() { return &___reportedBaseUri_43; }
	inline void set_reportedBaseUri_43(String_t* value)
	{
		___reportedBaseUri_43 = value;
		Il2CppCodeGenWriteBarrier(&___reportedBaseUri_43, value);
	}

	inline static int32_t get_offset_of_reportedEncoding_44() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___reportedEncoding_44)); }
	inline Encoding_t663144255 * get_reportedEncoding_44() const { return ___reportedEncoding_44; }
	inline Encoding_t663144255 ** get_address_of_reportedEncoding_44() { return &___reportedEncoding_44; }
	inline void set_reportedEncoding_44(Encoding_t663144255 * value)
	{
		___reportedEncoding_44 = value;
		Il2CppCodeGenWriteBarrier(&___reportedEncoding_44, value);
	}

	inline static int32_t get_offset_of_dtdInfo_45() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___dtdInfo_45)); }
	inline Il2CppObject * get_dtdInfo_45() const { return ___dtdInfo_45; }
	inline Il2CppObject ** get_address_of_dtdInfo_45() { return &___dtdInfo_45; }
	inline void set_dtdInfo_45(Il2CppObject * value)
	{
		___dtdInfo_45 = value;
		Il2CppCodeGenWriteBarrier(&___dtdInfo_45, value);
	}

	inline static int32_t get_offset_of_fragmentType_46() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___fragmentType_46)); }
	inline int32_t get_fragmentType_46() const { return ___fragmentType_46; }
	inline int32_t* get_address_of_fragmentType_46() { return &___fragmentType_46; }
	inline void set_fragmentType_46(int32_t value)
	{
		___fragmentType_46 = value;
	}

	inline static int32_t get_offset_of_fragmentParserContext_47() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___fragmentParserContext_47)); }
	inline XmlParserContext_t2728039553 * get_fragmentParserContext_47() const { return ___fragmentParserContext_47; }
	inline XmlParserContext_t2728039553 ** get_address_of_fragmentParserContext_47() { return &___fragmentParserContext_47; }
	inline void set_fragmentParserContext_47(XmlParserContext_t2728039553 * value)
	{
		___fragmentParserContext_47 = value;
		Il2CppCodeGenWriteBarrier(&___fragmentParserContext_47, value);
	}

	inline static int32_t get_offset_of_fragment_48() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___fragment_48)); }
	inline bool get_fragment_48() const { return ___fragment_48; }
	inline bool* get_address_of_fragment_48() { return &___fragment_48; }
	inline void set_fragment_48(bool value)
	{
		___fragment_48 = value;
	}

	inline static int32_t get_offset_of_incReadDecoder_49() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadDecoder_49)); }
	inline IncrementalReadDecoder_t403822042 * get_incReadDecoder_49() const { return ___incReadDecoder_49; }
	inline IncrementalReadDecoder_t403822042 ** get_address_of_incReadDecoder_49() { return &___incReadDecoder_49; }
	inline void set_incReadDecoder_49(IncrementalReadDecoder_t403822042 * value)
	{
		___incReadDecoder_49 = value;
		Il2CppCodeGenWriteBarrier(&___incReadDecoder_49, value);
	}

	inline static int32_t get_offset_of_incReadState_50() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadState_50)); }
	inline int32_t get_incReadState_50() const { return ___incReadState_50; }
	inline int32_t* get_address_of_incReadState_50() { return &___incReadState_50; }
	inline void set_incReadState_50(int32_t value)
	{
		___incReadState_50 = value;
	}

	inline static int32_t get_offset_of_incReadLineInfo_51() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadLineInfo_51)); }
	inline LineInfo_t1429635508  get_incReadLineInfo_51() const { return ___incReadLineInfo_51; }
	inline LineInfo_t1429635508 * get_address_of_incReadLineInfo_51() { return &___incReadLineInfo_51; }
	inline void set_incReadLineInfo_51(LineInfo_t1429635508  value)
	{
		___incReadLineInfo_51 = value;
	}

	inline static int32_t get_offset_of_incReadDepth_52() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadDepth_52)); }
	inline int32_t get_incReadDepth_52() const { return ___incReadDepth_52; }
	inline int32_t* get_address_of_incReadDepth_52() { return &___incReadDepth_52; }
	inline void set_incReadDepth_52(int32_t value)
	{
		___incReadDepth_52 = value;
	}

	inline static int32_t get_offset_of_incReadLeftStartPos_53() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadLeftStartPos_53)); }
	inline int32_t get_incReadLeftStartPos_53() const { return ___incReadLeftStartPos_53; }
	inline int32_t* get_address_of_incReadLeftStartPos_53() { return &___incReadLeftStartPos_53; }
	inline void set_incReadLeftStartPos_53(int32_t value)
	{
		___incReadLeftStartPos_53 = value;
	}

	inline static int32_t get_offset_of_incReadLeftEndPos_54() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___incReadLeftEndPos_54)); }
	inline int32_t get_incReadLeftEndPos_54() const { return ___incReadLeftEndPos_54; }
	inline int32_t* get_address_of_incReadLeftEndPos_54() { return &___incReadLeftEndPos_54; }
	inline void set_incReadLeftEndPos_54(int32_t value)
	{
		___incReadLeftEndPos_54 = value;
	}

	inline static int32_t get_offset_of_attributeValueBaseEntityId_55() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___attributeValueBaseEntityId_55)); }
	inline int32_t get_attributeValueBaseEntityId_55() const { return ___attributeValueBaseEntityId_55; }
	inline int32_t* get_address_of_attributeValueBaseEntityId_55() { return &___attributeValueBaseEntityId_55; }
	inline void set_attributeValueBaseEntityId_55(int32_t value)
	{
		___attributeValueBaseEntityId_55 = value;
	}

	inline static int32_t get_offset_of_emptyEntityInAttributeResolved_56() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___emptyEntityInAttributeResolved_56)); }
	inline bool get_emptyEntityInAttributeResolved_56() const { return ___emptyEntityInAttributeResolved_56; }
	inline bool* get_address_of_emptyEntityInAttributeResolved_56() { return &___emptyEntityInAttributeResolved_56; }
	inline void set_emptyEntityInAttributeResolved_56(bool value)
	{
		___emptyEntityInAttributeResolved_56 = value;
	}

	inline static int32_t get_offset_of_validationEventHandling_57() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___validationEventHandling_57)); }
	inline Il2CppObject * get_validationEventHandling_57() const { return ___validationEventHandling_57; }
	inline Il2CppObject ** get_address_of_validationEventHandling_57() { return &___validationEventHandling_57; }
	inline void set_validationEventHandling_57(Il2CppObject * value)
	{
		___validationEventHandling_57 = value;
		Il2CppCodeGenWriteBarrier(&___validationEventHandling_57, value);
	}

	inline static int32_t get_offset_of_onDefaultAttributeUse_58() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___onDefaultAttributeUse_58)); }
	inline OnDefaultAttributeUseDelegate_t3539809886 * get_onDefaultAttributeUse_58() const { return ___onDefaultAttributeUse_58; }
	inline OnDefaultAttributeUseDelegate_t3539809886 ** get_address_of_onDefaultAttributeUse_58() { return &___onDefaultAttributeUse_58; }
	inline void set_onDefaultAttributeUse_58(OnDefaultAttributeUseDelegate_t3539809886 * value)
	{
		___onDefaultAttributeUse_58 = value;
		Il2CppCodeGenWriteBarrier(&___onDefaultAttributeUse_58, value);
	}

	inline static int32_t get_offset_of_validatingReaderCompatFlag_59() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___validatingReaderCompatFlag_59)); }
	inline bool get_validatingReaderCompatFlag_59() const { return ___validatingReaderCompatFlag_59; }
	inline bool* get_address_of_validatingReaderCompatFlag_59() { return &___validatingReaderCompatFlag_59; }
	inline void set_validatingReaderCompatFlag_59(bool value)
	{
		___validatingReaderCompatFlag_59 = value;
	}

	inline static int32_t get_offset_of_addDefaultAttributesAndNormalize_60() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___addDefaultAttributesAndNormalize_60)); }
	inline bool get_addDefaultAttributesAndNormalize_60() const { return ___addDefaultAttributesAndNormalize_60; }
	inline bool* get_address_of_addDefaultAttributesAndNormalize_60() { return &___addDefaultAttributesAndNormalize_60; }
	inline void set_addDefaultAttributesAndNormalize_60(bool value)
	{
		___addDefaultAttributesAndNormalize_60 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_61() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___stringBuilder_61)); }
	inline StringBuilder_t1221177846 * get_stringBuilder_61() const { return ___stringBuilder_61; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuilder_61() { return &___stringBuilder_61; }
	inline void set_stringBuilder_61(StringBuilder_t1221177846 * value)
	{
		___stringBuilder_61 = value;
		Il2CppCodeGenWriteBarrier(&___stringBuilder_61, value);
	}

	inline static int32_t get_offset_of_rootElementParsed_62() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___rootElementParsed_62)); }
	inline bool get_rootElementParsed_62() const { return ___rootElementParsed_62; }
	inline bool* get_address_of_rootElementParsed_62() { return &___rootElementParsed_62; }
	inline void set_rootElementParsed_62(bool value)
	{
		___rootElementParsed_62 = value;
	}

	inline static int32_t get_offset_of_standalone_63() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___standalone_63)); }
	inline bool get_standalone_63() const { return ___standalone_63; }
	inline bool* get_address_of_standalone_63() { return &___standalone_63; }
	inline void set_standalone_63(bool value)
	{
		___standalone_63 = value;
	}

	inline static int32_t get_offset_of_nextEntityId_64() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___nextEntityId_64)); }
	inline int32_t get_nextEntityId_64() const { return ___nextEntityId_64; }
	inline int32_t* get_address_of_nextEntityId_64() { return &___nextEntityId_64; }
	inline void set_nextEntityId_64(int32_t value)
	{
		___nextEntityId_64 = value;
	}

	inline static int32_t get_offset_of_parsingMode_65() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___parsingMode_65)); }
	inline int32_t get_parsingMode_65() const { return ___parsingMode_65; }
	inline int32_t* get_address_of_parsingMode_65() { return &___parsingMode_65; }
	inline void set_parsingMode_65(int32_t value)
	{
		___parsingMode_65 = value;
	}

	inline static int32_t get_offset_of_readState_66() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___readState_66)); }
	inline int32_t get_readState_66() const { return ___readState_66; }
	inline int32_t* get_address_of_readState_66() { return &___readState_66; }
	inline void set_readState_66(int32_t value)
	{
		___readState_66 = value;
	}

	inline static int32_t get_offset_of_lastEntity_67() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___lastEntity_67)); }
	inline Il2CppObject * get_lastEntity_67() const { return ___lastEntity_67; }
	inline Il2CppObject ** get_address_of_lastEntity_67() { return &___lastEntity_67; }
	inline void set_lastEntity_67(Il2CppObject * value)
	{
		___lastEntity_67 = value;
		Il2CppCodeGenWriteBarrier(&___lastEntity_67, value);
	}

	inline static int32_t get_offset_of_afterResetState_68() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___afterResetState_68)); }
	inline bool get_afterResetState_68() const { return ___afterResetState_68; }
	inline bool* get_address_of_afterResetState_68() { return &___afterResetState_68; }
	inline void set_afterResetState_68(bool value)
	{
		___afterResetState_68 = value;
	}

	inline static int32_t get_offset_of_documentStartBytePos_69() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___documentStartBytePos_69)); }
	inline int32_t get_documentStartBytePos_69() const { return ___documentStartBytePos_69; }
	inline int32_t* get_address_of_documentStartBytePos_69() { return &___documentStartBytePos_69; }
	inline void set_documentStartBytePos_69(int32_t value)
	{
		___documentStartBytePos_69 = value;
	}

	inline static int32_t get_offset_of_readValueOffset_70() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___readValueOffset_70)); }
	inline int32_t get_readValueOffset_70() const { return ___readValueOffset_70; }
	inline int32_t* get_address_of_readValueOffset_70() { return &___readValueOffset_70; }
	inline void set_readValueOffset_70(int32_t value)
	{
		___readValueOffset_70 = value;
	}

	inline static int32_t get_offset_of_charactersInDocument_71() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___charactersInDocument_71)); }
	inline int64_t get_charactersInDocument_71() const { return ___charactersInDocument_71; }
	inline int64_t* get_address_of_charactersInDocument_71() { return &___charactersInDocument_71; }
	inline void set_charactersInDocument_71(int64_t value)
	{
		___charactersInDocument_71 = value;
	}

	inline static int32_t get_offset_of_charactersFromEntities_72() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___charactersFromEntities_72)); }
	inline int64_t get_charactersFromEntities_72() const { return ___charactersFromEntities_72; }
	inline int64_t* get_address_of_charactersFromEntities_72() { return &___charactersFromEntities_72; }
	inline void set_charactersFromEntities_72(int64_t value)
	{
		___charactersFromEntities_72 = value;
	}

	inline static int32_t get_offset_of_currentEntities_73() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___currentEntities_73)); }
	inline Dictionary_2_t3860214507 * get_currentEntities_73() const { return ___currentEntities_73; }
	inline Dictionary_2_t3860214507 ** get_address_of_currentEntities_73() { return &___currentEntities_73; }
	inline void set_currentEntities_73(Dictionary_2_t3860214507 * value)
	{
		___currentEntities_73 = value;
		Il2CppCodeGenWriteBarrier(&___currentEntities_73, value);
	}

	inline static int32_t get_offset_of_disableUndeclaredEntityCheck_74() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___disableUndeclaredEntityCheck_74)); }
	inline bool get_disableUndeclaredEntityCheck_74() const { return ___disableUndeclaredEntityCheck_74; }
	inline bool* get_address_of_disableUndeclaredEntityCheck_74() { return &___disableUndeclaredEntityCheck_74; }
	inline void set_disableUndeclaredEntityCheck_74(bool value)
	{
		___disableUndeclaredEntityCheck_74 = value;
	}

	inline static int32_t get_offset_of_outerReader_75() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___outerReader_75)); }
	inline XmlReader_t3675626668 * get_outerReader_75() const { return ___outerReader_75; }
	inline XmlReader_t3675626668 ** get_address_of_outerReader_75() { return &___outerReader_75; }
	inline void set_outerReader_75(XmlReader_t3675626668 * value)
	{
		___outerReader_75 = value;
		Il2CppCodeGenWriteBarrier(&___outerReader_75, value);
	}

	inline static int32_t get_offset_of_xmlResolverIsSet_76() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___xmlResolverIsSet_76)); }
	inline bool get_xmlResolverIsSet_76() const { return ___xmlResolverIsSet_76; }
	inline bool* get_address_of_xmlResolverIsSet_76() { return &___xmlResolverIsSet_76; }
	inline void set_xmlResolverIsSet_76(bool value)
	{
		___xmlResolverIsSet_76 = value;
	}

	inline static int32_t get_offset_of_Xml_77() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___Xml_77)); }
	inline String_t* get_Xml_77() const { return ___Xml_77; }
	inline String_t** get_address_of_Xml_77() { return &___Xml_77; }
	inline void set_Xml_77(String_t* value)
	{
		___Xml_77 = value;
		Il2CppCodeGenWriteBarrier(&___Xml_77, value);
	}

	inline static int32_t get_offset_of_XmlNs_78() { return static_cast<int32_t>(offsetof(XmlTextReaderImpl_t3122949129, ___XmlNs_78)); }
	inline String_t* get_XmlNs_78() const { return ___XmlNs_78; }
	inline String_t** get_address_of_XmlNs_78() { return &___XmlNs_78; }
	inline void set_XmlNs_78(String_t* value)
	{
		___XmlNs_78 = value;
		Il2CppCodeGenWriteBarrier(&___XmlNs_78, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
