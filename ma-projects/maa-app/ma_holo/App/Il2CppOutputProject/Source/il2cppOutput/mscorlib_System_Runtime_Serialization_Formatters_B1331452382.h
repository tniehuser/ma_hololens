﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.TypeCode[]
struct TypeCodeU5BU5D_t2030825476;
// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE[]
struct InternalPrimitiveTypeEU5BU5D_t2450497662;
// System.Type
struct Type_t;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.Converter
struct  Converter_t1331452382  : public Il2CppObject
{
public:

public:
};

struct Converter_t1331452382_StaticFields
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.Converter::primitiveTypeEnumLength
	int32_t ___primitiveTypeEnumLength_0;
	// System.Type[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.Converter::typeA
	TypeU5BU5D_t1664964607* ___typeA_1;
	// System.Type[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.Converter::arrayTypeA
	TypeU5BU5D_t1664964607* ___arrayTypeA_2;
	// System.String[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.Converter::valueA
	StringU5BU5D_t1642385972* ___valueA_3;
	// System.TypeCode[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.Converter::typeCodeA
	TypeCodeU5BU5D_t2030825476* ___typeCodeA_4;
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.Formatters.Binary.Converter::codeA
	InternalPrimitiveTypeEU5BU5D_t2450497662* ___codeA_5;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofISerializable
	Type_t * ___typeofISerializable_6;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofString
	Type_t * ___typeofString_7;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofConverter
	Type_t * ___typeofConverter_8;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofBoolean
	Type_t * ___typeofBoolean_9;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofByte
	Type_t * ___typeofByte_10;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofChar
	Type_t * ___typeofChar_11;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDecimal
	Type_t * ___typeofDecimal_12;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDouble
	Type_t * ___typeofDouble_13;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt16
	Type_t * ___typeofInt16_14;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt32
	Type_t * ___typeofInt32_15;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt64
	Type_t * ___typeofInt64_16;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofSByte
	Type_t * ___typeofSByte_17;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofSingle
	Type_t * ___typeofSingle_18;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofTimeSpan
	Type_t * ___typeofTimeSpan_19;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDateTime
	Type_t * ___typeofDateTime_20;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt16
	Type_t * ___typeofUInt16_21;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt32
	Type_t * ___typeofUInt32_22;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt64
	Type_t * ___typeofUInt64_23;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofObject
	Type_t * ___typeofObject_24;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofSystemVoid
	Type_t * ___typeofSystemVoid_25;
	// System.Reflection.Assembly System.Runtime.Serialization.Formatters.Binary.Converter::urtAssembly
	Assembly_t4268412390 * ___urtAssembly_26;
	// System.String System.Runtime.Serialization.Formatters.Binary.Converter::urtAssemblyString
	String_t* ___urtAssemblyString_27;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofTypeArray
	Type_t * ___typeofTypeArray_28;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofObjectArray
	Type_t * ___typeofObjectArray_29;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofStringArray
	Type_t * ___typeofStringArray_30;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofBooleanArray
	Type_t * ___typeofBooleanArray_31;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofByteArray
	Type_t * ___typeofByteArray_32;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofCharArray
	Type_t * ___typeofCharArray_33;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDecimalArray
	Type_t * ___typeofDecimalArray_34;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDoubleArray
	Type_t * ___typeofDoubleArray_35;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt16Array
	Type_t * ___typeofInt16Array_36;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt32Array
	Type_t * ___typeofInt32Array_37;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofInt64Array
	Type_t * ___typeofInt64Array_38;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofSByteArray
	Type_t * ___typeofSByteArray_39;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofSingleArray
	Type_t * ___typeofSingleArray_40;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofTimeSpanArray
	Type_t * ___typeofTimeSpanArray_41;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofDateTimeArray
	Type_t * ___typeofDateTimeArray_42;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt16Array
	Type_t * ___typeofUInt16Array_43;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt32Array
	Type_t * ___typeofUInt32Array_44;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofUInt64Array
	Type_t * ___typeofUInt64Array_45;
	// System.Type System.Runtime.Serialization.Formatters.Binary.Converter::typeofMarshalByRefObject
	Type_t * ___typeofMarshalByRefObject_46;

public:
	inline static int32_t get_offset_of_primitiveTypeEnumLength_0() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___primitiveTypeEnumLength_0)); }
	inline int32_t get_primitiveTypeEnumLength_0() const { return ___primitiveTypeEnumLength_0; }
	inline int32_t* get_address_of_primitiveTypeEnumLength_0() { return &___primitiveTypeEnumLength_0; }
	inline void set_primitiveTypeEnumLength_0(int32_t value)
	{
		___primitiveTypeEnumLength_0 = value;
	}

	inline static int32_t get_offset_of_typeA_1() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeA_1)); }
	inline TypeU5BU5D_t1664964607* get_typeA_1() const { return ___typeA_1; }
	inline TypeU5BU5D_t1664964607** get_address_of_typeA_1() { return &___typeA_1; }
	inline void set_typeA_1(TypeU5BU5D_t1664964607* value)
	{
		___typeA_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeA_1, value);
	}

	inline static int32_t get_offset_of_arrayTypeA_2() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___arrayTypeA_2)); }
	inline TypeU5BU5D_t1664964607* get_arrayTypeA_2() const { return ___arrayTypeA_2; }
	inline TypeU5BU5D_t1664964607** get_address_of_arrayTypeA_2() { return &___arrayTypeA_2; }
	inline void set_arrayTypeA_2(TypeU5BU5D_t1664964607* value)
	{
		___arrayTypeA_2 = value;
		Il2CppCodeGenWriteBarrier(&___arrayTypeA_2, value);
	}

	inline static int32_t get_offset_of_valueA_3() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___valueA_3)); }
	inline StringU5BU5D_t1642385972* get_valueA_3() const { return ___valueA_3; }
	inline StringU5BU5D_t1642385972** get_address_of_valueA_3() { return &___valueA_3; }
	inline void set_valueA_3(StringU5BU5D_t1642385972* value)
	{
		___valueA_3 = value;
		Il2CppCodeGenWriteBarrier(&___valueA_3, value);
	}

	inline static int32_t get_offset_of_typeCodeA_4() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeCodeA_4)); }
	inline TypeCodeU5BU5D_t2030825476* get_typeCodeA_4() const { return ___typeCodeA_4; }
	inline TypeCodeU5BU5D_t2030825476** get_address_of_typeCodeA_4() { return &___typeCodeA_4; }
	inline void set_typeCodeA_4(TypeCodeU5BU5D_t2030825476* value)
	{
		___typeCodeA_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeCodeA_4, value);
	}

	inline static int32_t get_offset_of_codeA_5() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___codeA_5)); }
	inline InternalPrimitiveTypeEU5BU5D_t2450497662* get_codeA_5() const { return ___codeA_5; }
	inline InternalPrimitiveTypeEU5BU5D_t2450497662** get_address_of_codeA_5() { return &___codeA_5; }
	inline void set_codeA_5(InternalPrimitiveTypeEU5BU5D_t2450497662* value)
	{
		___codeA_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeA_5, value);
	}

	inline static int32_t get_offset_of_typeofISerializable_6() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofISerializable_6)); }
	inline Type_t * get_typeofISerializable_6() const { return ___typeofISerializable_6; }
	inline Type_t ** get_address_of_typeofISerializable_6() { return &___typeofISerializable_6; }
	inline void set_typeofISerializable_6(Type_t * value)
	{
		___typeofISerializable_6 = value;
		Il2CppCodeGenWriteBarrier(&___typeofISerializable_6, value);
	}

	inline static int32_t get_offset_of_typeofString_7() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofString_7)); }
	inline Type_t * get_typeofString_7() const { return ___typeofString_7; }
	inline Type_t ** get_address_of_typeofString_7() { return &___typeofString_7; }
	inline void set_typeofString_7(Type_t * value)
	{
		___typeofString_7 = value;
		Il2CppCodeGenWriteBarrier(&___typeofString_7, value);
	}

	inline static int32_t get_offset_of_typeofConverter_8() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofConverter_8)); }
	inline Type_t * get_typeofConverter_8() const { return ___typeofConverter_8; }
	inline Type_t ** get_address_of_typeofConverter_8() { return &___typeofConverter_8; }
	inline void set_typeofConverter_8(Type_t * value)
	{
		___typeofConverter_8 = value;
		Il2CppCodeGenWriteBarrier(&___typeofConverter_8, value);
	}

	inline static int32_t get_offset_of_typeofBoolean_9() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofBoolean_9)); }
	inline Type_t * get_typeofBoolean_9() const { return ___typeofBoolean_9; }
	inline Type_t ** get_address_of_typeofBoolean_9() { return &___typeofBoolean_9; }
	inline void set_typeofBoolean_9(Type_t * value)
	{
		___typeofBoolean_9 = value;
		Il2CppCodeGenWriteBarrier(&___typeofBoolean_9, value);
	}

	inline static int32_t get_offset_of_typeofByte_10() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofByte_10)); }
	inline Type_t * get_typeofByte_10() const { return ___typeofByte_10; }
	inline Type_t ** get_address_of_typeofByte_10() { return &___typeofByte_10; }
	inline void set_typeofByte_10(Type_t * value)
	{
		___typeofByte_10 = value;
		Il2CppCodeGenWriteBarrier(&___typeofByte_10, value);
	}

	inline static int32_t get_offset_of_typeofChar_11() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofChar_11)); }
	inline Type_t * get_typeofChar_11() const { return ___typeofChar_11; }
	inline Type_t ** get_address_of_typeofChar_11() { return &___typeofChar_11; }
	inline void set_typeofChar_11(Type_t * value)
	{
		___typeofChar_11 = value;
		Il2CppCodeGenWriteBarrier(&___typeofChar_11, value);
	}

	inline static int32_t get_offset_of_typeofDecimal_12() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDecimal_12)); }
	inline Type_t * get_typeofDecimal_12() const { return ___typeofDecimal_12; }
	inline Type_t ** get_address_of_typeofDecimal_12() { return &___typeofDecimal_12; }
	inline void set_typeofDecimal_12(Type_t * value)
	{
		___typeofDecimal_12 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDecimal_12, value);
	}

	inline static int32_t get_offset_of_typeofDouble_13() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDouble_13)); }
	inline Type_t * get_typeofDouble_13() const { return ___typeofDouble_13; }
	inline Type_t ** get_address_of_typeofDouble_13() { return &___typeofDouble_13; }
	inline void set_typeofDouble_13(Type_t * value)
	{
		___typeofDouble_13 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDouble_13, value);
	}

	inline static int32_t get_offset_of_typeofInt16_14() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt16_14)); }
	inline Type_t * get_typeofInt16_14() const { return ___typeofInt16_14; }
	inline Type_t ** get_address_of_typeofInt16_14() { return &___typeofInt16_14; }
	inline void set_typeofInt16_14(Type_t * value)
	{
		___typeofInt16_14 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt16_14, value);
	}

	inline static int32_t get_offset_of_typeofInt32_15() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt32_15)); }
	inline Type_t * get_typeofInt32_15() const { return ___typeofInt32_15; }
	inline Type_t ** get_address_of_typeofInt32_15() { return &___typeofInt32_15; }
	inline void set_typeofInt32_15(Type_t * value)
	{
		___typeofInt32_15 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt32_15, value);
	}

	inline static int32_t get_offset_of_typeofInt64_16() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt64_16)); }
	inline Type_t * get_typeofInt64_16() const { return ___typeofInt64_16; }
	inline Type_t ** get_address_of_typeofInt64_16() { return &___typeofInt64_16; }
	inline void set_typeofInt64_16(Type_t * value)
	{
		___typeofInt64_16 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt64_16, value);
	}

	inline static int32_t get_offset_of_typeofSByte_17() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofSByte_17)); }
	inline Type_t * get_typeofSByte_17() const { return ___typeofSByte_17; }
	inline Type_t ** get_address_of_typeofSByte_17() { return &___typeofSByte_17; }
	inline void set_typeofSByte_17(Type_t * value)
	{
		___typeofSByte_17 = value;
		Il2CppCodeGenWriteBarrier(&___typeofSByte_17, value);
	}

	inline static int32_t get_offset_of_typeofSingle_18() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofSingle_18)); }
	inline Type_t * get_typeofSingle_18() const { return ___typeofSingle_18; }
	inline Type_t ** get_address_of_typeofSingle_18() { return &___typeofSingle_18; }
	inline void set_typeofSingle_18(Type_t * value)
	{
		___typeofSingle_18 = value;
		Il2CppCodeGenWriteBarrier(&___typeofSingle_18, value);
	}

	inline static int32_t get_offset_of_typeofTimeSpan_19() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofTimeSpan_19)); }
	inline Type_t * get_typeofTimeSpan_19() const { return ___typeofTimeSpan_19; }
	inline Type_t ** get_address_of_typeofTimeSpan_19() { return &___typeofTimeSpan_19; }
	inline void set_typeofTimeSpan_19(Type_t * value)
	{
		___typeofTimeSpan_19 = value;
		Il2CppCodeGenWriteBarrier(&___typeofTimeSpan_19, value);
	}

	inline static int32_t get_offset_of_typeofDateTime_20() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDateTime_20)); }
	inline Type_t * get_typeofDateTime_20() const { return ___typeofDateTime_20; }
	inline Type_t ** get_address_of_typeofDateTime_20() { return &___typeofDateTime_20; }
	inline void set_typeofDateTime_20(Type_t * value)
	{
		___typeofDateTime_20 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDateTime_20, value);
	}

	inline static int32_t get_offset_of_typeofUInt16_21() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt16_21)); }
	inline Type_t * get_typeofUInt16_21() const { return ___typeofUInt16_21; }
	inline Type_t ** get_address_of_typeofUInt16_21() { return &___typeofUInt16_21; }
	inline void set_typeofUInt16_21(Type_t * value)
	{
		___typeofUInt16_21 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt16_21, value);
	}

	inline static int32_t get_offset_of_typeofUInt32_22() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt32_22)); }
	inline Type_t * get_typeofUInt32_22() const { return ___typeofUInt32_22; }
	inline Type_t ** get_address_of_typeofUInt32_22() { return &___typeofUInt32_22; }
	inline void set_typeofUInt32_22(Type_t * value)
	{
		___typeofUInt32_22 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt32_22, value);
	}

	inline static int32_t get_offset_of_typeofUInt64_23() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt64_23)); }
	inline Type_t * get_typeofUInt64_23() const { return ___typeofUInt64_23; }
	inline Type_t ** get_address_of_typeofUInt64_23() { return &___typeofUInt64_23; }
	inline void set_typeofUInt64_23(Type_t * value)
	{
		___typeofUInt64_23 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt64_23, value);
	}

	inline static int32_t get_offset_of_typeofObject_24() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofObject_24)); }
	inline Type_t * get_typeofObject_24() const { return ___typeofObject_24; }
	inline Type_t ** get_address_of_typeofObject_24() { return &___typeofObject_24; }
	inline void set_typeofObject_24(Type_t * value)
	{
		___typeofObject_24 = value;
		Il2CppCodeGenWriteBarrier(&___typeofObject_24, value);
	}

	inline static int32_t get_offset_of_typeofSystemVoid_25() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofSystemVoid_25)); }
	inline Type_t * get_typeofSystemVoid_25() const { return ___typeofSystemVoid_25; }
	inline Type_t ** get_address_of_typeofSystemVoid_25() { return &___typeofSystemVoid_25; }
	inline void set_typeofSystemVoid_25(Type_t * value)
	{
		___typeofSystemVoid_25 = value;
		Il2CppCodeGenWriteBarrier(&___typeofSystemVoid_25, value);
	}

	inline static int32_t get_offset_of_urtAssembly_26() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___urtAssembly_26)); }
	inline Assembly_t4268412390 * get_urtAssembly_26() const { return ___urtAssembly_26; }
	inline Assembly_t4268412390 ** get_address_of_urtAssembly_26() { return &___urtAssembly_26; }
	inline void set_urtAssembly_26(Assembly_t4268412390 * value)
	{
		___urtAssembly_26 = value;
		Il2CppCodeGenWriteBarrier(&___urtAssembly_26, value);
	}

	inline static int32_t get_offset_of_urtAssemblyString_27() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___urtAssemblyString_27)); }
	inline String_t* get_urtAssemblyString_27() const { return ___urtAssemblyString_27; }
	inline String_t** get_address_of_urtAssemblyString_27() { return &___urtAssemblyString_27; }
	inline void set_urtAssemblyString_27(String_t* value)
	{
		___urtAssemblyString_27 = value;
		Il2CppCodeGenWriteBarrier(&___urtAssemblyString_27, value);
	}

	inline static int32_t get_offset_of_typeofTypeArray_28() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofTypeArray_28)); }
	inline Type_t * get_typeofTypeArray_28() const { return ___typeofTypeArray_28; }
	inline Type_t ** get_address_of_typeofTypeArray_28() { return &___typeofTypeArray_28; }
	inline void set_typeofTypeArray_28(Type_t * value)
	{
		___typeofTypeArray_28 = value;
		Il2CppCodeGenWriteBarrier(&___typeofTypeArray_28, value);
	}

	inline static int32_t get_offset_of_typeofObjectArray_29() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofObjectArray_29)); }
	inline Type_t * get_typeofObjectArray_29() const { return ___typeofObjectArray_29; }
	inline Type_t ** get_address_of_typeofObjectArray_29() { return &___typeofObjectArray_29; }
	inline void set_typeofObjectArray_29(Type_t * value)
	{
		___typeofObjectArray_29 = value;
		Il2CppCodeGenWriteBarrier(&___typeofObjectArray_29, value);
	}

	inline static int32_t get_offset_of_typeofStringArray_30() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofStringArray_30)); }
	inline Type_t * get_typeofStringArray_30() const { return ___typeofStringArray_30; }
	inline Type_t ** get_address_of_typeofStringArray_30() { return &___typeofStringArray_30; }
	inline void set_typeofStringArray_30(Type_t * value)
	{
		___typeofStringArray_30 = value;
		Il2CppCodeGenWriteBarrier(&___typeofStringArray_30, value);
	}

	inline static int32_t get_offset_of_typeofBooleanArray_31() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofBooleanArray_31)); }
	inline Type_t * get_typeofBooleanArray_31() const { return ___typeofBooleanArray_31; }
	inline Type_t ** get_address_of_typeofBooleanArray_31() { return &___typeofBooleanArray_31; }
	inline void set_typeofBooleanArray_31(Type_t * value)
	{
		___typeofBooleanArray_31 = value;
		Il2CppCodeGenWriteBarrier(&___typeofBooleanArray_31, value);
	}

	inline static int32_t get_offset_of_typeofByteArray_32() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofByteArray_32)); }
	inline Type_t * get_typeofByteArray_32() const { return ___typeofByteArray_32; }
	inline Type_t ** get_address_of_typeofByteArray_32() { return &___typeofByteArray_32; }
	inline void set_typeofByteArray_32(Type_t * value)
	{
		___typeofByteArray_32 = value;
		Il2CppCodeGenWriteBarrier(&___typeofByteArray_32, value);
	}

	inline static int32_t get_offset_of_typeofCharArray_33() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofCharArray_33)); }
	inline Type_t * get_typeofCharArray_33() const { return ___typeofCharArray_33; }
	inline Type_t ** get_address_of_typeofCharArray_33() { return &___typeofCharArray_33; }
	inline void set_typeofCharArray_33(Type_t * value)
	{
		___typeofCharArray_33 = value;
		Il2CppCodeGenWriteBarrier(&___typeofCharArray_33, value);
	}

	inline static int32_t get_offset_of_typeofDecimalArray_34() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDecimalArray_34)); }
	inline Type_t * get_typeofDecimalArray_34() const { return ___typeofDecimalArray_34; }
	inline Type_t ** get_address_of_typeofDecimalArray_34() { return &___typeofDecimalArray_34; }
	inline void set_typeofDecimalArray_34(Type_t * value)
	{
		___typeofDecimalArray_34 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDecimalArray_34, value);
	}

	inline static int32_t get_offset_of_typeofDoubleArray_35() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDoubleArray_35)); }
	inline Type_t * get_typeofDoubleArray_35() const { return ___typeofDoubleArray_35; }
	inline Type_t ** get_address_of_typeofDoubleArray_35() { return &___typeofDoubleArray_35; }
	inline void set_typeofDoubleArray_35(Type_t * value)
	{
		___typeofDoubleArray_35 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDoubleArray_35, value);
	}

	inline static int32_t get_offset_of_typeofInt16Array_36() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt16Array_36)); }
	inline Type_t * get_typeofInt16Array_36() const { return ___typeofInt16Array_36; }
	inline Type_t ** get_address_of_typeofInt16Array_36() { return &___typeofInt16Array_36; }
	inline void set_typeofInt16Array_36(Type_t * value)
	{
		___typeofInt16Array_36 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt16Array_36, value);
	}

	inline static int32_t get_offset_of_typeofInt32Array_37() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt32Array_37)); }
	inline Type_t * get_typeofInt32Array_37() const { return ___typeofInt32Array_37; }
	inline Type_t ** get_address_of_typeofInt32Array_37() { return &___typeofInt32Array_37; }
	inline void set_typeofInt32Array_37(Type_t * value)
	{
		___typeofInt32Array_37 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt32Array_37, value);
	}

	inline static int32_t get_offset_of_typeofInt64Array_38() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofInt64Array_38)); }
	inline Type_t * get_typeofInt64Array_38() const { return ___typeofInt64Array_38; }
	inline Type_t ** get_address_of_typeofInt64Array_38() { return &___typeofInt64Array_38; }
	inline void set_typeofInt64Array_38(Type_t * value)
	{
		___typeofInt64Array_38 = value;
		Il2CppCodeGenWriteBarrier(&___typeofInt64Array_38, value);
	}

	inline static int32_t get_offset_of_typeofSByteArray_39() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofSByteArray_39)); }
	inline Type_t * get_typeofSByteArray_39() const { return ___typeofSByteArray_39; }
	inline Type_t ** get_address_of_typeofSByteArray_39() { return &___typeofSByteArray_39; }
	inline void set_typeofSByteArray_39(Type_t * value)
	{
		___typeofSByteArray_39 = value;
		Il2CppCodeGenWriteBarrier(&___typeofSByteArray_39, value);
	}

	inline static int32_t get_offset_of_typeofSingleArray_40() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofSingleArray_40)); }
	inline Type_t * get_typeofSingleArray_40() const { return ___typeofSingleArray_40; }
	inline Type_t ** get_address_of_typeofSingleArray_40() { return &___typeofSingleArray_40; }
	inline void set_typeofSingleArray_40(Type_t * value)
	{
		___typeofSingleArray_40 = value;
		Il2CppCodeGenWriteBarrier(&___typeofSingleArray_40, value);
	}

	inline static int32_t get_offset_of_typeofTimeSpanArray_41() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofTimeSpanArray_41)); }
	inline Type_t * get_typeofTimeSpanArray_41() const { return ___typeofTimeSpanArray_41; }
	inline Type_t ** get_address_of_typeofTimeSpanArray_41() { return &___typeofTimeSpanArray_41; }
	inline void set_typeofTimeSpanArray_41(Type_t * value)
	{
		___typeofTimeSpanArray_41 = value;
		Il2CppCodeGenWriteBarrier(&___typeofTimeSpanArray_41, value);
	}

	inline static int32_t get_offset_of_typeofDateTimeArray_42() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofDateTimeArray_42)); }
	inline Type_t * get_typeofDateTimeArray_42() const { return ___typeofDateTimeArray_42; }
	inline Type_t ** get_address_of_typeofDateTimeArray_42() { return &___typeofDateTimeArray_42; }
	inline void set_typeofDateTimeArray_42(Type_t * value)
	{
		___typeofDateTimeArray_42 = value;
		Il2CppCodeGenWriteBarrier(&___typeofDateTimeArray_42, value);
	}

	inline static int32_t get_offset_of_typeofUInt16Array_43() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt16Array_43)); }
	inline Type_t * get_typeofUInt16Array_43() const { return ___typeofUInt16Array_43; }
	inline Type_t ** get_address_of_typeofUInt16Array_43() { return &___typeofUInt16Array_43; }
	inline void set_typeofUInt16Array_43(Type_t * value)
	{
		___typeofUInt16Array_43 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt16Array_43, value);
	}

	inline static int32_t get_offset_of_typeofUInt32Array_44() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt32Array_44)); }
	inline Type_t * get_typeofUInt32Array_44() const { return ___typeofUInt32Array_44; }
	inline Type_t ** get_address_of_typeofUInt32Array_44() { return &___typeofUInt32Array_44; }
	inline void set_typeofUInt32Array_44(Type_t * value)
	{
		___typeofUInt32Array_44 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt32Array_44, value);
	}

	inline static int32_t get_offset_of_typeofUInt64Array_45() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofUInt64Array_45)); }
	inline Type_t * get_typeofUInt64Array_45() const { return ___typeofUInt64Array_45; }
	inline Type_t ** get_address_of_typeofUInt64Array_45() { return &___typeofUInt64Array_45; }
	inline void set_typeofUInt64Array_45(Type_t * value)
	{
		___typeofUInt64Array_45 = value;
		Il2CppCodeGenWriteBarrier(&___typeofUInt64Array_45, value);
	}

	inline static int32_t get_offset_of_typeofMarshalByRefObject_46() { return static_cast<int32_t>(offsetof(Converter_t1331452382_StaticFields, ___typeofMarshalByRefObject_46)); }
	inline Type_t * get_typeofMarshalByRefObject_46() const { return ___typeofMarshalByRefObject_46; }
	inline Type_t ** get_address_of_typeofMarshalByRefObject_46() { return &___typeofMarshalByRefObject_46; }
	inline void set_typeofMarshalByRefObject_46(Type_t * value)
	{
		___typeofMarshalByRefObject_46 = value;
		Il2CppCodeGenWriteBarrier(&___typeofMarshalByRefObject_46, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
