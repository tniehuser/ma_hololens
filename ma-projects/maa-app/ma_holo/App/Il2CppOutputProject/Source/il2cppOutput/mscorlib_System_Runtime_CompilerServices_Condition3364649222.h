﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Runtime.CompilerServices.Ephemeron[]
struct EphemeronU5BU5D_t3006146916;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Object,System.Runtime.Serialization.SerializationInfo>
struct  ConditionalWeakTable_2_t3364649222  : public Il2CppObject
{
public:
	// System.Runtime.CompilerServices.Ephemeron[] System.Runtime.CompilerServices.ConditionalWeakTable`2::data
	EphemeronU5BU5D_t3006146916* ___data_0;
	// System.Object System.Runtime.CompilerServices.ConditionalWeakTable`2::_lock
	Il2CppObject * ____lock_1;
	// System.Int32 System.Runtime.CompilerServices.ConditionalWeakTable`2::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ConditionalWeakTable_2_t3364649222, ___data_0)); }
	inline EphemeronU5BU5D_t3006146916* get_data_0() const { return ___data_0; }
	inline EphemeronU5BU5D_t3006146916** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EphemeronU5BU5D_t3006146916* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of__lock_1() { return static_cast<int32_t>(offsetof(ConditionalWeakTable_2_t3364649222, ____lock_1)); }
	inline Il2CppObject * get__lock_1() const { return ____lock_1; }
	inline Il2CppObject ** get_address_of__lock_1() { return &____lock_1; }
	inline void set__lock_1(Il2CppObject * value)
	{
		____lock_1 = value;
		Il2CppCodeGenWriteBarrier(&____lock_1, value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(ConditionalWeakTable_2_t3364649222, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
