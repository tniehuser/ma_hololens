﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkInformation.LinuxNetworkInterface
struct  LinuxNetworkInterface_t3864470295  : public UnixNetworkInterface_t1000704527
{
public:
	// System.String System.Net.NetworkInformation.LinuxNetworkInterface::iface_path
	String_t* ___iface_path_5;
	// System.String System.Net.NetworkInformation.LinuxNetworkInterface::iface_operstate_path
	String_t* ___iface_operstate_path_6;
	// System.String System.Net.NetworkInformation.LinuxNetworkInterface::iface_flags_path
	String_t* ___iface_flags_path_7;

public:
	inline static int32_t get_offset_of_iface_path_5() { return static_cast<int32_t>(offsetof(LinuxNetworkInterface_t3864470295, ___iface_path_5)); }
	inline String_t* get_iface_path_5() const { return ___iface_path_5; }
	inline String_t** get_address_of_iface_path_5() { return &___iface_path_5; }
	inline void set_iface_path_5(String_t* value)
	{
		___iface_path_5 = value;
		Il2CppCodeGenWriteBarrier(&___iface_path_5, value);
	}

	inline static int32_t get_offset_of_iface_operstate_path_6() { return static_cast<int32_t>(offsetof(LinuxNetworkInterface_t3864470295, ___iface_operstate_path_6)); }
	inline String_t* get_iface_operstate_path_6() const { return ___iface_operstate_path_6; }
	inline String_t** get_address_of_iface_operstate_path_6() { return &___iface_operstate_path_6; }
	inline void set_iface_operstate_path_6(String_t* value)
	{
		___iface_operstate_path_6 = value;
		Il2CppCodeGenWriteBarrier(&___iface_operstate_path_6, value);
	}

	inline static int32_t get_offset_of_iface_flags_path_7() { return static_cast<int32_t>(offsetof(LinuxNetworkInterface_t3864470295, ___iface_flags_path_7)); }
	inline String_t* get_iface_flags_path_7() const { return ___iface_flags_path_7; }
	inline String_t** get_address_of_iface_flags_path_7() { return &___iface_flags_path_7; }
	inline void set_iface_flags_path_7(String_t* value)
	{
		___iface_flags_path_7 = value;
		Il2CppCodeGenWriteBarrier(&___iface_flags_path_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
