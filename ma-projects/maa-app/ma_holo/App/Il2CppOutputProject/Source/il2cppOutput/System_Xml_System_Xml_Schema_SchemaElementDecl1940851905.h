﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_SchemaDeclBase797759480.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"

// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef>
struct Dictionary_2_t2500647674;
// System.Collections.Generic.List`1<System.Xml.IDtdDefaultAttributeInfo>
struct List_1_t1695916396;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.XmlQualifiedName>
struct Dictionary_2_t2934452923;
// System.Xml.Schema.ContentValidator
struct ContentValidator_t2510151843;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.Schema.CompiledIdentityConstraint[]
struct CompiledIdentityConstraintU5BU5D_t899482317;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1940851905;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaElementDecl
struct  SchemaElementDecl_t1940851905  : public SchemaDeclBase_t797759480
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef> System.Xml.Schema.SchemaElementDecl::attdefs
	Dictionary_2_t2500647674 * ___attdefs_11;
	// System.Collections.Generic.List`1<System.Xml.IDtdDefaultAttributeInfo> System.Xml.Schema.SchemaElementDecl::defaultAttdefs
	List_1_t1695916396 * ___defaultAttdefs_12;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isIdDeclared
	bool ___isIdDeclared_13;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::hasNonCDataAttribute
	bool ___hasNonCDataAttribute_14;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isAbstract
	bool ___isAbstract_15;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isNillable
	bool ___isNillable_16;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::hasRequiredAttribute
	bool ___hasRequiredAttribute_17;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isNotationDeclared
	bool ___isNotationDeclared_18;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.XmlQualifiedName> System.Xml.Schema.SchemaElementDecl::prohibitedAttributes
	Dictionary_2_t2934452923 * ___prohibitedAttributes_19;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.SchemaElementDecl::contentValidator
	ContentValidator_t2510151843 * ___contentValidator_20;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.SchemaElementDecl::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_21;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaElementDecl::block
	int32_t ___block_22;
	// System.Xml.Schema.CompiledIdentityConstraint[] System.Xml.Schema.SchemaElementDecl::constraints
	CompiledIdentityConstraintU5BU5D_t899482317* ___constraints_23;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.SchemaElementDecl::schemaElement
	XmlSchemaElement_t2433337156 * ___schemaElement_24;

public:
	inline static int32_t get_offset_of_attdefs_11() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___attdefs_11)); }
	inline Dictionary_2_t2500647674 * get_attdefs_11() const { return ___attdefs_11; }
	inline Dictionary_2_t2500647674 ** get_address_of_attdefs_11() { return &___attdefs_11; }
	inline void set_attdefs_11(Dictionary_2_t2500647674 * value)
	{
		___attdefs_11 = value;
		Il2CppCodeGenWriteBarrier(&___attdefs_11, value);
	}

	inline static int32_t get_offset_of_defaultAttdefs_12() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___defaultAttdefs_12)); }
	inline List_1_t1695916396 * get_defaultAttdefs_12() const { return ___defaultAttdefs_12; }
	inline List_1_t1695916396 ** get_address_of_defaultAttdefs_12() { return &___defaultAttdefs_12; }
	inline void set_defaultAttdefs_12(List_1_t1695916396 * value)
	{
		___defaultAttdefs_12 = value;
		Il2CppCodeGenWriteBarrier(&___defaultAttdefs_12, value);
	}

	inline static int32_t get_offset_of_isIdDeclared_13() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___isIdDeclared_13)); }
	inline bool get_isIdDeclared_13() const { return ___isIdDeclared_13; }
	inline bool* get_address_of_isIdDeclared_13() { return &___isIdDeclared_13; }
	inline void set_isIdDeclared_13(bool value)
	{
		___isIdDeclared_13 = value;
	}

	inline static int32_t get_offset_of_hasNonCDataAttribute_14() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___hasNonCDataAttribute_14)); }
	inline bool get_hasNonCDataAttribute_14() const { return ___hasNonCDataAttribute_14; }
	inline bool* get_address_of_hasNonCDataAttribute_14() { return &___hasNonCDataAttribute_14; }
	inline void set_hasNonCDataAttribute_14(bool value)
	{
		___hasNonCDataAttribute_14 = value;
	}

	inline static int32_t get_offset_of_isAbstract_15() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___isAbstract_15)); }
	inline bool get_isAbstract_15() const { return ___isAbstract_15; }
	inline bool* get_address_of_isAbstract_15() { return &___isAbstract_15; }
	inline void set_isAbstract_15(bool value)
	{
		___isAbstract_15 = value;
	}

	inline static int32_t get_offset_of_isNillable_16() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___isNillable_16)); }
	inline bool get_isNillable_16() const { return ___isNillable_16; }
	inline bool* get_address_of_isNillable_16() { return &___isNillable_16; }
	inline void set_isNillable_16(bool value)
	{
		___isNillable_16 = value;
	}

	inline static int32_t get_offset_of_hasRequiredAttribute_17() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___hasRequiredAttribute_17)); }
	inline bool get_hasRequiredAttribute_17() const { return ___hasRequiredAttribute_17; }
	inline bool* get_address_of_hasRequiredAttribute_17() { return &___hasRequiredAttribute_17; }
	inline void set_hasRequiredAttribute_17(bool value)
	{
		___hasRequiredAttribute_17 = value;
	}

	inline static int32_t get_offset_of_isNotationDeclared_18() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___isNotationDeclared_18)); }
	inline bool get_isNotationDeclared_18() const { return ___isNotationDeclared_18; }
	inline bool* get_address_of_isNotationDeclared_18() { return &___isNotationDeclared_18; }
	inline void set_isNotationDeclared_18(bool value)
	{
		___isNotationDeclared_18 = value;
	}

	inline static int32_t get_offset_of_prohibitedAttributes_19() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___prohibitedAttributes_19)); }
	inline Dictionary_2_t2934452923 * get_prohibitedAttributes_19() const { return ___prohibitedAttributes_19; }
	inline Dictionary_2_t2934452923 ** get_address_of_prohibitedAttributes_19() { return &___prohibitedAttributes_19; }
	inline void set_prohibitedAttributes_19(Dictionary_2_t2934452923 * value)
	{
		___prohibitedAttributes_19 = value;
		Il2CppCodeGenWriteBarrier(&___prohibitedAttributes_19, value);
	}

	inline static int32_t get_offset_of_contentValidator_20() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___contentValidator_20)); }
	inline ContentValidator_t2510151843 * get_contentValidator_20() const { return ___contentValidator_20; }
	inline ContentValidator_t2510151843 ** get_address_of_contentValidator_20() { return &___contentValidator_20; }
	inline void set_contentValidator_20(ContentValidator_t2510151843 * value)
	{
		___contentValidator_20 = value;
		Il2CppCodeGenWriteBarrier(&___contentValidator_20, value);
	}

	inline static int32_t get_offset_of_anyAttribute_21() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___anyAttribute_21)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_21() const { return ___anyAttribute_21; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_21() { return &___anyAttribute_21; }
	inline void set_anyAttribute_21(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_21 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_21, value);
	}

	inline static int32_t get_offset_of_block_22() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___block_22)); }
	inline int32_t get_block_22() const { return ___block_22; }
	inline int32_t* get_address_of_block_22() { return &___block_22; }
	inline void set_block_22(int32_t value)
	{
		___block_22 = value;
	}

	inline static int32_t get_offset_of_constraints_23() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___constraints_23)); }
	inline CompiledIdentityConstraintU5BU5D_t899482317* get_constraints_23() const { return ___constraints_23; }
	inline CompiledIdentityConstraintU5BU5D_t899482317** get_address_of_constraints_23() { return &___constraints_23; }
	inline void set_constraints_23(CompiledIdentityConstraintU5BU5D_t899482317* value)
	{
		___constraints_23 = value;
		Il2CppCodeGenWriteBarrier(&___constraints_23, value);
	}

	inline static int32_t get_offset_of_schemaElement_24() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905, ___schemaElement_24)); }
	inline XmlSchemaElement_t2433337156 * get_schemaElement_24() const { return ___schemaElement_24; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_schemaElement_24() { return &___schemaElement_24; }
	inline void set_schemaElement_24(XmlSchemaElement_t2433337156 * value)
	{
		___schemaElement_24 = value;
		Il2CppCodeGenWriteBarrier(&___schemaElement_24, value);
	}
};

struct SchemaElementDecl_t1940851905_StaticFields
{
public:
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.SchemaElementDecl::Empty
	SchemaElementDecl_t1940851905 * ___Empty_25;

public:
	inline static int32_t get_offset_of_Empty_25() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1940851905_StaticFields, ___Empty_25)); }
	inline SchemaElementDecl_t1940851905 * get_Empty_25() const { return ___Empty_25; }
	inline SchemaElementDecl_t1940851905 ** get_address_of_Empty_25() { return &___Empty_25; }
	inline void set_Empty_25(SchemaElementDecl_t1940851905 * value)
	{
		___Empty_25 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
