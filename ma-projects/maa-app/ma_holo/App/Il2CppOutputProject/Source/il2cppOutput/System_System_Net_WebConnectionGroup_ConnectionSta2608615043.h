﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Net.WebConnection
struct WebConnection_t324679648;
// System.Net.WebConnectionGroup
struct WebConnectionGroup_t3242458773;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup/ConnectionState
struct  ConnectionState_t2608615043  : public Il2CppObject
{
public:
	// System.Net.WebConnection System.Net.WebConnectionGroup/ConnectionState::<Connection>k__BackingField
	WebConnection_t324679648 * ___U3CConnectionU3Ek__BackingField_0;
	// System.Net.WebConnectionGroup System.Net.WebConnectionGroup/ConnectionState::<Group>k__BackingField
	WebConnectionGroup_t3242458773 * ___U3CGroupU3Ek__BackingField_1;
	// System.Boolean System.Net.WebConnectionGroup/ConnectionState::busy
	bool ___busy_2;
	// System.DateTime System.Net.WebConnectionGroup/ConnectionState::idleSince
	DateTime_t693205669  ___idleSince_3;

public:
	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectionState_t2608615043, ___U3CConnectionU3Ek__BackingField_0)); }
	inline WebConnection_t324679648 * get_U3CConnectionU3Ek__BackingField_0() const { return ___U3CConnectionU3Ek__BackingField_0; }
	inline WebConnection_t324679648 ** get_address_of_U3CConnectionU3Ek__BackingField_0() { return &___U3CConnectionU3Ek__BackingField_0; }
	inline void set_U3CConnectionU3Ek__BackingField_0(WebConnection_t324679648 * value)
	{
		___U3CConnectionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConnectionU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CGroupU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectionState_t2608615043, ___U3CGroupU3Ek__BackingField_1)); }
	inline WebConnectionGroup_t3242458773 * get_U3CGroupU3Ek__BackingField_1() const { return ___U3CGroupU3Ek__BackingField_1; }
	inline WebConnectionGroup_t3242458773 ** get_address_of_U3CGroupU3Ek__BackingField_1() { return &___U3CGroupU3Ek__BackingField_1; }
	inline void set_U3CGroupU3Ek__BackingField_1(WebConnectionGroup_t3242458773 * value)
	{
		___U3CGroupU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGroupU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_busy_2() { return static_cast<int32_t>(offsetof(ConnectionState_t2608615043, ___busy_2)); }
	inline bool get_busy_2() const { return ___busy_2; }
	inline bool* get_address_of_busy_2() { return &___busy_2; }
	inline void set_busy_2(bool value)
	{
		___busy_2 = value;
	}

	inline static int32_t get_offset_of_idleSince_3() { return static_cast<int32_t>(offsetof(ConnectionState_t2608615043, ___idleSince_3)); }
	inline DateTime_t693205669  get_idleSince_3() const { return ___idleSince_3; }
	inline DateTime_t693205669 * get_address_of_idleSince_3() { return &___idleSince_3; }
	inline void set_idleSince_3(DateTime_t693205669  value)
	{
		___idleSince_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
