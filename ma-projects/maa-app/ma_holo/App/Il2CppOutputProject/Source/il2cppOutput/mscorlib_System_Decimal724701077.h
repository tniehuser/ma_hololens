﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Decimal724701077.h"

// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t724701077 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t724701077_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t59386216* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t724701077  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t724701077  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t724701077  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t724701077  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t724701077  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t724701077  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t724701077  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t59386216* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t59386216** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t59386216* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier(&___Powers10_6, value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Zero_7)); }
	inline Decimal_t724701077  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t724701077 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t724701077  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___One_8)); }
	inline Decimal_t724701077  get_One_8() const { return ___One_8; }
	inline Decimal_t724701077 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t724701077  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinusOne_9)); }
	inline Decimal_t724701077  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t724701077 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t724701077  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MaxValue_10)); }
	inline Decimal_t724701077  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t724701077 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t724701077  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinValue_11)); }
	inline Decimal_t724701077  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t724701077 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t724701077  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t724701077  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t724701077 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t724701077  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t724701077  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t724701077 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t724701077  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
