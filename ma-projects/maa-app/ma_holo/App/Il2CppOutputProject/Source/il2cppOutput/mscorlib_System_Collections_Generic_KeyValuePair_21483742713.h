﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "System_Xml_MS_Internal_Xml_Cache_XPathNodeRef2092605142.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct  KeyValuePair_2_t1483742713 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	XPathNodeRef_t2092605142  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	XPathNodeRef_t2092605142  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1483742713, ___key_0)); }
	inline XPathNodeRef_t2092605142  get_key_0() const { return ___key_0; }
	inline XPathNodeRef_t2092605142 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(XPathNodeRef_t2092605142  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1483742713, ___value_1)); }
	inline XPathNodeRef_t2092605142  get_value_1() const { return ___value_1; }
	inline XPathNodeRef_t2092605142 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(XPathNodeRef_t2092605142  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
