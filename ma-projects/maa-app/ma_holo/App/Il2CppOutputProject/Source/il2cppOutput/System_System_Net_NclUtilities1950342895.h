﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NclUtilities
struct  NclUtilities_t1950342895  : public Il2CppObject
{
public:

public:
};

struct NclUtilities_t1950342895_StaticFields
{
public:
	// System.Net.IPAddress[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.NclUtilities::_LocalAddresses
	IPAddressU5BU5D_t4087230954* ____LocalAddresses_0;
	// System.Object System.Net.NclUtilities::_LocalAddressesLock
	Il2CppObject * ____LocalAddressesLock_1;
	// System.String System.Net.NclUtilities::_LocalDomainName
	String_t* ____LocalDomainName_2;

public:
	inline static int32_t get_offset_of__LocalAddresses_0() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalAddresses_0)); }
	inline IPAddressU5BU5D_t4087230954* get__LocalAddresses_0() const { return ____LocalAddresses_0; }
	inline IPAddressU5BU5D_t4087230954** get_address_of__LocalAddresses_0() { return &____LocalAddresses_0; }
	inline void set__LocalAddresses_0(IPAddressU5BU5D_t4087230954* value)
	{
		____LocalAddresses_0 = value;
		Il2CppCodeGenWriteBarrier(&____LocalAddresses_0, value);
	}

	inline static int32_t get_offset_of__LocalAddressesLock_1() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalAddressesLock_1)); }
	inline Il2CppObject * get__LocalAddressesLock_1() const { return ____LocalAddressesLock_1; }
	inline Il2CppObject ** get_address_of__LocalAddressesLock_1() { return &____LocalAddressesLock_1; }
	inline void set__LocalAddressesLock_1(Il2CppObject * value)
	{
		____LocalAddressesLock_1 = value;
		Il2CppCodeGenWriteBarrier(&____LocalAddressesLock_1, value);
	}

	inline static int32_t get_offset_of__LocalDomainName_2() { return static_cast<int32_t>(offsetof(NclUtilities_t1950342895_StaticFields, ____LocalDomainName_2)); }
	inline String_t* get__LocalDomainName_2() const { return ____LocalDomainName_2; }
	inline String_t** get_address_of__LocalDomainName_2() { return &____LocalDomainName_2; }
	inline void set__LocalDomainName_2(String_t* value)
	{
		____LocalDomainName_2 = value;
		Il2CppCodeGenWriteBarrier(&____LocalDomainName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
