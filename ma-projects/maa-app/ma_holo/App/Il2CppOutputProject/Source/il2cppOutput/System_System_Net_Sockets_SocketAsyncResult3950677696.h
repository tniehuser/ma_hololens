﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_IOAsyncResult1276329107.h"
#include "System_System_Net_Sockets_SocketOperation3807127992.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"

// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Exception
struct Exception_t1927440687;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>
struct IList_1_t3135158083;
// System.Threading.WaitCallback
struct WaitCallback_t2798937288;
struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketAsyncResult
struct  SocketAsyncResult_t3950677696  : public IOAsyncResult_t1276329107
{
public:
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::socket
	Socket_t3821512045 * ___socket_5;
	// System.Net.Sockets.SocketOperation System.Net.Sockets.SocketAsyncResult::operation
	int32_t ___operation_6;
	// System.Exception System.Net.Sockets.SocketAsyncResult::DelayedException
	Exception_t1927440687 * ___DelayedException_7;
	// System.Net.EndPoint System.Net.Sockets.SocketAsyncResult::EndPoint
	EndPoint_t4156119363 * ___EndPoint_8;
	// System.Byte[] System.Net.Sockets.SocketAsyncResult::Buffer
	ByteU5BU5D_t3397334013* ___Buffer_9;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Offset
	int32_t ___Offset_10;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Size
	int32_t ___Size_11;
	// System.Net.Sockets.SocketFlags System.Net.Sockets.SocketAsyncResult::SockFlags
	int32_t ___SockFlags_12;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::AcceptSocket
	Socket_t3821512045 * ___AcceptSocket_13;
	// System.Net.IPAddress[] System.Net.Sockets.SocketAsyncResult::Addresses
	IPAddressU5BU5D_t4087230954* ___Addresses_14;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Port
	int32_t ___Port_15;
	// System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>> System.Net.Sockets.SocketAsyncResult::Buffers
	Il2CppObject* ___Buffers_16;
	// System.Boolean System.Net.Sockets.SocketAsyncResult::ReuseSocket
	bool ___ReuseSocket_17;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::CurrentAddress
	int32_t ___CurrentAddress_18;
	// System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncResult::AcceptedSocket
	Socket_t3821512045 * ___AcceptedSocket_19;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::Total
	int32_t ___Total_20;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::error
	int32_t ___error_21;
	// System.Int32 System.Net.Sockets.SocketAsyncResult::EndCalled
	int32_t ___EndCalled_22;

public:
	inline static int32_t get_offset_of_socket_5() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___socket_5)); }
	inline Socket_t3821512045 * get_socket_5() const { return ___socket_5; }
	inline Socket_t3821512045 ** get_address_of_socket_5() { return &___socket_5; }
	inline void set_socket_5(Socket_t3821512045 * value)
	{
		___socket_5 = value;
		Il2CppCodeGenWriteBarrier(&___socket_5, value);
	}

	inline static int32_t get_offset_of_operation_6() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___operation_6)); }
	inline int32_t get_operation_6() const { return ___operation_6; }
	inline int32_t* get_address_of_operation_6() { return &___operation_6; }
	inline void set_operation_6(int32_t value)
	{
		___operation_6 = value;
	}

	inline static int32_t get_offset_of_DelayedException_7() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___DelayedException_7)); }
	inline Exception_t1927440687 * get_DelayedException_7() const { return ___DelayedException_7; }
	inline Exception_t1927440687 ** get_address_of_DelayedException_7() { return &___DelayedException_7; }
	inline void set_DelayedException_7(Exception_t1927440687 * value)
	{
		___DelayedException_7 = value;
		Il2CppCodeGenWriteBarrier(&___DelayedException_7, value);
	}

	inline static int32_t get_offset_of_EndPoint_8() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___EndPoint_8)); }
	inline EndPoint_t4156119363 * get_EndPoint_8() const { return ___EndPoint_8; }
	inline EndPoint_t4156119363 ** get_address_of_EndPoint_8() { return &___EndPoint_8; }
	inline void set_EndPoint_8(EndPoint_t4156119363 * value)
	{
		___EndPoint_8 = value;
		Il2CppCodeGenWriteBarrier(&___EndPoint_8, value);
	}

	inline static int32_t get_offset_of_Buffer_9() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Buffer_9)); }
	inline ByteU5BU5D_t3397334013* get_Buffer_9() const { return ___Buffer_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_Buffer_9() { return &___Buffer_9; }
	inline void set_Buffer_9(ByteU5BU5D_t3397334013* value)
	{
		___Buffer_9 = value;
		Il2CppCodeGenWriteBarrier(&___Buffer_9, value);
	}

	inline static int32_t get_offset_of_Offset_10() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Offset_10)); }
	inline int32_t get_Offset_10() const { return ___Offset_10; }
	inline int32_t* get_address_of_Offset_10() { return &___Offset_10; }
	inline void set_Offset_10(int32_t value)
	{
		___Offset_10 = value;
	}

	inline static int32_t get_offset_of_Size_11() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Size_11)); }
	inline int32_t get_Size_11() const { return ___Size_11; }
	inline int32_t* get_address_of_Size_11() { return &___Size_11; }
	inline void set_Size_11(int32_t value)
	{
		___Size_11 = value;
	}

	inline static int32_t get_offset_of_SockFlags_12() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___SockFlags_12)); }
	inline int32_t get_SockFlags_12() const { return ___SockFlags_12; }
	inline int32_t* get_address_of_SockFlags_12() { return &___SockFlags_12; }
	inline void set_SockFlags_12(int32_t value)
	{
		___SockFlags_12 = value;
	}

	inline static int32_t get_offset_of_AcceptSocket_13() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___AcceptSocket_13)); }
	inline Socket_t3821512045 * get_AcceptSocket_13() const { return ___AcceptSocket_13; }
	inline Socket_t3821512045 ** get_address_of_AcceptSocket_13() { return &___AcceptSocket_13; }
	inline void set_AcceptSocket_13(Socket_t3821512045 * value)
	{
		___AcceptSocket_13 = value;
		Il2CppCodeGenWriteBarrier(&___AcceptSocket_13, value);
	}

	inline static int32_t get_offset_of_Addresses_14() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Addresses_14)); }
	inline IPAddressU5BU5D_t4087230954* get_Addresses_14() const { return ___Addresses_14; }
	inline IPAddressU5BU5D_t4087230954** get_address_of_Addresses_14() { return &___Addresses_14; }
	inline void set_Addresses_14(IPAddressU5BU5D_t4087230954* value)
	{
		___Addresses_14 = value;
		Il2CppCodeGenWriteBarrier(&___Addresses_14, value);
	}

	inline static int32_t get_offset_of_Port_15() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Port_15)); }
	inline int32_t get_Port_15() const { return ___Port_15; }
	inline int32_t* get_address_of_Port_15() { return &___Port_15; }
	inline void set_Port_15(int32_t value)
	{
		___Port_15 = value;
	}

	inline static int32_t get_offset_of_Buffers_16() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Buffers_16)); }
	inline Il2CppObject* get_Buffers_16() const { return ___Buffers_16; }
	inline Il2CppObject** get_address_of_Buffers_16() { return &___Buffers_16; }
	inline void set_Buffers_16(Il2CppObject* value)
	{
		___Buffers_16 = value;
		Il2CppCodeGenWriteBarrier(&___Buffers_16, value);
	}

	inline static int32_t get_offset_of_ReuseSocket_17() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___ReuseSocket_17)); }
	inline bool get_ReuseSocket_17() const { return ___ReuseSocket_17; }
	inline bool* get_address_of_ReuseSocket_17() { return &___ReuseSocket_17; }
	inline void set_ReuseSocket_17(bool value)
	{
		___ReuseSocket_17 = value;
	}

	inline static int32_t get_offset_of_CurrentAddress_18() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___CurrentAddress_18)); }
	inline int32_t get_CurrentAddress_18() const { return ___CurrentAddress_18; }
	inline int32_t* get_address_of_CurrentAddress_18() { return &___CurrentAddress_18; }
	inline void set_CurrentAddress_18(int32_t value)
	{
		___CurrentAddress_18 = value;
	}

	inline static int32_t get_offset_of_AcceptedSocket_19() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___AcceptedSocket_19)); }
	inline Socket_t3821512045 * get_AcceptedSocket_19() const { return ___AcceptedSocket_19; }
	inline Socket_t3821512045 ** get_address_of_AcceptedSocket_19() { return &___AcceptedSocket_19; }
	inline void set_AcceptedSocket_19(Socket_t3821512045 * value)
	{
		___AcceptedSocket_19 = value;
		Il2CppCodeGenWriteBarrier(&___AcceptedSocket_19, value);
	}

	inline static int32_t get_offset_of_Total_20() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___Total_20)); }
	inline int32_t get_Total_20() const { return ___Total_20; }
	inline int32_t* get_address_of_Total_20() { return &___Total_20; }
	inline void set_Total_20(int32_t value)
	{
		___Total_20 = value;
	}

	inline static int32_t get_offset_of_error_21() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___error_21)); }
	inline int32_t get_error_21() const { return ___error_21; }
	inline int32_t* get_address_of_error_21() { return &___error_21; }
	inline void set_error_21(int32_t value)
	{
		___error_21 = value;
	}

	inline static int32_t get_offset_of_EndCalled_22() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696, ___EndCalled_22)); }
	inline int32_t get_EndCalled_22() const { return ___EndCalled_22; }
	inline int32_t* get_address_of_EndCalled_22() { return &___EndCalled_22; }
	inline void set_EndCalled_22(int32_t value)
	{
		___EndCalled_22 = value;
	}
};

struct SocketAsyncResult_t3950677696_StaticFields
{
public:
	// System.Threading.WaitCallback System.Net.Sockets.SocketAsyncResult::<>f__am$cache0
	WaitCallback_t2798937288 * ___U3CU3Ef__amU24cache0_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_23() { return static_cast<int32_t>(offsetof(SocketAsyncResult_t3950677696_StaticFields, ___U3CU3Ef__amU24cache0_23)); }
	inline WaitCallback_t2798937288 * get_U3CU3Ef__amU24cache0_23() const { return ___U3CU3Ef__amU24cache0_23; }
	inline WaitCallback_t2798937288 ** get_address_of_U3CU3Ef__amU24cache0_23() { return &___U3CU3Ef__amU24cache0_23; }
	inline void set_U3CU3Ef__amU24cache0_23(WaitCallback_t2798937288 * value)
	{
		___U3CU3Ef__amU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t3950677696_marshaled_pinvoke : public IOAsyncResult_t1276329107_marshaled_pinvoke
{
	Socket_t3821512045 * ___socket_5;
	int32_t ___operation_6;
	Exception_t1927440687_marshaled_pinvoke* ___DelayedException_7;
	EndPoint_t4156119363 * ___EndPoint_8;
	uint8_t* ___Buffer_9;
	int32_t ___Offset_10;
	int32_t ___Size_11;
	int32_t ___SockFlags_12;
	Socket_t3821512045 * ___AcceptSocket_13;
	IPAddressU5BU5D_t4087230954* ___Addresses_14;
	int32_t ___Port_15;
	Il2CppObject* ___Buffers_16;
	int32_t ___ReuseSocket_17;
	int32_t ___CurrentAddress_18;
	Socket_t3821512045 * ___AcceptedSocket_19;
	int32_t ___Total_20;
	int32_t ___error_21;
	int32_t ___EndCalled_22;
};
// Native definition for COM marshalling of System.Net.Sockets.SocketAsyncResult
struct SocketAsyncResult_t3950677696_marshaled_com : public IOAsyncResult_t1276329107_marshaled_com
{
	Socket_t3821512045 * ___socket_5;
	int32_t ___operation_6;
	Exception_t1927440687_marshaled_com* ___DelayedException_7;
	EndPoint_t4156119363 * ___EndPoint_8;
	uint8_t* ___Buffer_9;
	int32_t ___Offset_10;
	int32_t ___Size_11;
	int32_t ___SockFlags_12;
	Socket_t3821512045 * ___AcceptSocket_13;
	IPAddressU5BU5D_t4087230954* ___Addresses_14;
	int32_t ___Port_15;
	Il2CppObject* ___Buffers_16;
	int32_t ___ReuseSocket_17;
	int32_t ___CurrentAddress_18;
	Socket_t3821512045 * ___AcceptedSocket_19;
	int32_t ___Total_20;
	int32_t ___error_21;
	int32_t ___EndCalled_22;
};
