﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_Match3164245899.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchSparse
struct  MatchSparse_t4194398671  : public Match_t3164245899
{
public:
	// System.Collections.Hashtable System.Text.RegularExpressions.MatchSparse::_caps
	Hashtable_t909839986 * ____caps_16;

public:
	inline static int32_t get_offset_of__caps_16() { return static_cast<int32_t>(offsetof(MatchSparse_t4194398671, ____caps_16)); }
	inline Hashtable_t909839986 * get__caps_16() const { return ____caps_16; }
	inline Hashtable_t909839986 ** get_address_of__caps_16() { return &____caps_16; }
	inline void set__caps_16(Hashtable_t909839986 * value)
	{
		____caps_16 = value;
		Il2CppCodeGenWriteBarrier(&____caps_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
