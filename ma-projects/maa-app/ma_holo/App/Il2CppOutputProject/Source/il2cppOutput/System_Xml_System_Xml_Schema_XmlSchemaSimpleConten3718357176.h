﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaContent3733871217.h"

// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct  XmlSchemaSimpleContentExtension_t3718357176  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentExtension::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_9;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentExtension::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_10;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentExtension::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_11;

public:
	inline static int32_t get_offset_of_attributes_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___attributes_9)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_9() const { return ___attributes_9; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_9() { return &___attributes_9; }
	inline void set_attributes_9(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_9 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_9, value);
	}

	inline static int32_t get_offset_of_anyAttribute_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___anyAttribute_10)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_10() const { return ___anyAttribute_10; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_10() { return &___anyAttribute_10; }
	inline void set_anyAttribute_10(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_10 = value;
		Il2CppCodeGenWriteBarrier(&___anyAttribute_10, value);
	}

	inline static int32_t get_offset_of_baseTypeName_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___baseTypeName_11)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_11() const { return ___baseTypeName_11; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_11() { return &___baseTypeName_11; }
	inline void set_baseTypeName_11(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseTypeName_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
