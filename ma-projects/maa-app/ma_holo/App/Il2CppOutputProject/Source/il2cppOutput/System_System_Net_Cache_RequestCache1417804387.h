﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cache.RequestCache
struct  RequestCache_t1417804387  : public Il2CppObject
{
public:

public:
};

struct RequestCache_t1417804387_StaticFields
{
public:
	// System.Char[] System.Net.Cache.RequestCache::LineSplits
	CharU5BU5D_t1328083999* ___LineSplits_0;

public:
	inline static int32_t get_offset_of_LineSplits_0() { return static_cast<int32_t>(offsetof(RequestCache_t1417804387_StaticFields, ___LineSplits_0)); }
	inline CharU5BU5D_t1328083999* get_LineSplits_0() const { return ___LineSplits_0; }
	inline CharU5BU5D_t1328083999** get_address_of_LineSplits_0() { return &___LineSplits_0; }
	inline void set_LineSplits_0(CharU5BU5D_t1328083999* value)
	{
		___LineSplits_0 = value;
		Il2CppCodeGenWriteBarrier(&___LineSplits_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
