﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfoEnumerator
struct  SerializationInfoEnumerator_t589103770  : public Il2CppObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfoEnumerator::m_members
	StringU5BU5D_t1642385972* ___m_members_0;
	// System.Object[] System.Runtime.Serialization.SerializationInfoEnumerator::m_data
	ObjectU5BU5D_t3614634134* ___m_data_1;
	// System.Type[] System.Runtime.Serialization.SerializationInfoEnumerator::m_types
	TypeU5BU5D_t1664964607* ___m_types_2;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_numItems
	int32_t ___m_numItems_3;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_currItem
	int32_t ___m_currItem_4;
	// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::m_current
	bool ___m_current_5;

public:
	inline static int32_t get_offset_of_m_members_0() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_members_0)); }
	inline StringU5BU5D_t1642385972* get_m_members_0() const { return ___m_members_0; }
	inline StringU5BU5D_t1642385972** get_address_of_m_members_0() { return &___m_members_0; }
	inline void set_m_members_0(StringU5BU5D_t1642385972* value)
	{
		___m_members_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_members_0, value);
	}

	inline static int32_t get_offset_of_m_data_1() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_data_1)); }
	inline ObjectU5BU5D_t3614634134* get_m_data_1() const { return ___m_data_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_data_1() { return &___m_data_1; }
	inline void set_m_data_1(ObjectU5BU5D_t3614634134* value)
	{
		___m_data_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_1, value);
	}

	inline static int32_t get_offset_of_m_types_2() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_types_2)); }
	inline TypeU5BU5D_t1664964607* get_m_types_2() const { return ___m_types_2; }
	inline TypeU5BU5D_t1664964607** get_address_of_m_types_2() { return &___m_types_2; }
	inline void set_m_types_2(TypeU5BU5D_t1664964607* value)
	{
		___m_types_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_types_2, value);
	}

	inline static int32_t get_offset_of_m_numItems_3() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_numItems_3)); }
	inline int32_t get_m_numItems_3() const { return ___m_numItems_3; }
	inline int32_t* get_address_of_m_numItems_3() { return &___m_numItems_3; }
	inline void set_m_numItems_3(int32_t value)
	{
		___m_numItems_3 = value;
	}

	inline static int32_t get_offset_of_m_currItem_4() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_currItem_4)); }
	inline int32_t get_m_currItem_4() const { return ___m_currItem_4; }
	inline int32_t* get_address_of_m_currItem_4() { return &___m_currItem_4; }
	inline void set_m_currItem_4(int32_t value)
	{
		___m_currItem_4 = value;
	}

	inline static int32_t get_offset_of_m_current_5() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t589103770, ___m_current_5)); }
	inline bool get_m_current_5() const { return ___m_current_5; }
	inline bool* get_address_of_m_current_5() { return &___m_current_5; }
	inline void set_m_current_5(bool value)
	{
		___m_current_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
