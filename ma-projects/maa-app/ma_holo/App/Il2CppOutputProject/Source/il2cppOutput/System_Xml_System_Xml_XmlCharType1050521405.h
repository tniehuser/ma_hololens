﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t1050521405 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t3397334013* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405, ___charProperties_2)); }
	inline ByteU5BU5D_t3397334013* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t3397334013* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier(&___charProperties_2, value);
	}
};

struct XmlCharType_t1050521405_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	Il2CppObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t3397334013* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_Lock_0)); }
	inline Il2CppObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline Il2CppObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(Il2CppObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Lock_0, value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t3397334013* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t3397334013* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_CharProperties_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_com
{
	uint8_t* ___charProperties_2;
};
