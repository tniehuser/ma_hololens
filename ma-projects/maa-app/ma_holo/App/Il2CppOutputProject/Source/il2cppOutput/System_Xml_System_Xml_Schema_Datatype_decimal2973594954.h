﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_Datatype_anySimpleTyp4012795865.h"

// System.Type
struct Type_t;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_decimal
struct  Datatype_decimal_t2973594954  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_decimal_t2973594954_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_decimal::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_decimal::listValueType
	Type_t * ___listValueType_94;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_decimal::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_95;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier(&___atomicValueType_93, value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier(&___listValueType_94, value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_95() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___numeric10FacetsChecker_95)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_95() const { return ___numeric10FacetsChecker_95; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_95() { return &___numeric10FacetsChecker_95; }
	inline void set_numeric10FacetsChecker_95(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_95 = value;
		Il2CppCodeGenWriteBarrier(&___numeric10FacetsChecker_95, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
