﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream4089752859.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2540099417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTyp859870085.h"

// Mono.Security.Protocol.Tls.Context
struct Context_t4285182719;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct  HandshakeMessage_t3938752374  : public TlsStream_t4089752859
{
public:
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::context
	Context_t4285182719 * ___context_12;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::handshakeType
	uint8_t ___handshakeType_13;
	// Mono.Security.Protocol.Tls.ContentType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::contentType
	uint8_t ___contentType_14;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::cache
	ByteU5BU5D_t3397334013* ___cache_15;

public:
	inline static int32_t get_offset_of_context_12() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___context_12)); }
	inline Context_t4285182719 * get_context_12() const { return ___context_12; }
	inline Context_t4285182719 ** get_address_of_context_12() { return &___context_12; }
	inline void set_context_12(Context_t4285182719 * value)
	{
		___context_12 = value;
		Il2CppCodeGenWriteBarrier(&___context_12, value);
	}

	inline static int32_t get_offset_of_handshakeType_13() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___handshakeType_13)); }
	inline uint8_t get_handshakeType_13() const { return ___handshakeType_13; }
	inline uint8_t* get_address_of_handshakeType_13() { return &___handshakeType_13; }
	inline void set_handshakeType_13(uint8_t value)
	{
		___handshakeType_13 = value;
	}

	inline static int32_t get_offset_of_contentType_14() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___contentType_14)); }
	inline uint8_t get_contentType_14() const { return ___contentType_14; }
	inline uint8_t* get_address_of_contentType_14() { return &___contentType_14; }
	inline void set_contentType_14(uint8_t value)
	{
		___contentType_14 = value;
	}

	inline static int32_t get_offset_of_cache_15() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___cache_15)); }
	inline ByteU5BU5D_t3397334013* get_cache_15() const { return ___cache_15; }
	inline ByteU5BU5D_t3397334013** get_address_of_cache_15() { return &___cache_15; }
	inline void set_cache_15(ByteU5BU5D_t3397334013* value)
	{
		___cache_15 = value;
		Il2CppCodeGenWriteBarrier(&___cache_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
