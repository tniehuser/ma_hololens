﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.TypeLoadExceptionHolder
struct  TypeLoadExceptionHolder_t427439951  : public Il2CppObject
{
public:
	// System.String System.Runtime.Serialization.TypeLoadExceptionHolder::m_typeName
	String_t* ___m_typeName_0;

public:
	inline static int32_t get_offset_of_m_typeName_0() { return static_cast<int32_t>(offsetof(TypeLoadExceptionHolder_t427439951, ___m_typeName_0)); }
	inline String_t* get_m_typeName_0() const { return ___m_typeName_0; }
	inline String_t** get_address_of_m_typeName_0() { return &___m_typeName_0; }
	inline void set_m_typeName_0(String_t* value)
	{
		___m_typeName_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_typeName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
