﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"

// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t717347117;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t192177157;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct  XmlSchemaSimpleTypeUnion_t91327365  : public XmlSchemaSimpleTypeContent_t1606103299
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseTypes
	XmlSchemaObjectCollection_t395083109 * ___baseTypes_9;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::memberTypes
	XmlQualifiedNameU5BU5D_t717347117* ___memberTypes_10;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseMemberTypes
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___baseMemberTypes_11;

public:
	inline static int32_t get_offset_of_baseTypes_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___baseTypes_9)); }
	inline XmlSchemaObjectCollection_t395083109 * get_baseTypes_9() const { return ___baseTypes_9; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_baseTypes_9() { return &___baseTypes_9; }
	inline void set_baseTypes_9(XmlSchemaObjectCollection_t395083109 * value)
	{
		___baseTypes_9 = value;
		Il2CppCodeGenWriteBarrier(&___baseTypes_9, value);
	}

	inline static int32_t get_offset_of_memberTypes_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___memberTypes_10)); }
	inline XmlQualifiedNameU5BU5D_t717347117* get_memberTypes_10() const { return ___memberTypes_10; }
	inline XmlQualifiedNameU5BU5D_t717347117** get_address_of_memberTypes_10() { return &___memberTypes_10; }
	inline void set_memberTypes_10(XmlQualifiedNameU5BU5D_t717347117* value)
	{
		___memberTypes_10 = value;
		Il2CppCodeGenWriteBarrier(&___memberTypes_10, value);
	}

	inline static int32_t get_offset_of_baseMemberTypes_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___baseMemberTypes_11)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_baseMemberTypes_11() const { return ___baseMemberTypes_11; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_baseMemberTypes_11() { return &___baseMemberTypes_11; }
	inline void set_baseMemberTypes_11(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___baseMemberTypes_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseMemberTypes_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
