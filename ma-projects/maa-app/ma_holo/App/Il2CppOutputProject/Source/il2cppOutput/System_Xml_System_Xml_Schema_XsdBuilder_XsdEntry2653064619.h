﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaNames_Token1005517746.h"
#include "System_Xml_System_Xml_Schema_XsdBuilder_State2667800191.h"

// System.Xml.Schema.XsdBuilder/State[]
struct StateU5BU5D_t717950118;
// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[]
struct XsdAttributeEntryU5BU5D_t3765641446;
// System.Xml.Schema.XsdBuilder/XsdInitFunction
struct XsdInitFunction_t862750143;
// System.Xml.Schema.XsdBuilder/XsdEndChildFunction
struct XsdEndChildFunction_t839604892;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdEntry
struct  XsdEntry_t2653064619  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XsdBuilder/XsdEntry::Name
	int32_t ___Name_0;
	// System.Xml.Schema.XsdBuilder/State System.Xml.Schema.XsdBuilder/XsdEntry::CurrentState
	int32_t ___CurrentState_1;
	// System.Xml.Schema.XsdBuilder/State[] System.Xml.Schema.XsdBuilder/XsdEntry::NextStates
	StateU5BU5D_t717950118* ___NextStates_2;
	// System.Xml.Schema.XsdBuilder/XsdAttributeEntry[] System.Xml.Schema.XsdBuilder/XsdEntry::Attributes
	XsdAttributeEntryU5BU5D_t3765641446* ___Attributes_3;
	// System.Xml.Schema.XsdBuilder/XsdInitFunction System.Xml.Schema.XsdBuilder/XsdEntry::InitFunc
	XsdInitFunction_t862750143 * ___InitFunc_4;
	// System.Xml.Schema.XsdBuilder/XsdEndChildFunction System.Xml.Schema.XsdBuilder/XsdEntry::EndChildFunc
	XsdEndChildFunction_t839604892 * ___EndChildFunc_5;
	// System.Boolean System.Xml.Schema.XsdBuilder/XsdEntry::ParseContent
	bool ___ParseContent_6;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___Name_0)); }
	inline int32_t get_Name_0() const { return ___Name_0; }
	inline int32_t* get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(int32_t value)
	{
		___Name_0 = value;
	}

	inline static int32_t get_offset_of_CurrentState_1() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___CurrentState_1)); }
	inline int32_t get_CurrentState_1() const { return ___CurrentState_1; }
	inline int32_t* get_address_of_CurrentState_1() { return &___CurrentState_1; }
	inline void set_CurrentState_1(int32_t value)
	{
		___CurrentState_1 = value;
	}

	inline static int32_t get_offset_of_NextStates_2() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___NextStates_2)); }
	inline StateU5BU5D_t717950118* get_NextStates_2() const { return ___NextStates_2; }
	inline StateU5BU5D_t717950118** get_address_of_NextStates_2() { return &___NextStates_2; }
	inline void set_NextStates_2(StateU5BU5D_t717950118* value)
	{
		___NextStates_2 = value;
		Il2CppCodeGenWriteBarrier(&___NextStates_2, value);
	}

	inline static int32_t get_offset_of_Attributes_3() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___Attributes_3)); }
	inline XsdAttributeEntryU5BU5D_t3765641446* get_Attributes_3() const { return ___Attributes_3; }
	inline XsdAttributeEntryU5BU5D_t3765641446** get_address_of_Attributes_3() { return &___Attributes_3; }
	inline void set_Attributes_3(XsdAttributeEntryU5BU5D_t3765641446* value)
	{
		___Attributes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Attributes_3, value);
	}

	inline static int32_t get_offset_of_InitFunc_4() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___InitFunc_4)); }
	inline XsdInitFunction_t862750143 * get_InitFunc_4() const { return ___InitFunc_4; }
	inline XsdInitFunction_t862750143 ** get_address_of_InitFunc_4() { return &___InitFunc_4; }
	inline void set_InitFunc_4(XsdInitFunction_t862750143 * value)
	{
		___InitFunc_4 = value;
		Il2CppCodeGenWriteBarrier(&___InitFunc_4, value);
	}

	inline static int32_t get_offset_of_EndChildFunc_5() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___EndChildFunc_5)); }
	inline XsdEndChildFunction_t839604892 * get_EndChildFunc_5() const { return ___EndChildFunc_5; }
	inline XsdEndChildFunction_t839604892 ** get_address_of_EndChildFunc_5() { return &___EndChildFunc_5; }
	inline void set_EndChildFunc_5(XsdEndChildFunction_t839604892 * value)
	{
		___EndChildFunc_5 = value;
		Il2CppCodeGenWriteBarrier(&___EndChildFunc_5, value);
	}

	inline static int32_t get_offset_of_ParseContent_6() { return static_cast<int32_t>(offsetof(XsdEntry_t2653064619, ___ParseContent_6)); }
	inline bool get_ParseContent_6() const { return ___ParseContent_6; }
	inline bool* get_address_of_ParseContent_6() { return &___ParseContent_6; }
	inline void set_ParseContent_6(bool value)
	{
		___ParseContent_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
