﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeZoneInfo_TransitionTime3441274853.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/AdjustmentRule
struct  AdjustmentRule_t2179708818  : public Il2CppObject
{
public:
	// System.DateTime System.TimeZoneInfo/AdjustmentRule::m_dateStart
	DateTime_t693205669  ___m_dateStart_0;
	// System.DateTime System.TimeZoneInfo/AdjustmentRule::m_dateEnd
	DateTime_t693205669  ___m_dateEnd_1;
	// System.TimeSpan System.TimeZoneInfo/AdjustmentRule::m_daylightDelta
	TimeSpan_t3430258949  ___m_daylightDelta_2;
	// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::m_daylightTransitionStart
	TransitionTime_t3441274853  ___m_daylightTransitionStart_3;
	// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::m_daylightTransitionEnd
	TransitionTime_t3441274853  ___m_daylightTransitionEnd_4;
	// System.TimeSpan System.TimeZoneInfo/AdjustmentRule::m_baseUtcOffsetDelta
	TimeSpan_t3430258949  ___m_baseUtcOffsetDelta_5;
	// System.Boolean System.TimeZoneInfo/AdjustmentRule::m_noDaylightTransitions
	bool ___m_noDaylightTransitions_6;

public:
	inline static int32_t get_offset_of_m_dateStart_0() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_dateStart_0)); }
	inline DateTime_t693205669  get_m_dateStart_0() const { return ___m_dateStart_0; }
	inline DateTime_t693205669 * get_address_of_m_dateStart_0() { return &___m_dateStart_0; }
	inline void set_m_dateStart_0(DateTime_t693205669  value)
	{
		___m_dateStart_0 = value;
	}

	inline static int32_t get_offset_of_m_dateEnd_1() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_dateEnd_1)); }
	inline DateTime_t693205669  get_m_dateEnd_1() const { return ___m_dateEnd_1; }
	inline DateTime_t693205669 * get_address_of_m_dateEnd_1() { return &___m_dateEnd_1; }
	inline void set_m_dateEnd_1(DateTime_t693205669  value)
	{
		___m_dateEnd_1 = value;
	}

	inline static int32_t get_offset_of_m_daylightDelta_2() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_daylightDelta_2)); }
	inline TimeSpan_t3430258949  get_m_daylightDelta_2() const { return ___m_daylightDelta_2; }
	inline TimeSpan_t3430258949 * get_address_of_m_daylightDelta_2() { return &___m_daylightDelta_2; }
	inline void set_m_daylightDelta_2(TimeSpan_t3430258949  value)
	{
		___m_daylightDelta_2 = value;
	}

	inline static int32_t get_offset_of_m_daylightTransitionStart_3() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_daylightTransitionStart_3)); }
	inline TransitionTime_t3441274853  get_m_daylightTransitionStart_3() const { return ___m_daylightTransitionStart_3; }
	inline TransitionTime_t3441274853 * get_address_of_m_daylightTransitionStart_3() { return &___m_daylightTransitionStart_3; }
	inline void set_m_daylightTransitionStart_3(TransitionTime_t3441274853  value)
	{
		___m_daylightTransitionStart_3 = value;
	}

	inline static int32_t get_offset_of_m_daylightTransitionEnd_4() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_daylightTransitionEnd_4)); }
	inline TransitionTime_t3441274853  get_m_daylightTransitionEnd_4() const { return ___m_daylightTransitionEnd_4; }
	inline TransitionTime_t3441274853 * get_address_of_m_daylightTransitionEnd_4() { return &___m_daylightTransitionEnd_4; }
	inline void set_m_daylightTransitionEnd_4(TransitionTime_t3441274853  value)
	{
		___m_daylightTransitionEnd_4 = value;
	}

	inline static int32_t get_offset_of_m_baseUtcOffsetDelta_5() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_baseUtcOffsetDelta_5)); }
	inline TimeSpan_t3430258949  get_m_baseUtcOffsetDelta_5() const { return ___m_baseUtcOffsetDelta_5; }
	inline TimeSpan_t3430258949 * get_address_of_m_baseUtcOffsetDelta_5() { return &___m_baseUtcOffsetDelta_5; }
	inline void set_m_baseUtcOffsetDelta_5(TimeSpan_t3430258949  value)
	{
		___m_baseUtcOffsetDelta_5 = value;
	}

	inline static int32_t get_offset_of_m_noDaylightTransitions_6() { return static_cast<int32_t>(offsetof(AdjustmentRule_t2179708818, ___m_noDaylightTransitions_6)); }
	inline bool get_m_noDaylightTransitions_6() const { return ___m_noDaylightTransitions_6; }
	inline bool* get_address_of_m_noDaylightTransitions_6() { return &___m_noDaylightTransitions_6; }
	inline void set_m_noDaylightTransitions_6(bool value)
	{
		___m_noDaylightTransitions_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
