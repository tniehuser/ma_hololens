﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_SimpleAsyncResult2937691397.h"

// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2828383075;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebAsyncResult
struct  WebAsyncResult_t905414499  : public SimpleAsyncResult_t2937691397
{
public:
	// System.Int32 System.Net.WebAsyncResult::nbytes
	int32_t ___nbytes_9;
	// System.IAsyncResult System.Net.WebAsyncResult::innerAsyncResult
	Il2CppObject * ___innerAsyncResult_10;
	// System.Net.HttpWebResponse System.Net.WebAsyncResult::response
	HttpWebResponse_t2828383075 * ___response_11;
	// System.IO.Stream System.Net.WebAsyncResult::writeStream
	Stream_t3255436806 * ___writeStream_12;
	// System.Byte[] System.Net.WebAsyncResult::buffer
	ByteU5BU5D_t3397334013* ___buffer_13;
	// System.Int32 System.Net.WebAsyncResult::offset
	int32_t ___offset_14;
	// System.Int32 System.Net.WebAsyncResult::size
	int32_t ___size_15;
	// System.Boolean System.Net.WebAsyncResult::EndCalled
	bool ___EndCalled_16;
	// System.Boolean System.Net.WebAsyncResult::AsyncWriteAll
	bool ___AsyncWriteAll_17;
	// System.Net.HttpWebRequest System.Net.WebAsyncResult::AsyncObject
	HttpWebRequest_t1951404513 * ___AsyncObject_18;

public:
	inline static int32_t get_offset_of_nbytes_9() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___nbytes_9)); }
	inline int32_t get_nbytes_9() const { return ___nbytes_9; }
	inline int32_t* get_address_of_nbytes_9() { return &___nbytes_9; }
	inline void set_nbytes_9(int32_t value)
	{
		___nbytes_9 = value;
	}

	inline static int32_t get_offset_of_innerAsyncResult_10() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___innerAsyncResult_10)); }
	inline Il2CppObject * get_innerAsyncResult_10() const { return ___innerAsyncResult_10; }
	inline Il2CppObject ** get_address_of_innerAsyncResult_10() { return &___innerAsyncResult_10; }
	inline void set_innerAsyncResult_10(Il2CppObject * value)
	{
		___innerAsyncResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___innerAsyncResult_10, value);
	}

	inline static int32_t get_offset_of_response_11() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___response_11)); }
	inline HttpWebResponse_t2828383075 * get_response_11() const { return ___response_11; }
	inline HttpWebResponse_t2828383075 ** get_address_of_response_11() { return &___response_11; }
	inline void set_response_11(HttpWebResponse_t2828383075 * value)
	{
		___response_11 = value;
		Il2CppCodeGenWriteBarrier(&___response_11, value);
	}

	inline static int32_t get_offset_of_writeStream_12() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___writeStream_12)); }
	inline Stream_t3255436806 * get_writeStream_12() const { return ___writeStream_12; }
	inline Stream_t3255436806 ** get_address_of_writeStream_12() { return &___writeStream_12; }
	inline void set_writeStream_12(Stream_t3255436806 * value)
	{
		___writeStream_12 = value;
		Il2CppCodeGenWriteBarrier(&___writeStream_12, value);
	}

	inline static int32_t get_offset_of_buffer_13() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___buffer_13)); }
	inline ByteU5BU5D_t3397334013* get_buffer_13() const { return ___buffer_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_13() { return &___buffer_13; }
	inline void set_buffer_13(ByteU5BU5D_t3397334013* value)
	{
		___buffer_13 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_13, value);
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___offset_14)); }
	inline int32_t get_offset_14() const { return ___offset_14; }
	inline int32_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int32_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_size_15() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___size_15)); }
	inline int32_t get_size_15() const { return ___size_15; }
	inline int32_t* get_address_of_size_15() { return &___size_15; }
	inline void set_size_15(int32_t value)
	{
		___size_15 = value;
	}

	inline static int32_t get_offset_of_EndCalled_16() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___EndCalled_16)); }
	inline bool get_EndCalled_16() const { return ___EndCalled_16; }
	inline bool* get_address_of_EndCalled_16() { return &___EndCalled_16; }
	inline void set_EndCalled_16(bool value)
	{
		___EndCalled_16 = value;
	}

	inline static int32_t get_offset_of_AsyncWriteAll_17() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___AsyncWriteAll_17)); }
	inline bool get_AsyncWriteAll_17() const { return ___AsyncWriteAll_17; }
	inline bool* get_address_of_AsyncWriteAll_17() { return &___AsyncWriteAll_17; }
	inline void set_AsyncWriteAll_17(bool value)
	{
		___AsyncWriteAll_17 = value;
	}

	inline static int32_t get_offset_of_AsyncObject_18() { return static_cast<int32_t>(offsetof(WebAsyncResult_t905414499, ___AsyncObject_18)); }
	inline HttpWebRequest_t1951404513 * get_AsyncObject_18() const { return ___AsyncObject_18; }
	inline HttpWebRequest_t1951404513 ** get_address_of_AsyncObject_18() { return &___AsyncObject_18; }
	inline void set_AsyncObject_18(HttpWebRequest_t1951404513 * value)
	{
		___AsyncObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___AsyncObject_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
