﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_MemberDescriptor3749827553.h"

// System.ComponentModel.TypeConverter
struct TypeConverter_t745995970;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Type[]
struct TypeU5BU5D_t1664964607;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_t4250402154  : public MemberDescriptor_t3749827553
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t745995970 * ___converter_11;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3614634134* ___editors_12;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t1664964607* ___editorTypes_13;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_14;

public:
	inline static int32_t get_offset_of_converter_11() { return static_cast<int32_t>(offsetof(PropertyDescriptor_t4250402154, ___converter_11)); }
	inline TypeConverter_t745995970 * get_converter_11() const { return ___converter_11; }
	inline TypeConverter_t745995970 ** get_address_of_converter_11() { return &___converter_11; }
	inline void set_converter_11(TypeConverter_t745995970 * value)
	{
		___converter_11 = value;
		Il2CppCodeGenWriteBarrier(&___converter_11, value);
	}

	inline static int32_t get_offset_of_editors_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_t4250402154, ___editors_12)); }
	inline ObjectU5BU5D_t3614634134* get_editors_12() const { return ___editors_12; }
	inline ObjectU5BU5D_t3614634134** get_address_of_editors_12() { return &___editors_12; }
	inline void set_editors_12(ObjectU5BU5D_t3614634134* value)
	{
		___editors_12 = value;
		Il2CppCodeGenWriteBarrier(&___editors_12, value);
	}

	inline static int32_t get_offset_of_editorTypes_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_t4250402154, ___editorTypes_13)); }
	inline TypeU5BU5D_t1664964607* get_editorTypes_13() const { return ___editorTypes_13; }
	inline TypeU5BU5D_t1664964607** get_address_of_editorTypes_13() { return &___editorTypes_13; }
	inline void set_editorTypes_13(TypeU5BU5D_t1664964607* value)
	{
		___editorTypes_13 = value;
		Il2CppCodeGenWriteBarrier(&___editorTypes_13, value);
	}

	inline static int32_t get_offset_of_editorCount_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_t4250402154, ___editorCount_14)); }
	inline int32_t get_editorCount_14() const { return ___editorCount_14; }
	inline int32_t* get_address_of_editorCount_14() { return &___editorCount_14; }
	inline void set_editorCount_14(int32_t value)
	{
		___editorCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
