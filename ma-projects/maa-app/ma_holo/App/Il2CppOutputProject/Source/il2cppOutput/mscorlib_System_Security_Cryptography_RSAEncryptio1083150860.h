﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_RSAEncryptio4275626723.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith1682985260.h"

// System.Security.Cryptography.RSAEncryptionPadding
struct RSAEncryptionPadding_t1083150860;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAEncryptionPadding
struct  RSAEncryptionPadding_t1083150860  : public Il2CppObject
{
public:
	// System.Security.Cryptography.RSAEncryptionPaddingMode System.Security.Cryptography.RSAEncryptionPadding::_mode
	int32_t ____mode_5;
	// System.Security.Cryptography.HashAlgorithmName System.Security.Cryptography.RSAEncryptionPadding::_oaepHashAlgorithm
	HashAlgorithmName_t1682985260  ____oaepHashAlgorithm_6;

public:
	inline static int32_t get_offset_of__mode_5() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860, ____mode_5)); }
	inline int32_t get__mode_5() const { return ____mode_5; }
	inline int32_t* get_address_of__mode_5() { return &____mode_5; }
	inline void set__mode_5(int32_t value)
	{
		____mode_5 = value;
	}

	inline static int32_t get_offset_of__oaepHashAlgorithm_6() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860, ____oaepHashAlgorithm_6)); }
	inline HashAlgorithmName_t1682985260  get__oaepHashAlgorithm_6() const { return ____oaepHashAlgorithm_6; }
	inline HashAlgorithmName_t1682985260 * get_address_of__oaepHashAlgorithm_6() { return &____oaepHashAlgorithm_6; }
	inline void set__oaepHashAlgorithm_6(HashAlgorithmName_t1682985260  value)
	{
		____oaepHashAlgorithm_6 = value;
	}
};

struct RSAEncryptionPadding_t1083150860_StaticFields
{
public:
	// System.Security.Cryptography.RSAEncryptionPadding System.Security.Cryptography.RSAEncryptionPadding::s_pkcs1
	RSAEncryptionPadding_t1083150860 * ___s_pkcs1_0;
	// System.Security.Cryptography.RSAEncryptionPadding System.Security.Cryptography.RSAEncryptionPadding::s_oaepSHA1
	RSAEncryptionPadding_t1083150860 * ___s_oaepSHA1_1;
	// System.Security.Cryptography.RSAEncryptionPadding System.Security.Cryptography.RSAEncryptionPadding::s_oaepSHA256
	RSAEncryptionPadding_t1083150860 * ___s_oaepSHA256_2;
	// System.Security.Cryptography.RSAEncryptionPadding System.Security.Cryptography.RSAEncryptionPadding::s_oaepSHA384
	RSAEncryptionPadding_t1083150860 * ___s_oaepSHA384_3;
	// System.Security.Cryptography.RSAEncryptionPadding System.Security.Cryptography.RSAEncryptionPadding::s_oaepSHA512
	RSAEncryptionPadding_t1083150860 * ___s_oaepSHA512_4;

public:
	inline static int32_t get_offset_of_s_pkcs1_0() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860_StaticFields, ___s_pkcs1_0)); }
	inline RSAEncryptionPadding_t1083150860 * get_s_pkcs1_0() const { return ___s_pkcs1_0; }
	inline RSAEncryptionPadding_t1083150860 ** get_address_of_s_pkcs1_0() { return &___s_pkcs1_0; }
	inline void set_s_pkcs1_0(RSAEncryptionPadding_t1083150860 * value)
	{
		___s_pkcs1_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_pkcs1_0, value);
	}

	inline static int32_t get_offset_of_s_oaepSHA1_1() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860_StaticFields, ___s_oaepSHA1_1)); }
	inline RSAEncryptionPadding_t1083150860 * get_s_oaepSHA1_1() const { return ___s_oaepSHA1_1; }
	inline RSAEncryptionPadding_t1083150860 ** get_address_of_s_oaepSHA1_1() { return &___s_oaepSHA1_1; }
	inline void set_s_oaepSHA1_1(RSAEncryptionPadding_t1083150860 * value)
	{
		___s_oaepSHA1_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_oaepSHA1_1, value);
	}

	inline static int32_t get_offset_of_s_oaepSHA256_2() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860_StaticFields, ___s_oaepSHA256_2)); }
	inline RSAEncryptionPadding_t1083150860 * get_s_oaepSHA256_2() const { return ___s_oaepSHA256_2; }
	inline RSAEncryptionPadding_t1083150860 ** get_address_of_s_oaepSHA256_2() { return &___s_oaepSHA256_2; }
	inline void set_s_oaepSHA256_2(RSAEncryptionPadding_t1083150860 * value)
	{
		___s_oaepSHA256_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_oaepSHA256_2, value);
	}

	inline static int32_t get_offset_of_s_oaepSHA384_3() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860_StaticFields, ___s_oaepSHA384_3)); }
	inline RSAEncryptionPadding_t1083150860 * get_s_oaepSHA384_3() const { return ___s_oaepSHA384_3; }
	inline RSAEncryptionPadding_t1083150860 ** get_address_of_s_oaepSHA384_3() { return &___s_oaepSHA384_3; }
	inline void set_s_oaepSHA384_3(RSAEncryptionPadding_t1083150860 * value)
	{
		___s_oaepSHA384_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_oaepSHA384_3, value);
	}

	inline static int32_t get_offset_of_s_oaepSHA512_4() { return static_cast<int32_t>(offsetof(RSAEncryptionPadding_t1083150860_StaticFields, ___s_oaepSHA512_4)); }
	inline RSAEncryptionPadding_t1083150860 * get_s_oaepSHA512_4() const { return ___s_oaepSHA512_4; }
	inline RSAEncryptionPadding_t1083150860 ** get_address_of_s_oaepSHA512_4() { return &___s_oaepSHA512_4; }
	inline void set_s_oaepSHA512_4(RSAEncryptionPadding_t1083150860 * value)
	{
		___s_oaepSHA512_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_oaepSHA512_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
