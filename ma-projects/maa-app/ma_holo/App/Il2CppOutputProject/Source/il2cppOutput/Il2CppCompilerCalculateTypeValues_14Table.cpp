﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_EncodingHelper2088181889.h"
#include "mscorlib_System_Text_Latin1Encoding3483306430.h"
#include "mscorlib_System_Text_NormalizationForm2455198143.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_Threading_LockQueue2670078596.h"
#include "mscorlib_System_Threading_Mutex297030111.h"
#include "mscorlib_System_Threading_NativeEventCalls1850675218.h"
#include "mscorlib_System_Threading_NativeOverlapped2890300033.h"
#include "mscorlib_System_Threading_ReaderWriterLock431196913.h"
#include "mscorlib_System_Threading_RegisteredWaitHandle2863394393.h"
#include "mscorlib_System_Threading_InternalThread4118012807.h"
#include "mscorlib_System_Threading_Timer791717973.h"
#include "mscorlib_System_Threading_Timer_TimerComparer876299723.h"
#include "mscorlib_System_Threading_Timer_Scheduler697594.h"
#include "mscorlib_System_Threading_TimerCallback1684927372.h"
#include "mscorlib_System_Threading_Volatile1178602802.h"
#include "mscorlib_System_ActivationContext1572332809.h"
#include "mscorlib_System_AppDomainInitializer3898244613.h"
#include "mscorlib_System_AppDomainManager54965696.h"
#include "mscorlib_System_AppDomainSetup611332832.h"
#include "mscorlib_System_ApplicationIdentity3292367950.h"
#include "mscorlib_System_ArgIterator2628088752.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Array_QSortStack3667817390.h"
#include "mscorlib_System_Array_SimpleEnumerator4019359169.h"
#include "mscorlib_System_AssemblyLoadEventArgs4233815743.h"
#include "mscorlib_System_AssemblyLoadEventHandler2169307382.h"
#include "mscorlib_System_IO_CStreamReader2584617632.h"
#include "mscorlib_System_IO_CStreamWriter2592797654.h"
#include "mscorlib_System_Console2311202731.h"
#include "mscorlib_System_Console_WindowsConsole1329628201.h"
#include "mscorlib_System_Console_WindowsConsole_WindowsCanc1197082027.h"
#include "mscorlib_System_Console_InternalCancelHandler2895223116.h"
#include "mscorlib_System_ConsoleDriver2410871357.h"
#include "mscorlib_System_DelegateData1572802995.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_DelegateSerializationHolder753405077.h"
#include "mscorlib_System_DelegateSerializationHolder_Delega3215410094.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_KnownTerminals368610438.h"
#include "mscorlib_System_MarshalByRefObject1285298191.h"
#include "mscorlib_System_MonoAsyncCall3796687503.h"
#include "mscorlib_System_MonoCustomAttrs2976585698.h"
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo2366110328.h"
#include "mscorlib_System_MonoType42245407.h"
#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_NullConsoleDriver2151813504.h"
#include "mscorlib_System_Nullable1429764613.h"
#include "mscorlib_System_NumberFormatter2933946347.h"
#include "mscorlib_System_NumberFormatter_CustomInfo128587492.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_OperatingSystem290860502.h"
#include "mscorlib_System_PlatformID1006634368.h"
#include "mscorlib_System_ResolveEventArgs1859808873.h"
#include "mscorlib_System_ResolveEventHandler3842432458.h"
#include "mscorlib_System_RuntimeArgumentHandle3259266975.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "mscorlib_System_RuntimeMethodHandle894824333.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "mscorlib_System_TermInfoDriver3839442152.h"
#include "mscorlib_System_ParameterizedStrings2474305833.h"
#include "mscorlib_System_ParameterizedStrings_FormatParam2947516451.h"
#include "mscorlib_System_ParameterizedStrings_LowLevelStack375164147.h"
#include "mscorlib_System_ByteMatcher2890532040.h"
#include "mscorlib_System_TermInfoNumbers1380902466.h"
#include "mscorlib_System_TermInfoReader3840788697.h"
#include "mscorlib_System_TermInfoStrings1425267120.h"
#include "mscorlib_System_TimeZoneInfoOptions106353153.h"
#include "mscorlib_System_TimeZoneInfo436210607.h"
#include "mscorlib_System_TimeZoneInfo_TimeZoneInfoResult2374900455.h"
#include "mscorlib_System_TimeZoneInfo_CachedData3197336479.h"
#include "mscorlib_System_TimeZoneInfo_OffsetAndRule2067953655.h"
#include "mscorlib_System_TimeZoneInfo_AdjustmentRule2179708818.h"
#include "mscorlib_System_TimeZoneInfo_TransitionTime3441274853.h"
#include "mscorlib_System_TimeZoneInfo_TZifType1855764066.h"
#include "mscorlib_System_TimeZoneInfo_TZifHead3630268560.h"
#include "mscorlib_System_TimeZoneInfo_TZVersion2863501951.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "mscorlib_System_TypeNames3900030990.h"
#include "mscorlib_System_TypeNames_ATypeName2751073424.h"
#include "mscorlib_System_TypeIdentifiers1234132058.h"
#include "mscorlib_System_TypeIdentifiers_Display4153290110.h"
#include "mscorlib_System_TypeIdentifiers_NoEscape69741980.h"
#include "mscorlib_System_ArraySpec2770119450.h"
#include "mscorlib_System_PointerSpec2712181132.h"
#include "mscorlib_System_TypeSpec2066641911.h"
#include "mscorlib_System_TypeSpec_DisplayNameFormat3375299945.h"
#include "mscorlib_System_UIntPtr1549717846.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_WeakReference1077405567.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (EncodingHelper_t2088181889), -1, sizeof(EncodingHelper_t2088181889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1400[4] = 
{
	EncodingHelper_t2088181889_StaticFields::get_offset_of_utf8EncodingWithoutMarkers_0(),
	EncodingHelper_t2088181889_StaticFields::get_offset_of_lockobj_1(),
	EncodingHelper_t2088181889_StaticFields::get_offset_of_i18nAssembly_2(),
	EncodingHelper_t2088181889_StaticFields::get_offset_of_i18nDisabled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (Latin1Encoding_t3483306430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (NormalizationForm_t2455198143)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1402[5] = 
{
	NormalizationForm_t2455198143::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (Interlocked_t1625106012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (LockQueue_t2670078596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[2] = 
{
	LockQueue_t2670078596::get_offset_of_rwlock_0(),
	LockQueue_t2670078596::get_offset_of_lockCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (Mutex_t297030111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (NativeEventCalls_t1850675218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (NativeOverlapped_t2890300033)+ sizeof (Il2CppObject), sizeof(NativeOverlapped_t2890300033 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1407[5] = 
{
	NativeOverlapped_t2890300033::get_offset_of_InternalLow_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeOverlapped_t2890300033::get_offset_of_InternalHigh_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeOverlapped_t2890300033::get_offset_of_OffsetLow_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeOverlapped_t2890300033::get_offset_of_OffsetHigh_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeOverlapped_t2890300033::get_offset_of_EventHandle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (ReaderWriterLock_t431196913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1408[5] = 
{
	ReaderWriterLock_t431196913::get_offset_of_seq_num_0(),
	ReaderWriterLock_t431196913::get_offset_of_state_1(),
	ReaderWriterLock_t431196913::get_offset_of_readers_2(),
	ReaderWriterLock_t431196913::get_offset_of_writer_lock_owner_3(),
	ReaderWriterLock_t431196913::get_offset_of_writer_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (RegisteredWaitHandle_t2863394393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1409[9] = 
{
	RegisteredWaitHandle_t2863394393::get_offset_of__waitObject_1(),
	RegisteredWaitHandle_t2863394393::get_offset_of__callback_2(),
	RegisteredWaitHandle_t2863394393::get_offset_of__state_3(),
	RegisteredWaitHandle_t2863394393::get_offset_of__finalEvent_4(),
	RegisteredWaitHandle_t2863394393::get_offset_of__cancelEvent_5(),
	RegisteredWaitHandle_t2863394393::get_offset_of__timeout_6(),
	RegisteredWaitHandle_t2863394393::get_offset_of__callsInProcess_7(),
	RegisteredWaitHandle_t2863394393::get_offset_of__executeOnlyOnce_8(),
	RegisteredWaitHandle_t2863394393::get_offset_of__unregistered_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (InternalThread_t4118012807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1410[34] = 
{
	InternalThread_t4118012807::get_offset_of_lock_thread_id_0(),
	InternalThread_t4118012807::get_offset_of_system_thread_handle_1(),
	InternalThread_t4118012807::get_offset_of_cached_culture_info_2(),
	InternalThread_t4118012807::get_offset_of_name_3(),
	InternalThread_t4118012807::get_offset_of_name_len_4(),
	InternalThread_t4118012807::get_offset_of_state_5(),
	InternalThread_t4118012807::get_offset_of_abort_exc_6(),
	InternalThread_t4118012807::get_offset_of_abort_state_handle_7(),
	InternalThread_t4118012807::get_offset_of_thread_id_8(),
	InternalThread_t4118012807::get_offset_of_stack_ptr_9(),
	InternalThread_t4118012807::get_offset_of_static_data_10(),
	InternalThread_t4118012807::get_offset_of_runtime_thread_info_11(),
	InternalThread_t4118012807::get_offset_of_current_appcontext_12(),
	InternalThread_t4118012807::get_offset_of_root_domain_thread_13(),
	InternalThread_t4118012807::get_offset_of__serialized_principal_14(),
	InternalThread_t4118012807::get_offset_of__serialized_principal_version_15(),
	InternalThread_t4118012807::get_offset_of_appdomain_refs_16(),
	InternalThread_t4118012807::get_offset_of_interruption_requested_17(),
	InternalThread_t4118012807::get_offset_of_synch_cs_18(),
	InternalThread_t4118012807::get_offset_of_threadpool_thread_19(),
	InternalThread_t4118012807::get_offset_of_thread_interrupt_requested_20(),
	InternalThread_t4118012807::get_offset_of_stack_size_21(),
	InternalThread_t4118012807::get_offset_of_apartment_state_22(),
	InternalThread_t4118012807::get_offset_of_critical_region_level_23(),
	InternalThread_t4118012807::get_offset_of_managed_id_24(),
	InternalThread_t4118012807::get_offset_of_small_id_25(),
	InternalThread_t4118012807::get_offset_of_manage_callback_26(),
	InternalThread_t4118012807::get_offset_of_interrupt_on_stop_27(),
	InternalThread_t4118012807::get_offset_of_flags_28(),
	InternalThread_t4118012807::get_offset_of_thread_pinning_ref_29(),
	InternalThread_t4118012807::get_offset_of_abort_protected_block_count_30(),
	InternalThread_t4118012807::get_offset_of_unused1_31(),
	InternalThread_t4118012807::get_offset_of_unused2_32(),
	InternalThread_t4118012807::get_offset_of_last_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (Timer_t791717973), -1, sizeof(Timer_t791717973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1411[7] = 
{
	Timer_t791717973_StaticFields::get_offset_of_scheduler_1(),
	Timer_t791717973::get_offset_of_callback_2(),
	Timer_t791717973::get_offset_of_state_3(),
	Timer_t791717973::get_offset_of_due_time_ms_4(),
	Timer_t791717973::get_offset_of_period_ms_5(),
	Timer_t791717973::get_offset_of_next_run_6(),
	Timer_t791717973::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (TimerComparer_t876299723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (Scheduler_t697594), -1, sizeof(Scheduler_t697594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[4] = 
{
	Scheduler_t697594_StaticFields::get_offset_of_instance_0(),
	Scheduler_t697594::get_offset_of_list_1(),
	Scheduler_t697594::get_offset_of_changed_2(),
	Scheduler_t697594_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (TimerCallback_t1684927372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (Volatile_t1178602802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (ActivationContext_t1572332809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[1] = 
{
	ActivationContext_t1572332809::get_offset_of__disposed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (AppDomainInitializer_t3898244613), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (AppDomainManager_t54965696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (AppDomainSetup_t611332832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1419[23] = 
{
	AppDomainSetup_t611332832::get_offset_of_application_base_0(),
	AppDomainSetup_t611332832::get_offset_of_application_name_1(),
	AppDomainSetup_t611332832::get_offset_of_cache_path_2(),
	AppDomainSetup_t611332832::get_offset_of_configuration_file_3(),
	AppDomainSetup_t611332832::get_offset_of_dynamic_base_4(),
	AppDomainSetup_t611332832::get_offset_of_license_file_5(),
	AppDomainSetup_t611332832::get_offset_of_private_bin_path_6(),
	AppDomainSetup_t611332832::get_offset_of_private_bin_path_probe_7(),
	AppDomainSetup_t611332832::get_offset_of_shadow_copy_directories_8(),
	AppDomainSetup_t611332832::get_offset_of_shadow_copy_files_9(),
	AppDomainSetup_t611332832::get_offset_of_publisher_policy_10(),
	AppDomainSetup_t611332832::get_offset_of_path_changed_11(),
	AppDomainSetup_t611332832::get_offset_of_loader_optimization_12(),
	AppDomainSetup_t611332832::get_offset_of_disallow_binding_redirects_13(),
	AppDomainSetup_t611332832::get_offset_of_disallow_code_downloads_14(),
	AppDomainSetup_t611332832::get_offset_of__activationArguments_15(),
	AppDomainSetup_t611332832::get_offset_of_domain_initializer_16(),
	AppDomainSetup_t611332832::get_offset_of_application_trust_17(),
	AppDomainSetup_t611332832::get_offset_of_domain_initializer_args_18(),
	AppDomainSetup_t611332832::get_offset_of_disallow_appbase_probe_19(),
	AppDomainSetup_t611332832::get_offset_of_configuration_bytes_20(),
	AppDomainSetup_t611332832::get_offset_of_serialized_non_primitives_21(),
	AppDomainSetup_t611332832::get_offset_of_U3CTargetFrameworkNameU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (ApplicationIdentity_t3292367950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1420[1] = 
{
	ApplicationIdentity_t3292367950::get_offset_of__fullName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (ArgIterator_t2628088752)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1421[4] = 
{
	ArgIterator_t2628088752::get_offset_of_sig_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArgIterator_t2628088752::get_offset_of_args_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArgIterator_t2628088752::get_offset_of_next_arg_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArgIterator_t2628088752::get_offset_of_num_args_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (Il2CppArray), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1423[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (QSortStack_t3667817390)+ sizeof (Il2CppObject), sizeof(QSortStack_t3667817390 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1424[2] = 
{
	QSortStack_t3667817390::get_offset_of_high_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QSortStack_t3667817390::get_offset_of_low_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (SimpleEnumerator_t4019359169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[3] = 
{
	SimpleEnumerator_t4019359169::get_offset_of_enumeratee_0(),
	SimpleEnumerator_t4019359169::get_offset_of_currentpos_1(),
	SimpleEnumerator_t4019359169::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1426[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (AssemblyLoadEventArgs_t4233815743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (AssemblyLoadEventHandler_t2169307382), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (CStreamReader_t2584617632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1429[1] = 
{
	CStreamReader_t2584617632::get_offset_of_driver_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (CStreamWriter_t2592797654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1430[1] = 
{
	CStreamWriter_t2592797654::get_offset_of_driver_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (Console_t2311202731), -1, sizeof(Console_t2311202731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1431[7] = 
{
	Console_t2311202731_StaticFields::get_offset_of_stdout_0(),
	Console_t2311202731_StaticFields::get_offset_of_stderr_1(),
	Console_t2311202731_StaticFields::get_offset_of_stdin_2(),
	Console_t2311202731_StaticFields::get_offset_of_inputEncoding_3(),
	Console_t2311202731_StaticFields::get_offset_of_outputEncoding_4(),
	Console_t2311202731_StaticFields::get_offset_of_cancel_event_5(),
	Console_t2311202731_StaticFields::get_offset_of_cancel_handler_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (WindowsConsole_t1329628201), -1, sizeof(WindowsConsole_t1329628201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1432[2] = 
{
	WindowsConsole_t1329628201_StaticFields::get_offset_of_ctrlHandlerAdded_0(),
	WindowsConsole_t1329628201_StaticFields::get_offset_of_cancelHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (WindowsCancelHandler_t1197082027), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (InternalCancelHandler_t2895223116), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (ConsoleDriver_t2410871357), -1, sizeof(ConsoleDriver_t2410871357_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1435[3] = 
{
	ConsoleDriver_t2410871357_StaticFields::get_offset_of_driver_0(),
	ConsoleDriver_t2410871357_StaticFields::get_offset_of_is_console_1(),
	ConsoleDriver_t2410871357_StaticFields::get_offset_of_called_isatty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (DelegateData_t1572802995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1436[3] = 
{
	DelegateData_t1572802995::get_offset_of_target_type_0(),
	DelegateData_t1572802995::get_offset_of_method_name_1(),
	DelegateData_t1572802995::get_offset_of_curried_first_arg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (Delegate_t3022476291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1437[11] = 
{
	Delegate_t3022476291::get_offset_of_method_ptr_0(),
	Delegate_t3022476291::get_offset_of_invoke_impl_1(),
	Delegate_t3022476291::get_offset_of_m_target_2(),
	Delegate_t3022476291::get_offset_of_method_3(),
	Delegate_t3022476291::get_offset_of_delegate_trampoline_4(),
	Delegate_t3022476291::get_offset_of_extra_arg_5(),
	Delegate_t3022476291::get_offset_of_method_code_6(),
	Delegate_t3022476291::get_offset_of_method_info_7(),
	Delegate_t3022476291::get_offset_of_original_method_info_8(),
	Delegate_t3022476291::get_offset_of_data_9(),
	Delegate_t3022476291::get_offset_of_method_is_virtual_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (DelegateSerializationHolder_t753405077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1438[1] = 
{
	DelegateSerializationHolder_t753405077::get_offset_of__delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (DelegateEntry_t3215410094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1439[7] = 
{
	DelegateEntry_t3215410094::get_offset_of_type_0(),
	DelegateEntry_t3215410094::get_offset_of_assembly_1(),
	DelegateEntry_t3215410094::get_offset_of_target_2(),
	DelegateEntry_t3215410094::get_offset_of_targetTypeAssembly_3(),
	DelegateEntry_t3215410094::get_offset_of_targetTypeName_4(),
	DelegateEntry_t3215410094::get_offset_of_methodName_5(),
	DelegateEntry_t3215410094::get_offset_of_delegateEntry_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1440[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (IntPtr_t)+ sizeof (Il2CppObject), sizeof(intptr_t), sizeof(IntPtr_t_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1442[2] = 
{
	IntPtr_t::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntPtr_t_StaticFields::get_offset_of_Zero_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (KnownTerminals_t368610438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (MarshalByRefObject_t1285298191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1444[1] = 
{
	MarshalByRefObject_t1285298191::get_offset_of__identity_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (MonoAsyncCall_t3796687503), sizeof(MonoAsyncCall_t3796687503_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1445[6] = 
{
	MonoAsyncCall_t3796687503::get_offset_of_msg_0(),
	MonoAsyncCall_t3796687503::get_offset_of_cb_method_1(),
	MonoAsyncCall_t3796687503::get_offset_of_cb_target_2(),
	MonoAsyncCall_t3796687503::get_offset_of_state_3(),
	MonoAsyncCall_t3796687503::get_offset_of_res_4(),
	MonoAsyncCall_t3796687503::get_offset_of_out_args_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (MonoCustomAttrs_t2976585698), -1, sizeof(MonoCustomAttrs_t2976585698_StaticFields), sizeof(MonoCustomAttrs_t2976585698_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1446[3] = 
{
	MonoCustomAttrs_t2976585698_StaticFields::get_offset_of_corlib_0(),
	THREAD_STATIC_FIELD_OFFSET,
	MonoCustomAttrs_t2976585698_StaticFields::get_offset_of_DefaultAttributeUsage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (AttributeInfo_t2366110328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1447[2] = 
{
	AttributeInfo_t2366110328::get_offset_of__usage_0(),
	AttributeInfo_t2366110328::get_offset_of__inheritanceLevel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (MonoType_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (MulticastDelegate_t3201952435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[1] = 
{
	MulticastDelegate_t3201952435::get_offset_of_delegates_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (NullConsoleDriver_t2151813504), -1, sizeof(NullConsoleDriver_t2151813504_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1450[1] = 
{
	NullConsoleDriver_t2151813504_StaticFields::get_offset_of_EmptyConsoleKeyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (Nullable_t1429764613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1452[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (NumberFormatter_t2933946347), -1, sizeof(NumberFormatter_t2933946347_StaticFields), sizeof(NumberFormatter_t2933946347_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1453[26] = 
{
	NumberFormatter_t2933946347_StaticFields::get_offset_of_MantissaBitsTable_0(),
	NumberFormatter_t2933946347_StaticFields::get_offset_of_TensExponentTable_1(),
	NumberFormatter_t2933946347_StaticFields::get_offset_of_DigitLowerTable_2(),
	NumberFormatter_t2933946347_StaticFields::get_offset_of_DigitUpperTable_3(),
	NumberFormatter_t2933946347_StaticFields::get_offset_of_TenPowersList_4(),
	NumberFormatter_t2933946347_StaticFields::get_offset_of_DecHexDigits_5(),
	NumberFormatter_t2933946347::get_offset_of__nfi_6(),
	NumberFormatter_t2933946347::get_offset_of__cbuf_7(),
	NumberFormatter_t2933946347::get_offset_of__NaN_8(),
	NumberFormatter_t2933946347::get_offset_of__infinity_9(),
	NumberFormatter_t2933946347::get_offset_of__isCustomFormat_10(),
	NumberFormatter_t2933946347::get_offset_of__specifierIsUpper_11(),
	NumberFormatter_t2933946347::get_offset_of__positive_12(),
	NumberFormatter_t2933946347::get_offset_of__specifier_13(),
	NumberFormatter_t2933946347::get_offset_of__precision_14(),
	NumberFormatter_t2933946347::get_offset_of__defPrecision_15(),
	NumberFormatter_t2933946347::get_offset_of__digitsLen_16(),
	NumberFormatter_t2933946347::get_offset_of__offset_17(),
	NumberFormatter_t2933946347::get_offset_of__decPointPos_18(),
	NumberFormatter_t2933946347::get_offset_of__val1_19(),
	NumberFormatter_t2933946347::get_offset_of__val2_20(),
	NumberFormatter_t2933946347::get_offset_of__val3_21(),
	NumberFormatter_t2933946347::get_offset_of__val4_22(),
	NumberFormatter_t2933946347::get_offset_of__ind_23(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (CustomInfo_t128587492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1454[14] = 
{
	CustomInfo_t128587492::get_offset_of_UseGroup_0(),
	CustomInfo_t128587492::get_offset_of_DecimalDigits_1(),
	CustomInfo_t128587492::get_offset_of_DecimalPointPos_2(),
	CustomInfo_t128587492::get_offset_of_DecimalTailSharpDigits_3(),
	CustomInfo_t128587492::get_offset_of_IntegerDigits_4(),
	CustomInfo_t128587492::get_offset_of_IntegerHeadSharpDigits_5(),
	CustomInfo_t128587492::get_offset_of_IntegerHeadPos_6(),
	CustomInfo_t128587492::get_offset_of_UseExponent_7(),
	CustomInfo_t128587492::get_offset_of_ExponentDigits_8(),
	CustomInfo_t128587492::get_offset_of_ExponentTailSharpDigits_9(),
	CustomInfo_t128587492::get_offset_of_ExponentNegativeSignOnly_10(),
	CustomInfo_t128587492::get_offset_of_DividePlaces_11(),
	CustomInfo_t128587492::get_offset_of_Percents_12(),
	CustomInfo_t128587492::get_offset_of_Permilles_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (Il2CppObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (OperatingSystem_t290860502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1456[3] = 
{
	OperatingSystem_t290860502::get_offset_of__platform_0(),
	OperatingSystem_t290860502::get_offset_of__version_1(),
	OperatingSystem_t290860502::get_offset_of__servicePack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (PlatformID_t1006634368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1457[8] = 
{
	PlatformID_t1006634368::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (ResolveEventArgs_t1859808873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1458[2] = 
{
	ResolveEventArgs_t1859808873::get_offset_of_m_Name_1(),
	ResolveEventArgs_t1859808873::get_offset_of_m_Requesting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (ResolveEventHandler_t3842432458), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (RuntimeArgumentHandle_t3259266975)+ sizeof (Il2CppObject), sizeof(RuntimeArgumentHandle_t3259266975 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1460[1] = 
{
	RuntimeArgumentHandle_t3259266975::get_offset_of_args_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (RuntimeFieldHandle_t2331729674)+ sizeof (Il2CppObject), sizeof(RuntimeFieldHandle_t2331729674 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1461[1] = 
{
	RuntimeFieldHandle_t2331729674::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (RuntimeMethodHandle_t894824333)+ sizeof (Il2CppObject), sizeof(RuntimeMethodHandle_t894824333 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1462[1] = 
{
	RuntimeMethodHandle_t894824333::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (RuntimeTypeHandle_t2330101084)+ sizeof (Il2CppObject), sizeof(RuntimeTypeHandle_t2330101084 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1463[1] = 
{
	RuntimeTypeHandle_t2330101084::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (StringComparison_t2376310518)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1464[7] = 
{
	StringComparison_t2376310518::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (TermInfoDriver_t3839442152), -1, sizeof(TermInfoDriver_t3839442152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1465[44] = 
{
	TermInfoDriver_t3839442152_StaticFields::get_offset_of_native_terminal_size_0(),
	TermInfoDriver_t3839442152_StaticFields::get_offset_of_terminal_size_1(),
	TermInfoDriver_t3839442152_StaticFields::get_offset_of_locations_2(),
	TermInfoDriver_t3839442152::get_offset_of_reader_3(),
	TermInfoDriver_t3839442152::get_offset_of_cursorLeft_4(),
	TermInfoDriver_t3839442152::get_offset_of_cursorTop_5(),
	TermInfoDriver_t3839442152::get_offset_of_title_6(),
	TermInfoDriver_t3839442152::get_offset_of_titleFormat_7(),
	TermInfoDriver_t3839442152::get_offset_of_cursorVisible_8(),
	TermInfoDriver_t3839442152::get_offset_of_csrVisible_9(),
	TermInfoDriver_t3839442152::get_offset_of_csrInvisible_10(),
	TermInfoDriver_t3839442152::get_offset_of_clear_11(),
	TermInfoDriver_t3839442152::get_offset_of_bell_12(),
	TermInfoDriver_t3839442152::get_offset_of_term_13(),
	TermInfoDriver_t3839442152::get_offset_of_stdin_14(),
	TermInfoDriver_t3839442152::get_offset_of_stdout_15(),
	TermInfoDriver_t3839442152::get_offset_of_windowWidth_16(),
	TermInfoDriver_t3839442152::get_offset_of_windowHeight_17(),
	TermInfoDriver_t3839442152::get_offset_of_bufferHeight_18(),
	TermInfoDriver_t3839442152::get_offset_of_bufferWidth_19(),
	TermInfoDriver_t3839442152::get_offset_of_buffer_20(),
	TermInfoDriver_t3839442152::get_offset_of_readpos_21(),
	TermInfoDriver_t3839442152::get_offset_of_writepos_22(),
	TermInfoDriver_t3839442152::get_offset_of_keypadXmit_23(),
	TermInfoDriver_t3839442152::get_offset_of_keypadLocal_24(),
	TermInfoDriver_t3839442152::get_offset_of_inited_25(),
	TermInfoDriver_t3839442152::get_offset_of_initLock_26(),
	TermInfoDriver_t3839442152::get_offset_of_initKeys_27(),
	TermInfoDriver_t3839442152::get_offset_of_origPair_28(),
	TermInfoDriver_t3839442152::get_offset_of_origColors_29(),
	TermInfoDriver_t3839442152::get_offset_of_cursorAddress_30(),
	TermInfoDriver_t3839442152::get_offset_of_fgcolor_31(),
	TermInfoDriver_t3839442152::get_offset_of_setfgcolor_32(),
	TermInfoDriver_t3839442152::get_offset_of_setbgcolor_33(),
	TermInfoDriver_t3839442152::get_offset_of_maxColors_34(),
	TermInfoDriver_t3839442152::get_offset_of_noGetPosition_35(),
	TermInfoDriver_t3839442152::get_offset_of_keymap_36(),
	TermInfoDriver_t3839442152::get_offset_of_rootmap_37(),
	TermInfoDriver_t3839442152::get_offset_of_rl_startx_38(),
	TermInfoDriver_t3839442152::get_offset_of_rl_starty_39(),
	TermInfoDriver_t3839442152::get_offset_of_control_characters_40(),
	TermInfoDriver_t3839442152_StaticFields::get_offset_of__consoleColorToAnsiCode_41(),
	TermInfoDriver_t3839442152::get_offset_of_echobuf_42(),
	TermInfoDriver_t3839442152::get_offset_of_echon_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (ParameterizedStrings_t2474305833), -1, 0, sizeof(ParameterizedStrings_t2474305833_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1466[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (FormatParam_t2947516451)+ sizeof (Il2CppObject), sizeof(FormatParam_t2947516451_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1467[2] = 
{
	FormatParam_t2947516451::get_offset_of__int32_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FormatParam_t2947516451::get_offset_of__string_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (LowLevelStack_t375164147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[2] = 
{
	LowLevelStack_t375164147::get_offset_of__arr_0(),
	LowLevelStack_t375164147::get_offset_of__count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (ByteMatcher_t2890532040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1469[2] = 
{
	ByteMatcher_t2890532040::get_offset_of_map_0(),
	ByteMatcher_t2890532040::get_offset_of_starts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (TermInfoNumbers_t1380902466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1470[35] = 
{
	TermInfoNumbers_t1380902466::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (TermInfoReader_t3840788697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1471[5] = 
{
	TermInfoReader_t3840788697::get_offset_of_boolSize_0(),
	TermInfoReader_t3840788697::get_offset_of_numSize_1(),
	TermInfoReader_t3840788697::get_offset_of_strOffsets_2(),
	TermInfoReader_t3840788697::get_offset_of_buffer_3(),
	TermInfoReader_t3840788697::get_offset_of_booleansOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (TermInfoStrings_t1425267120)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1472[396] = 
{
	TermInfoStrings_t1425267120::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (TimeZoneInfoOptions_t106353153)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1473[3] = 
{
	TimeZoneInfoOptions_t106353153::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (TimeZoneInfo_t436210607), -1, sizeof(TimeZoneInfo_t436210607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1474[14] = 
{
	TimeZoneInfo_t436210607::get_offset_of_m_id_0(),
	TimeZoneInfo_t436210607::get_offset_of_m_displayName_1(),
	TimeZoneInfo_t436210607::get_offset_of_m_standardDisplayName_2(),
	TimeZoneInfo_t436210607::get_offset_of_m_daylightDisplayName_3(),
	TimeZoneInfo_t436210607::get_offset_of_m_baseUtcOffset_4(),
	TimeZoneInfo_t436210607::get_offset_of_m_supportsDaylightSavingTime_5(),
	TimeZoneInfo_t436210607::get_offset_of_m_adjustmentRules_6(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_s_cachedData_7(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_s_maxDateOnly_8(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_s_minDateOnly_9(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_12(),
	TimeZoneInfo_t436210607_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (TimeZoneInfoResult_t2374900455)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1475[5] = 
{
	TimeZoneInfoResult_t2374900455::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (CachedData_t3197336479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1476[5] = 
{
	CachedData_t3197336479::get_offset_of_m_localTimeZone_0(),
	CachedData_t3197336479::get_offset_of_m_utcTimeZone_1(),
	CachedData_t3197336479::get_offset_of_m_systemTimeZones_2(),
	CachedData_t3197336479::get_offset_of_m_allSystemTimeZonesRead_3(),
	CachedData_t3197336479::get_offset_of_m_oneYearLocalFromUtc_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (OffsetAndRule_t2067953655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1477[3] = 
{
	OffsetAndRule_t2067953655::get_offset_of_year_0(),
	OffsetAndRule_t2067953655::get_offset_of_offset_1(),
	OffsetAndRule_t2067953655::get_offset_of_rule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (AdjustmentRule_t2179708818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1478[7] = 
{
	AdjustmentRule_t2179708818::get_offset_of_m_dateStart_0(),
	AdjustmentRule_t2179708818::get_offset_of_m_dateEnd_1(),
	AdjustmentRule_t2179708818::get_offset_of_m_daylightDelta_2(),
	AdjustmentRule_t2179708818::get_offset_of_m_daylightTransitionStart_3(),
	AdjustmentRule_t2179708818::get_offset_of_m_daylightTransitionEnd_4(),
	AdjustmentRule_t2179708818::get_offset_of_m_baseUtcOffsetDelta_5(),
	AdjustmentRule_t2179708818::get_offset_of_m_noDaylightTransitions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (TransitionTime_t3441274853)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1479[6] = 
{
	TransitionTime_t3441274853::get_offset_of_m_timeOfDay_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransitionTime_t3441274853::get_offset_of_m_month_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransitionTime_t3441274853::get_offset_of_m_week_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransitionTime_t3441274853::get_offset_of_m_day_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransitionTime_t3441274853::get_offset_of_m_dayOfWeek_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TransitionTime_t3441274853::get_offset_of_m_isFixedDateRule_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (TZifType_t1855764066)+ sizeof (Il2CppObject), sizeof(TZifType_t1855764066_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1480[3] = 
{
	TZifType_t1855764066::get_offset_of_UtcOffset_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifType_t1855764066::get_offset_of_IsDst_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifType_t1855764066::get_offset_of_AbbreviationIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (TZifHead_t3630268560)+ sizeof (Il2CppObject), sizeof(TZifHead_t3630268560 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1481[8] = 
{
	TZifHead_t3630268560::get_offset_of_Magic_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_IsGmtCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_IsStdCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_LeapCount_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_TimeCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_TypeCount_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TZifHead_t3630268560::get_offset_of_CharCount_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (TZVersion_t2863501951)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1482[4] = 
{
	TZVersion_t2863501951::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (TypeCode_t2536926201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1483[19] = 
{
	TypeCode_t2536926201::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (TypeNames_t3900030990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (ATypeName_t2751073424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (TypeIdentifiers_t1234132058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (Display_t4153290110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1489[2] = 
{
	Display_t4153290110::get_offset_of_displayName_0(),
	Display_t4153290110::get_offset_of_internal_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (NoEscape_t69741980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1490[1] = 
{
	NoEscape_t69741980::get_offset_of_simpleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (ArraySpec_t2770119450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1492[2] = 
{
	ArraySpec_t2770119450::get_offset_of_dimensions_0(),
	ArraySpec_t2770119450::get_offset_of_bound_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (PointerSpec_t2712181132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1493[1] = 
{
	PointerSpec_t2712181132::get_offset_of_pointer_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (TypeSpec_t2066641911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1494[7] = 
{
	TypeSpec_t2066641911::get_offset_of_name_0(),
	TypeSpec_t2066641911::get_offset_of_assembly_name_1(),
	TypeSpec_t2066641911::get_offset_of_nested_2(),
	TypeSpec_t2066641911::get_offset_of_generic_params_3(),
	TypeSpec_t2066641911::get_offset_of_modifier_spec_4(),
	TypeSpec_t2066641911::get_offset_of_is_byref_5(),
	TypeSpec_t2066641911::get_offset_of_display_fullname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (DisplayNameFormat_t3375299945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1495[4] = 
{
	DisplayNameFormat_t3375299945::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (UIntPtr_t)+ sizeof (Il2CppObject), sizeof(uintptr_t), sizeof(UIntPtr_t_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1496[2] = 
{
	UIntPtr_t_StaticFields::get_offset_of_Zero_0(),
	UIntPtr_t::get_offset_of__pointer_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (ValueType_t3507792607), sizeof(ValueType_t3507792607_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (Void_t1841601450)+ sizeof (Il2CppObject), 1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (WeakReference_t1077405567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1499[2] = 
{
	WeakReference_t1077405567::get_offset_of_isLongReference_0(),
	WeakReference_t1077405567::get_offset_of_gcHandle_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
