﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Uri
struct Uri_t19570940;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxyData
struct  WebProxyData_t1226725784  : public Il2CppObject
{
public:
	// System.Boolean System.Net.WebProxyData::bypassOnLocal
	bool ___bypassOnLocal_0;
	// System.Boolean System.Net.WebProxyData::automaticallyDetectSettings
	bool ___automaticallyDetectSettings_1;
	// System.Uri System.Net.WebProxyData::proxyAddress
	Uri_t19570940 * ___proxyAddress_2;
	// System.Collections.Hashtable System.Net.WebProxyData::proxyHostAddresses
	Hashtable_t909839986 * ___proxyHostAddresses_3;
	// System.Uri System.Net.WebProxyData::scriptLocation
	Uri_t19570940 * ___scriptLocation_4;
	// System.Collections.ArrayList System.Net.WebProxyData::bypassList
	ArrayList_t4252133567 * ___bypassList_5;

public:
	inline static int32_t get_offset_of_bypassOnLocal_0() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___bypassOnLocal_0)); }
	inline bool get_bypassOnLocal_0() const { return ___bypassOnLocal_0; }
	inline bool* get_address_of_bypassOnLocal_0() { return &___bypassOnLocal_0; }
	inline void set_bypassOnLocal_0(bool value)
	{
		___bypassOnLocal_0 = value;
	}

	inline static int32_t get_offset_of_automaticallyDetectSettings_1() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___automaticallyDetectSettings_1)); }
	inline bool get_automaticallyDetectSettings_1() const { return ___automaticallyDetectSettings_1; }
	inline bool* get_address_of_automaticallyDetectSettings_1() { return &___automaticallyDetectSettings_1; }
	inline void set_automaticallyDetectSettings_1(bool value)
	{
		___automaticallyDetectSettings_1 = value;
	}

	inline static int32_t get_offset_of_proxyAddress_2() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___proxyAddress_2)); }
	inline Uri_t19570940 * get_proxyAddress_2() const { return ___proxyAddress_2; }
	inline Uri_t19570940 ** get_address_of_proxyAddress_2() { return &___proxyAddress_2; }
	inline void set_proxyAddress_2(Uri_t19570940 * value)
	{
		___proxyAddress_2 = value;
		Il2CppCodeGenWriteBarrier(&___proxyAddress_2, value);
	}

	inline static int32_t get_offset_of_proxyHostAddresses_3() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___proxyHostAddresses_3)); }
	inline Hashtable_t909839986 * get_proxyHostAddresses_3() const { return ___proxyHostAddresses_3; }
	inline Hashtable_t909839986 ** get_address_of_proxyHostAddresses_3() { return &___proxyHostAddresses_3; }
	inline void set_proxyHostAddresses_3(Hashtable_t909839986 * value)
	{
		___proxyHostAddresses_3 = value;
		Il2CppCodeGenWriteBarrier(&___proxyHostAddresses_3, value);
	}

	inline static int32_t get_offset_of_scriptLocation_4() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___scriptLocation_4)); }
	inline Uri_t19570940 * get_scriptLocation_4() const { return ___scriptLocation_4; }
	inline Uri_t19570940 ** get_address_of_scriptLocation_4() { return &___scriptLocation_4; }
	inline void set_scriptLocation_4(Uri_t19570940 * value)
	{
		___scriptLocation_4 = value;
		Il2CppCodeGenWriteBarrier(&___scriptLocation_4, value);
	}

	inline static int32_t get_offset_of_bypassList_5() { return static_cast<int32_t>(offsetof(WebProxyData_t1226725784, ___bypassList_5)); }
	inline ArrayList_t4252133567 * get_bypassList_5() const { return ___bypassList_5; }
	inline ArrayList_t4252133567 ** get_address_of_bypassList_5() { return &___bypassList_5; }
	inline void set_bypassList_5(ArrayList_t4252133567 * value)
	{
		___bypassList_5 = value;
		Il2CppCodeGenWriteBarrier(&___bypassList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
