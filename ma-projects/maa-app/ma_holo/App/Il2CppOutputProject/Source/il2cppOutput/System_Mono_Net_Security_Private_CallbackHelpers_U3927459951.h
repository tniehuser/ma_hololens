﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t1929047274;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<MonoToPublic>c__AnonStorey5
struct  U3CMonoToPublicU3Ec__AnonStorey5_t927459951  : public Il2CppObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Net.Security.Private.CallbackHelpers/<MonoToPublic>c__AnonStorey5::callback
	MonoRemoteCertificateValidationCallback_t1929047274 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CMonoToPublicU3Ec__AnonStorey5_t927459951, ___callback_0)); }
	inline MonoRemoteCertificateValidationCallback_t1929047274 * get_callback_0() const { return ___callback_0; }
	inline MonoRemoteCertificateValidationCallback_t1929047274 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoRemoteCertificateValidationCallback_t1929047274 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
