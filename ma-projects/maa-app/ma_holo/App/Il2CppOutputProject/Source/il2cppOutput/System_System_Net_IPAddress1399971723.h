﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"

// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.String
struct String_t;
// System.UInt16[]
struct UInt16U5BU5D_t2527266722;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t1399971723  : public Il2CppObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_4;
	// System.String System.Net.IPAddress::m_ToString
	String_t* ___m_ToString_5;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_9;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t2527266722* ___m_Numbers_10;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_11;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_12;

public:
	inline static int32_t get_offset_of_m_Address_4() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Address_4)); }
	inline int64_t get_m_Address_4() const { return ___m_Address_4; }
	inline int64_t* get_address_of_m_Address_4() { return &___m_Address_4; }
	inline void set_m_Address_4(int64_t value)
	{
		___m_Address_4 = value;
	}

	inline static int32_t get_offset_of_m_ToString_5() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_ToString_5)); }
	inline String_t* get_m_ToString_5() const { return ___m_ToString_5; }
	inline String_t** get_address_of_m_ToString_5() { return &___m_ToString_5; }
	inline void set_m_ToString_5(String_t* value)
	{
		___m_ToString_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_ToString_5, value);
	}

	inline static int32_t get_offset_of_m_Family_9() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Family_9)); }
	inline int32_t get_m_Family_9() const { return ___m_Family_9; }
	inline int32_t* get_address_of_m_Family_9() { return &___m_Family_9; }
	inline void set_m_Family_9(int32_t value)
	{
		___m_Family_9 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_10() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Numbers_10)); }
	inline UInt16U5BU5D_t2527266722* get_m_Numbers_10() const { return ___m_Numbers_10; }
	inline UInt16U5BU5D_t2527266722** get_address_of_m_Numbers_10() { return &___m_Numbers_10; }
	inline void set_m_Numbers_10(UInt16U5BU5D_t2527266722* value)
	{
		___m_Numbers_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Numbers_10, value);
	}

	inline static int32_t get_offset_of_m_ScopeId_11() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_ScopeId_11)); }
	inline int64_t get_m_ScopeId_11() const { return ___m_ScopeId_11; }
	inline int64_t* get_address_of_m_ScopeId_11() { return &___m_ScopeId_11; }
	inline void set_m_ScopeId_11(int64_t value)
	{
		___m_ScopeId_11 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_12() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_HashCode_12)); }
	inline int32_t get_m_HashCode_12() const { return ___m_HashCode_12; }
	inline int32_t* get_address_of_m_HashCode_12() { return &___m_HashCode_12; }
	inline void set_m_HashCode_12(int32_t value)
	{
		___m_HashCode_12 = value;
	}
};

struct IPAddress_t1399971723_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t1399971723 * ___Any_0;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t1399971723 * ___Loopback_1;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t1399971723 * ___Broadcast_2;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t1399971723 * ___None_3;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t1399971723 * ___IPv6Any_6;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t1399971723 * ___IPv6Loopback_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t1399971723 * ___IPv6None_8;

public:
	inline static int32_t get_offset_of_Any_0() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Any_0)); }
	inline IPAddress_t1399971723 * get_Any_0() const { return ___Any_0; }
	inline IPAddress_t1399971723 ** get_address_of_Any_0() { return &___Any_0; }
	inline void set_Any_0(IPAddress_t1399971723 * value)
	{
		___Any_0 = value;
		Il2CppCodeGenWriteBarrier(&___Any_0, value);
	}

	inline static int32_t get_offset_of_Loopback_1() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Loopback_1)); }
	inline IPAddress_t1399971723 * get_Loopback_1() const { return ___Loopback_1; }
	inline IPAddress_t1399971723 ** get_address_of_Loopback_1() { return &___Loopback_1; }
	inline void set_Loopback_1(IPAddress_t1399971723 * value)
	{
		___Loopback_1 = value;
		Il2CppCodeGenWriteBarrier(&___Loopback_1, value);
	}

	inline static int32_t get_offset_of_Broadcast_2() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Broadcast_2)); }
	inline IPAddress_t1399971723 * get_Broadcast_2() const { return ___Broadcast_2; }
	inline IPAddress_t1399971723 ** get_address_of_Broadcast_2() { return &___Broadcast_2; }
	inline void set_Broadcast_2(IPAddress_t1399971723 * value)
	{
		___Broadcast_2 = value;
		Il2CppCodeGenWriteBarrier(&___Broadcast_2, value);
	}

	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___None_3)); }
	inline IPAddress_t1399971723 * get_None_3() const { return ___None_3; }
	inline IPAddress_t1399971723 ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(IPAddress_t1399971723 * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier(&___None_3, value);
	}

	inline static int32_t get_offset_of_IPv6Any_6() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Any_6)); }
	inline IPAddress_t1399971723 * get_IPv6Any_6() const { return ___IPv6Any_6; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Any_6() { return &___IPv6Any_6; }
	inline void set_IPv6Any_6(IPAddress_t1399971723 * value)
	{
		___IPv6Any_6 = value;
		Il2CppCodeGenWriteBarrier(&___IPv6Any_6, value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_7() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Loopback_7)); }
	inline IPAddress_t1399971723 * get_IPv6Loopback_7() const { return ___IPv6Loopback_7; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Loopback_7() { return &___IPv6Loopback_7; }
	inline void set_IPv6Loopback_7(IPAddress_t1399971723 * value)
	{
		___IPv6Loopback_7 = value;
		Il2CppCodeGenWriteBarrier(&___IPv6Loopback_7, value);
	}

	inline static int32_t get_offset_of_IPv6None_8() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6None_8)); }
	inline IPAddress_t1399971723 * get_IPv6None_8() const { return ___IPv6None_8; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6None_8() { return &___IPv6None_8; }
	inline void set_IPv6None_8(IPAddress_t1399971723 * value)
	{
		___IPv6None_8 = value;
		Il2CppCodeGenWriteBarrier(&___IPv6None_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
