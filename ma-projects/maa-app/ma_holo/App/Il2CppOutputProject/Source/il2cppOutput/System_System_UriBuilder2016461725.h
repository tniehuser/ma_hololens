﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriBuilder
struct  UriBuilder_t2016461725  : public Il2CppObject
{
public:
	// System.Boolean System.UriBuilder::m_changed
	bool ___m_changed_0;
	// System.String System.UriBuilder::m_fragment
	String_t* ___m_fragment_1;
	// System.String System.UriBuilder::m_host
	String_t* ___m_host_2;
	// System.String System.UriBuilder::m_password
	String_t* ___m_password_3;
	// System.String System.UriBuilder::m_path
	String_t* ___m_path_4;
	// System.Int32 System.UriBuilder::m_port
	int32_t ___m_port_5;
	// System.String System.UriBuilder::m_query
	String_t* ___m_query_6;
	// System.String System.UriBuilder::m_scheme
	String_t* ___m_scheme_7;
	// System.String System.UriBuilder::m_schemeDelimiter
	String_t* ___m_schemeDelimiter_8;
	// System.Uri System.UriBuilder::m_uri
	Uri_t19570940 * ___m_uri_9;
	// System.String System.UriBuilder::m_username
	String_t* ___m_username_10;

public:
	inline static int32_t get_offset_of_m_changed_0() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_changed_0)); }
	inline bool get_m_changed_0() const { return ___m_changed_0; }
	inline bool* get_address_of_m_changed_0() { return &___m_changed_0; }
	inline void set_m_changed_0(bool value)
	{
		___m_changed_0 = value;
	}

	inline static int32_t get_offset_of_m_fragment_1() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_fragment_1)); }
	inline String_t* get_m_fragment_1() const { return ___m_fragment_1; }
	inline String_t** get_address_of_m_fragment_1() { return &___m_fragment_1; }
	inline void set_m_fragment_1(String_t* value)
	{
		___m_fragment_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_fragment_1, value);
	}

	inline static int32_t get_offset_of_m_host_2() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_host_2)); }
	inline String_t* get_m_host_2() const { return ___m_host_2; }
	inline String_t** get_address_of_m_host_2() { return &___m_host_2; }
	inline void set_m_host_2(String_t* value)
	{
		___m_host_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_host_2, value);
	}

	inline static int32_t get_offset_of_m_password_3() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_password_3)); }
	inline String_t* get_m_password_3() const { return ___m_password_3; }
	inline String_t** get_address_of_m_password_3() { return &___m_password_3; }
	inline void set_m_password_3(String_t* value)
	{
		___m_password_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_password_3, value);
	}

	inline static int32_t get_offset_of_m_path_4() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_path_4)); }
	inline String_t* get_m_path_4() const { return ___m_path_4; }
	inline String_t** get_address_of_m_path_4() { return &___m_path_4; }
	inline void set_m_path_4(String_t* value)
	{
		___m_path_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_path_4, value);
	}

	inline static int32_t get_offset_of_m_port_5() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_port_5)); }
	inline int32_t get_m_port_5() const { return ___m_port_5; }
	inline int32_t* get_address_of_m_port_5() { return &___m_port_5; }
	inline void set_m_port_5(int32_t value)
	{
		___m_port_5 = value;
	}

	inline static int32_t get_offset_of_m_query_6() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_query_6)); }
	inline String_t* get_m_query_6() const { return ___m_query_6; }
	inline String_t** get_address_of_m_query_6() { return &___m_query_6; }
	inline void set_m_query_6(String_t* value)
	{
		___m_query_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_query_6, value);
	}

	inline static int32_t get_offset_of_m_scheme_7() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_scheme_7)); }
	inline String_t* get_m_scheme_7() const { return ___m_scheme_7; }
	inline String_t** get_address_of_m_scheme_7() { return &___m_scheme_7; }
	inline void set_m_scheme_7(String_t* value)
	{
		___m_scheme_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_scheme_7, value);
	}

	inline static int32_t get_offset_of_m_schemeDelimiter_8() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_schemeDelimiter_8)); }
	inline String_t* get_m_schemeDelimiter_8() const { return ___m_schemeDelimiter_8; }
	inline String_t** get_address_of_m_schemeDelimiter_8() { return &___m_schemeDelimiter_8; }
	inline void set_m_schemeDelimiter_8(String_t* value)
	{
		___m_schemeDelimiter_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_schemeDelimiter_8, value);
	}

	inline static int32_t get_offset_of_m_uri_9() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_uri_9)); }
	inline Uri_t19570940 * get_m_uri_9() const { return ___m_uri_9; }
	inline Uri_t19570940 ** get_address_of_m_uri_9() { return &___m_uri_9; }
	inline void set_m_uri_9(Uri_t19570940 * value)
	{
		___m_uri_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_uri_9, value);
	}

	inline static int32_t get_offset_of_m_username_10() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_username_10)); }
	inline String_t* get_m_username_10() const { return ___m_username_10; }
	inline String_t** get_address_of_m_username_10() { return &___m_username_10; }
	inline void set_m_username_10(String_t* value)
	{
		___m_username_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_username_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
