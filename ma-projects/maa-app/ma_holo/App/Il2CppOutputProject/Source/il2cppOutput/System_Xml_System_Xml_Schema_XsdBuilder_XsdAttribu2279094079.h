﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaNames_Token1005517746.h"

// System.Xml.Schema.XsdBuilder/XsdBuildFunction
struct XsdBuildFunction_t2636231335;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdBuilder/XsdAttributeEntry
struct  XsdAttributeEntry_t2279094079  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XsdBuilder/XsdAttributeEntry::Attribute
	int32_t ___Attribute_0;
	// System.Xml.Schema.XsdBuilder/XsdBuildFunction System.Xml.Schema.XsdBuilder/XsdAttributeEntry::BuildFunc
	XsdBuildFunction_t2636231335 * ___BuildFunc_1;

public:
	inline static int32_t get_offset_of_Attribute_0() { return static_cast<int32_t>(offsetof(XsdAttributeEntry_t2279094079, ___Attribute_0)); }
	inline int32_t get_Attribute_0() const { return ___Attribute_0; }
	inline int32_t* get_address_of_Attribute_0() { return &___Attribute_0; }
	inline void set_Attribute_0(int32_t value)
	{
		___Attribute_0 = value;
	}

	inline static int32_t get_offset_of_BuildFunc_1() { return static_cast<int32_t>(offsetof(XsdAttributeEntry_t2279094079, ___BuildFunc_1)); }
	inline XsdBuildFunction_t2636231335 * get_BuildFunc_1() const { return ___BuildFunc_1; }
	inline XsdBuildFunction_t2636231335 ** get_address_of_BuildFunc_1() { return &___BuildFunc_1; }
	inline void set_BuildFunc_1(XsdBuildFunction_t2636231335 * value)
	{
		___BuildFunc_1 = value;
		Il2CppCodeGenWriteBarrier(&___BuildFunc_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
