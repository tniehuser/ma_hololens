﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.AtomicBoolean
struct AtomicBoolean_t379413895;
// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<System.Boolean>
struct U3CFromAsyncImplU3Ec__AnonStorey2_t768948844;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1<System.Boolean>
struct  U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311  : public Il2CppObject
{
public:
	// System.Threading.AtomicBoolean System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1::invoked
	AtomicBoolean_t379413895 * ___invoked_0;
	// System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey2<TResult> System.Threading.Tasks.TaskFactory`1/<FromAsyncImpl>c__AnonStorey1::<>f__ref$2
	U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_invoked_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311, ___invoked_0)); }
	inline AtomicBoolean_t379413895 * get_invoked_0() const { return ___invoked_0; }
	inline AtomicBoolean_t379413895 ** get_address_of_invoked_0() { return &___invoked_0; }
	inline void set_invoked_0(AtomicBoolean_t379413895 * value)
	{
		___invoked_0 = value;
		Il2CppCodeGenWriteBarrier(&___invoked_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CFromAsyncImplU3Ec__AnonStorey1_t2092671311, ___U3CU3Ef__refU242_1)); }
	inline U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CFromAsyncImplU3Ec__AnonStorey2_t768948844 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
