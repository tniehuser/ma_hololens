﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_BaseValidator3557140249.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"

// System.Xml.HWStack
struct HWStack_t738999989;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;
// System.Xml.Schema.Parser
struct Parser_t1940171737;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdValidator
struct  XsdValidator_t535151321  : public BaseValidator_t3557140249
{
public:
	// System.Int32 System.Xml.Schema.XsdValidator::startIDConstraint
	int32_t ___startIDConstraint_15;
	// System.Xml.HWStack System.Xml.Schema.XsdValidator::validationStack
	HWStack_t738999989 * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::attPresence
	Hashtable_t909839986 * ___attPresence_17;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XsdValidator::nsManager
	XmlNamespaceManager_t486731501 * ___nsManager_18;
	// System.Boolean System.Xml.Schema.XsdValidator::bManageNamespaces
	bool ___bManageNamespaces_19;
	// System.Collections.Hashtable System.Xml.Schema.XsdValidator::IDs
	Hashtable_t909839986 * ___IDs_20;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.XsdValidator::idRefListHead
	IdRefNode_t224554150 * ___idRefListHead_21;
	// System.Xml.Schema.Parser System.Xml.Schema.XsdValidator::inlineSchemaParser
	Parser_t1940171737 * ___inlineSchemaParser_22;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XsdValidator::processContents
	int32_t ___processContents_23;
	// System.String System.Xml.Schema.XsdValidator::NsXmlNs
	String_t* ___NsXmlNs_27;
	// System.String System.Xml.Schema.XsdValidator::NsXs
	String_t* ___NsXs_28;
	// System.String System.Xml.Schema.XsdValidator::NsXsi
	String_t* ___NsXsi_29;
	// System.String System.Xml.Schema.XsdValidator::XsiType
	String_t* ___XsiType_30;
	// System.String System.Xml.Schema.XsdValidator::XsiNil
	String_t* ___XsiNil_31;
	// System.String System.Xml.Schema.XsdValidator::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_32;
	// System.String System.Xml.Schema.XsdValidator::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_33;
	// System.String System.Xml.Schema.XsdValidator::XsdSchema
	String_t* ___XsdSchema_34;

public:
	inline static int32_t get_offset_of_startIDConstraint_15() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___startIDConstraint_15)); }
	inline int32_t get_startIDConstraint_15() const { return ___startIDConstraint_15; }
	inline int32_t* get_address_of_startIDConstraint_15() { return &___startIDConstraint_15; }
	inline void set_startIDConstraint_15(int32_t value)
	{
		___startIDConstraint_15 = value;
	}

	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___validationStack_16)); }
	inline HWStack_t738999989 * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t738999989 ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t738999989 * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier(&___validationStack_16, value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___attPresence_17)); }
	inline Hashtable_t909839986 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t909839986 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t909839986 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier(&___attPresence_17, value);
	}

	inline static int32_t get_offset_of_nsManager_18() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___nsManager_18)); }
	inline XmlNamespaceManager_t486731501 * get_nsManager_18() const { return ___nsManager_18; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsManager_18() { return &___nsManager_18; }
	inline void set_nsManager_18(XmlNamespaceManager_t486731501 * value)
	{
		___nsManager_18 = value;
		Il2CppCodeGenWriteBarrier(&___nsManager_18, value);
	}

	inline static int32_t get_offset_of_bManageNamespaces_19() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___bManageNamespaces_19)); }
	inline bool get_bManageNamespaces_19() const { return ___bManageNamespaces_19; }
	inline bool* get_address_of_bManageNamespaces_19() { return &___bManageNamespaces_19; }
	inline void set_bManageNamespaces_19(bool value)
	{
		___bManageNamespaces_19 = value;
	}

	inline static int32_t get_offset_of_IDs_20() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___IDs_20)); }
	inline Hashtable_t909839986 * get_IDs_20() const { return ___IDs_20; }
	inline Hashtable_t909839986 ** get_address_of_IDs_20() { return &___IDs_20; }
	inline void set_IDs_20(Hashtable_t909839986 * value)
	{
		___IDs_20 = value;
		Il2CppCodeGenWriteBarrier(&___IDs_20, value);
	}

	inline static int32_t get_offset_of_idRefListHead_21() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___idRefListHead_21)); }
	inline IdRefNode_t224554150 * get_idRefListHead_21() const { return ___idRefListHead_21; }
	inline IdRefNode_t224554150 ** get_address_of_idRefListHead_21() { return &___idRefListHead_21; }
	inline void set_idRefListHead_21(IdRefNode_t224554150 * value)
	{
		___idRefListHead_21 = value;
		Il2CppCodeGenWriteBarrier(&___idRefListHead_21, value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___inlineSchemaParser_22)); }
	inline Parser_t1940171737 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_t1940171737 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_t1940171737 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier(&___inlineSchemaParser_22, value);
	}

	inline static int32_t get_offset_of_processContents_23() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___processContents_23)); }
	inline int32_t get_processContents_23() const { return ___processContents_23; }
	inline int32_t* get_address_of_processContents_23() { return &___processContents_23; }
	inline void set_processContents_23(int32_t value)
	{
		___processContents_23 = value;
	}

	inline static int32_t get_offset_of_NsXmlNs_27() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXmlNs_27)); }
	inline String_t* get_NsXmlNs_27() const { return ___NsXmlNs_27; }
	inline String_t** get_address_of_NsXmlNs_27() { return &___NsXmlNs_27; }
	inline void set_NsXmlNs_27(String_t* value)
	{
		___NsXmlNs_27 = value;
		Il2CppCodeGenWriteBarrier(&___NsXmlNs_27, value);
	}

	inline static int32_t get_offset_of_NsXs_28() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXs_28)); }
	inline String_t* get_NsXs_28() const { return ___NsXs_28; }
	inline String_t** get_address_of_NsXs_28() { return &___NsXs_28; }
	inline void set_NsXs_28(String_t* value)
	{
		___NsXs_28 = value;
		Il2CppCodeGenWriteBarrier(&___NsXs_28, value);
	}

	inline static int32_t get_offset_of_NsXsi_29() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___NsXsi_29)); }
	inline String_t* get_NsXsi_29() const { return ___NsXsi_29; }
	inline String_t** get_address_of_NsXsi_29() { return &___NsXsi_29; }
	inline void set_NsXsi_29(String_t* value)
	{
		___NsXsi_29 = value;
		Il2CppCodeGenWriteBarrier(&___NsXsi_29, value);
	}

	inline static int32_t get_offset_of_XsiType_30() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiType_30)); }
	inline String_t* get_XsiType_30() const { return ___XsiType_30; }
	inline String_t** get_address_of_XsiType_30() { return &___XsiType_30; }
	inline void set_XsiType_30(String_t* value)
	{
		___XsiType_30 = value;
		Il2CppCodeGenWriteBarrier(&___XsiType_30, value);
	}

	inline static int32_t get_offset_of_XsiNil_31() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiNil_31)); }
	inline String_t* get_XsiNil_31() const { return ___XsiNil_31; }
	inline String_t** get_address_of_XsiNil_31() { return &___XsiNil_31; }
	inline void set_XsiNil_31(String_t* value)
	{
		___XsiNil_31 = value;
		Il2CppCodeGenWriteBarrier(&___XsiNil_31, value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_32() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiSchemaLocation_32)); }
	inline String_t* get_XsiSchemaLocation_32() const { return ___XsiSchemaLocation_32; }
	inline String_t** get_address_of_XsiSchemaLocation_32() { return &___XsiSchemaLocation_32; }
	inline void set_XsiSchemaLocation_32(String_t* value)
	{
		___XsiSchemaLocation_32 = value;
		Il2CppCodeGenWriteBarrier(&___XsiSchemaLocation_32, value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_33() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsiNoNamespaceSchemaLocation_33)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_33() const { return ___XsiNoNamespaceSchemaLocation_33; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_33() { return &___XsiNoNamespaceSchemaLocation_33; }
	inline void set_XsiNoNamespaceSchemaLocation_33(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_33 = value;
		Il2CppCodeGenWriteBarrier(&___XsiNoNamespaceSchemaLocation_33, value);
	}

	inline static int32_t get_offset_of_XsdSchema_34() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321, ___XsdSchema_34)); }
	inline String_t* get_XsdSchema_34() const { return ___XsdSchema_34; }
	inline String_t** get_address_of_XsdSchema_34() { return &___XsdSchema_34; }
	inline void set_XsdSchema_34(String_t* value)
	{
		___XsdSchema_34 = value;
		Il2CppCodeGenWriteBarrier(&___XsdSchema_34, value);
	}
};

struct XsdValidator_t535151321_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtCDATA
	XmlSchemaDatatype_t1195946242 * ___dtCDATA_24;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtQName
	XmlSchemaDatatype_t1195946242 * ___dtQName_25;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XsdValidator::dtStringArray
	XmlSchemaDatatype_t1195946242 * ___dtStringArray_26;

public:
	inline static int32_t get_offset_of_dtCDATA_24() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtCDATA_24)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtCDATA_24() const { return ___dtCDATA_24; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtCDATA_24() { return &___dtCDATA_24; }
	inline void set_dtCDATA_24(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtCDATA_24 = value;
		Il2CppCodeGenWriteBarrier(&___dtCDATA_24, value);
	}

	inline static int32_t get_offset_of_dtQName_25() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtQName_25)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtQName_25() const { return ___dtQName_25; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtQName_25() { return &___dtQName_25; }
	inline void set_dtQName_25(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtQName_25 = value;
		Il2CppCodeGenWriteBarrier(&___dtQName_25, value);
	}

	inline static int32_t get_offset_of_dtStringArray_26() { return static_cast<int32_t>(offsetof(XsdValidator_t535151321_StaticFields, ___dtStringArray_26)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtStringArray_26() const { return ___dtStringArray_26; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtStringArray_26() { return &___dtStringArray_26; }
	inline void set_dtStringArray_26(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtStringArray_26 = value;
		Il2CppCodeGenWriteBarrier(&___dtStringArray_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
