﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.String
struct String_t;
// Mono.Net.Security.Private.LegacySslStream
struct LegacySslStream_t83997747;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream/<BeginAuthenticateAsClient>c__AnonStorey0
struct  U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423  : public Il2CppObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.Private.LegacySslStream/<BeginAuthenticateAsClient>c__AnonStorey0::clientCertificates
	X509CertificateCollection_t1197680765 * ___clientCertificates_0;
	// System.String Mono.Net.Security.Private.LegacySslStream/<BeginAuthenticateAsClient>c__AnonStorey0::targetHost
	String_t* ___targetHost_1;
	// Mono.Net.Security.Private.LegacySslStream Mono.Net.Security.Private.LegacySslStream/<BeginAuthenticateAsClient>c__AnonStorey0::$this
	LegacySslStream_t83997747 * ___U24this_2;

public:
	inline static int32_t get_offset_of_clientCertificates_0() { return static_cast<int32_t>(offsetof(U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423, ___clientCertificates_0)); }
	inline X509CertificateCollection_t1197680765 * get_clientCertificates_0() const { return ___clientCertificates_0; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_clientCertificates_0() { return &___clientCertificates_0; }
	inline void set_clientCertificates_0(X509CertificateCollection_t1197680765 * value)
	{
		___clientCertificates_0 = value;
		Il2CppCodeGenWriteBarrier(&___clientCertificates_0, value);
	}

	inline static int32_t get_offset_of_targetHost_1() { return static_cast<int32_t>(offsetof(U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423, ___targetHost_1)); }
	inline String_t* get_targetHost_1() const { return ___targetHost_1; }
	inline String_t** get_address_of_targetHost_1() { return &___targetHost_1; }
	inline void set_targetHost_1(String_t* value)
	{
		___targetHost_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetHost_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBeginAuthenticateAsClientU3Ec__AnonStorey0_t3081887423, ___U24this_2)); }
	inline LegacySslStream_t83997747 * get_U24this_2() const { return ___U24this_2; }
	inline LegacySslStream_t83997747 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LegacySslStream_t83997747 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
