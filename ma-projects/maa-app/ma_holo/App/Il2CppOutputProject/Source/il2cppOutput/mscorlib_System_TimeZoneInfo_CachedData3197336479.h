﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.TimeZoneInfo
struct TimeZoneInfo_t436210607;
// System.Collections.Generic.Dictionary`2<System.String,System.TimeZoneInfo>
struct Dictionary_2_t2350989869;
// System.TimeZoneInfo/OffsetAndRule
struct OffsetAndRule_t2067953655;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/CachedData
struct  CachedData_t3197336479  : public Il2CppObject
{
public:
	// System.TimeZoneInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeZoneInfo/CachedData::m_localTimeZone
	TimeZoneInfo_t436210607 * ___m_localTimeZone_0;
	// System.TimeZoneInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeZoneInfo/CachedData::m_utcTimeZone
	TimeZoneInfo_t436210607 * ___m_utcTimeZone_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.TimeZoneInfo> System.TimeZoneInfo/CachedData::m_systemTimeZones
	Dictionary_2_t2350989869 * ___m_systemTimeZones_2;
	// System.Boolean System.TimeZoneInfo/CachedData::m_allSystemTimeZonesRead
	bool ___m_allSystemTimeZonesRead_3;
	// System.TimeZoneInfo/OffsetAndRule modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeZoneInfo/CachedData::m_oneYearLocalFromUtc
	OffsetAndRule_t2067953655 * ___m_oneYearLocalFromUtc_4;

public:
	inline static int32_t get_offset_of_m_localTimeZone_0() { return static_cast<int32_t>(offsetof(CachedData_t3197336479, ___m_localTimeZone_0)); }
	inline TimeZoneInfo_t436210607 * get_m_localTimeZone_0() const { return ___m_localTimeZone_0; }
	inline TimeZoneInfo_t436210607 ** get_address_of_m_localTimeZone_0() { return &___m_localTimeZone_0; }
	inline void set_m_localTimeZone_0(TimeZoneInfo_t436210607 * value)
	{
		___m_localTimeZone_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_localTimeZone_0, value);
	}

	inline static int32_t get_offset_of_m_utcTimeZone_1() { return static_cast<int32_t>(offsetof(CachedData_t3197336479, ___m_utcTimeZone_1)); }
	inline TimeZoneInfo_t436210607 * get_m_utcTimeZone_1() const { return ___m_utcTimeZone_1; }
	inline TimeZoneInfo_t436210607 ** get_address_of_m_utcTimeZone_1() { return &___m_utcTimeZone_1; }
	inline void set_m_utcTimeZone_1(TimeZoneInfo_t436210607 * value)
	{
		___m_utcTimeZone_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_utcTimeZone_1, value);
	}

	inline static int32_t get_offset_of_m_systemTimeZones_2() { return static_cast<int32_t>(offsetof(CachedData_t3197336479, ___m_systemTimeZones_2)); }
	inline Dictionary_2_t2350989869 * get_m_systemTimeZones_2() const { return ___m_systemTimeZones_2; }
	inline Dictionary_2_t2350989869 ** get_address_of_m_systemTimeZones_2() { return &___m_systemTimeZones_2; }
	inline void set_m_systemTimeZones_2(Dictionary_2_t2350989869 * value)
	{
		___m_systemTimeZones_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_systemTimeZones_2, value);
	}

	inline static int32_t get_offset_of_m_allSystemTimeZonesRead_3() { return static_cast<int32_t>(offsetof(CachedData_t3197336479, ___m_allSystemTimeZonesRead_3)); }
	inline bool get_m_allSystemTimeZonesRead_3() const { return ___m_allSystemTimeZonesRead_3; }
	inline bool* get_address_of_m_allSystemTimeZonesRead_3() { return &___m_allSystemTimeZonesRead_3; }
	inline void set_m_allSystemTimeZonesRead_3(bool value)
	{
		___m_allSystemTimeZonesRead_3 = value;
	}

	inline static int32_t get_offset_of_m_oneYearLocalFromUtc_4() { return static_cast<int32_t>(offsetof(CachedData_t3197336479, ___m_oneYearLocalFromUtc_4)); }
	inline OffsetAndRule_t2067953655 * get_m_oneYearLocalFromUtc_4() const { return ___m_oneYearLocalFromUtc_4; }
	inline OffsetAndRule_t2067953655 ** get_address_of_m_oneYearLocalFromUtc_4() { return &___m_oneYearLocalFromUtc_4; }
	inline void set_m_oneYearLocalFromUtc_4(OffsetAndRule_t2067953655 * value)
	{
		___m_oneYearLocalFromUtc_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_oneYearLocalFromUtc_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
