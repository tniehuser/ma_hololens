﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaEntity
struct  SchemaEntity_t980649128  : public Il2CppObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaEntity::qname
	XmlQualifiedName_t1944712516 * ___qname_0;
	// System.String System.Xml.Schema.SchemaEntity::url
	String_t* ___url_1;
	// System.String System.Xml.Schema.SchemaEntity::pubid
	String_t* ___pubid_2;
	// System.String System.Xml.Schema.SchemaEntity::text
	String_t* ___text_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaEntity::ndata
	XmlQualifiedName_t1944712516 * ___ndata_4;
	// System.Int32 System.Xml.Schema.SchemaEntity::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 System.Xml.Schema.SchemaEntity::linePosition
	int32_t ___linePosition_6;
	// System.Boolean System.Xml.Schema.SchemaEntity::isParameter
	bool ___isParameter_7;
	// System.Boolean System.Xml.Schema.SchemaEntity::isExternal
	bool ___isExternal_8;
	// System.Boolean System.Xml.Schema.SchemaEntity::parsingInProgress
	bool ___parsingInProgress_9;
	// System.Boolean System.Xml.Schema.SchemaEntity::isDeclaredInExternal
	bool ___isDeclaredInExternal_10;
	// System.String System.Xml.Schema.SchemaEntity::baseURI
	String_t* ___baseURI_11;
	// System.String System.Xml.Schema.SchemaEntity::declaredURI
	String_t* ___declaredURI_12;

public:
	inline static int32_t get_offset_of_qname_0() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___qname_0)); }
	inline XmlQualifiedName_t1944712516 * get_qname_0() const { return ___qname_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_0() { return &___qname_0; }
	inline void set_qname_0(XmlQualifiedName_t1944712516 * value)
	{
		___qname_0 = value;
		Il2CppCodeGenWriteBarrier(&___qname_0, value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_pubid_2() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___pubid_2)); }
	inline String_t* get_pubid_2() const { return ___pubid_2; }
	inline String_t** get_address_of_pubid_2() { return &___pubid_2; }
	inline void set_pubid_2(String_t* value)
	{
		___pubid_2 = value;
		Il2CppCodeGenWriteBarrier(&___pubid_2, value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}

	inline static int32_t get_offset_of_ndata_4() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___ndata_4)); }
	inline XmlQualifiedName_t1944712516 * get_ndata_4() const { return ___ndata_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_ndata_4() { return &___ndata_4; }
	inline void set_ndata_4(XmlQualifiedName_t1944712516 * value)
	{
		___ndata_4 = value;
		Il2CppCodeGenWriteBarrier(&___ndata_4, value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_linePosition_6() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___linePosition_6)); }
	inline int32_t get_linePosition_6() const { return ___linePosition_6; }
	inline int32_t* get_address_of_linePosition_6() { return &___linePosition_6; }
	inline void set_linePosition_6(int32_t value)
	{
		___linePosition_6 = value;
	}

	inline static int32_t get_offset_of_isParameter_7() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___isParameter_7)); }
	inline bool get_isParameter_7() const { return ___isParameter_7; }
	inline bool* get_address_of_isParameter_7() { return &___isParameter_7; }
	inline void set_isParameter_7(bool value)
	{
		___isParameter_7 = value;
	}

	inline static int32_t get_offset_of_isExternal_8() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___isExternal_8)); }
	inline bool get_isExternal_8() const { return ___isExternal_8; }
	inline bool* get_address_of_isExternal_8() { return &___isExternal_8; }
	inline void set_isExternal_8(bool value)
	{
		___isExternal_8 = value;
	}

	inline static int32_t get_offset_of_parsingInProgress_9() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___parsingInProgress_9)); }
	inline bool get_parsingInProgress_9() const { return ___parsingInProgress_9; }
	inline bool* get_address_of_parsingInProgress_9() { return &___parsingInProgress_9; }
	inline void set_parsingInProgress_9(bool value)
	{
		___parsingInProgress_9 = value;
	}

	inline static int32_t get_offset_of_isDeclaredInExternal_10() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___isDeclaredInExternal_10)); }
	inline bool get_isDeclaredInExternal_10() const { return ___isDeclaredInExternal_10; }
	inline bool* get_address_of_isDeclaredInExternal_10() { return &___isDeclaredInExternal_10; }
	inline void set_isDeclaredInExternal_10(bool value)
	{
		___isDeclaredInExternal_10 = value;
	}

	inline static int32_t get_offset_of_baseURI_11() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___baseURI_11)); }
	inline String_t* get_baseURI_11() const { return ___baseURI_11; }
	inline String_t** get_address_of_baseURI_11() { return &___baseURI_11; }
	inline void set_baseURI_11(String_t* value)
	{
		___baseURI_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseURI_11, value);
	}

	inline static int32_t get_offset_of_declaredURI_12() { return static_cast<int32_t>(offsetof(SchemaEntity_t980649128, ___declaredURI_12)); }
	inline String_t* get_declaredURI_12() const { return ___declaredURI_12; }
	inline String_t** get_address_of_declaredURI_12() { return &___declaredURI_12; }
	inline void set_declaredURI_12(String_t* value)
	{
		___declaredURI_12 = value;
		Il2CppCodeGenWriteBarrier(&___declaredURI_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
