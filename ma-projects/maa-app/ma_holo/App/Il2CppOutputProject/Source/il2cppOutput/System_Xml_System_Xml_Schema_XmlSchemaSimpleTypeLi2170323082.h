﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct  XmlSchemaSimpleTypeList_t2170323082  : public XmlSchemaSimpleTypeContent_t1606103299
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeList::itemTypeName
	XmlQualifiedName_t1944712516 * ___itemTypeName_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::itemType
	XmlSchemaSimpleType_t248156492 * ___itemType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::baseItemType
	XmlSchemaSimpleType_t248156492 * ___baseItemType_11;

public:
	inline static int32_t get_offset_of_itemTypeName_9() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___itemTypeName_9)); }
	inline XmlQualifiedName_t1944712516 * get_itemTypeName_9() const { return ___itemTypeName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_itemTypeName_9() { return &___itemTypeName_9; }
	inline void set_itemTypeName_9(XmlQualifiedName_t1944712516 * value)
	{
		___itemTypeName_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemTypeName_9, value);
	}

	inline static int32_t get_offset_of_itemType_10() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___itemType_10)); }
	inline XmlSchemaSimpleType_t248156492 * get_itemType_10() const { return ___itemType_10; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_itemType_10() { return &___itemType_10; }
	inline void set_itemType_10(XmlSchemaSimpleType_t248156492 * value)
	{
		___itemType_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemType_10, value);
	}

	inline static int32_t get_offset_of_baseItemType_11() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___baseItemType_11)); }
	inline XmlSchemaSimpleType_t248156492 * get_baseItemType_11() const { return ___baseItemType_11; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_baseItemType_11() { return &___baseItemType_11; }
	inline void set_baseItemType_11(XmlSchemaSimpleType_t248156492 * value)
	{
		___baseItemType_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseItemType_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
