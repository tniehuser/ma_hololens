﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.IntSizedArray
struct  IntSizedArray_t2017939501  : public Il2CppObject
{
public:
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.IntSizedArray::objects
	Int32U5BU5D_t3030399641* ___objects_0;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.IntSizedArray::negObjects
	Int32U5BU5D_t3030399641* ___negObjects_1;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(IntSizedArray_t2017939501, ___objects_0)); }
	inline Int32U5BU5D_t3030399641* get_objects_0() const { return ___objects_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(Int32U5BU5D_t3030399641* value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier(&___objects_0, value);
	}

	inline static int32_t get_offset_of_negObjects_1() { return static_cast<int32_t>(offsetof(IntSizedArray_t2017939501, ___negObjects_1)); }
	inline Int32U5BU5D_t3030399641* get_negObjects_1() const { return ___negObjects_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_negObjects_1() { return &___negObjects_1; }
	inline void set_negObjects_1(Int32U5BU5D_t3030399641* value)
	{
		___negObjects_1 = value;
		Il2CppCodeGenWriteBarrier(&___negObjects_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
