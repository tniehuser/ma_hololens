﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.XmlSchemaObject>
struct Dictionary_2_t3040654148;
// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry>
struct List_1_t1879707642;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable
struct  XmlSchemaObjectTable_t3364835593  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.XmlSchemaObject> System.Xml.Schema.XmlSchemaObjectTable::table
	Dictionary_2_t3040654148 * ___table_0;
	// System.Collections.Generic.List`1<System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry> System.Xml.Schema.XmlSchemaObjectTable::entries
	List_1_t1879707642 * ___entries_1;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_t3364835593, ___table_0)); }
	inline Dictionary_2_t3040654148 * get_table_0() const { return ___table_0; }
	inline Dictionary_2_t3040654148 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_t3040654148 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier(&___table_0, value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_t3364835593, ___entries_1)); }
	inline List_1_t1879707642 * get_entries_1() const { return ___entries_1; }
	inline List_1_t1879707642 ** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(List_1_t1879707642 * value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier(&___entries_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
