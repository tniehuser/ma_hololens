﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t2204811018;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCode
struct  RegexCode_t2469392150  : public Il2CppObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexCode::_codes
	Int32U5BU5D_t3030399641* ____codes_0;
	// System.String[] System.Text.RegularExpressions.RegexCode::_strings
	StringU5BU5D_t1642385972* ____strings_1;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_trackcount
	int32_t ____trackcount_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexCode::_caps
	Hashtable_t909839986 * ____caps_3;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_capsize
	int32_t ____capsize_4;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCode::_fcPrefix
	RegexPrefix_t1013837165 * ____fcPrefix_5;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCode::_bmPrefix
	RegexBoyerMoore_t2204811018 * ____bmPrefix_6;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_anchors
	int32_t ____anchors_7;
	// System.Boolean System.Text.RegularExpressions.RegexCode::_rightToLeft
	bool ____rightToLeft_8;

public:
	inline static int32_t get_offset_of__codes_0() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____codes_0)); }
	inline Int32U5BU5D_t3030399641* get__codes_0() const { return ____codes_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__codes_0() { return &____codes_0; }
	inline void set__codes_0(Int32U5BU5D_t3030399641* value)
	{
		____codes_0 = value;
		Il2CppCodeGenWriteBarrier(&____codes_0, value);
	}

	inline static int32_t get_offset_of__strings_1() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____strings_1)); }
	inline StringU5BU5D_t1642385972* get__strings_1() const { return ____strings_1; }
	inline StringU5BU5D_t1642385972** get_address_of__strings_1() { return &____strings_1; }
	inline void set__strings_1(StringU5BU5D_t1642385972* value)
	{
		____strings_1 = value;
		Il2CppCodeGenWriteBarrier(&____strings_1, value);
	}

	inline static int32_t get_offset_of__trackcount_2() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____trackcount_2)); }
	inline int32_t get__trackcount_2() const { return ____trackcount_2; }
	inline int32_t* get_address_of__trackcount_2() { return &____trackcount_2; }
	inline void set__trackcount_2(int32_t value)
	{
		____trackcount_2 = value;
	}

	inline static int32_t get_offset_of__caps_3() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____caps_3)); }
	inline Hashtable_t909839986 * get__caps_3() const { return ____caps_3; }
	inline Hashtable_t909839986 ** get_address_of__caps_3() { return &____caps_3; }
	inline void set__caps_3(Hashtable_t909839986 * value)
	{
		____caps_3 = value;
		Il2CppCodeGenWriteBarrier(&____caps_3, value);
	}

	inline static int32_t get_offset_of__capsize_4() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____capsize_4)); }
	inline int32_t get__capsize_4() const { return ____capsize_4; }
	inline int32_t* get_address_of__capsize_4() { return &____capsize_4; }
	inline void set__capsize_4(int32_t value)
	{
		____capsize_4 = value;
	}

	inline static int32_t get_offset_of__fcPrefix_5() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____fcPrefix_5)); }
	inline RegexPrefix_t1013837165 * get__fcPrefix_5() const { return ____fcPrefix_5; }
	inline RegexPrefix_t1013837165 ** get_address_of__fcPrefix_5() { return &____fcPrefix_5; }
	inline void set__fcPrefix_5(RegexPrefix_t1013837165 * value)
	{
		____fcPrefix_5 = value;
		Il2CppCodeGenWriteBarrier(&____fcPrefix_5, value);
	}

	inline static int32_t get_offset_of__bmPrefix_6() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____bmPrefix_6)); }
	inline RegexBoyerMoore_t2204811018 * get__bmPrefix_6() const { return ____bmPrefix_6; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of__bmPrefix_6() { return &____bmPrefix_6; }
	inline void set__bmPrefix_6(RegexBoyerMoore_t2204811018 * value)
	{
		____bmPrefix_6 = value;
		Il2CppCodeGenWriteBarrier(&____bmPrefix_6, value);
	}

	inline static int32_t get_offset_of__anchors_7() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____anchors_7)); }
	inline int32_t get__anchors_7() const { return ____anchors_7; }
	inline int32_t* get_address_of__anchors_7() { return &____anchors_7; }
	inline void set__anchors_7(int32_t value)
	{
		____anchors_7 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_8() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____rightToLeft_8)); }
	inline bool get__rightToLeft_8() const { return ____rightToLeft_8; }
	inline bool* get_address_of__rightToLeft_8() { return &____rightToLeft_8; }
	inline void set__rightToLeft_8(bool value)
	{
		____rightToLeft_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
