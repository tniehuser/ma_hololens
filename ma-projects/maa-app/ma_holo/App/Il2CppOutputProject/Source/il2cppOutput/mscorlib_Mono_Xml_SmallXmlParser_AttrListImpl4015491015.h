﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.SmallXmlParser/AttrListImpl
struct  AttrListImpl_t4015491015  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> Mono.Xml.SmallXmlParser/AttrListImpl::attrNames
	List_1_t1398341365 * ___attrNames_0;
	// System.Collections.Generic.List`1<System.String> Mono.Xml.SmallXmlParser/AttrListImpl::attrValues
	List_1_t1398341365 * ___attrValues_1;

public:
	inline static int32_t get_offset_of_attrNames_0() { return static_cast<int32_t>(offsetof(AttrListImpl_t4015491015, ___attrNames_0)); }
	inline List_1_t1398341365 * get_attrNames_0() const { return ___attrNames_0; }
	inline List_1_t1398341365 ** get_address_of_attrNames_0() { return &___attrNames_0; }
	inline void set_attrNames_0(List_1_t1398341365 * value)
	{
		___attrNames_0 = value;
		Il2CppCodeGenWriteBarrier(&___attrNames_0, value);
	}

	inline static int32_t get_offset_of_attrValues_1() { return static_cast<int32_t>(offsetof(AttrListImpl_t4015491015, ___attrValues_1)); }
	inline List_1_t1398341365 * get_attrValues_1() const { return ___attrValues_1; }
	inline List_1_t1398341365 ** get_address_of_attrValues_1() { return &___attrValues_1; }
	inline void set_attrValues_1(List_1_t1398341365 * value)
	{
		___attrValues_1 = value;
		Il2CppCodeGenWriteBarrier(&___attrValues_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
