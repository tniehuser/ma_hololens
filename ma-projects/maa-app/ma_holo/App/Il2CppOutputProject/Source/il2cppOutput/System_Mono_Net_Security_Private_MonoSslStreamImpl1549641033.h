﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Net.Security.IMonoSslStream
struct IMonoSslStream_t424679660;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoSslStreamImpl
struct  MonoSslStreamImpl_t1549641033  : public Il2CppObject
{
public:
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.Private.MonoSslStreamImpl::impl
	Il2CppObject * ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(MonoSslStreamImpl_t1549641033, ___impl_0)); }
	inline Il2CppObject * get_impl_0() const { return ___impl_0; }
	inline Il2CppObject ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(Il2CppObject * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier(&___impl_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
