﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_DecompressionMethods2530166567.h"
#include "System_System_Net_HttpWebRequest_AuthorizationStat3879143100.h"

// System.Uri
struct Uri_t19570940;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.String
struct String_t;
// System.Net.HttpContinueDelegate
struct HttpContinueDelegate_t2713047268;
// System.Net.CookieContainer
struct CookieContainer_t2808809223;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Version
struct Version_t1755874712;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.Net.WebConnectionStream
struct WebConnectionStream_t1922483508;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2828383075;
// System.Net.WebAsyncResult
struct WebAsyncResult_t905414499;
// System.EventHandler
struct EventHandler_t277755526;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;
// System.Net.WebConnection
struct WebConnection_t324679648;
// Mono.Net.Security.IMonoTlsProvider
struct IMonoTlsProvider_t2506971578;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t302829305;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t2774612835;
// System.Action`1<System.IO.Stream>
struct Action_1_t3057236188;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpWebRequest
struct  HttpWebRequest_t1951404513  : public WebRequest_t1365124353
{
public:
	// System.Uri System.Net.HttpWebRequest::requestUri
	Uri_t19570940 * ___requestUri_12;
	// System.Uri System.Net.HttpWebRequest::actualUri
	Uri_t19570940 * ___actualUri_13;
	// System.Boolean System.Net.HttpWebRequest::hostChanged
	bool ___hostChanged_14;
	// System.Boolean System.Net.HttpWebRequest::allowAutoRedirect
	bool ___allowAutoRedirect_15;
	// System.Boolean System.Net.HttpWebRequest::allowBuffering
	bool ___allowBuffering_16;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.HttpWebRequest::certificates
	X509CertificateCollection_t1197680765 * ___certificates_17;
	// System.String System.Net.HttpWebRequest::connectionGroup
	String_t* ___connectionGroup_18;
	// System.Boolean System.Net.HttpWebRequest::haveContentLength
	bool ___haveContentLength_19;
	// System.Int64 System.Net.HttpWebRequest::contentLength
	int64_t ___contentLength_20;
	// System.Net.HttpContinueDelegate System.Net.HttpWebRequest::continueDelegate
	HttpContinueDelegate_t2713047268 * ___continueDelegate_21;
	// System.Net.CookieContainer System.Net.HttpWebRequest::cookieContainer
	CookieContainer_t2808809223 * ___cookieContainer_22;
	// System.Net.ICredentials System.Net.HttpWebRequest::credentials
	Il2CppObject * ___credentials_23;
	// System.Boolean System.Net.HttpWebRequest::haveResponse
	bool ___haveResponse_24;
	// System.Boolean System.Net.HttpWebRequest::haveRequest
	bool ___haveRequest_25;
	// System.Boolean System.Net.HttpWebRequest::requestSent
	bool ___requestSent_26;
	// System.Net.WebHeaderCollection System.Net.HttpWebRequest::webHeaders
	WebHeaderCollection_t3028142837 * ___webHeaders_27;
	// System.Boolean System.Net.HttpWebRequest::keepAlive
	bool ___keepAlive_28;
	// System.Int32 System.Net.HttpWebRequest::maxAutoRedirect
	int32_t ___maxAutoRedirect_29;
	// System.String System.Net.HttpWebRequest::mediaType
	String_t* ___mediaType_30;
	// System.String System.Net.HttpWebRequest::method
	String_t* ___method_31;
	// System.String System.Net.HttpWebRequest::initialMethod
	String_t* ___initialMethod_32;
	// System.Boolean System.Net.HttpWebRequest::pipelined
	bool ___pipelined_33;
	// System.Boolean System.Net.HttpWebRequest::preAuthenticate
	bool ___preAuthenticate_34;
	// System.Boolean System.Net.HttpWebRequest::usedPreAuth
	bool ___usedPreAuth_35;
	// System.Version System.Net.HttpWebRequest::version
	Version_t1755874712 * ___version_36;
	// System.Boolean System.Net.HttpWebRequest::force_version
	bool ___force_version_37;
	// System.Version System.Net.HttpWebRequest::actualVersion
	Version_t1755874712 * ___actualVersion_38;
	// System.Net.IWebProxy System.Net.HttpWebRequest::proxy
	Il2CppObject * ___proxy_39;
	// System.Boolean System.Net.HttpWebRequest::sendChunked
	bool ___sendChunked_40;
	// System.Net.ServicePoint System.Net.HttpWebRequest::servicePoint
	ServicePoint_t2765344313 * ___servicePoint_41;
	// System.Int32 System.Net.HttpWebRequest::timeout
	int32_t ___timeout_42;
	// System.Net.WebConnectionStream System.Net.HttpWebRequest::writeStream
	WebConnectionStream_t1922483508 * ___writeStream_43;
	// System.Net.HttpWebResponse System.Net.HttpWebRequest::webResponse
	HttpWebResponse_t2828383075 * ___webResponse_44;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncWrite
	WebAsyncResult_t905414499 * ___asyncWrite_45;
	// System.Net.WebAsyncResult System.Net.HttpWebRequest::asyncRead
	WebAsyncResult_t905414499 * ___asyncRead_46;
	// System.EventHandler System.Net.HttpWebRequest::abortHandler
	EventHandler_t277755526 * ___abortHandler_47;
	// System.Int32 System.Net.HttpWebRequest::aborted
	int32_t ___aborted_48;
	// System.Boolean System.Net.HttpWebRequest::gotRequestStream
	bool ___gotRequestStream_49;
	// System.Int32 System.Net.HttpWebRequest::redirects
	int32_t ___redirects_50;
	// System.Boolean System.Net.HttpWebRequest::expectContinue
	bool ___expectContinue_51;
	// System.Byte[] System.Net.HttpWebRequest::bodyBuffer
	ByteU5BU5D_t3397334013* ___bodyBuffer_52;
	// System.Int32 System.Net.HttpWebRequest::bodyBufferLength
	int32_t ___bodyBufferLength_53;
	// System.Boolean System.Net.HttpWebRequest::getResponseCalled
	bool ___getResponseCalled_54;
	// System.Exception System.Net.HttpWebRequest::saved_exc
	Exception_t1927440687 * ___saved_exc_55;
	// System.Object System.Net.HttpWebRequest::locker
	Il2CppObject * ___locker_56;
	// System.Boolean System.Net.HttpWebRequest::finished_reading
	bool ___finished_reading_57;
	// System.Net.WebConnection System.Net.HttpWebRequest::WebConnection
	WebConnection_t324679648 * ___WebConnection_58;
	// System.Net.DecompressionMethods System.Net.HttpWebRequest::auto_decomp
	int32_t ___auto_decomp_59;
	// System.Int32 System.Net.HttpWebRequest::readWriteTimeout
	int32_t ___readWriteTimeout_61;
	// Mono.Net.Security.IMonoTlsProvider System.Net.HttpWebRequest::tlsProvider
	Il2CppObject * ___tlsProvider_62;
	// Mono.Security.Interface.MonoTlsSettings System.Net.HttpWebRequest::tlsSettings
	MonoTlsSettings_t302829305 * ___tlsSettings_63;
	// System.Net.ServerCertValidationCallback System.Net.HttpWebRequest::certValidationCallback
	ServerCertValidationCallback_t2774612835 * ___certValidationCallback_64;
	// System.Net.HttpWebRequest/AuthorizationState System.Net.HttpWebRequest::auth_state
	AuthorizationState_t3879143100  ___auth_state_65;
	// System.Net.HttpWebRequest/AuthorizationState System.Net.HttpWebRequest::proxy_auth_state
	AuthorizationState_t3879143100  ___proxy_auth_state_66;
	// System.String System.Net.HttpWebRequest::host
	String_t* ___host_67;
	// System.Action`1<System.IO.Stream> System.Net.HttpWebRequest::ResendContentFactory
	Action_1_t3057236188 * ___ResendContentFactory_68;
	// System.Boolean System.Net.HttpWebRequest::<ThrowOnError>k__BackingField
	bool ___U3CThrowOnErrorU3Ek__BackingField_69;
	// System.Boolean System.Net.HttpWebRequest::unsafe_auth_blah
	bool ___unsafe_auth_blah_70;
	// System.Boolean System.Net.HttpWebRequest::<ReuseConnection>k__BackingField
	bool ___U3CReuseConnectionU3Ek__BackingField_71;
	// System.Net.WebConnection System.Net.HttpWebRequest::StoredConnection
	WebConnection_t324679648 * ___StoredConnection_72;

public:
	inline static int32_t get_offset_of_requestUri_12() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___requestUri_12)); }
	inline Uri_t19570940 * get_requestUri_12() const { return ___requestUri_12; }
	inline Uri_t19570940 ** get_address_of_requestUri_12() { return &___requestUri_12; }
	inline void set_requestUri_12(Uri_t19570940 * value)
	{
		___requestUri_12 = value;
		Il2CppCodeGenWriteBarrier(&___requestUri_12, value);
	}

	inline static int32_t get_offset_of_actualUri_13() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___actualUri_13)); }
	inline Uri_t19570940 * get_actualUri_13() const { return ___actualUri_13; }
	inline Uri_t19570940 ** get_address_of_actualUri_13() { return &___actualUri_13; }
	inline void set_actualUri_13(Uri_t19570940 * value)
	{
		___actualUri_13 = value;
		Il2CppCodeGenWriteBarrier(&___actualUri_13, value);
	}

	inline static int32_t get_offset_of_hostChanged_14() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___hostChanged_14)); }
	inline bool get_hostChanged_14() const { return ___hostChanged_14; }
	inline bool* get_address_of_hostChanged_14() { return &___hostChanged_14; }
	inline void set_hostChanged_14(bool value)
	{
		___hostChanged_14 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_15() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___allowAutoRedirect_15)); }
	inline bool get_allowAutoRedirect_15() const { return ___allowAutoRedirect_15; }
	inline bool* get_address_of_allowAutoRedirect_15() { return &___allowAutoRedirect_15; }
	inline void set_allowAutoRedirect_15(bool value)
	{
		___allowAutoRedirect_15 = value;
	}

	inline static int32_t get_offset_of_allowBuffering_16() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___allowBuffering_16)); }
	inline bool get_allowBuffering_16() const { return ___allowBuffering_16; }
	inline bool* get_address_of_allowBuffering_16() { return &___allowBuffering_16; }
	inline void set_allowBuffering_16(bool value)
	{
		___allowBuffering_16 = value;
	}

	inline static int32_t get_offset_of_certificates_17() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___certificates_17)); }
	inline X509CertificateCollection_t1197680765 * get_certificates_17() const { return ___certificates_17; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_certificates_17() { return &___certificates_17; }
	inline void set_certificates_17(X509CertificateCollection_t1197680765 * value)
	{
		___certificates_17 = value;
		Il2CppCodeGenWriteBarrier(&___certificates_17, value);
	}

	inline static int32_t get_offset_of_connectionGroup_18() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___connectionGroup_18)); }
	inline String_t* get_connectionGroup_18() const { return ___connectionGroup_18; }
	inline String_t** get_address_of_connectionGroup_18() { return &___connectionGroup_18; }
	inline void set_connectionGroup_18(String_t* value)
	{
		___connectionGroup_18 = value;
		Il2CppCodeGenWriteBarrier(&___connectionGroup_18, value);
	}

	inline static int32_t get_offset_of_haveContentLength_19() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___haveContentLength_19)); }
	inline bool get_haveContentLength_19() const { return ___haveContentLength_19; }
	inline bool* get_address_of_haveContentLength_19() { return &___haveContentLength_19; }
	inline void set_haveContentLength_19(bool value)
	{
		___haveContentLength_19 = value;
	}

	inline static int32_t get_offset_of_contentLength_20() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___contentLength_20)); }
	inline int64_t get_contentLength_20() const { return ___contentLength_20; }
	inline int64_t* get_address_of_contentLength_20() { return &___contentLength_20; }
	inline void set_contentLength_20(int64_t value)
	{
		___contentLength_20 = value;
	}

	inline static int32_t get_offset_of_continueDelegate_21() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___continueDelegate_21)); }
	inline HttpContinueDelegate_t2713047268 * get_continueDelegate_21() const { return ___continueDelegate_21; }
	inline HttpContinueDelegate_t2713047268 ** get_address_of_continueDelegate_21() { return &___continueDelegate_21; }
	inline void set_continueDelegate_21(HttpContinueDelegate_t2713047268 * value)
	{
		___continueDelegate_21 = value;
		Il2CppCodeGenWriteBarrier(&___continueDelegate_21, value);
	}

	inline static int32_t get_offset_of_cookieContainer_22() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___cookieContainer_22)); }
	inline CookieContainer_t2808809223 * get_cookieContainer_22() const { return ___cookieContainer_22; }
	inline CookieContainer_t2808809223 ** get_address_of_cookieContainer_22() { return &___cookieContainer_22; }
	inline void set_cookieContainer_22(CookieContainer_t2808809223 * value)
	{
		___cookieContainer_22 = value;
		Il2CppCodeGenWriteBarrier(&___cookieContainer_22, value);
	}

	inline static int32_t get_offset_of_credentials_23() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___credentials_23)); }
	inline Il2CppObject * get_credentials_23() const { return ___credentials_23; }
	inline Il2CppObject ** get_address_of_credentials_23() { return &___credentials_23; }
	inline void set_credentials_23(Il2CppObject * value)
	{
		___credentials_23 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_23, value);
	}

	inline static int32_t get_offset_of_haveResponse_24() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___haveResponse_24)); }
	inline bool get_haveResponse_24() const { return ___haveResponse_24; }
	inline bool* get_address_of_haveResponse_24() { return &___haveResponse_24; }
	inline void set_haveResponse_24(bool value)
	{
		___haveResponse_24 = value;
	}

	inline static int32_t get_offset_of_haveRequest_25() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___haveRequest_25)); }
	inline bool get_haveRequest_25() const { return ___haveRequest_25; }
	inline bool* get_address_of_haveRequest_25() { return &___haveRequest_25; }
	inline void set_haveRequest_25(bool value)
	{
		___haveRequest_25 = value;
	}

	inline static int32_t get_offset_of_requestSent_26() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___requestSent_26)); }
	inline bool get_requestSent_26() const { return ___requestSent_26; }
	inline bool* get_address_of_requestSent_26() { return &___requestSent_26; }
	inline void set_requestSent_26(bool value)
	{
		___requestSent_26 = value;
	}

	inline static int32_t get_offset_of_webHeaders_27() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___webHeaders_27)); }
	inline WebHeaderCollection_t3028142837 * get_webHeaders_27() const { return ___webHeaders_27; }
	inline WebHeaderCollection_t3028142837 ** get_address_of_webHeaders_27() { return &___webHeaders_27; }
	inline void set_webHeaders_27(WebHeaderCollection_t3028142837 * value)
	{
		___webHeaders_27 = value;
		Il2CppCodeGenWriteBarrier(&___webHeaders_27, value);
	}

	inline static int32_t get_offset_of_keepAlive_28() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___keepAlive_28)); }
	inline bool get_keepAlive_28() const { return ___keepAlive_28; }
	inline bool* get_address_of_keepAlive_28() { return &___keepAlive_28; }
	inline void set_keepAlive_28(bool value)
	{
		___keepAlive_28 = value;
	}

	inline static int32_t get_offset_of_maxAutoRedirect_29() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___maxAutoRedirect_29)); }
	inline int32_t get_maxAutoRedirect_29() const { return ___maxAutoRedirect_29; }
	inline int32_t* get_address_of_maxAutoRedirect_29() { return &___maxAutoRedirect_29; }
	inline void set_maxAutoRedirect_29(int32_t value)
	{
		___maxAutoRedirect_29 = value;
	}

	inline static int32_t get_offset_of_mediaType_30() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___mediaType_30)); }
	inline String_t* get_mediaType_30() const { return ___mediaType_30; }
	inline String_t** get_address_of_mediaType_30() { return &___mediaType_30; }
	inline void set_mediaType_30(String_t* value)
	{
		___mediaType_30 = value;
		Il2CppCodeGenWriteBarrier(&___mediaType_30, value);
	}

	inline static int32_t get_offset_of_method_31() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___method_31)); }
	inline String_t* get_method_31() const { return ___method_31; }
	inline String_t** get_address_of_method_31() { return &___method_31; }
	inline void set_method_31(String_t* value)
	{
		___method_31 = value;
		Il2CppCodeGenWriteBarrier(&___method_31, value);
	}

	inline static int32_t get_offset_of_initialMethod_32() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___initialMethod_32)); }
	inline String_t* get_initialMethod_32() const { return ___initialMethod_32; }
	inline String_t** get_address_of_initialMethod_32() { return &___initialMethod_32; }
	inline void set_initialMethod_32(String_t* value)
	{
		___initialMethod_32 = value;
		Il2CppCodeGenWriteBarrier(&___initialMethod_32, value);
	}

	inline static int32_t get_offset_of_pipelined_33() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___pipelined_33)); }
	inline bool get_pipelined_33() const { return ___pipelined_33; }
	inline bool* get_address_of_pipelined_33() { return &___pipelined_33; }
	inline void set_pipelined_33(bool value)
	{
		___pipelined_33 = value;
	}

	inline static int32_t get_offset_of_preAuthenticate_34() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___preAuthenticate_34)); }
	inline bool get_preAuthenticate_34() const { return ___preAuthenticate_34; }
	inline bool* get_address_of_preAuthenticate_34() { return &___preAuthenticate_34; }
	inline void set_preAuthenticate_34(bool value)
	{
		___preAuthenticate_34 = value;
	}

	inline static int32_t get_offset_of_usedPreAuth_35() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___usedPreAuth_35)); }
	inline bool get_usedPreAuth_35() const { return ___usedPreAuth_35; }
	inline bool* get_address_of_usedPreAuth_35() { return &___usedPreAuth_35; }
	inline void set_usedPreAuth_35(bool value)
	{
		___usedPreAuth_35 = value;
	}

	inline static int32_t get_offset_of_version_36() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___version_36)); }
	inline Version_t1755874712 * get_version_36() const { return ___version_36; }
	inline Version_t1755874712 ** get_address_of_version_36() { return &___version_36; }
	inline void set_version_36(Version_t1755874712 * value)
	{
		___version_36 = value;
		Il2CppCodeGenWriteBarrier(&___version_36, value);
	}

	inline static int32_t get_offset_of_force_version_37() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___force_version_37)); }
	inline bool get_force_version_37() const { return ___force_version_37; }
	inline bool* get_address_of_force_version_37() { return &___force_version_37; }
	inline void set_force_version_37(bool value)
	{
		___force_version_37 = value;
	}

	inline static int32_t get_offset_of_actualVersion_38() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___actualVersion_38)); }
	inline Version_t1755874712 * get_actualVersion_38() const { return ___actualVersion_38; }
	inline Version_t1755874712 ** get_address_of_actualVersion_38() { return &___actualVersion_38; }
	inline void set_actualVersion_38(Version_t1755874712 * value)
	{
		___actualVersion_38 = value;
		Il2CppCodeGenWriteBarrier(&___actualVersion_38, value);
	}

	inline static int32_t get_offset_of_proxy_39() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___proxy_39)); }
	inline Il2CppObject * get_proxy_39() const { return ___proxy_39; }
	inline Il2CppObject ** get_address_of_proxy_39() { return &___proxy_39; }
	inline void set_proxy_39(Il2CppObject * value)
	{
		___proxy_39 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_39, value);
	}

	inline static int32_t get_offset_of_sendChunked_40() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___sendChunked_40)); }
	inline bool get_sendChunked_40() const { return ___sendChunked_40; }
	inline bool* get_address_of_sendChunked_40() { return &___sendChunked_40; }
	inline void set_sendChunked_40(bool value)
	{
		___sendChunked_40 = value;
	}

	inline static int32_t get_offset_of_servicePoint_41() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___servicePoint_41)); }
	inline ServicePoint_t2765344313 * get_servicePoint_41() const { return ___servicePoint_41; }
	inline ServicePoint_t2765344313 ** get_address_of_servicePoint_41() { return &___servicePoint_41; }
	inline void set_servicePoint_41(ServicePoint_t2765344313 * value)
	{
		___servicePoint_41 = value;
		Il2CppCodeGenWriteBarrier(&___servicePoint_41, value);
	}

	inline static int32_t get_offset_of_timeout_42() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___timeout_42)); }
	inline int32_t get_timeout_42() const { return ___timeout_42; }
	inline int32_t* get_address_of_timeout_42() { return &___timeout_42; }
	inline void set_timeout_42(int32_t value)
	{
		___timeout_42 = value;
	}

	inline static int32_t get_offset_of_writeStream_43() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___writeStream_43)); }
	inline WebConnectionStream_t1922483508 * get_writeStream_43() const { return ___writeStream_43; }
	inline WebConnectionStream_t1922483508 ** get_address_of_writeStream_43() { return &___writeStream_43; }
	inline void set_writeStream_43(WebConnectionStream_t1922483508 * value)
	{
		___writeStream_43 = value;
		Il2CppCodeGenWriteBarrier(&___writeStream_43, value);
	}

	inline static int32_t get_offset_of_webResponse_44() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___webResponse_44)); }
	inline HttpWebResponse_t2828383075 * get_webResponse_44() const { return ___webResponse_44; }
	inline HttpWebResponse_t2828383075 ** get_address_of_webResponse_44() { return &___webResponse_44; }
	inline void set_webResponse_44(HttpWebResponse_t2828383075 * value)
	{
		___webResponse_44 = value;
		Il2CppCodeGenWriteBarrier(&___webResponse_44, value);
	}

	inline static int32_t get_offset_of_asyncWrite_45() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___asyncWrite_45)); }
	inline WebAsyncResult_t905414499 * get_asyncWrite_45() const { return ___asyncWrite_45; }
	inline WebAsyncResult_t905414499 ** get_address_of_asyncWrite_45() { return &___asyncWrite_45; }
	inline void set_asyncWrite_45(WebAsyncResult_t905414499 * value)
	{
		___asyncWrite_45 = value;
		Il2CppCodeGenWriteBarrier(&___asyncWrite_45, value);
	}

	inline static int32_t get_offset_of_asyncRead_46() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___asyncRead_46)); }
	inline WebAsyncResult_t905414499 * get_asyncRead_46() const { return ___asyncRead_46; }
	inline WebAsyncResult_t905414499 ** get_address_of_asyncRead_46() { return &___asyncRead_46; }
	inline void set_asyncRead_46(WebAsyncResult_t905414499 * value)
	{
		___asyncRead_46 = value;
		Il2CppCodeGenWriteBarrier(&___asyncRead_46, value);
	}

	inline static int32_t get_offset_of_abortHandler_47() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___abortHandler_47)); }
	inline EventHandler_t277755526 * get_abortHandler_47() const { return ___abortHandler_47; }
	inline EventHandler_t277755526 ** get_address_of_abortHandler_47() { return &___abortHandler_47; }
	inline void set_abortHandler_47(EventHandler_t277755526 * value)
	{
		___abortHandler_47 = value;
		Il2CppCodeGenWriteBarrier(&___abortHandler_47, value);
	}

	inline static int32_t get_offset_of_aborted_48() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___aborted_48)); }
	inline int32_t get_aborted_48() const { return ___aborted_48; }
	inline int32_t* get_address_of_aborted_48() { return &___aborted_48; }
	inline void set_aborted_48(int32_t value)
	{
		___aborted_48 = value;
	}

	inline static int32_t get_offset_of_gotRequestStream_49() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___gotRequestStream_49)); }
	inline bool get_gotRequestStream_49() const { return ___gotRequestStream_49; }
	inline bool* get_address_of_gotRequestStream_49() { return &___gotRequestStream_49; }
	inline void set_gotRequestStream_49(bool value)
	{
		___gotRequestStream_49 = value;
	}

	inline static int32_t get_offset_of_redirects_50() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___redirects_50)); }
	inline int32_t get_redirects_50() const { return ___redirects_50; }
	inline int32_t* get_address_of_redirects_50() { return &___redirects_50; }
	inline void set_redirects_50(int32_t value)
	{
		___redirects_50 = value;
	}

	inline static int32_t get_offset_of_expectContinue_51() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___expectContinue_51)); }
	inline bool get_expectContinue_51() const { return ___expectContinue_51; }
	inline bool* get_address_of_expectContinue_51() { return &___expectContinue_51; }
	inline void set_expectContinue_51(bool value)
	{
		___expectContinue_51 = value;
	}

	inline static int32_t get_offset_of_bodyBuffer_52() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___bodyBuffer_52)); }
	inline ByteU5BU5D_t3397334013* get_bodyBuffer_52() const { return ___bodyBuffer_52; }
	inline ByteU5BU5D_t3397334013** get_address_of_bodyBuffer_52() { return &___bodyBuffer_52; }
	inline void set_bodyBuffer_52(ByteU5BU5D_t3397334013* value)
	{
		___bodyBuffer_52 = value;
		Il2CppCodeGenWriteBarrier(&___bodyBuffer_52, value);
	}

	inline static int32_t get_offset_of_bodyBufferLength_53() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___bodyBufferLength_53)); }
	inline int32_t get_bodyBufferLength_53() const { return ___bodyBufferLength_53; }
	inline int32_t* get_address_of_bodyBufferLength_53() { return &___bodyBufferLength_53; }
	inline void set_bodyBufferLength_53(int32_t value)
	{
		___bodyBufferLength_53 = value;
	}

	inline static int32_t get_offset_of_getResponseCalled_54() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___getResponseCalled_54)); }
	inline bool get_getResponseCalled_54() const { return ___getResponseCalled_54; }
	inline bool* get_address_of_getResponseCalled_54() { return &___getResponseCalled_54; }
	inline void set_getResponseCalled_54(bool value)
	{
		___getResponseCalled_54 = value;
	}

	inline static int32_t get_offset_of_saved_exc_55() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___saved_exc_55)); }
	inline Exception_t1927440687 * get_saved_exc_55() const { return ___saved_exc_55; }
	inline Exception_t1927440687 ** get_address_of_saved_exc_55() { return &___saved_exc_55; }
	inline void set_saved_exc_55(Exception_t1927440687 * value)
	{
		___saved_exc_55 = value;
		Il2CppCodeGenWriteBarrier(&___saved_exc_55, value);
	}

	inline static int32_t get_offset_of_locker_56() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___locker_56)); }
	inline Il2CppObject * get_locker_56() const { return ___locker_56; }
	inline Il2CppObject ** get_address_of_locker_56() { return &___locker_56; }
	inline void set_locker_56(Il2CppObject * value)
	{
		___locker_56 = value;
		Il2CppCodeGenWriteBarrier(&___locker_56, value);
	}

	inline static int32_t get_offset_of_finished_reading_57() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___finished_reading_57)); }
	inline bool get_finished_reading_57() const { return ___finished_reading_57; }
	inline bool* get_address_of_finished_reading_57() { return &___finished_reading_57; }
	inline void set_finished_reading_57(bool value)
	{
		___finished_reading_57 = value;
	}

	inline static int32_t get_offset_of_WebConnection_58() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___WebConnection_58)); }
	inline WebConnection_t324679648 * get_WebConnection_58() const { return ___WebConnection_58; }
	inline WebConnection_t324679648 ** get_address_of_WebConnection_58() { return &___WebConnection_58; }
	inline void set_WebConnection_58(WebConnection_t324679648 * value)
	{
		___WebConnection_58 = value;
		Il2CppCodeGenWriteBarrier(&___WebConnection_58, value);
	}

	inline static int32_t get_offset_of_auto_decomp_59() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___auto_decomp_59)); }
	inline int32_t get_auto_decomp_59() const { return ___auto_decomp_59; }
	inline int32_t* get_address_of_auto_decomp_59() { return &___auto_decomp_59; }
	inline void set_auto_decomp_59(int32_t value)
	{
		___auto_decomp_59 = value;
	}

	inline static int32_t get_offset_of_readWriteTimeout_61() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___readWriteTimeout_61)); }
	inline int32_t get_readWriteTimeout_61() const { return ___readWriteTimeout_61; }
	inline int32_t* get_address_of_readWriteTimeout_61() { return &___readWriteTimeout_61; }
	inline void set_readWriteTimeout_61(int32_t value)
	{
		___readWriteTimeout_61 = value;
	}

	inline static int32_t get_offset_of_tlsProvider_62() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___tlsProvider_62)); }
	inline Il2CppObject * get_tlsProvider_62() const { return ___tlsProvider_62; }
	inline Il2CppObject ** get_address_of_tlsProvider_62() { return &___tlsProvider_62; }
	inline void set_tlsProvider_62(Il2CppObject * value)
	{
		___tlsProvider_62 = value;
		Il2CppCodeGenWriteBarrier(&___tlsProvider_62, value);
	}

	inline static int32_t get_offset_of_tlsSettings_63() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___tlsSettings_63)); }
	inline MonoTlsSettings_t302829305 * get_tlsSettings_63() const { return ___tlsSettings_63; }
	inline MonoTlsSettings_t302829305 ** get_address_of_tlsSettings_63() { return &___tlsSettings_63; }
	inline void set_tlsSettings_63(MonoTlsSettings_t302829305 * value)
	{
		___tlsSettings_63 = value;
		Il2CppCodeGenWriteBarrier(&___tlsSettings_63, value);
	}

	inline static int32_t get_offset_of_certValidationCallback_64() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___certValidationCallback_64)); }
	inline ServerCertValidationCallback_t2774612835 * get_certValidationCallback_64() const { return ___certValidationCallback_64; }
	inline ServerCertValidationCallback_t2774612835 ** get_address_of_certValidationCallback_64() { return &___certValidationCallback_64; }
	inline void set_certValidationCallback_64(ServerCertValidationCallback_t2774612835 * value)
	{
		___certValidationCallback_64 = value;
		Il2CppCodeGenWriteBarrier(&___certValidationCallback_64, value);
	}

	inline static int32_t get_offset_of_auth_state_65() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___auth_state_65)); }
	inline AuthorizationState_t3879143100  get_auth_state_65() const { return ___auth_state_65; }
	inline AuthorizationState_t3879143100 * get_address_of_auth_state_65() { return &___auth_state_65; }
	inline void set_auth_state_65(AuthorizationState_t3879143100  value)
	{
		___auth_state_65 = value;
	}

	inline static int32_t get_offset_of_proxy_auth_state_66() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___proxy_auth_state_66)); }
	inline AuthorizationState_t3879143100  get_proxy_auth_state_66() const { return ___proxy_auth_state_66; }
	inline AuthorizationState_t3879143100 * get_address_of_proxy_auth_state_66() { return &___proxy_auth_state_66; }
	inline void set_proxy_auth_state_66(AuthorizationState_t3879143100  value)
	{
		___proxy_auth_state_66 = value;
	}

	inline static int32_t get_offset_of_host_67() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___host_67)); }
	inline String_t* get_host_67() const { return ___host_67; }
	inline String_t** get_address_of_host_67() { return &___host_67; }
	inline void set_host_67(String_t* value)
	{
		___host_67 = value;
		Il2CppCodeGenWriteBarrier(&___host_67, value);
	}

	inline static int32_t get_offset_of_ResendContentFactory_68() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___ResendContentFactory_68)); }
	inline Action_1_t3057236188 * get_ResendContentFactory_68() const { return ___ResendContentFactory_68; }
	inline Action_1_t3057236188 ** get_address_of_ResendContentFactory_68() { return &___ResendContentFactory_68; }
	inline void set_ResendContentFactory_68(Action_1_t3057236188 * value)
	{
		___ResendContentFactory_68 = value;
		Il2CppCodeGenWriteBarrier(&___ResendContentFactory_68, value);
	}

	inline static int32_t get_offset_of_U3CThrowOnErrorU3Ek__BackingField_69() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___U3CThrowOnErrorU3Ek__BackingField_69)); }
	inline bool get_U3CThrowOnErrorU3Ek__BackingField_69() const { return ___U3CThrowOnErrorU3Ek__BackingField_69; }
	inline bool* get_address_of_U3CThrowOnErrorU3Ek__BackingField_69() { return &___U3CThrowOnErrorU3Ek__BackingField_69; }
	inline void set_U3CThrowOnErrorU3Ek__BackingField_69(bool value)
	{
		___U3CThrowOnErrorU3Ek__BackingField_69 = value;
	}

	inline static int32_t get_offset_of_unsafe_auth_blah_70() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___unsafe_auth_blah_70)); }
	inline bool get_unsafe_auth_blah_70() const { return ___unsafe_auth_blah_70; }
	inline bool* get_address_of_unsafe_auth_blah_70() { return &___unsafe_auth_blah_70; }
	inline void set_unsafe_auth_blah_70(bool value)
	{
		___unsafe_auth_blah_70 = value;
	}

	inline static int32_t get_offset_of_U3CReuseConnectionU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___U3CReuseConnectionU3Ek__BackingField_71)); }
	inline bool get_U3CReuseConnectionU3Ek__BackingField_71() const { return ___U3CReuseConnectionU3Ek__BackingField_71; }
	inline bool* get_address_of_U3CReuseConnectionU3Ek__BackingField_71() { return &___U3CReuseConnectionU3Ek__BackingField_71; }
	inline void set_U3CReuseConnectionU3Ek__BackingField_71(bool value)
	{
		___U3CReuseConnectionU3Ek__BackingField_71 = value;
	}

	inline static int32_t get_offset_of_StoredConnection_72() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513, ___StoredConnection_72)); }
	inline WebConnection_t324679648 * get_StoredConnection_72() const { return ___StoredConnection_72; }
	inline WebConnection_t324679648 ** get_address_of_StoredConnection_72() { return &___StoredConnection_72; }
	inline void set_StoredConnection_72(WebConnection_t324679648 * value)
	{
		___StoredConnection_72 = value;
		Il2CppCodeGenWriteBarrier(&___StoredConnection_72, value);
	}
};

struct HttpWebRequest_t1951404513_StaticFields
{
public:
	// System.Int32 System.Net.HttpWebRequest::defaultMaxResponseHeadersLength
	int32_t ___defaultMaxResponseHeadersLength_60;

public:
	inline static int32_t get_offset_of_defaultMaxResponseHeadersLength_60() { return static_cast<int32_t>(offsetof(HttpWebRequest_t1951404513_StaticFields, ___defaultMaxResponseHeadersLength_60)); }
	inline int32_t get_defaultMaxResponseHeadersLength_60() const { return ___defaultMaxResponseHeadersLength_60; }
	inline int32_t* get_address_of_defaultMaxResponseHeadersLength_60() { return &___defaultMaxResponseHeadersLength_60; }
	inline void set_defaultMaxResponseHeadersLength_60(int32_t value)
	{
		___defaultMaxResponseHeadersLength_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
