﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.LinkedList`1<System.WeakReference>
struct LinkedList_1_t1382113796;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t1032950796;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TimerThread
struct  TimerThread_t777216161  : public Il2CppObject
{
public:

public:
};

struct TimerThread_t777216161_StaticFields
{
public:
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_Queues
	LinkedList_1_t1382113796 * ___s_Queues_0;
	// System.Collections.Generic.LinkedList`1<System.WeakReference> System.Net.TimerThread::s_NewQueues
	LinkedList_1_t1382113796 * ___s_NewQueues_1;
	// System.Int32 System.Net.TimerThread::s_ThreadState
	int32_t ___s_ThreadState_2;
	// System.Threading.AutoResetEvent System.Net.TimerThread::s_ThreadReadyEvent
	AutoResetEvent_t15112628 * ___s_ThreadReadyEvent_3;
	// System.Threading.ManualResetEvent System.Net.TimerThread::s_ThreadShutdownEvent
	ManualResetEvent_t926074657 * ___s_ThreadShutdownEvent_4;
	// System.Threading.WaitHandle[] System.Net.TimerThread::s_ThreadEvents
	WaitHandleU5BU5D_t1032950796* ___s_ThreadEvents_5;
	// System.Collections.Hashtable System.Net.TimerThread::s_QueuesCache
	Hashtable_t909839986 * ___s_QueuesCache_6;

public:
	inline static int32_t get_offset_of_s_Queues_0() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_Queues_0)); }
	inline LinkedList_1_t1382113796 * get_s_Queues_0() const { return ___s_Queues_0; }
	inline LinkedList_1_t1382113796 ** get_address_of_s_Queues_0() { return &___s_Queues_0; }
	inline void set_s_Queues_0(LinkedList_1_t1382113796 * value)
	{
		___s_Queues_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Queues_0, value);
	}

	inline static int32_t get_offset_of_s_NewQueues_1() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_NewQueues_1)); }
	inline LinkedList_1_t1382113796 * get_s_NewQueues_1() const { return ___s_NewQueues_1; }
	inline LinkedList_1_t1382113796 ** get_address_of_s_NewQueues_1() { return &___s_NewQueues_1; }
	inline void set_s_NewQueues_1(LinkedList_1_t1382113796 * value)
	{
		___s_NewQueues_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_NewQueues_1, value);
	}

	inline static int32_t get_offset_of_s_ThreadState_2() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_ThreadState_2)); }
	inline int32_t get_s_ThreadState_2() const { return ___s_ThreadState_2; }
	inline int32_t* get_address_of_s_ThreadState_2() { return &___s_ThreadState_2; }
	inline void set_s_ThreadState_2(int32_t value)
	{
		___s_ThreadState_2 = value;
	}

	inline static int32_t get_offset_of_s_ThreadReadyEvent_3() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_ThreadReadyEvent_3)); }
	inline AutoResetEvent_t15112628 * get_s_ThreadReadyEvent_3() const { return ___s_ThreadReadyEvent_3; }
	inline AutoResetEvent_t15112628 ** get_address_of_s_ThreadReadyEvent_3() { return &___s_ThreadReadyEvent_3; }
	inline void set_s_ThreadReadyEvent_3(AutoResetEvent_t15112628 * value)
	{
		___s_ThreadReadyEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_ThreadReadyEvent_3, value);
	}

	inline static int32_t get_offset_of_s_ThreadShutdownEvent_4() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_ThreadShutdownEvent_4)); }
	inline ManualResetEvent_t926074657 * get_s_ThreadShutdownEvent_4() const { return ___s_ThreadShutdownEvent_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_s_ThreadShutdownEvent_4() { return &___s_ThreadShutdownEvent_4; }
	inline void set_s_ThreadShutdownEvent_4(ManualResetEvent_t926074657 * value)
	{
		___s_ThreadShutdownEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_ThreadShutdownEvent_4, value);
	}

	inline static int32_t get_offset_of_s_ThreadEvents_5() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_ThreadEvents_5)); }
	inline WaitHandleU5BU5D_t1032950796* get_s_ThreadEvents_5() const { return ___s_ThreadEvents_5; }
	inline WaitHandleU5BU5D_t1032950796** get_address_of_s_ThreadEvents_5() { return &___s_ThreadEvents_5; }
	inline void set_s_ThreadEvents_5(WaitHandleU5BU5D_t1032950796* value)
	{
		___s_ThreadEvents_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_ThreadEvents_5, value);
	}

	inline static int32_t get_offset_of_s_QueuesCache_6() { return static_cast<int32_t>(offsetof(TimerThread_t777216161_StaticFields, ___s_QueuesCache_6)); }
	inline Hashtable_t909839986 * get_s_QueuesCache_6() const { return ___s_QueuesCache_6; }
	inline Hashtable_t909839986 ** get_address_of_s_QueuesCache_6() { return &___s_QueuesCache_6; }
	inline void set_s_QueuesCache_6(Hashtable_t909839986 * value)
	{
		___s_QueuesCache_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_QueuesCache_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
