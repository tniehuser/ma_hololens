﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"

// System.Object
struct Il2CppObject;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Threading.SemaphoreSlim/TaskNode
struct TaskNode_t453295914;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_t2945603725;
// System.Action`1<System.Object>
struct Action_1_t2491248677;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SemaphoreSlim
struct  SemaphoreSlim_t461808439  : public Il2CppObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SemaphoreSlim::m_currentCount
	int32_t ___m_currentCount_0;
	// System.Int32 System.Threading.SemaphoreSlim::m_maxCount
	int32_t ___m_maxCount_1;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SemaphoreSlim::m_waitCount
	int32_t ___m_waitCount_2;
	// System.Object System.Threading.SemaphoreSlim::m_lockObj
	Il2CppObject * ___m_lockObj_3;
	// System.Threading.ManualResetEvent modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SemaphoreSlim::m_waitHandle
	ManualResetEvent_t926074657 * ___m_waitHandle_4;
	// System.Threading.SemaphoreSlim/TaskNode System.Threading.SemaphoreSlim::m_asyncHead
	TaskNode_t453295914 * ___m_asyncHead_5;
	// System.Threading.SemaphoreSlim/TaskNode System.Threading.SemaphoreSlim::m_asyncTail
	TaskNode_t453295914 * ___m_asyncTail_6;

public:
	inline static int32_t get_offset_of_m_currentCount_0() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_currentCount_0)); }
	inline int32_t get_m_currentCount_0() const { return ___m_currentCount_0; }
	inline int32_t* get_address_of_m_currentCount_0() { return &___m_currentCount_0; }
	inline void set_m_currentCount_0(int32_t value)
	{
		___m_currentCount_0 = value;
	}

	inline static int32_t get_offset_of_m_maxCount_1() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_maxCount_1)); }
	inline int32_t get_m_maxCount_1() const { return ___m_maxCount_1; }
	inline int32_t* get_address_of_m_maxCount_1() { return &___m_maxCount_1; }
	inline void set_m_maxCount_1(int32_t value)
	{
		___m_maxCount_1 = value;
	}

	inline static int32_t get_offset_of_m_waitCount_2() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_waitCount_2)); }
	inline int32_t get_m_waitCount_2() const { return ___m_waitCount_2; }
	inline int32_t* get_address_of_m_waitCount_2() { return &___m_waitCount_2; }
	inline void set_m_waitCount_2(int32_t value)
	{
		___m_waitCount_2 = value;
	}

	inline static int32_t get_offset_of_m_lockObj_3() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_lockObj_3)); }
	inline Il2CppObject * get_m_lockObj_3() const { return ___m_lockObj_3; }
	inline Il2CppObject ** get_address_of_m_lockObj_3() { return &___m_lockObj_3; }
	inline void set_m_lockObj_3(Il2CppObject * value)
	{
		___m_lockObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObj_3, value);
	}

	inline static int32_t get_offset_of_m_waitHandle_4() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_waitHandle_4)); }
	inline ManualResetEvent_t926074657 * get_m_waitHandle_4() const { return ___m_waitHandle_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_m_waitHandle_4() { return &___m_waitHandle_4; }
	inline void set_m_waitHandle_4(ManualResetEvent_t926074657 * value)
	{
		___m_waitHandle_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_waitHandle_4, value);
	}

	inline static int32_t get_offset_of_m_asyncHead_5() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_asyncHead_5)); }
	inline TaskNode_t453295914 * get_m_asyncHead_5() const { return ___m_asyncHead_5; }
	inline TaskNode_t453295914 ** get_address_of_m_asyncHead_5() { return &___m_asyncHead_5; }
	inline void set_m_asyncHead_5(TaskNode_t453295914 * value)
	{
		___m_asyncHead_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_asyncHead_5, value);
	}

	inline static int32_t get_offset_of_m_asyncTail_6() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439, ___m_asyncTail_6)); }
	inline TaskNode_t453295914 * get_m_asyncTail_6() const { return ___m_asyncTail_6; }
	inline TaskNode_t453295914 ** get_address_of_m_asyncTail_6() { return &___m_asyncTail_6; }
	inline void set_m_asyncTail_6(TaskNode_t453295914 * value)
	{
		___m_asyncTail_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_asyncTail_6, value);
	}
};

struct SemaphoreSlim_t461808439_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Boolean> System.Threading.SemaphoreSlim::s_trueTask
	Task_1_t2945603725 * ___s_trueTask_7;
	// System.Action`1<System.Object> System.Threading.SemaphoreSlim::s_cancellationTokenCanceledEventHandler
	Action_1_t2491248677 * ___s_cancellationTokenCanceledEventHandler_8;

public:
	inline static int32_t get_offset_of_s_trueTask_7() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439_StaticFields, ___s_trueTask_7)); }
	inline Task_1_t2945603725 * get_s_trueTask_7() const { return ___s_trueTask_7; }
	inline Task_1_t2945603725 ** get_address_of_s_trueTask_7() { return &___s_trueTask_7; }
	inline void set_s_trueTask_7(Task_1_t2945603725 * value)
	{
		___s_trueTask_7 = value;
		Il2CppCodeGenWriteBarrier(&___s_trueTask_7, value);
	}

	inline static int32_t get_offset_of_s_cancellationTokenCanceledEventHandler_8() { return static_cast<int32_t>(offsetof(SemaphoreSlim_t461808439_StaticFields, ___s_cancellationTokenCanceledEventHandler_8)); }
	inline Action_1_t2491248677 * get_s_cancellationTokenCanceledEventHandler_8() const { return ___s_cancellationTokenCanceledEventHandler_8; }
	inline Action_1_t2491248677 ** get_address_of_s_cancellationTokenCanceledEventHandler_8() { return &___s_cancellationTokenCanceledEventHandler_8; }
	inline void set_s_cancellationTokenCanceledEventHandler_8(Action_1_t2491248677 * value)
	{
		___s_cancellationTokenCanceledEventHandler_8 = value;
		Il2CppCodeGenWriteBarrier(&___s_cancellationTokenCanceledEventHandler_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
