﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_SystemException3877406272.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathException
struct  XPathException_t1503722168  : public SystemException_t3877406272
{
public:
	// System.String System.Xml.XPath.XPathException::res
	String_t* ___res_16;
	// System.String[] System.Xml.XPath.XPathException::args
	StringU5BU5D_t1642385972* ___args_17;
	// System.String System.Xml.XPath.XPathException::message
	String_t* ___message_18;

public:
	inline static int32_t get_offset_of_res_16() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___res_16)); }
	inline String_t* get_res_16() const { return ___res_16; }
	inline String_t** get_address_of_res_16() { return &___res_16; }
	inline void set_res_16(String_t* value)
	{
		___res_16 = value;
		Il2CppCodeGenWriteBarrier(&___res_16, value);
	}

	inline static int32_t get_offset_of_args_17() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___args_17)); }
	inline StringU5BU5D_t1642385972* get_args_17() const { return ___args_17; }
	inline StringU5BU5D_t1642385972** get_address_of_args_17() { return &___args_17; }
	inline void set_args_17(StringU5BU5D_t1642385972* value)
	{
		___args_17 = value;
		Il2CppCodeGenWriteBarrier(&___args_17, value);
	}

	inline static int32_t get_offset_of_message_18() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___message_18)); }
	inline String_t* get_message_18() const { return ___message_18; }
	inline String_t** get_address_of_message_18() { return &___message_18; }
	inline void set_message_18(String_t* value)
	{
		___message_18 = value;
		Il2CppCodeGenWriteBarrier(&___message_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
