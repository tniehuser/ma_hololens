﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Security.Cryptography.OidCollection
struct OidCollection_t3790243618;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_t3674631724  : public Il2CppObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::m_oids
	OidCollection_t3790243618 * ___m_oids_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::m_current
	int32_t ___m_current_1;

public:
	inline static int32_t get_offset_of_m_oids_0() { return static_cast<int32_t>(offsetof(OidEnumerator_t3674631724, ___m_oids_0)); }
	inline OidCollection_t3790243618 * get_m_oids_0() const { return ___m_oids_0; }
	inline OidCollection_t3790243618 ** get_address_of_m_oids_0() { return &___m_oids_0; }
	inline void set_m_oids_0(OidCollection_t3790243618 * value)
	{
		___m_oids_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_oids_0, value);
	}

	inline static int32_t get_offset_of_m_current_1() { return static_cast<int32_t>(offsetof(OidEnumerator_t3674631724, ___m_current_1)); }
	inline int32_t get_m_current_1() const { return ___m_current_1; }
	inline int32_t* get_address_of_m_current_1() { return &___m_current_1; }
	inline void set_m_current_1(int32_t value)
	{
		___m_current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
