﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Runtime.Serialization.DeserializationEventHandler
struct DeserializationEventHandler_t1801856893;
// System.Runtime.Serialization.SerializationEventHandler
struct SerializationEventHandler_t2339848500;
// System.Runtime.Serialization.ObjectHolder[]
struct ObjectHolderU5BU5D_t2337234454;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.ObjectHolderList
struct ObjectHolderList_t1856843635;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1912587528;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectManager
struct  ObjectManager_t2645893724  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.DeserializationEventHandler System.Runtime.Serialization.ObjectManager::m_onDeserializationHandler
	DeserializationEventHandler_t1801856893 * ___m_onDeserializationHandler_0;
	// System.Runtime.Serialization.SerializationEventHandler System.Runtime.Serialization.ObjectManager::m_onDeserializedHandler
	SerializationEventHandler_t2339848500 * ___m_onDeserializedHandler_1;
	// System.Runtime.Serialization.ObjectHolder[] System.Runtime.Serialization.ObjectManager::m_objects
	ObjectHolderU5BU5D_t2337234454* ___m_objects_2;
	// System.Object System.Runtime.Serialization.ObjectManager::m_topObject
	Il2CppObject * ___m_topObject_3;
	// System.Runtime.Serialization.ObjectHolderList System.Runtime.Serialization.ObjectManager::m_specialFixupObjects
	ObjectHolderList_t1856843635 * ___m_specialFixupObjects_4;
	// System.Int64 System.Runtime.Serialization.ObjectManager::m_fixupCount
	int64_t ___m_fixupCount_5;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.ObjectManager::m_selector
	Il2CppObject * ___m_selector_6;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.ObjectManager::m_context
	StreamingContext_t1417235061  ___m_context_7;
	// System.Boolean System.Runtime.Serialization.ObjectManager::m_isCrossAppDomain
	bool ___m_isCrossAppDomain_8;

public:
	inline static int32_t get_offset_of_m_onDeserializationHandler_0() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_onDeserializationHandler_0)); }
	inline DeserializationEventHandler_t1801856893 * get_m_onDeserializationHandler_0() const { return ___m_onDeserializationHandler_0; }
	inline DeserializationEventHandler_t1801856893 ** get_address_of_m_onDeserializationHandler_0() { return &___m_onDeserializationHandler_0; }
	inline void set_m_onDeserializationHandler_0(DeserializationEventHandler_t1801856893 * value)
	{
		___m_onDeserializationHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_onDeserializationHandler_0, value);
	}

	inline static int32_t get_offset_of_m_onDeserializedHandler_1() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_onDeserializedHandler_1)); }
	inline SerializationEventHandler_t2339848500 * get_m_onDeserializedHandler_1() const { return ___m_onDeserializedHandler_1; }
	inline SerializationEventHandler_t2339848500 ** get_address_of_m_onDeserializedHandler_1() { return &___m_onDeserializedHandler_1; }
	inline void set_m_onDeserializedHandler_1(SerializationEventHandler_t2339848500 * value)
	{
		___m_onDeserializedHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onDeserializedHandler_1, value);
	}

	inline static int32_t get_offset_of_m_objects_2() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_objects_2)); }
	inline ObjectHolderU5BU5D_t2337234454* get_m_objects_2() const { return ___m_objects_2; }
	inline ObjectHolderU5BU5D_t2337234454** get_address_of_m_objects_2() { return &___m_objects_2; }
	inline void set_m_objects_2(ObjectHolderU5BU5D_t2337234454* value)
	{
		___m_objects_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_objects_2, value);
	}

	inline static int32_t get_offset_of_m_topObject_3() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_topObject_3)); }
	inline Il2CppObject * get_m_topObject_3() const { return ___m_topObject_3; }
	inline Il2CppObject ** get_address_of_m_topObject_3() { return &___m_topObject_3; }
	inline void set_m_topObject_3(Il2CppObject * value)
	{
		___m_topObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_topObject_3, value);
	}

	inline static int32_t get_offset_of_m_specialFixupObjects_4() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_specialFixupObjects_4)); }
	inline ObjectHolderList_t1856843635 * get_m_specialFixupObjects_4() const { return ___m_specialFixupObjects_4; }
	inline ObjectHolderList_t1856843635 ** get_address_of_m_specialFixupObjects_4() { return &___m_specialFixupObjects_4; }
	inline void set_m_specialFixupObjects_4(ObjectHolderList_t1856843635 * value)
	{
		___m_specialFixupObjects_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_specialFixupObjects_4, value);
	}

	inline static int32_t get_offset_of_m_fixupCount_5() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_fixupCount_5)); }
	inline int64_t get_m_fixupCount_5() const { return ___m_fixupCount_5; }
	inline int64_t* get_address_of_m_fixupCount_5() { return &___m_fixupCount_5; }
	inline void set_m_fixupCount_5(int64_t value)
	{
		___m_fixupCount_5 = value;
	}

	inline static int32_t get_offset_of_m_selector_6() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_selector_6)); }
	inline Il2CppObject * get_m_selector_6() const { return ___m_selector_6; }
	inline Il2CppObject ** get_address_of_m_selector_6() { return &___m_selector_6; }
	inline void set_m_selector_6(Il2CppObject * value)
	{
		___m_selector_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_selector_6, value);
	}

	inline static int32_t get_offset_of_m_context_7() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_context_7)); }
	inline StreamingContext_t1417235061  get_m_context_7() const { return ___m_context_7; }
	inline StreamingContext_t1417235061 * get_address_of_m_context_7() { return &___m_context_7; }
	inline void set_m_context_7(StreamingContext_t1417235061  value)
	{
		___m_context_7 = value;
	}

	inline static int32_t get_offset_of_m_isCrossAppDomain_8() { return static_cast<int32_t>(offsetof(ObjectManager_t2645893724, ___m_isCrossAppDomain_8)); }
	inline bool get_m_isCrossAppDomain_8() const { return ___m_isCrossAppDomain_8; }
	inline bool* get_address_of_m_isCrossAppDomain_8() { return &___m_isCrossAppDomain_8; }
	inline void set_m_isCrossAppDomain_8(bool value)
	{
		___m_isCrossAppDomain_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
