﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_MS_Internal_Xml_XPath_AstNode2002670936.h"

// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2002670936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Filter
struct  Filter_t3094301242  : public AstNode_t2002670936
{
public:
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::input
	AstNode_t2002670936 * ___input_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Filter::condition
	AstNode_t2002670936 * ___condition_1;

public:
	inline static int32_t get_offset_of_input_0() { return static_cast<int32_t>(offsetof(Filter_t3094301242, ___input_0)); }
	inline AstNode_t2002670936 * get_input_0() const { return ___input_0; }
	inline AstNode_t2002670936 ** get_address_of_input_0() { return &___input_0; }
	inline void set_input_0(AstNode_t2002670936 * value)
	{
		___input_0 = value;
		Il2CppCodeGenWriteBarrier(&___input_0, value);
	}

	inline static int32_t get_offset_of_condition_1() { return static_cast<int32_t>(offsetof(Filter_t3094301242, ___condition_1)); }
	inline AstNode_t2002670936 * get_condition_1() const { return ___condition_1; }
	inline AstNode_t2002670936 ** get_address_of_condition_1() { return &___condition_1; }
	inline void set_condition_1(AstNode_t2002670936 * value)
	{
		___condition_1 = value;
		Il2CppCodeGenWriteBarrier(&___condition_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
