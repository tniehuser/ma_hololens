﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3938752374.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC3722381418.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Security.Protocol.Tls.CipherSuite
struct CipherSuite_t491456551;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
struct  TlsServerHello_t1289300668  : public HandshakeMessage_t3938752374
{
public:
	// Mono.Security.Protocol.Tls.SecurityCompressionType Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::compressionMethod
	int32_t ___compressionMethod_16;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::random
	ByteU5BU5D_t3397334013* ___random_17;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::sessionId
	ByteU5BU5D_t3397334013* ___sessionId_18;
	// Mono.Security.Protocol.Tls.CipherSuite Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::cipherSuite
	CipherSuite_t491456551 * ___cipherSuite_19;

public:
	inline static int32_t get_offset_of_compressionMethod_16() { return static_cast<int32_t>(offsetof(TlsServerHello_t1289300668, ___compressionMethod_16)); }
	inline int32_t get_compressionMethod_16() const { return ___compressionMethod_16; }
	inline int32_t* get_address_of_compressionMethod_16() { return &___compressionMethod_16; }
	inline void set_compressionMethod_16(int32_t value)
	{
		___compressionMethod_16 = value;
	}

	inline static int32_t get_offset_of_random_17() { return static_cast<int32_t>(offsetof(TlsServerHello_t1289300668, ___random_17)); }
	inline ByteU5BU5D_t3397334013* get_random_17() const { return ___random_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_random_17() { return &___random_17; }
	inline void set_random_17(ByteU5BU5D_t3397334013* value)
	{
		___random_17 = value;
		Il2CppCodeGenWriteBarrier(&___random_17, value);
	}

	inline static int32_t get_offset_of_sessionId_18() { return static_cast<int32_t>(offsetof(TlsServerHello_t1289300668, ___sessionId_18)); }
	inline ByteU5BU5D_t3397334013* get_sessionId_18() const { return ___sessionId_18; }
	inline ByteU5BU5D_t3397334013** get_address_of_sessionId_18() { return &___sessionId_18; }
	inline void set_sessionId_18(ByteU5BU5D_t3397334013* value)
	{
		___sessionId_18 = value;
		Il2CppCodeGenWriteBarrier(&___sessionId_18, value);
	}

	inline static int32_t get_offset_of_cipherSuite_19() { return static_cast<int32_t>(offsetof(TlsServerHello_t1289300668, ___cipherSuite_19)); }
	inline CipherSuite_t491456551 * get_cipherSuite_19() const { return ___cipherSuite_19; }
	inline CipherSuite_t491456551 ** get_address_of_cipherSuite_19() { return &___cipherSuite_19; }
	inline void set_cipherSuite_19(CipherSuite_t491456551 * value)
	{
		___cipherSuite_19 = value;
		Il2CppCodeGenWriteBarrier(&___cipherSuite_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
