﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_t243342855;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t1695958676  : public Stream_t3255436806
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t3397334013* ___buf_10;
	// System.String System.IO.FileStream::name
	String_t* ___name_11;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_t243342855 * ___safeHandle_12;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_13;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_14;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_15;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_16;
	// System.Boolean System.IO.FileStream::async
	bool ___async_17;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_18;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_19;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_20;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_21;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_22;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_23;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_24;

public:
	inline static int32_t get_offset_of_buf_10() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_10)); }
	inline ByteU5BU5D_t3397334013* get_buf_10() const { return ___buf_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_buf_10() { return &___buf_10; }
	inline void set_buf_10(ByteU5BU5D_t3397334013* value)
	{
		___buf_10 = value;
		Il2CppCodeGenWriteBarrier(&___buf_10, value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___name_11)); }
	inline String_t* get_name_11() const { return ___name_11; }
	inline String_t** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(String_t* value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier(&___name_11, value);
	}

	inline static int32_t get_offset_of_safeHandle_12() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___safeHandle_12)); }
	inline SafeFileHandle_t243342855 * get_safeHandle_12() const { return ___safeHandle_12; }
	inline SafeFileHandle_t243342855 ** get_address_of_safeHandle_12() { return &___safeHandle_12; }
	inline void set_safeHandle_12(SafeFileHandle_t243342855 * value)
	{
		___safeHandle_12 = value;
		Il2CppCodeGenWriteBarrier(&___safeHandle_12, value);
	}

	inline static int32_t get_offset_of_isExposed_13() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___isExposed_13)); }
	inline bool get_isExposed_13() const { return ___isExposed_13; }
	inline bool* get_address_of_isExposed_13() { return &___isExposed_13; }
	inline void set_isExposed_13(bool value)
	{
		___isExposed_13 = value;
	}

	inline static int32_t get_offset_of_append_startpos_14() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___append_startpos_14)); }
	inline int64_t get_append_startpos_14() const { return ___append_startpos_14; }
	inline int64_t* get_address_of_append_startpos_14() { return &___append_startpos_14; }
	inline void set_append_startpos_14(int64_t value)
	{
		___append_startpos_14 = value;
	}

	inline static int32_t get_offset_of_access_15() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___access_15)); }
	inline int32_t get_access_15() const { return ___access_15; }
	inline int32_t* get_address_of_access_15() { return &___access_15; }
	inline void set_access_15(int32_t value)
	{
		___access_15 = value;
	}

	inline static int32_t get_offset_of_owner_16() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___owner_16)); }
	inline bool get_owner_16() const { return ___owner_16; }
	inline bool* get_address_of_owner_16() { return &___owner_16; }
	inline void set_owner_16(bool value)
	{
		___owner_16 = value;
	}

	inline static int32_t get_offset_of_async_17() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___async_17)); }
	inline bool get_async_17() const { return ___async_17; }
	inline bool* get_address_of_async_17() { return &___async_17; }
	inline void set_async_17(bool value)
	{
		___async_17 = value;
	}

	inline static int32_t get_offset_of_canseek_18() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___canseek_18)); }
	inline bool get_canseek_18() const { return ___canseek_18; }
	inline bool* get_address_of_canseek_18() { return &___canseek_18; }
	inline void set_canseek_18(bool value)
	{
		___canseek_18 = value;
	}

	inline static int32_t get_offset_of_anonymous_19() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___anonymous_19)); }
	inline bool get_anonymous_19() const { return ___anonymous_19; }
	inline bool* get_address_of_anonymous_19() { return &___anonymous_19; }
	inline void set_anonymous_19(bool value)
	{
		___anonymous_19 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_20() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_dirty_20)); }
	inline bool get_buf_dirty_20() const { return ___buf_dirty_20; }
	inline bool* get_address_of_buf_dirty_20() { return &___buf_dirty_20; }
	inline void set_buf_dirty_20(bool value)
	{
		___buf_dirty_20 = value;
	}

	inline static int32_t get_offset_of_buf_size_21() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_size_21)); }
	inline int32_t get_buf_size_21() const { return ___buf_size_21; }
	inline int32_t* get_address_of_buf_size_21() { return &___buf_size_21; }
	inline void set_buf_size_21(int32_t value)
	{
		___buf_size_21 = value;
	}

	inline static int32_t get_offset_of_buf_length_22() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_length_22)); }
	inline int32_t get_buf_length_22() const { return ___buf_length_22; }
	inline int32_t* get_address_of_buf_length_22() { return &___buf_length_22; }
	inline void set_buf_length_22(int32_t value)
	{
		___buf_length_22 = value;
	}

	inline static int32_t get_offset_of_buf_offset_23() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_offset_23)); }
	inline int32_t get_buf_offset_23() const { return ___buf_offset_23; }
	inline int32_t* get_address_of_buf_offset_23() { return &___buf_offset_23; }
	inline void set_buf_offset_23(int32_t value)
	{
		___buf_offset_23 = value;
	}

	inline static int32_t get_offset_of_buf_start_24() { return static_cast<int32_t>(offsetof(FileStream_t1695958676, ___buf_start_24)); }
	inline int64_t get_buf_start_24() const { return ___buf_start_24; }
	inline int64_t* get_address_of_buf_start_24() { return &___buf_start_24; }
	inline void set_buf_start_24(int64_t value)
	{
		___buf_start_24 = value;
	}
};

struct FileStream_t1695958676_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_t3397334013* ___buf_recycle_8;
	// System.Object System.IO.FileStream::buf_recycle_lock
	Il2CppObject * ___buf_recycle_lock_9;

public:
	inline static int32_t get_offset_of_buf_recycle_8() { return static_cast<int32_t>(offsetof(FileStream_t1695958676_StaticFields, ___buf_recycle_8)); }
	inline ByteU5BU5D_t3397334013* get_buf_recycle_8() const { return ___buf_recycle_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_buf_recycle_8() { return &___buf_recycle_8; }
	inline void set_buf_recycle_8(ByteU5BU5D_t3397334013* value)
	{
		___buf_recycle_8 = value;
		Il2CppCodeGenWriteBarrier(&___buf_recycle_8, value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_9() { return static_cast<int32_t>(offsetof(FileStream_t1695958676_StaticFields, ___buf_recycle_lock_9)); }
	inline Il2CppObject * get_buf_recycle_lock_9() const { return ___buf_recycle_lock_9; }
	inline Il2CppObject ** get_address_of_buf_recycle_lock_9() { return &___buf_recycle_lock_9; }
	inline void set_buf_recycle_lock_9(Il2CppObject * value)
	{
		___buf_recycle_lock_9 = value;
		Il2CppCodeGenWriteBarrier(&___buf_recycle_lock_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
