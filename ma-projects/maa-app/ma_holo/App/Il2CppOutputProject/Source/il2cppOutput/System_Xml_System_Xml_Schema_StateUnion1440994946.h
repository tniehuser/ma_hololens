﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StateUnion
struct  StateUnion_t1440994946 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::State
			int32_t ___State_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___State_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::AllElementsRequired
			int32_t ___AllElementsRequired_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___AllElementsRequired_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::CurPosIndex
			int32_t ___CurPosIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___CurPosIndex_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::NumberOfRunningPos
			int32_t ___NumberOfRunningPos_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___NumberOfRunningPos_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_State_0() { return static_cast<int32_t>(offsetof(StateUnion_t1440994946, ___State_0)); }
	inline int32_t get_State_0() const { return ___State_0; }
	inline int32_t* get_address_of_State_0() { return &___State_0; }
	inline void set_State_0(int32_t value)
	{
		___State_0 = value;
	}

	inline static int32_t get_offset_of_AllElementsRequired_1() { return static_cast<int32_t>(offsetof(StateUnion_t1440994946, ___AllElementsRequired_1)); }
	inline int32_t get_AllElementsRequired_1() const { return ___AllElementsRequired_1; }
	inline int32_t* get_address_of_AllElementsRequired_1() { return &___AllElementsRequired_1; }
	inline void set_AllElementsRequired_1(int32_t value)
	{
		___AllElementsRequired_1 = value;
	}

	inline static int32_t get_offset_of_CurPosIndex_2() { return static_cast<int32_t>(offsetof(StateUnion_t1440994946, ___CurPosIndex_2)); }
	inline int32_t get_CurPosIndex_2() const { return ___CurPosIndex_2; }
	inline int32_t* get_address_of_CurPosIndex_2() { return &___CurPosIndex_2; }
	inline void set_CurPosIndex_2(int32_t value)
	{
		___CurPosIndex_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfRunningPos_3() { return static_cast<int32_t>(offsetof(StateUnion_t1440994946, ___NumberOfRunningPos_3)); }
	inline int32_t get_NumberOfRunningPos_3() const { return ___NumberOfRunningPos_3; }
	inline int32_t* get_address_of_NumberOfRunningPos_3() { return &___NumberOfRunningPos_3; }
	inline void set_NumberOfRunningPos_3(int32_t value)
	{
		___NumberOfRunningPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
