﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlReader3675626668.h"

// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t3122949129;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t3514170725  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReader::impl
	XmlTextReaderImpl_t3122949129 * ___impl_3;

public:
	inline static int32_t get_offset_of_impl_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t3514170725, ___impl_3)); }
	inline XmlTextReaderImpl_t3122949129 * get_impl_3() const { return ___impl_3; }
	inline XmlTextReaderImpl_t3122949129 ** get_address_of_impl_3() { return &___impl_3; }
	inline void set_impl_3(XmlTextReaderImpl_t3122949129 * value)
	{
		___impl_3 = value;
		Il2CppCodeGenWriteBarrier(&___impl_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
