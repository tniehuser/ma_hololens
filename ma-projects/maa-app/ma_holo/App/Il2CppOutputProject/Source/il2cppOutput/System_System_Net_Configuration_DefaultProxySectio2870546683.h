﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.DefaultProxySectionInternal
struct  DefaultProxySectionInternal_t2870546683  : public Il2CppObject
{
public:
	// System.Net.IWebProxy System.Net.Configuration.DefaultProxySectionInternal::webProxy
	Il2CppObject * ___webProxy_0;

public:
	inline static int32_t get_offset_of_webProxy_0() { return static_cast<int32_t>(offsetof(DefaultProxySectionInternal_t2870546683, ___webProxy_0)); }
	inline Il2CppObject * get_webProxy_0() const { return ___webProxy_0; }
	inline Il2CppObject ** get_address_of_webProxy_0() { return &___webProxy_0; }
	inline void set_webProxy_0(Il2CppObject * value)
	{
		___webProxy_0 = value;
		Il2CppCodeGenWriteBarrier(&___webProxy_0, value);
	}
};

struct DefaultProxySectionInternal_t2870546683_StaticFields
{
public:
	// System.Object System.Net.Configuration.DefaultProxySectionInternal::classSyncObject
	Il2CppObject * ___classSyncObject_1;

public:
	inline static int32_t get_offset_of_classSyncObject_1() { return static_cast<int32_t>(offsetof(DefaultProxySectionInternal_t2870546683_StaticFields, ___classSyncObject_1)); }
	inline Il2CppObject * get_classSyncObject_1() const { return ___classSyncObject_1; }
	inline Il2CppObject ** get_address_of_classSyncObject_1() { return &___classSyncObject_1; }
	inline void set_classSyncObject_1(Il2CppObject * value)
	{
		___classSyncObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___classSyncObject_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
