﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.WeakReference
struct WeakReference_t1077405567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.SharedReference
struct  SharedReference_t2137668360  : public Il2CppObject
{
public:
	// System.WeakReference System.Text.RegularExpressions.SharedReference::_ref
	WeakReference_t1077405567 * ____ref_0;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(SharedReference_t2137668360, ____ref_0)); }
	inline WeakReference_t1077405567 * get__ref_0() const { return ____ref_0; }
	inline WeakReference_t1077405567 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(WeakReference_t1077405567 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier(&____ref_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
