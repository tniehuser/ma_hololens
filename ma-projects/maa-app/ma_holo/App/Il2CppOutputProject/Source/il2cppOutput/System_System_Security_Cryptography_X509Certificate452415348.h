﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct  X500DistinguishedName_t452415348  : public AsnEncodedData_t463456204
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::name
	String_t* ___name_2;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X500DistinguishedName::canonEncoding
	ByteU5BU5D_t3397334013* ___canonEncoding_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t452415348, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_canonEncoding_3() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t452415348, ___canonEncoding_3)); }
	inline ByteU5BU5D_t3397334013* get_canonEncoding_3() const { return ___canonEncoding_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_canonEncoding_3() { return &___canonEncoding_3; }
	inline void set_canonEncoding_3(ByteU5BU5D_t3397334013* value)
	{
		___canonEncoding_3 = value;
		Il2CppCodeGenWriteBarrier(&___canonEncoding_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
