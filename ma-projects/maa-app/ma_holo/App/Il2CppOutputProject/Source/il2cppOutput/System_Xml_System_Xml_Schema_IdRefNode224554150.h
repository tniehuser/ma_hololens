﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.IdRefNode
struct  IdRefNode_t224554150  : public Il2CppObject
{
public:
	// System.String System.Xml.Schema.IdRefNode::Id
	String_t* ___Id_0;
	// System.Int32 System.Xml.Schema.IdRefNode::LineNo
	int32_t ___LineNo_1;
	// System.Int32 System.Xml.Schema.IdRefNode::LinePos
	int32_t ___LinePos_2;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.IdRefNode::Next
	IdRefNode_t224554150 * ___Next_3;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(IdRefNode_t224554150, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier(&___Id_0, value);
	}

	inline static int32_t get_offset_of_LineNo_1() { return static_cast<int32_t>(offsetof(IdRefNode_t224554150, ___LineNo_1)); }
	inline int32_t get_LineNo_1() const { return ___LineNo_1; }
	inline int32_t* get_address_of_LineNo_1() { return &___LineNo_1; }
	inline void set_LineNo_1(int32_t value)
	{
		___LineNo_1 = value;
	}

	inline static int32_t get_offset_of_LinePos_2() { return static_cast<int32_t>(offsetof(IdRefNode_t224554150, ___LinePos_2)); }
	inline int32_t get_LinePos_2() const { return ___LinePos_2; }
	inline int32_t* get_address_of_LinePos_2() { return &___LinePos_2; }
	inline void set_LinePos_2(int32_t value)
	{
		___LinePos_2 = value;
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(IdRefNode_t224554150, ___Next_3)); }
	inline IdRefNode_t224554150 * get_Next_3() const { return ___Next_3; }
	inline IdRefNode_t224554150 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(IdRefNode_t224554150 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier(&___Next_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
