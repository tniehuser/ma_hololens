﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_MS_Internal_Xml_XPath_XPathScanner_LexK1109665186.h"
#include "System_Xml_System_Xml_XmlCharType1050521405.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathScanner
struct  XPathScanner_t1765956737  : public Il2CppObject
{
public:
	// System.String MS.Internal.Xml.XPath.XPathScanner::xpathExpr
	String_t* ___xpathExpr_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathScanner::xpathExprIndex
	int32_t ___xpathExprIndex_1;
	// MS.Internal.Xml.XPath.XPathScanner/LexKind MS.Internal.Xml.XPath.XPathScanner::kind
	int32_t ___kind_2;
	// System.Char MS.Internal.Xml.XPath.XPathScanner::currentChar
	Il2CppChar ___currentChar_3;
	// System.String MS.Internal.Xml.XPath.XPathScanner::name
	String_t* ___name_4;
	// System.String MS.Internal.Xml.XPath.XPathScanner::prefix
	String_t* ___prefix_5;
	// System.String MS.Internal.Xml.XPath.XPathScanner::stringValue
	String_t* ___stringValue_6;
	// System.Double MS.Internal.Xml.XPath.XPathScanner::numberValue
	double ___numberValue_7;
	// System.Boolean MS.Internal.Xml.XPath.XPathScanner::canBeFunction
	bool ___canBeFunction_8;
	// System.Xml.XmlCharType MS.Internal.Xml.XPath.XPathScanner::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_9;

public:
	inline static int32_t get_offset_of_xpathExpr_0() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___xpathExpr_0)); }
	inline String_t* get_xpathExpr_0() const { return ___xpathExpr_0; }
	inline String_t** get_address_of_xpathExpr_0() { return &___xpathExpr_0; }
	inline void set_xpathExpr_0(String_t* value)
	{
		___xpathExpr_0 = value;
		Il2CppCodeGenWriteBarrier(&___xpathExpr_0, value);
	}

	inline static int32_t get_offset_of_xpathExprIndex_1() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___xpathExprIndex_1)); }
	inline int32_t get_xpathExprIndex_1() const { return ___xpathExprIndex_1; }
	inline int32_t* get_address_of_xpathExprIndex_1() { return &___xpathExprIndex_1; }
	inline void set_xpathExprIndex_1(int32_t value)
	{
		___xpathExprIndex_1 = value;
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_currentChar_3() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___currentChar_3)); }
	inline Il2CppChar get_currentChar_3() const { return ___currentChar_3; }
	inline Il2CppChar* get_address_of_currentChar_3() { return &___currentChar_3; }
	inline void set_currentChar_3(Il2CppChar value)
	{
		___currentChar_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_4, value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_5, value);
	}

	inline static int32_t get_offset_of_stringValue_6() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___stringValue_6)); }
	inline String_t* get_stringValue_6() const { return ___stringValue_6; }
	inline String_t** get_address_of_stringValue_6() { return &___stringValue_6; }
	inline void set_stringValue_6(String_t* value)
	{
		___stringValue_6 = value;
		Il2CppCodeGenWriteBarrier(&___stringValue_6, value);
	}

	inline static int32_t get_offset_of_numberValue_7() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___numberValue_7)); }
	inline double get_numberValue_7() const { return ___numberValue_7; }
	inline double* get_address_of_numberValue_7() { return &___numberValue_7; }
	inline void set_numberValue_7(double value)
	{
		___numberValue_7 = value;
	}

	inline static int32_t get_offset_of_canBeFunction_8() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___canBeFunction_8)); }
	inline bool get_canBeFunction_8() const { return ___canBeFunction_8; }
	inline bool* get_address_of_canBeFunction_8() { return &___canBeFunction_8; }
	inline void set_canBeFunction_8(bool value)
	{
		___canBeFunction_8 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_9() { return static_cast<int32_t>(offsetof(XPathScanner_t1765956737, ___xmlCharType_9)); }
	inline XmlCharType_t1050521405  get_xmlCharType_9() const { return ___xmlCharType_9; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_9() { return &___xmlCharType_9; }
	inline void set_xmlCharType_9(XmlCharType_t1050521405  value)
	{
		___xmlCharType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
