﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SignatureDescription
struct  SignatureDescription_t89145500  : public Il2CppObject
{
public:
	// System.String System.Security.Cryptography.SignatureDescription::_strKey
	String_t* ____strKey_0;
	// System.String System.Security.Cryptography.SignatureDescription::_strDigest
	String_t* ____strDigest_1;
	// System.String System.Security.Cryptography.SignatureDescription::_strFormatter
	String_t* ____strFormatter_2;
	// System.String System.Security.Cryptography.SignatureDescription::_strDeformatter
	String_t* ____strDeformatter_3;

public:
	inline static int32_t get_offset_of__strKey_0() { return static_cast<int32_t>(offsetof(SignatureDescription_t89145500, ____strKey_0)); }
	inline String_t* get__strKey_0() const { return ____strKey_0; }
	inline String_t** get_address_of__strKey_0() { return &____strKey_0; }
	inline void set__strKey_0(String_t* value)
	{
		____strKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____strKey_0, value);
	}

	inline static int32_t get_offset_of__strDigest_1() { return static_cast<int32_t>(offsetof(SignatureDescription_t89145500, ____strDigest_1)); }
	inline String_t* get__strDigest_1() const { return ____strDigest_1; }
	inline String_t** get_address_of__strDigest_1() { return &____strDigest_1; }
	inline void set__strDigest_1(String_t* value)
	{
		____strDigest_1 = value;
		Il2CppCodeGenWriteBarrier(&____strDigest_1, value);
	}

	inline static int32_t get_offset_of__strFormatter_2() { return static_cast<int32_t>(offsetof(SignatureDescription_t89145500, ____strFormatter_2)); }
	inline String_t* get__strFormatter_2() const { return ____strFormatter_2; }
	inline String_t** get_address_of__strFormatter_2() { return &____strFormatter_2; }
	inline void set__strFormatter_2(String_t* value)
	{
		____strFormatter_2 = value;
		Il2CppCodeGenWriteBarrier(&____strFormatter_2, value);
	}

	inline static int32_t get_offset_of__strDeformatter_3() { return static_cast<int32_t>(offsetof(SignatureDescription_t89145500, ____strDeformatter_3)); }
	inline String_t* get__strDeformatter_3() const { return ____strDeformatter_3; }
	inline String_t** get_address_of__strDeformatter_3() { return &____strDeformatter_3; }
	inline void set__strDeformatter_3(String_t* value)
	{
		____strDeformatter_3 = value;
		Il2CppCodeGenWriteBarrier(&____strDeformatter_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
