﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Net_Security_AuthenticatedStream1183414097.h"

// Mono.Security.Protocol.Tls.SslStreamBase
struct SslStreamBase_t934199321;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t3067886638;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t823784021;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream
struct  LegacySslStream_t83997747  : public AuthenticatedStream_t1183414097
{
public:
	// Mono.Security.Protocol.Tls.SslStreamBase Mono.Net.Security.Private.LegacySslStream::ssl_stream
	SslStreamBase_t934199321 * ___ssl_stream_10;
	// Mono.Security.Interface.ICertificateValidator Mono.Net.Security.Private.LegacySslStream::certificateValidator
	Il2CppObject * ___certificateValidator_11;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.LegacySslStream::provider
	MonoTlsProvider_t823784021 * ___provider_12;

public:
	inline static int32_t get_offset_of_ssl_stream_10() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___ssl_stream_10)); }
	inline SslStreamBase_t934199321 * get_ssl_stream_10() const { return ___ssl_stream_10; }
	inline SslStreamBase_t934199321 ** get_address_of_ssl_stream_10() { return &___ssl_stream_10; }
	inline void set_ssl_stream_10(SslStreamBase_t934199321 * value)
	{
		___ssl_stream_10 = value;
		Il2CppCodeGenWriteBarrier(&___ssl_stream_10, value);
	}

	inline static int32_t get_offset_of_certificateValidator_11() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___certificateValidator_11)); }
	inline Il2CppObject * get_certificateValidator_11() const { return ___certificateValidator_11; }
	inline Il2CppObject ** get_address_of_certificateValidator_11() { return &___certificateValidator_11; }
	inline void set_certificateValidator_11(Il2CppObject * value)
	{
		___certificateValidator_11 = value;
		Il2CppCodeGenWriteBarrier(&___certificateValidator_11, value);
	}

	inline static int32_t get_offset_of_provider_12() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___provider_12)); }
	inline MonoTlsProvider_t823784021 * get_provider_12() const { return ___provider_12; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_12() { return &___provider_12; }
	inline void set_provider_12(MonoTlsProvider_t823784021 * value)
	{
		___provider_12 = value;
		Il2CppCodeGenWriteBarrier(&___provider_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
