﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"

// System.String
struct String_t;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t2118142256;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDocumentation
struct  XmlSchemaDocumentation_t3832803992  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaDocumentation::source
	String_t* ___source_6;
	// System.String System.Xml.Schema.XmlSchemaDocumentation::language
	String_t* ___language_7;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaDocumentation::markup
	XmlNodeU5BU5D_t2118142256* ___markup_8;

public:
	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___source_6)); }
	inline String_t* get_source_6() const { return ___source_6; }
	inline String_t** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(String_t* value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier(&___source_6, value);
	}

	inline static int32_t get_offset_of_language_7() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___language_7)); }
	inline String_t* get_language_7() const { return ___language_7; }
	inline String_t** get_address_of_language_7() { return &___language_7; }
	inline void set_language_7(String_t* value)
	{
		___language_7 = value;
		Il2CppCodeGenWriteBarrier(&___language_7, value);
	}

	inline static int32_t get_offset_of_markup_8() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___markup_8)); }
	inline XmlNodeU5BU5D_t2118142256* get_markup_8() const { return ___markup_8; }
	inline XmlNodeU5BU5D_t2118142256** get_address_of_markup_8() { return &___markup_8; }
	inline void set_markup_8(XmlNodeU5BU5D_t2118142256* value)
	{
		___markup_8 = value;
		Il2CppCodeGenWriteBarrier(&___markup_8, value);
	}
};

struct XmlSchemaDocumentation_t3832803992_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaDocumentation::languageType
	XmlSchemaSimpleType_t248156492 * ___languageType_9;

public:
	inline static int32_t get_offset_of_languageType_9() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992_StaticFields, ___languageType_9)); }
	inline XmlSchemaSimpleType_t248156492 * get_languageType_9() const { return ___languageType_9; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_languageType_9() { return &___languageType_9; }
	inline void set_languageType_9(XmlSchemaSimpleType_t248156492 * value)
	{
		___languageType_9 = value;
		Il2CppCodeGenWriteBarrier(&___languageType_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
