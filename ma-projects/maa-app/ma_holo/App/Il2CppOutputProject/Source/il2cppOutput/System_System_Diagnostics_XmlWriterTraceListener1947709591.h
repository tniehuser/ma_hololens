﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Diagnostics_TextWriterTraceListener3365973051.h"

// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Xml.XmlTextWriter
struct XmlTextWriter_t2527250655;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.XmlWriterTraceListener
struct  XmlWriterTraceListener_t1947709591  : public TextWriterTraceListener_t3365973051
{
public:
	// System.Text.StringBuilder System.Diagnostics.XmlWriterTraceListener::strBldr
	StringBuilder_t1221177846 * ___strBldr_10;
	// System.Xml.XmlTextWriter System.Diagnostics.XmlWriterTraceListener::xmlBlobWriter
	XmlTextWriter_t2527250655 * ___xmlBlobWriter_11;
	// System.Boolean System.Diagnostics.XmlWriterTraceListener::shouldRespectFilterOnTraceTransfer
	bool ___shouldRespectFilterOnTraceTransfer_12;

public:
	inline static int32_t get_offset_of_strBldr_10() { return static_cast<int32_t>(offsetof(XmlWriterTraceListener_t1947709591, ___strBldr_10)); }
	inline StringBuilder_t1221177846 * get_strBldr_10() const { return ___strBldr_10; }
	inline StringBuilder_t1221177846 ** get_address_of_strBldr_10() { return &___strBldr_10; }
	inline void set_strBldr_10(StringBuilder_t1221177846 * value)
	{
		___strBldr_10 = value;
		Il2CppCodeGenWriteBarrier(&___strBldr_10, value);
	}

	inline static int32_t get_offset_of_xmlBlobWriter_11() { return static_cast<int32_t>(offsetof(XmlWriterTraceListener_t1947709591, ___xmlBlobWriter_11)); }
	inline XmlTextWriter_t2527250655 * get_xmlBlobWriter_11() const { return ___xmlBlobWriter_11; }
	inline XmlTextWriter_t2527250655 ** get_address_of_xmlBlobWriter_11() { return &___xmlBlobWriter_11; }
	inline void set_xmlBlobWriter_11(XmlTextWriter_t2527250655 * value)
	{
		___xmlBlobWriter_11 = value;
		Il2CppCodeGenWriteBarrier(&___xmlBlobWriter_11, value);
	}

	inline static int32_t get_offset_of_shouldRespectFilterOnTraceTransfer_12() { return static_cast<int32_t>(offsetof(XmlWriterTraceListener_t1947709591, ___shouldRespectFilterOnTraceTransfer_12)); }
	inline bool get_shouldRespectFilterOnTraceTransfer_12() const { return ___shouldRespectFilterOnTraceTransfer_12; }
	inline bool* get_address_of_shouldRespectFilterOnTraceTransfer_12() { return &___shouldRespectFilterOnTraceTransfer_12; }
	inline void set_shouldRespectFilterOnTraceTransfer_12(bool value)
	{
		___shouldRespectFilterOnTraceTransfer_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
