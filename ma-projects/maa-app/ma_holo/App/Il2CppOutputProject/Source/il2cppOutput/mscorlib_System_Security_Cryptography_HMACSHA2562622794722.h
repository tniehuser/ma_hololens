﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_HMAC130461695.h"

// System.Func`1<System.Security.Cryptography.HashAlgorithm>
struct Func_1_t284361645;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HMACSHA256
struct  HMACSHA256_t2622794722  : public HMAC_t130461695
{
public:

public:
};

struct HMACSHA256_t2622794722_StaticFields
{
public:
	// System.Func`1<System.Security.Cryptography.HashAlgorithm> System.Security.Cryptography.HMACSHA256::<>f__am$cache0
	Func_1_t284361645 * ___U3CU3Ef__amU24cache0_12;
	// System.Func`1<System.Security.Cryptography.HashAlgorithm> System.Security.Cryptography.HMACSHA256::<>f__am$cache1
	Func_1_t284361645 * ___U3CU3Ef__amU24cache1_13;
	// System.Func`1<System.Security.Cryptography.HashAlgorithm> System.Security.Cryptography.HMACSHA256::<>f__am$cache2
	Func_1_t284361645 * ___U3CU3Ef__amU24cache2_14;
	// System.Func`1<System.Security.Cryptography.HashAlgorithm> System.Security.Cryptography.HMACSHA256::<>f__am$cache3
	Func_1_t284361645 * ___U3CU3Ef__amU24cache3_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(HMACSHA256_t2622794722_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Func_1_t284361645 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Func_1_t284361645 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Func_1_t284361645 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(HMACSHA256_t2622794722_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Func_1_t284361645 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Func_1_t284361645 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Func_1_t284361645 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_14() { return static_cast<int32_t>(offsetof(HMACSHA256_t2622794722_StaticFields, ___U3CU3Ef__amU24cache2_14)); }
	inline Func_1_t284361645 * get_U3CU3Ef__amU24cache2_14() const { return ___U3CU3Ef__amU24cache2_14; }
	inline Func_1_t284361645 ** get_address_of_U3CU3Ef__amU24cache2_14() { return &___U3CU3Ef__amU24cache2_14; }
	inline void set_U3CU3Ef__amU24cache2_14(Func_1_t284361645 * value)
	{
		___U3CU3Ef__amU24cache2_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_15() { return static_cast<int32_t>(offsetof(HMACSHA256_t2622794722_StaticFields, ___U3CU3Ef__amU24cache3_15)); }
	inline Func_1_t284361645 * get_U3CU3Ef__amU24cache3_15() const { return ___U3CU3Ef__amU24cache3_15; }
	inline Func_1_t284361645 ** get_address_of_U3CU3Ef__amU24cache3_15() { return &___U3CU3Ef__amU24cache3_15; }
	inline void set_U3CU3Ef__amU24cache3_15(Func_1_t284361645 * value)
	{
		___U3CU3Ef__amU24cache3_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
