﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Int322071877448.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SpinLock
struct  SpinLock_t2136334209 
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SpinLock::m_owner
	int32_t ___m_owner_0;

public:
	inline static int32_t get_offset_of_m_owner_0() { return static_cast<int32_t>(offsetof(SpinLock_t2136334209, ___m_owner_0)); }
	inline int32_t get_m_owner_0() const { return ___m_owner_0; }
	inline int32_t* get_address_of_m_owner_0() { return &___m_owner_0; }
	inline void set_m_owner_0(int32_t value)
	{
		___m_owner_0 = value;
	}
};

struct SpinLock_t2136334209_StaticFields
{
public:
	// System.Int32 System.Threading.SpinLock::MAXIMUM_WAITERS
	int32_t ___MAXIMUM_WAITERS_1;

public:
	inline static int32_t get_offset_of_MAXIMUM_WAITERS_1() { return static_cast<int32_t>(offsetof(SpinLock_t2136334209_StaticFields, ___MAXIMUM_WAITERS_1)); }
	inline int32_t get_MAXIMUM_WAITERS_1() const { return ___MAXIMUM_WAITERS_1; }
	inline int32_t* get_address_of_MAXIMUM_WAITERS_1() { return &___MAXIMUM_WAITERS_1; }
	inline void set_MAXIMUM_WAITERS_1(int32_t value)
	{
		___MAXIMUM_WAITERS_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.SpinLock
struct SpinLock_t2136334209_marshaled_pinvoke
{
	int32_t ___m_owner_0;
};
// Native definition for COM marshalling of System.Threading.SpinLock
struct SpinLock_t2136334209_marshaled_com
{
	int32_t ___m_owner_0;
};
