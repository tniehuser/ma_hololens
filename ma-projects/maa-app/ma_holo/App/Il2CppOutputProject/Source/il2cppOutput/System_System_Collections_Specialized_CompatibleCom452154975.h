﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t1980576455;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.CompatibleComparer
struct  CompatibleComparer_t452154975  : public Il2CppObject
{
public:
	// System.Collections.IComparer System.Collections.Specialized.CompatibleComparer::_comparer
	Il2CppObject * ____comparer_0;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.CompatibleComparer::_hcp
	Il2CppObject * ____hcp_2;

public:
	inline static int32_t get_offset_of__comparer_0() { return static_cast<int32_t>(offsetof(CompatibleComparer_t452154975, ____comparer_0)); }
	inline Il2CppObject * get__comparer_0() const { return ____comparer_0; }
	inline Il2CppObject ** get_address_of__comparer_0() { return &____comparer_0; }
	inline void set__comparer_0(Il2CppObject * value)
	{
		____comparer_0 = value;
		Il2CppCodeGenWriteBarrier(&____comparer_0, value);
	}

	inline static int32_t get_offset_of__hcp_2() { return static_cast<int32_t>(offsetof(CompatibleComparer_t452154975, ____hcp_2)); }
	inline Il2CppObject * get__hcp_2() const { return ____hcp_2; }
	inline Il2CppObject ** get_address_of__hcp_2() { return &____hcp_2; }
	inline void set__hcp_2(Il2CppObject * value)
	{
		____hcp_2 = value;
		Il2CppCodeGenWriteBarrier(&____hcp_2, value);
	}
};

struct CompatibleComparer_t452154975_StaticFields
{
public:
	// System.Collections.IComparer modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.CompatibleComparer::defaultComparer
	Il2CppObject * ___defaultComparer_1;
	// System.Collections.IHashCodeProvider modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.CompatibleComparer::defaultHashProvider
	Il2CppObject * ___defaultHashProvider_3;

public:
	inline static int32_t get_offset_of_defaultComparer_1() { return static_cast<int32_t>(offsetof(CompatibleComparer_t452154975_StaticFields, ___defaultComparer_1)); }
	inline Il2CppObject * get_defaultComparer_1() const { return ___defaultComparer_1; }
	inline Il2CppObject ** get_address_of_defaultComparer_1() { return &___defaultComparer_1; }
	inline void set_defaultComparer_1(Il2CppObject * value)
	{
		___defaultComparer_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultComparer_1, value);
	}

	inline static int32_t get_offset_of_defaultHashProvider_3() { return static_cast<int32_t>(offsetof(CompatibleComparer_t452154975_StaticFields, ___defaultHashProvider_3)); }
	inline Il2CppObject * get_defaultHashProvider_3() const { return ___defaultHashProvider_3; }
	inline Il2CppObject ** get_address_of_defaultHashProvider_3() { return &___defaultHashProvider_3; }
	inline void set_defaultHashProvider_3(Il2CppObject * value)
	{
		___defaultHashProvider_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHashProvider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
