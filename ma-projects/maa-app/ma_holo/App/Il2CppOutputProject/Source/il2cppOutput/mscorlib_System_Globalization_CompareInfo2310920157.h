﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// Mono.Globalization.Unicode.SimpleCollator
struct SimpleCollator_t4081201584;
// System.Collections.Generic.Dictionary`2<System.String,Mono.Globalization.Unicode.SimpleCollator>
struct Dictionary_2_t1701013550;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CompareInfo
struct  CompareInfo_t2310920157  : public Il2CppObject
{
public:
	// System.String System.Globalization.CompareInfo::m_name
	String_t* ___m_name_0;
	// System.String System.Globalization.CompareInfo::m_sortName
	String_t* ___m_sortName_1;
	// System.Int32 System.Globalization.CompareInfo::culture
	int32_t ___culture_2;
	// Mono.Globalization.Unicode.SimpleCollator System.Globalization.CompareInfo::collator
	SimpleCollator_t4081201584 * ___collator_3;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_0, value);
	}

	inline static int32_t get_offset_of_m_sortName_1() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157, ___m_sortName_1)); }
	inline String_t* get_m_sortName_1() const { return ___m_sortName_1; }
	inline String_t** get_address_of_m_sortName_1() { return &___m_sortName_1; }
	inline void set_m_sortName_1(String_t* value)
	{
		___m_sortName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_sortName_1, value);
	}

	inline static int32_t get_offset_of_culture_2() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157, ___culture_2)); }
	inline int32_t get_culture_2() const { return ___culture_2; }
	inline int32_t* get_address_of_culture_2() { return &___culture_2; }
	inline void set_culture_2(int32_t value)
	{
		___culture_2 = value;
	}

	inline static int32_t get_offset_of_collator_3() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157, ___collator_3)); }
	inline SimpleCollator_t4081201584 * get_collator_3() const { return ___collator_3; }
	inline SimpleCollator_t4081201584 ** get_address_of_collator_3() { return &___collator_3; }
	inline void set_collator_3(SimpleCollator_t4081201584 * value)
	{
		___collator_3 = value;
		Il2CppCodeGenWriteBarrier(&___collator_3, value);
	}
};

struct CompareInfo_t2310920157_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Mono.Globalization.Unicode.SimpleCollator> System.Globalization.CompareInfo::collators
	Dictionary_2_t1701013550 * ___collators_4;
	// System.Boolean System.Globalization.CompareInfo::managedCollation
	bool ___managedCollation_5;
	// System.Boolean System.Globalization.CompareInfo::managedCollationChecked
	bool ___managedCollationChecked_6;

public:
	inline static int32_t get_offset_of_collators_4() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157_StaticFields, ___collators_4)); }
	inline Dictionary_2_t1701013550 * get_collators_4() const { return ___collators_4; }
	inline Dictionary_2_t1701013550 ** get_address_of_collators_4() { return &___collators_4; }
	inline void set_collators_4(Dictionary_2_t1701013550 * value)
	{
		___collators_4 = value;
		Il2CppCodeGenWriteBarrier(&___collators_4, value);
	}

	inline static int32_t get_offset_of_managedCollation_5() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157_StaticFields, ___managedCollation_5)); }
	inline bool get_managedCollation_5() const { return ___managedCollation_5; }
	inline bool* get_address_of_managedCollation_5() { return &___managedCollation_5; }
	inline void set_managedCollation_5(bool value)
	{
		___managedCollation_5 = value;
	}

	inline static int32_t get_offset_of_managedCollationChecked_6() { return static_cast<int32_t>(offsetof(CompareInfo_t2310920157_StaticFields, ___managedCollationChecked_6)); }
	inline bool get_managedCollationChecked_6() const { return ___managedCollationChecked_6; }
	inline bool* get_address_of_managedCollationChecked_6() { return &___managedCollationChecked_6; }
	inline void set_managedCollationChecked_6(bool value)
	{
		___managedCollationChecked_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
