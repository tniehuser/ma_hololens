﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Net.SimpleAsyncCallback
struct SimpleAsyncCallback_t3151114241;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1927440687;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SimpleAsyncResult
struct  SimpleAsyncResult_t2937691397  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent System.Net.SimpleAsyncResult::handle
	ManualResetEvent_t926074657 * ___handle_0;
	// System.Boolean System.Net.SimpleAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.SimpleAsyncResult::isCompleted
	bool ___isCompleted_2;
	// System.Net.SimpleAsyncCallback System.Net.SimpleAsyncResult::cb
	SimpleAsyncCallback_t3151114241 * ___cb_3;
	// System.Object System.Net.SimpleAsyncResult::state
	Il2CppObject * ___state_4;
	// System.Boolean System.Net.SimpleAsyncResult::callbackDone
	bool ___callbackDone_5;
	// System.Exception System.Net.SimpleAsyncResult::exc
	Exception_t1927440687 * ___exc_6;
	// System.Object System.Net.SimpleAsyncResult::locker
	Il2CppObject * ___locker_7;
	// System.Nullable`1<System.Boolean> System.Net.SimpleAsyncResult::user_read_synch
	Nullable_1_t2088641033  ___user_read_synch_8;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___handle_0)); }
	inline ManualResetEvent_t926074657 * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_t926074657 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_t926074657 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier(&___handle_0, value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___cb_3)); }
	inline SimpleAsyncCallback_t3151114241 * get_cb_3() const { return ___cb_3; }
	inline SimpleAsyncCallback_t3151114241 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(SimpleAsyncCallback_t3151114241 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier(&___cb_3, value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___state_4)); }
	inline Il2CppObject * get_state_4() const { return ___state_4; }
	inline Il2CppObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(Il2CppObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier(&___state_4, value);
	}

	inline static int32_t get_offset_of_callbackDone_5() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___callbackDone_5)); }
	inline bool get_callbackDone_5() const { return ___callbackDone_5; }
	inline bool* get_address_of_callbackDone_5() { return &___callbackDone_5; }
	inline void set_callbackDone_5(bool value)
	{
		___callbackDone_5 = value;
	}

	inline static int32_t get_offset_of_exc_6() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___exc_6)); }
	inline Exception_t1927440687 * get_exc_6() const { return ___exc_6; }
	inline Exception_t1927440687 ** get_address_of_exc_6() { return &___exc_6; }
	inline void set_exc_6(Exception_t1927440687 * value)
	{
		___exc_6 = value;
		Il2CppCodeGenWriteBarrier(&___exc_6, value);
	}

	inline static int32_t get_offset_of_locker_7() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___locker_7)); }
	inline Il2CppObject * get_locker_7() const { return ___locker_7; }
	inline Il2CppObject ** get_address_of_locker_7() { return &___locker_7; }
	inline void set_locker_7(Il2CppObject * value)
	{
		___locker_7 = value;
		Il2CppCodeGenWriteBarrier(&___locker_7, value);
	}

	inline static int32_t get_offset_of_user_read_synch_8() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t2937691397, ___user_read_synch_8)); }
	inline Nullable_1_t2088641033  get_user_read_synch_8() const { return ___user_read_synch_8; }
	inline Nullable_1_t2088641033 * get_address_of_user_read_synch_8() { return &___user_read_synch_8; }
	inline void set_user_read_synch_8(Nullable_1_t2088641033  value)
	{
		___user_read_synch_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
