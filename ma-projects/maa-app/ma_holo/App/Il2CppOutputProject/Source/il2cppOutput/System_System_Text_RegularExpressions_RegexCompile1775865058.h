﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCompiler/BacktrackNote
struct  BacktrackNote_t1775865058  : public Il2CppObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_codepos
	int32_t ____codepos_0;
	// System.Int32 System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_flags
	int32_t ____flags_1;
	// System.Reflection.Emit.Label System.Text.RegularExpressions.RegexCompiler/BacktrackNote::_label
	Label_t4243202660  ____label_2;

public:
	inline static int32_t get_offset_of__codepos_0() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____codepos_0)); }
	inline int32_t get__codepos_0() const { return ____codepos_0; }
	inline int32_t* get_address_of__codepos_0() { return &____codepos_0; }
	inline void set__codepos_0(int32_t value)
	{
		____codepos_0 = value;
	}

	inline static int32_t get_offset_of__flags_1() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____flags_1)); }
	inline int32_t get__flags_1() const { return ____flags_1; }
	inline int32_t* get_address_of__flags_1() { return &____flags_1; }
	inline void set__flags_1(int32_t value)
	{
		____flags_1 = value;
	}

	inline static int32_t get_offset_of__label_2() { return static_cast<int32_t>(offsetof(BacktrackNote_t1775865058, ____label_2)); }
	inline Label_t4243202660  get__label_2() const { return ____label_2; }
	inline Label_t4243202660 * get_address_of__label_2() { return &____label_2; }
	inline void set__label_2(Label_t4243202660  value)
	{
		____label_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
