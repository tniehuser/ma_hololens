﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Runtime.Serialization.SerializationEventHandler
struct SerializationEventHandler_t2339848500;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationObjectManager
struct  SerializationObjectManager_t4052555190  : public Il2CppObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationObjectManager::m_objectSeenTable
	Hashtable_t909839986 * ___m_objectSeenTable_0;
	// System.Runtime.Serialization.SerializationEventHandler System.Runtime.Serialization.SerializationObjectManager::m_onSerializedHandler
	SerializationEventHandler_t2339848500 * ___m_onSerializedHandler_1;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.SerializationObjectManager::m_context
	StreamingContext_t1417235061  ___m_context_2;

public:
	inline static int32_t get_offset_of_m_objectSeenTable_0() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4052555190, ___m_objectSeenTable_0)); }
	inline Hashtable_t909839986 * get_m_objectSeenTable_0() const { return ___m_objectSeenTable_0; }
	inline Hashtable_t909839986 ** get_address_of_m_objectSeenTable_0() { return &___m_objectSeenTable_0; }
	inline void set_m_objectSeenTable_0(Hashtable_t909839986 * value)
	{
		___m_objectSeenTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_objectSeenTable_0, value);
	}

	inline static int32_t get_offset_of_m_onSerializedHandler_1() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4052555190, ___m_onSerializedHandler_1)); }
	inline SerializationEventHandler_t2339848500 * get_m_onSerializedHandler_1() const { return ___m_onSerializedHandler_1; }
	inline SerializationEventHandler_t2339848500 ** get_address_of_m_onSerializedHandler_1() { return &___m_onSerializedHandler_1; }
	inline void set_m_onSerializedHandler_1(SerializationEventHandler_t2339848500 * value)
	{
		___m_onSerializedHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onSerializedHandler_1, value);
	}

	inline static int32_t get_offset_of_m_context_2() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4052555190, ___m_context_2)); }
	inline StreamingContext_t1417235061  get_m_context_2() const { return ___m_context_2; }
	inline StreamingContext_t1417235061 * get_address_of_m_context_2() { return &___m_context_2; }
	inline void set_m_context_2(StreamingContext_t1417235061  value)
	{
		___m_context_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
