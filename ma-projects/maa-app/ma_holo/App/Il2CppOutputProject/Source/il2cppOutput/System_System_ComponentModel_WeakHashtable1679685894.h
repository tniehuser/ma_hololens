﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakHashtable
struct  WeakHashtable_t1679685894  : public Hashtable_t909839986
{
public:

public:
};

struct WeakHashtable_t1679685894_StaticFields
{
public:
	// System.Collections.IEqualityComparer System.ComponentModel.WeakHashtable::_comparer
	Il2CppObject * ____comparer_12;

public:
	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(WeakHashtable_t1679685894_StaticFields, ____comparer_12)); }
	inline Il2CppObject * get__comparer_12() const { return ____comparer_12; }
	inline Il2CppObject ** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(Il2CppObject * value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier(&____comparer_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
