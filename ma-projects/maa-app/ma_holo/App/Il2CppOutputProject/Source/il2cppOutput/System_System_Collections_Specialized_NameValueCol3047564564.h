﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"

// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t3047564564  : public NameObjectCollectionBase_t2034248631
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::_all
	StringU5BU5D_t1642385972* ____all_11;
	// System.String[] System.Collections.Specialized.NameValueCollection::_allKeys
	StringU5BU5D_t1642385972* ____allKeys_12;

public:
	inline static int32_t get_offset_of__all_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ____all_11)); }
	inline StringU5BU5D_t1642385972* get__all_11() const { return ____all_11; }
	inline StringU5BU5D_t1642385972** get_address_of__all_11() { return &____all_11; }
	inline void set__all_11(StringU5BU5D_t1642385972* value)
	{
		____all_11 = value;
		Il2CppCodeGenWriteBarrier(&____all_11, value);
	}

	inline static int32_t get_offset_of__allKeys_12() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ____allKeys_12)); }
	inline StringU5BU5D_t1642385972* get__allKeys_12() const { return ____allKeys_12; }
	inline StringU5BU5D_t1642385972** get_address_of__allKeys_12() { return &____allKeys_12; }
	inline void set__allKeys_12(StringU5BU5D_t1642385972* value)
	{
		____allKeys_12 = value;
		Il2CppCodeGenWriteBarrier(&____allKeys_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
