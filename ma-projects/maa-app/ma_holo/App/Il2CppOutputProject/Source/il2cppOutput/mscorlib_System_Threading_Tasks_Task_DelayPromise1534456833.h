﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Threading_Tasks_Task_1_gen2445339805.h"
#include "mscorlib_System_Threading_CancellationToken1851405782.h"
#include "mscorlib_System_Threading_CancellationTokenRegistr1708859357.h"

// System.Threading.Timer
struct Timer_t791717973;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.Task/DelayPromise
struct  DelayPromise_t1534456833  : public Task_1_t2445339805
{
public:
	// System.Threading.CancellationToken System.Threading.Tasks.Task/DelayPromise::Token
	CancellationToken_t1851405782  ___Token_27;
	// System.Threading.CancellationTokenRegistration System.Threading.Tasks.Task/DelayPromise::Registration
	CancellationTokenRegistration_t1708859357  ___Registration_28;
	// System.Threading.Timer System.Threading.Tasks.Task/DelayPromise::Timer
	Timer_t791717973 * ___Timer_29;

public:
	inline static int32_t get_offset_of_Token_27() { return static_cast<int32_t>(offsetof(DelayPromise_t1534456833, ___Token_27)); }
	inline CancellationToken_t1851405782  get_Token_27() const { return ___Token_27; }
	inline CancellationToken_t1851405782 * get_address_of_Token_27() { return &___Token_27; }
	inline void set_Token_27(CancellationToken_t1851405782  value)
	{
		___Token_27 = value;
	}

	inline static int32_t get_offset_of_Registration_28() { return static_cast<int32_t>(offsetof(DelayPromise_t1534456833, ___Registration_28)); }
	inline CancellationTokenRegistration_t1708859357  get_Registration_28() const { return ___Registration_28; }
	inline CancellationTokenRegistration_t1708859357 * get_address_of_Registration_28() { return &___Registration_28; }
	inline void set_Registration_28(CancellationTokenRegistration_t1708859357  value)
	{
		___Registration_28 = value;
	}

	inline static int32_t get_offset_of_Timer_29() { return static_cast<int32_t>(offsetof(DelayPromise_t1534456833, ___Timer_29)); }
	inline Timer_t791717973 * get_Timer_29() const { return ___Timer_29; }
	inline Timer_t791717973 ** get_address_of_Timer_29() { return &___Timer_29; }
	inline void set_Timer_29(Timer_t791717973 * value)
	{
		___Timer_29 = value;
		Il2CppCodeGenWriteBarrier(&___Timer_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
