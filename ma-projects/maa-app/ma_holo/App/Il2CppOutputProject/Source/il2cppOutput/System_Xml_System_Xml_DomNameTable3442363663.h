﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlName[]
struct XmlNameU5BU5D_t4082579409;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DomNameTable
struct  DomNameTable_t3442363663  : public Il2CppObject
{
public:
	// System.Xml.XmlName[] System.Xml.DomNameTable::entries
	XmlNameU5BU5D_t4082579409* ___entries_0;
	// System.Int32 System.Xml.DomNameTable::count
	int32_t ___count_1;
	// System.Int32 System.Xml.DomNameTable::mask
	int32_t ___mask_2;
	// System.Xml.XmlDocument System.Xml.DomNameTable::ownerDocument
	XmlDocument_t3649534162 * ___ownerDocument_3;
	// System.Xml.XmlNameTable System.Xml.DomNameTable::nameTable
	XmlNameTable_t1345805268 * ___nameTable_4;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(DomNameTable_t3442363663, ___entries_0)); }
	inline XmlNameU5BU5D_t4082579409* get_entries_0() const { return ___entries_0; }
	inline XmlNameU5BU5D_t4082579409** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(XmlNameU5BU5D_t4082579409* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(DomNameTable_t3442363663, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(DomNameTable_t3442363663, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_ownerDocument_3() { return static_cast<int32_t>(offsetof(DomNameTable_t3442363663, ___ownerDocument_3)); }
	inline XmlDocument_t3649534162 * get_ownerDocument_3() const { return ___ownerDocument_3; }
	inline XmlDocument_t3649534162 ** get_address_of_ownerDocument_3() { return &___ownerDocument_3; }
	inline void set_ownerDocument_3(XmlDocument_t3649534162 * value)
	{
		___ownerDocument_3 = value;
		Il2CppCodeGenWriteBarrier(&___ownerDocument_3, value);
	}

	inline static int32_t get_offset_of_nameTable_4() { return static_cast<int32_t>(offsetof(DomNameTable_t3442363663, ___nameTable_4)); }
	inline XmlNameTable_t1345805268 * get_nameTable_4() const { return ___nameTable_4; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_4() { return &___nameTable_4; }
	inline void set_nameTable_4(XmlNameTable_t1345805268 * value)
	{
		___nameTable_4 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
