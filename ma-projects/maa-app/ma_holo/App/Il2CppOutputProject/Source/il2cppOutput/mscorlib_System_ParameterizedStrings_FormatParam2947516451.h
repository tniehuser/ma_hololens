﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParameterizedStrings/FormatParam
struct  FormatParam_t2947516451 
{
public:
	// System.Int32 System.ParameterizedStrings/FormatParam::_int32
	int32_t ____int32_0;
	// System.String System.ParameterizedStrings/FormatParam::_string
	String_t* ____string_1;

public:
	inline static int32_t get_offset_of__int32_0() { return static_cast<int32_t>(offsetof(FormatParam_t2947516451, ____int32_0)); }
	inline int32_t get__int32_0() const { return ____int32_0; }
	inline int32_t* get_address_of__int32_0() { return &____int32_0; }
	inline void set__int32_0(int32_t value)
	{
		____int32_0 = value;
	}

	inline static int32_t get_offset_of__string_1() { return static_cast<int32_t>(offsetof(FormatParam_t2947516451, ____string_1)); }
	inline String_t* get__string_1() const { return ____string_1; }
	inline String_t** get_address_of__string_1() { return &____string_1; }
	inline void set__string_1(String_t* value)
	{
		____string_1 = value;
		Il2CppCodeGenWriteBarrier(&____string_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ParameterizedStrings/FormatParam
struct FormatParam_t2947516451_marshaled_pinvoke
{
	int32_t ____int32_0;
	char* ____string_1;
};
// Native definition for COM marshalling of System.ParameterizedStrings/FormatParam
struct FormatParam_t2947516451_marshaled_com
{
	int32_t ____int32_0;
	Il2CppChar* ____string_1;
};
