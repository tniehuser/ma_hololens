﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"
#include "System_System_Diagnostics_TraceOptions4183547961.h"

// System.String
struct String_t;
// System.Diagnostics.TraceFilter
struct TraceFilter_t87508953;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListener
struct  TraceListener_t3414949279  : public MarshalByRefObject_t1285298191
{
public:
	// System.Int32 System.Diagnostics.TraceListener::indentLevel
	int32_t ___indentLevel_1;
	// System.Int32 System.Diagnostics.TraceListener::indentSize
	int32_t ___indentSize_2;
	// System.Diagnostics.TraceOptions System.Diagnostics.TraceListener::traceOptions
	int32_t ___traceOptions_3;
	// System.Boolean System.Diagnostics.TraceListener::needIndent
	bool ___needIndent_4;
	// System.String System.Diagnostics.TraceListener::listenerName
	String_t* ___listenerName_5;
	// System.Diagnostics.TraceFilter System.Diagnostics.TraceListener::filter
	TraceFilter_t87508953 * ___filter_6;
	// System.Collections.Specialized.StringDictionary System.Diagnostics.TraceListener::attributes
	StringDictionary_t1070889667 * ___attributes_7;
	// System.String System.Diagnostics.TraceListener::initializeData
	String_t* ___initializeData_8;

public:
	inline static int32_t get_offset_of_indentLevel_1() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___indentLevel_1)); }
	inline int32_t get_indentLevel_1() const { return ___indentLevel_1; }
	inline int32_t* get_address_of_indentLevel_1() { return &___indentLevel_1; }
	inline void set_indentLevel_1(int32_t value)
	{
		___indentLevel_1 = value;
	}

	inline static int32_t get_offset_of_indentSize_2() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___indentSize_2)); }
	inline int32_t get_indentSize_2() const { return ___indentSize_2; }
	inline int32_t* get_address_of_indentSize_2() { return &___indentSize_2; }
	inline void set_indentSize_2(int32_t value)
	{
		___indentSize_2 = value;
	}

	inline static int32_t get_offset_of_traceOptions_3() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___traceOptions_3)); }
	inline int32_t get_traceOptions_3() const { return ___traceOptions_3; }
	inline int32_t* get_address_of_traceOptions_3() { return &___traceOptions_3; }
	inline void set_traceOptions_3(int32_t value)
	{
		___traceOptions_3 = value;
	}

	inline static int32_t get_offset_of_needIndent_4() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___needIndent_4)); }
	inline bool get_needIndent_4() const { return ___needIndent_4; }
	inline bool* get_address_of_needIndent_4() { return &___needIndent_4; }
	inline void set_needIndent_4(bool value)
	{
		___needIndent_4 = value;
	}

	inline static int32_t get_offset_of_listenerName_5() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___listenerName_5)); }
	inline String_t* get_listenerName_5() const { return ___listenerName_5; }
	inline String_t** get_address_of_listenerName_5() { return &___listenerName_5; }
	inline void set_listenerName_5(String_t* value)
	{
		___listenerName_5 = value;
		Il2CppCodeGenWriteBarrier(&___listenerName_5, value);
	}

	inline static int32_t get_offset_of_filter_6() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___filter_6)); }
	inline TraceFilter_t87508953 * get_filter_6() const { return ___filter_6; }
	inline TraceFilter_t87508953 ** get_address_of_filter_6() { return &___filter_6; }
	inline void set_filter_6(TraceFilter_t87508953 * value)
	{
		___filter_6 = value;
		Il2CppCodeGenWriteBarrier(&___filter_6, value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___attributes_7)); }
	inline StringDictionary_t1070889667 * get_attributes_7() const { return ___attributes_7; }
	inline StringDictionary_t1070889667 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(StringDictionary_t1070889667 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_7, value);
	}

	inline static int32_t get_offset_of_initializeData_8() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___initializeData_8)); }
	inline String_t* get_initializeData_8() const { return ___initializeData_8; }
	inline String_t** get_address_of_initializeData_8() { return &___initializeData_8; }
	inline void set_initializeData_8(String_t* value)
	{
		___initializeData_8 = value;
		Il2CppCodeGenWriteBarrier(&___initializeData_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
