﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollectionNode
struct  XmlSchemaCollectionNode_t3122448512  : public Il2CppObject
{
public:
	// System.String System.Xml.Schema.XmlSchemaCollectionNode::namespaceUri
	String_t* ___namespaceUri_0;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XmlSchemaCollectionNode::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_1;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaCollectionNode::schema
	XmlSchema_t880472818 * ___schema_2;

public:
	inline static int32_t get_offset_of_namespaceUri_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_t3122448512, ___namespaceUri_0)); }
	inline String_t* get_namespaceUri_0() const { return ___namespaceUri_0; }
	inline String_t** get_address_of_namespaceUri_0() { return &___namespaceUri_0; }
	inline void set_namespaceUri_0(String_t* value)
	{
		___namespaceUri_0 = value;
		Il2CppCodeGenWriteBarrier(&___namespaceUri_0, value);
	}

	inline static int32_t get_offset_of_schemaInfo_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_t3122448512, ___schemaInfo_1)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_1() const { return ___schemaInfo_1; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_1() { return &___schemaInfo_1; }
	inline void set_schemaInfo_1(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___schemaInfo_1, value);
	}

	inline static int32_t get_offset_of_schema_2() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionNode_t3122448512, ___schema_2)); }
	inline XmlSchema_t880472818 * get_schema_2() const { return ___schema_2; }
	inline XmlSchema_t880472818 ** get_address_of_schema_2() { return &___schema_2; }
	inline void set_schema_2(XmlSchema_t880472818 * value)
	{
		___schema_2 = value;
		Il2CppCodeGenWriteBarrier(&___schema_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
