﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.IO.Stream
struct Stream_t3255436806;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// Mono.Security.Protocol.Tls.Context
struct Context_t4285182719;
// Mono.Security.Protocol.Tls.RecordProtocol
struct RecordProtocol_t3166895267;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.SslStreamBase
struct  SslStreamBase_t934199321  : public Stream_t3255436806
{
public:
	// System.IO.Stream Mono.Security.Protocol.Tls.SslStreamBase::innerStream
	Stream_t3255436806 * ___innerStream_9;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::inputBuffer
	MemoryStream_t743994179 * ___inputBuffer_10;
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.SslStreamBase::context
	Context_t4285182719 * ___context_11;
	// Mono.Security.Protocol.Tls.RecordProtocol Mono.Security.Protocol.Tls.SslStreamBase::protocol
	RecordProtocol_t3166895267 * ___protocol_12;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::ownsStream
	bool ___ownsStream_13;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Mono.Security.Protocol.Tls.SslStreamBase::disposed
	bool ___disposed_14;
	// System.Boolean Mono.Security.Protocol.Tls.SslStreamBase::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_15;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::negotiate
	Il2CppObject * ___negotiate_16;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::read
	Il2CppObject * ___read_17;
	// System.Object Mono.Security.Protocol.Tls.SslStreamBase::write
	Il2CppObject * ___write_18;
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::negotiationComplete
	ManualResetEvent_t926074657 * ___negotiationComplete_19;
	// System.Byte[] Mono.Security.Protocol.Tls.SslStreamBase::recbuf
	ByteU5BU5D_t3397334013* ___recbuf_20;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.SslStreamBase::recordStream
	MemoryStream_t743994179 * ___recordStream_21;

public:
	inline static int32_t get_offset_of_innerStream_9() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___innerStream_9)); }
	inline Stream_t3255436806 * get_innerStream_9() const { return ___innerStream_9; }
	inline Stream_t3255436806 ** get_address_of_innerStream_9() { return &___innerStream_9; }
	inline void set_innerStream_9(Stream_t3255436806 * value)
	{
		___innerStream_9 = value;
		Il2CppCodeGenWriteBarrier(&___innerStream_9, value);
	}

	inline static int32_t get_offset_of_inputBuffer_10() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___inputBuffer_10)); }
	inline MemoryStream_t743994179 * get_inputBuffer_10() const { return ___inputBuffer_10; }
	inline MemoryStream_t743994179 ** get_address_of_inputBuffer_10() { return &___inputBuffer_10; }
	inline void set_inputBuffer_10(MemoryStream_t743994179 * value)
	{
		___inputBuffer_10 = value;
		Il2CppCodeGenWriteBarrier(&___inputBuffer_10, value);
	}

	inline static int32_t get_offset_of_context_11() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___context_11)); }
	inline Context_t4285182719 * get_context_11() const { return ___context_11; }
	inline Context_t4285182719 ** get_address_of_context_11() { return &___context_11; }
	inline void set_context_11(Context_t4285182719 * value)
	{
		___context_11 = value;
		Il2CppCodeGenWriteBarrier(&___context_11, value);
	}

	inline static int32_t get_offset_of_protocol_12() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___protocol_12)); }
	inline RecordProtocol_t3166895267 * get_protocol_12() const { return ___protocol_12; }
	inline RecordProtocol_t3166895267 ** get_address_of_protocol_12() { return &___protocol_12; }
	inline void set_protocol_12(RecordProtocol_t3166895267 * value)
	{
		___protocol_12 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_12, value);
	}

	inline static int32_t get_offset_of_ownsStream_13() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___ownsStream_13)); }
	inline bool get_ownsStream_13() const { return ___ownsStream_13; }
	inline bool* get_address_of_ownsStream_13() { return &___ownsStream_13; }
	inline void set_ownsStream_13(bool value)
	{
		___ownsStream_13 = value;
	}

	inline static int32_t get_offset_of_disposed_14() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___disposed_14)); }
	inline bool get_disposed_14() const { return ___disposed_14; }
	inline bool* get_address_of_disposed_14() { return &___disposed_14; }
	inline void set_disposed_14(bool value)
	{
		___disposed_14 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_15() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___checkCertRevocationStatus_15)); }
	inline bool get_checkCertRevocationStatus_15() const { return ___checkCertRevocationStatus_15; }
	inline bool* get_address_of_checkCertRevocationStatus_15() { return &___checkCertRevocationStatus_15; }
	inline void set_checkCertRevocationStatus_15(bool value)
	{
		___checkCertRevocationStatus_15 = value;
	}

	inline static int32_t get_offset_of_negotiate_16() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___negotiate_16)); }
	inline Il2CppObject * get_negotiate_16() const { return ___negotiate_16; }
	inline Il2CppObject ** get_address_of_negotiate_16() { return &___negotiate_16; }
	inline void set_negotiate_16(Il2CppObject * value)
	{
		___negotiate_16 = value;
		Il2CppCodeGenWriteBarrier(&___negotiate_16, value);
	}

	inline static int32_t get_offset_of_read_17() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___read_17)); }
	inline Il2CppObject * get_read_17() const { return ___read_17; }
	inline Il2CppObject ** get_address_of_read_17() { return &___read_17; }
	inline void set_read_17(Il2CppObject * value)
	{
		___read_17 = value;
		Il2CppCodeGenWriteBarrier(&___read_17, value);
	}

	inline static int32_t get_offset_of_write_18() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___write_18)); }
	inline Il2CppObject * get_write_18() const { return ___write_18; }
	inline Il2CppObject ** get_address_of_write_18() { return &___write_18; }
	inline void set_write_18(Il2CppObject * value)
	{
		___write_18 = value;
		Il2CppCodeGenWriteBarrier(&___write_18, value);
	}

	inline static int32_t get_offset_of_negotiationComplete_19() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___negotiationComplete_19)); }
	inline ManualResetEvent_t926074657 * get_negotiationComplete_19() const { return ___negotiationComplete_19; }
	inline ManualResetEvent_t926074657 ** get_address_of_negotiationComplete_19() { return &___negotiationComplete_19; }
	inline void set_negotiationComplete_19(ManualResetEvent_t926074657 * value)
	{
		___negotiationComplete_19 = value;
		Il2CppCodeGenWriteBarrier(&___negotiationComplete_19, value);
	}

	inline static int32_t get_offset_of_recbuf_20() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___recbuf_20)); }
	inline ByteU5BU5D_t3397334013* get_recbuf_20() const { return ___recbuf_20; }
	inline ByteU5BU5D_t3397334013** get_address_of_recbuf_20() { return &___recbuf_20; }
	inline void set_recbuf_20(ByteU5BU5D_t3397334013* value)
	{
		___recbuf_20 = value;
		Il2CppCodeGenWriteBarrier(&___recbuf_20, value);
	}

	inline static int32_t get_offset_of_recordStream_21() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321, ___recordStream_21)); }
	inline MemoryStream_t743994179 * get_recordStream_21() const { return ___recordStream_21; }
	inline MemoryStream_t743994179 ** get_address_of_recordStream_21() { return &___recordStream_21; }
	inline void set_recordStream_21(MemoryStream_t743994179 * value)
	{
		___recordStream_21 = value;
		Il2CppCodeGenWriteBarrier(&___recordStream_21, value);
	}
};

struct SslStreamBase_t934199321_StaticFields
{
public:
	// System.Threading.ManualResetEvent Mono.Security.Protocol.Tls.SslStreamBase::record_processing
	ManualResetEvent_t926074657 * ___record_processing_8;

public:
	inline static int32_t get_offset_of_record_processing_8() { return static_cast<int32_t>(offsetof(SslStreamBase_t934199321_StaticFields, ___record_processing_8)); }
	inline ManualResetEvent_t926074657 * get_record_processing_8() const { return ___record_processing_8; }
	inline ManualResetEvent_t926074657 ** get_address_of_record_processing_8() { return &___record_processing_8; }
	inline void set_record_processing_8(ManualResetEvent_t926074657 * value)
	{
		___record_processing_8 = value;
		Il2CppCodeGenWriteBarrier(&___record_processing_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
