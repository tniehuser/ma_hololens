﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur1776195828.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.SourceElement
struct  SourceElement_t2880823865  : public ConfigurationElement_t1776195828
{
public:
	// System.Collections.Hashtable System.Diagnostics.SourceElement::_attributes
	Hashtable_t909839986 * ____attributes_21;

public:
	inline static int32_t get_offset_of__attributes_21() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865, ____attributes_21)); }
	inline Hashtable_t909839986 * get__attributes_21() const { return ____attributes_21; }
	inline Hashtable_t909839986 ** get_address_of__attributes_21() { return &____attributes_21; }
	inline void set__attributes_21(Hashtable_t909839986 * value)
	{
		____attributes_21 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_21, value);
	}
};

struct SourceElement_t2880823865_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Diagnostics.SourceElement::_properties
	ConfigurationPropertyCollection_t3473514151 * ____properties_15;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SourceElement::_propName
	ConfigurationProperty_t2048066811 * ____propName_16;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SourceElement::_propSwitchName
	ConfigurationProperty_t2048066811 * ____propSwitchName_17;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SourceElement::_propSwitchValue
	ConfigurationProperty_t2048066811 * ____propSwitchValue_18;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SourceElement::_propSwitchType
	ConfigurationProperty_t2048066811 * ____propSwitchType_19;
	// System.Configuration.ConfigurationProperty System.Diagnostics.SourceElement::_propListeners
	ConfigurationProperty_t2048066811 * ____propListeners_20;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____properties_15)); }
	inline ConfigurationPropertyCollection_t3473514151 * get__properties_15() const { return ____properties_15; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(ConfigurationPropertyCollection_t3473514151 * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier(&____properties_15, value);
	}

	inline static int32_t get_offset_of__propName_16() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____propName_16)); }
	inline ConfigurationProperty_t2048066811 * get__propName_16() const { return ____propName_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propName_16() { return &____propName_16; }
	inline void set__propName_16(ConfigurationProperty_t2048066811 * value)
	{
		____propName_16 = value;
		Il2CppCodeGenWriteBarrier(&____propName_16, value);
	}

	inline static int32_t get_offset_of__propSwitchName_17() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____propSwitchName_17)); }
	inline ConfigurationProperty_t2048066811 * get__propSwitchName_17() const { return ____propSwitchName_17; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSwitchName_17() { return &____propSwitchName_17; }
	inline void set__propSwitchName_17(ConfigurationProperty_t2048066811 * value)
	{
		____propSwitchName_17 = value;
		Il2CppCodeGenWriteBarrier(&____propSwitchName_17, value);
	}

	inline static int32_t get_offset_of__propSwitchValue_18() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____propSwitchValue_18)); }
	inline ConfigurationProperty_t2048066811 * get__propSwitchValue_18() const { return ____propSwitchValue_18; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSwitchValue_18() { return &____propSwitchValue_18; }
	inline void set__propSwitchValue_18(ConfigurationProperty_t2048066811 * value)
	{
		____propSwitchValue_18 = value;
		Il2CppCodeGenWriteBarrier(&____propSwitchValue_18, value);
	}

	inline static int32_t get_offset_of__propSwitchType_19() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____propSwitchType_19)); }
	inline ConfigurationProperty_t2048066811 * get__propSwitchType_19() const { return ____propSwitchType_19; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propSwitchType_19() { return &____propSwitchType_19; }
	inline void set__propSwitchType_19(ConfigurationProperty_t2048066811 * value)
	{
		____propSwitchType_19 = value;
		Il2CppCodeGenWriteBarrier(&____propSwitchType_19, value);
	}

	inline static int32_t get_offset_of__propListeners_20() { return static_cast<int32_t>(offsetof(SourceElement_t2880823865_StaticFields, ____propListeners_20)); }
	inline ConfigurationProperty_t2048066811 * get__propListeners_20() const { return ____propListeners_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propListeners_20() { return &____propListeners_20; }
	inline void set__propListeners_20(ConfigurationProperty_t2048066811 * value)
	{
		____propListeners_20 = value;
		Il2CppCodeGenWriteBarrier(&____propListeners_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
