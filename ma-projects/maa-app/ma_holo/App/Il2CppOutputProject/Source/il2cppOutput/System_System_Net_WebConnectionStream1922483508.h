﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.WebConnection
struct WebConnection_t324679648;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Object
struct Il2CppObject;
// System.AsyncCallback
struct AsyncCallback_t163412349;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream
struct  WebConnectionStream_t1922483508  : public Stream_t3255436806
{
public:
	// System.Boolean System.Net.WebConnectionStream::isRead
	bool ___isRead_9;
	// System.Net.WebConnection System.Net.WebConnectionStream::cnc
	WebConnection_t324679648 * ___cnc_10;
	// System.Net.HttpWebRequest System.Net.WebConnectionStream::request
	HttpWebRequest_t1951404513 * ___request_11;
	// System.Byte[] System.Net.WebConnectionStream::readBuffer
	ByteU5BU5D_t3397334013* ___readBuffer_12;
	// System.Int32 System.Net.WebConnectionStream::readBufferOffset
	int32_t ___readBufferOffset_13;
	// System.Int32 System.Net.WebConnectionStream::readBufferSize
	int32_t ___readBufferSize_14;
	// System.Int32 System.Net.WebConnectionStream::stream_length
	int32_t ___stream_length_15;
	// System.Int64 System.Net.WebConnectionStream::contentLength
	int64_t ___contentLength_16;
	// System.Int64 System.Net.WebConnectionStream::totalRead
	int64_t ___totalRead_17;
	// System.Int64 System.Net.WebConnectionStream::totalWritten
	int64_t ___totalWritten_18;
	// System.Boolean System.Net.WebConnectionStream::nextReadCalled
	bool ___nextReadCalled_19;
	// System.Int32 System.Net.WebConnectionStream::pendingReads
	int32_t ___pendingReads_20;
	// System.Int32 System.Net.WebConnectionStream::pendingWrites
	int32_t ___pendingWrites_21;
	// System.Threading.ManualResetEvent System.Net.WebConnectionStream::pending
	ManualResetEvent_t926074657 * ___pending_22;
	// System.Boolean System.Net.WebConnectionStream::allowBuffering
	bool ___allowBuffering_23;
	// System.Boolean System.Net.WebConnectionStream::sendChunked
	bool ___sendChunked_24;
	// System.IO.MemoryStream System.Net.WebConnectionStream::writeBuffer
	MemoryStream_t743994179 * ___writeBuffer_25;
	// System.Boolean System.Net.WebConnectionStream::requestWritten
	bool ___requestWritten_26;
	// System.Byte[] System.Net.WebConnectionStream::headers
	ByteU5BU5D_t3397334013* ___headers_27;
	// System.Boolean System.Net.WebConnectionStream::disposed
	bool ___disposed_28;
	// System.Boolean System.Net.WebConnectionStream::headersSent
	bool ___headersSent_29;
	// System.Object System.Net.WebConnectionStream::locker
	Il2CppObject * ___locker_30;
	// System.Boolean System.Net.WebConnectionStream::initRead
	bool ___initRead_31;
	// System.Boolean System.Net.WebConnectionStream::read_eof
	bool ___read_eof_32;
	// System.Boolean System.Net.WebConnectionStream::complete_request_written
	bool ___complete_request_written_33;
	// System.Int32 System.Net.WebConnectionStream::read_timeout
	int32_t ___read_timeout_34;
	// System.Int32 System.Net.WebConnectionStream::write_timeout
	int32_t ___write_timeout_35;
	// System.AsyncCallback System.Net.WebConnectionStream::cb_wrapper
	AsyncCallback_t163412349 * ___cb_wrapper_36;
	// System.Boolean System.Net.WebConnectionStream::IgnoreIOErrors
	bool ___IgnoreIOErrors_37;
	// System.Boolean System.Net.WebConnectionStream::<GetResponseOnClose>k__BackingField
	bool ___U3CGetResponseOnCloseU3Ek__BackingField_38;

public:
	inline static int32_t get_offset_of_isRead_9() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___isRead_9)); }
	inline bool get_isRead_9() const { return ___isRead_9; }
	inline bool* get_address_of_isRead_9() { return &___isRead_9; }
	inline void set_isRead_9(bool value)
	{
		___isRead_9 = value;
	}

	inline static int32_t get_offset_of_cnc_10() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___cnc_10)); }
	inline WebConnection_t324679648 * get_cnc_10() const { return ___cnc_10; }
	inline WebConnection_t324679648 ** get_address_of_cnc_10() { return &___cnc_10; }
	inline void set_cnc_10(WebConnection_t324679648 * value)
	{
		___cnc_10 = value;
		Il2CppCodeGenWriteBarrier(&___cnc_10, value);
	}

	inline static int32_t get_offset_of_request_11() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___request_11)); }
	inline HttpWebRequest_t1951404513 * get_request_11() const { return ___request_11; }
	inline HttpWebRequest_t1951404513 ** get_address_of_request_11() { return &___request_11; }
	inline void set_request_11(HttpWebRequest_t1951404513 * value)
	{
		___request_11 = value;
		Il2CppCodeGenWriteBarrier(&___request_11, value);
	}

	inline static int32_t get_offset_of_readBuffer_12() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___readBuffer_12)); }
	inline ByteU5BU5D_t3397334013* get_readBuffer_12() const { return ___readBuffer_12; }
	inline ByteU5BU5D_t3397334013** get_address_of_readBuffer_12() { return &___readBuffer_12; }
	inline void set_readBuffer_12(ByteU5BU5D_t3397334013* value)
	{
		___readBuffer_12 = value;
		Il2CppCodeGenWriteBarrier(&___readBuffer_12, value);
	}

	inline static int32_t get_offset_of_readBufferOffset_13() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___readBufferOffset_13)); }
	inline int32_t get_readBufferOffset_13() const { return ___readBufferOffset_13; }
	inline int32_t* get_address_of_readBufferOffset_13() { return &___readBufferOffset_13; }
	inline void set_readBufferOffset_13(int32_t value)
	{
		___readBufferOffset_13 = value;
	}

	inline static int32_t get_offset_of_readBufferSize_14() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___readBufferSize_14)); }
	inline int32_t get_readBufferSize_14() const { return ___readBufferSize_14; }
	inline int32_t* get_address_of_readBufferSize_14() { return &___readBufferSize_14; }
	inline void set_readBufferSize_14(int32_t value)
	{
		___readBufferSize_14 = value;
	}

	inline static int32_t get_offset_of_stream_length_15() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___stream_length_15)); }
	inline int32_t get_stream_length_15() const { return ___stream_length_15; }
	inline int32_t* get_address_of_stream_length_15() { return &___stream_length_15; }
	inline void set_stream_length_15(int32_t value)
	{
		___stream_length_15 = value;
	}

	inline static int32_t get_offset_of_contentLength_16() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___contentLength_16)); }
	inline int64_t get_contentLength_16() const { return ___contentLength_16; }
	inline int64_t* get_address_of_contentLength_16() { return &___contentLength_16; }
	inline void set_contentLength_16(int64_t value)
	{
		___contentLength_16 = value;
	}

	inline static int32_t get_offset_of_totalRead_17() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___totalRead_17)); }
	inline int64_t get_totalRead_17() const { return ___totalRead_17; }
	inline int64_t* get_address_of_totalRead_17() { return &___totalRead_17; }
	inline void set_totalRead_17(int64_t value)
	{
		___totalRead_17 = value;
	}

	inline static int32_t get_offset_of_totalWritten_18() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___totalWritten_18)); }
	inline int64_t get_totalWritten_18() const { return ___totalWritten_18; }
	inline int64_t* get_address_of_totalWritten_18() { return &___totalWritten_18; }
	inline void set_totalWritten_18(int64_t value)
	{
		___totalWritten_18 = value;
	}

	inline static int32_t get_offset_of_nextReadCalled_19() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___nextReadCalled_19)); }
	inline bool get_nextReadCalled_19() const { return ___nextReadCalled_19; }
	inline bool* get_address_of_nextReadCalled_19() { return &___nextReadCalled_19; }
	inline void set_nextReadCalled_19(bool value)
	{
		___nextReadCalled_19 = value;
	}

	inline static int32_t get_offset_of_pendingReads_20() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___pendingReads_20)); }
	inline int32_t get_pendingReads_20() const { return ___pendingReads_20; }
	inline int32_t* get_address_of_pendingReads_20() { return &___pendingReads_20; }
	inline void set_pendingReads_20(int32_t value)
	{
		___pendingReads_20 = value;
	}

	inline static int32_t get_offset_of_pendingWrites_21() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___pendingWrites_21)); }
	inline int32_t get_pendingWrites_21() const { return ___pendingWrites_21; }
	inline int32_t* get_address_of_pendingWrites_21() { return &___pendingWrites_21; }
	inline void set_pendingWrites_21(int32_t value)
	{
		___pendingWrites_21 = value;
	}

	inline static int32_t get_offset_of_pending_22() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___pending_22)); }
	inline ManualResetEvent_t926074657 * get_pending_22() const { return ___pending_22; }
	inline ManualResetEvent_t926074657 ** get_address_of_pending_22() { return &___pending_22; }
	inline void set_pending_22(ManualResetEvent_t926074657 * value)
	{
		___pending_22 = value;
		Il2CppCodeGenWriteBarrier(&___pending_22, value);
	}

	inline static int32_t get_offset_of_allowBuffering_23() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___allowBuffering_23)); }
	inline bool get_allowBuffering_23() const { return ___allowBuffering_23; }
	inline bool* get_address_of_allowBuffering_23() { return &___allowBuffering_23; }
	inline void set_allowBuffering_23(bool value)
	{
		___allowBuffering_23 = value;
	}

	inline static int32_t get_offset_of_sendChunked_24() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___sendChunked_24)); }
	inline bool get_sendChunked_24() const { return ___sendChunked_24; }
	inline bool* get_address_of_sendChunked_24() { return &___sendChunked_24; }
	inline void set_sendChunked_24(bool value)
	{
		___sendChunked_24 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_25() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___writeBuffer_25)); }
	inline MemoryStream_t743994179 * get_writeBuffer_25() const { return ___writeBuffer_25; }
	inline MemoryStream_t743994179 ** get_address_of_writeBuffer_25() { return &___writeBuffer_25; }
	inline void set_writeBuffer_25(MemoryStream_t743994179 * value)
	{
		___writeBuffer_25 = value;
		Il2CppCodeGenWriteBarrier(&___writeBuffer_25, value);
	}

	inline static int32_t get_offset_of_requestWritten_26() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___requestWritten_26)); }
	inline bool get_requestWritten_26() const { return ___requestWritten_26; }
	inline bool* get_address_of_requestWritten_26() { return &___requestWritten_26; }
	inline void set_requestWritten_26(bool value)
	{
		___requestWritten_26 = value;
	}

	inline static int32_t get_offset_of_headers_27() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___headers_27)); }
	inline ByteU5BU5D_t3397334013* get_headers_27() const { return ___headers_27; }
	inline ByteU5BU5D_t3397334013** get_address_of_headers_27() { return &___headers_27; }
	inline void set_headers_27(ByteU5BU5D_t3397334013* value)
	{
		___headers_27 = value;
		Il2CppCodeGenWriteBarrier(&___headers_27, value);
	}

	inline static int32_t get_offset_of_disposed_28() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___disposed_28)); }
	inline bool get_disposed_28() const { return ___disposed_28; }
	inline bool* get_address_of_disposed_28() { return &___disposed_28; }
	inline void set_disposed_28(bool value)
	{
		___disposed_28 = value;
	}

	inline static int32_t get_offset_of_headersSent_29() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___headersSent_29)); }
	inline bool get_headersSent_29() const { return ___headersSent_29; }
	inline bool* get_address_of_headersSent_29() { return &___headersSent_29; }
	inline void set_headersSent_29(bool value)
	{
		___headersSent_29 = value;
	}

	inline static int32_t get_offset_of_locker_30() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___locker_30)); }
	inline Il2CppObject * get_locker_30() const { return ___locker_30; }
	inline Il2CppObject ** get_address_of_locker_30() { return &___locker_30; }
	inline void set_locker_30(Il2CppObject * value)
	{
		___locker_30 = value;
		Il2CppCodeGenWriteBarrier(&___locker_30, value);
	}

	inline static int32_t get_offset_of_initRead_31() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___initRead_31)); }
	inline bool get_initRead_31() const { return ___initRead_31; }
	inline bool* get_address_of_initRead_31() { return &___initRead_31; }
	inline void set_initRead_31(bool value)
	{
		___initRead_31 = value;
	}

	inline static int32_t get_offset_of_read_eof_32() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___read_eof_32)); }
	inline bool get_read_eof_32() const { return ___read_eof_32; }
	inline bool* get_address_of_read_eof_32() { return &___read_eof_32; }
	inline void set_read_eof_32(bool value)
	{
		___read_eof_32 = value;
	}

	inline static int32_t get_offset_of_complete_request_written_33() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___complete_request_written_33)); }
	inline bool get_complete_request_written_33() const { return ___complete_request_written_33; }
	inline bool* get_address_of_complete_request_written_33() { return &___complete_request_written_33; }
	inline void set_complete_request_written_33(bool value)
	{
		___complete_request_written_33 = value;
	}

	inline static int32_t get_offset_of_read_timeout_34() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___read_timeout_34)); }
	inline int32_t get_read_timeout_34() const { return ___read_timeout_34; }
	inline int32_t* get_address_of_read_timeout_34() { return &___read_timeout_34; }
	inline void set_read_timeout_34(int32_t value)
	{
		___read_timeout_34 = value;
	}

	inline static int32_t get_offset_of_write_timeout_35() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___write_timeout_35)); }
	inline int32_t get_write_timeout_35() const { return ___write_timeout_35; }
	inline int32_t* get_address_of_write_timeout_35() { return &___write_timeout_35; }
	inline void set_write_timeout_35(int32_t value)
	{
		___write_timeout_35 = value;
	}

	inline static int32_t get_offset_of_cb_wrapper_36() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___cb_wrapper_36)); }
	inline AsyncCallback_t163412349 * get_cb_wrapper_36() const { return ___cb_wrapper_36; }
	inline AsyncCallback_t163412349 ** get_address_of_cb_wrapper_36() { return &___cb_wrapper_36; }
	inline void set_cb_wrapper_36(AsyncCallback_t163412349 * value)
	{
		___cb_wrapper_36 = value;
		Il2CppCodeGenWriteBarrier(&___cb_wrapper_36, value);
	}

	inline static int32_t get_offset_of_IgnoreIOErrors_37() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___IgnoreIOErrors_37)); }
	inline bool get_IgnoreIOErrors_37() const { return ___IgnoreIOErrors_37; }
	inline bool* get_address_of_IgnoreIOErrors_37() { return &___IgnoreIOErrors_37; }
	inline void set_IgnoreIOErrors_37(bool value)
	{
		___IgnoreIOErrors_37 = value;
	}

	inline static int32_t get_offset_of_U3CGetResponseOnCloseU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508, ___U3CGetResponseOnCloseU3Ek__BackingField_38)); }
	inline bool get_U3CGetResponseOnCloseU3Ek__BackingField_38() const { return ___U3CGetResponseOnCloseU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CGetResponseOnCloseU3Ek__BackingField_38() { return &___U3CGetResponseOnCloseU3Ek__BackingField_38; }
	inline void set_U3CGetResponseOnCloseU3Ek__BackingField_38(bool value)
	{
		___U3CGetResponseOnCloseU3Ek__BackingField_38 = value;
	}
};

struct WebConnectionStream_t1922483508_StaticFields
{
public:
	// System.Byte[] System.Net.WebConnectionStream::crlf
	ByteU5BU5D_t3397334013* ___crlf_8;

public:
	inline static int32_t get_offset_of_crlf_8() { return static_cast<int32_t>(offsetof(WebConnectionStream_t1922483508_StaticFields, ___crlf_8)); }
	inline ByteU5BU5D_t3397334013* get_crlf_8() const { return ___crlf_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_crlf_8() { return &___crlf_8; }
	inline void set_crlf_8(ByteU5BU5D_t3397334013* value)
	{
		___crlf_8 = value;
		Il2CppCodeGenWriteBarrier(&___crlf_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
