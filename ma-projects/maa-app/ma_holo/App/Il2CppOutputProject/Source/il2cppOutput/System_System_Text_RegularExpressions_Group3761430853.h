﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Text_RegularExpressions_Capture4157900610.h"

// System.Text.RegularExpressions.Group
struct Group_t3761430853;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t3761430853  : public Capture_t4157900610
{
public:
	// System.Int32[] System.Text.RegularExpressions.Group::_caps
	Int32U5BU5D_t3030399641* ____caps_4;
	// System.Int32 System.Text.RegularExpressions.Group::_capcount
	int32_t ____capcount_5;

public:
	inline static int32_t get_offset_of__caps_4() { return static_cast<int32_t>(offsetof(Group_t3761430853, ____caps_4)); }
	inline Int32U5BU5D_t3030399641* get__caps_4() const { return ____caps_4; }
	inline Int32U5BU5D_t3030399641** get_address_of__caps_4() { return &____caps_4; }
	inline void set__caps_4(Int32U5BU5D_t3030399641* value)
	{
		____caps_4 = value;
		Il2CppCodeGenWriteBarrier(&____caps_4, value);
	}

	inline static int32_t get_offset_of__capcount_5() { return static_cast<int32_t>(offsetof(Group_t3761430853, ____capcount_5)); }
	inline int32_t get__capcount_5() const { return ____capcount_5; }
	inline int32_t* get_address_of__capcount_5() { return &____capcount_5; }
	inline void set__capcount_5(int32_t value)
	{
		____capcount_5 = value;
	}
};

struct Group_t3761430853_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::_emptygroup
	Group_t3761430853 * ____emptygroup_3;

public:
	inline static int32_t get_offset_of__emptygroup_3() { return static_cast<int32_t>(offsetof(Group_t3761430853_StaticFields, ____emptygroup_3)); }
	inline Group_t3761430853 * get__emptygroup_3() const { return ____emptygroup_3; }
	inline Group_t3761430853 ** get_address_of__emptygroup_3() { return &____emptygroup_3; }
	inline void set__emptygroup_3(Group_t3761430853 * value)
	{
		____emptygroup_3 = value;
		Il2CppCodeGenWriteBarrier(&____emptygroup_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
