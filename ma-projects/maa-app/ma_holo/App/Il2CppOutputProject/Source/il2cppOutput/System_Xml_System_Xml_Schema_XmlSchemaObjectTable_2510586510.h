﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct  XmlSchemaObjectEntry_t2510586510 
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry::qname
	XmlQualifiedName_t1944712516 * ___qname_0;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry::xso
	XmlSchemaObject_t2050913741 * ___xso_1;

public:
	inline static int32_t get_offset_of_qname_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEntry_t2510586510, ___qname_0)); }
	inline XmlQualifiedName_t1944712516 * get_qname_0() const { return ___qname_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_0() { return &___qname_0; }
	inline void set_qname_0(XmlQualifiedName_t1944712516 * value)
	{
		___qname_0 = value;
		Il2CppCodeGenWriteBarrier(&___qname_0, value);
	}

	inline static int32_t get_offset_of_xso_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEntry_t2510586510, ___xso_1)); }
	inline XmlSchemaObject_t2050913741 * get_xso_1() const { return ___xso_1; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_xso_1() { return &___xso_1; }
	inline void set_xso_1(XmlSchemaObject_t2050913741 * value)
	{
		___xso_1 = value;
		Il2CppCodeGenWriteBarrier(&___xso_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct XmlSchemaObjectEntry_t2510586510_marshaled_pinvoke
{
	XmlQualifiedName_t1944712516 * ___qname_0;
	XmlSchemaObject_t2050913741 * ___xso_1;
};
// Native definition for COM marshalling of System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectEntry
struct XmlSchemaObjectEntry_t2510586510_marshaled_com
{
	XmlQualifiedName_t1944712516 * ___qname_0;
	XmlSchemaObject_t2050913741 * ___xso_1;
};
