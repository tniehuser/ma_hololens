﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Util_Tokenizer_TokenSource403933261.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// System.Security.Util.Tokenizer/ITokenReader
struct ITokenReader_t4193424590;
// System.Security.Util.Tokenizer/StringMaker
struct StringMaker_t3542918456;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Util.Tokenizer
struct  Tokenizer_t395826351  : public Il2CppObject
{
public:
	// System.Int32 System.Security.Util.Tokenizer::LineNo
	int32_t ___LineNo_0;
	// System.Int32 System.Security.Util.Tokenizer::_inProcessingTag
	int32_t ____inProcessingTag_1;
	// System.Byte[] System.Security.Util.Tokenizer::_inBytes
	ByteU5BU5D_t3397334013* ____inBytes_2;
	// System.Char[] System.Security.Util.Tokenizer::_inChars
	CharU5BU5D_t1328083999* ____inChars_3;
	// System.String System.Security.Util.Tokenizer::_inString
	String_t* ____inString_4;
	// System.Int32 System.Security.Util.Tokenizer::_inIndex
	int32_t ____inIndex_5;
	// System.Int32 System.Security.Util.Tokenizer::_inSize
	int32_t ____inSize_6;
	// System.Int32 System.Security.Util.Tokenizer::_inSavedCharacter
	int32_t ____inSavedCharacter_7;
	// System.Security.Util.Tokenizer/TokenSource System.Security.Util.Tokenizer::_inTokenSource
	int32_t ____inTokenSource_8;
	// System.Security.Util.Tokenizer/ITokenReader System.Security.Util.Tokenizer::_inTokenReader
	Il2CppObject * ____inTokenReader_9;
	// System.Security.Util.Tokenizer/StringMaker System.Security.Util.Tokenizer::_maker
	StringMaker_t3542918456 * ____maker_10;
	// System.String[] System.Security.Util.Tokenizer::_searchStrings
	StringU5BU5D_t1642385972* ____searchStrings_11;
	// System.String[] System.Security.Util.Tokenizer::_replaceStrings
	StringU5BU5D_t1642385972* ____replaceStrings_12;
	// System.Int32 System.Security.Util.Tokenizer::_inNestedIndex
	int32_t ____inNestedIndex_13;
	// System.Int32 System.Security.Util.Tokenizer::_inNestedSize
	int32_t ____inNestedSize_14;
	// System.String System.Security.Util.Tokenizer::_inNestedString
	String_t* ____inNestedString_15;

public:
	inline static int32_t get_offset_of_LineNo_0() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ___LineNo_0)); }
	inline int32_t get_LineNo_0() const { return ___LineNo_0; }
	inline int32_t* get_address_of_LineNo_0() { return &___LineNo_0; }
	inline void set_LineNo_0(int32_t value)
	{
		___LineNo_0 = value;
	}

	inline static int32_t get_offset_of__inProcessingTag_1() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inProcessingTag_1)); }
	inline int32_t get__inProcessingTag_1() const { return ____inProcessingTag_1; }
	inline int32_t* get_address_of__inProcessingTag_1() { return &____inProcessingTag_1; }
	inline void set__inProcessingTag_1(int32_t value)
	{
		____inProcessingTag_1 = value;
	}

	inline static int32_t get_offset_of__inBytes_2() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inBytes_2)); }
	inline ByteU5BU5D_t3397334013* get__inBytes_2() const { return ____inBytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__inBytes_2() { return &____inBytes_2; }
	inline void set__inBytes_2(ByteU5BU5D_t3397334013* value)
	{
		____inBytes_2 = value;
		Il2CppCodeGenWriteBarrier(&____inBytes_2, value);
	}

	inline static int32_t get_offset_of__inChars_3() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inChars_3)); }
	inline CharU5BU5D_t1328083999* get__inChars_3() const { return ____inChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of__inChars_3() { return &____inChars_3; }
	inline void set__inChars_3(CharU5BU5D_t1328083999* value)
	{
		____inChars_3 = value;
		Il2CppCodeGenWriteBarrier(&____inChars_3, value);
	}

	inline static int32_t get_offset_of__inString_4() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inString_4)); }
	inline String_t* get__inString_4() const { return ____inString_4; }
	inline String_t** get_address_of__inString_4() { return &____inString_4; }
	inline void set__inString_4(String_t* value)
	{
		____inString_4 = value;
		Il2CppCodeGenWriteBarrier(&____inString_4, value);
	}

	inline static int32_t get_offset_of__inIndex_5() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inIndex_5)); }
	inline int32_t get__inIndex_5() const { return ____inIndex_5; }
	inline int32_t* get_address_of__inIndex_5() { return &____inIndex_5; }
	inline void set__inIndex_5(int32_t value)
	{
		____inIndex_5 = value;
	}

	inline static int32_t get_offset_of__inSize_6() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inSize_6)); }
	inline int32_t get__inSize_6() const { return ____inSize_6; }
	inline int32_t* get_address_of__inSize_6() { return &____inSize_6; }
	inline void set__inSize_6(int32_t value)
	{
		____inSize_6 = value;
	}

	inline static int32_t get_offset_of__inSavedCharacter_7() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inSavedCharacter_7)); }
	inline int32_t get__inSavedCharacter_7() const { return ____inSavedCharacter_7; }
	inline int32_t* get_address_of__inSavedCharacter_7() { return &____inSavedCharacter_7; }
	inline void set__inSavedCharacter_7(int32_t value)
	{
		____inSavedCharacter_7 = value;
	}

	inline static int32_t get_offset_of__inTokenSource_8() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inTokenSource_8)); }
	inline int32_t get__inTokenSource_8() const { return ____inTokenSource_8; }
	inline int32_t* get_address_of__inTokenSource_8() { return &____inTokenSource_8; }
	inline void set__inTokenSource_8(int32_t value)
	{
		____inTokenSource_8 = value;
	}

	inline static int32_t get_offset_of__inTokenReader_9() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inTokenReader_9)); }
	inline Il2CppObject * get__inTokenReader_9() const { return ____inTokenReader_9; }
	inline Il2CppObject ** get_address_of__inTokenReader_9() { return &____inTokenReader_9; }
	inline void set__inTokenReader_9(Il2CppObject * value)
	{
		____inTokenReader_9 = value;
		Il2CppCodeGenWriteBarrier(&____inTokenReader_9, value);
	}

	inline static int32_t get_offset_of__maker_10() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____maker_10)); }
	inline StringMaker_t3542918456 * get__maker_10() const { return ____maker_10; }
	inline StringMaker_t3542918456 ** get_address_of__maker_10() { return &____maker_10; }
	inline void set__maker_10(StringMaker_t3542918456 * value)
	{
		____maker_10 = value;
		Il2CppCodeGenWriteBarrier(&____maker_10, value);
	}

	inline static int32_t get_offset_of__searchStrings_11() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____searchStrings_11)); }
	inline StringU5BU5D_t1642385972* get__searchStrings_11() const { return ____searchStrings_11; }
	inline StringU5BU5D_t1642385972** get_address_of__searchStrings_11() { return &____searchStrings_11; }
	inline void set__searchStrings_11(StringU5BU5D_t1642385972* value)
	{
		____searchStrings_11 = value;
		Il2CppCodeGenWriteBarrier(&____searchStrings_11, value);
	}

	inline static int32_t get_offset_of__replaceStrings_12() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____replaceStrings_12)); }
	inline StringU5BU5D_t1642385972* get__replaceStrings_12() const { return ____replaceStrings_12; }
	inline StringU5BU5D_t1642385972** get_address_of__replaceStrings_12() { return &____replaceStrings_12; }
	inline void set__replaceStrings_12(StringU5BU5D_t1642385972* value)
	{
		____replaceStrings_12 = value;
		Il2CppCodeGenWriteBarrier(&____replaceStrings_12, value);
	}

	inline static int32_t get_offset_of__inNestedIndex_13() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inNestedIndex_13)); }
	inline int32_t get__inNestedIndex_13() const { return ____inNestedIndex_13; }
	inline int32_t* get_address_of__inNestedIndex_13() { return &____inNestedIndex_13; }
	inline void set__inNestedIndex_13(int32_t value)
	{
		____inNestedIndex_13 = value;
	}

	inline static int32_t get_offset_of__inNestedSize_14() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inNestedSize_14)); }
	inline int32_t get__inNestedSize_14() const { return ____inNestedSize_14; }
	inline int32_t* get_address_of__inNestedSize_14() { return &____inNestedSize_14; }
	inline void set__inNestedSize_14(int32_t value)
	{
		____inNestedSize_14 = value;
	}

	inline static int32_t get_offset_of__inNestedString_15() { return static_cast<int32_t>(offsetof(Tokenizer_t395826351, ____inNestedString_15)); }
	inline String_t* get__inNestedString_15() const { return ____inNestedString_15; }
	inline String_t** get_address_of__inNestedString_15() { return &____inNestedString_15; }
	inline void set__inNestedString_15(String_t* value)
	{
		____inNestedString_15 = value;
		Il2CppCodeGenWriteBarrier(&____inNestedString_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
