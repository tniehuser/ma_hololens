﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.RuntimeStructs/MonoClass
struct  MonoClass_t2595527713 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MonoClass_t2595527713__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
