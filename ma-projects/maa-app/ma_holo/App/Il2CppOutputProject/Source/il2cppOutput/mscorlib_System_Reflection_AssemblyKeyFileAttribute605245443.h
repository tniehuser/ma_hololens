﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyKeyFileAttribute
struct  AssemblyKeyFileAttribute_t605245443  : public Attribute_t542643598
{
public:
	// System.String System.Reflection.AssemblyKeyFileAttribute::m_keyFile
	String_t* ___m_keyFile_0;

public:
	inline static int32_t get_offset_of_m_keyFile_0() { return static_cast<int32_t>(offsetof(AssemblyKeyFileAttribute_t605245443, ___m_keyFile_0)); }
	inline String_t* get_m_keyFile_0() const { return ___m_keyFile_0; }
	inline String_t** get_address_of_m_keyFile_0() { return &___m_keyFile_0; }
	inline void set_m_keyFile_0(String_t* value)
	{
		___m_keyFile_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_keyFile_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
