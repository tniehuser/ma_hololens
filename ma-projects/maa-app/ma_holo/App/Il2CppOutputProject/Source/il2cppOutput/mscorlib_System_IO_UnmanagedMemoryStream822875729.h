﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"

// System.Runtime.InteropServices.SafeBuffer
struct SafeBuffer_t699764933;
// System.Byte
struct Byte_t3683104436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.UnmanagedMemoryStream
struct  UnmanagedMemoryStream_t822875729  : public Stream_t3255436806
{
public:
	// System.Runtime.InteropServices.SafeBuffer System.IO.UnmanagedMemoryStream::_buffer
	SafeBuffer_t699764933 * ____buffer_8;
	// System.Byte* System.IO.UnmanagedMemoryStream::_mem
	uint8_t* ____mem_9;
	// System.Int64 System.IO.UnmanagedMemoryStream::_length
	int64_t ____length_10;
	// System.Int64 System.IO.UnmanagedMemoryStream::_capacity
	int64_t ____capacity_11;
	// System.Int64 System.IO.UnmanagedMemoryStream::_position
	int64_t ____position_12;
	// System.Int64 System.IO.UnmanagedMemoryStream::_offset
	int64_t ____offset_13;
	// System.IO.FileAccess System.IO.UnmanagedMemoryStream::_access
	int32_t ____access_14;
	// System.Boolean System.IO.UnmanagedMemoryStream::_isOpen
	bool ____isOpen_15;

public:
	inline static int32_t get_offset_of__buffer_8() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____buffer_8)); }
	inline SafeBuffer_t699764933 * get__buffer_8() const { return ____buffer_8; }
	inline SafeBuffer_t699764933 ** get_address_of__buffer_8() { return &____buffer_8; }
	inline void set__buffer_8(SafeBuffer_t699764933 * value)
	{
		____buffer_8 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_8, value);
	}

	inline static int32_t get_offset_of__mem_9() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____mem_9)); }
	inline uint8_t* get__mem_9() const { return ____mem_9; }
	inline uint8_t** get_address_of__mem_9() { return &____mem_9; }
	inline void set__mem_9(uint8_t* value)
	{
		____mem_9 = value;
	}

	inline static int32_t get_offset_of__length_10() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____length_10)); }
	inline int64_t get__length_10() const { return ____length_10; }
	inline int64_t* get_address_of__length_10() { return &____length_10; }
	inline void set__length_10(int64_t value)
	{
		____length_10 = value;
	}

	inline static int32_t get_offset_of__capacity_11() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____capacity_11)); }
	inline int64_t get__capacity_11() const { return ____capacity_11; }
	inline int64_t* get_address_of__capacity_11() { return &____capacity_11; }
	inline void set__capacity_11(int64_t value)
	{
		____capacity_11 = value;
	}

	inline static int32_t get_offset_of__position_12() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____position_12)); }
	inline int64_t get__position_12() const { return ____position_12; }
	inline int64_t* get_address_of__position_12() { return &____position_12; }
	inline void set__position_12(int64_t value)
	{
		____position_12 = value;
	}

	inline static int32_t get_offset_of__offset_13() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____offset_13)); }
	inline int64_t get__offset_13() const { return ____offset_13; }
	inline int64_t* get_address_of__offset_13() { return &____offset_13; }
	inline void set__offset_13(int64_t value)
	{
		____offset_13 = value;
	}

	inline static int32_t get_offset_of__access_14() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____access_14)); }
	inline int32_t get__access_14() const { return ____access_14; }
	inline int32_t* get_address_of__access_14() { return &____access_14; }
	inline void set__access_14(int32_t value)
	{
		____access_14 = value;
	}

	inline static int32_t get_offset_of__isOpen_15() { return static_cast<int32_t>(offsetof(UnmanagedMemoryStream_t822875729, ____isOpen_15)); }
	inline bool get__isOpen_15() const { return ____isOpen_15; }
	inline bool* get_address_of__isOpen_15() { return &____isOpen_15; }
	inline void set__isOpen_15(bool value)
	{
		____isOpen_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
