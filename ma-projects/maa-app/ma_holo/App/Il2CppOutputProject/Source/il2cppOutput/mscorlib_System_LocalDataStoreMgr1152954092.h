﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Collections.Generic.List`1<System.LocalDataStore>
struct List_1_t3892906904;
// System.Collections.Generic.Dictionary`2<System.String,System.LocalDataStoreSlot>
struct Dictionary_2_t2401110462;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalDataStoreMgr
struct  LocalDataStoreMgr_t1152954092  : public Il2CppObject
{
public:
	// System.Boolean[] System.LocalDataStoreMgr::m_SlotInfoTable
	BooleanU5BU5D_t3568034315* ___m_SlotInfoTable_0;
	// System.Int32 System.LocalDataStoreMgr::m_FirstAvailableSlot
	int32_t ___m_FirstAvailableSlot_1;
	// System.Collections.Generic.List`1<System.LocalDataStore> System.LocalDataStoreMgr::m_ManagedLocalDataStores
	List_1_t3892906904 * ___m_ManagedLocalDataStores_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.LocalDataStoreSlot> System.LocalDataStoreMgr::m_KeyToSlotMap
	Dictionary_2_t2401110462 * ___m_KeyToSlotMap_3;
	// System.Int64 System.LocalDataStoreMgr::m_CookieGenerator
	int64_t ___m_CookieGenerator_4;

public:
	inline static int32_t get_offset_of_m_SlotInfoTable_0() { return static_cast<int32_t>(offsetof(LocalDataStoreMgr_t1152954092, ___m_SlotInfoTable_0)); }
	inline BooleanU5BU5D_t3568034315* get_m_SlotInfoTable_0() const { return ___m_SlotInfoTable_0; }
	inline BooleanU5BU5D_t3568034315** get_address_of_m_SlotInfoTable_0() { return &___m_SlotInfoTable_0; }
	inline void set_m_SlotInfoTable_0(BooleanU5BU5D_t3568034315* value)
	{
		___m_SlotInfoTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_SlotInfoTable_0, value);
	}

	inline static int32_t get_offset_of_m_FirstAvailableSlot_1() { return static_cast<int32_t>(offsetof(LocalDataStoreMgr_t1152954092, ___m_FirstAvailableSlot_1)); }
	inline int32_t get_m_FirstAvailableSlot_1() const { return ___m_FirstAvailableSlot_1; }
	inline int32_t* get_address_of_m_FirstAvailableSlot_1() { return &___m_FirstAvailableSlot_1; }
	inline void set_m_FirstAvailableSlot_1(int32_t value)
	{
		___m_FirstAvailableSlot_1 = value;
	}

	inline static int32_t get_offset_of_m_ManagedLocalDataStores_2() { return static_cast<int32_t>(offsetof(LocalDataStoreMgr_t1152954092, ___m_ManagedLocalDataStores_2)); }
	inline List_1_t3892906904 * get_m_ManagedLocalDataStores_2() const { return ___m_ManagedLocalDataStores_2; }
	inline List_1_t3892906904 ** get_address_of_m_ManagedLocalDataStores_2() { return &___m_ManagedLocalDataStores_2; }
	inline void set_m_ManagedLocalDataStores_2(List_1_t3892906904 * value)
	{
		___m_ManagedLocalDataStores_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ManagedLocalDataStores_2, value);
	}

	inline static int32_t get_offset_of_m_KeyToSlotMap_3() { return static_cast<int32_t>(offsetof(LocalDataStoreMgr_t1152954092, ___m_KeyToSlotMap_3)); }
	inline Dictionary_2_t2401110462 * get_m_KeyToSlotMap_3() const { return ___m_KeyToSlotMap_3; }
	inline Dictionary_2_t2401110462 ** get_address_of_m_KeyToSlotMap_3() { return &___m_KeyToSlotMap_3; }
	inline void set_m_KeyToSlotMap_3(Dictionary_2_t2401110462 * value)
	{
		___m_KeyToSlotMap_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_KeyToSlotMap_3, value);
	}

	inline static int32_t get_offset_of_m_CookieGenerator_4() { return static_cast<int32_t>(offsetof(LocalDataStoreMgr_t1152954092, ___m_CookieGenerator_4)); }
	inline int64_t get_m_CookieGenerator_4() const { return ___m_CookieGenerator_4; }
	inline int64_t* get_address_of_m_CookieGenerator_4() { return &___m_CookieGenerator_4; }
	inline void set_m_CookieGenerator_4(int64_t value)
	{
		___m_CookieGenerator_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
