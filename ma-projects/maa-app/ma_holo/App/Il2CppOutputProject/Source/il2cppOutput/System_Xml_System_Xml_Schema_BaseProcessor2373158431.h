﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t2971213394;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t2373158431  : public Il2CppObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t2971213394 * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_0, value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___schemaNames_1)); }
	inline SchemaNames_t1619962557 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t1619962557 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier(&___schemaNames_1, value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___eventHandler_2)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___eventHandler_2, value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t2971213394 * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t2971213394 ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t2971213394 * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier(&___compilationSettings_3, value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier(&___NsXml_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
