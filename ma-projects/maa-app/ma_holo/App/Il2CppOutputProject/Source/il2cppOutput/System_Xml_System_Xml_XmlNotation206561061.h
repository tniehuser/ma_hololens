﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t206561061  : public XmlNode_t616554813
{
public:
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_1;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_2;
	// System.String System.Xml.XmlNotation::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_publicId_1() { return static_cast<int32_t>(offsetof(XmlNotation_t206561061, ___publicId_1)); }
	inline String_t* get_publicId_1() const { return ___publicId_1; }
	inline String_t** get_address_of_publicId_1() { return &___publicId_1; }
	inline void set_publicId_1(String_t* value)
	{
		___publicId_1 = value;
		Il2CppCodeGenWriteBarrier(&___publicId_1, value);
	}

	inline static int32_t get_offset_of_systemId_2() { return static_cast<int32_t>(offsetof(XmlNotation_t206561061, ___systemId_2)); }
	inline String_t* get_systemId_2() const { return ___systemId_2; }
	inline String_t** get_address_of_systemId_2() { return &___systemId_2; }
	inline void set_systemId_2(String_t* value)
	{
		___systemId_2 = value;
		Il2CppCodeGenWriteBarrier(&___systemId_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XmlNotation_t206561061, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
