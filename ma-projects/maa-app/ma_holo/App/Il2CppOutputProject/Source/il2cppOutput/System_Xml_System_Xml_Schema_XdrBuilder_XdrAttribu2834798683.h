﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_System_Xml_Schema_SchemaNames_Token1005517746.h"

// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.XdrBuilder/XdrBuildFunction
struct XdrBuildFunction_t4291501283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/XdrAttributeEntry
struct  XdrAttributeEntry_t2834798683  : public Il2CppObject
{
public:
	// System.Xml.Schema.SchemaNames/Token System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_Attribute
	int32_t ____Attribute_0;
	// System.Int32 System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_SchemaFlags
	int32_t ____SchemaFlags_1;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_Datatype
	XmlSchemaDatatype_t1195946242 * ____Datatype_2;
	// System.Xml.Schema.XdrBuilder/XdrBuildFunction System.Xml.Schema.XdrBuilder/XdrAttributeEntry::_BuildFunc
	XdrBuildFunction_t4291501283 * ____BuildFunc_3;

public:
	inline static int32_t get_offset_of__Attribute_0() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t2834798683, ____Attribute_0)); }
	inline int32_t get__Attribute_0() const { return ____Attribute_0; }
	inline int32_t* get_address_of__Attribute_0() { return &____Attribute_0; }
	inline void set__Attribute_0(int32_t value)
	{
		____Attribute_0 = value;
	}

	inline static int32_t get_offset_of__SchemaFlags_1() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t2834798683, ____SchemaFlags_1)); }
	inline int32_t get__SchemaFlags_1() const { return ____SchemaFlags_1; }
	inline int32_t* get_address_of__SchemaFlags_1() { return &____SchemaFlags_1; }
	inline void set__SchemaFlags_1(int32_t value)
	{
		____SchemaFlags_1 = value;
	}

	inline static int32_t get_offset_of__Datatype_2() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t2834798683, ____Datatype_2)); }
	inline XmlSchemaDatatype_t1195946242 * get__Datatype_2() const { return ____Datatype_2; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of__Datatype_2() { return &____Datatype_2; }
	inline void set__Datatype_2(XmlSchemaDatatype_t1195946242 * value)
	{
		____Datatype_2 = value;
		Il2CppCodeGenWriteBarrier(&____Datatype_2, value);
	}

	inline static int32_t get_offset_of__BuildFunc_3() { return static_cast<int32_t>(offsetof(XdrAttributeEntry_t2834798683, ____BuildFunc_3)); }
	inline XdrBuildFunction_t4291501283 * get__BuildFunc_3() const { return ____BuildFunc_3; }
	inline XdrBuildFunction_t4291501283 ** get_address_of__BuildFunc_3() { return &____BuildFunc_3; }
	inline void set__BuildFunc_3(XdrBuildFunction_t4291501283 * value)
	{
		____BuildFunc_3 = value;
		Il2CppCodeGenWriteBarrier(&____BuildFunc_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
