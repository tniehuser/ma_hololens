﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_EventAttributes2989788983.h"

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t3308873219;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t3203592177;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t644187984;
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t4238041457;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.EventBuilder
struct  EventBuilder_t2927243889  : public Il2CppObject
{
public:
	// System.String System.Reflection.Emit.EventBuilder::name
	String_t* ___name_0;
	// System.Type System.Reflection.Emit.EventBuilder::type
	Type_t * ___type_1;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.EventBuilder::typeb
	TypeBuilder_t3308873219 * ___typeb_2;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.EventBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_3;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::add_method
	MethodBuilder_t644187984 * ___add_method_4;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::remove_method
	MethodBuilder_t644187984 * ___remove_method_5;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::raise_method
	MethodBuilder_t644187984 * ___raise_method_6;
	// System.Reflection.Emit.MethodBuilder[] System.Reflection.Emit.EventBuilder::other_methods
	MethodBuilderU5BU5D_t4238041457* ___other_methods_7;
	// System.Reflection.EventAttributes System.Reflection.Emit.EventBuilder::attrs
	int32_t ___attrs_8;
	// System.Int32 System.Reflection.Emit.EventBuilder::table_idx
	int32_t ___table_idx_9;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_typeb_2() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___typeb_2)); }
	inline TypeBuilder_t3308873219 * get_typeb_2() const { return ___typeb_2; }
	inline TypeBuilder_t3308873219 ** get_address_of_typeb_2() { return &___typeb_2; }
	inline void set_typeb_2(TypeBuilder_t3308873219 * value)
	{
		___typeb_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeb_2, value);
	}

	inline static int32_t get_offset_of_cattrs_3() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___cattrs_3)); }
	inline CustomAttributeBuilderU5BU5D_t3203592177* get_cattrs_3() const { return ___cattrs_3; }
	inline CustomAttributeBuilderU5BU5D_t3203592177** get_address_of_cattrs_3() { return &___cattrs_3; }
	inline void set_cattrs_3(CustomAttributeBuilderU5BU5D_t3203592177* value)
	{
		___cattrs_3 = value;
		Il2CppCodeGenWriteBarrier(&___cattrs_3, value);
	}

	inline static int32_t get_offset_of_add_method_4() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___add_method_4)); }
	inline MethodBuilder_t644187984 * get_add_method_4() const { return ___add_method_4; }
	inline MethodBuilder_t644187984 ** get_address_of_add_method_4() { return &___add_method_4; }
	inline void set_add_method_4(MethodBuilder_t644187984 * value)
	{
		___add_method_4 = value;
		Il2CppCodeGenWriteBarrier(&___add_method_4, value);
	}

	inline static int32_t get_offset_of_remove_method_5() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___remove_method_5)); }
	inline MethodBuilder_t644187984 * get_remove_method_5() const { return ___remove_method_5; }
	inline MethodBuilder_t644187984 ** get_address_of_remove_method_5() { return &___remove_method_5; }
	inline void set_remove_method_5(MethodBuilder_t644187984 * value)
	{
		___remove_method_5 = value;
		Il2CppCodeGenWriteBarrier(&___remove_method_5, value);
	}

	inline static int32_t get_offset_of_raise_method_6() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___raise_method_6)); }
	inline MethodBuilder_t644187984 * get_raise_method_6() const { return ___raise_method_6; }
	inline MethodBuilder_t644187984 ** get_address_of_raise_method_6() { return &___raise_method_6; }
	inline void set_raise_method_6(MethodBuilder_t644187984 * value)
	{
		___raise_method_6 = value;
		Il2CppCodeGenWriteBarrier(&___raise_method_6, value);
	}

	inline static int32_t get_offset_of_other_methods_7() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___other_methods_7)); }
	inline MethodBuilderU5BU5D_t4238041457* get_other_methods_7() const { return ___other_methods_7; }
	inline MethodBuilderU5BU5D_t4238041457** get_address_of_other_methods_7() { return &___other_methods_7; }
	inline void set_other_methods_7(MethodBuilderU5BU5D_t4238041457* value)
	{
		___other_methods_7 = value;
		Il2CppCodeGenWriteBarrier(&___other_methods_7, value);
	}

	inline static int32_t get_offset_of_attrs_8() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___attrs_8)); }
	inline int32_t get_attrs_8() const { return ___attrs_8; }
	inline int32_t* get_address_of_attrs_8() { return &___attrs_8; }
	inline void set_attrs_8(int32_t value)
	{
		___attrs_8 = value;
	}

	inline static int32_t get_offset_of_table_idx_9() { return static_cast<int32_t>(offsetof(EventBuilder_t2927243889, ___table_idx_9)); }
	inline int32_t get_table_idx_9() const { return ___table_idx_9; }
	inline int32_t* get_address_of_table_idx_9() { return &___table_idx_9; }
	inline void set_table_idx_9(int32_t value)
	{
		___table_idx_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.EventBuilder
struct EventBuilder_t2927243889_marshaled_pinvoke
{
	char* ___name_0;
	Type_t * ___type_1;
	TypeBuilder_t3308873219 * ___typeb_2;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_3;
	MethodBuilder_t644187984 * ___add_method_4;
	MethodBuilder_t644187984 * ___remove_method_5;
	MethodBuilder_t644187984 * ___raise_method_6;
	MethodBuilderU5BU5D_t4238041457* ___other_methods_7;
	int32_t ___attrs_8;
	int32_t ___table_idx_9;
};
// Native definition for COM marshalling of System.Reflection.Emit.EventBuilder
struct EventBuilder_t2927243889_marshaled_com
{
	Il2CppChar* ___name_0;
	Type_t * ___type_1;
	TypeBuilder_t3308873219 * ___typeb_2;
	CustomAttributeBuilderU5BU5D_t3203592177* ___cattrs_3;
	MethodBuilder_t644187984 * ___add_method_4;
	MethodBuilder_t644187984 * ___remove_method_5;
	MethodBuilder_t644187984 * ___raise_method_6;
	MethodBuilderU5BU5D_t4238041457* ___other_methods_7;
	int32_t ___attrs_8;
	int32_t ___table_idx_9;
};
