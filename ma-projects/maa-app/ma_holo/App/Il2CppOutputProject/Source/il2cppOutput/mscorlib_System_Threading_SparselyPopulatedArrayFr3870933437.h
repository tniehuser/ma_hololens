﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Threading.SparselyPopulatedArrayFragment`1<System.Object>
struct SparselyPopulatedArrayFragment_1_t3870933437;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SparselyPopulatedArrayFragment`1<System.Object>
struct  SparselyPopulatedArrayFragment_1_t3870933437  : public Il2CppObject
{
public:
	// T[] System.Threading.SparselyPopulatedArrayFragment`1::m_elements
	ObjectU5BU5D_t3614634134* ___m_elements_0;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SparselyPopulatedArrayFragment`1::m_freeCount
	int32_t ___m_freeCount_1;
	// System.Threading.SparselyPopulatedArrayFragment`1<T> modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SparselyPopulatedArrayFragment`1::m_next
	SparselyPopulatedArrayFragment_1_t3870933437 * ___m_next_2;
	// System.Threading.SparselyPopulatedArrayFragment`1<T> modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.SparselyPopulatedArrayFragment`1::m_prev
	SparselyPopulatedArrayFragment_1_t3870933437 * ___m_prev_3;

public:
	inline static int32_t get_offset_of_m_elements_0() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayFragment_1_t3870933437, ___m_elements_0)); }
	inline ObjectU5BU5D_t3614634134* get_m_elements_0() const { return ___m_elements_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_elements_0() { return &___m_elements_0; }
	inline void set_m_elements_0(ObjectU5BU5D_t3614634134* value)
	{
		___m_elements_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_elements_0, value);
	}

	inline static int32_t get_offset_of_m_freeCount_1() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayFragment_1_t3870933437, ___m_freeCount_1)); }
	inline int32_t get_m_freeCount_1() const { return ___m_freeCount_1; }
	inline int32_t* get_address_of_m_freeCount_1() { return &___m_freeCount_1; }
	inline void set_m_freeCount_1(int32_t value)
	{
		___m_freeCount_1 = value;
	}

	inline static int32_t get_offset_of_m_next_2() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayFragment_1_t3870933437, ___m_next_2)); }
	inline SparselyPopulatedArrayFragment_1_t3870933437 * get_m_next_2() const { return ___m_next_2; }
	inline SparselyPopulatedArrayFragment_1_t3870933437 ** get_address_of_m_next_2() { return &___m_next_2; }
	inline void set_m_next_2(SparselyPopulatedArrayFragment_1_t3870933437 * value)
	{
		___m_next_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_next_2, value);
	}

	inline static int32_t get_offset_of_m_prev_3() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayFragment_1_t3870933437, ___m_prev_3)); }
	inline SparselyPopulatedArrayFragment_1_t3870933437 * get_m_prev_3() const { return ___m_prev_3; }
	inline SparselyPopulatedArrayFragment_1_t3870933437 ** get_address_of_m_prev_3() { return &___m_prev_3; }
	inline void set_m_prev_3(SparselyPopulatedArrayFragment_1_t3870933437 * value)
	{
		___m_prev_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_prev_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
