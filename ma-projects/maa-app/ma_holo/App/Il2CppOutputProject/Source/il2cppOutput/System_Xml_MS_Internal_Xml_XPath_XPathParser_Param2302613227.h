﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_MS_Internal_Xml_XPath_Function_Function2662603618.h"

// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.XPathParser/ParamInfo
struct  ParamInfo_t2302613227  : public Il2CppObject
{
public:
	// MS.Internal.Xml.XPath.Function/FunctionType MS.Internal.Xml.XPath.XPathParser/ParamInfo::ftype
	int32_t ___ftype_0;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::minargs
	int32_t ___minargs_1;
	// System.Int32 MS.Internal.Xml.XPath.XPathParser/ParamInfo::maxargs
	int32_t ___maxargs_2;
	// System.Xml.XPath.XPathResultType[] MS.Internal.Xml.XPath.XPathParser/ParamInfo::argTypes
	XPathResultTypeU5BU5D_t2966113519* ___argTypes_3;

public:
	inline static int32_t get_offset_of_ftype_0() { return static_cast<int32_t>(offsetof(ParamInfo_t2302613227, ___ftype_0)); }
	inline int32_t get_ftype_0() const { return ___ftype_0; }
	inline int32_t* get_address_of_ftype_0() { return &___ftype_0; }
	inline void set_ftype_0(int32_t value)
	{
		___ftype_0 = value;
	}

	inline static int32_t get_offset_of_minargs_1() { return static_cast<int32_t>(offsetof(ParamInfo_t2302613227, ___minargs_1)); }
	inline int32_t get_minargs_1() const { return ___minargs_1; }
	inline int32_t* get_address_of_minargs_1() { return &___minargs_1; }
	inline void set_minargs_1(int32_t value)
	{
		___minargs_1 = value;
	}

	inline static int32_t get_offset_of_maxargs_2() { return static_cast<int32_t>(offsetof(ParamInfo_t2302613227, ___maxargs_2)); }
	inline int32_t get_maxargs_2() const { return ___maxargs_2; }
	inline int32_t* get_address_of_maxargs_2() { return &___maxargs_2; }
	inline void set_maxargs_2(int32_t value)
	{
		___maxargs_2 = value;
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(ParamInfo_t2302613227, ___argTypes_3)); }
	inline XPathResultTypeU5BU5D_t2966113519* get_argTypes_3() const { return ___argTypes_3; }
	inline XPathResultTypeU5BU5D_t2966113519** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(XPathResultTypeU5BU5D_t2966113519* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___argTypes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
