﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.ComponentModel.WeakHashtable
struct WeakHashtable_t1679685894;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Diagnostics.BooleanSwitch
struct BooleanSwitch_t1490001656;
// System.Guid[]
struct GuidU5BU5D_t3556289988;
// System.Object
struct Il2CppObject;
// System.ComponentModel.RefreshEventHandler
struct RefreshEventHandler_t456069287;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeDescriptor
struct  TypeDescriptor_t3595688691  : public Il2CppObject
{
public:

public:
};

struct TypeDescriptor_t3595688691_StaticFields
{
public:
	// System.ComponentModel.WeakHashtable System.ComponentModel.TypeDescriptor::_providerTable
	WeakHashtable_t1679685894 * ____providerTable_0;
	// System.Collections.Hashtable System.ComponentModel.TypeDescriptor::_providerTypeTable
	Hashtable_t909839986 * ____providerTypeTable_1;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeDescriptor::_defaultProviders
	Hashtable_t909839986 * ____defaultProviders_2;
	// System.Int32 System.ComponentModel.TypeDescriptor::_metadataVersion
	int32_t ____metadataVersion_3;
	// System.Int32 System.ComponentModel.TypeDescriptor::_collisionIndex
	int32_t ____collisionIndex_4;
	// System.Diagnostics.BooleanSwitch System.ComponentModel.TypeDescriptor::TraceDescriptor
	BooleanSwitch_t1490001656 * ___TraceDescriptor_5;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineInitializeKeys
	GuidU5BU5D_t3556289988* ____pipelineInitializeKeys_6;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineMergeKeys
	GuidU5BU5D_t3556289988* ____pipelineMergeKeys_7;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineFilterKeys
	GuidU5BU5D_t3556289988* ____pipelineFilterKeys_8;
	// System.Guid[] System.ComponentModel.TypeDescriptor::_pipelineAttributeFilterKeys
	GuidU5BU5D_t3556289988* ____pipelineAttributeFilterKeys_9;
	// System.Object System.ComponentModel.TypeDescriptor::_internalSyncObject
	Il2CppObject * ____internalSyncObject_10;
	// System.ComponentModel.RefreshEventHandler System.ComponentModel.TypeDescriptor::Refreshed
	RefreshEventHandler_t456069287 * ___Refreshed_11;

public:
	inline static int32_t get_offset_of__providerTable_0() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____providerTable_0)); }
	inline WeakHashtable_t1679685894 * get__providerTable_0() const { return ____providerTable_0; }
	inline WeakHashtable_t1679685894 ** get_address_of__providerTable_0() { return &____providerTable_0; }
	inline void set__providerTable_0(WeakHashtable_t1679685894 * value)
	{
		____providerTable_0 = value;
		Il2CppCodeGenWriteBarrier(&____providerTable_0, value);
	}

	inline static int32_t get_offset_of__providerTypeTable_1() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____providerTypeTable_1)); }
	inline Hashtable_t909839986 * get__providerTypeTable_1() const { return ____providerTypeTable_1; }
	inline Hashtable_t909839986 ** get_address_of__providerTypeTable_1() { return &____providerTypeTable_1; }
	inline void set__providerTypeTable_1(Hashtable_t909839986 * value)
	{
		____providerTypeTable_1 = value;
		Il2CppCodeGenWriteBarrier(&____providerTypeTable_1, value);
	}

	inline static int32_t get_offset_of__defaultProviders_2() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____defaultProviders_2)); }
	inline Hashtable_t909839986 * get__defaultProviders_2() const { return ____defaultProviders_2; }
	inline Hashtable_t909839986 ** get_address_of__defaultProviders_2() { return &____defaultProviders_2; }
	inline void set__defaultProviders_2(Hashtable_t909839986 * value)
	{
		____defaultProviders_2 = value;
		Il2CppCodeGenWriteBarrier(&____defaultProviders_2, value);
	}

	inline static int32_t get_offset_of__metadataVersion_3() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____metadataVersion_3)); }
	inline int32_t get__metadataVersion_3() const { return ____metadataVersion_3; }
	inline int32_t* get_address_of__metadataVersion_3() { return &____metadataVersion_3; }
	inline void set__metadataVersion_3(int32_t value)
	{
		____metadataVersion_3 = value;
	}

	inline static int32_t get_offset_of__collisionIndex_4() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____collisionIndex_4)); }
	inline int32_t get__collisionIndex_4() const { return ____collisionIndex_4; }
	inline int32_t* get_address_of__collisionIndex_4() { return &____collisionIndex_4; }
	inline void set__collisionIndex_4(int32_t value)
	{
		____collisionIndex_4 = value;
	}

	inline static int32_t get_offset_of_TraceDescriptor_5() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___TraceDescriptor_5)); }
	inline BooleanSwitch_t1490001656 * get_TraceDescriptor_5() const { return ___TraceDescriptor_5; }
	inline BooleanSwitch_t1490001656 ** get_address_of_TraceDescriptor_5() { return &___TraceDescriptor_5; }
	inline void set_TraceDescriptor_5(BooleanSwitch_t1490001656 * value)
	{
		___TraceDescriptor_5 = value;
		Il2CppCodeGenWriteBarrier(&___TraceDescriptor_5, value);
	}

	inline static int32_t get_offset_of__pipelineInitializeKeys_6() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____pipelineInitializeKeys_6)); }
	inline GuidU5BU5D_t3556289988* get__pipelineInitializeKeys_6() const { return ____pipelineInitializeKeys_6; }
	inline GuidU5BU5D_t3556289988** get_address_of__pipelineInitializeKeys_6() { return &____pipelineInitializeKeys_6; }
	inline void set__pipelineInitializeKeys_6(GuidU5BU5D_t3556289988* value)
	{
		____pipelineInitializeKeys_6 = value;
		Il2CppCodeGenWriteBarrier(&____pipelineInitializeKeys_6, value);
	}

	inline static int32_t get_offset_of__pipelineMergeKeys_7() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____pipelineMergeKeys_7)); }
	inline GuidU5BU5D_t3556289988* get__pipelineMergeKeys_7() const { return ____pipelineMergeKeys_7; }
	inline GuidU5BU5D_t3556289988** get_address_of__pipelineMergeKeys_7() { return &____pipelineMergeKeys_7; }
	inline void set__pipelineMergeKeys_7(GuidU5BU5D_t3556289988* value)
	{
		____pipelineMergeKeys_7 = value;
		Il2CppCodeGenWriteBarrier(&____pipelineMergeKeys_7, value);
	}

	inline static int32_t get_offset_of__pipelineFilterKeys_8() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____pipelineFilterKeys_8)); }
	inline GuidU5BU5D_t3556289988* get__pipelineFilterKeys_8() const { return ____pipelineFilterKeys_8; }
	inline GuidU5BU5D_t3556289988** get_address_of__pipelineFilterKeys_8() { return &____pipelineFilterKeys_8; }
	inline void set__pipelineFilterKeys_8(GuidU5BU5D_t3556289988* value)
	{
		____pipelineFilterKeys_8 = value;
		Il2CppCodeGenWriteBarrier(&____pipelineFilterKeys_8, value);
	}

	inline static int32_t get_offset_of__pipelineAttributeFilterKeys_9() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____pipelineAttributeFilterKeys_9)); }
	inline GuidU5BU5D_t3556289988* get__pipelineAttributeFilterKeys_9() const { return ____pipelineAttributeFilterKeys_9; }
	inline GuidU5BU5D_t3556289988** get_address_of__pipelineAttributeFilterKeys_9() { return &____pipelineAttributeFilterKeys_9; }
	inline void set__pipelineAttributeFilterKeys_9(GuidU5BU5D_t3556289988* value)
	{
		____pipelineAttributeFilterKeys_9 = value;
		Il2CppCodeGenWriteBarrier(&____pipelineAttributeFilterKeys_9, value);
	}

	inline static int32_t get_offset_of__internalSyncObject_10() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ____internalSyncObject_10)); }
	inline Il2CppObject * get__internalSyncObject_10() const { return ____internalSyncObject_10; }
	inline Il2CppObject ** get_address_of__internalSyncObject_10() { return &____internalSyncObject_10; }
	inline void set__internalSyncObject_10(Il2CppObject * value)
	{
		____internalSyncObject_10 = value;
		Il2CppCodeGenWriteBarrier(&____internalSyncObject_10, value);
	}

	inline static int32_t get_offset_of_Refreshed_11() { return static_cast<int32_t>(offsetof(TypeDescriptor_t3595688691_StaticFields, ___Refreshed_11)); }
	inline RefreshEventHandler_t456069287 * get_Refreshed_11() const { return ___Refreshed_11; }
	inline RefreshEventHandler_t456069287 ** get_address_of_Refreshed_11() { return &___Refreshed_11; }
	inline void set_Refreshed_11(RefreshEventHandler_t456069287 * value)
	{
		___Refreshed_11 = value;
		Il2CppCodeGenWriteBarrier(&___Refreshed_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
