﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Reflection_TypeInfo3822613806.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"

// System.RuntimeType
struct RuntimeType_t2836228502;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.MonoTypeInfo
struct MonoTypeInfo_t1976057079;
// System.Object
struct Il2CppObject;
// System.Reflection.RuntimeConstructorInfo
struct RuntimeConstructorInfo_t311714390;
// System.Collections.Generic.Dictionary`2<System.Guid,System.Type>
struct Dictionary_2_t837519208;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1646117627;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeType
struct  RuntimeType_t2836228502  : public TypeInfo_t3822613806
{
public:
	// System.MonoTypeInfo System.RuntimeType::type_info
	MonoTypeInfo_t1976057079 * ___type_info_24;
	// System.Object System.RuntimeType::GenericCache
	Il2CppObject * ___GenericCache_25;
	// System.Reflection.RuntimeConstructorInfo System.RuntimeType::m_serializationCtor
	RuntimeConstructorInfo_t311714390 * ___m_serializationCtor_26;

public:
	inline static int32_t get_offset_of_type_info_24() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502, ___type_info_24)); }
	inline MonoTypeInfo_t1976057079 * get_type_info_24() const { return ___type_info_24; }
	inline MonoTypeInfo_t1976057079 ** get_address_of_type_info_24() { return &___type_info_24; }
	inline void set_type_info_24(MonoTypeInfo_t1976057079 * value)
	{
		___type_info_24 = value;
		Il2CppCodeGenWriteBarrier(&___type_info_24, value);
	}

	inline static int32_t get_offset_of_GenericCache_25() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502, ___GenericCache_25)); }
	inline Il2CppObject * get_GenericCache_25() const { return ___GenericCache_25; }
	inline Il2CppObject ** get_address_of_GenericCache_25() { return &___GenericCache_25; }
	inline void set_GenericCache_25(Il2CppObject * value)
	{
		___GenericCache_25 = value;
		Il2CppCodeGenWriteBarrier(&___GenericCache_25, value);
	}

	inline static int32_t get_offset_of_m_serializationCtor_26() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502, ___m_serializationCtor_26)); }
	inline RuntimeConstructorInfo_t311714390 * get_m_serializationCtor_26() const { return ___m_serializationCtor_26; }
	inline RuntimeConstructorInfo_t311714390 ** get_address_of_m_serializationCtor_26() { return &___m_serializationCtor_26; }
	inline void set_m_serializationCtor_26(RuntimeConstructorInfo_t311714390 * value)
	{
		___m_serializationCtor_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_serializationCtor_26, value);
	}
};

struct RuntimeType_t2836228502_StaticFields
{
public:
	// System.RuntimeType System.RuntimeType::ValueType
	RuntimeType_t2836228502 * ___ValueType_8;
	// System.RuntimeType System.RuntimeType::EnumType
	RuntimeType_t2836228502 * ___EnumType_9;
	// System.RuntimeType System.RuntimeType::ObjectType
	RuntimeType_t2836228502 * ___ObjectType_10;
	// System.RuntimeType System.RuntimeType::StringType
	RuntimeType_t2836228502 * ___StringType_11;
	// System.RuntimeType System.RuntimeType::DelegateType
	RuntimeType_t2836228502 * ___DelegateType_12;
	// System.Type[] System.RuntimeType::s_SICtorParamTypes
	TypeU5BU5D_t1664964607* ___s_SICtorParamTypes_13;
	// System.RuntimeType System.RuntimeType::s_typedRef
	RuntimeType_t2836228502 * ___s_typedRef_23;
	// System.Collections.Generic.Dictionary`2<System.Guid,System.Type> System.RuntimeType::clsid_types
	Dictionary_2_t837519208 * ___clsid_types_27;
	// System.Reflection.Emit.AssemblyBuilder System.RuntimeType::clsid_assemblybuilder
	AssemblyBuilder_t1646117627 * ___clsid_assemblybuilder_28;

public:
	inline static int32_t get_offset_of_ValueType_8() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___ValueType_8)); }
	inline RuntimeType_t2836228502 * get_ValueType_8() const { return ___ValueType_8; }
	inline RuntimeType_t2836228502 ** get_address_of_ValueType_8() { return &___ValueType_8; }
	inline void set_ValueType_8(RuntimeType_t2836228502 * value)
	{
		___ValueType_8 = value;
		Il2CppCodeGenWriteBarrier(&___ValueType_8, value);
	}

	inline static int32_t get_offset_of_EnumType_9() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___EnumType_9)); }
	inline RuntimeType_t2836228502 * get_EnumType_9() const { return ___EnumType_9; }
	inline RuntimeType_t2836228502 ** get_address_of_EnumType_9() { return &___EnumType_9; }
	inline void set_EnumType_9(RuntimeType_t2836228502 * value)
	{
		___EnumType_9 = value;
		Il2CppCodeGenWriteBarrier(&___EnumType_9, value);
	}

	inline static int32_t get_offset_of_ObjectType_10() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___ObjectType_10)); }
	inline RuntimeType_t2836228502 * get_ObjectType_10() const { return ___ObjectType_10; }
	inline RuntimeType_t2836228502 ** get_address_of_ObjectType_10() { return &___ObjectType_10; }
	inline void set_ObjectType_10(RuntimeType_t2836228502 * value)
	{
		___ObjectType_10 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectType_10, value);
	}

	inline static int32_t get_offset_of_StringType_11() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___StringType_11)); }
	inline RuntimeType_t2836228502 * get_StringType_11() const { return ___StringType_11; }
	inline RuntimeType_t2836228502 ** get_address_of_StringType_11() { return &___StringType_11; }
	inline void set_StringType_11(RuntimeType_t2836228502 * value)
	{
		___StringType_11 = value;
		Il2CppCodeGenWriteBarrier(&___StringType_11, value);
	}

	inline static int32_t get_offset_of_DelegateType_12() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___DelegateType_12)); }
	inline RuntimeType_t2836228502 * get_DelegateType_12() const { return ___DelegateType_12; }
	inline RuntimeType_t2836228502 ** get_address_of_DelegateType_12() { return &___DelegateType_12; }
	inline void set_DelegateType_12(RuntimeType_t2836228502 * value)
	{
		___DelegateType_12 = value;
		Il2CppCodeGenWriteBarrier(&___DelegateType_12, value);
	}

	inline static int32_t get_offset_of_s_SICtorParamTypes_13() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___s_SICtorParamTypes_13)); }
	inline TypeU5BU5D_t1664964607* get_s_SICtorParamTypes_13() const { return ___s_SICtorParamTypes_13; }
	inline TypeU5BU5D_t1664964607** get_address_of_s_SICtorParamTypes_13() { return &___s_SICtorParamTypes_13; }
	inline void set_s_SICtorParamTypes_13(TypeU5BU5D_t1664964607* value)
	{
		___s_SICtorParamTypes_13 = value;
		Il2CppCodeGenWriteBarrier(&___s_SICtorParamTypes_13, value);
	}

	inline static int32_t get_offset_of_s_typedRef_23() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___s_typedRef_23)); }
	inline RuntimeType_t2836228502 * get_s_typedRef_23() const { return ___s_typedRef_23; }
	inline RuntimeType_t2836228502 ** get_address_of_s_typedRef_23() { return &___s_typedRef_23; }
	inline void set_s_typedRef_23(RuntimeType_t2836228502 * value)
	{
		___s_typedRef_23 = value;
		Il2CppCodeGenWriteBarrier(&___s_typedRef_23, value);
	}

	inline static int32_t get_offset_of_clsid_types_27() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___clsid_types_27)); }
	inline Dictionary_2_t837519208 * get_clsid_types_27() const { return ___clsid_types_27; }
	inline Dictionary_2_t837519208 ** get_address_of_clsid_types_27() { return &___clsid_types_27; }
	inline void set_clsid_types_27(Dictionary_2_t837519208 * value)
	{
		___clsid_types_27 = value;
		Il2CppCodeGenWriteBarrier(&___clsid_types_27, value);
	}

	inline static int32_t get_offset_of_clsid_assemblybuilder_28() { return static_cast<int32_t>(offsetof(RuntimeType_t2836228502_StaticFields, ___clsid_assemblybuilder_28)); }
	inline AssemblyBuilder_t1646117627 * get_clsid_assemblybuilder_28() const { return ___clsid_assemblybuilder_28; }
	inline AssemblyBuilder_t1646117627 ** get_address_of_clsid_assemblybuilder_28() { return &___clsid_assemblybuilder_28; }
	inline void set_clsid_assemblybuilder_28(AssemblyBuilder_t1646117627 * value)
	{
		___clsid_assemblybuilder_28 = value;
		Il2CppCodeGenWriteBarrier(&___clsid_assemblybuilder_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
