﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Diagnostics_TypedElement4034655484.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ListenerElement
struct  ListenerElement_t2632490878  : public TypedElement_t4034655484
{
public:
	// System.Configuration.ConfigurationProperty System.Diagnostics.ListenerElement::_propListenerTypeName
	ConfigurationProperty_t2048066811 * ____propListenerTypeName_23;
	// System.Boolean System.Diagnostics.ListenerElement::_allowReferences
	bool ____allowReferences_24;
	// System.Collections.Hashtable System.Diagnostics.ListenerElement::_attributes
	Hashtable_t909839986 * ____attributes_25;
	// System.Boolean System.Diagnostics.ListenerElement::_isAddedByDefault
	bool ____isAddedByDefault_26;

public:
	inline static int32_t get_offset_of__propListenerTypeName_23() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878, ____propListenerTypeName_23)); }
	inline ConfigurationProperty_t2048066811 * get__propListenerTypeName_23() const { return ____propListenerTypeName_23; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propListenerTypeName_23() { return &____propListenerTypeName_23; }
	inline void set__propListenerTypeName_23(ConfigurationProperty_t2048066811 * value)
	{
		____propListenerTypeName_23 = value;
		Il2CppCodeGenWriteBarrier(&____propListenerTypeName_23, value);
	}

	inline static int32_t get_offset_of__allowReferences_24() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878, ____allowReferences_24)); }
	inline bool get__allowReferences_24() const { return ____allowReferences_24; }
	inline bool* get_address_of__allowReferences_24() { return &____allowReferences_24; }
	inline void set__allowReferences_24(bool value)
	{
		____allowReferences_24 = value;
	}

	inline static int32_t get_offset_of__attributes_25() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878, ____attributes_25)); }
	inline Hashtable_t909839986 * get__attributes_25() const { return ____attributes_25; }
	inline Hashtable_t909839986 ** get_address_of__attributes_25() { return &____attributes_25; }
	inline void set__attributes_25(Hashtable_t909839986 * value)
	{
		____attributes_25 = value;
		Il2CppCodeGenWriteBarrier(&____attributes_25, value);
	}

	inline static int32_t get_offset_of__isAddedByDefault_26() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878, ____isAddedByDefault_26)); }
	inline bool get__isAddedByDefault_26() const { return ____isAddedByDefault_26; }
	inline bool* get_address_of__isAddedByDefault_26() { return &____isAddedByDefault_26; }
	inline void set__isAddedByDefault_26(bool value)
	{
		____isAddedByDefault_26 = value;
	}
};

struct ListenerElement_t2632490878_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Diagnostics.ListenerElement::_propFilter
	ConfigurationProperty_t2048066811 * ____propFilter_20;
	// System.Configuration.ConfigurationProperty System.Diagnostics.ListenerElement::_propName
	ConfigurationProperty_t2048066811 * ____propName_21;
	// System.Configuration.ConfigurationProperty System.Diagnostics.ListenerElement::_propOutputOpts
	ConfigurationProperty_t2048066811 * ____propOutputOpts_22;

public:
	inline static int32_t get_offset_of__propFilter_20() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878_StaticFields, ____propFilter_20)); }
	inline ConfigurationProperty_t2048066811 * get__propFilter_20() const { return ____propFilter_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propFilter_20() { return &____propFilter_20; }
	inline void set__propFilter_20(ConfigurationProperty_t2048066811 * value)
	{
		____propFilter_20 = value;
		Il2CppCodeGenWriteBarrier(&____propFilter_20, value);
	}

	inline static int32_t get_offset_of__propName_21() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878_StaticFields, ____propName_21)); }
	inline ConfigurationProperty_t2048066811 * get__propName_21() const { return ____propName_21; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propName_21() { return &____propName_21; }
	inline void set__propName_21(ConfigurationProperty_t2048066811 * value)
	{
		____propName_21 = value;
		Il2CppCodeGenWriteBarrier(&____propName_21, value);
	}

	inline static int32_t get_offset_of__propOutputOpts_22() { return static_cast<int32_t>(offsetof(ListenerElement_t2632490878_StaticFields, ____propOutputOpts_22)); }
	inline ConfigurationProperty_t2048066811 * get__propOutputOpts_22() const { return ____propOutputOpts_22; }
	inline ConfigurationProperty_t2048066811 ** get_address_of__propOutputOpts_22() { return &____propOutputOpts_22; }
	inline void set__propOutputOpts_22(ConfigurationProperty_t2048066811 * value)
	{
		____propOutputOpts_22 = value;
		Il2CppCodeGenWriteBarrier(&____propOutputOpts_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
