﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.XmlNode
struct XmlNode_t616554813;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t616554813  : public Il2CppObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t616554813 * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t616554813, ___parentNode_0)); }
	inline XmlNode_t616554813 * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t616554813 ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t616554813 * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___parentNode_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
