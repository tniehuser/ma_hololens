﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_UnauthorizedAccessException886535555.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3067050131.h"
#include "mscorlib_System_UnhandledExceptionEventHandler1916531888.h"
#include "mscorlib_System_UnitySerializationHolder2045574117.h"
#include "mscorlib_System_UnSafeCharBuffer927766464.h"
#include "mscorlib_System_Version1755874712.h"
#include "mscorlib_System_Version_ParseFailureKind1129561897.h"
#include "mscorlib_System_Version_VersionResult785420859.h"
#include "mscorlib_Interop1365440873.h"
#include "mscorlib_Interop_mincore140566379.h"
#include "mscorlib_Interop_mincore_SYSTEMTIME2580015906.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMA2883369749.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMA1590223312.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMAT757325618.h"
#include "mscorlib_Interop_mincore_TIME_DYNAMIC_ZONE_INFORMAT748200035.h"
#include "mscorlib_Interop_mincore_TIME_ZONE_INFORMATION2855177307.h"
#include "mscorlib_Interop_mincore_TIME_ZONE_INFORMATION_U3C1060306023.h"
#include "mscorlib_Interop_mincore_TIME_ZONE_INFORMATION_U3CD186285493.h"
#include "mscorlib_Microsoft_Win32_Registry1325736645.h"
#include "mscorlib_Microsoft_Win32_RegistryHive2561794591.h"
#include "mscorlib_Microsoft_Win32_RegistryKey2287932784.h"
#include "mscorlib_Microsoft_Win32_RegistryKeyPermissionCheck460149061.h"
#include "mscorlib_Microsoft_Win32_RegistryValueKind3069013676.h"
#include "mscorlib_Microsoft_Win32_RegistryValueOptions3181579340.h"
#include "mscorlib_Microsoft_Win32_ExpandString1083116867.h"
#include "mscorlib_Microsoft_Win32_RegistryKeyComparer284335197.h"
#include "mscorlib_Microsoft_Win32_KeyHandler1744274711.h"
#include "mscorlib_Microsoft_Win32_UnixRegistryApi2447246231.h"
#include "mscorlib_Microsoft_Win32_Win32RegistryApi2321265302.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex1073906970.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo1449609243.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction1673853792.h"
#include "mscorlib_Mono_Globalization_Unicode_ContractionCom1150321365.h"
#include "mscorlib_Mono_Globalization_Unicode_Level2Map3322505726.h"
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicod1231687219.h"
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicod2040269023.h"
#include "mscorlib_System_Text_NormalizationCheck3690318147.h"
#include "mscorlib_System_Text_Normalization1914561689.h"
#include "mscorlib_Mono_Globalization_Unicode_NormalizationT1112052683.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator4081201584.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator2636657155.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_581002487.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_169451053.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator1556892101.h"
#include "mscorlib_System_Globalization_SortKey1270563137.h"
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBuffer1759538423.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManaged892502321.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_Key2001522803.h"
#include "mscorlib_Mono_Security_UriPartial2274729157.h"
#include "mscorlib_Mono_Security_Uri1510604476.h"
#include "mscorlib_Mono_Security_Uri_UriScheme683497865.h"
#include "mscorlib_Mono_Xml_SecurityParser30730985.h"
#include "mscorlib_Mono_Xml_SmallXmlParser3549787957.h"
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImpl4015491015.h"
#include "mscorlib_Mono_Xml_SmallXmlParserException2094031034.h"
#include "mscorlib_Mono_Runtime530188530.h"
#include "mscorlib_Mono_RuntimeClassHandle2775506138.h"
#include "mscorlib_Mono_RuntimeRemoteClassHandle2923639406.h"
#include "mscorlib_Mono_RuntimeGenericParamInfoHandle580017048.h"
#include "mscorlib_Mono_RuntimeEventHandle798447448.h"
#include "mscorlib_Mono_RuntimePropertyHandle3120528081.h"
#include "mscorlib_Mono_RuntimeGPtrArrayHandle1303258952.h"
#include "mscorlib_Mono_RuntimeMarshal3798850754.h"
#include "mscorlib_Mono_RuntimeStructs3054717562.h"
#include "mscorlib_Mono_RuntimeStructs_RemoteClass3863182804.h"
#include "mscorlib_Mono_RuntimeStructs_MonoClass2595527713.h"
#include "mscorlib_Mono_RuntimeStructs_GenericParamInfo1377222196.h"
#include "mscorlib_Mono_RuntimeStructs_GPtrArray3128553612.h"
#include "mscorlib_Mono_MonoAssemblyName1886479188.h"
#include "mscorlib_Mono_MonoAssemblyName_U3Cpublic_key_token1384907566.h"
#include "mscorlib_Mono_SafeGPtrArrayHandle547714345.h"
#include "mscorlib_Mono_SafeStringMarshal2486501886.h"
#include "mscorlib_System_AppContextSwitches4151153076.h"
#include "mscorlib_System_AppDomain2719102437.h"
#include "mscorlib_System_CLRConfig500485483.h"
#include "mscorlib_System_Globalization_CultureData3400086592.h"
#include "mscorlib_System_Globalization_CodePageDataItem2022420531.h"
#include "mscorlib_System_Globalization_EncodingTable1040637825.h"
#include "mscorlib_System_Globalization_InternalEncodingDataIt82919681.h"
#include "mscorlib_System_Globalization_InternalCodePageData1742109366.h"
#include "mscorlib_System_Environment3662374671.h"
#include "mscorlib_System_Environment_SpecialFolder1519540278.h"
#include "mscorlib_System_Environment_SpecialFolderOption2597972107.h"
#include "mscorlib_System_Runtime_CompilerServices_JitHelpers643465074.h"
#include "mscorlib_System_ParseNumbers3923564175.h"
#include "mscorlib_System_IO_PathInternal1852797604.h"
#include "mscorlib_System_MonoTypeInfo1976057079.h"
#include "mscorlib_System_Globalization_TextInfoToUpperData983078748.h"
#include "mscorlib_System_Globalization_TextInfoToLowerData630797643.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilderInstant3297116808.h"
#include "mscorlib_System_TypeNameParser95097906.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyH4147282775.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyV1223556284.h"
#include "mscorlib_System_Diagnostics_Debugger4214909883.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize900 = { sizeof (UInt64_t2909196914)+ sizeof (Il2CppObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable900[1] = 
{
	UInt64_t2909196914::get_offset_of_m_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize901 = { sizeof (UnauthorizedAccessException_t886535555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize902 = { sizeof (UnhandledExceptionEventArgs_t3067050131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable902[2] = 
{
	UnhandledExceptionEventArgs_t3067050131::get_offset_of__Exception_1(),
	UnhandledExceptionEventArgs_t3067050131::get_offset_of__IsTerminating_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize903 = { sizeof (UnhandledExceptionEventHandler_t1916531888), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize904 = { sizeof (UnitySerializationHolder_t2045574117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable904[8] = 
{
	UnitySerializationHolder_t2045574117::get_offset_of_m_instantiation_0(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_elementTypes_1(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_genericParameterPosition_2(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_declaringType_3(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_declaringMethod_4(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_data_5(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_assemblyName_6(),
	UnitySerializationHolder_t2045574117::get_offset_of_m_unityType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize905 = { sizeof (UnSafeCharBuffer_t927766464)+ sizeof (Il2CppObject), sizeof(UnSafeCharBuffer_t927766464_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable905[3] = 
{
	UnSafeCharBuffer_t927766464::get_offset_of_m_buffer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnSafeCharBuffer_t927766464::get_offset_of_m_totalSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UnSafeCharBuffer_t927766464::get_offset_of_m_length_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize906 = { sizeof (Version_t1755874712), -1, sizeof(Version_t1755874712_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable906[6] = 
{
	Version_t1755874712::get_offset_of__Major_0(),
	Version_t1755874712::get_offset_of__Minor_1(),
	Version_t1755874712::get_offset_of__Build_2(),
	Version_t1755874712::get_offset_of__Revision_3(),
	Version_t1755874712_StaticFields::get_offset_of_SeparatorsArray_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize907 = { sizeof (ParseFailureKind_t1129561897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable907[5] = 
{
	ParseFailureKind_t1129561897::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize908 = { sizeof (VersionResult_t785420859)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable908[5] = 
{
	VersionResult_t785420859::get_offset_of_m_parsedVersion_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VersionResult_t785420859::get_offset_of_m_failure_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VersionResult_t785420859::get_offset_of_m_exceptionArgument_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VersionResult_t785420859::get_offset_of_m_argumentName_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VersionResult_t785420859::get_offset_of_m_canThrow_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize909 = { sizeof (Interop_t1365440873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize910 = { sizeof (mincore_t140566379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize911 = { sizeof (SYSTEMTIME_t2580015906)+ sizeof (Il2CppObject), sizeof(SYSTEMTIME_t2580015906 ), 0, 0 };
extern const int32_t g_FieldOffsetTable911[8] = 
{
	SYSTEMTIME_t2580015906::get_offset_of_wYear_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wMonth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wDayOfWeek_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wDay_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wHour_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wMinute_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wSecond_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SYSTEMTIME_t2580015906::get_offset_of_wMilliseconds_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize912 = { sizeof (TIME_DYNAMIC_ZONE_INFORMATION_t2883369749)+ sizeof (Il2CppObject), sizeof(TIME_DYNAMIC_ZONE_INFORMATION_t2883369749 ), 0, 0 };
extern const int32_t g_FieldOffsetTable912[9] = 
{
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_Bias_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_StandardName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_StandardDate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_StandardBias_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_DaylightName_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_DaylightDate_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_DaylightBias_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_TimeZoneKeyName_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_DYNAMIC_ZONE_INFORMATION_t2883369749::get_offset_of_DynamicDaylightTimeDisabled_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize913 = { sizeof (U3CStandardNameU3E__FixedBuffer0_t1590223312)+ sizeof (Il2CppObject), sizeof(U3CStandardNameU3E__FixedBuffer0_t1590223312 ), 0, 0 };
extern const int32_t g_FieldOffsetTable913[1] = 
{
	U3CStandardNameU3E__FixedBuffer0_t1590223312::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize914 = { sizeof (U3CDaylightNameU3E__FixedBuffer1_t757325618)+ sizeof (Il2CppObject), sizeof(U3CDaylightNameU3E__FixedBuffer1_t757325618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable914[1] = 
{
	U3CDaylightNameU3E__FixedBuffer1_t757325618::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize915 = { sizeof (U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035)+ sizeof (Il2CppObject), sizeof(U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035 ), 0, 0 };
extern const int32_t g_FieldOffsetTable915[1] = 
{
	U3CTimeZoneKeyNameU3E__FixedBuffer2_t748200035::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize916 = { sizeof (TIME_ZONE_INFORMATION_t2855177307)+ sizeof (Il2CppObject), sizeof(TIME_ZONE_INFORMATION_t2855177307 ), 0, 0 };
extern const int32_t g_FieldOffsetTable916[7] = 
{
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_Bias_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_StandardName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_StandardDate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_StandardBias_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_DaylightName_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_DaylightDate_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TIME_ZONE_INFORMATION_t2855177307::get_offset_of_DaylightBias_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize917 = { sizeof (U3CStandardNameU3E__FixedBuffer3_t1060306023)+ sizeof (Il2CppObject), sizeof(U3CStandardNameU3E__FixedBuffer3_t1060306023 ), 0, 0 };
extern const int32_t g_FieldOffsetTable917[1] = 
{
	U3CStandardNameU3E__FixedBuffer3_t1060306023::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize918 = { sizeof (U3CDaylightNameU3E__FixedBuffer4_t186285493)+ sizeof (Il2CppObject), sizeof(U3CDaylightNameU3E__FixedBuffer4_t186285493 ), 0, 0 };
extern const int32_t g_FieldOffsetTable918[1] = 
{
	U3CDaylightNameU3E__FixedBuffer4_t186285493::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize920 = { sizeof (Registry_t1325736645), -1, sizeof(Registry_t1325736645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable920[8] = 
{
	Registry_t1325736645_StaticFields::get_offset_of_ClassesRoot_0(),
	Registry_t1325736645_StaticFields::get_offset_of_CurrentConfig_1(),
	Registry_t1325736645_StaticFields::get_offset_of_CurrentUser_2(),
	Registry_t1325736645_StaticFields::get_offset_of_DynData_3(),
	Registry_t1325736645_StaticFields::get_offset_of_LocalMachine_4(),
	Registry_t1325736645_StaticFields::get_offset_of_PerformanceData_5(),
	Registry_t1325736645_StaticFields::get_offset_of_Users_6(),
	Registry_t1325736645_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize921 = { sizeof (RegistryHive_t2561794591)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable921[8] = 
{
	RegistryHive_t2561794591::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize922 = { sizeof (RegistryKey_t2287932784), -1, sizeof(RegistryKey_t2287932784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable922[7] = 
{
	RegistryKey_t2287932784::get_offset_of_handle_1(),
	RegistryKey_t2287932784::get_offset_of_safe_handle_2(),
	RegistryKey_t2287932784::get_offset_of_hive_3(),
	RegistryKey_t2287932784::get_offset_of_qname_4(),
	RegistryKey_t2287932784::get_offset_of_isRemoteRoot_5(),
	RegistryKey_t2287932784::get_offset_of_isWritable_6(),
	RegistryKey_t2287932784_StaticFields::get_offset_of_RegistryApi_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize923 = { sizeof (RegistryKeyPermissionCheck_t460149061)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable923[4] = 
{
	RegistryKeyPermissionCheck_t460149061::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize924 = { sizeof (RegistryValueKind_t3069013676)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable924[9] = 
{
	RegistryValueKind_t3069013676::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize925 = { sizeof (RegistryValueOptions_t3181579340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable925[3] = 
{
	RegistryValueOptions_t3181579340::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize926 = { sizeof (ExpandString_t1083116867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable926[1] = 
{
	ExpandString_t1083116867::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize927 = { sizeof (RegistryKeyComparer_t284335197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize928 = { sizeof (KeyHandler_t1744274711), -1, sizeof(KeyHandler_t1744274711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable928[10] = 
{
	KeyHandler_t1744274711_StaticFields::get_offset_of_key_to_handler_0(),
	KeyHandler_t1744274711_StaticFields::get_offset_of_dir_to_handler_1(),
	KeyHandler_t1744274711::get_offset_of_Dir_2(),
	KeyHandler_t1744274711::get_offset_of_ActualDir_3(),
	KeyHandler_t1744274711::get_offset_of_IsVolatile_4(),
	KeyHandler_t1744274711::get_offset_of_values_5(),
	KeyHandler_t1744274711::get_offset_of_file_6(),
	KeyHandler_t1744274711::get_offset_of_dirty_7(),
	KeyHandler_t1744274711_StaticFields::get_offset_of_user_store_8(),
	KeyHandler_t1744274711_StaticFields::get_offset_of_machine_store_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize929 = { sizeof (UnixRegistryApi_t2447246231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize930 = { sizeof (Win32RegistryApi_t2321265302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable930[1] = 
{
	Win32RegistryApi_t2321265302::get_offset_of_NativeBytesPerCharacter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize931 = { sizeof (CodePointIndexer_t1073906970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable931[4] = 
{
	CodePointIndexer_t1073906970::get_offset_of_ranges_0(),
	CodePointIndexer_t1073906970::get_offset_of_TotalCount_1(),
	CodePointIndexer_t1073906970::get_offset_of_defaultIndex_2(),
	CodePointIndexer_t1073906970::get_offset_of_defaultCP_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize932 = { sizeof (TableRange_t2011406615)+ sizeof (Il2CppObject), sizeof(TableRange_t2011406615 ), 0, 0 };
extern const int32_t g_FieldOffsetTable932[5] = 
{
	TableRange_t2011406615::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TableRange_t2011406615::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TableRange_t2011406615::get_offset_of_Count_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TableRange_t2011406615::get_offset_of_IndexStart_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TableRange_t2011406615::get_offset_of_IndexEnd_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize933 = { sizeof (TailoringInfo_t1449609243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable933[4] = 
{
	TailoringInfo_t1449609243::get_offset_of_LCID_0(),
	TailoringInfo_t1449609243::get_offset_of_TailoringIndex_1(),
	TailoringInfo_t1449609243::get_offset_of_TailoringCount_2(),
	TailoringInfo_t1449609243::get_offset_of_FrenchSort_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize934 = { sizeof (Contraction_t1673853792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable934[4] = 
{
	Contraction_t1673853792::get_offset_of_Index_0(),
	Contraction_t1673853792::get_offset_of_Source_1(),
	Contraction_t1673853792::get_offset_of_Replacement_2(),
	Contraction_t1673853792::get_offset_of_SortKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize935 = { sizeof (ContractionComparer_t1150321365), -1, sizeof(ContractionComparer_t1150321365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable935[1] = 
{
	ContractionComparer_t1150321365_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize936 = { sizeof (Level2Map_t3322505726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable936[2] = 
{
	Level2Map_t3322505726::get_offset_of_Source_0(),
	Level2Map_t3322505726::get_offset_of_Replace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize937 = { sizeof (MSCompatUnicodeTable_t1231687219), -1, sizeof(MSCompatUnicodeTable_t1231687219_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable937[20] = 
{
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_MaxExpansionLength_0(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_ignorableFlags_1(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_categories_2(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_level1_3(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_level2_4(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_level3_5(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkCHScategory_6(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkCHTcategory_7(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkJAcategory_8(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkKOcategory_9(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkCHSlv1_10(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkCHTlv1_11(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkJAlv1_12(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkKOlv1_13(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_cjkKOlv2_14(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_tailoringArr_15(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_tailoringInfos_16(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_forLock_17(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_isReady_18(),
	MSCompatUnicodeTable_t1231687219_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize938 = { sizeof (MSCompatUnicodeTableUtil_t2040269023), -1, sizeof(MSCompatUnicodeTableUtil_t2040269023_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable938[7] = 
{
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Ignorable_0(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Category_1(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Level1_2(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Level2_3(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Level3_4(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_CjkCHS_5(),
	MSCompatUnicodeTableUtil_t2040269023_StaticFields::get_offset_of_Cjk_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize939 = { sizeof (NormalizationCheck_t3690318147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable939[4] = 
{
	NormalizationCheck_t3690318147::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize940 = { sizeof (Normalization_t1914561689), -1, sizeof(Normalization_t1914561689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable940[8] = 
{
	Normalization_t1914561689_StaticFields::get_offset_of_props_0(),
	Normalization_t1914561689_StaticFields::get_offset_of_mappedChars_1(),
	Normalization_t1914561689_StaticFields::get_offset_of_charMapIndex_2(),
	Normalization_t1914561689_StaticFields::get_offset_of_helperIndex_3(),
	Normalization_t1914561689_StaticFields::get_offset_of_mapIdxToComposite_4(),
	Normalization_t1914561689_StaticFields::get_offset_of_combiningClass_5(),
	Normalization_t1914561689_StaticFields::get_offset_of_forLock_6(),
	Normalization_t1914561689_StaticFields::get_offset_of_isReady_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize941 = { sizeof (NormalizationTableUtil_t1112052683), -1, sizeof(NormalizationTableUtil_t1112052683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable941[5] = 
{
	NormalizationTableUtil_t1112052683_StaticFields::get_offset_of_Prop_0(),
	NormalizationTableUtil_t1112052683_StaticFields::get_offset_of_Map_1(),
	NormalizationTableUtil_t1112052683_StaticFields::get_offset_of_Combining_2(),
	NormalizationTableUtil_t1112052683_StaticFields::get_offset_of_Composite_3(),
	NormalizationTableUtil_t1112052683_StaticFields::get_offset_of_Helper_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize942 = { sizeof (SimpleCollator_t4081201584), -1, sizeof(SimpleCollator_t4081201584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable942[13] = 
{
	SimpleCollator_t4081201584_StaticFields::get_offset_of_QuickCheckDisabled_0(),
	SimpleCollator_t4081201584_StaticFields::get_offset_of_invariant_1(),
	SimpleCollator_t4081201584::get_offset_of_textInfo_2(),
	SimpleCollator_t4081201584::get_offset_of_cjkIndexer_3(),
	SimpleCollator_t4081201584::get_offset_of_contractions_4(),
	SimpleCollator_t4081201584::get_offset_of_level2Maps_5(),
	SimpleCollator_t4081201584::get_offset_of_unsafeFlags_6(),
	SimpleCollator_t4081201584::get_offset_of_cjkCatTable_7(),
	SimpleCollator_t4081201584::get_offset_of_cjkLv1Table_8(),
	SimpleCollator_t4081201584::get_offset_of_cjkLv2Table_9(),
	SimpleCollator_t4081201584::get_offset_of_cjkLv2Indexer_10(),
	SimpleCollator_t4081201584::get_offset_of_lcid_11(),
	SimpleCollator_t4081201584::get_offset_of_frenchSort_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize943 = { sizeof (Context_t2636657155)+ sizeof (Il2CppObject), sizeof(Context_t2636657155_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable943[7] = 
{
	Context_t2636657155::get_offset_of_Option_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_NeverMatchFlags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_AlwaysMatchFlags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_Buffer1_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_Buffer2_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_PrevCode_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Context_t2636657155::get_offset_of_PrevSortKey_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize944 = { sizeof (PreviousInfo_t581002487)+ sizeof (Il2CppObject), sizeof(PreviousInfo_t581002487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable944[2] = 
{
	PreviousInfo_t581002487::get_offset_of_Code_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PreviousInfo_t581002487::get_offset_of_SortKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize945 = { sizeof (Escape_t169451053)+ sizeof (Il2CppObject), sizeof(Escape_t169451053_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable945[5] = 
{
	Escape_t169451053::get_offset_of_Source_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Escape_t169451053::get_offset_of_Index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Escape_t169451053::get_offset_of_Start_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Escape_t169451053::get_offset_of_End_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Escape_t169451053::get_offset_of_Optional_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize946 = { sizeof (ExtenderType_t1556892101)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable946[6] = 
{
	ExtenderType_t1556892101::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize947 = { sizeof (SortKey_t1270563137), sizeof(SortKey_t1270563137_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable947[4] = 
{
	SortKey_t1270563137::get_offset_of_source_0(),
	SortKey_t1270563137::get_offset_of_key_1(),
	SortKey_t1270563137::get_offset_of_options_2(),
	SortKey_t1270563137::get_offset_of_lcid_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize948 = { sizeof (SortKeyBuffer_t1759538423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable948[22] = 
{
	SortKeyBuffer_t1759538423::get_offset_of_l1b_0(),
	SortKeyBuffer_t1759538423::get_offset_of_l2b_1(),
	SortKeyBuffer_t1759538423::get_offset_of_l3b_2(),
	SortKeyBuffer_t1759538423::get_offset_of_l4sb_3(),
	SortKeyBuffer_t1759538423::get_offset_of_l4tb_4(),
	SortKeyBuffer_t1759538423::get_offset_of_l4kb_5(),
	SortKeyBuffer_t1759538423::get_offset_of_l4wb_6(),
	SortKeyBuffer_t1759538423::get_offset_of_l5b_7(),
	SortKeyBuffer_t1759538423::get_offset_of_source_8(),
	SortKeyBuffer_t1759538423::get_offset_of_l1_9(),
	SortKeyBuffer_t1759538423::get_offset_of_l2_10(),
	SortKeyBuffer_t1759538423::get_offset_of_l3_11(),
	SortKeyBuffer_t1759538423::get_offset_of_l4s_12(),
	SortKeyBuffer_t1759538423::get_offset_of_l4t_13(),
	SortKeyBuffer_t1759538423::get_offset_of_l4k_14(),
	SortKeyBuffer_t1759538423::get_offset_of_l4w_15(),
	SortKeyBuffer_t1759538423::get_offset_of_l5_16(),
	SortKeyBuffer_t1759538423::get_offset_of_lcid_17(),
	SortKeyBuffer_t1759538423::get_offset_of_options_18(),
	SortKeyBuffer_t1759538423::get_offset_of_processLevel2_19(),
	SortKeyBuffer_t1759538423::get_offset_of_frenchSort_20(),
	SortKeyBuffer_t1759538423::get_offset_of_frenchSorted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize949 = { sizeof (DSAManaged_t892502321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable949[13] = 
{
	DSAManaged_t892502321::get_offset_of_keypairGenerated_2(),
	DSAManaged_t892502321::get_offset_of_m_disposed_3(),
	DSAManaged_t892502321::get_offset_of_p_4(),
	DSAManaged_t892502321::get_offset_of_q_5(),
	DSAManaged_t892502321::get_offset_of_g_6(),
	DSAManaged_t892502321::get_offset_of_x_7(),
	DSAManaged_t892502321::get_offset_of_y_8(),
	DSAManaged_t892502321::get_offset_of_j_9(),
	DSAManaged_t892502321::get_offset_of_seed_10(),
	DSAManaged_t892502321::get_offset_of_counter_11(),
	DSAManaged_t892502321::get_offset_of_j_missing_12(),
	DSAManaged_t892502321::get_offset_of_rng_13(),
	DSAManaged_t892502321::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize950 = { sizeof (KeyGeneratedEventHandler_t2001522803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize951 = { sizeof (UriPartial_t2274729157)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable951[4] = 
{
	UriPartial_t2274729157::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize952 = { sizeof (Uri_t1510604476), -1, sizeof(Uri_t1510604476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable952[28] = 
{
	Uri_t1510604476::get_offset_of_isUnixFilePath_0(),
	Uri_t1510604476::get_offset_of_source_1(),
	Uri_t1510604476::get_offset_of_scheme_2(),
	Uri_t1510604476::get_offset_of_host_3(),
	Uri_t1510604476::get_offset_of_port_4(),
	Uri_t1510604476::get_offset_of_path_5(),
	Uri_t1510604476::get_offset_of_query_6(),
	Uri_t1510604476::get_offset_of_fragment_7(),
	Uri_t1510604476::get_offset_of_userinfo_8(),
	Uri_t1510604476::get_offset_of_isUnc_9(),
	Uri_t1510604476::get_offset_of_isOpaquePart_10(),
	Uri_t1510604476::get_offset_of_userEscaped_11(),
	Uri_t1510604476::get_offset_of_cachedToString_12(),
	Uri_t1510604476::get_offset_of_cachedLocalPath_13(),
	Uri_t1510604476::get_offset_of_cachedHashCode_14(),
	Uri_t1510604476::get_offset_of_reduce_15(),
	Uri_t1510604476_StaticFields::get_offset_of_hexUpperChars_16(),
	Uri_t1510604476_StaticFields::get_offset_of_SchemeDelimiter_17(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeFile_18(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeFtp_19(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeGopher_20(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeHttp_21(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeHttps_22(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeMailto_23(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeNews_24(),
	Uri_t1510604476_StaticFields::get_offset_of_UriSchemeNntp_25(),
	Uri_t1510604476_StaticFields::get_offset_of_schemes_26(),
	Uri_t1510604476_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize953 = { sizeof (UriScheme_t683497865)+ sizeof (Il2CppObject), sizeof(UriScheme_t683497865_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable953[3] = 
{
	UriScheme_t683497865::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t683497865::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t683497865::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize954 = { sizeof (SecurityParser_t30730985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable954[3] = 
{
	SecurityParser_t30730985::get_offset_of_root_12(),
	SecurityParser_t30730985::get_offset_of_current_13(),
	SecurityParser_t30730985::get_offset_of_stack_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize955 = { sizeof (SmallXmlParser_t3549787957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable955[12] = 
{
	SmallXmlParser_t3549787957::get_offset_of_handler_0(),
	SmallXmlParser_t3549787957::get_offset_of_reader_1(),
	SmallXmlParser_t3549787957::get_offset_of_elementNames_2(),
	SmallXmlParser_t3549787957::get_offset_of_xmlSpaces_3(),
	SmallXmlParser_t3549787957::get_offset_of_xmlSpace_4(),
	SmallXmlParser_t3549787957::get_offset_of_buffer_5(),
	SmallXmlParser_t3549787957::get_offset_of_nameBuffer_6(),
	SmallXmlParser_t3549787957::get_offset_of_isWhitespace_7(),
	SmallXmlParser_t3549787957::get_offset_of_attributes_8(),
	SmallXmlParser_t3549787957::get_offset_of_line_9(),
	SmallXmlParser_t3549787957::get_offset_of_column_10(),
	SmallXmlParser_t3549787957::get_offset_of_resetColumn_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize956 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize958 = { sizeof (AttrListImpl_t4015491015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable958[2] = 
{
	AttrListImpl_t4015491015::get_offset_of_attrNames_0(),
	AttrListImpl_t4015491015::get_offset_of_attrValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize959 = { sizeof (SmallXmlParserException_t2094031034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable959[2] = 
{
	SmallXmlParserException_t2094031034::get_offset_of_line_16(),
	SmallXmlParserException_t2094031034::get_offset_of_column_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize960 = { sizeof (Runtime_t530188530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize961 = { sizeof (RuntimeClassHandle_t2775506138)+ sizeof (Il2CppObject), sizeof(RuntimeClassHandle_t2775506138_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable961[1] = 
{
	RuntimeClassHandle_t2775506138::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize962 = { sizeof (RuntimeRemoteClassHandle_t2923639406)+ sizeof (Il2CppObject), sizeof(RuntimeRemoteClassHandle_t2923639406_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable962[1] = 
{
	RuntimeRemoteClassHandle_t2923639406::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize963 = { sizeof (RuntimeGenericParamInfoHandle_t580017048)+ sizeof (Il2CppObject), sizeof(RuntimeGenericParamInfoHandle_t580017048_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable963[1] = 
{
	RuntimeGenericParamInfoHandle_t580017048::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize964 = { sizeof (RuntimeEventHandle_t798447448)+ sizeof (Il2CppObject), sizeof(RuntimeEventHandle_t798447448 ), 0, 0 };
extern const int32_t g_FieldOffsetTable964[1] = 
{
	RuntimeEventHandle_t798447448::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize965 = { sizeof (RuntimePropertyHandle_t3120528081)+ sizeof (Il2CppObject), sizeof(RuntimePropertyHandle_t3120528081 ), 0, 0 };
extern const int32_t g_FieldOffsetTable965[1] = 
{
	RuntimePropertyHandle_t3120528081::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize966 = { sizeof (RuntimeGPtrArrayHandle_t1303258952)+ sizeof (Il2CppObject), sizeof(RuntimeGPtrArrayHandle_t1303258952_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable966[1] = 
{
	RuntimeGPtrArrayHandle_t1303258952::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize967 = { sizeof (RuntimeMarshal_t3798850754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize968 = { sizeof (RuntimeStructs_t3054717562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize969 = { sizeof (RemoteClass_t3863182804)+ sizeof (Il2CppObject), sizeof(RemoteClass_t3863182804_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable969[5] = 
{
	RemoteClass_t3863182804::get_offset_of_default_vtable_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RemoteClass_t3863182804::get_offset_of_xdomain_vtable_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RemoteClass_t3863182804::get_offset_of_proxy_class_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RemoteClass_t3863182804::get_offset_of_proxy_class_name_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RemoteClass_t3863182804::get_offset_of_interface_count_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize970 = { sizeof (MonoClass_t2595527713)+ sizeof (Il2CppObject), sizeof(MonoClass_t2595527713 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize971 = { sizeof (GenericParamInfo_t1377222196)+ sizeof (Il2CppObject), sizeof(GenericParamInfo_t1377222196_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable971[5] = 
{
	GenericParamInfo_t1377222196::get_offset_of_pklass_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GenericParamInfo_t1377222196::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GenericParamInfo_t1377222196::get_offset_of_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GenericParamInfo_t1377222196::get_offset_of_token_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GenericParamInfo_t1377222196::get_offset_of_constraints_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize972 = { sizeof (GPtrArray_t3128553612)+ sizeof (Il2CppObject), sizeof(GPtrArray_t3128553612_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable972[2] = 
{
	GPtrArray_t3128553612::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GPtrArray_t3128553612::get_offset_of_len_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize973 = { sizeof (MonoAssemblyName_t1886479188)+ sizeof (Il2CppObject), sizeof(MonoAssemblyName_t1886479188 ), 0, 0 };
extern const int32_t g_FieldOffsetTable973[13] = 
{
	MonoAssemblyName_t1886479188::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_culture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_hash_value_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_public_key_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_public_key_token_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_hash_alg_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_hash_len_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_flags_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_major_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_minor_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_build_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_revision_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MonoAssemblyName_t1886479188::get_offset_of_arch_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize974 = { sizeof (U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566)+ sizeof (Il2CppObject), sizeof(U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566 ), 0, 0 };
extern const int32_t g_FieldOffsetTable974[1] = 
{
	U3Cpublic_key_tokenU3E__FixedBuffer5_t1384907566::get_offset_of_FixedElementField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize975 = { sizeof (SafeGPtrArrayHandle_t547714345)+ sizeof (Il2CppObject), sizeof(SafeGPtrArrayHandle_t547714345_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable975[1] = 
{
	SafeGPtrArrayHandle_t547714345::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize976 = { sizeof (SafeStringMarshal_t2486501886)+ sizeof (Il2CppObject), sizeof(SafeStringMarshal_t2486501886_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable976[2] = 
{
	SafeStringMarshal_t2486501886::get_offset_of_str_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SafeStringMarshal_t2486501886::get_offset_of_marshaled_string_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize977 = { sizeof (AppContextSwitches_t4151153076), -1, sizeof(AppContextSwitches_t4151153076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable977[2] = 
{
	AppContextSwitches_t4151153076_StaticFields::get_offset_of_ThrowExceptionIfDisposedCancellationTokenSource_0(),
	AppContextSwitches_t4151153076_StaticFields::get_offset_of_NoAsyncCurrentCulture_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize978 = { sizeof (AppDomain_t2719102437), -1, sizeof(AppDomain_t2719102437_StaticFields), sizeof(AppDomain_t2719102437_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable978[23] = 
{
	AppDomain_t2719102437::get_offset_of__mono_app_domain_1(),
	AppDomain_t2719102437_StaticFields::get_offset_of__process_guid_2(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	AppDomain_t2719102437::get_offset_of__evidence_6(),
	AppDomain_t2719102437::get_offset_of__granted_7(),
	AppDomain_t2719102437::get_offset_of__principalPolicy_8(),
	THREAD_STATIC_FIELD_OFFSET,
	AppDomain_t2719102437_StaticFields::get_offset_of_default_domain_10(),
	AppDomain_t2719102437::get_offset_of_AssemblyLoad_11(),
	AppDomain_t2719102437::get_offset_of_AssemblyResolve_12(),
	AppDomain_t2719102437::get_offset_of_DomainUnload_13(),
	AppDomain_t2719102437::get_offset_of_ProcessExit_14(),
	AppDomain_t2719102437::get_offset_of_ResourceResolve_15(),
	AppDomain_t2719102437::get_offset_of_TypeResolve_16(),
	AppDomain_t2719102437::get_offset_of_UnhandledException_17(),
	AppDomain_t2719102437::get_offset_of_FirstChanceException_18(),
	AppDomain_t2719102437::get_offset_of__domain_manager_19(),
	AppDomain_t2719102437::get_offset_of_ReflectionOnlyAssemblyResolve_20(),
	AppDomain_t2719102437::get_offset_of__activation_21(),
	AppDomain_t2719102437::get_offset_of__applicationIdentity_22(),
	AppDomain_t2719102437::get_offset_of_compatibility_switch_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize979 = { sizeof (CLRConfig_t500485483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize980 = { sizeof (CultureData_t3400086592), -1, sizeof(CultureData_t3400086592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable980[21] = 
{
	CultureData_t3400086592::get_offset_of_sAM1159_0(),
	CultureData_t3400086592::get_offset_of_sPM2359_1(),
	CultureData_t3400086592::get_offset_of_sTimeSeparator_2(),
	CultureData_t3400086592::get_offset_of_saLongTimes_3(),
	CultureData_t3400086592::get_offset_of_saShortTimes_4(),
	CultureData_t3400086592::get_offset_of_iFirstDayOfWeek_5(),
	CultureData_t3400086592::get_offset_of_iFirstWeekOfYear_6(),
	CultureData_t3400086592::get_offset_of_waCalendars_7(),
	CultureData_t3400086592::get_offset_of_calendars_8(),
	CultureData_t3400086592::get_offset_of_sISO639Language_9(),
	CultureData_t3400086592::get_offset_of_sRealName_10(),
	CultureData_t3400086592::get_offset_of_bUseOverrides_11(),
	CultureData_t3400086592::get_offset_of_calendarId_12(),
	CultureData_t3400086592::get_offset_of_numberIndex_13(),
	CultureData_t3400086592::get_offset_of_iDefaultAnsiCodePage_14(),
	CultureData_t3400086592::get_offset_of_iDefaultOemCodePage_15(),
	CultureData_t3400086592::get_offset_of_iDefaultMacCodePage_16(),
	CultureData_t3400086592::get_offset_of_iDefaultEbcdicCodePage_17(),
	CultureData_t3400086592::get_offset_of_isRightToLeft_18(),
	CultureData_t3400086592::get_offset_of_sListSeparator_19(),
	CultureData_t3400086592_StaticFields::get_offset_of_s_Invariant_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize981 = { sizeof (CodePageDataItem_t2022420531), -1, sizeof(CodePageDataItem_t2022420531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable981[5] = 
{
	CodePageDataItem_t2022420531::get_offset_of_m_dataIndex_0(),
	CodePageDataItem_t2022420531::get_offset_of_m_uiFamilyCodePage_1(),
	CodePageDataItem_t2022420531::get_offset_of_m_webName_2(),
	CodePageDataItem_t2022420531::get_offset_of_m_flags_3(),
	CodePageDataItem_t2022420531_StaticFields::get_offset_of_sep_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize982 = { sizeof (EncodingTable_t1040637825), -1, sizeof(EncodingTable_t1040637825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable982[5] = 
{
	EncodingTable_t1040637825_StaticFields::get_offset_of_encodingDataPtr_0(),
	EncodingTable_t1040637825_StaticFields::get_offset_of_codePageDataPtr_1(),
	EncodingTable_t1040637825_StaticFields::get_offset_of_lastEncodingItem_2(),
	EncodingTable_t1040637825_StaticFields::get_offset_of_hashByName_3(),
	EncodingTable_t1040637825_StaticFields::get_offset_of_hashByCodePage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize983 = { sizeof (InternalEncodingDataItem_t82919681)+ sizeof (Il2CppObject), sizeof(InternalEncodingDataItem_t82919681_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable983[2] = 
{
	InternalEncodingDataItem_t82919681::get_offset_of_webName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalEncodingDataItem_t82919681::get_offset_of_codePage_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize984 = { sizeof (InternalCodePageDataItem_t1742109366)+ sizeof (Il2CppObject), sizeof(InternalCodePageDataItem_t1742109366_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable984[4] = 
{
	InternalCodePageDataItem_t1742109366::get_offset_of_codePage_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalCodePageDataItem_t1742109366::get_offset_of_uiFamilyCodePage_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalCodePageDataItem_t1742109366::get_offset_of_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalCodePageDataItem_t1742109366::get_offset_of_Names_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize985 = { sizeof (Environment_t3662374671), -1, sizeof(Environment_t3662374671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable985[3] = 
{
	0,
	Environment_t3662374671_StaticFields::get_offset_of_nl_1(),
	Environment_t3662374671_StaticFields::get_offset_of_os_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize986 = { sizeof (SpecialFolder_t1519540278)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable986[48] = 
{
	SpecialFolder_t1519540278::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize987 = { sizeof (SpecialFolderOption_t2597972107)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable987[4] = 
{
	SpecialFolderOption_t2597972107::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize988 = { sizeof (JitHelpers_t643465074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize989 = { sizeof (ParseNumbers_t3923564175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize990 = { sizeof (PathInternal_t1852797604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize991 = { sizeof (MonoTypeInfo_t1976057079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable991[2] = 
{
	MonoTypeInfo_t1976057079::get_offset_of_full_name_0(),
	MonoTypeInfo_t1976057079::get_offset_of_default_ctor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize992 = { sizeof (TextInfoToUpperData_t983078748), -1, sizeof(TextInfoToUpperData_t983078748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable992[8] = 
{
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_00e0_0586_0(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_1e01_1ff3_1(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_2170_2184_2(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_24d0_24e9_3(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_2c30_2ce3_4(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_2d00_2d25_5(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_a641_a697_6(),
	TextInfoToUpperData_t983078748_StaticFields::get_offset_of_range_a723_a78c_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize993 = { sizeof (TextInfoToLowerData_t630797643), -1, sizeof(TextInfoToLowerData_t630797643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable993[9] = 
{
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_00c0_0556_0(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_10a0_10c5_1(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_1e00_1ffc_2(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_2160_216f_3(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_24b6_24cf_4(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_2c00_2c2e_5(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_2c60_2ce2_6(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_a640_a696_7(),
	TextInfoToLowerData_t630797643_StaticFields::get_offset_of_range_a722_a78b_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize994 = { sizeof (TypeBuilderInstantiation_t3297116808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize995 = { sizeof (TypeNameParser_t95097906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize996 = { sizeof (AssemblyHashAlgorithm_t4147282775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable996[7] = 
{
	AssemblyHashAlgorithm_t4147282775::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize997 = { sizeof (AssemblyVersionCompatibility_t1223556284)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable997[4] = 
{
	AssemblyVersionCompatibility_t1223556284::get_offset_of_value___2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize998 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize999 = { sizeof (Debugger_t4214909883), -1, sizeof(Debugger_t4214909883_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable999[1] = 
{
	Debugger_t4214909883_StaticFields::get_offset_of_DefaultCategory_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
