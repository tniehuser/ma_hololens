﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_Compositor2211431003.h"

// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t287209776;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaExternal
struct  XmlSchemaExternal_t3943748629  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaExternal::location
	String_t* ___location_6;
	// System.Uri System.Xml.Schema.XmlSchemaExternal::baseUri
	Uri_t19570940 * ___baseUri_7;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaExternal::schema
	XmlSchema_t880472818 * ___schema_8;
	// System.String System.Xml.Schema.XmlSchemaExternal::id
	String_t* ___id_9;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaExternal::moreAttributes
	XmlAttributeU5BU5D_t287209776* ___moreAttributes_10;
	// System.Xml.Schema.Compositor System.Xml.Schema.XmlSchemaExternal::compositor
	int32_t ___compositor_11;

public:
	inline static int32_t get_offset_of_location_6() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___location_6)); }
	inline String_t* get_location_6() const { return ___location_6; }
	inline String_t** get_address_of_location_6() { return &___location_6; }
	inline void set_location_6(String_t* value)
	{
		___location_6 = value;
		Il2CppCodeGenWriteBarrier(&___location_6, value);
	}

	inline static int32_t get_offset_of_baseUri_7() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___baseUri_7)); }
	inline Uri_t19570940 * get_baseUri_7() const { return ___baseUri_7; }
	inline Uri_t19570940 ** get_address_of_baseUri_7() { return &___baseUri_7; }
	inline void set_baseUri_7(Uri_t19570940 * value)
	{
		___baseUri_7 = value;
		Il2CppCodeGenWriteBarrier(&___baseUri_7, value);
	}

	inline static int32_t get_offset_of_schema_8() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___schema_8)); }
	inline XmlSchema_t880472818 * get_schema_8() const { return ___schema_8; }
	inline XmlSchema_t880472818 ** get_address_of_schema_8() { return &___schema_8; }
	inline void set_schema_8(XmlSchema_t880472818 * value)
	{
		___schema_8 = value;
		Il2CppCodeGenWriteBarrier(&___schema_8, value);
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___id_9)); }
	inline String_t* get_id_9() const { return ___id_9; }
	inline String_t** get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(String_t* value)
	{
		___id_9 = value;
		Il2CppCodeGenWriteBarrier(&___id_9, value);
	}

	inline static int32_t get_offset_of_moreAttributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___moreAttributes_10)); }
	inline XmlAttributeU5BU5D_t287209776* get_moreAttributes_10() const { return ___moreAttributes_10; }
	inline XmlAttributeU5BU5D_t287209776** get_address_of_moreAttributes_10() { return &___moreAttributes_10; }
	inline void set_moreAttributes_10(XmlAttributeU5BU5D_t287209776* value)
	{
		___moreAttributes_10 = value;
		Il2CppCodeGenWriteBarrier(&___moreAttributes_10, value);
	}

	inline static int32_t get_offset_of_compositor_11() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___compositor_11)); }
	inline int32_t get_compositor_11() const { return ___compositor_11; }
	inline int32_t* get_address_of_compositor_11() { return &___compositor_11; }
	inline void set_compositor_11(int32_t value)
	{
		___compositor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
