﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mono.Net.Dns.SimpleResolver
struct SimpleResolver_t2933968162;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns
struct  Dns_t1335526197  : public Il2CppObject
{
public:

public:
};

struct Dns_t1335526197_StaticFields
{
public:
	// System.Boolean System.Net.Dns::use_mono_dns
	bool ___use_mono_dns_0;
	// Mono.Net.Dns.SimpleResolver System.Net.Dns::resolver
	SimpleResolver_t2933968162 * ___resolver_1;

public:
	inline static int32_t get_offset_of_use_mono_dns_0() { return static_cast<int32_t>(offsetof(Dns_t1335526197_StaticFields, ___use_mono_dns_0)); }
	inline bool get_use_mono_dns_0() const { return ___use_mono_dns_0; }
	inline bool* get_address_of_use_mono_dns_0() { return &___use_mono_dns_0; }
	inline void set_use_mono_dns_0(bool value)
	{
		___use_mono_dns_0 = value;
	}

	inline static int32_t get_offset_of_resolver_1() { return static_cast<int32_t>(offsetof(Dns_t1335526197_StaticFields, ___resolver_1)); }
	inline SimpleResolver_t2933968162 * get_resolver_1() const { return ___resolver_1; }
	inline SimpleResolver_t2933968162 ** get_address_of_resolver_1() { return &___resolver_1; }
	inline void set_resolver_1(SimpleResolver_t2933968162 * value)
	{
		___resolver_1 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
