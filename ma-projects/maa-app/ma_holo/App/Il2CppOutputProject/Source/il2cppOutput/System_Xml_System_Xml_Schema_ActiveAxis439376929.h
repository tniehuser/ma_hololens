﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.Asttree
struct Asttree_t3451058494;
// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ActiveAxis
struct  ActiveAxis_t439376929  : public Il2CppObject
{
public:
	// System.Int32 System.Xml.Schema.ActiveAxis::currentDepth
	int32_t ___currentDepth_0;
	// System.Boolean System.Xml.Schema.ActiveAxis::isActive
	bool ___isActive_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.ActiveAxis::axisTree
	Asttree_t3451058494 * ___axisTree_2;
	// System.Collections.ArrayList System.Xml.Schema.ActiveAxis::axisStack
	ArrayList_t4252133567 * ___axisStack_3;

public:
	inline static int32_t get_offset_of_currentDepth_0() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___currentDepth_0)); }
	inline int32_t get_currentDepth_0() const { return ___currentDepth_0; }
	inline int32_t* get_address_of_currentDepth_0() { return &___currentDepth_0; }
	inline void set_currentDepth_0(int32_t value)
	{
		___currentDepth_0 = value;
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}

	inline static int32_t get_offset_of_axisTree_2() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___axisTree_2)); }
	inline Asttree_t3451058494 * get_axisTree_2() const { return ___axisTree_2; }
	inline Asttree_t3451058494 ** get_address_of_axisTree_2() { return &___axisTree_2; }
	inline void set_axisTree_2(Asttree_t3451058494 * value)
	{
		___axisTree_2 = value;
		Il2CppCodeGenWriteBarrier(&___axisTree_2, value);
	}

	inline static int32_t get_offset_of_axisStack_3() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___axisStack_3)); }
	inline ArrayList_t4252133567 * get_axisStack_3() const { return ___axisStack_3; }
	inline ArrayList_t4252133567 ** get_address_of_axisStack_3() { return &___axisStack_3; }
	inline void set_axisStack_3(ArrayList_t4252133567 * value)
	{
		___axisStack_3 = value;
		Il2CppCodeGenWriteBarrier(&___axisStack_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
