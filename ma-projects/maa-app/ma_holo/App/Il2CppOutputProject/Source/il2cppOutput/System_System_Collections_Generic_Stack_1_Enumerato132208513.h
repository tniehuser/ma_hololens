﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3777177449;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct  Enumerator_t132208513 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator::_stack
	Stack_1_t3777177449 * ____stack_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.Stack`1/Enumerator::currentElement
	Il2CppObject * ___currentElement_3;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(Enumerator_t132208513, ____stack_0)); }
	inline Stack_1_t3777177449 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t3777177449 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t3777177449 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier(&____stack_0, value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_t132208513, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t132208513, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of_currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_t132208513, ___currentElement_3)); }
	inline Il2CppObject * get_currentElement_3() const { return ___currentElement_3; }
	inline Il2CppObject ** get_address_of_currentElement_3() { return &___currentElement_3; }
	inline void set_currentElement_3(Il2CppObject * value)
	{
		___currentElement_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentElement_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
