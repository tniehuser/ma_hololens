﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_Security_Cryptography_X509Certificat1197680765.h"

// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct  X509Certificate2Collection_t1108969367  : public X509CertificateCollection_t1197680765
{
public:

public:
};

struct X509Certificate2Collection_t1108969367_StaticFields
{
public:
	// System.String[] System.Security.Cryptography.X509Certificates.X509Certificate2Collection::newline_split
	StringU5BU5D_t1642385972* ___newline_split_1;

public:
	inline static int32_t get_offset_of_newline_split_1() { return static_cast<int32_t>(offsetof(X509Certificate2Collection_t1108969367_StaticFields, ___newline_split_1)); }
	inline StringU5BU5D_t1642385972* get_newline_split_1() const { return ___newline_split_1; }
	inline StringU5BU5D_t1642385972** get_address_of_newline_split_1() { return &___newline_split_1; }
	inline void set_newline_split_1(StringU5BU5D_t1642385972* value)
	{
		___newline_split_1 = value;
		Il2CppCodeGenWriteBarrier(&___newline_split_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
