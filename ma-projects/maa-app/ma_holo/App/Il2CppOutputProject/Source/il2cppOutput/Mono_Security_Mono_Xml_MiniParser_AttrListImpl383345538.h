﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.MiniParser/AttrListImpl
struct  AttrListImpl_t383345538  : public Il2CppObject
{
public:
	// System.Collections.ArrayList Mono.Xml.MiniParser/AttrListImpl::names
	ArrayList_t4252133567 * ___names_0;
	// System.Collections.ArrayList Mono.Xml.MiniParser/AttrListImpl::values
	ArrayList_t4252133567 * ___values_1;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(AttrListImpl_t383345538, ___names_0)); }
	inline ArrayList_t4252133567 * get_names_0() const { return ___names_0; }
	inline ArrayList_t4252133567 ** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(ArrayList_t4252133567 * value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier(&___names_0, value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(AttrListImpl_t383345538, ___values_1)); }
	inline ArrayList_t4252133567 * get_values_1() const { return ___values_1; }
	inline ArrayList_t4252133567 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t4252133567 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier(&___values_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
