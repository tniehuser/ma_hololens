﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.XmlSchemaParticle[]
struct XmlSchemaParticleU5BU5D_t4022221223;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t3434391819;
// System.Boolean[0...,0...]
struct BooleanU5BU2CU5D_t3568034316;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidator
struct  XmlSchemaValidator_t1978690704  : public Il2CppObject
{
public:

public:
};

struct XmlSchemaValidator_t1978690704_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtQName
	XmlSchemaDatatype_t1195946242 * ___dtQName_0;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtCDATA
	XmlSchemaDatatype_t1195946242 * ___dtCDATA_1;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::dtStringArray
	XmlSchemaDatatype_t1195946242 * ___dtStringArray_2;
	// System.Xml.Schema.XmlSchemaParticle[] System.Xml.Schema.XmlSchemaValidator::EmptyParticleArray
	XmlSchemaParticleU5BU5D_t4022221223* ___EmptyParticleArray_3;
	// System.Xml.Schema.XmlSchemaAttribute[] System.Xml.Schema.XmlSchemaValidator::EmptyAttributeArray
	XmlSchemaAttributeU5BU5D_t3434391819* ___EmptyAttributeArray_4;
	// System.Boolean[0...,0...] System.Xml.Schema.XmlSchemaValidator::ValidStates
	BooleanU5BU2CU5D_t3568034316* ___ValidStates_5;
	// System.String[] System.Xml.Schema.XmlSchemaValidator::MethodNames
	StringU5BU5D_t1642385972* ___MethodNames_6;

public:
	inline static int32_t get_offset_of_dtQName_0() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___dtQName_0)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtQName_0() const { return ___dtQName_0; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtQName_0() { return &___dtQName_0; }
	inline void set_dtQName_0(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtQName_0 = value;
		Il2CppCodeGenWriteBarrier(&___dtQName_0, value);
	}

	inline static int32_t get_offset_of_dtCDATA_1() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___dtCDATA_1)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtCDATA_1() const { return ___dtCDATA_1; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtCDATA_1() { return &___dtCDATA_1; }
	inline void set_dtCDATA_1(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtCDATA_1 = value;
		Il2CppCodeGenWriteBarrier(&___dtCDATA_1, value);
	}

	inline static int32_t get_offset_of_dtStringArray_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___dtStringArray_2)); }
	inline XmlSchemaDatatype_t1195946242 * get_dtStringArray_2() const { return ___dtStringArray_2; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_dtStringArray_2() { return &___dtStringArray_2; }
	inline void set_dtStringArray_2(XmlSchemaDatatype_t1195946242 * value)
	{
		___dtStringArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___dtStringArray_2, value);
	}

	inline static int32_t get_offset_of_EmptyParticleArray_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___EmptyParticleArray_3)); }
	inline XmlSchemaParticleU5BU5D_t4022221223* get_EmptyParticleArray_3() const { return ___EmptyParticleArray_3; }
	inline XmlSchemaParticleU5BU5D_t4022221223** get_address_of_EmptyParticleArray_3() { return &___EmptyParticleArray_3; }
	inline void set_EmptyParticleArray_3(XmlSchemaParticleU5BU5D_t4022221223* value)
	{
		___EmptyParticleArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyParticleArray_3, value);
	}

	inline static int32_t get_offset_of_EmptyAttributeArray_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___EmptyAttributeArray_4)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_EmptyAttributeArray_4() const { return ___EmptyAttributeArray_4; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_EmptyAttributeArray_4() { return &___EmptyAttributeArray_4; }
	inline void set_EmptyAttributeArray_4(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___EmptyAttributeArray_4 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyAttributeArray_4, value);
	}

	inline static int32_t get_offset_of_ValidStates_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___ValidStates_5)); }
	inline BooleanU5BU2CU5D_t3568034316* get_ValidStates_5() const { return ___ValidStates_5; }
	inline BooleanU5BU2CU5D_t3568034316** get_address_of_ValidStates_5() { return &___ValidStates_5; }
	inline void set_ValidStates_5(BooleanU5BU2CU5D_t3568034316* value)
	{
		___ValidStates_5 = value;
		Il2CppCodeGenWriteBarrier(&___ValidStates_5, value);
	}

	inline static int32_t get_offset_of_MethodNames_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___MethodNames_6)); }
	inline StringU5BU5D_t1642385972* get_MethodNames_6() const { return ___MethodNames_6; }
	inline StringU5BU5D_t1642385972** get_address_of_MethodNames_6() { return &___MethodNames_6; }
	inline void set_MethodNames_6(StringU5BU5D_t1642385972* value)
	{
		___MethodNames_6 = value;
		Il2CppCodeGenWriteBarrier(&___MethodNames_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
