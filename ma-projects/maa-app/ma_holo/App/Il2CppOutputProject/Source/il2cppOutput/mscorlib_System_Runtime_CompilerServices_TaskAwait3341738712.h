﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Threading.Tasks.Task
struct Task_t1843236107;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t3341738712 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1843236107 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t3341738712, ___m_task_0)); }
	inline Task_t1843236107 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1843236107 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1843236107 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_task_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3341738712_marshaled_pinvoke
{
	Task_t1843236107 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3341738712_marshaled_com
{
	Task_t1843236107 * ___m_task_0;
};
