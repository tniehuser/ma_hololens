﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// Mono.Net.CFDictionary
struct CFDictionary_t3548969133;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxySettings
struct  CFProxySettings_t4092929580  : public Il2CppObject
{
public:
	// Mono.Net.CFDictionary Mono.Net.CFProxySettings::settings
	CFDictionary_t3548969133 * ___settings_6;

public:
	inline static int32_t get_offset_of_settings_6() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580, ___settings_6)); }
	inline CFDictionary_t3548969133 * get_settings_6() const { return ___settings_6; }
	inline CFDictionary_t3548969133 ** get_address_of_settings_6() { return &___settings_6; }
	inline void set_settings_6(CFDictionary_t3548969133 * value)
	{
		___settings_6 = value;
		Il2CppCodeGenWriteBarrier(&___settings_6, value);
	}
};

struct CFProxySettings_t4092929580_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPEnable
	IntPtr_t ___kCFNetworkProxiesHTTPEnable_0;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPPort
	IntPtr_t ___kCFNetworkProxiesHTTPPort_1;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPProxy
	IntPtr_t ___kCFNetworkProxiesHTTPProxy_2;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigEnable
	IntPtr_t ___kCFNetworkProxiesProxyAutoConfigEnable_3;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigJavaScript
	IntPtr_t ___kCFNetworkProxiesProxyAutoConfigJavaScript_4;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigURLString
	IntPtr_t ___kCFNetworkProxiesProxyAutoConfigURLString_5;

public:
	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPEnable_0() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPEnable_0)); }
	inline IntPtr_t get_kCFNetworkProxiesHTTPEnable_0() const { return ___kCFNetworkProxiesHTTPEnable_0; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesHTTPEnable_0() { return &___kCFNetworkProxiesHTTPEnable_0; }
	inline void set_kCFNetworkProxiesHTTPEnable_0(IntPtr_t value)
	{
		___kCFNetworkProxiesHTTPEnable_0 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPPort_1() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPPort_1)); }
	inline IntPtr_t get_kCFNetworkProxiesHTTPPort_1() const { return ___kCFNetworkProxiesHTTPPort_1; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesHTTPPort_1() { return &___kCFNetworkProxiesHTTPPort_1; }
	inline void set_kCFNetworkProxiesHTTPPort_1(IntPtr_t value)
	{
		___kCFNetworkProxiesHTTPPort_1 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPProxy_2() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPProxy_2)); }
	inline IntPtr_t get_kCFNetworkProxiesHTTPProxy_2() const { return ___kCFNetworkProxiesHTTPProxy_2; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesHTTPProxy_2() { return &___kCFNetworkProxiesHTTPProxy_2; }
	inline void set_kCFNetworkProxiesHTTPProxy_2(IntPtr_t value)
	{
		___kCFNetworkProxiesHTTPProxy_2 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigEnable_3)); }
	inline IntPtr_t get_kCFNetworkProxiesProxyAutoConfigEnable_3() const { return ___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return &___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline void set_kCFNetworkProxiesProxyAutoConfigEnable_3(IntPtr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigEnable_3 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigJavaScript_4)); }
	inline IntPtr_t get_kCFNetworkProxiesProxyAutoConfigJavaScript_4() const { return ___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return &___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline void set_kCFNetworkProxiesProxyAutoConfigJavaScript_4(IntPtr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigJavaScript_4 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigURLString_5)); }
	inline IntPtr_t get_kCFNetworkProxiesProxyAutoConfigURLString_5() const { return ___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline IntPtr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return &___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline void set_kCFNetworkProxiesProxyAutoConfigURLString_5(IntPtr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigURLString_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
