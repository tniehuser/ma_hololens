﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"

// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNamespaceManager
struct  SchemaNamespaceManager_t3179848951  : public XmlNamespaceManager_t486731501
{
public:
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.SchemaNamespaceManager::node
	XmlSchemaObject_t2050913741 * ___node_8;

public:
	inline static int32_t get_offset_of_node_8() { return static_cast<int32_t>(offsetof(SchemaNamespaceManager_t3179848951, ___node_8)); }
	inline XmlSchemaObject_t2050913741 * get_node_8() const { return ___node_8; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_node_8() { return &___node_8; }
	inline void set_node_8(XmlSchemaObject_t2050913741 * value)
	{
		___node_8 = value;
		Il2CppCodeGenWriteBarrier(&___node_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
