﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522.h"

// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1153004758;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DES
struct  DES_t1353513560  : public SymmetricAlgorithm_t1108166522
{
public:

public:
};

struct DES_t1353513560_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.DES::s_legalBlockSizes
	KeySizesU5BU5D_t1153004758* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.DES::s_legalKeySizes
	KeySizesU5BU5D_t1153004758* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(DES_t1353513560_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t1153004758* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t1153004758** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t1153004758* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier(&___s_legalBlockSizes_9, value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(DES_t1353513560_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t1153004758* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t1153004758** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t1153004758* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier(&___s_legalKeySizes_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
