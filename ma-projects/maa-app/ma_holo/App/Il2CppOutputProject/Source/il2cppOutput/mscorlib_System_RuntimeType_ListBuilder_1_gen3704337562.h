﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.MethodInfo
struct MethodInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeType/ListBuilder`1<System.Reflection.MethodInfo>
struct  ListBuilder_1_t3704337562 
{
public:
	// T[] System.RuntimeType/ListBuilder`1::_items
	MethodInfoU5BU5D_t152480188* ____items_0;
	// T System.RuntimeType/ListBuilder`1::_item
	MethodInfo_t * ____item_1;
	// System.Int32 System.RuntimeType/ListBuilder`1::_count
	int32_t ____count_2;
	// System.Int32 System.RuntimeType/ListBuilder`1::_capacity
	int32_t ____capacity_3;

public:
	inline static int32_t get_offset_of__items_0() { return static_cast<int32_t>(offsetof(ListBuilder_1_t3704337562, ____items_0)); }
	inline MethodInfoU5BU5D_t152480188* get__items_0() const { return ____items_0; }
	inline MethodInfoU5BU5D_t152480188** get_address_of__items_0() { return &____items_0; }
	inline void set__items_0(MethodInfoU5BU5D_t152480188* value)
	{
		____items_0 = value;
		Il2CppCodeGenWriteBarrier(&____items_0, value);
	}

	inline static int32_t get_offset_of__item_1() { return static_cast<int32_t>(offsetof(ListBuilder_1_t3704337562, ____item_1)); }
	inline MethodInfo_t * get__item_1() const { return ____item_1; }
	inline MethodInfo_t ** get_address_of__item_1() { return &____item_1; }
	inline void set__item_1(MethodInfo_t * value)
	{
		____item_1 = value;
		Il2CppCodeGenWriteBarrier(&____item_1, value);
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ListBuilder_1_t3704337562, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}

	inline static int32_t get_offset_of__capacity_3() { return static_cast<int32_t>(offsetof(ListBuilder_1_t3704337562, ____capacity_3)); }
	inline int32_t get__capacity_3() const { return ____capacity_3; }
	inline int32_t* get_address_of__capacity_3() { return &____capacity_3; }
	inline void set__capacity_3(int32_t value)
	{
		____capacity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
