﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OptionalFieldAttribute
struct  OptionalFieldAttribute_t124318366  : public Attribute_t542643598
{
public:
	// System.Int32 System.Runtime.Serialization.OptionalFieldAttribute::versionAdded
	int32_t ___versionAdded_0;

public:
	inline static int32_t get_offset_of_versionAdded_0() { return static_cast<int32_t>(offsetof(OptionalFieldAttribute_t124318366, ___versionAdded_0)); }
	inline int32_t get_versionAdded_0() const { return ___versionAdded_0; }
	inline int32_t* get_address_of_versionAdded_0() { return &___versionAdded_0; }
	inline void set_versionAdded_0(int32_t value)
	{
		___versionAdded_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
