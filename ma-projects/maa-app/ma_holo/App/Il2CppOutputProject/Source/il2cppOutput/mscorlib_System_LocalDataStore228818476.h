﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.LocalDataStoreElement[]
struct LocalDataStoreElementU5BU5D_t30916843;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1152954092;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalDataStore
struct  LocalDataStore_t228818476  : public Il2CppObject
{
public:
	// System.LocalDataStoreElement[] System.LocalDataStore::m_DataTable
	LocalDataStoreElementU5BU5D_t30916843* ___m_DataTable_0;
	// System.LocalDataStoreMgr System.LocalDataStore::m_Manager
	LocalDataStoreMgr_t1152954092 * ___m_Manager_1;

public:
	inline static int32_t get_offset_of_m_DataTable_0() { return static_cast<int32_t>(offsetof(LocalDataStore_t228818476, ___m_DataTable_0)); }
	inline LocalDataStoreElementU5BU5D_t30916843* get_m_DataTable_0() const { return ___m_DataTable_0; }
	inline LocalDataStoreElementU5BU5D_t30916843** get_address_of_m_DataTable_0() { return &___m_DataTable_0; }
	inline void set_m_DataTable_0(LocalDataStoreElementU5BU5D_t30916843* value)
	{
		___m_DataTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_DataTable_0, value);
	}

	inline static int32_t get_offset_of_m_Manager_1() { return static_cast<int32_t>(offsetof(LocalDataStore_t228818476, ___m_Manager_1)); }
	inline LocalDataStoreMgr_t1152954092 * get_m_Manager_1() const { return ___m_Manager_1; }
	inline LocalDataStoreMgr_t1152954092 ** get_address_of_m_Manager_1() { return &___m_Manager_1; }
	inline void set_m_Manager_1(LocalDataStoreMgr_t1152954092 * value)
	{
		___m_Manager_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Manager_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
