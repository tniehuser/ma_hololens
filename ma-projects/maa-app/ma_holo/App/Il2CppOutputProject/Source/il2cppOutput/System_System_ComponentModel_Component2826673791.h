﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MarshalByRefObject1285298191.h"

// System.Object
struct Il2CppObject;
// System.ComponentModel.ISite
struct ISite_t1774720436;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1298116880;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t2826673791  : public MarshalByRefObject_t1285298191
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	Il2CppObject * ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_t1298116880 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t2826673791, ___site_2)); }
	inline Il2CppObject * get_site_2() const { return ___site_2; }
	inline Il2CppObject ** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(Il2CppObject * value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier(&___site_2, value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t2826673791, ___events_3)); }
	inline EventHandlerList_t1298116880 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_t1298116880 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_t1298116880 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier(&___events_3, value);
	}
};

struct Component_t2826673791_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	Il2CppObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t2826673791_StaticFields, ___EventDisposed_1)); }
	inline Il2CppObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline Il2CppObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(Il2CppObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier(&___EventDisposed_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
