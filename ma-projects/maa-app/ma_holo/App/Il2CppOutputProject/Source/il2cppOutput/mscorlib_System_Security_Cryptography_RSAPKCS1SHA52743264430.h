﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign2452358846.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1SHA512SignatureDescription
struct  RSAPKCS1SHA512SignatureDescription_t2743264430  : public RSAPKCS1SignatureDescription_t2452358846
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
