﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_Generic_List_1_gen4155544979.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityPr155967584.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CipherSuiteCollection
struct  CipherSuiteCollection_t2431504453  : public List_1_t4155544979
{
public:
	// Mono.Security.Protocol.Tls.SecurityProtocolType Mono.Security.Protocol.Tls.CipherSuiteCollection::protocol
	int32_t ___protocol_6;

public:
	inline static int32_t get_offset_of_protocol_6() { return static_cast<int32_t>(offsetof(CipherSuiteCollection_t2431504453, ___protocol_6)); }
	inline int32_t get_protocol_6() const { return ___protocol_6; }
	inline int32_t* get_address_of_protocol_6() { return &___protocol_6; }
	inline void set_protocol_6(int32_t value)
	{
		___protocol_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
