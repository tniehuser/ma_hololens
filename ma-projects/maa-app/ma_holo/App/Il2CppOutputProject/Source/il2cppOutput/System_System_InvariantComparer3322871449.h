﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Globalization.CompareInfo
struct CompareInfo_t2310920157;
// System.InvariantComparer
struct InvariantComparer_t3322871449;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvariantComparer
struct  InvariantComparer_t3322871449  : public Il2CppObject
{
public:
	// System.Globalization.CompareInfo System.InvariantComparer::m_compareInfo
	CompareInfo_t2310920157 * ___m_compareInfo_0;

public:
	inline static int32_t get_offset_of_m_compareInfo_0() { return static_cast<int32_t>(offsetof(InvariantComparer_t3322871449, ___m_compareInfo_0)); }
	inline CompareInfo_t2310920157 * get_m_compareInfo_0() const { return ___m_compareInfo_0; }
	inline CompareInfo_t2310920157 ** get_address_of_m_compareInfo_0() { return &___m_compareInfo_0; }
	inline void set_m_compareInfo_0(CompareInfo_t2310920157 * value)
	{
		___m_compareInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_compareInfo_0, value);
	}
};

struct InvariantComparer_t3322871449_StaticFields
{
public:
	// System.InvariantComparer System.InvariantComparer::Default
	InvariantComparer_t3322871449 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(InvariantComparer_t3322871449_StaticFields, ___Default_1)); }
	inline InvariantComparer_t3322871449 * get_Default_1() const { return ___Default_1; }
	inline InvariantComparer_t3322871449 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(InvariantComparer_t3322871449 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier(&___Default_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
