﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ArgumentException3259014390.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.DecoderFallbackException
struct  DecoderFallbackException_t561423693  : public ArgumentException_t3259014390
{
public:
	// System.Byte[] System.Text.DecoderFallbackException::bytesUnknown
	ByteU5BU5D_t3397334013* ___bytesUnknown_17;
	// System.Int32 System.Text.DecoderFallbackException::index
	int32_t ___index_18;

public:
	inline static int32_t get_offset_of_bytesUnknown_17() { return static_cast<int32_t>(offsetof(DecoderFallbackException_t561423693, ___bytesUnknown_17)); }
	inline ByteU5BU5D_t3397334013* get_bytesUnknown_17() const { return ___bytesUnknown_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytesUnknown_17() { return &___bytesUnknown_17; }
	inline void set_bytesUnknown_17(ByteU5BU5D_t3397334013* value)
	{
		___bytesUnknown_17 = value;
		Il2CppCodeGenWriteBarrier(&___bytesUnknown_17, value);
	}

	inline static int32_t get_offset_of_index_18() { return static_cast<int32_t>(offsetof(DecoderFallbackException_t561423693, ___index_18)); }
	inline int32_t get_index_18() const { return ___index_18; }
	inline int32_t* get_address_of_index_18() { return &___index_18; }
	inline void set_index_18(int32_t value)
	{
		___index_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
