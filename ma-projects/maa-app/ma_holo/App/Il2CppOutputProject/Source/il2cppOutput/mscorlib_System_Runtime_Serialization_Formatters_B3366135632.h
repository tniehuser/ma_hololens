﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1992316134.h"

// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Exception
struct Exception_t1927440687;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn
struct  BinaryMethodReturn_t3366135632  : public Il2CppObject
{
public:
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::returnValue
	Il2CppObject * ___returnValue_0;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::args
	ObjectU5BU5D_t3614634134* ___args_1;
	// System.Exception System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::exception
	Exception_t1927440687 * ___exception_2;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::callContext
	Il2CppObject * ___callContext_3;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::scallContext
	String_t* ___scallContext_4;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::properties
	Il2CppObject * ___properties_5;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::argTypes
	TypeU5BU5D_t1664964607* ___argTypes_6;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::bArgsPrimitive
	bool ___bArgsPrimitive_7;
	// System.Runtime.Serialization.Formatters.Binary.MessageEnum System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::messageEnum
	int32_t ___messageEnum_8;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::callA
	ObjectU5BU5D_t3614634134* ___callA_9;
	// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::returnType
	Type_t * ___returnType_10;

public:
	inline static int32_t get_offset_of_returnValue_0() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___returnValue_0)); }
	inline Il2CppObject * get_returnValue_0() const { return ___returnValue_0; }
	inline Il2CppObject ** get_address_of_returnValue_0() { return &___returnValue_0; }
	inline void set_returnValue_0(Il2CppObject * value)
	{
		___returnValue_0 = value;
		Il2CppCodeGenWriteBarrier(&___returnValue_0, value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___args_1)); }
	inline ObjectU5BU5D_t3614634134* get_args_1() const { return ___args_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(ObjectU5BU5D_t3614634134* value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier(&___args_1, value);
	}

	inline static int32_t get_offset_of_exception_2() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___exception_2)); }
	inline Exception_t1927440687 * get_exception_2() const { return ___exception_2; }
	inline Exception_t1927440687 ** get_address_of_exception_2() { return &___exception_2; }
	inline void set_exception_2(Exception_t1927440687 * value)
	{
		___exception_2 = value;
		Il2CppCodeGenWriteBarrier(&___exception_2, value);
	}

	inline static int32_t get_offset_of_callContext_3() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___callContext_3)); }
	inline Il2CppObject * get_callContext_3() const { return ___callContext_3; }
	inline Il2CppObject ** get_address_of_callContext_3() { return &___callContext_3; }
	inline void set_callContext_3(Il2CppObject * value)
	{
		___callContext_3 = value;
		Il2CppCodeGenWriteBarrier(&___callContext_3, value);
	}

	inline static int32_t get_offset_of_scallContext_4() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___scallContext_4)); }
	inline String_t* get_scallContext_4() const { return ___scallContext_4; }
	inline String_t** get_address_of_scallContext_4() { return &___scallContext_4; }
	inline void set_scallContext_4(String_t* value)
	{
		___scallContext_4 = value;
		Il2CppCodeGenWriteBarrier(&___scallContext_4, value);
	}

	inline static int32_t get_offset_of_properties_5() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___properties_5)); }
	inline Il2CppObject * get_properties_5() const { return ___properties_5; }
	inline Il2CppObject ** get_address_of_properties_5() { return &___properties_5; }
	inline void set_properties_5(Il2CppObject * value)
	{
		___properties_5 = value;
		Il2CppCodeGenWriteBarrier(&___properties_5, value);
	}

	inline static int32_t get_offset_of_argTypes_6() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___argTypes_6)); }
	inline TypeU5BU5D_t1664964607* get_argTypes_6() const { return ___argTypes_6; }
	inline TypeU5BU5D_t1664964607** get_address_of_argTypes_6() { return &___argTypes_6; }
	inline void set_argTypes_6(TypeU5BU5D_t1664964607* value)
	{
		___argTypes_6 = value;
		Il2CppCodeGenWriteBarrier(&___argTypes_6, value);
	}

	inline static int32_t get_offset_of_bArgsPrimitive_7() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___bArgsPrimitive_7)); }
	inline bool get_bArgsPrimitive_7() const { return ___bArgsPrimitive_7; }
	inline bool* get_address_of_bArgsPrimitive_7() { return &___bArgsPrimitive_7; }
	inline void set_bArgsPrimitive_7(bool value)
	{
		___bArgsPrimitive_7 = value;
	}

	inline static int32_t get_offset_of_messageEnum_8() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___messageEnum_8)); }
	inline int32_t get_messageEnum_8() const { return ___messageEnum_8; }
	inline int32_t* get_address_of_messageEnum_8() { return &___messageEnum_8; }
	inline void set_messageEnum_8(int32_t value)
	{
		___messageEnum_8 = value;
	}

	inline static int32_t get_offset_of_callA_9() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___callA_9)); }
	inline ObjectU5BU5D_t3614634134* get_callA_9() const { return ___callA_9; }
	inline ObjectU5BU5D_t3614634134** get_address_of_callA_9() { return &___callA_9; }
	inline void set_callA_9(ObjectU5BU5D_t3614634134* value)
	{
		___callA_9 = value;
		Il2CppCodeGenWriteBarrier(&___callA_9, value);
	}

	inline static int32_t get_offset_of_returnType_10() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632, ___returnType_10)); }
	inline Type_t * get_returnType_10() const { return ___returnType_10; }
	inline Type_t ** get_address_of_returnType_10() { return &___returnType_10; }
	inline void set_returnType_10(Type_t * value)
	{
		___returnType_10 = value;
		Il2CppCodeGenWriteBarrier(&___returnType_10, value);
	}
};

struct BinaryMethodReturn_t3366135632_StaticFields
{
public:
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::instanceOfVoid
	Il2CppObject * ___instanceOfVoid_11;

public:
	inline static int32_t get_offset_of_instanceOfVoid_11() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t3366135632_StaticFields, ___instanceOfVoid_11)); }
	inline Il2CppObject * get_instanceOfVoid_11() const { return ___instanceOfVoid_11; }
	inline Il2CppObject ** get_address_of_instanceOfVoid_11() { return &___instanceOfVoid_11; }
	inline void set_instanceOfVoid_11(Il2CppObject * value)
	{
		___instanceOfVoid_11 = value;
		Il2CppCodeGenWriteBarrier(&___instanceOfVoid_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
