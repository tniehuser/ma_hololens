﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConfiguration.XmlConfigurationString
struct  XmlConfigurationString_t3722747970  : public Il2CppObject
{
public:

public:
};

struct XmlConfigurationString_t3722747970_StaticFields
{
public:
	// System.String System.Xml.XmlConfiguration.XmlConfigurationString::XmlReaderSectionPath
	String_t* ___XmlReaderSectionPath_0;
	// System.String System.Xml.XmlConfiguration.XmlConfigurationString::XsltSectionPath
	String_t* ___XsltSectionPath_1;

public:
	inline static int32_t get_offset_of_XmlReaderSectionPath_0() { return static_cast<int32_t>(offsetof(XmlConfigurationString_t3722747970_StaticFields, ___XmlReaderSectionPath_0)); }
	inline String_t* get_XmlReaderSectionPath_0() const { return ___XmlReaderSectionPath_0; }
	inline String_t** get_address_of_XmlReaderSectionPath_0() { return &___XmlReaderSectionPath_0; }
	inline void set_XmlReaderSectionPath_0(String_t* value)
	{
		___XmlReaderSectionPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___XmlReaderSectionPath_0, value);
	}

	inline static int32_t get_offset_of_XsltSectionPath_1() { return static_cast<int32_t>(offsetof(XmlConfigurationString_t3722747970_StaticFields, ___XsltSectionPath_1)); }
	inline String_t* get_XsltSectionPath_1() const { return ___XsltSectionPath_1; }
	inline String_t** get_address_of_XsltSectionPath_1() { return &___XsltSectionPath_1; }
	inline void set_XsltSectionPath_1(String_t* value)
	{
		___XsltSectionPath_1 = value;
		Il2CppCodeGenWriteBarrier(&___XsltSectionPath_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
